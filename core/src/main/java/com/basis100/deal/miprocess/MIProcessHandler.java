package com.basis100.deal.miprocess;

import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import mosApp.MosSystem.PageEntry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.TypeConverter;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFETrigger;
import com.basis100.workflow.notification.NotificationMonitor;
import com.basis100.workflow.notification.WFNotificationEvent;
import com.basis100.workflow.notification.WorkflowNotificationListener;
import com.filogix.express.legacy.mosapp.IPageHandlerCommon;
import com.filogix.express.legacy.mosapp.ISessionStateModel;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.ServiceDelegate;
import com.filogix.externallinks.services.ServiceInfo;

// =================================================================================================================
//
// PROGRAMMER NOTE:
//
// This class extends Deal. This is done to support propogation of MI attributes - see propogateMIAttributes().
//
// =================================================================================================================

/**
 * <p>MIProcessHandler<p>
 * 
 * @version 1.1 MCM Team (Aug 7, 2008) modified for new BR DEC (XS 4.3) 
 */
public class MIProcessHandler extends Deal implements WorkflowNotificationListener
{ 
  
    private static final long serialVersionUID = 1L;

    private static Logger logger = LoggerFactory.getLogger(MIProcessHandler.class);
    
  private static MIProcessHandler INSTANCE = null;

  public static final int C_I_INITIAL_REQUEST_PENDING = 1;
  public static final int C_I_ERRORS_CORRECTED_AND_PENDING = 2;
  public static final int C_I_NOT_ENOUGH_INFORMATION = 3;
  public static final int C_I_NOT_AVAILABLE = 4;
  public static final int C_I_NOT_REQUIRED = 5;
  public static final int C_I_NOT_REQUIRED_OPEN_REQUEST = 6;
  public static final int C_I_NOT_AVAILABLE_OPEN_REQUEST = 7;
  public static final int C_I_CHANGES_PENDING = 8;
  public static final int C_I_CHANGE_NOT_AVAILABLE = 9;
  public static final int C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M = 10;

  //--BMO_MI_CR--start//
  public static final int BMO_TYPE_HANDLE_OUTSIDE_BXP = 11;
  //--BMO_MI_CR--end//

  private MIProcessHandler()
  {
    // !! Necessary - why? - because instances used for the deal propogate values functionality

    //Changed for Terrocatta  
      if(INSTANCE == null){
        NotificationMonitor nm = NotificationMonitor.getInstance();
        if(nm.getListeners() == null) {
            nm.init();
        }
        nm.addListener(this);
      }
  }

  /**
   *  The MIProcess handler exists as a singleton in order that it may be the sole
   *  MIProcessHandler instance to listen for MI related notification events.
   */
  public static synchronized MIProcessHandler getInstance()
  {
    if(INSTANCE == null)
     INSTANCE = new MIProcessHandler();

    return INSTANCE;
  }



  public int determineRcmCommStatus( Deal deal, SessionResourceKit srk ) throws Exception
  {
     int lStatus = deal.getMIStatusId();

     if(deal.getMIIndicatorId() == Mc.MII_NOT_REQUIRED || deal.getMIIndicatorId() == Mc.MII_UW_WAIVED)
     {
       switch(lStatus)
       {
         case Mc.MI_STATUS_BLANK:
         case Mc.MI_STATUS_CANCELLED:
         case Mc.MI_STATUS_DENIED:

            return C_I_NOT_REQUIRED;

         case Mc.MI_STATUS_INITIAL_REQUEST_PENDING:
         case Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED:
         case Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER:
         case Mc.MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER:
         case Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER:
         case Mc.MI_STATUS_ERRORS_RECEIVED_BY_INSURER:
         case Mc.MI_STATUS_ERRORS_CORRECTED_AND_PENDING:
         case Mc.MI_STATUS_CORRECTIONS_SUBMITTED:
         case Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY:
         case Mc.MI_STATUS_MANUAL_UNDERWRITING_REQUIRED:
         case Mc.MI_STATUS_CANCELLATION_PENDING:
         case Mc.MI_STATUS_CANCELLATION_SUBMITTED:
         case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC:
         case Mc.MI_STATUS_APPROVAL_RECEIVED:
         case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:
         case Mc.MI_STATUS_DENIED_RECEIVED:
         case Mc.MI_STATUS_RISKED_RECEIVED:
         case Mc.MI_STATUS_RISKED:
         case Mc.MI_STATUS_CHANGES_PENDING:
         case Mc.MI_STATUS_CHANGES_SUBMITTED:
         case Mc.MI_STATUS_APPROVED:

            return C_I_NOT_REQUIRED_OPEN_REQUEST;

         default : {throw new Exception("MIProcess: Status (" + lStatus + ") unhandled!");}

       }
     }

     // ZIVKO ALERT: do we really need passive message at this point, since we do not use
     // business rules?????/

       // Catherine #3740 -- begin ------------
     if (deal.getMITypeId() == Mc.MI_TYPE_BLANK
         || (deal.getMITypeId() == Mc.MI_TYPE_TRANSFER_POLICY && deal.getMIPolicyNumber() == null)) 
      {
         switch (lStatus) 
         {
           case Mc.MI_STATUS_BLANK:
           case Mc.MI_STATUS_DENIED:
           case Mc.MI_STATUS_CANCELLED:
   
             return C_I_NOT_AVAILABLE;
   
           case Mc.MI_STATUS_INITIAL_REQUEST_PENDING:
           case Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED:
           case Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER:
           case Mc.MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER:
           case Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER:
           case Mc.MI_STATUS_ERRORS_RECEIVED_BY_INSURER:
           case Mc.MI_STATUS_ERRORS_CORRECTED_AND_PENDING:
           case Mc.MI_STATUS_CORRECTIONS_SUBMITTED:
           case Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY:
           case Mc.MI_STATUS_MANUAL_UNDERWRITING_REQUIRED:
           case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC:
           case Mc.MI_STATUS_APPROVAL_RECEIVED:
           case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:
           case Mc.MI_STATUS_DENIED_RECEIVED:
           case Mc.MI_STATUS_RISKED_RECEIVED:
           case Mc.MI_STATUS_RISKED:
           case Mc.MI_STATUS_CHANGES_PENDING:
           case Mc.MI_STATUS_CHANGES_SUBMITTED:
           case Mc.MI_STATUS_APPROVED:
           case Mc.MI_STATUS_CANCELLATION_PENDING:
           case Mc.MI_STATUS_CANCELLATION_SUBMITTED:
   
             return C_I_NOT_AVAILABLE_OPEN_REQUEST;
   
           default: {
             throw new Exception("MIProcess: Status (" + lStatus + ") unhandled!");
         }
       }
     } else {
       switch (lStatus) 
       {
         case Mc.MI_STATUS_BLANK:
 
         case Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER:
         case Mc.MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER:
 
           return C_I_INITIAL_REQUEST_PENDING;
 
         case Mc.MI_STATUS_CANCELLATION_PENDING:
         case Mc.MI_STATUS_CANCELLATION_SUBMITTED:
 
           return C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M;
 
         case Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER:
         case Mc.MI_STATUS_ERRORS_RECEIVED_BY_INSURER:
 
         case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC:
         case Mc.MI_STATUS_CANCELLED:
 
         case Mc.MI_STATUS_DENIED_RECEIVED:
         case Mc.MI_STATUS_DENIED:
 
           return C_I_ERRORS_CORRECTED_AND_PENDING;
 
         case Mc.MI_STATUS_APPROVAL_RECEIVED:
         case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:
         case Mc.MI_STATUS_APPROVED:
 
           return C_I_CHANGES_PENDING;

// QC912 
         case Mc.MI_STATUS_INITIAL_REQUEST_PENDING:
         case Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED:
        	 
        	 return C_I_CHANGE_NOT_AVAILABLE;

// MI Statuses 21 and 22 are not used any more
//         case Mc.MI_STATUS_CHANGES_PENDING:
//         case Mc.MI_STATUS_CHANGES_SUBMITTED:
 
         case Mc.MI_STATUS_ERRORS_CORRECTED_AND_PENDING:
         case Mc.MI_STATUS_CORRECTIONS_SUBMITTED:
        	 
        	 return C_I_ERRORS_CORRECTED_AND_PENDING;
 
         case Mc.MI_STATUS_RISKED_RECEIVED:
         case Mc.MI_STATUS_RISKED:

         case Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY:
         case Mc.MI_STATUS_MANUAL_UNDERWRITING_REQUIRED:
        	 
//        	 if(deal.getMortgageInsurerId() == Mc.MI_INSURER_AIGUG){
        		 return C_I_INITIAL_REQUEST_PENDING;
//        	 } else {
//        		 return C_I_CHANGE_NOT_AVAILABLE;
//        	 }
        	 
         default:
 
           return C_I_CHANGE_NOT_AVAILABLE;
       }
     }
     // throw new Exception("MIProcess: Unknown MI Indicator id: " + deal.getMIIndicatorId() + " !!!! ");
     // Catherine #3740 -- end ------------

  }

  public boolean isManualMIType(Deal deal)
  {
     if(deal.getMIIndicatorId() == Mc.MII_UW_REQUIRED || deal.getMIIndicatorId() == Mc.MII_REQUIRED_STD_GUIDELINES)
     {
       if(  !(deal.getMITypeId() == Mc.MI_TYPE_FULL_SERVICE ||
              (deal.getMITypeId() == Mc.MI_TYPE_TRANSFER_POLICY  && deal.getMIPolicyNumber() != null)) )
          return true;
     }

     return false;
  }


  public boolean isMIApprovalStatus(int miStatusId)
  {
      switch (miStatusId)
      {
           case Mc.MI_STATUS_APPROVAL_RECEIVED:
           case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:
           case Mc.MI_STATUS_APPROVED:
              return true;
      }

      return false;
  }

  /**
   * Check for a MI response received status. If found set corresponding (flash) status and trigger the
   * workflow engine. This activity is intended to complete a Review MI Response task upon entry to the
   * MI Review screen.
   *
   * PROGRAMMER NOTE
   *
   * Condition MI status "approval received, premium changed" is detected as a special case and stored as the 1st
   * page condition flag on the page entry provided. I.e.
   *
   *    if status IS "approval received, premium changed"
   *
   *        pg.setPageCondition1(true)
   *
   * This information is ued to communicate the change in premium to the use since the status does not persist - i.e.
   * received status "approval received" and "approval received, premium changed" both map to post-received atatus
   * "approved".
   *
   **/

   public void checkMIResponse(int dealId, int copyId, SessionResourceKit srk, IPageHandlerCommon phc, PageEntry pg)
   {

    boolean inTx = false;
    Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());        

    logger.debug("======> Entry: MIProcessHandler.checkMIResponse");
    try
    {
      Deal deal = DBA.getDeal(srk, dealId, copyId, dcm);

      WorkflowTrigger trigger = new WorkflowTrigger(srk);
      trigger.setWorkflowTriggerValue(dealId, "Manual-Hold", "N");
      int newMIStatus = getResponseReceivedStatus(deal.getMIStatusId(), pg);


      if (newMIStatus == -1){
        phc.workflowTrigger(2, srk, pg, null);
        return;
      }

      inTx = srk.isInTransaction();

      if (inTx == false)
        srk.beginTransaction();

      deal.setMIStatusId(newMIStatus);

      //  propogate MI status
      (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(srk, deal,
        deal.getMIStatusId(), NO_MIIndicatorId, NO_MIPremiumAmount, NO_MortgageInsurerId, NO_MIPolicyNumber,
        NO_MIPayorId, NO_MITypeId, NO_MIUpfront, NO_MICommunicationFlag, NO_OpenMIResponseFlag,
        NO_MIExistingPolicyNumber, NO_MIComments, NO_MIResponse,NO_PreQualMICertNum,NO_CMHCPolicyNumber, NO_GEPolicyNumber,
        NO_LOCAmortizationMonths,           // Rel 3.1 - Sasa 
        NO_LOCInterestOnlyMaturityDate,     // Rel 3.1 - Sasa
        NO_MIFeeAmount,
        NO_MIPremiumPST,
        NO_MIPolicyNumAIGUG,
        this.MIPolicyNumPMI
        );

      deal.ejbStore();
      phc.workflowTrigger(2, srk, pg, null);

      if (inTx == false)
        srk.commitTransaction();
    }
    catch(Exception exc)
    {
      if (inTx == false)
        srk.cleanTransaction();

      String msgStr = exc.getMessage();
      if(msgStr == null){ msgStr = new String("no message in exception"); }
      msgStr = "Exception @MIProcessHandler.checkMIResponse: " + msgStr + " - ignored";

      logger.error(msgStr);
      logger.error(exc);

    }
   
    logger.debug("======> Exit: MIProcessHandler.checkMIResponse");
   }

  private int getResponseReceivedStatus(int inStatus, PageEntry pg)
  {
      switch (inStatus)
      {
        case Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER:
          return Mc.MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER;

        case Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER:
          return Mc.MI_STATUS_ERRORS_RECEIVED_BY_INSURER;

        case Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY:
          return Mc.MI_STATUS_MANUAL_UNDERWRITING_REQUIRED;

        case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC:
          return Mc.MI_STATUS_CANCELLED;

        case Mc.MI_STATUS_APPROVAL_RECEIVED:
          return Mc.MI_STATUS_APPROVED;

        case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:
          pg.setPageCondition1(true);
          return Mc.MI_STATUS_APPROVED;

        case Mc.MI_STATUS_DENIED_RECEIVED:
          return Mc.MI_STATUS_DENIED;

        case Mc.MI_STATUS_RISKED_RECEIVED:
          return Mc.MI_STATUS_RISKED;
      }

      return -1;
  }

  /**
   * Trigger communication to Emili or GE for all outgoing communication except cancel policy.
   *
   * The copy of the deal provided is automatically set as the recommended scenario (if not currently set
   * as recommended scenario).
   *
   * Note: The version that allows processing as a transaction perfoms a workflow trigger and there are
   *       additional needs to support this (e.g. page entry and other objects must be provided). The version
   *       that does not allow transaction processing assumes that the caller establishes the transaction
   *       context and that the caller is responsible for storing data and triggering the workflow engine.
   **/

  public void placeMIRequest(Deal deal, SessionResourceKit srk, int miRecommendCommunicationStatus) throws Exception
  {
      placeMIRequest(deal, srk, miRecommendCommunicationStatus, false, null, null, null);
  }


    public void placeMIRequest(Deal deal, SessionResourceKit srk, int miRecommendCommunicationStatus, boolean asTransaction, IPageHandlerCommon phc,
        PageEntry pg, String scenarioNumber) throws Exception {

        int miStatusToSet = recCommStatusToMiStatus(miRecommendCommunicationStatus);

        try {
            if (asTransaction == true) {
                srk.beginTransaction();
                phc.standardAdoptTxCopy(pg, true);
            }

            // propagate change thru copies
            DealPropagator dealPropagator = new DealPropagator(deal, null);

            dealPropagator.setMIStatusId(miStatusToSet);
            dealPropagator.setMIIndicatorId(deal.getMIIndicatorId());
            dealPropagator.ejbStore();

            // set as recommended scenarion (if not already so)
            if ("Y".equals(deal.getScenarioRecommended()) == false) {
                CCM ccm = new CCM(srk);
                ccm.setAsRecommendedScenario(deal);
            }

            // insert request record
            if(deal.getMortgageInsurerId() == Mc.MI_INSURER_AIGUG){
                ESBOutboundQueue.placeOutboundRequest(srk, dealPropagator, ServiceConst.SERVICE_TYPE_MI);
            } else {
                ESBOutboundQueue.placeOutboundRequest(srk, dealPropagator, ServiceConst.SERVICE_TYPE_MI_GENERIC);                
            }
            
            if (asTransaction) {
                deal.ejbStore();
                phc.workflowTrigger(2, srk, pg, null);
                srk.commitTransaction();
            }

        } catch (Exception exc) {
            if (asTransaction)
                srk.cleanTransaction();

            String msgStr = exc.getMessage();
            if (msgStr == null) {
                msgStr = new String("no message in exception");
            }
            msgStr = "Exception @MIProcessHandler.placeMIRequest: " + msgStr;
            logger.error(msgStr, exc);

            throw new Exception(msgStr);
        }
    }

  private int recCommStatusToMiStatus(int miRecommendCommunicationStatus)
  {
    switch (miRecommendCommunicationStatus)
    {
        case C_I_INITIAL_REQUEST_PENDING:
          return Mc.MI_STATUS_INITIAL_REQUEST_PENDING;

        case C_I_ERRORS_CORRECTED_AND_PENDING:
        case C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M:
          return Mc.MI_STATUS_ERRORS_CORRECTED_AND_PENDING;

        case C_I_CHANGES_PENDING:
          return Mc.MI_STATUS_CHANGES_PENDING;
    }

    return Mc.MI_STATUS_INITIAL_REQUEST_PENDING; // should never happen ... but ...
  }


  /**
   * Trigger communication to Emili or GE for all outgoing cancel requests.
   *
   * Note: Versions that allows processing as a transaction perfom workflow trigger and there are
   *       additional needs to support these calls (e.g. page entry and other objects must be provided.
   *       Versions that do not allow transaction processing assumes that the caller establishes the transaction
   *       context and that the caller is responsible for storing data and triggering the workflow engine.
   **/

//  public void placeMICancellation(Deal deal, SessionResourceKit srk)throws Exception
//  {
//      placeMICancellation(deal, srk, false, null, null);
//  }

//  public void placeMICancellation(Deal deal, SessionResourceKit srk, boolean asTransaction, IPageHandlerCommon phc, PageEntry pg)throws Exception
//  {
//     placeMICancellation(deal, srk, asTransaction, phc, pg, null);
//  }

//  public void placeMICancellation(Deal deal, SessionResourceKit srk, boolean asTransaction, IPageHandlerCommon phc, PageEntry pg, String scenarioNumber)throws Exception
//  {
//    if(isStatusBlankOrCancel(deal.getMIStatusId()))
//      return;
//
//    placeMICancelCommon(deal, srk, asTransaction, phc, pg, scenarioNumber);
//  }

  public void placeMICancellationIfOutstanding(Deal deal, SessionResourceKit srk)throws Exception
  {
    placeMICancellationIfOutstanding(deal, srk, false, null, null);
  }

  public void placeMICancellationIfOutstanding(Deal deal, SessionResourceKit srk, boolean asTransaction, IPageHandlerCommon phc, PageEntry pg)throws Exception
  {
    placeMICancellationIfOutstanding(deal, srk, asTransaction, phc,  pg, null );
  }

  public void placeMICancellationIfOutstanding(Deal deal, SessionResourceKit srk, boolean asTransaction, IPageHandlerCommon phc, PageEntry pg, String scenarioNumber)throws Exception
  {
    // New rule -- don't send cancle request if PolicyNumber not set -- By BILLY 28May2001
    if(deal.getMIPolicyNumber() == null || deal.getMIPolicyNumber().trim().equals("") || !isStatusOutstanding(deal.getMIStatusId()))
    {
      if (asTransaction == true)
      {
        srk.beginTransaction();
        phc.standardAdoptTxCopy(pg, true);
        srk.commitTransaction();
      }
      return;
    }

    placeMICancelCommon(deal, srk, asTransaction, phc, pg, scenarioNumber);
  }

    private void placeMICancelCommon(Deal deal, SessionResourceKit srk, boolean asTransaction, IPageHandlerCommon phc, PageEntry pg,
        String scenarioNumber) throws Exception {
        try {
            if (asTransaction == true) {
                srk.beginTransaction();
                phc.standardAdoptTxCopy(pg, true);
            }

            logger.debug("placeMICancelCommon(): start");

            // set as recommended scenarion (if not already so)
            if ("Y".equals(deal.getScenarioRecommended()) == false) {
                CCM ccm = new CCM(srk);
                ccm.setAsRecommendedScenario(deal);
            }

            logger.debug("placeMICancelCommon(): Setting MI status to Cancellation Pending");

            DealPropagator dealPropagator = new DealPropagator(deal, null);
            //MIIndicator=NotRequired makes MIProvider think this is Cancel request
            dealPropagator.setMIIndicatorId(Mc.MI_INDICATOR_NOT_REQUIRED);
            dealPropagator.setMIStatusId(Mc.MI_STATUS_CANCELLATION_SUBMITTED);
            dealPropagator.ejbStore();

            if(deal.getMortgageInsurerId() == Mc.MI_INSURER_AIGUG){
                ESBOutboundQueue.placeOutboundRequest(srk, dealPropagator, ServiceConst.SERVICE_TYPE_MI);
            } else {
                ESBOutboundQueue.placeOutboundRequest(srk, dealPropagator, ServiceConst.SERVICE_TYPE_MI_GENERIC);                
            }

            if (!srk.isInTransaction()) {
                srk.beginTransaction();
            }

            //QC357 - do a deal history record
            addDealHistoryOnAutoCancel(deal,srk);
            
            if (asTransaction) {
                deal.ejbStore();

                phc.workflowTrigger(2, srk, pg, null);
                srk.commitTransaction();
            }
        } catch (Exception exc) {
            if (asTransaction)
                srk.cleanTransaction();

            String msgStr = "Exception @placeMICancelCommon";
            logger.error(msgStr, exc);

            throw new Exception(msgStr);
        }
    }


   public void workflowNotified(WFNotificationEvent evt)
   {
     int type = evt.getWorkflowNotificationType();

     Log logger = LogFactory.getLog(getClass().getName());        
     if(type == Mc.WORKFLOW_NOTIFICATION_TYPE_MI_REPLY)
     {
        SessionResourceKit srk = new SessionResourceKit("MIReply");
        
        // set VPD for ML.
        // WFNotificationEvent has InsittuionProfileId
        srk.getExpressState().setDealInstitutionId(evt.getInstitutionProfileId());
        try
        {
          MasterDeal md = new MasterDeal(srk, null, evt.getDealId());

          // Should look for the Recommanded Scenario first if not use Gold
          // -- Changed by BILLY 02May2002
          Deal target = new Deal(srk, null);
          try{
            target = target.findByRecommendedScenario(new DealPK(evt.getDealId(), 1), true);
          }
          catch(Exception e)
          {
            logger.warn("No Recommended Copy found :: Dealid = " + evt.getDealId() + " :: Fallback to use Gold copyid="+e);
            target = target.findByGoldCopy(evt.getDealId());
          }

          int theCopyId = target.getCopyId();

          int underwriterId = target.getUnderwriterUserId();

          srk.beginTransaction();

          WFETrigger.workflowTrigger(2, srk, underwriterId , target.getDealId(), target.getCopyId(),-1, null);

          srk.commitTransaction();
        }
        catch(Exception e)
        {
          srk.cleanTransaction();

          String msg = (evt != null) ? ("" + evt.getDealId()) : "{cannot determine - null event}";

          logger.error("MIProcessHandler: Failed to trigger workflow for: " + msg);
          logger.error(e);
        }

        srk.freeResources();
     }
   }

  /*
   *  Test MI details for validity. Returms messages related to MI conditions/problems (e.g. critical message(s)
   *  indicate serious MI data problems)
   **/

  ////public PassiveMessage validateMIAttributes(Deal deal, SessionState theSession, PageEntry pe, SessionResourceKit srk) throws Exception
  public PassiveMessage validateMIAttributes(Deal deal, ISessionStateModel theSession, PageEntry pe, SessionResourceKit srk) throws Exception
  {
    PassiveMessage       pm     = new PassiveMessage();

    BusinessRuleExecutor brExec = new BusinessRuleExecutor();
    DealPK dealpk = (DealPK)deal.getPk();

    Vector v = new Vector();

    // if uw required or waived no action necessary (always considered valid)
    int indicator = deal.getMIIndicatorId();

    if (indicator == Mc.MII_NOT_REQUIRED || indicator == Mc.MII_UW_WAIVED)
    {
        // rule "MI Required?" rules

        v.add("DE-18");

        /// v.add("DE-198");
        /// v.add("DE-244");

        pm = brExec.BREValidator(dealpk, srk, pm, v);

        return pm;
    }
    // ZIVKO marker for entering the code for application for loan assessment and MI Pre-Qualification

    // run "MI Data Validation" rules
    return validateMI(deal, theSession, pe, srk);
  }

  // Validate data (generally, before communicating with MI carrier). This involves all normal deal data validations
  // (e.g. DE rules, DI rules, etc.) as well as MI specific rules.

  ////public PassiveMessage validateMI(Deal deal, SessionState theSession, PageEntry pe, SessionResourceKit srk) throws Exception
  /**
   * <p>validateMI</p>
   * <p>check BR before sending MI request</P>
   * 
   * @version 1.1 MCM Team (Aug 7, 2008) added new BR DEC (XS 4.3) 
   */
  public PassiveMessage validateMI(Deal deal, ISessionStateModel theSession, PageEntry pe, SessionResourceKit srk) throws Exception
  {
      PassiveMessage       pm     = new PassiveMessage();
      BusinessRuleExecutor brExec = new BusinessRuleExecutor();

      brExec.BREValidator(theSession, pe, srk, pm, "MI-%");

      brExec.BREValidator(theSession, pe, srk, pm, "DI-%");

      brExec.BREValidator(theSession, pe, srk, pm, "DE-%");

      brExec.BREValidator(theSession, pe, srk, pm, "IC-%");

      brExec.BREValidator(theSession, pe, srk, pm, "UN-%");



      // local scope rules ...

      int lim = 0;
      Object [] objs = null;

      // borrower rules
      try
      {
          Collection borrowers = deal.getBorrowers();
          lim = borrowers.size();
          objs = borrowers.toArray();
      }
      catch (Exception e) { lim = 0; } // assume no borrowers

      for (int i = 0; i < lim; ++i)
      {
          Borrower borr = (Borrower)objs[i];
          brExec.setCurrentProperty(-1);
          brExec.setCurrentBorrower(borr.getBorrowerId());
          // MCM team
          brExec.setCurrentComponent(-1);

          PassiveMessage pmBorr = brExec.BREValidator(theSession, pe, srk, null, "DEA-%");

          if (pmBorr != null)
          {
            pm.addSeparator();
            
            //-------Change to read message subtitle from property files --- By Clement 24 Nov 2006
            //pm.addMsg("The following message(s) apply to applicant: " + borr.formFullName(), PassiveMessage.INFO);
            String applicantLabel = (BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM", theSession.getLanguageId())
                    + "<br>");

            pm.addMsg(applicantLabel + borr.formFullName(), PassiveMessage.INFO);
            //--------------------------------------------------------------------------------------
            
            pm.addAllMessages(pmBorr);
            pm.addSeparator();
          }

      }

      // property rules
      try
      {
          Collection properties = deal.getProperties();
          lim = properties.size();
          objs = properties.toArray();
      }
      catch (Exception e) { lim = 0; } // assume no properties

      for (int i = 0; i < lim; ++i)
      {
       Property prop = (Property)objs[i];
          brExec.setCurrentProperty(prop.getPropertyId());
          brExec.setCurrentBorrower(-1);
          // MCM team
          brExec.setCurrentComponent(-1);

          PassiveMessage pmProp = brExec.BREValidator(theSession, pe, srk, null, "DEP-%");

          if (pmProp != null)
          {
            pm.addSeparator();
            
            //Change to read message from property files --- By Clement 24 Nov 2006
            //pm.addMsg("The following message(s) apply to property: " + prop.formPropertyAddress(), PassiveMessage.INFO);                
            String applicantLabel = (BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM", theSession.getLanguageId())
                    + "<br>");

            pm.addMsg(applicantLabel + prop.formPropertyAddress(), PassiveMessage.INFO);
            //--------------------------------------------------------------------------------------
            
            pm.addAllMessages(pmProp);
            pm.addSeparator();
          }
      }
      
      // component rules
      
      brExec.BREValidator(theSession, pe, srk, pm, "DMC-%");

      try {
          Collection components = deal.getComponents();
          lim = components.size();
          objs = components.toArray();
      }
      catch (Exception e) { lim = 0; } // assume no properties

      String preCompType = "";
      int compIndex = 0;
      // MCM team (XS4.3) Starts
      for (int i = 0; i < lim; ++i) {
          Component comp = (Component)objs[i];
          brExec.setCurrentComponent(comp.getComponentId());
          brExec.setCurrentBorrower(-1);
          brExec.setCurrentProperty(-1);

          PassiveMessage pmProp = brExec.BREValidator(theSession, pe, srk, null, "DEC-%");

          if (pmProp != null)
          {
            pm.addSeparator();
            
            //Change to read message from property files --- By Clement 24 Nov 2006
            //pm.addMsg("The following message(s) apply to property: " + prop.formPropertyAddress(), PassiveMessage.INFO);                
            String label = (BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_COMPONENT_PROBLEM", theSession.getLanguageId())
                    + "<br>");
            String compType = (BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), 
                    "COMPONENTTYPE", comp.getComponentTypeId(), theSession.getLanguageId()));
            if (preCompType.equals(compType)) {
                compIndex++;
            } else {
                preCompType = compType;
                compIndex = 1;
            }
            pm.addMsg(label + compType + " " + compIndex, PassiveMessage.INFO);
            //--------------------------------------------------------------------------------------
            
            pm.addAllMessages(pmProp);
            pm.addSeparator();
          }
      }

      /*
      if (pm.getNumMessages() > 0)
      {
          // Add sub-title based on Screen spec. -- by BILLY 02Feb2001
          String tmpMsg = Sc.PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM + "<br>" +
              "(Deal #: " + deal.getDealId() + ", " + "Scenario #: " + deal.getScenarioNumber() + ")";
          pm.setInfoMsg(tmpMsg);
          return pm;
      }
      */

      return pm;
  }

  //============================================================================
  // Automatic update of deal MI attributes trigger
  // This section covers the case when user answers "YES" in the dialogue box
  // "Do you want the system to update the mortgage insurance indicator and
  // associated details"
  //============================================================================

  public void updateMIAttributes(Deal deal, SessionResourceKit srk, boolean asTransaction) throws Exception
  {
    Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());        
    try
    {
      if (asTransaction)
        srk.beginTransaction();

      if(deal.getMIIndicatorId() == Mc.MII_NOT_REQUIRED)
      {
        //  propogate MI indicator, status and reset attributes
        deal.setMIIndicatorId(Mc.MII_REQUIRED_STD_GUIDELINES);
        deal.setMIStatusId(Mc.MI_STATUS_BLANK);
        deal.setMIPayorId(Mc.MI_PAYOR_BORROWER);
        deal.setMITypeId(Mc.MI_TYPE_FULL_SERVICE);
        deal.setMortgageInsurerId(Mc.MI_INSURER_CMHC);
        deal.setMIUpfront("N");

        (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(srk, deal,
          deal.getMIStatusId(), deal.getMIIndicatorId(), NO_MIPremiumAmount, deal.getMortgageInsurerId(), NO_MIPolicyNumber,
          deal.getMIPayorId(), deal.getMITypeId(), deal.getMIUpfront(), NO_MICommunicationFlag, NO_OpenMIResponseFlag,
          NO_MIExistingPolicyNumber, NO_MIComments, NO_MIResponse,NO_PreQualMICertNum, NO_CMHCPolicyNumber, NO_GEPolicyNumber,
          deal.getLocAmortizationMonths(),                  // Rel 3.1 - Sasa
          pGetTime(deal.getLocInterestOnlyMaturityDate()),  // Rel 3.1 - Sasa
          deal.getMIFeeAmount(),
          deal.getMIPremiumPST(),
          deal.getMIPolicyNumAIGUG(),
          deal.getMIPolicyNumPMI()
          );
      }
      else if(deal.getMIIndicatorId() == Mc.MII_REQUIRED_STD_GUIDELINES)
      {
        //  propogate reset attributes
        MIProcessHandler propogator = MIProcessHandler.getMIAttributesPropogator();

        resetMIFields(deal, srk, propogator);

        propogator.propogateMIAttributes(srk, deal,
          NO_MIStatusId, NO_MIIndicatorId, NO_MIPremiumAmount, NO_MortgageInsurerId, NO_MIPolicyNumber,
          NO_MIPayorId, NO_MITypeId, NO_MIUpfront, NO_MICommunicationFlag, NO_OpenMIResponseFlag,
          NO_MIExistingPolicyNumber, NO_MIComments, NO_MIResponse,NO_PreQualMICertNum, NO_CMHCPolicyNumber, NO_GEPolicyNumber,
          NO_LOCAmortizationMonths,         // Rel 3.1 - Sasa 
          NO_LOCInterestOnlyMaturityDate,       // Rel 3.1 - Sasa
          NO_MIFeeAmount,
          NO_MIPremiumPST,
          NO_MIPolicyNumAIGUG,
          NO_MIPolicyNumPMI
          );
      }

      deal.ejbStore();

      if (deal.getCalcMonitor() != null)
        deal.getCalcMonitor().calc();

      if (asTransaction)
        srk.commitTransaction();
    }
    catch (Exception exc)
    {
      if (asTransaction)
        srk.cleanTransaction();

      String msgStr = exc.getMessage();
      if(msgStr == null){ msgStr = new String("no message in exception"); }
      msgStr = "Exception @MIProcessHandler.updateMIAttributes: " + msgStr;
      logger.error(msgStr);
      logger.error(exc);

      throw new Exception(msgStr);
    }
  }


  /**
   * Propogate MI attributes (selected) all copies of a deal. All unlocked copies of the specified deal are updated.
   * However, the copy corresponding to the deal entity passed is not updated - caller responsibility.
   *
   * Symboloc constants provided to indicate no update to corresponding argument. For example the call to update only
   * MI Indicator with value 10 would be:
   *
   *    propogateMIAttributes(srk, deal,
   *            NO_MIStatusId, 10, NO_MIPremiumAmount, NO_MortgageInsurerId, NO_MIPolicyNumber,
   *            NO_MIPayorId, NO_MITypeId, NO_MIUpfront, NO_MICommunicationFlag, NO_OpenMIResponseFlag,
   *            NO_MIExistingPolicyNumber, NO_MIComments, NO_MIComments, NO_MIResponse, NO_PreQualMICertNum, 
   *            NO_CMHCPolicyNumber, NO_GEPolicyNumber, NO_LOCAmortizationMonths, NO_LOCInterestOnlyMaturityDate);
   *
   * The constants allow easy pick and choose of inputs without having to consult the method signature when starting
   * with template below:
   *
   *    propogateMIAttributes(srk, deal,
   *            NO_MIStatusId, NO_MIIndicatorId, NO_MIPremiumAmount, NO_MortgageInsurerId, NO_MIPolicyNumber,
   *            NO_MIPayorId, NO_MITypeId, NO_MIUpfront, NO_MICommunicationFlag, NO_OpenMIResponseFlag,
   *            NO_MIExistingPolicyNumber, NO_MIComments, NO_MIResponse, NO_PreQualMICertNum, NO_CMHCPolicyNumber,
   *            NO_GEPolicyNumber, NO_LOCAmortizationMonths, NO_LOCInterestOnlyMaturityDate );
   **/

    public static final int     NO_MIStatusId                    = -1;
    public static final int     NO_MIIndicatorId                 = -1;
    public static final double  NO_MIPremiumAmount               = -1d;
    public static final int     NO_MortgageInsurerId             = -1;
    public static final String  NO_MIPolicyNumber                = "NONE";
    public static final int     NO_MIPayorId                     = -1;
    public static final int     NO_MITypeId                      = -1;
    public static final String  NO_MIUpfront                     = "NONE";
    public static final String  NO_MICommunicationFlag           = "NONE";
    public static final String  NO_OpenMIResponseFlag            = "NONE";
    public static final String  NO_MIExistingPolicyNumber        = "NONE";
    public static final String  NO_MIComments                    = "NONE";
    public static final String  NO_MIResponse                    = "NONE";
    public static final String  NO_PreQualMICertNum              = "NONE";
    public static final String  NO_CMHCPolicyNumber              = "NONE";
    public static final String  NO_GEPolicyNumber                = "NONE";
    public static final int     NO_LOCAmortizationMonths       = -1;            // Rel 3.1 - Sasa
    public static final long    NO_LOCInterestOnlyMaturityDate = -1;            // Rel 3.1 - Sasa
    public static final double  NO_MIFeeAmount                 = -1d;
    public static final double  NO_MIPremiumPST                = -1d;
    public static final String  NO_MIPolicyNumAIGUG            = "NONE";
    public static final String  NO_MIPolicyNumPMI              = "NONE"; 
  // usage : (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(.....)

  public static MIProcessHandler getMIAttributesPropogator()
  {
    return new MIProcessHandler();
  }


  public void propogateMIAttributes(SessionResourceKit srk, Deal deal,
                                        int MIStatusId,  int MIIndicatorId,  double MIPremiumAmount,  int MortgageInsurerId, 
                                        String MIPolicyNumber, int MIPayorId, int MITypeId, String MIUpfront, 
                                        String MICommunicationFlag, String OpenMIResponseFlag, String MIExistingPolicyNumber, 
                                        String MIComments, String MIResponse, String MIPreQualNum, String MICMHCPolicyNumber, 
                                        String MIGEPolicyNumber, 
                                        int LOCAmortizationMonths,              // Rel 3.1 - Sasa
                                        long LOCInterestOnlyMaturityDate,       // Rel 3.1 - Sasa : Number of milliseconds since
                                                                                // January 1, 1970, 00:00:00 GMT represented by 
                                                                                // the original Date object
                                        double MIFeeAmount,
                                        double MIPremiumPST,
                                        String MIPolicyNumAIGUG,
                                        String MIPolicyNumPMI
                                        ) throws Exception	
  {
      // propogator method required a (new) isntance - cannot be called via the singleton
      //  - use getMIAttributesPropogator() for instance

    	Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());        
      logger.info("CP-1 propogateMIAttributes  MICMHCPolicyNumber: " + MICMHCPolicyNumber);
      logger.info("CP-2 propogateMIAttributes  MIGEPolicyNumber: " + MIGEPolicyNumber);
      logger.info("CP-3 propogateMIAttributes  MIPolicyNumber: " + MIPolicyNumber);
      if (this == INSTANCE)
        throw new Exception("Exception @MIProcessHandler.propogateMIAttributes: Illegal state exception, new isntance required");

      // -- field updates -- //


      if (isNot(MIIndicatorId, NO_MIIndicatorId))
      {
          //====================================================================
          // this line will force change on deal record.
          // Change posted by Zivko & Billy on Mar 05, 2002
          //// SYNCADD.
          // Bug : we should force to update the all the fields
          //  -- By Billy 07June2002
          //====================================================================
          //setMIIndicatorId(deal.getMIIndicatorId());
          setMIIndicatorId(MIIndicatorId);
          this.forceChange("MIIndicatorId");
      }

      if (isNot(MIStatusId, NO_MIStatusId)){
          //setMIStatusId(deal.getMIStatusId());
          setMIStatusId(MIStatusId);
          this.forceChange("MIStatusId");
      }

      if (isNot(MIPremiumAmount, NO_MIPremiumAmount)){

          // 4.3GR, Update Total Loan Amount -- start
          this.preservePreviousMIAmount(deal, MIPremiumAmount);
          // 4.3GR, Update Total Loan Amount -- end

          setMIPremiumAmount(MIPremiumAmount);
          this.forceChange("MIPremiumAmount");
      }

      if (isNot(MortgageInsurerId, NO_MortgageInsurerId)){
          //setMortgageInsurerId(deal.getMortgageInsurerId());
          setMortgageInsurerId(MortgageInsurerId);
          this.forceChange("mortgageInsurerId");
      }

      if (isNot(MIPolicyNumber, NO_MIPolicyNumber)){
          //setMIPolicyNumber( deal.getMIPolicyNumber() );
          setMIPolicyNumber(MIPolicyNumber);
          this.forceChange("MIPolicyNumber");
      }

      if (MICMHCPolicyNumber != null && isNot(MICMHCPolicyNumber, NO_CMHCPolicyNumber)){
        logger.info("CP-4 propogateMIAttributes  MIPolicyNumber: " + MICMHCPolicyNumber);
          //setMIPolicyNumber( deal.getMIPolicyNumber() );
          setMIPolicyNumCMHC(MICMHCPolicyNumber);
          this.forceChange("MIPolicyNumCMHC");
      }

      if (MIGEPolicyNumber != null && isNot(MIGEPolicyNumber, NO_GEPolicyNumber)){
        logger.info("CP-5 propogateMIAttributes  MIPolicyNumber: " + MIGEPolicyNumber);
          //setMIPolicyNumber( deal.getMIPolicyNumber() );
          setMIPolicyNumGE(MIGEPolicyNumber);
          this.forceChange("MIPolicyNumGE");
      }


      if (isNot(MIPayorId, NO_MIPayorId)){
          //setMIPayorId( deal.getMIPayorId() );
          setMIPayorId(MIPayorId);
          this.forceChange("MIPayorId");
      }

      if (isNot(MIUpfront, NO_MIUpfront)){
          //setMIUpfront( deal.getMIUpfront() );
          setMIUpfront(MIUpfront);
          this.forceChange("MIUpfront");
      }

      if (isNot(MITypeId, NO_MITypeId)){
          //setMITypeId( deal.getMITypeId() );
          setMITypeId(MITypeId);
          this.forceChange("MITypeId");
      }

      if (isNot(MICommunicationFlag, NO_MICommunicationFlag)){
          //setMICommunicationFlag( deal.getMICommunicationFlag() );
          setMICommunicationFlag(MICommunicationFlag);
          this.forceChange("MICommunicationFlag");
      }

      if (isNot(OpenMIResponseFlag, NO_OpenMIResponseFlag)){
          //setOpenMIResponseFlag(deal.getOpenMIResponseFlag());
          setOpenMIResponseFlag(OpenMIResponseFlag);
          this.forceChange("openMIResponseFlag");
      }

      if (isNot(MIExistingPolicyNumber, NO_MIExistingPolicyNumber)){
          //setMIExistingPolicyNumber( deal.getMIExistingPolicyNumber() );
          setMIExistingPolicyNumber(MIExistingPolicyNumber);
          this.forceChange("MIExistingPolicyNumber");
      }
      if (isNot(MIComments, NO_MIComments)){
          //setMIComments( deal.getMIComments() );
          setMIComments(MIComments);
          this.forceChange("MIComments");
      }

      if (isNot(MIResponse, NO_MIResponse)){
          //setMortgageInsuranceResponse(  deal.getMortgageInsuranceResponse() );
          setMortgageInsuranceResponse(MIResponse);
          this.forceChange("mortgageInsuranceResponse");
      }

      if (isNot(MIPreQualNum, NO_PreQualMICertNum )){
          //setPreQualificationMICertNum(  deal.getPreQualificationMICertNum() );
          setPreQualificationMICertNum(MIPreQualNum);
          this.forceChange("preQualificationMICertNum");
      }
      
      // Rel 3.1 - Sasa
      if (isNot(LOCAmortizationMonths, NO_LOCAmortizationMonths)) {
          setLocAmortizationMonths(LOCAmortizationMonths);
          this.forceChange("locAmortizationMonths");
      }
      
      if (isNot(LOCInterestOnlyMaturityDate, NO_LOCInterestOnlyMaturityDate)) {
          super.setLocInterestOnlyMaturityDate(new Date(LOCInterestOnlyMaturityDate));
          this.forceChange("locInterestOnlyMaturityDate");
      }
      // End of Rel 3.1 - Sasa

      if(isNot(MIFeeAmount, NO_MIFeeAmount)) {
          this.setMIFeeAmount(MIFeeAmount);
          this.forceChange("MIFeeAmount");
      }
      
      if(isNot(MIPremiumPST, NO_MIPremiumPST)) {
          this.setMIPremiumPST(MIPremiumPST);
          this.forceChange("MIPremiumPST");
      }
      
      if(isNot(MIPolicyNumAIGUG, NO_MIPolicyNumAIGUG)) {
          this.setMIPolicyNumAIGUG(MIPolicyNumAIGUG);
          this.forceChange("MIPolicyNumAIGUG");
      }

      if(isNot(MIPolicyNumPMI, NO_MIPolicyNumPMI)) {
          this.setMIPolicyNumPMI(MIPolicyNumPMI);
          this.forceChange("MIPolicyNumPMI");
      }
      
      // db update //

      boolean inTx = srk.isInTransaction();

      try
      {
          JdbcExecutor jExec = srk.getJdbcExecutor();
          this.setSessionResourceKit(srk);
          this.setDealId(deal.getDealId());
          if (inTx == false)
            srk.beginTransaction();

          // modified by BILLY to add special handle for mortgageInsuranceResponse field
          // Check if mortgageInsuranceResponse changed
          if(this.isValueChange("mortgageInsuranceResponse"))
          {
            // Perform the change by PrepareStatement (by use special update method)
            this.updateMortgageInsuranceResponse(true);
            // Reset the value changed flag to avoid update again
            this.resetChange("mortgageInsuranceResponse");
          }
          // ===========================================================================

          StringBuffer buffer = new StringBuffer();

          buffer.append("Update DEAL set ");

          //Should only handle the changes -- modified by BILLY 25Feb2002
          //formUpdateFieldsList(buffer, false);
          if(formUpdateFieldsList(buffer, true) != null)
          {
            /*
            buffer.append(" where dealId = " + deal.getDealId() + " and copyId <> " + deal.getCopyId() +
                          " and scenarioLocked <> 'Y'");
            */

            /*
            buffer.append(" where dealId = " + deal.getDealId() + " and scenarioLocked <> 'Y'");
            */

            buffer.append(" where dealId = " + deal.getDealId());

            String debug = buffer.toString();

            jExec.executeUpdate(debug);
          }
          // else do no updates

          //4.4 above SQL will cause data mismatch between db and cache
          //so removing all deal entity objects from cache
          ThreadLocalEntityCache.removeFromCache(Deal.class);
          
          if (inTx == false)
            srk.commitTransaction();
      }
      catch (Exception e)
      {
          if (inTx == false)
            srk.cleanTransaction();

          logger.error("Exception @MIProcessHandler.propogateMIAttributes");
          logger.error(e);

          throw e;
      }
  }

  //====================================================================================================
  // NOTE: Method does the same thing as the propogateDealFeeRemoval(SessionResourceKit srk, Deal deal)
  // the difference between the two is that in this method we provide the list of fee types that are to
  // be removed. The class that calls this method must know which types of fees are to be removed.
  //====================================================================================================
  public void propogateDealFeeRemoval(SessionResourceKit srk, Deal deal, int[] reqTypes)throws Exception
  {

		Log logger = LogFactory.getLog(getClass().getName() + ':'+ srk.getIdentity());
    boolean inTx = srk.isInTransaction();

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();

      if (inTx == false)
        srk.beginTransaction();

      StringBuffer buffer = new StringBuffer();
      buffer.append("Delete from DEALFEE where dealid = ").append(deal.getDealId());
      //--> Bug fix : should only remove the current copy
      //--> By Billy 21Sept2004
      //buffer.append(" AND copyId in ( select copyid from deal where copytype in('S', 'G')");
      //buffer.append(" and scenariolocked <> 'Y')");
      buffer.append(" AND copyId = ").append(deal.getCopyId());
      //==================================================

			//#DG762 
			buffer.append(" AND feeid in ( select distinct(feeId) from fee where feetypeid in( ")
				.append(TypeConverter.stringFromIntArrayType(reqTypes))
				.append("))");

			final String sql = buffer.toString();
			jExec.executeUpdate(sql);

    if (inTx == false)
      srk.commitTransaction();
    }
    catch (Exception e)
    {
      if (inTx == false)
        srk.cleanTransaction();
			logger.error("Exception @MIProcessHandler.propogateFeeRemoval");
			logger.error(e);

      throw e;
    }
  }

  public void propogateDealFeeRemoval(SessionResourceKit srk, Deal deal)throws Exception
  {
    Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());        
    boolean inTx = srk.isInTransaction();

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();

      if (inTx == false)
        srk.beginTransaction();

      StringBuffer buffer = new StringBuffer();
      buffer.append("Delete from DEALFEE where dealid = ").append(deal.getDealId());
      //#DG762
      //FXP30577:  Macq 4.2 - MI premium is great than 0 for waived deals  - begin - commenting
      //buffer.append(" AND copyId in ( select copyid from deal where dealid=")
      	//.append(deal.getDealId()).append(" and copytype in('S', 'G')");
     // buffer.append(" and scenariolocked <> 'Y')");
      //FXP30577:  Macq 4.2 - MI premium is great than 0 for waived deals  - end - commenting
     
      

      int[] reqTypes = { Mc.FEE_TYPE_CMHC_PREMIUM,
                      Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE,
                      Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM,
                      Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE,
                      Mc.FEE_TYPE_CMHC_FEE,
                      Mc.FEE_TYPE_CMHC_FEE_PAYABLE,
                      Mc.FEE_TYPE_GE_CAPITAL_FEE,
                      Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE,
                      Mc.FEE_TYPE_GE_CAPITAL_PREMIUM,
                      Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE,
                      Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM,
                      Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE };

      //#DG762
      buffer.append(" AND feeid in( select distinct(feeId) from fee where feetypeid in( ")
				.append(TypeConverter.stringFromIntArrayType(reqTypes))
				.append("))");

			final String sql = buffer.toString();
			logger.info("MIProcessHandler - propogateDealFeeRemoval - " + sql);
			jExec.executeUpdate(sql);

    if (inTx == false)
      srk.commitTransaction();
    }
    catch (Exception e)
    {
      if (inTx == false)
        srk.cleanTransaction();
      logger.error("Exception @MIProcessHandler.propogateFeeRemoval");
      logger.error(e);

      throw e;
    }
  }

  private boolean isNot(int a, int b) { return a != b; }
  private boolean isNot(double a, double b) { return a != b; }
  private boolean isNot(long a, long b) {return a != b; }           // Rel 3.1 - Sasa
  private boolean isNot(String a, String b)
  {
    if (a == null && b == null) return false; // both null
    if (a == null || b == null) return true;  // only one null
    return (!a.equals(b));
  }

  /**
    * Called to reset MI fields to initial values (e.g. after cancelling, etc.)
    *
    * PROGRAMMER NOTE:
    *
    * If a propogator object is provided (a mi handler posing as deal entity - see comment at top of class)
    * the updates will be communicated to it. However propogation will only occur if the propogateMIAttributes()
    * method of the propogator object is subsequently called.
    **/

  private void resetMIFields(Deal deal, SessionResourceKit srk, MIProcessHandler propogator) throws Exception
  {
     deal.setMIIndicatorId(Mc.MII_NOT_REQUIRED);
     deal.setMIPremiumAmount(0d);
     deal.setMIPayorId(Mc.MI_PAYOR_BLANK);
     deal.setMITypeId(Mc.MI_TYPE_BLANK);
     deal.setMortgageInsurerId(Mc.MI_INSURER_BLANK);
     deal.setMIUpfront("N");

     deal.setMIPolicyNumber(null);

     if (propogator != null)
     {
     propogator.setMIIndicatorId(Mc.MII_NOT_REQUIRED);
     propogator.setMIPremiumAmount(0d);
     propogator.setMIPayorId(Mc.MI_PAYOR_BLANK);
     propogator.setMITypeId(Mc.MI_TYPE_BLANK);
     propogator.setMortgageInsurerId(Mc.MI_INSURER_BLANK);
     propogator.setMIUpfront("N");
     propogator.setMIPolicyNumber(null);
     }
  }

  public static void setForIngestionHighRatio(Deal gold, SessionResourceKit srk) throws Exception{
      // Tracker #3472 - Sasha
      // gold.setMITypeId(Mc.MI_TYPE_FULL_SERVICE);
      if( gold.getMITypeId() == 0 ) gold.setMITypeId(Mc.MI_TYPE_FULL_SERVICE);
      // End of Tracker #3472

     gold.setMIIndicatorId(Mc.MII_REQUIRED_STD_GUIDELINES);
     gold.setMIPayorId(Mc.MI_PAYOR_BORROWER);
      gold.setMortgageInsurerId(Mc.MI_INSURER_CMHC);
     gold.setMIUpfront("N");
  }

  private PassiveMessage runMIBusinessRules(Deal deal, SessionResourceKit srk) throws Exception
  {
    PassiveMessage       pm     = new PassiveMessage();
    BusinessRuleExecutor brExec = new BusinessRuleExecutor();
    DealPK dealpk = (DealPK)deal.getPk();

    Vector v = null;
    v = new Vector();
     v.add("MI-%");
     pm = brExec.BREValidator(dealpk, srk, pm, v);
    return pm;
  }

  private boolean isStatusBlankOrCancel(int pStatus){
    switch(pStatus){
      case Mc.MI_STATUS_BLANK                           :{return true;}
      case Mc.MI_STATUS_CANCELLATION_PENDING            :{return true;}
      case Mc.MI_STATUS_CANCELLATION_SUBMITTED          :{return true;}
      case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC   :{return true;}
      case Mc.MI_STATUS_CANCELLED                       :{return true;}
      default                                           :{return false;}
    }
  }

  private boolean isStatusOutstanding(int pStatus){
    switch(pStatus){
      case Mc.MI_STATUS_BLANK                           :{return false;}
      case Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER :{return false;}
      case Mc.MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER :{return false;}
      case Mc.MI_STATUS_CANCELLATION_PENDING            :{return false;}
      case Mc.MI_STATUS_CANCELLATION_SUBMITTED          :{return false;}
      case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC   :{return false;}
      case Mc.MI_STATUS_CANCELLED                       :{return false;}
      case Mc.MI_STATUS_DENIED                          :{return false;}
      case Mc.MI_STATUS_DENIED_RECEIVED                 :{return false;}
      default                                           :{return true;}
    }
  }

  public void updateOnReply(DealPK deal, SessionResourceKit srk) throws Exception
  {
  	Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());        
     logger.debug("DEAL HISTORY LOGGING - UPDATE ON REPLY - STARTED");
     DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);
     int dealId = deal.getId();
     int copyId = deal.getCopyId();
     int upid = srk.getExpressState().getUserProfileId();
     dhl.log(dealId, copyId, dhl.mortgageInsuranceResponse(dealId, copyId), Mc.DEAL_TX_TYPE_INTERFACE,upid );
     logger.debug("DEAL HISTORY LOGGING - UPDATE ON REPLY - ENDED");
  }

  public void updateOnSubmission(Deal inDeal, SessionResourceKit srk)throws Exception
  {
    Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());        
    int pStatus = inDeal.getMIStatusId();
    int nStatus = pStatus;

    //advance the status:
    switch(pStatus)
    {
      case Mc.MI_STATUS_INITIAL_REQUEST_PENDING:
       nStatus = Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED;
       break;

      case Mc.MI_STATUS_CANCELLATION_PENDING:
       nStatus = Mc.MI_STATUS_CANCELLATION_SUBMITTED;
       break;

      case Mc.MI_STATUS_ERRORS_CORRECTED_AND_PENDING:
       nStatus = Mc.MI_STATUS_CORRECTIONS_SUBMITTED;
       break;

      case Mc.MI_STATUS_CHANGES_PENDING:
       nStatus = Mc.MI_STATUS_CHANGES_SUBMITTED;
       break;
    }

    //inDeal.setMIStatusId(nStatus);

    if(nStatus == Mc.MI_STATUS_CANCELLATION_SUBMITTED)
    {
      inDeal.setOpenMIResponseFlag("C");

      (MIProcessHandler.getMIAttributesPropogator()).propogateDealFeeRemoval(srk,inDeal);
    }

    (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(srk, inDeal,
        nStatus, inDeal.getMIIndicatorId(), inDeal.getMIPremiumAmount(),
        inDeal.getMortgageInsurerId(), inDeal.getMIPolicyNumber(),
        inDeal.getMIPayorId(), inDeal.getMITypeId(), inDeal.getMIUpfront(),
        NO_MICommunicationFlag, inDeal.getOpenMIResponseFlag(),
        NO_MIExistingPolicyNumber, NO_MIComments, NO_MIResponse,NO_PreQualMICertNum ,
        inDeal.getMIPolicyNumCMHC(), inDeal.getMIPolicyNumGE(),
        inDeal.getLocAmortizationMonths(),                      // Rel 3.1 - Sasa
        pGetTime(inDeal.getLocInterestOnlyMaturityDate()),      // Rel 3.1 - Sasa
        inDeal.getMIFeeAmount(),
        inDeal.getMIPremiumPST(),
        inDeal.getMIPolicyNumAIGUG(),
        inDeal.getMIPolicyNumPMI()
        );

    //log to history:
    logger.debug("DEAL HISTORY LOGGING - UPDATE ON SUBMISSION - STARTED");
    DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);
    int dealId = inDeal.getDealId();
    int copyId = inDeal.getCopyId();
    int upid = srk.getExpressState().getUserProfileId();
    dhl.log(dealId, copyId, dhl.mortgageInsuranceRequest(dealId, copyId),Mc.DEAL_TX_TYPE_INTERFACE,upid );
    WFETrigger.workflowTrigger(2, srk, inDeal.getUnderwriterUserId() , inDeal.getDealId(), inDeal.getCopyId(),-1, null);
    logger.debug("DEAL HISTORY LOGGING - UPDATE ON SUBMISSION - ENDED");
  }

  /**
   * Makes sure the date is not null
   * 
   * @param locInterestOnlyMaturityDate
   * @return
   */
  private long pGetTime(Date locInterestOnlyMaturityDate) {
      if( locInterestOnlyMaturityDate != null )
          return locInterestOnlyMaturityDate.getTime();
      else
          return NO_LOCInterestOnlyMaturityDate;
  }
  
   /**
   * 
   * @param srk
   * @param deal
   * @throws Exception
   */
//====================================================================================================
  //Note: This method is basically the same as propagateDealFeeRemoval(srk, deal) except that we base the 
  //fee's to delete on the fee table miTypeFlag settings as per the AIG UG spec.
  //====================================================================================================
  public void removeFeeFromDeal(SessionResourceKit srk, Deal deal) throws Exception {
    Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());        
        boolean inTx = srk.isInTransaction();

        try
        {
            JdbcExecutor jExec = srk.getJdbcExecutor();

            if (inTx == false)
                srk.beginTransaction();

            String query = "DELETE FROM DEALFEE WHERE FEEID IN (SELECT FEEID FROM FEE WHERE MITYPEFLAG='Y')" +
                           " AND DEALID = " + deal.getDealId();

            jExec.executeUpdate(query);

            if (inTx == false)
                srk.commitTransaction();
        }
        catch (Exception e)
        {
          if (inTx == false)
            srk.cleanTransaction();
	      logger.error("Exception @MIProcessHandler.removeFeeFromDeal");
	      logger.error(e);

          throw e;
        }
    }
  
  // 4.3GR, Update Total Loan Amount -- start
  public void preservePreviousMIAmount(Deal deal, double miPremiumAmountNew)
  throws RemoteException, FinderException {

      // this method only updates recomended scenario.
      // this is requested by Raheal and Midori

      if (deal == null) return;
      if (miPremiumAmountNew == MIProcessHandler.NO_MIPremiumAmount) return;
      // make sure updating scenario recommended copy
      if ("Y".equalsIgnoreCase(deal.getScenarioRecommended()) == false) {
          deal = deal.findByRecommendedScenario((DealPK) deal.getPk(), true);
      }
      if (deal == null) return;
      if (deal.getMIPremiumAmount() == miPremiumAmountNew) return;

      // preserve previous mi amount
      deal.setMIPremiumAmountPrevious(deal.getMIPremiumAmount());
      // flag that tells users MI Premium is changed
      deal.setMIPremiumAmountChangeNortified("N");
      deal.ejbStore();
  }
  // 4.3GR, Update Total Loan Amount -- end
  
  /**
   * prepare and send out MI Auto Cancel Request
   * 
   * @param srk
   * @param deal
   * @throws Exception
   */
  public void generateCGMICancelRequest(SessionResourceKit srk, Deal deal)
          throws Exception {

      DealPK pk = new DealPK(deal.getDealId(), deal.getCopyId());
      Request lastRequest = new Request(srk);
      lastRequest = lastRequest.findLastRequest(pk);
      ServiceInfo serviceInfo = new ServiceInfo();
      serviceInfo.setDeal(deal);
      serviceInfo.setDealId(new Integer(deal.getDealId()));
      serviceInfo.setCopyId(new Integer(deal.getCopyId()));
      serviceInfo.setSrk(srk);
      serviceInfo.setLanguageId(deal.getSessionResourceKit().getLanguageId());
      serviceInfo.setProductType(new Integer(deal.getMortgageInsurerId()));

      serviceInfo.setRequestId(lastRequest.getRequestId());
      ServiceDelegate SD = new ServiceDelegate();
      SD.sendMIRequest(serviceInfo);
  }

  /**
   * Express 5.0 MI Cancel check the if dealstatus is deny or collapse, 
   * and prepare out MI Cancel Request
   * @return
   * @throws Exception 
   */
  public void sendCGMICancelRequest(SessionResourceKit srk, Deal deal) throws Exception {

      boolean processQueue = false;
      boolean dealStatus = false;
      boolean miStatus = false;

      int miStatusId = deal.getMIStatusId();
      int dealStatusId = deal.getStatusId();

      if (dealStatusId == Mc.DEAL_COLLAPSED || dealStatusId == Mc.DEAL_DENIED)
          dealStatus = true;
      if (miStatusId < Mc.MI_STATUS_CANCELLATION_PENDING
              || miStatusId > Mc.MI_STATUS_CANCELLED)
          miStatus = true;

      if ((deal.getMIPolicyNumber() != null) && dealStatus && miStatus)
          processQueue = true;
      
      if (processQueue) {
          generateCGMICancelRequest(srk, deal);
          //addDealHistoryOnAutoCancel(deal, srk);
      }
  }
  
  private void addDealHistoryOnAutoCancel(Deal deal, SessionResourceKit srk)
  {
      try
      {
	     DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);
	     int dealId = deal.getDealId();
	     int copyId = deal.getCopyId();
	     int upid = srk.getExpressState().getUserProfileId();
	     int MIId = deal.getMortgageInsurerId();
	     dhl.log(dealId, copyId, dhl.getMIAutoCancelMsg(MIId), Mc.DEAL_TX_TYPE_EVENT, upid );
      }
      catch (RemoteException e)
      {
    	  logger.error("Remote Exception adding deal history on auto MI cancellation");
    	  e.printStackTrace();
      } 
      catch (FinderException e) 
      {
    	  logger.error("Finder Exception adding deal history on auto MI cancellation");
    	  e.printStackTrace();
      } 
      catch (Exception e) 
      {
    	  logger.error("General Exception adding deal history on auto MI cancellation");
    	  e.printStackTrace();

      }
      
  }
}




