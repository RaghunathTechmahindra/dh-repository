package com.basis100.deal.miprocess;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;

public class MIProcessHandlerWrapper
{
  
  private int MIStatusId = MIProcessHandler.NO_MIStatusId;
  private int MIIndicatorId = MIProcessHandler.NO_MIIndicatorId;
  private double MIPremiumAmount = MIProcessHandler.NO_MIPremiumAmount;
  private int MortgageInsurerId = MIProcessHandler.NO_MortgageInsurerId;
  private String MIPolicyNumber = MIProcessHandler.NO_MIPolicyNumber;
  private int MIPayorId = MIProcessHandler.NO_MIPayorId;
  private int MITypeId = MIProcessHandler.NO_MITypeId;
  private String MIUpfront = MIProcessHandler.NO_MIUpfront;
  private String MICommunicationFlag = MIProcessHandler.NO_MICommunicationFlag;
  private String OpenMIResponseFlag = MIProcessHandler.NO_OpenMIResponseFlag;
  private String MIExistingPolicyNumber = MIProcessHandler.NO_MIExistingPolicyNumber;
  private String MIComments = MIProcessHandler.NO_MIComments;
  private String MIResponse = MIProcessHandler.NO_MIResponse;
  private String PreQualMICertNum = MIProcessHandler.NO_PreQualMICertNum;
  private String CMHCPolicyNumber = MIProcessHandler.NO_CMHCPolicyNumber;
  private String GEPolicyNumber = MIProcessHandler.NO_GEPolicyNumber;
  private int LOCAmortizationMonths = MIProcessHandler.NO_LOCAmortizationMonths;
  private long LOCInterestOnlyMaturityDate = MIProcessHandler.NO_LOCInterestOnlyMaturityDate;
  private double MIFeeAmount = MIProcessHandler.NO_MIFeeAmount;
  private double MIPremiumPST = MIProcessHandler.NO_MIPremiumPST;
  private String MIPolicyNumAIGUG = MIProcessHandler.NO_MIPolicyNumAIGUG;
  private String MIPolicyNumPMI = MIProcessHandler.NO_MIPolicyNumPMI;

  public MIProcessHandlerWrapper()
  {
  }
  
//  public void propogateDealFeeRemoval(SessionResourceKit srk, Deal deal, int[] reqTypes) throws Exception
//  {
//    handler.propogateDealFeeRemoval(srk, deal, reqTypes);
//  }


  public void propogateDealFeeRemoval(SessionResourceKit srk, Deal deal) throws Exception
  {
    MIProcessHandler.getMIAttributesPropogator().removeFeeFromDeal(srk, deal);
  }


  public void propagateMIAttributes(SessionResourceKit srk, Deal deal) throws Exception
  {
    if (srk == null) 
      throw new IllegalArgumentException("srk is null");
    if (deal == null) 
      throw new IllegalArgumentException("deal is null");
    
    MIProcessHandler.getMIAttributesPropogator().propogateMIAttributes
    (srk, deal, MIStatusId, MIIndicatorId,
        MIPremiumAmount, MortgageInsurerId, MIPolicyNumber, MIPayorId,
        MITypeId, MIUpfront, MICommunicationFlag, OpenMIResponseFlag,
        MIExistingPolicyNumber, MIComments, MIResponse, PreQualMICertNum,
        CMHCPolicyNumber, GEPolicyNumber, LOCAmortizationMonths,
        LOCInterestOnlyMaturityDate, MIFeeAmount, MIPremiumPST,
        MIPolicyNumAIGUG,MIPolicyNumPMI);
  }


  public String getCMHCPolicyNumber()
  {
    return CMHCPolicyNumber;
  }

  public void setCMHCPolicyNumber(String policyNumber)
  {
    CMHCPolicyNumber = policyNumber;
  }

  public String getGEPolicyNumber()
  {
    return GEPolicyNumber;
  }

  public void setGEPolicyNumber(String policyNumber)
  {
    GEPolicyNumber = policyNumber;
  }

  public int getLOCAmortizationMonths()
  {
    return LOCAmortizationMonths;
  }

  public void setLOCAmortizationMonths(int amortizationMonths)
  {
    LOCAmortizationMonths = amortizationMonths;
  }

  public long getLOCInterestOnlyMaturityDate()
  {
    return LOCInterestOnlyMaturityDate;
  }

  public void setLOCInterestOnlyMaturityDate(long interestOnlyMaturityDate)
  {
    LOCInterestOnlyMaturityDate = interestOnlyMaturityDate;
  }

  public String getMIComments()
  {
    return MIComments;
  }

  public void setMIComments(String comments)
  {
    MIComments = comments;
  }

  public String getMICommunicationFlag()
  {
    return MICommunicationFlag;
  }

  public void setMICommunicationFlag(String communicationFlag)
  {
    MICommunicationFlag = communicationFlag;
  }

  public String getMIExistingPolicyNumber()
  {
    return MIExistingPolicyNumber;
  }

  public void setMIExistingPolicyNumber(String existingPolicyNumber)
  {
    MIExistingPolicyNumber = existingPolicyNumber;
  }

  public double getMIFeeAmount()
  {
    return MIFeeAmount;
  }

  public void setMIFeeAmount(double feeAmount)
  {
    MIFeeAmount = feeAmount;
  }

  public int getMIIndicatorId()
  {
    return MIIndicatorId;
  }

  public void setMIIndicatorId(int indicatorId)
  {
    MIIndicatorId = indicatorId;
  }

  public int getMIPayorId()
  {
    return MIPayorId;
  }

  public void setMIPayorId(int payorId)
  {
    MIPayorId = payorId;
  }

  public String getMIPolicyNumAIGUG()
  {
    return MIPolicyNumAIGUG;
  }

  public void setMIPolicyNumAIGUG(String policyNumAIGUG)
  {
    MIPolicyNumAIGUG = policyNumAIGUG;
  }

  public String getMIPolicyNumPMI()
  {
      return MIPolicyNumPMI;
  }

  public void setMIPolicyNumPMI(String policyNumPMI)
  {
      MIPolicyNumPMI = policyNumPMI;
  }
  
  public String getMIPolicyNumber()
  {
    return MIPolicyNumber;
  }

  public void setMIPolicyNumber(String policyNumber)
  {
    MIPolicyNumber = policyNumber;
  }

  public double getMIPremiumAmount()
  {
    return MIPremiumAmount;
  }

  public void setMIPremiumAmount(double premiumAmount)
  {
    MIPremiumAmount = premiumAmount;
  }

  public double getMIPremiumPST()
  {
    return MIPremiumPST;
  }

  public void setMIPremiumPST(double premiumPST)
  {
    MIPremiumPST = premiumPST;
  }

  public String getMIResponse()
  {
    return MIResponse;
  }

  public void setMIResponse(String response)
  {
    MIResponse = response;
  }

  public int getMIStatusId()
  {
    return MIStatusId;
  }

  public void setMIStatusId(int statusId)
  {
    MIStatusId = statusId;
  }

  public int getMITypeId()
  {
    return MITypeId;
  }

  public void setMITypeId(int typeId)
  {
    MITypeId = typeId;
  }

  public String getMIUpfront()
  {
    return MIUpfront;
  }

  public void setMIUpfront(String upfront)
  {
    MIUpfront = upfront;
  }

  public int getMortgageInsurerId()
  {
    return MortgageInsurerId;
  }

  public void setMortgageInsurerId(int mortgageInsurerId)
  {
    MortgageInsurerId = mortgageInsurerId;
  }

  public String getOpenMIResponseFlag()
  {
    return OpenMIResponseFlag;
  }

  public void setOpenMIResponseFlag(String openMIResponseFlag)
  {
    OpenMIResponseFlag = openMIResponseFlag;
  }

  public String getPreQualMICertNum()
  {
    return PreQualMICertNum;
  }

  public void setPreQualMICertNum(String preQualMICertNum)
  {
    PreQualMICertNum = preQualMICertNum;
  }
}
