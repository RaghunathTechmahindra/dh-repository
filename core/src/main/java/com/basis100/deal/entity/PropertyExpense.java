
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PropertyExpensePK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;

public class PropertyExpense extends DealEntity {
    protected int propertyExpenseId;
    protected int propertyExpensePeriodId;
    protected int propertyExpenseTypeId;
    protected int propertyId;
    protected double propertyExpenseAmount;
    protected String propertyExpenseDescription;
    protected double monthlyExpenseAmount;
    protected boolean peIncludeInGDS;
    protected boolean peIncludeInTDS;
    protected int pePercentInGDS;
    protected int pePercentInTDS;
    protected int pePercentOutGDS;
    protected int pePercentOutTDS;

    protected int copyId;
    protected int institutionProfileId;

    private PropertyExpensePK pk;

    public PropertyExpense(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException {

        super(srk, dcm);
    }

    public PropertyExpense(SessionResourceKit srk, CalcMonitor dcm, int id,
            int copyId) throws RemoteException, FinderException {

        super(srk, dcm);

        pk = new PropertyExpensePK(id, copyId);

        findByPrimaryKey(pk);
    }

    public PropertyExpense create(PropertyPK ppk) throws RemoteException,
            CreateException {

        pk = createPrimaryKey(ppk.getCopyId());

        // set default value for PropertyExpense type and associated defaults --
        // by BILLY 11Jan2001
        // set PropertyExpenseType Default to option 0 (Municipal Taxes)
        int propertyExpenseTypeId = 0;

        // added inst id for ML: -1 is no institutionnized table inst Id
        String percentInGDS = PicklistData.getMatchingColumnValue( -1,
                "PropertyExpenseType", propertyExpenseTypeId, "GDSINCLUSION");
        String includeInGDS = ("0".equals(percentInGDS)) ? "'N'" : "'Y'";

        // added inst id for ML: -1 is no institutionnized table inst Id
        String percentInTDS = PicklistData.getMatchingColumnValue( -1, 
                "PropertyExpenseType", propertyExpenseTypeId, "TDSINCLUSION");
        String includeInTDS = ("0".equals(percentInTDS)) ? "'N'" : "'Y'";

        String sql = "Insert into PropertyExpense( "
                + "PropertyExpenseId, PropertyId, copyId, propertyexpenseamount, "
                + "propertyexpensetypeid, pepercentingds, peincludeingds, pepercentintds, peincludeintds,INSTITUTIONPROFILEID )"
                + " Values ( " + pk.getId() + "," + ppk.getId() + ","
                + pk.getCopyId() + ", 0.0, " + propertyExpenseTypeId + ", "
                + percentInGDS + ", " + includeInGDS + ", " + percentInTDS
                + ", " + includeInTDS + ","
                + srk.getExpressState().getDealInstitutionId() + ")";
        // ======================== End of changes -- by BILLY 11Jan2001
        // ===========================

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "propertyexpense Entity - PropertyExpense - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);

        if (dcm != null) {
            // dcm.inputEntity(this);
            dcm.inputCreatedEntity(this);
        }

        return this;
    }

    private PropertyExpensePK createPrimaryKey(int copyId)
            throws CreateException {

        String sql = "Select propertyexpenseseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }
            jExec.closeData(key);
            if (id == -1) {
                throw new Exception();
            }

        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "propertyexpense Entity create() exception getting PropertyExpenseId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }

        return new PropertyExpensePK(id, copyId);
    }

    public PropertyExpense findByName(String description, int copyId)
            throws RemoteException, FinderException {

        if (description == null) {
            return null;
        }

        description = description.toUpperCase();
        String sql = "Select * from " + getEntityTableName().toUpperCase()
                + " where upper(propertyexpensedescription) = '" + description
                + "' and copyId = " + copyId;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                this.copyId = copyId;
                setPropertiesFromQueryResult(key);
                this.pk = new PropertyExpensePK(this.propertyExpenseId, copyId);
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByName = " + description;
                logger.error(msg);
                return null;
            }
        } catch (Exception e) {
            logger.error("finder sql: " + sql);
            logger.error(e);
            return null;
        }
        return this;
    }

    public Collection findByParentAndType(PropertyPK pk, int type)
            throws RemoteException, FinderException {

        StringBuffer sqlbuf = new StringBuffer(
                "Select * from PropertyExpense where ");
        sqlbuf.append("propertyExpenseTypeId = ").append(type);
        sqlbuf.append(" and propertyId = ").append(pk.getId());
        sqlbuf.append(" and copyId = ").append(pk.getCopyId());

        Collection records = new ArrayList();

        try {
            int key = jExec.execute(sqlbuf.toString());

            for (; jExec.next(key);) {
                PropertyExpense pe = new PropertyExpense(srk, dcm);
                pe.setPropertiesFromQueryResult(key);
                pe.pk = new PropertyExpensePK(pe.getPropertyExpenseId(), pe
                        .getCopyId());

                records.add(pe);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger.error("finder sql: " + sqlbuf.toString());
            logger.error(e);
            return null;
        }

        return records;
    }

    public Collection<PropertyExpense> findByProperty(PropertyPK pk) throws Exception {

        StringBuffer sqlbuf = new StringBuffer(
                "Select * from PropertyExpense where ");
        sqlbuf.append("propertyId = ").append(pk.getId());
        sqlbuf.append(" and copyId = ").append(pk.getCopyId());

        Collection<PropertyExpense> records = new ArrayList<PropertyExpense>();

        try {
            int key = jExec.execute(sqlbuf.toString());

            for (; jExec.next(key);) {
                PropertyExpense pe = new PropertyExpense(srk, dcm);
                pe.setPropertiesFromQueryResult(key);
                pe.pk = new PropertyExpensePK(pe.getPropertyExpenseId(), pe
                        .getCopyId());

                records.add(pe);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger.error("finder sql: " + sqlbuf.toString());
            logger.error(e);
            return null;
        }

        return records;
    }

    public Collection findByDeal(DealPK dpk) throws Exception {

        StringBuffer sqlbuf = new StringBuffer(
                "Select * from PropertyExpense where ");
        sqlbuf
                .append("propertyId in( select PropertyId from property where dealId = ");
        sqlbuf.append(dpk.getId()).append(" and copyId = ").append(
                dpk.getCopyId());
        sqlbuf.append(") and copyId = ").append(dpk.getCopyId());

        Collection records = new ArrayList();

        String sql = sqlbuf.toString();

        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                PropertyExpense pe = new PropertyExpense(srk, dcm);
                pe.setPropertiesFromQueryResult(key);
                pe.pk = new PropertyExpensePK(pe.getPropertyExpenseId(), pe
                        .getCopyId());

                records.add(pe);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            String msg = "PropertyExpense: @findByDeal() error occured.";
            logger.error(msg);
            logger.error(e);
            throw e;
        }

        return records;
    }

    public PropertyExpense findByPrimaryKey(PropertyExpensePK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from propertyexpense " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public PropertyExpense deepCopy() throws CloneNotSupportedException {

        return (PropertyExpense) this.clone();
    }

    // GETTERS & SETTERS

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    public int getCopyId() {

        return this.copyId;
    }

    public int getPropertyExpenseId() {

        return this.propertyExpenseId;
    }

    public void setPropertyExpenseId(int propertyExpenseId) {

        this.testChange("propertyExpenseId", propertyExpenseId);
        this.propertyExpenseId = propertyExpenseId;
    }

    public int getPropertyExpensePeriodId() {

        return this.propertyExpensePeriodId;
    }

    public void setPropertyExpensePeriodId(int propertyExpensePeriodId) {

        this.testChange("propertyExpensePeriodId", propertyExpensePeriodId);
        this.propertyExpensePeriodId = propertyExpensePeriodId;
    }

    public int getPropertyExpenseTypeId() {

        return this.propertyExpenseTypeId;
    }

    public void setPropertyExpenseTypeId(int propertyExpenseTypeId) {

        this.testChange("propertyExpenseTypeId", propertyExpenseTypeId);
        this.propertyExpenseTypeId = propertyExpenseTypeId;
    }

    public int getPropertyId() {

        return this.propertyId;
    }

    public void setPropertyId(int propertyId) {

        this.testChange("propertyId", propertyId);
        this.propertyId = propertyId;
    }

    public double getPropertyExpenseAmount() {

        return this.propertyExpenseAmount;
    }

    public void setPropertyExpenseAmount(double propertyExpenseAmount) {

        this.testChange("propertyExpenseAmount", propertyExpenseAmount);
        this.propertyExpenseAmount = propertyExpenseAmount;
    }

    public String getPropertyExpenseDescription() {

        return this.propertyExpenseDescription;
    }

    public void setPropertyExpenseDescription(String propertyExpenseDescription) {

        this.testChange("propertyExpenseDescription",
                propertyExpenseDescription);
        this.propertyExpenseDescription = propertyExpenseDescription;
    }

    public double getMonthlyExpenseAmount() {

        return this.monthlyExpenseAmount;
    }

    public void setMonthlyExpenseAmount(double monthlyExpenseAmount) {

        this.testChange("monthlyExpenseAmount", monthlyExpenseAmount);
        this.monthlyExpenseAmount = monthlyExpenseAmount;
    }

    public boolean getPeIncludeInGDS() {

        return this.peIncludeInGDS;
    }

    public boolean getPeIncludeInTDS() {

        return this.peIncludeInTDS;
    }

    public int getPePercentInGDS() {

        return this.pePercentInGDS;
    }

    public int getPePercentInTDS() {

        return this.pePercentInTDS;
    }

    public int getPePercentOutGDS() {

        return this.pePercentOutGDS;
    }

    public int getPePercentOutTDS() {

        return this.pePercentOutTDS;
    }

    public void setPeIncludeInGDS(String c) {

        this.testChange("peIncludeInGDS", c);
        this.peIncludeInGDS = TypeConverter.booleanFrom(c, "N");
    }

    public void setPeIncludeInTDS(String c) {

        this.testChange("peIncludeInTDS", c);
        this.peIncludeInTDS = TypeConverter.booleanFrom(c, "N");
    }

    public void setPePercentInGDS(int i) {

        this.testChange("pePercentInGDS", i);
        this.pePercentInGDS = i;
    }

    public void setPePercentInTDS(int i) {

        this.testChange("pePercentInTDS", i);
        this.pePercentInTDS = i;
    }

    public void setPePercentOutGDS(int i) {

        this.testChange("pePercentOutGDS", i);
        this.pePercentOutGDS = i;
    }

    public void setPePercentOutTDS(int i) {

        this.testChange("pePercentOutTDS", i);
        this.pePercentOutTDS = i;
    }

    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public void setPropertiesFromQueryResult(int key) throws Exception {
        this.setPropertyExpenseId(jExec.getInt(key, "PropertyExpenseId"));
        this.setPropertyExpensePeriodId(jExec.getInt(key,
                "PropertyExpensePeriodId"));
        this.setPropertyExpenseTypeId(jExec
                .getInt(key, "PropertyExpenseTypeId"));
        this.setPropertyId(jExec.getInt(key, "PropertyId"));
        this.setPropertyExpenseAmount(jExec.getDouble(key,
                "PropertyExpenseAmount"));
        this.setPropertyExpenseDescription(jExec.getString(key,
                "PropertyExpenseDescription"));
        this.setMonthlyExpenseAmount(jExec.getDouble(key,
                "MonthlyExpenseAmount"));
        this.setPeIncludeInGDS(jExec.getString(key, "PeIncludeInGDS"));
        this.setPeIncludeInTDS(jExec.getString(key, "PeIncludeInTDS"));
        this.setPePercentInGDS(jExec.getInt(key, "PePercentInGDS"));
        this.setPePercentInTDS(jExec.getInt(key, "PePercentInTDS"));
        this.setPePercentOutGDS(jExec.getInt(key, "PePercentOutGDS"));
        this.setPePercentOutTDS(jExec.getInt(key, "PePercentOutTDS"));
        this.setInstitutionProfileId(jExec.getInt(key, "InstitutionProfileId"));
        copyId = jExec.getInt(key, "copyId");
    }

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0) {
            dcm.inputEntity(this);
        }

        return ret;
    }

    public String getEntityTableName() {

        String name = this.getClass().getName();
        int index = name.lastIndexOf(".");

        if (index == -1) {
            return name;
        }

        return name.substring(index + 1, name.length());
    }

    public Vector findByMyCopies() throws RemoteException, FinderException {

        Vector v = new Vector();

        String sql = "Select * from PropertyExpense where propertyExpenseId = "
                + getPropertyExpenseId() + " AND copyId <> " + getCopyId();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                PropertyExpense iCpy = new PropertyExpense(srk, dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new PropertyExpensePK(iCpy.getPropertyExpenseId(),
                        iCpy.getCopyId());

                v.addElement(iCpy);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "PropertyExpense - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return v;
    }

    public int getClassId() {

        return ClassId.PROPERTYEXPENSE;
    }

    public boolean isAuditable() {

        return true;
    }

    public boolean equals(Object o) {

        if (o instanceof PropertyExpense) {
            PropertyExpense oa = (PropertyExpense) o;
            if ((this.propertyExpenseId == oa.getPropertyExpenseId())
                    && (this.copyId == oa.getCopyId())) {
                return true;
            }
        }
        return false;
    }

    protected EntityContext getEntityContext() throws RemoteException {

        EntityContext ctx = this.entityContext;
        try {
            ctx.setContextText(this.propertyExpenseDescription);

            Property property = new Property(srk, null, this.propertyId,
                    this.copyId);
            ctx.setContextSource(property.getPropertyStreetNumber()
                    + " "
                    + property.getPropertyStreetName()
                    + " "
                    + PicklistData.getDescription("AddressType", property
                            .getPropertyTypeId()));

            // Modified by Billy for performance tuning -- By BILLY 28Jan2002
            // Deal deal = new Deal ( srk, dcm, property.getDealId() ,
            // property.getCopyId() ) ;
            // String sc = String.valueOf ( deal.getScenarioNumber ()) +
            // deal.getCopyType () ;
            // if ( deal == null )
            // return ctx;
            // String sc = String.valueOf ( deal.getScenarioNumber () ) +
            // deal.getCopyType();
            Deal deal = new Deal(srk, dcm);
            String sc = String.valueOf(deal.getScenarioNumber(property
                    .getDealId(), property.getCopyId()))
                    + deal.getCopyType(property.getDealId(), property
                            .getCopyId());
            // ===================================================================================

            ctx.setScenarioContext(sc);

            ctx.setApplicationId(property.getDealId());

        } catch (Exception e) {
            if (e instanceof FinderException) {
                logger
                        .warn("PropertyExpense.getEntityContext Parent Not Found. PropertyExpenseId="
                                + this.propertyExpenseId);
                return ctx;
            }
            throw (RemoteException) e;
        } // ----------- end of getEntityContext ----------------------
        return ctx;
    } // ----------------- end of getEntityContext ---------------------

}
