/**
 * <p>
 * Title: SysProperty.java
 * </p>
 * 
 * <p>
 * Description: SysProperty class is enitty class for system property in DB
 * </p>
 * <p>
 * It originally mossys.proproperties,,,
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2007
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @version 1.0(Initial Version � May 24, 2007)
 * 
 */
package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Properties;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * 
 * SysProperty
 * 
 * @version 2.0 Oct 3, 2007
 * @author Midori Aida
 */
public class SysProperty extends EntityBase {
    public static final int    SYSTEMTYPE_ALL               = 0;
    public static final int    SYSTEMTYPE_GUI               = 1;
    public static final int    SYSTEMTYPE_DP                = 2;
    public static final int    SYSTEMTYPE_IG                = 3;
    public static final int    SYSTEMTYPE_AME               = 4;
    public static final int    SYSTEMTYPE_WS                = 5;
    public static final int    SYSTEMTYPE_ECM               = 6;

    public static final String TABLE                        = "SYSPROPERTY";
    public static final String COLOMUN_SYSPROPERTYTYPEID    = "SYSPROPERTYTYPEID";
    public static final String COLOMUN_NAME                 = "NAME";
    public static final String COLOMUN_INSTITUTIONPROFILEID = "INSTITUTIONPROFILEID";
    public static final String COLOMUN_VALUE                = "VALUE";
    public static final String COLOMUN_DESCRIPTION          = "DESCRIPTION";
    public static final String COLOMUN_BUILD_VERSION        = "BUILD_VERSION";
    public static final String COLOMUN_CREATED_DATE         = "CREATED_DATE";
    public static final String COLOMUN_NOTES                = "NOTES";

    /*
     * SysPropertyTypeId should be only one par application i.e. GUI, DP, IG,
     * AME, WS,,,,, The value is from flat file?
     */
    protected int              _sysPropertyTypeId           = -1;
    protected int              _sysPropertyId;
    protected int              _institutionProfileId;
    protected String           _name;
    protected String           _value;
    protected String           _buildVersion;
    protected Date             _createdDate;
    protected String           _description;
    protected String           _notes;

    public SysProperty(SessionResourceKit srk, int sysPropertyTypeId) {
        super(srk);
        _sysPropertyTypeId = sysPropertyTypeId;
    }

    /**
     * 
     * @param name
     * @return
     * @throws FinderException
     */
    public SysProperty findByName(String name) throws FinderException {
        if (name == null || name.trim().length() == 0) {
            return null;
        }

        String sql = "Select * from " + TABLE + " where "
                + COLOMUN_SYSPROPERTYTYPEID + " in (0, " + _sysPropertyTypeId
                + " ) and " + COLOMUN_NAME + " = '" + name + "' order by "
                + COLOMUN_SYSPROPERTYTYPEID;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // only the first one i.e. syspropertyid can only be one
                        // record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "SYSPROPERTY Entity: @findByName(), key= " + name
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "SYSPROPERTY Entity - findByName() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this._name = name;
        return this;
    }

    /**
     * 
     * @param key
     * @throws Exception
     */
    private void setPropertiesFromQueryResult(int key) throws Exception {
        setSysPropertyId(jExec.getInt(key, COLOMUN_SYSPROPERTYTYPEID));
        setName(jExec.getString(key, COLOMUN_NAME));
        setInstitutionProfileId(jExec.getInt(key, COLOMUN_INSTITUTIONPROFILEID));
        setValue(jExec.getString(key, COLOMUN_VALUE));
        setDescription(jExec.getString(key, COLOMUN_DESCRIPTION));
        setBuildVersion(jExec.getString(key, COLOMUN_BUILD_VERSION));
        setCreatedDate(jExec.getDate(key, COLOMUN_CREATED_DATE));
        setNotes(jExec.getString(key, COLOMUN_NOTES));
    }

    public String getBuildVersion() {
        return _buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        _buildVersion = buildVersion;
    }

    public Date getCreatedDate() {
        return _createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        _createdDate = createdDate;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public int getInstitutionProfileId() {
        return _institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        _institutionProfileId = institutionProfileId;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public void setSysPropertyId(int sysPropertyId) {
        _sysPropertyId = sysPropertyId;
    }

    public int getSysPropertyId() {
        return _sysPropertyId;
    }

    public String getValue() {
        return _value;
    }

    public void setValue(String value) {
        _value = value;
    }

    public String getNotex() {
        return _notes;
    }

    public void setNotes(String notes) {
        this._notes = notes;
    }

    /**
     * 
     * @param name
     * @param defaultValue
     * @return
     */
    public String getProperty(String name, String defaultValue) {
        try {
            findByName(name);
            String dbValue = getValue();
            if (dbValue == null || dbValue.trim().length() == 0)
                return defaultValue;
            else
                return dbValue;
        } catch (FinderException fe) {
            return defaultValue;
        }
    }

    /**
     * get all properties for the institution Id
     * @param insititutionId
     * @return
     * @throws FinderException
     */
    public Properties getProperties(int insititutionId) throws FinderException {
        // String sql = "Select * from " + TABLE + " where " +
        // COLOMUN_SYSPROPERTYTYPEID + " in (0, "
        String sql = "Select * from " + TABLE + " where "
                + COLOMUN_SYSPROPERTYTYPEID + " in (" + _sysPropertyTypeId
                + " ) and " + COLOMUN_INSTITUTIONPROFILEID + " = "
                + insititutionId;

        try {
            Properties prop = new Properties();
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                setPropertiesFromQueryResult(key);
                prop.setProperty(getName(), getValue());
            }

            jExec.closeData(key);
            if (prop.isEmpty()) {
                FinderException fe = new FinderException(
                        "SYSPROPERTY Entity - getProperties() exception");
                logger.error(fe.getMessage());
                logger.error("propertis are empty for insitutionId = "
                        + insititutionId);

                throw fe;
            }
            return prop;
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "SYSPROPERTY Entity - getProperties() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
    }

    /**
     * 
     * @return
     * @throws FinderException
     */
    public Collection<SysProperty> getSysProperties() throws FinderException {
        String sql = "Select * from " + TABLE + " where "
                + COLOMUN_SYSPROPERTYTYPEID + " in (0, " + _sysPropertyTypeId
                + ")";

        try {
            Collection<SysProperty> sysProps = new LinkedHashSet<SysProperty>();
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                SysProperty sysProp = new SysProperty(srk, _sysPropertyTypeId);
                sysProp.setPropertiesFromQueryResult(key);
                sysProps.add(sysProp);
            }

            jExec.closeData(key);
            if (sysProps.isEmpty()) {
                FinderException fe = new FinderException(
                        "SYSPROPERTY Entity - getProperties() exception");
                logger.error(fe.getMessage());
                logger.error("propertis are empty");

                throw fe;
            }
            return sysProps;
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "SYSPROPERTY Entity - getProperties() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
    }

}
