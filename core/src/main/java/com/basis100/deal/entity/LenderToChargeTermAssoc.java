package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.LenderToChargeTermAssocPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class  LenderToChargeTermAssoc extends DealEntity
{
   protected int    lenderToTermId;              //  NUMBER 13
   protected int    lenderProfileId;             //  NUMBER 13
   protected int    chargeTermId;                //  NUMBER 13
   protected String description;                 //  VARCHAR2(35)

   protected LenderToChargeTermAssocPK pk;

   public LenderToChargeTermAssoc(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

   public LenderToChargeTermAssoc(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new LenderToChargeTermAssocPK(id);

      findByPrimaryKey(pk);
   }


  public LenderToChargeTermAssoc create(LenderToChargeTermAssocPK pk)throws RemoteException, CreateException
  {
      String sql = "Insert into lendertochargetermassoc(" + pk.getName() + " ) Values ( " + pk.getId() + ")";
      try
      {
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("lendertochargetermassoc Entity -LenderToChargeTermAssoc - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
      this.pk = pk;
      this.lenderToTermId = pk.getId();
      srk.setModified(true);
      return this;
  }

  public LenderToChargeTermAssocPK createPrimaryKey()throws CreateException
  {
     String sql = "Select lendertochargetermassocseq.nextval from dual";
     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }
         jExec.closeData(key);
         if( id == -1 ) throw new Exception();

      }
      catch (Exception e)
      {
        CreateException ce = new CreateException(
        " Entity LenderToChargeTermAssoc create() exception getting lenderToTermId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
      }

      return new LenderToChargeTermAssocPK(id);
  }

  public LenderToChargeTermAssoc findByName(String description) throws RemoteException, FinderException
  {
     String sql = "Select * from "+ getEntityTableName() +
     " where description = '" + description + "'";

     boolean gotRecord = false;

     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
             gotRecord = true;
             setPropertiesFromQueryResult(key);
             this.pk = new LenderToChargeTermAssocPK(this.lenderToTermId);
             break;
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "Deal Entity: @findByName = " + description;
          logger.error(msg);
          return null;
        }
      }
      catch (Exception e)
      {
        logger.error("finder sql: " + sql);
        logger.error(e);
        return null;
      }
    return this;
  }

  public LenderToChargeTermAssoc findByPrimaryKey(LenderToChargeTermAssocPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "select * from lendertochargetermassoc where " + pk.getName()+ " = '" + pk.getId() + "'";

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
   }
   public LenderToChargeTermAssoc deepCopy() throws CloneNotSupportedException
   {
      return (LenderToChargeTermAssoc)this.clone();
   }
//GETTERS & SETTERS
   public int getLenderToTermId()
   {
      return this.lenderToTermId;
   }
   public void setLenderToTermId(int value)
   {
       this.lenderToTermId =  value;
       this.testChange("lenderToTermId", value);
   }

   public int getLenderProfileId()
   {
      return this.lenderProfileId;
   }
   public void setLenderProfileId(int value)
   {
      this.lenderProfileId = value;
      this.testChange("lenderProfileId", value);
   }

   public int getChargeTermId()
   {
      return this.chargeTermId;
   }
   public void setChargeTermId(int value)
   {
      this.chargeTermId = value;
      this.testChange("chargeTermId", value);
   }

   public String getDescription()
   {
      return this.description;
   }
     public void setDescription(String value)
   {
      this.description = value;
     	this.testChange("description", value);
   }

     public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }
      

    /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
   public void setField(String fieldName, String value) throws Exception
  {
      doSetField(this, this.getClass(), fieldName, value);
   }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      int index = 0;
      setLenderProfileId( jExec.getInt(key,++index));
      setChargeTermId( jExec.getInt(key,++index));
      setLenderToTermId( jExec.getInt(key,++index));
      setDescription( jExec.getString(key,++index));

   }


    /**
    * @returns the size of this entity's change map -ie the number of keys;
    */
    protected int performUpdate()  throws Exception
    {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
    }

    public String getEntityTableName()
    {
      String name = this.getClass().getName();
      int index = name.lastIndexOf(".");

      if(index == -1) return "";

      return name.substring(index + 1, name.length());
    }



}
