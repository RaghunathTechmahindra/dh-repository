package com.basis100.deal.entity;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 */

import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import MosSystem.Mc;

import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.*;
import com.basis100.resources.SessionResourceKit;

public class Condition extends DealEntity
{
   protected int     conditionId;
   protected int     conditionTypeId;
   protected String  conditionDesc;
   protected int     conditionResponsibilityRoleId;
   protected int     conditionBasedOnDateId;
   protected int     durationDays;
   protected int     conditionCalculationTypeId;
   protected boolean stApprovalBypass;
   protected String  userSelectable;
   protected int     institutionId;
   protected int     dtVerbiageGenerationType;

   private ConditionPK pk;

   private static int LANGUAGE_MAX = 1;

   private List verb;
   
   public static interface Ec { // #DG638 those are just local entity constants 
     public static final String CONDITION = "Condition";
		public static interface CL { // columns constants 
			public static final String CONDITION_ID = "conditionId";
			public static final String CONDITION_TYPE_ID = "conditionTypeId";
			public static final String CONDITION_RESPONSIBILITY_ROLE_ID = "conditionResponsibilityRoleId";
			public static final String DURATION_DAYS = "durationDays";
			public static final String CONDITION_CALCULATION_TYPE_ID = "conditionCalculationTypeId";
			public static final String CONDITION_DESC = "conditionDesc";
			public static final String ST_APPROVAL_BYPASS = "stApprovalBypass";
		  public static final String CONDITION_BASED_ON_DATE_ID = "conditionBasedOnDateId";
		}
	}   
   
   public Condition(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
      verb = new ArrayList();		//#DG638
   }

   public Condition(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      //#DG638 super(srk);
      this(srk);

      pk = new ConditionPK(id);
      findByPrimaryKey(pk);
   }


  protected ConditionPK createPrimaryKey()throws CreateException
  {
    String sql = "Select Conditionseq.nextval from dual";
    int id = -1;

    try
    {
       int key = jExec.execute(sql);

       for (; jExec.next(key);  )
       {
           id = jExec.getInt(key,1);  // can only be one record
       }

       jExec.closeData(key);

       if( id == -1 ) throw new Exception();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Condition Entity create() exception getting ConditionId from sequence");
      logger.error(ce.getMessage());
      logger.error(e);
      throw ce;
    }

    return new ConditionPK(id);
  }

	//#DG638 rewritten
  public Condition findByPrimaryKey(ConditionPK pka) 
  	throws RemoteException, FinderException {

 		pk = pka;
		String sql = "Select *  from Condition where " + pka.getName() + " = "+ pka.getId();
		return findBySql(sql);
   }

	//#DG638 rewritten
  /**
   * Finds Condition records with the supplied conditionTypeId and conditionDesc.<br>
   * <b>Note: </b>This is a <i>relatively</i> temporary method due to be deprecated but providing compatibility
   * for document generation <code>TagExtractor</code> subclass calls by label in release one.
   * @param aid -  conditionTypeId
   * @param desc - conditionDesc
   * @return a collection of Condition Entities
   */
  public Condition findByTypeAndDescription(int aid, String desc) throws RemoteException, FinderException
  {

    String sql = "Select * from Condition where conditionTypeId = " + aid +
    " and conditionDesc = \'" + desc + "\'";
    return findBySql(sql);
  }

  /** #DG638 unified method to retrieve a single condition
   * 
   * @param pk
   * @param sql
   * @return
   * @throws FinderException
   */
  public Condition findBySql(String sql) throws FinderException {
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

 	gotRecord = jExec.next(key);
 	if(gotRecord)	// only 0 or 1 record is expected 
            setPropertiesFromQueryResult(key);

        jExec.closeData(key);

 	if (!gotRecord ) {
          String msg = "Condition Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
          throw new FinderException(msg);
        }
 	loadVerbiage();
      }
      catch (Exception e) {
 	FinderException fe = new FinderException(
 			"Condition Entity - findByPrimaryKey() exception:" + e);

 	if (gotRecord || !getSilentMode() ) {
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
 	}
          throw fe;
      }

      return this;
  }


   //#DG638 rewritten
   public Collection findByConditionType(int aid) throws Exception
  {

    String sql = "Select * from Condition where conditionTypeId = " + aid;

    return findBySqlColl(sql);
   }

	//#DG638 rewritten
   public Collection findAllExceptType(int aid) throws Exception {

		String sql = "Select * from Condition where conditionTypeId != " + aid;
		return findBySqlColl(sql);
	}

 	/** #DG638 unified method to retrieve a list of conditions
 	 *
 	 * @param sql sql input command
 	 * @return list of conditions
 	 * @throws Exception
 	 */
 	public Collection findBySqlColl(String sql) throws Exception {
		Collection conditionList = new Vector();
      try {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            Condition condition = new Condition(srk);
            condition.setPropertiesFromQueryResult(key);
            condition.loadVerbiage();
            condition.pk = new ConditionPK(condition.getConditionId());
            conditionList.add(condition);
        }
        jExec.closeData(key);
      }
      catch (Exception e)
      {
          Exception fe = new Exception("Condition Entity - findByConditionType() exception:" + e);

			if (!getSilentMode()) {
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
			}
          throw fe;
      }
      return conditionList;
   }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     this.setConditionId(jExec.getShort(key, "CONDITIONID"));
     this.setConditionTypeId(jExec.getInt(key, "CONDITIONTYPEID"));
     this.setConditionResponsibilityRoleId(jExec.getInt(key, "CONDITIONRESPONSIBILITYROLEID"));
     this.setConditionBasedOnDateId(jExec.getInt(key, "CONDITIONBASEDONDATEID"));
     this.setDurationDays(jExec.getInt(key, "DURATIONDAYS"));
     this.setConditionCalculationTypeId( jExec.getInt(key, "CONDITIONCALCULATIONTYPEID"));
     this.setStApprovalBypass(jExec.getString(key, "STAPPROVALBYPASS"));
     this.setConditionDesc(jExec.getString(key, "CONDITIONDESC"));
     this.setUserSelectable(jExec.getString(key, "USERSELECTABLE"));
	 this.setDTVerbiageGenerationType(jExec.getInt(key, "DTVERBIAGEGENERATIONTYPEID"));
     this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID")); 
   }

   private void loadVerbiage() throws Exception
   {
   	  //=================================================================
   	  // CLOB Performance Enhancement June 2010
      String sql = "select LANGUAGEPREFERENCEID, CONDITIONLABEL, length(CONDITIONTEXT) text_length, CAST(SUBSTR(CONDITIONTEXT,1,4000) as VARCHAR(4000)) conditiontext_varchar, CONDITIONTEXT from ConditionVerbiage " +
      "where ConditionId = " + getConditionId() + " order by LANGUAGEPREFERENCEID";
      //=================================================================
      try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )        {
            //#DG638 this.setVerbiagePropertiesFromQueryResult(key);      
        	//=================================================================
        	//CLOB Performance Enhancement June 2010
        	String tmpConText = "";
        	if(jExec.getInt(key, "text_length") <= 4000) {	
        		tmpConText = jExec.getString(key, "conditiontext_varchar");
        	} else {
        		tmpConText = TypeConverter.stringFromClob(jExec.getClob(key, "CONDITIONTEXT"));
        	}
        	final ConditionVerbiage conditionVerbiage = new ConditionVerbiage(this.conditionId,
                  jExec.getInt(key, "LANGUAGEPREFERENCEID"),jExec.getString(key, "CONDITIONLABEL"),
                  tmpConText);
          //================================================================
          
          verb.add(conditionVerbiage);        	
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("Condition Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }

   }

		/* #DG638 
   private void setVerbiagePropertiesFromQueryResult(int key) throws Exception   {
     if(verb == null)
      verb = new ArrayList();
     verb.add(new ConditionVerbiage(this.conditionId,
          jExec.getInt(key,1),jExec.getString(key,2),jExec.getString(key,3)));
   }*/


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate()  throws Exception
  {
      ListIterator li = verb.listIterator();
      ConditionVerbiage cv = null;

      while(li.hasNext())
      {
        cv = (ConditionVerbiage)li.next();

        if(cv.getConditionId() < 0)    //new record if id = -1
        {
          cv.createRecord(this.getConditionId());
        }
        else
        {
          cv.store(this.getConditionId());
        }

      }

      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      //#DG638 return "Condition";
      return Ec.CONDITION;
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a ConditionPK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }




   /**
    * <pre>
    *    create a Condition record given a ConditionPK primary key.
    *     these create methods assume a standard list of records and are therefore
    *     a utility for record entry.
    *    @param pk - the primary key for this record
    *    @return this instance
    * </pre>
    */
   public Condition create(ConditionPK pk)throws RemoteException, CreateException
   {
        String sql = "Insert into Condition(" + pk.getName() + ", INSTITUTIONPROFILEID )"
            + " Values ( '" + pk.getId() + "'" + ", " + srk.getExpressState().getDealInstitutionId() 
            + " )";

        try
        {
          jExec.executeUpdate(sql);
          this.findByPrimaryKey(pk);
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Condition Entity - ConditionType - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

        srk.setModified(true);
        this.pk = pk;

        return this;
   }
  /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }


   /**
    *   <pre>
    *    create a Condition record given an id (primary key) and a description
    *    @param id the primary key for this record
    *    @param desc the description of this Condition object
    *    @return this instance
    *   </pre>
    */

   public Condition create( int conditionTypeId, String conditionDesc,
                             int conditionResponsibilityRoleId, int conditionBasedOnDateId,
                              int durationDays, int conditionCalculationTypeId,
                                boolean stApprovalBypass
                                )throws RemoteException, CreateException
   {
        pk = this.createPrimaryKey();

        StringBuffer sqlbuf = new StringBuffer() ;
        sqlbuf.append("Insert into Condition(conditionId, conditionDesc, " );
        sqlbuf.append("conditionResponsibilityRoleId,  conditionBasedOnDateId, ");
        sqlbuf.append("durationDays,  conditionCalculationTypeId ");
        sqlbuf.append("stApprovalBypass Values ( '");
        sqlbuf.append(pk.getId()).append( "', '");
        sqlbuf.append(conditionDesc).append( "', '");
        sqlbuf.append(conditionResponsibilityRoleId).append( "', '");
        sqlbuf.append(conditionBasedOnDateId).append( "', '");
        sqlbuf.append(durationDays).append( "', '");
        sqlbuf.append(conditionCalculationTypeId).append( "', '");
        sqlbuf.append(TypeConverter.stringFromBoolean(stApprovalBypass,"N","Y")).append( "')");

        try
        {
          jExec.executeUpdate(sqlbuf.toString());
          findByPrimaryKey(pk);
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Condition Entity - ConditionType - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }
     srk.setModified(true);
     return this;
  }



  public void setConditionId(int value  )
  {
    this.testChange(Ec.CL.CONDITION_ID, value);
    this.conditionId = value;
  }

  public int getConditionId(){return this.conditionId;}

  public void setConditionTypeId(int value )
  {
    this.testChange(Ec.CL.CONDITION_TYPE_ID, value);
    this.conditionTypeId = value;
  }

  public int getConditionTypeId(){return this.conditionTypeId;}

  public void setConditionLabel(int ndx, String text)
  {

    ConditionVerbiage cv = null;

    //#DG638 if(verb == null) verb = new ArrayList();

    try
    {
      cv = (ConditionVerbiage)verb.get(ndx);
    }
    catch(ArrayIndexOutOfBoundsException a){;}

    if(cv == null)

    verb.set(ndx,new ConditionVerbiage(this.conditionId,ndx, text, null));
    cv.setConditionLabel(text);
  }

  public String getConditionLabel(int lang)
  {
    try
    {
      ConditionVerbiage cv = (ConditionVerbiage)verb.get(lang);
      return cv.getConditionLabel();
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
      return null;
    }
     catch(NullPointerException np)
    {
      return null;
    }

  }

  public String getConditionTextByLanguage(int languagePref)
  {
    try
    {
      ConditionVerbiage cv = null;

      Iterator i = verb.iterator();

      while(i.hasNext())
      {
        cv = (ConditionVerbiage)i.next();
        if(cv.getLanguagePreferenceId() == languagePref)
        {
          return cv.getConditionText();
        }
      }
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
      return null;
    }
     catch(NullPointerException np)
    {
      return null;
    }

    return null;
  }

  public void setConditionResponsibilityRoleId(int value)
  {
    this.testChange(Ec.CL.CONDITION_RESPONSIBILITY_ROLE_ID, value);
    this.conditionResponsibilityRoleId = value;
  }
  public int getConditionResponsibilityRoleId(){ return this.conditionResponsibilityRoleId;}


  public void setDurationDays(int value)
  {
    this.testChange(Ec.CL.DURATION_DAYS, value);
    this.durationDays = value;
  }
  public int getDurationDays(){ return this.durationDays;}

  public void setConditionCalculationTypeId(int value)
  {
    this.testChange(Ec.CL.CONDITION_CALCULATION_TYPE_ID, value);
    this.conditionCalculationTypeId = value;
  }
  public int getConditionCalculationTypeId(){ return this.conditionCalculationTypeId;}



  public void setLanguagePreferenceId(int lang, int id)
  {
    if(lang < 0 || lang > LANGUAGE_MAX)
     return;

    ConditionVerbiage cv = null;

    try
    {
      cv = (ConditionVerbiage)verb.get(lang);
    }
    catch(ArrayIndexOutOfBoundsException a){;}

    if(cv == null)

    verb.set(lang,new ConditionVerbiage(this.conditionId,lang, null, null));

  }


  public int getLanguagePreferenceId(int ndx)
  {
    try
    {
      ConditionVerbiage cv = (ConditionVerbiage)verb.get(ndx);
      return cv.getLanguagePreferenceId();
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
      return -1;
    }
    catch(NullPointerException np)
    {
      return -1;
    }

  }

  public void setConditionText(int ndx, String text)
  {
    //#DG638 if(verb == null) verb = new ArrayList();
    ConditionVerbiage cv = null;

    try
    {
      cv = (ConditionVerbiage)verb.get(ndx);
    }
    catch(ArrayIndexOutOfBoundsException a){;}

    if(cv == null)
    verb.set(ndx,new ConditionVerbiage(this.conditionId ,-1, null, text));
  }

  public String getConditionText(int ndx)
  {
    try
    {
      ConditionVerbiage cv = (ConditionVerbiage)verb.get(ndx);
      return cv.getConditionText();
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
      return null;
    }
    catch(NullPointerException np)
    {
      return null;
    }
  }

  public String getConditionLabelByLanguage(int languagePref)
  {
    try
    {
      ConditionVerbiage cv = null;

      Iterator i = verb.iterator();

      while(i.hasNext())
      {
        cv = (ConditionVerbiage)i.next();
        if(cv.getLanguagePreferenceId() == languagePref)
        {
          return cv.getConditionLabel();
        }
      }
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
      return null;
    }
     catch(NullPointerException np)
    {
      return null;
    }

    return null;
  }


  public void setConditionDesc(String value)
  {
      this.testChange(Ec.CL.CONDITION_DESC, value);
      this.conditionDesc = value;
  }
  public String getConditionDesc(){ return this.conditionDesc;}

  private void setStApprovalBypass(String bp)
  {
    this.stApprovalBypass = TypeConverter.booleanFrom(bp,"F");
  }

  public void setStApprovalBypass(boolean value)
  {
    this.testChange(Ec.CL.ST_APPROVAL_BYPASS, value);
    this.stApprovalBypass = value;
  }
  public boolean getStApprovalBypass(){ return this.stApprovalBypass;}

  public void setConditionBasedOnDateId(int value)
  {
    this.testChange(Ec.CL.CONDITION_BASED_ON_DATE_ID, value);
    this.conditionBasedOnDateId = value;
  }
  public int getConditionBasedOnDateId() { return this.conditionBasedOnDateId ; }


  /**
   * Business method for use by DocumentTracking entity
   */
  /* #DG638 not used 
  public boolean hasVerbiage()  {
    if(verb == null) return false;
    return !verb.isEmpty();
  }*/

  /**
   * Business method for use by DocumentTracking entity
   */
  public int verbiageSize()
  {
    /* 		//#DG638
    if(hasVerbiage())
     return verb.size();
    else
     return 0;*/
    return verb.size();
  }

  /**
   *  A less than perfect tactic for disabling a condition is to set it's type
   *  to Mc.CONDITION_TYPE_INACTIVE. Hence the following convienience method for
   *  checking the value. Uninitialized conditions will of course appear to be active.
   */
  public boolean isActiveType()
  {
    return getConditionTypeId() != Mc.CONDITION_TYPE_INACTIVE;
  }

  @SuppressWarnings("unchecked")
  public List<ConditionVerbiage> getConditionVerbiages(){
	  return verb;
  }

  public class ConditionVerbiage
  {
    String conditionText;  //This is >= 4K ==> needed special handling when update to database -- BILLY 25Feb2002
    String conditionLabel;
    int languagePreferenceId;
    int conditionId = -1;

    ConditionVerbiage(int conditionId, int lang, String lab, String verb)
    {
      conditionText = verb;
      conditionLabel =  lab;
      languagePreferenceId = lang;
      this.conditionId = conditionId;
    }
    
    //4.4 Entity Cache
    public ConditionVerbiage(ConditionVerbiage other){
        conditionText = other.conditionText;
        conditionLabel =  other.conditionLabel;
        languagePreferenceId = other.languagePreferenceId;
        this.conditionId = other.conditionId;
    } 

    public void setConditionText(String Text)
    {
      conditionText = Text;
    }
    public String getConditionText(){ return conditionText;}

    public void setLanguagePreferenceId(int id)
    {
      languagePreferenceId = id;
    }
    public int getLanguagePreferenceId(){return languagePreferenceId;}


    public void setConditionLabel(String label)
    {
      conditionLabel = label;
    }
    public String getConditionLabel(){ return conditionLabel;}


    public void setConditionId(int id)
    {
      conditionId = id;
    }

    public int getConditionId(){return conditionId;}

    // Modified to use PrepareStatement to handle the SQL problem with String > 4K
    //    -- By BILLY 25Feb2002
    void store(int id)throws Exception
    {
      /*
      String sql = "Update ConditionVerbiage set conditionLabel = " +
                    conditionLabel + ", conditionText = " +
                    conditionText + " where conditionId = " +
                    id + " and languagePreferenceId = " +
                    languagePreferenceId;
      */
      String sql = "Update ConditionVerbiage set conditionLabel = ?, conditionText = ? " +
                    " where conditionId = ? and languagePreferenceId = ?";
      
      // FXP25883
      Clob clob = null;
        try
        {
          // Get the PrepareStatement
          PreparedStatement pstmt = jExec.getCon().prepareStatement(sql);

          // Set conditionLabel
          pstmt.setString(1, conditionLabel);
          /* #DG638 
          String tmpData = conditionText;
          StringBufferInputStream strStream = new StringBufferInputStream(tmpData);
          pstmt.setAsciiStream(2, strStream, tmpData.length()); */
          
          // FXP25883
          clob = createClob(conditionText);
          pstmt.setClob(6, clob);         

          // Set conditionId
          pstmt.setInt(3, conditionId);

          // Set conditionId
          pstmt.setInt(4, languagePreferenceId);

          pstmt.executeUpdate();

          //jExec.executeUpdate(sql);

        }
        catch(Exception s)
        {
           throw new Exception("ConditionVerbiage - could not update record : " + s);
        } finally {// FXP25883 
            try {
                if (clob != null) 
                    oracle.sql.CLOB.freeTemporary((oracle.sql.CLOB)clob);
            } catch (SQLException sqle) {
                logger.debug("could not release CLOB temp");
                logger.error(sqle);
            }
        }
    }

    void createRecord(int id)throws CreateException
    {
      StringBuffer col = new StringBuffer("Insert into ConditionVerbiage( conditionId, ");

      StringBuffer val = new StringBuffer(" values( ").append(id);

      if(conditionText != null)
      {
        col.append("conditionText, ");
        val.append(", ").append(conditionText).append("' ");
      }
      if(conditionLabel != null)
      {
        col.append("conditionLabel, ");
        val.append(", ").append(conditionLabel).append("' ");
      }
      if(languagePreferenceId != -1)
      {
        col.append("languagePreferenceId, ");
        val.append(", ").append(languagePreferenceId);

      }
      col.append(") ");
      val.append(") ");
      String sql = col.toString() + val.toString();

      try
      {
        jExec.executeUpdate(sql);
      }
      catch(Exception s)
      {
         throw new CreateException("ConditionVerbiage - could not create record : " + s.getMessage());
      }

    }
  }


  /**
   * @return the institutionId
   */
  public int getInstitutionId()
  {
    return institutionId;
  }

  /**
   * @param institutionId the institutionId to set
   */
  public void setInstitutionId(int institutionId)
  {
    this.testChange("institutionProfileId", institutionId);
    this.institutionId = institutionId;
  }

  /**
   * @return the userSelectable
   */
  public String getUserSelectable()
  {
    return userSelectable;
  }

  /**
   * @param userSelectable the userSelectable to set
   */
  public void setUserSelectable(String userSelectable)
  {
    this.userSelectable = userSelectable;
  }
  
  public int getDTVerbiageGenerationType() {
	  return dtVerbiageGenerationType;
  }
  
  public void setDTVerbiageGenerationType(int dtVerbiageGenerationType) {
	  this.dtVerbiageGenerationType = dtVerbiageGenerationType;
  }

  public Object clone(){
	  Condition clonedCondition = null;
	  try {
		 clonedCondition = (Condition)super.clone();
	  } catch (CloneNotSupportedException e) {
		logger.error(e);
	  }
	  return clonedCondition;
  }
}

