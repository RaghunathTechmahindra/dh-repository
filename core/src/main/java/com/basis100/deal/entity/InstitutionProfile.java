package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class InstitutionProfile extends DealEntity
{
   protected int      institutionProfileId;
   protected String   institutionName;
   protected String   brandName;
   protected int      contactId;
   protected int      profileStatusId;
   protected String   CCAPSId;
   protected String   CMHCId;
   protected String   GEId;
   protected int      timeZoneEntryId;
   protected double   tranRate;
   protected String   ECNILenderId;
   protected String   equifaxId;
   protected String   mortyLenderId;
   protected int      interestCompoundId;
   protected boolean  threeYearRateCompareDiscount;
   protected String   CSClientNumber;
   protected String   CSMortgageNumber;
   protected String   ESClientNumber;
   protected String   ESMortgageNumber;
   protected int      REBackDateLimit;
   protected int      REEffectiveDateLimit;
   protected boolean  enableDupeCheckFlag;
   protected int      dupeCheckTimeFrame ;
   protected String   AIGUGId;    //AAtcha
   protected String   institutionAbbr;  //MAida for ML
   protected String   PMIId;

   private InstitutionProfilePK pk;

   public InstitutionProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

   public InstitutionProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new InstitutionProfilePK(id);

      findByPrimaryKey(pk);
   }

    public InstitutionProfile findByPrimaryKey(InstitutionProfilePK pk) throws RemoteException, FinderException
   {
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      // Currently we only support only ONE institution, in order not to hard code the ID
      //  here we just do a search and get the first on as the required record.
      // It is only a temp fix.  Needed to be rework later.
      String sql = "Select * from InstitutionProfile where " + pk.getName()+ " = " + pk.getId();
      //String sql = "Select * from InstitutionProfile";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }
            
          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "InstitutionProfile: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("InstitutionProfile - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

   /** findByFirst:
   *
   * Find by first record in table - there should only be one!
   *
   **/

   public InstitutionProfile findByFirst() throws RemoteException, FinderException
   {
      String sql = "Select * from InstitutionProfile";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "InstitutionProfile: @findByFirst(), entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Page Entity: @findByFirst(), exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }

        this.pk = new InstitutionProfilePK(this.getInstitutionProfileId());
        return this;
    }
   
   
   /**
    * Get the lowest institutionProfileId
    * @return
    * @throws RemoteException
    * @throws FinderException
    */
   public int findLowestId() throws RemoteException, FinderException
   {
      String sql = "Select min(institutionProfileId) as institutionProfileId from InstitutionProfile";

      int instProfileId = 0;
      try
      {
          int key = jExec.execute(sql);
          for (; jExec.next(key); )
          {
              instProfileId = jExec.getInt(key,"institutionProfileId");
              break; // can only be one record
          }
          jExec.closeData(key);
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Page Entity: @findLowestId(), exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        return instProfileId;
    }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setInstitutionProfileId(jExec.getInt(key,"institutionProfileId"));
      this.setInstitutionName(jExec.getString(key,"institutionName"));
      this.setBrandName(jExec.getString(key,"brandName"));
      this.setContactId( jExec.getInt(key,"contactId"));
      this.setProfileStatusId(jExec.getInt(key,"profileStatusId"));
      this.setCCAPSId(jExec.getString(key,"CCAPSId"));
      this.setCMHCId(jExec.getString(key,"CMHCId"));
      this.setGEId(jExec.getString(key,"GEId"));
      this.setTimeZoneEntryId(jExec.getInt(key,"timeZoneEntryId"));
      this.setTranRate(jExec.getDouble(key,"tranRate"));
      this.setECNILenderId(jExec.getString(key,"ECNILenderId"));
      this.setEquifaxId(jExec.getString(key,"equifaxId"));
      this.setMortyLenderId(jExec.getString(key,"mortyLenderId"));
      this.setInterestCompoundId(jExec.getInt(key,"interestCompoundingId"));
      this.setThreeYearRateCompareDiscount(jExec.getString(key,"threeYearRateCompareDiscount"));
      this.setCSClientNumber(jExec.getString(key,"CSClientNumber"));
      this.setCSMortgageNumber(jExec.getString(key,"CSMortgageNumber"));
      this.setESClientNumber(jExec.getString(key,"ESClientNumber"));
      this.setESMortgageNumber(jExec.getString(key,"ESMortgageNumber"));
      this.setREBackDateLimit(jExec.getInt(key,"REBackDateLimit"));
      this.setREEffectiveDateLimit(jExec.getInt(key,"REEffectiveDateLimit"));
      this.setEnableDupeCheckFlag(jExec.getString(key,"enableDupeCheckFlag"));
      this.setDupeCheckTimeFrame(jExec.getInt(key,"dupeCheckTimeFrame"));
      this.setAIGUGId(jExec.getString(key,"AIGUGId"));  //AAtcha
      this.setInstitutionAbbr(jExec.getString(key,"institutionAbbr"));  //MAida for ML
      this.setPMIId(jExec.getString(key,"PMIId"));  //Mohan for PMI

   }

  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
 protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "InstitutionProfile";
   }    

  /**
   *   gets the profileStatusId associated with this entity
   *
   */
    public int getProfileStatusId()
   {
      return this.profileStatusId ;
   }



        /**
     *   gets the pk associated with this entity
     *
     *   @return a InstitutionProfilePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

  public InstitutionProfilePK createPrimaryKey()throws CreateException
  {
       String sql = "Select InstitutionProfileseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           
           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {

          CreateException ce = new CreateException("InstitutionProfile Entity create() exception getting InstitutionProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

    srk.setModified(true);
    return new InstitutionProfilePK(id);
  }



  /**
   * creates a InstitutionProfile using the InstitutionProfileId with mininimum fields
   *
   */

  public InstitutionProfile create(InstitutionProfilePK pk)throws RemoteException, CreateException
  {
      String sql = "Insert into InstitutionProfile(" + pk.getName() + " ) Values ( " + pk.getId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("InstitutionProfile Entity - InstitutionProfile - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      return this;
  }



  public void setInstitutionProfileId(int value)
  {
    this.testChange("institutionProfileId", value);
    this.institutionProfileId = value;
  }
  public int getInstitutionProfileId(){ return this.institutionProfileId ; }


  public void setInstitutionName(String value )
  {
    this.testChange("institutionName", value);
    this.institutionName = value;
  }
  public String getInstitutionName(){ return this.institutionName ; }


  public void setContactId(int value )
  {
    this.testChange("contactId", value);
    this.contactId = value;
  }
  public int getContactId(){ return this.contactId ; }

  /**
  *   retrieves the Contact parent record associated with this entity
  *
  *   @return a Contact
  */
  public Contact getContact()throws FinderException,RemoteException
  {
    return new Contact(srk,this.getContactId(),1);
  }

  public void setProfileStatusId(int value )
  {
    this.testChange("profileStatusId", value);
    this.profileStatusId = value;
  }

  public void setCCAPSId(String value)
  {
    this.testChange("CCAPSId", value);
    this.CCAPSId = value;
  }
  public String getCCAPSId(){return this.CCAPSId; }

  public void setCMHCId(String value)
  {
    this.testChange("CMHCId", value);
    this.CMHCId = value;
  }
  public String getCMHCId(){return this.CMHCId; }

  public void setGEId(String value)
  {
    this.testChange("GEId", value);
    this.GEId = value;
  }
  public String getGEId(){return this.GEId; }

  public void setTimeZoneEntryId(int value)
  {
    timeZoneEntryId = value;
    this.testChange("timeZoneEntryId", value);
  }
  public int getTimeZoneEntryId(){ return timeZoneEntryId; }

  public void setTranRate(double value)
  {
    this.testChange("tranRate", value);
    this.tranRate = value;
  }
  public double getTranRate(){ return tranRate;}

  public void setECNILenderId(String value)
  {
    this.testChange("ECNILenderId", value);
    this.ECNILenderId = value;
  }
  public String getECNILenderId(){return this.ECNILenderId; }

  public void setEquifaxId(String value)
  {
    this.testChange("equifaxId", value);
    this.equifaxId = value;
  }
  public String getEquifaxId(){return this.equifaxId; }

  public void setMortyLenderId(String value)
  {
  this.testChange("mortyLenderId", value);
  this.mortyLenderId = value;
  }
  public String getMortyLenderId(){return this.mortyLenderId; }

  public void setThreeYearRateCompareDiscount(boolean value)
  {
    this.testChange("threeYearRateCompareDiscount", value);
    this.threeYearRateCompareDiscount = value;
  }
  public void setThreeYearRateCompareDiscount(String value)
  {
    this.testChange("threeYearRateCompareDiscount", value);
    this.threeYearRateCompareDiscount = TypeConverter.booleanFrom(value);
  }
  public boolean getThreeYearRateCompareDiscount(){return this.threeYearRateCompareDiscount; }

  public void setCSClientNumber(String value)
  {
    this.testChange("CSClientNumber", value);
    this.CSClientNumber = value;
  }
  public String getCSClientNumber(){return this.CSClientNumber; }

  public void setCSMortgageNumber(String value)
  {
    this.testChange("CSMortgageNumber", value);
    this.CSMortgageNumber = value;
  }
  public String getCSMortgageNumber(){return this.CSMortgageNumber; }

  public void setESClientNumber(String value)
  {
    this.testChange("ESClientNumber", value);
    this.ESClientNumber = value;
  }
  public String getESClientNumber(){return this.ESClientNumber; }


  public void setESMortgageNumber(String value)
  {
    this.testChange("ESMortgageNumber", value);
    this.ESMortgageNumber = value;
  }
  public String getESMortgageNumber(){return this.ESMortgageNumber; }


  public void setREBackDateLimit(int value)
  {
    this.testChange("REBackDateLimit", value);
    this.REBackDateLimit = value;
  }
  public int    getREBackDateLimit(){return this.REBackDateLimit; }


  public void setREEffectiveDateLimit(int value)
  {
    this.testChange("REEffectiveDateLimit", value);
    this.REEffectiveDateLimit = value;
  }
  public int    getREEffectiveDateLimit(){return this.REEffectiveDateLimit; }


  public void setBrandName(String value)
  {
    this.testChange("BrandName", value);
    this.brandName = value;
  }
  public String getBrandName(){return this.brandName; }


  public void setInterestCompoundId(int value )
  {
    this.testChange("interestCompoundId", value);
    this.interestCompoundId = value;
  }
  public int    getInterestCompoundId(){return this.interestCompoundId; }

  public void setEnableDupeCheckFlag(boolean value)
  {
    this.testChange("enableDupeCheckFlag", value);
    this.enableDupeCheckFlag = value;
  }
  public void setEnableDupeCheckFlag(String value)
  {
    this.testChange("enableDupeCheckFlag", value);
    this.enableDupeCheckFlag = TypeConverter.booleanFrom(value);
  }
  public boolean getEnableDupeCheckFlag(){return this.enableDupeCheckFlag; }

  public boolean isDupeCheckEnabled(){return this.enableDupeCheckFlag; }

  public void setDupeCheckTimeFrame(int value )
  {
    this.testChange("dupeCheckTimeFrame", value);
    this.dupeCheckTimeFrame = value;
  }
  public int    getDupeCheckTimeFrame(){return this.dupeCheckTimeFrame; }
//AAtcha start
  public void setAIGUGId(String value)
  {
    this.testChange("AIGUGId",value);
    this.AIGUGId = value;
  }
  
  public String getAIGUGId(){return this.AIGUGId;}
  //AAtcha ends
  
  // added for ML - Midori, July 2, 2007 == START

  /**
   *<p>findByCMCHID</p>
   * get InstituitionProfile entity by CMCHID that is unique id, 
   * the col doesn't have unique constraint though
   * 
   * @param String id: unique id for CMCH (MI) 
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByCMHCID(String id)throws RemoteException, FinderException
  {
    return findByUniqueKey("CMHCID", id);
  }
  /**
   *<p>findByGEID</p>
   * get InstituitionProfile entity by GEID that is unique id, 
   * the col doesn't have unique constraint though
   * 
   * @param String id: unique id for GE (MI)
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByGEID(String id)throws RemoteException, FinderException
  {
    return findByUniqueKey("GEID", id);
  }
  /**
   *<p>findByAIGUGID</p>
   * get InstituitionProfile entity by AIUGID that is unique id, 
   * the col doesn't have unique constraint though
   * 
   * @param String id: unique id for AIGUG (MI)
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByAIGUGID(String id)throws RemoteException, FinderException
  {
    return findByUniqueKey("AIGUGID", id);
  }
  /**
   *<p>findByECNID</p>
   * get InstituitionProfile entity by ECNILENDERID that is unique id, 
   * the col doesn't have unique constraint though
   * 
   * @param String id: unique id for ECNI
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByECNID(String id)throws RemoteException, FinderException
  {
    return findByUniqueKey("ECNILENDERID", id);
  }
  /**
   *<p>findByEQUIFAXID</p>
   * get InstituitionProfile entity by EQUIFAXID that is unique id, 
   * the col doesn't have unique constraint though
   * 
   * @param String id: unique id for EQUIFAX
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByEQUIFAXID(String id)throws RemoteException, FinderException
  {
    return findByUniqueKey("EQUIFAXID", id);
  }
  /**
   *<p>findByMORTYLENDERID</p>
   * get InstituitionProfile entity by MORTYLENDERID that is unique id, 
   * the col doesn't have unique constraint though
   * 
   * @param String id: unique id for MORTYLENDER
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByMORTYLENDERID(String id)throws RemoteException, FinderException
  {
    return findByUniqueKey("MORTYLENDERID", id);
  }
  /**
   *<p>findByCCAPSID</p>
   * get InstituitionProfile entity by CCAPSID that is unique id, 
   * the col doesn't have unique constraint though
   * 
   * @param String id: unique id for CCAP (BMO)
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByCCAPSID(String id)throws RemoteException, FinderException
  {
    return findByUniqueKey("CCAPSID", id);
  }
  
  /**
   *<p>findByUniqueKey</p>
   * get InstituitionProfile entity found by any unique key 
   * the col doesn't have unique constraint though
   * 
   * @param String colName: column name that have unique key, such as CMHCID, GEID,,, 
   * @param String id: unique id for the column
   * @return InstitutionProfile entity
   */
  public InstitutionProfile findByUniqueKey(String colName, String id)throws RemoteException, FinderException
  {
    String sql = "Select * from InstitutionProfile where " + colName + "= '" + id + "'";   
    boolean gotRecord = false;    
    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }
         
      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "InstitutionProfile: @findByUniqueKey(), colName = " + colName 
          + " key= " + id + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException("InstitutionProfile - findByUniqueKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
    return this;
  }

  // added for ML - Midori
  public String getInstitutionAbbr()
  {
    return institutionAbbr;
  }

  // added for ML - Midori
  public void setInstitutionAbbr(String institutionAbbr)
  {
    this.institutionAbbr = institutionAbbr;
  }
  
  	//added for PMI - Mohan
    public String getPMIId() {
    	return PMIId;
    }
    
    //added for PMI - Mohan
	public void setPMIId(String PMIId) {
		this.testChange("pmiid", PMIId);
		this.PMIId = PMIId;
	}

	// to call this method, VPD should be off
    // i.e. not specified institionProfileId should be set
    public Collection<InstitutionProfile> getAllInstitutions() throws FinderException {
        
        Collection<InstitutionProfile> institutions = new ArrayList<InstitutionProfile>();
        String sql = "Select * from InstitutionProfile";   
        boolean gotRecord = false;
        try {
              int key = jExec.execute(sql);
    
              for (; jExec.next(key); ) {
                  InstitutionProfile profile = new InstitutionProfile(srk);
                  gotRecord = true;
                  profile.setPropertiesFromQueryResult(key);
                  institutions.add(profile);
              }
              jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "InstitutionProfile: @getAllInstitutions() entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("InstitutionProfile - getAllInstitutions() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        return institutions;
    }
    
    /**
     * Get the map of all institutions
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public HashMap<Integer,String> getInstitutionNames() throws RemoteException, FinderException
    {
       String sql = "Select institutionProfileId,  institutionName from InstitutionProfile";
       HashMap<Integer,String> institutions = new HashMap<Integer,String>();
       int instProfileId = 0;
       String instituionName;
       try
       {
           int key = jExec.execute(sql);
           for (; jExec.next(key); )
           {
               instProfileId = jExec.getInt(key,"institutionProfileId");
               instituionName = jExec.getString(key,"institutionName");
               institutions.put(new Integer(instProfileId), instituionName);
           }
           jExec.closeData(key);
         }
         catch (Exception e)
         {
           FinderException fe = new FinderException("Page Entity: @findLowestId(), exception");

           logger.error(fe.getMessage());
           logger.error("finder sql: " + sql);
           logger.error(e);

           throw fe;
         }
         return institutions;
     }
}
