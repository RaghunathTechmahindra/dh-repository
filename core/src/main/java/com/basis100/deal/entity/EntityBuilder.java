package com.basis100.deal.entity;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;
/**
 * <p>
 * Title: EntityBuilder
 * </p>
 * <p>
 * Description: Class to build all the entities.
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * @version 1.3 <br>
 *          Date: 08/02/2006 <br>
 * @author: NBC/PP Implementation Team<br>
 *          Change: <br>
 *          modified methods to handle creation of BorrowerIdentification <br> -
 *          modified following methods <br>
 *          constructEntity(String name, SessionResourceKit srk, CalcMonitor
 *          dcm, int id, int copyId)<br>
 *          createEntity(String name, SessionResourceKit srk, CalcMonitor dcm,
 *          DealEntity container))<br>
 * @version 1.4 <br>
 *          Date: Jun/23/20086 <br>
 * @author: MCM Implementation Team<br>
 *          Change: <br>
 *          modified methods to handle creation of ComponentLOC, Component<br> -
 *          modified following methods <br>
 *          constructEntity(String name, SessionResourceKit srk, CalcMonitor
 *          dcm, int id, int copyId)<br>
 * @version 1.5 <br>
 *          Date: Jun/25/20086 <br>
 * @author: MCM Implementation Team<br> - added ComponentMtg  to constructEntity
 * @version 1.6 <br>
 *          Date: 04-Jul-2008 <br>
 * @author: MCM Implementation Team<br> - added dcm in constructor's for
 *          Component,ComponentMortgage,ComponentLoc components in the
 *          constructEntity(..)
 * 
 * @version 1.7  <br> 
 * 			Date: July 3, 2008 <br>
 * @author: MCM Implementation Team<br>  added ComponentOverdraft to constructEntity
 *  * @version 1.8  <br> 
 *      Date: July 9, 2008 <br>
 * @author: MCM Implementation Team<br> added dcm in constructor's for ComponentLoan in the constructEntity
 * @version 1.9  <br> 
 *      Date: July 30, 2008 <br>
 * @author: MCM Implementation Team<br> added dcm in constructor's for ComponentCreditCard and ComponentOverDraft in the constructEntity
 * @version 1.10 <br>
 *  Date:07-Aug-2008 <br>
 *  Story :XS_1.1 <br>
 *  @author :MCM Implementation Team<br> Modified constructEntity(..),createEntity(..) to add QualifyDetail  
 * @version 1.11 <br>
 *  Date:11-Aug-2008 <br>
 *  Story :XS_1.2 <br>
 * @author :MCM Implementation Team<br> Modified constructEntity(..),createEntity(..) to add Component and related tables  
 */
public class EntityBuilder
{
  static int INITIAL = 1;

  public static DealEntity constructEntity(String name, SessionResourceKit srk, CalcMonitor dcm, int id, int copyId)
    throws FinderException, RemoteException
  {

     if(name.equals("Addr"))                  return new Addr(srk, id, copyId);
     if(name.equals("Asset"))                 return new Asset(srk, dcm, id, copyId);
     if(name.equals("Borrower"))              return new Borrower(srk,dcm, id, copyId);
     if(name.equals("BorrowerAddress"))       return new BorrowerAddress(srk,dcm, id,copyId);
     if(name.equals("BranchProfile"))         return new BranchProfile(srk, id);
     if(name.equals("Condition"))             return new Condition(srk, id);
     if(name.equals("Contact"))               return new Contact(srk,id,copyId);
     if(name.equals("CreditBureauReport"))    return new CreditBureauReport(srk, id);
     if(name.equals("CreditReference"))       return new CreditReference(srk,dcm ,id,copyId);
     if(name.equals("Deal"))                  return new Deal(srk,dcm,id,copyId);
     if(name.equals("DealFee"))               return new DealFee(srk,dcm,id,copyId);
     if(name.equals("DealNotes"))             return new DealNotes(srk,id);
     if(name.equals("Fee"))                   return new Fee(srk,dcm,id);
     if(name.equals("DownPaymentSource"))     return new DownPaymentSource(srk, dcm, id,copyId);
     if(name.equals("DocumentTracking"))      return new DocumentTracking(srk,id,copyId);
     if(name.equals("EscrowPayment"))         return new EscrowPayment(srk,dcm, id,copyId);
     if(name.equals("EmploymentHistory"))     return new EmploymentHistory(srk, id,copyId);
     if(name.equals("GroupProfile"))          return new GroupProfile(srk,id);
     if(name.equals("Income"))                return new Income(srk,dcm,id,copyId);
     if(name.equals("InvestorProfile"))       return new InvestorProfile(srk,id);
     if(name.equals("Liability"))             return new Liability(srk,dcm,id,copyId);
     if(name.equals("LenderProfile"))         return new LenderProfile(srk,id);
     if(name.equals("MtgProd"))               return new MtgProd(srk,dcm,id);
     if(name.equals("PricingProfile"))        return new PricingProfile(srk, id);
     if(name.equals("Property"))              return new Property(srk, dcm, id, copyId);
     if(name.equals("PropertyExpense"))       return new PropertyExpense(srk, dcm, id, copyId);
     if(name.equals("SourceFirmProfile"))     return new SourceFirmProfile(srk,id);
     if(name.equals("SourceOfBusinessProfile"))return new SourceOfBusinessProfile(srk,id);
     if(name.equals("Bridge"))                return new Bridge(srk, dcm, id, copyId);
     //--DJ_LDI_CR--start--//
     if(name.equals("LifeDisabilityPremiums"))return new LifeDisabilityPremiums(srk,dcm,id,copyId);
     //--DJ_LDI_CR--end--//
     //--DJ_CR136--start--//
     if(name.equals("InsureOnlyApplicant"))return new InsureOnlyApplicant(srk,dcm,id,copyId);
     if(name.equals("LifeDisPremiumsIOnlyA"))return new LifeDisPremiumsIOnlyA(srk,dcm,id,copyId);
     //--DJ_CR136--end--//
     //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
     if(name.equals("PrimeIndexRateProfile"))        return new PrimeIndexRateProfile(srk, id);
     //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

     if(name.equals("UserProfile"))
     {
          UserProfile ret = new UserProfile(srk);
          // added instituionProfileId from srk.ExpressState
          //TODO: check it works
          ret = ret.findByPrimaryKey(new UserProfileBeanPK(id, srk.getExpressState().getDealInstitutionId()));
          return ret;
     }
     if(name.equals("PartyProfile"))     return new PartyProfile(srk,id);
     if(name.equals("PricingRateInventory"))     return new PricingRateInventory(srk,id);

     //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//

     if(name.equals("BorrowerIdentification"))
    	 return new BorrowerIdentification(srk, id, copyId);

     //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//
     //  ***** Added dcm in construcor's Change by MCM Impl. Team - Version 1.6 - Start *****//
     //  ***** Change by MCM Impl. Team - Version 1.4 - Start *****//
     if(name.equals("ComponentLOC"))
    	 return new ComponentLOC(srk, dcm, id, copyId);
     if(name.equals("Component"))
    	 return new Component(srk, dcm,  id, copyId);
     //  ***** Change by MCM Impl. Team - Version 1.4 - End *****//
     //  ***** Change by MCM Impl. Team - Version 1.5 - Start *****//
     if(name.equals("ComponentMtg")|| name.equals("ComponentMortgage"))//  ***** Change by MCM Impl. Team - Version 1.10 - XS_1.2*****//
         return new ComponentMortgage(srk, dcm, id, copyId);
     //  ***** Change by MCM Impl. Team - Version 1.5 - End *****//
     //   ***** Added dcm in construcor's Change by MCM Impl. Team - Version 1.6 - End *****//
     //  ***** Change by MCM Impl. Team - Version 1.5 - Start *****//
     if(name.equals("ComponentCreditCard"))
         return new ComponentCreditCard(srk,dcm, id, copyId);
     //  ***** Change by MCM Impl. Team - Version 1.5 - End *****//

     //  ***** Change by MCM Impl. Team - Version 1.7 - Start *****//
     if(name.equals("ComponentOverdraft"))
         return new ComponentOverdraft(srk,dcm, id, copyId);
     //  ***** Change by MCM Impl. Team - Version 1.7 - End *****//

     //  ***** Change by MCM Impl. Team - Version 1.8 - Start *****//
     // ***** XS 2.37 MCM Impl. Team start *****//
     if(name.equals("ComponentLoan"))
         return new ComponentLoan(srk,dcm, id, copyId);
     // ***** XS 2.37 MCM Impl. Team End *****/
     //  ***** Change by MCM Impl. Team - Version 1.8 - End *****//
     
     //  ***** Change by MCM Impl. Team - Version 1.9 -XS_1.1 -  Starts *****//
     if(name.equals("QualifyDetail"))
         return new QualifyDetail(srk, id);
     //***** Change by MCM Impl. Team - Version 1.9 -XS_1.1 -  Ends *****//
     return null;
  }
  
  
  public static DealEntity constructEntity(int classId, SessionResourceKit srk, CalcMonitor dcm, int id, int copyId)
    throws FinderException, RemoteException
  {
	  String className = "";
	  switch (classId) {
	      case (ClassId.DEAL): {
	    	  className = "Deal";
	    	  break;
	      }
	      case (ClassId.BORROWER): {
	    	  className = "Borrower";
	    	  break;
	      }
	      case (ClassId.PROPERTY): {
	    	  className = "Property";
	    	  break;
	      }
	      case (ClassId.ASSET): {
	    	  className = "Asset";
	    	  break;
	      }
	      case (ClassId.DOWNPAYMENTSOURCE): {
	    	  className = "DownPaymentSource";
	    	  break;
	      }
	      case (ClassId.INCOME): {
	    	  className = "Income";
	    	  break;
	      }
	      case (ClassId.LIABILITY): {
	    	  className = "Liability";
	    	  break;
	      }
	      case (ClassId.PROPERTYEXPENSE): {
	    	  className = "PropertyExpense";
	    	  break;
	      }
	      case (ClassId.ESCROW): {
	    	  className = "EscrowPayment";
	    	  break;
	      }
	      case (ClassId.CONDITION): {
	    	  className = "Condition";
	    	  break;
	      }
	      case (ClassId.PRICINGPROFILE): {
	    	  className = "PricingProfile";
	    	  break;
	      }
	      case (ClassId.PRICINGRATEINVENTORY): {
	    	  className = "PricingRateInventory";
	    	  break;
	      }
	      case (ClassId.FEE): {
	    	  className = "Fee";
	    	  break;
	      }
	      case (ClassId.DEALFEE): {
	    	  className = "DealFee";
	    	  break;
	      }
	      case (ClassId.EMPLOYMENTHISTORY): {
	    	  className = "EmploymentHistory";
	    	  break;
	      }
	      case (ClassId.DOCUMENTTRACKING): {
	    	  className = "DocumentTracking";
	    	  break;
	      }
	      case (ClassId.MTGPROD): {
	    	  className = "MtgProd";
	    	  break;
	      }
	      case (ClassId.BRIDGE): {
	    	  className = "Bridge";
	    	  break;
	      }
	      case (ClassId.PARTYPROFILE): {
	    	  className = "PartyProfile";
	    	  break;
	      }
	      case (ClassId.BORROWERADDRESS): {
	    	  className = "BorrowerAddress";
	    	  break;
	      }
	      case (ClassId.CREDITREFERENCE): {
	    	  className = "CreditReference";
	    	  break;
	      }
	      case (ClassId.CONTACT): {
	    	  className = "Contact";
	    	  break;
	      }
	      case (ClassId.CREDITBUREAUREPORT): {
	    	  className = "CreditBureauReport";
	    	  break;
	      }
	      case (ClassId.LIFEDISABILITYPREMIUMS): {
	    	  className = "LifeDisabilityPremiums";
	    	  break;
	      }
	      case (ClassId.INSUREONLYAPPLICANT): {
	    	  className = "InsureOnlyApplicant";
	    	  break;
	      }
	      case (ClassId.LIFEDISPREMIUMSIONLYA): {
	    	  className = "LifeDisPremiumsIOnlyA";
	    	  break;
	      }
	      case (ClassId.BORROWERIDENTIFICATION): {
	    	  className = "BorrowerIdentification";
	    	  break;
	      }
	  }
	  
	  return constructEntity(className, srk, dcm, id, copyId);
  }

    /**
   *  Constructs a new DealEntity subclass and calls create(..). The use of the
   *  id parameter is subclass specific. Subclasses not required to fullfill
   *  foriegn key constraints on creation do not use the parentId parameter.
   *  If a entity is not creatable (lacks a sequence for generation of primary keys) or
   *  is not represented here - null is returned.
   *  @version 1.0 Initial Version
   *  @version 1.4 XS-1.1 07-Aug-2008 Added if condition for Creating QualifyDetails.
   *  @version 1.5 XS-1.2 11-Aug-2008 Added if condition for Creating Component and related tables. 
   *  @returns the named entity or null if it can not be created.
   */
  public static DealEntity
              createEntity(String name, SessionResourceKit srk,
                            CalcMonitor dcm, DealEntity container)
                              throws FinderException, RemoteException, CreateException
  {
       if(name.equals("Asset"))
       {
           Asset ret = new Asset(srk,dcm);
           return ret.create((BorrowerPK)container.getPk());
       }
       if(name.equals("Borrower"))
       {
           Borrower ret = new Borrower(srk,dcm);
           return ret.create((DealPK)container.getPk());
       }
       if(name.equals("BorrowerAddress"))
       {
           BorrowerAddress ret = new BorrowerAddress(srk);
           return ret.create((BorrowerPK)container.getPk());
       }

      if(name.equals("Bridge"))
      {
           Bridge ret = new Bridge(srk,dcm);
           return ret.create((DealPK)container.getPk());
       }

       if(name.equals("Contact"))
       {
           Contact contact = new Contact(srk);

           String containerName = container.getClass().getName();

           if(containerName.equals("com.basis100.deal.entity.EmploymentHistory"))
           {
              EmploymentHistory eh = (EmploymentHistory)container;
              contact = contact.create((EmploymentHistoryPK)eh.getPk());
              eh.setContactId(contact.getContactId());
              return contact;
           }
           else
           {
              return contact.create();
           }
       }
       if(name.equals("CreditBureauReport"))
       {
           CreditBureauReport ret = new CreditBureauReport(srk);
           return ret.create((DealPK)container.getPk());
       }
       if(name.equals("CreditReference"))
       {
           CreditReference ret = new CreditReference(srk, dcm);
           return ret.create((BorrowerPK)container.getPk());
       }
       if(name.equals("Deal"))
       {
           MasterDeal md = new MasterDeal(srk,dcm);
           MasterDealPK pk = md.createPrimaryKey(srk.getExpressState().getDealInstitutionId());
           // this method create Deal entity and return it
           return md.create(pk);
       }
       if(name.equals("DealFee"))
       {
           DealFee df = new DealFee(srk,dcm);
           return df.create((DealPK)container.getPk());
       }
       if(name.equals("DealNotes"))
       {
           DealNotes dn = new DealNotes(srk);
           return dn.create((DealPK)container.getPk());
       }
       if(name.equals("DownPaymentSource"))
       {
           DownPaymentSource ret = new DownPaymentSource(srk,dcm);
           return ret.create((DealPK)container.getPk());
       }
       if(name.equals("EscrowPayment"))
       {
           EscrowPayment ret = new EscrowPayment(srk,dcm);
           return ret.create((DealPK)container.getPk());
       }

       if(name.equals("EmploymentHistory"))
       {
           EmploymentHistory ret = new EmploymentHistory(srk);
           return ret.create((BorrowerPK)container.getPk());
       }
       if(name.equals("Income"))
       {
           Income inc = new Income(srk,dcm);
           String classname = container.getClass().getName();

           if( classname.equals("com.basis100.deal.entity.EmploymentHistory"))
           {
              EmploymentHistory parent = (EmploymentHistory)container;
              inc = inc.create((EmploymentHistoryPK)container.getPk());
              parent.setIncomeId(inc.getIncomeId());
              return inc;

           }
           else
           {
              return inc.create((BorrowerPK)container.getPk());
           }
       }
       if(name.equals("Liability"))
       {
           Liability ret = new Liability(srk,dcm);
           return ret.create((BorrowerPK)container.getPk());
       }
       if(name.equals("Property"))
       {           
           Property ret = new Property(srk,dcm);
           return ret.create((DealPK)container.getPk());
       }
       if(name.equals("PropertyExpense"))
       {
           PropertyExpense ret = new PropertyExpense(srk,dcm);
           return ret.create((PropertyPK)container.getPk());
       }
       if(name.equals("SourceOfBusinessProfile"))
       {
           SourceOfBusinessProfile ret = new SourceOfBusinessProfile(srk);
           return ret.create(ret.createPrimaryKey());
       }
       //--DJ_LDI_CR--start--//
       if(name.equals("LifeDisabilityPremiums"))
       {
           LifeDisabilityPremiums ret = new LifeDisabilityPremiums(srk,dcm);
           return ret.create((BorrowerPK)container.getPk(), 1);
       }
       //--DJ_LDI_CR--end--//
       //--DJ_CR136--start--//
       if(name.equals("InsureOnlyApplicant"))
       {
           InsureOnlyApplicant ret = new InsureOnlyApplicant(srk,dcm);
           return ret.create((DealPK)container.getPk());
       }
       if(name.equals("LifeDisPremiumsIOnlyA"))
       {
           LifeDisPremiumsIOnlyA ret = new LifeDisPremiumsIOnlyA(srk,dcm);
           return ret.create((InsureOnlyApplicantPK)container.getPk(), 1);
       }
       //--DJ_CR136--end--//

       //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
       if(name.equals("BorrowerIdentification"))
       {
    	   BorrowerIdentification ret = new BorrowerIdentification(srk,dcm);
           return ret.create((BorrowerPK)container.getPk());
       }
       //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//
       

       //  ***** Change by MCM Impl. Team - Version 1.4 - XS_1.1 -  Start *****//
       if(name.equals("QualifyDetail"))
       {   
           QualifyDetail qualifyDet = new QualifyDetail(srk,dcm);  
           DealPK dealPK  = (DealPK)container.getPk();           
           return qualifyDet.create(dealPK.getId(), 0, 0);
       }
       //  ***** Change by MCM Impl. Team - Version 1.4 - XS-1.1  -End *****//
       
       //***** Change by MCM Impl. Team - Version 1.5 - XS-1.2  -Start *****//
       if(name.equals("ComponentLOC")){
    	 ComponentLOC componentLoc = new ComponentLOC(srk,dcm);
    	 ComponentPK componentPk = (ComponentPK)container.getPk();
    	 return componentLoc.create(componentPk.getId(), componentPk.getCopyId());
       }
       if(name.equals("Component"))
       {
    	 Component component = new Component(srk,dcm);
    	 DealPK dealPK = (DealPK)container.getPk();
    	 return component.create(dealPK.getId(),dealPK.getCopyId(),0,0);
       }
       if(name.equals("ComponentMortgage")){
    	 ComponentMortgage componentMtg = new ComponentMortgage(srk,dcm);
    	 ComponentPK componentPk = (ComponentPK)container.getPk();
    	 return componentMtg.create(componentPk.getId(), componentPk.getCopyId());
       }
       if(name.equals("ComponentCreditCard")){
    	 ComponentCreditCard componentCreditCard = new ComponentCreditCard(srk,dcm);
    	 ComponentPK componentPk = (ComponentPK)container.getPk();
    	 return componentCreditCard.create(componentPk.getId(), componentPk.getCopyId());
       }
       if(name.equals("ComponentOverdraft")){
    	 ComponentOverdraft componentOverdft = new ComponentOverdraft(srk,dcm);
    	 ComponentPK componentPk = (ComponentPK)container.getPk();
    	 return componentOverdft.create(componentPk.getId(), componentPk.getCopyId());
       }
       if(name.equals("ComponentLoan")){
    	 ComponentLoan componentLoan = new ComponentLoan(srk,dcm);
    	 ComponentPK componentPk = (ComponentPK)container.getPk();
    	 return componentLoan.create(componentPk.getId(), componentPk.getCopyId());
       }
       //***** Change by MCM Impl. Team - Version 1.5 - XS-1.2  -End *****//
   return null;
  }
  
	public static DealEntity createBlankEntity(
			Class<? extends DealEntity> clazz) throws RemoteException, FinderException {
		return createBlankEntity(clazz, null, null);
	}

	public static DealEntity createBlankEntity(
			Class<? extends DealEntity> clazz, SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException {

		if(clazz == Addr.class) return new Addr(srk);
		if(clazz == Asset.class) return new Asset(srk, dcm);
		if(clazz == Borrower.class) return new Borrower(srk,dcm);
		if(clazz == BorrowerAddress.class) return new BorrowerAddress(srk,dcm);
		if(clazz == BranchProfile.class) return new BranchProfile(srk);
		if(clazz == Condition.class) return new Condition(srk);
		if(clazz == Contact.class) return new Contact(srk);
		if(clazz == CreditReference.class) return new CreditReference(srk,dcm);
		if(clazz == Deal.class) return new Deal(srk,dcm);
		if(clazz == DealFee.class) return new DealFee(srk,dcm);
		if(clazz == DealNotes.class) return new DealNotes(srk);
		if(clazz == Fee.class) return new Fee(srk,dcm);
		if(clazz == DownPaymentSource.class) return new DownPaymentSource(srk,dcm);
		if(clazz == DocumentTracking.class) return new DocumentTracking(srk);
		if(clazz == EscrowPayment.class) return new EscrowPayment(srk,dcm);
		if(clazz == EmploymentHistory.class) return new EmploymentHistory(srk,dcm);
		if(clazz == GroupProfile.class) return new GroupProfile(srk);
		if(clazz == Income.class) return new Income(srk,dcm);
		if(clazz == InvestorProfile.class) return new InvestorProfile(srk);
		if(clazz == Liability.class) return new Liability(srk,dcm);
		if(clazz == LenderProfile.class) return new LenderProfile(srk);
		if(clazz == MtgProd.class) return new MtgProd(srk,dcm);
		if(clazz == PricingProfile.class) return new PricingProfile(srk);
		if(clazz == Property.class) return new Property(srk,dcm);
		if(clazz == PropertyExpense.class) return new PropertyExpense(srk,dcm);
		if(clazz == SourceFirmProfile.class) return new SourceFirmProfile(srk);
		if(clazz == SourceOfBusinessProfile.class) return new SourceOfBusinessProfile(srk);
		if(clazz == Bridge.class) return new Bridge(srk,dcm);
		if(clazz == LifeDisabilityPremiums.class) return new LifeDisabilityPremiums(srk,dcm);
		if(clazz == InsureOnlyApplicant.class) return new InsureOnlyApplicant(srk,dcm);
		if(clazz == LifeDisPremiumsIOnlyA.class) return new LifeDisPremiumsIOnlyA(srk,dcm);
		if(clazz == PrimeIndexRateProfile.class) return new PrimeIndexRateProfile(srk);
		if(clazz == UserProfile.class) return new UserProfile(srk);
		if(clazz == PartyProfile.class) return new PartyProfile(srk);
		if(clazz == PricingRateInventory.class) return new PricingRateInventory(srk);
		if(clazz == BorrowerIdentification.class) return new BorrowerIdentification(srk,dcm);
		if(clazz == ComponentLOC.class) return new ComponentLOC(srk,dcm);
		if(clazz == Component.class) return new Component(srk,dcm);
		if(clazz == ComponentMortgage.class) return new ComponentMortgage(srk,dcm);
		if(clazz == ComponentCreditCard.class) return new ComponentCreditCard(srk,dcm);
		if(clazz == ComponentOverdraft.class) return new ComponentOverdraft(srk,dcm);
		if(clazz == ComponentLoan.class) return new ComponentLoan(srk,dcm);
		if(clazz == QualifyDetail.class) return new QualifyDetail(srk,dcm);
		if(clazz == AppraisalOrder.class) return new AppraisalOrder(srk,dcm);
		if(clazz == AssetType.class) return new AssetType(srk);
		if(clazz == Channel.class) return new Channel(srk);
		if(clazz == ComponentSummary.class) return new ComponentSummary(srk,dcm);
		if(clazz == CreditBureauReport.class) return new CreditBureauReport(srk);
		if(clazz == DealEventStatusSnapshot.class) return new DealEventStatusSnapshot(srk);
		if(clazz == IncomeType.class) return new IncomeType(srk);
		if(clazz == InstitutionProfile.class) return new InstitutionProfile(srk);
		if(clazz == LiabilityType.class) return new LiabilityType(srk);
		if(clazz == MasterDeal.class) return new MasterDeal(srk,dcm);
		if(clazz == PropertyExpenseType.class) return new PropertyExpenseType(srk);
		if(clazz == Province.class) return new Province(srk);
		if(clazz == RegionProfile.class) return new RegionProfile(srk);
		if(clazz == Request.class) return new Request(srk);
		if(clazz == SystemType.class) return new SystemType(srk);
		if(clazz == TimeZoneEntry.class) return new TimeZoneEntry(srk);

		throw new ExpressRuntimeException(
				"Express doesn't know how to instanciate "
						+ clazz.getSimpleName() + ". please add code to "
						+ EntityBuilder.class.getName() + "#createBlankEntity");
	}

}



