package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.IngestionQueuePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class IngestionQueue extends DealEntity {

	public static final String TABLE_NAME = "ingestionqueue";
	public static final String COLUMN_NAME_INGESTIONQUEUEID = "ingestionQueueId";
	public static final String COLUMN_NAME_SOURCEAPPLICATIONID = "sourceApplicationId";
	public static final String COLUMN_NAME_ECNILENDERID = "ecniLenderId";
	public static final String COLUMN_NAME_SENDERCHANNEL = "senderChannel";
	public static final String COLUMN_NAME_RECEIVERCHANNEL = "receiverchannel";
	public static final String COLUMN_NAME_RECEIVEDTIMESTAMP = "receivedTimestamp";
	public static final String COLUMN_NAME_INGESTIONQUEUESTATUSID = "ingestionQueueStatusId";
	public static final String COLUMN_NAME_STATUSDATETIMESTAMP = "statusDateTimestamp";
	public static final String COLUMN_NAME_RETRYCOUNTER = "retryCounter";
	public static final String COLUMN_NAME_DEALXML = "dealXml";
	public static final String SQL_SELECT_SEQUEUENCE_NEXT = "SELECT ingestionqueuesequence.NEXTVAL FROM DUAL";
	public static final String SQL_INSERT_INGESTIONQUEUE = "INSERT INTO ingestionqueue VALUES (?,?,?,?,?,?,?,?,?,?)";
	public static final String SQL_UPDATE_INGESTIONQUEUE_DEALXML = "UPDATE ingestionqueue SET dealXml=? WHERE ingestionQueueId=?";

	public static final int STATUS_SUCCESS = -1;
	public static final int STATUS_READY_FOR_PROCESSING = 0;
	public static final int STATUS_IN_PROGRESS = 1;
	public static final int STATUS_DEAL_LOCKED = 2;
	public static final int STATUS_ERROR = 3;

	private static final long serialVersionUID = 1262499253029229274L;

	private IngestionQueuePK ingestionQueuePK;
	private int ingestionQueueId;
	private String sourceApplicationId;
	private String ecniLenderId;
	private String senderChannel;
	private String receiverChannel;
	private Date receivedTimestamp;
	private int ingestionQueueStatusId;
	private Date statusDateTimestamp;
	private int retryCounter;
	private String dealXml;

	public IngestionQueue(SessionResourceKit srk) throws RemoteException {
		super(srk);
	}

	public IngestionQueue(SessionResourceKit srk, int ingestionQueueId) throws RemoteException, FinderException {
		super(srk);
		ingestionQueuePK = new IngestionQueuePK(ingestionQueueId);
		findByPrimaryKey(ingestionQueuePK);
	}

	public int getIngestionQueueId() {
		return ingestionQueueId;
	}

	public void setIngestionQueueId(int ingestionQueueId) {
		this.testChange(COLUMN_NAME_INGESTIONQUEUEID, ingestionQueueId);
		this.ingestionQueueId = ingestionQueueId;
	}

	public String getSourceApplicationId() {
		return sourceApplicationId;
	}

	public void setSourceApplicationId(String sourceApplicationId) {
		this.testChange(COLUMN_NAME_SOURCEAPPLICATIONID, sourceApplicationId);
		this.sourceApplicationId = sourceApplicationId;
	}

	public String getEcniLenderId() {
		return ecniLenderId;
	}

	public void setEcniLenderId(String ecniLenderId) {
		this.testChange(COLUMN_NAME_ECNILENDERID, ecniLenderId);
		this.ecniLenderId = ecniLenderId;
	}

	public String getSenderChannel() {
		return senderChannel;
	}

	public void setSenderChannel(String senderChannel) {
		this.testChange(COLUMN_NAME_SENDERCHANNEL, senderChannel);
		this.senderChannel = senderChannel;
	}

	public String getReceiverChannel() {
		return receiverChannel;
	}

	public void setReceiverChannel(String receiverChannel) {
		this.testChange(COLUMN_NAME_RECEIVERCHANNEL, receiverChannel);
		this.receiverChannel = receiverChannel;
	}

	public Date getReceivedTimestamp() {
		return receivedTimestamp;
	}

	public void setReceivedTimestamp(Date receivedTimestamp) {
		this.testChange(COLUMN_NAME_RECEIVEDTIMESTAMP, receivedTimestamp);
		this.receivedTimestamp = receivedTimestamp;
	}

	public int getIngestionQueueStatusId() {
		return ingestionQueueStatusId;
	}

	public void setIngestionQueueStatusId(int ingestionQueueStatusId) {
		this.testChange(COLUMN_NAME_INGESTIONQUEUESTATUSID, ingestionQueueStatusId);
		this.ingestionQueueStatusId = ingestionQueueStatusId;
	}

	public Date getStatusDateTimestamp() {
		return statusDateTimestamp;
	}

	public void setStatusDateTimestamp(Date statusDateTimestamp) {
		this.testChange(COLUMN_NAME_STATUSDATETIMESTAMP, statusDateTimestamp);
		this.statusDateTimestamp = statusDateTimestamp;
	}

	public int getRetryCounter() {
		return retryCounter;
	}

	public void setRetryCounter(int retryCounter) {
		this.testChange(COLUMN_NAME_RETRYCOUNTER, retryCounter);
		this.retryCounter = retryCounter;
	}

	public String getDealXml() {
		return dealXml;
	}

	public void setDealXml(String dealXml) {
		this.testChange(COLUMN_NAME_DEALXML, dealXml);
		this.dealXml = dealXml;
	}

	public String toString() {
		return COLUMN_NAME_INGESTIONQUEUEID + "=" + ingestionQueueId;
	}

	protected int performUpdate() throws Exception {
		int ret = doPerformUpdate(new String[] { COLUMN_NAME_DEALXML });
		forceUpdateDealXml(); //TODO DealEntity should rewrite to handle clob and blob
		return ret;
	}

	private void forceUpdateDealXml() throws SQLException, IOException {
		Connection connection = jExec.getCon();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_UPDATE_INGESTIONQUEUE_DEALXML);
			statement.setClob(1, createClob(dealXml));
			statement.setInt(2, ingestionQueuePK.getId());
			statement.executeUpdate();
		} catch (SQLException sqle) {
			try {
				jExec.rollback();
			} catch (Exception e) { //IllegalStateException, JdbcTransactionException
				logger.error("Rollback failed.");
				logger.error(e);
			}
			throw sqle;
		} catch (IOException ioe) {
			try {
				jExec.rollback();
			} catch (Exception e) { //IllegalStateException, JdbcTransactionException
				logger.error("Rollback failed.");
				logger.error(e);
			}
			throw ioe;
		} finally {
			try {
				statement.close();
			} catch (SQLException sqle) {
				logger.warn(sqle.getMessage());
			}
		}
	}

	private void setPropertiesFromQueryResult(int key) throws Exception {
		this.setIngestionQueueId(jExec.getInt(key, COLUMN_NAME_INGESTIONQUEUEID));
		this.setSourceApplicationId(jExec.getString(key, COLUMN_NAME_SOURCEAPPLICATIONID));
		this.setEcniLenderId(jExec.getString(key, COLUMN_NAME_ECNILENDERID));
		this.setSenderChannel(jExec.getString(key, COLUMN_NAME_SENDERCHANNEL));
		this.setReceiverChannel(jExec.getString(key, COLUMN_NAME_RECEIVERCHANNEL));
		this.setReceivedTimestamp(jExec.getDate(key, COLUMN_NAME_RECEIVEDTIMESTAMP));
		this.setIngestionQueueStatusId(jExec.getInt(key, COLUMN_NAME_INGESTIONQUEUESTATUSID));
		this.setStatusDateTimestamp(jExec.getDate(key, COLUMN_NAME_STATUSDATETIMESTAMP));
		this.setRetryCounter(jExec.getInt(key, COLUMN_NAME_RETRYCOUNTER));
		this.setDealXml(readClob(jExec.getClob(key, COLUMN_NAME_DEALXML)));
	}

	public IEntityBeanPK getPk() {
		return ingestionQueuePK;
	}

	public String getEntityTableName() {
		return TABLE_NAME;
	}

	public Collection<IngestionQueue> findByStatus(int ingestionQueueStatusId) throws FinderException {
		Collection<IngestionQueue> queues = new ArrayList<IngestionQueue>();
		String sql = "SELECT * FROM ingestionqueue WHERE ingestionqueuestatusid=" + ingestionQueueStatusId + " order by statusdatetimestamp";
		try {
			int key = jExec.execute(sql);
			while (jExec.next(key)) {
				IngestionQueue queue = new IngestionQueue(srk);
				queue.setPropertiesFromQueryResult(key);
				queue.ingestionQueuePK = new IngestionQueuePK(queue.ingestionQueueId);
				queues.add(queue);
			}
			jExec.closeData(key);
		} catch (Exception e) {
			FinderException fe = new FinderException("Exception caught while getting IngestionQueue");
			logger.error(fe.getMessage());
			logger.error(e);
			throw fe;
		}
		return queues;
	}

	public IngestionQueue findByPrimaryKey(IngestionQueuePK ingestionQueuePK) throws FinderException {
		String sql = "SELECT * FROM ingestionqueue " + ingestionQueuePK.getWhereClause();
		//TODO: inappropriate design JdbcExecutor:
		//methods throwing Exception? should unhide SQLException; FinderException (user exception) should be separated from SQLException
		//no finally block to handle close connection?
		//redudant exception
		boolean gotRecord = false;
		int key = 0;
		try {
			key = jExec.execute(sql);
			gotRecord = jExec.next(key);
			if (gotRecord)
				setPropertiesFromQueryResult(key);
			jExec.closeData(key);
			if (!gotRecord) {
				String msg = "IngestionQueue Entity: @findByPrimaryKey(), key= " + ingestionQueuePK + ", entity not found";
				throw new FinderException(msg);
			}
		} catch (Exception e) {
			if (gotRecord == false)
				throw (FinderException) e;
			FinderException fe = new FinderException("Errors while executing: " + sql);
			throw fe;
		} finally {
			if (key != 0)
				try {
					jExec.closeData(key);
				} catch (Exception ignore) {
					logger.warn(ignore.getMessage());
				}
		}
		this.ingestionQueuePK = ingestionQueuePK;
		return this;
	}

	public IngestionQueue findNextAvailable(int lockedDealRetryWaitInSeconds) throws FinderException {
		String sql = "SELECT * FROM (SELECT * FROM ingestionqueue WHERE ingestionqueuestatusid=0 OR (ingestionqueuestatusid=2 AND (SYSDATE-statusdatetimestamp)>(" + lockedDealRetryWaitInSeconds + "/86400)) ORDER BY receivedtimestamp) WHERE ROWNUM=1";
		boolean gotRecord = false;
		int key = 0;
		try {
			key = jExec.execute(sql);
			gotRecord = jExec.next(key);
			if (gotRecord)
				setPropertiesFromQueryResult(key);
			this.ingestionQueuePK = new IngestionQueuePK(this.ingestionQueueId);
			jExec.closeData(key);
			if (!gotRecord)
				return null;
		} catch (Exception e) {
			if (gotRecord == false)
				throw (FinderException) e;
			FinderException fe = new FinderException("Errors while executing: " + sql);
			throw fe;
		} finally {
			if (key != 0)
				try {
					jExec.closeData(key);
				} catch (Exception ignore) {
					logger.warn(ignore.getMessage());
				}
		}
		return this;
	}

	public Collection<IngestionQueue> findBySourceApplicationId(String sourceApplicationId, int ingestionQueueStatusId, Date prior) throws FinderException {
		Collection<IngestionQueue> queues = new ArrayList<IngestionQueue>();
		String sql = "SELECT * FROM ingestionqueue WHERE sourceApplicationId='" + sourceApplicationId + "' AND ingestionQueueStatusId=" + ingestionQueueStatusId + " AND receivedTimestamp<" + sqlStringFrom(prior);
		try {
			int key = jExec.execute(sql);
			while (jExec.next(key)) {
				IngestionQueue queue = new IngestionQueue(srk);
				queue.setPropertiesFromQueryResult(key);
				queue.ingestionQueuePK = new IngestionQueuePK(queue.ingestionQueueId);
				queues.add(queue);
			}
			jExec.closeData(key);
		} catch (Exception e) {
			FinderException fe = new FinderException("Exception caught while getting IngestionQueue");
			logger.error(fe.getMessage());
			logger.error(e);
			throw fe;
		}
		return queues;
	}

	protected IngestionQueuePK createPrimaryKey() throws CreateException {
		String sql = "SELECT ingestionqueuesequence.NEXTVAL FROM DUAL";
		int id = -1;
		int key = 0;
		try {
			key = jExec.execute(sql);
			jExec.next(key);
			id = jExec.getInt(key, 1);
			if (id == -1)
				throw new Exception();
		} catch (Exception e) {
			CreateException ce = new CreateException("ingestionqueuesequence Entity create() exception getting ingestionqueuesequenceid from sequence");
			logger.error(ce.getMessage());
			logger.error(e);
			throw ce;
		} finally {
			if (key != 0)
				try {
					jExec.closeData(key);
				} catch (Exception ignore) {
					logger.warn(ignore.getMessage());
				}
		}
		return new IngestionQueuePK(id);
	}

	public IngestionQueue create(String sourceApplicationId, String ecniLenderId, String senderChannel, String receiverChannel, Date receivedTimestamp, String dealXml) throws CreateException {
		return create(sourceApplicationId, ecniLenderId, senderChannel, receiverChannel, receivedTimestamp, STATUS_READY_FOR_PROCESSING, new Date(), 0, dealXml);
	}

	//TODO: It is always a bad design to save huge data like Clob or Blob in memory like field dealxml.
	//Since Express never consider this, field dealxml will have the same implementation as others. 
	public IngestionQueue create(String sourceApplicationId, String ecniLenderId, String senderChannel, String receiverChannel, Date receivedTimestamp, int ingestionQueueStatusId, Date statusDateTimestamp, int retryCounter, String dealXml) throws CreateException {
		//TODO: This is totally waste of resources, two trips to db? This design can be found thruout Express.
		IngestionQueuePK ingestionQueuePK = createPrimaryKey();

		//Since Express is using an in-house connection pool/management which behaviour very differently
		//DO NOT close the connection to return to pool, just leave as is.
		Connection connection = jExec.getCon();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT_INGESTIONQUEUE);
			statement.setInt(1, ingestionQueuePK.getId());
			statement.setString(2, sourceApplicationId);
			statement.setString(3, ecniLenderId);
			statement.setString(4, senderChannel);
			statement.setString(5, receiverChannel);
			statement.setTimestamp(6, new java.sql.Timestamp(receivedTimestamp.getTime()));
			statement.setInt(7, ingestionQueueStatusId);
			statement.setTimestamp(8, new java.sql.Timestamp(statusDateTimestamp.getTime()));
			statement.setInt(9, retryCounter);
			statement.setClob(10, createClob(dealXml));
			statement.executeUpdate();
			logger.info(SQL_INSERT_INGESTIONQUEUE + ", " + ingestionQueuePK);

			//TODO Another inappropriate design, why make another trip to db?
			return findByPrimaryKey(ingestionQueuePK);
		} catch (Exception e) { //IOException and SQLException
			//Instead of connection roll back, do in-house rollback
			try {
				jExec.rollback();
			} catch (Exception ee) { //IllegalStateException, JdbcTransactionException
				logger.error("Rollback failed.");
				logger.error(ee);
			}
			throw new CreateException(e);
		} finally {
			try {
				statement.close();
			} catch (SQLException sqle) {
				throw new CreateException(sqle);
			}
		}
	}

}
