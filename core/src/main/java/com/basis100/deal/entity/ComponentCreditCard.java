/*
 * @(#)ComponentCreditCard.java Apr 29, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ComponentCreditCardPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: ComponentCreditCard
 * </p>
 * <p>
 * Description: This is an entity class representing ComponentCreditCard. This
 * class provides accessor and mutator methods for all the entity attributes.
 * This class will map the entity and database fields. This class will save the
 * appropriate data into the ComponentCreditCard Table.
 * </p>
 * @version 1.0 Initial Version
 * @version 1.1 15-Jul-2008 Added constructor's with dcm as one of the paramter,
 *          modified create(..), findByPrimaryKey(..) method's and removed
 *          createPrimaryKey() method.
 */
public class ComponentCreditCard extends DealEntity
{
    protected int componentId;

    protected int copyId;

    protected int institutionProfileId;

    protected double creditCardAmount;

    protected ComponentCreditCardPK pk;

    /**
     * @param srk
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentCreditCard(SessionResourceKit srk) throws RemoteException,
            FinderException
    {
        super(srk);
    }

    /**
     * @param srk
     * @param id
     * @param copyId
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentCreditCard(SessionResourceKit srk, int id, int copyId)
            throws RemoteException, FinderException
    {
        super(srk);

        pk = new ComponentCreditCardPK(id, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * <p>
     * Description: Creates the ComponentCreditCard entity object by taking
     * session resource kit and calc monitor objects and calls the super class
     * constructor by passing the session resource kit and calc monitor objects.
     * </p>
     * @version 1.0 15-Jul-2008 Initial Version
     * @param srk
     *            stores the session level information.
     * @param dcm
     *            runs the calculation of this ComponentCreditCard entity.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentCreditCard(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException
    {
        super(srk, dcm);
    }

    /**
     * <p>
     * Description: Creates the ComponentCreditCard entity object by taking
     * session resource kit and calc monitor objects, component id and the copy
     * id of the existing Component.Calls the super class constructor and
     * creates the primary key for the ComponentCreditCard entity and calls the
     * findByPrimaryKey() method.
     * </p>
     * @version 1.0 15-Jul-2008 Initial Version
     * @param srk
     *            stores the session level information.
     * @param componentId
     *            component id value passed to the entity PK class.
     * @param dcm
     *            runs the calculation of this ComponentCreditCard entity.
     * @param copyId
     *            copy id of the Component.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     * @see com.basis100.deal.entity.ComponentCreditCard#findByPrimaryKey(com.basis100.deal.pk.ComponentCreditCardPK )
     */
    public ComponentCreditCard(SessionResourceKit srk, CalcMonitor dcm,
            int componentId, int copyId) throws RemoteException,
            FinderException
    {
        super(srk, dcm);

        pk = new ComponentCreditCardPK(componentId, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * @param key
     * @throws Exception
     */
    private void setPropertiesFromQueryResult(int key) throws Exception
    {
        this.setComponentId(jExec.getInt(key, "COMPONENTID"));
        this.setCopyId(jExec.getInt(key, "COPYID"));
        this.setCreditCardAmount(jExec.getDouble(key, "CREDITCARDAMOUNT"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    /**
     * <p>
     * Description: Retrieves a row from the database using the primary key
     * passed in.
     * </p>
     * @param pk
     * @return
     * @throws RemoteException
     * @throws FinderException
     * @version 1.0 Initial version
     * @version 1.1 15-Jul-2008 Added this.copyId = pk.getCopyId();
     */
    public ComponentCreditCard findByPrimaryKey(ComponentCreditCardPK pk)
            throws RemoteException, FinderException
    {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from ComponentCreditCard where componentId = "
                + pk.getId() + " AND copyId = " + pk.getCopyId();

        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "ComponentCreditCard Entity: @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                if(getSilentMode() == false)
                    logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException(
                    "ComponentCreditCard Entity - findByPrimaryKey() exception");

            if(getSilentMode() == false){
                logger.error(fe.getMessage());
                logger.error("finder sql: " + sql);
                logger.error(e);
            }
            throw fe;
        }
        // V1.1
        this.copyId = pk.getCopyId();
        // V1.1
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * @param componentId
     * @param copyId
     * @return
     * @throws RemoteException
     * @throws CreateException
     * @version 1.0 Initial version
     * @version 1.1 15-Jul-2008 Added check for dcm and added a check for
     *          inputCreatedEntity(..)
     */
    public ComponentCreditCard create(int componentId, int copyId)
            throws RemoteException, CreateException
    {

        pk = new ComponentCreditCardPK(componentId, copyId);
        // SQL
        String sql = "Insert into ComponentCreditCard( componentId, copyId, institutionProfileId ) Values ( "
                + componentId
                + ","
                + copyId
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        // Execute
        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the create
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "ComponentCreditCard Entity - Component - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        // V1.1
        if (dcm != null)
            dcm.inputCreatedEntity(this);
        // V1.1
        return this;
    }

    /**
     * @return the componentId
     */
    public int getComponentId()
    {
        return componentId;
    }

    /**
     * @param componentId
     *            the componentId to set
     */
    public void setComponentId(int componentId)
    {
        this.testChange("componentId", componentId);
        this.componentId = componentId;
    }

    /**
     * @return the copyId
     */
    public int getCopyId()
    {
        return copyId;
    }

    /**
     * @param copyId
     *            the copyId to set
     */
    public void setCopyId(int copyId)
    {
        this.testChange("copyId", copyId);
        this.copyId = copyId;
    }

    /**
     * @return the institutionProfileId
     */
    public int getInstitutionProfileId()
    {
        return institutionProfileId;
    }

    /**
     * @param institutionProfileId
     *            the institutionProfileId to set
     */
    public void setInstitutionProfileId(int institutionProfileId)
    {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    /**
     * @return the creditCardAmount
     */
    public double getCreditCardAmount()
    {
        return creditCardAmount;
    }

    /**
     * @param creditCardAmount
     *            the creditCardAmount to set
     */
    public void setCreditCardAmount(double creditCardAmount)
    {
        this.testChange("creditCardAmount", creditCardAmount);
        this.creditCardAmount = creditCardAmount;
    }

    /**
     * <p>
     * Description: Performs updates on the ComponentLOC entity
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return int returns the no of rows updated.
     */
    protected int performUpdate() throws Exception
    {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0)
        {
            dcm.inputEntity(this);
        }

        return ret;
    }

    /**
     * getS the pk associated with this entity
     * @return a DealPK
     */
    public IEntityBeanPK getPk()
    {
        return (IEntityBeanPK) this.pk;
    }

    /**
     * <p>
     * Description: Returns the ComponentLOC entity table name
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return String - table name of the entity
     */
    public String getEntityTableName()
    {
        return "ComponentCreditCard";
    }

    /**
     * <p>
     * isAuditable
     * </p>
     * <p>
     * Descriptio: gets if this entity is auditable or not
     * @version 1.0 XS_2.30
     * @return true
     */
    public boolean isAuditable()
    {

        return true;
    }

    /**
     * <p>
     * Description: Returns the class id of the ComponentCreditCard entity
     * </p>
     * @version 1.0 XS_11.5 10-Jul-2008 Initial Version
     * @return int - classid of ComponentCreditCard entity.
     */
    public int getClassId()
    {
        return ClassId.COMPONENTCREDITCARD;
    }

    /**
     * returns EntityContext. 
     * @since 4.0 MCM, Oct 28, 2008, FXP23168, Hiro
     */
    protected EntityContext getEntityContext() throws RemoteException {

        EntityContext ctx = this.entityContext;
        try {
            Component comp = new Component(srk, componentId, copyId);
            ctx.setContextText(this.getEntityTableName());
            int langId = srk.getLanguageId();
            int instId = srk.getExpressState().getDealInstitutionId();
            String description = BXResources.getPickListDescription(instId,
                    "COMPONENTTYPE", comp.getComponentTypeId(), langId);
            ctx.setContextSource(description);
            ctx.setApplicationId(comp.getDealId());

        } catch (FinderException e) {
            logger.error(getClass().getSimpleName() + ".getEntityContext " +
                    "FinderException caught. ComponentId= " + componentId + " CopyId=" + copyId);
        }
        return ctx;
    } 
}
