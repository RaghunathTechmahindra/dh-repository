package com.basis100.deal.entity;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DCConfigPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>Title: Doc Central configuration entity </p>
 * <p>Description: Doc Central configuration entity </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: filogix</p>
 * @author Catherine Rutgaizer
 * @version 1.0
 */

public class DCConfig extends DealEntity{
  protected int dcConfigId;
  protected int dcFolderCreationId;
  protected int dcLenderId;
  protected int dcFolderConfigId;
  protected String dcRequestingParty;
  protected String dcReceivingParty;
  protected String dcAccountIdentifier;
  protected String dcAccountPassword;
  protected int institutionId;

  private DCConfigPK pk;

  public DCConfig(SessionResourceKit srk) throws RemoteException {
    super(srk);
  }

  public DCConfig(SessionResourceKit srk, int id) throws RemoteException,
      FinderException, RemoteException {
    super(srk);

    pk = new DCConfigPK(id);
    findByPrimaryKey(pk);
  }

  public DCConfig findByPrimaryKey(DCConfigPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM DCCONFIG " + pk.getWhereClause();
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break;                                                  // can only be one record
        }

        jExec.closeData(key);
        if (gotRecord == false)
        {
          String msg = "DCConfig: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("DCConfig Entity - findByPrimaryKey() exception");
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);
          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }
  
  public DCConfig findOne() throws RemoteException, FinderException
  {
    String sql = "SELECT * FROM DCCONFIG ";
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
            break;                                                  // can only be one record
        }

        jExec.closeData(key);
        if (gotRecord == false)
        {
          String msg = "DCConfig: @findOne), entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("DCConfig Entity - findByPrimaryKey() exception");
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);
          throw fe;
      }
      this.pk = new DCConfigPK(this.getDcConfigId());
      return this;
  }
  
  private void setPropertiesFromQueryResult(int key) throws Exception
  {
       setDcConfigId(jExec.getInt(key, "DCCONFIGID"));
       setDcFolderCreationId(jExec.getInt(key, "DCFOLDERCREATIONID"));
       setDcLenderId(jExec.getInt(key, "DCLENDERID"));
       setDcFolderConfigId(jExec.getInt(key, "DCFOLDERCOFIGID"));
       setDcRequestingParty(jExec.getString(key, "DCREQUESTINGPARTY"));
       setDcReceivingParty(jExec.getString(key, "DCRECEIVINGPARTY"));
       setDcAccountIdentifier(jExec.getString(key, "DCACCOUNTIDENTIFIER"));
       setDcAccountPassword(jExec.getString(key, "DCACCOUNTPASSWORD"));
  }

  public String getEntityTableName(){
    return "DCConfig";
  }

  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }

  public int getClassId(){
    return ClassId.DEFAULT;
  }

  public String getStringValue(String fieldName)
  {
      return doGetStringValue(this, this.getClass(), fieldName);
  }

  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }

  protected int performUpdate()  throws Exception
  {
  Class clazz = this.getClass();
  int ret = doPerformUpdate(this, clazz);

  return ret;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof DCConfig )
      {
        DCConfig oa = ( DCConfig ) o;
        if  (  this.dcConfigId == oa.getDcConfigId () )
            return true;
      }
    return false;
  }

// ====================== properties setters ======================
  public int getDcConfigId () {
    return this.dcConfigId;
  }

  public int getDcFolderCreationId () {
    return this.dcFolderCreationId;
  }

  public String getDcReceivingParty () {
    return this.dcReceivingParty;
  }

  public int getDcFolderConfigId () {
    return this.dcFolderConfigId;
  }

  public int getDcLenderId () {
    return this.dcLenderId;
  }

  public String getDcRequestingParty () {
    return this.dcRequestingParty;
  }

  public String getDcAccountIdentifier () {
    return dcAccountIdentifier;
  }
  public String getDcAccountPassword () {
    return this.dcAccountPassword;
  }

// ====================== getters ======================
  public void setDcConfigId (int dcConfigId) {
    this.testChange("dcConfigId", dcConfigId);
    this.dcConfigId = dcConfigId;
  }

  public void setDcFolderCreationId (int dcFolderCreationId) {
    this.testChange("dcFolderCreationId", dcFolderCreationId);
    this.dcFolderCreationId = dcFolderCreationId;
  }

  public void setDcLenderId (int dcLenderId) {
    this.testChange("dcLenderId", dcLenderId);
    this.dcLenderId = dcLenderId;
  }

  public void setDcFolderConfigId (int dcFolderConfigId) {
    this.testChange("dcFolderConfigId", dcFolderConfigId);
    this.dcFolderConfigId = dcFolderConfigId;
  }

  public void setDcRequestingParty (String dcRequestingParty) {
    this.testChange("dcRequestingParty", dcRequestingParty);
    this.dcRequestingParty = dcRequestingParty;
  }

  public void setDcReceivingParty (String dcReceivingParty) {
    this.testChange("dcReceivingParty", dcReceivingParty);
    this.dcReceivingParty = dcReceivingParty;
  }

  public void setDcAccountIdentifier (String dcAccountIdentifier) {
    this.testChange("dcAccountIdentifier", dcAccountIdentifier);
    this.dcAccountIdentifier = dcAccountIdentifier;
  }

  public void setDcAccountPassword (String dcAccountPassword) {
    this.testChange("dcConfigId", dcConfigId);
    this.dcAccountPassword = dcAccountPassword;
  }

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }
    
    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
        this.testChange("institutionProfileId", institutionId);
        this.institutionId = institutionId;
    }

}
