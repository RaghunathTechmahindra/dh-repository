package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.IncomePK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;

public class Income extends DealEntity
{

   protected int      incomePeriodId;
   protected int      incomeTypeId;
   protected int      incomeId;
   protected double   incomeAmount;
   protected int      borrowerId;
   protected boolean  incIncludeInGDS;
   protected boolean  incIncludeInTDS;
   protected int      incPercentInGDS;
   protected int      incPercentInTDS;
   protected int      incPercentOutGDS;
   protected int      incPercentOutTDS;
   protected String   incomeDescription;
   protected int      copyId;
   protected double   annualIncomeAmount;
   protected double   monthlyIncomeAmount;
   protected int      instProfileId;

   private IncomePK pk;

   public Income(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
   {
      super(srk,dcm);
   }

   public Income(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
   {
      super(srk,dcm);

      pk = new IncomePK(id, copyId);

      findByPrimaryKey(pk);
   }



   public Income findByPrimaryKey(IncomePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from Income " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              copyId = pk.getCopyId();
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key: " + pk.getName()+ "=" + pk.getId() +
                         "; CopyId=" + pk.getCopyId() +   ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
   }

   /**
    *  Find income records not associated with an EmploymentHistory record for
    *  the given Borrower.
    */
   public Collection findByOtherIncome(BorrowerPK bpk) throws Exception
   {
      Collection incomes = new ArrayList();

      StringBuffer buf = new StringBuffer("select * from income where borrowerid = ");
      buf.append(bpk.getId()).append(" and copyId = ").append(bpk.getCopyId());
      buf.append(" and incomeId not in( ");
      buf.append("select incomeId from employmenthistory where borrowerId = ");
      buf.append(bpk.getId()).append(" and copyId = ");
      buf.append(bpk.getCopyId()).append(")");

      try
      {
          int key = jExec.execute(buf.toString());

          for (; jExec.next(key); )
          {
            Income in = new Income(srk,dcm);
            in.setPropertiesFromQueryResult(key);
            in.pk = new IncomePK(in.getIncomeId(),in.getCopyId());
            incomes.add(in);
          }

          jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Page Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + buf.toString());
          logger.error(e);

          throw fe;
        }

    return incomes;
   }
   public Collection<Income> findByBorrower(BorrowerPK pk) throws Exception
   {
      Collection<Income> incomes = new ArrayList<Income>();

      String sql = "Select * from Income " ;
            sql +=  pk.getWhereClause();

       try
       {

          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            Income in = new Income(srk,dcm);
            in.setPropertiesFromQueryResult(key);
            in.pk = new IncomePK(in.getIncomeId(),in.getCopyId());
            incomes.add(in);
          }

          jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while finding Borrower::Incomes");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return incomes;
   }

  /**
  *  Find income records not associated with an EmploymentHistory record for
  *  the given Borrower.
  */
   public Collection findByDeal(DealPK pk) throws Exception
   {
      Collection incs = new ArrayList();

      StringBuffer buf = new StringBuffer("Select * from Income ");
      buf.append(" where borrowerId in( Select BorrowerId from Borrower where dealId = ");
      buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
      buf.append(") and copyId = ").append(pk.getCopyId());

      try
      {
          int key = jExec.execute(buf.toString());

          for (; jExec.next(key); )
          {
            Income in = new Income(srk,dcm);
            in.setPropertiesFromQueryResult(key);
            in.pk = new IncomePK(in.getIncomeId(),in.getCopyId());
            incs.add(in);
          }

          jExec.closeData(key);

      }
      catch (Exception e)
      {
        logger.error(e.getMessage());
        logger.error("Error sql: " + buf.toString());
        logger.error(e);

        throw e;
      }

    return incs;
   }

   public Vector findByMyCopies() throws RemoteException, FinderException
   {
      Vector v = new Vector();

      String sql = "Select * from Income where incomeId = " + getIncomeId() + " AND copyId <> " + getCopyId();

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              Income iCpy = new Income(srk,dcm);

              iCpy.setPropertiesFromQueryResult(key);
              iCpy.pk = new IncomePK(iCpy.getIncomeId(), iCpy.getCopyId());

              v.addElement(iCpy);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Income - findByMyCopies exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        return v;
   }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     this.setIncomePeriodId(jExec.getInt(key,"INCOMEPERIODID"));
     this.setIncomeTypeId(jExec.getInt(key,"INCOMETYPEID"));
     this.setIncomeId(jExec.getInt(key,"INCOMEID"));
     this.setIncomeAmount(jExec.getDouble(key,"INCOMEAMOUNT"));
     this.setBorrowerId(jExec.getInt(key,"BORROWERID"));
     this.setIncIncludeInGDS(jExec.getString(key,"INCINCLUDEINGDS"));
     this.setIncIncludeInTDS(jExec.getString(key,"INCINCLUDEINTDS"));

     this.setIncPercentInGDS(jExec.getInt(key,"INCPERCENTINGDS"));
     this.setIncPercentInTDS(jExec.getInt(key,"INCPERCENTINTDS"));

     this.setIncPercentOutGDS(jExec.getInt(key,"INCPERCENTOUTGDS"));
     this.setIncPercentOutTDS(jExec.getInt(key,"INCPERCENTOUTTDS"));

     this.setIncomeDescription(jExec.getString(key,"INCOMEDESCRIPTION"));
     copyId = jExec.getInt(key, "COPYID");
     setAnnualIncomeAmount( jExec.getDouble(key,"ANNUALINCOMEAMOUNT"));
     setMonthlyIncomeAmount( jExec.getDouble(key, "MONTHLYINCOMEAMOUNT")) ;
     setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }




  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)        //!!!!
  {
    return doGetStringValue(this, this.getClass(), fieldName);
  }


   /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception      //!!!!
   {
      doSetField(this,this.getClass(), fieldName, value);
   }

  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed           //!!!!
   */
  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();
    int ret = doPerformUpdate(this, clazz);

    // Optimization:
    // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;
  }

  public String getEntityTableName()
  {
    return "Income";
  }


  /**
   *   gets the incomePeriodId associated with this entity
   *
   */
  public int getIncomePeriodId()
  {
    return this.incomePeriodId ;
  }




  /**
   *   gets the pk associated with this entity
   *
   *   @return a IncomePK
   */
  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }

  /**
   *   gets the incomeId associated with this entity
   *
   *   @return a int
   */
  public int getIncomeId()
  {
    return this.incomeId ;
  }

  /**
   *   gets the copyId associated with this entity
   *
   *   @return a int
   */
  //!!! change
  public int getCopyId()
  {
    return this.copyId;
  }


  /**
   *   gets the borrowerId associated with this entity
   *
   *   @return a int
   */
  public int getBorrowerId()
  {
    return this.borrowerId ;
  }

  /**
   *   gets the incomeTypeId associated with this entity
   */
  public int getIncomeTypeId()
  {
    return this.incomeTypeId ;
  }

  /**
   *   gets the incomeAmount associated with this entity
   *
   *   @return a double
   */
  public double getIncomeAmount()
  {
    return this.incomeAmount ;
  }
  /**
   *   gets the incomeAmount associated with this entity
   *
   *   @return a double
   */
  public double getAnnualIncomeAmount()
  {
    return this.annualIncomeAmount ;
  }

  public double getMonthlyIncomeAmount()
  {
    return this.monthlyIncomeAmount ;
  }

  public boolean getIncIncludeInGDS()
  {
    return this.incIncludeInGDS ;
  }

  public boolean getIncIncludeInTDS()
  {
    return this.incIncludeInTDS;
  }
  public int getIncPercentInGDS()
  {
    return this.incPercentInGDS;
  }

  public int getIncPercentInTDS()
  {
    return this.incPercentInTDS;
  }

  public int getIncPercentOutGDS()
  {
    return this.incPercentOutGDS;
  }

  public int getIncPercentOutTDS()
  {
    return this.incPercentOutTDS;
  }

  public String getIncomeDescription()
  {
    return this.incomeDescription;
  }


  private IncomePK createPrimaryKey(int copyId)throws CreateException
  {
       String sql = "Select Incomeseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Income Entity create() exception getting IncomeId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }


        return new IncomePK(id, copyId);
  }

   /**
   * creates a Income using the Income primary key and a borrower primary key
   *  - the mininimum (ie.non-null) fields
   *
   */

  public Income create(BorrowerPK bpk)throws RemoteException, CreateException
  {

    pk = createPrimaryKey(bpk.getCopyId());

    // set default value for income type and associated defaults   -- by BILLY 12Jan2001
    // Assume this is only used for Other Incomes --
    //    default to the first option matched with "employmentrelated <> 'Y'"
    int incomeTypeId = 0;
    try{
    IncomeType theFirstEntry = new IncomeType(getSessionResourceKit());
    theFirstEntry = theFirstEntry.findFirstMatched("where employmentrelated <> 'Y' ORDER BY incometypeid  ASC");
    incomeTypeId = theFirstEntry.getIncomeTypeId();
    }
    catch(Exception e)
    {
      logger.error(e);
    }
logger.debug("BILLY -- the first matched incomeTypeId = " +  incomeTypeId);

    // added inst id for ML: -1 is no institutionnized table inst Id
    String percentInGDS = PicklistData.getMatchingColumnValue(-1, "IncomeType", 
                                                              incomeTypeId, "GDSINCLUSION");
    String includeInGDS = ("0".equals(percentInGDS)) ? "'N'" : "'Y'";

    // added inst id for ML: -1 is no institutionnized table inst Id
    String percentInTDS = PicklistData.getMatchingColumnValue(-1, "IncomeType", 
                                                              incomeTypeId, "TDSINCLUSION");
    String includeInTDS = ("0".equals(percentInTDS)) ? "'N'" : "'Y'";

    String sql = "Insert into Income( " +
    "IncomeId, BorrowerId, copyId, incomeperiodid, incomeamount, " +
    "incometypeid, incpercentingds, incincludeingds, incpercentintds, incincludeintds, INSTITUTIONPROFILEID)" +
    " Values ( " +
      pk.getId() + "," + bpk.getId() + "," + pk.getCopyId() + ", 0, 0.0, " +
      incomeTypeId + ", " + percentInGDS + ", " + includeInGDS + ", " + percentInTDS 
      + ", " + includeInTDS + ", " + srk.getExpressState().getDealInstitutionId() + ")";
    // ================== End of changes   -- by BILLY 11Jan2001 =============================

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      this.trackEntityCreate (); // Track the creation
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Income Entity - Income - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    srk.setModified(true);

    if(dcm != null)
          //dcm.inputEntity(this);
          dcm.inputCreatedEntity ( this );

    return this;
 }

    /**
    * @deprecated
   * creates a Income using the Income primary key and a borrower primary key
   *  - the mininimum (ie.non-null) fields
   *
   Note: This method makes no sense since an in income is not compelled to have an employmentHistory
   * more correct is an employmentHistory create requiring and existing income.
   *
   */

  public Income create(EmploymentHistoryPK ehpk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(ehpk.getCopyId());
    EmploymentHistory eh = null;

    try
    {
      eh = new EmploymentHistory(srk,ehpk.getId(),ehpk.getCopyId());
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Income Entity - Income - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    // set default value for income type and associated defaults     -- by BILLY 11Jan2001
    // Assume this for Employment Incomes -- Default to option 0 (Salary)
    int incomeTypeId = 0;

    // added inst id for ML: -1 is no institutionnized table inst Id
    String percentInGDS = PicklistData.getMatchingColumnValue(-1, "IncomeType", 
                                                              incomeTypeId, "GDSINCLUSION");
    String includeInGDS = ("0".equals(percentInGDS)) ? "'N'" : "'Y'";

    // added inst id for ML: -1 is no institutionnized table inst Id
    String percentInTDS = PicklistData.getMatchingColumnValue(-1, "IncomeType", 
                                                              incomeTypeId, "TDSINCLUSION");
    String includeInTDS = ("0".equals(percentInTDS)) ? "'N'" : "'Y'";

    String sql = "Insert into Income( " +
    "IncomeId, BorrowerId, copyId, incomeperiodid, incomeamount, " +
    "incometypeid, incpercentingds, incincludeingds, incpercentintds, incincludeintds, institutionProfileID )" +
    " Values ( " +
      pk.getId() + "," + eh.getBorrowerId() + "," + pk.getCopyId() + ", 0, 0.0, " +
      incomeTypeId + ", " + percentInGDS + ", " + includeInGDS + ", " + percentInTDS 
      + ", " + includeInTDS + ", " + srk.getExpressState().getDealInstitutionId() + 
    ")";
    // ====================== End of changes   -- by BILLY 11Jan2001 ============================

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      eh.setIncomeId(this.getIncomeId());
      trackEntityCreate();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Income Entity - Income - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

     srk.setModified(true);

    if(dcm != null)
      //dcm.inputEntity(this);
      dcm.inputCreatedEntity ( this );

    return this;
 }

  public void setIncomeId(int id)
  {
   this.testChange ("incomeId", id );
   this.incomeId = id;
  }

  public void setIncomePeriodId(int id)
  {
     this.testChange ("incomePeriodId", id );
     this.incomePeriodId = id;
  }

   public void setIncomeTypeId(int id)
  {
     this.testChange ("incomeTypeId", id );
     this.incomeTypeId = id;
  }

   public void setIncomeAmount(double amt)
  {
     this.testChange ("incomeAmount", amt );
     this.incomeAmount = amt;
  }

   public void setAnnualIncomeAmount(double amt)
  {
     this.testChange ("annualIncomeAmount", amt ) ;
     this.annualIncomeAmount = amt;
  }

    public void setMonthlyIncomeAmount( double mia)
  {
      this.testChange ("monthlyIncomeAmount", mia );
      this.monthlyIncomeAmount = mia;
  }

  public void setBorrowerId(int id)
  {
     this.testChange ("borrowerId", id );
     this.borrowerId = id;
  }

  public void setIncIncludeInGDS(String c)
  {
    setIncIncludeInGDS(TypeConverter.booleanFrom(c));

  }

  public void setIncIncludeInTDS(String c)
  {
     setIncIncludeInTDS(TypeConverter.booleanFrom(c));
  }

  public void setIncIncludeInGDS(boolean b)
  {
     this.testChange ("incIncludeInGDS", b  );
     this.incIncludeInGDS = b;
  }

  public void setIncIncludeInTDS(boolean b)
  {
     this.testChange ("incIncludeInTDS", b );
     this.incIncludeInTDS = b;
  }

  public void setIncPercentInGDS(int i)
  {
     this.testChange ("incPercentInGDS", i ) ;
     this.incPercentInGDS = i;
  }


  public void setIncPercentInTDS(int i)
  {
     this.testChange ("incPercentInTDS", i ) ;
     this.incPercentInTDS = i;
  }

  public void setIncPercentOutGDS(int i)
  {
     this.testChange ("incPercentOutGDS", i ) ;
     this.incPercentOutGDS = i;
  }


  public void setIncPercentOutTDS(int i)
  {
    this.testChange ("incPercentOutTDS", i ) ;
    this.incPercentOutTDS = i;
  }

  public void setIncomeDescription( String s )
  {
    this.testChange ("incomeDescription", s ) ;
    this.incomeDescription = s;
  }

  public EmploymentHistory getEmploymentHistories() throws Exception
  {
     EmploymentHistory eh = new EmploymentHistory(srk);

     // supress finder exception if not found (e.g. income may be an 'other' income  - no associated with an
     // employment history
     eh.setSilentMode(true);

     return eh.findByIncome(this.pk);
  }

  public int getClassId()
  {
    return ClassId.INCOME;
  }

  public boolean isAuditable()
  {
    return true;
  }


  public boolean equals ( Object o )
  {
    if ( o instanceof Income )
      {
        if  (
             ( this.incomeId  == ((Income)o).getIncomeId () )
              &&   ( this.copyId == ((Income)o).getCopyId () )
             )
                return true ;
      }
    return false;
  }

  protected EntityContext getEntityContext() throws RemoteException
  {
    EntityContext ctx = this.entityContext  ;
    try
    {
      ctx.setContextText ( this.incomeDescription  ) ;
      Borrower borrower = new Borrower( srk, null , this.borrowerId , this.copyId  );
      if ( borrower.getBorrowerFirstName () != null && borrower.getBorrowerLastName ()!= null )
          ctx.setContextSource (borrower.getNameForEntityContext()) ;

      //Modified by Billy for performance tuning -- By BILLY 28Jan2002
      //Deal deal = new Deal( srk, null);
      //deal = deal.findByPrimaryKey( new DealPK( borrower.getDealId () , borrower.getCopyId () ) ) ;
      //if ( deal == null )
      //  return ctx;
      //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
      Deal deal = new Deal(srk, null);
      String  sc = String.valueOf (deal.getScenarioNumber(borrower.getDealId () , borrower.getCopyId ())) +
          deal.getCopyType(borrower.getDealId () , borrower.getCopyId ());
      // ===================================================================================

    ctx.setScenarioContext ( sc);

      ctx.setApplicationId ( borrower.getDealId() ) ;

    } catch ( Exception e )
    {
      if ( e instanceof FinderException )
      {
        logger.warn( "Income.getEntityContext Parent Not Found. IncomeId=" + this.incomeId  );
        return ctx;
      }
      throw ( RemoteException ) e ;
    } // end of -- try -- catch ----
    return ctx;
  } // -------------- End of getEntityContext ----------------------------------

  // Overload this method for special request to validate and set data -- By BILLY 31Jan2001
  public void ejbStore() throws RemoteException
  {
    validateData();
    super.ejbStore();
  }

  // New Method to validate and set data before save -- by BILLY 31Jan2001
  private void validateData()
  {
    // Set Include GDS and TDS to NO if this Income is used for EmploymentHistory and
    //  the EmploymentHistory.Status == Previous (1)
    try
    {
      EmploymentHistory theEmploy = getEmploymentHistories();
      if( theEmploy != null)
      {
        if( theEmploy.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_PREVIOUS )
        {
          // Force include GDS and TDS to false
          setIncIncludeInGDS(false);
          setIncIncludeInTDS(false);
          // Force GDS and TDS % to 0
          setIncPercentInGDS(0);
          setIncPercentInTDS(0);
        }
      }
    }
    catch(Exception e)
    {
      ; // Record not find -- ok, see getEmploymentHistories() for explanation !!
    }
  }
    public int getInstProfileId() {
    	return instProfileId;
    }
    
    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
    }
}
