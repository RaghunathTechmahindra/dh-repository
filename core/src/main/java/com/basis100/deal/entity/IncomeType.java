package com.basis100.deal.entity;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.IncomeTypePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class IncomeType extends DealEntity {

	protected int incomeTypeId;

	protected int GDSInclusion;

	protected int TDSInclusion;

	protected String ITDescription;

	protected String employmentRelated;

	private IncomeTypePK pk;

	public IncomeType(SessionResourceKit srk) throws RemoteException,
			FinderException {
		super(srk);
	}

	public IncomeType(SessionResourceKit srk, int id) throws RemoteException,
			FinderException {
		super(srk);

		pk = new IncomeTypePK(id);

		findByPrimaryKey(pk);
	}

	public IncomeType findByPrimaryKey(IncomeTypePK pk) throws RemoteException,
			FinderException {
		
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from IncomeType " + pk.getWhereClause();

		boolean gotRecord = false;

		try {
			int key = jExec.execute(sql);

			for (; jExec.next(key);) {
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false) {
				String msg = "IncomeType: @findByPrimaryKey(), key= " + pk
						+ ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		} catch (Exception e) {
			FinderException fe = new FinderException(
					"IncomeType Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}
		this.pk = pk;
		ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
		return this;
	}

	/**
	 * New method to find the first entry matched the input whereClause Usage :
	 * used in getting the first matched option for OtherIncome type
	 */
	public IncomeType findFirstMatched(String whereClause)
			throws RemoteException, FinderException {
		String sql = "Select * from IncomeType " + whereClause;

		boolean gotRecord = false;

		try {
			int key = jExec.execute(sql);

			for (; jExec.next(key);) {
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false) {
				String msg = "IncomeType: @findFirstMatched(), whereClause= "
						+ whereClause + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		} catch (Exception e) {
			FinderException fe = new FinderException(
					"IncomeType Entity - findFirstMatched() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return this;
	}

	private void setPropertiesFromQueryResult(int key) throws Exception {
		setIncomeTypeId(jExec.getInt(key, "INCOMETYPEID"));
		setITDescription(jExec.getString(key, "ITDESCRIPTION"));
		setEmploymentRelated(jExec.getString(key, "EMPLOYMENTRELATED"));
		setGDSInclusion(jExec.getInt(key, "GDSINCLUSION"));
		setTDSInclusion(jExec.getInt(key, "TDSINCLUSION"));
	}

	/**
	 * gets the value of instance fields as String. if a field does not exist
	 * (or the type is not serviced)** null is returned. if the field exists
	 * (and the type is serviced) but the field is null an empty String is
	 * returned.
	 * 
	 * @returns value of bound field as a String;
	 * @param fieldName
	 *            as String
	 * 
	 * Other entity types are not yet serviced ie. You can't get the Province
	 * object for province.
	 */
	public String getStringValue(String fieldName) {
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**
	 * sets the value of instance fields as String. if a field does not exist or
	 * is not serviced an Exception is thrown if the field exists (and the type
	 * is serviced) but the field is null an empty String is returned.
	 * 
	 * @param fieldName
	 *            as String
	 * @param the
	 *            value as a String
	 * 
	 * Other entity types are not yet serviced ie. You can't set the Province
	 * object in Addr.
	 */
	public void setField(String fieldName, String value) throws Exception {
		doSetField(this, this.getClass(), fieldName, value);
	}

	/**
	 * Updates any Database field that has changed since the last
	 * synchronization ie. the last findBy... call
	 * 
	 * @return the number of updates performed
	 */
	protected int performUpdate() throws Exception {
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);

		return ret;
	}

	public String getEntityTableName() {
		return "IncomeType";
	}

	/**
	 * gets the incomeId associated with this entity
	 * 
	 * @return a int
	 */
	public int getIncomeTypeId() {
		return this.incomeTypeId;
	}

	/**
	 * gets the pk associated with this entity
	 * 
	 * @return a IncomeTypePK
	 */
	public IEntityBeanPK getPk() {
		return (IEntityBeanPK) this.pk;
	}

	public int getGDSInclusion() {
		return this.GDSInclusion;
	}

	public int getTDSInclusion() {
		return this.TDSInclusion;
	}

	public String getITDescription() {
		return this.ITDescription;
	}

	public String getEmploymentRelated() {
		return this.employmentRelated;
	}

	public IncomeType create(int id, String desc, String erel, double gdsInc,
			double tdsInc) throws RemoteException, CreateException {

		String sql = "Insert into IncomeType Values ( " + id + "," + " '" + desc + "'" + ","
		+ " '" + erel + "'" + "," + gdsInc + "," + tdsInc + ")";
		try {
			jExec.executeUpdate(sql);
			logger.error("check sql work or not =========" + sql);
			findByPrimaryKey(pk);
			this.trackEntityCreate(); // track the creation
		} catch (Exception e) {
			CreateException ce = new CreateException(
					"IncomeType Entity - IncomeType - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);

			throw ce;
		}

		srk.setModified(true);

		if (dcm != null)
			dcm.inputEntity(this);

		return this;
	}

	public IncomeType deepCopy() throws CloneNotSupportedException {
		return (IncomeType) this.clone();
	}

	public void setIncomeTypeId(int id) {
		this.testChange("incomeTypeId", id);
		this.incomeTypeId = id;
	}

	public void setGDSInclusion(int c) {
		this.testChange("GDSInclusion", c);
		this.GDSInclusion = c;
	}

	public void setTDSInclusion(int c) {
		this.testChange("TDSInclusion", c);
		this.TDSInclusion = c;
	}

	public void setITDescription(String desc) {
		this.testChange("ITDescription", desc);
		this.ITDescription = desc;
	}

	public void setEmploymentRelated(String q) {
		this.testChange("employmentRelated", q);
		this.employmentRelated = q;
	}

	public int getClassId() {
		return ClassId.DEFAULT;
	}

	public boolean equals(Object o) {
		if (o instanceof IncomeType) {
			IncomeType oa = (IncomeType) o;
			if (this.incomeTypeId == oa.getIncomeTypeId())
				return true;
		}
		return false;
	}
	

}
