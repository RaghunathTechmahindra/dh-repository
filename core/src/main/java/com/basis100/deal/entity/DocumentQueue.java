package com.basis100.deal.entity;

import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.DocumentQueuePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.DBA;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import com.basis100.deal.docprep.Dc;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * Entity related to DocumentQueue record. The DocumentQueue is the arrival
 * place for all document/report requests
 * 
 * @version 1.1 <br>
 *          Date: 08/04/2006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Changes made to facilitate Rate Synchronization in Expert <br> -
 *          added method create
 */
public class DocumentQueue extends DealEntity {
    protected int documentQueueId;

    protected Date timeRequested;

    protected int requestorUserId;

    protected String documentFullName;
    
    protected String emailAddress;

    protected String emailFullName;

    protected String emailSubject;

    protected int languagePreferenceId;

    protected int lenderProfileId;

    protected int documentTypeId;

    protected String documentFormat;

    protected String documentVersion;

    protected int dealId;

    protected int dealCopyId;

    protected String scenarioNumber;

    protected int requestStatusId;
    
    protected int documentStatus;

    protected String emailText;

    protected String faxNumbers;

    protected int instProfileId;

    public DocumentQueuePK pk;

    public DocumentQueue(SessionResourceKit srk) throws RemoteException,
	    FinderException {
	super(srk);
    }

    public DocumentQueue(SessionResourceKit srk, int id)
	    throws RemoteException, FinderException {
	super(srk);

	pk = new DocumentQueuePK(id);

	findByPrimaryKey(pk);
    }

    /**
         * Finds the record identified by the the primary key id contained in
         * the DocumentQueuePK parameter.
         * 
         * @return this DocumentQueue instance populated with data from the
         *         corresponding record.
         * @throws FinderException
         *                 if record not found
         */
    public DocumentQueue findByPrimaryKey(DocumentQueuePK pk)
	    throws RemoteException, FinderException {
	String sql = "Select * from documentQueue " + pk.getWhereClause();

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
			+ ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }

	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "Deal Entity - findByPrimaryKey() exception");
	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);
	    throw fe;
	}

	this.pk = pk;
	return this;
    }

    /**
         * Finds the record identified by the the primary key id contained in
         * the DocumentQueuePK parameter.
         * 
         * @return this DocumentQueue instance populated with data from the
         *         corresponding record.
         * @throws FinderException
         *                 if record not found
         */
    public Collection findByOrphanedRequest(int period, int requeue)
	    throws RemoteException, FinderException, Exception {
	List found = new ArrayList();

	Calendar cal = Calendar.getInstance();
	cal.add(cal.MINUTE, (period * -1));
	Date maxdate = cal.getTime();

	String sql = "Select * from documentQueue where documentQueueId < 0 and "
		+ "timeRequested < "
		+ DBA.sqlStringFrom(maxdate)
		+ " and requestStatusId <= " + requeue;

	logger.trace(sql);
	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		DocumentQueue dq = new DocumentQueue(srk);
		dq.setPropertiesFromQueryResult(key);
		dq.pk = new DocumentQueuePK(dq.getDocumentQueueId());

		found.add(dq);
	    }

	    jExec.closeData(key);

	} catch (Exception e) {
	    String msg = "DocumentQueue exception in findByOrphanedRequest";

	    if (e.getMessage() != null)
		msg = msg + e.getMessage();

	    logger.error(msg);
	    logger.error(e);
	    throw e;
	}

	return found;
    }

    /**
         * Finds unhandled records that are older than the period whose value
         * does not equal the given value: disableval.
         * 
         * @return a collection of DocumentQueue entities
         * @throws FinderException
         *                 if record not found
         */
    public Collection findByUnclearedUnhandledOrphans(int period, int disableval)
	    throws RemoteException, FinderException, Exception {
	List found = new ArrayList();

	Calendar cal = Calendar.getInstance();
	cal.add(cal.HOUR_OF_DAY, (period * -1));
	Date maxdate = cal.getTime();

	String sql = "Select * from documentQueue where documentQueueId < 0 and "
		+ "timeRequested < "
		+ DBA.sqlStringFrom(maxdate)
		+ " and "
		+ "requestStatusId <> " + disableval;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		DocumentQueue dq = new DocumentQueue(srk);

		dq.setPropertiesFromQueryResult(key);

		dq.pk = new DocumentQueuePK(dq.getDocumentQueueId());

		found.add(dq);
	    }

	    jExec.closeData(key);

	} catch (Exception e) {
	    String msg = "DocumentQueue exception in findByOrphanedRequest";

	    if (e.getMessage() != null)
		msg = msg + e.getMessage();

	    logger.error(msg);
	    logger.error(e);
	    throw e;
	}

	return found;
    }

    /**
         * checks the queue table for any new Document Generation Requests
         */
    public DocumentQueue findNextEntryTest() throws Exception {
	String sql = "Select MIN(documentQueueId) from DocumentQueue where documentQueueId > 0 AND "
		+ "requestStatusId = " + Dc.DOCUMENT_REQUEST_STATUS_NEW;

	int id = -1;

	int key = jExec.execute(sql);

	for (; jExec.next(key);) {
	    id = jExec.getInt(key, 1);
	    break; // one record at a time;
	}

	jExec.closeData(key);

	if (id <= 0)
	    return null;

	DocumentQueue queue = null;

	try {
	    queue = new DocumentQueue(srk);
	    queue = queue.findByPrimaryKey(new DocumentQueuePK(id));
	} catch (Exception ex) {
	    return null;
	}

	return queue;
    }

    /**
         * checks the queue table for any new Document Generation Requests
         */
    public DocumentQueue findNextEntry() throws Exception {
	String sql = "Select MIN(documentQueueId) from DocumentQueue where documentQueueId > 0 AND "
		+ "requestStatusId <> " + Dc.DOCUMENT_REQUEST_STATUS_DISABLED;

	int id = -1;

	int key = jExec.execute(sql);

	for (; jExec.next(key);) {
	    id = jExec.getInt(key, 1);
	    break; // one record at a time;
	}

	jExec.closeData(key);

	if (id <= 0)
	    return null;

	DocumentQueue queue = null;

	try {
	    queue = new DocumentQueue(srk);
	    queue = queue.findByPrimaryKey(new DocumentQueuePK(id));
	} catch (Exception ex) {
	    return null;
	}

	return queue;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
	setDocumentQueueId(jExec.getInt(key, "DOCUMENTQUEUEID"));
	setTimeRequested(jExec.getDate(key, "TIMEREQUESTED"));
	setRequestorUserId(jExec.getInt(key, "REQUESTORUSERID"));
	setEmailAddress(jExec.getString(key, "EMAILADDRESS"));
	setEmailFullName(jExec.getString(key, "EMAILFULLNAME"));
	setEmailSubject(jExec.getString(key, "EMAILSUBJECT"));
	setLanguagePreferenceId(jExec.getInt(key, "LANGUAGEPREFERENCEID"));
	setLenderProfileId(jExec.getInt(key, "LENDERPROFILEID"));
	setDocumentTypeId(jExec.getInt(key, "DOCUMENTTYPEID"));
	setDocumentFormat(jExec.getString(key, "DOCUMENTFORMAT"));
	setDocumentVersion(jExec.getString(key, "DOCUMENTVERSION"));
	setDealId(jExec.getInt(key, "DEALID"));
	setDealCopyId(jExec.getInt(key, "DEALCOPYID"));
	setScenarioNumber(jExec.getString(key, "SCENARIONUMBER"));
	setRequestStatusId(jExec.getInt(key, "REQUESTSTATUSID"));
	setEmailText(jExec.getString(key, "EMAILTEXT"));
	setFaxNumbers(jExec.getString(key, "FAXNUMBERS"));
	setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }
    
    /**
         * gets the value of instance fields as String. if a field does not
         * exist (or the type is not serviced)** null is returned. if the field
         * exists (and the type is serviced) but the field is null an empty
         * String is returned.
         * 
         * @returns value of bound field as a String;
         * @param fieldName
         *                as String
         * 
         * Other entity types are not yet serviced ie. You can't get the
         * Province object for province.
         */
    public String getStringValue(String fieldName) {
	return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
         * gets the pk associated with this entity
         * 
         * @return a DealFeePK
         */
    public IEntityBeanPK getPk() {
	return (IEntityBeanPK) this.pk;
    }

    /**
         * sets the value of instance fields as String. if a field does not
         * exist or is not serviced an Exception is thrown if the field exists
         * (and the type is serviced) but the field is null an empty String is
         * returned.
         * 
         * @param fieldName
         *                as String
         * @param the
         *                value as a String
         * 
         * Other entity types are not yet serviced ie. You can't set the
         * Province object in Addr.
         */
    public void setField(String fieldName, String value) throws Exception {
	doSetField(this, this.getClass(), fieldName, value);
    }

    protected int performUpdate() throws Exception {
	return (doPerformUpdate(this, this.getClass()));
    }

    /**
         * gets the documentQueueId associated with this entity
         * 
         * @return a int
         */
    public int getDocumentQueueId() {
	return this.documentQueueId;
    }

    public Date getTimeRequested() {
	return this.timeRequested;
    }

    public int getRequestorUserId() {
	return this.requestorUserId;
    }

    public String getEmailAddress() {
	return this.emailAddress;
    }

    public String getEmailFullName() {
	return this.emailFullName;
    }

    public String getEmailSubject() {
	return this.emailSubject;
    }

    public int getLanguagePreferenceId() {
	return this.languagePreferenceId;
    }

    public int getLenderProfileId() {
	return this.lenderProfileId;
    }

    public String getDocumentFormat() {
	return this.documentFormat;
    }

    public String getDocumentVersion() {
	return this.documentVersion;
    }

    public int getDealId() {
	return this.dealId;
    }

    public int getDealCopyId() {
	return dealCopyId;
    }

    public String getScenarioNumber() {
	return this.scenarioNumber;
    }

    public int getRequestStatusId() {
	return this.requestStatusId;
    }

    public int getDocumentTypeId() {
	return this.documentTypeId;
    }

    public String getEmailText() {
	return this.emailText;
    }

    public String getFaxNumbers() {
	return this.faxNumbers;
    }

    public DocumentQueuePK createPrimaryKey() throws CreateException {
	String sql = "Select DocumentQueueId.nextval from dual";
	int id = -1;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		id = jExec.getInt(key, 1); // can only be one record
	    }
	    jExec.closeData(key);
	    if (id == -1)
		throw new Exception();

	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DocumentQueue Entity create() exception getting DocumentQueueId from sequence");
	    logger.error(ce.getMessage());
	    logger.error(e);
	    throw ce;
	}

	return new DocumentQueuePK(id);
    }

    /**
         * creates a DocumentQueue record
         */
    public DocumentQueue create(Date timeRequested, int requestorUserId,
	    String emailAddress, String emailFullName, String emailSubject,
	    int dealId, int dealCopyId, DocumentProfilePK dppk,
	    String pFaxNumbers) throws RemoteException, CreateException {
	return create(timeRequested, requestorUserId, emailAddress,
		emailFullName, emailSubject, "", dealId, dealCopyId, null,
		dppk, pFaxNumbers);
    }

    /**
         * creates a DocumentQueue record (this method was retained to ensure
         * backward-compatibility)
         */
    public DocumentQueue create(Date timeRequested, int requestorUserId,
	    String emailAddress, String emailFullName, String emailSubject,
	    int dealId, int dealCopyId, String scenarioNumber,
	    DocumentProfilePK dppk, String pFaxNumbers) throws RemoteException,
	    CreateException {
	return create(timeRequested, requestorUserId, emailAddress,
		emailFullName, emailSubject, "", dealId, dealCopyId,
		scenarioNumber, dppk, pFaxNumbers);
    }

    /**
         * creates a DocumentQueue record
         */
    public DocumentQueue create(Date timeRequested, int requestorUserId,
	    String emailAddress, String emailFullName, String emailSubject,
	    String emailText, int dealId, int dealCopyId,
	    String scenarioNumber, DocumentProfilePK dppk, String pFaxNumbers)
	    throws RemoteException, CreateException {
	this.pk = this.createPrimaryKey();

	// temp fix: remove not null constraint in db

	if (emailSubject == null || emailSubject.length() == 0)
	    emailSubject = "Not Required";

	String comma = ", ";

	StringBuffer sqlbuf = new StringBuffer(
		"Insert into DocumentQueue values( ");
	sqlbuf.append(sqlStringFrom(pk.getId()) + ",");
	sqlbuf.append(sqlStringFrom(timeRequested)).append(comma);
	sqlbuf.append(sqlStringFrom(requestorUserId)).append(comma);
	sqlbuf.append(sqlStringFrom(emailAddress)).append(comma);
	sqlbuf.append(sqlStringFrom(emailFullName)).append(comma);
	sqlbuf.append(sqlStringFrom(emailSubject)).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getLanguagePreferenceId())).append(
		comma);
	sqlbuf.append(sqlStringFrom(dppk.getLenderProfileId())).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getDocumentTypeId())).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getDocumentFormat())).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getDocumentVersion())).append(comma);
	if (dealId > 0) {
	    sqlbuf.append(sqlStringFrom(dealId)).append(comma);
	    sqlbuf.append(sqlStringFrom(dealCopyId)).append(comma);
	} else {
	    sqlbuf.append(" null ").append(comma);
	    sqlbuf.append(" null ").append(comma);
	}
	sqlbuf.append(sqlStringFrom(scenarioNumber)).append(comma);
	sqlbuf.append(sqlStringFrom(0)).append(comma);
	sqlbuf.append(sqlStringFrom(emailText)).append(comma);
	sqlbuf.append(sqlStringFrom(pFaxNumbers)).append(comma);
	sqlbuf.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()));
	sqlbuf.append(")");
	String sql = new String(sqlbuf);

	try {
	    jExec.executeUpdate(sql);
	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DocumentQueue Entity - DocumentQueue - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);

	    throw ce;
	}

    this.pk = pk;
    this.timeRequested = timeRequested;
    this.requestorUserId = requestorUserId;
	this.documentQueueId = pk.getId();
	this.dealId = dealId;
	this.dealCopyId = dealCopyId;
	this.emailFullName = emailFullName;
	this.emailAddress = emailAddress;
	this.emailSubject = emailSubject;
	this.documentVersion = dppk.getDocumentVersion();
	this.documentFormat = dppk.getDocumentFormat();
	this.languagePreferenceId = dppk.getLanguagePreferenceId();
	this.lenderProfileId = dppk.getLenderProfileId();
	this.scenarioNumber = scenarioNumber;
	this.emailText = emailText;
    this.faxNumbers = pFaxNumbers;
    this.documentTypeId = dppk.getDocumentTypeId();
    this.instProfileId = srk.getExpressState().getDealInstitutionId();

	srk.setModified(true);
	return this;
    }

    // ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
    /**
         * create Method used to create a document entity in the DocumentQueue
         * 
         * @param timeRequested
         *                Date and Time when the Document entry was added<br>
         * @param requestorUserId
         *                User identification number of the requestor<br>
         * @param emailAddress
         *                Mail address to which the document needs to be
         *                delivered<br>
         * @param emailSubject
         *                Mail subject line text
         * @param emailText
         *                Text content for the mail body
         * @param dppk
         *                DocumentProfilePK - Primary key entity
         * @return DocumentQueue
         * @throws CreateException
         *                 DocumentQueue record insertion fails
         */
    public DocumentQueue create(Date timeRequested, int requestorUserId,
	    String emailAddress, String emailSubject, String emailText,
	    DocumentProfilePK dppk) throws CreateException {
	this.pk = this.createPrimaryKey();

	// emailSubject is a mandatory field in the DB
	if (emailSubject == null || emailSubject.length() == 0)
	    emailSubject = "Not Defined";

	String comma = ", ";

	// Preparing insert SQL for DocumentQueue
	StringBuffer sqlbuf = new StringBuffer("Insert into DocumentQueue(");
	sqlbuf.append(" DocumentQueueId, TimeRequested, RequestorUserId,"
		+ " EmailAddress, EmailSubject, LanguagePreferenceId,"
		+ " LenderProfileId, DocumentTypeId, DocumentFormat, "
		+ " DocumentVersion, RequestStatusId,   EmailText, INSTITUTIONPROFILEID");
	sqlbuf.append(") values (");
	sqlbuf.append(sqlStringFrom(pk.getId())).append(comma);
	sqlbuf.append(sqlStringFrom(timeRequested)).append(comma);
	sqlbuf.append(sqlStringFrom(requestorUserId)).append(comma);
	sqlbuf.append(sqlStringFrom(emailAddress)).append(comma);
	sqlbuf.append(sqlStringFrom(emailSubject)).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getLanguagePreferenceId())).append(
		comma);
	sqlbuf.append(sqlStringFrom(dppk.getLenderProfileId())).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getDocumentTypeId())).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getDocumentFormat())).append(comma);
	sqlbuf.append(sqlStringFrom(dppk.getDocumentVersion())).append(comma);
	sqlbuf.append(sqlStringFrom(0)).append(comma);
	sqlbuf.append(sqlStringFrom(emailText)).append(comma);
	sqlbuf.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()));
	sqlbuf.append(")");
	String sql = new String(sqlbuf);

	// Execute the SQL statement
	try {
	    jExec.executeUpdate(sql);
	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DocumentQueue Entity - DocumentQueue - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);
	    throw ce;
	}

	// Initialize the instance
	this.documentQueueId = pk.getId();
	this.timeRequested = timeRequested;
	this.requestorUserId = requestorUserId;
	this.emailAddress = emailAddress;
	this.emailSubject = emailSubject;
	this.documentVersion = dppk.getDocumentVersion();
	this.documentFormat = dppk.getDocumentFormat();
	this.languagePreferenceId = dppk.getLanguagePreferenceId();
	this.lenderProfileId = dppk.getLenderProfileId();
	this.documentTypeId = dppk.getDocumentTypeId();
    this.emailText = emailText;
    this.instProfileId = srk.getExpressState().getDealInstitutionId();


	// Set modified flag to true
	srk.setModified(true);
	return this;
    }

    // ***** Change by NBC Impl. Team - Version 1.1 - End *****//

    // Catherine Rutgaizer - for Alert Messages
    // Doc central project and future use
    public DocumentQueue create(Date timeRequested, int requestorUserId,
	    String emailAddress, String emailFullName, String emailSubject,
	    String emailText, int dealId, int dealCopyId)
	    throws RemoteException, CreateException {
	this.pk = this.createPrimaryKey();

	// temp fix: remove not null constraint in db

	if (emailSubject == null || emailSubject.length() == 0)
	    emailSubject = "Not Required";

	String comma = ", ";

	StringBuffer sqlbuf = new StringBuffer("Insert into DocumentQueue ");
	sqlbuf.append("( DocumentQueueId,   TimeRequested,  RequestorUserId,"
		+ " EmailAddress,      EmailSubject,   LanguagePreferenceId,"
		+ " LenderProfileId,   DocumentTypeId, DocumentFormat, "
		+ " DocumentVersion,   DealId,         DealCopyId, "
		+ " RequestStatusId,   EmailText, INSTITUTIONPROFILEID)");
	sqlbuf.append(" values(  ");
	sqlbuf.append(sqlStringFrom(pk.getId()) + ",");
	sqlbuf.append(sqlStringFrom(timeRequested)).append(comma);
	sqlbuf.append(sqlStringFrom(requestorUserId)).append(comma);
	sqlbuf.append(sqlStringFrom(emailAddress)).append(comma);
	sqlbuf.append(sqlStringFrom(emailSubject)).append(comma);
	sqlbuf.append(sqlStringFrom("0")).append(comma); // lang pref
	sqlbuf.append(sqlStringFrom("0")).append(comma); // lender
                                                                // profile
	sqlbuf.append(sqlStringFrom("99")).append(comma); // document type
                                                                // id
	sqlbuf.append(sqlStringFrom("rtf1")).append(comma);
	sqlbuf.append(sqlStringFrom("GenX1.0")).append(comma);
	if (dealId > 0) {
	    sqlbuf.append(sqlStringFrom(dealId)).append(comma);
	    sqlbuf.append(sqlStringFrom(dealCopyId)).append(comma);
	} else {
	    sqlbuf.append(" null ").append(comma);
	    sqlbuf.append(" null ").append(comma);
	}
	sqlbuf.append(sqlStringFrom("0")).append(comma); // request
                                                                // status
	sqlbuf.append(sqlStringFrom(emailText)).append(comma);
	sqlbuf.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()));
	sqlbuf.append(")");
	String sql = new String(sqlbuf);

	try {
	    jExec.executeUpdate(sql);
	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DocumentQueue.create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);

	    throw ce;
	}

	this.pk = pk;
	this.documentQueueId = pk.getId();
	this.dealId = dealId;
	this.dealCopyId = dealCopyId;
	this.emailFullName = emailFullName;
	this.emailAddress = emailAddress;
	this.emailSubject = emailSubject;
	this.scenarioNumber = scenarioNumber;
	this.emailText = emailText;

	srk.setModified(true);
	return this;
    }

    public void ejbRemove(boolean calc) throws RemoteException {
	ejbRemove();
    }

    public void ejbRemove() throws RemoteException {
	String sql = "DELETE FROM DocumentQueue where documentQueueId = "
		+ getDocumentQueueId();

	try {
	    jExec.executeUpdate(sql);
	} catch (Exception e) {
	    RemoteException dpe = new RemoteException(
		    "Error attempting to remove DocumentQueue entry - "
			    + pk.getId());
	    logger.error(dpe.getMessage());
	    logger.error(e);

	    throw dpe;
	}

    }

    public void setDocumentQueueId(int value) {
	this.testChange("documentQueueId", value);
	this.documentQueueId = value;
    }

    public void setTimeRequested(Date value) {
	this.testChange("timeRequested", value);
	this.timeRequested = value;
    }

    public void setRequestorUserId(int value) {
	this.testChange("requestorUserId", value);
	this.requestorUserId = value;
    }

    public void setEmailAddress(String value) {
	this.testChange("emailAddress", value);
	this.emailAddress = value;
    }

    public void setEmailFullName(String value) {
	this.testChange("emailFullName", value);
	this.emailFullName = value;
    }

    public void setEmailSubject(String value) {
	this.testChange("emailSubject", value);
	this.emailSubject = value;
    }

    public void setLanguagePreferenceId(int value) {
	this.testChange("languagePreferenceId", value);
	this.languagePreferenceId = value;
    }

    public void setLenderProfileId(int value) {
	this.testChange("lenderProfileId", value);
	this.lenderProfileId = value;
    }

    public void setDocumentTypeId(int value) {
	this.testChange("documentTypeId", value);
	this.documentTypeId = value;
    }

    public void setDocumentFormat(String value) {
	this.testChange("documentFormat", value);
	this.documentFormat = value;
    }

    public void setDocumentVersion(String value) {
	this.testChange("documentVersion", value);
	this.documentVersion = value;
    }

    public void setDealId(int value) {
	this.testChange("dealId", value);
	this.dealId = value;
    }

    public void setDealCopyId(int value) {
	this.testChange("dealCopyId", value);
	this.dealCopyId = value;
    }

    public void setScenarioNumber(String value) {
	this.testChange("scenarioNumber", value);
	this.scenarioNumber = value;
    }

    public void setRequestStatusId(int value) {
	this.testChange("requestStatusId", value);
	this.requestStatusId = value;
    }

    public void setEmailText(String value) {
	this.testChange("emailText", value);
	this.emailText = value;
    }

    public void setFaxNumbers(String value) {
	this.testChange("faxNumbers", value);
	this.faxNumbers = value;
    }

    // business method

    /**
         * @return the DocumentProfile corresponding to this request if one can
         *         be formed else return null
         */
    public DocumentProfile buildDocumentProfile() {
	try {
	    DocumentProfile profile = new DocumentProfile(srk);

	    DocumentProfilePK dppk = new DocumentProfilePK(documentVersion,
		    languagePreferenceId, lenderProfileId, documentTypeId,
		    documentFormat);
//	    DocumentProfilePK dppk = new DocumentProfilePK(documentFullName, documentVersion,
//		    languagePreferenceId, lenderProfileId, documentTypeId, documentFormat,
//		    documentStatus);
	    profile.findByPrimaryKey(dppk);

	    return profile;

	} catch (Exception ex) {
	    return null;

	}

    }

    public String toString() {
	if (pk == null)
	    return "Uninitialized DocumentQueue Entity: " + hashCode();
	else
	    return this.pk.toString();
    }

    public String getEntityTableName() {
	return "DocumentQueue";
    }

    public int getInstProfileId() {
	return instProfileId;
    }

    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
    }

}
