package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.CreditReferencePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class CreditReference extends DealEntity
{
   protected int      creditReferenceId;
   protected String   institutionName;
   protected String   accountNumber;
   protected double   currentBalance;
   protected int      creditRefTypeId;
   protected int      borrowerId;
   protected int      copyId;
   protected String   creditReferenceDescription;
   protected int      timeWithReference;
   
   protected int      institutionId;

   private CreditReferencePK pk;

   public CreditReference(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public CreditReference(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
   {
      super(srk,dcm);
   }

   public CreditReference(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
   {
      super(srk,dcm);

      pk = new CreditReferencePK(id, copyId);

      findByPrimaryKey(pk);
   }

   public CreditReference findByPrimaryKey(CreditReferencePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from CreditReference " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException();
          logger.error(e.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
   }

   public Collection<CreditReference> findByBorrower(BorrowerPK pk) throws Exception
   {
      Collection<CreditReference> creditReferences = new ArrayList<CreditReference>();

      String sql = "Select * from CreditReference " ;
            sql +=  pk.getWhereClause();
            sql +=  " order by currentBalance";
       try
       {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            CreditReference cr = new CreditReference(srk,dcm);
            cr.setPropertiesFromQueryResult(key);
            cr.pk = new CreditReferencePK(cr.getCreditReferenceId(),cr.getCopyId());

            creditReferences.add(cr);
          }

          jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Borrower::CreditReferences");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

       return creditReferences;
    }

     /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);

   }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     this.setCreditReferenceId(jExec.getInt(key, "CREDITREFERENCEID"));
     this.setInstitutionName(jExec.getString(key, "INSTITUTIONNAME"));
     this.setAccountNumber(jExec.getString(key, "ACCOUNTNUMBER"));
     this.setCurrentBalance(jExec.getDouble(key, "CURRENTBALANCE"));
     this.setCreditRefTypeId(jExec.getInt(key, "CREDITREFTYPEID"));
     this.setBorrowerId(jExec.getInt(key, "BORROWERID"));
     this.setCopyId(jExec.getInt(key, "COPYID"));
     this.setCreditReferenceDescription(jExec
             .getString(key, "CREDITREFERENCEDESCRIPTION"));
     this.setTimeWithReference(jExec.getInt(key, "TIMEWITHREFERENCE"));
     this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();
    int ret = doPerformUpdate(this, clazz);

    // Optimization:
    // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;
  }

  public String getEntityTableName()
  {
     return "CreditReference" ;
  }


    public int getCreditRefTypeId()
   {
      return this.creditRefTypeId ;
   }

		/**
     *   gets the currentBalance associated with this entity
     *
     *   @return a double
     */
    public double getCurrentBalance()
   {
      return this.currentBalance ;
   }


		/**
     *   gets the pk associated with this entity
     *
     *   @return a CreditReferencePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

		/**
     *   gets the borrowerId associated with this entity
     *
     *   @return a int
     */
    public int getBorrowerId()
   {
      return this.borrowerId ;
   }

		/**
     *   gets the creditReferenceId associated with this entity
     *
     *   @return a int
     */
    public int getCreditReferenceId()
   {
      return this.creditReferenceId ;
   }

		/**
     *   gets the institutionName associated with this entity
     *
     *   @return a String
     */
    public String getInstitutionName()
   {
      return this.institutionName ;
   }

		/**
     *   gets the accountNumber associated with this entity
     *
     *   @return a String
     */
   public String getAccountNumber()
   {
      return this.accountNumber ;
   }

   public int getTimeWithReference()
   {
      return this.timeWithReference;
   }

   public String getCreditReferenceDescription()
	 {
		 return this.creditReferenceDescription;
   }

   public int getCopyId()
   {
      return copyId;
   }

   private CreditReferencePK createPrimaryKey(int copyId)throws CreateException
   {
     String sql = "Select CreditReferenceseq.nextval from dual";

     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }

         jExec.closeData(key);

         if( id == -1 ) throw new Exception();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("CreditReference Entity create() exception getting CreditReferenceId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
      }

      return new CreditReferencePK(id, copyId);
  }

  /**
   * creates a CreditReference using the CreditReference primary key and a borrower primary key
   *  - the mininimum (ie.non-null) fields
   *
   */

    public CreditReference create(BorrowerPK bpk)throws RemoteException, CreateException
    {

       pk = createPrimaryKey(bpk.getCopyId());

       String sql = "Insert into CreditReference(CreditReferenceId, BorrowerId, copyId, " 
          + " institutionProfileId ) Values ( "  +  pk.getId() + "," + bpk.getId() 
          + "," + pk.getCopyId() + ", " + srk.getExpressState().getDealInstitutionId() + ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("CreditReference Entity - CreditReference - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

       srk.setModified(true);
       return this;
    }


   public CreditReference deepCopy() throws CloneNotSupportedException
   {
      return (CreditReference)this.clone();
   }

   public void setCreditReferenceId(int id)
   {
      this.testChange ("creditReferenceId", id ) ;
      this.creditReferenceId = id;
   }
   public void setCreditRefTypeId(int id)
   {
      this.testChange ("creditRefTypeId", id );
      this.creditRefTypeId = id;
   }

   public void setInstitutionName(String name)
	 {
     this.testChange ("institutionName", name);
		 this.institutionName = name;
  }
   public void setAccountNumber(String  num  )
   {
    this.testChange ("accountNumber", num);
		this.accountNumber = num;
   }
   public void setCurrentBalance(double cb )
	 {
     this.testChange ("currentBalance", cb);
		 this.currentBalance = cb;
   }
   public void setCopyId(int id )
	 {
     this.testChange ("copyId", id );
		 this.copyId = id;
   }

   public void setBorrowerId(int id)
	 {
     this.testChange ("borrowerId", id );
		 this.borrowerId = id;
   }

   public void setCreditReferenceDescription(String desc)
	 {
     this.testChange ("creditReferenceDescription", desc) ;
		 this.creditReferenceDescription = desc;
   }

   public void setTimeWithReference(int time)
	 {
     this.testChange ("timeWithReference", time);
		 this.timeWithReference = time;
   }
   
   public Vector findByMyCopies() throws RemoteException, FinderException
   {
      Vector v = new Vector();

      String sql = "Select * from CreditReference where creditReferenceId = " +
         getCreditReferenceId() + " AND copyId <> " + getCopyId();

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              CreditReference iCpy = new CreditReference(srk);

              iCpy.setPropertiesFromQueryResult(key);
              iCpy.pk = new CreditReferencePK(iCpy.getCreditReferenceId(), iCpy.getCopyId());

              v.addElement(iCpy);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {

          FinderException fe = new FinderException("CreditReference - findByMyCopies exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        return v;
   }

  public boolean isAuditable()
  {
    return true;
  }

  public int getClassId()
  {
    return ClassId.CREDITREFERENCE;
  }

  protected EntityContext getEntityContext() throws RemoteException
  {
    EntityContext ctx = this.entityContext  ;

    try
    {
      ctx.setContextText (  this.creditReferenceDescription  ) ;

      Borrower borrower = new Borrower(srk, null , this.getBorrowerId (), this.getCopyId ( ) ) ;
      if ( borrower == null )
        return ctx;

      ctx.setContextSource (borrower.getNameForEntityContext()) ;

      //Modified by Billy for performance tuning -- By BILLY 28Jan2002
      //Deal deal = new Deal(srk, null , borrower.getDealId (), borrower.getCopyId ( ) ) ;
      //if ( deal == null )
      //  return ctx;
      //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
      Deal deal = new Deal(srk, null);
      String  sc = String.valueOf (deal.getScenarioNumber(borrower.getDealId(), borrower.getCopyId())) +
          deal.getCopyType(borrower.getDealId(), borrower.getCopyId());

      ctx.setScenarioContext  ( sc ) ;

      ctx.setApplicationId ( borrower.getDealId() ) ;

    } catch ( Exception e )
    {
      if ( e instanceof FinderException )
      {
        logger.warn( "CreditReference.getEntityContext Parent Not Found. CreditReferenceId =" + this.creditReferenceId  );
        return ctx;
      }
      throw ( RemoteException ) e ;
    }

    return ctx ;
  } // ------------------ end of getEntityContext() -----------------------------------

  public  boolean equals( Object o )
  {
    if ( o instanceof CreditReference )
      {
        CreditReference oa = ( CreditReference )  o;
        if  ( ( creditReferenceId == oa.getCreditReferenceId () )
          && ( copyId  ==  oa.getCopyId ()  )  )
            return true;
      }
    return false;
  }


    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId
     */
    public void setInstitutionId(int institutionId) {
        this.testChange("institutionProfileId", institutionId);
        this.institutionId = institutionId;
    }
    
}

