/**
 * Title:        MailDestinationType
 * Description:  Class to repersent the MailDestnationType Table
 * Copyright:    Copyright (c) 2002
 * Company:      Basis100
 * @author Billy Lam
 * @version
 *
 * 29/Nov/2007 DVG #DG666 NBC218271: Susie Zanon NBC Express Production - ANO-328 - Too many Email Notifications
 * 03/May/2006 DVG #DG414 #3023  CR 303  funcional spec for the CR 303 (changes to document CR 040 )
 * 23/AUG/2004-DVG #DG60 SCR#538 new implementation for Mail destination
 *  * * @version 1.1  <br>
 * Date: 06/06/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added a case for pec email for service branch in selectdelivermethod()
 */

package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import MosSystem.Mc;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.MailDestinationTypePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import config.Config;

public class MailDestinationType extends DealEntity
{

  protected int            mailDestinationTypeId;
  protected String         mailDestinationTypeDesc;

  private MailDestinationTypePK pk;

  public MailDestinationType(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }

  public MailDestinationType(SessionResourceKit srk, int id) throws RemoteException, FinderException
  {
    super(srk);
    pk = new MailDestinationTypePK(id);
    findByPrimaryKey(pk);
  }

  public MailDestinationType findByPrimaryKey(MailDestinationTypePK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from MailDestinationType " + pk.getWhereClause();
    boolean gotRecord = false;
    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }
        jExec.closeData(key);

        //#DG666 rewritten
        if (!gotRecord ) {
          String msg = "MailDestinationType: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
      	//#DG666 rewritten to allow logging supression
        FinderException fe = new FinderException("MailDestinationType Entity - findByPrimaryKey() exception:"+e);

  			if (gotRecord || !getSilentMode() ) {
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
  			}	          
          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }


  private void setPropertiesFromQueryResult(int key) throws Exception
  {
       setMailDestinationTypeId(jExec.getInt(key,"MAILDESTINATIONTYPEID"));
       setMailDestinationTypeDesc(jExec.getString(key,"MAILDESTINATIONTYPEDESC"));
  }


   /**
  *  gets the value of instance fields as String.
  *  if a field does not exist (or the type is not serviced)** null is returned.
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *  @returns value of bound field as a String;
  *  @param  fieldName as String
  *
  *  Other entity types are not yet serviced   ie. You can't get the Province object
  *  for province.
  */
  public String getStringValue(String fieldName)
  {
      return doGetStringValue(this, this.getClass(), fieldName);
  }

  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  *
  * @param fieldName String
  * @param value String
  * @throws Exception
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }


  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return int the number of updates performed
  * @throws Exception
  */
  protected int performUpdate()  throws Exception
  {
  Class clazz = this.getClass();
  int ret = doPerformUpdate(this, clazz);

  return ret;
  }

  public String getEntityTableName()
  {
    return "MailDestinationType";
  }

  public int getMailDestinationTypeId()
  {
    return this.mailDestinationTypeId;
  }

  public void setMailDestinationTypeId(int id)
  {
    this.testChange ("mailDestinationTypeId", id ) ;
    this.mailDestinationTypeId = id;
  }

  public String getMailDestinationTypeDesc()
  {
    return this.mailDestinationTypeDesc;
  }

  public void setMailDestinationTypeDesc(String desc)
  {
   this.testChange ("mailDestinationTypeDesc", desc ) ;
   this.mailDestinationTypeDesc = desc;
  }

  /**
   *   gets the pk associated with this entity
   *
   *   @return a MailDestinationTypePK
   */
  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }

  public MailDestinationType create(int id, String desc)throws RemoteException, CreateException
  {
    String sql = "Insert into MailDestinationType Values ( " +
     id + ",'" + desc +"')";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      this.trackEntityCreate ();  // track the creation
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("MailDestinationType Entity - MailDestinationType - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);

    if(dcm != null)
        dcm.inputEntity(this);

    return this;
  }

  public MailDestinationType deepCopy() throws CloneNotSupportedException
  {
    return (MailDestinationType)this.clone();
  }

  public int getClassId()
  {
    return ClassId.DEFAULT;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof MailDestinationType )
      {
        MailDestinationType oa = ( MailDestinationType ) o;
        if  (  this.mailDestinationTypeId == oa.getMailDestinationTypeId ()  )
            return true;
      }
    return false;
  }

  //#DG60 SCR#538 new implementation for Mail destination
  /**
   * Get the mail destination list from db and build a list
   * @param srk SessionResourceKit the db connection
   * @param deal Deal the current deal
   * @param dppk DocumentProfilePK the current document profile keys
   * @return List the mail destination list
   */
  public static List getEMailAddressList(SessionResourceKit srk, Deal deal,
                                         DocumentProfilePK dppk) {
    return getDestinatByType(srk, deal, dppk,
                                 Mc.PREF_DELIVERY_METHOD_BY_E_MAIL);
  }

  /**
   * Get the fax destination list from db and build a list
   * @param srk SessionResourceKit the db connection
   * @param deal Deal the current deal
   * @param dppk DocumentProfilePK the current document profile keys
   * @return List the mail destination list
   */
  public static List getFaxNumberList(SessionResourceKit srk, Deal deal,
                                      DocumentProfilePK dppk) {
    return getDestinatByType(srk, deal, dppk,
                                 Mc.PREF_DELIVERY_METHOD_BY_FAX);
  }

  //#DG60 SCR#538 common helper function for email and fax
  private static List getDestinatByType(SessionResourceKit srk, Deal deal,
  		DocumentProfilePK dppk, int deliverType) {
    SysLogger logger = srk.getSysLogger();
    List theList = new ArrayList();
    try
    {
      DocumentProfile theDoc = new DocumentProfile(srk);
      theDoc.findByPrimaryKey(dppk);

      int key;
      JdbcExecutor jExec = srk.getJdbcExecutor();

      String sql = "Select * from documentMailDest where DocumentTypeId = " + theDoc.getDocumentTypeId();
    	//#DG666 rewritten 
      key = jExec.execute(sql);

      // I'd really appreciate a findColumn function !!
      //int liIDXMailDestinationTypeId = lojExec.getResultSetMetaData(key).findColumn("MAILDESTINATIONTYPEID");
      for (; jExec.next(key); ) {
        int mailDestinationTypeId = jExec.getInt(key, "MAILDESTINATIONTYPEID");

        buildDestinationList(srk, deal, theList, mailDestinationTypeId, deliverType);
      }
      jExec.closeData(key);
    }
    catch (Exception e)
    {
    	//#DG666 rewritten 
      FinderException fe = new FinderException("Exception @getEMailAddressList : " +e);
      logger.error(fe.getMessage());
      return null;
    }
    return theList.isEmpty()? null: theList;
  }

  //#DG60 SCR#538 new implementation for Mail destination
  //#DG414 added referral source destination. rewritten / refactored
  /**
   * Select and colect the preferred delivery method
   * @param srk SessionResourceKit the db connection
   * @param deal Deal current deal
   * @param theList List output list
   * @param mailDestinationTypeId int
   * @param deliverType int
   * @throws FinderException
   * @throws RemoteException
   * * @version 1.1  <br>
 * Date: 06/06/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added a case for pec email for service branch <br>
   */
  //Catherine: 23-Mar-06: for 3.1 disclosure: renamed the method to reflect its functionality
  private static void buildDestinationList(SessionResourceKit srk, Deal deal,  
  		List theList, int mailDestinationTypeId,int deliverType) 
  		throws FinderException, RemoteException {

    int userId = -1;
    Contact contact;
    PartyProfile oneProfile;
    int institutionId = deal.getInstitutionProfileId();

    switch (mailDestinationTypeId) {
      case Mc.MAIL_TO_UNDERWRITER:
        userId = deal.getUnderwriterUserId();
        addUserDeliver(srk, theList, deliverType, userId, institutionId);
        break;

      case Mc.MAIL_TO_ADMINISTRATOR:
        userId = deal.getAdministratorId();
        addUserDeliver(srk, theList, deliverType, userId, institutionId);
        break;

      case Mc.MAIL_TO_FUNDER:
        userId = deal.getFunderProfileId();
        addUserDeliver(srk, theList, deliverType, userId, institutionId);
        break;

      case Mc.MAIL_TO_CURRUSER:
        userId = srk.getExpressState().getUserProfileId();
        addUserDeliver(srk, theList, deliverType, userId, institutionId);
        break;

        //DG#60 added solicitor
      case Mc.MAIL_TO_SOLICITOR:
        oneProfile = new PartyProfile(srk);
        oneProfile = oneProfile.findByDealSolicitor(deal.getDealId());
        addPreferdDeliver(theList, deliverType, oneProfile.getContact());
        break;

      case Mc.MAIL_TO_SOB:
        contact = deal.getSourceOfBusinessProfile().getContact();
        addPreferdDeliver(theList, deliverType, contact);
        break;

      case Mc.MAIL_TO_ORIGINATIONBRANCH: {
        oneProfile = new PartyProfile(srk);
        Collection col = oneProfile.findByDealAndType(deal.getDealId(),
            Mc.PARTY_TYPE_ORIGINATION_BRANCH);
        addParties(theList, deliverType, col);
        break;
      }

      case Mc.MAIL_TO_APPRAISER: {
        oneProfile = new PartyProfile(srk);
        Collection col = oneProfile.findByDealAndType(deal.getDealId(),
            Mc.PARTY_TYPE_APPRAISER);
        addParties(theList, deliverType, col);
        break;
        }

//    ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
      case Mc.MAIL_TO_SERVICEBRANCH: {
          oneProfile = new PartyProfile(srk);
          Collection col = oneProfile.findByDealAndType(deal.getDealId(),
              Mc.PARTY_TYPE_SERVICE_BRANCH);
        addParties(theList, deliverType, col);		//#DG666
          break;
        }
//    ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
      case Mc.MAIL_TO_REFSOURCE: {
        oneProfile = new PartyProfile(srk);
        Collection col = oneProfile.findByDealAndType(deal.getDealId(),
            Mc.PARTY_TYPE_REFERRAL_SOURCE);
        addParties(theList, deliverType, col);
        break;
      }

      case Mc.MAIL_TO_DISABLED:
        return;

      default:
        // Wrong MailDestinationType or not specified
        return;
    }
  }

  /**
   * add collection of parties destinations to list
   * @param theList List
   * @param deliverType int
   * @param col Collection
   * @throws RemoteException
   * @throws FinderException
   */
  private static void addParties(List theList, int deliverType,
      Collection col) throws RemoteException, FinderException {
  	
    Iterator it = col.iterator();
    while (it.hasNext()) {
      PartyProfile oneProfile = (PartyProfile) it.next();
      addPreferdDeliver(theList, deliverType, oneProfile.getContact());
    }
  }

  /**
   * add user deliver destination to list
   * @param srk SessionResourceKit
   * @param theList List
   * @param deliverType int
   * @param userId int
   * @throws FinderException
   * @throws RemoteException
   */
  private static void addUserDeliver(SessionResourceKit srk, List theList,
      int deliverType, int userId, int institutionId) throws FinderException, RemoteException {
      UserProfile loUserProfile = new UserProfile(srk);
      loUserProfile.findByPrimaryKey(new UserProfileBeanPK(userId, institutionId));
    addPreferdDeliver(theList, deliverType, loUserProfile.getContact());
  }

  /**
   * get either email or fax from contact
   * @param theList List
   * @param deliverType int
   * @param contact Contact
   */
  private static void addPreferdDeliver(List theList, int deliverType, Contact contact) {
      if (contact.getPreferredDeliveryMethodId() == deliverType) {
      String destina;
        if (deliverType == Mc.PREF_DELIVERY_METHOD_BY_FAX)
        destina = contact.getContactFaxNumber();
        else   //        Mc.PREF_DELIVERY_METHOD_BY_E_MAIL);
        destina = contact.getContactEmailAddress();
      theList.add(destina);
    }
  }

  // Catherine: 23-Mar-06: for 3.1 disclosure: ---- begin ----
  // returns true as a default
  public boolean isDestinationScreen(int documentTypeId){
    boolean gotRecord = false;
    
    try
    {
      int key = 0;
      JdbcExecutor exec = srk.getJdbcExecutor();
      String sql = "SELECT * FROM DOCUMENTMAILDEST WHERE DOCUMENTTYPEID = " + documentTypeId + 
        " AND MAILDESTINATIONTYPEID = " + Mc.MAIL_DISPLAY_ON_SCREEN; 
    	//#DG666 rewritten 
        key = exec.execute(sql);
      for (; exec.next(key); ) {
        gotRecord = true;
        break;
      }
      exec.closeData(key);
    }
    catch (Exception e)
    {
    	//#DG666 rewritten 
      FinderException fe = new FinderException("Exception @getEMailAddressList : " +e);
      logger.error(fe.getMessage());
      gotRecord = true;
    }
    
    return gotRecord;
  }

}
