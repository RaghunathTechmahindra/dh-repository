package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealFeePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;

public class DealFee extends DealEntity
{
    private static final long serialVersionUID = 1L;

    private final static Logger logger = LoggerFactory.getLogger(DealFee.class);
    
   /**
    *  The ID for the entry in this table and provides for linkage to the deal
    *  as well as for lookup purposes to obtain the applicable description.
    */
   protected int    dealFeeId;
   protected int    copyId;
   /** Reference to Fee Type table for the description for the entry applicable to this deal.*/
   protected int    feeId;
   /** Amount of the fee recorded on this entry.*/
   protected double feeAmount;
   /** Description of the fee recorded on this entry.*/
   protected String feeComments;
   /** Internal field that references the fee entry to the deal record.*/
   protected int    dealId;

   protected boolean payableIndicator;
   protected int     feePaymentMethodId;
   protected int     feeStatusId;
   protected Date    feePaymentDate;
   protected int     payorProfileId;
   protected int     institutionId;

   protected DealFeePK pk;

   public DealFee(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
   {
      super(srk,dcm);
   }

   public DealFee(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
   {
      super(srk,dcm);

      pk = new DealFeePK(id,copyId);

      findByPrimaryKey(pk);
   }

  public DealFee findByPrimaryKey(DealFeePK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from dealFee " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
      }
      catch (Exception e)
      {
        FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

        logger.error(fe.getMessage(), e);
        logger.error("finder sql: " + sql);

        throw fe;
      }

    this.pk = pk;
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }

  public Collection findByDeal(DealPK pk) throws Exception
  {

    Collection found = new ArrayList();

    String sql = "Select * from DealFee " ;
          sql +=  pk.getWhereClause();

    try
    {

      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        DealFee df = new DealFee(srk,dcm);
        df.setPropertiesFromQueryResult(key);
        df.pk = new DealFeePK(df.getDealFeeId(),df.getCopyId());

        found.add(df);
      }
      jExec.closeData(key);

    }
    catch (Exception e)
    {
      Exception fe = new Exception("Exception caught while getting Deal::DealFees");
      logger.error(fe.getMessage(), e);

      throw fe;
    }

     return found;
  }


  public Collection findByDealAndTypes(DealPK dpk, int[] types) throws Exception
  {
    Collection found = new ArrayList();

    StringBuffer typebuf  = new StringBuffer(" feeid in( select feeId from fee where feetypeid in( ");
    int end = types.length - 1;

    for(int i = 0; i <= end; i++)
    {
      typebuf.append(String.valueOf(types[i]));

      if(i != end)
       typebuf.append(", ");
      else
       typebuf.append(") ");
    }

    StringBuffer sqlbuf = new StringBuffer("Select * from DealFee where dealId = ");
    sqlbuf.append(dpk.getId()).append(" AND copyId = ").append(dpk.getCopyId());
    sqlbuf.append(" AND ").append(typebuf).append(")");

    String sql = sqlbuf.toString();

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        DealFee df = new DealFee(srk,dcm);
        df.setPropertiesFromQueryResult(key);
        df.pk = new DealFeePK(df.getDealFeeId(),df.getCopyId());

        found.add(df);
      }

      jExec.closeData(key);


    }
    catch (Exception e)
    {
        Exception fe = new Exception("DealFee Entity - findByDealAndType() exception");

        logger.error(fe.getMessage(), e);
        logger.error("finder sql: " + sql);

        throw fe;
    }

    return found;
  }


  public Collection<DealFee> findByDealAndType(DealPK dpk, int feeType) throws Exception
  {
    Collection<DealFee> found = new ArrayList<DealFee>();

    StringBuffer sqlbuf = new StringBuffer("Select * from DealFee where dealId = ");
    sqlbuf.append(dpk.getId()).append(" AND copyId = ").append(dpk.getCopyId());
    sqlbuf.append(" AND feeid in( select feeId from fee where feetypeid = ");
    sqlbuf.append(feeType).append(")");

    String sql = sqlbuf.toString();

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        DealFee df = new DealFee(srk,dcm);
        df.setPropertiesFromQueryResult(key);
        df.pk = new DealFeePK(df.getDealFeeId(),df.getCopyId());

        found.add(df);
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
        Exception fe = new Exception("DealFee Entity - findByDealAndType() exception");

        logger.error(fe.getMessage(), e);
        logger.error("finder sql: " + sql);

        throw fe;
    }

    return found;
  }

  public DealFee findFirstByDealAndType(DealPK dpk, int feeType) throws Exception
  {

    StringBuffer sqlbuf = new StringBuffer("Select * from DealFee where dealId = ");
    sqlbuf.append(dpk.getId()).append(" AND copyId = ").append(dpk.getCopyId());
    sqlbuf.append(" AND feeid in( select feeId from fee where feetypeid = ");
    sqlbuf.append(feeType).append(")");

    String sql = sqlbuf.toString();

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        setPropertiesFromQueryResult(key);
        gotRecord = true;
        break;
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "DealFee Entity: @findFirstByDealAndType(), key= " + dpk + ", entity not found";
        logger.error(msg);
        return null;
      }
    }
    catch (Exception e)
    {
        Exception fe = new Exception("DealFee Entity - findFirstByDealAndType() exception");

        logger.error(fe.getMessage(), e);
        logger.error("finder sql: " + sql);

        throw fe;
    }

    return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception
  {

      this.setDealFeeId(jExec.getInt(key, "DEALFEEID"));
      this.setFeeId(jExec.getInt(key, "FEEID"));
      this.setFeeAmount(jExec.getDouble(key, "FEEAMOUNT"));
      this.setFeeComments(jExec.getString(key, "FEECOMMENTS"));
      this.setDealId(jExec.getInt(key, "DEALID"));
      this.setPayableIndicator(jExec.getString(key, "PAYABLEINDICATOR"));
      this.setFeePaymentMethodId(jExec.getInt(key, "FEEPAYMENTMETHODID"));
      this.setFeeStatusId(jExec.getInt(key, "FEESTATUSID"));
      this.setFeePaymentDate(jExec.getDate(key, "FEEPAYMENTDATE"));
      this.setPayorProfileId(jExec.getInt(key, "PAYORPROFILEID"));
      this.setCopyId(jExec.getInt(key, "COPYID"));
      this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));

      this.pk = new DealFeePK(getDealFeeId(),getCopyId());

  }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

    /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }

  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();

    int ret = doPerformUpdate(this, clazz);

    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;
  }


  public String getEntityTableName()
  {
   return "DealFee";
  }

  public Fee getFee()throws RemoteException, FinderException
  {
    return new Fee(srk,dcm,this.feeId);
  }


  /**
  *   gets the feeAmount associated with this entity
  *
  *   @return a double
  */
  public double getFeeAmount()
  {
    return this.feeAmount ;
  }

		/**
     *   gets the dealFeeId associated with this entity
     *
     *   @return a int
     */
    public int getDealFeeId()
   {
      return this.dealFeeId ;
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a DealFeePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

		/**
     *   gets the dealId associated with this entity
     *
     *   @return a int
     */
    public int getDealId()
   {
      return this.dealId ;
   }

		/**
     *   gets the feeComments associated with this entity
     *
     *   @return a String
     */
   public String getFeeComments()
   {
      return this.feeComments ;
   }


   public int getFeeId()
	 {
     return feeId;
   }

   public int getCopyId()
	 {
     return this.copyId;
   }
   public void setCopyId(int value)
   {
      this.testChange ("copyId", value);
      this.copyId = value;
   }


   public boolean getPayableIndicator()
	 {
     return this.payableIndicator;
   }

   public void setPayableIndicator(String value)
   {
      this.testChange ("payableIndicator", value);
      this.payableIndicator = TypeConverter.booleanFrom(value, "N");
   }

   public int getFeePaymentMethodId()
	 {
     return this.feePaymentMethodId;
   }


   public void setFeePaymentMethodId(int value)
   {
      this.testChange ("feePaymentmethodId", value);
      this.feePaymentMethodId = value;
   }

   public int getFeeStatusId()
	 {
     return this.feeStatusId;
   }
   public void setFeeStatusId(int value)
   {
      this.testChange ("feeStatusId", value);
      this.feeStatusId = value;
   }

   public Date getFeePaymentDate()
   {
     return this.feePaymentDate;
   }
   public void setFeePaymentDate(Date value)
   {
      this.testChange ("feePaymentDate", value);
      this.feePaymentDate = value;
   }

   public int getPayorProfileId()
	 {
     return this.payorProfileId;
   }
   public void setPayorProfileId(int value)
   {
      this.testChange ("payorProfileId", value);
      this.payorProfileId = value;
   }







   public DealFee deepCopy() throws CloneNotSupportedException
   {
      return (DealFee)this.clone();
   }

  private DealFeePK createPrimaryKey(int copyId)throws CreateException
  {
       String sql = "Select DealFeeseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           jExec.closeData(key);
           if( id == -1 ) throw new Exception();

        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("DealFee Entity create() exception getting DealFeeId from sequence");
            logger.error(ce.getMessage(), e);
            throw ce;

        }

        return new DealFeePK(id, copyId);
  }

  /**
   * creates an DealFee using the DealFeeId - the mininimum (ie.non-null) fields
   *
   */

  public DealFee create(DealPK dpk)throws RemoteException, CreateException
  {
    //ALERT: temporary setting dealfee default to application fee.
    // Modified to default FeeType to 0 -- Billy 22Nov2001
    pk = createPrimaryKey(dpk.getCopyId());

    String sql = "Insert into DealFee( dealFeeid, dealId, feeId, copyId, institutionProfileId ) Values ( ";
    sql +=  pk.getId() + ", " + dpk.getId() + ", 0, " + pk.getCopyId() + ", "
            + srk.getExpressState().getDealInstitutionId() + ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      this.trackEntityCreate ();
    }
    catch (Exception e)
    {

      CreateException ce = new CreateException("DealFee Entity - DealFee - create() exception");
      logger.error(ce.getMessage(), e);

      throw ce;
    }

    srk.setModified(true);

    if(dcm != null)
      //dcm.inputEntity(this);
      dcm.inputCreatedEntity ( this );

    return this;
  }

  public void setDealFeeId(int id)
	{
     this.testChange ("dealFeeId", id );
		 this.dealFeeId = id;
  }
  public void setDealId(int id)
	{
     this.testChange ("dealId", id );
		 this.dealId = id;
  }
  public void setFeeComments(String ad )
	{
    this.testChange ("feeComments", ad);
		this.feeComments = ad;
  }
  public void setFeeAmount(double av)
	{
     this.testChange ("feeAmount", av);
		 this.feeAmount = av;
  }


  public void setFeeId(int id)
  {
  this.testChange ("feeId", id );
  this.feeId = id;
  }

  //!!!!
  public Vector findByMyCopies() throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from DealFee where borrowerId = " +
       getDealFeeId() + " AND copyId <> " + getCopyId();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            DealFee iCpy = new DealFee(srk,dcm);

            iCpy.setPropertiesFromQueryResult(key);
            iCpy.pk = new DealFeePK(iCpy.getDealFeeId(), iCpy.getCopyId());

            v.addElement(iCpy);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("DealFee - findByMyCopies exception");

          logger.error(fe.getMessage(), e);
          logger.error("finder sql: " + sql);

          throw fe;
      }
    return v;
  }

  public int getClassId()
  {
    return ClassId.DEALFEE;
  }

  public boolean isAuditable()
  {
    return true;
  }



 protected EntityContext getEntityContext() throws RemoteException
 {
  EntityContext ctx = this.entityContext ;

  try
  {
    ctx.setContextText( this.feeComments );
    ctx.setContextSource ( String.valueOf ( this.feeAmount )  );

    //Modified by Billy for performance tuning -- By BILLY 28Jan2002
    //Deal deal = new Deal( srk, null , this.dealId , this.copyId  );
    //if ( deal == null )
    //  return ctx;
    //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
    Deal deal = new Deal(srk, null);
    String  sc = String.valueOf (deal.getScenarioNumber(this.dealId , this.copyId)) +
        deal.getCopyType(this.dealId , this.copyId);
    // ===================================================================================

    ctx.setScenarioContext( sc);

    ctx.setApplicationId ( this.dealId ) ;

  } catch ( Exception e )
  {
    if ( e instanceof FinderException )
    {
      logger.warn( "DealFee.getEntityContext Parent Not Found. DealFeeId=" + this.dealFeeId );
      return ctx;
    }

    throw ( RemoteException ) e;
  }
  return ctx ;
 } // ----------------- End of getEntityContext -----------------------

  public  boolean equals( Object o )
  {
    if ( o instanceof DealFee )
      {
        DealFee oa = ( DealFee )  o;
        if  ( ( dealFeeId  == oa.getDealFeeId () )
          && ( copyId  ==  oa.getCopyId ()  )  )
            return true;
      }
    return false;
  }

  // Method to find all MasterFees (with OffSet fee definited)
  //  -- By Billy 07March2002
  public Collection findAllMasterFees(DealPK dpk, int feeGererationType) throws Exception
  {
    Collection found = new ArrayList();

    String sql = "Select df.* from DealFee df, Fee f where df.dealId = " + dpk.getId() +
      " AND df.copyId = " + dpk.getCopyId() +
      " AND df.feeId = f.feeId " +
      " AND f.offsetingfeetypeid is not null" +
      " AND f.feegenerationtypeid = " + feeGererationType;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        DealFee df = new DealFee(srk,dcm);
        df.setPropertiesFromQueryResult(key);
        df.pk = new DealFeePK(df.getDealFeeId(),df.getCopyId());

        found.add(df);
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
        Exception fe = new Exception("DealFee Entity - findAllMasterFees() exception");

        logger.error(fe.getMessage(), e);
        logger.error("finder sql: " + sql);

        throw fe;
    }

    return found;
  }

  // Method to check and return the OffSetingFeeTypeId
  //  -- Return -1 if no offset fee setup
  //  -- By Billy 08March2002
  public int checkOffSetFeeTypeId() throws Exception
  {
    return checkOffSetFeeTypeId(-1);
  }
  public int checkOffSetFeeTypeId(int feeGererationType) throws Exception
  {
    String sql = "Select mf.offsetingfeetypeid from Fee mf, Fee osf where " +
      " mf.feeId = " + this.feeId +
      " AND mf.offsetingfeetypeid is not null" +
      " AND mf.offsetingfeetypeid = osf.feeId";
    if(feeGererationType >= 0)
      sql += " AND osf.feegenerationtypeid = " + feeGererationType;

    int result = -1;
    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        result = jExec.getInt(key,1);
        break;
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
        Exception fe = new Exception("DealFee Entity - checkOffSetFeeTypeId() exception");
        logger.error(fe.getMessage(), e);
        logger.error("finder sql: " + sql);

        throw fe;
    }
    return result;
  }

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId
     *            the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
        testChange("institutionProfileId", institutionId);
        this.institutionId = institutionId;
    }
    
    public int removeMIFees(int dealId) throws RemoveException  {
        
        String sql = "DELETE FROM DEALFEE WHERE FEEID IN (SELECT FEEID FROM FEE WHERE MITYPEFLAG='Y') AND DEALID = " + dealId;
        int result = 0;
        try {
             
            result = jExec.executeUpdate(sql);

        } catch (Exception e) {
            RemoveException re = new RemoveException("DealFee failed to remove MI Fees", e);
            logger.error("failed to execute " + sql, e);
            throw re;
        }
        return result;
    }
}
