/*
 * @(#)QualifyDetail.java May 1, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.QualifyDetailPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
/**
 * Title: QualifyDetail Description: Entity to repersent QualifyDetail
 * @author MCM Implementation Team
 * @version 1.0 Initial Version
 * @version 1.1: 14-JUN-2008 XS_2.13 
 * -Added Methods performUpdate(),getEntityTableName(),getClassId,findByDeal()
 * @version 1.2 XS_1.1 08-Aug-2008 Modified getClassId() method.
 * 
 */
public class QualifyDetail extends DealEntity {

    protected int             institutionProfileId;
    protected int             dealId;
    protected int             amortizationTerm;
    protected int             interestCompoundingId;
    protected double          pAndIPaymentAmountQualify;
    protected double          qualifyGds;
    protected double          qualifyRate;
    protected double          qualifyTds;
    protected int             repaymentTypeId;
    protected QualifyDetailPK pk;

    /**
     * 
     * @param srk
     * @throws RemoteException
     * @throws FinderException
     */
    public QualifyDetail(SessionResourceKit srk) throws RemoteException,
    FinderException {
        super(srk);
    }
    /**
     * 
     * @param srk
     * @param dcm
     * @throws RemoteException
     * @throws FinderException
     */
    public QualifyDetail(SessionResourceKit srk, CalcMonitor dcm)
    throws RemoteException, FinderException {
        super(srk, dcm);
    }
    /**
     * 
     * @param srk
     * @param id
     * @param copyId
     * @throws RemoteException
     * @throws FinderException
     */
    public QualifyDetail(SessionResourceKit srk, int id) throws RemoteException, FinderException
    {
        super(srk);

        pk = new QualifyDetailPK(id);

        findByPrimaryKey(pk);
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        this.setDealId(jExec.getInt(key, "DEALID"));
        this.setAmortizationTerm(jExec.getInt(key, "AMORTIZATIONTERM"));
        this.setInterestCompoundingId(jExec.getInt(key, "INTERESTCOMPOUNDINGID"));
        this.setPAndIPaymentAmountQualify(jExec.getDouble(key, "PANDIPAYMENTAMOUNTQUALIFY"));
        this.setQualifyGds(jExec.getDouble(key, "QUALIFYGDS"));
        this.setQualifyRate(jExec.getDouble(key, "QUALIFYRATE"));
        this.setQualifyTds(jExec.getDouble(key, "QUALIFYTDS"));
        this.setRepaymentTypeId(jExec.getInt(key, "REPAYMENTTYPEID"));
    }


    /**
     * gets the pk associated with this entity
     * 
     * @return a PropertyPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * @return the TableName
     */
    public String getEntityTableName() {

        return "QualifyDetail";
    }
    /**
     * <p>
     * Description: Returns the class id of the ComponentMortgage entity
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 08-Aug-2008 XS_1.1 Modified ClassId from 'PROPERTY' to 'QUALIFYDETAIL'
     * @return int - classid of QualifyDetail entity.
     * 
     */
    public int getClassId() {

        return ClassId.QUALIFYDETAIL;
    }
    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0) {
            dcm.inputEntity(this);
        }

        return ret;
    }
    /**
     * 
     * @param pk
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public Collection findByDeal(DealPK pk) throws Exception {

        Collection qualifyDetail = new Vector();

        String sql = "Select * from QualifyDetail where dealId = ";

        sql += pk.getId();
        sql += " order by DealId";

        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                QualifyDetail qd= new QualifyDetail(srk, dcm);

                qd.setPropertiesFromQueryResult(key);
                qd.pk = new QualifyDetailPK(qd.getDealId());

                qualifyDetail.add(qd);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
            "Exception caught while fetching Deal::QualifyDetail ");

            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return qualifyDetail;
    }

    /**
     * 
     * @param pk
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public QualifyDetail findByPrimaryKey(QualifyDetailPK pk)
    throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from QualifyDetail where dealId = "
            + pk.getId();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "QualifyDetail Entity: @findByPrimaryKey(), key= "
                    + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
            "QualifyDetail Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * 
     * @param dealId
     * @return
     */
    private QualifyDetailPK createPrimaryKey(int dealId){
        return new QualifyDetailPK(dealId);
    }

    /**
     * 
     * @param dealId
     * @param interestCompoundingId
     * @param repaymentTypeId
     * @return
     * @throws RemoteException
     * @throws CreateException
     */
    public QualifyDetail create(int dealId, int interestCompoundingId,
            int repaymentTypeId) throws RemoteException, CreateException {

        pk = createPrimaryKey(dealId);

        //SQL
        String sql = "Insert into QualifyDetail( dealId, institutionProfileId, interestCompoundingId, repaymentTypeId) Values ( "
            + dealId + ","+ srk.getExpressState().getDealInstitutionId() 
            + "," + interestCompoundingId+ "," + repaymentTypeId+ ")";

        //Execute
        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate (); // track the create
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("QualifyDetail Entity - Component - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        return this;
    }



    /**
     * @return the institutionProfileId
     */
    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    /**
     * @param institutionProfileId the institutionProfileId to set
     */
    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange ("institutionProfileId", institutionProfileId );
        this.institutionProfileId = institutionProfileId;
    }
    /**
     * @return the dealId
     */
    public int getDealId() {
        return dealId;
    }
    /**
     * @param dealId the dealId to set
     */
    public void setDealId(int dealId) {
        this.testChange ("dealId", dealId );
        this.dealId = dealId;
    }
    /**
     * @return the amortizationTerm
     */
    public int getAmortizationTerm() {
        return amortizationTerm;
    }
    /**
     * @param amortizationTerm the amortizationTerm to set
     */
    public void setAmortizationTerm(int amortizationTerm) {
        this.testChange ("amortizationTerm", amortizationTerm );
        this.amortizationTerm = amortizationTerm;
    }
    /**
     * @return the interestCompoundingId
     */
    public int getInterestCompoundingId() {
        return interestCompoundingId;
    }
    /**
     * @param interestCompoundingId the interestCompoundingId to set
     */
    public void setInterestCompoundingId(int interestCompoundingId) {
        this.testChange ("interestCompoundingId", interestCompoundingId );
        this.interestCompoundingId = interestCompoundingId;
    }
    /**
     * @return the pAndIPaymentAmountQualify
     */
    public double getPAndIPaymentAmountQualify() {
        return pAndIPaymentAmountQualify;
    }
    /**
     * @param andIPaymentAmountQualify the pAndIPaymentAmountQualify to set
     */
    public void setPAndIPaymentAmountQualify(double andIPaymentAmountQualify) {
        this.testChange ("pAndIPaymentAmountQualify", andIPaymentAmountQualify );
        pAndIPaymentAmountQualify = andIPaymentAmountQualify;
    }
    /**
     * @return the qualifyGds
     */
    public double getQualifyGds() {
        return qualifyGds;
    }
    /**
     * @param qualifyGds the qualifyGds to set
     */
    public void setQualifyGds(double qualifyGds) {
        this.testChange ("qualifyGds", qualifyGds );
        this.qualifyGds = qualifyGds;
    }
    /**
     * @return the qualifyRate
     */
    public double getQualifyRate() {
        return qualifyRate;
    }
    /**
     * @param qualifyRate the qualifyRate to set
     */
    public void setQualifyRate(double qualifyRate) {
        this.testChange ("qualifyRate", qualifyRate );
        this.qualifyRate = qualifyRate;
    }
    /**
     * @return the qualifyTds
     */
    public double getQualifyTds() {
        return qualifyTds;
    }
    /**
     * @param qualifyTds the qualifyTds to set
     */
    public void setQualifyTds(double qualifyTds) {
        this.testChange ("qualifyTds", qualifyTds );
        this.qualifyTds = qualifyTds;
    }
    /**
     * @return the repaymentTypeId
     */
    public int getRepaymentTypeId() {
        return repaymentTypeId;
    }
    /**
     * @param repaymentTypeId the repaymentTypeId to set
     */
    public void setRepaymentTypeId(int repaymentTypeId) {
        this.testChange ("repaymentTypeId", repaymentTypeId );
        this.repaymentTypeId = repaymentTypeId;
    }

}
