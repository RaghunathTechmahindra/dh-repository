
package com.basis100.deal.entity;

/**
 * 19/Apr/2005 DVG #DG190 GE Money AU DATX Equifax upload 02/Mar/2005 DVG #DG150
 * #1015 CR120 - Borrower information not appearing in proper order
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.docprep.gui.DPAlertManager;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.datx.messagebroker.equifax.Address;
import com.filogix.datx.messagebroker.equifax.EquifaxCreditMessageSender;
import com.filogix.datx.messagebroker.equifax.EquifaxRequestBean;
import com.filogix.datx.messagebroker.equifax.EquifaxResponseBean;
import com.filogix.datx.messagebroker.equifax.IEquifaxConstants;

public class Borrower extends DealEntity {
    protected int borrowerId;
    protected int copyId;
    protected int dealId;
    protected int borrowerNumber;
    protected String borrowerFirstName;
    protected String borrowerMiddleInitial;
    protected String borrowerLastName;
    protected String firstTimeBuyer;
    protected Date borrowerBirthDate;
    protected int nsfOccurenceTypeId;
    protected int borrowerGenderId;
    protected int salutationId;
    protected int maritalStatusId;
    protected String socialInsuranceNumber;
    protected String borrowerHomePhoneNumber;
    protected String borrowerWorkPhoneNumber;
    protected String borrowerFaxNumber;
    protected String borrowerEmailAddress;
    protected String clientReferenceNumber;
    protected int numberOfDependents;
    protected int numberOfTimesBankrupt;
    protected int raceId;
    protected int creditScore;
    protected double GDS;
    protected double TDS;
    protected int educationLevelId;
    protected double netWorth;
    //FXP26596, 4.2GR, Oct 21, 2009 - start
    // protected boolean existingClient;
    protected String existingClient;
    //FXP26596, 4.2GR, Oct 21, 2009 - end
    protected String creditSummary;
    protected int languagePreferenceId;
    protected int borrowerGeneralStatusId;
    protected int borrowerTypeId;
    protected int citizenshipTypeId;
    protected String primaryBorrowerFlag;
    protected double totalAssetAmount;
    protected double totalLiabilityAmount;
    protected int paymentHistoryTypeId;
    protected double totalIncomeAmount;
    protected Date dateOfCreditBureauProfile;
    protected Date creditBureauOnFileDate;
    protected String bureauAttachment;
    protected String existingClientComments;
    protected int bankruptcyStatusId;
    protected int age;
    protected boolean staffOfLender;
    protected String staffType;
    protected double totalLiabilityPayments;
    protected double GDS3Year;
    protected double TDS3Year;
    protected String SINCheckDigit;
    protected int creditBureauNameId;
    protected String borrowerWorkPhoneExtension;
    protected String creditBureauSummary;

    protected int insuranceProportionsId;
    protected double lifePercentCoverageReq;
    protected int disabilityStatusId;
    protected int lifeStatusId;
    protected int smokeStatusId;
    protected double disPercentCoverageReq;

    protected String guarantorOtherLoans;

    // transient property - not a physical database column
    protected boolean isGuarantor;
    protected String employeeNumber;
    protected int prefContactMethodId;
    protected int solicitationId;

    protected int institutionProfileId;
    
    //FFATE
    protected int suffixId;
    protected String borrowerCellPhoneNumber;
    protected Date CBAuthorizationDate;
    protected String CBAuthorizationMethod;

    private BorrowerPK pk;

    public Borrower(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException {

        super(srk, dcm);
    }

    public Borrower(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId)
            throws RemoteException, FinderException {

        super(srk, dcm);

        pk = new BorrowerPK(id, copyId);

        findByPrimaryKey(pk);
    }

    //
    // Misc. Business Methods
    //

    public String getNameForEntityContext() {

        if (borrowerLastName == null || borrowerLastName.length() <= 0) {
            return "NameNotSet B" + borrowerId;
        }

        String fstPart = "";

        if (borrowerFirstName != null && borrowerFirstName.length() > 0) {
            fstPart = borrowerFirstName.substring(0, 1) + " ";
        }

        return fstPart + borrowerLastName;
    }

    public String formFullName() {

        if (borrowerLastName == null || borrowerLastName.length() <= 0) {
            return "NameNotSet B" + borrowerId;
        }

        String fullName = borrowerLastName + ", " + borrowerFirstName;

        if (borrowerMiddleInitial != null
                && (!borrowerMiddleInitial.trim().equals(""))) {
            return fullName + " " + borrowerMiddleInitial + ".";
        }

        return fullName;
    }

    public boolean isGuarantor() {

        return isGuarantor;
    }

    //Clement Cheng, add for payload generator
    public String getIsGuarantor()
    {
//       return  TypeConverter.stringFromBoolean(this.isGuarantor,"Y","N") ;
       return  this.isGuarantor ? "Y" : "N";
    }
    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public Object getFieldValue(String fieldName) {

        return doGetFieldValue(this, this.getClass(), fieldName);
    }

    public int getCopyId() {

        return this.copyId;
    }

    // --DJ_LI_CR--start//
    // Changed to int corresponding the BorrowerGender table
    public int getBorrowerGenderId() {

        return this.borrowerGenderId;
    }

    // --DJ_LI_CR--end//

    // --DJ_CR201.2--start//
    public String getGuarantorOtherLoans() {

        return this.guarantorOtherLoans;
    }

    // --DJ_CR201.2--end//

    /**
     * gets the borrowerFirstName associated with this entity
     * 
     * @return a String
     */
    public String getBorrowerFirstName() {

        return this.borrowerFirstName;
    }

    /**
     * gets the TDS associated with this entity
     * 
     * @return a double
     */
    public double getTDS() {

        return this.TDS;
    }

    /**
     * gets the languagePreferenceId associated with this entity
     * 
     * @return a int
     */
    public int getLanguagePreferenceId() {

        return this.languagePreferenceId;
    }

    /**
     * gets the salutationId associated with this entity
     * 
     * @return a int
     */
    public int getSalutationId() {

        return this.salutationId;
    }

    /**
     * gets the citizenshipTypeId associated with this entity
     * 
     * @return a int
     */
    public int getCitizenshipTypeId() {

        return this.citizenshipTypeId;
    }

    /**
     * gets the dealId associated with this entity
     * 
     * @return a int
     */
    public int getDealId() {

        return this.dealId;
    }

    /**
     * gets the primaryBorrowerFlag associated with this entity
     * 
     * @return a String
     */
    public String getPrimaryBorrowerFlag() {

        return this.primaryBorrowerFlag;
    }

    /**
     * gets the creditScore associated with this entity
     * 
     * @return a int
     */
    public int getCreditScore() {

        return this.creditScore;
    }

    /**
     * gets the borrowerWorkPhoneNumber associated with this entity
     * 
     * @return a String
     */
    public String getBorrowerWorkPhoneNumber() {

        return this.borrowerWorkPhoneNumber;
    }

    /**
     * gets the liability associated with this entity
     * 
     * @return a Liability;
     */
    public Collection<Liability> getLiabilities() throws Exception {

        return new Liability(srk, dcm).findByBorrower(this.pk);
    }

    /**
     * gets the maritalStatusId associated with this entity
     * 
     * @return a int
     */
    public int getMaritalStatusId() {

        return this.maritalStatusId;
    }

    public Collection<Asset> getAssets() throws Exception {

        return new Asset(srk, dcm).findByBorrower(this.pk);
    }

    public String getBorrowerMiddleInitial() {

        return this.borrowerMiddleInitial;
    }

    public int getBorrowerGeneralStatusId() {

        return this.borrowerGeneralStatusId;
    }

    public double getNetWorth() {

        return this.netWorth;
    }

    public int getBorrowerNumber() {

        return this.borrowerNumber;
    }

    public Collection<BorrowerAddress> getBorrowerAddresses() throws Exception {

        return new BorrowerAddress(srk).findByBorrower(this.pk);
    }

    // Modified by BILLY to pass the CalcMonitor -- Need to calc if the Income
    // changed or removed
    public Collection<EmploymentHistory> getEmploymentHistories() throws Exception {

        return new EmploymentHistory(srk, dcm).findByBorrower(this.pk);
    }

    public Collection<CreditReference> getCreditReferences() throws Exception {

        return new CreditReference(srk, dcm).findByBorrower(this.pk);
    }

    public double getTotalLiabilityAmount() {

        return this.totalLiabilityAmount;
    }

    // --DJ_LDI_CR--start--//
    public Collection getLifeDisabilityPremiums() throws Exception {

        return new LifeDisabilityPremiums(srk, dcm).findByBorrower(this.pk);
    }

    // --DJ_LDI_CR--end--//

    public AdjudicationApplicant getAdjudicationApplicant() throws Exception {

        return new AdjudicationApplicant(srk, getBorrowerId(), getCopyId());
    }

    public Collection getAdjudicationApplicantsByBorrower() throws Exception {

        return new AdjudicationApplicant(srk).findByBorrower(this.pk);
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a BorrowerPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * gets the numberOfTimesBankrupt associated with this entity
     * 
     * @return a int
     */
    public int getNumberOfTimesBankrupt() {

        return this.numberOfTimesBankrupt;
    }

    public int getNumberOfDependents() {

        return this.numberOfDependents;
    }

    public String getSocialInsuranceNumber() {

        return this.socialInsuranceNumber;
    }

    public int getBorrowerId() {

        return this.borrowerId;
    }

    public String getBorrowerEmailAddress() {

        return this.borrowerEmailAddress;
    }

    public double getTotalAssetAmount() {

        return this.totalAssetAmount;
    }

    /**
     * gets the creditSummary associated with this entity
     * 
     * @return a String
     */
    public String getCreditSummary() {

        return this.creditSummary;
    }

    public int getRaceId() {

        return this.raceId;
    }

    public int getEducationLevelId() {

        return this.educationLevelId;
    }

    public double getGDS() {

        return this.GDS;
    }

    public String getBorrowerLastName() {

        return this.borrowerLastName;
    }

    public String getExistingClient() {

//        return TypeConverter.stringFromBoolean(existingClient, "N", "Y");
        return existingClient;
    }

    public boolean isExistingClient() {

//        return existingClient;
        return "Y".equals(existingClient) ? true : false;
    }

    public String getBorrowerFaxNumber() {

        return this.borrowerFaxNumber;
    }

    public double getTotalIncomeAmount() {

        return this.totalIncomeAmount;
    }

    public String getClientReferenceNumber() {

        return this.clientReferenceNumber;
    }

    public int getNsfOccurenceTypeId() {

        return this.nsfOccurenceTypeId;
    }

    public String getFirstTimeBuyer() {

        return this.firstTimeBuyer;
    }

    public Date getBorrowerBirthDate() {

        return this.borrowerBirthDate;
    }

    public int getPaymentHistoryTypeId() {

        return this.paymentHistoryTypeId;
    }

    public String getBorrowerHomePhoneNumber() {

        return this.borrowerHomePhoneNumber;
    }

    public Collection<Income> getIncomes() throws Exception {

        return new Income(srk, dcm).findByBorrower(this.pk);
    }

    // --DJ_LI_CR--start//
    public int getInsuranceProportionsId() {

        return this.insuranceProportionsId;
    }

    public double getLifePercentCoverageReq() {

        return this.lifePercentCoverageReq;
    }

    public int getDisabilityStatusId() {

        return this.disabilityStatusId;
    }

    public int getLifeStatusId() {

        return this.lifeStatusId;
    }

    public int getSmokeStatusId() {

        return this.smokeStatusId;
    }

    public double getDisPercentCoverageReq() {

        return this.disPercentCoverageReq;
    }

    // --DJ_LI_CR--end//

    public int getBorrowerTypeId() {

        return this.borrowerTypeId;
    }

    public Date getDateOfCreditBureauProfile() {

        return this.dateOfCreditBureauProfile;
    }

    public Date getCreditBureauOnFileDate() {

        return this.creditBureauOnFileDate;
    }

    public String getBureauAttachment() {

        return this.bureauAttachment;
    }

    public String getExistingClientComments() {

        return this.existingClientComments;
    }

    public int getBankruptcyStatusId() {

        return this.bankruptcyStatusId;
    }

    public int getAge() {

        return this.age;
    }

    public boolean getStaffOfLender() {

        return this.staffOfLender;
    }

    public String getStaffType() {

        return this.staffType;
    }

    public double getTotalLiabilityPayments() {

        return this.totalLiabilityPayments;
    }

    // ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    /**
     * gets the list of borrower identifications associated with borrower
     * 
     * @return a Borrower identification list
     */
    public Collection getBorrowerIdentifications() throws Exception {

        return new BorrowerIdentification(srk, dcm).findByBorrower(this.pk);
    }

    // ***** Change by NBC Impl. Team - Version 1.2 - End *****//
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't
        // attach the entity to Calc. again
        if (dcm != null && ret > 0) {
            dcm.inputEntity(this);
        }

        return ret;
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public String getEntityTableName() {

        return "Borrower";
    }

    public List getChildren() throws Exception {

        List children = new ArrayList();
        // do not alter the order of the calls this method:
        // Mercator relies on the order that results!!!!
        // any new calls to addAll should go after the existing calls.
        children.addAll(this.getBorrowerAddresses());
        children.addAll(this.getIncomes());
        children.addAll(this.getAssets());
        children.addAll(this.getLiabilities());
        children.addAll(this.getCreditReferences());
        children.addAll(this.getEmploymentHistories());

        // --DJ_LDI_CR--start--//
        // Add LifeDisabilityPremiums to the Children list if any
        children.addAll(this.getLifeDisabilityPremiums());
        // --DJ_LDI_CR--end--//
        // HJ:START
        children.addAll(this.getBorrowerIdentifications());
        // HJ:END
        // children.addAll(this.getServiceContacts()); // Catherine todo:
        // implement
        // this for Appraisal!
        return children;
    }

    public void setDealId(int id) {

        this.testChange("dealId", id);
        this.dealId = id;
    }

    public void setBorrowerId(int id) {

        this.testChange("borrowerId", id);
        this.borrowerId = id;
    }

    public void setBorrowerNumber(int num) {

        // Compare ( num ) with the previous value
        // put into the changeValueMap only if the values are different
        this.testChange("borrowerNumber", num);
        this.borrowerNumber = num;
    }

    public void setBorrowerFirstName(String name) {

        this.testChange("borrowerFirstName", name);
        this.borrowerFirstName = name;
    }

    public void setBorrowerMiddleInitial(String mi) {

        this.testChange("borrowerMiddleInitial", mi);
        this.borrowerMiddleInitial = mi;
    }

    public void setBorrowerLastName(String name) {

        this.testChange("borrowerLastName", name);
        this.borrowerLastName = name;
    }

    public void setFirstTimeBuyer(String value) {

        this.testChange("firstTimeBuyer", value);
        this.firstTimeBuyer = value;
    }

    public void setBorrowerBirthDate(Date ddmmyyyy) {

        this.testChange("borrowerBirthDate", ddmmyyyy);
        this.borrowerBirthDate = ddmmyyyy;
    }

    public void setNSFOccurenceTypeId(int id) {

        this.testChange("nsfOccurenceTypeId", id);
        this.nsfOccurenceTypeId = id;
    }

    // --DJ_LI_CR--start//
    // Changed to int corresponding the BorrowerGender table.
    public void setBorrowerGenderId(int genderId) {

        this.testChange("borrowerGenderId", genderId);
        this.borrowerGenderId = genderId;
    }

    // --DJ_LI_CR--end//

    // --DJ_CR201.2--start//
    public void setGuarantorOtherLoans(String value) {

        this.testChange("guarantorOtherLoans", value);
        this.guarantorOtherLoans = value;
    }

    // --DJ_CR201.2--end//

    public void setSalutationId(int id) {

        this.testChange("salutationId", id);
        this.salutationId = id;
    }

    public void setMaritalStatusId(int id) {

        this.testChange("maritalStatusId", id);
        this.maritalStatusId = id;
    }

    public void setSocialInsuranceNumber(String sin) {

        this.testChange("socialInsuranceNumber", sin);
        this.socialInsuranceNumber = sin;
    }

    public void setBorrowerHomePhoneNumber(String number) {

        this.testChange("borrowerHomePhoneNumber", number);
        this.borrowerHomePhoneNumber = number;
    }

    // --DJ_LI_CR--start//
    public void setInsuranceProportionsId(int id) {

        this.testChange("insuranceProportionsId", id);
        this.insuranceProportionsId = id;
    }

    public void setLifePercentCoverageReq(double lpcr) {

        this.testChange("lifePercentCoverageReq", lpcr);
        this.lifePercentCoverageReq = lpcr;
    }

    public void setDisabilityStatusId(int id) {

        this.testChange("disabilityStatusId", id);
        this.disabilityStatusId = id;
    }

    public void setLifeStatusId(int id) {

        this.testChange("lifeStatusId", id);
        this.lifeStatusId = id;
    }

    public void setSmokeStatusId(int id) {

        this.testChange("smokeStatusId", id);
        this.smokeStatusId = id;
    }

    public void setDisPercentCoverageReq(double dpcr) {

        this.testChange("disPercentCoverageReq", dpcr);
        this.disPercentCoverageReq = dpcr;
    }

    // --DJ_LI_CR--end//

    public void setBorrowerWorkPhoneNumber(String number) {

        this.testChange("borrowerWorkPhoneNumber", number);
        this.borrowerWorkPhoneNumber = number;
    }

    public void setBorrowerFaxNumber(String number) {

        this.testChange("borrowerFaxNumber", number);
        this.borrowerFaxNumber = number;
    }

    public void setBorrowerEmailAddress(String email) {

        this.testChange("borrowerEmailAddress", email);
        this.borrowerEmailAddress = email;
    }

    public void setClientReferenceNumber(String refNum) {

        this.testChange("clientReferenceNumber", refNum);
        this.clientReferenceNumber = refNum;
    }

    public void setNumberOfDependents(int numdep) {

        this.testChange("numberOfDependents", numdep);
        this.numberOfDependents = numdep;
    }

    public void setNumberOfTimesBankrupt(int num) {

        this.testChange("numberOfTimesBankrupt", num);
        this.numberOfTimesBankrupt = num;
    }

    public void setRaceId(int id) {

        this.testChange("raceId", id);
        this.raceId = id;
    }

    public void setCreditScore(int score) {

        this.testChange("creditScore", score);
        this.creditScore = score;
    }

    public void setGDS(double gds) {

        this.testChange("GDS", gds);
        this.GDS = gds;
    }

    public void setTDS(double tds) {

        this.testChange("TDS", tds);
        this.TDS = tds;
    }

    public void setEducationLevelId(int id) {

        this.testChange("educationLevelId", id);
        this.educationLevelId = id;
    }

    public void setNetWorth(double worth) {

        this.testChange("netWorth", worth);
        this.netWorth = worth;
    }

    public void setExistingClient(String client) {

        this.testChange("existingClient", client);
        //this.existingClient = TypeConverter.booleanFrom(client, "N");
        this.existingClient = client;
    }

    public void setExistingClient(boolean client) {

        this.testChange("existingClient", client);
        //this.existingClient = client;
        this.existingClient = client ? "Y" : "N";
    }

    public void setCreditSummary(String summary) {

        this.testChange("creditSummary", summary);
        this.creditSummary = summary;
    }

    public void setLanguagePreferenceId(int id) {

        this.testChange("languagePreferenceId", id);
        this.languagePreferenceId = id;
    }

    public void setBorrowerGeneralStatusId(int id) {

        this.testChange("borrowerGeneralStatusId", id);
        this.borrowerGeneralStatusId = id;
    }

    public void setBorrowerTypeId(int id) {

        this.testChange("borrowerTypeId", id);
        this.borrowerTypeId = id;
    }

    public void setCitizenshipTypeId(int id) {

        this.testChange("citizenshipTypeId", id);
        this.citizenshipTypeId = id;
    }

    public void setPrimaryBorrowerFlag(String flag) {

        this.testChange("primaryBorrowerFlag", flag);
        this.primaryBorrowerFlag = flag;
    }

    public void setTotalAssetAmount(double assetAmount) {

        this.testChange("totalAssetAmount", assetAmount);
        this.totalAssetAmount = assetAmount;
    }

    public void setTotalLiabilityAmount(double liabilityAmount) {

        this.testChange("totalLiabilityAmount", liabilityAmount);
        this.totalLiabilityAmount = liabilityAmount;
    }

    public void setPaymentHistoryTypeId(int id) {

        this.testChange("paymentHistoryTypeId", id);
        this.paymentHistoryTypeId = id;
    }

    public void setTotalIncomeAmount(double totalIncome) {

        this.testChange("totalIncomeAmount", totalIncome);
        this.totalIncomeAmount = totalIncome;
    }

    public void setDateOfCreditBureauProfile(Date d) {

        this.testChange("dateOfCreditBureauProfile", d);
        this.dateOfCreditBureauProfile = d;
    }

    public void setCreditBureauOnFileDate(Date d) {

        this.testChange("creditBureauOnFileDate", d);
        this.creditBureauOnFileDate = d;

    }

    public void setBureauAttachment(String c) {

        this.testChange("bureauAttachment", c);
        this.bureauAttachment = c;
    }

    public void setExistingClientComments(String cc) {

        this.testChange("existingClientComments", cc);
        this.existingClientComments = cc;
    }

    public void setBankruptcyStatusId(int bs) {

        this.testChange("bankruptcyStatusId", bs);
        this.bankruptcyStatusId = bs;
    }

    public void setStaffType(String st) {

        this.testChange("staffType", st);
        this.staffType = st;
    }

    public void setAge(int a) {

        this.testChange("age", a);
        this.age = a;
    }

    public void setStaffOfLender(String b) {

        this.testChange("staffOfLender", b);
        this.staffOfLender = TypeConverter.booleanFrom(b, "N");
    }

    public void setTotalLiabilityPayments(double tot) {

        this.testChange("totalLiabilityPayments", tot);
        this.totalLiabilityPayments = tot;
    }

    public double getGDS3Year() {

        return this.GDS3Year;
    }

    public void setGDS3Year(double value) {

        this.testChange("GDS3Year", value);
        this.GDS3Year = value;
    }

    public double getTDS3Year() {

        return this.TDS3Year;
    }

    public void setTDS3Year(double value) {

        this.testChange("TDS3Year", value);
        this.TDS3Year = value;
    }

    public String getSINCheckDigit() {

        return this.SINCheckDigit;
    }

    public void setSINCheckDigit(String value) {

        this.testChange("SINCheckDigit", value);
        this.SINCheckDigit = value;
    }

    public int getCreditBureauNameId() {

        return this.creditBureauNameId;
    }

    public void setCreditBureauNameId(int value) {

        this.testChange("creditBureauNameId", value);
        this.creditBureauNameId = value;

    }

    public String getBorrowerWorkPhoneExtension() {

        return this.borrowerWorkPhoneExtension;
    }

    public void setBorrowerWorkPhoneExtension(String value) {

        this.testChange("borrowerWorkPhoneExtension", value);
        this.borrowerWorkPhoneExtension = value;
    }

    // Additional Field -- Billy 09Jan2002
    public String getCreditBureauSummary() {

        return this.creditBureauSummary;
    }

    public void setCreditBureauSummary(String value) {

        this.testChange("creditBureauSummary", value);
        this.creditBureauSummary = value;
    }

    // ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    /**
     * @return Returns the employeeNumber.
     */
    public String getEmployeeNumber() {

        return employeeNumber;
    }

    /**
     * @param employeeNumber
     *            The employeeNumber to set.
     */
    public void setEmployeeNumber(String employeeNumber) {

        this.testChange("employeeNumber", employeeNumber);
        this.employeeNumber = employeeNumber;
    }

    /**
     * @return Returns the prefContactMethodId.
     */
    public int getPrefContactMethodId() {

        return prefContactMethodId;
    }

    /**
     * @param prefContactMethodId
     *            The prefContactMethodId to set.
     */
    public void setPrefContactMethodId(int prefContactMethodId) {

        this.testChange("prefContactMethodId", prefContactMethodId);
        this.prefContactMethodId = prefContactMethodId;
    }

    /**
     * @return Returns the solicitationId.
     */
    public int getSolicitationId() {

        return solicitationId;
    }

    /**
     * @param solicitationId
     *            The solicitationId to set.
     */
    public void setSolicitationId(int solicitationId) {

        this.testChange("solicitationId", solicitationId);
        this.solicitationId = solicitationId;
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
    
    
    public int getSuffixId() {
        return this.suffixId;
    }

    public void setSuffixId(int value) {
        this.testChange("suffixId", value);
        this.suffixId = value;
    }
    
    public String getBorrowerCellPhoneNumber() {
        return this.borrowerCellPhoneNumber;
    }

    public void setBorrowerCellPhoneNumber(String value) {
        this.testChange("borrowerCellPhoneNumber", value);
        this.borrowerCellPhoneNumber = value;
    }
    
    public Date getCbAuthorizationDate() {
        return this.CBAuthorizationDate;
    }

    public void setCbAuthorizationDate(Date value) {
        this.testChange("CBAuthorizationDate", value);
        this.CBAuthorizationDate = value;
    }
    
    public String getCbAuthorizationMethod() {
        return this.CBAuthorizationMethod;
    }

    public void setCbAuthorizationMethod(String value) {
        this.testChange("CBAuthorizationMethod", value);
        this.CBAuthorizationMethod = value;
    }


    // ***** Change by NBC Impl. Team - Version 1.2 - End *****//
    // /////////////////////End of all setters and getters
    // /////////////////////////////////////

    /**
     * <pre>
     *  Finds a Borrower with the primary key = pk
     * 
     *  To obtain a populated instance of Borrower given borrowerId = 5 :
     * 
     *  Borrower b = new Borrower(sessionResourceKit);
     *  b = b.findByPrimaryKey(new BorrowerPk(5));
     * 
     *  To obtain the Assets for the above via a call to the database:
     *  Collection assets = b.getAssets();
     * 
     *  To populate the Borrower and it's child element Collections set DealEntity.deep = true
     *  before the call to findByPrimaryKey(...)
     * </pre>
     */

    public Borrower findByPrimaryKey(BorrowerPK pk) throws RemoteException,
            FinderException {
    	
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from Borrower " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                this.pk = pk;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Borrower Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";

                if (getSilentMode() == false) {
                    logger.error(msg);
                }

                throw new FinderException(msg);
            }
        } catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true) {
                throw (FinderException) e;
            }

            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public Borrower findByAsset(Asset a) throws RemoteException,
            FinderException {

        BorrowerPK pk = new BorrowerPK(a.getBorrowerId(), a.getCopyId());
        return findByPrimaryKey(pk);
    }

    public Borrower findByIncome(Income inc) throws RemoteException,
            FinderException {

        BorrowerPK pk = new BorrowerPK(inc.getBorrowerId(), inc.getCopyId());
        return findByPrimaryKey(pk);
    }

    public Borrower findByLiability(Liability li) throws RemoteException,
            FinderException {

        BorrowerPK pk = new BorrowerPK(li.getBorrowerId(), li.getCopyId());
        return findByPrimaryKey(pk);
    }

    public List findByGenXDupeCheck(String borrowerFirstName,
            String borrowerMiddleInitial, String borrowerLastName,
            Date borrowerBirthDate, Date minApplicationDate) throws Exception {

        List found = new ArrayList();
        // Modified to use table joint instead of IN(...) for better performance
        // -- By BILLY 27Feb2002
        StringBuffer sql = new StringBuffer(
                "Select b.* from Borrower b, deal d where");

        if (borrowerFirstName == null) {
            sql.append(" b.borrowerFirstName is null ");
        } else {
            sql.append(" upper(b.borrowerFirstName) = ").append(
                    sqlStringFrom(borrowerFirstName.toUpperCase()));
        }

        if (borrowerMiddleInitial == null) {
            sql.append(" and b.borrowerMiddleInitial is null ");
        } else {
            sql.append(" and upper(b.borrowerMiddleInitial) = ").append(
                    sqlStringFrom(borrowerMiddleInitial.toUpperCase()));
        }

        if (borrowerLastName == null) {
            sql.append(" and b.borrowerLastName is null ");
        } else {
            sql.append(" and upper(b.borrowerLastName) = ").append(
                    sqlStringFrom(borrowerLastName.toUpperCase()));
        }

        if (borrowerBirthDate == null) {
            sql.append(" and b.borrowerBirthDate is null ");
        } else {
            sql.append(" and b.borrowerBirthDate = ").append(
                    sqlStringFrom(borrowerBirthDate));
        }

        sql
                .append(" AND b.dealId = d.dealid AND ( ( d.copyType = 'G' or d.copyType = 'S' ) and d.scenariorecommended = 'Y') ");
        // sql.append(" AND b.dealId = d.dealid AND d.copyType = 'G' ");

        if (minApplicationDate != null) // if a date is supplied
        {
            sql.append(" AND  d.applicationDate > ").append(
                    sqlStringFrom(minApplicationDate));
        }
        sql.append(" order by b.dealid");

        String sqlString = sql.toString();

        try {
            int key = jExec.execute(sqlString);

            // for (; jExec.next(key); )
            // {
            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                found.add(borrower);
            }
            // }

            jExec.closeData(key);
        } catch (Exception e) {
            e = new Exception(
                    "Borrower Entity - findByDemographicComponents() exception for "
                            + this.getClass().getName());

            logger.error(e.getMessage());
            logger.error("Error in Collection type finder - sql: " + sql);
            logger.error(e);
        }

        return found;
    }

    public List findByDupeCheckCriteria(String borrowerFirstName,
            String borrowerMiddleInitial, String borrowerLastName,
            Date borrowerBirthDate, Date minApplicationDate) throws Exception {

        List found = new ArrayList();
        // Modified to use table joint instead of IN(...) for better performance
        // -- By BILLY 27Feb2002
        StringBuffer sql = new StringBuffer(
                "Select b.* from Borrower b, deal d where");

        if (borrowerFirstName == null) {
            sql.append(" b.borrowerFirstName is null ");
        } else {
            sql.append(" upper(b.borrowerFirstName) = ").append(
                    sqlStringFrom(borrowerFirstName.toUpperCase()));
        }

        if (borrowerMiddleInitial == null) {
            sql.append(" and b.borrowerMiddleInitial is null ");
        } else {
            sql.append(" and upper(b.borrowerMiddleInitial) = ").append(
                    sqlStringFrom(borrowerMiddleInitial.toUpperCase()));
        }

        if (borrowerLastName == null) {
            sql.append(" and b.borrowerLastName is null ");
        } else {
            sql.append(" and upper(b.borrowerLastName) = ").append(
                    sqlStringFrom(borrowerLastName.toUpperCase()));
        }

        if (borrowerBirthDate == null) {
            sql.append(" and b.borrowerBirthDate is null ");
        } else {
            sql.append(" and b.borrowerBirthDate = ").append(
                    sqlStringFrom(borrowerBirthDate));
        }

        sql.append(" AND b.dealId = d.dealid AND d.copyType = 'G' ");

        if (minApplicationDate != null) // if a date is supplied
        {
            sql.append(" AND  d.applicationDate > ").append(
                    sqlStringFrom(minApplicationDate));
        }
        sql.append(" order by b.dealid");

        String sqlString = sql.toString();

        try {
            int key = jExec.execute(sqlString);

            // for (; jExec.next(key); )
            // {
            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                found.add(borrower);
            }
            // }

            jExec.closeData(key);
        } catch (Exception e) {
            e = new Exception(
                    "Borrower Entity - findByDemographicComponents() exception for "
                            + this.getClass().getName());

            logger.error(e.getMessage());
            logger.error("Error in Collection type finder - sql: " + sql);
            logger.error(e);
        }

        return found;
    }

    public Collection<Borrower> findByDeal(DealPK pk) throws Exception {

        Collection borrowers = new ArrayList();

        String sql = "Select * from Borrower ";
        sql += pk.getWhereClause();
        sql += " order by PrimaryBorrowerFlag desc , BORROWERID asc";
        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                borrowers.add(borrower);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while getting Deal::Borrowers");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return borrowers;
    }

    public Collection findByDealAndCurrentRequest(DealPK pk) throws Exception {

        Collection borrowers = new ArrayList();

        String sql = "Select b.* from Borrower b, BorrowerRequestAssoc ba";
        sql += " where ba.requestid in (select max(requestid) from Request";
        sql += " where dealid = " + pk.getId();
        sql += " ) and ba.borrowerid = b.borrowerid";
        sql += " order by PrimaryBorrowerFlag desc";
        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                borrowers.add(borrower);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while getting Deal::Borrowers");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return borrowers;
    }

    public Collection findByDealAndType(DealPK pk, int borrowerTypeId)
            throws Exception {

        Collection borrowers = new ArrayList();

        String sql = "Select * from Borrower ";
        sql += pk.getWhereClause() + " and borrowertypeId = " + borrowerTypeId;
        sql += " order by BorrowerNumber";
        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                borrowers.add(borrower);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while getting Deal::Borrowers");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return borrowers;
    }

    /**
     * returns all borrower records matching the given sin and its check digit
     */
    public Collection findBySocialInsuranceNumber(String sin) throws Exception {

        List found = new ArrayList();
        StringBuffer sql = new StringBuffer("Select * from Borrower where ");
        sql.append("socialInsuranceNumber = \'" + sin + "\'");

        try {

            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                found.add(borrower);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new FinderException(
                    "Borrower Entity - findBySocialInsuranceNumber() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("Error in collection type finder - sql: " + sql);
            logger.error(e);
        }

        return found;
    }

    /**
     * returns all borrower records matching the given sin and its check digit
     */
    public Collection findByGenXDupeCheckSin(String sin, Date minDate)
            throws Exception {

        List found = new ArrayList();

        // first make sure sin is not zero [by definition null or after trimming
        // composed of only '0' characters]
        if (sin == null) {
            return found;
        }

        String tstSin = sin.trim();
        int tstLen = tstSin.length();
        boolean allZeros = true;

        for (int i = 0; i < tstLen && allZeros == true; ++i) {
            if (tstSin.charAt(i) != '0') {
                allZeros = false;
            }
        }

        if (allZeros == true) {
            return found;
        }

        // --- //

        StringBuffer sql = new StringBuffer(
                "Select b.* from Borrower b, Deal d where ");

        // will not return null SIN match
        sql.append("b.socialInsuranceNumber = ").append(sqlStringFrom(sin));

        // sql.append(" AND b.dealId = d.dealid AND d.copyType = 'G' ");
        sql
                .append(" AND b.dealId = d.dealid AND ( ( d.copyType = 'G' or d.copyType = 'S' ) and d.scenariorecommended = 'Y') ");

        if (minDate != null) // if a date is supplied
        {
            sql.append(" AND  d.applicationDate > ").append(
                    sqlStringFrom(minDate));
        }
        sql.append(" order by b.dealid");

        try {

            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                found.add(borrower);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new FinderException(
                    "Borrower Entity - findBySocialInsuranceNumber() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("Error in collection type finder - sql: " + sql);
            logger.error(e);
        }

        return found;
    }

    /**
     * returns all borrower records matching the given sin and its check digit
     */
    public Collection findByDupeCheckSin(String sin, Date minDate)
            throws Exception {

        List found = new ArrayList();

        // first make sure sin is not zero [by definition null or after trimming
        // composed of only '0' characters]
        if (sin == null) {
            return found;
        }

        String tstSin = sin.trim();
        int tstLen = tstSin.length();
        boolean allZeros = true;

        for (int i = 0; i < tstLen && allZeros == true; ++i) {
            if (tstSin.charAt(i) != '0') {
                allZeros = false;
            }
        }

        if (allZeros == true) {
            return found;
        }

        // --- //

        StringBuffer sql = new StringBuffer(
                "Select b.* from Borrower b, Deal d where ");

        // will not return null SIN match
        sql.append("b.socialInsuranceNumber = ").append(sqlStringFrom(sin));

        sql.append(" AND b.dealId = d.dealid AND d.copyType = 'G' ");

        if (minDate != null) // if a date is supplied
        {
            sql.append(" AND  d.applicationDate > ").append(
                    sqlStringFrom(minDate));
        }
        sql.append(" order by b.dealid");

        try {

            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                found.add(borrower);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new FinderException(
                    "Borrower Entity - findBySocialInsuranceNumber() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("Error in collection type finder - sql: " + sql);
            logger.error(e);
        }

        return found;
    }

    public Collection findByAllOnDeal(DealPK dealPk) throws Exception {

        Collection v = new Vector();

        String sql = "Select * from Borrower where dealId  = " + dealPk.getId()
                + " AND copyId = " + dealPk.getCopyId();
        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Borrower iCpy = new Borrower(srk, dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new BorrowerPK(iCpy.getBorrowerId(), iCpy.getCopyId());

                v.add(iCpy);
            }

            jExec.closeData(key);

        } catch (Exception ee) {
            logger.error(ee.getMessage());
            logger.error("Error in collection finder - sql: " + sql);
            logger.error(ee);

            throw ee;
        }
        return v;
    }

    public Borrower findByPrimaryBorrower(int dealId, int copyId)
            throws RemoteException, FinderException {

        String sql = "Select * from Borrower where dealId = " + dealId
                + " AND copyId = " + copyId + " AND primaryBorrowerFlag = 'Y'";

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryBorrower(), dealId= "
                        + dealId + ", copyId= " + copyId + ", entity not found";

                if (getSilentMode() == false) {
                    logger.error(msg);
                }

                throw new FinderException(msg);
            }
        } catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true) {
                throw (FinderException) e;
            }

            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryBorrower() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.pk = new BorrowerPK(borrowerId, copyId);

        return this;
    }
    
    /**
     * <p>findByPrimaryKeyWithInstitutionId</p>
     * this method was created for iWorkQ and mWorkQ page that use user VPN
     * FXP23322
     * 
     * @param pk
     * @param institutionId
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public Borrower findByPrimaryBorrowerWithInstitutionId(int dealId, int copyId, int institutionId) 
        throws RemoteException, FinderException {

        String sql = "Select * from Borrower where dealId = " + dealId
                + " AND copyId = " + copyId + " AND primaryBorrowerFlag = 'Y'"
        	+ " and institutionProfileId = " + institutionId;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);
	
            for (; jExec.next(key);) {
		        gotRecord = true;
		        this.pk = pk;
		        setPropertiesFromQueryResult(key);
		        break; // can only be one record
		    }

            jExec.closeData(key);

	    if (gotRecord == false) {
	        String msg = "Borrower Entity: @findByPrimaryBorrowerWithInstitutionId(), key= " + pk
	                + ", and institutionProfileId = " + institutionId + " entity not found";
	
	        if (getSilentMode() == false) {
	            logger.error(msg);
	        }
	
	        throw new FinderException(msg);
	    }
		} catch (Exception e) {
		    if (gotRecord == false && getSilentMode() == true) {
		        throw (FinderException) e;
		    }
		
		    FinderException fe = new FinderException(
		            "Deal Entity - findByPrimaryKey() exception for "
		                    + this.getClass().getName());
		
		    logger.error(fe.getMessage());
		    logger.error("finder sql: " + sql);
		    logger.error(e);
		
		    throw fe;
		}
	
		return this;
    }
    

    public Borrower findByFirstBorrower(int dealId, int copyId)
            throws RemoteException, FinderException {

        String sql = "Select * from Borrower where dealId = " + dealId
                + " AND copyId = " + copyId + " ORDER BY borrowerId";

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // only want one
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByFirstBorrower(), dealId= "
                        + dealId + "; copyId= " + copyId + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByFirstBorrower() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.pk = new BorrowerPK(borrowerId, copyId);

        return this;
    }

    public Collection findByExistingClient(int dealId, int copyId)
            throws Exception {

        String sql = "Select * from Borrower where dealId = " + dealId
                + " AND copyId = " + copyId + " AND existingClient = 'Y'";

        Collection borrowers = new ArrayList();

        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Borrower borrower = new Borrower(srk, dcm);
                borrower.setPropertiesFromQueryResult(key);
                borrower.pk = new BorrowerPK(borrower.getBorrowerId(), borrower
                        .getCopyId());

                borrowers.add(borrower);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Borrower Entity - findByExistingClient() exception for "
                            + this.getClass().getName());

            logger.error(e.getMessage());
            logger.error(fe);

            throw fe;
        }

        return borrowers;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

        this.setBorrowerId(jExec.getInt(key, "BORROWERID"));
        copyId = jExec.getInt(key, "COPYID");
        this.setDealId(jExec.getInt(key, "DEALID"));

        this.setBorrowerNumber(jExec.getInt(key, "BORROWERNUMBER"));
        this.setBorrowerFirstName(jExec.getString(key, "BORROWERFIRSTNAME"));
        this.setBorrowerMiddleInitial(jExec.getString(key,
                "BORROWERMIDDLEINITIAL"));
        this.setBorrowerLastName(jExec.getString(key, "BORROWERLASTNAME"));
        this.setFirstTimeBuyer(jExec.getString(key, "FIRSTTIMEBUYER"));
        this.setBorrowerBirthDate(jExec.getDate(key, "BORROWERBIRTHDATE"));
        this.setNSFOccurenceTypeId(jExec.getShort(key, "NSFOCCURENCETYPEID"));

        this.setSalutationId(jExec.getInt(key, "SALUTATIONID"));
        this.setMaritalStatusId(jExec.getInt(key, "MARITALSTATUSID"));
        this.setSocialInsuranceNumber(jExec.getString(key,
                "SOCIALINSURANCENUMBER"));
        this.setBorrowerHomePhoneNumber(jExec.getString(key,
                "BORROWERHOMEPHONENUMBER"));
        this.setBorrowerWorkPhoneNumber(jExec.getString(key,
                "BORROWERWORKPHONENUMBER"));
        this.setBorrowerFaxNumber(jExec.getString(key, "BORROWERFAXNUMBER"));
        this.setBorrowerEmailAddress(jExec.getString(key,
                "BORROWEREMAILADDRESS"));
        this.setClientReferenceNumber(jExec.getString(key,
                "CLIENTREFERENCENUMBER"));
        this.setNumberOfDependents(jExec.getShort(key, "NUMBEROFDEPENDENTS"));
        this.setNumberOfTimesBankrupt(jExec.getShort(key,
                "NUMBEROFTIMESBANKRUPT"));

        this.setRaceId(jExec.getInt(key, "RACEID"));
        this.setCreditScore(jExec.getInt(key, "CREDITSCORE"));
        this.setGDS(jExec.getDouble(key, "GDS"));
        this.setTDS(jExec.getDouble(key, "TDS"));
        this.setEducationLevelId(jExec.getInt(key, "EDUCATIONLEVELID"));

        this.setNetWorth(jExec.getDouble(key, "NETWORTH"));
        this.setExistingClient(jExec.getString(key, "EXISTINGCLIENT"));
        this.setCreditSummary(jExec.getString(key, "CREDITSUMMARY"));
        this.setLanguagePreferenceId(jExec.getInt(key, "LANGUAGEPREFERENCEID"));
        this.setBorrowerGeneralStatusId(jExec.getInt(key,
                "BORROWERGENERALSTATUSID"));
        this.setBorrowerTypeId(jExec.getInt(key, "BORROWERTYPEID"));
        this.setCitizenshipTypeId(jExec.getInt(key, "CITIZENSHIPTYPEID"));
        this
                .setPrimaryBorrowerFlag(jExec.getString(key,
                        "PRIMARYBORROWERFLAG"));
        this.setTotalAssetAmount(jExec.getDouble(key, "TOTALASSETAMOUNT"));
        this.setTotalLiabilityAmount(jExec.getDouble(key,
                "TOTALLIABILITYAMOUNT"));
        this.setPaymentHistoryTypeId(jExec.getInt(key, "PAYMENTHISTORYTYPEID"));
        this.setTotalIncomeAmount(jExec.getDouble(key, "TOTALINCOMEAMOUNT"));

        this.setDateOfCreditBureauProfile(jExec.getDate(key,
                "DATEOFCREDITBUREAUPROFILE"));
        this.setCreditBureauOnFileDate(jExec.getDate(key,
                "CREDITBUREAUONFILEDATE"));
        this.setBureauAttachment(jExec.getString(key, "BUREAUATTACHMENT"));
        this.setExistingClientComments(jExec.getString(key,
                "EXISTINGCLIENTCOMMENTS"));
        this.setBankruptcyStatusId(jExec.getInt(key, "BANKRUPTCYSTATUSID"));
        this.setAge(jExec.getInt(key, "AGE"));
        this.setStaffOfLender(jExec.getString(key, "STAFFOFLENDER"));
        this.setStaffType(jExec.getString(key, "STAFFTYPE"));
        this.setTotalLiabilityPayments(jExec.getDouble(key,
                "TOTALLIABILITYPAYMENTS"));
        this.setGDS3Year(jExec.getDouble(key, "GDS3YEAR"));
        this.setTDS3Year(jExec.getDouble(key, "TDS3YEAR"));
        this.setSINCheckDigit(jExec.getString(key, "SINCHECKDIGIT"));
        this.setCreditBureauNameId(jExec.getInt(key, "CREDITBUREAUNAMEID"));
        this.setBorrowerWorkPhoneExtension(jExec.getString(key,
                "BORROWERWORKPHONEEXTENSION"));

        this
                .setCreditBureauSummary(jExec.getString(key,
                        "CREDITBUREAUSUMMARY"));
        this.setBorrowerGenderId(jExec.getInt(key, "BORROWERGENDERID"));
        this.setInsuranceProportionsId(jExec.getInt(key,
                "INSURANCEPROPORTIONSID"));
        this.setLifePercentCoverageReq(jExec.getDouble(key,
                "LIFEPERCENTCOVERAGEREQ"));
        this.setSmokeStatusId(jExec.getInt(key, "SMOKESTATUSID"));
        this.setDisabilityStatusId(jExec.getInt(key, "DISABILITYSTATUSID"));
        this.setLifeStatusId(jExec.getInt(key, "LIFESTATUSID"));
        this.setDisPercentCoverageReq(jExec.getDouble(key,
                "DISPERCENTCOVERAGEREQ"));
        this
                .setGuarantorOtherLoans(jExec.getString(key,
                        "GUARANTOROTHERLOANS"));
        this.setSolicitationId(jExec.getInt(key, "SOLICITATIONID"));
        this.setPrefContactMethodId(jExec.getInt(key, "PREFCONTACTMETHODID"));
        this.setEmployeeNumber(jExec.getString(key, "EMPLOYEENUMBER"));

        // Hard code short cut (to set as constant from mc or sc after
        // VersionManager up...
        if (borrowerTypeId == Mc.BT_GUARANTOR) {
            isGuarantor = true;
        } else {
            isGuarantor = false;
        }
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        
        this.setSuffixId(jExec.getInt(key, "SUFFIXID"));
        this.setBorrowerCellPhoneNumber(jExec.getString(key, "BORROWERCELLPHONENUMBER"));
        this.setCbAuthorizationDate(jExec.getDate(key, "CBAUTHORIZATIONDATE"));
        this.setCbAuthorizationMethod(jExec.getString(key, "CBAUTHORIZATIONMETHOD"));

    }

    private BorrowerPK createPrimaryKey(int copyId) throws CreateException {

        String sql = "Select Borrowerseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "Borrower Entity create() exception getting BorrowerId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        return new BorrowerPK(id, copyId);
    }

    /**
     * creates a Borrower using the BorrowerId with mininimum fields
     * 
     */
    public Borrower create(DealPK dpk) throws RemoteException, CreateException {

        pk = createPrimaryKey(dpk.getCopyId());

        int profileId = srk.getExpressState().getDealInstitutionId();

        String sql = "Insert into Borrower( BorrowerId, DealId, copyId, institutionProfileId ) Values ( "
                + pk.getId()
                + ","
                + dpk.getId()
                + ","
                + pk.getCopyId()
                + ","
                + profileId + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate(); // Track the create
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "Borrower Entity - Borrower - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);

        if (dcm != null) {
            // dcm.inputEntity(this, true );
            dcm.inputCreatedEntity(this);
        }

        return this;
    }

    public Vector findByMyCopies() throws RemoteException, FinderException {

        Collection v = new Vector();

        String sql = "Select * from Borrower where borrowerId = "
                + getBorrowerId() + " AND copyId <> " + getCopyId();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Borrower iCpy = new Borrower(srk, dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new BorrowerPK(iCpy.getBorrowerId(), iCpy.getCopyId());

                v.add(iCpy);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Borrower - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return (Vector) v;
    }

    public int getClassId() {

        return ClassId.BORROWER;
    }

    public boolean isAuditable() {

        return true;
    }

    public boolean isPrimaryBorrower() {

        // #DG150 this field now can be Y,N or C !
        // return TypeConverter.booleanFrom(getPrimaryBorrowerFlag(),"N");
        return TypeConverter.charTypeFrom(getPrimaryBorrowerFlag()) == 'Y';
    }

    protected void removeSons() throws Exception { // Remove all the sons ,

        // Call by ejbRemove

        // Liability, Asset , Income ,
        try {
            Collection liabilities = this.getLiabilities();
            Iterator itLi = liabilities.iterator();

            while (itLi.hasNext()) {
                Liability liability = (Liability) itLi.next();
                if (itLi.hasNext()) {
                    liability.ejbRemove(false);
                } else {
                    liability.ejbRemove(true);
                }
            }// --------- end of all liabilities ---------------

            Collection assets = this.getAssets();
            Iterator itAs = assets.iterator();

            while (itAs.hasNext()) {
                Asset asset = (Asset) itAs.next();
                if (itAs.hasNext()) {
                    asset.ejbRemove(false);
                } else {
                    asset.ejbRemove(true);
                }
            }// -------- end of all Assets -------------

            // employment histories must go before incomes!
            Collection employmentHistories = this.getEmploymentHistories();
            Iterator itEH = employmentHistories.iterator();
            while (itEH.hasNext()) { // Employment History may affect
                // Calcualtion ??? ----------
                EmploymentHistory employmentHistory = (EmploymentHistory) itEH
                        .next();
                employmentHistory.ejbRemove();

            } // -------- end of Employment Histories ----------

            Collection incomes = this.getIncomes();
            Iterator itIn = incomes.iterator();

            while (itIn.hasNext()) {
                Income income = (Income) itIn.next();
                if (itIn.hasNext()) {
                    income.ejbRemove(false);
                } else {
                    income.ejbRemove(true);
                }
            }// --------- end of Incomes ----------

            Collection creditReferences = this.getCreditReferences();
            Iterator itCR = creditReferences.iterator();
            while (itCR.hasNext()) { // --------- Credit References won't
                // affect Calculation !!
                CreditReference creditReference = (CreditReference) itCR.next();
                creditReference.ejbRemove(false);
            } // ---------end of Credit References-----------

            Collection borrowerAddresses = this.getBorrowerAddresses();
            Iterator itBA = borrowerAddresses.iterator();
            while (itBA.hasNext()) { // ----- BorrowerAddress won't affect
                // Calculation -----------
                BorrowerAddress borrowerAddress = (BorrowerAddress) itBA.next();
                borrowerAddress.ejbRemove();
            } // ----------- end of all Borrower Addresses --------------

            // --DJ_LDI_CR--start--//
            // Remove the AppraisalOrder Record if any
            Collection premiums = this.getLifeDisabilityPremiums();
            Iterator itPr = premiums.iterator();

            while (itPr.hasNext()) {
                LifeDisabilityPremiums premium = (LifeDisabilityPremiums) itPr
                        .next();
                if (itPr.hasNext()) {
                    premium.ejbRemove(false);
                } else {
                    premium.ejbRemove(true);
                }
            }// --------- end of all Life Disability Premiums ---------------
            // --DJ_LDI_CR--end--//

            // remove the adjudication applicant
            // the REQUEST is going to do this
            /*
             * AdjudicationApplicant adjApplicant = getAdjudicationApplicants();
             * 
             * if (adjApplicant != null) { adjApplicant.ejbRemove(); }
             */
            // HJ:START
            Collection borrowerIdentifications = this
                    .getBorrowerIdentifications();
            Iterator itBi = borrowerIdentifications.iterator();
            while (itBi.hasNext()) {
                BorrowerIdentification borrowerIdentification = (BorrowerIdentification) itBi
                        .next();
                if (itBi.hasNext()) {

                    borrowerIdentification.ejbRemove(false);
                } else {
                    borrowerIdentification.ejbRemove(true);
                }
            }
            // -------- start of all AdjudicationApplicantRequest-------------
            Collection adjAppReqs = null;
            try {
                adjAppReqs = this.getAdjudicationApplRequestByBorrower();
            } catch (Exception e) {
                // Swallow the exception
                // if this is part of Transactional copy management there wont
                // be
                // any adjudicationApplicantRequest
                // associated with this borrower
                // since removesons of request would have removed the
                // adjudicationapplicantrequests prior to this point

            }
            if (adjAppReqs != null) {
                Iterator itAdjAppReqs = adjAppReqs.iterator();
                while (itAdjAppReqs.hasNext()) {
                    AdjudicationApplicantRequest adjAppReq = (AdjudicationApplicantRequest) itAdjAppReqs
                            .next();
                    if (itAdjAppReqs.hasNext()) {
                        adjAppReq.ejbRemove(false);
                    } else {
                        adjAppReq.ejbRemove(true);
                    }
                }
            }
            // -------- end of all AdjudicationApplicantRequest-------------
            try {
                removeChildAssocs();
            } catch (Exception e) {
                // Swallow the exception
                // if this is part of Transactional copy management there wont
                // be
                // any childAssoc
                // associated with this borrower
                // since removesons of REQUEST would have removed the
                // borrowerrequestassoc

            }

            // HJ:END
            // -------- start of all AdjudicationApplicant -------------
            Collection aapps = this.getAdjudicationApplicantsByBorrower();
            Iterator itAApp = aapps.iterator();
            while (itAApp.hasNext()) {
                AdjudicationApplicant aapp = (AdjudicationApplicant) itAApp
                        .next();
                if (itAApp.hasNext()) {
                    aapp.ejbRemove(false);
                } else {
                    aapp.ejbRemove(true);
                }
            }
            // -------- end of all AdjudicationApplicant -------------
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * getS the AdjudicationApplicantRequests associated with this entity
     * 
     * @return a Collection of AdjudicationApplicantRequests;
     */
    public Collection getAdjudicationApplRequestByBorrower() throws Exception {

        return new AdjudicationApplicantRequest(srk).findByBorrower(this.pk);
    }

    /**
     * remove child assocs.
     */
    public void removeChildAssocs() throws Exception {

        String sql = "DELETE FROM BorrowerRequestAssoc WHERE borrowerid = "
                + getBorrowerId() + " AND copyid = " + getCopyId();

        try {
            jExec.executeUpdate(sql);
        } catch (Exception e) {
            logger.error("Error on remove childAssocs: ");
            throw new Exception("Error on remove childAssocs: ", e);
        }
    }

    public boolean equals(Object o) {

        if (o instanceof Borrower) {
            Borrower oa = (Borrower) o;
            if ((this.borrowerId == oa.getBorrowerId())
                    && (this.copyId == oa.getCopyId())) {
                return true;
            }
        }
        return false;
    }

    protected EntityContext getEntityContext() throws RemoteException { // !!!!!!!!!!!!!!!!!!

        // No
        // necessary
        // using
        // the
        // getMethod,
        // since
        // the

        // entityContext is
        // is a inner class of EntityBase,also EntityBase has a variable
        // contains
        // the
        //
        EntityContext ctx = this.entityContext;

        try {
            ctx.setContextText(PicklistData.getDescription("BorrowerType", this
                    .getBorrowerTypeId()));

            // --Merge--//
            // Bug fixed by BILLY -- 26June2002
            String txtSrc = "";
            if (this.borrowerFirstName != null
                    && this.borrowerFirstName.length() > 0) {
                txtSrc += this.borrowerFirstName.substring(0, 1);
            }
            if (this.borrowerLastName != null
                    && this.borrowerLastName.length() > 0) {
                txtSrc += " " + this.borrowerLastName;
            }
            ctx.setContextSource(txtSrc);
            // ============================================================================

            // Modified by Billy for performance tuning -- By BILLY 28Jan2002
            // Deal deal = new Deal( srk, null , this.dealId , this.copyId );
            // if ( deal == null )
            // return ctx;
            // String sc = String.valueOf ( deal.getScenarioNumber () ) +
            // deal.getCopyType();
            Deal deal = new Deal(srk, null);
            String sc = String.valueOf(deal.getScenarioNumber(this.dealId,
                    this.copyId))
                    + deal.getCopyType(this.dealId, this.copyId);
            // ===================================================================================

            ctx.setScenarioContext(sc);

            ctx.setApplicationId(this.dealId);
        } catch (Exception e) {
            if (e instanceof FinderException) {
                logger
                        .warn("Borrower.getEntityContext Parent Not Found. BorrowerId="
                                + this.borrowerId);
                return ctx;
            }
            throw (RemoteException) e;
        }

        return ctx;

    } // -------------- End of getEntityContext --------------------------

    // #DG190 called from GE Money AU DATX mdfml template retrieve Equifax cb
    public String getEquifaxCreditReportXml() throws Exception {

        String responseXML = null;
        PropertiesCache pc = PropertiesCache.getInstance();
        try {
            // configure request receiver, sender, and authentication parameters
            EquifaxRequestBean requestObj = new EquifaxRequestBean();
            requestObj.setReceivingPartyId(pc.getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                    "com.filogix.datx.equifaxCreditReport.receivingpartyid",
                    null));
            requestObj.setRequestingPartyId(pc.getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                    "com.filogix.datx.equifaxCreditReport.requestingpartyid",
                    null));
            requestObj.setAccountIdentifier(pc.getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                    "com.filogix.datx.equifaxCreditReport.accountid", null));
            requestObj.setAccountPassword(pc.getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                    "com.filogix.datx.equifaxCreditReport.accountpassword",
                    null));
            requestObj
                    .setLocaleCode(languagePreferenceId == Mc.LANGUAGE_PREFERENCE_FRENCH ? EquifaxRequestBean.LANG_FRENCH
                            : EquifaxRequestBean.LANG_ENGLISH);
            requestObj
                    .setCustomerMbrNum(pc
                            .getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                                    "com.filogix.datx.equifaxCreditReport.equifaxCustomerMemberNumber",
                                    null));
            requestObj.setCustomerCustCode(pc.getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                    "com.filogix.datx.equifaxCreditReport.equifaxCustomerCode",
                    null));
            requestObj
                    .setCustomerSecCode(pc
                            .getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                                    "com.filogix.datx.equifaxCreditReport.equifaxCustomerSecurityCode",
                                    null));

            // requestObj.setCustomerAppNum(dealId);
            // only support single (i.e. non-joint) requests because front end
            // UI
            // doesn't
            // permit more than one applicant
            requestObj.setJointReport(false);

            // set first borrower info. This will always occur
            requestObj.setB1BirthDate((new SimpleDateFormat("yyyyMMdd"))
                    .format(borrowerBirthDate));
            requestObj.setB1FirstName(borrowerFirstName);
            requestObj.setB1LastName(borrowerLastName);
            requestObj.setB1MidlName(borrowerMiddleInitial);
            if (socialInsuranceNumber != null) {
                String sin = socialInsuranceNumber.trim();
                if (!(sin.equals("") || sin.regionMatches(0, "0000000000", 0,
                        sin.length()))) {
                    requestObj.setB1SIN(sin);
                }
            }
            // configure borrower address(es)
            ArrayList reqAddressses = new ArrayList();
            for (Iterator addrIt = getBorrowerAddresses().iterator(); addrIt
                    .hasNext();) {
                BorrowerAddress baddr = (BorrowerAddress) addrIt.next();
                Addr addr = baddr.getAddr();
                String postal = StringUtil.concat(addr.getPostalFSA(),
                        addr.getPostalLDU()).toString();
                String prov = BXResources.getPickListDescription(
                		srk.getExpressState().getDealInstitutionId(), "PROVINCESHORTNAME", addr.getProvinceId(),
                        languagePreferenceId);
                Address borrowerAddr = new Address(null,
                        addr.getAddressLine1(), addr.getAddressLine2(), addr
                                .getCity(), prov, postal);
                reqAddressses.add(borrowerAddr);
            }
            requestObj.setAddress(reqAddressses);

            ArrayList outFormatsRequest = new ArrayList();
            outFormatsRequest.add(IEquifaxConstants.FORMAT_XML);
            requestObj.setOutputFormat(outFormatsRequest);

            // generate DatX XML request
            EquifaxCreditMessageSender requestSender = new EquifaxCreditMessageSender();
            String reqXML = requestSender.createRequest(requestObj);
            // logger.info(reqXML);

            // Send XML request to DatX, and return the response
            responseXML = requestSender.execute(reqXML, pc
                    .getUnchangedProperty(srk.getExpressState().getDealInstitutionId(),
                            "com.filogix.datx.equifaxCreditReport.url", null));
            // test only, for it messes up 'getDeclaredFields' responseXML =
            // myexecute(reqXML,
            // pc.getUnchangedProperty("com.filogix.datx.equifaxCreditReport.url",null));

            EquifaxResponseBean responseObj = requestSender
                    .createResponse(responseXML);

            // reqXML = new
            // String(responseObj.getFFF(IEquifaxConstants.FORMAT_XML));
            responseXML = new String(responseObj
                    .getFFF(IEquifaxConstants.FORMAT_XML));
        } catch (Exception ex) {
            String msg = "getEquifaxCreditReportXml: "
                    + StringUtil.stack2string(ex);
            logger.error(msg);
            new DPAlertManager().sendEmailAlert(msg,
                    "getEquifaxCreditReportXml");
            // notifyListeners(new ActionEvent(ex,1,"Handler ERROR"));
            // throw new Exception(msg);
        }
        return responseXML;
    }

}
