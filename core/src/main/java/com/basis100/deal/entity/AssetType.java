package com.basis100.deal.entity;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AssetTypePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class AssetType extends DealEntity
{

  protected int            assetTypeId;
  protected String         assetTypeDescription;
  protected int            netWorthInclusion;

  private AssetTypePK pk;

  public AssetType(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }

  public AssetType(SessionResourceKit srk, int id) throws RemoteException, FinderException
  {
    super(srk);

    pk = new AssetTypePK(id);

    findByPrimaryKey(pk);
  }



  public AssetType findByPrimaryKey(AssetTypePK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from AssetType " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "AssetType: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("AssetType Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception {
		setAssetTypeId(jExec.getInt(key, "ASSETTYPEID"));
		setAssetTypeDescription(jExec.getString(key, "ASSETTYPEDESCRIPTION"));
		setNetWorthInclusion(jExec.getInt(key, "NETWORTHINCLUSION"));

	}

  /**
	 * gets the value of instance fields as String. if a field does not exist
	 * (or the type is not serviced)** null is returned. if the field exists
	 * (and the type is serviced) but the field is null an empty String is
	 * returned.
	 * 
	 * @returns value of bound field as a String;
	 * @param fieldName
	 *            as String
	 * 
	 * Other entity types are not yet serviced ie. You can't get the Province
	 * object for province.
	 */
  public String getStringValue(String fieldName)
  {
      return doGetStringValue(this, this.getClass(), fieldName);
  }

  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  @param  fieldName as String
  *  @param  the value as a String
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }


  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return the number of updates performed
  */
  protected int performUpdate()  throws Exception
  {
  Class clazz = this.getClass();
  int ret = doPerformUpdate(this, clazz);

  return ret;
  }

  public String getEntityTableName()
  {
    return "AssetType"; 
  }



  /**
   *   gets the assetId associated with this entity
   *
   *   @return a int
   */
  public int getAssetTypeId()
  {
    return this.assetTypeId ;
  }


  /**
   *   gets the pk associated with this entity
   *
   *   @return a AssetTypePK
   */
  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }


  public int getNetWorthInclusion()
  {
    return this.netWorthInclusion;
  }

  public String getAssetTypeDescription(){ return this.assetTypeDescription;}

  public AssetType create(int id, String desc, int gdsInc)throws RemoteException, CreateException
  {
    String sql = "Insert into AssetType Values ( " +
     id + ", '" + desc +  "' ," + gdsInc + ")";

    try
    {
      jExec.executeUpdate(sql);
      this.trackEntityCreate ();  // track the creation
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("AssetType Entity - AssetType - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    this.assetTypeId = id;
    this.setAssetTypeDescription(desc);
    this.setNetWorthInclusion(gdsInc);
    srk.setModified(true);
    return this;
  }

  public AssetType deepCopy() throws CloneNotSupportedException
  {
    return (AssetType)this.clone();
  }

  public void setAssetTypeId(int id)
  {
   this.testChange ("assetTypeId", id ) ;
   this.assetTypeId = id;
  }


  public void setNetWorthInclusion(int c)
  {
   this.testChange ("netWorthInclusion", c ) ;
   this.netWorthInclusion = c;
  }


  public void setAssetTypeDescription(String desc)
  {
   this.testChange ("assetTypeDescription", desc ) ;
   this.assetTypeDescription = desc;
  }


  public int getClassId()
  {
    return ClassId.DEFAULT;
  }

 
  public  boolean equals( Object o )
  {
    if ( o instanceof AssetType )
      {
        AssetType oa = ( AssetType ) o;
        if  (  this.assetTypeId == oa.getAssetTypeId ()  )
            return true;
      }
    return false;
  }

}
