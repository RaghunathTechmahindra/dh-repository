package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.CrossSellProfilePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class CrossSellProfile extends DealEntity
{
   protected int     crossSellProfileId;
   protected String  CSDescription;
   protected int     profileStatusId;

   private CrossSellProfilePK pk;

   public CrossSellProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public CrossSellProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new CrossSellProfilePK(id);

      findByPrimaryKey(pk);
   }

   public CrossSellProfile findByPrimaryKey(CrossSellProfilePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from CrossSellProfile where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }
            
          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {

          FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {  
      this.setCrossSellProfileId(jExec.getInt(key, "CROSSSELLPROFILEID"));
      this.setCSDescription(jExec.getString(key, "CSDESCRIPTION"));
      this.setProfileStatusId(jExec.getInt(key, "PROFILESTATUSID"));
   }

  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "CrossSellProfile"; 
   }

	

   public int getProfileStatusId()
   {
      return this.profileStatusId ;
   }


   public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }


   public int getCrossSellProfileId()
   {
      return this.crossSellProfileId ;
   }

   public String getCSDescription()
   {
      return this.CSDescription ;
   }


   private CrossSellProfilePK createPrimaryKey()throws CreateException
   {
       String sql = "Select CrossSellProfileSeq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           
           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("CrossSellProfile Entity create() exception getting CrossSellProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

    return new CrossSellProfilePK(id);
  }



  /**
   * creates a CrossSellProfile using the CrossSellProfileId with mininimum fields
   *
   */

  public CrossSellProfile create(CrossSellProfilePK pk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey();

    String sql = "Insert into CrossSellProfile(" + pk.getName() + " ) Values ( " + pk.getId() + ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate();
    }
    catch (Exception e)
    {

      CreateException ce = new CreateException("CrossSellProfile Entity - CrossSellProfile - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);
    return this;
  }

   public void setCrossSellProfileId(int value)
	{
		 this.testChange("crossSellProfileId", value);
		 this.crossSellProfileId = value;
  }

   public void setCSDescription(String value)
	{
		 this.testChange("CSDescription", value);
		 this.CSDescription = value;
  }

  public void setProfileStatusId(int value )
	{
	  this.testChange("profileStatusId", value);
	  this.profileStatusId = value;
  }

 
}
