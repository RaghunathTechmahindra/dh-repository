package com.basis100.deal.entity;

import java.sql.PreparedStatement;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ServiceProductPk;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * @author CRutgaizer
 * @author ESteel
 */
public class ServiceProduct extends DealEntity  {
  
  // main attributes
  protected int     serviceProductId;                                          // surrogate primary key
  
  protected String  serviceProductName;
  protected String  serviceProductVersion;
  protected String  serviceProductDescription;
  protected Date    startDate;
  protected Date    endDate;
  protected Date    disableDate ;
  
  // all foreign keys follow here
  protected int     serviceProviderId;
  protected int     serviceTypeId;
  protected int     serviceSubTypeId;
  protected int     serviceSubSubTypeId;
    protected int         institutionProfileId;

  private ServiceProductPk pk;

  public ServiceProduct(SessionResourceKit srk) throws RemoteException, FinderException {
    super(srk);
  }

  public ServiceProduct(SessionResourceKit srk, int id) throws RemoteException, FinderException {
    super(srk);

    pk = new ServiceProductPk(id);

    findByPrimaryKey(pk);
  }

	private ServiceProductPk createPrimaryKey() throws CreateException
	{
	    String sql = "SELECT SERVICEPRODUCTSEQ.NEXTVAL FROM DUAL";
	    int id = -1;
	
	    try
	    {
	        int key = jExec.execute(sql);
	
	        for (; jExec.next(key);  )
	        {
	            id = jExec.getInt(key,1);  // can only be one record
	        }
	
	        jExec.closeData(key);
	
	        if( id == -1 ) throw new Exception();
	     }
	     catch (Exception e)
	     {
	         CreateException ce = new CreateException("ServiceProduct Entity create() exception getting ServiceProductId from sequence");
	         logger.error(ce.getMessage());
	         logger.error(e);
	         throw ce;
	     }
	
	     return new ServiceProductPk(id);
	}
	
	/**
	 * Test for equality.
	 * @param obj The (ServiceProduct) object to test for equality.
	 */
	public boolean equals(Object obj) {
		if (obj instanceof ServiceProduct) {
	    ServiceProduct otherObj = (ServiceProduct) obj;
	    if  (this.serviceProductId == otherObj.getServiceProductId()) {
	        return true;
	    }
	  }
		return false;  	
	}

	public ServiceProduct findByPrimaryKey(ServiceProductPk pk) throws FinderException {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
	  String sql = "SELECT * FROM SERVICEPRODUCT " + pk.getWhereClause();
	  this.pk = pk;
	  findByQuery(sql);
	  ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	  return this;
	}

	public String getEntityTableName() {
	  return "ServiceProduct";
	}

	public IEntityBeanPK getPk() {
	  return pk;
	}

	protected int performUpdate() throws Exception {
	  Class clazz = this.getClass();
	
	  int ret = doPerformUpdate(this, clazz);
	
	  return ret;
	}

	/**
	 *  Gets the value of instance fields as String.
	 *  If a field does not exist (or the type is not serviced)** null is returned.
	 *  If the field exists (and the type is serviced) but the field is null an empty
	 *  String is returned.
	 *  @returns value of bound field as a String;
	 *  @param  fieldName as String
	 *
	 *  Other entity types are not yet serviced   ie. You can't get the Province object
	 *  for province.
	 */
	public String getStringValue(String fieldName)
	{
	   return doGetStringValue(this, this.getClass(), fieldName);
	}

	private void setPropertiesFromQueryResult(int key) throws Exception {
	  // main attributes
	  this.setServiceProductId(jExec.getInt(key, "SERVICEPRODUCTID"));
	  this.setServiceTypeId(jExec.getInt(key, "SERVICETYPEID"));
	  this.setServiceSubTypeId(jExec.getInt(key, "SERVICESUBTYPEID"));
	  this.setServiceSubSubTypeId(jExec.getInt(key, "SERVICESUBSUBTYPEID"));
	  this.setServiceProviderId(jExec.getInt(key, "SERVICEPROVIDERID"));
	  this.setServiceProductName(jExec.getString(key, "SERVICEPRODUCTNAME"));
	  this.setServiceProductDescription(jExec.getString(key, "SERVICEPRODUCTDESC"));
	  this.setServiceProductVersion(jExec.getString(key, "SERVICEPRODUCTVERSION"));
	  this.setStartDate(jExec.getDate(key, "STARTDATE"));
	  this.setEndDate(jExec.getDate(key, "ENDDATE"));
	  this.setDisableDate(jExec.getDate(key, "DISABLEDATE"));    
      this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
	}

	private ServiceProduct findByQuery(String sql) throws FinderException {
    boolean gotRecord = false;

    try {
      int key = jExec.execute(sql);

      for (; jExec.next(key);) {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "ServiceProduct Entity: @findByQuery(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("ServiceProduct Entity - findByQuery() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
    this.pk = pk;

    return this;
  }
  
  public ServiceProduct findByProviderAndTypeAndSubType(int providerId, int serviceTypeId, 
                                                        int serviceSubTypeId, boolean isSubType) 
    throws FinderException 
  {
    StringBuffer sqlB = new StringBuffer( 
                    "SELECT * " +
                    " FROM SERVICEPRODUCT " + 
                    " WHERE serviceProviderId = " + providerId + 
                    " AND serviceTypeId = " + serviceTypeId
                 );

    sqlB.append(isSubType ? " AND serviceSubTypeId = " + serviceSubTypeId : "");

    return findByQuery(sqlB.toString());
  }

  // ----------- setters / getters ----------- start
  public Date getDisableDate() {
    return disableDate;
  }

  public void setDisableDate(Date disableDate) {
  	testChange("disableDate", disableDate);
    this.disableDate = disableDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
  	testChange("endDate", endDate);
    this.endDate = endDate;
  }

  public String getServiceProductDescription() {
    return serviceProductDescription;
  }

  public void setServiceProductDescription(String serviceProductDescription) {
  	testChange("serviceProductDesc", serviceProductDescription);
    this.serviceProductDescription = serviceProductDescription;
  }

  public int getServiceProductId() {
    return serviceProductId;
  }

  public void setServiceProductId(int serviceProductId) {
  	testChange("serviceProductId", serviceProductId);
    this.serviceProductId = serviceProductId;
  }

  public String getServiceProductName() {
    return serviceProductName;
  }

  public void setServiceProductName(String serviceProductName) {
  	testChange("serviceProductName", serviceProductName);
    this.serviceProductName = serviceProductName;
  }

  public String getServiceProductVersion() {
    return serviceProductVersion;
  }

  public void setServiceProductVersion(String serviceProductVersion) {
  	testChange("serviceProductVersion", serviceProductVersion);
    this.serviceProductVersion = serviceProductVersion;
  }

  public int getServiceSubTypeId() {
    return serviceSubTypeId;
  }

  public void setServiceSubTypeId(int serviceSubTypeId) {
  	testChange("serviceSubTypeId", serviceSubTypeId);
    this.serviceSubTypeId = serviceSubTypeId;
  }

  public int getServiceTypeId() {
    return serviceTypeId;
  }

  public void setServiceTypeId(int serviceTypeId) {
  	testChange("serviceTypeId", serviceTypeId);
    this.serviceTypeId = serviceTypeId;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
  	testChange("startDate", startDate);
    this.startDate = startDate;
  }

  public int getServiceProviderId() {
    return serviceProviderId;
  }

  public void setServiceProviderId(int serviceProviderId) {
  	testChange("serviceProviderId", serviceProviderId);
    this.serviceProviderId = serviceProviderId;
  }

	public int getServiceSubSubTypeId() {
		return serviceSubSubTypeId;
	}


	public void setServiceSubSubTypeId(int serviceSubSubTypeId) {
  	testChange("serviceSubSubTypeId", serviceSubSubTypeId);
		this.serviceSubSubTypeId = serviceSubSubTypeId;
	}

  // ----------- setters / getters ----------- end
	
  public int findChannelIdByTransactionType(int providerId, int productId) throws FinderException{
    boolean gotRecord = false;
    int result = 0;
    
    String sql = 
        "SELECT C.CHANNELID " + 
        "  FROM CHANNEL C, " +
        "       SERVICEPRODUCTREQUESTASSOC A " +
        " WHERE C.CHANNELID = A.CHANNELID " +
        "   AND A.SERVICEPRODUCTID = ? " +
        "   AND A.SERVICETRANSACTIONTYPEID = ?";
    
    try {
      PreparedStatement pstmt = jExec.getPreparedStatement(sql);
      pstmt.setInt(1, productId);
      pstmt.setInt(2, providerId);
      
      int key = jExec.executePreparedStatement(pstmt, sql);

      for (; jExec.next(key);) {
        gotRecord = true;
        result = jExec.getInt(key, 0);
        break; // get the first record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "Channel Entity: @findByServiceProductAndTransaction(), providerId = " + providerId + 
          ", productId =" + productId + "entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e1) {
      FinderException fe = new FinderException("Channel Entity - findByQuery() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e1);

      throw fe;
    }
    return result;
  }
  
  public String getServiceSubTypeName() throws FinderException
  {
      boolean gotRecord = false;
      String result = "";
      
      String sql = 
          "SELECT C.SERVICESUBTYPENAME " + 
          "  FROM SERVICESUBTYPE C, " +
          "       SERVICEPRODUCT A " +
          " WHERE C.SERVICESUBTYPEID = A.SERVICESUBTYPEID " +
          "   AND A.SERVICESUBTYPEID = ? ";
      
      try 
      {
        PreparedStatement pstmt = jExec.getPreparedStatement(sql);
        pstmt.setInt(1, serviceSubTypeId);
        int key = jExec.executePreparedStatement(pstmt, sql);

        for (; jExec.next(key);) {
          gotRecord = true;
          result = jExec.getString(key, 1);
          break; // get the first record
        }

        jExec.closeData(key);

        if (gotRecord == false) {
          String msg = "Channel Entity: @getServiceSubTypeName(), serviceSubTypeId = " + serviceSubTypeId + "entity not found"; 
          logger.error(msg);
          throw new FinderException(msg);
        }
      } catch (Exception e1) {
        FinderException fe = new FinderException("ServiceProduct Entity - findByQuery() exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e1);

        throw fe;
      }
      return result;
    }
  public String getServiceSubSubTypeName() throws FinderException
  {
      boolean gotRecord = false;
      String result = "";
      
      String sql = 
          "SELECT C.SERVICESUBSUBTYPENAME " + 
          "  FROM SERVICESUBSUBTYPE C, " +
          "       SERVICEPRODUCT A " +
          " WHERE C.SERVICESUBSUBTYPEID = A.SERVICESUBSUBTYPEID " +
          "   AND A.SERVICESUBSUBTYPEID = ? ";
      
      try 
      {
        PreparedStatement pstmt = jExec.getPreparedStatement(sql);
        pstmt.setInt(1, serviceSubSubTypeId);
        int key = jExec.executePreparedStatement(pstmt, sql);

        for (; jExec.next(key);) {
          gotRecord = true;
          result = jExec.getString(key, 0);
          break; // get the first record
        }

        jExec.closeData(key);

        if (gotRecord == false) {
          String msg = "Channel Entity: @getServiceSubSubTypeName(), serviceSubSubTypeId = " + serviceSubSubTypeId + "entity not found"; 
          logger.error(msg);
          throw new FinderException(msg);
        }
      } catch (Exception e1) {
        FinderException fe = new FinderException("ServiceProduct Entity - getServiceSubSubTypeName() exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e1);

        throw fe;
      }
      return result;
    }
  public String getServiceProviderName() throws FinderException
  {
      boolean gotRecord = false;
      String result = "";
      
      String sql = 
          "SELECT C.SERVICEPROVIDERNAME " + 
          "  FROM SERVICEPROVIDER C, " +
          "       SERVICEPRODUCT A " +
          " WHERE C.SERVICEPROVIDERID = A.SERVICEPROVIDERID " +
          "   AND A.SERVICEPRODUCTID = ? ";
      
      try 
      {
        PreparedStatement pstmt = jExec.getPreparedStatement(sql);
        pstmt.setInt(1, serviceProductId);
        int key = jExec.executePreparedStatement(pstmt, sql);

        for (; jExec.next(key);) {
          gotRecord = true;
          result = jExec.getString(key, 1);
          break; // get the first record
        }

        jExec.closeData(key);

        if (gotRecord == false) {
          String msg = "Channel Entity: @getServiceProviderName(), serviceProviderId = " + serviceProviderId + "emtity not found"; 
          logger.error(msg);
          throw new FinderException(msg);
        }
      } catch (Exception e1) {
        FinderException fe = new FinderException("ServiceProduct Entity - getServiceProviderName() exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e1);

        throw fe;
      }
      return result;
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    
    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }


}
