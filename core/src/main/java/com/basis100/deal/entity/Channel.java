package com.basis100.deal.entity;

import java.sql.PreparedStatement;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ChannelPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * this class will hold info service channel (i.e. name, protocol, ip, port)
 * 
 * @author crutgaizer
 * @author ESteel
 */

public class Channel extends DealEntity {
  
  protected int     channelId;
  
  protected String  protocol;
  protected String  ip;
  protected int     port;
  protected String  password;
  protected String  path;
  
  protected String  userId;
  protected int     channelTypeId;
  protected String  wsServicePort;

  protected int     institutionId;
  
  private ChannelPK pk;

  /**
   * Simple Constructor. Create an empty Channel.
   * @param srk
   * @throws RemoteException
   * @throws FinderException
   */
  public Channel(SessionResourceKit srk) throws RemoteException, FinderException {
    super(srk);
  }

  /**
   * Create a Channel and populate it from the database.
   * @param srk SessionResourceKit to use
   * @param id The Channel's ID.
   * @throws RemoteException
   * @throws FinderException
   */
  public Channel(SessionResourceKit srk, int id) throws RemoteException, FinderException {
    super(srk);

    pk = new ChannelPK(id);

    findByPrimaryKey(pk);
  }
  
  /**
   * @return the Channel's primary key 
   */
  public IEntityBeanPK getPk() {
    return pk;
  }
  
  /**
   * @return Returns the name of the DB Table this class represents.
   */
  public String getEntityTableName() {
    return "Channel";
  }
  
	/**
	 * Tests whether this Channel is equal to the given (Channel)
	 * object.
	 * @param obj The Object to test for equality.
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Channel) {
	    Channel otherObj = (Channel) obj;
	    if  (this.channelId == otherObj.getChannelId()) {
	        return true;
	    }
	  }
		return false;  	
	}
  
  /**
   *  Gets the value of instance fields as String.
   *  If a field does not exist (or the type is not serviced)** null is returned.
   *  If the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

  private Channel findByQuery(String sql) throws FinderException {
    boolean gotRecord = false;

    try {
      int key = jExec.execute(sql);

      for (; jExec.next(key);) {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "Channel Entity: @findByQuery(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("Channel Entity - findByQuery() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }

    return this;
  }
  
  /**
   * Populates this Channel with information identified by the given primary key.
   * @param pk The Primary Key corresponding to required information.
   * @return This, populated with the appropriate information.
   * @throws FinderException
   */
  public Channel findByPrimaryKey(ChannelPK pk) throws FinderException {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM CHANNEL " + pk.getWhereClause();
    this.pk = pk;
    findByQuery(sql);
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }

  /* *
   * Populates this Channel with information identified by the given product ID.
   * @param productId
   * @return This, populated with appropriate information.
   * @throws FinderException
   * /
   * 
   * SERVICEPRODUCT has changed.
  public Channel findByProduct(int productId) throws FinderException {
    String sql = "SELECT channelId FROM SERVICEPRODUCT WHERE productId = " + productId;

    return findByQuery(sql);
  }*/

  private void setPropertiesFromQueryResult(int key) throws Exception {
    this.setChannelId(jExec.getInt(key, "CHANNELID"));
    this.setChannelTypeId(jExec.getInt(key, "CHANNELTYPEID"));
    this.setProtocol(jExec.getString(key, "PROTOCOL"));
    this.setIp(jExec.getString(key, "IP"));
    this.setPort(jExec.getInt(key, "PORT"));
    this.setUserId(jExec.getString(key, "USERID"));
    this.setPassword(jExec.getString(key, "PASSWORD"));
    this.setPath(jExec.getString(key, "PATH"));
    this.setWsServicePort(jExec.getString(key, "WSSERVICEPORT"));
    this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
  }


  /**
   * Performs database update.
   * 
   * @throws Exception
   */
  protected int performUpdate() throws Exception {
    Class clazz = this.getClass();

    int ret = doPerformUpdate(this, clazz);

    return ret;
  }

  // ----------- setters / getters ----------- start
  public int getChannelId() {
    return channelId;
  }

  public void setChannelId(int channelId) {
    testChange("channelId", channelId);
    this.channelId = channelId;
  }
  
  public int getChannelTypeId() {
  	return channelTypeId;
  }
  
  public void setChannelTypeId(int channelTypeId) {
    testChange("channelTypeId", channelTypeId);      
  	this.channelTypeId = channelTypeId;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    testChange("ip", ip);
    this.ip = ip;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    testChange("password", password);
    this.password = password;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    testChange("path", path);
    this.path = path;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    testChange("port", port);
    this.port = port;
  }

  public String getProtocol() {
    return protocol;
  }

  public void setProtocol(String protocol) {
    testChange("protocol", protocol);
    this.protocol = protocol;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    testChange("userId", userId);
    this.userId = userId;
  }  // ----------- setters / getters ----------- end
  
  public Channel findByServiceProductAndTransactionType(int productId, int transactionTypeId) throws FinderException{
    boolean gotRecord = false;
    String sql = 
        "SELECT C.* " + 
        "  FROM CHANNEL C, " +
        "       SERVICEPRODUCTREQUESTASSOC A " +
        " WHERE C.CHANNELID = A.CHANNELID " +
        "   AND A.SERVICEPRODUCTID = ? " +
        "   AND A.SERVICETRANSACTIONTYPEID = ?";
    
    try {
      PreparedStatement pstmt = jExec.getPreparedStatement(sql);
      pstmt.setInt(1, productId);
      pstmt.setInt(2, transactionTypeId);
      
      int key = jExec.executePreparedStatement(pstmt, sql);

      for (; jExec.next(key);) {
        gotRecord = true;        
        setPropertiesFromQueryResult(key);
        break; // get the first record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "Channel Entity: @findByServiceProductAndTransaction(), transactionTypeId = " + transactionTypeId + 
          ", productId = " + productId + ". Entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e1) {
      FinderException fe = new FinderException("Channel Entity - findByQuery() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e1);

      throw fe;
    }
    return this;
  }

  public String getWsServicePort() {
    return wsServicePort;
  }

  public void setWsServicePort(String wsServicePort) {
    testChange("wsServicePort", wsServicePort);
    this.wsServicePort = wsServicePort;
  }

  /**
   * @return the institutionId
   */
  public int getInstitutionId() {
    return institutionId;
  }

  /**
   * @param institutionId the institutionId to set
   */
  public void setInstitutionId(int institutionId) {
    this.testChange("institutionProfileId", institutionId);
    this.institutionId = institutionId;
  }
}
