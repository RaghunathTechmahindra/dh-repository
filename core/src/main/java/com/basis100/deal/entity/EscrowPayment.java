package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EscrowPaymentPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class  EscrowPayment extends DealEntity
{
  protected int    escrowPaymentId;           //  NUMBER 13
  protected int    copyId;                    //  NUMBER 4
  protected int    escrowTypeId;              //  NUMBER 2
  protected int    dealId;                    //  NUMBER 13
  protected String escrowPaymentDescription;  // escrowpaymentdescrition VARCHAR2(35)
  protected double escrowPaymentAmount;
  protected int 	instProfileId;

  private EscrowPaymentPK pk;

  public EscrowPayment(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
  {
    super(srk,dcm);
  }

  public EscrowPayment(SessionResourceKit srk,CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
  {
      super(srk,dcm);

      pk = new EscrowPaymentPK(id, copyId);

      findByPrimaryKey(pk);
  }

  /*Added the following constructor which takes in SessionResourceKit object 
   *Change done to incorporate Deal compare for AdminFee
   *Change Date: 27-Jul-06
   **/
  public EscrowPayment(SessionResourceKit srk)throws RemoteException, FinderException
  {
	super(srk);  
  }

  public EscrowPayment create(DealPK dpk)throws RemoteException, CreateException
  {

    pk = createPrimaryKey(dpk.getCopyId());

    String sql = "Insert into EscrowPayment(" + pk.getName() +  " , " + dpk.getName() + ", copyId, INSTITUTIONPROFILEID) Values (" +
     pk.getId() + ", " + dpk.getId() + ", " + pk.getCopyId() + ", " + srk.getExpressState().getDealInstitutionId() + ")";
    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      this.trackEntityCreate (); // Track the creation
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("EscrowPayment Entity - EscrowPayment - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    srk.setModified(true);

    if(dcm != null)
      //dcm.inputEntity(this);
      dcm.inputCreatedEntity ( this );

    return this;

  }

  public EscrowPaymentPK createPrimaryKey(int copyId)throws CreateException
  {
     String sql = "Select escrowpaymentseq.nextval from dual";
     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }
         jExec.closeData(key);
         if( id == -1 ) throw new Exception();

      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("EscrowPayment Entity create() exception getting escrowPaymentId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
      }

      return new EscrowPaymentPK(id, copyId);
  }
   public EscrowPayment findByName(String description) throws RemoteException, FinderException
   {
      return findByName(description, 1);
   }

   public EscrowPayment findByName(String description, int copyId) throws RemoteException, FinderException
   {
      String sql = "Select * from escrowpayment where " +
                    StringUtil.formFieldMatchExpression("escrowpaymentdescription", description, false) +
                    " and copyId = " + copyId;

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
             gotRecord = true;
             setPropertiesFromQueryResult(key);
             this.pk = new EscrowPaymentPK(this.escrowPaymentId, copyId);
             break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByName = " + description;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {

          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }
      return this;
   }


   public EscrowPayment findByPrimaryKey(EscrowPaymentPK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from escrowpayment " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          gotRecord = true;
          setPropertiesFromQueryResult(key);
           break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
      }
      catch (Exception e)
      {
        FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
   }
   public EscrowPayment deepCopy() throws CloneNotSupportedException
   {
      return (EscrowPayment)this.clone();
   }

   public Collection findByDealAndType(DealPK pk, int pType) throws Exception{
      String sql = "Select * from escrowpayment " + pk.getWhereClause() + " AND ESCROWTYPEID = " + pType;

      Collection escrowPayments = new Vector();

       try
       {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            EscrowPayment epay =  new EscrowPayment(srk,dcm);
            epay.setPropertiesFromQueryResult(key);
            epay.pk = new EscrowPaymentPK(epay.getEscrowPaymentId(),epay.getCopyId());
            escrowPayments.add(epay);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {

          Exception fe = new Exception("Exception caught while fetching Deal::escrowPayments");

          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }
        return escrowPayments;
   }

   public Collection<EscrowPayment> findByDeal(DealPK pk) throws Exception
   {
      String sql = "Select * from escrowpayment " + pk.getWhereClause();

      Collection<EscrowPayment> escrowPayments = new Vector<EscrowPayment>();

       try
       {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            EscrowPayment epay =  new EscrowPayment(srk,dcm);
            epay.setPropertiesFromQueryResult(key);
            epay.pk = new EscrowPaymentPK(epay.getEscrowPaymentId(),epay.getCopyId());
            escrowPayments.add(epay);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {

          Exception fe = new Exception("Exception caught while fetching Deal::escrowPayments");

          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }


        return escrowPayments;
    }


//GETTERS & SETTERS
   public int getEscrowPaymentId()
   {
      return this.escrowPaymentId;
   }

   public void setEscrowPaymentId(int escrowPaymentId)
   {
       this.testChange ("escrowPaymentId", escrowPaymentId );
       this.escrowPaymentId =  escrowPaymentId;
   }

   public void setCopyId(int copyId)
   {
     this.testChange ("copyId", copyId );
     this.copyId = copyId;
   }
   public int getCopyId(){return this.copyId;}

   public int getEscrowTypeId()
   {
      return this.escrowTypeId;
   }

   public void setEscrowTypeId(int escrowTypeId)
   {
      this.testChange ("escrowTypeId", escrowTypeId);
      this.escrowTypeId = escrowTypeId;
   }

   public int getDealId()
   {
      return this.dealId;
   }

   public int getClassId()
   {
     return ClassId.ESCROW;
   }

   public void setDealId(int dealId)
   {
      this.testChange ("dealId", dealId);
      this.dealId = dealId;
   }


   public void setEscrowPaymentDescription(String escrowPaymentDescription)
   {
      this.testChange ("escrowPaymentDescription", escrowPaymentDescription );
      this.escrowPaymentDescription = escrowPaymentDescription;
   }
   public String getEscrowPaymentDescription()
   {
      return this.escrowPaymentDescription;
   }

   public void setEscrowPaymentAmount(double escrowPaymentAmount)
   {
      this.testChange ("escrowPaymentAmount", escrowPaymentAmount);
      this.escrowPaymentAmount = escrowPaymentAmount;
   }
   public double getEscrowPaymentAmount()
   {
      return this.escrowPaymentAmount;
   }


   //#DG600 not used - delete after 1/Jan/2009
   /**
    * National Bank Implementation /PP
    * @date 03-Aug-07
    * @author NBC/PP Implementation Team
    * @iteration:4
    * @story Name:Update Deal Compare with Admin Fee
    * @version 1.2
    * @return a double value
    * @param  dealID as int ,copyID as int and pType as int
    * This method is overloaded to take copyid as additional parameter and return ESCROWPAYMENTAMOUNT (Admin Fee) only. 
    */
   // ***** Addition by NBC/PP Implementation Team -version 1.2- Method findByDealIdAndType - Start *****//
   public double findByDealIdAndType(int dealID,int copyID, int pType){
	   double adminFee=0.0;
	   
	   String sql = "Select ESCROWPAYMENTAMOUNT from escrowpayment where DEALID = "+dealID+" AND COPYID = "+copyID+" AND ESCROWTYPEID = "+pType;
	   
	   try{
		   int key =0; 
		
		   if (jExec != null) 
		   {
			  key = jExec.execute(sql);
			   for (; jExec.next(key);  )
		         {
		             adminFee =(double)jExec.getInt(key,1);  // can only be one record
		         }
		         jExec.closeData(key);
		    }
		   else{
			   logger.info(this.getClass().getName() + "jExec is null");
		   }
	   	   }catch (Exception e)
	   	   		{
	   		   		logger.error(e);
	   	   		}
	   
	   return adminFee;
   } 
   // ***** Addition by NBC/PP Implementation Team -version 1.2- Method findByDealIdAndType - End *****//
   
   /**
    * National Bank Implementation /PP
    * @date 03-Aug-07
    * @author NBC/PP Implementation Team
    * @iteration:4
    * @story Name:Update Deal Compare with Admin Fee
    * @version 1.2
    * @return a int value
    * @param  dealID as int ,copyID as int
    * The method getEscrowTypeId is overloaded to take dealID and copyid as  parameter and return ESCROWTYPEID only. 
    */
// ***** Addition by NBC/PP Implementation Team -version 1.2- Method getEscrowTypeId - Start *****//
   public int getEscrowTypeId(int dealID,int copyID){
	   int escrowTypeId = 0;
	   String sql = "Select ESCROWTYPEID from escrowpayment where DEALID = "+dealID+" AND COPYID = "+copyID;
	   try{
		   int key =0; 
		   if (jExec != null) 
		   {
			  key = jExec.execute(sql);
			   for (; jExec.next(key);  )
		         {
				   escrowTypeId =(int)jExec.getInt(key,1);  // can only be one record
		         }
			   
		         jExec.closeData(key);
		    }
		   else{
			   logger.info(this.getClass().getName() + "jExec is null");
		   }
	   	   }catch (Exception e)
	   	   		{
	   		   		logger.error(e);
	   	   		}
	   
	   return escrowTypeId;
   } 
   // ***** Addition by NBC/PP Implementation Team -version 1.2- Method getEscrowTypeId - End *****//
	 //#DG600 end   */

   public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

 /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
   public void setField(String fieldName, String value) throws Exception
  {
      doSetField(this, this.getClass(), fieldName, value);
   }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setEscrowPaymentId(jExec.getInt(key,"ESCROWPAYMENTID"));
      this.copyId = jExec.getInt(key,"COPYID");
      this.setEscrowTypeId( jExec.getInt(key,"ESCROWTYPEID"));
      this.setDealId(jExec.getInt(key,"DEALID"));
      this.setEscrowPaymentDescription(jExec.getString(key,"ESCROWPAYMENTDESCRIPTION"));
      this.setEscrowPaymentAmount(jExec.getDouble(key,"ESCROWPAYMENTAMOUNT"));
      this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }


  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return the number of updates performed
  */
  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();

    int ret = doPerformUpdate(this, clazz);

    //==========================================================================================
    // Special handle for the Calc 107 (EscrowPaymentAmount). Which needed to trigger the calc
    //  by it's target field.  However, it is not supported.  In order to achieve it, we force a
    //  change on the EscrowTypeId which will also triggers the calc.
    //    -- By BILLY 11March2002
    if(this.isValueChange("escrowPaymentAmount") == true)
    {
      // Force the EscrowTypeId to mark as changed
      this.forceChange("escrowTypeId");
    }
    //===========================================================================================

    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;

  }

  public String getEntityTableName()
  {
    return "EscrowPayment";
  }

   public Vector findByMyCopies() throws RemoteException, FinderException
   {
      Vector v = new Vector();

      String sql = "Select * from EscrowPayment where escrowPaymentId = " +
         getEscrowPaymentId() + " AND copyId <> " + getCopyId();

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              EscrowPayment iCpy = new EscrowPayment(srk,dcm);

              iCpy.setPropertiesFromQueryResult(key);
              iCpy.pk = new EscrowPaymentPK(iCpy.getEscrowPaymentId(), iCpy.getCopyId());

              v.addElement(iCpy);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("EscrowPayment - findByMyCopies exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
      return v;
  }

  public boolean isAuditable()
  {
    return true;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof EscrowPayment )
      {
        EscrowPayment oa = ( EscrowPayment  ) o;
        if  ( ( this.escrowPaymentId  == oa.getEscrowPaymentId ()  )
          && ( this.copyId  ==  oa.getCopyId ()  )  )
            return true;
      }
    return false;
  }

  protected EntityContext getEntityContext() throws RemoteException
  {
    EntityContext ctx = this.entityContext  ;

    try
    {
      ctx.setContextText ( this.escrowPaymentDescription  );
      ctx.setContextSource( String.valueOf ( this.escrowPaymentAmount  ))  ;

      //Modified by Billy for performance tuning -- By BILLY 28Jan2002
      //Deal  deal = new Deal ( srk, dcm, this.dealId  , this.copyId ) ;
      //String sc = String.valueOf ( deal.getScenarioNumber ()) + deal.getCopyType () ;
      //if ( deal == null )
      //  return ctx;
      //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
      Deal deal = new Deal(srk, null);
      String  sc = String.valueOf (deal.getScenarioNumber(this.dealId , this.copyId)) +
          deal.getCopyType(this.dealId , this.copyId);
      // ===================================================================================

      ctx.setScenarioContext( sc ) ;

      ctx.setApplicationId ( this.dealId ) ;

    } catch ( Exception e )
    {
      if ( e instanceof FinderException )
      {
        logger.warn( "EscrowPayment.getEntityContext Parent Not Found. EscrowPaymentId =" + this.escrowPaymentId );
        return ctx;
      }
      throw ( RemoteException ) e ;
    } // end of try .. . catch ...
    return ctx ;
  } // --------- End of getEntityContext ------------------
  
    public int getInstProfileId() {
    	return instProfileId;
    }
    
    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
    }
}
