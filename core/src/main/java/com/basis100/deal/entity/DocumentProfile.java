package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


/**
 *
 *  EntityRelated to DocumentProfile Record

 */
public class DocumentProfile extends DealEntity {

   protected String  documentFullName;
   protected String  documentDescription;
   protected String  documentVersion ;
   protected String  documentTemplatePath;
   protected String  documentSchemaPath;
   protected int     languagePreferenceId;
   protected int     lenderProfileId;
   protected int     documentTypeId;
   protected String  documentFormat;
   protected int     documentProfileStatus;
   protected String  replyFlag;
   protected String  digestKey;
   // Addition field to specify Mail Destination -- By BILLY 07Jan2002
   protected int     mailDestinationTypeId;
   protected int     instProfileId;


   protected DocumentProfilePK pk;

   public DocumentProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

 /**
  *  Finds the record identified by the the primary key id contained in the
  *  DocumentProfilePK parameter.
  *  @return this DocumentProfile instance populated with data from the corresponding record.
  *  @throws FinderException if record not found
  */
   public DocumentProfile findByPrimaryKey(DocumentProfilePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from documentProfile " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            logger.error("finderSql: " + sql);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("DocumentProfile Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }


   /**
    * Populates this DocumentProfile instance with data from DB 
    *  
    * @param  key	DB key of the record to be used to populate fields
    * 
    * @throws Exception
    */
   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setDocumentFullName(jExec.getString(key,"DOCUMENTFULLNAME"));
      this.setDocumentDescription(jExec.getString(key,"DOCUMENTDESCRIPTION"));
      this.setDocumentVersion(jExec.getString(key,"DOCUMENTVERSION"));
      this.setDocumentTemplatePath( jExec.getString(key,"DOCUMENTTEMPLATEPATH"));
      this.setDocumentSchemaPath(jExec.getString(key,"DOCUMENTSCHEMAPATH"));
      this.setLanguagePreferenceId(jExec.getInt(key,"LANGUAGEPREFERENCEID"));
      this.setLenderProfileId(jExec.getInt(key,"LENDERPROFILEID"));
      this.setDocumentTypeId(jExec.getInt(key,"DOCUMENTTYPEID"));
      this.setDocumentFormat(jExec.getString(key,"DOCUMENTFORMAT"));
      this.setDocumentProfileStatus(jExec.getInt(key,"DOCUMENTPROFILESTATUS"));
      this.setReplyFlag(jExec.getString(key,"REPLYFLAG"));
      this.setDigestKey(jExec.getString(key,"DIGESTKEY"));
      // Addition field to specify Mail Destination -- By BILLY 07Jan2002
      //this.setMailDestinationTypeId(jExec.getInt(key,"MAILDESTINATIONTYPEID"));
      this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));

   }
   
  /**
  *  Finds the DocumentProfile record identified by the the name String supplied.
  *  @return this DocumentProfile instance populated with data from the corresponding record
  *  or null if no record is found.
  *  records: FinderException if record not found
  */
   public DocumentProfile findByName(String fullname) throws RemoteException
   {

      String sql = "Select * from DocumentProfile where fullname = '" + fullname + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               break;
          }
          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "DocumentProfile Entity: failed to @findByName = " + sql;
            logger.error(msg);
            return null;
          }
      }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
      }
      return this;
   }

   /**
    * Finds the DocumentProfile record identified by the documentTypeId supplied
    * 
    * @param  docTypeId
    * 
    * @return this, populated with data from the corresponding record
    */
   public DocumentProfile findByDocumentTypeId(int docTypeId ) {
	   
       String sql = "Select * from DocumentProfile where documenttypeid = " + Integer.toString(docTypeId);

       boolean gotRecord = false;
       try {
    	   int key = jExec.execute(sql);
       	
       		for (; jExec.next(key); ) {
       			gotRecord = true;
       			setPropertiesFromQueryResult(key);
       			break;
       		}
       	
       		jExec.closeData(key);
       	
       		if (gotRecord == false) {
       			String msg = "DocumentProfile Entity: failed to @findByDocumentTypeId = " + sql;
       			logger.error(msg);
       			return null;
       		}
       }
       catch (Exception e) {
       		logger.error("finder sql: " + sql);
       		logger.error(e);
       		return null;
       }
       return this;
   }
   
   /**
    * Rel 3.1 - Sasha
    * 
    * Finds the DocumentProfile record identified by the documentTypeId supplied
    * 
    * @param  docTypeId
    * @return this, populated with data from the corresponding record
    */
   public DocumentProfile findByDocumentTypeIdAndLanguageId(int docTypeId, int languageId ) {
	   
       String sql = "Select * from DocumentProfile where documenttypeid = " + Integer.toString(docTypeId);
       sql += " and languagepreferenceid = " + Integer.toString(languageId);

       boolean gotRecord = false;
       try {
    	   int key = jExec.execute(sql);
       	
       		for (; jExec.next(key); ) {
       			gotRecord = true;
       			setPropertiesFromQueryResult(key);
       			break;
       		}
       	
       		jExec.closeData(key);
       	
       		if (gotRecord == false) {
       			String msg = "DocumentProfile Entity: failed to @findByDocumentTypeId = " + sql;
       			logger.error(msg);
       			return null;
       		}
       }
       catch (Exception e) {
       		logger.error("finder sql: " + sql);
       		logger.error(e);
       		return null;
       }
       return this;
   }   

   /**
    * Finds the DocumentProfile record identified by the the class name used to create document
    * 
    * @param  className
    * @param  lang
    * 
    * @return this, populated with data from the corresponding record
    * @throws RemoteException
    */
    public DocumentProfile findByClassName(String className, int lang) throws RemoteException {
    	 
    	String sql = "Select * from DocumentProfile where documentschemapath = '" + className + "'";
        sql += " AND languagepreferenceid = " + Integer.toString(lang);

        boolean gotRecord = false;
        try {
        	int key = jExec.execute(sql);
        	
        	for (; jExec.next(key); ) {
        		gotRecord = true;
        		setPropertiesFromQueryResult(key);
        		break;
        	}
        	jExec.closeData(key);
        	
        	if (gotRecord == false) {
        		String msg = "DocumentProfile Entity: failed to @findByClassName = " + sql;
        		logger.error(msg);
        		return null;
        	}
        }
        catch (Exception e) {
        	logger.error("finder sql: " + sql);
        	logger.error(e);
        	return null;
        }
        return this;
     }   

  /**
  *  Finds the First DocumentProfile record identified by the the fullname attributes supplied.
  *  @return this DocumentProfile instance populated with data from the corresponding record
  *  or null if no record is found.
  *  records: FinderException if record not found
  */
   public Collection findByNameAttibutes(String lender, String category, String lang) throws Exception
   {
      Collection found = new ArrayList();

      String sql = "Select * from DocumentProfile where DocumentFullName like '%" ;
      sql += lender + "%' AND documentfullname like '%";
      sql += category + "%' AND documentfullname like '%";
      sql += lang + "%' order by documentProfileId";


      try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          DocumentProfile dp = new DocumentProfile(srk);
          dp.setPropertiesFromQueryResult(key);


          found.add(dp);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while fetching DocumentProfiles");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

      return found;
   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
    return doGetStringValue(this, this.getClass(), fieldName);
  }


   /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this,this.getClass(), fieldName, value);
   }


   protected int performUpdate()  throws Exception
   {
      return (doPerformUpdate(this, this.getClass()));
   }

   public String getEntityTableName()
   {
      return "DocumentProfile";
   }

  /**
   *   gets the pk associated with this entity
   *
   *   @return a DealFeePK
   */
   public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }


   public String getDocumentFullName(){ return this.documentFullName ;}
   public String getDocumentDescription(){ return this.documentDescription ;}
   public String getDocumentVersion (){ return this.documentVersion ;}
   public String getDocumentSchemaPath(){ return this.documentSchemaPath ;}
   public String getDocumentTemplatePath(){ return this.documentTemplatePath ;}
   public int getLanguagePreferenceId(){ return this.languagePreferenceId ;}
   public int getLenderProfileId(){ return this.lenderProfileId ;}
   public int getDocumentTypeId(){ return this.documentTypeId ;}
   public String getDocumentFormat(){ return this.documentFormat ;}
   public boolean expectReply(){return TypeConverter.booleanFrom(this.documentFormat);}
   public String getDigestKey(){ return this.digestKey ;}
   public String getReplyFlag(){ return this.replyFlag;}
   public int getDocumentProfileStatus(){ return this.documentProfileStatus;}
   // Addition field to specify Mail Destination -- By BILLY 07Jan2002
   //public int getMailDestinationTypeId(){ return this.mailDestinationTypeId;}


  /**
   ***** WARNING ****
   * this method had never called, since the table is staic.
   * when starting to use it, need to be reviewed. 
   * creates a DocumentProfile using the DocumentProfile primary key and a borrower primary key
   *  - the mininimum (ie.non-null) fields
   *
   */
    public DocumentProfile create(DocumentProfilePK pk)throws RemoteException, CreateException
    {
        String sql = "Insert into DocumentProfile(documentFullName, documentVersion, languagePreferenceId, " +
        "lenderProfileId,  documentTypeId, documentFormat, documentProfileStatus, INSTITUTIONPROFILEID) Values (" + "'" + pk.getDocumentFullName() + "'" + ", " + "'" + pk.getDocumentVersion() + "'" + ", " + pk.getLanguagePreferenceId() + ", " +
         pk.getLenderProfileId() + ", " + pk.getDocumentTypeId() + ", " + "'" + pk.getDocumentFormat() + "'" + "," + pk.getDocumentProfileStatus() + "," + srk.getExpressState().getDealInstitutionId() + ")";
        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("DocumentProfile Entity - DocumentProfile - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

        this.pk = pk;

        return this;
    }

    /**
     * Helper method to giv the information whether this profile contains
     * more than one template.
     */
    public boolean containsMultipleTemplates(){
      if(documentTemplatePath == null){ return false; }
      if(documentTemplatePath.trim().indexOf(";") == -1){return false; }
      return true;
    }

    /**
     * Helper method to retrieve templates in case that this profile
     * contains more than one document.
     */
    public Vector extractTemplates(){
      Vector rv = new Vector();
      if(documentTemplatePath == null || documentTemplatePath.trim().equals("")){ return rv; }
      if(containsMultipleTemplates()){
        return extractTemplates(documentTemplatePath);
      }else{
        rv.addElement( documentTemplatePath.trim() );
      }
      return rv;
    }

    /**
     * Helper method to extract multiple templates.
     */
    private Vector extractTemplates(String pTemplatePath){
      Vector rv = new Vector();
      StringTokenizer st = new StringTokenizer(pTemplatePath, ";");
      while( st.hasMoreTokens() ){
        String oneTok = st.nextToken();
        if( oneTok != null && !oneTok.equals("") ){
          rv.addElement(oneTok);
        }
      }
      return rv;
    }

    public void setDocumentFullName(String val)
    {
      this.testChange("documentFullName", val);
      this.documentFullName = val;
    }
    public void setDocumentDescription(String val)
    {
      this.testChange("documentDescription", val);
      this.documentDescription = val;
    }
    public void setDocumentVersion(String val)
    {
      this.testChange("documentVersion", val);
      this.documentVersion = val;
    }
    public void setDocumentSchemaPath(String val)
    {
      this.testChange("documentSchemaPath", val);
      this.documentSchemaPath = val;
    }
    public void setDocumentTemplatePath(String val)
    {
      this.testChange("documentTemplatePath", val);
      this.documentTemplatePath = val;
    }
    public void setLanguagePreferenceId(int val)
    {
      this.testChange("languagePreferenceId", val);
      this.languagePreferenceId = val;
    }
    public void setLenderProfileId(int val)
    {
      this.testChange("lenderProfileId", val);
      this.lenderProfileId = val;
    }
    public void setDocumentTypeId(int val)
    {
      this.testChange("documentTypeId", val);
      this.documentTypeId = val;
    }

    public void setDocumentFormat(String val)
    {
      this.testChange("documentFormat", val);
      this.documentFormat = val;
    }
    public void setDigestKey(String val)
    {
      this.testChange("digestKey", val);
      this.digestKey = val;
    }
    public void setReplyFlag(String val)
    {
      this.testChange("replyFlag", val);
      this.replyFlag = val;
    }
    public void setDocumentProfileStatus(int val)
    {
      this.testChange("documentProfileStatus", val);
      this.documentProfileStatus = val;
    }
/*    // Addition field to specify Mail Destination -- By BILLY 07Jan2002
    public void setMailDestinationTypeId(int val)
    {
      this.testChange("mailDestinationTypeId", val);
      this.mailDestinationTypeId = val;
    } */
    public int getInstProfileId() {
    	return instProfileId;
    }
    
    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
   }

}
