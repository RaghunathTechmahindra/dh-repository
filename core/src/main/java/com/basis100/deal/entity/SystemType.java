package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.SystemTypePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class SystemType extends DealEntity {

	   EntityContext ctx;

	   private int    systemTypeId;
       private String systemTypeDescription;
       private String channelMedia;
       private String dcChannelCode;
       private String channelMask;   // added for 4.2GR
       private int institutonProfileId;
       
	   private SystemTypePK pk;
	   
	   public SystemType(){}

	   public SystemType(SessionResourceKit srk) throws RemoteException, FinderException
	   {
	      super(srk);
	   }

    public SystemType(SessionResourceKit srk, int id) throws RemoteException, FinderException {
      super(srk);
      SystemTypePK pk = new SystemTypePK(id);

      findByPrimaryKey(pk);
   }

    public SystemType findByPrimaryKey(SystemTypePK pk) throws RemoteException, FinderException
    {
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from SystemType " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "JobTitle: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("JobTitle Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

    	setSystemTypeId(jExec.getInt(key,"SYSTEMTYPEID"));
        setSystemTypeDescription(jExec.getString(key,"SYSTEMTYPEDESCRIPTION"));
        setChannelMedia(jExec.getString(key,"CHANNELMEDIA"));
        setDcChannelCode(jExec.getString(key,"DCCHANNELCODE"));
        setChannelMask(jExec.getString(key,"CHANNELMASK"));
        setInstitutonProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));

    }


	public int getSystemTypeId() {
		return systemTypeId;
	}
	public void setSystemTypeId(int systemTypeId) {
		this.systemTypeId = systemTypeId;
	}
	public String getSystemTypeDescription() {
		return systemTypeDescription;
	}
	public void setSystemTypeDescription(String systemTypeDescription) {
		this.systemTypeDescription = systemTypeDescription;
	}

    public String getChannelMask() {
        return channelMask;
    }

    public void setChannelMask(String channelMask) {
        this.channelMask = channelMask;
    }

    public String getChannelMedia() {
        return channelMedia;
    }

    public void setChannelMedia(String channelMedia) {
        this.channelMedia = channelMedia;
    }

    public String getDcChannelCode() {
        return dcChannelCode;
    }

    public void setDcChannelCode(String dcChannelCode) {
        this.dcChannelCode = dcChannelCode;
    }

    public int getInstitutonProfileId() {
        return institutonProfileId;
    }

    public void setInstitutonProfileId(int institutonProfileId) {
        this.institutonProfileId = institutonProfileId;
    }
}
