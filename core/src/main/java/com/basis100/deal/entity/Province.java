package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ProvincePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class Province extends DealEntity
{
  public int      provinceId;
  public String   provinceName;
  public String   provinceAbbreviation;
  public double   provinceTaxRate; //Provincial tax rate.
   
  private ProvincePK pk;

  public Province(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }

  public Province(SessionResourceKit srk, int id) throws RemoteException, FinderException
  {
    super(srk);
    this.pk = new ProvincePK(id);
    findByPrimaryKey(pk);
  }


  public Province deepCopy() throws CloneNotSupportedException
  {
    return (Province)this.clone();
  }

  //
  // Remote-Home inerface methods:
  //
  // The following methods have counterparts in Enterprise Bean classes - in an
  // Enterprise Entity Bean each methods name would be prefixed with "ejb"
  //


  public Province findByPrimaryKey(ProvincePK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from province where ProvinceId = " + pk.getId();

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          gotRecord = true;
          setPropertiesFromQueryResult(key);
          break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
          String msg = "Page Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
      }
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
    this.pk = pk;
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }


  public Province findByName(String name) throws RemoteException, FinderException
  {
    if (name == null)
      return null;

    name = name.toUpperCase();
    String sql = "Select * from Province where upper(ProvinceName) = '" + name + "'";

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
           gotRecord = true;
           setPropertiesFromQueryResult(key);
           this.pk = new ProvincePK(this.provinceId);
           break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "Deal Entity: @findByName = " + name;
        logger.error(msg);
        return null;
      }
    }
    catch (Exception e)
    {
      logger.error("finder sql: " + sql);
      logger.error(e);
      return null;
    }



    return this;
  }

  /**
   *  finds the province record by abbreviation -  a non-standard helper method
   */
   public Province findByAbbreviation(String abb) throws RemoteException, FinderException
   {
      if (abb == null)
        return null;

      abb = abb.toUpperCase();
      String sql = "Select * from Province where upper(provinceAbbreviation) = '" + abb + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               this.pk = new ProvincePK(this.provinceId);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByAbbreviation = " + abb;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }

        return this;
    }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     setProvinceId(jExec.getInt(key, "provinceId"));
     setProvinceName(jExec.getString(key, "provinceName"));
     setProvinceAbbreviation(jExec.getString(key, "provinceAbbreviation"));
     setProvinceTaxRate(jExec.getDouble(key,"provinceTaxRate"));
   }

   protected int performUpdate() throws Exception
   {
      Class clazz = this.getClass();
      int ret = doPerformUpdate(this, clazz);

      return ret;
   }

   public String getEntityTableName()
   {
      return "Province"; 

   }
	
		/**
     *   gets the provinceAbbreviation associated with this entity
     *
     *   @return a String
     */
    public String getProvinceAbbreviation()
   {
      return this.provinceAbbreviation ;
   }

		/**
     *   gets the provinceName associated with this entity
     *
     *   @return a String
     */
    public String getProvinceName()
   {
      return this.provinceName ;
   }

		/**
     *   gets the provinceId associated with this entity
     *
     */
    public int getProvinceId()
   {
      return this.provinceId ;
   }





		/**
     *   gets the pk associated with this entity
     *
     *   @return a ProvincePK
     */
    public IEntityBeanPK getPk()
   {
      return this.pk ;
   }

   /**
    * <pre>
    *    create a Province record given a ProvincePK primary key.
    *     these create methods assume a standard list of records and are therefore
    *     a utility for record entry.
    *    @param pk - the primary key for this record
    *    @return this instance
    * </pre>
    */
   public Province create(ProvincePK pk)throws RemoteException, CreateException
   {
        String sql = "Insert into Province(" + pk.getName() + " ) Values ( " + pk.getId() + ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate (); // track the creation
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Province Entity - ProvinceType - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

   return this;
  }

   /**
    *    <pre>
    *    create a Province record given an id (primary key)
    *     these create methods assume a standard list of records and are therefore
    *     a utility for record entry.
    *    @param id - the primary key for this record
    *    @return this instance
    *   </pre>
    */
   public Province create(int id)throws RemoteException, CreateException
   {
      pk = new ProvincePK(id);
      return this.create(pk);
   }
   /**
    *   <pre>
    *    create a Province record given an id (primary key),a name and abbreviation
    *    @param id the primary key for this record
    *    @param name the name of the Province 
    *    @param abb the abbreviation for this Province = name;
    *    @return this instance
    *   </pre>
    */
   public Province create(int id, String name, String abb)throws RemoteException, CreateException
   {
        String sql = "Insert into Province ( ProvinceId, ProvinceName, ProvinceAbbreviation ) Values ( '" + id + "', '" + name+ "', '" + abb +"' )";
        pk = new ProvincePK(id);
        
        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate (); // track the creation
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Province Entity - Province - create() exception");
          logger.error(ce.getMessage());
          logger.error("create sql: " + sql);
          logger.error(e);

          throw ce;
        }
       srk.setModified(true);
       return this;
   }


   public void setProvinceId(int id)
	 {
		  this.testChange("provinceId", id);
      provinceId = id;
   }
   public void setProvinceName(String aName)
	 {
		  this.testChange("provinceName", aName);
      provinceName = aName;
   }
   public void setProvinceAbbreviation(String aName)
	 {
		  this.testChange("provinceAbbreviation", aName);
      provinceAbbreviation = aName;
   }

   public double getProvinceTaxRate(){ return this.provinceTaxRate ; }
   public void setProvinceTaxRate(double aName)
	 {
		  this.testChange("provinceTaxRate",aName);
      provinceTaxRate = aName;
   }

   public List getAllProvince() throws FinderException
   {
     List allProvinces = new ArrayList();
     String sql = "Select * from province";

     boolean gotRecord = false;

     try
     {
       int key = jExec.execute(sql);

       for (; jExec.next(key); )
       {
           gotRecord = true;
           Province p = new Province(srk);
           p.setPropertiesFromQueryResult(key);
           allProvinces.add(p);
       }

       jExec.closeData(key);

       if (gotRecord == false)
       {
           String msg = "Page Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
           logger.error(msg);
           throw new FinderException(msg);
       }
     }
     catch (Exception e)
     {
       FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

       logger.error(fe.getMessage());
       logger.error("finder sql: " + sql);
       logger.error(e);

       throw fe;
     }
     return allProvinces;
   }
}
