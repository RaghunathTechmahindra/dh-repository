package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealHistoryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.DBA;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;


/**
 *<pre>
 * DealHistoryBean:
 * Enterprise bean entity representing a record MOS datbase table 'DealHistory'.
 *
 * <b>Notes:</b>
 *  On ejbCreate the value of property <dealHistoryId> is obtained as the next
 * available value of sequence DEALHISTORYSEQ. This sequence must exist in the database, e.g.:
 *
 *            <code> create sequence DealHistorySEQ </code>;
 */

public class DealHistory extends DealEntity
{
    EntityContext ctx;

    // properties

    int     dealHistoryId;
    int     dealId;
    int     statusId;
    Date    transactionDate;
    int     transactionTypeId;
    String  transactionText;
    int     userProfileId;
    int     institutionId;

    DealHistoryPK pk;


    //
    // Constructor: Accepts session resources object (access to system logger and Jdbc
    //              connection resources.
    //

    public DealHistory(SessionResourceKit srk)throws RemoteException
    {
        super(srk);
    }

    // setters/getters

    public int getDealHistoryId()
    {
        return dealHistoryId;
    }

    public void setDealHistoryId(int dealHistoryId)
    {
        this.dealHistoryId = dealHistoryId;
    }

    public int getDealId()
    {
        return dealId;
    }

    public void setDealId(int dealId)
    {
        this.dealId = dealId;
    }

    public int getStatusId()
    {
        return statusId;
    }

    public void setStatusId(int statusId)
    {
        this.statusId = statusId;
    }


    public Date getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public int getTransactionTypeId()
    {
        return transactionTypeId;
    }

    public void setTransactionTypeId(int transactionTypeId)
    {
        this.transactionTypeId = transactionTypeId;
    }

    public String getTransactionText()
    {
        return transactionText;
    }

    public void setTransactionText(String transactionText)
    {
        this.transactionText = transactionText;
    }

    public int getUserProfileId()
    {
        return userProfileId;
    }

    public void setUserProfileId(int userProfileId)
    {
        this.userProfileId = userProfileId;
    }

    //
    // Remote-Home inerface methods:
    //
    // The following methods have counterparts in Enterprise Bean classes - in an
    // Enterprise Entity Bean each methods name would be prefixed with "ejb"
    //

    public DealHistory create(int dealId, int userProfileId, int transactionTypeId, int statusId,
                               String transactionText,  Date transactionDate)
                               throws RemoteException, CreateException
    {

        try
        {
            pk = createPrimaryKey();
            setDealHistoryId(pk.getId());
        }
        catch (CreateException e) { throw e; }

        String sql = "Insert into DealHistory(" +
                     "dealHistoryId, dealId, userProfileId, transactionTypeId, statusId, " +
                     "transactionText, transactionDate, institutionProfileId) " +
                     " Values (" +
                     dealHistoryId + ", " +
                     dealId + ", " +
                     userProfileId + ", " +
                     transactionTypeId + ", " +
                     statusId +
                     ", " + DBA.sqlStringFrom(transactionText) + ", " +
                     // ALERT ::: FIX transactionDate
                     "sysdate, " + srk.getExpressState().getDealInstitutionId() + " )";
        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("DealHistory Entity - create() exception");

            logger.error(ce.getMessage());
            logger.error("create sql: " + sql);
            logger.error(e);

            throw ce;
        }
       srk.setModified(true);
       return this;
    }

    public void remove() throws RemoteException, RemoveException
    {
        String sql = "Delete from DealHistory where dealHistoryId = " + getDealHistoryId();
        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoveException re = new RemoveException("DealHistory Entity - remove() exception");

            logger.error(re.getMessage());
            logger.error("remove sql: " + sql);
            logger.error(e);

            throw re;
        }
    }

    // finders:

    public DealHistory findLatestUpdate(int i_intDealId)throws RemoteException, FinderException{
      String sql = "select * from dealhistory where transactiondate in (select max(transactiondate) from dealhistory where dealid = " + i_intDealId+")"+
       " and dealid = "+ i_intDealId;
     boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "DealHistory Entity: @findLatestUpdate(), key=" + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("DealHistory Entity - findLatestUpdate() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

    public DealHistory findByPrimaryKey(DealHistoryPK pk) throws RemoteException, FinderException
    {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select  * from DealHistory where dealHistoryId = " + pk.getId();

        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "DealHistory Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("DealHistory Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    //deprecate
    // findByDeal:
    //
    // This finder return a collection of history records. The records are all those
    // associated with the specified deal. The assigned tasks
    // are ordered by transaction date

    public Vector findByDeal(int dealId) throws RemoteException, FinderException
    {
        Vector v = new Vector();

        String sql = "Select * from DealHistory where dealId = " + dealId + " Order by transactionDate";

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                DealHistory aTask = new DealHistory(srk);

                aTask.setPropertiesFromQueryResult(key);
                aTask.pk = new DealHistoryPK(aTask.getDealHistoryId());

                v.addElement(aTask);
            }

            jExec.closeData(key);
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("DealHistory Entity - findByDeal() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return v;
    }



    public Collection findByDeal(DealPK pk) throws RemoteException, FinderException
    {
        Collection history = new ArrayList();

        String sql = "Select * from DealHistory where dealId = " + pk.getId() +
                      " Order by transactionDate";

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
              DealHistory aTask = new DealHistory(srk);
              aTask.setPropertiesFromQueryResult(key);
              aTask.pk = new DealHistoryPK(aTask.getDealHistoryId());
              history.add(aTask);
            }

            jExec.closeData(key);
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("DealHistory Entity - findByDeal() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }

        return history;
    }

    /**
    *   gets the pk associated with this entity
    *
    *   @return a DealHistoryPK
    */
    public IEntityBeanPK getPk()
    {
      return (IEntityBeanPK)this.pk ;
    }


    public String getEntityTableName()
    {
      return "DealHistory";
    }


    //
    // Methods implemented in Enterprise Bean Instances that have no remote object
    // counterparts - i.e. EJB container callback methods, and context related methods.
    //

    public void setEntityContext(EntityContext ctx)
    {
        this.ctx = ctx;
    }

    public void unsetEntityContext()
    {
        ctx = null;
    }

    public void ejbActivate() throws RemoteException{;}
    public void ebjPassivate () throws RemoteException {;}

    public void ebjLoad() throws RemoteException
    {
        // implementation not required for now!
    }

    public void ejbStore() throws RemoteException
    {
        String sql = "Update  DealHistory set " +
                     "dealHistoryId = " + getDealHistoryId() + ", " +
                     "dealId = "         + getDealId() + ", " +
                     "userProfileId = "  + getUserProfileId() + ", " +
                     "transactionTypeId = "     + getTransactionTypeId() + ", " +
                     "statusId = "  + getStatusId() + ", " +
                     "transactionText = '"  + getTransactionText() + "' " +
                     // "transactionDate = " + + ", " +
                     "Where dealHistoryId = " + getDealHistoryId();


        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoteException re = new RemoteException("DealHistory Entity - ejbStore() exception");

            logger.error(re.getMessage());
            logger.error("ejbStore sql: " + sql);
            logger.error(e);

            throw re;
        }
        
        //4.4 Entity Cache
        ThreadLocalEntityCache.writeToCache(this, this.getPk());
    }


    //
    // Private
    //

    private void setPropertiesFromQueryResult(int key) throws Exception
    {
        setDealId                   (jExec.getInt(key, 1));
        setDealHistoryId            (jExec.getInt(key, 2));
        setStatusId                 (jExec.getInt(key, 3));
        setTransactionDate          (jExec.getDate(key, 4));
        setTransactionTypeId        (jExec.getInt(key, 5));
        setTransactionText          (jExec.getString(key, 6));
        setUserProfileId            (jExec.getInt(key, 7));
    }

  public DealHistoryPK createPrimaryKey() throws CreateException
  {
       String sql = "Select dealhistoryseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           jExec.closeData(key);
           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Deal History Entity create() exception getting dealHistoryId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new DealHistoryPK(id);
  }
      /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
        testChange("institutionProfileId", institutionId);
        this.institutionId = institutionId;
    }

}
