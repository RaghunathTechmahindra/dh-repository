package com.basis100.deal.entity;

import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.*;


///////////////// class ChangeMapValue is the Item of the HashTable:ChangeValueMap ////////////////
public class ChangeMapValue
{
  private String origValue ;
  private String currValue ;
  private String propertyName;
  private boolean changeFlag;


  public ChangeMapValue(String name, String initVal)
  {
    propertyName = name;
    origValue = initVal;
    currValue = initVal;
  }

  public void setChangeFlag( boolean cf )
  {
   changeFlag = cf;
  }

  public boolean getChangeFlag()
  {
    return changeFlag;
  }

  public String getPropertyName()
  {
    return propertyName;
  }

  public void setOrigValue( String value )
  {
    origValue = value;
  }

  public String getOrigValue()
  {
   return origValue ;
  }

  public void setCurValue(String value)
  {
    this.currValue = value;
  }

  public String getCurValue()
  {
    return currValue ;
  }

  public String getCurrentAsHistoryString()
  {

    if(currValue != null && currValue.startsWith("'"))
    {
      if (currValue.length() == 2)
        return ""; // empty string since must be ''

     return currValue.substring(1,currValue.length() - 1);
    }
    else
     return currValue;
  }

  public String getOriginalAsHistoryString()
  {
    if(origValue != null && origValue.startsWith("'"))
    {
      if (origValue.length() == 2)
        return ""; // empty string since must be ''

     return origValue.substring(1,origValue.length() - 1);
    }
    else
     return origValue;
  }

  //4.4 Entity Cache -- start
  @Override
  public boolean equals(Object obj) {
	  if(obj == null) return false;
	  if(this == obj) return true;
	  
	  if (obj instanceof ChangeMapValue == false)return false;
	  
	  ChangeMapValue other =  (ChangeMapValue)obj;
	  if(StringUtil.isEqual(this.propertyName, other.propertyName) == false) return false;
	  if(StringUtil.isEqual(this.currValue, other.currValue) == false) return false;
	  if(StringUtil.isEqual(this.origValue, other.origValue) == false) return false;
	  return this.changeFlag == other.changeFlag;
  }

  public ChangeMapValue duplicate(){
	  ChangeMapValue cmv = new ChangeMapValue(this.propertyName, this.currValue);
	  cmv.setOrigValue(this.origValue);
	  cmv.setChangeFlag(this.changeFlag);
	  return cmv;
  }

  @Override
  public String toString() {

	  StringBuilder sb = new StringBuilder();
	  sb.append("[");
	  sb.append("prop:").append(propertyName).append(",");
	  sb.append("orig:").append(origValue).append(",");
	  sb.append("curr:").append(currValue).append(",");
	  sb.append("flag:").append(changeFlag).append("]");
	  return sb.toString();
  }
  //4.4 Entity Cache -- end
}
