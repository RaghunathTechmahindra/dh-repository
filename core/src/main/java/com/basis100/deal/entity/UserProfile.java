package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.PasswordEncoder;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.Parameters;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;

import config.UserAdminConfigManager;

/**
 * 
 * UserProfileBean:
 * 
 * Enterprise bean entity representing a record MOS datbase table 'userProfile'.
 * 
 * The a user profile entity consists of the properties in the userProfile table
 * as well as the set of approval authority entries for the user. Approval
 * authority entries correspond to records in the approvalAuthorityTable. These
 * are manifested as the following indexed properties:
 * 
 * int approvalAuthorityTypeId // identifies type of approval authority, i.e.
 * 1=L.O.B int approvalAuthorityTypeValue // value under authority type, i.e.
 * 2=L.O.B. Type 2 double approvalLimit // approval authority amount
 * 
 * When a approval authority record is created the unique key field
 * approvalAuthorityId is obtained as the next available value of sequence
 * APPROVALAUTHORITYSEQ. This sequence must exist in the database, e.g.:
 * 
 * create sequence APPROVALAUTHORITYSEQ;
 * 
 * The approvalAuthorityId is not of interest generally within the application
 * logic; this is true also of the approvalAuthority table field userProfileId
 * since it always matches the userProfileId of the entity itself.
 * 
 * The number of approval authority entries is determined by:
 * 
 * public int getNumberOfApprovalAuthorityEntries();
 */
public class UserProfile extends DealEntity{

    private final static Log _log = LogFactory.getLog(UserProfile.class);

  // quasi-user types
  public static final int JOINT_APPROVERS = 999;

  // used in the findBy...InGroup() methods
  private static String anyAdmin =
    "(userTypeId = " + Mc.USER_TYPE_JR_ADMIN + " OR " + "userTypeId = " + Mc.USER_TYPE_ADMIN
    + " OR " + "userTypeId = " + Mc.USER_TYPE_SR_ADMIN + ")";
  private static String noJrAdmin =
    "(userTypeId = " + Mc.USER_TYPE_ADMIN + " OR " + "userTypeId = " + Mc.USER_TYPE_SR_ADMIN + ")";
  private static String justSrAdmin = "userTypeId = " + Mc.USER_TYPE_SR_ADMIN;
  private static String anyUnderwriter =
    "(userTypeId = " + Mc.USER_TYPE_JR_UNDERWRITER + " OR " + "userTypeId = "
    + Mc.USER_TYPE_UNDERWRITER + " OR " + "userTypeId = " + Mc.USER_TYPE_SR_UNDERWRITER + ")";
  private static String noJrUnderwriter =
    "(userTypeId = " + Mc.USER_TYPE_UNDERWRITER + " OR " + "userTypeId = "
    + Mc.USER_TYPE_SR_UNDERWRITER + ")";
  private static String justSrUnderwriter = "userTypeId = " + Mc.USER_TYPE_SR_UNDERWRITER;
  private static final String LOWER_ASC = "abcdefghijklmnopqrstuvwxyz";
  private static final String LOWER_DESC = "zyxwvutsrqponmlkjihgfedcba";
  private static final String NUMBER_ASC = "0123456789";
  private static final String NUMBER_DESC = "9876543210";
  EntityContext ctx;

  // properties
  //--> Only expose the necessary fields for DocPrep
  //--> As DocPrep can only pickup the protected fields.
  protected int userProfileId;
  protected int userTypeId;
  protected int groupProfileId;
  protected String standardAccess;
  protected String bilingual;
  protected int secondApproverUserId;
  protected int jointApproverUserId;
  protected int contactId;
  protected int profileStatusId;
  protected int partnerUserId;
  protected int standInUserId;
  protected int managerId;
  protected String upBusinessId;
  private String userLogin;
  private String taskAlert;
  // Change to support multiple groups assignnment
  private List groupIds = new ArrayList();
  private Vector approvalAuthority = new Vector();
  private UserSecurityPolicy currSecurityPolicy = null;

  // transient properties - available for master work queue interface only 
  // (specifically, populated by UserProfileQuery.getUsersInGroups())
  int regionId;
  String regionName;
  String regionShortName;
  String regionBusinessId;
  int regionProfileStatusId;
  int branchId;
  String branchName;
  String branchShortName;
  String branchBusinessId;
  int branchProfileStatusId;
  int groupId;
  String groupName;
  String groupShortName;
  String groupBusinessId;
  int groupProfileStatusId;

  int institutionId;  
  // map InstitutionProfileId to UserProfileId
  Map<Integer, Integer> instIdToUPId;
  

  /**
   * Constructor: Accepts session resources object (access to system logger
   * and Jdbc connection resources.
   */
  public UserProfile(SessionResourceKit srk) throws RemoteException {
      super(srk);
  }

  // --> Method to get Approval authority records
  public void setupApprovalAuthorityRecords()
  {
    // get approval authority records
    try
    {
      String sql =
        "Select " + "approvalAuthorityId, approvalAuthorityTypeId, "
        + "approvalAuthorityTypeValue, approvalLimit, institutionProfileId "
        + "from ApprovalAuthority where userProfileId = " + this.userProfileId
        + " AND institutionProfileId = " + getInstitutionId();
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        ApprovalAuthRecord aar = new ApprovalAuthRecord();
        setAAPropertiesFromQueryResult(aar, key);
        approvalAuthority.addElement(aar);
      }

      jExec.closeData(key);
    }
    catch (Exception ex)
    {
      logger.error("Exception when getting approval authority records : " + ex.getMessage());
    }
  }

  //--> Method to get current securityPolicy record
  public void setupCurrSecurityPolicy()
    throws Exception
  {
    currSecurityPolicy = new UserSecurityPolicy(srk);

    try
    {
      currSecurityPolicy.findCurrentRecordByUserLogin(this.userLogin);
    }
    catch (FinderException fe)
    {
      logger.warn("No current Password record found !!");
      currSecurityPolicy = null;
    }
  }

  /**
   * Method to get all groups assigned to the current user
   * Userprofile is loaded from DB everytime it's necessary
   * Sesssion has only userProfileId and srk.
   * when it's loaded, i.e. findByPrimaryKey ect, 
   * VPD must be set to get correct Grop 
   */
  private void setupGroupIds() {
      // get all group ids from UserToGroupAssoc table
      try {
          String sql = "Select groupprofileid "
              + "from userToGroupAssoc where userProfileId = "
              + this.userProfileId + " and institutionProfileId = " + getInstitutionId() 
              + " order by groupprofileid";
          if (_log.isDebugEnabled())
              _log.debug("Seaching Group Profiles: " + sql);
          int key = jExec.execute(sql);
          boolean isFirst = true;
          int theGpId = 0;

          for (; jExec.next(key);) {
              theGpId = jExec.getInt(key, 1);

              // --> To over write the GroupProfileId on the UserProfile table
              if (isFirst) {
                  setGroupProfileId(theGpId);
                  isFirst = false;
              }

              Integer grpId = new Integer(theGpId);
              if (!groupIds.contains(grpId))
              groupIds.add(new Integer(theGpId));
          }

          jExec.closeData(key);
          if (_log.isDebugEnabled())
              _log.debug("Found " + groupIds.size() + " groups!");
      } catch (Exception ex) {
          logger.error("Exception when getting group ids : "
                  + ex.getMessage());
      }
  }

  /*
   * setup InstitutionProfileId to UserProfileID map
   */
  private void setupInstIdToUPId() {
      instIdToUPId = new HashMap<Integer, Integer>();
      
      // temporaly srk that can access all institutions.
      SessionResourceKit psrk = new SessionResourceKit("SYSTEM", true);
      psrk.getExpressState().cleanAllIds();
      try {
          String sql = "Select userProfileId, institutionProfileId "
              + "from userprofile where userLogin = '"
              + this.userLogin + "'";
          if (_log.isDebugEnabled())
              _log.debug("couting instituionProfileIds Profiles: " + sql);
          JdbcExecutor pjExec = psrk.getJdbcExecutor();
          int key = pjExec.execute(sql);

          for (; pjExec.next(key);) {
              Integer upId = new Integer(pjExec.getInt(key, "userProfileId"));
              Integer instId = new Integer(pjExec.getInt(key, "institutionProfileId"));

              instIdToUPId.put(instId, upId);
          }

          pjExec.closeData(key);
          if (_log.isDebugEnabled())
              _log.debug("Found " + groupIds.size() + " groups!");
      } catch (Exception ex) {
          logger.error("Exception when getting group ids : "
                  + ex.getMessage());
      }
      finally {
          psrk.freeResources();
          psrk = null;
      }
  }
  
  
  /*
   * get  InstitutionProfileId to UserProfileID map for only Quick links enabled institution
   */
  public HashMap getQuickLinksEnabledInstitutions(String userLogin ) {
	  HashMap instIdToUPIdQL = new HashMap<Integer, Integer>();
      
      // temporaly srk that can access all institutions.
      SessionResourceKit psrk = new SessionResourceKit("SYSTEM", true);
      psrk.getExpressState().cleanAllIds();
      try {
          
           StringBuffer sbsql = new StringBuffer();
           sbsql.append(" select up.userProfileId, sp.institutionProfileId ")  
          .append(" from userprofile up, sysproperty sp where up.userLogin = '")
          .append(userLogin)
          .append("' ")
          .append(" and up.institutionprofileid = sp.institutionprofileid ")
          .append(" and sp.name='com.filogix.ingestion.quicklinks.enabled' ")
          .append(" and sp.value='Y' ");
                   
          
          if (_log.isDebugEnabled())
              _log.debug("couting Quick link enabled instituionProfileIds Profiles: " + sbsql.toString());
          JdbcExecutor pjExec = psrk.getJdbcExecutor();
          int key = pjExec.execute(sbsql.toString());

          for (; pjExec.next(key);) {
              Integer upId = new Integer(pjExec.getInt(key, "userProfileId"));
              Integer instId = new Integer(pjExec.getInt(key, "institutionProfileId"));

              instIdToUPIdQL.put(instId, upId);
          }

          pjExec.closeData(key);
          if (_log.isDebugEnabled())
              _log.debug("Found " + instIdToUPIdQL.size() + " Institutions!");
      } catch (Exception ex) {
          logger.error("Exception when getting Institution Profile ids : "
                  + ex.getMessage());
      }
      finally {
          psrk.freeResources();
          psrk = null;
      }
      return instIdToUPIdQL;
  }
  
  
  /**
     * 
     */
  public UserProfile create(int userProfileId, int userTypeId, int groupProfileId,
    String userLogin, String standardAccess, String bilingual, int secondApproverUserId,
    int jointApproverUserId, int contactId, int profileStatusId, int partnerUserId,
    int standIdUserId, String taskAlert, int institutionId)
    throws RemoteException, CreateException
  {
    String sql =
      "Insert into UserProfile("
      + "userProfileId, userTypeId, groupProfileId, userLogin, standardAccess, "
      + "bilingual, higherApproverUserId, jointApproverUserId, "
      + "contactId, profileStatusId, partnerUserId, standInUserId, "
      + "TASKALERT, institutionProfileId " + ") Values (" + userProfileId
      + ",  " + userTypeId + ",  " + groupProfileId + ",  '" + userLogin + "', '" + standardAccess
      + "', '" + bilingual + "', " + secondApproverUserId + ", " + jointApproverUserId + ", "
      + contactId + ", " + profileStatusId + ", " + partnerUserId + ", " + standIdUserId + ", '"
      + taskAlert + "', " + institutionId +  ")";
    UserProfileBeanPK pk = new UserProfileBeanPK(userProfileId, institutionId);

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("UserProfile Entity - create() exception");
      logger.error(ce.getMessage());
      logger.error("create sql: " + sql);
      logger.error(e);
      throw ce;
    }

    srk.setModified(true);

    return this;
  }

  public void ebjLoad() throws RemoteException
  {
    // implementation not required for now!
  }

  public void ebjPassivate()
    throws RemoteException
  {
    ;
  }

  public void ejbActivate()
    throws RemoteException
  {
    ;
  }

  //TODO: review sql
  //should we insert all aad?
  public void ejbStore()
    throws RemoteException
  {
    String sql = null;

    try
    {
      sql =
        "UPDATE USERPROFILE SET " + "USERTYPEID = " + getUserTypeId() + ", " + "GROUPPROFILEID = "
        + getGroupProfileId() + ", " + "USERLOGIN = '" + getUserLogin() + "', "
        + "STANDARDACCESS = '" + getStandardAccess() + "', " + "BILINGUAL = '" + getBilingual()
        + "', " + "HIGHERAPPROVERUSERID = " + getSecondApproverUserId() + ", "
        + "JOINTAPPROVERUSERID = " + getJointApproverUserId() + ", " + "CONTACTID = "
        + getContactId() + ", " + "PROFILESTATUSID = " + getProfileStatusId() + ", "
        + "PARTNERUSERID = " + getPartnerUserId() + ", " + "STANDINUSERID = " + getStandInUserId()
        + ", " + "MANAGERID = " + getManagerId() + ", " + "UPBUSINESSID = '" + getUpBusinessId()
        + "', " + "TASKALERT = '" + getTaskAlert() + "' " + " WHERE USERPROFILEID = " + userProfileId 
        + " AND INSTITUTIONPROFILEID = " + getInstitutionId();
      if (_log.isDebugEnabled())
          _log.debug("UPDATE SQL: " + sql);
      jExec.executeUpdate(sql);

      // update/add approval authority records
      int size = approvalAuthority.size();

      for (int i = 0; i < size; ++i)
      {
        ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority.elementAt(i);

        if (aar.getApprovalAuthorityId() == 0)
        {
          // not currently in database - add
            // should we insert for all userInstitutionProfileId?
          sql =
            "INSERT INTO APPROVALAUTHORITY (" + " APPROVALAUTHORITYID," + " USERPROFILEID,"
            + " APPROVALAUTHORITYTYPEID, " + " APPROVALLIMIT," + " APPROVALAUTHORITYTYPEVALUE," 
            + "INSTITUTIONPROFILEID " + " ) "
            + " VALUES( " + "APPROVALAUTHORITYSEQ.NEXTVAL, " + getUserProfileId() + ", "
            + aar.getApprovalAuthorityTypeId() + ", " + aar.getApprovalLimit() + ", "
              + aar.getApprovalAuthorityTypeValue() +", "
              + getInstitutionId() + ")";
          jExec.executeUpdate(sql);
          sql =
            "select approvalAuthorityId from ApprovalAuthority where " + "userProfileId = "
            + getUserProfileId() + " AND INSTITUTIONPROFILEID = " + getInstitutionId() 
            + " Order By approvalAuthorityId";

          int key = jExec.execute(sql);

          for (; jExec.next(key);)
          {
            aar.setApprovalAuthorityId(jExec.getInt(key, 1));  // last
          }

          aar.update = false;
        }

        if (aar.update == true)
        {
          // update required
          sql =
            "update ApprovalAuthority set " + " userProfileId = " + getUserProfileId()
            + " approvalAuthorityTypeId = " + aar.getApprovalAuthorityTypeId()
            + " approvalAuthorityTypeValue = " + aar.getApprovalAuthorityTypeValue()
            + " approvalLimit = " + aar.getApprovalLimit() + " Where userProfileId = "
            + getUserProfileId() + " AND approvalAuthorityId = " + aar.getApprovalAuthorityId()
            + " AND institutionProfileId = " + getInstitutionId();
;
          jExec.executeUpdate(sql);
          aar.update = false;
        }
      }

      //--> Added handle for multiple groups assignment
      // remove all assigned groups -- this is the short cut to remove all
      // and add all again may needed to check before all in the future if the EffectedDate
      // is used for any purpose!!
      sql = "delete from usertogroupassoc where userprofileid = " + getUserProfileId() 
          + " and institutionProfileid = " + getInstitutionId();
      int result = jExec.executeUpdate(sql);
      logger.debug ("deletedusertogroupassoc: " + sql );
      logger.debug( "" + result + " row dated");
      // insert the selected groupids
      for (int i = 0; i < groupIds.size(); ++i)
      {
        sql =
          "insert into usertogroupassoc (" + "userProfileId, groupProfileId, "
          + " institutionProfileId) Values ("
          + getUserProfileId() + ", " + groupIds.get(i) + ", " + getInstitutionId() + ")";
        jExec.executeUpdate(sql);
      }
    }
    catch (Exception e)
    {
      RemoteException re = new RemoteException("UserProfile Entity - ejbStore() exception");
      logger.error(re.getMessage());
      logger.error("ejbStore sql: " + sql);
      logger.error(e);
      throw re;
    }
  }

  public Vector findByAdministratorsInGroup(int groupProfileId, int adminType, boolean activeOnly, int institutionId )
    throws RemoteException, FinderException
  {
    if (adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN;
    }

    return findByUserTypeInGroup(groupProfileId, adminType, activeOnly,
      "findByAdministratorsInGroup()", institutionId);
  }

  // finders:
  public UserProfile findByPrimaryKey(UserProfileBeanPK pk)
    throws RemoteException, FinderException
  {
    String sql = "Select * from UserProfile where userProfileId = " + pk.getUserProfileId()
                + " AND institutionProfileId = " + pk.getInstitutionId();
    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);

        break;  // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "UserProfile Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }

      // get approval authority records
      setupApprovalAuthorityRecords();

      // Addition to handle Current Password stuff -- by BILLY 16April2002
      setupCurrSecurityPolicy();

      // clear first.
      groupIds.clear();
      setupGroupIds();
      
      setupInstIdToUPId();
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException("UserProfile Entity - findByPrimaryKey() exception");
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);
      throw fe;
    }

    return this;
  }

  public UserProfile findByContactId(int contactId, int institutionId)
    throws RemoteException, FinderException
  {
    String sql = "Select * from UserProfile where contactId = " + contactId 
                + " AND institutionProfileId = " + institutionId;
    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);

        break;  // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg =
          "UserProfile Entity: @findByPrimaryKey(), contact Id =" + contactId
          + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }

      // get approval authority records
      setupApprovalAuthorityRecords();

      // Addition to handle Current Password stuff -- by BILLY 16April2002
      setupCurrSecurityPolicy();

      //
      groupIds.clear();
      setupGroupIds();
      setupInstIdToUPId();
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException("UserProfile Entity - findByContactId() exception");
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);
      throw fe;
    }

    return this;
  }

  public Vector findByUnderwritersInGroup(int groupProfileId, int uwType, boolean activeOnly, int institutionId)
    throws RemoteException, FinderException
  {
    if (uwType == -1)
    {
      uwType = Mc.USER_TYPE_JR_UNDERWRITER;
    }

    return findByUserTypeInGroup(groupProfileId, uwType, activeOnly, "findByUnderwritersInGroup()", institutionId);
  }

  public Vector findByUserTypeInGroup(int groupProfileId, int userTypeId, boolean activeOnly, int institutionId)
    throws RemoteException, FinderException
  {
    return findByUserTypeInGroup(groupProfileId, userTypeId, activeOnly, null, institutionId);
  }

  //  userTypeId -1 == all user types
  //  groupProfileId -1 == search across entire institution
  private Vector findByUserTypeInGroup(int groupProfileId, int userTypeId, boolean activeOnly,
    String inFnName, int institutionId)
    throws RemoteException, FinderException
  {
    String fnName = "findByUserTypeInGroup()";

    if (inFnName != null)
    {
      fnName = inFnName;

      //=================================================
      //--> CRF#37 :: Source/Firm to Group routing for TD
      //=================================================
      //--> Modified to support multiple group assignment for user
      //--> By Billy 06Aug2003
    }

    String sql = "Select * from UserProfile p, usertogroupassoc a";

    //Ignore UserProfileId <= 0 -- Debug By BILLY 02May2002
    sql = sql + " where (p.userProfileId > 0)";

    if (groupProfileId != -1)
    {
      sql =
        sql
        + " AND (a.userprofileid = p.userprofileid) "
        + " AND (a.institutionProfileId = p.institutionProfileId)"
        + " AND (p.institutionProfileId = " + institutionId + ") "
        + " AND (a.groupprofileid = "
        + groupProfileId + ") ";

      //else
      //sql = sql + " where groupprofileid >= 0"; // need something else
      // downstream problem!
      //==========================================================
    }

    if (userTypeId != -1)
    {
      if (userTypeId == JOINT_APPROVERS)
      {
        // get profiles of users designated as joint approvers (the
        // designation id indirect via
        // the content of a user id in the <jointApproverUserId> field -
        // e.g.:
        //
        //   {some profile}.isJointApprover :: NO SUCH FIELD (e.g. joint
        // approvers not declared directly)
        //
        //   {some profile}.jointApproverUserId = >0, the user indicated
        // (indirectly) is a joint approver
        //
        // changed fo ML.  
        sql =
          sql
          + " AND (p.jointApproverUserId > 0) ";
      }
      else
      {
        sql = sql + " AND ( p.userTypeid in " + userTypeSelection(userTypeId) + ") ";  // query
      }
    }

    if (activeOnly)
    {
      sql = sql + " AND ( p.profileStatusId = " + Sc.PROFILE_STATUS_ACTIVE + ")";
    }

    Vector users = new Vector();
    //boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        //gotRecord = true;

        UserProfile nextUser = new UserProfile(srk);
        nextUser.setPropertiesFromQueryResult(key);
        users.addElement(nextUser);
      }

      jExec.closeData(key);

      int len = users.size();

      if (len == 0)
      {
        return users;
      }

      for (int i = 0; i < len; ++i)
      {
        UserProfile user = (UserProfile) users.elementAt(i);

        // get approval authority records
        user.setupApprovalAuthorityRecords();

        // Addition to handle Current Password stuff 
        user.setupCurrSecurityPolicy();

        //--> Change to support multiple groups assignnment
        setupGroupIds();
        setupInstIdToUPId();
      }
    }
    catch (Exception e)
    {
      FinderException fe =
        new FinderException("UserProfile Entity - " + fnName + " exception, userType = "
          + userTypeId);
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);
      throw fe;
    }

    return users;
  }
  
  /**
   * <pre>
   *  Given a group (Id), user type id and flag for active profile return:
   *
   * . list of qualifying users from for the branch that contains the specified
   *   group (including those from the specified group), or,
   *
   * . null, if no qualifying users located.
   *   
   * Notes:
   *
   * . For an administrator or underwriter type the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   *   This method uses join one query to fetch group users
   * </pre>
   **/
  public Vector findByUserTypeInGroupId(int groupProfileId, int userTypeId, boolean activeOnly,
		    String inFnName, int institutionId)
		    throws RemoteException, FinderException
  {
    String fnName = "findByUserTypeInBranch()";

    if (inFnName != null)
    {
      fnName = inFnName;

      //=================================================
      //--> CRF#37 :: Source/Firm to Group routing for TD
      //=================================================
      //--> Modified to support multiple group assignment for user
      //--> By Billy 06Aug2003
    }
    StringBuffer sql = new StringBuffer();
    sql.append(" Select * from UserProfile p, usertogroupassoc a,  groupprofile g ");

    //Ignore UserProfileId <= 0 -- Debug By BILLY 02May2002
    sql.append(" where (p.userProfileId > 0)");

    if (groupProfileId != -1)
    {
    	sql.append(" AND (a.userprofileid = p.userprofileid) ");
    	sql.append( " AND (a.institutionProfileId = p.institutionProfileId)");
    	sql.append( " AND a.groupprofileid = g.groupprofileid ");
    	sql.append( " AND a.institutionProfileId =  g.institutionProfileId ");
    	sql.append( " AND (a.institutionProfileId = " + institutionId + ") ");
    	sql.append( " AND (g.branchprofileid = ( select branchprofileid from groupProfile where ");
    	sql.append( " groupProfileId = ");
    	sql.append(   groupProfileId);
    	sql.append( " AND institutionProfileId = ");
    	sql.append( institutionId );
    	sql.append( "))" );
    }
    if (userTypeId != -1)
    {
      if (userTypeId == JOINT_APPROVERS)
      {
        // get profiles of users designated as joint approvers (the
        // designation id indirect via
        // the content of a user id in the <jointApproverUserId> field -
        // e.g.:
        //
        //   {some profile}.isJointApprover :: NO SUCH FIELD (e.g. joint
        // approvers not declared directly)
        //
        //   {some profile}.jointApproverUserId = >0, the user indicated
        // (indirectly) is a joint approver
        //
        // changed fo ML.  
        
        sql.append(" AND (p.jointApproverUserId > 0) ");
      }
      else
      {
    	  sql.append(" AND ( p.userTypeid in " + userTypeSelection(userTypeId) + ") ");  // query
      }
    }

    if (activeOnly)
    {
      sql.append(" AND ( p.profileStatusId = " + Sc.PROFILE_STATUS_ACTIVE + ")");
    }

    Vector users = new Vector();
    //boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql.toString());

      for (; jExec.next(key);)
      {
        //gotRecord = true;

        UserProfile nextUser = new UserProfile(srk);
        nextUser.setPropertiesFromQueryResult(key);
        users.addElement(nextUser);
      }

      jExec.closeData(key);

      int len = users.size();

      if (len == 0)
      {
        return users;
      }

      for (int i = 0; i < len; ++i)
      {
        UserProfile user = (UserProfile) users.elementAt(i);

        // get approval authority records
        user.setupApprovalAuthorityRecords();

        // Addition to handle Current Password stuff 
        user.setupCurrSecurityPolicy();

        //--> Change to support multiple groups assignnment
        setupGroupIds();
        setupInstIdToUPId();
      }
    }
    catch (Exception e)
    {
      FinderException fe =
        new FinderException("UserProfile Entity - " + fnName + " exception, userType = "
          + userTypeId);
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);
      throw fe;
    }

    return users;
  }

  public List getSecondaryParents() throws Exception {
        List sps = new ArrayList();
        Object o = getContact();

        if (o != null) {
            sps.add(o);
        }

        return sps;
    }

  // Indexed property..
  private int getApprovalAuthorityId(int ndx) {
        if ((ndx < 0) || (ndx >= approvalAuthority.size())) {
            return 0; // invalid index result code
        }

        ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority
                .elementAt(ndx);

        return aar.getApprovalAuthorityId();
    }

  public int getApprovalAuthorityTypeId(int ndx) {
        if ((ndx < 0) || (ndx >= approvalAuthority.size())) {
            return 0; // invalid index result code
        }

        ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority
                .elementAt(ndx);

        return aar.getApprovalAuthorityTypeId();
    }

  public int getApprovalAuthorityTypeValue(int ndx) {
        if ((ndx < 0) || (ndx >= approvalAuthority.size())) {
            return 0; // invalid index result code
        }

        ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority
                .elementAt(ndx);

        return aar.getApprovalAuthorityTypeValue();
    }

  public double getApprovalLimit(int ndx) {
        if ((ndx < 0) || (ndx >= approvalAuthority.size())) {
            return 0; // invalid index result code
        }

        ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority
                .elementAt(ndx);

        return aar.getApprovalLimit();
    }

  public String getBilingual() {
        return bilingual;
    }

    public int getContactId() {
        return contactId;
    }

    public Contact getContact() throws FinderException, RemoteException {
        return new Contact(srk, this.contactId, 1);
    }

    public int getGroupProfileId() {
        return groupProfileId;
    }

    public int getJointApproverUserId() {
        return jointApproverUserId;
    }

    public int getNumberOfApprovalAuthorityEntries() {
        return approvalAuthority.size();
    }

    public int getProfileStatusId() {
        return profileStatusId;
    }

    public int getSecondApproverUserId() {
        return secondApproverUserId;
    }

    public String getStandardAccess() {
        return standardAccess;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public int getPartnerUserId() {
        return partnerUserId;
    }

    public int getStandInUserId() {
        return standInUserId;
    }

    public String getUpBusinessId() {
        return upBusinessId;
    }

    public int getManagerId() {
        return managerId;
    }

    public int getUserProfileId() {
        return userProfileId;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

  public void setUpBusinessId(String upBusinessId) {
        this.upBusinessId = upBusinessId;
    }

    public void setBilingual(String bilingual) {
        this.bilingual = bilingual;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public void setSecondApproverUserId(int secondApproverUserId) {
        this.secondApproverUserId = secondApproverUserId;
    }

    public void setStandardAccess(String standardAccess) {
        this.standardAccess = standardAccess;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public void setUserProfileId(int userProfileId) {
        this.userProfileId = userProfileId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public void setGroupProfileId(int groupProfileId) {
        this.groupProfileId = groupProfileId;
    }

    public void setJointApproverUserId(int jointApproverUserId) {
        this.jointApproverUserId = jointApproverUserId;
    }

    public void setProfileStatusId(int profileStatusId) {
        this.profileStatusId = profileStatusId;
    }

    public void setPartnerUserId(int partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public void setStandInUserId(int standInUserId) {
        this.standInUserId = standInUserId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setRegionShortName(String regionShortName) {
        this.regionShortName = regionShortName;
    }

    public void setRegionBusinessId(String regionBusinessId) {
        this.regionBusinessId = regionBusinessId;
    }

    public void setRegionProfileStatusId(int regionProfileStatusId) {
        this.regionProfileStatusId = regionProfileStatusId;
    }

    public int getRegionId() {
        return regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getRegionShortName() {
        return regionShortName;
    }

    public String getRegionBusinessId() {
        return regionBusinessId;
    }

    public int getRegionProfileStatusId() {
        return regionProfileStatusId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public void setBranchShortName(String branchShortName) {
        this.branchShortName = branchShortName;
    }

    public void setBranchBusinessId(String branchBusinessId) {
        this.branchBusinessId = branchBusinessId;
    }

    public void setBranchProfileStatusId(int branchProfileStatusId) {
        this.branchProfileStatusId = branchProfileStatusId;
    }

    public int getBranchId() {
        return branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getBranchShortName() {
        return branchShortName;
    }

    public String getBranchBusinessId() {
        return branchBusinessId;
    }

    public int getBranchProfileStatusId() {
        return branchProfileStatusId;
    }

  public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setGroupShortName(String groupShortName) {
        this.groupShortName = groupShortName;
    }

    public void setGroupBusinessId(String groupBusinessId) {
        this.groupBusinessId = groupBusinessId;
    }

    public void setGroupProfileStatusId(int groupProfileStatusId) {
        this.groupProfileStatusId = groupProfileStatusId;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupShortName() {
        return groupShortName;
    }

    public String getGroupBusinessId() {
        return groupBusinessId;
    }

    public int getGroupProfileStatusId() {
        return groupProfileStatusId;
    }

    public void setGroupIds(List groupIds) {
        this.groupIds = groupIds;
    }

    public void setGroupIds(Object[] groupIdsObj) {
        List result = new ArrayList();

        if ((groupIdsObj != null) && (groupIdsObj.length > 0)) {
            for (int i = 0; i < groupIdsObj.length; i++) {
                try {
                    // Convert the Object to Integer
                    Integer oneId = Integer.valueOf((String) groupIdsObj[i]);

                    // GroupId should be >= 0
                    if (oneId.intValue() >= 0) {
                        result.add(oneId);
                    }
                } catch (Exception e) {
                    // Skip it if any exception
                    logger
                            .warn("@UserProfile.setGroupIds :: Problem when converting Obj[] to Integer :: "
                                    + e);
                }
            }
        }

        this.groupIds = result;
    }

    public List getGroupIds() {
        return groupIds;
    }

    public Object[] getGroupIdsInObjArray() {
        if ((groupIds == null) || (groupIds.size() <= 0)) {
            return null;
        }

        Object[] result = new Object[groupIds.size()];

        for (int i = 0; i < groupIds.size(); i++) {
            result[i] = ((Integer) (groupIds.get(i))).toString();
        }

        return result;
    }

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
      return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
      this.institutionId = institutionId;
//      setupApprovalAuthorityRecords();
//      setupGroupIds();
    }

  /**
   * Method to check if group is already assigned
   */
  public boolean isGroupAssigned(int groupId)
  {
    int size = groupIds.size();

    for (int i = 0; i < size; ++i)
    {
      if (groupId == ((Integer) (groupIds.get(i))).intValue())
      {
        return true;
      }
    }

    return false;
  }

  // New method to Reset all Login Flags to N for Project Init use
  //  -- Should be called only once when application starts
  //  -- By BILLY 19April2002
  public void initLoginFlags()
  {
    String sql = null;

    try
    {
      sql = "Update UserSecurityPolicy set LoginFlag = 'N'";
      jExec.executeUpdate(sql);
    }
    catch (Exception e)
    {
      logger.error("Exception @initLoginFlags sql: " + sql);
      logger.error("Error Msg : " + e.getMessage());
    }
  }

  // Business methods
  public boolean hasApprovalAuthorityForLOB(double amount, int lineOfBusinessId)
  {
    int len = getNumberOfApprovalAuthorityEntries();

    for (int i = 0; i < len; ++i)
    {
      if (getApprovalAuthorityTypeId(i) != Mc.APPROV_AUTH_TYPE_LOB)
      {
        continue;
      }

      if (getApprovalAuthorityTypeValue(i) != lineOfBusinessId)
      {
        continue;
      }

      if (getApprovalLimit(i) >= amount)
      {
        return true;
      }
    }

    return false;
  }

  public void remove()
    throws RemoteException, RemoveException
  {
    String errSql = null;

    try
    {
      //String sql = "Delete from UserProfile where userProfileId = " + getUserProfileId();
      jExec.executeUpdate(errSql);
      //sql = "Delete from ApprovalAuthority where userProfileId = " + getUserProfileId();
      jExec.executeUpdate(errSql);
    }
    catch (Exception e)
    {
      RemoveException re = new RemoveException("UserProfile Entity - remove() exception");
      logger.error(re.getMessage());
      logger.error("remove sql: " + errSql);
      logger.error(e);
      throw re;
    }
  }

  private void setAAPropertiesFromQueryResult(ApprovalAuthRecord aar, int key)
    throws Exception
  {
    aar.setApprovalAuthorityId(jExec.getInt(key, "approvalAuthorityId"));
    aar.setApprovalAuthorityTypeId(jExec.getInt(key, "approvalAuthorityTypeId"));
    aar.setApprovalAuthorityTypeValue(jExec.getInt(key, "approvalAuthorityTypeValue"));
    aar.setApprovalLimit(jExec.getDouble(key, "approvalLimit"));
    aar.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
    aar.update = false;
  }

  private void setApprovalAuthorityId(int ndx, int value)
  {
    if ((ndx < 0) || (ndx >= approvalAuthority.size()))
    {
      return;
    }

    ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority.elementAt(ndx);
    aar.setApprovalAuthorityId(value);
  }

  public void setApprovalAuthorityTypeId(int ndx, int value)
  {
    if ((ndx < 0) || (ndx >= approvalAuthority.size()))
    {
      return;
    }

    ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority.elementAt(ndx);
    aar.setApprovalAuthorityTypeId(value);
  }

  public void setApprovalAuthorityTypeValue(int ndx, int value)
  {
    if ((ndx < 0) || (ndx >= approvalAuthority.size()))
    {
      return;
    }

    ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority.elementAt(ndx);
    aar.setApprovalAuthorityTypeValue(value);
  }

  public void setApprovalLimit(int ndx, double value)
  {
    if ((ndx < 0) || (ndx >= approvalAuthority.size()))
    {
      return;
    }

    ApprovalAuthRecord aar = (ApprovalAuthRecord) approvalAuthority.elementAt(ndx);
    aar.setApprovalLimit(value);
  }

  public String getEntityTableName()
  {
    return "UserProfile";
  }

  //
  // Methods implemented in Enterprise Bean Instances that have no remote
  // object
  // counterparts - i.e. EJB container callback methods, and context related
  // methods.
  public void setEntityContext(EntityContext ctx)
  {
    this.ctx = ctx;
  }

  public void unsetEntityContext()
  {
    ctx = null;
  }

  private void setPropertiesFromQueryResult(int key)
    throws Exception
  {
    setUserProfileId(jExec.getInt(key, "USERPROFILEID"));
    setUserLogin(jExec.getString(key, "USERLOGIN"));
    setUserTypeId(jExec.getInt(key, "USERTYPEID"));
    setGroupProfileId(jExec.getInt(key, "GROUPPROFILEID"));
    setJointApproverUserId(jExec.getInt(key, "JOINTAPPROVERUSERID"));
    setContactId(jExec.getInt(key, "CONTACTID"));
    setProfileStatusId(jExec.getInt(key, "PROFILESTATUSID"));
    setStandardAccess(jExec.getString(key, "STANDARDACCESS"));
    setBilingual(jExec.getString(key, "BILINGUAL"));
    setPartnerUserId(jExec.getInt(key, "PARTNERUSERID"));
    setStandInUserId(jExec.getInt(key, "STANDINUSERID"));
    setManagerId(jExec.getInt(key, "MANAGERID"));
    setSecondApproverUserId(jExec.getInt(key, "HIGHERAPPROVERUSERID"));
    setUpBusinessId(jExec.getString(key, "UPBUSINESSID"));
    setTaskAlert(jExec.getString(key, "TASKALERT"));
    setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
  }

  private String userTypeSelection(int userTypeId)
  {   
    if (userTypeId == -1)
    {
      userTypeId = Mc.USER_TYPE_JR_UNDERWRITER;
    }

    switch (userTypeId) {
        case Mc.USER_TYPE_SR_ADMIN:
            return "( " + Mc.USER_TYPE_SR_ADMIN + ")";
        case Mc.USER_TYPE_ADMIN:
            return "( " + Mc.USER_TYPE_ADMIN + ", " + Mc.USER_TYPE_SR_ADMIN + ")";
        case Mc.USER_TYPE_JR_ADMIN:
            return "( " + Mc.USER_TYPE_ADMIN + ", " + Mc.USER_TYPE_SR_ADMIN 
                + ", " + Mc.USER_TYPE_JR_ADMIN + ")";
        case Mc.USER_TYPE_UNDERWRITER:
            return "( " + Mc.USER_TYPE_UNDERWRITER + ", " + Mc.USER_TYPE_SR_UNDERWRITER + ")";
        case Mc.USER_TYPE_SR_UNDERWRITER:
            return "( " + Mc.USER_TYPE_SR_UNDERWRITER + ")";
        case Mc.USER_TYPE_JR_UNDERWRITER:
            return "( " + Mc.USER_TYPE_UNDERWRITER + ", " + Mc.USER_TYPE_SR_UNDERWRITER
                + ", " + Mc.USER_TYPE_JR_UNDERWRITER + ") " ;
        default:
            return "( " + userTypeId + " )";
    }
    
  }

  /**
   * Method to get the Security Values
   */
  private int getSecurityPolicyValue(int type)
  {
    int id = 0;

    try
    {
      String sql =
        " SELECT SECURITYPOLICY.VALUE FROM SECURITYPOLICY"
        + " WHERE SECURITYPOLICY.SECURITYPOLICYID = " + type;
      int key = jExec.execute(sql);

      while (jExec.next(key))
      {
        id = jExec.getInt(key, 1);

        break;
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      return id;
    }

    return id;
  }

  ////////////////////////////////////////////////////////////////////////////
  // Add Password strength handling -- By Billy 26Oct2001
  //
  //  For instance we could have ID 5 'Password Strength' Value 0 = none, 1 =
  //  must have mixed case, 2 = must have mixed alpha/numeric, 3 = must have
  //  special chars, 4 = must have 1 and 2, 5 = ALL/
  //
  //  Then the password change screen could look at this table every time a
  // user
  //  changes their password and can enforce accordingly. This is in response
  //  to BW changing their passwords to 'password' again.
  //
  //  Another good entry would be the minimum password size instead of hard
  //  coding the the screen.
  //
  //  SECURITYPROFILE entry
  //  ID 6 'Minimum Password Length' Value = 8
  //==============================================================================
  public String validateNewPassword(String newPassword, int languageId)
  {
    String returnStr = "";
    int pwStrength = getSecurityPolicyValue(Sc.PASSWORD_POLICY_PASSWORD_STRENGTH);
    int pwMinLen = getSecurityPolicyValue(Sc.PASSWORD_POLICY_PASSWORD_MIN_LENGTH);
    int pwConsecutiveCharAllowed = getSecurityPolicyValue(Sc.PASSWORD_POLICY_PASSWORD_CONSECUTIVE_CHAR_ALLOWED);
    int pwCharSequencingAllowed = getSecurityPolicyValue(Sc.PASSWORD_POLICY_PASSWORD_CHAR_SEQUENCING_ALLOWED);

    // Assume the newpassword is not null -- suppose it's already checked
    // before calling this method
    // Check if the new password length OK.
    if (newPassword.length() < pwMinLen)
    {
      // Invalidate Length
      //--Release2.1--//
      returnStr = BXResources.getSysMsg("UP_PW_ERROR_MINLENGTH", languageId);

      try
      {
        returnStr = com.basis100.deal.util.Format.printf(returnStr, new Parameters(pwMinLen));
      }
      catch (Exception e)
      {
        logger.warn("String Format error @UserProfile.validateNewPassword !!");
      }

      return returnStr;

      //======================================================================
    }

    if (pwStrength == Sc.PASSWORDD_STRENGTH_NONE)
    {
      return returnStr;
    }

    boolean isCheckMixedCase = false;
    boolean isCheckAlphNum = false;
    boolean isCheckSpecChar = false;

    // Set to Check mixed case
    if ((pwStrength == Sc.PASSWORDD_STRENGTH_MIXED_CASE)
          || (pwStrength == Sc.PASSWORDD_STRENGTH_MIXED_AND_ALPHNUM)
          || (pwStrength == Sc.PASSWORDD_STRENGTH_ALL))
    {
      isCheckMixedCase = true;
    }

    // Set to Check Alpha/Num
    if ((pwStrength == Sc.PASSWORDD_STRENGTH_ALPH_NUM)
          || (pwStrength == Sc.PASSWORDD_STRENGTH_MIXED_AND_ALPHNUM)
          || (pwStrength == Sc.PASSWORDD_STRENGTH_ALL))
    {
      isCheckAlphNum = true;
    }

    // Set to Check Special Char
    if ((pwStrength == Sc.PASSWORDD_STRENGTH_SPEC_CHAR)
          || (pwStrength == Sc.PASSWORDD_STRENGTH_ALL))
    {
      isCheckSpecChar = true;
    }

    // Check it
    boolean mixedCaseCheckOK = false;
    boolean alphNumCheckOK = false;
    boolean specCharCheckOK = false;
    boolean upperCaseFound = false;
    boolean lowerCaseFound = false;
    boolean alphaCharFound = false;
    boolean numCharFound = false;
    int x;

    for (x = 0; x < newPassword.length(); x++)
    {
      char c = newPassword.charAt(x);

      // Check mixed case
      if ((isCheckMixedCase == true) && (mixedCaseCheckOK == false))
      {
        if ((upperCaseFound == false) && Character.isUpperCase(c))
        {
          upperCaseFound = true;
        }

        if ((lowerCaseFound == false) && Character.isLowerCase(c))
        {
          lowerCaseFound = true;
        }

        if ((upperCaseFound == true) && (lowerCaseFound == true))
        {
          mixedCaseCheckOK = true;
        }
      }
        // Check Mixed Case

      // Check Alpha / Num
      if ((isCheckAlphNum == true) && (alphNumCheckOK == false))
      {
        if ((alphaCharFound == false) && Character.isLetter(c))
        {
          alphaCharFound = true;
        }

        if ((numCharFound == false) && Character.isDigit(c))
        {
          numCharFound = true;
        }

        if ((alphaCharFound == true) && (numCharFound == true))
        {
          alphNumCheckOK = true;
        }
      }
        // Check Alpha / Num

      //Check Special Char
      if ((isCheckSpecChar == true) && (specCharCheckOK == false))
      {
        if (!Character.isLetterOrDigit(c))
        {
          specCharCheckOK = true;
        }
      }
        // Check Special Char
    }
      // for

    // Generate return string
    if ((isCheckMixedCase == true) && (mixedCaseCheckOK == false))
    {
      // Mixed Case check Failed
      //--Release2.1--//
      //returnStr += "- the new password must be in mixed cases." +
      // "<br>";
      returnStr += BXResources.getSysMsg("UP_PW_ERROR_MIXCASES", languageId);
    }

    if ((isCheckAlphNum == true) && (alphNumCheckOK == false))
    {
      // Mixed Case check Failed
      //--Release2.1--//
      //returnStr += "- the new password must be have Alpha and Numeric
      // character." + "<br>";
      returnStr += BXResources.getSysMsg("UP_PW_ERROR_ALPNUM", languageId);
    }

    if ((isCheckSpecChar == true) && (specCharCheckOK == false))
    {
      // Mixed Case check Failed
      //--Release2.1--//
      //returnStr += "- the new password must have special character." +
      // "<br>";
      returnStr += BXResources.getSysMsg("UP_PW_ERROR_SPECCHAR", languageId);
    }

    // Security Enhancement: PSE-UC-2
    if (pwConsecutiveCharAllowed>0 && newPassword.matches(".*(.)(\\1){"+pwConsecutiveCharAllowed+"}.*")) {
       String temp = BXResources.getSysMsg("UP_PW_ERROR_CONSECUTIVE_CHAR_ALLOWED", languageId);
       try {
          temp = com.basis100.deal.util.Format.printf(temp, new Parameters(pwConsecutiveCharAllowed));
       } catch (Exception e) {
          logger.warn("String Format error @UserProfile.validateNewPassword !!");
       }
       returnStr += temp;       
    }
    // Security Enhancement: PSE-UC-3
    if (pwCharSequencingAllowed>0) {
       String newPasswordLower = newPassword.toLowerCase();
       int last = newPasswordLower.length() - (pwCharSequencingAllowed);
       for (int i = 0; i < last; i++) {
          String sequence = newPasswordLower.substring(i, i + pwCharSequencingAllowed + 1);
          if (LOWER_ASC.indexOf(sequence) > -1 || LOWER_DESC.indexOf(sequence) > -1 || NUMBER_ASC.indexOf(sequence) > -1 || NUMBER_DESC.indexOf(sequence) > -1) {
             String temp = BXResources.getSysMsg("UP_PW_ERROR_CHAR_SEQUENCING_ALLOWED", languageId);
             try {
                temp = com.basis100.deal.util.Format.printf(temp, new Parameters(pwCharSequencingAllowed));
             } catch (Exception e) {
                logger.warn("String Format error @UserProfile.validateNewPassword !!");
             }
             returnStr += temp;  
             break;
          }
       }
    }
    
    // Fist check if the new password equals the current password
    if (currSecurityPolicy != null)
    {
      if (currSecurityPolicy.getPassword().equals(newPassword))
      {
        //--Release2.1--//
        //returnStr += "- the new password is same as the current
        // password.";
        returnStr += BXResources.getSysMsg("UP_PW_ERROR_SAMECURR", languageId);
      }
      else
      {
        // Check all previous password records
        int lim = 0;
        Object[] objs = null;

        try
        {
          Collection previousPWs =
            new UserSecurityPolicy(srk).findPreviousRecordByUserId(this.userLogin);
          lim = previousPWs.size();
          objs = previousPWs.toArray();
        }
        catch (Exception e)
        {
          lim = 0;
        }
          // assume no previous record

        // comment for ML - MIDORI
        // this is upto instance, not upto institution
        // so that don't need to add institutions
        if (PropertiesCache.getInstance()
              .getInstanceProperty("com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
//          .getProperty(getInstitutionId(),"com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
        {
          newPassword = PasswordEncoder.getEncodedPassword(newPassword);

          //================================================
        }

        for (int i = 0; i < lim; ++i)
        {
          UserSecurityPolicy onePW = (UserSecurityPolicy) objs[i];

          if ((onePW != null) && onePW.getPassword().equals(newPassword))
          {
            //--Release2.1--//
            //returnStr += "- the new password was used before.";
            returnStr = BXResources.getSysMsg("UP_PW_ERROR_USED", languageId);

            break;
          }
        }
      }
    }

    return returnStr;
  }
    // End of validateNewPassword

  public void makePasswordChange(String newPassword)
    throws Exception
  {
    makePasswordChange(newPassword, this.isNewPassword(), this.isPasswordLocked(),
      this.isPasswordReset());
  }

  public void makePasswordChange(String newPassword, boolean isNew, boolean isLock, boolean isReset)
    throws Exception
  {
    try
    {
      // Make the Current Password record ==> Previous
      if (currSecurityPolicy != null)
      {
        currSecurityPolicy.setUserSecurityPolicyStatus(Sc.USERSECUTITYPOLICY_STATUS_PREVIOUS);
        currSecurityPolicy.Update();
      }
      else
      {
        currSecurityPolicy = new UserSecurityPolicy(srk);
      }

      // Create new password record
      currSecurityPolicy.create(this.userLogin, Sc.USERSECUTITYPOLICY_STATUS_CURRENT,
        newPassword);

      if (isNew)
      {
        currSecurityPolicy.setUserSecurityPolicyStatus(Sc.USERSECUTITYPOLICY_STATUS_NEW);
      }

      if (isReset)
      {
        currSecurityPolicy.setPasswordReset("Y");
      }

      if (isLock)
      {
        currSecurityPolicy.setLocked("Y");
      }

      currSecurityPolicy.Update();

      // Remove all previous password records which expired
      Collection previousPWs =
        new UserSecurityPolicy(srk).findPreviousRecordByUserId(this.userLogin);
      int lim = previousPWs.size();
      Object[] objs = previousPWs.toArray();
      int pwRetention = getSecurityPolicyValue(Sc.PASSWORD_POLICY_PASSWORD_RETENTION);

      //logger.debug("The pwRetention = " + pwRetention);
      for (int i = pwRetention; i < lim; ++i)
      {
        UserSecurityPolicy onePW = (UserSecurityPolicy) objs[i];
        onePW.Remove();
      }

      srk.setModified(true);
    }
    catch (Exception e)
    {
      logger.error("Exception @UserProfile.makePasswordChange : " + e.getMessage());
      throw e;
    }
  }

  // Method to get current password
  public String getPassword()
  {
    if (currSecurityPolicy != null)
    {
      return currSecurityPolicy.getPassword();
    }
    else
    {
      //--> Bug fix :: Should return "" instead of null
      //--> By Billy 03Sept2003
      return "";
    }

    //===============================================
  }

  // Method to check if the current password id New
  public boolean isNewPassword()
  {
    if ((currSecurityPolicy != null)
          && (currSecurityPolicy.getUserSecurityPolicyStatus() == Sc.USERSECUTITYPOLICY_STATUS_NEW))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  // Method to check if password rest required
  public boolean isPasswordReset()
  {
    if (currSecurityPolicy != null)
    {
      return currSecurityPolicy.isPasswordReset();
    }
    else
    {
      return false;
    }
  }

  // Method to check if password rest required
  public boolean isPasswordLocked()
  {
    if (currSecurityPolicy != null)
    {
      return currSecurityPolicy.isLocked();
    }
    else
    {
      return false;
    }
  }

  // Method to get LastPasswordChangedate
  public Date getLastPasswordChangedDate()
  {
    if (currSecurityPolicy != null)
    {
      return currSecurityPolicy.getLastPasswordChange();
    }
    else
    {
      return new Date();
    }
  }

  // Method to set current password attributes
  public void setPasswordAttributes(boolean isNew, boolean isLocked, boolean isReset)
  {
    try
    {
      if (isNew)
      {
        currSecurityPolicy.setUserSecurityPolicyStatus(Sc.USERSECUTITYPOLICY_STATUS_NEW);
      }
      else
      {
        currSecurityPolicy.setUserSecurityPolicyStatus(Sc.USERSECUTITYPOLICY_STATUS_CURRENT);
      }

      if (isReset)
      {
        currSecurityPolicy.setPasswordReset("Y");
      }
      else
      {
        currSecurityPolicy.setPasswordReset("N");
      }

      if (isLocked)
      {
        currSecurityPolicy.setLocked("Y");
      }
      else
      {
        currSecurityPolicy.setLocked("N");
      }

      currSecurityPolicy.Update();
      srk.setModified(true);
    }
    catch (Exception e)
    {
      logger.error("Exception @UserProfile.setPasswordAttributes : " + e.getMessage());
    }
  }

  // Method to get/set Current GraceCount
  public int getPasswordGraceCount()
  {
    if (currSecurityPolicy != null)
    {
      return currSecurityPolicy.getCurrentGraceCount();
    }
    else
    {
      return 0;
    }
  }

  // PasswordGraceCount must be same amont institutions in a instance
  public void setPasswordGraceCount(int graceCount)
  {
    try
    {
      currSecurityPolicy.setCurrentGraceCount(graceCount);
      currSecurityPolicy.Update();
      srk.setModified(true);
    }
    catch (Exception e)
    {
      logger.error("Exception @UserProfile.setPasswordGraceCount : " + e.getMessage());
    }
  }

  // Method to get/set Failed Attempts
  // PasswordFailedAttempts must be same amont institutions in a instance
  public int getPasswordFailedAttempts()
  {
    if (currSecurityPolicy != null)
    {
      return currSecurityPolicy.getFailedAttempts();
    }
    else
    {
      return 0;
    }
  }

  public void setPasswordFailedAttempts(int failedAttempts)
  {
    try
    {
      currSecurityPolicy.setFailedAttempts(failedAttempts);
      currSecurityPolicy.Update();
      srk.setModified(true);
    }
    catch (Exception e)
    {
      logger.error("Exception @UserProfile.setPasswordFailedAttempts : " + e.getMessage());
    }
  }

  // Method to get/set Last Password Change Time
  public Date getPasswordChangeTime()
  {
    if (currSecurityPolicy != null)
    {
      return currSecurityPolicy.getLastPasswordChange();
    }
    else
    {
      return null;
    }
  }

  public void setPasswordChangeTime(Date theDate)
  {
    try
    {
      currSecurityPolicy.setLastPasswordChange(theDate);
      currSecurityPolicy.Update();
      srk.setModified(true);
    }
    catch (Exception e)
    {
      logger.error("Exception @UserProfile.setPasswordChangeTime : " + e.getMessage());
    }
  }
  
    public void setLastLoginTime(Date lastLoginTime) {
        try {
            currSecurityPolicy.setLastLoginTime(lastLoginTime);
            currSecurityPolicy.Update();
            srk.setModified(true);
        } catch (Exception e) {
            logger.error("Exception @UserProfile.setLastLoginTime : "
                    + e.getMessage());
        }
    }

    public void setLastLogoutTime(Date lastLogoutTime) {
        try {
            currSecurityPolicy.setLastLogoutTime(lastLogoutTime);
            currSecurityPolicy.Update();
            srk.setModified(true);
        } catch (Exception e) {
            logger.error("Exception @UserProfile.setLastLogoutTime : "
                    + e.getMessage());
        }
    }

    public void setLoginFlag(String loginFlag) {
        try {
            currSecurityPolicy.setLoginFlag(loginFlag);
            currSecurityPolicy.Update();
            srk.setModified(true);
        } catch (Exception e) {
            logger.error("Exception @UserProfile.setLoginFlag : "
                    + e.getMessage());
        }
    }

    public String getLoginFlag() {
        if (currSecurityPolicy != null) {
            return currSecurityPolicy.getLoginFlag();
        } else {
            return "N";
        }
    }

    public Date getLastLoginTime() {
        if (currSecurityPolicy != null) {
            return currSecurityPolicy.getLastLoginTime();
        } else {
            return new Date();
        }
    }

    public Date getLastLogoutTime() {
        if (currSecurityPolicy != null) {
            return currSecurityPolicy.getLastLogoutTime();
        } else {
            return new Date();
        }
    }
    
    public boolean isUserLogOn() {
        if ((getLoginFlag() != null) && getLoginFlag().equals("Y")) {
            return true;
        } else {
            return false;
        }
    }
  
  public UserProfile findByLoginId(String loginId)
    throws RemoteException, FinderException
  {
    String sql = "Select * from UserProfile where UPPER(UserLogin) = UPPER('" + loginId + "')";
    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);
      
      for (; jExec.next(key);)
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);

        break;  // can only be one record
      }
      jExec.closeData(key);
      if (gotRecord == false)
      {
        String msg =
          "UserProfile Entity: @findByLoginId(), Login Id =" + loginId + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }

      UserProfileBeanPK pk = new UserProfileBeanPK(getUserProfileId(), getInstitutionId());
      // get approval authority records
      setupApprovalAuthorityRecords();
      // Addition to handle Current Password stuff -- by BILLY 16April2002
      setupCurrSecurityPolicy();
      //===================================================================
      //=================================================
      //--> CRF#37 :: Source/Firm to Group routing for TD
      //=================================================
      //--> Change to support multiple groups assignnment
      //--> By Billy 06Aug2003
      setupGroupIds();
      setupInstIdToUPId();
      //=================================================
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException("UserProfile Entity - findByLoginId() exception");
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);
      throw fe;
    }
    _log.debug("findByLoginId end");
    return this;
  }

  // Method to get User Full Name (FisrtName + Initial + LastName
  public String getUserFullName()
  {
    try
    {
      Contact contact = getContact();

      return contact.getContactFullName2();
    }
    catch (Exception e)
    {
      logger.warn("UserId (" + getUserProfileId() + ") has no Contact Info. !!!");

      return "";
    }
  }

  // Method to get UserType Description
  public String getUserTypeDescription()
  {
    String desc = PicklistData.getDescription("UserType", this.getUserTypeId());

    return desc;
  }

  // Method to get User's TimeZoneId
  // 
  //******** COMMENT FOR ML ****************
  // this method is called when user longin
  // it may be called when dealInstituionPRofileId is set in Session ??
  public int getUserTimeZoneId()
  {
    int userTimeZone = 5;  //default to EST

    try
    {
        //added InstitutionProfileId for ML.
        //supposed to UserTimeZone is same among all institutions that the user can access
      String sql =
        "Select a.TIMEZONEENTRYID from BRANCHPROFILE a, GROUPPROFILE b, USERPROFILE c WHERE "
        + " c.USERPROFILEID = " + getUserProfileId()
        + " AND c.GROUPPROFILEID = b.GROUPPROFILEID AND b.BRANCHPROFILEID = a.BRANCHPROFILEID "
        + " AND a.institutionProfileid = " + getInstitutionId()
        + " AND b.institutionProfileid = " + getInstitutionId() 
        + " AND c.institutionProfileid = " + getInstitutionId();
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        userTimeZone = jExec.getInt(key, 1);

        break;
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      logger.error("Exception @UserProfile.getUserTimeZoneId (Default to EST) : " + e.getMessage());
    }

    return userTimeZone;
  }

  // Method to get AllowedAuthenticationAttempts (System settings)
  public int getAllowedAuthenticationAttempts()
  {
    return getSecurityPolicyValue(Sc.PASSWORD_POLICY_ALLOWED_AUTH_ATTEMPTS);
  }

  // Method to get MaxFailedAttempts (System settings)
  public int getMaxFailedAttempts()
  {
    return getSecurityPolicyValue(Sc.PASSWORD_POLICY_PASSWORD_MAX_FAILED);
  }

  // Method to get New Password Valid days (System settings)
  public int getNewPasswordValidDays()
  {
    return getSecurityPolicyValue(Sc.PASSWORD_POLICY_NEWPASSWORD_VALID_DAYS);
  }

  //--Release2.1--//
  // New Method to get User default Language
  //--> By Billy 12Nov2002
  public int getUserLanguageId()
  {
    try
    {
      Contact contact = getContact();

      return contact.getLanguagePreferenceId();
    }
    catch (Exception e)
    {
      logger.warn("UserId (" + getUserProfileId() + ") has no Contact Info. !!!");

      return 0;  // Fallback to return 0 English
    }
  }

  //========================================
  //==================================================================================================
  //--Merge--EncriptedPassword--//
  //======================================================================
  // Util method to encode all Passwords in UserSecutityPolicy Table
  // Note: Before calling this method we should first make a backup of the
  //      UserSecutityPolicy table.
  // By Billy 21 Oct 2002
  //======================================================================
  public void encodeAllPassword(SessionResourceKit srk)
    throws Exception
  {
    // Get all Password Records
    Collection allPWs = new UserSecurityPolicy(srk).getAllRecords();
    int lim = allPWs.size();
    Object[] objs = allPWs.toArray();

    for (int i = 0; i < lim; ++i)
    {
      UserSecurityPolicy onePW = (UserSecurityPolicy) objs[i];

      // Encode the Password
      String orgPW = onePW.getPassword();
      String pwEncodedPW = PasswordEncoder.getEncodedPassword(orgPW);
      onePW.setPassword(pwEncodedPW);
      onePW.Update();
    }
  }

  //--> New requirement (for COOP) to track the chnages of UserAdmin fields
  //--> By Billy 15June2003
  public UserAdminHistory getUserAdminHistory()
  {
    return new UserAdminHistory(this.srk);
  }

  public String toString()
  {
    return "{UserId:" + this.userProfileId + ", LoginId:" + this.userLogin + "}";
  }

  public String getStringValue(String fieldName)
  {
    return doGetStringValue(this, this.getClass(), fieldName);
  }

  public Object getFieldValue(String fieldName)
  {
    return doGetFieldValue(this, this.getClass(), fieldName);
  }

  //-- ========== SCR#537 begins ========== --//
  //-- by Neil on Dec/02/2004

  /**
   * DOCUMENT ME!
   *
   * @return Returns the taskAlert.
   */
  public String getTaskAlert()
  {
    return taskAlert;
  }

  /**
   * DOCUMENT ME!
   *
   * @param taskAlert The taskAlert to set.
   */
  public void setTaskAlert(String taskAlert)
  {
    this.taskAlert = taskAlert;
  }

  //-- ========== SCR#537 ends ========== --//
  //--> Cervus II : Added new AUTO usertype (AME spec. section 3.2.1)
  //--> By Billy 28Sept2004

  /**
   * To find any user in the System with the specific UserType. If the input UserType = -1, will
   * return all users in the system.
   *
   * @param userTypeId int
   * @param activeOnly boolean
   *
   * @return Vector
   *
   * @throws RemoteException
   * @throws FinderException
   */
  // Comment for ML - Midori
  // this method is called from AME or Workflow Process including AAG,
  // i.e. pageLess process
  // TODO: review with the component
  // basically, VPD in srk must be set before call this methods 
  public Vector findByUserType(int userTypeId, boolean activeOnly)
    throws RemoteException, FinderException
  {
    StringBuffer sql = new StringBuffer ("Select * from UserProfile");
    sql.append(" where (userProfileId > 0) ");

    if (userTypeId != -1)
    {
      // actually, this is not UserTypeId (JOINT_APPROVERS = 999)
      // com.baiss100.deal.aag use this method to retreive JOINT_APPROVERS users using this variable
      if (userTypeId == JOINT_APPROVERS)
      {
        sql.append( " AND (jointApproverUserId > 0) ");
      }
      else
      { // query based upon user type
        sql.append(" AND (usertypeID in  " + userTypeSelection(userTypeId) + ")" );  
      }
    }

    if (activeOnly)
    {
      sql.append(" AND (profileStatusId = " + Sc.PROFILE_STATUS_ACTIVE + ")");
    }

    Vector users = new Vector();

    //boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql.toString());

      for (; jExec.next(key);)
      {
        //gotRecord = true;

        UserProfile nextUser = new UserProfile(srk);
        nextUser.setPropertiesFromQueryResult(key);
        users.addElement(nextUser);
      }

      jExec.closeData(key);

      int len = users.size();

      if (len == 0)
      {
        return users;
      }

      for (int i = 0; i < len; ++i)
      {
        UserProfile user = (UserProfile) users.elementAt(i);

        // get approval authority records
        user.setupApprovalAuthorityRecords();

        // Addition  to handle Current Password stuff -- by BILLY 16April2002
        user.setupCurrSecurityPolicy();
        setupGroupIds();
        setupInstIdToUPId();

        //=================================================
      }
        // For
    }
    catch (Exception e)
    {
      FinderException fe =
        new FinderException("UserProfile Entity - " + "findByUserType" + " exception, userType = "
          + userTypeId);

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql.toString());
      logger.error(e);

      throw fe;
    }

    return users;
  }
  
  /**
   * find all userProfile data which related to user's login id
   * Note: when you use this method, you have to use 'vpd free' srk,
   * otherwise you will get only one specific userProfile instance.  
   * @param loginId : login id
   * @return multiple UserProfile instance in List
   * @throws FinderException when no data found
   */
  public List<UserProfile> findByLoginIdAllData(String loginId) throws FinderException {
      
      List<UserProfile> found = new ArrayList<UserProfile>();
      if(loginId == null || loginId.equals("")) return found;
      
      StringBuffer sql = new StringBuffer();
      sql.append("SELECT up.* ");
      sql.append("  FROM UserProfile up");
      sql.append("       JOIN InstitutionProfile ip");
      sql.append("       ON up.institutionProfileId = ip.institutionProfileid");
      sql.append(" WHERE UPPER(up.UserLogin)        = '");
      sql.append(loginId.toUpperCase()).append("'");
      sql.append(" ORDER BY ip.INSTITUTIONSEQUENCE");
      
      boolean gotRecord = false;

      try {
          int key = jExec.execute(sql.toString());
          for (; jExec.next(key);) {
              gotRecord = true;
              UserProfile up = new UserProfile(srk);
              up.setPropertiesFromQueryResult(key);
              found.add(up);
          }

          jExec.closeData(key);
      } catch (Exception e) {
          logger.error("finder sql: " + sql.toString());
          logger.error(e);
          logger.error(StringUtil.stack2string(e));
          throw new ExpressRuntimeException(e);
      }

      if (gotRecord == false && getSilentMode() == false) {
          String msg = "UserProfile Entity: @findByLoginIdAllData(), Login Id ="
              + loginId + ", entity not found";
          logger.error("finder sql: " + sql.toString());
          logger.error(msg);
          throw new FinderException(msg);
      }
      return found;
  }
  
  //
  // Inner class for holding properties of an approval authority record
  //
  class ApprovalAuthRecord
  {
    int approvalAuthorityId;  // 0 if record not in database added to

    // database
    int approvalAuthorityTypeId;
    int approvalAuthorityTypeValue;
    double approvalLimit;
    int institutionProfileId;
    boolean update = false;

    ApprovalAuthRecord()
    {
      ;
    }

    ApprovalAuthRecord(int approvalAuthorityId, int approvalAuthorityTypeId,
      int approvalAuthorityTypeValue, double approvalLimit, int institutionProfileid)
    {
      this.approvalAuthorityId = approvalAuthorityId;
      this.approvalAuthorityTypeId = approvalAuthorityTypeId;
      this.approvalAuthorityTypeValue = approvalAuthorityTypeValue;
      this.approvalLimit = approvalLimit;
      this.institutionProfileId = institutionProfileid;
    }

    int getApprovalAuthorityId()
    {
      return approvalAuthorityId;
    }

    void setApprovalAuthorityId(int approvalAuthorityId)
    {
      this.approvalAuthorityId = approvalAuthorityId;
      update = true;
    }

    int getApprovalAuthorityTypeId()
    {
      return approvalAuthorityTypeId;
    }

    void setApprovalAuthorityTypeId(int approvalAuthorityTypeId)
    {
      this.approvalAuthorityTypeId = approvalAuthorityTypeId;
      update = true;
    }

    int getApprovalAuthorityTypeValue()
    {
      return approvalAuthorityTypeValue;
    }

    void setApprovalAuthorityTypeValue(int approvalAuthorityTypeValue)
    {
      this.approvalAuthorityTypeValue = approvalAuthorityTypeValue;
      update = true;
    }

    double getApprovalLimit()
    {
      return approvalLimit;
    }

    void setApprovalLimit(double approvalLimit)
    {
      this.approvalLimit = approvalLimit;
      update = true;
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }
  }

  // Inner class for UserSecurityPolicy record
  class UserSecurityPolicy
  {
    protected Date lastPasswordChange;
    protected int currentGraceCount;
    protected String password;
    protected String passwordReset;
    protected int userSecurityPolicyStatus;
    protected int failedAttempts;
    protected String locked;
    protected String userLogin;
    private Date lastLoginTime;
    private Date lastLogoutTime;
    private String loginFlag;
    
    private SessionResourceKit srk = null;

    UserSecurityPolicy(SessionResourceKit srk)
    {
      this.srk = srk;
    }

    public Date getLastPasswordChange()
    {
      return lastPasswordChange;
    }

    public void setLastPasswordChange(Date value)
    {
      this.lastPasswordChange = value;
    }

    public int getCurrentGraceCount()
    {
      return currentGraceCount;
    }

    public void setCurrentGraceCount(int value)
    {
      this.currentGraceCount = value;
    }

    public String getPassword()
    {
      return password;
    }

    public void setPassword(String value)
    {
      this.password = value;
    }

    public String getPasswordReset()
    {
      return passwordReset;
    }

    public boolean isPasswordReset()
    {
      return ((passwordReset != null) && passwordReset.equals("Y"));
    }

    public void setPasswordReset(String value)
    {
      this.passwordReset = value;
    }

    public int getUserSecurityPolicyStatus()
    {
      return userSecurityPolicyStatus;
    }

    public void setUserSecurityPolicyStatus(int value)
    {
      this.userSecurityPolicyStatus = value;
    }

    public int getFailedAttempts()
    {
      return failedAttempts;
    }

    public void setFailedAttempts(int value)
    {
      this.failedAttempts = value;
    }

    public String getLocked()
    {
      return locked;
    }

    public boolean isLocked()
    {
      return ((locked != null) && locked.equals("Y"));
    }

    public void setLocked(String value)
    {
      this.locked = value;
    }

    /**
     * @return the userLogin
     */
    public String getUserLogin() {
      return userLogin;
    }

    /**
     * @param userLogin the userLogin to set
     */
    public void setUserLogin(String userLogin) {
      this.userLogin = userLogin;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getLastLogoutTime() {
        return lastLogoutTime;
    }

    public void setLastLogoutTime(Date lastLogoutTime) {
        this.lastLogoutTime = lastLogoutTime;
    }

    public String getLoginFlag() {
        return loginFlag;
    }

    public void setLoginFlag(String loginFlag) {
        this.loginFlag = loginFlag;
    }
    
    public UserSecurityPolicy create(String userLogin, int status, String newPassword)
      throws RemoteException, CreateException
    {
      String sql =
        "Insert into UserSecurityPolicy("
        + "userLogin, lastPasswordChange, currentGraceCount, password, passwordReset, "
        + "userSecurityPolicyStatus, failedAttempts, locked, " 
        + "lastLoginTime, lastLogoutTime, loginFlag"
        + ") Values ( '" + userLogin
        + "',  " + "SYSDATE" + ",  " + 0 + ",  '" + newPassword + "', '" + "N" + "', " + status
        + ", " + 0 + ", '" + "N" + "', " + sqlStringFrom(lastLoginTime) + ", " 
        + sqlStringFrom(lastLogoutTime) + ", " + "'N'" 
        + " )";

      try
      {
        jExec.executeUpdate(sql);

        // Find the Current Password record -- Here assumed that the
        // previous "current Password" record was
        // changed to status (Previous = 3) and can only has 1 Current
        // Password exist at anytime.
        this.findCurrentRecordByUserLogin(userLogin);
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("UserSecurityPolicy - create() exception");
        logger.error(ce.getMessage());
        logger.error("create sql: " + sql);
        logger.error(e);
        throw ce;
      }

      return this;
    }

    public void Update() throws RemoteException
    {
      String sql = null;

      try
      {
        sql =
          "Update UserSecurityPolicy set " + "userLogin = '" + getUserLogin() + "', "
          + "lastPasswordChange = " + sqlStringFrom(getLastPasswordChange()) + ", "
          + "currentGraceCount = " + getCurrentGraceCount() + ", " + "password = '" + getPassword()
          + "', " + "passwordReset = '" + getPasswordReset() + "', "
          + "userSecurityPolicyStatus = " + getUserSecurityPolicyStatus() + ", "
          + "failedAttempts = " + getFailedAttempts() + ", " + "locked = '" + getLocked() + "', "
          + "LASTLOGINTIME = " + sqlStringFrom(getLastLoginTime()) + ", " + "LASTLOGOUTTIME = "
          + sqlStringFrom(getLastLogoutTime()) + ", " + "LOGINFLAG = '" + getLoginFlag() + "' "
          + "Where userLogin = '" + getUserLogin() + "' and lastPasswordChange = "
          + sqlStringFrom(getLastPasswordChange());
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        RemoteException re = new RemoteException("UserSecurityPolicy - Update() exception");
        logger.error(re.getMessage());
        logger.error("The sql: " + sql);
        logger.error(e);
        throw re;
      }
    }

    public void Remove() throws RemoteException
    {
      String sql = null;

      try
      {
        sql =
          "Delete from UserSecurityPolicy " + "Where userLogin = '" + getUserLogin()
          + "' and lastPasswordChange = " + sqlStringFrom(getLastPasswordChange());
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        RemoteException re = new RemoteException("UserSecurityPolicy - Remove() exception");
        logger.error(re.getMessage());
        logger.error("The sql: " + sql);
        logger.error(e);
        throw re;
      }
    }

    public UserSecurityPolicy findCurrentRecordByUserLogin(String userLogin)
      throws RemoteException, FinderException
    {
      // should be only one record for ML
      String sql =
        "Select * from UserSecurityPolicy where userLogin = '" + userLogin
        + "' and userSecurityPolicyStatus <> " + Sc.USERSECUTITYPOLICY_STATUS_PREVIOUS;
      boolean gotRecord = false;

      try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key);)
        {
          gotRecord = true;
          setPropertiesFromQueryResult(key);

          break;  // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg =
            "UserSecurityPolicy Entity: @findCurrentRecordByUserId(), userLogin = " + userLogin
            + ", no current password record found!";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
        FinderException fe =
          new FinderException("UserSecurityPolicy Entity - findCurrentRecordByUserId() exception");
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);
        throw fe;
      }

      return this;
    }

    // Method to get all Previous Password records ordered by LastPasswordChangeDate
    public Collection findPreviousRecordByUserId(String userId)
    {
      Collection records = new ArrayList();
      String sql =
        "Select * from UserSecurityPolicy where UserLogin = '" + userId
        + "' and userSecurityPolicyStatus = " + Sc.USERSECUTITYPOLICY_STATUS_PREVIOUS;
      sql += " order by lastpasswordchange desc";

      try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key);)
        {
          UserSecurityPolicy oneRecord = new UserSecurityPolicy(srk);
          oneRecord.setPropertiesFromQueryResult(key);
          records.add(oneRecord);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
        Exception fe =
          new Exception("Exception caught @UserSecurityPolicy.findPreviousRecordByUserId");
        logger.error(fe.getMessage());
        logger.error(e);
      }

      return records;
    }

    /**
     * Method to get all Password records ordered by userId
     */
    public Collection getAllRecords() {
        Collection records = new ArrayList();
        String sql = "Select * from UserSecurityPolicy order by userid desc";

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                UserSecurityPolicy oneRecord = new UserSecurityPolicy(srk);
                oneRecord.setPropertiesFromQueryResult(key);
                records.add(oneRecord);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
            "Exception caught @UserSecurityPolicy.findAllRecords");
            logger.error(fe.getMessage());
            logger.error(e);
        }

        return records;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
        setLastPasswordChange(jExec.getDate(key, "LASTPASSWORDCHANGE"));
        setCurrentGraceCount(jExec.getInt(key, "CURRENTGRACECOUNT"));
        setPassword(jExec.getString(key, "PASSWORD"));
        setPasswordReset(jExec.getString(key, "PASSWORDRESET"));
        setUserSecurityPolicyStatus(jExec.getInt(key,
        "USERSECURITYPOLICYSTATUS"));
        setFailedAttempts(jExec.getInt(key, "FAILEDATTEMPTS"));
        setLocked(jExec.getString(key, "LOCKED"));
        setUserLogin(jExec.getString(key, "USERLOGIN"));
        setLastLoginTime(jExec.getDate(key, "LASTLOGINTIME"));
        setLastLogoutTime(jExec.getDate(key, "LASTLOGOUTTIME"));
        setLoginFlag(jExec.getString(key, "LOGINFLAG"));
    }
  }
    // End of Class UserSecurityPolicy

  public class UserAdminHistory {
        // Define Event Descriptions
        public final static String UA_EVENT_TYPE_UPDATE_STR = "Update";
        public final static String UA_EVENT_TYPE_CREATE_STR = "Create";
        public final static String UA_EVENT_TYPE_DELETE_STR = "Delete";
        private SessionResourceKit srk; // The resource kit
        private SysLogger logger;
        private JdbcExecutor jExec;
        private int auditFieldId; // The key reference to the auditable field
        private int uaCurrentValue; // New Value in INT
        private int uaPreviousValue; // Original Value in INT
        private String uaCurrentValueText; // New Value in Text
        private String uaPreviousValueText; // Original Value in Text
        private int changedByUserId; // User ID who made the change
        private int changedUserId; // User ID being changed
        private Date uaEventTime; // Event time stamp
        private String uaEventDesc;  // Type of event e.g.
                                        // Update/Create/Delete
        private int institutionProfileId;

    // The following is not DB field(s)
        private int eventType;
        

        // Constructor
        public UserAdminHistory(SessionResourceKit inSrk) {
            this.srk = inSrk;
            logger = inSrk.getSysLogger();
            jExec = inSrk.getJdbcExecutor();
        }

        public void setGenericInfo(int userId) {
            setGenericInfo(userId, UserAdminConfigManager.UA_EVENT_TYPE_UPDATE);
        }

        // default is UPDATE

        public void setGenericInfo(int userId, int type) { // this section of
            changedByUserId = srk.getExpressState().getUserProfileId();
            changedUserId = userId;
            uaEventTime = new Date();
            eventType = type;

            switch (type) {
            case UserAdminConfigManager.UA_EVENT_TYPE_CREATE:
                uaEventDesc = UA_EVENT_TYPE_CREATE_STR;

                break;

            case UserAdminConfigManager.UA_EVENT_TYPE_DELETE:
                uaEventDesc = UA_EVENT_TYPE_DELETE_STR;

                break;

            default:
                uaEventDesc = UA_EVENT_TYPE_UPDATE_STR;

                break;
            }
        }

        public void setAuditFieldId(String displayFieldName) {
            String sql = "select auditFieldId from auditField where auditFieldName = "
                    + DBA.sqlStringFrom(displayFieldName);
            boolean gotRecord = false;
            int theAuditFieldId = 0; // default to Unknown

            try {
                int key = jExec.execute(sql);

                for (; jExec.next(key);) {
                    gotRecord = true;
                    theAuditFieldId = jExec.getInt(key, 1);

                    break; // can only be one record
                }

                jExec.closeData(key);

                if (gotRecord == false) {
                    String msg = "UserProfile Entity: @UserAdminHistory.setAuditFieldId(), AuditField Not find !! "
                            + "FieldName = "
                            + displayFieldName
                            + " :: Default to Unknown !!";
                    logger.error(msg);
                    theAuditFieldId = 0;
                }

                // Set the returned ID
                setAuditFieldId(theAuditFieldId);
            } catch (Exception e) {
                logger
                        .error("UserProfile Entity: @UserAdminHistory.setAuditFieldId() Exception : "
                                + e);
            }
        }

        public void setAuditFieldId(int value) {
            auditFieldId = value;
        }

        public void setUaCurrentValue(int value) {
            uaCurrentValue = value;
        }

        public void setUaCurrentValueText(String value) {
            uaCurrentValueText = value;
        }

        public void setUaPreviousValue(int value) {
            uaPreviousValue = value;
        }

        public void setUaPreviousValueText(String value) {
            uaPreviousValueText = value;
        }

        public int getEventType() {
            return eventType;
        }
        
        public int getInstitutionProfileId() {
            return institutionProfileId;
        }

        public void setInstitutionProfileId(int institutionProfileId) {
            this.institutionProfileId = institutionProfileId;
        }

    public void store() throws Exception
    {
      StringBuffer sb = new StringBuffer();
      
      // added instituionProfileId for ML.
      sb.append("Insert into useraudit "
        + " (userauditid, auditfieldid, uacurrentvalue, uapreviousvalue,"
        + " uacurrentvaluetext, uapreviousvaluetext, changedbyuserid,"
        + " changeduserid, uaeventtime, uaeventdesc, institutionProfileId) " + " Values ( ");
      sb.append("userauditidSeq.nextval, ");
      sb.append(auditFieldId);
      sb.append(", ");
      sb.append(uaCurrentValue);
      sb.append(", ");
      sb.append(uaPreviousValue);
      sb.append(", ");
      sb.append(DBA.sqlStringFrom(uaCurrentValueText, 50));
      sb.append(", ");
      sb.append(DBA.sqlStringFrom(uaPreviousValueText, 50));
      sb.append(", ");
      sb.append(changedByUserId);
      sb.append(", ");
      sb.append(changedUserId);
      sb.append(", ");
      sb.append(DBA.sqlStringFrom(uaEventTime));
      sb.append(", ");
      sb.append(DBA.sqlStringFrom(uaEventDesc, 20));
      sb.append(", ");
      sb.append(srk.getExpressState().getDealInstitutionId());
      sb.append(")");

      try
      {
        int key = jExec.executeUpdate(sb.toString());
      }
      catch (Exception e)
      {
        logger.error("Exception @UserProfile.UserAdminHistory.store: Inserting audit record");
        logger.error(e);
      }
    }

  }
    /**
     * <P>isMultiAddess</p>
     * <p>check if this user can access more than one institutions</p>
     * @return boolean
     * 
     */
    public boolean isMultiAccess() {
        if (instIdToUPId == null || instIdToUPId.size() < 2)
            return false;
        return true;
    }

    /**
     * <P>getUserProfileId</p>
     * <p>get UserProfileId for this user associated with the institution
     * @param int: institutionProfileId
     * @return int
     * 
     */
    public int getUserProfileId(int institutionId) {
        if (instIdToUPId == null)
            return -1;
        Integer upId = instIdToUPId.get(new Integer(institutionId));
        if (upId == null) return -1;
        else return upId.intValue();
    }

    /**
     * @return the instIdToUPId
     */
    public Map<Integer, Integer> getInstIdToUPId() {
        return instIdToUPId;
    }

    /**
     * @param instIdToUPId the instIdToUPId to set
     */
    public void setInstIdToUPId(Map<Integer, Integer> instIdToUPId) {
        this.instIdToUPId = instIdToUPId;
    }
    
    /*
     * method to change institutionId, userprofileid in srk
     */
    public UserProfile findByChangeUserProfile(String loginId, int institutionId)
            throws RemoteException, FinderException {
        String sql = "Select * from UserProfile where userLogin = '" + loginId
                + "' AND institutionProfileId = " + institutionId;
        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; //we just need this one
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                logger.error("can't find a UserProfile in changeUserProfile");
                return null;
            }

            // get approval authority records
            setupApprovalAuthorityRecords();

            // Addition to handle Current Password stuff -- by BILLY 16April2002
            setupCurrSecurityPolicy();

            // clear first.
            groupIds.clear();
            setupGroupIds();
            setupInstIdToUPId();
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "UserProfile Entity - findByPrimaryKey() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            return null;
        }
        return this;
    }
    
    /*
     * method to get branchprofileid based on groupprofileid
     */
    public int getUserBranchInfo(String groupprofileid, int institutionProfileId)
    {
    	int branchProfileId = -1;
	    try
	    {
	        String theSQL = "SELECT BRANCHPROFILE.BRANCHPROFILEID "
	          + " FROM GROUPPROFILE, BRANCHPROFILE, REGIONPROFILE "
	          + " WHERE GROUPPROFILE.BRANCHPROFILEID  =  BRANCHPROFILE.BRANCHPROFILEID "
	          + " AND GROUPPROFILE.institutionprofileid = BRANCHPROFILE.institutionprofileid "
	          + " AND BRANCHPROFILE.REGIONPROFILEID  =  REGIONPROFILE.REGIONPROFILEID "
	          + " AND BRANCHPROFILE.institutionprofileid = REGIONPROFILE.institutionprofileid "
	          + " and BRANCHPROFILE.institutionprofileid = " + institutionProfileId 
	          + " AND GROUPPROFILE.GROUPPROFILEID =  " + groupprofileid;
	        JdbcExecutor jExec = srk.getJdbcExecutor();
	        int key = jExec.execute(theSQL);
	
	        if (jExec.next(key))
	        {
	        	branchProfileId =  jExec.getInt(key, 1);
	        }
	
	        jExec.closeData(key);
	      }
	      catch (Exception e)
	      {
	        logger.error(
	          "Exception @UserProfile.getUserBranchInfo: Problem encountered in getting user branch information.");
	        logger.error(e.getMessage());
	      }
	    return branchProfileId;
    }
}
