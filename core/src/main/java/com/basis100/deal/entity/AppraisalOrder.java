package com.basis100.deal.entity;

/**
 * Title: AppraisalOrder
 * Description: Entity to repersent AppraisalOrder Table
 * Copyright:   Copyright (c) 2004
 * Company:     Filogix Inc
 * @author      Billy Lam
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;

public class AppraisalOrder extends DealEntity
{
  protected int     appraisalOrderId;
  protected int     copyId;
  protected int     propertyId;
  protected boolean useBorrowerInfo;
  protected String  propertyOwnerName;
  protected String  contactName;
  protected String  contactWorkPhoneNum;
  protected String  contactWorkPhoneNumExt;
  protected String  contactCellPhoneNum;
  protected String  contactHomePhoneNum;
  protected String  commentsForAppraiser;
  protected int     appraisalStatusId;
  protected int 	instProfileId;

  private AppraisalOrderPK pk;

  /**
   * Constructor
   *
   * @param srk SessionResourceKit
   * @param dcm CalcMonitor
   * @throws RemoteException
   * @throws FinderException
   */
  public AppraisalOrder(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
  {
    super(srk,dcm);
  }

  /**
   * Full constructor
   *
   * @param srk SessionResourceKit
   * @param dcm CalcMonitor
   * @param id int
   * @param copyId int
   * @throws RemoteException
   * @throws FinderException
   */
  public AppraisalOrder(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
  {
    super(srk,dcm);
    pk = new AppraisalOrderPK(id, copyId);
    findByPrimaryKey(pk);
  }

  public AppraisalOrder create(PropertyPK ppk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(ppk.getCopyId());

    String sql = "Insert into AppraisalOrder( " +
    "AppraisalOrderId, PropertyId, copyId, useBorrowerInfo, appraisalStatusId, INSTITUTIONPROFILEID)" +
    " Values ( " +
      pk.getId() + "," + ppk.getId() + "," + pk.getCopyId() + ", 'N', 0," + srk.getExpressState().getDealInstitutionId() +
    ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate (); // track the creation
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("AppraisalOrder Entity - AppraisalOrder - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    srk.setModified(true);

    if(dcm != null)
      dcm.inputCreatedEntity ( this );

    return this;
  }


  private AppraisalOrderPK createPrimaryKey(int copyId)throws CreateException
  {
     String sql = "Select appraisalorderseq.nextval from dual";
     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }
         jExec.closeData(key);
         if( id == -1 ) throw new Exception();

      }
      catch (Exception e)
      {
          CreateException ce = new CreateException("AppraisalOrder Entity create() exception getting AppraisalOrderID from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;

      }

      return new AppraisalOrderPK(id, copyId);
  }

  public AppraisalOrder findByProperty(PropertyPK pk) throws Exception
  {
    StringBuffer sqlbuf
        = new StringBuffer("Select * from AppraisalOrder where ");
    sqlbuf.append("propertyId = ").append(pk.getId());
    sqlbuf.append(" and copyId = ").append(pk.getCopyId());
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sqlbuf.toString());

        for (; jExec.next(key); )
        {
            gotRecord = true;
            this.copyId = pk.getCopyId();
            setPropertiesFromQueryResult(key);
            break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false) {
          String msg = "AppraisalOrder: @findByProperty(), key= " + pk + ", entity not found";
          logger.info(msg);
          return null;
        }
      }
      catch (Exception e) {
        FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sqlbuf);
        logger.error(e);
        return null;

      }

    this.pk = new AppraisalOrderPK(this.appraisalOrderId, this.copyId);
    return this;
  }

  public Collection findByDeal(DealPK dpk)throws Exception
  {

    StringBuffer sqlbuf
      = new StringBuffer("Select * from AppraisalOrder where ");
    sqlbuf.append("propertyId in( select PropertyId from property where dealId = ");
    sqlbuf.append(dpk.getId()).append(" and copyId = ").append(dpk.getCopyId());
    sqlbuf.append(") and copyId = ").append(dpk.getCopyId());

    Collection records = new ArrayList();

    String sql = sqlbuf.toString();

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        AppraisalOrder ao = new AppraisalOrder(srk,dcm);
        ao.setPropertiesFromQueryResult(key);
        ao.pk = new AppraisalOrderPK(ao.getAppraisalOrderId(),ao.getCopyId());

        records.add(ao);
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      String msg = "AppraisalOrder: @findByDeal() error occured.";
      logger.error(msg);
      logger.error(e);
      return null;
    }

    return records;
  }

  public AppraisalOrder findByPrimaryKey(AppraisalOrderPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from AppraisalOrder " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            this.copyId = pk.getCopyId();
            setPropertiesFromQueryResult(key);
            break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }

    this.pk = pk;
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }

  public AppraisalOrder deepCopy() throws CloneNotSupportedException
  {
    return (AppraisalOrder)this.clone();
  }

//GETTERS & SETTERS
  public int getAppraisalOrderId()
  {
    return this.appraisalOrderId;
  }
  public void setAppraisalOrderId(int value)
  {
     this.testChange("appraisalOrderId", value ) ;
     this.appraisalOrderId =  value;
  }

  public int getCopyId(){
    return this.copyId;
  }

  public int getPropertyId()
  {
    return this.propertyId;
  }
  public void setPropertyId(int value)
  {
    this.testChange ("propertyId", value );
    this.propertyId = value;
  }

  public boolean getUseBorrowerInfo()
  {
    return this.useBorrowerInfo ;
  }
  public void setUseBorrowerInfo(String c)
  {
    this.testChange ("useBorrowerInfo", c ) ;
    this.useBorrowerInfo = TypeConverter.booleanFrom(c,"N");
  }

  public String getPropertyOwnerName()
  {
    return this.propertyOwnerName;
  }
  public void setPropertyOwnerName(String value)
  {
    this.testChange ("propertyOwnerName", value );
    this.propertyOwnerName = value;
  }

  public String getContactName()
  {
    return this.contactName;
  }
  public void setContactName(String value)
  {
    this.testChange ("contactName", value );
    this.contactName = value;
  }

  public String getContactWorkPhoneNum()
  {
    return this.contactWorkPhoneNum;
  }
  public void setContactWorkPhoneNum(String value)
  {
    this.testChange ("contactWorkPhoneNum", value );
    this.contactWorkPhoneNum = value;
  }

  public String getContactWorkPhoneNumExt()
  {
    return this.contactWorkPhoneNumExt;
  }
  public void setContactWorkPhoneNumExt(String value)
  {
    this.testChange ("contactWorkPhoneNumExt", value );
    this.contactWorkPhoneNumExt = value;
  }

  public String getContactCellPhoneNum()
  {
    return this.contactCellPhoneNum;
  }
  public void setContactCellPhoneNum(String value)
  {
    this.testChange ("contactCellPhoneNum", value );
    this.contactCellPhoneNum = value;
  }

  public String getContactHomePhoneNum()
  {
    return this.contactHomePhoneNum;
  }
  public void setContactHomePhoneNum(String value)
  {
    this.testChange ("contactHomePhoneNum", value );
    this.contactHomePhoneNum = value;
  }

  public String getCommentsForAppraiser()
  {
    return this.commentsForAppraiser;
  }
  public void setCommentsForAppraiser(String value)
  {
    this.testChange ("commentsForAppraiser", value );
    this.commentsForAppraiser = value;
  }

  public int getAppraisalStatusId()
  {
    return this.appraisalStatusId;
  }
  public void setAppraisalStatusId(int value)
  {
    this.testChange ("appraisalStatusId", value );
    this.appraisalStatusId = value;
  }

  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }

  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   */
  public String getStringValue(String fieldName)
  {
    return doGetStringValue(this, this.getClass(), fieldName);
  }
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }

  public void setPropertiesFromQueryResult(int key) throws Exception
  {
    this.setAppraisalOrderId(jExec.getInt(key,"appraisalOrderId"));
    copyId = jExec.getInt(key,"copyId");
    this.setPropertyId(jExec.getInt(key,"propertyId"));
    this.setUseBorrowerInfo(jExec.getString(key,"useBorrowerInfo"));
    this.setPropertyOwnerName(jExec.getString(key,"propertyOwnerName"));
    this.setContactName(jExec.getString(key,"contactName"));
    this.setContactWorkPhoneNum(jExec.getString(key,"contactWorkPhoneNum"));
    this.setContactWorkPhoneNumExt(jExec.getString(key,"contactWorkPhoneNumExt"));
    this.setContactCellPhoneNum(jExec.getString(key,"contactCellPhoneNum"));
    this.setContactHomePhoneNum(jExec.getString(key,"contactHomePhoneNum"));
    this.setCommentsForAppraiser(jExec.getString(key,"commentsForAppraiser"));
    this.setAppraisalStatusId(jExec.getInt(key,"appraisalStatusId"));
    this.setInstProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
  }

  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();
    int ret = doPerformUpdate(this, clazz);

    // Optimization:
    // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;
  }

  public String getEntityTableName()
  {
    String name = this.getClass().getName();
    int index = name.lastIndexOf(".");

    if(index == -1) return name;

    return name.substring(index + 1, name.length());
  }

  public Vector findByMyCopies() throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from AppraisalOrder where AppraisalOrderId = " +
       getAppraisalOrderId() + " AND copyId <> " + getCopyId();

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          AppraisalOrder iCpy = new AppraisalOrder(srk,dcm);

          iCpy.setPropertiesFromQueryResult(key);
          iCpy.pk = new AppraisalOrderPK(iCpy.getAppraisalOrderId(), iCpy.getCopyId());

          v.addElement(iCpy);
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException("AppraisalOrder - findByMyCopies exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
    return v;
  }

  public int getClassId()
  {
    return ClassId.APPRAISALORDER;
  }

  public boolean isAuditable()
  {
    return true;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof AppraisalOrder )
    {
      AppraisalOrder oa = (AppraisalOrder) o;
      if ( (this.appraisalOrderId == oa.getAppraisalOrderId())
          && (this.copyId == oa.getCopyId()))
        return true;
    }
    return false;
  }

  protected EntityContext getEntityContext () throws RemoteException
  {
    EntityContext ctx = this.entityContext  ;
    try
    {
      ctx.setContextText ( "AppraisalOrder"  ) ;

      Property property = new Property( srk, null , this.propertyId , this.copyId  ) ;
      ctx.setContextSource ( property.getPropertyStreetNumber () + " "
          +  property.getPropertyStreetName ()+ " "
           + PicklistData.getDescription ( srk.getExpressState().getDealInstitutionId(),"AddressType" , property.getPropertyTypeId () ) )  ;

      //Modified by Billy for performance tuning -- By BILLY 28Jan2002
      //Deal  deal = new Deal ( srk, dcm, property.getDealId() , property.getCopyId() ) ;
      //String sc = String.valueOf ( deal.getScenarioNumber ()) + deal.getCopyType () ;
      //if ( deal == null )
      //  return ctx;
      //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
      Deal deal = new Deal(srk, dcm);
      String  sc = String.valueOf (deal.getScenarioNumber(property.getDealId() , property.getCopyId())) +
          deal.getCopyType(property.getDealId() , property.getCopyId());
      // ===================================================================================

      ctx.setScenarioContext ( sc ) ;

      ctx.setApplicationId ( property.getDealId() ) ;

    } catch ( Exception e )
    {
      if ( e instanceof FinderException )
      {
        logger.warn( "AppraisalOrder.getEntityContext Parent Not Found !" );
        return ctx;
      }
      throw ( RemoteException ) e ;
    }  // ----------- end of getEntityContext ----------------------
   return ctx;
  } // ----------------- end of getEntityContext  ---------------------

public int getInstProfileId() {
	return instProfileId;
}

public void setInstProfileId(int instProfileId) {
	this.testChange("INSTITUTIONPROFILEID", instProfileId);
	this.instProfileId = instProfileId;
}
}
