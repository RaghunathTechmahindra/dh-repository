package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.DisclosureQuestionPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: DisclosureQuestion.java
 * </p>
 * 
 * <p>
 * Description: Entity for generic Disclosure Question table. Used by all
 * disclosure documents
 * </p>
 * 
 * <p>
 * Copyright:
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class DisclosureQuestion extends DealEntity {
    protected int disclosureQuestionId;

    protected int documentTypeId;

    protected int dealId;

    protected String questionCode;

    protected boolean isResourceBundleKey;

    protected String response;

    protected int instProfileId;

    private DisclosureQuestionPK pk;

    public DisclosureQuestion(SessionResourceKit srk) throws RemoteException,
	    FinderException {
	super(srk);
    }

    public DisclosureQuestion(SessionResourceKit srk, int id)
	    throws RemoteException, FinderException {
	super(srk);

	pk = new DisclosureQuestionPK(id);

	findByPrimaryKey(pk);
    }

    public IEntityBeanPK getPk() {
	return pk;
    }

    public DisclosureQuestion findByPrimaryKey(DisclosureQuestionPK pk)
	    throws RemoteException, FinderException {
    	
    if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
	String sql = "Select * from DisclosureQuestion " + pk.getWhereClause();

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "DisclosureQuestion Entity: @findByPrimaryKey(), key= "
			+ pk + ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "DisclosureQuestion Entity - findByPrimaryKey() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}
	this.pk = pk;
	ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	return this;
    }

    /**
         * DisclosureQuestion findByDealIdDocumentTypeIdAndQuestionCode
         * 
         * @param dealId
         * @param documentTypeId
         * @param questionCode
         * @return DisclosureQuestion
         * 
         * @throws RemoteException
         * @throws FinderException
         */
    public DisclosureQuestion findByDealIdDocumentTypeIdAndQuestionCode(
	    int dealId, int documentTypeId, String questionCode)
	    throws RemoteException, FinderException {

	String sql = "Select * from DisclosureQuestion where dealId = "
		+ dealId;
	sql += " and documentTypeId = " + documentTypeId;
	sql += " and questionCode = '" + questionCode + "'";

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);
	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }
	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "DisclosureQuestion Entity: @findByDealId_DocumentTypeId_QuestionCode(), key= "
			+ pk + ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "DisclosureQuestion Entity - findByDealIdAndQuestionCode() exception");
	    throw fe;
	}
	this.pk = new DisclosureQuestionPK(disclosureQuestionId);

	return this;
    }

    public DisclosureQuestion findByDealIdAndQuestionCode(int dealId,
	    String questionCode) throws RemoteException, FinderException {
	String sql = "Select * from DisclosureQuestion where dealId = "
		+ dealId + " and questionCode = '" + questionCode + "'";

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "DisclosureQuestion Entity: @findByDealIdAndQuestionCode(), key= "
			+ pk + ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "DisclosureQuestion Entity - findByDealIdAndQuestionCode() exception");

	    // this is not really an error...the caller may just be checking
                // to see if a record exists
	    // logger.error(fe.getMessage());
	    // logger.error("finder sql: " + sql);
	    // logger.error(e);

	    throw fe;
	}
	this.pk = new DisclosureQuestionPK(disclosureQuestionId);

	return this;
    }

    /**
         * Creates a DisclosureQuestion with mininimum fields
         * 
         * @param dpk
         *                DealPK
         * @return DisclosureQuestion
         * @throws RemoteException
         * @throws CreateException
         */
    public DisclosureQuestion create(DealPK dpk, int documentTypeId,
	    String questionCode, String isResourceBundleKey)
	    throws RemoteException, CreateException {
	pk = createPrimaryKey();

	String sql = "Insert into DisclosureQuestion( DisclosureQuestionId, DocumentTypeId, DealId, QuestionCode, IsResourceBundleKey, INSTITUTIONPROFILEID) Values ( "
		+ pk.getId()
		+ ","
		+ documentTypeId
		+ ","
		+ dpk.getId()
		+ ", '"
		+ questionCode + "' , '" + isResourceBundleKey + "' , " + srk.getExpressState().getDealInstitutionId() + ")";

	try {
	    jExec.executeUpdate(sql);
	    findByPrimaryKey(pk);
	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DisclosureQuestion Entity - DisclosureQuestion - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);

	    throw ce;
	}

	srk.setModified(true);

	if (dcm != null)
	    dcm.inputCreatedEntity(this);

	return this;
    }

    public int performUpdate() throws Exception {
	Class clazz = this.getClass();

	int ret = doPerformUpdate(this, clazz);

	// Optimization:
	// After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
	// if (dcm != null && ret > 0) dcm.inputEntity(this);

	return ret;
    }

    /**
         * The DB table name
         * 
         * @return String
         */
    public String getEntityTableName() {
	return "DisclosureQuestion";
    }

    private DisclosureQuestionPK createPrimaryKey() throws CreateException {
	String sql = "Select DisclosureQuestionSeq.nextval from dual";
	int id = -1;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		id = jExec.getInt(key, 1); // can only be one record
	    }

	    jExec.closeData(key);

	    if (id == -1)
		throw new Exception();
	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DisclosureQuestion Entity create() exception getting DisclosureQuestionId from sequence");
	    logger.error(ce.getMessage());
	    logger.error(e);
	    throw ce;
	}

	return new DisclosureQuestionPK(id);
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
	this.setDisclosureQuestionId(jExec.getInt(key, "DISCLOSUREQUESTIONID"));
	this.setDocumentTypeId(jExec.getInt(key, "DOCUMENTTYPEID"));
	this.setDealId(jExec.getInt(key, "DEALID"));
	this.setQuestionCode(jExec.getString(key, "QUESTIONCODE"));
	String isResourceBundleKey = jExec.getString(key, "ISRESOURCEBUNDLEKEY");
	this.setIsResourceBundleKey(isResourceBundleKey.equals("Y") ? true
		: false);
	this.setResponse(jExec.getString(key, "RESPONSE"));
	this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    public int getDealId() {
	return dealId;
    }

    public int getDisclosureQuestionId() {
	return disclosureQuestionId;
    }

    public int getDocumentTypeId() {
	return documentTypeId;
    }

    public boolean isIsResourceBundleKey() {
	return isResourceBundleKey;
    }

    public String getQuestionCode() {
	return questionCode;
    }

    public String getResponse() {
	return response;
    }

    public void setDealId(int dealId) {
	this.dealId = dealId;
    }

    public void setDisclosureQuestionId(int disclosureQuestionId) {
	this.disclosureQuestionId = disclosureQuestionId;
    }

    public void setDocumentTypeId(int documentTypeId) {
	this.testChange("documentTypeId", documentTypeId);
	this.documentTypeId = documentTypeId;
    }

    public void setIsResourceBundleKey(boolean isResourceBundleKey) {
	this.testChange("isResourceBundleKey", isResourceBundleKey);
	this.isResourceBundleKey = isResourceBundleKey;
    }

    public void setQuestionCode(String questionCode) {
	this.testChange("questionCode", questionCode);
	this.questionCode = questionCode;
    }

    public void setResponse(String response) {
	this.testChange("response", response);
	this.response = response;
    }

    public int getInstProfileId() {
	return instProfileId;
    }

    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
   }

}
