package com.basis100.deal.entity.simplecache;

import com.basis100.deal.pk.IEntityBeanPK;

public class PKWrapper {
	
	final IEntityBeanPK pk_;
	final int institution_;

	PKWrapper(IEntityBeanPK pk, int institution) {
		this.pk_ = pk;
		this.institution_ = institution;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) return false;
		if (obj instanceof PKWrapper == false) return false;
		
		PKWrapper other = (PKWrapper) obj;
		if (this.institution_ != other.institution_) return false;
		
		return this.pk_.equals(other.pk_);
	}

	@Override
	public int hashCode() {
		return this.pk_.hashCode() ^ this.institution_;
	}

	@Override
	public String toString() {
		return pk_.getClass().getSimpleName() + "|" +pk_.getName() + "| id:" + pk_.getId() + "| cp:"
				+ pk_.getCopyId() + "| inst:" + institution_;
	}
}