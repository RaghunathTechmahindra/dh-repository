package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationApplicantPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class AdjudicationApplicant extends DealEntity{
	protected int	requestId;
	protected int	applicantNumber;
	protected int	copyId;
	protected int	borrowerId;
	protected int	instProfileId;
	protected String	applicantCurrentInd;

	public AdjudicationApplicantPK pk;

	public AdjudicationApplicant(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public AdjudicationApplicant(SessionResourceKit srk, int id, int copyid) throws RemoteException, FinderException
	{
		super(srk);
	    pk = new AdjudicationApplicantPK(id,copyid);
	    findByPrimaryKey(pk);	  }

	/**
	 * search for AdjudicationApplicant record using primary key
	 * 
	 * @return a AdjudicationApplicant
	 */
	public AdjudicationApplicant findByPrimaryKey(AdjudicationApplicantPK pk) throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from AdjudicationApplicant " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "AdjudicationApplicant: @findByPrimaryKey(), key= " + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException("AdjudicationApplicant Entity - findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}

	/**  sets properties for AdjudicationApplicant */
	public void setPropertiesFromQueryResult(int key) throws Exception
	{
		setRequestId(jExec.getInt(key,"requestid"));
	    setApplicantNumber(jExec.getInt(key,"applicantNumber"));
	    setCopyId(jExec.getInt(key,"copyid"));
	    setBorrowerId(jExec.getInt(key,"borrowerid"));
	    setApplicantCurrentInd(jExec.getString(key,"applicantCurrentInd"));
	    setInstProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	public String getEntityTableName()
	{
	    return "AdjudicationApplicant";
	}

	/**  gets the pk associated with this entity
	 *   @return a AdjudicationApplicantPK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}

	public String getApplicantCurrentInd()
	{
		return this.applicantCurrentInd;
	}

	public void setApplicantCurrentInd(String ind)
	{
		this.testChange ("applicantCurrentInd", ind ) ;
		this.applicantCurrentInd = ind;
	}

	public int getApplicantNumber()
	{
		return this.applicantNumber;
	}

	public void setApplicantNumber(int id)
	{
		this.testChange ("applicantNumber", id);
		this.applicantNumber = id;
	}

	public int getBorrowerId()
	{
		return this.borrowerId;
	}

	public void setBorrowerId(int id)
	{
		this.testChange ("borrowerId", id);
		this.borrowerId = id;
	}

	public int getCopyId()
	{
		return this.copyId;
	}

	public void setCopyId(int id)
	{
		this.testChange ("copyId", id);
		this.copyId = id;
	}

	public int getRequestId()
	{
		return this.requestId;
	}

	public void setRequestId(int id)
	{
		this.testChange ("requestId", id);
		this.requestId = id;
	}

	/**  creates AdjudicationApplicant record into database
	 *   @return a AdjudicationApplicant */
	public AdjudicationApplicant create(RequestPK rpk, BorrowerPK bpk,
											int appnum, String appcurrind)throws RemoteException, CreateException
	{
		int key = 0;
		// Create BORROWERREQUESTASSOC record first
		StringBuffer sqla = new StringBuffer("Insert into BorrowerRequestAssoc( requestId" +
			",copyId, borrowerId,INSTITUTIONPROFILEID) Values ( ");
		sqla.append(sqlStringFrom(rpk.getId()) + ", ");
		sqla.append(sqlStringFrom(bpk.getCopyId()) + ", ");
		sqla.append(sqlStringFrom(bpk.getId()) + ", ");
		sqla.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");

		String sql = new String(sqla);

		try
		{
			key = jExec.executeUpdate(sql);
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException("AdjudicationApplicant Entity - AdjudicationApplicant - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			throw ce;
		}
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

	  	key = 0;
		// Insert ADJUDICATIONAPPLICANT record next
		StringBuffer sqlb = new StringBuffer("Insert into AdjudicationApplicant( requestId" +
	  			",applicantNumber, copyId, borrowerId, applicantCurrentInd,INSTITUTIONPROFILEID) Values ( ");
		sqlb.append(sqlStringFrom(rpk.getId()) + ", ");
	  	sqlb.append(sqlStringFrom(appnum) + ", ");
			sqlb.append(sqlStringFrom(bpk.getCopyId()) + ", ");
			sqlb.append(sqlStringFrom(bpk.getId()) + ", ");
			sqlb.append(sqlStringFrom(appcurrind) + ", ");
			sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");

		String sql2 = new String(sqlb);

		try
		{
			key = jExec.executeUpdate(sql2);
			findByPrimaryKey(new AdjudicationApplicantPK(rpk.getId(),appnum));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("AdjudicationApplicant Entity - AdjudicationApplicant - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);

			throw ce;
		}
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
	    return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof AdjudicationApplicant)
		{
			AdjudicationApplicant oa = (AdjudicationApplicant) o;
			if  (  this.requestId == oa.getRequestId ()  )
				return true;
		}
		return false;
	}

    public Collection findByBorrower(BorrowerPK pk) throws Exception
    {
      Collection adjapplicants = new ArrayList();

      String sql = "Select * from AdjudicationApplicant " ;
            sql +=  pk.getWhereClause();
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
            	AdjudicationApplicant adjapplicant = new AdjudicationApplicant(srk);
            	adjapplicant.setPropertiesFromQueryResult(key);
            	adjapplicant.pk = new AdjudicationApplicantPK(adjapplicant.getRequestId(),adjapplicant.getApplicantNumber());
            	adjapplicants.add(adjapplicant);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Borrower::AdjudicationApplicant");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return adjapplicants;
    }

    public Collection findByRequest(RequestPK pk) throws Exception {
      Collection adjapplicants = new ArrayList();

      String sql = "Select * from AdjudicationApplicant ";
      sql += pk.getWhereClause();
      try {
        int key = jExec.execute(sql);
        for (; jExec.next(key); ) {
          AdjudicationApplicant adjapplicant = new AdjudicationApplicant(srk);
          adjapplicant.setPropertiesFromQueryResult(key);
          adjapplicant.pk = new AdjudicationApplicantPK(adjapplicant.getRequestId(),
              adjapplicant.getApplicantNumber());
          adjapplicants.add(adjapplicant);
        }
        jExec.closeData(key);

      }
      catch (Exception e) {
        Exception fe = new Exception(
            "Exception caught while getting Borrower::AdjudicationApplicant");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

      return adjapplicants;
    }

    public Collection getSAMAdjudicationApplicants() throws Exception
    {
      return new SAMAdjudicationApplicant(srk).findByAdjudicationApplicant(this.pk);
    }

    public void setPk(AdjudicationApplicantPK pk) {
        this.pk = pk;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Clones everything except for the PK. Inserts the new record into the DB.
     */
    public Object clone(RequestPK rpk) throws CloneNotSupportedException
    {
        AdjudicationApplicant clone = null;

        try {
            // create applicant to generate the new PK
            clone = new AdjudicationApplicant(srk);
            clone.create(rpk, new BorrowerPK(this.getBorrowerId(), 
                                             this.getCopyId()), 
                                             this.getApplicantNumber(), 
                                             this.getApplicantCurrentInd());


            // save newly created PK before cloning.
            AdjudicationApplicantPK aPK = (AdjudicationApplicantPK) clone.getPk();

            // call object's clone (create shallow copy).
            clone = (AdjudicationApplicant) super.clone();

            // restore PK
            clone.setPk(aPK);
            clone.setApplicantNumber(aPK.getCopyId());
            clone.setRequestId(aPK.getId());

            clone.doPerformUpdateForce();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in AdjudicationApplicant.clone(). Trying to clone " +
                    "applicantId = " + applicantNumber + "; requestId = " + requestId);
        }

        return clone;

    }

    /**
     * We need to override this because we also need to delete the borrowerrequestassociation as well as the adj applicant
     * @param needCalculation boolean
     * @throws RemoteException
     */
    public void ejbRemove( boolean needCalculation ) throws RemoteException
    {
      String sql =null ;
      IEntityBeanPK pk = getPk();

      try
      {
       this.removeSons(); // Delete all the sons

       // first delete the record as normal
       super.ejbRemove(needCalculation);

       // then delete the association
       sql = "Delete From BORROWERREQUESTASSOC where REQUESTID = " + this.getRequestId() + " and BORROWERID = " + this.getBorrowerId();
       jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        String msg =  "Failed to remove record " + pk.getWhereClause();
        RemoteException re = new RemoteException(msg);
        logger.error(re.getMessage());
        logger.error("remove sql: " + sql);
        logger.error(e);

        throw re;
      }

    }

    protected void removeSons() throws Exception
    {
      Collection samAdjudicationApplicants = getSAMAdjudicationApplicants();
      Iterator samAdjAppIt = samAdjudicationApplicants.iterator();

      while (samAdjAppIt.hasNext()) {
        SAMAdjudicationApplicant saa = (SAMAdjudicationApplicant)samAdjAppIt.next();

        saa.ejbRemove();
      }
    }

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}
}
