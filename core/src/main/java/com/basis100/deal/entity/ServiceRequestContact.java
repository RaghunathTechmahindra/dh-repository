/*
 * ServiceRequestContact.java
 * 
 * ServiceRequestContact Entity Class (Connectivity to AVM services)
 * 
 * created: 06-FEB-2006
 * author:  Edward Steel
 * 
 * TODO: test findByBorrowerIdAndCopyId : ServiceRequestContact
 *           test createPrimaryKey : ServiceRequestContactPK
 *           add assoc table awareness to performUpdate() 
 */
package com.basis100.deal.entity;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceRequestContactPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


/**
 * ServiceRequestContact entity class.
 * 
 * @author ESteel
 */
public class ServiceRequestContact extends DealEntity {

    
    protected int     serviceRequestContactId;
    protected int     requestId;
        
    protected String nameFirst;
    protected String nameLast;
    protected String emailAddress;
    protected String workAreaCode;
    protected String workPhoneNumber;
    protected String workExtension;
    protected String cellAreaCode;
    protected String cellPhoneNumber;
    protected String homeAreaCode;
    protected String homePhoneNumber;
    protected String propertyOwnerName;
    protected String comments;
    
    // for association
    //FXP27162
    private boolean haveAssoc;
    //End of FXP27162
    protected int borrowerId;
    protected int copyId;
    
    // for ML
    protected int institutionProfileId;
    
    private ServiceRequestContactPK pk;
    
    /**
     * Constructor. Creates an empty entity
     * @param srk SessionResourceKit to use.
     */
    public ServiceRequestContact(SessionResourceKit srk) throws RemoteException {
        super(srk);
    }
    
    /**
     * Constructor, also populates. Creates an empty entity then fills it from
     * table.
     * @param srk SessionResourceKit to use.
     * @param id  The row's ServiceRequestContactId.
     * @param requestId The row's requestId.
     * @throws RemoteException If ServiceRequestContact cannot be created.
     * @throws FinderException If ServiceRequestConcact cannot be populated.
     */
    public ServiceRequestContact(SessionResourceKit srk, int id, int requestId, int copyId) 
            throws RemoteException, FinderException {
        super(srk);
        
        pk = new ServiceRequestContactPK(id, requestId, copyId);
        
        findByPrimaryKey(pk);
    }
    
    public ServiceRequestContactPK createPrimaryKey(int requestId, int copyId) 
            throws CreateException {
    String sql = "Select serviceRequestContactseq.nextval from dual";
    int id = -1;

    try {
      int key = jExec.execute(sql);

      for(; jExec.next(key); ) {
        id = jExec.getInt(key, 1);
        break;
      }
      jExec.closeData(key);
      if(id == -1) {
        throw new CreateException();
      }
    }
    catch(Exception e) {
      CreateException ce = new CreateException(
        "Request createPrimaryKey() :exception: getting RequestId from sequence");
      logger.error(ce.getMessage());
      logger.error(e);
      throw ce;
    }
    
    return new ServiceRequestContactPK(id, requestId, copyId);
    }
    
    /**
     * Create and insert into database. This method does not handle the association
     * with Borrower, and the assoc table.
     * @param requestId The request ID to set.
     * @return This, with information retrieved from the database.
     * @throws CreateException If the record cannot be inserted, or the data
     * cannot subsequently be retrieved.
     */
    public ServiceRequestContact create(int requestId, int copyId) 
            throws CreateException {
        pk = createPrimaryKey(requestId, copyId);
        
        String sql = "INSERT INTO ServiceRequestContact(ServiceRequestContactId, "
          + "RequestId, copyId, institutionProfileId) VALUES (" + pk.getId() 
          + ", " + pk.getRequestId() + ", " + pk.getCopyId()+ ", " 
          + srk.getExpressState().getDealInstitutionId() + ")";
        
        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        }
        catch (Exception e) {
            CreateException ce = new CreateException("ServiceRequestContact entity - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            
            throw ce;
        }
        
        this.haveAssoc = false;
        this.borrowerId = -1;
        this.copyId = -1;
        srk.setModified(true);
        
        return this;        
    }
    
    
    
    /**
     * Create and insert into database. This method does handle borrowerId and copyId
     * but doesn't handle the association table.
     * @param borrowerId The borrower ID to set.
     * @param copyId The copy ID to set.
     * @return This, with information retrieved from database.
     * @throws CreateException If the record cannot be inserted, or the data
     * cannot subsequently be retrieved.
     */
    public ServiceRequestContact create(ServiceRequestContactPK pk, int borrowerId, int copyId) 
            throws CreateException {
        String sql = "INSERT INTO ServiceRequestContact(ServiceRequestContactId, "
            + "RequestId, borrowerId, copyId, institutionProfileId)" +
         " VALUES (" + pk.getId() + ", " + pk.getRequestId() + ", " +
         sqlStringFrom(borrowerId) + ", " + sqlStringFrom(copyId) + ", "
         + srk.getExpressState().getDealInstitutionId() +")";
        
        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        }
        catch (Exception e) {
            CreateException ce = new CreateException("ServiceRequestContact - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            
            throw ce;
        }
        
        this.haveAssoc = false;
        this.borrowerId = -1;
        this.copyId = -1;
        srk.setModified(true);
        
        return this;
    }
    
    /**
     * Create and insert into database. This method handles the association table.
     * @param borrowerId The borrower ID to set.
     * @param copyId The copy ID to set.
     * @return This, with information retrieved from database.
     * @throws CreateException If the record cannot be inserted, or the data
     * cannot subsequently be retrieved.
     */
    public ServiceRequestContact createWithBorrowerAssoc(int requestId, int borrowerId, int copyId)
            throws CreateException {
        
        ServiceRequestContactPK spk = createPrimaryKey(requestId, copyId);
        
        
        String assocSql = "INSERT INTO BorrowerRequestAssoc (requestId, copyId, borrowerId, institutionProfileId)"
                + " VALUES ("   + sqlStringFrom(spk.getRequestId()) + ", "
                + sqlStringFrom(copyId) + ", " + sqlStringFrom(borrowerId) 
                + ", " + srk.getExpressState().getDealInstitutionId() + ")";

        int key = 0;
        // create in assoc table.
        try {
            key = jExec.executeUpdate(assocSql);
            // create in Service Request Contact table
            create(spk, borrowerId, copyId);
        }
        catch (Exception e) {
            CreateException ce = new CreateException("ServiceRequestContact Entity - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            
            throw ce;
        }
        finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch(Exception e) {
                logger.error(e);
            }
        }
        this.haveAssoc = true;
        this.borrowerId = borrowerId;
        this.copyId = copyId;
        srk.setModified(true);
        return this;
    }
    
    /**
     * Tests whether this ServiceProvider is equal to the given (ServiceProvider)
     * object.
     * @param obj The Object to test for equality.
     */
    public boolean equals(Object obj) {
        if (obj instanceof ServiceRequestContact) {
        ServiceRequestContact otherObj = (ServiceRequestContact) obj;
        if  (this.getServiceRequestContactId() == otherObj.getServiceRequestContactId() &&
                 this.getRequestId() == otherObj.getRequestId()) {
            return true;
        }
      }
        return false;   
    }
    
    /**
     * Populates entity from database.
     * @param pk Primary Key for the relevant row in the ServiceRequestContact table. 
     * @return This (populated) <code>ServiceRequestContact</code>.
     * @throws FinderException
     */
    public ServiceRequestContact findByPrimaryKey(ServiceRequestContactPK pk) 
            throws FinderException {    
    	
    if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM SERVICEREQUESTCONTACT" + pk.getWhereClause();

    boolean gotRecord = false;

    try {
      int key = jExec.execute(sql);

      for (; jExec.next(key);) {
        gotRecord = true;
        this.pk = pk;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "ServiceRequestContact Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("ServiceRequestContact Entity - findByPrimaryKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
	ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  } 
    
    public ServiceRequestContact findByBorrowerIdAndCopyId(int borrowerId, int copyId) 
            throws Exception { 
        StringBuffer sqlb = new StringBuffer("SELECT * FROM SERVICEREQUESTCONTACT");
        sqlb.append(" WHERE BORROWERID = ");
        sqlb.append(sqlStringFrom(borrowerId));
        sqlb.append(" AND COPYID = ");
        sqlb.append(sqlStringFrom(copyId));
        
        boolean gotRecord = false;
        String sql = new String(sqlb);
        
        try {
            int key = jExec.execute(sql);
            
            for(; jExec.next(key); ) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break;
            }
            
            jExec.closeData(key);
            
            if (gotRecord == false) {
                String msg = "ServiceRequestContact entity: @findByBorrowerIdAndCopyId(), borrowerId = " +
                borrowerId + ", copyId = " + copyId + ", entity not found.";
                
                if (getSilentMode() == false)
                    logger.error(msg);
                
                throw new FinderException(msg);
            }
        }
        catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException) e;
            
            FinderException fe = new FinderException("Deal Entity - findByPrimaryProperty() exception");
            
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        
        this.pk = new ServiceRequestContactPK(serviceRequestContactId, requestId, copyId);
        
        return this;            
    }

   // Catherine: 22-Mar-06 ----------- begin -----------  
  public ServiceRequestContact findLastByRequestId(int requestId) throws FinderException
  {
    
    /*
    StringBuffer sqlb = new StringBuffer("SELECT * FROM SERVICEREQUESTCONTACT");
    sqlb.append(" WHERE REQUESTID = ");
    sqlb.append(sqlStringFrom(requestId));
    sqlb.append(" ORDER BY SERVICEREQUESTCONTACTID DESC"); 
    String sql = sqlb.toString();
    */

    String sql = "SELECT * FROM SERVICEREQUESTCONTACT WHERE REQUESTID = ? ORDER BY SERVICEREQUESTCONTACTID DESC";
    
    boolean gotRecord = false;

    try {
      
      PreparedStatement pstmt = jExec.getPreparedStatement(sql);
      pstmt.setInt(1, requestId);
      
      // int key = jExec.execute(sql);
      int key = jExec.executePreparedStatement(pstmt, sql);
      
      for (; jExec.next(key);) {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // get the first record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "ServiceRequestContact Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("ServiceRequestContact Entity - findByPrimaryKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }

    return this;    
  }
  // Catherine: 22-Mar-06 ----------- end -------------  
  
    /**
     * Get the table's name.
     * @return The table's name.
     */
    public String getEntityTableName() {
        return "ServiceRequestContact";
    }
    
    /**
     * Get this Contact's PK.
     * @return This Primary Key.
     */
    public IEntityBeanPK getPk() {
        return pk;
    }
    
    /**
     *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName) {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  
  /**
   * Performs a database update.
   */
  protected int performUpdate() throws Exception {
    Class clazz = this.getClass();

    int ret = doPerformUpdate(this, clazz);

    return ret;
  }
  
  private void setPropertiesFromQueryResult(int key) throws Exception{
    setRequestId(jExec.getInt(key, "REQUESTID"));
    setServiceRequestContactId(jExec.getInt(key, "SERVICEREQUESTCONTACTID"));
    setCopyId(jExec.getInt(key, "COPYID"));
    setBorrowerId(jExec.getInteger(key, "BORROWERID"));
    setFirstName(jExec.getString(key, "NAMEFIRST"));
    setLastName(jExec.getString(key, "NAMELAST"));
    setEmailAddress(jExec.getString(key, "EMAILADDRESS"));
    setWorkAreaCode(jExec.getString(key, "WORKAREACODE"));
    setWorkPhoneNumber(jExec.getString(key, "WORKPHONENUMBER"));
    setWorkExtension(jExec.getString(key, "WORKEXTENSION"));
    setCellAreaCode(jExec.getString(key, "CELLAREACODE"));
    setCellPhoneNumber(jExec.getString(key, "CELLPHONENUMBER"));
    setHomeAreaCode(jExec.getString(key, "HOMEAREACODE"));
    setHomePhoneNumber(jExec.getString(key, "HOMEPHONENUMBER"));
    setPropertyOwnerName(jExec.getString(key, "PROPERTYOWNERNAME"));
    setComments(jExec.getString(key, "COMMENTS"));
    setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
  }
  
    //------- BEGIN: getters and setters -----------------------------------------
    /**
     * @return Returns the borrower ID.
     */
    public int getBorrowerId() {
        return borrowerId;
    }

    /**
     * @return Returns the contact's cell area code.
     */
    public String getCellAreaCode() {
        return cellAreaCode;
    }

    /**
     * @return Returns the contact's cell phone number (without area code).
     */
    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    /**
     * @return Returns this contact's comments.
     */
    public String getComments() {
        return comments;
    }

    /**
     * @return Returns the deal's copy ID.
     */
    public int getCopyId() {
        return copyId;
    }

    /**
     * @return Returns the contact's email address.
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @return Returns the contact's first name.
     */
    public String getFirstName() {
        return nameFirst;
    }

    /**
     * @return Returns the contact's home area code.
     */
    public String getHomeAreaCode() {
        return homeAreaCode;
    }

    /**
     * @return Returns the contact's home phone number (without area code).
     */
    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    /**
     * @return Returns the contact's last name.
     */
    public String getLastName() {
        return nameLast;
    }

    /**
     * @return Returns the service request contact ID.
     */
    public int getServiceRequestContactId() {
        return serviceRequestContactId;
    }

    /**
     * @return Returns the service request ID.
     */
    public int getRequestId() {
        return requestId;
    }
    
    /**
     * @return Returns the contact's work area code.
     */
    public String getWorkAreaCode() {
        return workAreaCode;
    }

    /**
     * @return Returns the contact's work phone number (without area code).
     */
    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }
    
    /**
     * @return Returns the work extension.
     */
    public String getWorkExtension() {
        return workExtension;
    }

    /**
     * Get the value of the property owner name
     */
    public String getPropertyOwnerName() {
        return propertyOwnerName;
    }

    /**
     * Set the value of the property owner name
     */
    public void setPropertyOwnerName(String propertyOwnerName) {
        testChange("propertyOwnerName", propertyOwnerName);
        this.propertyOwnerName = propertyOwnerName;
    }

    /**
     * @param borrowerId The borrower ID to set.
     */
    public void setBorrowerId(int borrowerId) {

        setBorrowerId(new Integer(borrowerId));
    }
    public void setBorrowerId(Integer borrowerId) {

        testChange("borrowerId", borrowerId);
        if (borrowerId != null) {
            this.borrowerId = borrowerId.intValue();
        }
    }

    /**
     * @param cellAreaCode The cell area code to set.
     */
    public void setCellAreaCode(String cellAreaCode) {
        testChange("cellAreaCode", cellAreaCode);
        this.cellAreaCode = cellAreaCode;
    }

    /**
     * @param cellPhoneNumber The cell phone number (without area code) to set.
     */
    public void setCellPhoneNumber(String cellPhoneNumber) {
        testChange("cellPhoneNumber", cellPhoneNumber);
        this.cellPhoneNumber = cellPhoneNumber;
    }

    /**
     * @param comments The contact's comments to set.
     */
    public void setComments(String comments) {
        testChange("comments", comments);
        this.comments = comments;
    }

    /**
     * @param copyId The copy ID to set.
     */
    public void setCopyId(int copyId) {
        testChange("copyId", copyId);
        this.copyId = copyId;
    }

    /**
     * @param emailAddress The email address to set.
     */
    public void setEmailAddress(String emailAddress) {
        testChange("emailAddress", emailAddress);
        this.emailAddress = emailAddress;
    }

    /**
     * @param firstName The first name to set.
     */
    public void setFirstName(String firstName) {
        testChange("nameFirst", firstName);
        this.nameFirst = firstName;
    }

    /**
     * @param homeAreaCode The home area code to set.
     */
    public void setHomeAreaCode(String homeAreaCode) {
        testChange("homeAreaCode", homeAreaCode);
        this.homeAreaCode = homeAreaCode;
    }

    /**
     * @param homePhoneNumber The home phone number (without area code) to set.
     */
    public void setHomePhoneNumber(String homePhoneNumber) {
        testChange("homePhoneNumber", homePhoneNumber);
        this.homePhoneNumber = homePhoneNumber;
    }

    /**
     * @param lastName The last name to set.
     */
    public void setLastName(String lastName) {
        testChange("nameLast", lastName);
        this.nameLast = lastName;
    }

    /**
     * @param serviceRequestContactId The service request contact ID to set.
     */
    public void setServiceRequestContactId(int serviceRequestContactId) {
        testChange("serviceRequestContactId", serviceRequestContactId);
        this.serviceRequestContactId = serviceRequestContactId;
    }

    /**
     * @param serviceRequestId The service request ID to set.
     */
    public void setRequestId(int requestId) {
        testChange("requestId", requestId);
        this.requestId = requestId;
    }
    
    /**
     * @param workAreaCode The work area code to set.
     */
    public void setWorkAreaCode(String workAreaCode) {
        testChange("workAreaCode", workAreaCode);
        this.workAreaCode = workAreaCode;
    }

    /**
     * @param workPhoneNumber The work phone number (without area code) to set.
     */
    public void setWorkPhoneNumber(String workPhoneNumber) {
        testChange("workPhoneNumber", workPhoneNumber);
        this.workPhoneNumber = workPhoneNumber;
    }
    
    /**
     * @param workExtension The work extension to set.
     */
    public void setWorkExtension(String workExtension) {
        testChange("workExtension", workExtension);
        this.workExtension = workExtension;
    }
    //------- END: getters and setters -------------------------------------------


    public Collection findByRequest(RequestPK pk) throws Exception
    {
      Collection servreqs = new ArrayList();

      String sql = "Select * from ServiceRequestContact ";
      sql +=  pk.getWhereClause();

      try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              ServiceRequestContact servreq = new ServiceRequestContact(srk);
              servreq.setPropertiesFromQueryResult(key);
              servreq.pk =
                  new ServiceRequestContactPK(servreq.
                                              getServiceRequestContactId(),
                                              servreq.getRequestId(),
                                              servreq.getCopyId());
              servreqs.add(servreq);
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Request::ServiceRequest");
          logger.error(fe.getMessage());
          logger.error(e);
          throw fe;
        }

        return servreqs;
    }

public void cutBorrowerAssoc() throws Exception {

        String sql = "UPDATE ServiceRequestContact SET borrowerid = null "
            + "WHERE requestid = " + getRequestId() + " AND "
            + "copyid = " + getCopyId();

        try {
            jExec.executeUpdate(sql);
        } catch (Exception e) {
            logger.error("Error on remove sons: ");
            throw new Exception("Error on remove sons: ", e);
        }
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    
    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
}
