
package com.basis100.deal.entity;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris Created on Oct 29, 2004
 * 
 */
public class AutoDecision extends DealEntity implements Cloneable {

    private int dealId;
    private int attemptNum;
    private int status;
    private int institutionProfileId;

    private DealPK pk;

    public AutoDecision(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException {

        super(srk, dcm);
    }

    public AutoDecision(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    /**
     * @return Returns the dealId.
     */
    public int getDealId() {

        return dealId;
    }

    /**
     * @return Returns the helixAttemptNum.
     */
    public int getAttemptNum() {

        return attemptNum;
    }

    /**
     * @return Returns the status.
     */
    public int getStatus() {

        return status;
    }

    /**
     * @param dealId
     *            The dealId to set.
     */
    public void setDealId(int dealId) {

        this.testChange("dealId", dealId);
        this.dealId = dealId;
    }

    /**
     * @param helixAttemptNum
     *            The attemptNum to set.
     */
    public void setAttemptNum(int attemptNum) {

        this.testChange("attemptNum", attemptNum);
        this.attemptNum = attemptNum;
    }

    /**
     * @param status
     *            The status to set.
     */
    public void setStatus(int status) {

        this.testChange("status", status);
        this.status = status;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.basis100.deal.entity.DealEntity#getEntityTableName() *
     */
    public String getEntityTableName() {

        return "AutoDecision";
    }

    /**
     * findByDealId
     * 
     * @param pk
     *            DealPK
     * @throws RemoteException
     * @throws FinderException
     * @return Collection
     */
    public AutoDecision findByDealId(int dealId) throws RemoteException,
            FinderException {

        String sql = "Select * from " + getEntityTableName()
                + " where dealId = " + dealId;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "AutoDecision Entity: @findByDealId(), key= "
                        + dealId + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException();
            logger.error(e.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

    public AutoDecision create(int dealId) {

        String sql = "insert into AutoDecision (DealId, Status, AttemptNum,INSTITUTIONPROFILEID) values("
                + dealId
                + ", "
                + 0
                + ", "
                + 0
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {

            int key = jExec.execute(sql);
            findByDealId(dealId);
            trackEntityCreate();
            jExec.closeData(key);
            trackEntityCreate();
        } catch (Exception e) {

            //CreateException ce = new CreateException(
            //        "AutoDecision Entity - create() exception");
            logger.error(e.getMessage());
            logger.error("creation sql: " + sql);
            logger.error(e);
        }

        return this;
    }

    // --> Billy ==> Mike : consider to override the parent's "ejbStore" method
    // for consistance :
    // --> No parameter input needed. ==> Review Implementation Note @ top.
    // --> 08Nov2004

    protected int performUpdate() throws Exception {

        logger.debug("====Performing update====");
        Class clazz = this.getClass();

        int ret = doPerformUpdate(this, clazz);
        return ret;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.basis100.deal.entity.DealEntity#doPerformUpdate(java.lang.Object,
     *      java.lang.Class)
     */
    protected int doPerformUpdate(Object me, Class meInfo) throws Exception {

        StringBuffer buffer = new StringBuffer();

        buffer.append("Update " + getEntityTableName() + " set ");

        if (formUpdateFieldsList(buffer, true) == null) {
            return 0;
        }

        buffer.append(" where dealId = " + dealId);

        String sql = new String(buffer);
        int ret = jExec.executeUpdate(sql);

        return ret;
    }

    public void ejbRemove(boolean needCalculation) throws RemoteException {

        String sql = null;

        try {

            sql = "Delete From " + this.getEntityTableName()
                    + " where dealId = " + getDealId();

            jExec.executeUpdate(sql);

            this.trackEntityRemoval(); // Track the Removal

            if (dcm == null) {
                return; // if the dcm is null then we don't need to visit the
                // rest of this method - brad
            }

            if (needCalculation) { // If need Calculation ..........
                dcm.inputRemovedEntity(this);
                dcm.removeTarget(this);
                dcm.calc();
            }
            dcm.removeInput(this);
        } catch (Exception e) {
            String msg = "Failed to remove record " + "where dealId = "
                    + getDealId();
            RemoteException re = new RemoteException(msg);
            logger.error(re.getMessage());
            logger.error("remove sql: " + sql);
            logger.error(e);

            throw re;
        }

    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

        this.setDealId(jExec.getInt(key, "DEALID"));
        this.setStatus(jExec.getInt(key, "STATUS"));
        this.setAttemptNum(jExec.getInt(key, "ATTEMPTNUM"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    /**
     * This method assumes the findBy... had been invoked, and a record has
     * already been found.
     * 
     * @throws RemoteException
     */
    public void incrementAttempt() throws RemoteException {

        int num = getAttemptNum();
        num += 1;
        setAttemptNum(num);
        ejbStore();
    }

    public void incrementAttempt(int dealId) throws RemoteException,
            FinderException {

        findByDealId(dealId);
        int num = getAttemptNum();
        num += 1;
        setAttemptNum(num);
        ejbStore();
    }

    public String toString() {

        return getDealId() + " / " + getStatus() + " / " + getAttemptNum();
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}