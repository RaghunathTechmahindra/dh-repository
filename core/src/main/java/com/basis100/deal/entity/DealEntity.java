package com.basis100.deal.entity;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.lang.reflect.Field;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.DisplayFieldPicklist;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.express.datasource.ExpressConnectionHelper;

/** Superclass for deal related enties
 *
 * <p>Title: FXpressJ2EE</p>
 * <p>Description: FXpress NetDynamics project converted to the J2EE realm with the JATO as an iPlanet Application Framework.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Filogix</p>
 * @author not attributable
 * @version 1.0
 *
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 * 01/Aug/2007 DVG #DG614 deleting masterDeal does not remove DocumentQueue records
 * 06/Jan/2005 DVG #DG124 cr#136 INSUREONLYAPPLICANT ingestion, minor correction
 */
public class DealEntity extends EntityBase implements Cloneable
{  
   // this preseve the value of the entity property,
   protected Map changeValueMap = new HashMap();

   public static final String YES = "Y";
   public static final String NO  = "N";
   public static final int INITIAL_COPY_ID = 1;
   public static final int DEFAULT_COPY_VALUE = 1;

   // runtime private member (always false unless explicitly set)
   protected boolean propagateChanges = false;

   protected boolean auditable = false;

   public CalcMonitor dcm;

   public DealEntity(){}

   public DealEntity(SessionResourceKit srk)throws RemoteException
   {
      super(srk);
   }

   public DealEntity(SessionResourceKit srk, CalcMonitor calcMon)throws RemoteException
   {
      super(srk);
      dcm = calcMon;
   }

  /**
   * ejbStore stores the present state of the entity in the database. This accomplished
   * whith a call to the performUpdate method implementation of the calling class.
   *
   * @throws RemoteException
   */
   public void ejbStore() throws RemoteException
   {
	   if ( !isDataChanged() ) return;

	   srk.setModified(true);

	   // FXP32254
	   if(dcm == null) 
		   logger.error("dcm object is null - monitor not provided");

	   try
	   {
		   performUpdate();
		   this.trackEntityChange ();  // Track all the change
		   
		   // 4.4 Entity Cache -- start
		   ThreadLocalEntityCache.writeToCache(this, this.getPk());
		   // 4.4 Entity Cache -- end
	   }
	   catch (Exception e)
	   {
		   //Aug 14, 2009, Hiro - changed to return meaningful error message
		   StringBuilder msg = new StringBuilder();
		   msg.append("DealEntity#ejbStore");
		   msg.append("(").append(this.getClass().getName()).append(")");
		   msg.append(" exception = [").append(e.getMessage()).append("]");

		   logger.error(msg.toString());
		   logger.error(e);

		   throw new RemoteException(msg.toString());
	   }
   }
  
  protected int doPerformUpdate(Object me, Class meInfo) throws Exception
   {
      StringBuffer buffer = new StringBuffer();

      buffer.append("Update " + getEntityTableName() + " set ");

      logger.debug("DealEntity@EntityTableNamesBuffer: " + buffer.toString());

      // Modified by to return immediately if nothing to change -- by BILLY 25Feb2002
      if(formUpdateFieldsList(buffer, true) == null)
      {
        return 0;
      }

/*
      String currentKey = null;
      try
      {
        buffer.append("Update " + getEntityTableName() + " set ");
        Iterator cmv = changeValueMap.values().iterator();
        ChangeMapValue mapVal = null;
        int i = 0;
        while(cmv.hasNext())
        {
           mapVal = (ChangeMapValue)cmv.next();
           if(mapVal.getChangeFlag())
           {
             if(i > 0 )
                buffer.append(", ");

             buffer.append(mapVal.getPropertyName());
             buffer.append(" = ");
             buffer.append(mapVal.getCurValue());

             i++;
           }
        }
      }
      catch(Exception e)
      {
        String msg =  "PerformUpdate error - info:\n ";
        msg += "Supplied Field String: " + currentKey;
        msg += "\n Exception.getMessage() - " + e.getMessage();
        throw new Exception(msg);
      }
*/

      buffer.append(getPk().getWhereClause());

      logger.debug("DealEntity@PK_WhereClause: " + getPk().getWhereClause());
      logger.debug("DealEntity@EntityTableNamesBuffer_AfterPK: " + buffer.toString());

      String sql = new String(buffer);
      int ret = jExec.executeUpdate(sql);

      return ret;
   }

   /**
    * Same as other doPerformUpdates except the call to formUpdateFieldsList() sends false as the second arg.
    * Under normal circumstances, this should not be used.
    * @return int
    * @throws Exception
    */
   protected int doPerformUpdateForce() throws Exception {
     //Object me = this;
     //Class meInfo = me.getClass();
     StringBuffer buffer = new StringBuffer();

     buffer.append("Update " + getEntityTableName() + " set ");

     if(formUpdateFieldsList(buffer, false) == null)
     {
       return 0;
     }

     buffer.append(getPk().getWhereClause());

     String sql = new String(buffer);
     int ret = jExec.executeUpdate(sql);

     return ret;
   }

  protected int doPerformUpdate(String[] arrExclude) throws Exception
  {
     StringBuffer buffer = new StringBuffer();

     buffer.append("Update " + getEntityTableName() + " set ");

     logger.debug("DealEntity@EntityTableNamesBuffer: " + buffer.toString());

     if(formUpdateFieldsList(buffer, true, arrExclude) == null)
     {
       return 0;
     }

     buffer.append(getPk().getWhereClause());

     logger.debug("DealEntity@PK_WhereClause: " + getPk().getWhereClause());
     logger.debug("DealEntity@EntityTableNamesBuffer_AfterPK: " + buffer.toString());

     String sql = new String(buffer);
     int ret = jExec.executeUpdate(sql);

     return ret;
  }

  /**
   * Form the comma delimited list if fields and values for the sql update operation. The entity change map is
   * interrogated for fields updated - typically only changed fields are required (see doPerformUpdate()) but
   * all fields can be specified by setting changedOnly = false in call.
   *
   * @param buffer StringBuffer
   * @param changedOnly boolean
   * @return StringBuffer
   */

   protected StringBuffer formUpdateFieldsList(StringBuffer buffer, boolean changedOnly)
   {
      // create new buffer if not provided
      if (buffer == null)
        buffer = new StringBuffer();

      Iterator cmv = changeValueMap.values().iterator();

      ChangeMapValue mapVal = null;

      int i = 0;

      while(cmv.hasNext())
      {
         mapVal = (ChangeMapValue)cmv.next();

         if(mapVal.getChangeFlag() || changedOnly == false)
         {
           if(i > 0 )
              buffer.append(", ");

           buffer.append(mapVal.getPropertyName());
           buffer.append(" = ");
           buffer.append(mapVal.getCurValue());

           i++;
         }
      }

      // Modified to return NULL if NO field changed -- BY BILLY 25Feb2002
      if(i == 0)
        return null;
      else
        return buffer;
   }

   /**
    * Form the comma delimited list if fields and values for the sql update operation. The entity change map is
    * interrogated for fields updated - typically only changed fields are required (see doPerformUpdate()) but
    * all fields can be specified by setting changedOnly = false in call.
    * Does not include fields that are in the excludeList
    *
    * @param buffer StringBuffer
    * @param changedOnly boolean
    * @param excludeArray String[]
    * @return StringBuffer
    */
    protected StringBuffer formUpdateFieldsList(StringBuffer buffer, boolean changedOnly, String[] excludeArray)
    {
        boolean isIncluded = true;
        boolean isSettingFields = false;
        int k = 0;
        // create new buffer if not provided
        if (buffer == null)
         buffer = new StringBuffer();

       Iterator cmv = changeValueMap.values().iterator();

       ChangeMapValue mapVal = null;

       int i = 0;

       while(cmv.hasNext())
       {
          mapVal = (ChangeMapValue)cmv.next();

          if(mapVal.getChangeFlag() || changedOnly == false)
          {
            if(i > 0 )

/*
 *  Do not include fields that are in the excludeArray
 */
            isIncluded = true;
            for (int j = 0; j < excludeArray.length; j++) {
                if (mapVal.getPropertyName().toUpperCase().equals(excludeArray[j].toUpperCase()))
                {
                    isIncluded = false;
                }
            }
            if (isIncluded)
            {
                if (k > 0) {
                    buffer.append(", ");
                }
                buffer.append(mapVal.getPropertyName());
                buffer.append(" = ");
                buffer.append(mapVal.getCurValue());
                // a field is to be updated
                isSettingFields = true;
                k++;
            }
            i++;
          }
       }

       // return NULL if NO field changed
       if(!isSettingFields)
         return null;
       else
         return buffer;
    }

   protected void doSetField(DealEntity target, Class self, String fieldName, String value) throws Exception
   {
      try
      {
    	  
          Field field = self.getDeclaredField(fieldName);

          logger.debug("DealEntity.doSetField entity=[" + self.getSimpleName() + "] field = [" + fieldName + "] value=[" + value + "]");
          
          /*#DG608 added clob and optimized, the order DOES make a difference 

          String type = field.getType().getName();
          if(type == String.class || type == Clob.class)  		 {
                field.set(target,value);
                this.testChange ( fieldName, value );
             }
          else if(type == Class.forName("int"))  		 {
                int iValue = TypeConverter.intTypeFrom(value) ;
                field.setInt(target, iValue );
                this.testChange ( fieldName, iValue );
             }
          else if(type == Class.forName("short"))  		 {
                 short sValue = TypeConverter.shortTypeFrom ( value ) ;
                 field.setShort(target, sValue );
                 this.testChange ( fieldName, sValue );
              }
          else if(type == Class.forName("long"))  		 {
                 long lValue = TypeConverter.longTypeFrom ( value ) ;
                 field.setLong(target,  lValue );
                 this.testChange( fieldName, lValue ) ;
              }
          else if(type == Class.forName("double"))  		 {
                 double dValue = TypeConverter.doubleTypeFrom ( value ) ;
                 field.setDouble(target, dValue );
                 this.testChange( fieldName, dValue );
             }
          else if(type == java.sql.Date.class || type == java.util.Date.class)  		 {
                 Date daValue = TypeConverter.dateFrom(value) ;
                 field.set(target, daValue );
                 this.testChange( fieldName, daValue );
              }
          else if(type == Class.forName("char"))  		 {
                 char cValue = TypeConverter.charTypeFrom(value) ;
                 field.setChar(target, cValue );
                 this.testChange( fieldName, cValue ) ;
              }
          else if(type == Class.forName("float"))  		 {
                 float fValue = TypeConverter.floatTypeFrom ( value ) ;
                 field.setFloat(target, fValue );
                 this.testChange( fieldName, fValue ) ;
              }
          else if(type == Class.forName("boolean"))  		 {
                 boolean bValue = TypeConverter.booleanFrom(value,"N");
                 field.setBoolean(target, bValue  );
                 this.testChange( fieldName, bValue ) ;
              }
          else
              throw new Exception("Field type not found");*/
				
          Class type = field.getType();
  		 
          if(type == String.class)  		 {
                 field.set(target,value);
                 this.testChange ( fieldName, value );
              }
          else if(type == Integer.TYPE)  		 {
                 int iValue = TypeConverter.intTypeFrom(value) ;
                 field.setInt(target, iValue );
                 this.testChange ( fieldName, iValue );
              }
          else if(type == Double.TYPE)
          {
                 double dValue = TypeConverter.doubleTypeFrom ( value ) ;
                 field.setDouble(target, dValue );
                 this.testChange( fieldName, dValue );
              }
          else if(type == java.sql.Date.class || type == java.util.Date.class)
          {
                 Date daValue = TypeConverter.dateFrom(value) ;
                 field.set(target, daValue );
                 this.testChange( fieldName, daValue );
              }
          else if(type == Boolean.TYPE)  		 {
                  boolean bValue = TypeConverter.booleanFrom(value,"N");
                  field.setBoolean(target, bValue  );
                  this.testChange( fieldName, bValue ) ;
              }
          else if(type == Character.TYPE)  		 {
                 char cValue = TypeConverter.charTypeFrom(value) ;
                 field.setChar(target, cValue );
                 this.testChange( fieldName, cValue ) ;
              }
          else if(type == Clob.class)  		 {  // @TODO if ever needed ... does not work as is
                 field.set(target,value);
                 this.testChange ( fieldName, value );
              }
          else if(type == Short.TYPE)  		 {
                 short sValue = TypeConverter.shortTypeFrom ( value ) ;
                 field.setShort(target, sValue );
                 this.testChange ( fieldName, sValue );
              }
          else if(type == Long.TYPE)  		 {
                 long lValue = TypeConverter.longTypeFrom ( value ) ;
                 field.setLong(target,  lValue );
                 this.testChange( fieldName, lValue ) ;
              }
          else if(type == Float.TYPE)  		 {
                 float fValue = TypeConverter.floatTypeFrom ( value ) ;
                 field.setFloat(target, fValue );
                 this.testChange( fieldName, fValue ) ;
              }
          else
             throw new Exception("Field type not found");
      }
      catch(Exception e)
      {
        String msg = "Unable to set Field: " + fieldName + " due to: " + e;  //#DG124 minor correction
        logger.error(msg);
        throw new Exception(msg);
      }
   }

  protected String doGetStringValue(Object target, Class self, String fieldName)
  {
     try
     {
        Field field = self.getDeclaredField(fieldName);

        String type = field.getType().getName();

        if(type.equals("java.lang.String"))
            return TypeConverter.stringTypeFrom( (String)field.get(target));
        else if(type.equals("int"))
            return TypeConverter.stringTypeFrom(field.getInt(target));
        else if(type.equals("short"))
            return TypeConverter.stringTypeFrom(field.getShort(target));
        else if(type.equals("long"))
            return TypeConverter.stringTypeFrom(field.getLong(target));
        else if(type.equals("double"))
            return TypeConverter.stringTypeFrom(field.getDouble(target));
        else if(type.equals("java.sql.Date"))
            return TypeConverter.stringTypeFrom((java.sql.Date)field.get(target));
        else if(type.equals("java.util.Date"))
            return TypeConverter.stringTypeFrom((java.util.Date)field.get(target));
        else if(type.equals("char"))
            return TypeConverter.stringTypeFrom(field.getChar(target));
        else if(type.equals("float"))
            return TypeConverter.stringTypeFrom(field.getFloat(target));
        else if(type.equals("boolean"))
            return TypeConverter.stringFromBoolean(field.getBoolean(target),YES,NO);
        else
            return null;

      }
      catch(Exception ex)
      {
         //field not found
         return null;
      }
  }

  private int createEntityId(String tablename)throws Exception
  {

    String sql = "Select " + tablename + "seq.nextval from dual";
    int id = -1;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key);  )
      {
         id = jExec.getInt(key,1);  // can only be one record
      }

      jExec.closeData(key);
      if( id == -1 ) throw new Exception();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("DealEntity.createEntityId exception getting new id from sequence " + tablename + "seq");
      logger.error(ce.getMessage());
      logger.error(e);
      throw ce;
    }

    return id;
  }

  /**
   * this method has a purpose to return all entites associated to a deal through
   * <entity>deal association table.
   *
   * @throws Exception
   * @return List
   */

  public List getAssociatedEntities() throws Exception
  {
    List retEntitiesList = new ArrayList();

    return retEntitiesList;
  }

  /**
   *   Default implementation - appropriate for entity with no 'children'.
   * Mercator relies on the order of the calls int the implementations of this method.
   * Any new calls to addAll() should be place after existing calls.
   *
   * @throws Exception
   * @return List
   */

  public List getChildren() throws Exception
  {
    List children = new ArrayList();

    return children;
  }

  /**
   *   Default implementation - appropriate for entity with no 'children'.
   *
   * @throws Exception
   * @return List
   */

  public List getSecondaryParents() throws Exception
  {
    List sps = new ArrayList();

    return sps;
  }


  /**
   *   Default implementation.
   *
   *   @ param destParent - the MasterDeal owner of the new copy
   *
   * @param srcMD MasterDeal
   * @param destMD MasterDeal
   * @param parentName String
   * @param keyId String
   * @param copyId int - the copyId of the new copy
   * @throws Exception
   */
  public void copyEntity(MasterDeal srcMD, MasterDeal destMD, String parentName, String keyId, int copyId) throws Exception
  {
    // !!! DO NOT CHANGE ORDER OF STATEMENTS  !!!

    copySecondaryParents(srcMD, destMD, copyId);

    String parentKey = doCopy(srcMD, this, destMD, parentName, keyId, copyId);

    copyChildren(srcMD, destMD, this.getEntityTableName(), parentKey, copyId);
  }


    /**
     *   Default implementation.
     *
     *   @ param destParent - the MasterDeal owner of the new copy
     *
     * @param srcParent MasterDeal
     * @param destParent MasterDeal
     * @param parentName String
     * @param parentKey String
     * @param copyId int - the copyId of the new copy
     * @throws Exception
     */
    public void copyChildren(MasterDeal srcParent, MasterDeal destParent,
                             String parentName, String parentKey, int copyId)
        throws Exception {

        // copy assocs who are special children of this entity.
        //copyChildAssocs(copyId);
    	copyChildAssocs(parentKey, copyId);

        List children = getChildren();
        Iterator it = children.iterator();

        while(it.hasNext()) {
            DealEntity de = (DealEntity)it.next();
            de.copyEntity(srcParent, destParent, parentName, parentKey, copyId);
        }
    }

    /**
     * copy child assocs.
     */
    protected void copyChildAssocs(int newCopyId) throws Exception {

        // default implementation is do nothing!
    }

    /**
     * copy child assocs.
     * Added this signature to differentiate between transaction copy & new deal
     */
    protected void copyChildAssocs(String parentKey, int newCopyId) throws Exception {

        // default implementation is do nothing!
    }

   /**
   *   Default implementation - does nothing - override in subclasses that constitute
   *   the membership of the copy tree if they have copy tree secondary parents
   *   @ param destParent - the MasterDeal owner of the new copy

   * @param srcMD MasterDeal
   * @param destMD MasterDeal
   * @param copyId int - the copyId of the new copy
   * @throws Exception
   */

  public void copySecondaryParents(MasterDeal srcMD, MasterDeal destMD, int copyId)  throws Exception
  {
    List sps = getSecondaryParents();

    Iterator it = sps.iterator();

    while(it.hasNext())
    {
      DealEntity de = (DealEntity)it.next();

      de.copyEntity(srcMD, destMD, null, null, copyId);
    }
  }


  protected String doCopy(MasterDeal srcMD, DealEntity src, MasterDeal destMD, String parentName, String parentKey, int copyId) throws DealCopyException, Exception
  {
      String keyString = null;


      boolean copyInSameTree = (srcMD.getDealId() == destMD.getDealId());
////logger.debug("DealEntity@doCopySourceMdDealId: " + srcMD.getDealId());
////logger.debug("DealEntity@doCopyDestMdDealId: " + destMD.getDealId());

      String tableName = src.getEntityTableName();
      String pkname = tableName + "Id" ;
      String parentField = parentName + "Id";
      List vals = getParentNames(tableName);

////logger.debug("DealEntity@doCopyPKName: " + pkname);
////logger.debug("DealEntity@doCopyVals: " + vals.toString());

      Map cvm = src.getChangeValueMap();

      //buffer for property names
      StringBuffer buffer = new StringBuffer( "Insert into " + tableName + " ( ");
      buffer.append( " copyId " );

      //buffer for corresponding values
      StringBuffer valueBuffer = new StringBuffer( " ) Values ( " + copyId );

      Iterator it = cvm.values().iterator();

      ChangeMapValue current = null;
      String propertyName = null;
      String srcValue = null;



      while( it.hasNext())
      {
        current = (ChangeMapValue)it.next();

        propertyName = current.getPropertyName();
        srcValue = current.getCurValue();

        //skip over copyId
        if(propertyName.equals("copyId") ) continue;

        //Skip mortgageInsuranceResponse because it's a LONG field needed special handle
        //    -- By BILLY 04April2002
        if(propertyName.equals("mortgageInsuranceResponse") ) continue;
        //==============================================================================

        buffer.append(", ");
        buffer.append(propertyName);
        valueBuffer.append(", ");

        //support for copyFrom another parent: In any case the destination
        //dealId must be used while all other primary key values are fine if the
        // copy occurs in the same tree else they are replaced with new seq ids.
        if(copyInSameTree)
        {
          if(propertyName.equals("dealId") )
          {
            valueBuffer.append(destMD.getDealId());
          }
          else
          {
            valueBuffer.append(srcValue);
          }
        }
        else
        {
          if(propertyName.equalsIgnoreCase(pkname))   //handle primary key
          {
            if(tableName.equals("Deal"))
            {
              valueBuffer.append(destMD.getDealId());
            }
            else
            {
              int returnKey = createEntityId(tableName);
              keyString = String.valueOf(returnKey);

              srcMD.cacheSrcCopyValue(propertyName, srcValue, keyString);
              valueBuffer.append(keyString);
            }
          }
          else if(propertyName.equalsIgnoreCase(parentField)) //handle principle (passed in) foriegn key
          {
            valueBuffer.append(parentKey);
          }
          // -------------- Catherine, 16Feb05, BMO to CCAPS #972 -------------------
          //*
          else if(propertyName.equals("servicingMortgageNumber") && PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),"com.filogix.deal.copy.lender.notes","N").equalsIgnoreCase("Y"))
          {
              Deal origDeal = new Deal(srk, null, destMD.getDealId(), destMD.getGoldCopyId());                           
              if (origDeal != null){
                valueBuffer.append(origDeal.getServicingMortgageNumber());
              } else {
                valueBuffer.append(srcValue);
              }
          }
          // -------------- Catherine, 16Feb05, BMO to CCAPS #972 end ---------------
          else if(propertyName.equals("applicationId"))
          {
            valueBuffer.append(destMD.getDealId());
          }
          else  //handle other foriegn keys
          {
            if(!vals.isEmpty() && vals.contains(propertyName))
            {
              String value = srcMD.getCachedCopyValue(propertyName, srcValue);
              valueBuffer.append(value);
            }
            else
            {
              valueBuffer.append(srcValue);
            }
          }
        }
      }

      //String debug = valueBuffer.toString();

      buffer.append(valueBuffer + ")" );

      String sql = new String(buffer);

      logger.info("DealEntity - doCopy() : "+ sql);
      try
      {
        src.jExec.executeUpdate(sql);
      }
      catch(Exception s)
      {
        String msg = "Exception while performingCopy: Insert record exception: entity = " + tableName;
        logger.error(msg);
        logger.error("Exception doCopy: sql = <" + sql + ">");
        logger.error(s);

        throw new DealCopyException(msg, s);
      }

      if(keyString == null)
       keyString = String.valueOf(destMD.getDealId());

      return keyString;

  }

  //this can all be done with xml
  public List getParentNames(String childTable)
  {
      List retList = new ArrayList();

      if(childTable.equalsIgnoreCase("EmploymentHistory"))
      {
        retList.add("incomeId");
        retList.add("contactId");
        return retList;
      }
      else if(childTable.equalsIgnoreCase("Contact"))
      {
        retList.add("addrId");
        return retList;
      }
      else if(childTable.equalsIgnoreCase("BorrowerAddress"))
      {
        retList.add("addrId");
        return retList;
      }
      //--DJ_CR136--start--//
      else if(childTable.equalsIgnoreCase("InsureOnlyApplicant"))
      {
        retList.add("insureOnlyApplicantId");
        return retList;
      }
      //--DJ_CR136--end--//
      //PVCS Ticket # 1809 - begin //
      else if(childTable.equalsIgnoreCase("AdjudicationApplicantRequest"))
      {
        retList.add("borrowerId");
        return retList;
      }
      else if(childTable.equalsIgnoreCase("BncAdjudicationApplRequest"))
      {
        retList.add("requestId");
        return retList;
      }
      else if(childTable.equalsIgnoreCase("ServiceRequest"))
      {
        retList.add("requestId");
        retList.add("propertyId");
        return retList;
      }
      else if(childTable.equalsIgnoreCase("ServiceRequestContact"))
      {
        retList.add("borrowerId");
        return retList;
      }
      // PVCS Ticket # 1809 - end //
     return retList;
  }

  /**
   *   Default implementation.
   *
   * @param v Vector
   * @throws Exception
   * @return Vector
   */

  public Vector dumpEntity(Vector v)  throws Exception
  {
    if (v == null)
      v = new Vector();

    doDump(v);

    dumpChildren(v);
    dumpSecondaryParents(v);

    return v;
  }


  /**
   *   Default implementation.
   *
   * @param v Vector
   * @throws Exception
   * @return Vector
   */

  public Vector dumpChildren(Vector v)  throws Exception
  {
    List children = getChildren();

    Iterator it = children.iterator();

    while(it.hasNext())
    {
      DealEntity de = (DealEntity)it.next();

      de.dumpEntity(v);
    }

    return v;
  }

   /**
   *   Default implementation.
   *
   * @param v Vector
   * @throws Exception
   * @return Vector
   */

  public Vector dumpSecondaryParents(Vector v)  throws Exception
  {
    List sps = getSecondaryParents();

    Iterator it = sps.iterator();

    while(it.hasNext())
    {
      DealEntity de = (DealEntity)it.next();

      de.dumpEntity(v);
    }

    return v;
  }


  protected Vector doDump(Vector v)
  {

      String tableName = getEntityTableName();

      Map cvm = getChangeValueMap();

      if (v == null)
        v = new Vector();

      v.add(" ");
      v.add("Table: " + tableName);
      v.add(" ");

      Iterator it = cvm.values().iterator();

      ChangeMapValue current  = null;
      String propertyName     = null;
      String value            = null;

      while( it.hasNext())
      {
        current = (ChangeMapValue)it.next();
        propertyName  = current.getPropertyName();
        value         = current.getCurValue();

        v.add(" " + propertyName + " = {" + value + "}");
      }

      return v;
  }


  /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @ param  fieldName as String
   *  @ param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   *
   * @param fieldName String
   * @param value String
   * @throws Exception
   */
  public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this,this.getClass(), fieldName, value);
   }

  public Object getFieldValue( String fieldName){
    return doGetFieldValue(this, this.getClass(),  fieldName);
  }

    protected Object doGetFieldValue(Object target, Class self, String fieldName){
      try{
        Field field =  self.getDeclaredField(fieldName);
        if(field == null){ return null; }

        String type = field.getType().getName();

        if(type.equals("java.lang.String"))
            return field.get(this);
        else if(type.equals("int"))
            return field.get(this);
        else if(type.equals("short"))
            return field.get(this);
        else if(type.equals("long"))
            return field.get(this);
        else if(type.equals("double"))
            return field.get(this);
        else if(type.equals("java.sql.Date"))
            return field.get(this);
        else if(type.equals("java.util.Date"))
            return field.get(this);
        else if(type.equals("char"))
            return field.get(this);
        else if(type.equals("float"))
            return field.get(this);
        else if(type.equals("boolean"))
            return field.get(this);
        else
            return null;
      }catch(Exception exc){
        return null;
      }
    }


  /**
  *  gets the value of instance fields as String.
  *  if a field does not exist (or the type is not serviced)** null is returned.
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *  @returns value of bound field as a String;
  *  @param  fieldName as String
  *
  *  Other entity types are not yet serviced   ie. You can't get the Province object
  *  for province.
  */
  public String getStringValue(String fieldName)
  {
    return doGetStringValue(this, this.getClass(), fieldName);
  }


  public boolean isDataChanged()
  {
    Iterator cmit = changeValueMap.values().iterator();

    while ( cmit.hasNext())
    {
      ChangeMapValue cmv = ( ChangeMapValue ) cmit.next();
      if ( cmv.getChangeFlag () )
        return true ;
    }

    return false;
  }

  public Collection getChangedValues()
  {
    Iterator cmit = changeValueMap.values().iterator();
    List l = new ArrayList();

    while ( cmit.hasNext())
    {
      ChangeMapValue cmv = ( ChangeMapValue ) cmit.next();

      if ( cmv.getChangeFlag () )
      {
        l.add(cmv);
      }
    }

    return l;
  }


  public Collection getChangedFields()
  {
    Iterator cmit = changeValueMap.values().iterator();
    List l = new ArrayList();

    while ( cmit.hasNext())
    {
      ChangeMapValue cmv = ( ChangeMapValue ) cmit.next();

      if ( cmv.getChangeFlag () )
      {
        l.add(cmv.getPropertyName());
      }
    }

    return l;
  }



  public boolean isAuditable()
  {
    return false;
  }


  public int getClassId()
  {
    return ClassId.DEFAULT;
  }

  protected int performUpdate() throws Exception
  {
    return 0;
  }


  public String getEntityTableName()
  {
    return "";
  }

  public SessionResourceKit getSessionResourceKit()
  {
   return srk;
  }

  protected int getChangeMapSizeNew()
  {
   int size=0;
   Iterator cVM = this.changeValueMap.values().iterator();

   while ( cVM.hasNext() )
   {
      ChangeMapValue cmv = ( ChangeMapValue ) cVM.next() ;
      if ( cmv.getChangeFlag () )  size ++;
   }
   return size  ;
  }

  public Map getChangeValueMap()
  {
    return changeValueMap;
  }


  public IEntityBeanPK getPk()
  {
     return null;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    DealEntity other  = (DealEntity)object;
    return getPk().equals(other.getPk());
  }

  public int hashCode()
  {
    IEntityBeanPK pk = getPk();

    if (pk == null)
      return super.hashCode();

    return getPk().hashCode();
  }





  protected Date fetchApplicationDate()
  {
    String sql = "SELECT SYSDATE FROM DUAL";
    Date sysdate = null;
    try
    {

      int key = jExec.execute(sql);
      jExec.next(key);
      sysdate = jExec.getDate(key,"sysdate");
      jExec.closeData(key);

      return sysdate;
    }
    catch (Exception e)
    {
        logger.error("Exception @fetchApplicationDate()");
        logger.error(e);
    }

    // on exception return current (system) date
    return new Date();
  }

  public String sqlStringFrom(String in)
  {
    if(in == null)
      return "null";

    in = TypeConverter.stringTypeFrom(in);

    in = StringUtil.makeQuoteSafe(in);

    return "\'" + in + "\'";
  }

   public String sqlStringFrom(int in)
   {
      return String.valueOf(in);
   }

    public String sqlStringFrom(long l)
   {
      return String.valueOf(l);
   }

   public String sqlStringFrom(short in)
   {
      return String.valueOf((int)in);
   }

   public String sqlStringFrom(char c)
   {
     return "\'" + String.valueOf(c) + "\'";
   }

   public String sqlStringFrom(double d)
   {
      return String.valueOf(d);
   }

   public String sqlStringFrom(float f)
   {
      return String.valueOf(f);
   }

   public String sqlStringFrom(boolean b)
   {
     String value = "N";

     if(b)value = "Y";

     return "\'" + value + "\'";
   }

   /**
    * Default date format for MOS Oracle: YYYY-MM-DD HH24:MI:SS
    *
    * Note: This method assumes that date fields in the database are nullable. Hence
    *       the string "null" is returned if the date proided is null.
    *
    * @param d Date
    * @return String
    */
   public String sqlStringFrom(java.util.Date d)
   {
      String strRep = TypeConverter.stringTypeFrom(d);

      if(strRep == null)
        return "null";
      else
        return "\'" + strRep + "\'";
   }

   
   //START EDD:avm additions
   public String sqlStringFrom(byte[] bs) {
      return "empty_blob()";
   }
   //END EDD



   /**
   * Gets a <code>Vector</code> of property names that are to be copied (copy utils package).
   * Generally for an entity the list excludes only primary key fields.
   *
   * ALERT - A cache should be implemened to optimize retrieval of vectors.
   *
   * @return a Vector of Strings representing fields
   */
   public Vector getCopyFieldsVector()
   {
     Vector fldNames = new Vector();

     Vector excluded = getExcludedCopyFieldsVector();

     Iterator cVM = this.changeValueMap.values().iterator();

     while (cVM.hasNext())
     {
        ChangeMapValue cmv = (ChangeMapValue)cVM.next();

        String propName = cmv.getPropertyName();

        if (excluded.contains(propName))
          continue;

        fldNames.add(propName);
     }

     return fldNames;
   }

   //
   // Fields excluded from copy fields list. Generally only the <copyId> property
   // for an entity must be excluded from the copy fields list (e.g. copyies are
   // different only in the copyid property). Hence the getExcludedCopyFieldsVector()
   // method (below) returns only the copy id field.
   //
   // However should there ever be a need to exclude more fields for a particular
   // entity then the entity subclass can override getExcludedCopyFieldsVector().
   //

   private static Vector defaultExcludedCopyFields = new Vector();
   static
   {
      defaultExcludedCopyFields.add("copyId");
   }

   public Vector getExcludedCopyFieldsVector()
   {
      return defaultExcludedCopyFields;
   }

   public Vector getPropagateFieldsVector()
   {return new Vector();}

   public Vector findByMyCopies() throws RemoteException, FinderException
   {return new Vector();}


   public void setPropagateChanges(boolean b)
   {
       propagateChanges = b;
   }
     public boolean getPropagateChanges()
   {
      return this.propagateChanges;
   }



   //!!!!
/**
  *   @ returns an Enumeration of this Entities changeMap keys for fields that propagate.
  */
 /*
  public Vector getPropagateKeys()
  {
    if (propagateChanges == false)
      return null;

    Vector toPropagate = getPropagateFieldsVector();

    if( toPropagate == null)
      return null;
    Vector v = new Vector();

    Enumeration keys = getChangeMapKeysNew();

    while(keys.hasMoreElements())
    { 
       String currentKey = (String)keys.nextElement();
       if (((Boolean)this.changeMap.get(currentKey)).booleanValue() == true &&
        toPropagate.indexOf(currentKey) > 0 )
             v.addElement(currentKey);
    }

    if (v.size() == 0)
        return null;

    return v;
  }
  */
 /**
  * @throws RemoteException
  */
 public void ejbRemove() throws RemoteException
  {
    //#DG614 ejbRemove( true );  // Calculation required by default (if we have a calc monitor of course!)
	 	ejbRemove(getPk(), true );  // Calculation required by default (if we have a calc monitor of course!)
  }

 	/** #DG614
 	 * remove record by give PK
  * 
  * @param pk some Pk
  * @param needCalculation
  * @throws RemoteException
  */
  public void ejbRemoveByPK(IEntityBeanPK pk, boolean needCalculation) throws RemoteException
  {
		ejbRemove(pk, needCalculation);
  }

  /** #DG614
   * remove record by this PK
   * 
   * @param pk some Pk
   * @param needCalculation
   * @throws RemoteException
   */
  public void ejbRemove( boolean needCalculation ) throws RemoteException
  {
  	ejbRemove(getPk(), needCalculation);
  }
  
  //#DG614public void ejbRemove( boolean needCalculation ) throws RemoteException
  public void ejbRemove(IEntityBeanPK pk, boolean needCalculation) throws RemoteException
  {
    String sql =null ;
    //#DG614IEntityBeanPK pk = getPk();

    try
    {
     //4.4 Entity Cache -- start
     //remove from cache
     ThreadLocalEntityCache.removeFromCache(this, pk);
     // 4.4 Entity Cache -- end

     this.removeSons(); // Delete all the sons

     sql = "Delete From " +  this.getEntityTableName ()  + pk.getWhereClause();
     jExec.executeUpdate(sql);

     this.trackEntityRemoval (); // Track the Removal

     if(dcm == null)
      return;  //if the dcm is null then we don't need to visit the rest of this method - brad

     if ( needCalculation )
      { // If need Calculation ..........
        dcm.inputRemovedEntity( this );
        dcm.removeTarget ( this );
        dcm.calc();
      }
     dcm.removeInput ( this );
    }
    catch (Exception e)
    {
      String msg =  "Failed to remove record " + pk.getWhereClause();
      RemoteException re = new RemoteException(msg);
      logger.error(re.getMessage());
      logger.error("remove sql: " + sql);
      logger.error(e);

      throw re;
    }

  }

  protected byte[] readBlob(Blob blob) throws Exception {
    if (blob != null) {
      long length = blob.length();
      InputStream is = blob.getBinaryStream();
      byte[] bytes = new byte[(int) length];

      is.read(bytes, 0, (int) length);

      return bytes;
    } else
      return new byte[1];
  }

  //#DG638 reimplemented - only for convenience or temporarily
  //	   should always use TypeConverter.stringFromClob instead
  protected String readClob(Clob cloba) throws SQLException, IOException  {
	  return TypeConverter.stringFromClob(cloba);    
	  }
  
  //#DG608 create a clob, assign the string value and return it
  protected Clob createClob(String noteText) throws SQLException, IOException {

	  // trying to get the native connection.
	  ExpressConnectionHelper helper = (ExpressConnectionHelper) ExpressCoreContext.
		  getService(ExpressCoreContext.EXPRESS_CONNECTION_HELPER_KEY);
	  Connection nativeConn = helper.getNativeConnection(jExec.getCon());

	  oracle.sql.CLOB clob =
		  oracle.sql.CLOB.createTemporary(nativeConn, true, oracle.sql.CLOB.DURATION_SESSION);
	  clob.open(oracle.sql.CLOB.MODE_READWRITE);
	  Writer out = clob.getCharacterOutputStream();
	  out.write(noteText);
	  out.close();
	  return clob;
  }
  //#DG608 end

  protected void removeSons()   throws Exception
  {
    // override by sons.
  }

  public CalcMonitor getCalcMonitor()
  {
         return dcm;
  }

  public void setCalcMonitor(CalcMonitor dcm)
  {
         this.dcm = dcm;
  }

//*************************************************************************************************
//******** Beginning of Low Level Aduit************************************************************
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // Inert a ChangeMapvalue into the DisplayFieldTable
  // return DisplayFieldTableId
 private int updateDisplayFieldTable(String entityName, String attributeName, String displayFieldName) throws Exception
  { // insert the displayfield to the DisplayField table
      ///////////////////// Get the Sequence ID First ////////////////////////////////
      int displayFieldId= -1  ;
      try
      {
          String sql = "Select DisplayFieldseq.nextval from dual";
          int key = jExec.execute(sql);
          for (; jExec.next(key);  )
          {   displayFieldId  = jExec.getInt(key,1);  // can only be one record
          }
          jExec.closeData(key);
          if( displayFieldId == -1 ) throw new Exception();
      } catch ( Exception e )
      {
           CreateException ce = new CreateException("DealEntity.DisplayField exception getting DispalyFieldId from sequence");
           logger.error(ce.getMessage());
           logger.error(e);
           throw ce;
      }///////////////// End of getting the sequance Id for DisplayField ////////////////////
      try
      {
          String sql = "Insert into DisplayField ( DisplayFieldId, EntityName, AttributeName, DisplayFieldName, INSTITUTIONPROFILEID) Values( " ;
          sql +=  displayFieldId + "," ;
          sql +=  "'" + entityName + "'," ;
          sql +=  "'" + attributeName + "'," ;
          sql +=  "'" + DisplayFieldPicklist.getDisplayField ( displayFieldName) + "'," ;
          sql +=  "" + srk.getExpressState().getDealInstitutionId()  + " ) " ;
          int key = jExec.execute(sql);
          jExec.closeData(key);
      } catch ( Exception e )
      {
           CreateException ce = new CreateException("DealEntity.DisplayField exception Insertting to DisplayField");
           logger.error(ce.getMessage());
           logger.error(e);
           throw ce;
      }
    return displayFieldId;
 } // ----------  End of updateDisplayFieldTable()   -------------------------



  protected void testChange( String propertyName, int newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, Integer newValue ) {

      String nv = "null";
      if (newValue != null) {
          nv = sqlStringFrom(newValue.intValue());
      }

      doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, float newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, char newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, long newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, double newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, boolean newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, Date newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

  protected void testChange( String propertyName, String newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }
  
  //START EDD: additions for AVM
  protected void testChange( String propertyName, byte[] newValue )
  {
    String nv = sqlStringFrom(newValue);
    doTestChange(propertyName, nv);
  }

 protected void doTestChange( String propertyName, String newValue )
 { // this method will define a change, modify the changeMap.
   ChangeMapValue changeMapValue = (ChangeMapValue ) (this.changeValueMap.get( propertyName) ) ;

   if ( changeMapValue == null ) // First Time , initial it
   {
      changeMapValue = new ChangeMapValue(propertyName, newValue);
      changeMapValue.setChangeFlag ( false );
      this.changeValueMap.put ( propertyName, changeMapValue );
   }
   else
   {
     String origValue = changeMapValue.getOrigValue() ;

     if (newValue.equals("''"))
     {
        // only possible with String (type) property - consider equivalent to null (no value)
        if (origValue.equals("null"))
          return;
     }

     changeMapValue.setCurValue (newValue) ;
     changeMapValue.setChangeFlag ( !origValue.equals(changeMapValue.getCurValue()));
   }
 }

 // Additional methods -- By BILLY 25Feb2002
 // To reset the changed flag of the particular field in the ChangeValueMap
 // Usage : to reset the changed flag of a field, so that the formUpdateFieldsList will not
 //         pick up this up.  Which mainly used for particular handle of the LONGCHAR or CLOB fields
 //         which will make the SQL (if > 4K) crash.
 public void resetChange( String propertyName )
 { // this method will define a change, modify the changeMap.
   ChangeMapValue changeMapValue = (ChangeMapValue ) (this.changeValueMap.get( propertyName) ) ;

   if ( changeMapValue != null )
   {
      changeMapValue.setChangeFlag ( false );
      this.changeValueMap.put ( propertyName, changeMapValue );
   }
 }

 // Method to force to trigger a change of a particular field
 public void forceChange( String propertyName )
 { // this method will define a change, modify the changeMap.
   ChangeMapValue changeMapValue = (ChangeMapValue) (this.changeValueMap.get( propertyName) ) ;

   if ( changeMapValue != null )
   {
      changeMapValue.setChangeFlag ( true );
      this.changeValueMap.put ( propertyName, changeMapValue );
   }
 }

 // Method to check if a particular field changed
 public boolean isValueChange( String propertyName )
 { // this method will define a change, modify the changeMap.
   ChangeMapValue changeMapValue = (ChangeMapValue ) (this.changeValueMap.get( propertyName) ) ;

   if ( changeMapValue != null )
   {
      return changeMapValue.getChangeFlag();
   }
   else
      return false;
 }


 protected void setDataBaseSystemDate(String propertyName) {

     doTestChange(propertyName, Mc.DATABASE_SYSTEM_DATE);
     forceChange(propertyName);
 }

///////////////////////////////// Track //////////////////////////////////////////////////\
//go through the changeMap, changeValueMap and record ///////////////////
 /*
AUDITABLE CLASSES ACCORDING TO SPEC
Asset
Borrower
BorrowerAddress
Bridge
CreditReference
Deal
DealFee
DocumentTracking
DownPaymentSource
EmploymentHistory
EscrowPayment
Income
Liability
PartyDealPropertyAssociation
Property
PropertyExpense
  */

 protected void trackEntityChange() throws Exception
 {
     Collection changeValueMapItems = this.changeValueMap.values () ;  // get all the changeMapItems

     Iterator itCVMPI = changeValueMapItems.iterator () ;
     
     //start FXP24404, rearrange from FXP23168
     //start FXP23168, MCM Oct 28, 2008, 
     if (srk.isAuditOn() && this.isAuditable())
     {
         // make sure EntityContext is populated.
         // Purpose of this, is only to initial the EntityContext
         if (this.getEntityContext() != null) 
         {
             while ( itCVMPI.hasNext () )    // loop through all the items
             {
                 ChangeMapValue aChange = (ChangeMapValue) itCVMPI.next ();
                 if ( aChange.getChangeFlag () )
                 {
                     this.transactionHistory.setGenericInfo() ; // this will populate with generic info.

                     // displayField: ( EntityName, AttributeName, DisplayFieldName ) ==> return displayFieldId
/*
                     int historyFieldNameId = updateDisplayFieldTable( 
                         this.getEntityTableName () , aChange.getPropertyName (), aChange.getPropertyName ());
*/
//4.4 Transaction History
                     this.transactionHistory.setEntityName(this.getEntityTableName() );
                     this.transactionHistory.setAttributeName(aChange.getPropertyName() );
                     this.transactionHistory.setDisplayFieldName(DisplayFieldPicklist.getDisplayField(aChange.getPropertyName() ) );
                     
                     //this.transactionHistory.setHistoryFieldNameId(historyFieldNameId);
                     this.transactionHistory.setCurrentValueText ( aChange.getCurrentAsHistoryString() );
                     this.transactionHistory.setPreviousValueText ( aChange.getOriginalAsHistoryString() );
                     this.transactionHistory.store() ;
                     aChange.setOrigValue(aChange.getCurValue());
                     aChange.setChangeFlag (false);
                 }
             }
             return;
         }
     }
     // audit disabled - just reset all change flags
     while ( itCVMPI.hasNext () )    // loop through all the items
     {
         ChangeMapValue aChange = (ChangeMapValue) itCVMPI.next ();
         if ( aChange.getChangeFlag () )
         {
             aChange.setOrigValue(aChange.getCurValue());
             aChange.setChangeFlag (false);
         }
     } // end of while .. all items ...
 }


//----------------TrackEntityCreate() trigger whenever a new entity is create -------------
protected void trackEntityCreate() throws Exception
{
    // check for auditing enabled
	// 4.4, added check for isAuditable
    if (srk.isAuditOn() == false || this.isAuditable() == false)
      return;

    // make sure EntityContext is populated.
    EntityBase.EntityContext etc = this.getEntityContext () ;  // Purpose of this, is only to initial the EntityContext

    this.transactionHistory.setGenericInfo() ; // this will populate with generic info.
    // displayField: ( EntityName, AttributeName, DisplayFieldName ) ==> return displayFieldId
/*    int historyFieldNameId = updateDisplayFieldTable( this.getEntityTableName () , "" , "");
    this.transactionHistory.setHistoryFieldNameId(historyFieldNameId);
*/
//4.4 Transaction History
    this.transactionHistory.setEntityName(this.getEntityTableName() );
    this.transactionHistory.setAttributeName("");
    this.transactionHistory.setDisplayFieldName("");

    this.transactionHistory.setCurrentValueText ( "Created" );
    this.transactionHistory.setPreviousValueText ( "" );

    this.transactionHistory.store() ;
}
//----------------End of TrackEntityCreate() ----------------------------------------------

//----------------TrackEntityRemoval() trigger whenever a new entity is create -------------
protected void trackEntityRemoval() throws Exception
{
    // check for auditing enabled
    if (srk.isAuditOn() == false || this.isAuditable() == false)
      return;

    // make sure EntityContext is populated.
    EntityBase.EntityContext etc = this.getEntityContext () ;  // Purpose of this, is only to initial the EntityContext

    this.transactionHistory.setGenericInfo() ; // this will populate with generic info.
    // displayField: ( EntityName, AttributeName, DisplayFieldName ) ==> return displayFieldId
/*    int historyFieldNameId = updateDisplayFieldTable( this.getEntityTableName () , "" , "");
    this.transactionHistory.setHistoryFieldNameId(historyFieldNameId);
*/
//4.4 Transaction History
    this.transactionHistory.setEntityName(this.getEntityTableName() );
    this.transactionHistory.setAttributeName("");
    this.transactionHistory.setDisplayFieldName("");

    this.transactionHistory.setCurrentValueText ( "Deleted" );
    this.transactionHistory.setPreviousValueText ( "" );

    this.transactionHistory.store() ;
}
//----------------End of TrackEntityRemoval() ----------------------------------------------
  protected EntityBase.EntityContext  getEntityContext ()   throws RemoteException
  { // this should be override by children
    return null ;
  }



///*************************End of Low Level Audit ****************************************
//*****************************************************************************************

  /**
   * Keeps the format of the HTML code
   */
  protected String parseHTML(String report)
  {
    report = report.replaceAll("&lt;","<");
    report = report.replaceAll("&gt;",">");
    //report = report.replaceAll("&nbsp;"," ");
    report = report.replaceAll("&quot;","\"");
    report = report.replaceAll("&amp;","&");
    return report;
  }
  
}
