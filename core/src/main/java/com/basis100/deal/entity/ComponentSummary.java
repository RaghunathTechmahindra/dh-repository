/*
 * @(#)ComponentSummary.java May 1, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * @version 1.1  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  <br>
 * XS_2.9 -- 17/06/2008 
 * - added performUpdate,getPk, getEntityTableName to the entity
 * 
 * @version 1.2  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  20/06/2008 
 * - added findByDeal(DealPK pk) to the entity for copy managememt
 *  
 * @version 1.3  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  24/07/2008
 * Added following methods: 
 * - getClassId()
 * 
 * @version 1.4  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  July 31, 2008
 * fixed getPrimaryKeyName, added numberOfCompontns for  
 * - getClassId()
 * 
 */
public class ComponentSummary extends DealEntity {
    protected int         dealId;
    protected int         copyId;
    protected int         institutionProfileId;
    protected double      totalAmount;
    protected double      totalCashbackAmount;
        
    protected ComponentSummaryPK pk;

    /**
     * 
     * @param srk
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentSummary(SessionResourceKit srk) throws RemoteException,
            FinderException {
        super(srk);
    }
    
    /**
     * <p>
     * Description: Creates the Component entity object by taking session
     * resource kit and calc monitor objects and calls the super class
     * constructor by passing the session resource kit and calc monitor objects.
     * </p>
     * @version 1.0 FXP23117 Initial Version
     * @param srk
     *            stores the session level information.
     * @param dcm
     *            runs the calculation of this Component entity.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentSummary(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException
    {
        super(srk, dcm);
    }
    
    /**
     * 
     * @param srk
     * @param id
     * @param copyId
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentSummary(SessionResourceKit srk, int id, int copyId) throws RemoteException, FinderException
    {
       super(srk);

       pk = new ComponentSummaryPK(id, copyId);

       findByPrimaryKey(pk);
    }
    
    /**
     * 
     * @param key
     * @throws Exception
     */
    private void setPropertiesFromQueryResult(int key) throws Exception {
        this.setDealId(jExec.getInt(key, "dealId"));
        this.setCopyId(jExec.getInt(key, "COPYID"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        this.setTotalAmount(jExec.getDouble(key, "TOTALAMOUNT"));
        this.setTotalCashbackAmount(jExec.getDouble(key, "TOTALCASHBACKAMOUNT"));
    }
    
    
    /**
     * <p>getPrimaryKeyName</p>
     * <p>Description:get first primary key name </p>
     * @return String
     * 
     * @version 1.
     */
    protected String getPrimaryKeyName()  {
        return "dealId";
    }
    
    
    /**
     * <p>
     * Description: Returns the class id of the COMPONENTSUMMARY entity
     * </p>
     * @return int - classid of COMPONENTSUMMARY entity.
     */
    public int getClassId()
    {

        return ClassId.COMPONENTSUMMARY;
    }
    
    /**
     * 
     * @param componentId
     * @param copyId
     * @return
     * @throws RemoteException
     * @throws CreateException
     */
    public ComponentSummary create(int dealId, int copyId) throws RemoteException, CreateException{
        
        pk = new ComponentSummaryPK(dealId, copyId);
       
        //SQL
        String sql = "Insert into ComponentSummary( dealId, copyId, institutionProfileId ) Values ( "
            + dealId + ","+ copyId 
            + "," + srk.getExpressState().getDealInstitutionId()+ ")";
        
        //Execute
        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          this.trackEntityCreate (); // track the create
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("ComponentSummary Entity - Component - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }
        srk.setModified(true);
        return this;
    }

    /**
     * 
     * @param pk
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentSummary findByPrimaryKey(ComponentSummaryPK pk)
            throws RemoteException, FinderException {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from ComponentSummary where dealId = "
                + pk.getId() + " AND copyId = " + pk.getCopyId();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "ComponentSummary Entity: @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                if(getSilentMode() == false)
                    logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "ComponentSummary Entity - findByPrimaryKey() exception");

            if(getSilentMode() == false){
                logger.error(fe.getMessage());
                logger.error("finder sql: " + sql);
                logger.error(e);
            }
            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    
    
    
    
    
    /**
     * @return the componentId
     */
    public int getDealId() {
        return dealId;
    }

    /**
     * @param componentId
     *            the componentId to set
     */
    public void setDealId(int dealId) {
        this.testChange ("dealId", dealId );
        this.dealId = dealId;
    }

    /**
     * @return the copyId
     */
    public int getCopyId() {
        return copyId;
    }

    /**
     * @param copyId
     *            the copyId to set
     */
    public void setCopyId(int copyId) {
        this.testChange ("copyId", copyId );
        this.copyId = copyId;
    }

    /**
     * @return the institutionProfileId
     */
    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    /**
     * @param institutionProfileId
     *            the institutionProfileId to set
     */
    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange ("institutionProfileId", institutionProfileId );
        this.institutionProfileId = institutionProfileId;
    }

    /**
     * @return the totalAmount
     */
    public double getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(double totalAmount) {
        this.testChange ("totalAmount", totalAmount );
        this.totalAmount = totalAmount;
    }

    /**
     * @return the totalCashbackAmount
     */
    public double getTotalCashbackAmount() {
        return totalCashbackAmount;
    }

    /**
     * @param totalCashbackAmount the totalCashbackAmount to set
     */
    public void setTotalCashbackAmount(double totalCashbackAmount) {
        this.testChange ("totalCashbackAmount", totalCashbackAmount );
        this.totalCashbackAmount = totalCashbackAmount;
    }

   
    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't
        // attach the entity to Calc. again
        if (dcm != null && ret > 0) {
            dcm.inputEntity(this);
        }

        return ret;
    }

    /**
     * gets the pk associated with this entity
     * @return a ComponentSummaryPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * @return the TableName
     */
    public String getEntityTableName() {

        return "ComponentSummary";
    }
    
    /**
     * <p>
     * Description: Returns the collection of the componentsSummary associated to the
     * deal
     * </p>
     * 
     * @version 1.0 20-Jun-2008 - Initial Version
     * @param DealPK - Deal Primary key
     * @return Collection - collection of componentSummary
     * @throws Exception
     */
    public Collection findByDeal(DealPK pk) throws Exception
    {

        Collection componentSummaries = new ArrayList();

        String sql = "Select * from ComponentSummary ";
        sql += pk.getWhereClause();
        
        try
        {

            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                ComponentSummary componentSummary = new ComponentSummary(srk);
                componentSummary.setPropertiesFromQueryResult(key);
                logger.debug("componentSummary.getDealId()" + componentSummary.getDealId()+ " : componentSummary.getCopyId()" + componentSummary.getCopyId());
                componentSummary.pk = new ComponentSummaryPK(componentSummary.getDealId(),componentSummary.getCopyId());
                componentSummaries.add(componentSummary);
            }

            jExec.closeData(key);

        }
        catch (Exception e)
        {
            Exception fe = new Exception(
                    "Exception caught while getting Deal::Components");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return componentSummaries;
    }

    /**
     * @throws RemoteException
     */
    public void ejbRemove() throws RemoteException
     {
           ejbRemove(getPk(), false );  // Calculation required by default (if we have a calc monitor of course!)
     }

}
