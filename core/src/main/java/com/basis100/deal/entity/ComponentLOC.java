/*
 * @(#)ComponentLOC.java May 1, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: ComponentLOC
 * </p>
 * <p>
 * Description: This is an entity class representing ComponentLOC. This class
 * provides accessor and mutator methods for all the entity attributes. This
 * class will map the entity and database fields. This class will save the
 * appropriate data into the ComponentLOC Table.
 * </p>
 * @version 1.0 Initial Version
 * @version 1.1 XS_11.11 10-Jun-2008 Added getEntityTableName(), getClassId(),
 *          getPk(), performUpdate() methods , added code comments ,Added
 *          constructor's and removed the createPrimaryKey method
 *          
 * @version 1.2 19-Jun-2008 Added findByComponent() for copy management   
 * @version 1.4 XS_2.26 14-Jul-2008 added create(int componentId, int copyId) method
 * @version 1.5 XS_2.26 14-Jul-2008 deleted create(int componentId, int copyId,int paymentFrequencyId, 
 * 									int prepaymentOptionsId,int privilegePaymementId) as paymentFrequencyId, 
 * 									prepaymentOptionsId , privilegePaymementId are defaulted in the DB 
 */
public class ComponentLOC extends DealEntity
{

    protected int componentId;

    protected int copyId;

    protected int institutionProfileId;

    protected double advanceHold;

    protected String commissionCode;

    protected double discount;

    protected String existingAccountIndicator;

    protected String existingAccountNumber;

    protected Date firstPaymentDate;

    protected Date interestAdjustmentDate;

    protected double locAmount;

    protected String miAllocateFlag;

    protected double netInterestRate;

    protected double pAndIPaymentAmount;

    protected double pAndIPaymentAmountMonthly;

    protected int paymentFrequencyId;

    protected double premium;

    protected int prePaymentOptionsId;

    protected int privilegePaymentId;

    protected String propertyTaxAllocateFlag;

    protected double propertyTaxEscrowAmount;

    protected double totalLocAmount;

    protected double totalPaymentAmount;

    protected ComponentLOCPK pk;

    /**
     * <p>
     * Description:Default constructor.
     * </p>
     * @version 1.0 Initial Version
     */
    public ComponentLOC()
    {

    }

    /**
     * <p>
     * Description: Creates the ComponentLOC entity object by taking session
     * resource kit object and calls the super class constructor by passing the
     * session resource kit object.
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @param srk
     *            stores the session level information.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentLOC(SessionResourceKit srk) throws RemoteException,
            FinderException
    {
        super(srk);
    }

    /**
     * <p>
     * Description: Creates the ComponentLOC entity object by taking session
     * resource kit and calc monitor objects and calls the super class
     * constructor by passing the session resource kit and calc monitor objects.
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @param srk
     *            stores the session level information.
     * @param dcm
     *            runs the calculation of this ComponentLOC entity.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentLOC(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException
    {
        super(srk, dcm);
    }

    /**
     * <p>
     * Description: Creates the ComponentLOC entity object by taking session
     * resource kit, component id and the copy id of the existing
     * Component.Calls the super class constructor and creates the primary key
     * for the ComponentLOC entity and calls the findByPrimaryKey() method.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.6 Modified id to componentId in the constructor
     * @param srk
     *            stores the session level information.
     * @param componentId
     *            component id value passed to the entity PK class.
     * @param copyId
     *            copy id of the Component.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     * @see com.basis100.deal.entity.ComponentLOC#findByPrimaryKey(com.basis100.deal.pk.ComponentLOCPK )
     */
    public ComponentLOC(SessionResourceKit srk, int componentId, int copyId)
            throws RemoteException, FinderException
    {
        super(srk);

        pk = new ComponentLOCPK(componentId, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * <p>
     * Description: Creates the ComponentLOC entity object by taking session
     * resource kit and calc monitor objects, component id and the copy id of
     * the existing Component.Calls the super class constructor and creates the
     * primary key for the ComponentLOC entity and calls the findByPrimaryKey()
     * method.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.6 Modified id to componentId in the constructor
     * @param srk
     *            stores the session level information.
     * @param componentId
     *            component id value passed to the entity PK class.
     * @param dcm
     *            runs the calculation of this ComponentLOC entity.
     * @param copyId
     *            copy id of the Component.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     * @see com.basis100.deal.entity.ComponentLOC#findByPrimaryKey(com.basis100.deal.pk.ComponentLOCPK )
     */
    public ComponentLOC(SessionResourceKit srk, CalcMonitor dcm,
            int componentId, int copyId) throws RemoteException,
            FinderException
    {
        super(srk, dcm);

        pk = new ComponentLOCPK(componentId, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * <p>
     * Description: Sets the ComponentLOC entity properties from the resultset
     * returned from the database
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @param key
     *            the row number in the database.
     * @throws Exception
     *             exception thrown while storing the values into the resultset.
     */
    private void setPropertiesFromQueryResult(int key) throws Exception
    {
        this.setComponentId(jExec.getInt(key, "COMPONENTID"));
        this.setCopyId(jExec.getInt(key, "COPYID"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        this.setAdvanceHold(jExec.getDouble(key, "ADVANCEHOLD"));
        this.setCommissionCode(jExec.getString(key, "COMMISSIONCODE"));
        this.setDiscount(jExec.getDouble(key, "DISCOUNT"));
        this.setExistingAccountIndicator(jExec.getString(key,
                "EXISTINGACCOUNTINDICATOR"));
        this.setExistingAccountNumber(jExec.getString(key,
                "EXISTINGACCOUNTNUMBER"));
        this.setFirstPaymentDate(jExec.getDate(key, "FIRSTPAYMENTDATE"));
        this.setInterestAdjustmentDate(jExec.getDate(key,
                "INTERESTADJUSTMENTDATE"));
        this.setLocAmount(jExec.getDouble(key, "LOCAMOUNT"));
        this.setMiAllocateFlag(jExec.getString(key, "MIALLOCATEFLAG"));
        this.setNetInterestRate(jExec.getDouble(key, "NETINTERESTRATE"));
        this.setPAndIPaymentAmount(jExec.getDouble(key, "PANDIPAYMENTAMOUNT"));
        this.setPAndIPaymentAmountMonthly(jExec.getDouble(key,
                "PANDIPAYMENTAMOUNTMONTHLY"));
        this.setPaymentFrequencyId(jExec.getInt(key, "PAYMENTFREQUENCYID"));
        this.setPremium(jExec.getDouble(key, "PREMIUM"));
        this.setPrePaymentOptionsId(jExec.getInt(key, "PREPAYMENTOPTIONSID"));
        this.setPrivilegePaymentId(jExec.getInt(key, "PRIVILEGEPAYMENTID"));
        this.setPropertyTaxAllocateFlag(jExec.getString(key,
                "PROPERTYTAXALLOCATEFLAG"));
        this.setPropertyTaxEscrowAmount(jExec.getDouble(key,
                "PROPERTYTAXESCROWAMOUNT"));
        this.setTotalLocAmount(jExec.getDouble(key, "TOTALLOCAMOUNT"));
        this.setTotalPaymentAmount(jExec.getDouble(key, "TOTALPAYMENTAMOUNT"));
    }

    /**
     * <p>
     * Description: Retrieves a row from the database using the primary key
     * passed in.
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @param pk
     *            primary key object of the ComponentLOC entity.
     * @return ComponentLOC Component LOC object.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentLOC findByPrimaryKey(ComponentLOCPK pk)
            throws RemoteException, FinderException
    {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from ComponentLOC where componentId = "
                + pk.getId() + " AND copyId = " + pk.getCopyId();

        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "ComponentLOC Entity: @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                if(getSilentMode() == false)
                    logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException(
                    "ComponentLOC Entity - findByPrimaryKey() exception");

            if(getSilentMode() == false){
                logger.error(fe.getMessage());
                logger.error("finder sql: " + sql);
                logger.error(e);
            }

            throw fe;
        }
        this.copyId = pk.getCopyId();
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * @param componentId
     * @param copyId
     * @param paymentFrequencyId
     * @param prepaymentOptionsId
     * @param privilegePaymementId
     * @return
     * @throws RemoteException
     * @throws CreateException
     */
    public ComponentLOC create(int componentId, int copyId) throws RemoteException, CreateException
    {

        pk = new ComponentLOCPK(componentId, copyId);
        // SQL
        String sql = "Insert into ComponentLOC( componentId, copyId, institutionProfileId) Values ( "
                + componentId
                + ","
                + copyId
                + ","
                + srk.getExpressState().getDealInstitutionId() +  ")";

        // Execute
        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the create
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "ComponentLOC Entity - Component - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        if (dcm != null)
            dcm.inputCreatedEntity(this);

        return this;
    }

    /**
     * @return the componentId
     */
    public int getComponentId()
    {
        return componentId;
    }

    /**
     * @param componentId
     *            the componentId to set
     */
    public void setComponentId(int componentId)
    {
        this.testChange("componentId", componentId);
        this.componentId = componentId;
    }

    /**
     * @return the copyId
     */
    public int getCopyId()
    {
        return copyId;
    }

    /**
     * @param copyId
     *            the copyId to set
     */
    public void setCopyId(int copyId)
    {
        this.testChange("copyId", copyId);
        this.copyId = copyId;
    }

    /**
     * @return the institutionProfileId
     */
    public int getInstitutionProfileId()
    {
        return institutionProfileId;
    }

    /**
     * @param institutionProfileId
     *            the institutionProfileId to set
     */
    public void setInstitutionProfileId(int institutionProfileId)
    {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    /**
     * @return the advanceHold
     */
    public double getAdvanceHold()
    {
        return advanceHold;
    }

    /**
     * @param advanceHold
     *            the advanceHold to set
     */
    public void setAdvanceHold(double advanceHold)
    {
        this.testChange("advanceHold", advanceHold);
        this.advanceHold = advanceHold;
    }

    /**
     * @return the commissionCode
     */
    public String getCommissionCode()
    {
        return commissionCode;
    }

    /**
     * @param commissionCode
     *            the commissionCode to set
     */
    public void setCommissionCode(String commissionCode)
    {
        this.testChange("commissionCode", commissionCode);
        this.commissionCode = commissionCode;
    }

    /**
     * @return the discount
     */
    public double getDiscount()
    {
        return discount;
    }

    /**
     * @param discount
     *            the discount to set
     */
    public void setDiscount(double discount)
    {
        this.testChange("discount", discount);
        this.discount = discount;
    }

    /**
     * @return the existingAccountIndicator
     */
    public String getExistingAccountIndicator()
    {
        return existingAccountIndicator;
    }

    /**
     * @param existingAccountIndicator
     *            the existingAccountIndicator to set
     */
    public void setExistingAccountIndicator(String existingAccountIndicator)
    {
        this.testChange("existingAccountIndicator", existingAccountIndicator);
        this.existingAccountIndicator = existingAccountIndicator;
    }

    /**
     * @return the existingAccountNumber
     */
    public String getExistingAccountNumber()
    {
        return existingAccountNumber;
    }

    /**
     * @param existingAccountNumber
     *            the existingAccountNumber to set
     */
    public void setExistingAccountNumber(String existingAccountNumber)
    {
        this.testChange("existingAccountNumber", existingAccountNumber);
        this.existingAccountNumber = existingAccountNumber;
    }

    /**
     * @return the firstPaymentDate
     */
    public Date getFirstPaymentDate()
    {
        return firstPaymentDate;
    }

    /**
     * @param firstPaymentDate
     *            the firstPaymentDate to set
     */
    public void setFirstPaymentDate(Date firstPaymentDate)
    {
        this.testChange("firstPaymentDate", firstPaymentDate);
        this.firstPaymentDate = firstPaymentDate;
    }

    /**
     * @return the interestAdjustmentDate
     */
    public Date getInterestAdjustmentDate()
    {
        return interestAdjustmentDate;
    }

    /**
     * @param interestAdjustmentDate
     *            the interestAdjustmentDate to set
     */
    public void setInterestAdjustmentDate(Date interestAdjustmentDate)
    {
        this.testChange("interestAdjustmentDate", interestAdjustmentDate);
        this.interestAdjustmentDate = interestAdjustmentDate;
    }

    /**
     * @return the locAmount
     */
    public double getLocAmount()
    {
        return locAmount;
    }

    /**
     * @param locAmount
     *            the locAmount to set
     */
    public void setLocAmount(double locAmount)
    {
        this.testChange("locAmount", locAmount);
        this.locAmount = locAmount;
    }

    /**
     * @return the mialLocateFlag
     */
    public String getMiAllocateFlag()
    {
        return miAllocateFlag;
    }

    /**
     * @param mialLocateFlag
     *            the mialLocateFlag to set
     */
    public void setMiAllocateFlag(String miAllocateFlag)
    {
        this.testChange("miAllocateFlag", miAllocateFlag);
        this.miAllocateFlag = miAllocateFlag;
    }

    /**
     * @return the netInterestRate
     */
    public double getNetInterestRate()
    {
        return netInterestRate;
    }

    /**
     * @param netInterestRate
     *            the netInterestRate to set
     */
    public void setNetInterestRate(double netInterestRate)
    {
        this.testChange("netInterestRate", netInterestRate);
        this.netInterestRate = netInterestRate;
    }

    /**
     * @return the pAndIPaymentAmount
     */
    public double getPAndIPaymentAmount()
    {
        return pAndIPaymentAmount;
    }

    /**
     * @param andIPaymentAmount
     *            the pAndIPaymentAmount to set
     */
    public void setPAndIPaymentAmount(double andIPaymentAmount)
    {
        this.testChange("pAndIPaymentAmount", andIPaymentAmount);
        pAndIPaymentAmount = andIPaymentAmount;
    }

    /**
     * @return the pAndIPaymentAmountMonthly
     */
    public double getPAndIPaymentAmountMonthly()
    {
        return pAndIPaymentAmountMonthly;
    }

    /**
     * @param andIPaymentAmountMonthly
     *            the pAndIPaymentAmountMonthly to set
     */
    public void setPAndIPaymentAmountMonthly(double andIPaymentAmountMonthly)
    {
        this.testChange("pAndIPaymentAmountMonthly", andIPaymentAmountMonthly);
        pAndIPaymentAmountMonthly = andIPaymentAmountMonthly;
    }

    /**
     * @return the paymentFrequencyId
     */
    public int getPaymentFrequencyId()
    {
        return paymentFrequencyId;
    }

    /**
     * @param paymentFrequencyId
     *            the paymentFrequencyId to set
     */
    public void setPaymentFrequencyId(int paymentFrequencyId)
    {
        this.testChange("paymentFrequencyId", paymentFrequencyId);
        this.paymentFrequencyId = paymentFrequencyId;
    }

    /**
     * @return the premium
     */
    public double getPremium()
    {
        return premium;
    }

    /**
     * @param premium
     *            the premium to set
     */
    public void setPremium(double premium)
    {
        this.testChange("premium", premium);
        this.premium = premium;
    }

    /**
     * @return the prepaymentOptionsId
     */
    public int getPrePaymentOptionsId()
    {
        return prePaymentOptionsId;
    }

    /**
     * @param prepaymentOptionsId
     *            the prepaymentOptionsId to set
     */
    public void setPrePaymentOptionsId(int prePaymentOptionsId)
    {
        this.testChange("prePaymentOptionsId", prePaymentOptionsId);
        this.prePaymentOptionsId = prePaymentOptionsId;
    }

    /**
     * @return the priviledgePaymentId
     */
    public int getPrivilegePaymentId()
    {
        return privilegePaymentId;
    }

    /**
     * @param privilegePaymentId
     *            the privilegePaymentId to set
     */
    public void setPrivilegePaymentId(int privilegePaymentId)
    {
        this.testChange("privilegePaymentId", privilegePaymentId);
        this.privilegePaymentId = privilegePaymentId;
    }

    /**
     * @return the propertyTaxAllocateFlag
     */
    public String getPropertyTaxAllocateFlag()
    {
        return propertyTaxAllocateFlag;
    }

    /**
     * @param propertyTaxAllocateFlag
     *            the propertyTaxAllocateFlag to set
     */
    public void setPropertyTaxAllocateFlag(String propertyTaxAllocateFlag)
    {
        this.testChange("propertyTaxAllocateFlag", propertyTaxAllocateFlag);
        this.propertyTaxAllocateFlag = propertyTaxAllocateFlag;
    }

    /**
     * @return the propertyTaxEcrowAmount
     */
    public double getPropertyTaxEscrowAmount()
    {
        return propertyTaxEscrowAmount;
    }

    /**
     * @param propertyTaxEcrowAmount
     *            the propertyTaxEcrowAmount to set
     */
    public void setPropertyTaxEscrowAmount(double propertyTaxEcrowAmount)
    {
        this.testChange("propertyTaxEscrowAmount", propertyTaxEcrowAmount);
        this.propertyTaxEscrowAmount = propertyTaxEcrowAmount;
    }

    /**
     * @return the totalLocAmount
     */
    public double getTotalLocAmount()
    {
        return totalLocAmount;
    }

    /**
     * @param totalLocAmount
     *            the totalLocAmount to set
     */
    public void setTotalLocAmount(double totalLocAmount)
    {
        this.testChange("totalLocAmount", totalLocAmount);
        this.totalLocAmount = totalLocAmount;
    }

    /**
     * @return the totalPaymentAmount
     */
    public double getTotalPaymentAmount()
    {
        return totalPaymentAmount;
    }

    /**
     * @param totalPaymentAmount
     *            the totalPaymentAmount to set
     */
    public void setTotalPaymentAmount(double totalPaymentAmount)
    {
        this.testChange("totalPaymentAmount", totalPaymentAmount);
        this.totalPaymentAmount = totalPaymentAmount;
    }

    /**
     * <p>
     * Description: Performs updates on the ComponentLOC entity
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return int returns the no of rows updated.
     */
    protected int performUpdate() throws Exception
    {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0)
        {
            dcm.inputEntity(this);
        }

        return ret;
    }

    /**
     * <p>
     * Description: Returns the pk object
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return IEntityBeanPK ComponentLOC entity pk object
     */
    public IEntityBeanPK getPk()
    {
        return (IEntityBeanPK) this.pk;
    }

    /**
     * <p>
     * Description: Returns the ComponentLOC entity table name
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return String - table name of the entity
     */
    public String getEntityTableName()
    {
        return "ComponentLOC";
    }

    /**
     * <p>
     * Description: Returns the class id of the ComponentLOC entity
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return int - classid of ComponentLOC entity.
     */
    public int getClassId()
    {

        return ClassId.COMPONENTLOC;
    }
    
    /**
     * <p>
     * Description: Gets a collection of ComponentLOCs for the component
     * </p>
     * @version 1.0 
     * @param ComponentPK
     *  
     */
    public Collection findByComponent(ComponentPK pk) throws Exception {

        StringBuffer sqlbuf = new StringBuffer(
                "Select * from ComponentLOC where ");
        sqlbuf.append("componentId = ").append(pk.getId());
        sqlbuf.append(" and copyId = ").append(pk.getCopyId());

        Collection records = new ArrayList();

        try {
            int key = jExec.execute(sqlbuf.toString());

            for (; jExec.next(key);) {
                ComponentLOC loc = new ComponentLOC(srk, dcm);
                loc.setPropertiesFromQueryResult(key);
                loc.pk = new ComponentLOCPK(loc.getComponentId(), loc.getCopyId());

                records.add(loc);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger.error("finder sql: " + sqlbuf.toString());
            logger.error(e);
            return null;
        }

        return records;
    }
    /**
     *   <p>isAuditable</p>
     *   <p>Descriptio: gets if this entity is auditable or not
     *   @version 1.0 XS_2.30
     *   @return true
     */
    public boolean isAuditable() {

        return true;
    }

    /**
     * returns EntityContext. 
     * @since 4.0 MCM, Oct 28, 2008, FXP23168, Hiro
     */
    protected EntityContext getEntityContext() throws RemoteException {

        EntityContext ctx = this.entityContext;
        try {
            Component comp = new Component(srk, componentId, copyId);
            ctx.setContextText(this.getEntityTableName());
            int langId = srk.getLanguageId();
            int instId = srk.getExpressState().getDealInstitutionId();
            String description = BXResources.getPickListDescription(instId,
                    "COMPONENTTYPE", comp.getComponentTypeId(), langId);
            ctx.setContextSource(description);
            ctx.setApplicationId(comp.getDealId());

        } catch (FinderException e) {
            logger.error(getClass().getSimpleName() + ".getEntityContext " +
                    "FinderException caught. ComponentId= " + componentId + " CopyId=" + copyId);
        }
        return ctx;
    } 
}
