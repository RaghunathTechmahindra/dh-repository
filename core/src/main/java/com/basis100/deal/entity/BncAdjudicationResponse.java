
package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BncAdjudicationResponsePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * 
 * <p>
 * Title: BncAdjudicationResponse
 * </p>
 * <p>
 * Description: Entity class to handles all database communication to the
 * BncAdjudicationResponse table
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */
public class BncAdjudicationResponse extends DealEntity {
    private String productCode;
    private int submissionCount;
    private String miDecision;

    private BncAdjudicationResponsePK pk;
    private double amountInsured;
    private double globalNetworth;
    private int marketRiskId;
    private int neighbourhoodRiskId;
    private int primaryApplicantRiskId;
    private int propertyRiskId;
    private int randomDigit;
    private int recommendedCodeId;
    private int responseId;
    private int secondaryApplicantRiskId;
    private int globalUsedCreditBureauNameId;
    private int institutionProfileId;

    public static final String RECOMMENDEDCODEID = "recommendedCodeId";
    public static final String AMOUNTINSURED = "amountInsured";
    public static final String PRIMARYAPPLICANTRISKID = "primaryApplicantRiskId";
    public static final String SECONDARYAPPLICANTRISKID = "secondaryApplicantRiskId";
    public static final String MARKETRISKID = "marketRiskId";
    public static final String PROPERTYRISKID = "propertyRiskId";
    public static final String NEIGHBOURHOODRISKID = "neighbourhoodRiskId";
    public static final String RANDOMDIGIT = "randomDigit";
    public static final String GLOBALNETWORTH = "globalNetworth";
    public static final String GLOBALUSEDCREDITBUREAUNAMEID = "globalUsedCreditBureauNameId";
    public static final String PRODUCTCODE = "productCode";
    public static final String SUBMISSIONCOUNT = "submissionCount";
    public static final String MIDECISION = "miDecision";
    public static final String INSTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public BncAdjudicationResponse(SessionResourceKit srk)
            throws RemoteException {

        super(srk);
    }

    public BncAdjudicationResponse() {

    }

    public int getResponseId() {

        return responseId;
    }

    public void setResponseId(int responseId) {

        testChange(BncAdjudicationResponsePK.RESPONSEID, responseId);
        this.responseId = responseId;
    }

    public int getRecommendedCodeId() {

        return recommendedCodeId;
    }

    public void setRecommendedCodeId(int recommendedCodeId) {

        testChange(RECOMMENDEDCODEID, recommendedCodeId);
        this.recommendedCodeId = recommendedCodeId;
    }

    public double getAmountInsured() {

        return amountInsured;
    }

    public void setAmountInsured(double amountInsured) {

        testChange(AMOUNTINSURED, amountInsured);
        this.amountInsured = amountInsured;
    }

    public int getPrimaryApplicantRiskId() {

        return primaryApplicantRiskId;
    }

    public void setPrimaryApplicantRiskId(int primaryApplicantRiskId) {

        testChange(PRIMARYAPPLICANTRISKID, primaryApplicantRiskId);
        this.primaryApplicantRiskId = primaryApplicantRiskId;
    }

    public int getSecondaryApplicantRiskId() {

        return secondaryApplicantRiskId;
    }

    public void setSecondaryApplicantRiskId(int secondaryApplicantRiskId) {

        testChange(SECONDARYAPPLICANTRISKID, secondaryApplicantRiskId);
        this.secondaryApplicantRiskId = secondaryApplicantRiskId;
    }

    public int getMarketRiskId() {

        return marketRiskId;
    }

    public void setMarketRiskId(int marketRiskId) {

        testChange(MARKETRISKID, marketRiskId);
        this.marketRiskId = marketRiskId;
    }

    public int getPropertyRiskId() {

        return propertyRiskId;
    }

    public void setPropertyRiskId(int propertyRiskId) {

        testChange(PROPERTYRISKID, propertyRiskId);
        this.propertyRiskId = propertyRiskId;
    }

    public int getNeighbourhoodRiskId() {

        return neighbourhoodRiskId;
    }

    public void setNeighbourhoodRiskId(int neighbourhoodRiskId) {

        testChange(NEIGHBOURHOODRISKID, neighbourhoodRiskId);
        this.neighbourhoodRiskId = neighbourhoodRiskId;
    }

    public int getRandomDigit() {

        return randomDigit;
    }

    public void setRandomDigit(int randomDigit) {

        testChange(RANDOMDIGIT, randomDigit);
        this.randomDigit = randomDigit;
    }

    public double getGlobalNetworth() {

        return globalNetworth;
    }

    public void setGlobalNetworth(double globalNetworth) {

        testChange(GLOBALNETWORTH, globalNetworth);
        this.globalNetworth = globalNetworth;
    }

    public int getGlobalUsedCreditBureauNameId() {

        return globalUsedCreditBureauNameId;
    }

    public void setGlobalUsedCreditBureauNameId(int globalUsedCreditBureauNameId) {

        testChange(GLOBALUSEDCREDITBUREAUNAMEID, globalUsedCreditBureauNameId);
        this.globalUsedCreditBureauNameId = globalUsedCreditBureauNameId;
    }

    public String getProductCode() {

        return productCode;
    }

    public void setProductCode(String productCode) {

        testChange(PRODUCTCODE, productCode);
        this.productCode = productCode;
    }

    public int getSubmissionCount() {

        return submissionCount;
    }

    public void setSubmissionCount(int submissionCount) {

        testChange(SUBMISSIONCOUNT, submissionCount);
        this.submissionCount = submissionCount;
    }

    public String getMiDecision() {

        return miDecision;
    }

    public void setMiDecision(String miDecision) {

        testChange(MIDECISION, miDecision);
        this.miDecision = miDecision;
    }

    public void setPk(BncAdjudicationResponsePK primaryKey) {

        this.pk = primaryKey;
    }

    /**
     * search for BncAdjudicationResponse record using primary key
     * 
     * @param pk
     *            BncAdjudicationResponsePK Primary key to be searched by
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws FinderException
     *             Thrown if unable to find a matching row
     * @return BncAdjudicationResponse object that matches the primary key given
     */
    public BncAdjudicationResponse findByPrimaryKey(BncAdjudicationResponsePK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from " + getEntityTableName()
                + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = getEntityTableName()
                        + ": @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(getEntityTableName()
                    + " Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * Set all properties of this object using the values that is returned by
     * the current database query
     * 
     * @param key
     *            int The index of the database query result list
     * @throws Exception
     *             Thrown if error
     */
    public void setPropertiesFromQueryResult(int key) throws Exception {

        setResponseId(jExec.getInt(key, BncAdjudicationResponsePK.RESPONSEID));
        setRecommendedCodeId(jExec.getInt(key, RECOMMENDEDCODEID));
        setAmountInsured(jExec.getDouble(key, AMOUNTINSURED));
        setPrimaryApplicantRiskId(jExec.getInt(key, PRIMARYAPPLICANTRISKID));
        setSecondaryApplicantRiskId(jExec.getInt(key, SECONDARYAPPLICANTRISKID));
        setMarketRiskId(jExec.getInt(key, MARKETRISKID));
        setPropertyRiskId(jExec.getInt(key, PROPERTYRISKID));
        setNeighbourhoodRiskId(jExec.getInt(key, NEIGHBOURHOODRISKID));
        setRandomDigit(jExec.getInt(key, RANDOMDIGIT));
        setGlobalNetworth(jExec.getDouble(key, GLOBALNETWORTH));
        setGlobalUsedCreditBureauNameId(jExec.getInt(key,
                GLOBALUSEDCREDITBUREAUNAMEID));
        setProductCode(jExec.getString(key, PRODUCTCODE));
        setSubmissionCount(jExec.getInt(key, SUBMISSIONCOUNT));
        setMiDecision(jExec.getString(key, MIDECISION));
        setInstitutionProfileId(jExec.getInt(key, INSTITUTIONPROFILEID));
    }

    /**
     * Create and inserts a BncAdjudicationResponse into the database
     * 
     * @param rpk
     *            BncAdjudicationResponsePK Primary key
     * @param recommendCodeId
     *            int Recommended code ID
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws CreateException
     *             Thrown if unable to create row
     * @return BncAdjudicationResponse The BncAdjudicationResponse object has
     *         has been inserted into the database
     */
    public BncAdjudicationResponse create(BncAdjudicationResponsePK rpk,
            int recommendCodeId) throws RemoteException, CreateException {

        int key = 0;
        StringBuffer sqlb = new StringBuffer("Insert into "
                + getEntityTableName() + "( responseid"
                + ", recommendedCodeId, INSTITUTIONPROFILEID) VALUES ( ");
        sqlb.append(sqlStringFrom(rpk.getId()) + ", ");
        sqlb.append(sqlStringFrom(recommendCodeId) + ", ");
        sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId())
                + ")");

        String sql = new String(sqlb);

        try {
            key = jExec.executeUpdate(sql);
            findByPrimaryKey(new BncAdjudicationResponsePK(rpk.getId()));
            this.trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(getEntityTableName()
                    + " Entity - " + getEntityTableName()
                    + " - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        } finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            } catch (Exception e) {
                logger.error(e.toString());
            }
        }
        srk.setModified(true);
        return this;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // ///////////////////// OVERWRITING BASE CLASS METHODS
    // /////////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);
        return ret;
    }

    public String getEntityTableName() {

        return "BncAdjudicationResponse";
    }

    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public boolean equals(Object o) {

        if (o instanceof BncAdjudicationResponse) {
            BncAdjudicationResponse oa = (BncAdjudicationResponse) o;
            if (this.pk.equals(oa.getPk())) {
                return true;
            }
        }
        return false;
    }

    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public Object clone() throws CloneNotSupportedException {

        throw new CloneNotSupportedException();
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}
