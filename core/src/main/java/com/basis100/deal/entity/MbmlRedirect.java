package com.basis100.deal.entity;

import java.sql.PreparedStatement;
import java.util.Date;

import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class MbmlRedirect extends DealEntity

{
	  protected String                     sourceApplicationId;	  
	  protected int                        institutionProfileId;
	  protected int                        statusId;
	  protected Date                       statusDate;
	  protected int                        lenderProfileId;
	  
	  public MbmlRedirect(SessionResourceKit srk) throws RemoteException, FinderException
	  {
	    super(srk);
	  } 
	  
	  public MbmlRedirect findBySourceApplicationId(String sourceApplicationId) throws RemoteException, FinderException
	   {
	      
	      String sql = "Select * from MbmlRedirect where sourceApplicationId = ?";	      

	      boolean gotRecord = false;

	      try
	      {
	          
	          PreparedStatement pstmt = jExec.getPreparedStatement(sql);
	          // Set sourceApplicationId
	          pstmt.setString(1, sourceApplicationId);
	          
	          logger.debug("@MbmlRedirect.indBySourceApplicationId : sourceApplicationId = " + sourceApplicationId );
	          int key = jExec.executePreparedStatement(pstmt, sql);
	          // =============================================================================================

	          for (; jExec.next(key);  )
	          {
	              gotRecord = true;	              
	              setPropertiesFromQueryResult(key);
	              break; 
	          }

	          jExec.closeData(key);
	          pstmt.close();

	          if (gotRecord == false)
	          {
	           return null;
	          }

	        }
	        catch (Exception e)
	        {
	           
	            FinderException fe = new FinderException("MbmlRedirect - findBySourceApplicationId() exception");
	            logger.error(fe.getMessage());
	            logger.error("finder sql: " + sql);
	            logger.error(e);

	            throw fe;
	        }	        

	        return this;
	    }
	  
	  
	  private void setSourceApplicationId(String sourceApplicationId)
	  {
	   this.testChange ("sourceApplicationId", sourceApplicationId );
	   this.sourceApplicationId = sourceApplicationId;
	  }

	  public String getSourceApplicationId()
	  {
	    return this.sourceApplicationId ;
	  }
	  
	  private void setInstitutionProfileId(int institutionProfileId)
	  {
	   this.testChange ("institutionProfileId", institutionProfileId );
	   this.institutionProfileId = institutionProfileId;
	  }

	  public int getInstitutionProfileId()
	  {
	    return this.institutionProfileId ;
	  }
	  
	  private void setStatusId(int statusId)
	  {
	   this.testChange ("statusId", statusId );
	   this.statusId = statusId;
	  }

	  public int getStatusId()
	  {
	    return this.statusId ;
	  }
	  
	  private void setStatusDate(Date  d )
	   {
	     this.testChange ("statusDate", d ) ;
	     this.statusDate = d;
	   }
	  
	  public Date getStatusDate()
	   {
	      return this.statusDate ;
	   }
	  
	  private void setLenderProfileId(int lenderProfileId)
	  {
	   this.testChange ("lenderProfileId", lenderProfileId );
	   this.lenderProfileId = lenderProfileId;
	  }

	  public int getLenderProfileId()
	  {
	    return this.lenderProfileId ;
	  }
	  
	  private void setPropertiesFromQueryResult(int key) throws Exception
	  {
		  setSourceApplicationId( jExec.getString(key, "SOURCEAPPLICATIONID"));
		  setInstitutionProfileId( jExec.getInt(key, "INSTITUTIONPROFILEID"));
		  setStatusId(jExec.getInt(key, "STATUSID"));
		  setStatusDate(jExec.getDate(key, "STATUSDATE"));	    
		  setLenderProfileId(jExec.getInt(key, "LENDERPROFILEID"));
	    
	  }




	  public String getEntityTableName()
	  {
	    return "MBMLREDIRECT";
	  }

}
