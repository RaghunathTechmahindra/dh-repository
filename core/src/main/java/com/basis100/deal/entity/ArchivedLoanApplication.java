package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class ArchivedLoanApplication extends DealEntity
{
  protected int      archivedLoanApplicationId;
  protected int 	 instProfileId;
  protected String   sourceApplicationId;
  protected int 	 dealId;
  protected Date 	 receivedTimeStamp;
  protected Date 	 archivedTimeStamp;
  protected String   dealXML = "";

  private ArchivedLoanApplicationPK pk;

  public ArchivedLoanApplication(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }

  public ArchivedLoanApplication(SessionResourceKit srk, int id) throws RemoteException, FinderException
  {
    super(srk);

    pk = new ArchivedLoanApplicationPK(id);

    findByPrimaryKey(pk);
  }

  private ArchivedLoanApplicationPK createPrimaryKey()throws CreateException
  {
     String sql = "Select ArchivedLoanApplicationseq.nextval from dual";
     int id = -1;

     try
     {
        int key = jExec.execute(sql);

        if(jExec.next(key))
          id = jExec.getInt(key,1);  // should only ever be one record
		else
		  throw new Exception();
        jExec.closeData(key);

      }
      catch (Exception e)
      {
          CreateException ce = new CreateException("ArchivedLoanApplication Entity create() exception getting archivedLoanApplicationId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;

      }

      return new ArchivedLoanApplicationPK(id);
  }

  /**
  * creates a ArchivedLoanApplication using the archivedLoanApplicationId with mininimum fields
  * id argument represents dealid.  SourceApp argument represents SourceApplicationId
  */
  public ArchivedLoanApplication create(int id, String SourceApp)throws RemoteException, CreateException
  {
    pk = createPrimaryKey();

    StringBuffer sql = new StringBuffer ("Insert into ArchivedLoanApplication (archivedLoanApplicationId, institutionprofileid, sourceapplicationid, dealid, dealXML) Values ( ");
    sql.append(sqlStringFrom(pk.getId()) + ", ");    //archivedLoanApplicationId
    sql.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ", ");   //institutionprofileid
    sql.append(sqlStringFrom(SourceApp) + ", ");   //sourceapplicationid
    sql.append(sqlStringFrom(id) + ", ");   //dealid
    sql.append(sqlStringFrom(" ")+ ")");

    String sqlString = new String(sql);
    try
    {
      jExec.executeUpdate(sqlString);
      findByPrimaryKey(pk);
      trackEntityCreate ();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("ArchivedLoanApplication Entity - ArchivedLoanApplication - create() exception", e);
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);
    return this;
  }


  public ArchivedLoanApplication findByPrimaryKey(ArchivedLoanApplicationPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from ArchivedLoanApplication " + pk.getWhereClause();

    try
    {
        int key = jExec.execute(sql);

		if (jExec.next(key))
			setPropertiesFromQueryResult(key);
		else
		{
			String msg = "ArchivedLoanApplication Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
			logger.error(msg);
          	throw new FinderException(msg);
		}

        jExec.closeData(key);

      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("ArchivedLoanApplication Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }

  /**
   * setPropertiesFromQueryResult
   * This method sets the values in entity to save data in database.
   */
  private void setPropertiesFromQueryResult(int key) throws Exception
  {
    setArchivedLoanApplicationId( jExec.getInt(key, "ARCHIVEDLOANAPPLICATIONID"));
    setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    setSourceApplicationId(jExec.getString(key, "SOURCEAPPLICATIONID"));
    setDealId(jExec.getInt(key, "DEALID"));
    setReceivedTimeStamp(jExec.getDate(key, "RECEIVEDTIMESTAMP"));
    setArchivedTimeStamp(jExec.getDate(key, "ARCHIVEDTIMESTAMP"));
    setDealXML(readClob(jExec.getClob(key, "DEALXML")));
  }


  protected int performUpdate()  throws Exception
  {
	//return doPerformUpdate(this, this.getClass());
	int ret = doPerformUpdate(new String[] { "dealXML" });
	forceUpdateDealXML();
	return ret;
  }

  //stolen from Alvin....
  private void forceUpdateDealXML() throws SQLException, IOException 
  {
	Connection connection = jExec.getCon();
	PreparedStatement statement = null;
	String updateString = "UPDATE archivedLoanApplication SET dealXML=? WHERE archivedLoanApplicationId=?";
	try 
	{
		statement = connection.prepareStatement(updateString);
		statement.setClob(1, createClob(dealXML));
		statement.setInt(2, pk.getId());
		statement.executeUpdate();
	} 
	catch (SQLException sqle) 
	{
		try 
		{
			jExec.rollback();
		}
		catch (Exception e) //IllegalStateException, JdbcTransactionException
		{ 
			logger.error("Rollback failed.");
			logger.error(e);
		}
		throw sqle;
	} 
	catch (IOException ioe) 
	{
		try 
		{
			jExec.rollback();
		} 
		catch (Exception e) //IllegalStateException, JdbcTransactionException
		{
			logger.error("Rollback failed.");
			logger.error(e);
		}
		throw ioe;
	} 
	finally 
	{
		try 
		{
			statement.close();
		}
		catch (SQLException sqle) 
		{
			logger.warn(sqle.getMessage());
		}
	}
  }
  
  
  public String getEntityTableName()
  {
    return "archivedLoanApplication";
  }

  /**
  *  gets the value of instance fields as String.
  *  if a field does not exist (or the type is not serviced)** null is returned.
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *  @returns value of bound field as a String;
  *  @param  fieldName as String
  *
  *  Other entity types are not yet serviced   ie. You can't get the Province object
  *  for province.
  */
  public String getStringValue(String fieldName)
  {
   return doGetStringValue(this, this.getClass(), fieldName);
  }

  public Object getFieldValue(String fieldName){
    return doGetFieldValue(this, this.getClass(), fieldName);
  }


  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  @param  fieldName as String
  *  @param  the value as a String
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }


  /**
  *   gets the pk associated with this entity
  *
  *   @return a archivedLoanPK
  */
  public IEntityBeanPK getPk()
  {
    return this.pk ;
  }
	
  private void setArchivedLoanApplicationId(int id)
  {
   this.testChange ("archivedLoanApplicationId", id );
   this.archivedLoanApplicationId = id;
  }

  public int getArchivedLoanApplicationId()
  {
    return this.archivedLoanApplicationId ;
  }

  public void setSourceApplicationId(String sourceAppId)
  {
    this.testChange ("sourceApplicationId", sourceAppId);
    this.sourceApplicationId = sourceAppId;
  }

  public String getSourceApplicationId()
  {
    return this.sourceApplicationId;
  }

  private void setDealId(int id)
  {
    this.testChange ("dealId", id );
    this.dealId = id;
  }

  public int getDealId()
  {
    return this.dealId;
  }

  public void setReceivedTimeStamp(Date date)
  {
    this.testChange ("receivedTimeStamp", date);
    this.receivedTimeStamp = date;
  }

  public Date getReceivedTimeStamp()
  {
    return this.receivedTimeStamp;
  }

  public void setArchivedTimeStamp(Date date)
  {
    this.testChange ("archivedTimeStamp", date);
    this.archivedTimeStamp = date;
  }

  public Date getArchivedTimeStamp()
  {
    return this.archivedTimeStamp;
  }

  public void setDealXML(String XMLPayload)
  {
    this.testChange("dealXML", XMLPayload);
    this.dealXML = XMLPayload;
  }

  public String getDealXML()
  {
    return this.dealXML;
  }

  public int getInstProfileId()
  {
	return instProfileId;
  }

  public void setInstProfileId(int instProfileId)
  {
     this.testChange("INSTITUTIONPROFILEID", instProfileId);
     this.instProfileId = instProfileId;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof ArchivedLoanApplication )
      {
        ArchivedLoanApplication oa = (ArchivedLoanApplication)  o;
        if  (archivedLoanApplicationId == oa.getArchivedLoanApplicationId())
            return true;
      }
    return false;
  }

}
