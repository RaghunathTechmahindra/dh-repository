package com.basis100.deal.entity.simplecache;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ChangeMapValue;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.EntityBuilder;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.EntityBase;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.state.ExpressState;

public class ThreadLocalEntityCache {

	private final static boolean ENABLED 
	= "Y".equals(PropertiesCache.getInstance().getProperty(-1, 
			"com.basis100.deal.entity.simpleentitycache", "N"));

	private final static SimpleEntityCacheConfig config 
	= (SimpleEntityCacheConfig) ExpressCoreContext.getService("simpleEntityCacheConfig");

	private final static Logger logger 
	= LoggerFactory.getLogger(ThreadLocalEntityCache.class);

	private static ThreadLocal<Map<PKWrapper, DealEntity>> threadLocalEntityMap 
	= new ThreadLocal<Map<PKWrapper, DealEntity>>() {
		protected synchronized HashMap<PKWrapper, DealEntity> initialValue() {
			return new HashMap<PKWrapper, DealEntity>();
		}
	};

	private static ThreadLocal<Boolean> suspended 
	= new ThreadLocal<Boolean>() {
		protected synchronized Boolean initialValue() {
			return Boolean.FALSE;
		}
	};

	private static Map<Class<? extends DealEntity>, List<Field>> fieldsListMap
	= new ConcurrentHashMap<Class<? extends DealEntity>, List<Field>>();

	/**
	 * turn off temporally. clearCache will turn on again 
	 */
	public static void suspend() {
		suspended.set(Boolean.TRUE);
	}

	/**
	 * write fields value to cloned deal entity and cache it with threadLocal
	 */
	public static void writeToCache(DealEntity de, IEntityBeanPK pk){

		if (ENABLED == false) return;
		if (suspended.get()) return;

		validateEntity(de);
		validatePK(pk);

		if(isTargetEntity(de) == false)return;

		//don't use cache when deal institution is not set 
		int institution = getInstId(de);
		if(institution < 0) return; 

		try{
			DealEntity newEntity = EntityBuilder.createBlankEntity(de.getClass());
			copyFields(de, newEntity);
			threadLocalEntityMap.get().put(new PKWrapper(pk, institution), newEntity);
		} catch (Exception e) {
			logger.error("feild to write to cache", e);
			throw new ExpressRuntimeException(e);
		}
	}

	/**
	 * this method returns true, when a saved entity is found and succeed to
	 * copy its fields' value to passed DealEntity object
	 */
	public static boolean readFromCache(DealEntity de, IEntityBeanPK pk) {
		if (ENABLED == false) return false;
		if (suspended.get()) return false; //temporally off

		validateEntity(de);
		validatePK(pk);

		if(isTargetEntity(de) == false) return false;

		//don't use cache when deal institution is not set 
		int institution = getInstId(de);
		if(institution < 0) return false; 

		PKWrapper pkw = new PKWrapper(pk, institution);
		DealEntity saved = threadLocalEntityMap.get().get(pkw);
		if(saved == null) return false; //no cached dealEntity found
		try{
			copyFields(saved, de);
			attachPK(de, pk);
		} catch (Exception e) {
			logger.error("fild to read from cache", e);
			throw new ExpressRuntimeException(e);
		}
		return true;

	}


	/**
	 * remove cached entity
	 * @return true - cached object was deleted, false - no data was deleted
	 */
	public static void removeFromCache(DealEntity de, IEntityBeanPK pk) {
		if (ENABLED == false);
		if (suspended.get()); //temporally off

		validateEntity(de);
		validatePK(pk);

		if(isTargetEntity(de) == false) return;

		threadLocalEntityMap.get().remove(new PKWrapper(pk, getInstId(de)));
	}

	public static void removeFromCache(Class<? extends DealEntity> deClass) {
		if (deClass == null) return;
		if (ENABLED == false) return;
		if (suspended.get()) return; // temporally off
		if (isTargetEntity(deClass) == false) return;

		Map<PKWrapper, DealEntity> map = threadLocalEntityMap.get();
		for (Iterator<PKWrapper> it = map.keySet().iterator(); it.hasNext();) {
			if (deClass.equals(map.get(it.next()).getClass()))it.remove();
		}
	}

	/**
	 * clear all cached deal entity
	 */
	public static void clearCache() {

		if (ENABLED == false) return;

		threadLocalEntityMap.get().clear();
		suspended.set(Boolean.FALSE);
	}

	/**
	 * this method basically copies one deal-entity's fields to another.
	 * this method try to copy shallow reference as long as the
	 * value is immutable so as to improve performance.
	 */
	static void copyFields(DealEntity orig, DealEntity dest) throws IllegalArgumentException, 
	IllegalAccessException, InstantiationException, InvocationTargetException, 
	NoSuchMethodException, SecurityException, NoSuchFieldException{

		for(Field field : getTargetFiledList(orig.getClass())){

			Object value = field.get(orig);
			if (value==null) continue;
			if (value instanceof IEntityBeanPK) continue; //skip PK, it will be set in readFromCache

			try {
				value = cloneObject(value);
			} catch (Exception e) {
				throw new ExpressRuntimeException("faild to clone filed: " 
						+ orig.getClass().getSimpleName() + "." + field.getName(), e);
			}
			if(value == null) continue;

			field.set(dest, value);
		}

		//special handling for DocumentTracking.DocTrackingVerbiage
		if (orig instanceof DocumentTracking) {
			handleDocumentTrackingVerbiage((DocumentTracking)orig, (DocumentTracking)dest);
		}
		//special handling for Condition.ConditionVerbiage
		if (orig instanceof Condition){
			handleConditionVerbiage((Condition)orig, (Condition)dest);
		}
	}

	@SuppressWarnings("unchecked")
	static void handleDocumentTrackingVerbiage(DocumentTracking dtOrig, DocumentTracking dtDest) throws IllegalArgumentException, SecurityException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException{

		List<DocumentTracking.DocTrackingVerbiage> dtVerbOrigList = dtOrig.getVerb();
		for(DocumentTracking.DocTrackingVerbiage dtVerbOrig : dtVerbOrigList){

			DocumentTracking.DocTrackingVerbiage dtVerbDest = dtDest.new DocTrackingVerbiage(dtVerbOrig);
			//outer.srk -> inner.srk; can be null
			dtVerbDest.setSessionResourceKit(dtDest.getSessionResourceKit());
			//outer.dcm -> inner.dcm; can be null
			dtVerbDest.setCalcMonitor(dtDest.getCalcMonitor());
			//add to list
			dtDest.getVerb().add(dtVerbDest);
		}
	}

	static void handleConditionVerbiage(Condition condOrig, Condition condDest) throws IllegalArgumentException, SecurityException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException{

		List<Condition.ConditionVerbiage> condVerbOrigList = condOrig.getConditionVerbiages();
		for(Condition.ConditionVerbiage condVerbOrig : condVerbOrigList){
			Condition.ConditionVerbiage condVerbDest = condDest.new ConditionVerbiage(condVerbOrig);
			condDest.getConditionVerbiages().add(condVerbDest);
		}
	}

	/**
	 * this method puts together all copy-able fields per entity class.
	 * saves the result in static ConcurrentHashMap filed for reuse.
	 */
	@SuppressWarnings("unchecked")
	static List<Field> getTargetFiledList(Class<? extends DealEntity> entityClass){

		List<Field> fieldsList = fieldsListMap.get(entityClass);
		if(fieldsList != null) return fieldsList;

		fieldsList = new ArrayList<Field>();

		List<Class> copyTargetClasses = new ArrayList<Class>();
		copyTargetClasses.addAll(ClassUtils.getAllSuperclasses(entityClass));
		copyTargetClasses.add(entityClass);

		for(Class clazz : copyTargetClasses ){

			//skip Object,EntityBase - no fields need to be copied in these classes
			if(clazz == Object.class)continue;
			if(clazz == EntityBase.class)continue; //srk, jdbcExec, etc will be skipped

			for(Field field: clazz.getDeclaredFields()){
				Class fieldType = field.getType();
				if(fieldType == SessionResourceKit.class) continue;//skip srk
				if(fieldType == CalcMonitor.class) continue;//skip dcm
				//do not simply copy inner classes

				if(Modifier.isStatic(field.getModifiers())) continue;
				if(field.isAccessible() == false) field.setAccessible(true);
				//save to list
				fieldsList.add(field);
			}
		}
		//save list for reuse
		fieldsListMap.put(entityClass, fieldsList);
		logger.info("saved fildsList for class:" + entityClass.getSimpleName()
				+ " (size:" + fieldsList.size() + ") to fieldsListMap for reuse");

		return fieldsList;
	}

	/**
	 * returns cloned object. if the value is immutable, just returns the same
	 * object's reference.
	 */
	@SuppressWarnings("unchecked")
	static Object cloneObject(Object value) throws IllegalAccessException, 
	InstantiationException, InvocationTargetException, NoSuchMethodException{

		//it's ok to use shallow copy if the value's immutable
		//95% of fields will be caught by this line.
		if (checkMutable(value.getClass()) == false) return value; 

		Object cloned = null;

		if(value instanceof Date ){
			cloned = new Date(((Date)value).getTime());
		} else if(value instanceof Collection<?> ){

			//do not clone arrays that contains inner class
			Object sample = CollectionUtils.get(value, 0);
			if(sample instanceof DocumentTracking.DocTrackingVerbiage) return null;
			if(sample instanceof Condition.ConditionVerbiage) return null;

			cloned = value.getClass().newInstance();
			for(Object o : (Collection<?>)value){
				((Collection)cloned).add(cloneObject(o));
			}
		} else if(value instanceof Map<?,?> ){
			cloned = value.getClass().newInstance();
			for(Iterator<?> i = ((Map<?,?>)value).keySet().iterator(); i.hasNext();){
				Object key = i.next();
				((Map)cloned).put(cloneObject(key), cloneObject(((Map)value).get(key)));
			}
		} else if(value instanceof ChangeMapValue ){
			cloned = ((ChangeMapValue)value).duplicate();
		} else if (ClassUtils.isInnerClass(value.getClass())) {
			throw new InstantiationException(
					"Express tryed to clone inner Class. because inner class has "
					+ "hidden reference to its enclosed class's instance, you need "
					+ "to add special handler like 'handleDocumentTrackingVerbiage' method");
		} else {
			// if Express should execute this line, you will have to
			// add a logic that handles the field
			throw new InstantiationException(
					"Express doesn't know how to duplicate the field's value, "
					+ "Please add handling code to ThreadLocalEntityCache.cloneObject");
		}
		return cloned;
	}

	/**
	 * return if the filed is immutable(false) or mutable(true)
	 */
	static boolean checkMutable(Class<?> clazz){
		//95% of fields in entity beans are String or primitive type.
		//this method just exists not to deep clone everything.
		if(clazz.isPrimitive()) return false;
		//primitive fields will be converted to Object (char -> Character)
		if(clazz == String.class) return false;
		if(clazz == Integer.class) return false;
		if(clazz == Long.class) return false;
		if(clazz == Double.class) return false;
		if(clazz == Boolean.class) return false;
		if(clazz == Character.class) return false;
		if(clazz == Byte.class) return false;
		if(clazz == Short.class) return false;
		if(clazz == Float.class) return false;

		return true; //the field is mutable - need deep clone
	}

	/**
	 * returns DealInstitutionProfileId
	 */
	static int getInstId(DealEntity de){
		SessionResourceKit srk = de.getSessionResourceKit();
		if(srk == null) return -1;
		ExpressState expressState = srk.getExpressState();
		if(expressState == null) return -1;
		return expressState.getDealInstitutionId();
	}

	/**
	 * attaching PK object to DealEntity object. since pk filed exist in
	 * sub-class layer (like Deal, Borrower..), we don't have super method which
	 * sets PK object, thus this method use reflection to set PK.
	 */
	static void attachPK(DealEntity de, IEntityBeanPK pk)
	throws IllegalArgumentException, IllegalAccessException {

		Class<? extends IEntityBeanPK> pkClass = pk.getClass();
		for (Field field : de.getClass().getDeclaredFields()) {
			if (field.getType() == pkClass) {
				field.setAccessible(true);
				field.set(de, pk);
				return;
			}
		}
		throw new ExpressRuntimeException("no PK filed (" + pkClass.getName()
				+ ") found in the class : " + de.getClass().getName());
	}

	static void validateEntity(DealEntity de){
		if (de == null) throw new IllegalArgumentException("No entity bean specified");
		SessionResourceKit srk = de.getSessionResourceKit();
		if (srk == null) throw new IllegalArgumentException("No SessionResourceKit specified");
	}

	static void validatePK(IEntityBeanPK pk){
		if (pk == null) throw new IllegalArgumentException("No PK specified");
	}

	static boolean isTargetEntity(DealEntity de){
		return isTargetEntity(de.getClass());
	}

	static boolean isTargetEntity(Class<? extends DealEntity> clazz){

		String entityName = clazz.getSimpleName();
		for(String name : config.getTargetEntities()){
			if("*".equals(name))return true;
			if(entityName.equalsIgnoreCase(name))return true;
		}
		return false;
	}

}
