package com.basis100.deal.entity;

/**
 * 27/Feb/2007 DVG #DG582 FXP14477: NBC - PecEmail document not produced for deal 600802 
 */

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.*;
import com.basis100.resources.SessionResourceKit;

/**
 * 
 * <p>
 * Title: WorkflowTrigger
 * </p>
 * <p>
 * Description:
 * </p>
 * Implementation Note : As the Workflow Trigger is not considered as trackable
 * data of the deal the Entity Tracking mechanism is skipped.
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: Filgoix
 * </p>
 * 
 * @author Michael Morris
 * @version 1.0
 */

public class WorkflowTrigger extends DealEntity implements Cloneable {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 1L;

	private DealPK pk;

	private String value = null;

	private String triggerKey = null;

	private int dealId;
	
	private int institutionProfileId;

	public WorkflowTrigger(SessionResourceKit srk) throws RemoteException,
			FinderException {
		super(srk);
	}

	public WorkflowTrigger(SessionResourceKit srk, CalcMonitor dcm)
			throws RemoteException, FinderException {
		super(srk, dcm);
	}

	public void setValue(String value) {

		this.testChange("value", value);
		this.value = value;
	}

	public void setTriggerKey(String triggerKey) {

		this.testChange("workflowtriggerkey", triggerKey);
		this.triggerKey = triggerKey;
	}

	public void setDealId(int dealId) {

		this.testChange("dealId", dealId);
		this.dealId = dealId;
	}

	public String getValue() {
		return value;
	}

	public String getTriggerKey() {
		return triggerKey;
	}

	public int getDealId() {
		return dealId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.basis100.deal.entity.DealEntity#getEntityTableName() *
	 */
	public String getEntityTableName() {

		return "workflowtrigger";
	}

	/**
	 * findByDealId
	 * 
	 * @param int
	 *            dealId
	 * @throws RemoteException
	 * @throws FinderException
	 * @return Collection
	 */
	//--> Billy ==> Mike : may simplify to pass int Dealid instead
	//--> 08Nov2004
	public Collection findByDealId(int dealId) throws RemoteException,
			FinderException {

		Collection workflowTriggers = new ArrayList();

		String sql = "Select * from WorkflowTrigger " + " where dealId = " + dealId;
		boolean gotRecord = false;

		try {
			int key = jExec.execute(sql);

			for (; jExec.next(key);) {
				gotRecord = true;
				WorkflowTrigger trigger = new WorkflowTrigger(srk, dcm);
				trigger.setPropertiesFromQueryResult(key);
				trigger.pk = pk;
				workflowTriggers.add(trigger);
			}

			jExec.closeData(key);

			if (gotRecord == false) {
				String msg = "Workflow Trigger Entity: @findByDealId(), key= " + pk
						+ ", entity not found";
				logger.trace(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e) {
			FinderException fe = new FinderException();
			logger.error(e.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return workflowTriggers;
	}

	/**
	 * This method will create the trigger record in the DB if no item is found.
	 * The value element is not used in the query, it is used to set the value
	 * of the trigger at the time of query.
	 * 
	 * @param dealId
	 * @param triggerKey
	 * @return
	 * @throws FinderException
	 * @throws CreateException
	 */
	public WorkflowTrigger findByCompositeKey(int dealId, String triggerKey,
			String triggerValue) throws CreateException {
		try {
			findByCompositeKey(dealId, triggerKey);
			setValue(triggerValue);
		}
		catch (FinderException fe) {
			create(dealId, triggerKey, triggerValue);
		}
		return this;
	}

	//--> Billy ==> Mike : may simplify to pass int Dealid instead
	//--> 08Nov2004
	public WorkflowTrigger findByCompositeKey(int dealId, String triggerKey)
			throws FinderException {

		//Locate only one single workflow trigger based on the dealId and
		// triggerKey
		String sql = "Select * from workflowTrigger " + " where dealId = " + dealId
				+ " AND workflowtriggerkey = '" + triggerKey + "'";
		boolean gotRecord = false;

		try {
			int key = jExec.execute(sql);

			for (; jExec.next(key);) {
				gotRecord = true;

				this.setPropertiesFromQueryResult(key);
				this.pk = pk;
			}

			jExec.closeData(key);

			if (gotRecord == false) {
				String msg = "Workflow Trigger Entity: @findByDealId(), key= " + pk
						+ ", entity not found";
				logger.trace(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e) {
			if (!gotRecord && getSilentMode()) //#DG582 don't clutter log file
				throw (FinderException) e;
			FinderException fe = new FinderException();
			logger.error(e.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return this;
	}

	//--> Billy ==> Mike : May need a method : public String
	// getWorkflowTriggerValue(int dealid, String triggerKey)
	//--> return "" (empty string if key not found)
	//--> 08Nov2004
	public String getWorkflowTriggerValue(int dealId, String triggerKey) {

		String tVal = "";
		try {

			findByCompositeKey(dealId, triggerKey);
			tVal = getValue();
		}
		catch (FinderException fe) {

			//The exception was logged in the find method, and in this case we
			//will deal with this exception by returning an empty string.
		}

		return tVal;
	}

	//--> Billy ==> Mike : consider to change the method name to just "create"
	// with parameters as following :
	//--> int dealid, String triggerKey, String value
	//--> 08Nov2004
	public WorkflowTrigger create(int dealId, String triggerKey, String value)
			throws CreateException {

		String sql = "insert into workflowTrigger (dealId, workflowtriggerkey, value, institutionProfileId) ";
		sql = sql + "values (" + dealId + ", '" + triggerKey + "', '" + value + "', " + srk.getExpressState().getDealInstitutionId() 
				+ ")";

		try {
			int key = jExec.execute(sql);
			findByCompositeKey(dealId, triggerKey);
			trackEntityCreate();
			jExec.closeData(key);
		}
		catch (Exception e) {
			CreateException ce = new CreateException(
					"WorkflowTrigger Entity - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);

			throw ce;
		}

		return this;
	}

	//--> Billy ==> Mike : May need a method : public void
	// setWorkflowTriggerValue(int dealid, String triggerKey, String value)
	//--> If key exist ==> modify value, otherwise, create the record
	//--> 08Nov2004
	public void setWorkflowTriggerValue(int dealId, String triggerKey,
			String value) throws CreateException, RemoteException {

		try {
			// 3.2.2GR: added for clean log 
			setSilentMode(true);
			findByCompositeKey(dealId, triggerKey);
			setSilentMode(false);
		}
		catch (FinderException fe) {
			setSilentMode(false);
			/**
			 * If we recieve a FinderException, then that means the tuple does
			 * not exist and the record should be created.
			 */
			create(dealId, triggerKey, value);
		}
		setDealId(dealId);
		setTriggerKey(triggerKey);
		setValue(value);
		ejbStore();
	}

	//--> Billy ==> Mike : consider to override the parent's "ejbStore" method
	// for consistance :
	//--> No parameter input needed. ==> Review Implementation Note @ top.
	//--> 08Nov2004

	protected int performUpdate() throws Exception {

		logger.debug("====Performing update====");
		Class clazz = this.getClass();

		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.basis100.deal.entity.DealEntity#doPerformUpdate(java.lang.Object,
	 *      java.lang.Class)
	 */
	protected int doPerformUpdate(Object me, Class meInfo) throws Exception {

		StringBuffer buffer = new StringBuffer();

		buffer.append("Update " + getEntityTableName() + " set ");

		if (formUpdateFieldsList(buffer, true) == null) {
			return 0;
		}

		buffer.append(" where dealId = " + dealId + " and workflowtriggerkey = '"
				+ getTriggerKey()+ "'");

		String sql = new String(buffer);
		int ret = jExec.executeUpdate(sql);

		return ret;
	}

	//--> Billy ==> Mike : consider to override the parent's "ejbRemove" method
	// for consistance :
	//--> No parameter input needed. ==> Review Implementation Note @ top.
	//--> 08Nov2004
	public void deleteWorkflowTrigger(DealPK pk) throws RemoveException {

		String sql = "delete from WorkflowTrigger where dealId = " + pk.getId();
		sql = sql + " AND workflowtriggerkey = '" + getTriggerKey() + "'";

		try {
			int key = jExec.executeUpdate(sql);
		}
		catch (Exception e) {
			RemoveException re = new RemoveException(
					"WorkflowTrigger Entity - deleteWorkflowTrigger() exception");
			logger.error(re.getMessage());
			logger.error(e);

			throw re;
		}

	}

	public void ejbRemove(boolean needCalculation) throws RemoteException {
		String sql = null;

		try {

			sql = "Delete From " + this.getEntityTableName() + " where dealId = "
					+ getDealId() + " and workflowtriggerkey = '" + getTriggerKey() + "'";
			jExec.executeUpdate(sql);

			this.trackEntityRemoval(); // Track the Removal

			if (dcm == null)
				return; //if the dcm is null then we don't need to visit the
			// rest of this method - brad

			if (needCalculation) { // If need Calculation ..........
				dcm.inputRemovedEntity(this);
				dcm.removeTarget(this);
				dcm.calc();
			}
			dcm.removeInput(this);
		}
		catch (Exception e) {
			String msg = "Failed to remove record " + "where dealId = " + getDealId();
			RemoteException re = new RemoteException(msg);
			logger.error(re.getMessage());
			logger.error("remove sql: " + sql);
			logger.error(e);

			throw re;
		}

	}

	//--> Billy ==> Mike : May need a method : public void
	// deleteWorkflowTrigger(int dealid, String triggerKey) ??
	//--> 08Nov2004

	private void setPropertiesFromQueryResult(int key) throws Exception {

		int index = 0;
		this.setDealId(jExec.getInt(key, ++index));
		this.setTriggerKey(jExec.getString(key, ++index));
		this.setValue(jExec.getString(key, ++index));
	}

	public String toString() {

		return getDealId() + " / " + getTriggerKey() + " / " + getValue() + " / ";
	}

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}
