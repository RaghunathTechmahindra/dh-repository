
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.BorrowerAddressPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class BorrowerAddress extends DealEntity {
    protected int borrowerAddressId;
    protected int copyId;
    protected int instProfileId;
    protected int borrowerAddressTypeId;
    protected int borrowerId;
    protected int addrId;
    protected int monthsAtAddress;
    protected int residentialStatusId;
    private int institutionProfileId;
    // ============================================================

    private BorrowerAddressPK pk;

    public BorrowerAddress(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    public BorrowerAddress(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException {

        super(srk, dcm);
    }

    public BorrowerAddress(SessionResourceKit srk, CalcMonitor dcm, int id,
            int copyId) throws RemoteException, FinderException {

        super(srk, dcm);

        pk = new BorrowerAddressPK(id, copyId);

        findByPrimaryKey(pk);
    }

    public BorrowerAddress deepCopy() throws CloneNotSupportedException {

        return (BorrowerAddress) this.clone();
    }

    public BorrowerAddress findByPrimaryKey(BorrowerAddressPK pk)
            throws RemoteException, FinderException {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from BorrowerAddress " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public BorrowerAddress findByAddr(AddrPK pk) throws RemoteException,
            FinderException {

        String sql = "Select * from BorrowerAddress " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = new BorrowerAddressPK(borrowerAddressId, copyId);
        return this;
    }

    public BorrowerAddress findByCurrentAddress(int borrowerId, int copyId)
            throws RemoteException, FinderException {

        String sql = "Select * from BorrowerAddress where borrowerId = "
                + borrowerId + " AND borrowerAddressTypeId = "
                + Mc.BT_ADDR_TYPE_CURRENT + " AND copyId = " + copyId;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "BorrowerAddress: @findByCurrentAddress(), borrower Id = "
                        + borrowerId + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {

            FinderException fe = new FinderException(
                    "BorrowerAddress - findByCurrentAddress() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.pk = new BorrowerAddressPK(this.getBorrowerAddressId(), this
                .getCopyId());
        return this;
    }

    public BorrowerAddress findBySaleAddress(int borrowerId, int copyId)
            throws RemoteException, FinderException {

        String sql = "Select * from BorrowerAddress where borrowerId = "
                + borrowerId + " AND borrowerAddressTypeId = "
                + Mc.BT_ADDR_TYPE_SALE_PROPERTY + " AND copyId = " + copyId;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "BorrowerAddress: @findByCurrentAddress(), borrower Id = "
                        + borrowerId + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {

            FinderException fe = new FinderException(
                    "BorrowerAddress - findByCurrentAddress() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.pk = new BorrowerAddressPK(this.getBorrowerAddressId(), this
                .getCopyId());
        return this;
    }

    public Collection<BorrowerAddress> findByBorrower(BorrowerPK pk) throws Exception {

        Collection<BorrowerAddress> baddrs = new ArrayList<BorrowerAddress>();

        String sql = "Select * from BorrowerAddress ";
        sql += pk.getWhereClause();
        sql += " order by borrowerAddressTypeId";

        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                BorrowerAddress ba = new BorrowerAddress(srk, dcm);
                ba.setPropertiesFromQueryResult(key);
                ba.pk = new BorrowerAddressPK(ba.getBorrowerAddressId(), ba
                        .getCopyId());

                baddrs.add(ba);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while gettinging Borrower::BorrowerAddresss");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return baddrs;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

        setBorrowerAddressId(jExec.getInt(key, "BORROWERADDRESSID"));
        setBorrowerAddressTypeId(jExec.getInt(key, "BORROWERADDRESSTYPEID"));
        setBorrowerId(jExec.getInt(key, "BORROWERID"));
        setMonthsAtAddress(jExec.getShort(key, "MONTHSATADDRESS"));
        setAddrId(jExec.getInt(key, "ADDRID"));
        setCopyId(jExec.getInt(key, "COPYID"));
        setResidentialStatusId(jExec.getInt(key, "RESIDENTIALSTATUSID"));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public Object getFieldValue(String fieldName) {

        return doGetFieldValue(this, this.getClass(), fieldName);
    }

    public List getSecondaryParents() throws Exception {

        List sps = new ArrayList();

        sps.add(getAddr());

        return sps;
    }

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    public String getEntityTableName() {

        return "BorrowerAddress";
    }

    /**
     * gets the monthsAtAddress associated with this entity
     * 
     * @return an int
     */
    public int getMonthsAtAddress() {

        return this.monthsAtAddress;
    }

    /**
     * gets the borrowerAddressType associated with this entity
     * 
     * @return a String
     */
    public int getBorrowerAddressTypeId() {

        return this.borrowerAddressTypeId;
    }

    /**
     * gets the addr associated with this entity
     * 
     * @return a Addr
     */
    public Addr getAddr() throws FinderException, RemoteException {

        return new Addr(srk, this.addrId, this.getCopyId());
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a BorrowerAddressPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * gets the borrowerId associated with this entity
     * 
     * @return a int
     */
    public int getBorrowerId() {

        return this.borrowerId;
    }

    /**
     * gets the borrowerAddressId associated with this entity
     * 
     * @return a int
     */
    public int getBorrowerAddressId() {

        return this.borrowerAddressId;
    }

    /**
     * gets the addrId associated with this entity
     * 
     * @return a int
     */
    public int getAddrId() {

        return this.addrId;
    }

    public int getClassId() {

        return ClassId.BORROWERADDRESS;
    }

    public boolean isAuditable() {

        return true;
    }

    private BorrowerAddressPK createPrimaryKey(int copyId)
            throws CreateException {

        String sql = "Select BorrowerAddressseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {

            CreateException ce = new CreateException(
                    "BorrowerAddress Entity create() exception getting BorrowerAddressId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }

        return new BorrowerAddressPK(id, copyId);
    }

    /**
     * creates an BorrowerAddress using the BorrowerAddressId - the mininimum
     * (ie.non-null) fields
     * 
     */
    public BorrowerAddress create(BorrowerPK bpk) throws RemoteException,
            CreateException {

        pk = createPrimaryKey(bpk.getCopyId());
        int addrid = -1;

        try {
            Addr add = new Addr(srk);
            add.create(pk);
            addrid = add.getAddrId();
        } catch (Exception ex) {
            CreateException ce = new CreateException(
                    "Addr Entity - BorrowerAddress - create() exception");
            logger.error(ce.getMessage());
            logger.error(ex);

            throw ce;
        }

        // Check if the current address exists. If yes => default address type
        // to
        // PREVIOUS
        // -- By BILLY 18Jan2001
        int theAddressType;
        if (isCurrentAddress(bpk.getId(), pk.getCopyId())) {
            theAddressType = Mc.BT_ADDR_TYPE_PREVIOUS;
        } else {
            theAddressType = Mc.BT_ADDR_TYPE_CURRENT;
        }

        String sql = "Insert into BorrowerAddress( borrowerId, borrowerAddressId, addrid, copyId, borrowerAddressTypeId, INSTITUTIONPROFILEID) Values ( "
                + bpk.getId()
                + ", "
                + pk.getId()
                + ", "
                + addrid
                + ", "
                + pk.getCopyId()
                + ", "
                + theAddressType
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            this.findByPrimaryKey(pk);
            this.trackEntityCreate(); // Track the create
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "BorrowerAddress Entity - BorrowerAddress - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);
        this.pk = pk;

        return this;
    }

    /**
     * Creates the BorrowerAddress and Copy the address data from the
     * srcBAddress -- By Billy 20March2001
     */
    public BorrowerAddress copyBAddress(Borrower theBorrower,
            BorrowerAddress srcBAddress) throws RemoteException,
            CreateException {

        pk = createPrimaryKey(theBorrower.getCopyId());
        int addrid = -1;

        try {
            Addr add = new Addr(srk);
            add.create(pk);
            // Copy all address data from srcAddress
            Addr srcAdd = srcBAddress.getAddr();
            add.setAddressLine1(srcAdd.getAddressLine1());
            add.setAddressLine2(srcAdd.getAddressLine2());
            add.setCity(srcAdd.getCity());
            add.setPostalFSA(srcAdd.getPostalFSA());
            add.setPostalLDU(srcAdd.getPostalLDU());
            add.setProvinceId(srcAdd.getProvinceId());
            add.ejbStore();
            addrid = add.getAddrId();
        } catch (Exception ex) {
            CreateException ce = new CreateException(
                    "Addr Entity - BorrowerAddress - copyBAddress() exception");
            logger.error(ce.getMessage());
            logger.error(ex);

            throw ce;
        }

        int theAddressType = Mc.BT_ADDR_TYPE_CURRENT;
        int theMonthAtAddr = srcBAddress.getMonthsAtAddress();

        String sql = "Insert into BorrowerAddress( borrowerId, borrowerAddressId, addrid, copyId, borrowerAddressTypeId, monthsataddress, INSTITUTIONPROFILEID) Values ( "
                + theBorrower.getBorrowerId()
                + ", "
                + pk.getId()
                + ", "
                + addrid
                + ", "
                + pk.getCopyId()
                + ", "
                + theAddressType
                + ", "
                + theMonthAtAddr
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            this.findByPrimaryKey(pk);
            this.trackEntityCreate(); // Track the create
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "BorrowerAddress Entity - BorrowerAddress - copyBAddress() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);
        this.pk = pk;

        return this;
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public void setBorrowerAddressId(int id) {

        this.testChange("borrowerAddressId", id);
        this.borrowerAddressId = id;
    }

    public void setBorrowerAddressTypeId(int type) {

        this.testChange("borrowerAddressTypeId", type);
        this.borrowerAddressTypeId = type;
    }

    public void setBorrowerId(int id) {

        this.testChange("borrowerId", id);
        this.borrowerId = id;
    }

    private void setAddrId(int id) {

        this.testChange("addrId", id);
        this.addrId = id;
    }

    public void setMonthsAtAddress(int months) {

        this.testChange("monthsAtAddress", months);
        this.monthsAtAddress = months;
    }

    public int getCopyId() {

        return this.copyId;
    }

    public void setCopyId(int copyId) {

        this.testChange("copyId", copyId);
        this.copyId = copyId;
    }

    // Additional field for CMHC modification -- By BILLY 14Feb2002
    public int getResidentialStatusId() {

        return this.residentialStatusId;
    }

    public void setResidentialStatusId(int value) {

        this.testChange("residentialStatusId", value);
        this.residentialStatusId = value;
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    // =============================================================

    public Vector findByMyCopies() throws RemoteException, FinderException {

        Vector v = new Vector();

        String sql = "Select * from BorrowerAddress where BorrowerAddressId = "
                + getBorrowerAddressId() + " AND copyId <> " + getCopyId();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                BorrowerAddress iCpy = new BorrowerAddress(srk);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new BorrowerAddressPK(iCpy.getBorrowerAddressId(),
                        iCpy.getCopyId());

                v.addElement(iCpy);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "BorrowerAddress - findBy  () exception");

            logger.error(e.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return v;
    }

    public void ejbRemove() throws RemoteException {

        ejbRemove(false);
    }

    public void ejbRemove(boolean calc) throws RemoteException {

        try {
            String sql = "Delete From BorrowerAddress "
                    + this.pk.getWhereClause();
            jExec.executeUpdate(sql);

            Addr addr = this.getAddr();

            if (addr != null) {
                addr.ejbRemove(false); // remvoe address should not trigger
                // calculation
            }

        } catch (Exception e) {
            String msg = "Exception removing BorrowerAddress record: "
                    + this.pk;

            logger.error(msg);
            logger.error(e);

            throw new RemoteException(msg);
        }
        
        //4.4 Entity Cache
        ThreadLocalEntityCache.removeFromCache(this, pk);

    }

    protected EntityContext getEntityContext() throws RemoteException {

        EntityContext ctx = this.entityContext;
        try {
            Addr addr = new Addr(srk, this.getAddrId(), this.getCopyId());
            if (addr == null) {
                return ctx;
            }
            ctx.setContextText(addr.getAddressLine1());

            Borrower borrower = new Borrower(srk, null, this.borrowerId,
                    this.copyId);
            if (borrower == null) {
                return ctx;
            }

            ctx.setContextSource(borrower.getNameForEntityContext());

            // Modified by Billy for performance tuning -- By BILLY 28Jan2002
            // Deal deal = new Deal( srk, null);
            // deal = deal.findByPrimaryKey( new DealPK( borrower.getDealId () ,
            // borrower.getCopyId () ) ) ;
            // if ( deal == null )
            // return ctx;
            // String sc = String.valueOf ( deal.getScenarioNumber () ) +
            // deal.getCopyType();
            Deal deal = new Deal(srk, null);
            String sc = String.valueOf(deal.getScenarioNumber(borrower
                    .getDealId(), borrower.getCopyId()))
                    + deal.getCopyType(borrower.getDealId(), borrower
                            .getCopyId());
            // ===================================================================================

            ctx.setScenarioContext(sc);

            ctx.setApplicationId(borrower.getDealId());
        } catch (Exception e) {
            if (e instanceof FinderException) // If FinderException is thrown.
            {
                logger
                        .warn("BorrowerAddress.getEntityContext Parent Not Found. BorrowerAddressId="
                                + this.borrowerAddressId);
                return ctx;
            }
            throw (RemoteException) e;
        }

        return ctx;
    }// -------------------------- end of getEntityContext

    // -------------------------------------------

    public boolean equals(Object o) {

        if (o instanceof BorrowerAddress) {
            BorrowerAddress oa = (BorrowerAddress) o;
            if ((this.borrowerAddressId == oa.getBorrowerAddressId())
                    && (this.copyId == oa.getCopyId())) {
                return true;
            }
        }
        return false;
    }

    // New method to check if the Current address exist -- By BILLY 18Jan2001
    public boolean isCurrentAddress(int borrowerId, int copyId) {

        String sql = "Select * from BorrowerAddress where borrowerId = "
                + borrowerId + " AND borrowerAddressTypeId = '"
                + Mc.BT_ADDR_TYPE_CURRENT + "' AND copyId = " + copyId;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                break; // can only be one record
            }

            jExec.closeData(key);

        } catch (Exception e) {
            gotRecord = false;

        }

        return gotRecord;
    }

}
