/**
 * <p>
 * Title: Payload.java
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jul 5, 2006)
 * 
 */

package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PayloadTypePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class PayloadType extends DealEntity {
    protected int payloadTypeId;
    protected int documentTypeId;
    protected String payloadTypeDesc;
    protected String payloadVersion;
    protected String xmlSchemaName;
    protected String noNameSchemaLocation;
    protected int institutionProfileId;
    private PayloadTypePK pk;

    /**
     * Simple Constructor. Create an empty PayloadType.
     * 
     * @param srk
     * @throws RemoteException
     * @throws FinderException
     */
    public PayloadType(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    /**
     * Create a Channel and populate it from the database.
     * 
     * @param srk
     *            SessionResourceKit to use
     * @param id
     *            The Channel's ID.
     * @throws RemoteException
     * @throws FinderException
     */
    public PayloadType(SessionResourceKit srk, int id) throws RemoteException,
            FinderException {

        super(srk);

        findByPrimaryKey(pk);
    }

    /**
     * @return the Channel's primary key
     */
    public IEntityBeanPK getPk() {

        return pk;
    }

    /**
     * @return Returns the name of the DB Table this class represents.
     */
    public String getEntityTableName() {

        return "PayloadType";
    }

    /**
     * Tests whether this Channel is equal to the given (Channel) object.
     * 
     * @param obj
     *            The Object to test for equality.
     */
    public boolean equals(Object obj) {

        if (obj instanceof PayloadType) {
            PayloadType otherObj = (PayloadType) obj;
            if (this.payloadTypeId == otherObj.getPayloadTypeId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the value of instance fields as String. If a field does not exist
     * (or the type is not serviced)** null is returned. If the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    private PayloadType findByQuery(String sql) throws FinderException {

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Channel Entity: @findByQuery(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Channel Entity - findByQuery() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

    /**
     * Populates this Channel with information identified by the given primary
     * key.
     * 
     * @param pk
     *            The Primary Key corresponding to required information.
     * @return This, populated with the appropriate information.
     * @throws FinderException
     */
    public PayloadType findByPrimaryKey(PayloadTypePK pk)
            throws FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "SELECT * FROM PAYLOADTYPE " + pk.getWhereClause();
        this.pk = pk;
        findByQuery(sql);
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

        this.setPayloadTypeId(jExec.getInt(key, "PAYLOADTYPEID"));
        this.setDocumentTypeId(jExec.getInt(key, "DOCUMENTTYPEID"));
        this.setPayloadTypeDesc(jExec.getString(key, "PAYLOADTYPEDESC"));
        this.setPayloadVersion(jExec.getString(key, "PAYLOADVERSION"));
        this.setXmlSchemaName(jExec.getString(key, "XMLSCHEMANAME"));
        this.setNoNameSchemaLocation(jExec.getString(key,
                "NONAMESCHEMALOCAtion"));
        this.setInstitutionProfileId(jExec.getInt(key, "InstitutionProfileId"));
    }

    /**
     * Performs database update.
     * 
     * @throws Exception
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        int ret = doPerformUpdate(this, clazz);

        return ret;
    }

    // ----------- setters / getters ----------- start

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    public int getDocumentTypeId() {

        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {

        this.documentTypeId = documentTypeId;
    }

    public String getNoNameSchemaLocation() {

        return noNameSchemaLocation;
    }

    public void setNoNameSchemaLocation(String noNameSchemaLocation) {

        this.noNameSchemaLocation = noNameSchemaLocation;
    }

    public String getPayloadTypeDesc() {

        return payloadTypeDesc;
    }

    public void setPayloadTypeDesc(String payloadTypeDesc) {

        this.payloadTypeDesc = payloadTypeDesc;
    }

    public int getPayloadTypeId() {

        return payloadTypeId;
    }

    public void setPayloadTypeId(int payloadTypeId) {

        this.payloadTypeId = payloadTypeId;
    }

    public String getPayloadVersion() {

        return payloadVersion;
    }

    public void setPayloadVersion(String payloadVersion) {

        this.payloadVersion = payloadVersion;
    }

    public String getXmlSchemaName() {

        return xmlSchemaName;
    }

    public void setXmlSchemaName(String xmlSchemaName) {

        this.xmlSchemaName = xmlSchemaName;
    }

}
