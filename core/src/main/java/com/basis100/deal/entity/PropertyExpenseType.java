
package com.basis100.deal.entity;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PropertyExpenseTypePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class PropertyExpenseType extends DealEntity {

    protected int propertyExpenseTypeId;
    protected int GDSInclusion;
    protected int TDSInclusion;
    protected String PETDescription;

    private PropertyExpenseTypePK pk;

    public PropertyExpenseType(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    public PropertyExpenseType(SessionResourceKit srk, int id)
            throws RemoteException, FinderException {

        super(srk);

        pk = new PropertyExpenseTypePK(id);

        findByPrimaryKey(pk);
    }

    public PropertyExpenseType findByPrimaryKey(PropertyExpenseTypePK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from PropertyExpenseType " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "PropertyExpenseType: @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "PropertyExpenseType Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
        setPropertyExpenseTypeId(jExec.getInt(key, "PropertyExpenseTypeId"));
        setPETDescription(jExec.getString(key, "PETDescription"));
        setGDSInclusion(jExec.getInt(key, "GDSInclusion"));
        setTDSInclusion(jExec.getInt(key, "TDSInclusion"));
    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        return ret;
    }

    public String getEntityTableName() {

        return "PropertyExpenseType";
    }

    /**
     * gets the propertyExpenseId associated with this entity
     * 
     * @return a int
     */
    public int getPropertyExpenseTypeId() {

        return this.propertyExpenseTypeId;
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a PropertyExpenseTypePK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public int getGDSInclusion() {

        return this.GDSInclusion;
    }

    public int getTDSInclusion() {

        return this.TDSInclusion;
    }

    public String getPETDescription() {

        return this.PETDescription;
    }

    public PropertyExpenseType create(int id, String desc, double gdsInc,
            double tdsInc) throws RemoteException, CreateException {

        String sql = "Insert into PropertyExpenseType Values ( " + id + ",'"
                + desc + "'," + gdsInc + "," + tdsInc + ")";

        // Added here
        pk = new PropertyExpenseTypePK(id);

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PropertyExpenseType Entity - PropertyExpenseType - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);

        if (dcm != null) {
            // dcm.inputEntity(this);
            dcm.inputCreatedEntity(this);
        }

        return this;
    }

    public PropertyExpenseType deepCopy() throws CloneNotSupportedException {

        return (PropertyExpenseType) this.clone();
    }

    public void setPropertyExpenseTypeId(int id) {

        this.testChange("propertyExpenseTypeId", id);
        this.propertyExpenseTypeId = id;
    }

    public void setGDSInclusion(int c) {

        this.testChange("GDSInclusion", c);
        this.GDSInclusion = c;
    }

    public void setTDSInclusion(int c) {

        this.testChange("TDSInclusion", c);
        this.TDSInclusion = c;
    }

    public void setPETDescription(String desc) {

        this.testChange("PETDescription", desc);
        this.PETDescription = desc;
    }

    public int getClassId() {

        return ClassId.DEFAULT;
    }

    public boolean equals(Object o) {

        if (o instanceof PropertyExpenseType) {
            PropertyExpenseType oa = (PropertyExpenseType) o;
            if (this.propertyExpenseTypeId == oa.getPropertyExpenseTypeId()) {
                return true;
            }
        }
        return false;
    }

}
