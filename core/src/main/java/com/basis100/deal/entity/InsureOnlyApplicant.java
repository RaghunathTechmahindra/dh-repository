/*
 * Created on Nov 18, 2004 Author : Neil Kong
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;


/**
 * <p>Title: FXpressJ2EE</p>
 * <p>Description: FXpress NetDynamics project converted to the J2EE realm with the JATO as an iPlanet Application Framework.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Filogix</p>
* @author Neil Kong on Nov/18/2004
 * @version 1.0
*
* 06/Jan/2005 DVG #DG124 cr#136 INSUREONLYAPPLICANT ingestion, minor correction
 * Jan/2005 vlad various minor fixes (PK for LifeDisPremiumsOnlyA, etc.)
 * and revision to the express commons (setPropertiesFromQueryResult(key) for example).
 * IMPORTANT CHANGES!
 * 1. testChange(parameter) method is case sensitive and parameter MUST be
 * identical to an appropriate tag.
 *
 * 2. findByDeal(DealPK dpk) method is revisited in order to trigger
 * ChangeValueMap during the ingestion.
 */
public class InsureOnlyApplicant extends DealEntity
{
  //#DG124 fields must be protected instead of private

    protected int insureOnlyApplicantId;
    protected int copyId;
    protected int dealId;
    protected String insureOnlyApplicantFirstName;
    protected String insureOnlyApplicantLastName;
    protected Date insureOnlyApplicantBirthDate;
    protected char primaryInsureOnlyApplicantFlag = 'N';
    protected int insureOnlyApplicantGenderId;
    protected int insuranceProportionsId;
    protected double lifePercentCoverageReq;
    protected int smokeStatusId;
    protected int disabilityStatusId;
    protected int lifeStatusId;
    protected double disPercentCoverageReq;
    protected int insureOnlyApplicantAge;
    protected int institutionId;
    private InsureOnlyApplicantPK pk;
    //public final String CLASS_NAME = "com.basis100.deal.entity.InsureOnlyApplicant";

    // TODO: getEntityContext() needed?
    /**
     * constructor
     *
     * @param srk SessionResourceKit
     * @param dcm CalcMonitor
     * @throws RemoteException
     * @throws FinderException
     */
    public InsureOnlyApplicant(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException
    {
        super(srk, dcm);
    }

    /**
     * constructor
     *
     * @param srk SessionResourceKit
     * @param dcm CalcMonitor
     * @param id int
     * @param copyId int
     * @throws RemoteException
     * @throws FinderException
     */
    public InsureOnlyApplicant(SessionResourceKit srk, CalcMonitor dcm, int id,
            int copyId) throws RemoteException, FinderException
    {
        super(srk, dcm);
        pk = new InsureOnlyApplicantPK(id, copyId);
        findByPrimaryKey(pk);
    }

    /**
     * create an entity with minimum fields.
     *
     * @param dpk DealPK
     * @throws RemoteException
     * @throws CreateException
     * @return InsureOnlyApplicant
     */
    public InsureOnlyApplicant create(DealPK dpk) throws RemoteException,
            CreateException
    {
        pk = createPrimaryKey(dpk.getCopyId());

//temp
/*
logger.debug("IOA@create::Id: " + pk.getId());
logger.debug("IOA@create::CopyId: " + dpk.getCopyId());
logger.debug("IOA@create::DealId: " + dpk.getId());
*/
        int lifeStatusId = 0;
        int disabilityStatusId = 0;
        int insuranceProportionsId = 0;
        int insureOnlyApplicantGenderId = 0;
        int smokeStatusId = 0;

        StringBuffer sqlBuff = new StringBuffer(
                "INSERT INTO INSUREONLYAPPLICANT (");
        sqlBuff.append("INSUREONLYAPPLICANTID, ");
        sqlBuff.append("COPYID, ");
        sqlBuff.append("DEALID, ");
        sqlBuff.append("LIFESTATUSID, ");
        sqlBuff.append("DISABILITYSTATUSID, ");
        sqlBuff.append("INSURANCEPROPORTIONSID, ");
        sqlBuff.append("INSUREONLYAPPLICANTGENDERID, ");
        sqlBuff.append("SMOKESTATUSID, ");
        sqlBuff.append("PRIMARYINSUREONLYAPPLICANTFLAG, ");
        sqlBuff.append("INSTITUTIONPROFILEID");
        sqlBuff.append(") VALUES (");
        sqlBuff.append(pk.getId());
        sqlBuff.append(", ");
        sqlBuff.append(pk.getCopyId());
        sqlBuff.append(", ");
        sqlBuff.append(dpk.getId());
        sqlBuff.append(", ");
        sqlBuff.append(lifeStatusId);
        sqlBuff.append(", ");
        sqlBuff.append(disabilityStatusId);
        sqlBuff.append(", ");
        sqlBuff.append(insuranceProportionsId);
        sqlBuff.append(", ");
        sqlBuff.append(insureOnlyApplicantGenderId);
        sqlBuff.append(", ");
        sqlBuff.append(smokeStatusId);
        sqlBuff.append(", 'N',");
        sqlBuff.append(srk.getExpressState().getDealInstitutionId());
        sqlBuff.append(")");
        String sql = sqlBuff.toString();

        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate(); // Track the create
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "Insure Only Applicant Entity - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        srk.setModified(true);

        if (dcm != null)
            //dcm.inputEntity(this, true );
            dcm.inputCreatedEntity(this);
        return this;
    }

    /**
     * Generates a PK using copyId
     *
     * @param copyId int
     * @throws CreateException
     * @return InsureOnlyApplicantPK
     */
    private InsureOnlyApplicantPK createPrimaryKey(int copyId)
            throws CreateException
    {
        int id = -1;
        String sql = "SELECT INSUREONLYAPPLICANTSEQ.NEXTVAL FROM DUAL";
        try
        {
            int key = jExec.execute(sql);
            if (jExec.next(key))
            {
                id = jExec.getInt(key, 1);
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("Insure Only Applicant Entity create() exception getting InsureOnlyApplicantId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }
        InsureOnlyApplicantPK ioaPk = null;
        if (id == -1)
        {
          CreateException ce = new CreateException("Insure Only Applicant Entity create() exception: Failed to get ID from INSUREONLYAPPLICANTSEQ.");
            throw ce;
        }
        else
        {
            ioaPk = new InsureOnlyApplicantPK(id, copyId);
        }
        return ioaPk;
    }


    /**
     * @param key int
     * @throws Exception
     */

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     this.setInsureOnlyApplicantId(jExec.getInt(key, "INSUREONLYAPPLICANTID")) ;
     copyId = jExec.getInt(key, "COPYID" ) ;
     this.setDealId(jExec.getInt(key, "DEALID"));
     this.setInsureOnlyApplicantFirstName(jExec.getString(key, "INSUREONLYAPPLICANTFIRSTNAME"));
     this.setInsureOnlyApplicantLastName(jExec.getString(key, "INSUREONLYAPPLICANTLASTNAME"));
     this.setInsureOnlyApplicantBirthDate(jExec.getDate(key, "INSUREONLYAPPLICANTBIRTHDATE"));
     this.setPrimaryInsureOnlyApplicantFlag(jExec.getString(key, "PRIMARYINSUREONLYAPPLICANTFLAG").charAt(0));
     this.setInsureOnlyApplicantGenderId(jExec.getInt(key, "INSUREONLYAPPLICANTGENDERID"));
     this.setInsuranceProportionsId(jExec.getInt(key, "INSURANCEPROPORTIONSID"));
     this.setLifePercentCoverageReq(jExec.getDouble(key, "LIFEPERCENTCOVERAGEREQ"));
     this.setSmokeStatusId(jExec.getInt(key, "SMOKESTATUSID"));
     this.setDisabilityStatusId(jExec.getInt(key, "DISABILITYSTATUSID"));
     this.setLifeStatusId(jExec.getInt(key, "LIFESTATUSID"));
     this.setDisPercentCoverageReq(jExec.getDouble(key, "DISPERCENTCOVERAGEREQ"));
     this.setInsureOnlyApplicantAge(jExec.getInt(key, "INSUREONLYAPPLICANTAGE"));
     this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }


    public  boolean equals( Object o )
    {
      if ( o instanceof InsureOnlyApplicant )
        {
          InsureOnlyApplicant ioa  = ( InsureOnlyApplicant )  o;
          if  ( ( this.insureOnlyApplicantId  == ioa.getInsureOnlyApplicantId () )
            && ( this.copyId  ==  ioa.getCopyId ()  )  )
              return true;
        }
      return false;
    }

    /**
     * @param dpk DealPK
     * @throws RemoteException
     * @throws FinderException
     * @return Collection
     */
    public Collection findAllByDeal(DealPK dpk) throws RemoteException,
            FinderException
    {
        StringBuffer sqlBuff = new StringBuffer("SELECT * FROM ");
        sqlBuff.append("INSUREONLYAPPLICANT WHERE DEALID=");
        sqlBuff.append(dpk.getId());
        sqlBuff.append(" AND COPYID=");
        sqlBuff.append(dpk.getCopyId());
        String sql = sqlBuff.toString();
        return doQuery(sql, jExec);
    }

    public InsureOnlyApplicant findByPrimaryKey(InsureOnlyApplicantPK pk) throws RemoteException, FinderException
    {
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from insureOnlyApplicant " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "InsureOnlyApplicant Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("InsureOnlyApplicant Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }


///
    /**
     * @param dpk DealPK
     * @throws RemoteException
     * @throws FinderException
     * @return Collection
     */
    public Collection findByDeal(DealPK dpk) throws RemoteException,
            FinderException
    {
        try
        {
            StringBuffer sqlBuff = new StringBuffer(
                    "SELECT * FROM INSUREONLYAPPLICANT ");
            // D.getWhereClause()
            sqlBuff.append(dpk.getWhereClause());
            sqlBuff.append(" ORDER BY PRIMARYINSUREONLYAPPLICANTFLAG DESC");
            String sql = sqlBuff.toString();
            return doQuery(sql, jExec);
        }
        catch (RemoteException re)
        {
            StringBuffer errMsgBuff = new StringBuffer("@");
            errMsgBuff.append("InsureOnlyApplicant");
            errMsgBuff.append(".findByDeal(), Exception = ");
            errMsgBuff.append(re.getMessage());
            String errMsg = errMsgBuff.toString();
            logger.error(errMsg);
            logger.error(re);
            throw new RemoteException(errMsg);
        }
        catch (FinderException fe)
        {
            StringBuffer errMsgBuff = new StringBuffer("@");
            errMsgBuff.append("InsureOnlyApplicant");
            errMsgBuff.append(".findByDeal(), Exception = ");
            errMsgBuff.append(fe.getMessage());
            String errMsg = errMsgBuff.toString();
            logger.error(errMsg);
            logger.error(fe);
            throw new FinderException(errMsg);
        }
    }

    public InsureOnlyApplicant findByLifeDisPremiumsIOnlyA(LifeDisPremiumsIOnlyA ldpioa) throws RemoteException, FinderException
    {
      InsureOnlyApplicantPK pk = new InsureOnlyApplicantPK(ldpioa.getInsureOnlyApplicantId(), ldpioa.getCopyId());
      return findByPrimaryKey(pk);
    }

    /**
     * @param sql String
     * @param jExec JdbcExecutor
     * @throws RemoteException
     * @throws FinderException
     * @return Collection
     */
    private Collection doQuery(String sql, JdbcExecutor jExec)
            throws RemoteException, FinderException
    {
        Collection col = new Vector();
        int key;
        try
        {
            key = jExec.execute(sql);
            while (jExec.next(key))
            {
                // create a new entity
                InsureOnlyApplicant ioa = new InsureOnlyApplicant(srk, dcm);
                ///ioa.setPropertiesFromQueryResult(jExec, key);
                ioa.setPropertiesFromQueryResult(key);

                ioa.pk = new InsureOnlyApplicantPK(ioa.getInsureOnlyApplicantId(), ioa.getCopyId());
                // add newly created entity to collection
                col.add(ioa);
            }
            jExec.closeData(key);
        }
        catch (RemoteException re)
        {
            StringBuffer errMsgBuff = new StringBuffer("@");
            errMsgBuff.append("InsureOnlyApplicant");
            errMsgBuff.append(".findAllByDeal(), Exception = ");
            errMsgBuff.append(re.getMessage());
            String errMsg = errMsgBuff.toString();
            logger.error(errMsg);
            logger.error(re);
            throw new RemoteException(errMsg);
        }
        catch (FinderException fe)
        {
            StringBuffer errMsgBuff = new StringBuffer("@");
            errMsgBuff.append("InsureOnlyApplicant");
            errMsgBuff.append(".findAllByDeal(), Exception = ");
            errMsgBuff.append(fe.getMessage());
            String errMsg = errMsgBuff.toString();
            logger.error(errMsg);
            logger.error(fe);
            throw new RemoteException(errMsg);
        }
        catch (Exception e)
        {
            FinderException fe = (FinderException) e;
            StringBuffer errMsgBuff = new StringBuffer("@");
            errMsgBuff.append("InsureOnlyApplicant");
            errMsgBuff.append(".findAllByDeal(), Exception = ");
            errMsgBuff.append(fe.getMessage());
            String errMsg = errMsgBuff.toString();
            logger.error(errMsg);
            logger.error(fe);
            throw new RemoteException(errMsg);
        }
        return col;
    }

    /**
     * @return Returns the copyId.
     */
    public int getCopyId()
    {
        return copyId;
    }

    /**
     * @return Returns the insuranceProportionsId.
     */
    public int getInsuranceProportionsId()
    {
        return insuranceProportionsId;
    }

    /**
     * @param dealId
     *            The dealId to set.
     */
    public void setDealId(int dealId)
    {
        this.testChange("dealId", dealId);
        this.dealId = dealId;
    }

    /**
     * @param disabilityStatusId
     *            The disabilityStatusId to set.
     */
    public void setDisabilityStatusId(int disabilityStatusId)
    {
        this.testChange("disabilityStatusId", disabilityStatusId);
        this.disabilityStatusId = disabilityStatusId;
    }

    /**
     * @param disPercentCoverageReq
     *            The disPercentCoverageReq to set.
     */
    public void setDisPercentCoverageReq(double disPercentCoverageReq)
    {
        this.testChange("disPercentCoverageReq", disPercentCoverageReq);
        this.disPercentCoverageReq = disPercentCoverageReq;
    }

    /**
     * @param insuranceProportionsId
     *            The insuranceProportionsId to set.
     */
    public void setInsuranceProportionsId(int insuranceProportionsId)
    {
        this.testChange("insuranceProportionsId", insuranceProportionsId);
        this.insuranceProportionsId = insuranceProportionsId;
    }

    /**
     * @param insureOnlyApplicantBirthDate Date
     *            The insureOnlyApplicantBirthdate to set.
     */
    public void setInsureOnlyApplicantBirthDate(
            Date insureOnlyApplicantBirthDate)
    {
        this.testChange("insureOnlyApplicantBirthDate", insureOnlyApplicantBirthDate);
        this.insureOnlyApplicantBirthDate = insureOnlyApplicantBirthDate;
    }

    /**
     * @param insureOnlyApplicantFirstName
     *            The insureOnlyApplicantFirstName to set.
     */
    public void setInsureOnlyApplicantFirstName(
            String insureOnlyApplicantFirstName)
    {
      this.testChange("insureOnlyApplicantFirstName", insureOnlyApplicantFirstName);
      this.insureOnlyApplicantFirstName = insureOnlyApplicantFirstName;
    }

    /**
     * @param insureOnlyApplicantGenderId
     *            The insureOnlyApplicantGenderId to set.
     */
    public void setInsureOnlyApplicantGenderId(int insureOnlyApplicantGenderId)
    {
        this.testChange("insureOnlyApplicantGenderId", insureOnlyApplicantGenderId);
        this.insureOnlyApplicantGenderId = insureOnlyApplicantGenderId;
    }

    /**
     * @param insureOnlyApplicantAge
     *            The insureOnlyApplicantAge to set.
     */
    public void setInsureOnlyApplicantAge(int insureOnlyApplicantAge)
    {
        this.testChange("insureOnlyApplicantAge", insureOnlyApplicantAge);
        this.insureOnlyApplicantAge = insureOnlyApplicantAge;
    }

    /**
     * @param insureOnlyApplicantId
     *            The insureOnlyApplicantId to set.
     */
    public void setInsureOnlyApplicantId(int insureOnlyApplicantId)
    {
        this.testChange("insureOnlyApplicantId", insureOnlyApplicantId);
        this.insureOnlyApplicantId = insureOnlyApplicantId;
    }

    /**
     * @param insureOnlyApplicantLastName
     *            The insureOnlyApplicantLastName to set.
     */
    public void setInsureOnlyApplicantLastName(
            String insureOnlyApplicantLastName)
    {
        this.testChange("insureOnlyApplicantLastName", insureOnlyApplicantLastName);
        this.insureOnlyApplicantLastName = insureOnlyApplicantLastName;
    }

    /**
     * @param lifePercentCoverageReq
     *            The lifePercentCoverageReq to set.
     */
    public void setLifePercentCoverageReq(double lifePercentCoverageReq)
    {
        this.testChange("lifePercentCoverageReq", lifePercentCoverageReq);
        this.lifePercentCoverageReq = lifePercentCoverageReq;
    }

    /**
     * @param lifeStatusId
     *            The lifeStatusId to set.
     */
    public void setLifeStatusId(int lifeStatusId)
    {
        this.testChange("lifeStatusId", lifeStatusId);
        this.lifeStatusId = lifeStatusId;
    }

    /**
     * @param primaryInsureOnlyApplicantFlag
     *            The primaryInsureOnlyApplicantFlag to set.
     */
    public void setPrimaryInsureOnlyApplicantFlag(
            char primaryInsureOnlyApplicantFlag)
    {
        this.testChange("primaryInsureOnlyApplicantFlag",
                primaryInsureOnlyApplicantFlag);
        this.primaryInsureOnlyApplicantFlag = primaryInsureOnlyApplicantFlag;
    }

    /**
     * @param smokeStatusId
     *            The smokeStatusId to set.
     */
    public void setSmokeStatusId(int smokeStatusId)
    {
        this.testChange("smokeStatusId", smokeStatusId);
        this.smokeStatusId = smokeStatusId;
    }

    /**
     * @return Returns the dealId.
     */
    public int getDealId()
    {
        return dealId;
    }

    /**
     * @return Returns the disabilityStatusId.
     */
    public int getDisabilityStatusId()
    {
        return disabilityStatusId;
    }

    /**
     * @return Returns the disPercentCoverageReq.
     */
    public double getDisPercentCoverageReq()
    {
        return disPercentCoverageReq;
    }

    /**
     * @return Returns the insureOnlyApplicantBirthdate.
     */
    public Date getInsureOnlyApplicantBirthDate()
    {
        return insureOnlyApplicantBirthDate;
    }

    /**
     * @return Returns the insureOnlyApplicantFirstName.
     */
    public String getInsureOnlyApplicantFirstName()
    {
        return insureOnlyApplicantFirstName;
    }

    /**
     * @return Returns the insureOnlyApplicantGenderId.
     */
    public int getInsureOnlyApplicantGenderId()
    {
        return insureOnlyApplicantGenderId;
    }

    /**
     * @return Returns the insureOnlyApplicantAge.
     */
    public int getInsureOnlyApplicantAge()
    {
        return insureOnlyApplicantAge;
    }

    /**
     * @return Returns the insureOnlyApplicantId.
     */
    public int getInsureOnlyApplicantId()
    {
        return insureOnlyApplicantId;
    }

    /**
     * @return Returns the insureOnlyApplicantLastName.
     */
    public String getInsureOnlyApplicantLastName()
    {
        return insureOnlyApplicantLastName;
    }

    /**
     * @return Returns the lifePercentCoverageReq.
     */
    public double getLifePercentCoverageReq()
    {
        return lifePercentCoverageReq;
    }

    /**
     * @return Returns the lifeStatusId.
     */
    public int getLifeStatusId()
    {
        return lifeStatusId;
    }

    /**
     * @throws Exception
     * @return Returns the Collection of LifeDisPremiums.
     */
    public Collection getLifeDisPremiumsIOnlyA()throws Exception
    {
      return new LifeDisPremiumsIOnlyA(srk,dcm).findByInsureOnlyApplicant(this.pk);
    }

    /**
     * @return Returns the pk.
     */
    public IEntityBeanPK getPk()
    {
        return (IEntityBeanPK) pk;
    }

    /**
     * @return Returns the primaryInsureOnlyApplicantFlag.
     */
    public char getPrimaryInsureOnlyApplicantFlag()
    {
        return primaryInsureOnlyApplicantFlag;
    }

    /**
     * @return Returns the smokeStatusId.
     */
    public int getSmokeStatusId()
    {
        return smokeStatusId;
    }

    /**
     * @return Returns ClassId.INSUREONLYAPPLICANT
     */
    public int getClassId()
    {
        return ClassId.INSUREONLYAPPLICANT;
    }

    /**
     * @return String
     */
    public String getEntityTableName()
    {
      return "InsureOnlyApplicant";
    }

    /**
     * @return Vector
     * @throws RemoteException
     * @throws FinderException
     */
    public Vector findByMyCopies() throws RemoteException, FinderException
    {
        try
        {
            StringBuffer sqlBuff = new StringBuffer(
                    "SELECT * FROM INSUREONLYAPPLICANT WHERE");
            sqlBuff.append(" INSUREONLYAPPLICANTID = ");
            sqlBuff.append(insureOnlyApplicantId);
            sqlBuff.append(" AND COPYID <> ");
            sqlBuff.append(copyId);
            String sql = sqlBuff.toString();
            return (Vector) doQuery(sql, jExec);
        }
        catch (RemoteException re)
        {
            StringBuffer errMsgBuff = new StringBuffer("@");
            errMsgBuff.append("InsureOnlyApplicant");
            errMsgBuff.append(".findByMyCopies(), Exception = ");
            errMsgBuff.append(re.getMessage());
            String errMsg = errMsgBuff.toString();
            logger.error(errMsg);
            logger.error(re);
            throw new RemoteException(errMsg);
        }
        catch (FinderException fe)
        {
            StringBuffer errMsgBuff = new StringBuffer("@");
            errMsgBuff.append("InsureOnlyApplicant");
            errMsgBuff.append(".findByMyCopies(), Exception = ");
            errMsgBuff.append(fe.getMessage());
            String errMsg = errMsgBuff.toString();
            logger.error(errMsg);
            logger.error(fe);
            throw new FinderException(errMsg);
        }
    }

    public List getChildren() throws Exception
    {
      List children = new ArrayList();
      //do not alter the order of the calls this method:
      //Mercator relies on the order that results!!!!
      //any new calls to addAll should go after the existing calls.

      // Add LifeDisabilityPremiums to the Children list if any
      children.addAll(this.getLifeDisPremiumsIOnlyA());
      return children;
    }

    protected void removeSons() throws Exception
    { // Remove all the sons , Call by ejbRemove
      // LifeDisPremiumsIOnlyA
      try
      {
        // Remove the LifeDisPremiumsIOnlyA Record if any
        Collection premiums = this.getLifeDisPremiumsIOnlyA () ;
        Iterator  itPr = premiums.iterator () ;

        while ( itPr.hasNext () )
        {
          LifeDisPremiumsIOnlyA premium = ( LifeDisPremiumsIOnlyA ) itPr.next () ;
          if ( itPr.hasNext () )
            premium.ejbRemove ( false);
          else
            premium.ejbRemove ( true );
        }//  ---------  end of all Life Disability Premiums ---------------
      }
      catch ( Exception e )
      {
        throw e;
      }
    }

    /**
     * @param fieldName String
     * @return Object
     */
    public Object getFieldValue(String fieldName)
    {
        return doGetFieldValue(this, this.getClass(), fieldName);
    }

    /**
     * Gets the value of instance fields as String. If a field does not exist
     * (or the type is not serviced), null is returned. If the field exists (and
     * the type is serviced) but the field is null, an empty String is returned.
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     *
     * @param fieldName String
     * @return String value of bound field as a String;
     */
    public String getStringValue(String fieldName)
    {
        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * @return TRUE
     */
    public boolean isAuditable()
    {
        return true;
    }

    /**
     * @throws Exception
     * @return int
     */
     protected int performUpdate()  throws Exception
     {
       Class clazz = this.getClass();

       int ret = doPerformUpdate(this, clazz);

       // Optimization:
       // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
       if(dcm != null && ret >0 ) dcm.inputEntity(this);

       return ret;
     }


    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned. Other
     * entity types are not yet serviced ie. You can't set the Province object
     * in Addr.
     *
     * @param fieldName String
     * @param value String
     * @throws Exception
     */
    public void setField(String fieldName, String value) throws Exception
    {
        doSetField(this, this.getClass(), fieldName, value);
    }

    public int getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(int institutionId) {
        testChange("INSTITUTIONPROFILEID", institutionId);
        this.institutionId = institutionId;
    }
}
