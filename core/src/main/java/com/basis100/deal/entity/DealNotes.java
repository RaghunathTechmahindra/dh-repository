package com.basis100.deal.entity;

/**
 * 18.Nov.2008 DVG #DG776 FXP23487: vMCM_express_Deal Summary Lite: Broker notes show up in French, but the applicant preferred language is english     
 * 29/Feb/2008 DVG #DG702 MLM x BDN merge adjustments
 * 23/Oct/2007 DVG #DG646 LEN192740 express problem detected by development
 * 10/Oct/2007 DVG #DG636 FXP18688: DupeCheck - BDN Express - The deal Note in Deal Y is not correct when the active deal (deal X) has preApproval offered status, and the incoming deal Y is the firm up deal 
 * 26/Sep/2007 DVG #DG630 Mos2commit files failing on QA1 for MLM
    - hide constant fields as a local interface
 * 28/Jun/2007 DVG #DG608 LEN192740  express problem detected by development 
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 * 18/Jul/2006 DVG #DG464 #3963  Accented letters and punctuation symbols are not displayed correctly in Deal Notes
 * 23/May/2006 DVG #DG426 #3242  apostrophe quadruples when used in notes
 *    - took the chance to remove deprecated code
 */

import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Formattable;
import java.util.Locale;
import java.util.Map.Entry;

import MosSystem.Mc;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealNotesPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;


public class DealNotes extends DealEntity implements Xc {
    protected int     dealId;
    protected int     dealNotesId;
    protected int     dealNotesCategoryId;
    protected Date    dealNotesDate;
    protected int     applicationId;
    protected int     dealNotesUserId;
    protected int     instProfileId;
    protected String  dealNotesText;
    protected int  languagePreferenceId;    //#DG600

    private DealNotesPK pk;

    //#DG600 #DG630
    public static interface Ec { // those are just local entity constants 
        public static final String DEAL_NOTES = "DealNotes"; // getClass.getSimpleName();
        public static interface CL { // columns constants  #DG646 
            public static final String APPLICATION_ID = "applicationId";
            public static final String LANGUAGE_PREFERENCE_ID = "languagePreferenceId";
            public static final String DEAL_ID = "dealId";
            public static final String DEAL_NOTES_ID = "dealNotesId";
            public static final String DEAL_NOTES_CATEGORY_ID = "dealNotesCategoryId";
            public static final String DEAL_NOTES_USER_ID = "dealNotesUserId";
            public static final String DEAL_NOTES_TEXT = "dealNotesText";
            public static final String DEAL_NOTES_DATE = "dealNotesDate";
        }
    }

    public DealNotes(SessionResourceKit srk) throws RemoteException,
        FinderException {
    super(srk);
    }

    public DealNotes(SessionResourceKit srk, int id) throws RemoteException,
        FinderException {
    super(srk);

    pk = new DealNotesPK(id);

    findByPrimaryKey(pk);
    }

    /**
         * <pre>
         *    create a DealNotes record given a DealNotesPK primary key.
         *     these create methods assume a standard list of records and are therefore
         *     a utility for record entry.
         *    @param pk - the primary key for this record
         *    @return this instance
         * </pre>
         */
    public DealNotes create(DealPK dpk) throws RemoteException, CreateException {
    pk = createPrimaryKey();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date now = new Date();
        String sysdate = formatter.format(now);
        
    //long sysdate = now.getTime();

    String sql = "Insert into DealNotes( DealNotesId, DealId, dealNotesDate, dealNotesText, INSTITUTIONPROFILEID) Values ( "
        + pk.getId()
        + ","
        + dpk.getId()
        + ", '"
        + sysdate
        + "' ,"
        + "'temporary dealNotestext'"
        + ","
        + srk.getExpressState().getDealInstitutionId() + ")";

    try {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        setDealNotesText(null); //#DG608 force change        
        trackEntityCreate();
    } catch (Exception e) {
        CreateException ce = new CreateException(
            "DealNotes Entity - DealNotesType - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
    }
    srk.setModified(true);
    return this;
    }

   //#DG600 added languageid column
    /**
    * <pre>
    *    create a DealNotes record given an id (primary key) and a description
    *    @param id the primary key for this record
    *    @param desc the description of this DealNotes object
    * </pre>
    * @param dpk the primary key for this record
    * @param applicationId application id
    * @param category category id
    * @param noteText the note text
    * @param locala the locale used to get the language id
    * @return this instance
    * @throws RemoteException
    * @throws CreateException
         */
    public DealNotes create(DealPK dpk, int applicationId, int category, int userId, 
            String noteText, int langId) throws RemoteException, CreateException {
        pk = createPrimaryKey();


        StringBuffer sql = new StringBuffer();
        sql.append("INSERT INTO DEALNOTES ( ");
        sql.append(" DEALNOTESID,");
        sql.append(" DEALID,");
        sql.append(" APPLICATIONID,");
        sql.append(" DEALNOTESCATEGORYID,");
        sql.append(" DEALNOTESUSERID,");
        sql.append(" DEALNOTESTEXT,");
        sql.append(" LANGUAGEPREFERENCEID,");
        sql.append(" DEALNOTESDATE,");
        sql.append(" INSTITUTIONPROFILEID ");
        sql.append(" ) VALUES (");
        sql.append(" ?, ?, ?, ?, ?, ?, ?, SYSDATE, ? ");
        sql.append(" )");

        Clob clob = null;
        try {

            PreparedStatement ps = jExec.getCon().prepareStatement(sql.toString());

            ps.clearParameters();
            ps.setInt(1, pk.getId());
            ps.setInt(2, dpk.getId());
            ps.setInt(3, applicationId);
            ps.setInt(4, category);
            ps.setInt(5, userId);
            //#DG608
            // FXP25883 
            noteText = StringUtil.makeBackSlashSafe(noteText);
			clob = createClob(noteText);
            ps.setClob(6, clob); 
            
            ps.setInt(7, langId);
            ps.setInt(8, srk.getExpressState().getDealInstitutionId());

            int result = ps.executeUpdate();
            ps.close();

            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the creation

        } catch (Exception e) {
            StringBuffer err = new StringBuffer(
            "DealNotes Entity - DealNotesType - create() exception: ");
            err.append(sql + " ");
            err.append("DEALNOTESID = " + pk.getId());
            err.append("; DEALID = " + dpk.getId());
            err.append("; APPLICATIONID = " + applicationId);
            err.append("; DEALNOTESCATEGORYID = " + category);
            err.append("; DEALNOTESUSERID = " + userId);
            err.append("; DEALNOTESTEXT = " + noteText);
            err.append("; LANGUAGEPREFERENCEID = " + langId);
            err.append("; INSTITUTIONPROFILEID = " + srk.getExpressState().getDealInstitutionId());

            logger.error(e);
            logger.error(StringUtil.stack2string(e));
            CreateException ce = new CreateException(err.toString());
            logger.error(ce);

            throw ce;
        } finally {// FXP25883 
            try {
                if (clob != null) 
                    oracle.sql.CLOB.freeTemporary((oracle.sql.CLOB)clob);
            } catch (SQLException sqle) {
                logger.debug("could not release CLOB temp");
                logger.error(sqle);
            }
        }

        srk.setModified(true);
        return this;
    }
    
    /**
         * <pre>
         *    create a DealNotes record given an id (primary key) and a description
         *    @param id the primary key for this record
         *    @param desc the description of this DealNotes object
         *    @return this instance
         * 
         *    Warning - the dateString parameter is ignored - current date/time used instead (Oracle syntax)
         * 
         * </pre>
         */
   public DealNotes create(DealPK dpk, int aplId,  int categId, String noteText,
       String dateString, int dealNotesUserId )throws RemoteException, CreateException {
     return create(dpk, aplId, categId, dealNotesUserId, noteText, Mc.LANGUAGE_UNKNOW);
   }

   // #DG600 default language
   public DealNotes create(DealPK dpk, int aplId, int categId, String noteText) 
   throws RemoteException, CreateException {
     return create(dpk, aplId, categId, 0, noteText, Mc.LANGUAGE_UNKNOW);
   }
   
   //#DG636 convenience method
   public void create(Deal deal, int categId, int userId, Formattable ... noteFmtb) throws RemoteException, CreateException {
         int aplId = TypeConverter.intTypeFrom(deal.getApplicationId());
       create((DealPK) deal.getPk(), aplId, categId, userId, noteFmtb);     
     }

   /** #DG636 this method is only used locally... keep it for now ... refactor(delete) later
    * 
    *   <pre>
    *   Iterate throught all the available languages localizing and call create 
    *   for each one.
    *    @param deal for which to create the notes
    *    @param categId notes category Id
    *    @param noteFmtb the list of non localized items 
    *   </pre>
    */
   public void create(DealPK dpk, int aplId, int categId, int userId, Formattable ... noteFmtb) 
    throws RemoteException, CreateException 
    {
       
         Entry<Integer, Locale>[] locales = BXResources.getLocales();
         for (Entry<Integer, Locale> intLoc: locales) {
             Locale locala = intLoc.getValue();
             /***** FXP24154 Ingestion_Deal Notes text is null starts *****/
             if ( noteFmtb != null ) {
             /***** FXP24154 Ingestion_Deal Notes text is null ends *****/
                 String nota = String.format(locala, "%s", (Object [])noteFmtb);
                 nota = StringUtil.makeBackSlashSafe(nota);
                 create(dpk,aplId, categId, userId, nota, intLoc.getKey());
             }
         }
    }

   /**
    * copy deal notes from original deal to the new deal
    * @param fromDeal source deal
    * @param toDeal destination deal
    * @param categoryLenderID category lender id, if valid copy only the selected notes, if -1 copies all
    * @throws Exception insert exception
    */
   public void copyDealNotes(Deal fromDeal, Deal toDeal, int categoryLenderID) throws Exception {

     logger.debug("Method copyDealNotes entered.");
     
     StringBuffer sb = new StringBuffer("Insert into DealNotes " +
     "select  ?, DealNotesseq.nextval, dealnotesCategoryId, dealNotesDate, "
     +" applicationId, dealNotesUserId, dealNotesText, institutionProfileId, languagePreferenceId" 
     +" from DealNotes where dealId = ? ");
     if (categoryLenderID >= Mc.DEAL_NOTES_CATEGORY_GENERAL) 
         sb.append(" and dealnotesCategoryId = ").append(categoryLenderID);
     
     final String sql = sb.toString();
     try
     {
         PreparedStatement ps = jExec.getCon().prepareStatement(sql);
         ps.setInt(1, toDeal.getDealId());
         ps.setInt(2, fromDeal.getDealId());
         ps.executeUpdate();
         ps.close();

         this.trackEntityCreate (); // track the creation
     }
     catch (Exception e)
     {
         CreateException ce = new CreateException("DealNotes Entity - DealNotesType - create() exception");
         logger.error(ce);
         logger.error(e);
         throw ce;
     }
     srk.setModified(true);
     logger.debug("Method copyDealNotes finished.");
   }   
   //#DG600 end 

   //#DG776 
   public Collection findByDeal(DealPK pk) throws FinderException
   {
        String sql = "Select * from DealNotes where dealId = "+  pk.getId();
        return findBySqlColl(sql);
   }

   //#DG776 find by deal and lang. id  
  public Collection findByDealLang(DealPK pk, int... langIds) throws FinderException
  {
    StringBuffer sb = new StringBuffer(88);
    sb.append("Select * from DealNotes where dealId = ").append(pk.getId())
        .append(" and ").append(Ec.CL.LANGUAGE_PREFERENCE_ID).append(" in (");
    for (int i : langIds) {
        sb.append(i).append(',');
    }
    sb.setLength(sb.length()-1);
    sb.append(')');
    return findBySqlColl(sb.toString());
  }
   
    public Collection findBySqlColl(String sql) throws FinderException {

        Collection dnList = new ArrayList();
        try {
            int key = jExec.execute(sql);
            while (jExec.next(key)) {
                DealNotes dn = new DealNotes(srk);
                dn.setPropertiesFromQueryResult(key);
                dn.pk = new DealNotesPK(dn.getDealNotesId());
                dnList.add(dn);
            }
            jExec.closeData(key);

        }
        catch (Exception e) {
            FinderException fe = new FinderException("Entity - findBySqlColl() exception:" + sql, e);
            if (!getSilentMode())
                logger.error(fe);
            throw fe;
        }
        return dnList;
    }


    public DealNotes findByPrimaryKey(DealNotesPK pk) throws RemoteException,
        FinderException {
    	
    if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from DealNotes " + pk.getWhereClause();

    boolean gotRecord = false;

    try {
        int key = jExec.execute(sql);

        for (; jExec.next(key);) {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
        }

        jExec.closeData(key);

        //#DG702 rewritten to allow logging supression
        if (!gotRecord) {
        throw new FinderException();
        }
    } catch (Exception e) {
        //#DG702 rewritten to allow logging supression
        FinderException fe = new FinderException("DealNotes Entity - findByPrimaryKey() exception:"+sql, e);
        if (gotRecord || !getSilentMode())
            logger.error(fe);

        throw fe;
    }
    this.pk = pk;
	ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
    }

    public void setDealNotesId(int id) {
    this.testChange("dealNotesId", id);
    this.dealNotesId = id;
    }

    public void setDealId(int id) {
    this.testChange("dealId", id);
    this.dealId = id;
    }

    public void setDealNotesCategoryId(int id) {
    this.testChange ("dealNotesCategoryId", id );
    this.dealNotesCategoryId = id;
    }

    public void setDealNotesDate(Date date) {
    this.testChange("dealNotesDate", date);
    this.dealNotesDate = date;
    }

   public void setDealNotesText(String noteText) throws Exception
   {
     this.testChange ("dealNotesText", noteText);
     this.dealNotesText = StringUtil.makeBackSlashSafe(noteText);
    }

    public void setDealNotesUserId(int id) {
    this.testChange("dealNotesUserId", id);
    this.dealNotesUserId = id;
    }

    public void setApplicationId(int id) {
    this.testChange("applicationId", id);
    this.applicationId = id;
    }

   /** #DG600
   * 
   * @param languagePreferenceId
   *          the languagePreferenceId to set
   */
    public void setLanguagePreferenceId(int id) {
      this.testChange ("languagePreferenceId", id);
      this.languagePreferenceId = id;
    }

    public int getDealId() {
    return this.dealId;
    }

    public int getDealNotesId() {
    return this.dealNotesId;
    }

    public int getDealNotesCategoryId() {
    return this.dealNotesCategoryId;
    }

    public Date getDealNotesDate() {
    return this.dealNotesDate;
    };

   public String getDealNotesText() throws Exception {
    return StringUtil.makeBackSlashSafe(this.dealNotesText);
    }

    public int getDealNotesUserId() {
    return this.dealNotesUserId;
    }

    public int getApplicationId() {
    return this.applicationId;
    }

   /** #DG600
   * 
   * @return the languagePreferenceId
   */
   public int getLanguagePreferenceId() {
    return languagePreferenceId;
   }

    /**
         * gets the value of instance fields as String. if a field does not
         * exist (or the type is not serviced)** null is returned. if the field
         * exists (and the type is serviced) but the field is null an empty
         * String is returned.
         * 
         * @returns value of bound field as a String;
         * @param fieldName
         *                as String
         * 
         * Other entity types are not yet serviced ie. You can't get the
         * Province object for province.
         */
    public String getStringValue(String fieldName) {
    return doGetStringValue(this, this.getClass(), fieldName);
    }

  /*#DG608  
    protected void updateDealNotesText() {
    try {
        String state = "update dealnotes set dealnotestext = ? where dealnotesid = ?";

        String note = (dealNotesText == null) ? "" : StringUtil.makeBackSlashSafe(dealNotesText);

        logger.debug("DealNotes.updateDealNotesText : SQL statement : "
            + state);
        logger.debug("DealNotes.updateDealNotesText : DataLength : "
            + note.length());

        PreparedStatement ps = this.jExec.getCon().prepareStatement(state);

        StringReader strStream = new StringReader(note);
        ps.setCharacterStream(1, strStream, note.length());

        ps.setInt(2, dealNotesId);
        logger.debug("DealNotes.updateDealNotesText: Dealid = " + dealId);
        ps.executeUpdate();
        ps.close();
    } catch (Exception e) {
        logger.error("Exception in DealNotes.updateDealNotesText : "
            + e.getMessage());
    }
    }
  } */
    /*
     * added try-catch for 4.2GR CM/DSU
     */
    protected void updateDealNotesText(String noteText) throws Exception
    {   
      String sql = "update DealNotes set dealNotesText = ? " +
        pk.getWhereClause();

      // FXP25883
      Clob clob = null;
      try {
          PreparedStatement ps = jExec.getCon().prepareStatement(sql);
          
          // FXP25883 -- test change
          noteText = StringUtil.makeBackSlashSafe(noteText);
          clob = createClob(noteText);
          ps.setClob(1, clob);         
    
          int result = ps.executeUpdate();
          ps.close();
      } catch (SQLException sqle){
          throw new Exception(
                  "DealNotes - could not update record : SQL = "
                      + sql + " :: ");
      } finally {// FXP25883 
          try {
              if (clob != null) 
                  oracle.sql.CLOB.freeTemporary((oracle.sql.CLOB)clob);
          } catch (SQLException sqle) {
              logger.debug("could not release CLOB temp");
              logger.error(sqle);
          }
      }
    }
    /**
         * Updates any Database field that has changed since the last
         * synchronization ie. the last findBy... call
         * 
         * @return the number of updates performed
         */
    protected int performUpdate()  throws Exception
    {
        Class clazz = this.getClass();

        //#DG608 if set to empty delete it instead
        final String noteText = getDealNotesText();
        if (noteText == null) {
            ejbRemove();
            return 0;
        }
        // modified by JOHN to add a special handler for the dealNotesText field
        // Check if dealNotesText changed
        if(this.isValueChange("dealNotesText"))
        {
                // Perform the change via PrepareStatement (using special update method)
          updateDealNotesText(noteText);

          // Reset the value changed flag to avoid update again
          resetChange("dealNotesText");
        } 

        return (doPerformUpdate(this, clazz));
    }

    public String getEntityTableName() {
      return "DealNotes";
    }

    /**
         * gets the pk associated with this entity
         * 
         * @return a DealNotesPK
         */
    public IEntityBeanPK getPk() {
    return (IEntityBeanPK) this.pk;
    }

    /**
         * sets the value of instance fields as String. if a field does not
         * exist or is not serviced an Exception is thrown if the field exists
         * (and the type is serviced) but the field is null an empty String is
         * returned.
         * 
         * @param fieldName
         *                as String
         * @param the
         *                value as a String
         * 
         * Other entity types are not yet serviced ie. You can't set the
         * Province object in Addr.
         */
    public void setField(String fieldName, String value) throws Exception {
    doSetField(this, this.getClass(), fieldName, value);
    }

    //
    // Implementation
    //

    private void setPropertiesFromQueryResult(int key) throws Exception {
	    this.setDealId(jExec.getInt(key, "DEALID"));
	    this.setDealNotesId(jExec.getInt(key, "DEALNOTESID"));
	    this.setDealNotesCategoryId(jExec.getInt(key, "DEALNOTESCATEGORYID"));
	    this.setDealNotesDate(jExec.getDate(key, "DEALNOTESDATE"));
	    this.setApplicationId(jExec.getInt(key, "APPLICATIONID"));
	    this.setDealNotesUserId(jExec.getInt(key, "DEALNOTESUSERID"));
	    //#DG608 this.setDealNotesText(jExec.getString(key, "DEALNOTESTEXT"));
	    this.setDealNotesText(readClob(jExec.getClob(key,"DEALNOTESTEXT")));
	    this.setLanguagePreferenceId(jExec.getInt(key,"LanguagePreferenceId")); //#DG600
	    this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));

    }

    private DealNotesPK createPrimaryKey() throws CreateException {
    String sql = "Select DealNotesseq.nextval from dual";
    int id = -1;

    try {
        int key = jExec.execute(sql);

        for (; jExec.next(key);) {
        id = jExec.getInt(key, 1); // can only be one record
        }

        jExec.closeData(key);

        if (id == -1)
        throw new Exception();
    } catch (Exception e) {
        CreateException ce = new CreateException(
            "DealNotes Entity create() exception getting DealNotesId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;

    }

    return new DealNotesPK(id);
    }

   // bilingual deal notes
   //#DG702 refactored to reuse 'findLastNoteTextByDealCategory' 
   public String findLastConditionNoteText(int dealId, int languageId) throws FinderException
   {
     return findLastNoteTextByDealCategory(dealId, languageId, Mc.DEAL_NOTES_CATEGORY_CONDITION); 
   }
   
   public String findLastNoteTextByDealCategory(int dealId, int languageId, int categId) throws FinderException
   {
    String lResult = " ";
    jExec = getSessionResourceKit().getJdbcExecutor();

    // Serghei Feb.03,2009
    // PARADIGM ML FXP24081 "Requested Product" and "Rate and Payment" missing in warranty.
    String sql = "SELECT * FROM (" + 
                        "select dealnotestext from dealnotes where dealid = " + dealId +
                        " and dealnotesdate = (select max(dealnotesdate) from dealnotes where dealid = " + dealId +
                                               " and dealnotescategoryid = " + categId +" ) " +
                        " and ( languagePreferenceId = -1 or languagePreferenceId = " + languageId + ")" + 
                        " and dealnotescategoryid = " + categId + " order by dealnotesdate desc " + 
                 ") WHERE ROWNUM <= 1";
     
      try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key);) {
        lResult = TypeConverter.stringFromClob(jExec.getClob(key, 1));
        break;
        }

        jExec.closeData(key);
    } catch (Exception e) {
        //#DG702 rewritten to allow logging supression
        FinderException fe = new FinderException("deal notes Entity - findLastNoteTextByDealCategory() exception:"+sql, e);
        if (!getSilentMode()) 
            logger.error(fe);
        throw fe;
    }

    if (lResult == null) {
        lResult = " ";
    }
    
    lResult = StringUtil.makeBackSlashSafe(lResult);
    
    logger.debug("DealNotes@findLastNoteTextByDealCategory: lResult = <"
        + lResult + ">");
    return lResult;
    }
   
   public DealNotes findLastDealNotesByDealCategory(int dealId, int languageId, int categId) throws FinderException
   {

    jExec = getSessionResourceKit().getJdbcExecutor();
    
    String sql = "SELECT * FROM (" + 
                        "select * from dealnotes where dealid = " + dealId +
                        " and ( languagePreferenceId = -1 or languagePreferenceId = " + languageId + ")" + 
                        " and dealnotescategoryid = " + categId + " order by dealnotesid desc " + 
                 ") WHERE ROWNUM <= 1";
     
      try
      {
        int key = jExec.execute(sql);

        if(jExec.next(key)) //Only 1 result possible.
            setPropertiesFromQueryResult(key);
        else return null;

        jExec.closeData(key);
    } catch (Exception e) {
        FinderException fe = new FinderException("deal notes Entity - findLastDealNotesByDealCategory() exception:"+sql, e);
        if (!getSilentMode()) 
            logger.error(fe);
        throw fe;
    }

    return this;
    }
   
    // <!-- Catherine, 7-Apr-05, pvcs#817 end -->

   //#DG600
   public String[] findRateNotes(Deal deal1, Deal deal2, String prefix) throws Exception {
     
     logger.debug("Method findRateNotes entered.");
     
     /* this is gonna look something like this
       select DEALNOTESTEXT  from dealnotes 
       where dealid = 1079 and DealNotesUserId = 0 
            and rownum <= 1 and dealnotestext like 'Net rate from ingestion%'
         order by DealNotesdate */
     
     prefix = prefix.replaceAll("\'", "\'\'");
     String[] resulta = new String[2];
     String sql;
     int resKey;
     
     sql = "select DEALNOTESTEXT from \n" 
         + "   dealnotes where dealid = "+deal1.getDealId()+" and DealNotesUserId = 0" 
         + "   and rownum <= 1 and dealnotestext like '"+prefix+"%' \n" 
         + "   order by DealNotesdate \n";       
     resKey = jExec.execute(sql);
     if (jExec.next(resKey))
         resulta[0] = TypeConverter.stringFromClob(jExec.getClob(resKey, 1));       //#DG702
     jExec.closeData(resKey);

     sql = "select DEALNOTESTEXT from \n" 
         + "   dealnotes where dealid = "+deal2.getDealId()+" and DealNotesUserId = 0" 
         + "   and rownum <= 1 and dealnotestext like '"+prefix+"%' \n" 
         + "   order by DealNotesdate \n";       
     resKey = jExec.execute(sql);
     if (jExec.next(resKey))
         resulta[1] = TypeConverter.stringFromClob(jExec.getClob(resKey, 1));       //#DG702
     jExec.closeData(resKey);

     logger.debug("DealNotes@findRateNotes finished");
     return resulta;
   }
   //#DG600 end 

    public int getInstProfileId() {
    return instProfileId;
    }

    public void setInstProfileId(int institutionProfileId) {
    this.testChange("institutionProfileId", institutionProfileId);
    this.instProfileId = institutionProfileId;
    }

}
