package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ChargeTermPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class ChargeTerm extends DealEntity
{
   protected int      chargeTermId;
   protected int      provinceId;
   protected String   chargeTermDescription;
   protected int      lenderProfileId;
   protected String   chargeTermFillingNumber;
   protected String   chargeTermFormNumber;
   protected String   chargeTermQualifier;
   protected boolean  secondaryFlag;
   protected int      conditionLanguagePreferenceId;
   protected int      institutionId;

   private ChargeTermPK pk;

   public ChargeTerm(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

   public ChargeTerm(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new ChargeTermPK(id);

      findByPrimaryKey(pk);
   }

   public ChargeTerm findByPrimaryKey(ChargeTermPK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from ChargeTerm where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "ChargeTerm: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("ChargeTerm Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     *  returns the first ChargeTerm record meeting the criteria
     */
    public ChargeTerm findFirstByLenderProvinceAndQualifier(int lenderId, int provinceId, String qualifier)
      throws RemoteException, FinderException
   {
      String sql = "Select * from ChargeTerm where lenderProfileId = " + lenderId;
      sql += " and ProvinceId = " + provinceId + " and chargeTermQualifier = '";
      sql += qualifier + "' Order By chargeTermId";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "ChargeTerm: @findFirstByLenderProvinceAndQualifier(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {

          FinderException fe = new FinderException("ChargeTerm Entity - findFirstByLenderProvinceAndQualifier() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        return this;
    }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setChargeTermId(jExec.getInt(key, "CHARGETERMID"));
      this.setProvinceId( jExec.getInt(key, "PROVINCEID"));
      this.setChargeTermDescription(jExec.getString(key, "CHARGETERMDESCRIPTION"));
      this.setLenderProfileId(jExec.getInt(key, "LENDERPROFILEID"));
      this.setChargeTermFillingNumber(jExec.getString(key, "CHARGETERMFILLINGNUMBER"));
      this.setChargeTermFormNumber(jExec.getString(key, "CHARGETERMFORMNUMBER"));
      this.setChargeTermQualifier(jExec.getString(key, "CHARGETERMQUALIFIER"));
      this.setSecondaryFlag(jExec.getString(key,"SECONDARYFLAG"));
      this.setConditionLanguagePreferenceId(jExec.getInt(key, "CONDITIONLANGUAGEPREFERENCEID"));
      this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }

 
  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "ChargeTerm"; 
   }	

   /**
    *   retrieves the Contact parent record associated with this entity
    *
    *   @return a Contact
    */
    public Contact getContact()throws FinderException,RemoteException
    {
      return new Contact(srk,this.getProvinceId(),1);
    }


		/**
     *   gets the chargeTermQualifier associated with this entity
     *
     *   @return a String
     */
    public String getChargeTermQualifier()
   {
      return this.chargeTermQualifier ;
   }


		/**
     *   gets the pk associated with this entity
     *
     *   @return a LenderProfilePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

   	/**
     *   gets the chargeTermId associated with this entity
     *
     *   @return a int
     */
    public int getChargeTermId()
   {
      return this.chargeTermId ;
   }
		/**
     *   gets the chargeTermId associated with this entity
     *
     *   @return a int
     */
    public int getLenderProfileId()
   {
      return this.lenderProfileId ;
   }

		/**
     *   gets the provinceId associated with this entity
     *
     *   @return a int
     */
    public int getProvinceId()
   {
      return this.provinceId ;
   }
   
   public String getChargeTermFormNumber(){return this.chargeTermFormNumber; }
   public String getChargeTermFillingNumber(){return this.chargeTermFillingNumber; }
   public boolean getSecondaryFlag(){return this.secondaryFlag; }
   public String getChargeTermDescription(){return this.chargeTermDescription; }
   public int    getConditionLanguagePreferenceId(){return this.conditionLanguagePreferenceId; }


  private ChargeTermPK createPrimaryKey()throws CreateException
  {
       String sql = "Select ChargeTermseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           
           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("LenderProfile Entity create() exception getting LenderProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new ChargeTermPK(id);
  }



  /**
   * creates a ChargeTerm entity
   *
   */

  public ChargeTerm create(int provinceId, int languagePrefId, int lenderProfileId)throws RemoteException, CreateException
  {
    pk = createPrimaryKey();

    StringBuffer sb = new StringBuffer();
    sb.append( "Insert into ChargeTerm( chargeTermId, provinceId,  " );
    sb.append( "conditionLanguagePreferenceId, lenderProfileId, institutionProfileId) Values(");
    sb.append(pk.getId()).append(", ");
    sb.append(provinceId).append(", ");
    sb.append(languagePrefId).append(", ");
    sb.append(lenderProfileId).append(", ");
    sb.append(srk.getExpressState().getDealInstitutionId()).append(") ");
    try
    {
      jExec.executeUpdate(sb.toString());
      this.findByPrimaryKey(pk);
      this.trackEntityCreate();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("ChargeTerm Entity - ChargeTerm - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);
    return this;
  }

  public void setChargeTermId(int value)
	{
		 this.testChange("chargeTermId", value);
		 this.chargeTermId = value;
  }

  public void setLenderProfileId(int value)
	{
		 this.testChange("lenderProfileId", value);
		 this.lenderProfileId = value;
  }
 
   public void setChargeTermQualifier(String value )
	{
		 this.testChange("chargeTermQualifier", value);
		 this.chargeTermQualifier = value;
  }
   public void setProvinceId(int value )
	{
		 this.testChange("provinceId", value);
		 this.provinceId = value;
  }

  public void setChargeTermFormNumber(String value)
	{
	 this.testChange("chargeTermFormNumber", value);
	 this.chargeTermFormNumber = value;
  }

  public void setChargeTermFillingNumber(String value)
	{
	 this.testChange("chargeTermFillingNumber", value);
	 this.chargeTermFillingNumber = value;
  }

  public void setSecondaryFlag(boolean value)
	{
	 this.testChange("secondaryFlag", value);
	 this.secondaryFlag = value;
  }
  public void setSecondaryFlag(String value)
  {
    this.testChange("secondaryFlag", value);
    this.secondaryFlag = TypeConverter.booleanFrom(value);
  }
  
  public void setChargeTermDescription(String value)
	{
	 this.testChange("chargeTermDescription", value);
	 this.chargeTermDescription = value;
  }

  public void setConditionLanguagePreferenceId(int value )
	{
		 this.testChange("conditionLanguagePreferenceId", value);
		 this.conditionLanguagePreferenceId = value;
  }

  /**
   * @return the institutionId
   */
  public int getInstitutionId() {
    return institutionId;
  }

  /**
   * @param institutionId the institutionId to set
   */
  public void setInstitutionId(int institutionId) {
    this.testChange("institutionProfileId", institutionId);
    this.institutionId = institutionId;
  }

}
