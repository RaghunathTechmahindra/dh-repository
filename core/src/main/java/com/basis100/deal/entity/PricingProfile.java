
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class PricingProfile extends DealEntity {
    protected int pricingProfileId;
    protected String indexName;
    protected int profileStatusId;
    protected String ppBusinessId;
    protected int interestRateChangeFrequency;
    protected int pricingRegistrationTerm;
    protected boolean primeIndexIndicator;
    protected int pricingStatusId;
    protected Date rateDisabledDate;
    protected String ppDescription;
    protected String rateCode; // Add by Eden 06/29/2000
    protected String rateCodeDescription;
    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    protected int primeIndexRateProfileId;
    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//
    protected int institutionProfileId;
    protected PricingProfilePK pk;

    public PricingProfile(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    public PricingProfile(SessionResourceKit srk, int id)
            throws RemoteException, FinderException {

        super(srk);

        pk = new PricingProfilePK(id);

        findByPrimaryKey(pk);
    }

    public PricingProfile findByPricingRateInventory(int priid)
            throws Exception {

        StringBuffer sql = new StringBuffer(
                "Select * from pricingprofile where pricingprofileid = (select pricingprofileid from pricingrateinventory where pricingrateinventoryid = "
                        + priid + ")");
        //boolean gotRecord = false;
        PricingProfile pp = null;
        try {
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {
                //gotRecord = true;
                pp = new PricingProfile(srk);
                pp.setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PricingProfile Entity - findByPricingRateInventory("
                            + priid + ") exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return pp;
    }

    public PricingProfile findByPrimaryKey(PricingProfilePK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from PricingProfile where " + pk.getName()
                + " = " + pk.getId();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public List findByRateCode(String rateCode) throws Exception {

        List found = new ArrayList();
        StringBuffer sql = new StringBuffer(
                "Select * from PricingProfile where ");
        sql.append("rateCode = \'" + rateCode.trim() + "\'");

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                PricingProfile pp = new PricingProfile(srk);
                pp.setPropertiesFromQueryResult(key);
                pp.pk = new PricingProfilePK(pp.getPricingProfileId());
                found.add(pp);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PricingProfile Entity - findByRateCode() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
    }

    // End of Find by Rate Code

    public List findByRateCodeDescription(String rateCodeDescription)
            throws Exception {

        List found = new ArrayList();
        StringBuffer sql = new StringBuffer(
                "Select * from PricingProfile where ");
        sql.append("rateCodeDescription = \'" + rateCodeDescription.trim()
                + "\'");

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                PricingProfile pp = new PricingProfile(srk);
                pp.setPropertiesFromQueryResult(key);
                pp.pk = new PricingProfilePK(pp.getPricingProfileId());
                found.add(pp);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PricingProfile Entity - findByRateCodeDescription() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
    }

    // * Get Pricing Rate Inventory related ( PricingProfileId )
    /**
     * Returns the Collection of PricingRateInventory objects whose ProcingProfileId
     * is equals to the PricingProfileId of the current object. 
     * 
     * FXP27736 Note: fecthPricingRateInventories is no longer being used because it 
     * generates excess sql calls. We are calling the findByPricingProfileId in the 
     * PricingRateInventory class instead.
     */
    public Collection getPricingRateInventories() throws FinderException {
    	try{
    		PricingRateInventory priSelector = new PricingRateInventory(srk);
    		return priSelector.findByPricingProfileId(pk.getId());
    	} catch (Exception e) {
               FinderException fe = new FinderException(
                       "Exception caught while fetching PricingProfile:PricingRateInventory");
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }
	}

    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    public List findByPrimeIndexRateProfileId(int id) throws Exception {

        List found = new ArrayList();
        StringBuffer sql = new StringBuffer(
                "Select * from PricingProfile where ");
        sql.append("primeIndexRateProfileId = " + id);
        sql.append(" order by pricingProfileId asc ");

        // temp
        logger.debug("PPE@findByPrimeIndexRateProfileId: " + sql.toString());

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                PricingProfile pp = new PricingProfile(srk);
                pp.setPropertiesFromQueryResult(key);
                found.add(pp);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PricingProfile Entity - findByPrimeIndexRateProfileId(() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    // --Ticket#1736--18July2005--start--//
    public PricingRateInventory findLatestInventoryByPricingProfileId(int id)
            throws Exception {

        PricingRateInventory pri = null;
        StringBuffer sql = new StringBuffer(
                "Select * from PricingRateInventory where ");
        sql.append("pricingProfileId = " + id);
        sql.append(" AND indexExpiryDate is null");

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                pri = new PricingRateInventory(srk, jExec.getInt(key, 1));
                break; // should be only one record.
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PricingProfile Entity - findLatestInventoryByPricingProfileId() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return pri;
    }

    // --Ticket#1736--18July2005--end--//

    private void setPropertiesFromQueryResult(int key) throws Exception {
        this.setPricingProfileId(jExec.getInt(key, "PricingProfileId"));
        this.setIndexName(jExec.getString(key, "IndexName"));
        this.setProfileStatusId(jExec.getInt(key, "ProfileStatusId"));
        this.setPPBusinessId(jExec.getString(key, "PPBusinessId"));
        this.setInterestRateChangeFrequency(jExec.getInt(key,
                "InterestRateChangeFrequency"));
        this.setPricingRegistrationTerm(jExec.getInt(key,
                "PricingRegistrationTerm"));
        this
                .setPrimeIndexIndicator(jExec.getString(key,
                        "PrimeIndexIndicator"));
        this.setPricingStatusId(jExec.getInt(key, "PricingStatusId"));
        this.setRateDisabledDate(jExec.getDate(key, "RateDisabledDate"));
        this.setPPDescription(jExec.getString(key, "PPDescription"));
        this.setRateCode(jExec.getString(key, "RateCode"));
        this
                .setRateCodeDescription(jExec.getString(key,
                        "RateCodeDescription"));
        // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        this.setPrimeIndexRateProfileId(jExec.getInt(key,
                "PrimeIndexRateProfileId"));
        // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//
        this.setInstitutionProfileId(jExec.getInt(key, "InstitutionProfileId"));
    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    public String getEntityTableName() {

        return "PricingProfile";
    }

    /**
     * gets the profileStatusId associated with this entity
     * 
     */
    public int getProfileStatusId() {

        return this.profileStatusId;
    }

    /**
     * gets the pricingRegistrationTerm associated with this entity
     * 
     *   @return a int
     */
    public int getPricingRegistrationTerm () {
        return this.pricingRegistrationTerm;
    }

    /**
     * gets the pricingProfileId associated with this entity
     * 
     */
    public int getPricingProfileId() {

        return this.pricingProfileId;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    /**
     * gets the primeIndexRateProfileId associated with this entity
     * 
     */
    public int getPrimeIndexRateProfileId() {

        return this.primeIndexRateProfileId;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    /**
     * gets the interestRateChangeFrequency associated with this entity
     * 
     *   @return a int
     */
    public int getInterestRateChangeFrequency() {

        return this.interestRateChangeFrequency;
    }

    // gets the Rate Code
    public String getRateCode() {

        return this.rateCode;
    }

    public String getRateCodeDescription() {

        return this.rateCodeDescription;
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a PricingProfilePK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * gets the indexName associated with this entity
     * 
     * @return a String
     */
    public String getIndexName() {

        return this.indexName;
    }

    public int getPricingStatusId() {

        return this.pricingStatusId;
    }

    public Date getRateDisabledDate() {

        return this.rateDisabledDate;
    }

    public String getPPDescription() {

        return this.ppDescription;
    }

    public String getPPBusinessId() {

        return this.ppBusinessId;
    }

    public boolean getPrimeIndexIndicator() {

        return this.primeIndexIndicator;
    }

    public PricingProfilePK createPrimaryKey() throws CreateException {

        String sql = "Select PricingProfileseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PricingProfile Entity create() exception getting PricingProfileId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        return new PricingProfilePK(id);
    }

    /**
     * creates a PricingProfile using the PricingProfileId with mininimum fields
     * 
     */

    public PricingProfile create(PricingProfilePK pk) throws RemoteException,
            CreateException {

        String sql = "Insert into PricingProfile(" + pk.getName()
                + ",INSTITUTIONPROFILEID ) Values ( " + pk.getId() + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PricingProfile Entity - PricingProfile - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        this.pk = pk;
        srk.setModified(true);
        return this;
    }

    public void setPricingProfileId(int value) {

        this.testChange("pricingProfileId", value);
        this.pricingProfileId = value;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    /**
     * gets the primeIndexRateProfileId associated with this entity
     * 
     */
    public void setPrimeIndexRateProfileId(int value) {

        this.testChange("primeIndexRateProfileId", value);
        this.primeIndexRateProfileId = value;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    public void setInterestRateChangeFrequency(int value) {

        this.testChange("interestRateChangeFrequency", value);
        this.interestRateChangeFrequency = value;
    }

    public void setIndexName(String value) {

        this.testChange("indexName", value);
        this.indexName = value;
    }

    public void setPricingRegistrationTerm(int value) {

        this.testChange("pricingRegistrationTerm", value);
        this.pricingRegistrationTerm = value;
    }

    public void setProfileStatusId(int value) {

        this.testChange("profileStatusId", value);
        this.profileStatusId = value;
    }

    public void setPPBusinessId(String value) {

        this.testChange("ppBusinessId", value);
        this.ppBusinessId = value;
    }

    public void setPrimeIndexIndicator(String value) {

        setPrimeIndexIndicator(TypeConverter.booleanFrom(value));
    }

    public void setPrimeIndexIndicator(boolean value) {

        this.testChange("primeIndexIndicator", value);
        this.primeIndexIndicator = value;
    }

    public void setPricingStatusId(int value) {

        this.testChange("pricingStatusId", value);
        this.pricingStatusId = value;
    }

    public void setPPDescription(String value) {

        this.testChange("ppDescription", value);
        this.ppDescription = value;
    }

    public void setRateDisabledDate(Date value) {

        this.testChange("rateDisabledDate", value);
        this.rateDisabledDate = value;
    }

    public void setRateCode(String value) {

        this.testChange("rateCode", value);
        this.rateCode = value;
    }

    public void setRateCodeDescription(String value) {

        this.testChange("RateCodeDescription", value);
        this.rateCodeDescription = value;
    }
}
