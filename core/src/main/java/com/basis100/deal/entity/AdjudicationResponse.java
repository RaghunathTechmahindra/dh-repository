package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationResponsePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class AdjudicationResponse extends DealEntity{
	protected int           responseId;
	protected int	        adjudicationStatusId;
	protected int 			instProfileId;
	protected String		adjudicationConfirmNo;

	private AdjudicationResponsePK pk;
	private int globalInternalScore;
	private int globalCreditBureauScore;
	private String globalCreditScoreCode;

	public static final String GLOBALCREDITBUREAUSCORE ="globalCreditBureauScore";
	public static final String GLOBALCREDITSCORECODE = "globalCreditScoreCode";
	public static final String GLOBALINTERNALSCORE = "globalInternalScore";
	public static final String GLOBALRISKRATING = "globalRiskRating";
	private double globalRiskRating;

	public AdjudicationResponse(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public AdjudicationResponse(SessionResourceKit srk, int id) throws RemoteException, FinderException
	{
		super(srk);
	    pk = new AdjudicationResponsePK(id);
	    findByPrimaryKey(pk);
	  }

	/**  search for AdjudicationResponse record using primary key
	*   @return a AdjudicationResponse */
	public AdjudicationResponse findByPrimaryKey(AdjudicationResponsePK pk) throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from AdjudicationResponse " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "AdjudicationResponse: @findByPrimaryKey(), key= " + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException("AdjudicationResponse Entity - findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}


    /**  sets properties for AdjudicationResponse */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	  	this.setResponseId(jExec.getInt(key, "responseId"));
	  	this.setAdjudicationStatusId(jExec.getInt(key, "adjudicationStatusId"));
	  	this.setAdjudicationConfirmNo(jExec.getString(key, "adjudicationConfirmNo"));
		this.setGlobalCreditBureauScore(jExec.getInt(key,
			GLOBALCREDITBUREAUSCORE));
		this.setGlobalCreditScoreCode(jExec.getString(key,
			GLOBALCREDITSCORECODE));
		this.setGlobalInternalScore(jExec.getInt(key, GLOBALINTERNALSCORE));
		this.setGlobalRiskRating(jExec.getDouble(key, GLOBALRISKRATING));
		this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	public String getEntityTableName()
	{
	    return "AdjudicationResponse";
	}

	/**  gets the pk associated with this entity
	 *   @return a AdjudicationResponsePK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}

	public int getResponseId()
	{
		return this.responseId;
	}

	public void setResponseId(int id)
	{
		this.testChange ("responseId", id);
		this.responseId = id;
	}

	public int getAdjudicationStatusId()
	{
		return this.adjudicationStatusId;
	}

	public void setAdjudicationStatusId(int id)
	{
		this.testChange ("adjudicationStatusId", id);
		this.adjudicationStatusId = id;
	}

	public String getAdjudicationConfirmNo()
	{
		return this.adjudicationConfirmNo;
	}

	public void setAdjudicationConfirmNo(String num)
	{
		this.testChange ("adjudicationConfirmNo", num ) ;
		this.adjudicationConfirmNo = num;
	}

	/**  creates AdjudicationResponse record into database
	 *   @return a AdjudicationResponse */
	public AdjudicationResponse create(ResponsePK rpk,
										int adjstatus)throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into AdjudicationResponse(" + rpk.getName() + ","
				+ "adjudicationStatusId,INSTITUTIONPROFILEID) Values ( "
				+ rpk.getId() + "," + adjstatus + ","
				+ srk.getExpressState().getDealInstitutionId() + ")";

		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new AdjudicationResponsePK(rpk.getId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("AdjudicationResponse Entity - AdjudicationResponse - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);

			throw ce;
		}
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
		return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof AdjudicationResponse)
		{
			AdjudicationResponse oa = (AdjudicationResponse) o;
			if  (  this.responseId == oa.getResponseId ()  )
				return true;
		}
		return false;
	}

    public Collection getSAMResponses() throws Exception
    {
      return new SAMResponse(srk).findByAdjudicationResponse(this.pk);
    }

    public Collection findByResponse(ResponsePK pk) throws Exception
    {
      Collection adjresponses = new ArrayList();

      String sql = "Select * from AdjudicationResponse " ;
            sql +=  pk.getWhereClause();
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              AdjudicationResponse adjresponse = new AdjudicationResponse(srk);
              adjresponse.setPropertiesFromQueryResult(key);
              adjresponse.pk = new AdjudicationResponsePK(adjresponse.getResponseId());
              adjresponses.add(adjresponse);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Response::AdjudicationResponse");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return adjresponses;
    }

    public Collection findByDeal(DealPK pk) throws Exception
    {
      Collection adjresponses = new ArrayList();

      String sql = "Select ar.* from AdjudicationResponse ar, Response r " ;
            sql += "where ar.responseId = r.responseId and r.dealId = ";
            sql += pk.getId();
            sql += " order by r.RequestId desc";
      try
      {

            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              AdjudicationResponse adjresponse = new AdjudicationResponse(srk,jExec.getInt(key,"responseId"));
              adjresponse.setPropertiesFromQueryResult(key);
              adjresponse.pk = new AdjudicationResponsePK(adjresponse.getResponseId());
              adjresponses.add(adjresponse);
            }

            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::AdjudicationResponse");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }
        return adjresponses;
    }

    public AdjudicationResponse findByLastRecForThisDeal(DealPK pk) throws Exception
    {
        {
        	Collection c = null;
        	AdjudicationResponse ar = null;

        	c = findByDeal(pk);
        	if (c.size() > 0)
        	{
        		ar = (AdjudicationResponse)c.toArray()[0];
        	}
        	return ar;
        }
    }

    private void setPk(AdjudicationResponsePK pk) {
      this.pk = pk;
    }

    /**
     * This object should only be cloned with a known new PK.
     */
    public Object clone() throws CloneNotSupportedException {
      throw new CloneNotSupportedException();
    }

    /**
     * This object should only be cloned with a known new response PK.
     * @return Object
     */
    public Object clone(ResponsePK newPk)
    {
      AdjudicationResponse clonedAdjResponse = null;

      try {
          // first create the response to generate the new PK
          clonedAdjResponse = new AdjudicationResponse(srk);
          clonedAdjResponse = clonedAdjResponse.create(newPk, this.adjudicationStatusId);

          clonedAdjResponse = (AdjudicationResponse) super.clone();

          clonedAdjResponse.setPk(new AdjudicationResponsePK(newPk.getId()));
          clonedAdjResponse.setResponseId(newPk.getId());

          clonedAdjResponse.doPerformUpdateForce();
      }
      catch (Exception ex) {
        ex.printStackTrace();
        logger.error("Error in AdjudicationResponse.clone().  Trying to clone responseid = "
                + this.responseId + " " + ex.getMessage());
      }

      return clonedAdjResponse;
    }

    protected void removeSons() throws Exception
    {

 	   // -------- start of all SAMResponse -------------
 	   Collection sresps = this.getSAMResponses();
 	   Iterator itResp = sresps.iterator () ;
 	   while ( itResp.hasNext () )
 	   {
 		  SAMResponse sresp = (SAMResponse) itResp.next () ;
 		   if ( itResp.hasNext () )
 		   {
 			   sresp.ejbRemove ( false );
 		   }
 	       else{
 	    	   sresp.ejbRemove ( true );
 	      }
 	   }
 	   // -------- end of all SAMResponse -------------
    }   

	public int getGlobalInternalScore()
	{
		return globalInternalScore;
	}

	public void setGlobalInternalScore(int globalInternalScore)
	{
		this.testChange(GLOBALINTERNALSCORE, globalInternalScore);
		this.globalInternalScore = globalInternalScore;
	}

	public int getGlobalCreditBureauScore()
	{
		return globalCreditBureauScore;
	}

	public void setGlobalCreditBureauScore(int globalCreditBureauScore)
	{
		this.testChange(GLOBALCREDITBUREAUSCORE, globalCreditBureauScore);
		this.globalCreditBureauScore = globalCreditBureauScore;
	}

	public double getGlobalRiskRating()
	{
		return globalRiskRating;
	}

	public void setGlobalRiskRating(double globalRiskRating)
	{
		this.testChange(GLOBALRISKRATING, globalRiskRating);
		this.globalRiskRating = globalRiskRating;
	}

	public String getGlobalCreditScoreCode()
	{
		return globalCreditScoreCode;
	}

	public void setGlobalCreditScoreCode(String globalCreditScoreCode)
	{
		this.testChange(GLOBALCREDITSCORECODE, globalCreditScoreCode);
		this.globalCreditScoreCode = globalCreditScoreCode;
	}

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}
}
