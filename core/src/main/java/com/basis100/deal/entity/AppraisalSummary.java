/*
 * AppraisalSummary.java: Appraisal Summary Entity Class
 * 
 * Created: 13-FEB-2006
 * Author:  Edward Steel
 */
package com.basis100.deal.entity;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AppraisalSummaryPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

/**
 * Appraisal Summary Entity Class
 * @author ESteel
 */
public class AppraisalSummary extends DealEntity {
	
	protected int responseId;
	protected int instProfileId;
	
	protected byte[] appraisalReport;
	protected Date appraisalDate;
	protected double actualAppraisalValue;	
	
	private AppraisalSummaryPK pk;
	
	/**
	 * Simple Constructor. Creates an empty entity.
	 * @param srk Session Resource Kit to use.
	 * @throws RemoteException If the entity could not be created.
	 */
	public AppraisalSummary(SessionResourceKit srk) throws RemoteException {
		super(srk);
	}
	
	/**
	 * Constructor. Create empty entity and then populate it from the database.
	 * @param srk Session Resource Kit to use.
	 * @param id ID of this summary's record.
	 * @throws RemoteException If the entity couldn't be created.
	 * @throws FinderException If the entity couldn't be subsequently retrieved.
	 */
	public AppraisalSummary(SessionResourceKit srk, int id) throws RemoteException,
	FinderException {
		super(srk);
		pk = new AppraisalSummaryPK(id);
		
		findByPrimaryKey(pk);
	}
	
	/**
	 * @return Time Appraisal Summary's primary key.
	 */
	public IEntityBeanPK getPk() {
		return pk;
	}
	
	public AppraisalSummary create(int responseId) 
			throws RemoteException, CreateException{
		this.pk = new AppraisalSummaryPK(responseId);
		
		String sql = "INSERT INTO APPRAISALSUMMARY(RESPONSEID, INSTITUTIONPROFILEID) VALUES (" + 
			sqlStringFrom(responseId) + "," + srk.getExpressState().getDealInstitutionId() + ")";
		
		try {
			jExec.execute(sql);
			findByPrimaryKey(pk);
			trackEntityCreate();
		}	catch (Exception e) {
			CreateException ce = new CreateException(
						"AppraisalSummary Entity - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;			
		}
		
		srk.setModified(true);
		return this;
	}
	
	/**
	 * Get Entity's name.
	 * @return This Entity's name.
	 */
	public String getEntityTableName() {
		return "AppraisalSummary";
	}
	
	public boolean equals(Object object) {
		if (object instanceof AppraisalSummary) {
			AppraisalSummary otherSummary = (AppraisalSummary) object;
			if(this.responseId == otherSummary.getResponseId()) { 
				return true;
			}
		}
		return false;
	}

	public AppraisalSummary findByPrimaryKey(AppraisalSummaryPK pk)
		throws FinderException {
		this.pk = pk;
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "SELECT * FROM APPRAISALSUMMARY " + pk.getWhereClause();
		findByQuery(sql);
		ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
		return this;
	}
	
  public Collection findByResponse(ResponsePK pk) throws Exception
  {
    Collection appsumms = new ArrayList();

    String sql = "Select * from AppraisalSummary " ;
          sql +=  pk.getWhereClause();
     try
     {
          int key = jExec.execute(sql);
          for (; jExec.next(key); )
          {
            AppraisalSummary appsumm = new AppraisalSummary(srk);
            appsumm.setPropertiesFromQueryResult(key);
            appsumm.pk = new AppraisalSummaryPK(appsumm.getResponseId());
            appsumms.add(appsumm);
          }
          jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while getting Response::AdjudicationResponse");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }
      return appsumms;
  }
  
	private AppraisalSummary findByQuery(String sql) throws FinderException {
		boolean gotRecord = false;
		
		try {
			int key = jExec.execute(sql);
			
			for (; jExec.next(key);) {
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record.
			}
			
			jExec.closeData(key);
			
			if (gotRecord == false) {
				String msg = "AppraisalSummary Entity: @findByQuery(), key= " + pk + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		} catch (Exception e) {
			FinderException fe = new FinderException("AppraisalSummary Entity - findByQuery() exception");
			
			logger.error(fe.getMessage());
			logger.error("finder sql " + sql);
			logger.error(e);
			
			throw fe;
		}
		
		return this;
	}
	
	
	private void setPropertiesFromQueryResult(int key) throws Exception {
		
		setResponseId(jExec.getInt(key, "RESPONSEID"));	
		setAppraisalReport(readBlob(jExec.getBlob(key, "APPRAISALREPORT")));
		setAppraisalDate(jExec.getDate(key, "APPRAISALDATE"));
		setActualAppraisalValue(jExec.getDouble(key, "ACTUALAPPRAISALVALUE"));
		setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));	
	}
	
  /**
   * Performs database update.
   * 
   * @throws Exception
   */
  protected int performUpdate() throws Exception {
    Class clazz = this.getClass();

    int ret = doPerformUpdate(this, clazz);
  	writeReport();

    return ret;
  }
  
  
  /**
   *  Writes the current appraisal report to the database column. Method logic is
   *  predicated on a read having been performed during findBy or create.
   */
  private void writeReport()
  {

  	byte[] data = getAppraisalReport();
  	
     //========================================================================
  	 // fix from CreditBureauReport
     //========================================================================
     srk.setCloseJdbcConnectionOnRelease(true);

     JdbcExecutor je =  srk.getJdbcExecutor();
     Connection con = je.getCon();
     ResultSet rs = null;
     Statement stm = null;

     try
     {
    	 con.setAutoCommit(false);
       oracle.sql.BLOB blob = null;
       stm = con.createStatement() ;

       String st1 = "Update AppraisalSummary set appraisalReport = empty_blob()" + getPk().getWhereClause();

       stm.executeUpdate(st1);

       String st2 = "Select appraisalReport from appraisalSummary " + getPk().getWhereClause();

       rs = stm.executeQuery(st2);

       if (rs.next())
       {
         blob = (oracle.sql.BLOB)rs.getObject(1);

         if(blob == null)
          throw new Exception("AppraisalSummary: Could not obtain LOB locator: " + getPk());
       }



       OutputStream os = blob.getBinaryOutputStream();
       ByteArrayOutputStream bop = new ByteArrayOutputStream(data.length);
       bop.write(data);
       bop.writeTo(os);
       bop.flush();
       bop.close();
       os.close();
       
       rs.close();
       stm.close();
       
       
     }
     catch(Exception e)
     {
       e.printStackTrace();
       logger.error("AppraisalSummary: failed to write data to report: " + this.getPk());
       logger.info("In Transaction: " + je.isInTransaction());

       String msg = "";
       if(e.getMessage() == null)
        msg = "Unknown Error Occurred";
       logger.error("AppraisalSummary: Reason for failure: " + msg);

       logger.error(e);

       try{ rs.close();      }catch(Exception ea){;}
       try{ stm.close();     }catch(Exception eb){;}
     }

  }
  
	//----- START: getters / setters ---------------------------------------------
	/**
	 * @return Returns the actual appraisal value.
	 */
	public double getActualAppraisalValue() {
		return actualAppraisalValue;
	}

	/**
	 * @param actualAppraisalValue The actual appraisal value to set.
	 */
	public void setActualAppraisalValue(double actualAppraisalValue) {
		testChange("actualAppraisalValue", actualAppraisalValue);
		this.actualAppraisalValue = actualAppraisalValue;
	}

	/**
	 * @return Returns the appraisal report.
	 */
	public byte[] getAppraisalReport() {
		return appraisalReport;
	}

	/**
	 * @param appraisalReport The appraisal report to set.
	 */
	public void setAppraisalReport(byte[] appraisalReport) {
		testChange("appraisalReport", appraisalReport);
		this.appraisalReport = appraisalReport;
	}

	/**
	 * @return Returns the appraisal date.
	 */
	public Date getAppraisalDate() {
		return appraisalDate;
	}

	/**
	 * @param appraisalDate The appraisal date to set.
	 */
	public void setAppraisalDate(Date appraisalDate) {
		testChange("appraisalDate", appraisalDate);
		this.appraisalDate = appraisalDate;
	}

	/**
	 * @return Returns the response ID.
	 */
	public int getResponseId() {
		return responseId;
	}

	/**
	 * @param responseId The responseID to set.
	 */
	public void setResponseId(int responseId) {
		testChange("responseId", responseId);
		this.responseId = responseId;
	}

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}
 
	//------ END: getters / setters ----------------------------------------------
}
