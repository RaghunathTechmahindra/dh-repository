
package com.basis100.deal.entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.deal.pk.PricingRateInventoryPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

// Eden 06/30/2000
/**
 * 
 * @version 1.5 <br>
 * @author: NBC/PP Implementation Team <br>
 *          Date: 06/28/2006 <br>
 *          Change: <br>
 *          added a new method to get PricingRateInventory based on rateCode &
 *          indexEffectiveDate. <br> - public PricingRateInventory
 *          getPricingRateInventoryId(String rateCode, String
 *          indexEffectiveDate) <br>
 * @version 1.6 August 14, 2008 MCM Team (XS 9.3 Modified the methods to change 
 * 				the rate float down functionality for Components.)          
 */

public class PricingRateInventory extends DealEntity {

    protected int pricingRateInventoryID;
    protected int pricingProfileID;
    protected double discountPercentage;
    protected double internalRatePercentage;
    protected Date indexEffectiveDate;
    protected Date indexExpiryDate;
    protected double baseRatePercentage;
    protected double bestRate;
    protected double maximumDiscountAllowed;
    protected int institutionProfileId;
    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    protected int primeIndexRateInventoryId;
    protected double primeBaseAdj;
    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    // --Ticket#1736--18July2005--start--//
    protected double teaserDiscount;
    protected int teaserTerm;
    // --Ticket#1736--18July2005--end--//

    protected PricingRateInventoryPK pk;

    public PricingRateInventory(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    public PricingRateInventory(SessionResourceKit srk, int id)
            throws RemoteException, FinderException {

        super(srk);

        pk = new PricingRateInventoryPK(id);

        findByPrimaryKey(pk);
    }

    public PricingRateInventory findByPrimaryKey(PricingRateInventoryPK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from PricingRateInventory where " + pk.getName()
                + " = " + pk.getId();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key, false);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public List findByPricingProfileId(int id) throws Exception {

        List found = new ArrayList();
        StringBuffer sql = new StringBuffer(
                "Select * from PricingRateInventory where ");
        sql.append("pricingProfileId = " + id);

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                PricingRateInventory pri = new PricingRateInventory(srk);
                pri.setPropertiesFromQueryResult(key, false);
                found.add(pri);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PricingRateInventory Entity - findByPricingProfileId() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
    }
    
    /**
     * Returns the associated PricingRateInventory entity of the date specified.
     * 
     * @param component
     * @param date
     * @return
     * @throws Exception
     */
    public PricingRateInventory findByComponentAndDate(Component component, Date date)
    throws Exception {
    	
    	return findByPricingProfileAndDate(component.getActualPricingProfileId(),date);
    	
    }
    
    /**
     * Returns the associated PricingRateInventory entity of the date specified.
     * 
     * @param deal
     * @param date
     * @return
     * @throws Exception
     */
    public PricingRateInventory findByDealAndDate(Deal deal, Date date)
    throws Exception {
    	
    	return findByPricingProfileAndDate(deal.getActualPricingProfileId(),date);
    	
    }
    /**
     * 
     * Returns the associated PricingRateInventory entity of the date specified.
     * 
     * @param deal
     * @param date
     * @return pricingRateInventory
     * @throws Exception
     */
    public PricingRateInventory findByPricingProfileAndDate(int actualPricingProfileId, Date date)
            throws Exception {

        String sql = "Select * from PricingRateInventory where ";
        sql += "(indexEffectiveDate <= " + sqlStringFrom(date) + " and ";
        sql += "(indexExpiryDate >=  " + sqlStringFrom(date)
                + " or indexExpiryDate is null)) ";
        sql += "and pricingProfileId = " + actualPricingProfileId;
        sql += " and not(indexEffectiveDate > " + sqlStringFrom(date)
                + ") order by";
        sql += " internalRatePercentage asc";

        Collection pris = priSQLCall(sql);

        // throws an exception if there are no records
        if (pris.isEmpty()) {
            Exception ex = new Exception(
                    "Exception caught while fetching Deal and Date::PricingRateInventory::Empty record");
            logger.error(ex.getMessage());
            throw ex;
        }
        return (PricingRateInventory) pris.toArray()[0];
    }
    
    /**
     * Returns the PricingRateInventory entity of the best
     * internalRatePercentage (minimum rate) between the date specified and
     * current date.
     * @param component
     * @param startDate
     * @return
     * @throws Exception
     */
    public PricingRateInventory findPriByMinRate(Component component, Date startDate)
    throws Exception {
    	
    	return findPriByMinRate(component.getActualPricingProfileId(),startDate);
    }
    
    /**
     * Returns the PricingRateInventory entity of the best
     * internalRatePercentage (minimum rate) between the date specified and
     * current date.
     * @param deal
     * @param startDate
     * @return
     * @throws Exception
     */
    public PricingRateInventory findPriByMinRate(Deal deal, Date startDate)
    throws Exception {
    	
    	return findPriByMinRate(deal.getActualPricingProfileId(),startDate);
    }
    /**
     * Returns the PricingRateInventory entity of the best
     * internalRatePercentage (minimum rate) between the date specified and
     * current date.
     * 
     * @param deal
     * @param startDate
     * @return PricingRateInventory
     * @throws Exception
     */

    public PricingRateInventory findPriByMinRate(int actualPricingProfileId, Date startDate)
            throws Exception {

        Date endDate = new Date();
        String sql = "Select * from PricingRateInventory where (indexEffectiveDate >= "
                + sqlStringFrom(startDate);
        sql += " or (indexExpiryDate >= " + sqlStringFrom(startDate)
                + ") or indexExpiryDate is null) ";
        sql += "and pricingProfileId = " + actualPricingProfileId;
        sql += " and not(indexEffectiveDate > " + sqlStringFrom(endDate);
        sql += ") order by internalRatePercentage asc, pricingRateInventoryId desc";

        Collection pris = priSQLCall(sql);

        // throws an exception if there are no records
        if (pris.isEmpty()) {
            Exception ex = new Exception(
                    "Exception caught while fetching Deal and Date::PricingRateInventory::Empty record");
            logger.error(ex.getMessage());
            throw ex;
        }
        return (PricingRateInventory) pris.toArray()[0];
    }
    
    /**
     * Returns the PricingRateInventory entity of the best
     * internalRatePercentage (minimum rate) between the start date and the end
     * date.
     * @param component
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    public PricingRateInventory findPriByMinRate(Component component, Date startDate,
            Date endDate) throws Exception {
    	
    	return findPriByMinRate(component.getActualPricingProfileId(),startDate,endDate);
    }
    
    /**
     * Returns the PricingRateInventory entity of the best
     * internalRatePercentage (minimum rate) between the start date and the end
     * date.
     * @param deal
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    public PricingRateInventory findPriByMinRate(Deal deal, Date startDate,
            Date endDate) throws Exception {
    	
    	return findPriByMinRate(deal.getActualPricingProfileId(),startDate,endDate);
    }
    /**
     * Returns the PricingRateInventory entity of the best
     * internalRatePercentage (minimum rate) between the start date and the end
     * date.
     * 
     * @param deal
     * @param startDate
     * @param endDate
     * @return PricingRateInventory
     * @throws Exception
     */

    public PricingRateInventory findPriByMinRate(int actualPricingProfileId,Date startDate,
            Date endDate) throws Exception {

        String sql = "Select * from PricingRateInventory where (indexEffectiveDate >= "
                + sqlStringFrom(startDate);
        sql += " or (indexExpiryDate >= " + sqlStringFrom(startDate) + ")) ";
        sql += "and pricingProfileId = " + actualPricingProfileId;
        sql += " and not(indexEffectiveDate > " + sqlStringFrom(endDate);
        sql += ") order by internalRatePercentage asc, pricingRateInventoryId desc";

        Collection pris = priSQLCall(sql);

        // throws an exception if there are no records
        if (pris.isEmpty()) {
            Exception ex = new Exception(
                    "Exception caught while fetching Deal and Date::PricingRateInventory::Empty record: "
                            + sql);
            logger.error(ex.getMessage());
            throw ex;
        }
        return (PricingRateInventory) pris.toArray()[0];
    }

    private Collection priSQLCall(String sql) throws Exception {

        Collection pris = new Vector();
        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                // for each record, create PricingRateInventoryEntity
                PricingRateInventory pri = new PricingRateInventory(srk);
                pri.setPropertiesFromQueryResult(key, false);
                pri.pk = new PricingRateInventoryPK(pri
                        .getPricingRateInventoryID());
                pris.add(pri);
            }
            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while fetching Deal and Date::PricingRateInventory");
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }
        return pris;
    }

    /**
     * Returns the PricingRateInventory entity associated to the
     * pricingRateInventoryId
     * 
     * @param priid
     * @return PricingRateInventory
     * @throws Exception
     */
    public PricingRateInventory findByPricingRateInventory(int priid)
            throws Exception {

        String sql = "Select * from PricingRateInventory where pricingRateInventoryId = "
                + priid;
        // creates PricingRateInventory entity
        PricingRateInventory pri = new PricingRateInventory(srk);
        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                pri.setPropertiesFromQueryResult(key, false);
                pri.pk = new PricingRateInventoryPK(pri
                        .getPricingRateInventoryID());
            }
            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while fetching PricingRateInventory");
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }
        return pri;
    }

    private void setPropertiesFromQueryResult(int key, boolean deep)
            throws Exception {

        this.setPricingRateInventoryID(jExec.getInt(key,
                "PricingRateInventoryID"));
        this.setPricingProfileID(jExec.getInt(key, "PricingProfileID"));
        this.setDiscountPercentage(jExec.getDouble(key, "DiscountPercentage"));
        this.setInternalRatePercentage(jExec.getDouble(key,
                "InternalRatePercentage"));
        this.setIndexEffectiveDate(jExec.getDate(key, "IndexEffectiveDate"));
        this.setIndexExpiryDate(jExec.getDate(key, "IndexExpiryDate"));
        this.setBaseRatePercentage(jExec.getDouble(key, "BaseRatePercentage"));
        this.setBestRate(jExec.getDouble(key, "BestRate"));
        this.setMaximumDiscountAllowed(jExec.getDouble(key,
                "MaximumDiscountAllowed"));
        // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        this.setPrimeIndexRateInventoryId(jExec.getInt(key,
                "PrimeIndexRateInventoryId"));
        this.setPrimeBaseAdj(jExec.getDouble(key, "PrimeBaseAdj"));
        // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//
        // --Ticket#1736--18July2005--start--//
        this.setTeaserDiscount(jExec.getDouble(key, "TeaserDiscount"));
        this.setTeaserTerm(jExec.getInt(key, "TeaserTerm"));
        // --Ticket#1736--18July2005--end--//
        this.setInstitutionProfileId(jExec.getInt(key, "InstitutionProfileId"));
    }

    /**
     * gets the value of instace fields as String. if a field does not exist (or
     * the type is not serviced)** null is returned. if the field exists (and
     * the type is serviced) but the field is null an empty String is returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    public String getEntityTableName() {

        return "PricingRateInventory";
    }

    /** all the gets */
    public double getBaseRatePercentage() {

        return this.baseRatePercentage;
    }

    public double getDiscountPercentage() {

        return this.discountPercentage;
    }

    public double getInternalRatePercentage() {

        return this.internalRatePercentage;
    }

    public int getPricingProfileID() {

        return this.pricingProfileID;
    }

    //add by Clement for beanutil 
    public PricingProfile getPricingProfile() throws FinderException, RemoteException{
        return new PricingProfile(srk,this.getPricingProfileID());
    }

    public int getPricingRateInventoryID() {

        return this.pricingRateInventoryID;
    }

    public Date getIndexEffectiveDate() {

        return this.indexEffectiveDate;
    }

    public Date getIndexExpiryDate() {

        return this.indexExpiryDate;
    }

    public double getBestRate() {

        return this.bestRate;
    }

    public double getMaximumDiscountAllowed() {

        return this.maximumDiscountAllowed;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    public int getPrimeIndexRateInventoryId() {

        return this.primeIndexRateInventoryId;
    }

    public double getPrimeBaseAdj() {

        return this.primeBaseAdj;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    // --Ticket#1736--18July2005--start--//
    public double getTeaserDiscount() {

        return teaserDiscount;
    }

    public void setTeaserDiscount(double value) {

        this.testChange("teaserDiscount", value);
        this.teaserDiscount = value;
    }

    public int getTeaserTerm() {

        return teaserTerm;
    }

    public void setTeaserTerm(int value) {

        this.testChange("teaserTerm", value);
        this.teaserTerm = value;
    }

    // --Ticket#1736--18July2005--end--//

    /**
     * gets the pk associated with this entity
     * 
     * @return a PricingRateInventoryPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public PricingRateInventoryPK createPrimaryKey() throws CreateException {

        String sql = "Select PricingRateInventoryseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PricingRateInventory Entity create() exception getting PricingProfileId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        return new PricingRateInventoryPK(id);
    }

    /**
     * creates a PricingRateInventory using the PricingRateInventoryId with
     * mininimum fields
     * 
     */

    public PricingRateInventory create(PricingRateInventoryPK pk)
            throws RemoteException, CreateException {

        String sql = "Insert into PricingRateInventory(" + pk.getName()
                + ", INSTITUTIONPROFILEID) Values ( " + pk.getId() + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PricingRateInventory Entity - PricinRateInventory - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        this.pk = pk;

        return this;
    }

    /**
     * creates a PricingRateInventory using the PricingRateInventoryId with
     * mininimum fields
     * 
     */

    public PricingRateInventory create(PricingRateInventoryPK pk,
            PricingProfilePK ppk) throws RemoteException, CreateException {

        String sql = "Insert into PricingRateInventory( pricingRateInventoryID, pricingProfileID,INSTITUTIONPROFILEID ) Values ( "
                + pk.getId()
                + ", "
                + ppk.getId()
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityChange();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PricingRateInventory Entity - PricingRateInventory - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        return this;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    /**
     * creates a PricingRateInventory record
     */
    public PricingRateInventory create(int pricingProfileID,
            double discountPercentage, double internalRatePercentage,
            String indexEffectiveDate, String indexExpiryDate,
            double baseRatePercentage, double bestRate,
            double maximumDiscountAllowed, int primeIndexRateInventoryId,
            double primeBaseAdj,
            // --Ticket#1736--18July2005--start--//
            double teaserDiscount, int teaserTerm) throws RemoteException,
            CreateException
    // --Ticket#1736--18July2005--end--//
    {

        pk = createPrimaryKey();

        String sql = "Insert into pricingRateInventory (pricingRateInventoryID,  pricingProfileID, discountPercentage, "
                + " internalRatePercentage, indexEffectiveDate, "
                + " indexExpiryDate, baseRatePercentage, bestRate, maximumDiscountAllowed, "
                + " primeIndexRateInventoryId, primeBaseAdj, teaserDiscount, teaserTerm, INSTITUTIONPROFILEID ) Values ( "
                + pk.getId()
                + ", "
                + pricingProfileID
                + ", "
                + discountPercentage
                + ", "
                + internalRatePercentage
                + ", "
                + indexEffectiveDate
                + ", "
                + indexExpiryDate
                + ", "
                + baseRatePercentage
                + ", "
                + bestRate
                + ", "
                + maximumDiscountAllowed
                + ", "
                + primeIndexRateInventoryId
                +
                // --Ticket#1736--18July2005--start--//
                ", "
                + primeBaseAdj
                + ", "
                + teaserDiscount
                + ", "
                + teaserTerm
                + "," + srk.getExpressState().getDealInstitutionId() + ")";
        // --Ticket#1736--18July2005--end--//

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // Track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PricingRateInventory Entity 'copy' - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        return this;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    // ---------------- All the sets ------------------------------------
    public void setBaseRatePercentage(double val) {

        this.testChange("baseRatePercentage", val);
        this.baseRatePercentage = val;
    }

    public void setDiscountPercentage(double val) {

        this.testChange("discountPercentage", val);
        this.discountPercentage = val;
    }

    public void setInternalRatePercentage(double val) {

        this.testChange("internalRatePercentage", val);
        this.internalRatePercentage = val;
    }

    public void setPricingProfileID(int val) {

        this.testChange("pricingProfileID", val);
        this.pricingProfileID = val;
    }

    public void setPricingRateInventoryID(int val) {

        this.testChange("pricingRateInventoryID", val);
        this.pricingRateInventoryID = val;
    }

    public void setIndexEffectiveDate(Date val) {

        this.testChange("indexEffectiveDate", val);
        this.indexEffectiveDate = val;
    }

    public void setIndexExpiryDate(Date val) {

        this.testChange("indexExpiryDate", val);
        this.indexExpiryDate = val;
    }

    public void setBestRate(double val) {

        this.testChange("bestRate", val);
        this.bestRate = val;
    }

    public void setMaximumDiscountAllowed(double val) {

        this.testChange("maximumDiscountAllowed", val);
        this.maximumDiscountAllowed = val;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    public void setPrimeIndexRateInventoryId(int val) {

        this.testChange("primeIndexRateInventoryId", val);
        this.primeIndexRateInventoryId = val;
    }

    public void setPrimeBaseAdj(double val) {

        this.testChange("primeBaseAdj", val);
        this.primeBaseAdj = val;
    }

    // --Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    /**
     * This method returns <code>PricingRateInventory</code> corresponding to
     * indexeffectivedate & ratecode<br>
     * 
     * @param pricingProfileId -
     *            pricing profile rate code <br>
     * @param indexEffectiveDateString -
     *            index effective date related to pricing rate inventory <br>
     * @return - returns <code>PricingRateInventory</code><br>
     * @throws Exception -
     *             If any exception occurs <br>
     */
    public PricingRateInventory findByPricingProfileAndEffectiveDate(
            int pricingProfileId, String indexEffectiveDateString)
            throws Exception {

    	//FXP26840, 4.2GR, Oct 26 09
    	// CMX and FCX sends different format of Date
    	// converting string to data using TypeConverter so that both type of format 
    	// can be understood.
    	Date iedDate = TypeConverter.dateFrom(indexEffectiveDateString);
    	String iedString = "";
    	if(iedDate != null){
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		iedString = sdf.format(iedDate);
    	}

        StringBuffer buf = new StringBuffer();
        buf.append("Select * from PricingRateInventory where");
        buf.append(" to_char(indexEffectiveDate,'yyyy-MM-dd HH24:mi:ss') ='");
        buf.append(iedString);
        buf.append("' and PricingProfileId = ");
        buf.append(pricingProfileId);

        String sql = buf.toString();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key, false);
                break; // can only be one record
            }
            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "PricingRateInventory Entity: @findByPricingProfileAndEffectiveDate("
                        + pricingProfileId
                        + ","
                        + indexEffectiveDateString
                        + "), entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while fetching PricingRateInventory");
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }
        return this;
    }

    /**
     * Ported logic from DealCalcUtil.PandI3YearRate
     * this method returns most recent PricingRateInventry record
     * that is related to the parameter rateCode.
     * 
     * @param rateCode
     * @return Most Recent PricingRateInventry
     * @throws FinderException
     * @since 4.3
     */
    @SuppressWarnings("unchecked")
    public PricingRateInventory findMostRecentByRateCode(String rateCode)
    throws FinderException {

        PricingRateInventory thePricingRateInventory = null;
        Date today = new Date(); // Get current date.

        Collection<PricingProfile> pricingProfiles;
        try {
            pricingProfiles = 
                (new PricingProfile(srk)).findByRateCode(rateCode);
        } catch (Exception e) {
            throw new FinderException("PricingProfile where rateCode = "
                + rateCode + " wasn't found.", e);
        }

        for(PricingProfile pricingProfile: pricingProfiles){

            for(PricingRateInventory aPricingRateInventory : 
                (Collection<PricingRateInventory>)
                pricingProfile.getPricingRateInventories()){

                Date idxEffectiveDate = aPricingRateInventory.getIndexEffectiveDate();
                // Find the most recent date
                if (idxEffectiveDate != null && idxEffectiveDate.before(today)) { 
                    if (thePricingRateInventory == null 
                            || idxEffectiveDate.after(thePricingRateInventory.getIndexEffectiveDate()))
                        thePricingRateInventory = aPricingRateInventory; // Swap
                }
            }
        } 

        if(thePricingRateInventory == null){ 
            throw new FinderException("PricingRateInventory where rateCode = "
                + rateCode + " wasn't found.");
        }
        return thePricingRateInventory;
    }
}
