/*
 * AvmSummary.java AvmSummary Entity Class (Connectivity to AVM services)
 * 
 * created: 06-FEB-2005 modified: by Catherine, March 2006 author: Edward Steel
 */

package com.basis100.deal.entity;

import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AvmSummaryPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

/**
 * AvmSummary entity class.
 * 
 * @author ESteel
 */
public class AvmSummary extends DealEntity {

    protected int responseId;
    protected int institutionProfileId;

    protected String avmReport;
    protected double avmValue;
    protected String confidenceLevel;
    protected double highAvmRange;
    protected double lowAvmRange;
    protected Date valuationDate;

    private AvmSummaryPK pk;

    // indicate if a blob update for this field is needed.
    private boolean isReportChanged = false;

    /**
     * @param srk
     *            The SessionResourceKit to use.
     * @param calcMon
     *            The CalcMonitor to use.
     * @throws RemoteException
     */
    public AvmSummary(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    /**
     * 
     * @param id
     *            ResponseId of this AvmSummary record.
     * @throws RemoteException
     * @throws FinderException
     */
    public AvmSummary(SessionResourceKit srk, int id) throws RemoteException,
            FinderException {

        super(srk);
        pk = new AvmSummaryPK(id);

        findByPrimaryKey(pk);
    }

    /**
     * @return The Avm Summary's primary key.
     */
    public IEntityBeanPK getPk() {

        return pk;
    }

    /**
     * @return The name of the DB table this class represents.
     */
    public String getEntityTableName() {

        return "AvmSummary";
    }

    /**
     * Tests whether this AvmSummary is equal to the given (AvmSummary) object.
     * 
     * @param obj
     *            The Object to test for equality.
     */
    public boolean equals(Object obj) {

        if (obj instanceof AvmSummary) {
            AvmSummary otherObj = (AvmSummary) obj;
            if (this.responseId == otherObj.getResponseId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the value of instance fields as String. If a field does not exist
     * (or the type is not serviced)** null is returned. If the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * Populate this entity from the database.
     * 
     * @param pk
     *            Primary Key referencing this row.
     * @return This, populated from the database.
     * @throws FinderException
     *             If the row could not be retrieved.
     */
    public AvmSummary findByPrimaryKey(AvmSummaryPK pk) throws FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        this.pk = pk;
        String sql = "SELECT * FROM AVMSUMMARY " + pk.getWhereClause();
        findByQuery(sql);
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public AvmSummary create(int responseId) throws CreateException {

        StringBuffer sqlb = new StringBuffer();

        sqlb
                .append("INSERT INTO AvmSummary (RESPONSEID, INSTITUTIONPROFILEID) VALUES (");
        sqlb.append(sqlStringFrom(responseId));
        sqlb.append(",");
        sqlb
                .append(sqlStringFrom(srk.getExpressState()
                        .getDealInstitutionId()));
        sqlb.append(")");

        String sql = new String(sqlb);
        int key = 0;

        try {
            key = jExec.execute(new String(sql));
            findByPrimaryKey(new AvmSummaryPK(responseId));
            trackEntityCreate();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "AvmSummary Entity - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        } finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }

        srk.setModified(true);
        return this;
    }

    public Collection findByResponse(ResponsePK pk) throws Exception {

        Collection avmsumms = new ArrayList();

        String sql = "Select * from AvmSummary ";
        sql += pk.getWhereClause();
        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                AvmSummary avmsumm = new AvmSummary(srk);
                avmsumm.setPropertiesFromQueryResult(key);
                avmsumm.pk = new AvmSummaryPK(avmsumm.getResponseId());
                avmsumms.add(avmsumm);
            }
            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while getting Response::AdjudicationResponse");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }
        return avmsumms;
    }

    private AvmSummary findByQuery(String sql) throws FinderException {

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "AvmSummary Entity: @findByQuery(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "AvmSummary Entity - findByQuery() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

        this.setResponseId(jExec.getInt(key, "RESPONSEID"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        this.setAvmReport(readClob(jExec.getClob(key, "AVMREPORT")));
        this.setAvmValue(jExec.getDouble(key, "AVMVALUE"));
        this.setConfidenceLevel(jExec.getString(key, "CONFIDENCELEVEL"));
        this.setHighAvmRange(jExec.getDouble(key, "HIGHAVMRANGE"));
        this.setLowAvmRange(jExec.getDouble(key, "LOWAVMRANGE"));
        this.setValuationDate(jExec.getDate(key, "VALUATIONDATE"));

    }

    /**
     * Performs database update.
     * 
     * @throws Exception
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        if (isReportChanged == true) {
            writeReport();
            isReportChanged = false;
        }
        int ret = doPerformUpdate(this, clazz);

        return ret;
    }

    /**
     * Writes the current AvmReport to the database column. Method logic is
     * predicated on a read having been performed during findBy or create.
     */
    private void writeReport() {

        //String report = getAvmReport();

        // ========================================================================
        // fix from CreditBureauReport
        // ========================================================================
        srk.setCloseJdbcConnectionOnRelease(true);

        JdbcExecutor je = srk.getJdbcExecutor();
        Connection con = je.getCon();
        ResultSet rs = null;
        Statement stm = null;
        Writer writer = null;

        try {
            con.setAutoCommit(false);
            oracle.sql.CLOB clob = null;
            stm = con.createStatement();

            String st1 = "UPDATE AVMSUMMARY SET AVMREPORT = EMPTY_CLOB() "
                    + getPk().getWhereClause();

            stm.executeUpdate(st1);

            String st2 = "SELECT AVMREPORT FROM AVMSUMMARY "
                    + getPk().getWhereClause();

            rs = stm.executeQuery(st2);

            if (rs.next()) {
                clob = (oracle.sql.CLOB) rs.getObject(1);

                if (clob == null) {
                    throw new Exception(
                            "AvmSummary: Could not obtain LOB locator: "
                                    + this.pk);
                }
            }

            writer = clob.getCharacterOutputStream();

            writer.write(getAvmReport());
            writer.flush();
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("AvmSummary: failed to write data to report: "
                    + this.getPk());
            logger.info("In Transaction: " + je.isInTransaction());

            String msg = "";
            if (e.getMessage() == null) {
                msg = "Unknown Error Occurred";
            }
            logger.error("AvmSummary: Reason for failure: " + msg);

            logger.error(e);

        } finally {
            try {
                if(rs != null) rs.close();
            } catch (Exception ignore){}
            try {
                if(stm != null) stm.close();
            } catch (Exception ignore){}
        }

    }

    // ---- START getters / setters
    // -----------------------------------------------
    /**
     * @return Returns the AVM report.
     */
    public String getAvmReport() {

        return avmReport;
    }

    /**
     * @return Returns the Response ID.
     */
    public int getResponseId() {

        return responseId;
    }

    /**
     * @return Returns the AVM value.
     */
    public double getAvmValue() {

        return avmValue;
    }

    /**
     * @return Returns the confidence value.
     */
    public String getConfidenceLevel() {

        return confidenceLevel;
    }

    /**
     * @return Returns the high range of the AVM result.
     */
    public double getHighAvmRange() {

        return highAvmRange;
    }

    /**
     * @return Returns the low range of the AVM result.
     */
    public double getLowAvmRange() {

        return lowAvmRange;
    }

    /**
     * @param avmReport
     *            The AVM report to set.
     */
    public void setAvmReport(String avmReport) {

        // testChange("avmReport", responseId); // this is done for test only!!
        isReportChanged = true;
        this.avmReport = avmReport;
    }

    /**
     * @param responseId
     *            The Response ID to set.
     */
    public void setResponseId(int responseId) {

        testChange("responseId", responseId);
        this.responseId = responseId;
    }

    /**
     * @param avmValue
     *            The AVM value to set.
     */
    public void setAvmValue(double avmValue) {

        testChange("avmValue", avmValue);
        this.avmValue = avmValue;
    }

    /**
     * @param confidenceLevel
     *            The confidence value to set.
     */
    public void setConfidenceLevel(String confidenceLevel) {

        testChange("confidenceLevel", confidenceLevel);
        this.confidenceLevel = confidenceLevel;
    }

    /**
     * @param highAvmRange
     *            The high range of the AVM value to set.
     */
    public void setHighAvmRange(double highAvmRange) {

        testChange("highAvmRange", highAvmRange);
        this.highAvmRange = highAvmRange;
    }

    /**
     * @param lowAvmRange
     *            The low range of the AVM value to set.
     */
    public void setLowAvmRange(double lowAvmRange) {

        testChange("lowAvmRange", lowAvmRange);
        this.lowAvmRange = lowAvmRange;
    }

    public Date getValuationDate() {

        return valuationDate;
    }

    public void setValuationDate(Date valuationDate) {

        testChange("valuationDate", valuationDate);
        this.valuationDate = valuationDate;
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
}
