package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class ArchivedLoanDecision extends DealEntity
{
	protected int      archivedLoanDecisionId;
	protected int 	 instProfileId;
	protected String   sourceApplicationId;
	protected int 	 dealId;
	protected int		 copyId;
	protected Date     submissionDateTimeStamp;
	protected String   FCXLoanDecision;

	private ArchivedLoanDecisionPK pk;

	public ArchivedLoanDecision(SessionResourceKit srk) throws RemoteException, FinderException
	{
		super(srk);
	}

	public ArchivedLoanDecision(SessionResourceKit srk, int id) throws RemoteException, FinderException
	{
		super(srk);

		pk = new ArchivedLoanDecisionPK(id);

		findByPrimaryKey(pk);
	}

	private ArchivedLoanDecisionPK createPrimaryKey()throws CreateException
	{
		String sql = "Select ArchivedLoanDecisionseq.nextval from dual";
		int id = -1;

		try
		{
			int key = jExec.execute(sql);

			if(jExec.next(key))
				id = jExec.getInt(key,1);  // should only ever be one record
			else
				throw new Exception();
			jExec.closeData(key);

		}
		catch (Exception e)
		{
			CreateException ce = new CreateException("ArchivedLoanDecision Entity create() exception getting ArchivedLoanDecisionId from sequence");
			logger.error(ce.getMessage());
			logger.error(e);
			throw ce;

		}

		return new ArchivedLoanDecisionPK(id);
	}

	/**
	 * creates a ArchivedLoanDecision using the archivedLoanDecisionId with mininimum fields
	 * id argument represents deal.dealId,
	 * copyid represents deal.copyId
	 * sourceApp represents deal.sourceapplicationid
	 */
	public ArchivedLoanDecision create(int id, int copyid, String sourceApp) throws RemoteException, CreateException
	{
		pk = createPrimaryKey();

		StringBuffer sql = new StringBuffer ("Insert into ArchivedLoanDecision (ArchivedLoanDecisionId, institutionprofileid, sourceapplicationid, dealid, copyid) Values ( ");
		sql.append(sqlStringFrom(pk.getId()) + ", ");  //ArchivedLoanDecisionId
		sql.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ", ");  //institutionprofileid
		sql.append(sqlStringFrom(sourceApp) + ", ");  //sourceapplicationid
		sql.append(sqlStringFrom(id) + ", ");  //dealid
		sql.append(sqlStringFrom(copyid)+ ")");  //copyid

		String sqlString = new String(sql);

		try
		{
			jExec.executeUpdate(sqlString);
			findByPrimaryKey(pk);
			trackEntityCreate ();
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException("ArchivedLoanDecision Entity - ArchivedLoanDecision - create() exception", e);
			logger.error(ce.getMessage());
			logger.error(e);

			throw ce;
		}
		srk.setModified(true);
		return this;
	}

	public ArchivedLoanDecision findByPrimaryKey(ArchivedLoanDecisionPK pk) throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from ArchivedLoanDecision " + pk.getWhereClause();

		try
		{
			int key = jExec.execute(sql);

			if (jExec.next(key))
				setPropertiesFromQueryResult(key);
			else
			{
				String msg = "ArchivedLoanDecision Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}

			jExec.closeData(key);

		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("ArchivedLoanDecision Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}
		this.pk = pk;
		ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
		return this;
	}

	/**
	 * setPropertiesFromQueryResult
	 * This method sets the values in entity to save data in database.
	 */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
		setArchivedLoanDecisionId( jExec.getInt(key, "ARCHIVEDLOANDECISIONID"));
		setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
		setSourceApplicationId(jExec.getString(key, "SOURCEAPPLICATIONID"));
		setDealId(jExec.getInt(key, "DEALID"));
		setCopyId(jExec.getInt(key, "COPYID"));
		setSubmissionDateTimeStamp(jExec.getDate(key, "SUBMISSIONDATETIMESTAMP"));
		setFCXLoanDecision(jExec.getString(key, "FCXLOANDECISION"));
	}


	protected int performUpdate()  throws Exception
	{
		//return doPerformUpdate(this, this.getClass());
		int ret = doPerformUpdate(new String[] { "FCXLoanDecision" });
		updateFCXLoanDecision();
		return ret;
	}

	private void updateFCXLoanDecision() throws SQLException, IOException {
		
		if (changeValueMap.containsKey("FCXLoanDecision") == false ) return;
		
		Connection connection = jExec.getCon();
		PreparedStatement statement = null;
		String updateString = "UPDATE ArchivedLoanDecision SET FCXLoanDecision=? WHERE ArchivedLoanDecisionId=?";
		try {
			statement = connection.prepareStatement(updateString);
			statement.setClob(1, createClob(FCXLoanDecision));
			statement.setInt(2, pk.getId());
			statement.executeUpdate();
		} catch (SQLException sqle) {
			try {
				jExec.rollback();
			} catch (Exception e) // IllegalStateException,
			// JdbcTransactionException
			{
				logger.error("Rollback failed.");
				logger.error(e);
			}
			throw sqle;
		} catch (IOException ioe) {
			try {
				jExec.rollback();
			} catch (Exception e) // IllegalStateException,
			// JdbcTransactionException
			{
				logger.error("Rollback failed.");
				logger.error(e);
			}
			throw ioe;
		} finally {
			try {
				statement.close();
			} catch (SQLException sqle) {
				logger.warn(sqle.getMessage());
			}
		}
	}

	public String getEntityTableName()
	{
		return "ArchivedLoanDecision";
	}

	/**
	 *  gets the value of instance fields as String.
	 *  if a field does not exist (or the type is not serviced)** null is returned.
	 *  if the field exists (and the type is serviced) but the field is null an empty
	 *  String is returned.
	 *  @returns value of bound field as a String;
	 *  @param  fieldName as String
	 *
	 *  Other entity types are not yet serviced   ie. You can't get the Province object
	 *  for province.
	 */
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	public Object getFieldValue(String fieldName){
		return doGetFieldValue(this, this.getClass(), fieldName);
	}


	/**
	 *  sets the value of instance fields as String.
	 *  if a field does not exist or is not serviced an Exception is thrown
	 *  if the field exists (and the type is serviced) but the field is null an empty
	 *  String is returned.
	 *
	 *  @param  fieldName as String
	 *  @param  the value as a String
	 *
	 *  Other entity types are not yet serviced   ie. You can't set the Province object
	 *  in Addr.
	 */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}


	/**
	 *   gets the pk associated with this entity
	 *
	 *   @return a archivedLoanDecisionPK
	 */
	public IEntityBeanPK getPk()
	{
		return this.pk;
	}

	private void setArchivedLoanDecisionId(int id)
	{
		this.testChange ("archivedLoanDecisionId", id );
		this.archivedLoanDecisionId = id;
	}

	public int getArchivedLoanDecisionId()
	{
		return this.archivedLoanDecisionId;
	}

	public void setSourceApplicationId(String sourceAppId)
	{
		this.testChange ("sourceApplicationId", sourceAppId);
		this.sourceApplicationId = sourceAppId;
	}

	public String getSourceApplicationId()
	{
		return this.sourceApplicationId;
	}

	private void setDealId(int id)
	{
		this.testChange ("dealId", id );
		this.dealId = id;
	}

	public int getDealId()
	{
		return this.dealId ;
	}

	private void setCopyId(int id)
	{
		this.testChange ("copyId", id );
		this.copyId = id;
	}

	public int getCopyId()
	{
		return this.copyId ;
	}

	public void setSubmissionDateTimeStamp(Date date)
	{
		this.testChange ("submissionDateTimeStamp", date);
		this.submissionDateTimeStamp = date;
	}

	public Date getSubmissionDateTimeStamp()
	{
		return this.submissionDateTimeStamp ;
	}

	public void setFCXLoanDecision(String FCXPayload)
	{
		this.testChange("FCXLoanDecision", FCXPayload);
		this.FCXLoanDecision = FCXPayload;
	}

	public String getFCXLoanDecision()
	{
		return this.FCXLoanDecision ;
	}

	public int getInstProfileId()
	{
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId)
	{
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof ArchivedLoanDecision )
		{
			ArchivedLoanDecision oa = (ArchivedLoanDecision)  o;
			if  (archivedLoanDecisionId == oa.getArchivedLoanDecisionId())
				return true;
		}
		return false;
	}

}
