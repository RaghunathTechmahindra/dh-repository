/*
 * @(#)Component.java Apr 29, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ComponentCreditCardPK;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentOverdraftPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: Component
 * </p>
 * <p>
 * Description: This is an entity class representing Component. This class
 * provides accessor and mutator methods for all the entity attributes. This
 * class will map the entity and database fields. This class will save the
 * appropriate data into the Component Table.
 * </p>
 * @version 1.0 Apr 29, 2008
 * @version 1.1 XS_11.11 10-Jun-2008 Added getEntityTableName(),
 *          getClassId(),getPk(), performUpdate(),findByDeal(DealPk) methods and
 *          added code comments.Modifed testChange attributes for
 *          setFirstPaymentDateLimit(), setTeaserPiAmount ()
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 * 
 * @version 1.2  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  <br>
 * 19-JUN-2008
 * Added getChildren, getComponentLOC , getComponentMortgage for COpy management 
 *
 * @version 1.3 (July 4, 2008): changed getChildren method to avoid Exceptions<br>
 * @version 1.4 XS_2.26 14-Jul-2008 added create(int componentId, int copyId) method
 * @version 1.5 add variable repaymentTypeId to create method
 * @author MCM Impl Team<br>
 * 
 * @version 1.6 (July 15, 2008): XS_2.39 added order componentTypId before compnentId<br>
 * @version 1.7 (July 22, 2008): XS_2.39 modified order as MTG -> LOC -> CreditCard -> Loan -> Overdraft
 *                               in findByDeal methods<br>
 *                               
 * @version 1.8 Deprecated create(int dealId, int copyId, int componentTypeId,int mtgProdId, 
 * 				int pricingRateInventoryId, int repaymentTypeId) for Component as 
 * 				pricingRateInventoryId, repaymentTypeId have been defaulted in the DB
 * @version 1.9 July 31, 2008:fixing SQL in findByDeal.
 * @version 1.10 August 14, 2008 MCM Team (XS 9.3 Modified the methods to change 
 * 				the rate float down functionality for Components.)
 * @version 1.11 artf771147  Sept 03, 2008
 * @version 1.12 FXP22957 Corrected the parameter for setPricingRateInventoryId method
 * @version 1.13 FXP22615 Reverted back to 5547 version changes
 * @vesrion 1.14 Oct 30,2008 FXP23183 Added new method getPrimeIndex(componetId,copyId)
 * @version 1.15 FXP23331, MCM/4.1GR, Jan 13, 2009, fixed that Component.getComponets didn't add dcm to components.
 */
public class Component extends DealEntity
{

    protected int componentId;

    protected int copyId;

    protected String additionalInformation;

    protected int dealId;

    protected int componentTypeId;

    protected Date firstPaymentDateLimit;

    protected int mtgProdId;

    protected double postedRate;

    protected int pricingRateInventoryId;

    protected int repaymentTypeId;

    protected Date teaserMaturityDate;

    protected double teaserNetInterestRate;

    protected double teaserPiAmount;

    protected double teaserRateInterestSaving;

    protected int institutionProfileId;

    protected ComponentPK pk;
    

    /**
     * <p>
     * Description:Default constructor.
     * </p>
     * @version 1.0 Initial Version
     */

    public Component() throws RemoteException, FinderException
    {

    }

    /**
     * @param srk
     * @throws RemoteException
     * @throws FinderException
     */
    public Component(SessionResourceKit srk) throws RemoteException,
            FinderException
    {
        super(srk);
    }

    /**
     * <p>
     * Description: Creates the Component entity object by taking session
     * resource kit and calc monitor objects and calls the super class
     * constructor by passing the session resource kit and calc monitor objects.
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @param srk
     *            stores the session level information.
     * @param dcm
     *            runs the calculation of this Component entity.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public Component(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException
    {
        super(srk, dcm);
    }

    /**
     * @param srk
     * @param id
     * @param copyId
     * @throws RemoteException
     * @throws FinderException
     */
    public Component(SessionResourceKit srk, int id, int copyId)
            throws RemoteException, FinderException
    {
        super(srk);

        pk = new ComponentPK(id, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * <p>
     * Description: Creates the Component entity object by taking session
     * resource kit and calc monitor objects, component id and the copy id of
     * the existing Component.Calls the super class constructor and creates the
     * primary key for the Component entity and calls the findByPrimaryKey()
     * method.
     * </p>
     * @version 1.0 Initial Version
     * @param srk
     *            stores the session level information.
     * @param componentId
     *            component id value passed to the entity PK class.
     * @param dcm
     *            runs the calculation of this Component entity.
     * @param copyId
     *            copy id of the Component.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     *             (non-java doc)
     * @see com.basis100.deal.entity.Component#findByPrimaryKey(com.basis100.deal.pk.ComponentPK )
     */
    public Component(SessionResourceKit srk, CalcMonitor dcm, int componentId,
            int copyId) throws RemoteException, FinderException
    {
        super(srk, dcm);

        pk = new ComponentPK(componentId, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * @param key
     * @throws Exception
     */
    private void setPropertiesFromQueryResult(int key) throws Exception
    {
        this.setComponentId(jExec.getInt(key, "COMPONENTID"));
        this.setCopyId(jExec.getInt(key, "COPYID"));
        this.setAdditionalInformation(jExec.getString(key,
                "ADDITIONALINFORMATION"));
        this.setDealId(jExec.getInt(key, "DEALID"));
        this.setComponentTypeId(jExec.getInt(key, "COMPONENTTYPEID"));
        this.setFirstPaymentDateLimit(jExec.getDate(key,
                "FIRSTPAYMENTDATELIMIT"));
        this.setMtgProdId(jExec.getInt(key, "MTGPRODID"));
        this.setPostedRate(jExec.getDouble(key, "POSTEDRATE"));
        this.setPricingRateInventoryId(jExec.getInt(key,
                "PRICINGRATEINVENTORYID"));
        this.setRepaymentTypeId(jExec.getInt(key, "REPAYMENTTYPEID"));
        this.setTeaserMaturityDate(jExec.getDate(key, "TEASERMATURITYDATE"));
        this.setTeaserNetInterestRate(jExec
                .getDouble(key, "TEASERNETINTERESTRATE"));
        this.setTeaserPiAmount(jExec.getDouble(key, "TEASERPIAMOUNT"));
        this.setTeaserRateInterestSaving(jExec.getDouble(key,
                "TEASERRATEINTERESTSAVING"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    /**
     * @param pk
     * @return	
     * @throws RemoteException
     * @throws FinderException
     */
    public Component findByPrimaryKey(ComponentPK pk) throws RemoteException,
            FinderException
    {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from Component where componentId = "
                + pk.getId() + " AND copyId = " + pk.getCopyId();

        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Component Entity: @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                if(getSilentMode() == false)
                    logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException(
                    "Component Entity - findByPrimaryKey() exception");
            if(getSilentMode() == false){
                logger.error(fe.getMessage());
                logger.error("finder sql: " + sql);
                logger.error(e);
            }

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * @param copyId
     * @return
     * @throws CreateException
     */
    private ComponentPK createPrimaryKey(int copyId) throws CreateException
    {

        String sql = "Select Componentseq.nextval from dual";
        int id = -1;

        try
        {
            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);
            if (id == -1)
                throw new Exception();
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "Component Entity create() exception getting ComponentId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }

        return new ComponentPK(id, copyId);
    }

    /**
     * @param dealId
     * @param copyId
     * @param componentTypeId
     * @param mtgProdId
     * @param pricingRateInventoryId
     * @param repaymentTypeId
     * @return
     * @throws RemoteException
     * @throws CreateException
     */
    public Component create(int dealId, int copyId, int componentTypeId,int mtgProdId)
            throws RemoteException, CreateException
    {
        // create the primary key
        pk = createPrimaryKey(copyId);
        // SQL
        String sql = "Insert into Component( componentId, copyId, institutionProfileId, dealId, componentTypeId, mtgProdId) Values ( "
                + pk.getId()
                + ","
                + pk.getCopyId()
                + ","
                + srk.getExpressState().getDealInstitutionId()
                + ","
                + dealId
                + ","
                + componentTypeId
                + ","
                + mtgProdId + ")";

        // Execute
        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the create
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "Component Entity - Component - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        if (dcm != null)
            // dcm.inputEntity(this);
            dcm.inputCreatedEntity(this);

        return this;
    }

    /**
     * (non-Javadoc)
     * @version 1.0 Initial version
     * @version 1.1 10-Jun-2008 XS_11.11 & XS_11.6
     * @see com.basis100.deal.entity.DealEntity#removeSons()
     */
    @Override
    protected void removeSons() throws Exception
    {
        // look up children
        /***********************************************************************
         * *************MCM Impl team changes Starts - XS_11.11
         **********************************************************************/
        /**
         * MCM Impl Team. Important -- XT_11.11 -- 10-Jun-2008.
         * <p>
         * Created new Primary Key classes for all the component entities .
         * Constructor will return respective object based on the srk,
         * componentId and CopyId. So no need to pass the PK to the ejbRemove
         * and added if conditions to check component type.
         * </p>
         */
        try {
            if (this.componentTypeId == Mc.COMPONENT_TYPE_CREDITCARD) {
    
                ComponentCreditCard componentCC = new ComponentCreditCard(srk,dcm,
                        this.componentId, this.copyId);
                componentCC.ejbRemove(false);
    
            } else if (this.componentTypeId == Mc.COMPONENT_TYPE_LOAN) {
    
                ComponentLoan componentLoan = new ComponentLoan(srk,dcm,
                        this.componentId, this.copyId);
                componentLoan.ejbRemove(false);
    
            } else if (this.componentTypeId == Mc.COMPONENT_TYPE_LOC) {
    
                ComponentLOC componentLOC = new ComponentLOC(srk,dcm, this.componentId,
                        this.copyId);
                componentLOC.ejbRemove(false);
    
            }
            if (this.componentTypeId == Mc.COMPONENT_TYPE_MORTGAGE) {
                ComponentMortgage componentMrtg = new ComponentMortgage(srk,dcm,
                        this.componentId, this.copyId);
                componentMrtg.ejbRemove(false);
    
            } else if (this.componentTypeId == Mc.COMPONENT_TYPE_OVERDRAFT) {
                ComponentOverdraft componentOver = new ComponentOverdraft(srk,dcm,
                        this.componentId, this.copyId);
                componentOver.ejbRemove(false);
            }
            /** *************MCM Impl team changes ends - XS_11.11****************** */
        // if children don't exist, then just ignore
        } catch (FinderException fe) {
            logger.trace("removeSons@Component: children don't exist with componentType: ComponentId = " + componentId );
        }
    }

    /**
     * @return the componentId
     */
    public int getComponentId()
    {
        return componentId;
    }

    /**
     * @param componentId
     *            the componentId to set
     */
    public void setComponentId(int componentId)
    {
        this.testChange("componentId", componentId);
        this.componentId = componentId;
    }

    /**
     * @return the copyId
     */
    public int getCopyId()
    {
        return copyId;
    }

    /**
     * @param copyId
     *            the copyId to set
     */
    public void setCopyId(int copyId)
    {
        this.testChange("copyId", copyId);
        this.copyId = copyId;
    }

    /**
     * @return the additionalInformation
     */
    public String getAdditionalInformation()
    {
        return additionalInformation;
    }

    /**
     * @param additionalInformation
     *            the additionalInformation to set
     */
    public void setAdditionalInformation(String additionalInformation)
    {
        this.testChange("additionalInformation", additionalInformation);
        this.additionalInformation = additionalInformation;
    }

    /**
     * @return the dealId
     */
    public int getDealId()
    {
        return dealId;
    }

    /**
     * @param dealId
     *            the dealId to set
     */
    public void setDealId(int dealId)
    {
        this.testChange("dealId", dealId);
        this.dealId = dealId;
    }

    /**
     * @return the componentTypeId
     */
    public int getComponentTypeId()
    {
        return componentTypeId;
    }

    /**
     * @param componentTypeId
     *            the componentTypeId to set
     */
    public void setComponentTypeId(int componentTypeId)
    {
        this.testChange("componentTypeId", componentTypeId);
        this.componentTypeId = componentTypeId;
    }

    /**
     * @return the firstPaymentDateLimit
     */
    public Date getFirstPaymentDateLimit()
    {
        return firstPaymentDateLimit;
    }

    /**
     * @param firstPaymentDateLimit
     */

    /**
     * <p>
     * Description: Sets the totalPaymentAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 12-Jun-2008 - Modified the testChange attribute name
     *          from 'FirstPaymentDateLimit' to 'firstPaymentDateLimit'
     * @param firstPaymentDateLimit
     *            the firstPaymentDateLimit to set
     */
    public void setFirstPaymentDateLimit(Date firstPaymentDateLimit)
    {
        this.testChange("firstPaymentDateLimit", firstPaymentDateLimit);
        this.firstPaymentDateLimit = firstPaymentDateLimit;
    }

    /**
     * @return the mtgProdId
     */
    public int getMtgProdId()
    {
        return mtgProdId;
    }

    /**
     * @param mtgProdId
     *            the mtgProdId to set
     */
    public void setMtgProdId(int mtgProdId)
    {
        this.testChange("mtgProdId", mtgProdId);
        this.mtgProdId = mtgProdId;
    }

    /**
     * @return the postedRate
     */
    public double getPostedRate()
    {
        return postedRate;
    }

    /**
     * @param postedRate
     *            the postedRate to set
     */
    public void setPostedRate(double postedRate)
    {
        this.testChange("postedRate", postedRate);
        this.postedRate = postedRate;
    }

    /**
     * @return the prisingRateInventoryId
     */
    public int getPricingRateInventoryId()
    {
        return pricingRateInventoryId;
    }

    /**
     * @param prisingRateInventoryId
     *            the prisingRateInventoryId to set
     */
    public void setPricingRateInventoryId(int pricingRateInventoryId)
    {
        this.testChange("pricingRateInventoryId", pricingRateInventoryId);
        this.pricingRateInventoryId = pricingRateInventoryId;
    }

    /**
     * @return the repaymentTypeId
     */
    public int getRepaymentTypeId()
    {
        return repaymentTypeId;
    }

    /**
     * @param repaymentTypeId
     *            the repaymentTypeId to set
     */
    public void setRepaymentTypeId(int repaymentTypeId)
    {
        this.testChange("repaymentTypeId", repaymentTypeId);
        this.repaymentTypeId = repaymentTypeId;
    }

    /**
     * @return the teaserMaturityDate
     */
    public Date getTeaserMaturityDate()
    {
        return teaserMaturityDate;
    }

    /**
     * @param teaserMaturityDate
     *            the teaserMaturityDate to set
     */
    public void setTeaserMaturityDate(Date teaserMaturityDate)
    {
        this.testChange("teaserMaturityDate", teaserMaturityDate);
        this.teaserMaturityDate = teaserMaturityDate;
    }

    /**
     * @return the teaserInterestRate
     */
    public double getTeaserNetInterestRate()
    {
        return teaserNetInterestRate;
    }

    /**
     * @param teaserInterestRate
     *            the teaserInterestRate to set
     */
    public void setTeaserNetInterestRate(double teaserInterestRate)
    {
        this.testChange("teaserNetInterestRate", teaserInterestRate);
        this.teaserNetInterestRate = teaserInterestRate;
    }

    /**
     * @return the teaserPiaAmount
     */
    public double getTeaserPiAmount()
    {
        return teaserPiAmount;
    }

    /**
     * <p>
     * Description: Sets the totalPaymentAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 12-Jun-2008 - Modified the testChange attribute name
     *          from 'teaserPiaAmount' to 'teaserPiAmount'
     * @param teaserPiaAmount
     *            the teaserPiaAmount to set
     */
    public void setTeaserPiAmount(double teaserPiaAmount)
    {
        this.testChange("teaserPiAmount", teaserPiaAmount);
        this.teaserPiAmount = teaserPiaAmount;
    }

    /**
     * @return the teaserRateInterestSaving
     */
    public double getTeaserRateInterestSaving()
    {
        return teaserRateInterestSaving;
    }

    /**
     * @param teaserRateInterestSaving
     *            the teaserRateInterestSaving to set
     */
    public void setTeaserRateInterestSaving(double teaserRateInterestSaving)
    {
        this.testChange("teaserRateInterestSaving", teaserRateInterestSaving);
        this.teaserRateInterestSaving = teaserRateInterestSaving;
    }

    /**
     * @return the institutionProfileId
     */
    public int getInstitutionProfileId()
    {
        return institutionProfileId;
    }

    /**
     * @param institutionProfileId
     *            the institutionProfileId to set
     */
    public void setInstitutionProfileId(int institutionProfileId)
    {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    /**
     * <p>
     * Description: Performs updates on the Component entity
     * </p>
     * 
     * @version 1.0 XS_11.11 10-Jun-2008 Initial Version
     * @return int returns the no of rows updated.
     */
    protected int performUpdate() throws Exception
    {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0)
        {
            dcm.inputEntity(this);
        }

        return ret;
    }

    /**
     * <p>
     * Description: Returns the collection of the components associated to the
     * deal
     * </p>
     * 
     * @param DealPK -
     *            Deal Primary key
     * @return Collection - collection of component Objects
     * @throws Exception
     * @version 1.0 XS_11.11 09-Jun-2008 - Initial Version
     * @version 1.6 July 15, 2008 XS_2.39 added order componentTypId before compnentId  
     * @version 1.7 July 22, 2008 XS 2.39 modified order as MTG->LOC->CreditCard->Loan->Overdraft
     * @version 1.9 July 31, 2008 fixed SQL.
     */
    public Collection<Component> findByDeal(DealPK pk) throws Exception
    {
        Collection<Component> components = new ArrayList<Component>();
        String where = pk.getWhereClause();
        StringBuffer sqlBuffer = new StringBuffer ("Select * from ( ")
            .append(" select 1 seq, component.* from component  ")
            .append(where).append(" and componentTypeId = ")
            .append(Mc.COMPONENT_TYPE_MORTGAGE)
            .append(" union ")
            .append(" select 2 seq, component.* from component  ")
            .append(where).append(" and componentTypeId = ")
            .append(Mc.COMPONENT_TYPE_LOC)
            .append(" union ")
            .append(" select 3 seq, component.* from component  ")
            .append(where).append(" and componentTypeId = ")
            .append(Mc.COMPONENT_TYPE_CREDITCARD)
            .append(" union ")
            .append(" select 4 seq, component.* from component  ")
            .append(where).append(" and componentTypeId = ")
            .append(Mc.COMPONENT_TYPE_LOAN)
            .append(" union ")
            .append(" select 5 seq, component.* from component  ")
            .append(where).append(" and componentTypeId = ")
            .append(Mc.COMPONENT_TYPE_OVERDRAFT)
            .append(" ) order by seq, componentId ");
        
        String sql = sqlBuffer.toString();
            
        try
        {

            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
            	//FXP23331, MCM/4.1GR, Jan 13, 2009 - start
                //Component component = new Component(srk);
                Component component = new Component(srk, dcm);
                //FXP23331, MCM/4.1GR, Jan 13, 2009 - end
                component.setPropertiesFromQueryResult(key);
                component.pk = new ComponentPK(component.getComponentId(),
                        component.getCopyId());
                components.add(component);
            }

            jExec.closeData(key);

        }
        catch (Exception e)
        {
            Exception fe = new Exception(
                    "Exception caught while getting Deal::Components");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return components;
    }

    /**
     * <p>
     * Description: Returns the pk object associated with this entity
     * </p>
     * 
     * @version 1.0 XS_11.11 Initial Version
     * @return IEntityBeanPK Component entity pk object
     */
    public IEntityBeanPK getPk()
    {
        return (IEntityBeanPK) this.pk;
    }

    /**
     * <p>
     * Description: Returns the entity table name
     * </p>
     * 
     * @version 1.0 XS_11.11 11-Jun-2008 Initial Version
     * @return String - table name of the entity
     */
    public String getEntityTableName()
    {
        return "Component";
    }

    /**
     * <p>
     * Description: Returns the class id of the Component entity
     * </p>
     * 
     * @version 1.0 XS_11.11 11-Jun-2008 - Initial Version
     * @return int - classid of Component entity.
     */
    public int getClassId()
    {

        return ClassId.COMPONENT;
    }
 
    /**
     *   <p>getChildren</p>
     *   <p>Description: gets the children of Component table</p>
     *
     *   @return a List of component children;
     */
    public List getChildren() throws Exception {

    	List children = new ArrayList();
    	Object child = null;

    	switch (getComponentTypeId()) {
    	case Mc.COMPONENT_TYPE_MORTGAGE:
    		child = getComponentMortgage();
    		break;
    	case Mc.COMPONENT_TYPE_LOC: 
    		child = getComponentLOC();
    		break;
    	case Mc.COMPONENT_TYPE_CREDITCARD: 
    		child = getComponentCreditCard();
    		break;
    	case Mc.COMPONENT_TYPE_LOAN: 
    		child = getComponentLoan();
    		break;
    	case Mc.COMPONENT_TYPE_OVERDRAFT: 
    		child = getComponentOverdraft();
    		break;
    	default: 
    		break;
    	}
    	if (child != null)
    		children.add(child);

    	return children;
    }
    
    /**
     *   <p>ComponentLOC</p>
     *   <p>Description: gets the componentLOC associated to the Component
     *
     *   @return componentLOCs;
     *   @version 1.1: modified to return only one ComponentLOC (MCM Team)
     */
    public ComponentLOC getComponentLOC(){ 
        
        try {
        return (new ComponentLOC(srk, dcm).findByPrimaryKey(
                new ComponentLOCPK(getComponentId(), getCopyId())));
        } catch (Exception e) {
            return null;
        }

    }
    /**
     *   <p>ComponentMortgage</p>
     *   <p>Description: gets the ComponentMortgage associated to the Component
     *
     *   @return componentMortgages;
     *   @version 1.1: modified to return only one ComponentMortgage (MCM Team)
     */
    public ComponentMortgage getComponentMortgage(){
        try {
            return (new ComponentMortgage(srk, dcm).findByPrimaryKey(
                    new ComponentMortgagePK(getComponentId(), getCopyId())));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *   <p>componentCreditCard</p>
     *   <p>Description: gets the componentCreditCard associated to the Component
     *
     *   @return componentCreditCard;
     *   @version 1.0: XS_2.36
     */
    public ComponentCreditCard getComponentCreditCard(){
        try {
            return (new ComponentCreditCard(srk).findByPrimaryKey(
                    new ComponentCreditCardPK(getComponentId(), getCopyId())));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *   <p>ComponentLoan</p>
     *   <p>Description: gets the ComponentLoan associated to the Component
     *
     *   @return ComponentLoan;
     *   @version 1.0: XS_2.36
     */
    public ComponentLoan getComponentLoan() {
        try {
            return (new ComponentLoan(srk).findByPrimaryKey(
                    new ComponentLoanPK(getComponentId(), getCopyId())));
        } catch (Exception e) {
            return null;
        }

    }

    /**
     *   <p>ComponentOverdraft</p>
     *   <p>Description: gets the ComponentOverdraft associated to the Component
     *
     *   @return ComponentOverdraft;
     *   @version 1.0: XS_2.36
     */
    public ComponentOverdraft getComponentOverdraft() {
        try {
            return (new ComponentOverdraft(srk).findByPrimaryKey(
                    new ComponentOverdraftPK(getComponentId(), getCopyId())));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *   <p>isAuditable</p>
     *   <p>Descriptio: gets if this entity is auditable or not
     *   
     *   @return true
     */
    public boolean isAuditable() {

        return true;
    }
    /**
     * <p>
     * Returns the pricingProfileId associated with the PricingRateInventory associated with this component
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * @return the PricingProfile
     */
    public int getActualPricingProfileId()  throws Exception
    {
        PricingProfile pp = new PricingProfile(srk);

        pp = pp.findByPricingRateInventory(this.pricingRateInventoryId);

        return pp.getPricingProfileId();
    }
    /**
     * <p>
     * getS the PricingRateInventory associated with this entity
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * @return the PricingRateInventory
     */
    public PricingRateInventory getPricingRateInventory() throws Exception
    {
    	//MtgProd mtgProd=new MtgProd(srk,dcm,this.getMtgProdId());
        return new PricingRateInventory(srk).findByPricingRateInventory(this.getPricingRateInventoryId());
    }
    /**
     * Returns PricingProfile of deal
     * @return the pricingProfile associated with the PricingRateInventory associated with this component
     */
    public PricingProfile getActualPricingProfile()  throws Exception
    {
        PricingProfile pp = new PricingProfile(srk);
        pp = pp.findByPricingRateInventory(this.getPricingRateInventoryId());

        return pp;
    }
    /**
     *   getS the PricingRateInventory associated with this entity by Date
     *
     *   @return a PricingRateInventory;
     */
    public PricingRateInventory getPriByDate(Date d) throws Exception
    {
        return new PricingRateInventory(srk).findByComponentAndDate(this, d);
    }
    /**
     *   getS the PricingRateInventory for the best rate (minimum rate)
     *            between the dates
     *
     *   @return a PricingRateInventory;
     */
    public PricingRateInventory getPriByMinRate(Date startDate) throws Exception
    {
        return new PricingRateInventory(srk).findPriByMinRate(this, startDate);
    }
    /**
     *   getS the PricingRateInventory for the best rate (minimum rate)
     *            between the dates
     *
     *   @return a PricingRateInventory;
     */
    public PricingRateInventory getPriByMinRate(Date startDate, Date endDate) throws Exception
    {
        return new PricingRateInventory(srk).findPriByMinRate(this, startDate, endDate);
    }

    /**
     * 
     * @version 1.11 fixed for artf771147 : update transaction history
     */
    protected EntityContext getEntityContext() throws RemoteException {
        EntityContext ctx = this.entityContext;
        try {
            ctx.setContextText("Component");
            int langId = srk.getLanguageId();
            int instId = srk.getExpressState().getDealInstitutionId();
            String description = BXResources.getPickListDescription(instId,
                    "COMPONENTTYPE", this.getComponentTypeId(), langId);
            ctx.setContextSource(description);
            ctx.setApplicationId(this.dealId);

        } catch (Exception e) {
            if (e instanceof FinderException) {
                logger
                        .warn("Deal.getEntityContext FinderException caught. DealId= "
                                + this.dealId);
                return ctx;
            }
            throw (RemoteException) e;
        }
        return ctx;
    } 
    /**
       * Bugfix:FXP23183
       * This method is used to get the prime indexName for the given componentId
       * 
       * @param parent the componentId,copyId
       * @param name the name
       * @version 1.0 30-10-2008 NST_ID Initial version
     */
    public String getPrimeIndex(int componentId,int copyId) throws RemoteException, FinderException{
        String result="";
        String sql = new String("SELECT PRIMEINDEXNAME "
        + " FROM PricingRateInventory,Component,primeindexrateinventory,primeindexrateprofile  WHERE " 
        + " Component.pricingrateinventoryid=PricingRateInventory.pricingrateinventoryid "
        + " AND component.institutionprofileid=PricingRateInventory.institutionprofileid "
        + " AND PricingRateInventory.primeindexrateinventoryid = primeindexrateinventory.primeindexrateinventoryid "
        + " AND PricingRateInventory.institutionprofileid=primeindexrateinventory.institutionprofileid "
        + " AND primeindexrateinventory.primeindexrateprofileid=primeindexrateprofile.primeindexrateprofileid "
        + " AND primeindexrateinventory.institutionprofileid=primeindexrateprofile.institutionprofileid "
        + " AND Component.componentid= "+ componentId + " AND component.copyid = "+ copyId );
       
        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                result=jExec.getString(key, 1);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
              "Component Entity - getPrimeIndex() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

        }
        return result;
    }
    
}
