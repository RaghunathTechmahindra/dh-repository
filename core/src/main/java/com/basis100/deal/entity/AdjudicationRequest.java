package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationRequestPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class AdjudicationRequest extends DealEntity{
	protected int	requestId;
	protected int	scenarioNumber;
	protected int	noOfApplicants;
	protected int	instProfileId;

	private AdjudicationRequestPK pk;

	public AdjudicationRequest(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public AdjudicationRequest(SessionResourceKit srk, int id) throws RemoteException, FinderException
	{
		super(srk);
	    pk = new AdjudicationRequestPK(id);
	    findByPrimaryKey(pk);
	}

	/**  search for AdjudicationRequest record using primary key
	*   @return a AdjudicationRequest */
	public AdjudicationRequest findByPrimaryKey(AdjudicationRequestPK pk) throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from AdjudicationRequest " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "AdjudicationRequest: @findByPrimaryKey(), key= " + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException("AdjudicationRequest Entity - findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}

    public Collection findByRequest(RequestPK pk) throws Exception
    {
      Collection adjrequests = new ArrayList();

      String sql = "Select * from AdjudicationRequest " ;
            sql +=  pk.getWhereClause();
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              AdjudicationRequest adjrequest = new AdjudicationRequest(srk);
              adjrequest.setPropertiesFromQueryResult(key);
              adjrequest.pk = new AdjudicationRequestPK(adjrequest.getRequestId());
              adjrequests.add(adjrequest);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Request::AdjudicationRequest");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return adjrequests;
    }
	
    public Collection getSAMRequests() throws Exception
    {
      return new SAMRequest(srk).findByRequest(this.pk);
    }	

    /**  sets properties for AdjudicationRequest */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	  	this.setRequestId(jExec.getInt(key, "requestId"));
	  	this.setScenarioNumber(jExec.getInt(key, "scenarioNumber"));
	  	this.setNoOfApplicants(jExec.getInt(key, "noOfApplicants"));
	  	this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	public String getEntityTableName()
	{
	    return "AdjudicationRequest"; 
	}

	/**  gets the pk associated with this entity
	 *   @return a AdjudicationRequestPK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}	

	//  get/set for the requestId associated with this entity
	public int getRequestId()
	{ 
		return this.requestId;
	}

	public void setRequestId(int id)
	{
		this.testChange ("requestId", id);
		this.requestId = id;
	}

	//  get/set for the scenarioNumber associated with this entity
	public int getScenarioNumber()
	{ 
		return this.scenarioNumber;
	}

	public void setScenarioNumber(int id)
	{
		this.testChange ("scenarioNumber", id);
		this.scenarioNumber = id;
	}

	//  get/set for the noOfApplicants associated with this entity
	public int getNoOfApplicants()
	{ 
		return this.noOfApplicants;
	}

	public void setNoOfApplicants(int id)
	{
		this.testChange ("noOfApplicants", id);
		this.noOfApplicants = id;
	}

	/**
	 * creates AdjudicationRequest record into database minimum fields
	 * 
	 * @return a AdjudicationRequest
	 * @deprecated This method is not right. It seems not functioning since it
	 *             misses the copyId from the PK.
	 */
	public AdjudicationRequest create(RequestPK rpk)throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into AdjudicationRequest(" + rpk.getName() +  ") Values ( " + rpk.getId() + ")";
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new AdjudicationRequestPK(rpk.getId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("AdjudicationRequest Entity - AdjudicationRequest - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
		return this;
	}

	/**
	 * creates AdjudicationRequest record into database
	 * 
	 * @return a AdjudicationRequest
	 * @deprecated This method is not right. It seems not functioning since it
	 *             misses the copyId from the PK.
	 */
	public AdjudicationRequest create(RequestPK rpk,
										int scenarionum, 
										int numofapp)throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into AdjudicationRequest(" + rpk.getName() +  "," +
		"ScenarioNumber, NoOfApplicants) Values ( " + rpk.getId() + "," + scenarionum + "," + numofapp + ")";
		
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new AdjudicationRequestPK(rpk.getId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("AdjudicationRequest Entity - AdjudicationRequest - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
		return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof AdjudicationRequest)
		{
			AdjudicationRequest oa = (AdjudicationRequest) o;
			if  (  this.requestId == oa.getRequestId ()  )
				return true;
		}
		return false;
	}
	
    public Collection findByDeal(DealPK pk) throws Exception
    {
      Collection adjrequests = new ArrayList();

      String sql = "Select ar.* from AdjudicationRequest ar, Request r " ;
            sql += "where ar.requestId = r.requestId and r.dealId = ";
            sql += pk.getId();
            sql += " order by r.RequestId desc";
      try
      {

            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
            	AdjudicationRequest adjrequest = new AdjudicationRequest(srk,jExec.getInt(key,"requestId"));
            	adjrequest.setPropertiesFromQueryResult(key);
            	adjrequest.pk = new AdjudicationRequestPK(adjrequest.getRequestId());
            	adjrequests.add(adjrequest);
            }

            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::AdjudicationResponse");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }
        return adjrequests;
    }

    public AdjudicationRequest findByLastRecForThisDeal(DealPK pk) throws Exception
    {
        {
        	Collection c = null;
        	AdjudicationRequest ar = null;

        	c = findByDeal(pk);
        	if (c.size() > 0)
        	{
        		ar = (AdjudicationRequest)c.toArray()[0];
        	}
        	return ar;
        }    	
    }    
    
    private void setPk(AdjudicationRequestPK pk) {
        this.pk = pk;
      }
    
    /**
     * This object should only be cloned with a known new PK.
     */
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    
    public Object clone(RequestPK newPk) {
        
        AdjudicationRequest clonedRequest = null;
        
        try {
            // first create teh request to generate the new PK
            clonedRequest = new AdjudicationRequest(srk);
            clonedRequest = clonedRequest.create(newPk);
                      
            clonedRequest = (AdjudicationRequest) super.clone();
            
            clonedRequest.setPk(new AdjudicationRequestPK(newPk.getId()));
            clonedRequest.setRequestId(newPk.getId());
           
            clonedRequest.doPerformUpdateForce();            
        }
        catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in AdjudicationRequest.clone().  Trying to clone requestid = " + this.requestId + " " + ex.getMessage());
        }
        
        return clonedRequest;        
    }

    protected void removeSons() throws Exception
    {

 	   // -------- start of all SAMRequest -------------
 	   Collection areqs = this.getSAMRequests();
 	   Iterator itReq = areqs.iterator () ;
 	   while ( itReq.hasNext () )
 	   {
 		  SAMRequest areq = (SAMRequest) itReq.next () ;
 		   if ( itReq.hasNext () )
 		   {
 			   areq.ejbRemove ( false );
 		   }
 	       else{
 	    	   areq.ejbRemove ( true );
 	      }
 	   }
 	   // -------- end of all SAMRequest -------------
    }

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}   
}
