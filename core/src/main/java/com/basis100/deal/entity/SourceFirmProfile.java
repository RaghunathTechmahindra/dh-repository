package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
/**
 * @version 1.0 - Existing version
 * @version 1.1 | MCM Impl Team. | 21-Aug-2008 | Modified createFirmGroupAssoc() method
 *          to insert a comma in 'Insert' statement.
 *                                 
 *  
 */

public class SourceFirmProfile extends DealEntity
{
   protected String  sourceFirmName;
   protected int     systemTypeId;
   protected int     sourceFirmProfileId;
   protected int     contactId;
   protected int     profileStatusId;
   protected String  sfShortName;
   protected String  sfBusinessId;
   protected String  alternativeId;
   protected int     sfMOProfileId;
   protected String  sourceFirmCode;
   protected int     institutionProfileId;
   protected String  SFLicenseRegistrationNumber;

   private SourceFirmProfilePK pk;

   public SourceFirmProfile() {}

   public SourceFirmProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

   public SourceFirmProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);
      pk = new SourceFirmProfilePK(id);
      findByPrimaryKey(pk);
   }

   public SourceFirmProfile findByPrimaryKey(SourceFirmProfilePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from SourceFirmProfile where " + pk.getName()+ " = '" + pk.getId() + "'";
      boolean gotRecord = false;
      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

   //********** WARNING ***********
   // no reference was found
   // can it expexct only one record?
   // contact has copyid, but not sourceFirmProfile. how copyid will be?
   // midori Sep 10, 2007
   public SourceFirmProfile findByContactPK(ContactPK pk) throws RemoteException, FinderException
    {
      String sql = "Select * from SourceFirmProfile where " + pk.getName()+ " = '" + pk.getId() + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "SourceFirmProfile Entity: @findByContactPK, key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("SourceFirmProfile Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

   //********** WARNING ***********
   // no reference was found
   // can it expexct only one record?
   // name is duplicated, i.e. more than sourceFirmProfile with same name
   // midori Sep 10, 2007
   /**
    * Attempts to find the record that corresponds to the description
    *
    */
   public SourceFirmProfile findByName(String name) throws RemoteException, FinderException
   {

      String sql = "Select * from sourceFirmProfile where SourceFirmName = '" + name + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               this.pk = new SourceFirmProfilePK(this.sourceFirmProfileId);

               break;

          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "SourceFirmProfile Entity: @findByName = " + name;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }

      return this;
   }

  //-- FXLink Phase II --//
  // Change to use SOB.sourceFirmProfileId instead of SourceToFirmAssoc table
  // By Billy 06Nov2003
  public Collection findByAssociatedSourceOfBusiness(SourceOfBusinessProfilePK pk)
  {
      List retList = new ArrayList();

      StringBuffer buf = new StringBuffer("Select sf.* from sourcefirmProfile sf, sourceofbusinessprofile sob where");
      buf.append(" (sob.sourceofbusinessprofileid = ");
      buf.append(pk.getId());
      buf.append(" and sob.sourceFirmProfileId = sf.sourceFirmProfileId");
      buf.append(")");

      try
      {
          int key = jExec.execute(buf.toString());

          for (; jExec.next(key); )
          {
             SourceFirmProfile profile = new SourceFirmProfile(srk);
             profile.setPropertiesFromQueryResult(key);
             profile.pk = new SourceFirmProfilePK(profile.getSourceFirmProfileId());

             retList.add(profile);
          }

          jExec.closeData(key);

        }
        catch (Exception e)
        {
            logger.error("Error in finder - SourceToFirm : " + buf.toString());
            logger.error(e);
            return retList;
        }

      return retList;
  }

  public Collection findBySFBusinessIdAndSystemType(String parBusinessId, int pSystemType)
  {
    List retList = new ArrayList();
    StringBuffer buf = new StringBuffer("Select * from sourcefirmProfile where SFBUSINESSID = \'");
    buf.append(parBusinessId).append("\'");
    buf.append(" and  systemtypeid = " + pSystemType);

    try
    {
      int key = jExec.execute(buf.toString());
      for (; jExec.next(key); )
      {
        SourceFirmProfile profile = new SourceFirmProfile(srk);
        profile.setPropertiesFromQueryResult(key);
        profile.pk = new SourceFirmProfilePK(profile.getSourceFirmProfileId());

        retList.add(profile);
      }
      jExec.closeData(key);
    }
    catch (Exception e)
    {
        logger.error("Error in finder - findBySFBusinessIdAndSystemType : " + buf.toString());
        logger.error(e);
        return retList;
    }
    return retList;
  }


  public Collection findBySFBusinessId(String parBusinessId)
  {
    List retList = new ArrayList();
    StringBuffer buf = new StringBuffer("Select * from sourcefirmProfile where SFBUSINESSID = \'");
    buf.append(parBusinessId).append("\'");

    try
    {
      int key = jExec.execute(buf.toString());
      for (; jExec.next(key); )
      {
        SourceFirmProfile profile = new SourceFirmProfile(srk);
        profile.setPropertiesFromQueryResult(key);
        profile.pk = new SourceFirmProfilePK(profile.getSourceFirmProfileId());

        retList.add(profile);
      }
      jExec.closeData(key);
    }
    catch (Exception e)
    {
        logger.error("Error in finder - findBySourceAndDealSystemType : " + buf.toString());
        logger.error(e);
        return retList;
    }
    return retList;
  }

  //-- FXLink Phase II --//
  // Change to use SOB.sourceFirmProfileId instead of SourceToFirmAssoc table
  // By Billy 06Nov2003
  public Collection findBySourceAndDealSystemType(SourceOfBusinessProfilePK pk, DealPK dpk)
  {
      List retList = new ArrayList();

      StringBuffer buf = new StringBuffer("Select sf.* from sourcefirmProfile sf, sourceofbusinessprofile sob where");
      buf.append(" sob.sourceFirmProfileId = sf.sourceFirmProfileId");
      buf.append(" AND sob.sourceofbusinessprofileid = ");
      buf.append(pk.getId()).append(" AND systemTypeId = ");
      buf.append("(Select systemTypeid from deal where dealid = ").append(dpk.getId());
      buf.append(" and copyId = ").append(dpk.getCopyId()).append(")") ;

      try
      {
          int key = jExec.execute(buf.toString());

          for (; jExec.next(key); )
          {
             SourceFirmProfile profile = new SourceFirmProfile(srk);
             profile.setPropertiesFromQueryResult(key);
             profile.pk = new SourceFirmProfilePK(profile.getSourceFirmProfileId());

             retList.add(profile);
          }

          jExec.closeData(key);

        }
        catch (Exception e)
        {
            logger.error("Error in finder - findBySourceAndDealSystemType : " + buf.toString());
            logger.error(e);
            return retList;
        }

      return retList;
  }

   //-- FXLink Phase II --//
  public SourceFirmProfile findBySystemTypeAndSFCode( int pSystemType, String pSourceFirmCode ) throws FinderException
  {
      StringBuffer buf = new StringBuffer("SELECT * from sourceFirmProfile WHERE ");
      buf.append(" systemtypeid = ").append(pSystemType).append(" AND ");
      buf.append(" sourcefirmcode = \'").append(pSourceFirmCode).append("\'");

      String sql = buf.toString();

      boolean gotRecord = false;
      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              this.pk = new SourceFirmProfilePK(this.sourceFirmProfileId);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "SourceFirmProfile not found - findBySystemTypeAndSFCode : Type=" + pSystemType + " SourceFirmCode=" + pSourceFirmCode;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Error in finder - findBySystemTypeAndSFCode : " + buf.toString());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        return this;
  }

  public void createFirmUserAssoc
    (int sourceUserProfileId, Date effectiveDate)throws Exception
  {
      String sql = "Insert into sourcefirmtouserassoc( SourceFirmProfileId, "
        +"userProfileId, EffectiveDate, institutionProfileId) Values ( " 
        + pk.getId() + "," + sourceUserProfileId + "," 
        + sqlStringFrom(effectiveDate) + ", " 
        + srk.getExpressState().getDealInstitutionId() + " )";
      try
      {
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceFirmProfile Entity - createFirmUserAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }
  public void updateFirmUserAssoc(int userProfileId, Date effectiveDate ) throws Exception
  {
      String sql = "Select userProfileId, effectiveDate from sourcefirmtouserassoc where SourceFirmProfileId = " +
                 pk.getId();
      int  prevProfileId = 0;
      Date prevDate = null;
      boolean  gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for( ;jExec.next(key); )
          {
              prevProfileId = jExec.getInt(key,1);
              prevDate = jExec.getDate(key,2);

              if(prevProfileId == userProfileId  && !isNewDate( prevDate, effectiveDate))
              {
                  jExec.closeData(key);
                  return;
              }
              gotRecord = true;
              break;
          }
          jExec.closeData(key);

          if(gotRecord)
          {
               String updateSql = "Update sourcefirmtouserassoc set userProfileId = " +
                 userProfileId + ", effectiveDate = " + sqlStringFrom(effectiveDate) +
                 " where SourceFirmProfileId = " + pk.getId();

               jExec.executeUpdate(updateSql);
          }
          else
          {
               createFirmUserAssoc(userProfileId, effectiveDate);
          }
      }
      catch (Exception e)
      {
        Exception ce = new Exception("SourceFirmProfile Entity - updateFirmeUserAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }


  public void deleteFirmUserAssoc() throws Exception
  {
      String deleteSql = "Delete from sourcefirmtouserassoc where SourceFirmProfileId = " +
          pk.getId();
      try
      {
        jExec.executeUpdate(deleteSql);
      }
      catch(Exception e)
      {
        Exception ce = new Exception("SourceFirmProfile Entity - deleteFirmUserAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      setSourceFirmName(jExec.getString(key,"sourceFirmName"));
      setSystemTypeId(jExec.getInt(key, "systemTypeId"));
      setSourceFirmProfileId(jExec.getInt(key, "sourceFirmProfileId"));     //pk
      setContactId(jExec.getInt(key, "contactId"));
      setProfileStatusId(jExec.getInt(key, "profileStatusId"));
      setSfShortName(jExec.getString(key, "sfShortName"));
      setSfBusinessId(jExec.getString(key, "sfBusinessId"));
      setAlternativeId(jExec.getString(key, "alternativeId"));
      setSfMOProfileId(jExec.getInt(key, "sfMOProfileId" ));
      setSourceFirmCode(jExec.getString(key, "sourceFirmCode"));
      setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
      setSfLicenseRegistrationNumber(jExec.getString(key, "SFLicenseRegistrationNumber"));
      //===========================================
   }

   /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
    public void setField(String fieldName, String value) throws Exception
    {
      doSetField(this, this.getClass(), fieldName, value);
    }



    /**
    *  gets the value of instance fields as String.
    *  if a field does not exist (or the type is not serviced)** null is returned.
    *  if the field exists (and the type is serviced) but the field is null an empty
    *  String is returned.
    *  @returns value of bound field as a String;
    *  @param  fieldName as String
    *
    *  Other entity types are not yet serviced   ie. You can't get the Province object
    *  for province.
    */
    public String getStringValue(String fieldName)
    {
     return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
    *  Updates any Database field that has changed since the last synchronization
    *  ie. the last findBy... call
    *
    *  @return the number of updates performed
    */
    protected int performUpdate()  throws Exception
    {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
    }

    /**
    * using the class name - builds and returns the table name associated with this
    * entity
    */
    public String getEntityTableName()
    {
      return "SourceFirmProfile";
    }

    /**
    * gets this entity's primary key object
    * @return IEntityBeanPK holding an int representation of the primary key
    */
    public IEntityBeanPK getPk()
    {
      return (IEntityBeanPK)pk;
    }

    public SourceFirmProfilePK createPrimaryKey()throws CreateException
    {
      String sql = "Select SourceFirmProfileseq.nextval from dual";
      int id = -1;

      try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key);  )
        {
           id = jExec.getInt(key,1);  // can only be one record
        }

        jExec.closeData(key);

        if( id == -1 ) throw new Exception();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceOfBusinessProfile Entity create() exception getting SourceOfBusinessProfileId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
      }

      return new SourceFirmProfilePK(id);
    }


    /**
    * creates a SourceFirmProfile using a primary key
    */
    public SourceFirmProfile create()throws RemoteException, CreateException
    {
      pk = createPrimaryKey();
      return create(pk);
    }
    public SourceFirmProfile create(SourceFirmProfilePK pk)throws RemoteException, CreateException
    {
      String sql = "Insert into SourceFirmProfile( SourceFirmProfileId, "
        + "institutionProfileId) Values ( " + pk.getId() + "," 
        + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate (); // track the creation
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceFirmProfile Entity - SourceFirmProfile - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }


      try
      {
        Contact c = new Contact(srk);
        c.create();
        this.setContactId(c.getContactId());
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceFirmProfile Entity - SourceOfBusinessProfile - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      srk.setModified(true);
      return this;
    }


    public List getSecondaryParents() throws Exception
    {
      List sps = new ArrayList();

      Object o = getContact();

      if(o != null)
      sps.add(o);

      return sps;
    }


    public String getSourceFirmName(){ return this.sourceFirmName ;}
    public int    getSystemTypeId(){ return this.systemTypeId ;}

    public int getSourceFirmProfileId(){ return this.sourceFirmProfileId ;}     //pk
    public int getContactId(){ return this.contactId ;}
    public int getProfileStatusId(){ return this.profileStatusId ;}
    public String getSfShortName(){ return this.sfShortName ;}
    public String getSfBusinessId(){ return this.sfBusinessId ;}
    public int getSfMOProfileId(){ return this.sfMOProfileId ;}

  /**
  *   retrieves the Contact parent record associated with this entity
  *
  *   @return a Contact
  */
  public Contact getContact()throws FinderException,RemoteException
  {
    return new Contact(srk,this.getContactId(),1);
  }

  //-- FXLink Phase II --//
  // New additional field -- By Billy 06Nov2003
  public String getSourceFirmCode(){ return this.sourceFirmCode ;}
  //===========================================

  public void setSourceFirmName(String value)
  {
   this.testChange("sourceFirmName", value);
   this.sourceFirmName = value;
  }
  public void setSystemTypeId(int value)
  {
    this.testChange("systemTypeId", value);
    this.systemTypeId = value;
  }
  public void setSourceFirmProfileId(int value)
  {
    this.testChange("sourceFirmProfileId", value);
    this.sourceFirmProfileId = value;
  }     //pk
  public void setContactId(int value)
  {
    this.testChange("contactId", value);
    this.contactId = value;
  }

  public void setProfileStatusId(int value)
  {
    this.testChange("profileStatusId", value);
    this.profileStatusId = value;
  }
  public void setSfShortName(String value)
  {
    this.testChange("sfShortName", value);
    this.sfShortName = value;
  }
  public void setSfBusinessId(String value)
  {
    this.testChange("sfBusinessId", value);
    this.sfBusinessId = value;
  }
  public void setSfMOProfileId(int value)
  {
    this.testChange("sfMOProfileId", value);
    this.sfMOProfileId = value;
  }

  //-- FXLink Phase II --//
  // New additional field -- By Billy 06Nov2003
  public void setSourceFirmCode(String value)
  {
    this.testChange("sourceFirmCode", value);
    this.sourceFirmCode = value;
  }

  public void setAlternativeId(String id)
  {
   this.testChange("alternativeId", id);
   this.alternativeId = id;
  }
  public String getAlternativeId(){  return this.alternativeId;  }

  private boolean isNewDate(Date prevDate, Date effectiveDate)
  {
    // To compare only the Date only i.e. set time to 00:00:00
      Date thePrevDate = new Date(prevDate.getYear(), prevDate.getMonth(), prevDate.getDate());
    Date theEffDate = new Date(effectiveDate.getYear(), effectiveDate.getMonth(), effectiveDate.getDate());

    if(thePrevDate.compareTo(effectiveDate) != 0 )
      return true;

    return false;
  }

  public boolean isDupeBusId(String sfbusinessid, int SourceFirmProfileId) throws RemoteException, FinderException
   {
      String sql = "Select sfmoprofileid from sourcefirmProfile where "
                  + "sfmoprofileid <> " + SourceFirmProfileId
                  + " and sfbusinessid = '" + sfbusinessid + "'";

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            jExec.closeData(key);
            return true;
          }
          jExec.closeData(key);
          return false;

        }
        catch (Exception e)
        {
          logger.error("Error in finder - isDupeBusId : " + sql);
          logger.error(e);
          return false;
        }
   }

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> new methods to handle SourceFirm to Group association
  //--> By Billy 07Aug2003
  //MCM Impl Team. Change Log | 21-Aug-2008 | A comma is being placed in the in the insert Query.
  public void createFirmGroupAssoc(int groupProfileId, Date effectiveDate)throws Exception
  {
      String sql = "Insert into sourceFirmToGroupAssoc( SourceFirmProfileId, "
        + "groupProfileId, EffectiveDate, institutionProfileId) Values ( " 
        + pk.getId() + "," + groupProfileId + "," + sqlStringFrom(effectiveDate)+"," // MCM Impl Team Chanage | comma appended to this line. 
        + srk.getExpressState().getDealInstitutionId() + " )";
      try
      {
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceFirmProfile Entity - createFirmGroupAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }
  public void updateFirmGroupAssoc(int groupProfileId, Date effectiveDate ) throws Exception
  {
      String sql = "Select groupProfileId, effectiveDate from sourceFirmToGroupAssoc where SourceFirmProfileId = " +
                 pk.getId();
      int  prevProfileId = 0;
      Date prevDate = null;
      boolean  gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for( ;jExec.next(key); )
          {
              prevProfileId = jExec.getInt(key,1);
              prevDate = jExec.getDate(key,2);

              if(prevProfileId == groupProfileId  && !isNewDate( prevDate, effectiveDate))
              {
                  jExec.closeData(key);
                  return;
              }
              gotRecord = true;
              break;
          }
          jExec.closeData(key);

          if(gotRecord)
          {
               String updateSql = "Update sourceFirmToGroupAssoc set groupProfileId = " +
                 groupProfileId + ", effectiveDate = " + sqlStringFrom(effectiveDate) +
                 " where SourceFirmProfileId = " + pk.getId();

               jExec.executeUpdate(updateSql);
          }
          else
          {
               createFirmGroupAssoc(groupProfileId, effectiveDate);
          }
      }
      catch (Exception e)
      {
        Exception ce = new Exception("SourceFirmProfile Entity - updateFirmeGroupAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }

  public void deleteFirmGroupAssoc() throws Exception
  {
      String deleteSql = "Delete from sourceFirmToGroupAssoc where SourceFirmProfileId = " +
          pk.getId();
      try
      {
        jExec.executeUpdate(deleteSql);
      }
      catch(Exception e)
      {
        Exception ce = new Exception("SourceFirmProfile Entity - deleteFirmGroupAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }
  //=================================================================

  //-- FXLink Phase II --//
  // Changed to check unique by SystemTypeId + sourceFirmCode
  //--> By Billy 13Nov2003
  public boolean isDupeSystemCode(String systemCode, int SystemTypeId) throws RemoteException, FinderException
  {
    return isDupeSystemCode(systemCode, SystemTypeId, 0);
  }
  public boolean isDupeSystemCode(String systemCode, int SystemTypeId, int mySFId) throws RemoteException, FinderException
  {
    String sql = "Select sourcefirmProfileid from sourcefirmProfile where "
                + "sourcefirmProfileid <> " + mySFId
                + " and sourceFirmCode = '" + systemCode + "'"
                + " and SystemTypeId = '" + SystemTypeId + "'";

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
          logger.debug("BILLY==> isDupeSystemCode dupicated records found!!");
          jExec.closeData(key);
          return true;
        }

        jExec.closeData(key);
        return false;

      }
      catch (Exception e)
      {
        logger.error("Error in finder - isDupeSystemCode : " + sql);
        logger.error(e);
        return false;
      }
  }

  public int getInstitutionProfileId()
  {
    return institutionProfileId;
  }

  public void setInstitutionProfileId(int institutionProfileId)
  {
      testChange("institutionProfileId", institutionProfileId);
      this.institutionProfileId = institutionProfileId;
  }

  public String getSfLicenseRegistrationNumber()
  {
    return SFLicenseRegistrationNumber;
  }

  public void setSfLicenseRegistrationNumber(String sfLicenseRegistrationNumber)
  {
      testChange("SFLicenseRegistrationNumber", sfLicenseRegistrationNumber);
      this.SFLicenseRegistrationNumber = sfLicenseRegistrationNumber;
  }
  
}
