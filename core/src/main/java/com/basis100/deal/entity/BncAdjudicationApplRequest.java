
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BncAdjudicationApplRequestPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class BncAdjudicationApplRequest extends DealEntity {
    protected int requestId;
    protected int applicantNumber;
    protected int requestedCreditBureauNameId;
    protected int copyId;
    protected int institutionProfileId;

    protected BncAdjudicationApplRequestPK pk;

    public BncAdjudicationApplRequest(SessionResourceKit srk)
            throws RemoteException {

        super(srk);
    }

    public BncAdjudicationApplRequest() {

    }

    public void setPk(BncAdjudicationApplRequestPK primaryKey) {

        this.pk = primaryKey;
    }

    public BncAdjudicationApplRequest findByPrimaryKey(
            BncAdjudicationApplRequestPK pk) throws RemoteException,
            FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from " + getEntityTableName()
                + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = getEntityTableName()
                        + ": @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(getEntityTableName()
                    + " Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * Find all the BNCADJUDICATIONAPPLREQUEST records for a given request ID
     * 
     * @param requestId
     * @return Collection of BncAdjudicationApplRequest objects.
     * @throws Exception
     */
    public Collection findAllByRequest(RequestPK requestPK) throws Exception {

        Collection results = new ArrayList();

        String sql = "SELECT * FROM BNCADJUDICATIONAPPLREQUEST WHERE REQUESTID="
                + requestPK.getId();

        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                BncAdjudicationApplRequest request = new BncAdjudicationApplRequest(
                        srk);
                request.setPropertiesFromQueryResult(key);
                request.pk = new BncAdjudicationApplRequestPK(request
                        .getRequestId(), request.getApplicantNumber(), request
                        .getCopyId());
                results.add(request);
            }
            jExec.closeData(key);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw e;
        }

        return results;
    }

    /**
     * Set all properties of this object using the values that are returned by
     * the current database query
     * 
     * @param key
     *            int The index of the database query result list
     * @throws Exception
     *             Thrown if error
     */
    public void setPropertiesFromQueryResult(int key) throws Exception {

        setRequestId(jExec.getInt(key, BncAdjudicationApplRequestPK.REQUESTID));
        setApplicantNumber(jExec.getInt(key, "applicantNumber"));
        setRequestedCreditBureauNameId(jExec.getInt(key,
                "requestedCreditBureauNameId"));
        setCopyId(jExec.getInt(key, "copyId"));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    /**
     * Creates a BncAdjudicationApplRequest using the
     * BncAdjudicationApplRequestPK and requestedCreditBureauNameId.
     * 
     */

    public BncAdjudicationApplRequest create(BncAdjudicationApplRequestPK pk,
            int requestedCreditBureauNameId) throws RemoteException,
            CreateException {

        int key = 0;
        String sql = "Insert into " + getEntityTableName() + " (" + "requestId"
                + ", " + "applicantNumber" + ", "
                + "requestedCreditBureauNameId" + ", "
                + "copyId, INSTITUTIONPROFILEID) " + "Values ( " + pk.getId()
                + ", " + pk.getApplicantNumber() + ", "
                + requestedCreditBureauNameId + ", " + pk.getCopyId() + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            key = jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        } catch (Exception e) {
            CreateException ce = new CreateException(getEntityTableName()
                    + " Entity - " + getEntityTableName()
                    + " - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        } finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            } catch (Exception e) {
                logger.error(e.toString());
            }
        }
        srk.setModified(true);
        return this;
    }

    /**
     * This method is a placeholder, the functionality for creating an
     * ADJUDICATIONAPPLREQUEST record should be contained within the
     * AdjudicationApplRequest class.
     * 
     * @param pk
     *            BncAdjudicationApplRequestPK
     * @param copyId
     * @param borrowerId
     * @param requestedCreditBureauNameId
     * @return
     * @throws RemoteException
     * @throws CreateException
     */
    public BncAdjudicationApplRequest createWithAdjudicationApplRequest(
            BncAdjudicationApplRequestPK pk, RequestPK rpk, BorrowerPK bpk,
            int appNum, String appCurrInd, int requestedCreditBureauNameId)
            throws RemoteException, CreateException {

        AdjudicationApplicantRequest adjApplRequest = new AdjudicationApplicantRequest(
                srk);
        adjApplRequest.create(rpk, bpk, appNum, appCurrInd);
        adjApplRequest.ejbStore();

        BncAdjudicationApplRequest request = create(pk,
                requestedCreditBureauNameId);
        request.ejbStore();
        return request;
    }

    public void setRequestId(int value) {

        this.testChange("requestId", value);
        this.requestId = value;
    }

    public int getRequestId() {

        return this.requestId;
    }

    public void setApplicantNumber(int value) {

        this.testChange("applicantNumber", value);
        this.applicantNumber = value;
    }

    public int getApplicantNumber() {

        return this.applicantNumber;
    }

    public void setRequestedCreditBureauNameId(int value) {

        this.testChange("requestedCreditBureauNameId", value);
        this.requestedCreditBureauNameId = value;
    }

    public int getRequestedCreditBureauNameId() {

        return this.requestedCreditBureauNameId;
    }

    public void setCopyId(int value) {

        this.testChange("copyId", value);
        this.copyId = value;
    }

    public int getCopyId() {

        return this.copyId;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // ///////////////////// OVERWRITING BASE CLASS METHODS
    // /////////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);
        return ret;
    }

    public String getEntityTableName() {

        return "BncAdjudicationApplRequest";
    }

    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public boolean equals(Object o) {

        if (o instanceof BncAdjudicationApplRequest) {
            BncAdjudicationApplRequest oa = (BncAdjudicationApplRequest) o;
            if (this.pk.equals(oa.getPk())) {
                return true;
            }
        }
        return false;
    }

    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public Object clone() throws CloneNotSupportedException {

        throw new CloneNotSupportedException();
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}
