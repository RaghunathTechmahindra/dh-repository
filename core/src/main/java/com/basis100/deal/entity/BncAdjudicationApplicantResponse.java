
package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BncAdjudicationApplicantResponsePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: BncAdjudicationApplicantResponse
 * </p>
 * <p>
 * Description: Entity class to handles all database communication to the
 * BncAdjudicationApplicantResponse table
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */
public class BncAdjudicationApplicantResponse extends DealEntity {
    private BncAdjudicationApplicantResponsePK pk;
    private int responseId;
    private int applicantNumber;
    private int instProfileId;
    private int usedCreditBureauNamedId;
    private double networth;

    public static final String USEDCREDITBUREAUNAMEDID = "usedCreditBureauNamedId";
    public static final String NETWORTH = "networth";
    public static final String INSTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public BncAdjudicationApplicantResponse() {

    }

    public BncAdjudicationApplicantResponse(SessionResourceKit srk)
            throws RemoteException {

        super(srk);
    }

    public int getResponseId() {

        return responseId;
    }

    public void setResponseId(int responseId) {

        testChange(BncAdjudicationApplicantResponsePK.RESPONSEID, responseId);
        this.responseId = responseId;
    }

    public int getApplicantNumber() {

        return applicantNumber;
    }

    public void setApplicantNumber(int applicantNumber) {

        testChange(BncAdjudicationApplicantResponsePK.APPLICANTNUMBER,
                applicantNumber);
        this.applicantNumber = applicantNumber;
    }

    public double getNetworth() {

        return networth;
    }

    public void setNetworth(double networth) {

        testChange(NETWORTH, networth);
        this.networth = networth;
    }

    public int getUsedCreditBureauNamedId() {

        return usedCreditBureauNamedId;
    }

    public void setUsedCreditBureauNamedId(int usedCreditBureauNamedId) {

        testChange(USEDCREDITBUREAUNAMEDID, usedCreditBureauNamedId);
        this.usedCreditBureauNamedId = usedCreditBureauNamedId;
    }

    public void setPk(BncAdjudicationApplicantResponsePK primaryKey) {

        this.pk = primaryKey;
    }

    /**
     * search for BncAdjudicationApplicantResponse record using primary key
     * 
     * @param pk
     *            BncAdjudicationApplicantResponsePK Primary key to be searched
     *            by
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws FinderException
     *             Thrown if unable to find a matching row
     * @return BncAdjudicationApplicantResponse object that matches the primary
     *         key given
     */
    public BncAdjudicationApplicantResponse findByPrimaryKey(
            BncAdjudicationApplicantResponsePK pk) throws RemoteException,
            FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from " + getEntityTableName()
                + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = getEntityTableName()
                        + ": @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(getEntityTableName()
                    + " Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * Set all properties of this object using the values that is returned by
     * the current database query
     * 
     * @param key
     *            int The index of the database query result list
     * @throws Exception
     *             Thrown if error
     */
    public void setPropertiesFromQueryResult(int key) throws Exception {

        setResponseId(jExec.getInt(key,
                BncAdjudicationApplicantResponsePK.RESPONSEID));
        setApplicantNumber(jExec.getInt(key,
                BncAdjudicationApplicantResponsePK.APPLICANTNUMBER));
        setUsedCreditBureauNamedId(jExec.getInt(key, USEDCREDITBUREAUNAMEDID));
        setNetworth(jExec.getDouble(key, NETWORTH));
        setInstitutionProfileId(jExec.getInt(key, INSTITUTIONPROFILEID));
    }

    /**
     * Create and inserts a BncAdjudicationApplicantResponse into the database
     * 
     * @param rpk
     *            BncAdjudicationApplicantResponse Primary key
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws CreateException
     *             Thrown if unable to create row
     * @return BncAdjudicationApplicantResponse The
     *         BncAdjudicationApplicantResponse object has has been inserted
     *         into the database
     */
    public BncAdjudicationApplicantResponse create(
            BncAdjudicationApplicantResponsePK rpk) throws RemoteException,
            CreateException {

        int key = 0;
        StringBuffer sqlb = new StringBuffer("Insert into "
                + getEntityTableName() + "( "
                + BncAdjudicationApplicantResponsePK.RESPONSEID + " ,"
                + BncAdjudicationApplicantResponsePK.APPLICANTNUMBER
                + " , INSTITUTIONPROFILEID" + " ) VALUES ( ");
        sqlb.append(sqlStringFrom(rpk.getId()) + ", ");
        sqlb.append(sqlStringFrom(rpk.getApplicantNumber()) + ", ");
        sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId())
                + ")");

        String sql = new String(sqlb);

        try {
            key = jExec.executeUpdate(sql);
            findByPrimaryKey(new BncAdjudicationApplicantResponsePK(
                    rpk.getId(), rpk.getApplicantNumber()));
            this.trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(getEntityTableName()
                    + " Entity - " + getEntityTableName()
                    + " - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        } finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            } catch (Exception e) {
                logger.error(e.toString());
            }
        }
        srk.setModified(true);
        return this;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // ///////////////////// OVERWRITING BASE CLASS METHODS
    // /////////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);
        return ret;
    }

    public String getEntityTableName() {

        return "BncAdjudicationApplResponse";
    }

    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public boolean equals(Object o) {

        if (o instanceof BncAdjudicationApplicantResponse) {
            BncAdjudicationApplicantResponse oa = (BncAdjudicationApplicantResponse) o;
            if (this.pk.equals(oa.getPk())) {
                return true;
            }
        }
        return false;
    }

    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public Object clone() throws CloneNotSupportedException {

        throw new CloneNotSupportedException();
    }

    public int getInstitutionProfileId() {

        return instProfileId;
    }

    public void setInstitutionProfileId(int instProfileId) {

        this.testChange("instProfileId", instProfileId);
        this.instProfileId = instProfileId;
    }

}
