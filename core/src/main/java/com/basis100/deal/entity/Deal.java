package com.basis100.deal.entity;

/**
 * 30/Oct/2007 DVG #DG654 FXP18958: Deal integrity check - crash 
 * 01/Aug/2007 DVG #DG614 deleting masterDeal does not remove DocumentQueue records
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 * 30/May/2005 DVG #DG216 #1462  GE-Production - AU request - retrieve specific copy id
 */

import java.io.ByteArrayInputStream;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

/**
 *
 * Notes:
 * <OL>
 * <LI/> scenarioRecommended <br/>
 *    <PRE>
 *    This property is set as 'Y' on the scenario that has been designated as the 'recommended scenario', and
 *    only one scenario (copy of deal with copyType = 'S') can be marked as the recommended scenario. However,
 *    if no scenario has been marked as the recommended scenario (including when no scenarios exist) the
 *    scenarioRecommended property of the 'gold' copy (the single copy of deal with copyType = 'G') is set to
 *    'Y'. Whenever a recommended scenario has been designated for a deal the scenarioRecommended property of the
 *    'gold' copy is set to 'N'
 *    </PRE>
 * </OL>
 * 
 * version 1.9  <br>
 * Date: 07/21/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 * Added 3 new fields cash back amount, cash back percent, cash back override in deal entity
 * 
 * @version 1.10  <br>
 * Date: 08/01/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Modified to add field for affiliation program Id<br>
 *	- added getter/setter for affiliation program id<br>
 *  - modify the setPropertiesFromQueryResult for affiliation program id<br>
 *  
 * @version 1.11  <br>
 * Date: 08/10/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 * Modified to add field for balanceRemainingAtEndOfTerm, totalCostOfBorrowing, totalAllFeeAmount<br>
 * - added getter/setter for balanceRemainingAtEndOfTerm, totalCostOfBorrowing, totalAllFeeAmount<br>
 * - modify the setPropertiesFromQueryResult for balanceRemainingAtEndOfTerm, totalCostOfBorrowing, totalAllFeeAmount<br>
 *
 * @version 1.12  <br>
 * Date: 08/15/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 * Modified to add field for productTypeId,locRepaymentTypeId<br>
 * - added getter/setter for productTypeId,locRepaymentTypeId<br>
 * - modify the setPropertiesFromQueryResult for productTypeId,locRepaymentTypeId<br>
 * 
 * @version 1.13  <br>
 * Date: 08/22/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 * Modified to add field for interestOverTerm, teaserInterestSaved<br>
 * - added getter/setter for interestOverTerm, teaserInterestSaved<br>
 * - modify the setPropertiesFromQueryResult for interestOverTerm, teaserInterestSaved<br>
 * 
 * @version 1.14  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  <br>
 * XS_2.7 -- 06/06/2008 -- Modified to add field for refiAdditionalInformation<br>
 * - added getter/setter for refiAdditionalInformation<br>
 * - modify the setPropertiesFromQueryResult for refiAdditionalInformation<br>
 * @version 1.15  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  <br>
 * XS_11.11 & XS_11.6 -- 10/06/2008 - Added getComponents() method<br>
 *  @version 1.16  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  <br>
 * XS_2.13 -- 14-JUN-2008 -- Modified removeDescendants() method to add ejbRemove to remove QualifyDetail<br>
 * - QualifyDetail(srk).ejbRemove
 *  *  @version 1.17  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  <br>
 * XS_2.9 -- 17-JUN-2008 -- Modified removeDescendants() method to add ejbRemove to remove ComponentSummary<br>
 * - ComponentSummary(srk).ejbRemove
 *  @version 1.18  <br>
 * Author: MCM Impl Team <br>
 * Change Log:  <br>
 * 19-JUN-2008
 * Modified getChildren , removeDescendants for copy management of components
 * 
 * @version 1.19, FXP23331, MCM/4.1GR, Jan 13, 2009, fixed that Deal.findByDeal didn't add dcm to component. 
 */
public class Deal extends DealEntity
{
    protected int                        dealId;
    protected int                        copyId;
    protected int                        sourceOfBusinessProfileId;
    protected String                     applicationId;
    protected Date                       applicationDate;
    protected double                     totalLoanAmount;
    protected int                        amortizationTerm;
    protected Date                       estimatedClosingDate;
    protected int                        dealTypeId;
    protected int                        lenderProfileId;
    protected String                     sourceApplicationId;
    protected double                     totalPurchasePrice;
    protected double                     postedInterestRate;  //nn
    protected int                        underwriterUserId;
    protected double                     estimatedPaymentAmount;
    protected double                     PandiPaymentAmount;
    protected double                     escrowPaymentAmount;
    protected double                     MIPremiumAmount;
    protected double                     netLoanAmount;
    protected Date                       statusDate;
    protected double                     discount;
    protected String                     secondaryFinancing;   //nn
    protected String                     originalMortgageNumber;
    protected int                        denialReasonId;
    protected int                        renewalOptionsId;
    protected double                     netInterestRate;
    protected Date                       rateDate;
    protected int                        dealPurposeId;
    protected double                     buydownRate;
    protected int                        specialFeatureId;
    protected double                     interimInterestAmount;
    protected double                     downPaymentAmount;
    protected int                        lineOfBusinessId;
    protected int                        mortgageInsurerId;
    protected int                        mtgProdId;
    protected String                     MIPolicyNumber;
    protected Date                       firstPaymentDate;
    protected double                     netPaymentAmount;
    protected String                     MARSNameSearch;
    protected String                     MARSPropertySearch;
    protected String                     MILoanNumber;
    protected double                     commisionAmount;
    protected Date                       maturityDate;
    protected Date                       commitmentIssueDate;
    protected Date                       commitmentAcceptDate;
    protected String                     refExistingMtgNumber;
    protected double                     combinedTotalLiabilities;
    protected String                     bankABANumber;
    protected String                     bankAccountNumber;
    protected String                     bankName;
    protected Date                       actualClosingDate;
    protected double                     combinedTotalAssets;
    protected double                     combinedTotalIncome;
    protected double                     combinedTDS;
    protected double                     combinedGDS;
    protected Date                       renewalDate;
    protected int                        numberOfBorrowers;
    protected int                        numberOfGuarantors;
    protected String                     servicingMortgageNumber;
    protected int                        paymentTermId;
    protected int                        interestTypeId;
    protected int                        paymentFrequencyId;
    protected int                        prePaymentOptionsId;
    protected int                        rateFormulaId;
    protected int                        statusId;
    protected int                        riskRatingId;
    protected String                     electronicSystemSource;
    protected Date                       commitmentExpirationDate;
    protected String                     requestedSolicitorName;
    protected String                     requestedAppraisorName;
    protected int                        interestCompoundId;
    protected int                        investorProfileId;
    protected int                        secondApproverId;
    protected int                        jointApproverId;
    protected int                        branchProfileId;
    protected int                        sourceFirmProfileId;
    protected int                        groupProfileId;
    protected int                        administratorId;
    protected int                        pricingProfileId;
    protected double                     refinanceNewMoney;
    protected Date                       interimInterestAdjustmentDate;
    protected int                        privilegePaymentId;
    protected double                     advanceHold;
    protected String                     clientNumber;
    protected double                     combinedLTV;
    protected double                     combinedTotalAnnualHeatingExp;
    protected double                     combinedTotalAnnualTaxExp;
    protected double                     combinedTotalCondoFeeExp;
    protected double                     combinedTotalPropertyExp ;
    protected int                        commitmentPeriod;
    protected String                     commitmentProduced;
    protected int                        crossSellProfileId;
    protected Date                       datePreAppFirm;
    protected int                        fifthApproverId;
    protected int                        finalApproverId;
    protected int                        financeProgramId;
    protected int                        fourthApproverId;
    protected boolean                    investorApproved;
    protected String                     investorConfirmationNumber;
    protected double                     lienHolderSecondaryFinancing;
    protected int                        lienPositionId;
    protected int                        MIPayorId;
    protected int                        MITypeId;
    protected double                     newMoney;
    protected Date                       nextRateChangeDate;
    protected int                        numberOfProperties;
    protected double                     oldMoney;
    protected Date                       onHoldDate;   //Add
    protected boolean                    preApproval;
    protected double                     premium;
    protected boolean                    rateLock;
    protected Date                       rateLockExpirationDate;
    protected int                        rateLockPeriod;
    protected String                     referenceDealNumber;
    protected int                        referenceDealTypeId;
    protected double                     refiCurrentBal;
    protected double                     remainingBalanceSecondaryFin;
    protected Date                       returnDate;
    protected String                     secondaryFinancingLienHolder;
    protected String                     sourceRefAppNBR;
    protected String                     sourceSystemMailBoxNBR;
    protected int                        taxPayorId;
    protected int                        thirdApproverId;
    protected double                     totalActAppraisedValue;
    protected double                     totalEstAppraisedValue;
    protected double                     totalPropertyExpenses;
    protected double                     additionalPrincipal;
    protected double                     totalPaymentAmount;
    protected int                        actualPaymentTerm;
    protected double                     combinedGDSBorrower;
    protected double                     combinedTDSBorrower;
    protected double                     combinedGDS3Year;
    protected double                     combinedTDS3Year;
    protected double                     combinedTotalEquityAvailable;
    protected int                        effectiveAmortizationMonths;
    protected double                     postedRate;
    protected double                     blendedRate;
    protected String                     channelMedia;
    protected int                        closingTypeId;
    protected String                     MIUpfront;
    protected Date                       preApprovalConclusionDate;
    protected Date                       preApprovalOriginalOfferDate;
    protected String                     previouslyDeclined;
    protected double                     requiredDownpayment;
    protected int                        MIIndicatorId;
    protected int                        previousMIIndicatorId;
    protected int                        repaymentTypeId;
    protected int                        decisionModificationTypeId;
    protected String                     scenarioApproved;
    protected String                     scenarioRecommended;
    protected int                        scenarioNumber;
    protected String                     scenarioDescription;
    protected String                     MIComments;
    protected int                        MIStatusId;
    protected int                        systemTypeId;
    protected double                     totalFeeAmount;
    protected double                     maximumLoanAmount;
    protected String                     scenarioLocked;
    protected double                     totalLoanBridgeAmount;
    protected String                     solicitorSpecialInstructions  ;
    // LONG CHAR field -- needed special handle, due to the SQL statement limitation (must less than 4K)
    // Have to use PreparedStatement instead, See method updateMortgageInsuranceResponse().  -- By BILLY 25Feb2002
    protected String                     mortgageInsuranceResponse;
    // ==============================================================================================
    protected double                     combinedLendingValue;
    protected double                     existingLoanAmount;
    protected double                     GDSTDS3YearRate;
    protected double                     maximumPrincipalAmount;
    protected double                     maximumTDSExpensesAllowed;
    protected String                     MICommunicationFlag;
    protected String                     MIExistingPolicyNumber;
    protected double                     minimumIncomeRequired;
    protected double                     PAPurchasePrice;
    protected String                     instructionProduced;
    protected double                     teaserDiscount;
    protected Date                       teaserMaturityDate;
    protected double                     teaserNetInterestRate;
    protected double                     teaserPIAmount;
    protected int                        teaserTerm;
    protected String                     copyType;
    protected String                     openMIResponseFlag;
    protected int                        resolutionConditionId;
    protected String                     checkNotesFlag;
    protected Date                       closingDatePlus90Days;
    // New fields for new calculation changes -- Billy 23Nov2001
    protected double                     MIPremiumPST;
    protected double                     perdiemInterestAmount;
    protected int                        IADNumberOfDays;
    protected double                     PandIPaymentAmountMonthly;
    protected Date                       firstPaymentDateMonthly;
    protected int                        funderProfileId;
    // ==========================================================

    // New fields for CMHC modifications -- Billy 07Feb2002
    protected String                     progressAdvance;
    protected double                     advanceToDateAmount;
    protected int                        advanceNumber;
    protected String                     MIRUIntervention;
    protected String                     preQualificationMICertNum;
    protected String                     refiPurpose;
    protected double                     refiOrigPurchasePrice;
    protected String                     refiBlendedAmortization;
    protected double                     refiOrigMtgAmount;
    protected String                     refiImprovementsDesc;
    protected double                     refiImprovementAmount;
    protected double                     nextAdvanceAmount;
    protected Date                       refiOrigPurchaseDate;
    protected String                     refiCurMortgageHolder;
    // ==========================================================
    //// SYNCADD.
    // New field for SOB resubmission handling -- By Billy 12June2002
    protected String                     resubmissionFlag;
    //===============================================================
    //===============================================================
    // New fields to handle MIPolicyNum problem : the number lost if user cancel MI
    //  and re-send again later.  -- Modified by Billy 14July2003
    protected String                     MIPolicyNumCMHC;
    protected String                     MIPolicyNumGE;
    //=============================================================================
    //-- FXLink Phase II --//
    //--> New Field for Market Type Indicator
    //--> By Billy 18Nov2003
    protected String                     mccMarketType;
    //==============================================================
    //--> Ticket#127 -- BMO Tracking broker Commissions
    //--> New field added to indicate if the Broker is paid or not
    //--> By Billy 24Nov2003
    protected String                     brokerCommPaid;
    //==============================================================
    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> New Field for HoldReasonId
    //--> By Billy 06Jan2004
    protected int                        holdReasonId;
    //--DJ_LDI_CR--start//
    protected double           pmntPlusLifeDisability;
    protected double           lifePremium;
    protected double           disabilityPremium;
    protected double           combinedPremium;
    protected double           interestRateIncrement;
    protected double           interestRateIncLifeDisability;
    protected double           equivalentInterestRate;
    //--DJ_LDI_CR--end//
    //--DJ_CR010--start//
    protected String           commisionCode;
    //--DJ_CR010--end//
    //--DJ_CR203.1--start//
    protected String           multiProject;
    protected String           proprietairePlus;
    protected double           proprietairePlusLOC;
    //--DJ_CR203.1--end//
    // Ticket #433 -- new field for FX Link II
    //--> By Billy 14June2004
    protected String           sourceApplicationVersion;
    //========================================


    //======================================== ActiveX Ver 1.0.2 Start ========================================
    // -- DJ Ticket#499 - Catherine  --
    protected double           addCreditCostInsIncurred;
    protected double           totalCreditCost;
    protected double           intWithoutInsCreditCost;
    // -- DJ Ticket#499 - Catherine  end --
    //======================================== ActiveX Ver 1.0.2 end ==========================================

    //Added by Mike, may not be my task to add the upload serial Id
    //Clement Cheng, Change the type from double to int to match the DB type
    protected int uploadSerialNumber;

    protected String fundingUploadDone; // Catherine, 10-Jan-05, XD to MCAP

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    protected double primeBaseAdj;
    protected int primeIndexId;
    protected double primeIndexRate;
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    // SEAN DJ SPEC-Progress Advance Type July 19, 2005: Added one new field.
    protected int progressAdvanceTypeId;
    // SEAN DJ SPEC-PAT END

    // SEAN GECF Income Verification Document Type Drop Down.
    protected int incomeVerificationTypeId;


    // END GECF IV Document Type Drop Down.
    protected int rateGuaranteePeriod; // renamed from dealRateGuaranteePeriod to rateGuaranteePeriod
    //Start - MI additions
//  protected int           productTypeId;
    protected String        progressAdvanceInspectionBy;
    protected String        selfDirectedRRSP;
    protected String        cmhcProductTrackerIdentifier;
//  protected int           locRepaymentTypeId;
    protected String        requestStandardService;
    protected int           locAmortizationMonths;
    protected Date          locInterestOnlyMaturityDate;
    protected int           refiProductTypeId;
    protected double        pandIUsing3YearRate;
    //End - MI additions

    protected String closingUploadDone; // GE to FCT Link, Catherine, 1-Sep-05

//  AAtcha starts
    protected String 		  MIPolicyNumAIGUG;
    protected double 		  MIFeeAmount;
    protected String 		  MIPremiumErrorMessage;
    //AAtcha Ends

    //***** Change by NBC/PP Implementation Team - Version 1.6 - Start *****//
    protected String floatDownTaskCreatedFlag;
    protected String floatDownCompletedFlag;
    //***** Change by NBC/PP Implementation Team - Version 1.6 - End *****//

    // ***** Change by NBC Impl. Team - Version 1.9 - Start *****//
    protected double cashBackAmount;
    protected double cashBackPercent;
    protected String cashBackAmountOverride;
    // ***** Change by NBC Impl. Team - Version 1.9 - End*****//

    //  ***** Change by NBC Impl. Team - Version 1.10 - Start *****//
    protected int affiliationProgramId;
    //  ***** Change by NBC Impl. Team - Version 1.10 - End *****//

    //  ***** Change by NBC Impl. Team - Version 1.11 - Start *****//
    protected double balanceRemainingAtEndOfTerm;
    protected double totalCostOfBorrowing;
    //  ***** Change by NBC Impl. Team - Version 1.11 - End *****//

    //  ***** Change by NBC Impl. Team - Version 1.12 - Start *****//
    protected int productTypeId;
    protected int locRepaymentTypeId;
    //	  ***** Change by NBC Impl. Team - Version 1.12 - End *****//

    //	  ***** Change by NBC Impl. Team - Version 1.13 - Start *****//
    protected double interestOverTerm;
    protected double teaserRateInterestSaving;
    //	  ***** Change by NBC Impl. Team - Version 1.13 - End *****//

    // XueBin 20060725 add new fields according to DJ specs (Outstanding Credit Scoring Response flag)
    protected String outCreditScoreResponse; 
    protected String criticalChangeFlag;

    //  **** added for CR03 - 3.2 Gen - Start ****//
    protected String autoCalcCommitExpiryDate;
    //  **** added for CR03 - 3.2 Gen - End ****//

    //  **** added for ML - Start ****//
    protected int institutionProfileId;
    //  **** added for NL - End ****//

    protected String MIPolicyNumPMI;
  
    //4.3GR, Update Total Loan Amount -- start
    protected double miPremiumAmountPrevious;
	protected String miPremiumAmountChangeNortified;
    //4.3GR, Update Total Loan Amount -- end

    /***************MCM Impl team changes starts - XS_2.7 *******************/
    protected String refiAdditionalInformation;
    /***************MCM Impl team changes ends - XS_2.7 *********************/
    
    protected Date financingWaiverDate;

    //4.4 Submission Agent
    protected int sourceOfBusinessProfile2ndaryId;
    //4.4 Submission Agent - end
    
    //Qualify Rate
    protected String qualifyingOverrideFlag;
	protected String qualifyingRateOverrideFlag;
    protected int overrideQualProd;
    
    //Generic MI
    protected int MIDealRequestNumber;

    private DealPK  pk;

    public Deal()  {} // necessary to support MIProcessHandler only

    public Deal(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
    {
        super(srk,dcm);
    }

    public Deal(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
    {
        super(srk,dcm);

        pk = new DealPK(id, copyId);

        findByPrimaryKey(pk);
    }

    public DealPK createPrimaryKey(int copyId, int institutionId)throws CreateException
    {
        try
        {
            InstitutionProfile institution = new InstitutionProfile(srk);
            institution = institution.findByPrimaryKey(new InstitutionProfilePK(institutionId));
            String institutionAbbr = institution.getInstitutionAbbr();

            String sql = "Select " + institutionAbbr + "_dealseq.nextval from dual";
            int id = -1;

            int key = jExec.execute(sql);

            for (; jExec.next(key);  )
            {
                id = jExec.getInt(key,1);  // can only be one record
            }

            jExec.closeData(key);

            if( id == -1 ) throw new Exception();
            return new DealPK(id, copyId);
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("Deal Entity create() exception getTing dealId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

    }



    /**
     * creates a deal using a primary key
     */

    public Deal create(DealPK pk)throws RemoteException, CreateException
    {
        String appId = String.valueOf(pk.getId());

        String sql = "Insert into Deal(" + pk.getName() +
        ", " + "applicationId" + ", " + "copyId" + ", " + "institutionProfileId" + ")" 
        + " Values ( " + pk.getId() + ", '" + appId +
        "', " + pk.getCopyId() + ", " + srk.getExpressState().getDealInstitutionId() +" )";

        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("Deal Entity - Deal - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        if(dcm != null)
            //dcm.inputEntity(this);
            dcm.inputCreatedEntity ( this );


        return this;
    }


    public Collection findBySouAppIdAndSysType(String sourceAppId, int pSysTypeId, Date minDate) throws Exception
    {
        List found = new ArrayList();
        StringBuffer sqlbuf = new StringBuffer("Select * from Deal where ");

        sqlbuf.append(" SOURCEAPPLICATIONID = ").append( sqlStringFrom( sourceAppId) );
        sqlbuf.append(" AND systemTypeId = ").append(pSysTypeId);
        sqlbuf.append(" AND copyType = 'G' ");
        if(minDate != null) //if a date is supplied
            sqlbuf.append(" AND applicationDate > ").append(sqlStringFrom(minDate));

        String sql = sqlbuf.toString();
        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                found.add(iCpy);
            }

            jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("Exception caught finding Deal by DupeCheck Criteria" );

            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }

        return found;
    }

    //===============================================================================================
    // method to be used by Dupcheck
    //===============================================================================================
    public Collection findByDCSourceApplicationId( String sourceAppId, Date minDate) throws Exception
    {
        List found = new ArrayList();
        StringBuffer sqlbuf = new StringBuffer("Select * from Deal where ");

        sqlbuf.append(" SOURCEAPPLICATIONID = ").append( sqlStringFrom( sourceAppId) );
        sqlbuf.append(" AND copyType = 'G' ");
        if(minDate != null) //if a date is supplied
            sqlbuf.append(" AND applicationDate > ").append(sqlStringFrom(minDate));

        String sql = sqlbuf.toString();
        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                found.add(iCpy);
            }

            jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("Exception caught finding Deal by DupeCheck Criteria" );

            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }

        return found;

    }

    //===============================================================================================
    // method to be used by Dupcheck
    //===============================================================================================
    public Collection findByDCSourceMailBox( String mailBoxNbr, Date minDate) throws Exception
    {
        List found = new ArrayList();
        StringBuffer sqlbuf = new StringBuffer("Select * from Deal where ");

        sqlbuf.append(" SOURCESYSTEMMAILBOXNBR = ").append( sqlStringFrom( mailBoxNbr) );
        sqlbuf.append(" AND copyType = 'G' ");
        if(minDate != null) //if a date is supplied
            sqlbuf.append(" AND applicationDate > ").append(sqlStringFrom(minDate));

        String sql = sqlbuf.toString();
        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                found.add(iCpy);
            }

            jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("Exception caught finding Deal by DupeCheck Criteria" );

            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }

        return found;

    }

    public Collection findByDupeCheckCriteria(String sourceMailBox, String sourceAppId, Date minDate) throws Exception
    {
        List found = new ArrayList();

        StringBuffer sqlbuf = new StringBuffer("Select * from Deal where ");

        if(sourceMailBox == null)
            sqlbuf.append("SourceSystemMailboxNbr is null");
        else
            sqlbuf.append("SourceSystemMailboxNbr = ").append(sqlStringFrom( sourceMailBox));

        if(sourceAppId == null)
            sqlbuf.append(" AND SourceApplicationId is null");
        else
            sqlbuf.append(" AND SourceApplicationId = ").append(sqlStringFrom( sourceAppId));

        sqlbuf.append(" AND copyType = 'G' ");

        if(minDate != null) //if a date is supplied
            sqlbuf.append(" AND applicationDate > ").append(sqlStringFrom(minDate));

        String sql = sqlbuf.toString();
        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                found.add(iCpy);
            }

            jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("Exception caught finding Deal by DupeCheck Criteria" );

            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }

        return found;
    }

    //#DG600 not used - delete after 1/Jan/2009
    // ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    /**
     * findByScenarioRecommended
     * This method gets the previous rate value for the deal from DB. It uses the scenario recommended copy of deal.
     * The rate value is fetched from pricingrateinventory table.
     * @param deal Deal object <br>
     * @return double : internalratepercentage value from the pricingrateinventory table<br>
     */
    public double findByScenarioRecommended(Deal deal) throws FinderException 
    {
        // query to get the previous rate value from the pricingrateinventory table on scenario recommended copy of deal.
        String sql= "select pri.internalratepercentage from pricingrateinventory pri, deal deal" +
        " where pri.pricingrateinventoryid = deal.pricingprofileid and deal.scenariorecommended = 'Y' and deal.dealid=?";

        boolean gotRecord = false;
        double internalratepercentage = 0;
        try
        {
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid to the prepared statment
            pstmt.setInt(1, deal.getDealId());
            int key = jExec.executePreparedStatement(pstmt, sql);

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                internalratepercentage= jExec.getDouble(key, 1);
                break; // can only be one record
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findBySecnarioRecommended(), key= " + internalratepercentage + ", Previous rate value not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByPrimaryKey() exception");
            logger.error(fe.getMessage());
            logger.error("In Deal:findBySecnarioRecommended finder sql:  " + sql);
            logger.error(e);

            throw fe;
        }
        return internalratepercentage;
    }

//  ***** Change by NBC Impl. Team - Version 1.2 - End*****//


    public Deal findByPrimaryKey(DealPK pk) throws RemoteException, FinderException
    {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache

        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal " + pk.getWhereClause();
        String sql = "Select * from Deal where dealid = ? and copyid = ?";
        // =============================================================================================

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, pk.getId());
            // Set Copyid
            pstmt.setInt(2, pk.getCopyId());
            logger.debug("@Deal.findByPrimaryKey : Dealid = " + pk.getId() + " Copyid = " + pk.getCopyId());
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByPrimaryKey() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    // findByRecommendedScenario:
    //
    // If found the copy id property of the primary key (object) will be set.

    public Deal findByRecommendedScenario(DealPK pk, boolean allowGold) throws RemoteException, FinderException
    {
        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal " + pk.getWhereClauseDealIdOnly();
        String sql = "Select * from Deal where dealid = ? ";
        //===========================================================================

        if (allowGold == true)
            sql = sql + " AND (copyType = 'S' OR copyType = 'G') AND scenarioRecommended = 'Y'";
        else
            sql = sql + " AND copyType = 'S' AND scenarioRecommended = 'Y'";

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, pk.getId());
            logger.debug("@Deal.findByRecommendedScenario : Dealid = " + pk.getId());
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);

                pk.setCopyId(getCopyId());

                break; // can only be one record
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByRecommendedScenario(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByRecommendedScenario() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;
    }


    public Deal findByGoldCopy(int dealId) throws RemoteException, FinderException
    {
        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal where dealId = " + dealId;
        //sql = sql + " AND copyType = 'G'";
        String sql = "Select * from Deal where dealId = ? AND copyType = 'G'";
        // ====================================================================

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, dealId);
            logger.debug("@Deal.findByGoldCopy : Dealid = " + dealId);
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;

                setPropertiesFromQueryResult(key);
                this.pk = new DealPK(this.getDealId(), this.getCopyId());

                break; // can only be one record
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByGoldCopy(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByGoldCopy() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;
    }


    // findByGoldCopy
    //
    // If found the copy id property of the primary key (object) will be set.

    public Deal findByGoldCopy(DealPK pk) throws RemoteException, FinderException
    {
        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal " + pk.getWhereClauseDealIdOnly();
        //sql = sql + " AND copyType = 'G'";
        String sql = "Select * from Deal where dealid = ? AND copyType = 'G'";
        // =============================================================================================

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, pk.getId());
            logger.debug("@Deal.findByGoldCopy : Dealid = " + pk.getId());
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);

                pk.setCopyId(getCopyId());

                break; // can only be one record
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByGoldCopy(), key= " + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;


            FinderException fe = new FinderException("Deal - findByGoldCopy() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;
    }

    // findByUnlockedScenario:
    //
    // If found the copy id property of the primary key (object) will be set.
    //
    // Locates the first unlocked scenarion as order by copy id

    public Deal findByUnlockedScenario(DealPK pk) throws RemoteException, FinderException
    {
        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal " + pk.getWhereClauseDealIdOnly();
        //sql = sql + " AND copyType = 'S' AND scenarioLocked = 'N' ORDER BY copyId";
        String sql = "Select * from Deal where dealid = ? AND copyType = 'S' AND scenarioLocked = 'N' ORDER BY copyId";

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, pk.getId());
            logger.debug("@Deal.findByUnlockedScenario : Dealid = " + pk.getId());
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);

                pk.setCopyId(getCopyId());

                break; // want first one only
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByUnlockedScenario(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByUnlockedScenario() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;
    }

    // findByHighestScenario:
    //
    // If found the copy id property of the primary key (object) will be set.
    //
    // Locates the scenario with the highest copy id (most recently created) (if one)

    public Deal findByHighestScenario(DealPK pk) throws RemoteException, FinderException
    {
        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal " + pk.getWhereClauseDealIdOnly();
        //sql = sql + " AND copyType = 'S' order by copyid desc";
        String sql = "Select * from Deal where dealid = ? AND copyType = 'S' order by copyid desc";
        // ===========================================================================================

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, pk.getId());
            logger.debug("@Deal.findByGoldCopy : Dealid = " + pk.getId());
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);

                pk.setCopyId(getCopyId());

                break; // want first one only
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByHighestScenario(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByHighestScenario() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;
    }


    /**
     * Locates the recommended scenario for the given dealId
     * where the copyType is not transactional -> <i>copyType != T.</i>
     *
     *
     *
     */
    public Deal findByNonTransactionalRecommendedScenario(int dealId) throws RemoteException, FinderException
    {
        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal where dealId = " + dealId +  " AND copyType <> 'T' AND scenarioRecommended = 'Y'";
        String sql = "Select * from Deal where dealId = ? AND copyType <> 'T' AND scenarioRecommended = 'Y'";
        //String getIPIdIndexSql = "Select column_id from user_tab_columns where table_name = 'DEAL' and column_name = 'INSTITUTIONPROFILEID'";
        // ==================================================================================================================

        boolean gotRecord = false;

        try
        {

            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, dealId);
            logger.debug("@Deal.findByNonTransactionalRecommendedScenario : Dealid = " + dealId);
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);

                // ad
                this.pk = new DealPK(dealId, copyId);

                break; // can only be one record
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByNonTransactionalRecommendedScenario(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByNonTransactionalRecommendedScenario() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

    /**
     * Locates the scenario with the the given scenarioNumber for the given dealId<br/>
     * where the copyType is not transactional -> <i>copyType != T.</i>
     *
     *
     *
     */
    public Deal findByNonTransactionalScenario(int scenarioNumber, int dealId) throws RemoteException, FinderException
    {
        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal where dealId = " + dealId + " and scenarioNumber = " + scenarioNumber
        //+ " AND copyType <> 'T' ";
        String sql = "Select * from Deal where dealId = ? and scenarioNumber = ? AND copyType <> 'T' ";
        //=======================================================================================================

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, dealId);
            // Set scenarioNumber
            pstmt.setInt(2, scenarioNumber);
            logger.debug("@Deal.findByGoldCopy : Dealid = " + dealId + " ScenarioNumber = " + scenarioNumber);
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;

                setPropertiesFromQueryResult(key);
                //added srk.getInstitutionId() for ML - July 6, 2007 Midori
                this.pk = new DealPK(dealId, copyId);

                break; // want first one only
            }

            jExec.closeData(key);
            pstmt.close();

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByHighestScenario(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByNonTransactionalScenario() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }



    // #DG216 new function for GE- AU request - retrieve specific copy id
    /**
     * Locates the scenario with the highest copyId for the given dealId<br/>
     * where the copyType is not transactional -> <i>copyType != T.</i>
     *
     * @param dealId int
     * @return Deal
     * @throws FinderException
     */
    public Deal findByNonTransactional(int dealId) throws FinderException
    {
        String sql = "Select * from Deal where dealId = ? AND copyType <> 'T' and rownum <= 1 order by copyid desc";
        //=======================================================================================================

        boolean gotRecord = false;
        try {
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, dealId);
            logger.debug("@Deal.findByNonTransactional: Dealid = " + dealId);
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================
            gotRecord = jExec.next(key);
            if (gotRecord) {
                setPropertiesFromQueryResult(key);
                //added srk.getInstitutionId() for ML - July 6, 2007 Midori
                this.pk = new DealPK(dealId, copyId);
            }
            jExec.closeData(key);
            pstmt.close();
        }
        catch (Exception e) {
            FinderException fe = new FinderException("Deal - findByNonTransactional() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }
        if (!gotRecord) {
            String msg = "Deal Entity: @findByNonTransactional(), key= " + pk +", entity not found";
            if (!getSilentMode())
                logger.error(msg);
            throw new FinderException(msg);
        }
        return this;
    }

    /** findBySelectableScenarios()
     *
     * Find all copies of deal (id) that are true scenario and optionally the gold
     * copy as well.
     *
     **/

    public Collection findBySelectableScenarios(int dealId, boolean includeGold, boolean includeLocked) throws RemoteException, FinderException
    {
        Collection found = new ArrayList();

        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal where dealId = " + dealId + " AND ";
        String sql = "Select * from Deal where dealId = ? AND ";

        if (includeGold == true)
            sql = sql + " (copyType = 'S' OR copyType = 'G')";
        else
            sql = sql + " copyType = 'S'";

        if (includeLocked == false)
            sql = sql + " AND scenariolocked = 'N'";

        sql = sql + " order by copytype, scenarionumber";


        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, dealId);
            logger.debug("@Deal.findBySelectableScenarios : Dealid = " + dealId);
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                // added  srk.getInstitutionId() for ML - July 6, 
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                found.add(iCpy);
            }

            jExec.closeData(key);
            pstmt.close();
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return found;
    }

    /** findByAllUnlocked()
     *
     * Find all copies of deal (id) that are unlocked - the current gold copy is permitted
     * in the result list.
     *
     **/

    public Collection findByAllUnlocked(int dealId) throws RemoteException, FinderException
    {
        Collection found = new ArrayList();

        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal where dealId = " + dealId;
        //sql = sql + " AND scenarioLocked = 'N' ORDER BY copyId";
        String sql = "Select * from Deal where dealId = ? AND scenarioLocked = 'N' ORDER BY copyId";
        // =============================================================================================

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, dealId);
            logger.debug("@Deal.findByAllUnlocked : Dealid = " + dealId);
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                // added  srk.getInstitutionId() for ML - July 6, 
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                found.add(iCpy);
            }

            jExec.closeData(key);
            pstmt.close();
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return found;
    }

    public Collection findByAllUnlockedScenarios(int dealId) throws RemoteException, FinderException
    {
        Collection found = new ArrayList();

        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal where dealId = " + dealId;
        //sql = sql + " AND copyType = 'S'  AND scenarioLocked = 'N' ORDER BY copyId";
        String sql = "Select * from Deal where dealId = ? AND copyType = 'S'  AND scenarioLocked = 'N' ORDER BY copyId";

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, dealId);
            logger.debug("@Deal.findByAllUnlockedScenarios : Dealid = " + dealId);
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                // added  srk.getInstitutionId() for ML - July 6, 
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                found.add(iCpy);
            }

            jExec.closeData(key);
            pstmt.close();
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return found;
    }


    public Deal findByAsset(Asset a) throws RemoteException, FinderException
    {
        return findByBorrowerChild("Asset", a);
    }

    public Deal findByIncome(Income a) throws RemoteException, FinderException
    {
        return findByBorrowerChild("Income", a);
    }

    public Deal findByLiability(Liability a) throws RemoteException, FinderException
    {
        return findByBorrowerChild("Liability", a);
    }

    public Deal findByEmploymentHistory(EmploymentHistory a) throws RemoteException, FinderException
    {
        return findByBorrowerChild("EmploymentHistory", a);
    }

    public Deal findByBorrowerAddress(BorrowerAddress a) throws RemoteException, FinderException
    {
        return findByBorrowerChild("BorrowerAddress", a);
    }

    public Deal findByLifeDisabilityPremiums(LifeDisabilityPremiums a) throws RemoteException, FinderException
    {
        return findByBorrowerChild("LifeDisabilityPremiums", a);
    }

    //--DJ_CR136--23Nov2004--start--//
    public Deal findByLifeDisPremiumsIOnlyA(LifeDisPremiumsIOnlyA a) throws RemoteException, FinderException
    {
        return findByInsureOnlyApplicantChild("LifeDisPremiumsIOnlyA", a);
    }

    private Deal findByInsureOnlyApplicantChild(String entityName, DealEntity entity) throws RemoteException, FinderException
    {
        InsureOnlyApplicant ioa = null;
        int id = -1;
        int copy = -1;

        if(entityName.equals("LifeDisPremiumsIOnlyA"))
        {
            LifeDisPremiumsIOnlyA ldp = (LifeDisPremiumsIOnlyA)entity;
            id = ldp.getInsureOnlyApplicantId();
            logger.debug("DealEntity@FindByInsureOnlyAppl:getInsureOnlyApplicantId: " + id);

            copy = ldp.getCopyId();
        }

        try
        {
            ioa = new InsureOnlyApplicant(srk,dcm,id,copy);
        }
        catch(Exception e)
        {
            String msg = "Finder Error - Parent not found. ";
            logger.error(msg);
            throw new FinderException(msg + this.getClass().getName());
        }
        // added  srk.getInstitutionId() for ML - July 6, 
        DealPK pk = new DealPK(ioa.getDealId(),ioa.getCopyId());

        String sql = "Select * from Deal where dealid = ? and copyid = ?";

        boolean gotRecord = false;

        try
        {
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, pk.getId());
            // Set Copyid
            pstmt.setInt(2, pk.getCopyId());
            logger.debug("@Deal.findByPrimaryKey : Dealid = " + pk.getId() + " Copyid = " + pk.getCopyId());
            int key = jExec.executePreparedStatement(pstmt, sql);

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;
    }
    //--DJ_CR136--23Nov2004--end--//

    private Deal findByBorrowerChild(String entityName, DealEntity entity) throws RemoteException, FinderException
    {
        Borrower borr = null;
        int id = -1;
        int copy = -1;

        if(entityName.equals("Asset"))
        {
            Asset asset = (Asset)entity;
            id = asset.getBorrowerId();
            copy = asset.getCopyId();
        }
        else if(entityName.equals("Income"))
        {
            Income income = (Income)entity;
            id = income.getBorrowerId();
            copy = income.getCopyId();

        }
        else if(entityName.equals("Liability"))
        {
            Liability liab = (Liability)entity;
            id = liab.getBorrowerId();
            copy = liab.getCopyId();
        }
        else if(entityName.equals("BorrowerAddress"))
        {
            BorrowerAddress ba = (BorrowerAddress)entity;
            id = ba.getBorrowerId();
            copy = ba.getCopyId();
        }
        else if(entityName.equals("EmploymentHistory"))
        {
            EmploymentHistory eh = (EmploymentHistory)entity;
            id = eh.getBorrowerId();
            copy = eh.getCopyId();
        }
        //--DJ_LDI_CR--start--//
        else if(entityName.equals("LifeDisabilityPremiums"))
        {
            LifeDisabilityPremiums ldp = (LifeDisabilityPremiums)entity;
            id = ldp.getBorrowerId();
            copy = ldp.getCopyId();
        }
        //--DJ_LDI_CR--start--//

        try
        {
            borr = new Borrower(srk,dcm,id,copy);
        }
        catch(Exception e)
        {
            String msg = "Finder Error - Parent not found. ";
            logger.error(msg);
            throw new FinderException(msg + this.getClass().getName());
        }

        // added  srk.getInstitutionId() for ML - July 6, 
        DealPK pk = new DealPK(borr.getDealId(),borr.getCopyId());

        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal " + pk.getWhereClause();
        String sql = "Select * from Deal where dealid = ? and copyid = ?";
        //=========================================================

        boolean gotRecord = false;

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, pk.getId());
            // Set Copyid
            pstmt.setInt(2, pk.getCopyId());
            logger.debug("@Deal.findByPrimaryKey : Dealid = " + pk.getId() + " Copyid = " + pk.getCopyId());
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;
    }

    public Deal findByPropertyExpense(PropertyExpense pe) throws RemoteException, FinderException
    {
        Property prop = null;

        try
        {
            prop = new Property(srk,dcm,pe.getPropertyId(),pe.getCopyId());
        }
        catch(Exception e)
        {
            String msg = "Finder Error - Parent not found. ";
            logger.error(msg);
            throw new FinderException(msg + this.getClass().getName());
        }

        DealPK pk = new DealPK(prop.getDealId(),prop.getCopyId());


        String sql = "Select * from Deal " + pk.getWhereClause();

        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key);  )
            {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("Deal - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.dealId = pk.getId();

        return this;

    }

    /**
     * setPropertiesFromQueryResult
     * 
     * @param key int <br>
     * @return none<br>
     * @version 1.5 <br>
     * Date: 06/28/2006 <br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     *          Added call to set floatDownTaskCreatedFlag,
     *          floatDownCompletedFlag attributes
     *          
     *          @version 1.9 <br>
     *          Date: 07/21/2006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br>
     *          Added call to set cash amount, cash percentage, cash override attributes
     *          
     * 
     * 			@version 1.10 <br>
     * 			Date: 08/01/2006 <br>
     * 			Author: NBC/PP Implementation Team <br>
     * 			Change: <br>
     * 			Added call to set affiliationProgramId attribute
     * 
     * 			@version 1.11 <br>
     * 			Date: 06/10/2008 <br>
     * 			Author: MCM Impl Team <br>
     * 			Change: <br>
     * 			Added call to setRefiAdditionalInformation attribute
     * 
     *          @version 1.12 <br>
     *          Date: 08/28/2008 <br>
     *          Author: MCM Impl Team <br>
     *          Change: <br>
     *          MCM team ML Ticket merge FXP22064
     *
     */
    private synchronized void setPropertiesFromQueryResult(int key) throws Exception
    {

        this.setDealId( jExec.getInt(key, "DealId"));
        this.setCopyId( jExec.getInt(key, "CopyId"));
        this.setSourceOfBusinessProfileId( jExec.getInt(key, "SourceOfBusinessProfileId"));
        this.setApplicationId( jExec.getString(key, "ApplicationId"));
        this.setApplicationDate( jExec.getDate(key, "ApplicationDate"));
        this.setTotalLoanAmount( jExec.getDouble(key, "TotalLoanAmount"));
        this.setAmortizationTerm( jExec.getInt(key, "AmortizationTerm"));
        this.setEstimatedClosingDate( jExec.getDate(key, "EstimatedClosingDate"));
        this.setDealTypeId( jExec.getShort(key, "DealTypeId"));
        this.setLenderProfileId( jExec.getInt(key, "LenderProfileId"));
        this.setSourceApplicationId( jExec.getString(key, "SourceApplicationId"));
        this.setTotalPurchasePrice( jExec.getDouble(key, "TotalPurchasePrice"));
        this.setPostedInterestRate( jExec.getDouble(key, "PostedInterestRate"));
        this.setUnderwriterUserId( jExec.getInt(key, "UnderwriterUserId"));   // why userid is numeric?
        this.setEstimatedPaymentAmount( jExec.getDouble(key, "EstimatedPaymentAmount"));
        this.setPandiPaymentAmount( jExec.getDouble(key, "PandiPaymentAmount"));
        this.setEscrowPaymentAmount( jExec.getDouble(key, "EscrowPaymentAmount"));
        this.setMIPremiumAmount( jExec.getDouble(key, "MIPremiumAmount"));
        this.setNetLoanAmount( jExec.getDouble(key, "NetLoanAmount"));
        this.setStatusDate( jExec.getDate(key, "StatusDate"));
        this.setDiscount( jExec.getDouble(key, "Discount"));
        this.setSecondaryFinancing(jExec.getString(key, "SecondaryFinancing"));
        this.setOriginalMortgageNumber( jExec.getString(key, "OriginalMortgageNumber"));
        this.setDenialReasonId( jExec.getShort(key, "DenialReasonId"));
        this.setRenewalOptionsId( jExec.getShort(key, "RenewalOptionsId"));
        this.setNetInterestRate( jExec.getDouble(key, "NetInterestRate"));
        this.setRateDate( jExec.getDate(key, "RateDate"));
        this.setDealPurposeId( jExec.getShort(key, "DealPurposeId"));
        this.setBuydownRate( jExec.getDouble(key, "BuydownRate"));
        this.setSpecialFeatureId( jExec.getShort(key, "SpecialFeatureId"));
        this.setInterimInterestAmount( jExec.getDouble(key, "InterimInterestAmount"));
        this.setDownPaymentAmount( jExec.getDouble(key, "DownPaymentAmount"));
        this.setLineOfBusinessId( jExec.getShort(key, "LineOfBusinessId"));
        this.setMortgageInsurerId( jExec.getShort(key, "MortgageInsurerId"));
        this.setMtgProdId( jExec.getInt(key, "MtgProdId"));
        this.setMIPolicyNumber( jExec.getString(key, "MIPolicyNumber"));
        this.setFirstPaymentDate( jExec.getDate(key, "FirstPaymentDate"));
        this.setNetPaymentAmount( jExec.getDouble(key, "NetPaymentAmount"));
        this.setMARSNameSearch( jExec.getString(key, "MARSNameSearch"));
        this.setMARSPropertySearch( jExec.getString(key, "MARSPropertySearch"));
        this.setMILoanNumber( jExec.getString(key, "MILoanNumber"));
        this.setCommisionAmount( jExec.getDouble(key, "CommisionAmount"));
        this.setMaturityDate( jExec.getDate(key, "MaturityDate"));
        this.setCommitmentIssueDate( jExec.getDate(key, "CommitmentIssueDate"));
        this.setCommitmentAcceptDate( jExec.getDate(key, "CommitmentAcceptDate"));
        this.setRefExistingMtgNumber( jExec.getString(key, "RefExistingMtgNumber"));
        this.setCombinedTotalLiabilities( jExec.getDouble(key, "CombinedTotalLiabilities"));
        this.setBankABANumber( jExec.getString(key, "BankABANumber"));
        this.setBankAccountNumber( jExec.getString(key, "BankAccountNumber"));
        this.setBankName( jExec.getString(key, "BankName"));
        this.setActualClosingDate( jExec.getDate(key, "ActualClosingDate"));
        this.setCombinedTotalAssets( jExec.getDouble(key, "CombinedTotalAssets"));
        this.setCombinedTotalIncome( jExec.getDouble(key, "CombinedTotalIncome"));
        this.setCombinedTDS( jExec.getDouble(key, "CombinedTDS"));
        this.setCombinedGDS( jExec.getDouble(key, "CombinedGDS"));
        this.setRenewalDate( jExec.getDate(key, "RenewalDate"));
        this.setNumberOfBorrowers( jExec.getShort(key, "NumberOfBorrowers"));
        this.setNumberOfGuarantors( jExec.getShort(key, "NumberOfGuarantors"));
        this.setServicingMortgageNumber( jExec.getString(key, "ServicingMortgageNumber"));
        this.setPaymentTermId( jExec.getShort(key, "PaymentTermId"));
        this.setInterestTypeId( jExec.getShort(key, "InterestTypeId"));
        this.setPaymentFrequencyId( jExec.getShort(key, "PaymentFrequencyId"));
        this.setPrePaymentOptionsId( jExec.getShort(key, "PrePaymentOptionsId"));
        this.setRateFormulaId( jExec.getShort(key, "RateFormulaId"));
        this.setStatusId( jExec.getShort(key, "StatusId"));
        this.setRiskRatingId( jExec.getShort(key, "RiskRatingId"));
        this.setElectronicSystemSource( jExec.getString(key, "ElectronicSystemSource"));
        this.setCommitmentExpirationDate( jExec.getDate(key, "CommitmentExpirationDate"));
        this.setRequestedSolicitorName( jExec.getString(key, "RequestedSolicitorName"));
        this.setRequestedAppraisorName( jExec.getString(key, "RequestedAppraisorName"));
        this.setInterestCompoundId( jExec.getInt(key, "InterestCompoundId"));
        this.setInvestorProfileId( jExec.getInt(key, "InvestorProfileId"));
        this.setSecondApproverId( jExec.getInt(key, "SecondApproverId"));
        this.setJointApproverId( jExec.getInt(key, "JointApproverId"));
        this.setBranchProfileId( jExec.getInt(key, "BranchProfileId"));
        this.setSourceFirmProfileId( jExec.getInt(key, "SourceFirmProfileId"));
        this.setGroupProfileId( jExec.getInt(key, "GroupProfileId"));
        this.setAdministratorId( jExec.getInt(key, "AdministratorId"));
        this.setPricingProfileId( jExec.getInt(key, "PricingProfileId"));
        this.setRefinanceNewMoney( jExec.getDouble(key, "RefinanceNewMoney"));
        this.setInterimInterestAdjustmentDate( jExec.getDate(key, "InterimInterestAdjustmentDate"));
        this.setPriviligePaymentId( jExec.getInt(key, "PrivilegePaymentId"));
        this.setAdvanceHold( jExec.getDouble(key, "AdvanceHold"));
        this.setClientNumber( jExec.getString(key, "ClientNumber"));
        this.setCombinedLTV( jExec.getDouble(key, "CombinedLTV"));
        this.setCombinedTotalAnnualHeatingExp( jExec.getDouble(key, "CombinedTotalAnnualHeatingExp"));
        this.setCombinedTotalAnnualTaxExp( jExec.getDouble(key, "CombinedTotalAnnualTaxExp"));
        this.setCombinedTotalCondoFeeExp( jExec.getDouble(key, "CombinedTotalCondoFeeExp"));
        this.setCombinedTotalPropertyExp( jExec.getDouble(key, "CombinedTotalPropertyExp"));
        this.setCommitmentPeriod( jExec.getInt(key, "CommitmentPeriod"));
        this.setCommitmentProduced( jExec.getString(key, "CommitmentProduced"));
        this.setCrossSellProfileId( jExec.getInt(key, "CrossSellProfileId"));
        this.setDatePreAppFirm( jExec.getDate(key, "DatePreAppFirm"));
        this.setFifthApproverId( jExec.getInt(key, "FifthApproverId"));
        this.setFinalApproverId( jExec.getInt(key, "FinalApproverId"));
        this.setFinanceProgramId( jExec.getInt(key, "FinanceProgramId"));
        this.setFourthApproverId( jExec.getInt(key, "FourthApproverId"));
        this.setInvestorApproved( jExec.getString(key, "InvestorApproved"));
        this.setInvestorConfirmationNumber( jExec.getString(key, "InvestorConfirmationNumber"));
        this.setLienHolderSecondaryFinancing( jExec.getDouble(key, "LienHolderSecondaryFinancing"));
        this.setLienPositionId( jExec.getInt(key, "LienPositionId"));
        this.setMIPayorId( jExec.getInt(key, "MIPayorId"));
        this.setMITypeId( jExec.getInt(key, "MITypeId"));
        this.setNewMoney( jExec.getDouble(key, "NewMoney"));
        this.setNextRateChangeDate( jExec.getDate(key, "NextRateChangeDate"));
        this.setNumberOfProperties( jExec.getInt(key, "NumberOfProperties"));
        this.setOldMoney( jExec.getDouble(key, "OldMoney"));
        this.setOnHoldDate( jExec.getDate(key, "OnHoldDate"));
        this.setPreApproval(jExec.getString(key, "PreApproval"));
        this.setPremium( jExec.getDouble(key, "Premium"));
        this.setRateLock(jExec.getString(key, "RateLock"));
        this.setRateLockExpirationDate( jExec.getDate(key, "RateLockExpirationDate"));
        this.setRateLockPeriod( jExec.getInt(key, "RateLockPeriod"));
        this.setReferenceDealNumber( jExec.getString(key, "ReferenceDealNumber"));
        this.setReferenceDealTypeId( jExec.getInt(key, "ReferenceDealTypeId"));
        this.setRefiCurrentBal( jExec.getDouble(key, "RefiCurrentBal"));
        this.setRemainingBalanceSecondaryFin( jExec.getDouble(key, "RemainingBalanceSecondaryFin"));
        this.setReturnDate( jExec.getDate(key, "ReturnDate"));
        this.setSecondaryFinancingLienHolder( jExec.getString(key, "SecondaryFinancingLienHolder"));
        this.setSourceRefAppNBR( jExec.getString(key, "SourceRefAppNBR"));
        this.setSourceSystemMailBoxNBR( jExec.getString(key, "SourceSystemMailBoxNBR"));
        this.setTaxPayorId( jExec.getInt(key, "TaxPayorId"));
        this.setThirdApproverId( jExec.getInt(key, "ThirdApproverId"));
        this.setTotalActAppraisedValue( jExec.getDouble(key, "TotalActAppraisedValue"));
        this.setTotalEstAppraisedValue( jExec.getDouble(key, "TotalEstAppraisedValue"));
        this.setTotalPropertyExpenses( jExec.getDouble(key, "TotalPropertyExpenses"));
        this.setAdditionalPrincipal( jExec.getDouble(key, "AdditionalPrincipal"));
        this.setTotalPaymentAmount( jExec.getDouble(key, "TotalPaymentAmount"));
        this.setActualPaymentTerm( jExec.getInt(key, "ActualPaymentTerm"));
        this.setCombinedGDSBorrower( jExec.getDouble(key, "CombinedGDSBorrower"));
        this.setCombinedTDSBorrower( jExec.getDouble(key, "CombinedTDSBorrower"));
        this.setCombinedGDS3Year( jExec.getDouble(key, "CombinedGDS3Year"));
        this.setCombinedTDS3Year( jExec.getDouble(key, "CombinedTDS3Year"));
        this.setCombinedTotalEquityAvailable( jExec.getDouble(key, "CombinedTotalEquityAvailable"));
        this.setEffectiveAmortizationMonths( jExec.getInt(key, "EffectiveAmortizationMonths"));
        this.setPostedRate( jExec.getDouble(key, "PostedRate"));
        this.setBlendedRate( jExec.getDouble(key, "BlendedRate"));
        this.setChannelMedia( jExec.getString(key, "ChannelMedia"));
        this.setClosingTypeId( jExec.getInt(key, "ClosingTypeId"));
        this.setMIUpfront( jExec.getString(key, "MIUpfront"));
        this.setPreApprovalConclusionDate( jExec.getDate(key, "PreApprovalConclusionDate"));
        this.setPreApprovalOriginalOfferDate( jExec.getDate(key, "PreApprovalOriginalOfferDate"));
        this.setPreviouslyDeclined( jExec.getString(key, "PreviouslyDeclined"));
        this.setRequiredDownpayment( jExec.getDouble(key, "RequiredDownpayment"));
        this.setMIIndicatorId( jExec.getInt(key, "MIIndicatorId"));
        this.setPreviousMIIndicatorId( jExec.getInt(key, "previousMIIndicatorId"));
        this.setRepaymentTypeId( jExec.getInt(key, "RepaymentTypeId"));
        this.setDecisionModificationTypeId( jExec.getInt(key, "DecisionModificationTypeId"));
        this.setScenarioApproved( jExec.getString(key, "ScenarioApproved"));
        this.setScenarioRecommended( jExec.getString(key, "ScenarioRecommended"));
        this.setScenarioNumber( jExec.getInt(key, "ScenarioNumber"));
        this.setScenarioDescription( jExec.getString(key, "ScenarioDescription"));
        this.setMIComments( jExec.getString(key, "MIComments"));
        this.setMIStatusId( jExec.getInt(key, "MIStatusId"));
        this.setSystemTypeId( jExec.getInt(key, "SystemTypeId"));
        this.setTotalFeeAmount( jExec.getDouble(key, "TotalFeeAmount"));
        this.setMaximumLoanAmount( jExec.getDouble(key, "MaximumLoanAmount"));
        this.setScenarioLocked( jExec.getString(key, "ScenarioLocked"));
        this.setTotalLoanBridgeAmount( jExec.getDouble(key, "TotalLoanBridgeAmount"));
        this.setSolicitorSpecialInstructions( jExec.getString(key, "SolicitorSpecialInstructions"));
        this.setMortgageInsuranceResponse(readMortgageInsuranceResponse());
        this.setCombinedLendingValue(jExec.getDouble(key, "CombinedLendingValue"));
        this.setExistingLoanAmount(jExec.getDouble(key, "ExistingLoanAmount"));
        this.setGDSTDS3YearRate(jExec.getDouble(key, "GDSTDS3YearRate"));
        this.setMaximumPrincipalAmount(jExec.getDouble(key, "MaximumPrincipalAmount"));
        this.setMaximumTDSExpensesAllowed(jExec.getDouble(key, "MaximumTDSExpensesAllowed"));
        this.setMICommunicationFlag(jExec.getString(key, "MICommunicationFlag"));
        this.setMIExistingPolicyNumber(jExec.getString(key, "MIExistingPolicyNumber"));
        this.setMinimumIncomeRequired(jExec.getDouble(key, "MinimumIncomeRequired"));
        this.setPAPurchasePrice(jExec.getDouble(key, "PAPurchasePrice"));
        this.setInstructionProduced(jExec.getString(key, "InstructionProduced"));
        this.setTeaserDiscount(jExec.getDouble(key, "TeaserDiscount"));
        this.setTeaserMaturityDate(jExec.getDate(key, "TeaserMaturityDate"));
        this.setTeaserNetInterestRate(jExec.getDouble(key, "TeaserNetInterestRate"));
        this.setTeaserPIAmount(jExec.getDouble(key, "TeaserPIAmount"));
        this.setTeaserTerm(jExec.getInt(key, "TeaserTerm"));
        this.setCopyType(jExec.getString(key, "CopyType"));
        this.setOpenMIResponseFlag(jExec.getString(key, "OpenMIResponseFlag"));
        this.setResolutionConditionId(jExec.getInt(key, "ResolutionConditionId"));

        this.setCheckNotesFlag(jExec.getString(key, "CheckNotesFlag"));
        this.setClosingDatePlus90Days( jExec.getDate ( key, "ClosingDatePlus90Days" ) ) ;

        // New fields for new calculation changes -- Billy 23Nov2001
        this.setMIPremiumPST(jExec.getDouble(key, "MIPremiumPST"));
        this.setPerdiemInterestAmount(jExec.getDouble(key, "PerdiemInterestAmount"));
        this.setIADNumberOfDays(jExec.getInt(key, "IADNumberOfDays"));
        this.setPandIPaymentAmountMonthly(jExec.getDouble(key, "PandIPaymentAmountMonthly"));
        this.setFirstPaymentDateMonthly( jExec.getDate ( key, "FirstPaymentDateMonthly" ) ) ;
        this.setFunderProfileId(jExec.getInt(key, "FunderProfileId"));

        // ==========================================================

        // New fields for CMHC modification -- Billy 07Feb2002
        this.setProgressAdvance(jExec.getString(key, "ProgressAdvance"));
        this.setAdvanceToDateAmount(jExec.getDouble(key, "AdvanceToDateAmount"));
        this.setAdvanceNumber(jExec.getInt(key, "AdvanceNumber"));
        this.setMIRUIntervention(jExec.getString(key, "MIRUIntervention"));
        this.setPreQualificationMICertNum(jExec.getString(key, "PreQualificationMICertNum"));
        this.setRefiPurpose(jExec.getString(key, "RefiPurpose"));
        this.setRefiOrigPurchasePrice(jExec.getDouble(key, "RefiOrigPurchasePrice"));
        this.setRefiBlendedAmortization(jExec.getString(key, "RefiBlendedAmortization"));
        this.setRefiOrigMtgAmount(jExec.getDouble(key, "RefiOrigMtgAmount"));
        this.setRefiImprovementsDesc(jExec.getString(key, "RefiImprovementsDesc"));
        this.setRefiImprovementAmount(jExec.getDouble(key, "RefiImprovementAmount"));
        this.setNextAdvanceAmount(jExec.getDouble(key, "NextAdvanceAmount"));
        this.setRefiOrigPurchaseDate( jExec.getDate ( key, "RefiOrigPurchaseDate" ) ) ;
        this.setRefiCurMortgageHolder(jExec.getString(key, "RefiCurMortgageHolder"));
        // ==========================================================

        //// SYNCADD.
        // New field for SOB resubmission handling -- By Billy 12June2002
        this.setResubmissionFlag(jExec.getString(key, "ResubmissionFlag"));
        //===============================================================

        //===============================================================
        // New fields to handle MIPolicyNum problem : the number lost if user cancel MI
        //  and re-send again later.  -- Modified by Billy 14July2003
        this.setMIPolicyNumCMHC(jExec.getString(key, "MIPolicyNumCMHC"));
        this.setMIPolicyNumGE(jExec.getString(key, "MIPolicyNumGE"));
        //===============================================================

        //-- FXLink Phase II --//
        //--> New Field for Market Type Indicator
        //--> By Billy 18Nov2003
        this.setMccMarketType(jExec.getString(key, "MccMarketType"));
        //===============================================================

        //==============================================================
        //--> Ticket#127 -- BMO Tracking broker Commissions
        //--> New field added to indicate if the Broker is paid or not
        //--> By Billy 24Nov2003
        this.setBrokerCommPaid(jExec.getString(key, "BrokerCommPaid"));
        //===============================================================
        //==============================================================
        //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
        //--> New Field for HoldReasonId
        //--> By Billy 06Jan2004
        this.setHoldReasonId(jExec.getInt(key, "HoldReasonId"));
        //--DJ_LI_CR--start//
        this.setPmntPlusLifeDisability(jExec.getDouble(key, "PmntPlusLifeDisability"));
        this.setLifePremium(jExec.getDouble(key, "LifePremium"));
        this.setDisabilityPremium(jExec.getDouble(key, "DisabilityPremium"));
        this.setCombinedPremium(jExec.getDouble(key, "CombinedPremium"));
        this.setInterestRateIncrement(jExec.getDouble(key, "InterestRateIncrement"));
        this.setInterestRateIncLifeDisability(jExec.getDouble(key, "InterestRateIncLifeDisability"));
        this.setEquivalentInterestRate(jExec.getDouble(key, "EquivalentInterestRate"));
        //--DJ_LI_CR--end//

        //--DJ_CR010--start//
        this.setCommisionCode(jExec.getString(key, "CommisionCode"));
        //--DJ_CR010--end//

        //--DJ_CR203.1--start//
        this.setMultiProject(jExec.getString(key, "MultiProject"));
        this.setProprietairePlus(jExec.getString(key, "ProprietairePlus"));
        this.setProprietairePlusLOC(jExec.getDouble(key, "ProprietairePlusLOC"));
        //--DJ_CR203.1--end//
        // Ticket #433 -- new field for FX Link II
        //--> By Billy 14June2004
        this.setSourceApplicationVersion(jExec.getString(key, "SourceApplicationVersion"));

        //======================================== ActiveX Ver 1.0.2 Start ========================================
        // -- DJ Ticket#499 - Catherine  --
        this.setAddCreditcostInsIncurred(jExec.getDouble(key, "AddCreditcostInsIncurred"));
        this.setTotalCreditCost(jExec.getDouble(key, "TotalCreditCost"));
        this.setIntWithoutInsCreditCost(jExec.getDouble(key, "IntWithoutInsCreditCost"));
        // -- DJ Ticket#499 - Catherine end --
        //======================================== ActiveX Ver 1.0.2 End ==========================================
        this.setUploadSerialNumber(jExec.getInt(key, "UploadSerialNumber")); // Mike, March 11, 2005 - Cervus Phase II

        this.setFundingUploadDone(jExec.getString(key, "FundingUploadDone")); // Catherine, 10-Jan-05, XD to MCAP

        //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        this.setPrimeBaseAdj(jExec.getDouble(key, "PrimeBaseAdj"));
        this.setPrimeIndexId(jExec.getInt(key, "PrimeIndexId"));
        this.setPrimeIndexRate(jExec.getDouble(key, "PrimeIndexRate"));
        //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

        // SEAN DJ SPEC-Progress Advance Type July 19, 2005: Load the data from
        // database search result.
        this.setProgressAdvanceTypeId(jExec.getShort(key, "ProgressAdvanceTypeId"));
        // SEAN DJ SPEC-PAT END
        // SEAN GECF Income Verification Document Type Drop Down,
        this.setIncomeVerificationTypeId(jExec.getShort(key, "IncomeVerificationTypeId"));
        // END GECF IV Type drop Down
        this.setClosingUploadDone(jExec.getString(key, "ClosingUploadDone"));  // Catherine, 6-Oct-05, GE to FCT

        // START - MI Additions
        this.setProductTypeId(jExec.getInt(key, "ProductTypeId")); 
        this.setProgressAdvanceInspectionBy(jExec.getString(key, "ProgressAdvanceInspectionBy")); 
        this.setSelfDirectedRRSP(jExec.getString(key, "SelfDirectedRRSP")); 
        this.setCmhcProductTrackerIdentifier(jExec.getString(key, "CmhcProductTrackerIdentifier")); 
        this.setLocRepaymentTypeId(jExec.getInt(key, "LocRepaymentTypeId")); 
        this.setRequestStandardService(jExec.getString(key, "RequestStandardService"));
        this.setLocAmortizationMonths(jExec.getInt(key, "LocAmortizationMonths"));
        this.setLocInterestOnlyMaturityDate(jExec.getDate(key, "LocInterestOnlyMaturityDate"));
        this.setRefiProductTypeId(jExec.getInt(key, "RefiProductTypeId")); 
        this.setPandIUsing3YearRate(jExec.getDouble(key, "PandIUsing3YearRate")); 
        //END - MI Additions
        //AAtcha starts
        this.setMIPolicyNumAIGUG(jExec.getString(key, "MIPolicyNumAIGUG"));
        this.setMIFeeAmount(jExec.getDouble(key, "MIFeeAmount"));
        this.setMIPremiumErrorMessage(jExec.getString(key, "MIPremiumErrorMessage"));
        //AAtcha ends

        this.setRateGuaranteePeriod(jExec.getInt(key, "RateGuaranteePeriod"));
        //***** Change by NBC/PP Implementation Team - Version 1.5 - Start *****//      
        this.setFloatDownTaskCreatedFlag(jExec.getString(key, "FloatDownTaskCreatedFlag"));
        this.setFloatDownCompletedFlag(jExec.getString(key, "FloatDownCompletedFlag"));
        // ***** Change by NBC/PP Implementation Team - Version 1.5 - End

        // ***** Change by NBC Impl. Team - Version 1.9 - Start *****//
        this.setCashBackAmount(jExec.getDouble(key, "CashBackAmount"));
        this.setCashBackPercent(jExec.getDouble(key, "CashBackPercent"));
        this.setCashBackAmountOverride(jExec.getString(key, "CashBackAmountOverride"));
        // ***** Change by NBC Impl. Team - Version 1.9 - End*****//

        //    ***** Change by NBC Impl. Team - Version 1.10 - Start *****//
        this.setAffiliationProgramId(jExec.getInt(key, "AffiliationProgramId"));
        //    ***** Change by NBC Impl. Team - Version 1.10 - End *****//

        //    ***** Change by NBC Impl. Team - Version 1.11 - Start *****//
        
        /******* MCM team ML Ticket merge FXP22064 start ******/
        this.setTotalCostOfBorrowing(jExec.getDouble(key, "TOTALCOSTOFBORROWING"));
        /******* MCM team ML Ticket merge FXP22064 end ******/
        this.setBalanceRemainingAtEndOfTerm(jExec.getDouble(key, "BalanceRemainingAtEndOfTerm"));
        //    ***** Change by NBC Impl. Team - Version 1.11 - End *****//

        //    ***** Change by NBC Impl. Team - Version 1.12 - Start *****//
        //  this.setProductTypeId(jExec.getInt(key,++index));
        //  this.setLocRepaymentTypeId(jExec.getInt(key,++index));
        //    ***** Change by NBC Impl. Team - Version 1.12 - End *****//
        //    ***** added for ML  - Start *****//
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        //    ***** added for ML - End *****//

        //    ***** Change by NBC Impl. Team - Version 1.13 - Start *****//
        this.setInterestOverTerm(jExec.getDouble(key, "InterestOverTerm"));
        this.setTeaserRateInterestSaving(jExec.getDouble(key, "TeaserRateInterestSaving"));
        //    ***** Change by NBC Impl. Team - Version 1.13 - End *****//

        //    ***** added for CR03 - 3.2Gen  - Start *****//
        this.setAutoCalcCommitExpiryDate(jExec.getString(key, "AutoCalcCommitExpiryDate"));
        //    ***** added for CR03 - 3.2Gen - End *****//
        this.setMIPolicyNumPMI(jExec.getString(key,"MIPolicyNumPMI"));

        // Outstanding Credit Scoring Response flag
        this.setOutCreditScoreResponse(jExec.getString(key, "OutCreditScoreResponse"));     
        this.setCriticalChangeFlag(jExec.getString(key, "CriticalChangeFlag"));

        /***************MCM Impl team changes starts - XS_2.7 *******************/
        this.setRefiAdditionalInformation(jExec.getString(key, "RefiAdditionalInformation"));
        /***************MCM Impl team changes ends - XS_2.7 *******************/

        //4.3GR, Update Total Loan Amount -- start
        this.setMIPremiumAmountChangeNortified(jExec.getString(key, "miPremiumAmountChangeNortified"));
        this.setMIPremiumAmountPrevious(jExec.getDouble(key, "miPremiumAmountPrevious"));
        //4.3GR, Update Total Loan Amount -- end
        
        this.setFinancingWaiverDate(jExec.getDate(key, "financingWaiverDate"));

        //4.4 Submission Agent
        this.setSourceOfBusinessProfile2ndaryId(jExec.getInt(key, "sourceOfBusinessProfile2ndary"));
        
        //Qualify Rate
        this.setQualifyingOverrideFlag(jExec.getString(key, "qualifyingOverrideFlag"));
        this.setQualifyingRateOverrideFlag(jExec.getString(key, "qualifyingRateOverrideFlag"));
        
        //if ("Y".equals(yorn)) this.setQualifyingOverrideFlag("Y");
        //else this.setQualifyingOverrideFlag("N");
        
        this.setOverrideQualProd(jExec.getInt(key, "overrideQualProd"));
        
        this.setMIDealRequestNumber(jExec.getInt(key, "MIDealRequestNumber"));
    }

    /*
     * assignCurrentApprover:
     *
     * This call is made to assign the approver to the deal, not to record the fact that the approver
     * actually approverd the deal.
     *
     * PROGRAMMER NOTES
     *
     * Since a deal may require approval by multiple underwriters we require a means to track the current
     * underwriter examining the deal - there are fields for the assigned underwriter, up to 4 additional
     * higher/second approvers, and for a joint approver. This satisfies the storage requirements for the
     * maximum number of users required to approve the deal according to the rules of the current
     * Approval Authority Guideline module in use (and hopefully for any AAG module that may be introduced
     * in the future!).
     *
     * However, there is no property that can tell us information about the current approver - e.g. is the
     * current approver the second higher approver, etc.
     *
     * This information can be recorded via a simple 'current approver' index value, as follows:
     *
     *    0 - current underwriter is underwriter assigned to deal (actually AAG does not set this value -
     *        it is the default when the deal record is created)
     *
     *    1 - current underwriter is the 1st Higher Underwriter
     *    2 - current underwriter is the 2nd Higher Underwriter
     *    3 - current underwriter is the 3rd Higher Underwriter
     *    4 - current underwriter is the 4th Higher Underwriter
     *    5 - current underwriter is joint underwriter
     *
     * However, if a joint approver has been assigned to the deal we directly know the profile id of
     * the current approver via getJointApproverId(). A joint approver may be assigned before all possible
     * higher approvers are used so we always check for a joint approver assignment first - for example the
     * current approver may be 3, yet a joint approver assigned - this would mean the current prior to
     * the joint approver was the 3rh Higher approver.
     *
     * Hence we could implement a property to hold the current approver that tests for a joint approver
     * assignment and if there isn't one, determined the current approver based upon the current approver
     * index. The method currentApproverId() return the profile id of the current approver based on the
     * information above.
     *
     * However, the deal does not have a field to store the current approver index. Instead, we store the
     * value in the <finalApproverId> field. This is safe since the final approver id field is only populated
     * at the time of a final approval decision - it has no meaning prior to final approval. The following
     * getter and setter methods are simply cover methods that access the underlying <finalApproverId>
     * property - used to make the code more readable!
     *
     *              getCurrentApproverNdx()  --> equivalent to getFinalApproverId()
     *              setCurrentApproverNdx()  --> equivalent to settFinalApproverId()
     *
     **/

    public void assignCurrentApprover(int approverUserId, boolean isJointApprover)
    {
        if (isJointApprover == true)
        {
            setJointApproverId(approverUserId); // but .. case where task re-routed!!!
            setCurrentApproverNdx(5); // even if we didn't actually go through all higher approvers!

            return;
        }

        // not joint approver - then must be 'next' higher approve

        setCurrentApproverNdx(getCurrentApproverNdx() + 1); // advance current approver index

        switch (getCurrentApproverNdx())
        {
        case 1:
            setSecondApproverId(approverUserId);
            break;

        case 2:
            setThirdApproverId(approverUserId);
            break;

        case 3:
            setFourthApproverId(approverUserId);
            break;

        case 4:
            setFifthApproverId(approverUserId);
            break;
        }
    }

    public void transferApproveralHistory(Deal other)
    {
        other.setUnderwriterUserId(getUnderwriterUserId());
        other.setSecondApproverId(getSecondApproverId());
        other.setThirdApproverId(getThirdApproverId());
        other.setFourthApproverId(getFourthApproverId());
        other.setFifthApproverId(getFifthApproverId());
        other.setJointApproverId(getJointApproverId());
        other.setFinalApproverId(getFinalApproverId());
    }

    public void clearApproveralHistory()
    {
        setSecondApproverId(0);
        setThirdApproverId(0);
        setFourthApproverId(0);
        setFifthApproverId(0);
        setJointApproverId(0);
        setFinalApproverId(0);
    }


    /*
     * recordApproval:
     *
     * This call is made record the profile id of an approver that approves the deal. Assignment of the
     * approver (via assignCurrentApprover()) happens prior to actual approval. This call is necessary
     * upon approval because the approver meed not necessarilly be the same user previously assigned -
     * i.e. due to a task re-route for example that directs another underwriter to grant the approval.
     *
     **/

    public void recordApproval(int approverUserId)
    {
        if (getJointApproverId() > 0)
        {
            setJointApproverId(approverUserId); // had progressed to final joint approval
            return;
        }

        switch (getCurrentApproverNdx())
        {
        case 0:
            // the underwriter who actual provids initial approval 'owns' the deal
            setUnderwriterUserId(approverUserId);
            return;

        case 1:
            setSecondApproverId(approverUserId);
            break;

        case 2:
            setThirdApproverId(approverUserId);
            break;

        case 3:
            setFourthApproverId(approverUserId);
            break;

        case 4:
            setFifthApproverId(approverUserId);
            break;
        }
    }

    /*
     * currentApproverUserId:
     *
     * Returns the user id of the current approver assigned to the deal.
     *
     **/

    public int currentApprovedUserId()
    {
        if (getJointApproverId() > 0)
            return getJointApproverId();

        switch (getCurrentApproverNdx())
        {
        case 0:
            return getUnderwriterUserId();

        case 1:
            return getSecondApproverId();

        case 2:
            return getThirdApproverId();

        case 3:
            return getFourthApproverId();

        }

        // must be fifth (last higher approver)
        return getFifthApproverId();
    }

    /*
     * Called to determine if selection of a higher/second approver is allowed. Currently up to
     * four higher/second approvals are allowed - after that a joint approver must be sought
     *
     **/

    public boolean higherApproverAllowed()
    {
        return getCurrentApproverNdx() < 4;
    }

    public int getCurrentApproverNdx()
    {
        return getFinalApproverId();
    }

    public void setCurrentApproverNdx(int ndx)
    {
        setFinalApproverId(ndx);
    }

    /**
     *  gets the value of instance fields as String.
     *  if a field does not exist (or the type is not serviced)** null is returned.
     *  if the field exists (and the type is serviced) but the field is null an empty
     *  String is returned.
     *  @returns value of bound field as a String;
     *  @param  fieldName as String
     *
     *  Other entity types are not yet serviced   ie. You can't get the Province object
     *  for province.
     */
    public String getStringValue(String fieldName)
    {
        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public Object getFieldValue(String fieldName){
        return doGetFieldValue(this, this.getClass(), fieldName);
    }

    /**
     *  sets the value of instance fields as String.
     *  if a field does not exist or is not serviced an Exception is thrown
     *  if the field exists (and the type is serviced) but the field is null an empty
     *  String is returned.
     *
     *  @param  fieldName as String
     *  @param  the value as a String
     *
     *  Other entity types are not yet serviced   ie. You can't set the Province object
     *  in Addr.
     */
    public void setField(String fieldName, String value) throws Exception
    {
        doSetField(this, this.getClass(), fieldName, value);
    }

    public String getEntityTableName()
    {
        return "Deal";
    }

    public List getAssociatedEntities() throws Exception
    {
        List retEntitiesList = new ArrayList();

        retEntitiesList.addAll(this.getPartyProfiles());

        return retEntitiesList;
    }


    public List getChildren() throws Exception
    {
        //do not alter the order of the calls this method:
        //Mercator relies on the order that results!!!!
        //any new calls to addAll should go after existing calls.
        List children = new ArrayList();

        children.addAll(this.getProperties());
        children.addAll(this.getBorrowers());

        Object o =  this.getBridge();
        if (o != null)
            children.add(o);

        children.addAll(this.getDocumentTracks());
        children.addAll(this.getEscrowPayments());
        children.addAll(this.getDownPaymentSources());
        children.addAll(this.getDealFees());

        //--DJ_LDI_CR--start--//
        // Add InsureOnlyApplicants to the Children list if any
        children.addAll(this.getInsureOnlyApplicants());
        //--DJ_LDI_CR--end--//

        children.addAll(this.getRequests());   // Catherine: for AVM

        /***************MCM Impl team changes starts - @version 1.18*******************/
        logger.debug("getChildren: Deal");
        children.addAll(this.getComponents()); 
        children.addAll(this.getComponentSummary()); 
        /***************MCM Impl team changes ends - @version 1.18*******************/
        
        return children;
    }


    protected int performUpdate()  throws Exception
    {
        Class clazz = this.getClass();

        // modified by BILLY to add special handle for mortgageInsuranceResponse field
        // Check if mortgageInsuranceResponse changed
        if(this.isValueChange("mortgageInsuranceResponse"))
        {
            // Perform the change by PrepareStatement (by use special update method)
            this.updateMortgageInsuranceResponse();
            // Reset the value changed flag to avoid update again
            this.resetChange("mortgageInsuranceResponse");
        }
        // ===========================================================================

        int ret = doPerformUpdate(this, clazz);
        // Optimization:
        // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
        if(dcm != null && ret >0 ) dcm.inputEntity(this);

        return ret;
    }


    /**
     *   getS the postedInterestRate associated with this entity
     *
     *   @return a double
     */
    public double getPostedInterestRate()
    {
        return this.postedInterestRate ;
    }

    /**
     *   getS the amortizationTerm associated with this entity
     *
     *   @return a int
     */
    public int getAmortizationTerm()
    {
        return this.amortizationTerm ;
    }

    /**
     *   getS the interimInterestAmount associated with this entity
     *
     *   @return a double
     */
    public double getInterimInterestAmount()
    {
        return this.interimInterestAmount ;
    }



    /**
     *   getS the applicationId associated with this entity
     *
     *   @return a String
     */
    public String getApplicationId()
    {
        return this.applicationId ;
    }

    /**
     *   getS the netLoanAmount associated with this entity
     *
     *   @return a double
     */
    public double getNetLoanAmount()
    {
        return this.netLoanAmount ;
    }

    /**
     *   getS the lineOfBusinessId associated with this entity
     *
     *   @return a int
     */
    public int getLineOfBusinessId()
    {
        return this.lineOfBusinessId ;
    }

    /**
     *   getS the combinedTotalIncome associated with this entity
     *
     *   @return a double
     */
    public double getCombinedTotalIncome()
    {
        return this.combinedTotalIncome ;
    }

    /**
     *   getS the sergvicingMortgageNumber associated with this entity
     *
     *   @return a String
     */
    public String getServicingMortgageNumber()
    {
        return this.servicingMortgageNumber ;
    }

    /**
     *   getS the paymentFrequencyId associated with this entity
     *
     *   @return a int
     */
    public int getPaymentFrequencyId()
    {
        return this.paymentFrequencyId ;
    }

    /**
     *   getS the mtgProd associated with this entity
     *
     *   @return a MtgProd
     */
    public MtgProd getMtgProd()throws FinderException, RemoteException
    {
        return new MtgProd(srk,null,this.mtgProdId);
    }


    /**
     *   getS the MIPolicyNumber associated with this entity
     *
     *   @return a String
     */
    public String getMIPolicyNumber()
    {
        return this.MIPolicyNumber ;
    }

    /**
     *   getS the sourceOfBusinessProfile associated with this entity
     *
     *   @return a SourceOfBusinessProfile
     */
    public SourceOfBusinessProfile getSourceOfBusinessProfile()throws FinderException, RemoteException
    {
        return new SourceOfBusinessProfile(srk,this.getSourceOfBusinessProfileId());
    }

    /**
     *   getS the requestedAppraisorName associated with this entity
     *
     *   @return a String
     */
    public String getRequestedAppraisorName()
    {
        return this.requestedAppraisorName ;
    }

    /**
     *   getS the statusId associated with this entity
     *
     *   @return a int
     */
    public int getStatusId()
    {
        return this.statusId ;
    }
    //// SYNCADD.
    // For performance issue to avoid "Select * from Deal" (bad performance)
    //   -- By BILLY 20Aug2002
    public int getStatusId(int dealId, int copyId)throws RemoteException, FinderException
    {
        int theStatusId = 0;
        boolean gotRecord = false;
        String sql = "Select statusid from Deal where dealId = " +
        dealId + " AND copyId = " + copyId;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                theStatusId = jExec.getInt(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @getStatusId(dealid = " + dealId + ", copyid = " + copyId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - getCopyType() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return theStatusId;
    }
    //=======================================================

    /**
     *   getS the sourceApplicationId associated with this entity
     *
     *   @return a String
     */
    public String getSourceApplicationId()
    {
        return this.sourceApplicationId ;
    }

    /**
     *   getS the denialReasonId associated with this entity
     *
     *   @return a int
     */
    public int getDenialReasonId()
    {
        return this.denialReasonId ;
    }
    //// SYNCADD.
    // For performance issue to avoid "Select * from Deal" (bad performance)
    //   -- By BILLY 20Aug2002
    public int getDenialReasonId(int dealId, int copyId)throws RemoteException, FinderException
    {
        int theId = 0;
        boolean gotRecord = false;
        String sql = "Select denialreasonid from Deal where dealId = " +
        dealId + " AND copyId = " + copyId;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                theId = jExec.getInt(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @getDenialReasonId(dealid = " + dealId + ", copyid = " + copyId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - getCopyType() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return theId;
    }
    //=======================================================

    /**
     *   getS the combinedTotalLiabilities associated with this entity
     *
     *   @return a double
     */
    public double getCombinedTotalLiabilities()
    {
        return this.combinedTotalLiabilities ;
    }



    /**
     *   getS the MILoanNumber associated with this entity
     *
     *   @return a String
     */
    public String getMILoanNumber()
    {
        return this.MILoanNumber ;
    }

    /**
     *   getS the dealId associated with this entity
     *
     *   @return a int
     */
    public int getDealId()
    {
        return this.dealId ;
    }

    /**
     *   getS the netPaymentAmount associated with this entity
     *
     *   @return a double
     */
    public double getNetPaymentAmount()
    {
        return this.netPaymentAmount ;
    }


    /**
     *   getS the renewalOptionsId associated with this entity
     *
     *   @return a int
     */
    public int getRenewalOptionsId()
    {
        return this.renewalOptionsId ;
    }

    /**
     *   getS the interestTypeId associated with this entity
     *
     *   @return a int
     */
    public int getInterestTypeId()
    {
        return this.interestTypeId ;
    }


    /**
     *   getS the combinedTDS associated with this entity
     *
     *   @return a double
     */
    public double getCombinedTDS()
    {
        return this.combinedTDS ;
    }

    /**
     *   getS the mtgProdId associated with this entity
     *
     *   @return a int
     */
    public int getMtgProdId()
    {
        return this.mtgProdId ;
    }

    /**
     *   getS the pk associated with this entity
     *
     *   @return a DealPK
     */
    public IEntityBeanPK getPk()
    {
        return (IEntityBeanPK)this.pk ;
    }

    /**
     *   getS the renewalDate associated with this entity
     *
     *   @return a Date
     */
    public Date getRenewalDate()
    {
        return this.renewalDate ;
    }

    /**
     *   getS the originalMortgageNumber associated with this entity
     *
     *   @return a String
     */
    public String getOriginalMortgageNumber()
    {
        return this.originalMortgageNumber ;
    }

    /**
     *   getS the totalLoanAmount associated with this entity
     *
     *   @return a double
     */
    public double getTotalLoanAmount()
    {
        return this.totalLoanAmount ;
    }

    /**
     *   getS the dealPurposeId associated with this entity
     *
     *   @return a int
     */
    public int getDealPurposeId()
    {
        return this.dealPurposeId ;
    }

    /**
     *   getS the bankAccountNumber associated with this entity
     *
     *   @return a String
     */
    public String getBankAccountNumber()
    {
        return this.bankAccountNumber ;
    }


    /**
     *   getS the commitmentExpirationDate associated with this entity
     *
     *   @return a Date
     */
    public Date getCommitmentExpirationDate()
    {
        return this.commitmentExpirationDate ;
    }

    /**
     *   getS the properties associated with this entity as a Collection
     *
     *   @return a Collection of Property objects;
     */
    public Collection<DownPaymentSource> getDownPaymentSources() throws Exception
    {
        return new DownPaymentSource(srk,dcm).findByDeal(this.pk);
    }


    /**
     *   getS the properties associated with this entity as a Collection
     *
     *   @return a Collection of Property objects;
     */
    public Collection<Property> getProperties() throws Exception
    {
        return new Property(srk,dcm).findByDeal(this.pk);
    }

    /**
     *   @version 15 <br>
     * 	Date: 15-JUN-2008 <br>
     * 	Author: MCM Impl Team <br>
     *   getS the properties associated with this entity as a Collection
     *
     *   @return a Collection of Property objects;
     */
     public Collection getQualifyDetail() throws Exception
     {
         return new QualifyDetail(srk).findByDeal(this.pk);
     }


    /**
     *   getS the properties associated with this entity as a Collection
     *
     *   @return a Collection of PropertyExpense objects;
     */
    public Collection getPropertyExpenses() throws Exception
    {
        return new PropertyExpense(srk,dcm).findByDeal(this.pk);
    }

    public Collection getDocumentTracks() throws Exception
    {
        return new DocumentTracking(srk).findByDeal(this.pk);
    }

    public Collection getCreditBureauReports() throws Exception
    {
        return new CreditBureauReport(srk).findByDeal(this.pk);
    }

    public Collection getDealHistories() throws Exception
    {
        return new DealHistory(srk).findByDeal(this.pk);
    }

    /**
     *   getS the totalPurchasePrice associated with this entity
     *
     *   @return a double
     */
    public double getTotalPurchasePrice()
    {
        return this.totalPurchasePrice ;
    }

    /**
     *   getS the mortgageInsurerId associated with this entity
     *
     *   @return a int
     */
    public int getMortgageInsurerId()
    {
        return this.mortgageInsurerId ;
    }

    /**
     *   getS the underwriterUserId associated with this entity
     *
     *   @return a int
     */
    public int getUnderwriterUserId()
    {
        return this.underwriterUserId ;
    }

    /**
     *   getS the administratorId associated with this entity
     *
     *   @return a int
     */
    public int getAdministratorId()
    {
        return this.administratorId ;
    }


    /**
     *   getS the maturityDate associated with this entity
     *
     *   @return a Date
     */
    public Date getMaturityDate()
    {
        return this.maturityDate ;
    }

    /**
     *   getS the riskRatingId associated with this entity
     *
     *   @return a int
     */
    public int getRiskRatingId()
    {
        return this.riskRatingId ;
    }

    /**
     *   getS the dealTypeId associated with this entity
     *
     *   @return a int
     */
    public int getDealTypeId()
    {
        return this.dealTypeId ;
    }

    /**
     *   getS the firstPaymentDate associated with this entity
     *
     *   @return a Date
     */
    public Date getFirstPaymentDate()
    {
        return this.firstPaymentDate ;
    }

    /**
     *   getS the escrowPaymentAmount associated with this entity
     *
     *   @return a double
     */
    public double getEscrowPaymentAmount()
    {
        return this.escrowPaymentAmount ;
    }

    /**
     *   getS the rateFormulaId associated with this entity
     *
     *   @return a int
     */
    public int getRateFormulaId()
    {
        return this.rateFormulaId ;
    }


    /**
     *   getS the bankABANumber associated with this entity
     *
     *   @return a String
     */
    public String getBankABANumber()
    {
        return this.bankABANumber ;
    }

    /**
     *   getS the buydownRate associated with this entity
     *
     *   @return a double
     */
    public double getBuydownRate()
    {
        return this.buydownRate ;
    }





    /**
     *   getS the numberOfGuarantors associated with this entity
     *
     *   @return a int
     */
    public int getNumberOfGuarantors()
    {
        int count = 0;
        try
        {
            Collection v = this.getBorrowers();

            Iterator e = v.iterator();
            while(e.hasNext())
            {
                Borrower b = (Borrower)e.next();
                if(b.isGuarantor())count++;
            }

        }
        catch(Exception e)
        {
            // Jan 30, 2007 -- start 
            // TODO : we might have to fix this fucntion.
            logger.error(e.getMessage());
            logger.error(" ---------------------- exception detected at Deal.getNumberOfGuarantors");
            logger.error(" -- An exception was detected."); 
            logger.error(" -- This method returns �-1� as the result for now, is this expected value?");
            logger.error(" ---------------------- ");
            logger.error(e);
            // Jan 30, 2007 -- end
            return -1;
        }

        return count;
    }

    /**
     *   getS the rateDate associated with this entity
     *
     *   @return a Date
     */
    public Date getRateDate()
    {
        return this.rateDate ;
    }

    /**
     *   getS the paymentTermId associated with this entity
     *
     *   @return a int
     */
    public int getPaymentTermId()
    {
        return this.paymentTermId ;
    }

    /**
     *   getS the discount associated with this entity
     *
     *   @return a double
     */
    public double getDiscount()
    {
        return this.discount ;
    }

    /**
     *   getS the applicationDate associated with this entity
     *
     *   @return a Date
     */
    public Date getApplicationDate()
    {
        return this.applicationDate ;
    }

    /**
     *   getS the MIPremiumAmount associated with this entity
     *
     *   @return a double
     */
    public double getMIPremiumAmount()
    {
        return this.MIPremiumAmount ;
    }

    /**
     *   getS the borrower associated with this entity
     *
     *   @return a Borrower;
     */
    public Collection<Borrower> getBorrowers() throws Exception
    {
        return new Borrower(srk,dcm).findByDeal(this.pk);
    }

    /**
     * gets a Collection of Borrowers associated with the last
     * deal and request
     * @return Collection of Borrower
     * @throws Exception
     */

    public Collection getBorrowersUsingCurrentRequest() throws Exception
    {
        return new Borrower(srk,dcm).findByDealAndCurrentRequest(this.pk);
    }

    /**
     *   gets the Collection of Requests associated with this entity
     *
     *   @return Collection of Requests;
     */
    public Collection getRequests() throws Exception
    {
        return new Request(srk).findRequestsByDealIdAndCopyId(this.pk);
    }

    //--DJ_LDI_CR--start--//
    /**
     *   getS the InsureOnlyApplicant associated with this entity
     *
     *   @return a InsureOnlyApplicant;
     */
    public Collection getInsureOnlyApplicants() throws Exception
    {
        return new InsureOnlyApplicant(srk, dcm).findByDeal(this.pk);
    }
    //--DJ_LDI_CR--end--//

    /**
     *   getS the bridge associated with this entity
     *
     *   @return a Bridge;
     */
    public Bridge getBridge() throws Exception
    {
        Bridge bridge = new Bridge(srk,dcm);

        bridge.setSilentMode(true);  // no considered error if unfound

        return bridge.findByDeal(this.pk);
    }


    /**
     *   getS the Assets associated with this entity
     *
     *   @return a Borrower;
     */
    public Collection getAssets() throws Exception
    {
        return new Asset(srk,dcm).findByDeal(this.pk);
    }

    /**
     *   getS the Incomes associated with this entity
     *
     *   @return a Borrower;
     */
    public Collection getIncomes() throws Exception
    {
        return new Income(srk,dcm).findByDeal(this.pk);
    }

    /**
     *   getS the Liabilities associated with this entity
     *
     *   @return a Borrower;
     */
    public Collection getLiabilities() throws Exception
    {
        return new Liability(srk,dcm).findByDeal(this.pk);
    }

    /**
     *   getS the PricingRateInventory associated with this entity by Date
     *
     *   @return a PricingRateInventory;
     */
    public PricingRateInventory getPriByDate(Date d) throws Exception
    {
        return new PricingRateInventory(srk).findByDealAndDate(this, d);
    }


    /**
     *   getS the PricingRateInventory for the best rate (minimum rate)
     *            between the dates
     *
     *   @return a PricingRateInventory;
     */
    public PricingRateInventory getPriByMinRate(Date startDate) throws Exception
    {
        return new PricingRateInventory(srk).findPriByMinRate(this, startDate);
    }


    /**
     *   getS the PricingRateInventory for the best rate (minimum rate)
     *            between the dates
     *
     *   @return a PricingRateInventory;
     */
    public PricingRateInventory getPriByMinRate(Date startDate, Date endDate) throws Exception
    {
        return new PricingRateInventory(srk).findPriByMinRate(this, startDate, endDate);
    }

    /**
     *   getS the PricingRateInventory associated with this entity
     *
     *   @return a PricingRateInventory;
     */
    public PricingRateInventory getPricingRateInventory() throws Exception
    {
        return new PricingRateInventory(srk).findByPricingRateInventory(this.getPricingProfileId());
    }


    /**
     *   getS the specialFeatureId associated with this entity
     *
     *   @return a int
     */
    public int getSpecialFeatureId()
    {
        return this.specialFeatureId ;
    }

    /**
     *   getS the MARSNameSearch associated with this entity
     *
     *   @return a String
     */
    public String getMARSNameSearch()
    {
        return this.MARSNameSearch ;
    }

    /**
     *   getS the MARSPropertySearch associated with this entity
     *
     *   @return a String
     */
    public String getMARSPropertySearch()
    {
        return this.MARSPropertySearch ;
    }

    /**
     *   getS the downPaymentAmount associated with this entity
     *
     *   @return a double
     */
    public double getDownPaymentAmount()
    {
        return this.downPaymentAmount ;
    }



    /**
     *   getS the commisionAmount associated with this entity
     *
     *   @return a double
     */
    public double getCommisionAmount()
    {
        return this.commisionAmount ;
    }

    /**
     *   getS the lineOfBusiness associated with this entity
     *
     *   @return a LineOfBusiness
     */

    /**
     *   getS the statusDate associated with this entity
     *
     *   @return a Date
     */
    public Date getStatusDate()
    {
        return this.statusDate ;
    }

    /**
     *   getS the prepaymentOptionsId associated with this entity
     *
     *   @return a int
     */
    public int getPrePaymentOptionsId()
    {
        return this.prePaymentOptionsId ;
    }

    /**
     *   getS the netInterestRate associated with this entity
     *
     *   @return a double
     */
    public double getNetInterestRate()
    {
        return this.netInterestRate ;
    }

    /**
     *   getS the actualClosingDate associated with this entity
     *
     *   @return a Date
     */
    public Date getActualClosingDate()
    {
        return this.actualClosingDate ;
    }

    /**
     *   getS the PandiPaymentAmount associated with this entity
     *
     *   @return a double
     */
    public double getPandiPaymentAmount()
    {
        return this.PandiPaymentAmount ;
    }


    /**
     *   getS the bankName associated with this entity
     *
     *   @return a String
     */
    public String getBankName()
    {
        return this.bankName ;
    }

    /**
     *   getS the ComittmentAcceptDate associated with this entity
     *
     *   @return a Date
     */
    public Date getCommitmentAcceptDate()
    {
        return this.commitmentAcceptDate ;
    }
    /**
     *   getS the ComittmentAcceptDate associated with this entity
     *
     *   @return a Date
     */
    public String getRefExistingMtgNumber()
    {
        return this.refExistingMtgNumber;
    }
    /**
     *   getS the combinedTotalAssets associated with this entity
     *
     *   @return a double
     */
    public double getCombinedTotalAssets()
    {
        return this.combinedTotalAssets ;
    }

    /**
     *   getS the lenderProfile associated with this entity
     *
     *   @return a LenderProfile
     */
    public LenderProfile getLenderProfile()throws FinderException, RemoteException
    {
        return new LenderProfile(srk,this.getLenderProfileId());
    }

    /**
     *   getS the ComittmentIssueDate associated with this entity
     *
     *   @return a Date
     */
    public Date getCommitmentIssueDate()
    {
        return this.commitmentIssueDate ;
    }


    /**
     *   getS the numberOfBorrowers associated with this entity
     *
     *   @return a int
     */
    public int getNumberOfBorrowers()
    {

        return numberOfBorrowers;
    }


    /**
     *   getS the sourceOfBusinessProfileId associated with this entity
     *
     *   @return a int
     */
    public int getSourceOfBusinessProfileId()
    {
        return this.sourceOfBusinessProfileId ;
    }

    /**
     *   getS the combinedGDS associated with this entity
     *
     *   @return a double
     */
    public double getCombinedGDS()
    {
        return this.combinedGDS ;
    }

    /**
     *   getS the lenderProfileId associated with this entity
     *
     *   @return a int
     */
    public int getLenderProfileId()
    {
        return this.lenderProfileId ;
    }

    /**
     *   getS the electronicSystemSource associated with this entity
     *
     *   @return a String
     */
    public String getElectronicSystemSource()
    {
        return this.electronicSystemSource ;
    }

    /**
     *   getS the requestedSolicitorName associated with this entity
     *
     *   @return a String
     */
    public String getRequestedSolicitorName()
    {
        return this.requestedSolicitorName ;
    }

    /**
     *   getS the estimatedClosingDate associated with this entity
     *
     *   @return a Date
     */
    public Date getEstimatedClosingDate()
    {
        return this.estimatedClosingDate ;
    }



    /**
     *   getS the secondaryFinancing associated with this entity
     *
     *   @return a char
     */
    public String getSecondaryFinancing()
    {
        return this.secondaryFinancing ;
    }

    public boolean isSecondaryFinancing()
    {
        return TypeConverter.booleanFrom(this.secondaryFinancing,"N");
    }

    /**
     *   getS the estimatedPaymentAmount associated with this entity
     *
     *   @return a double
     */
    public double getEstimatedPaymentAmount()
    {
        return this.estimatedPaymentAmount ;
    }

    public int getInterestCompoundId()
    {
        return this.interestCompoundId ;
    }

    public int getInvestorProfileId()
    {
        return this.investorProfileId ;
    }

    public InvestorProfile getInvestorProfile()throws RemoteException,FinderException
    {
        return new InvestorProfile(srk,investorProfileId);
    }

    public int getSecondApproverId()
    {
        return this.secondApproverId ;
    }

    public int getJointApproverId()
    {
        return this.jointApproverId ;
    }

    public int getBranchProfileId()
    {
        return this.branchProfileId ;
    }

    //added by Clement for NBC servicing payload Oct 2007
    public BranchProfile getBranchProfile()throws FinderException, RemoteException
    {
        return new BranchProfile(srk,this.getBranchProfileId());
    }

    public int getSourceFirmProfileId()
    {
        return this.sourceFirmProfileId ;
    }

    public SourceFirmProfile getSourceFirmProfile()throws FinderException, RemoteException
    {
        return new SourceFirmProfile(srk,this.getSourceFirmProfileId());
    }

    public GroupProfile getGroupProfile()throws RemoteException,FinderException
    {
        return new GroupProfile(srk,groupProfileId);
    }

    public int getGroupProfileId()
    {
        return this.groupProfileId ;
    }

    public PricingProfile getPricingProfile()throws RemoteException,FinderException
    {
        return new PricingProfile(srk,pricingProfileId);
    }

    /**
     *  @return the pricingRateInventoryId associated with this deal
     */
    public int getPricingProfileId()
    {
        return pricingProfileId;
    }

    /**
     * Business Method - supports upload
     * @return the pricingProfileId associated with the PricingRateInventory associated with this deal
     */
    public int getActualPricingProfileId()  throws Exception
    {
        PricingProfile pp = new PricingProfile(srk);

        pp = pp.findByPricingRateInventory(this.pricingProfileId);

        return pp.getPricingProfileId();
    }

    /**
     * Returns PricingProfile of deal
     * @return the pricingProfile associated with the PricingRateInventory associated with this deal
     */
    public PricingProfile getActualPricingProfile()  throws Exception
    {
        PricingProfile pp = new PricingProfile(srk);

        pp = pp.findByPricingRateInventory(this.pricingProfileId);

        return pp;
    }

    public double getRefinanceNewMoney()
    {
        return refinanceNewMoney;
    }

    public Date getInterimInterestAdjustmentDate()
    {
        return interimInterestAdjustmentDate;
    }

    public int getPrivilegePaymentId()
    {
        return privilegePaymentId;
    }



    public double getAdvanceHold(){return this.advanceHold ;}
    public String getClientNumber(){return this.clientNumber ;}
    public double getCombinedLTV(){return this.combinedLTV ;}
    public double getCombinedTotalAnnualHeatingExp(){return this.combinedTotalAnnualHeatingExp ;}
    public double getCombinedTotalAnnualTaxExp(){return this.combinedTotalAnnualTaxExp ;}
    public double getCombinedTotalCondoFeeExp(){return this.combinedTotalCondoFeeExp ;}
    public double getCombinedTotalPropertyExp (){return this.combinedTotalPropertyExp ;}
    public int getCommitmentPeriod(){return this.commitmentPeriod ;}
    public String getCommitmentProduced(){return this.commitmentProduced ;}
    public int getCrossSellProfileId(){return this.crossSellProfileId ;}
    public Date getDatePreAppFirm(){return this.datePreAppFirm ;}
    public int getFifthApproverId(){return this.fifthApproverId ;}
    public int getFinalApproverId(){return this.finalApproverId ;}
    public int getFinanceProgramId(){return this.financeProgramId ;}
    public int getFourthApproverId(){return this.fourthApproverId ;}

    public String getInvestorApproved()
    {
        return  TypeConverter.stringFromBoolean(this.investorApproved,"Y","N") ;
    }

    public String getInvestorConfirmationNumber(){return this.investorConfirmationNumber ;}
    public double getLienHolderSecondaryFinancing(){return this.lienHolderSecondaryFinancing ;}
    public int getLienPositionId(){return this.lienPositionId ;}
    public int getMIPayorId(){return this.MIPayorId ;}
    public int getMITypeId(){return this.MITypeId ;}
    public double getNewMoney(){return this.newMoney ;}
    public Date getNextRateChangeDate(){return this.nextRateChangeDate ;}
    public int getNumberOfProperties(){return this.numberOfProperties ;}
    public double getOldMoney(){return this.oldMoney ;}
    public String getPreApproval()
    {
        return  TypeConverter.stringFromBoolean(this.preApproval,"Y","N") ;
    }

    public double getPremium(){return this.premium ;}
    public String getRateLock()
    {
        return  TypeConverter.stringFromBoolean(this.rateLock,"Y","N") ;
    }
    public Date getRateLockExpirationDate(){return this.rateLockExpirationDate ;}
    public int getRateLockPeriod(){return this.rateLockPeriod ;}
    public String getReferenceDealNumber(){return this.referenceDealNumber ;}
    public int getReferenceDealTypeId(){return this.referenceDealTypeId ;}
    public double getRefiCurrentBal(){return this.refiCurrentBal ;}
    public double getRemainingBalanceSecondaryFin(){return this.remainingBalanceSecondaryFin ;}
    public Date getReturnDate(){return this.returnDate ;}
    public String getSecondaryFinancingLienHolder(){return this.secondaryFinancingLienHolder ;}
    public String getSourceRefAppNBR(){return this.sourceRefAppNBR ;}
    public String getSourceSystemMailBoxNBR(){return this.sourceSystemMailBoxNBR ;}
    public int getTaxPayorId(){return this.taxPayorId ;}
    public int getThirdApproverId(){return this.thirdApproverId ;}
    public double getTotalActAppraisedValue(){return this.totalActAppraisedValue ;}
    public double getTotalEstAppraisedValue(){return this.totalEstAppraisedValue ;}
    public double getTotalPropertyExpenses(){return this.totalPropertyExpenses ;}
    public double getAdditionalPrincipal(){return this.additionalPrincipal ;}
    public double getTotalPaymentAmount(){return this.totalPaymentAmount;}

    //======================================== ActiveX Ver 1.0.2 Start ========================================
    // -- DJ Ticket#499 - Catherine  --
    public double getAddCreditCostInsIncurred(){return this.addCreditCostInsIncurred; }
    public double getTotalCreditCost(){return this.totalCreditCost; }
    public double getIntWithoutInsCreditCost(){return this.intWithoutInsCreditCost; }

    public void setAddCreditcostInsIncurred(double amt )
    {
        this.testChange ("addCreditcostInsIncurred", amt ) ;
        this.addCreditCostInsIncurred = amt;
    }

    public void setTotalCreditCost(double amt )
    {
        this.testChange ("totalCreditCost", amt ) ;
        this.totalCreditCost = amt;
    }

    public void setIntWithoutInsCreditCost(double amt )
    {
        this.testChange ("intWithoutInsCreditCost", amt ) ;
        this.intWithoutInsCreditCost = amt;
    }
    // -- DJ Ticket#499 - Catherine  end --
    //======================================== ActiveX Ver 1.0.2 End ==========================================

    // ------ Catherine, 10-Jan-05, XD to MCAP
    public String getFundingUploadDone(){ return  this.fundingUploadDone ;};
    public void setFundingUploadDone(String string)
    {
        this.testChange ("fundingUploadDone", string ) ;
        this.fundingUploadDone = string;
    }
    // ------ Catherine, 10-Jan-05, XD to MCAP end

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    public double getPrimeBaseAdj() {return this.primeBaseAdj;}
    public int getPrimeIndexId() { return this.primeIndexId;}
    public double getPrimeIndexRate() { return this.primeIndexRate;}
    public void setPrimeBaseAdj(double amt )
    {
        this.testChange("primeBaseAdj", amt);
        this.primeBaseAdj = amt;
    }

    public void setPrimeIndexId(int id )
    {
        this.testChange("primeIndexId", id);
        this.primeIndexId = id;
    }

    public void setPrimeIndexRate(double amt )
    {
        this.testChange("primeIndexRate", amt);
        this.primeIndexRate = amt;
    }
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    // ------ Catherine, 10-Jan-05, GE to FCT
    public String getClosingUploadDone(){ return  this.closingUploadDone ;};

    public void setClosingUploadDone(String string)
    {
        this.testChange ("closingUploadDone", string ) ;
        this.closingUploadDone = string;
    }
    // ------ Catherine, 10-Jan-05, GE to FCT end

    public int getCopyId(){ return this.copyId; }

    public void setCopyId(int id)
    {
        this.testChange("copyId", id ) ;
        this.copyId = id;
    }

    /**
     * Method returns party profile records associated to this deal.
     */
    public Collection<PartyProfile> getPartyProfiles() throws Exception
    {
        return new PartyProfile(srk).findByDealAndType(this.pk,-1);
    }


    /**
     *   getS the DealFees associated with this entity as a vector
     *
     *   @return a Vector of DownPaymentSource objects;
     */
    public Collection getDealFees() throws Exception
    {
        return new DealFee(srk,dcm).findByDeal(this.pk);
    }

    /**
     *   gets the EmploymentHistories associated with this entity as a vector
     *
     *   @return a Vector of DownPaymentSource objects;
     */
    public Collection getEmploymentHistories() throws Exception
    {
        return new EmploymentHistory(srk).findByDeal(this.pk);
    }

    /**
     *   getS the DealNotes associated with this entity as a vector
     *
     *   @return a Vector of DownPaymentSource objects;
     */
    public Collection getDealNotes() throws Exception
    {
        return new DealNotes(srk).findByDeal(this.pk);
    }

    /**
     *   getS the EscrowPayments associated with this entity as a vector
     *
     *   @return a Collection of DownPaymentSource objects;
     */
    public Collection<EscrowPayment> getEscrowPayments() throws Exception
    {
        EscrowPayment esc = new EscrowPayment(srk,dcm);
        return esc.findByDeal((DealPK)this.getPk());
    }

    //#DG600 not used - delete after 1/Jan/2009
    /**
     * National Bank Implementation /PP
     * @date 03-Aug-07
     * @author NBC/PP Implementation Team
     * @iteration:4
     * @story Name:Update Deal Compare with Admin Fee
     * @version 1.11
     * @return a double value
     * @param  srk as SessionResourceKit object
     * The getAdminFeeAmount takes srk object as input parameter and  
     * returns a the AdminFee amount value of the particular deal. 
     */


    //***** Change by NBC/PP Implementation Team -version 1.11 - Method getAdminFeeAmount - Start *****//

    public double getAdminFeeAmount(SessionResourceKit srk) throws Exception {
        double adminFeeAmount=0.0;
        int dealID = this.getDealId();
        int copyID = this.getCopyId();
        EscrowPayment ep = new EscrowPayment(srk);
        adminFeeAmount = ep.findByDealIdAndType(dealID,copyID,Mc.ESCROWTYPE_ADMIN_FEE);
        return adminFeeAmount;
    }

    //***** Change by NBC/PP Implementation Team -version 1.11- Method getAdminFeeAmount - End *****//



    public void setDealId(int id) {
        this.testChange ( "dealId", id ) ;
        this.dealId = id;
    }

    public void setSourceOfBusinessProfileId(int id )
    {
        this.testChange ("sourceOfBusinessProfileId", id ) ;
        this.sourceOfBusinessProfileId = id;
    }

    public void setApplicationId(String id )
    {
        this.testChange ("applicationId", id ) ;
        this.applicationId = id;
    }
    public void setApplicationDate(Date  d )
    {
        this.testChange ("applicationDate", d ) ;
        this.applicationDate = d;
    }
    public void setTotalLoanAmount(double amt )
    {
        this.testChange ("totalLoanAmount", amt ) ;
        this.totalLoanAmount = amt;
    }
    public void setAmortizationTerm(int months)
    {
        this.testChange ("amortizationTerm", months ) ;
        this.amortizationTerm = months;
    }
    public void setEstimatedClosingDate( Date d)
    {
        this.testChange ("estimatedClosingDate", d ) ;
        this.estimatedClosingDate = d;
    }
    public void setDealTypeId(int id )
    {
        this.testChange ("dealTypeId", id ) ;
        this.dealTypeId = id;
    }

    public void setLenderProfileId(int  id )
    {
        this.testChange ("lenderProfileId", id ) ;
        this.lenderProfileId = id;
    }

    public void setSourceApplicationId(String  id )
    {
        this.testChange ("sourceApplicationId", id ) ;
        this.sourceApplicationId = id;
    }

    public void setTotalPurchasePrice(double price )
    {
        this.testChange ( "totalPurchasePrice", price ) ;
        this.totalPurchasePrice = price;
    }
    public void setPostedInterestRate(double r)
    {
        this.testChange ("postedInterestRate", r ) ;
        this.postedInterestRate = r;
    }
    public void setUnderwriterUserId(int id )
    {
        this.testChange ("underwriterUserId", id ) ;
        this.underwriterUserId =id;
    }   // why userid is numeric?
    public void setAdministratorId(int id )
    {
        this.testChange ("administratorId", id ) ;
        this.administratorId =id;
    }
    public void setEstimatedPaymentAmount(double amt )
    {
        this.testChange ( "estimatedPaymentAmount", amt ) ;
        this.estimatedPaymentAmount = amt;
    }
    public void setPandiPaymentAmount(double amt )
    {
        this.testChange("PandiPaymentAmount", amt) ;
        this.PandiPaymentAmount = amt;
    }
    public void setEscrowPaymentAmount(double amt )
    {
        this.testChange ("escrowPaymentAmount", amt ) ;
        this.escrowPaymentAmount = amt;
    }
    public void setMIPremiumAmount(double amt )
    {
        this.testChange ("MIPremiumAmount", amt ) ;
        this.MIPremiumAmount = amt;
    }
    public void setNetLoanAmount(double amt )
    {
        this.testChange ("netLoanAmount", amt ) ;
        this.netLoanAmount = amt;
    }

    public void setStatusDate(Date  d)
    {
        this.testChange ("statusDate", d ) ;
        this.statusDate = d;
    }
    public void setDiscount(double val )
    {
        this.testChange ("discount", val ) ;
        this.discount = val;
    }
    public void setSecondaryFinancing(String string)
    {
        this.testChange ("secondaryFinancing", string ) ;
        this.secondaryFinancing = string;
    }

    public void setSecondaryFinancing(char c)
    {
        Character character = new Character(c);
        this.testChange ("secondaryFinancing", c ) ;
        this.secondaryFinancing = character.toString();
    }

    public void setOriginalMortgageNumber(String num )
    {
        this.testChange ("originalMortgageNumber", num ) ;
        this.originalMortgageNumber = num;
    }

    public void setDenialReasonId(int id )
    {
        this.testChange ("denialReasonId", id ) ;
        this.denialReasonId = id;
    }

    public void setRenewalOptionsId(int id )
    {
        this.testChange ("renewalOptionsId", id ) ;
        this.renewalOptionsId = id;
    }

    public void setNetInterestRate(double r )
    {
        this.testChange ("netInterestRate", r ) ;
        this.netInterestRate = r;
    }
    public void setRateDate(Date d)
    {
        this.testChange ("rateDate", d ) ;
        this.rateDate = d;
    }
    public void setDealPurposeId(int id )
    {
        this.testChange ("dealPurposeId", id ) ;
        this.dealPurposeId = id;
    }

    public void setBuydownRate(double rate)
    {
        this.testChange ("buydownRate", rate ) ;
        this.buydownRate = rate;
    }
    public void setSpecialFeatureId( int id )
    {
        this.testChange ("specialFeatureId", id ) ;
        this.specialFeatureId = id;
    }

    public void setInterimInterestAmount(double amt )
    {
        this.testChange ("interimInterestAmount", amt ) ;
        this.interimInterestAmount = amt;
    }
    public void setDownPaymentAmount(double  amt )
    {
        this.testChange("downPaymentAmount", amt ) ;
        this.downPaymentAmount = amt;
    }
    public void setLineOfBusinessId(int id )
    {
        this.testChange ("lineOfBusinessId", id ) ;
        this.lineOfBusinessId = id;
    }

    public void setMortgageInsurerId(int id )
    {
        this.testChange ("mortgageInsurerId", id ) ;
        this.mortgageInsurerId = id;
    }

    public void setMtgProdId(int id )
    {
        this.testChange ("mtgProdId", id ) ;
        this.mtgProdId = id;
    }

    public void setMIPolicyNumber(String n )
    {
        this.testChange ("MIPolicyNumber", n ) ;
        this.MIPolicyNumber = n;
    }

    public void setFirstPaymentDate(Date d)
    {
        this.testChange("firstPaymentDate", d ) ;
        this.firstPaymentDate = d;
    }
    public void setNetPaymentAmount(double amt )
    {
        this.testChange("netPaymentAmount", amt ) ;
        this.netPaymentAmount = amt;
    }
    public void setMARSNameSearch(String srch )
    {
        this.testChange ("MARSNameSearch", srch ) ;
        this.MARSNameSearch = srch;
    }
    public void setMARSPropertySearch(String srch)
    {
        this.testChange ("MARSPropertySearch", srch ) ;
        this.MARSPropertySearch = srch;
    }
    public void setMILoanNumber(String  n )
    {
        this.testChange ("MILoanNumber", n ) ;
        this.MILoanNumber = n;
    }
    public void setCommisionAmount(double amt )
    {
        this.testChange ("commisionAmount", amt ) ;
        this.commisionAmount = amt;
    }

    public void setMaturityDate(Date d )
    {
        this.testChange ("maturityDate", d ) ;
        this.maturityDate = d;
    }
    public void setCommitmentIssueDate(Date d )
    {
        this.testChange ("commitmentIssueDate", d ) ;
        this.commitmentIssueDate = d;
    }
    public void setCommitmentAcceptDate(Date d )
    {
        this.testChange ("commitmentAcceptDate", d ) ;
        this.commitmentAcceptDate = d;
    }
    public void setRefExistingMtgNumber(String num )
    {
        this.testChange ("refExistingMtgNumber", num ) ;
        this.refExistingMtgNumber = num;
    }

    public void setCombinedTotalLiabilities(double totalLiab )
    {
        this.testChange ("combinedTotalLiabilities", totalLiab);
        this.combinedTotalLiabilities = totalLiab;
    }
    public void setBankABANumber(String n )
    {
        this.testChange ("bankABANumber", n ) ;
        this.bankABANumber = n;
    }
    public void setBankAccountNumber(String n )
    {
        this.testChange ("bankAccountNumber", n ) ;
        this.bankAccountNumber = n;
    }
    public void setBankName(String  name)
    {
        this.testChange ("bankName", name ) ;
        this.bankName = name;
    }
    public void setActualClosingDate(Date d)
    {
        this.testChange ("actualClosingDate", d ) ;
        this.actualClosingDate = d;
    }
    public void setCombinedTotalAssets(double  cta )
    {
        this.testChange("combinedTotalAssets", cta);
        this.combinedTotalAssets = cta;
    }
    public void setCombinedTotalIncome(double  cti )
    {
        this.testChange ("combinedTotalIncome", cti ) ;
        this.combinedTotalIncome = cti;
    }
    public void setCombinedTDS(double  ctds)
    {
        this.testChange ("combinedTDS", ctds ) ;
        this.combinedTDS = ctds;
    }
    public void setCombinedGDS(double  cgds )
    {
        this.testChange ("combinedGDS", cgds ) ;
        this.combinedGDS = cgds;
    }
    public void setRenewalDate(Date d )
    {
        this.testChange ("renewalDate", d ) ;
        this.renewalDate = d;
    }
    public void setNumberOfBorrowers(int n )
    {
        this.testChange ("numberOfBorrowers", n ) ;
        this.numberOfBorrowers = n;
    }
    public void setNumberOfGuarantors(int n )
    {
        this.testChange ("numberOfGuarantors", n ) ;
        this.numberOfGuarantors = n;
    }
    public void setServicingMortgageNumber(String n)
    {
        this.testChange ("servicingMortgageNumber", n ) ;
        this.servicingMortgageNumber = n;
    }
    public void setPaymentTermId(int  id )
    {
        this.testChange ("paymentTermId", id ) ;
        this.paymentTermId = id;
    }

    public void setInterestTypeId(int id )
    {
        this.testChange ("interestTypeId", id ) ;
        this.interestTypeId =  id;
    }

    public void setPaymentFrequencyId(int id )
    {
        this.testChange ("paymentFrequencyId", id ) ;
        this.paymentFrequencyId = id;
    }

    public void setPrePaymentOptionsId(int id )
    {
        this.testChange ("prePaymentOptionsId", id  ) ;
        this.prePaymentOptionsId = id;
    }

    public void setRateFormulaId(int id )
    {
        this.testChange ("rateFormulaId", id ) ;
        this.rateFormulaId = id;
    }

    public void setStatusId(int id )
    {
        this.testChange ("statusId", id ) ;
        this.statusId = id;
    }

    public void setRiskRatingId(int id )
    {
        this.testChange ("riskRatingId", id ) ;
        this.riskRatingId = id;
    }

    public void setElectronicSystemSource(String src)
    {
        this.testChange ("electronicSystemSource", src ) ;
        this.electronicSystemSource = src;
    }
    public void setCommitmentExpirationDate(Date d )
    {
        this.testChange ("commitmentExpirationDate", d ) ;
        this.commitmentExpirationDate = d;
    }
    public void setRequestedSolicitorName(String name)
    {
        this.testChange ("requestedSolicitorName", name ) ;
        this.requestedSolicitorName = name;
    }
    public void setRequestedAppraisorName(String name )
    {
        this.testChange ("requestedAppraisorName", name ) ;
        this.requestedAppraisorName = name;
    }

    public void setInterestCompoundId(int id)
    {
        this.testChange("interestCompoundId", id ) ;
        this.interestCompoundId = id;
    }
    public void setInvestorProfileId(int id)
    {
        this.testChange("investorProfileId", id ) ;
        this.investorProfileId = id;
    }
    public void setSecondApproverId(int id)
    {
        this.testChange ("secondApproverId", id ) ;
        this.secondApproverId = id;
    }

    public void setJointApproverId(int id)
    {
        this.testChange ("jointApproverId", id ) ;
        this.jointApproverId = id;
    }

    public void setBranchProfileId(int id)
    {
        this.testChange ("branchProfileId", id ) ;
        this.branchProfileId = id;
    }

    public void setSourceFirmProfileId(int id)
    {
        this.testChange ("sourceFirmProfileId", id ) ;
        this.sourceFirmProfileId = id;
    }

    public void setGroupProfileId(int id)
    {
        this.testChange ("groupProfileId", id ) ;
        this.groupProfileId = id;
    }

    public void setPricingProfileId(int id)
    {
        this.testChange("pricingProfileId", id ) ;
        this.pricingProfileId = id;
    }

    public void setRefinanceNewMoney(double ref)
    {
        this.testChange ("refinanceNewMoney", ref ) ;
        this.refinanceNewMoney = ref;
    }

    public void setInterimInterestAdjustmentDate(Date in)
    {
        this.testChange ("interimInterestAdjustmentDate", in ) ;
        this.interimInterestAdjustmentDate = in;
    }

    public void setPriviligePaymentId(int id)
    {
        this.testChange ("privilegePaymentId", id ) ;
        this.privilegePaymentId = id;
    }

    public void setMITypeId(int  value)
    {
        this.testChange ("MITypeId", value ) ;
        this.MITypeId = value;
    }


    public void setAdvanceHold(double  value)
    {
        this.testChange ("advanceHold", value ) ;
        this.advanceHold = value;
    }

    public void setClientNumber(String  value)
    {
        this.testChange ("clientNumber", value ) ;
        this.clientNumber = value;
    }

    public void setCombinedLTV(double  value)
    {
        this.testChange ("combinedLTV", value ) ;
        this.combinedLTV = value;
    }

    public void setCombinedTotalAnnualHeatingExp(double  value)
    {
        this.testChange ("combinedTotalAnnualHeatingExp", value ) ;
        this.combinedTotalAnnualHeatingExp = value;
    }

    public void setCombinedTotalAnnualTaxExp(double  value)
    {
        this.testChange ("combinedTotalAnnualTaxExp", value ) ;
        this.combinedTotalAnnualTaxExp = value;
    }

    public void setCombinedTotalCondoFeeExp(double  value)
    {
        this.testChange ("combinedTotalCondoFeeExp", value ) ;
        this.combinedTotalCondoFeeExp = value;
    }

    public void setCombinedTotalPropertyExp (double  value)
    {
        this.testChange ("combinedTotalPropertyExp", value ) ;
        this.combinedTotalPropertyExp = value;
    }

    public void setCommitmentPeriod(int value)
    {
        this.testChange ("commitmentPeriod", value ) ;
        this.commitmentPeriod = value;
    }

    public void setCommitmentProduced(String value)
    {
        this.testChange ("commitmentProduced", value ) ;
        this.commitmentProduced = value;
    }

    public void setCrossSellProfileId(int  value)
    {
        this.testChange ("crossSellProfileId", value ) ;
        this.crossSellProfileId = value;
    }

    public void setDatePreAppFirm(Date  value)
    {
        this.testChange ("datePreAppFirm", value ) ;
        this.datePreAppFirm = value;
    }

    public void setFifthApproverId(int  value)
    {
        this.testChange ("fifthApproverId", value ) ;
        this.fifthApproverId = value;
    }

    public void setFinalApproverId(int  value)
    {
        this.testChange ("finalApproverId", value ) ;
        this.finalApproverId = value;
    }

    public void setFinanceProgramId(int  value)
    {
        this.testChange ("financeProgramId", value ) ;
        this.financeProgramId = value;
    }

    public void setFourthApproverId(int  value)
    {
        this.testChange ("fourthApproverId" , value ) ;
        this.fourthApproverId = value;
    }

    public void setInvestorApproved(String  value)
    {
        this.testChange ("investorApproved", value ) ;
        this.investorApproved = TypeConverter.booleanFrom(value,"N");
    }

    public void setInvestorConfirmationNumber(String  value)
    {
        this.testChange ("investorConfirmationNumber", value ) ;
        this.investorConfirmationNumber = value;
    }

    public void setLienHolderSecondaryFinancing(double value)
    {
        this.testChange ("lienHolderSecondaryFinancing", value ) ;
        this.lienHolderSecondaryFinancing = value;
    }

    public void setLienPositionId(int  value)
    {
        this.testChange ("lienPositionId", value ) ;
        this.lienPositionId = value;
    }

    public void setMIPayorId(int  value)
    {
        this.testChange ("MIPayorId", value ) ;
        this.MIPayorId = value;
    }
    public void setNewMoney(double  value)
    {
        this.testChange ( "newMoney", value ) ;
        this.newMoney = value;
    }
    public void setNextRateChangeDate(Date  value)
    {
        this.testChange ("nextRateChangeDate", value ) ;
        this.nextRateChangeDate = value;
    }
    public void setNumberOfProperties(int  value)
    {
        this.testChange ("numberOfProperties", value ) ;
        this.numberOfProperties = value;
    }
    public void setOldMoney(double  value)
    {
        this.testChange ("oldMoney", value ) ;
        this.oldMoney = value;
    }
    public void setPreApproval(String value)
    {
        this.testChange ("preApproval" , value ) ;
        this.preApproval = TypeConverter.booleanFrom(value,"N");
    }
    public void setPremium(double  value)
    {
        this.testChange ("premium", value ) ;
        this.premium = value;
    }
    public void setRateLock(String value)
    {
        this.testChange ("rateLock", value ) ;
        this.rateLock = TypeConverter.booleanFrom(value,"N");
    }
    public void setRateLockExpirationDate(Date value)
    {
        this.testChange ("rateLockExpirationDate", value ) ;
        this.rateLockExpirationDate = value;
    }
    public void setRateLockPeriod(int  value)
    {
        this.testChange ("rateLockPeriod", value ) ;
        this.rateLockPeriod = value;
    }
    public void setReferenceDealNumber(String value)
    {
        this.testChange ("referenceDealNumber",  value ) ;
        this.referenceDealNumber = value;
    }
    public void setReferenceDealTypeId(int  value)
    {
        this.testChange ("referenceDealTypeId", value ) ;
        this.referenceDealTypeId = value;
    }
    public void setRefiCurrentBal(double  value)
    {
        this.testChange ("refiCurrentBal", value ) ;
        this.refiCurrentBal = value;
    }
    public void setRemainingBalanceSecondaryFin(double  value)
    {
        this.testChange ("remainingBalanceSecondaryFin", value ) ;
        this.remainingBalanceSecondaryFin = value;
    }
    public void setReturnDate(Date  value)
    {
        this.testChange ("returnDate", value ) ;
        this.returnDate = value;
    }
    public void setSecondaryFinancingLienHolder(String  value)
    {
        this.testChange ("secondaryFinancingLienHolder", value ) ;
        this.secondaryFinancingLienHolder = value;
    }
    public void setSourceRefAppNBR(String  value)
    {
        this.testChange ("sourceRefAppNBR", value ) ;
        this.sourceRefAppNBR = value;
    }
    public void setSourceSystemMailBoxNBR(String  value)
    {
        this.testChange ("sourceSystemMailBoxNBR", value ) ;
        this.sourceSystemMailBoxNBR = value;
    }

    public void setTaxPayorId(int value)
    {
        this.testChange ("taxPayorId", value ) ;
        this.taxPayorId = value;
    }

    public void setThirdApproverId(int value)
    {
        this.testChange ("thirdApproverId", value ) ;
        this.thirdApproverId = value;
    }

    public void setTotalActAppraisedValue(double  value)
    {
        this.testChange ("totalActAppraisedValue", value ) ;
        this.totalActAppraisedValue = value;
    }

    public void setTotalEstAppraisedValue(double  value)
    {
        this.testChange ("totalEstAppraisedValue", value ) ;
        this.totalEstAppraisedValue = value;
    }

    public void setTotalPropertyExpenses(double  value)
    {
        this.testChange ("totalPropertyExpenses", value ) ;
        this.totalPropertyExpenses = value;
    }

    public void setAdditionalPrincipal(double  value)
    {
        this.testChange ("additionalPrincipal", value ) ;
        this.additionalPrincipal = value;
    }

    public void setTotalPaymentAmount(double value)
    {
        this.testChange ("totalPaymentAmount", value ) ;
        this.totalPaymentAmount = value;
    }
    //!!!!!!
    public int getActualPaymentTerm(){return actualPaymentTerm;}
    public void setActualPaymentTerm(int value)
    {
        this.testChange ("actualPaymentTerm", value ) ;
        this.actualPaymentTerm = value;
    }


    public double getCombinedGDSBorrower(){return combinedGDSBorrower;}
    public void setCombinedGDSBorrower(double value)
    {
        this.testChange ("combinedGDSBorrower", value ) ;
        this.combinedGDSBorrower = value;
    }

    public double getCombinedTDSBorrower(){return combinedTDSBorrower;}
    public void setCombinedTDSBorrower(double value)
    {
        this.testChange ("combinedTDSBorrower", value ) ;
        this.combinedTDSBorrower = value;
    }

    public double getCombinedGDS3Year(){return combinedGDS3Year;}
    public void setCombinedGDS3Year(double value)
    {
        this.testChange ("combinedGDS3Year", value ) ;
        this.combinedGDS3Year = value;
    }

    public double getCombinedTDS3Year(){return combinedTDS3Year;}
    public void setCombinedTDS3Year(double value)
    {
        this.testChange ("combinedTDS3Year", value ) ;
        this.combinedTDS3Year = value;
    }

    public double getCombinedTotalEquityAvailable(){return combinedTotalEquityAvailable;}
    public void setCombinedTotalEquityAvailable(double value)
    {
        this.testChange ("combinedTotalEquityAvailable", value ) ;
        this.combinedTotalEquityAvailable = value;
    }

    public int getEffectiveAmortizationMonths(){return effectiveAmortizationMonths;}
    public void setEffectiveAmortizationMonths(int value)
    {
        this.testChange ( "effectiveAmortizationMonths", value ) ;
        this.effectiveAmortizationMonths = value;
    }

    public double getPostedRate(){return postedRate;}
    public void setPostedRate(double value)
    {
        this.testChange ("postedRate", value ) ;
        this.postedRate = value;
    }

    public double getBlendedRate(){return blendedRate;}
    public void setBlendedRate(double value)
    {
        this.testChange ("blendedRate", value ) ;
        this.blendedRate = value;
    }

    public String getChannelMedia(){return channelMedia;}
    public void setChannelMedia(String value)
    {
        this.testChange ("channelMedia", value ) ;
        this.channelMedia = value;
    }

    public int getClosingTypeId(){return closingTypeId;}
    public void setClosingTypeId(int value)
    {
        this.testChange ("closingTypeId", value ) ;
        this.closingTypeId = value;
    }

    public String getMIUpfront(){return MIUpfront;}
    public void setMIUpfront(String value)
    {
        this.testChange ( "MIUpfront", value ) ;
        this.MIUpfront = value;
    }

    public Date getPreApprovalConclusionDate(){return preApprovalConclusionDate;}
    public void setPreApprovalConclusionDate(Date value)
    {
        this.testChange ("preApprovalConclusionDate", value ) ;
        this.preApprovalConclusionDate = value;
    }

    public Date getPreApprovalOriginalOfferDate(){return preApprovalOriginalOfferDate;}
    public void setPreApprovalOriginalOfferDate(Date value)
    {
        this.testChange ("preApprovalOriginalOfferDate", value ) ;
        this.preApprovalOriginalOfferDate = value;
    }

    public String getPreviouslyDeclined(){return previouslyDeclined;}
    public void setPreviouslyDeclined(String value)
    {
        this.testChange ( "previouslyDeclined", value ) ;
        this.previouslyDeclined = value;
    }

    public double getRequiredDownpayment(){return requiredDownpayment;}
    public void setRequiredDownpayment(double value)
    {
        this.testChange ("requiredDownpayment", value ) ;
        this.requiredDownpayment = value;
    }

    public int getMIIndicatorId(){return MIIndicatorId;}
    public void setMIIndicatorId(int value)
    {
        this.testChange ("MIIndicatorId", value ) ;
        this.MIIndicatorId = value;
    }

    public int getPreviousMIIndicatorId(){return previousMIIndicatorId;}
    public void setPreviousMIIndicatorId(int value)
    {
        this.testChange ("previousMIIndicatorId", value ) ;
        this.previousMIIndicatorId = value;
    }
    
    public int getRepaymentTypeId(){return repaymentTypeId;}
    public void setRepaymentTypeId(int value)
    {
        this.testChange ( "repaymentTypeId", value ) ;
        this.repaymentTypeId = value;
    }

    public int getDecisionModificationTypeId(){return decisionModificationTypeId;}
    public void setDecisionModificationTypeId(int value)
    {
        this.testChange ( "decisionModificationTypeId", value ) ;
        this.decisionModificationTypeId = value;
    }

    public String getScenarioApproved(){return scenarioApproved;}
    public void setScenarioApproved(String value)
    {
        this.testChange ( "scenarioApproved", value ) ;
        this.scenarioApproved = value;
    }

    public String getScenarioRecommended(){return scenarioRecommended;}
    public void setScenarioRecommended(String value)
    {
        this.testChange ( "scenarioRecommended", value ) ;
        this.scenarioRecommended = value;
    }

    public int getScenarioNumber(){return scenarioNumber;}
    // For performance issue to avoid "Select * from Deal" (bad performance)
    //   -- By BILLY 28Jan2002
    public int getScenarioNumber(int dealId, int copyId)throws RemoteException, FinderException
    {
        int theScenarioNumber = -1;
        boolean gotRecord = false;
        String sql = "Select scenarionumber from Deal where dealId = " +
        dealId + " AND copyId = " + copyId;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                theScenarioNumber = jExec.getInt(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @getScenarioNumber(dealid = " + dealId + ", copyid = " + copyId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - getScenarioNumber() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return theScenarioNumber;
    }
    //=======================================================
    public void setScenarioNumber(int value)
    {
        this.testChange ("scenarioNumber", value ) ;
        this.scenarioNumber = value;
    }

    public String getScenarioDescription(){return scenarioDescription;}
    public void setScenarioDescription(String value)
    {
        this.testChange ( "scenarioDescription", value ) ;
        this.scenarioDescription = value;
    }

    public String getMIComments(){return MIComments;}
    public void setMIComments(String value)
    {
        this.testChange ( "MIComments", value ) ;
        this.MIComments = value;
    }

    public int getMIStatusId(){return MIStatusId;}
    public void setMIStatusId(int value)
    {
        this.testChange ( "MIStatusId", value ) ;
        this.MIStatusId = value;
    }

    public int getSystemTypeId(){return systemTypeId;}
    public void setSystemTypeId(int value)
    {
        this.testChange ( "systemTypeId", value ) ;
        this.systemTypeId = value;
    }

    public double getTotalFeeAmount(){return totalFeeAmount;}
    public void setTotalFeeAmount(double value)
    {
        this.testChange ( "totalFeeAmount", value ) ;
        this.totalFeeAmount = value;
    }

    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> New Field for HoldReasonId
    //--> By Billy 06Jan2004
    public int getHoldReasonId()
    {
        return this.holdReasonId;

    }
    public void setHoldReasonId(int value)

    {
        this.testChange ( "holdReasonId", value ) ;
        this.holdReasonId = value;
    }

    public double getMaximumLoanAmount(){return maximumLoanAmount;}
    public void setMaximumLoanAmount(double value)
    {
        this.testChange ( "maximumLoanAmount", value ) ;
        this.maximumLoanAmount = value;
    }

    public String getScenarioLocked(){return scenarioLocked;}
    public void setScenarioLocked(String value)
    {
        this.testChange ( "scenarioLocked", value ) ;
        this.scenarioLocked = value;
    }

    public double getTotalLoanBridgeAmount(){return totalLoanBridgeAmount;}
    public void setTotalLoanBridgeAmount(double value)
    {
        this.testChange ( "totalLoanBridgeAmount", value ) ;
        this.totalLoanBridgeAmount = value;
    }

    public String getMortgageInsuranceResponse(){return mortgageInsuranceResponse;}
    public void setMortgageInsuranceResponse(String value)
    {
        this.testChange ( "mortgageInsuranceResponse", value ) ;
        this.mortgageInsuranceResponse = value;
    }
    public Date getOnHoldDate(){return this.onHoldDate;}
    public void setOnHoldDate(Date value)
    {
        this.testChange ( "onHoldDate", value ) ;
        this.onHoldDate = value;
    }

    public String getSolicitorSpecialInstructions(){return solicitorSpecialInstructions;}
    public void setSolicitorSpecialInstructions(String value)
    {
        this.testChange ( "solicitorSpecialInstructions", value ) ;
        this.solicitorSpecialInstructions = value;
    }

    public double getCombinedLendingValue(){return combinedLendingValue;}
    public void setCombinedLendingValue(double value)
    {
        this.testChange ( "combinedLendingValue", value ) ;
        this.combinedLendingValue = value;
    }

    public double getExistingLoanAmount(){return existingLoanAmount;}
    public void setExistingLoanAmount(double value)
    {
        this.testChange ("existingLoanAmount", value ) ;
        this.existingLoanAmount = value;
    }

    public double getGDSTDS3YearRate(){return GDSTDS3YearRate;}
    public void setGDSTDS3YearRate(double value)
    {
        this.testChange ("GDSTDS3YearRate", value ) ;
        this.GDSTDS3YearRate = value;
    }

    public double getMaximumPrincipalAmount(){return maximumPrincipalAmount;}
    public void setMaximumPrincipalAmount(double value)
    {
        this.testChange ( "maximumPrincipalAmount", value ) ;
        this.maximumPrincipalAmount = value;
    }

    public double getMaximumTDSExpensesAllowed(){return maximumTDSExpensesAllowed;}
    public void setMaximumTDSExpensesAllowed(double value)
    {
        this.testChange ( "maximumTDSExpensesAllowed", value ) ;
        this.maximumTDSExpensesAllowed = value;
    }

    public String getMICommunicationFlag(){return MICommunicationFlag;}
    public void setMICommunicationFlag(String value)
    {
        this.testChange ( "MICommunicationFlag", value ) ;
        this.MICommunicationFlag = value;
    }

    public String getMIExistingPolicyNumber(){return MIExistingPolicyNumber;}
    public void setMIExistingPolicyNumber(String value)
    {
        this.testChange ( "MIExistingPolicyNumber", value ) ;
        this.MIExistingPolicyNumber = value;
    }

    public double getMinimumIncomeRequired(){return minimumIncomeRequired;}
    public void setMinimumIncomeRequired(double value)
    {
        this.testChange ( "minimumIncomeRequired", value ) ;
        this.minimumIncomeRequired = value;
    }

    public double getPAPurchasePrice(){return PAPurchasePrice;}
    public void setPAPurchasePrice(double value)
    {
        this.testChange ( "PAPurchasePrice", value ) ;
        this.PAPurchasePrice = value;
    }

    public String getInstructionProduced(){return instructionProduced;}
    public void setInstructionProduced(String value)
    {
        this.testChange ( "instructionProduced", value ) ;
        this.instructionProduced = value;
    }


    public double getTeaserDiscount(){return teaserDiscount;}
    public void setTeaserDiscount(double value)
    {
        this.testChange ( "teaserDiscount", value ) ;
        this.teaserDiscount = value;
    }

    public Date getTeaserMaturityDate(){return teaserMaturityDate;}
    public void setTeaserMaturityDate(Date value)
    {
        this.testChange ( "teaserMaturityDate", value ) ;
        this.teaserMaturityDate = value;
    }

    public double getTeaserNetInterestRate(){return teaserNetInterestRate;}
    public void setTeaserNetInterestRate(double value)
    {
        this.testChange ( "teaserNetInterestRate", value ) ;
        this.teaserNetInterestRate = value;
    }

    public double getTeaserPIAmount(){return teaserPIAmount;}
    public void setTeaserPIAmount(double value)
    {
        this.testChange ( "teaserPIAmount", value ) ;
        this.teaserPIAmount = value;
    }

    public int getTeaserTerm(){return teaserTerm;}
    public void setTeaserTerm(int value)
    {
        this.testChange ( "teaserTerm", value ) ;
        this.teaserTerm = value;
    }

    public String getCopyType(){return copyType;}
    // For performance issue to avoid "Select * from Deal" (bad performance)
    //   -- By BILLY 28Jan2002
    public String getCopyType(int dealId, int copyId)throws RemoteException, FinderException
    {
        String theCopyType = "";
        boolean gotRecord = false;
        String sql = "Select copytype from Deal where dealId = " +
        dealId + " AND copyId = " + copyId;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                theCopyType = jExec.getString(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @getCopyType(dealid = " + dealId + ", copyid = " + copyId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - getCopyType() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return theCopyType;
    }
    //=======================================================
    public void setCopyType(String value)
    {
        this.testChange ( "copyType", value ) ;
        this.copyType = value;
    }

    public void setOpenMIResponseFlag(String value)
    {
        this.testChange ( "openMIResponseFlag", value ) ;
        this.openMIResponseFlag = value;
    }

    public String getOpenMIResponseFlag()
    {
        return this.openMIResponseFlag;
    }

    public void setResolutionConditionId(int value)
    {
        this.testChange ("resolutionConditionId", value ) ;
        this.resolutionConditionId = value;
    }
    public int getResolutionConditionId(){return this.resolutionConditionId;}

    public void setCheckNotesFlag(String value)
    {
        this.testChange ("checkNotesFlag", value ) ;
        this.checkNotesFlag = value;
    }
    public String getCheckNotesFlag(){return this.checkNotesFlag;}


    public void setClosingDatePlus90Days( Date value )
    {
        this.testChange ( "closingDatePlus90Days", value );
        this.closingDatePlus90Days  = value ;
    }
    public Date getClosingDatePlus90Days(){ return this.closingDatePlus90Days ; }


    public int getStatusCategoryId()
    {

        int status = getStatusId();

        String sql = "select statusCategoryId from status where statusId = " + status;

        int desc = 0;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                desc = jExec.getInt(key,1);
                break;
            }

            jExec.closeData(key);

        }
        catch (Exception e)
        {
            return -1;
        }

        return desc;
    }


    public Vector findByMyCopies() throws RemoteException, FinderException
    {
        Vector v = new Vector();

        //Modified to use PrepareStatement fro better performance
        //    -- By BILLY 26Feb2002
        //String sql = "Select * from Deal where dealId = " +
        //   getDealId() + " AND copyId <> " + getCopyId();
        String sql = "Select * from Deal where dealId = ? AND copyId <> ?";
        // ======================================================================

        try
        {
            //For testing purpose - use PrepareStatement and check if there is any imporvment in performance
            //    -- By BILLY 26Feb2002
            //int key = jExec.execute(sql);
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            // Set dealid
            pstmt.setInt(1, getDealId());
            // Set Copyid
            pstmt.setInt(2, getCopyId());
            logger.debug("@Deal.findByMyCopies : Dealid = " + getDealId() + " CoptId = " + getCopyId());
            int key = jExec.executePreparedStatement(pstmt, sql);
            // =============================================================================================

            for (; jExec.next(key); )
            {
                Deal iCpy = new Deal(srk,dcm);

                iCpy.setPropertiesFromQueryResult(key);
                // added  srk.getInstitutionId() for ML - July 6, 
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());

                v.add(iCpy);
            }

            jExec.closeData(key);
            pstmt.close();
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return v;
    }

    public List<Deal> findAllCopies() throws RemoteException, FinderException {
    	return findAllCopies(this.getDealId());
    }
    
    public List<Deal> findAllCopies(int dealid) throws RemoteException, FinderException {
        List<Deal> list = new ArrayList<Deal>();

        String sql = "Select * from Deal where dealId = ? ";

        try {
            PreparedStatement pstmt = jExec.getPreparedStatement(sql);
            pstmt.setInt(1, dealid);
            int key = jExec.executePreparedStatement(pstmt, sql);

            for (; jExec.next(key);) {
                Deal iCpy = new Deal(srk, dcm);
                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new DealPK(iCpy.getDealId(), iCpy.getCopyId());
                list.add(iCpy);
            }

            jExec.closeData(key);
            pstmt.close();
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return list;
    }
    
    public int getClassId()
    {
        return ClassId.DEAL;
    }

    public boolean isAuditable()
    {
        return true;
    }

//    void removeCopy ()  throws RemoteException
//    {
//        String sql = "Delete From Deal " + pk.getWhereClause();
//
//        try
//        {
//        	this.removeDescendants(false);
//
//            jExec.executeUpdate(sql);
//
//            this.trackEntityRemoval (); // Track the Removal
//        }
//        catch  ( Exception e )
//        {
//            String msg =  "Failed to remove record " + pk.getWhereClause();
//            RemoteException re = new RemoteException(msg);
//            logger.error(re.getMessage());
//            logger.error("remove sql: " + sql);
//            logger.error(e);
//
//            throw re;
//        }
//    }

    void remove (boolean lastCopy)  throws RemoteException
    {
        String sql = "Delete From Deal " + pk.getWhereClause();

        try
        {
            this.removeDescendants(lastCopy);

            jExec.executeUpdate(sql);

            this.trackEntityRemoval (); // Track the Removal
        }
        catch  ( Exception e )
        {
            String msg =  "Failed to remove record " + pk.getWhereClause();
            RemoteException re = new RemoteException(msg);
            logger.error(re.getMessage());
            logger.error("remove sql: " + sql);
            logger.error(e);

            throw re;
        }
    }


    public void ejbRemove ( ) throws RemoteException
    { //  when deal is the top of the calculation tree
        try
        {
            // added institutionProfileId for ML
            MasterDealPK masterDealPK = new MasterDealPK( this.dealId);
            MasterDeal masterDeal = new MasterDeal( srk, dcm );
            masterDeal.findByPrimaryKey ( masterDealPK ) ;  // find the MasterDeal
            masterDeal.deleteCopy(this.getCopyId());
        }
        catch (Exception e)
        {
            logger.error(e);
            throw (RemoteException) e;
        }
    }

    private void removeDescendants(boolean lastCopy)  throws RemoteException , FinderException, Exception
    {
        Collection requests = this.getRequests ();
        Iterator itReqs = requests.iterator () ;
        while ( itReqs.hasNext () )
        {
            Request request = ( Request ) itReqs.next () ;
            request.ejbRemove ( false );
        }// End of removing all the requests...

        Collection borrowers = this.getBorrowers ();
        Iterator itBo = borrowers.iterator () ;
        while ( itBo.hasNext () )
        {
            Borrower borrower = ( Borrower ) itBo.next () ;
            borrower.ejbRemove ( false );
        }// End of removing all the borrowers...

        //--DJ_LDI_CR--start--//
        Collection insureOnlyAppls = this.getInsureOnlyApplicants();
        Iterator itIoa = insureOnlyAppls.iterator () ;
        while ( itIoa.hasNext () )
        {
            InsureOnlyApplicant insureOnlyAppl = ( InsureOnlyApplicant ) itIoa.next () ;
            insureOnlyAppl.ejbRemove ( false );
        }// End of removing all the InsureOnlyApplicants...
        //--DJ_LDI_CR--end--//

        Collection properties = this.getProperties ();
        Iterator itPr = properties.iterator () ;
        while ( itPr.hasNext () )
        {
            Property property = ( Property ) itPr.next ();
            property.ejbRemove ( false );
        } // end of removing all the properties .....
        
        Collection dealFees = this.getDealFees ();
        Iterator itDF = dealFees.iterator () ;
        while ( itDF.hasNext () )
        {
            DealFee dealFee = ( DealFee ) itDF.next () ;
            dealFee.ejbRemove ( false );
        } // ------ end of removing all dealFee--------

        Bridge bridge = this.getBridge ();
        if (bridge != null)
            bridge.ejbRemove ( false );
        // ------ end of removing all Bridges--------


        Collection escrowPayments = this.getEscrowPayments () ;
        Iterator escit = escrowPayments.iterator () ;
        while ( escit.hasNext () )
        {
            EscrowPayment escrowPayment = ( EscrowPayment ) escit.next () ;
            escrowPayment.ejbRemove ( false  );
        } // -------- end of removing all doucment tracking  -----



        Collection documentTrackings = this.getDocumentTracks () ;
        Iterator itDT = documentTrackings.iterator () ;
        while ( itDT.hasNext () )
        {
            DocumentTracking documentTracking = ( DocumentTracking ) itDT.next () ;
            documentTracking.ejbRemove ( false  );
        } // -------- end of removing all doucment tracking  -----



        Collection downPayments = this.getDownPaymentSources();
        Iterator dpit = downPayments.iterator() ;
        while ( dpit.hasNext() )
        {
            DownPaymentSource dps = ( DownPaymentSource ) dpit.next () ;
            dps.ejbRemove ( false  );
        } //


        /***************MCM Impl team changes starts - @version 1.18*******************/
        Collection components  = this.getComponents();
        Iterator itCo = components.iterator () ;
        while ( itCo.hasNext () )
        {
        	Component component = ( Component ) itCo.next () ;
        	component.ejbRemove ( false );
        }
        
        
        /***************MCM Impl team changes ends - @version 1.18*******************/
        
        /***************MCM Impl team changes starts - XS_2.9@version 1.17*******************/
        
        new ComponentSummary(srk).ejbRemove(new DealPK(this.dealId,this.copyId ), false);
        
        /***************MCM Impl team changes starts - XS_2.9@version 1.17*******************/
        
        if(lastCopy) // remove dealLevel entities
        {
        	MasterDealPK mdpk = new MasterDealPK(this.dealId);
        	MasterDeal md = new MasterDeal(srk, null);
        	md.removeSons(srk, mdpk);
        }
    }

    protected EntityContext getEntityContext() throws RemoteException
    {
        EntityContext ctx = this.entityContext  ;
        try
        {
            ctx.setContextText ( "Deal" );

            Borrower borrower = null;
            try
            {
                borrower = new Borrower(srk, null);
                borrower = borrower.findByPrimaryBorrower(getDealId(), getCopyId());
            }
            catch (FinderException fe)
            {
                return ctx;
            }
            ctx.setContextSource ( borrower.getNameForEntityContext() ) ;

            String sc = String.valueOf ( this.getScenarioNumber () ) + this.getCopyType () ;
            ctx.setScenarioContext ( sc ) ;

            ctx.setApplicationId( this.dealId  ) ;

        } catch ( Exception  e )
        {
            if ( e instanceof FinderException )
            {
                logger.warn( "Deal.getEntityContext FinderException caught. DealId= "+ this.dealId ) ;
                return ctx;
            }
            throw ( RemoteException ) e ;

        } // End of try  .. catch ....

        return ctx;
    } // --------- End of getEntityContext() ---------------------

    public  boolean equals( Object o )
    {
        if ( o instanceof Deal )
        {
            Deal oa = ( Deal )  o;
            if  ( ( dealId  == oa.getDealId () )
                    && ( copyId  ==  oa.getCopyId ()  )  )
                return true;
        }
        return false;
    }

    // New fields for new calculation changes -- Billy 23Nov2001
    // -- Get / Set methods
    public double getMIPremiumPST()
    {
        return this.MIPremiumPST;
    }
    public void setMIPremiumPST(double value)
    {
        this.testChange ( "MIPremiumPST", value ) ;
        this.MIPremiumPST = value;
    }

    public double getPerdiemInterestAmount()
    {
        return this.perdiemInterestAmount;
    }
    public void setPerdiemInterestAmount(double value)
    {
        this.testChange ( "perdiemInterestAmount", value ) ;
        this.perdiemInterestAmount = value;
    }

    public int getIADNumberOfDays()
    {
        return this.IADNumberOfDays;
    }
    public void setIADNumberOfDays(int value)
    {
        this.testChange ( "IADNumberOfDays", value ) ;
        this.IADNumberOfDays = value;
    }

    public double getPandIPaymentAmountMonthly()
    {
        return this.PandIPaymentAmountMonthly;
    }
    public void setPandIPaymentAmountMonthly(double value)
    {
        this.testChange ( "PandIPaymentAmountMonthly", value ) ;
        this.PandIPaymentAmountMonthly = value;
    }

    public Date getFirstPaymentDateMonthly()
    {
        return this.firstPaymentDateMonthly;
    }
    public void setFirstPaymentDateMonthly(Date value)
    {
        this.testChange ( "firstPaymentDateMonthly", value ) ;
        this.firstPaymentDateMonthly = value;
    }

    public int getFunderProfileId()
    {
        return this.funderProfileId;
    }
    public void setFunderProfileId(int value)
    {
        this.testChange ( "funderProfileId", value ) ;
        this.funderProfileId = value;
    }
    // ==========================================================

    // New fields for CMHC Modifications -- Billy 07Feb2002
    // -- Get / Set methods
    public String getProgressAdvance()
    {
        return this.progressAdvance;
    }
    public void setProgressAdvance(String value)
    {
        this.testChange ( "progressAdvance", value ) ;
        this.progressAdvance = value;
    }

    public double getAdvanceToDateAmount()
    {
        return this.advanceToDateAmount;
    }
    public void setAdvanceToDateAmount(double value)
    {
        this.testChange ( "advanceToDateAmount", value ) ;
        this.advanceToDateAmount = value;
    }

    public int getAdvanceNumber()
    {
        return this.advanceNumber;
    }
    public void setAdvanceNumber(int value)
    {
        this.testChange ( "advanceNumber", value ) ;
        this.advanceNumber = value;
    }

    public String getMIRUIntervention()
    {
        return this.MIRUIntervention;
    }
    public void setMIRUIntervention(String value)
    {
        this.testChange ( "MIRUIntervention", value ) ;
        this.MIRUIntervention = value;
    }

    public String getPreQualificationMICertNum()
    {
        return this.preQualificationMICertNum;
    }
    public void setPreQualificationMICertNum(String value)
    {
        this.testChange ( "preQualificationMICertNum", value ) ;
        this.preQualificationMICertNum = value;
    }

    public String getRefiPurpose()
    {
        return this.refiPurpose;
    }
    public void setRefiPurpose(String value)
    {
        this.testChange ( "refiPurpose", value ) ;
        this.refiPurpose = value;
    }

    public double getRefiOrigPurchasePrice()
    {
        return this.refiOrigPurchasePrice;
    }
    public void setRefiOrigPurchasePrice(double value)
    {
        this.testChange ( "refiOrigPurchasePrice", value ) ;
        this.refiOrigPurchasePrice = value;
    }

    public String getRefiBlendedAmortization()
    {
        return this.refiBlendedAmortization;
    }
    public void setRefiBlendedAmortization(String value)
    {
        this.testChange ( "refiBlendedAmortization", value ) ;
        this.refiBlendedAmortization = value;
    }

    public double getRefiOrigMtgAmount()
    {
        return this.refiOrigMtgAmount;
    }
    public void setRefiOrigMtgAmount(double value)
    {
        this.testChange ( "refiOrigMtgAmount", value ) ;
        this.refiOrigMtgAmount = value;
    }

    public String getRefiImprovementsDesc()
    {
        return this.refiImprovementsDesc;
    }
    public void setRefiImprovementsDesc(String value)
    {
        this.testChange ( "refiImprovementsDesc", value ) ;
        this.refiImprovementsDesc = value;
    }

    public double getRefiImprovementAmount()
    {
        return this.refiImprovementAmount;
    }
    public void setRefiImprovementAmount(double value)
    {
        this.testChange ( "refiImprovementAmount", value ) ;
        this.refiImprovementAmount = value;
    }

    public double getNextAdvanceAmount()
    {
        return this.nextAdvanceAmount;
    }
    public void setNextAdvanceAmount(double value)
    {
        this.testChange ( "nextAdvanceAmount", value ) ;
        this.nextAdvanceAmount = value;
    }

    public Date getRefiOrigPurchaseDate()
    {
        return this.refiOrigPurchaseDate;
    }
    public void setRefiOrigPurchaseDate(Date value)
    {
        this.testChange ( "refiOrigPurchaseDate", value ) ;
        this.refiOrigPurchaseDate = value;
    }

    public String getRefiCurMortgageHolder()
    {
        return this.refiCurMortgageHolder;
    }
    public void setRefiCurMortgageHolder(String value)
    {
        this.testChange ( "refiCurMortgageHolder", value ) ;
        this.refiCurMortgageHolder = value;
    }

    //// SYNCADD.
    // New field for SOB resubmission handling -- By Billy 12June2002
    public String getResubmissionFlag()
    {
        return this.resubmissionFlag;
    }
    public void setResubmissionFlag(String value)
    {
        this.testChange ( "resubmissionFlag", value ) ;
        this.resubmissionFlag = value;
    }
    //===============================================================

    //===============================================================
    // New fields to handle MIPolicyNum problem : the number lost if user cancel MI
    //  and re-send again later.  -- Modified by Billy 14July2003
    public String getMIPolicyNumCMHC()
    {
        return this.MIPolicyNumCMHC;
    }
    public void setMIPolicyNumCMHC(String value)
    {
        this.testChange ( "MIPolicyNumCMHC", value ) ;
        this.MIPolicyNumCMHC = value;
    }

    public String getMIPolicyNumGE()
    {
        return this.MIPolicyNumGE;
    }
    public void setMIPolicyNumGE(String value)
    {
        this.testChange ( "MIPolicyNumGE", value ) ;
        this.MIPolicyNumGE = value;
    }
    //===============================================================

    //-- FXLink Phase II --//
    //--> New Field for Market Type Indicator
    //--> By Billy 18Nov2003
    public String getMccMarketType()
    {
        return this.mccMarketType;
    }
    public void setMccMarketType(String value)
    {
        this.testChange ( "mccMarketType", value ) ;
        this.mccMarketType = value;
    }
    // For performance issue to avoid "Select * from Deal" (bad performance)
    public String getMccMarketType(int dealId, int copyId)throws RemoteException, FinderException
    {
        String theMccMarketType = "";
        boolean gotRecord = false;
        String sql = "Select mccMarketType from Deal where dealId = " +
        dealId + " AND copyId = " + copyId;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                theMccMarketType = jExec.getString(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @getMccMarketType(dealid = " + dealId + ", copyid = " + copyId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - getMccMarketType() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return theMccMarketType;
    }
    //===============================================================

    ///--> Ticket#127 -- BMO Tracking broker Commissions
    //--> New field added to indicate if the Broker is paid or not
    //--> By Billy 24Nov2003
    public String getBrokerCommPaid()
    {
        return this.brokerCommPaid;
    }
    public void setBrokerCommPaid(String value)
    {
        this.testChange ( "brokerCommPaid", value ) ;
        this.brokerCommPaid = value;
    }
    //===============================================================
    //--DJ_LI_CR--start//
    /**
     *   gets the PmntPlusLifeDisability associated with this entity
     *
     *   @return a double
     */
    public double getPmntPlusLifeDisability()
    {
        return this.pmntPlusLifeDisability;
    }

    public double getLifePremium()
    {
        return this.lifePremium;
    }

    public double getDisabilityPremium()
    {
        return this.disabilityPremium;
    }

    public double getCombinedPremium()
    {
        return this.combinedPremium;
    }

    public double getInterestRateIncrement()
    {
        return this.interestRateIncrement;
    }

    public double getInterestRateIncLifeDisability()
    {
        return this.interestRateIncLifeDisability;
    }

    public double getEquivalentInterestRate()
    {
        return this.equivalentInterestRate;
    }
    //--DJ_LI_CR--end//

    //--DJ_CR010--start//
    public String getCommisionCode()
    {
        return this.commisionCode;
    }
    //--DJ_CR010--end//

    //--DJ_CR203.1--start//
    public String getMultiProject()
    {
        return this.multiProject;
    }

    public String getProprietairePlus()
    {
        return this.proprietairePlus;
    }

    public double getProprietairePlusLOC()
    {
        return this.proprietairePlusLOC;
    }
    //--DJ_CR203.1--end//

    /**
     * getCashBackAmount
     * 
     * @param None
     *            <br>
     * 
     * @return double : the result of getCashBackAmount <br>
     */
    public double getCashBackAmount() {
        return this.cashBackAmount;
    }

    /**
     * setCashBackAmount
     * 
     * @param double
     *            <br>
     * 
     * @return Void : the result of setCashBackAmount <br>
     */
    public void setCashBackAmount(double cashBackAmount) {
        testChange("cashBackAmount", cashBackAmount);
        this.cashBackAmount = cashBackAmount;
    }

    /**
     * getCashBackPercent
     * 
     * @param None
     *            <br>
     * 
     * @return double : the result of getCashBackPercent <br>
     */
    public double getCashBackPercent() {
        return this.cashBackPercent;
    }

    /**
     * setCashBackPercent
     * 
     * @param double
     *            <br>
     * 
     * @return void : the result of setCashBackPercent <br>
     */
    public void setCashBackPercent(double cashBackPercent) {
        testChange("cashBackPercent", cashBackPercent);
        this.cashBackPercent = cashBackPercent;
    }

    /**
     * getCashBackAmountOverride
     * 
     * @param None
     *            <br>
     * 
     * @return String : the result of getCashBackAmountOverride <br>
     */
    public String getCashBackAmountOverride() {
        return this.cashBackAmountOverride;
    }

    /**
     * setCashBackAmountOverride
     * 
     * @param String
     *            <br>
     * 
     * @return void : the result of setCashBackAmountOverride <br>
     */

    public void setCashBackAmountOverride(String cashBackAmountOverride) {
        testChange("cashBackAmountOverride", cashBackAmountOverride);
        this.cashBackAmountOverride = cashBackAmountOverride;

    }

    // ***** Change by NBC Impl. Team - Version 1.9 - End *****//
    // Ticket #433 -- new field for FX Link II
    //--> By Billy 14June2004
    public String getSourceApplicationVersion()
    {
        return this.sourceApplicationVersion;
    }
    //========================================

    //--DJ_LI_CR--start//
    public void setPmntPlusLifeDisability(double value)
    {
        this.testChange ( "pmntPlusLifeDisability", value ) ;
        this.pmntPlusLifeDisability = value;
    }

    public void setLifePremium(double value)
    {
        this.testChange ( "LifePremium", value ) ;
        this.lifePremium = value;
    }

    public void setDisabilityPremium(double value)
    {
        this.testChange ( "DisabilityPremium", value ) ;
        this.disabilityPremium = value;
    }

    public void setCombinedPremium(double value)
    {
        this.testChange ( "CombinedPremium", value ) ;
        this.combinedPremium = value;
    }

    public void setInterestRateIncrement(double value)
    {
        this.testChange ( "InterestRateIncrement", value ) ;
        this.interestRateIncrement = value;
    }

    public void setInterestRateIncLifeDisability(double value)
    {
        this.testChange ( "InterestRateIncLifeDisability", value ) ;
        this.interestRateIncLifeDisability = value;
    }

    public void setEquivalentInterestRate(double value)
    {
        this.testChange ( "equivalentInterestRate", value ) ;
        this.equivalentInterestRate = value;
    }
    //--DJ_LI_CR--end//

    //--DJ_CR010--start//
    public void setCommisionCode(String value)
    {
        this.testChange ( "commisionCode", value ) ;
        this.commisionCode = value;
    }
    //--DJ_CR010--end//

    // SEAN DJ SPEC-Progress Advance Type July 19, 2005: The getter and setter.
    public int getProgressAdvanceTypeId() {

        return this.progressAdvanceTypeId;
    }

    public void setProgressAdvanceTypeId(int progressAdvanceTypeId) {

        this.testChange("progressAdvanceTypeId", progressAdvanceTypeId);
        this.progressAdvanceTypeId = progressAdvanceTypeId;
    }
    // SEAN DJ SPEC-PAT END

    //--DJ_CR203.1--start//
    public void setMultiProject(String value)
    {
        this.testChange ( "multiProject", value ) ;
        this.multiProject = value;
    }

    public void setProprietairePlus(String value)
    {
        this.testChange ( "proprietairePlus", value ) ;
        this.proprietairePlus = value;
    }

    public void setProprietairePlusLOC(double value)
    {
        this.testChange ( "proprietairePlusLOC", value ) ;
        this.proprietairePlusLOC = value;
    }
    //--DJ_CR203.1--end//

    // SEAN GECF Income Verification Document Type Drop Down: Getter Setter.
    /**
     * Get the value of the income verification type id.
     */
    public int getIncomeVerificationTypeId() {

        return this.incomeVerificationTypeId;
    }

    /**
     * Set the value of the income verification type id.
     */
    public void setIncomeVerificationTypeId(int incomeVerificationTypeId) {

        this.testChange("incomeVerificationTypeId", incomeVerificationTypeId);
        this.incomeVerificationTypeId = incomeVerificationTypeId;
    }
    // END GECF IV document type drop down.

    /**
     * Get the rate guarantee period.
     */
    public int getRateGuaranteePeriod() {
        return this.rateGuaranteePeriod;
    }

    /**
     * Set the value of the rate guarantee period.
     */
    public void setRateGuaranteePeriod(int rateguarnum) {
        this.testChange("rateGuaranteePeriod", rateguarnum);
        this.rateGuaranteePeriod = rateguarnum;
    }

    // Ticket #433 -- new field for FX Link II
    //--> By Billy 14June2004
    public void setSourceApplicationVersion(String value)
    {
        this.testChange ( "sourceApplicationVersion", value ) ;
        this.sourceApplicationVersion = value;
    }
    //=========================================

    // START - MI additions 


    public String getCmhcProductTrackerIdentifier() {
        return cmhcProductTrackerIdentifier;
    }

    public int getLocAmortizationMonths() {
        return locAmortizationMonths;
    }

    public Date getLocInterestOnlyMaturityDate() {
        return locInterestOnlyMaturityDate;
    }

    // public int getLocRepaymentTypeId() {
    //   return locRepaymentTypeId;
    // }

    public double getPandIUsing3YearRate() {
        return pandIUsing3YearRate;
    }

    // public int getProductTypeId() {
    //   return productTypeId;
    // }

    public String getProgressAdvanceInspectionBy() {
        return progressAdvanceInspectionBy;
    }

    public int getRefiProductTypeId() {
        return refiProductTypeId;
    }

    public String getRequestStandardService() {
        return requestStandardService;
    }

    public String getSelfDirectedRRSP() {
        return selfDirectedRRSP;
    }

    public void setCmhcProductTrackerIdentifier(String productTrackerIdentifier) {
        this.testChange("cmhcProductTrackerIdentifier", productTrackerIdentifier);
        cmhcProductTrackerIdentifier = productTrackerIdentifier;
    }

    public void setLocAmortizationMonths(int amortizationMonths) {
        this.testChange("locAmortizationMonths", amortizationMonths);
        locAmortizationMonths = amortizationMonths;
    }

    public void setLocInterestOnlyMaturityDate(Date interestOnlyMaturityDate) {
        this.testChange("locInterestOnlyMaturityDate", interestOnlyMaturityDate);
        locInterestOnlyMaturityDate = interestOnlyMaturityDate;
    }

//  public void setLocRepaymentTypeId(int repaymentTypeId) {
//  this.testChange("locRepaymentTypeId", repaymentTypeId);
//  locRepaymentTypeId = repaymentTypeId;
//  }

    public void setPandIUsing3YearRate(double pandIUsing3YearRate) {
        this.testChange("pandIUsing3YearRate",pandIUsing3YearRate);
        this.pandIUsing3YearRate = pandIUsing3YearRate;
    }

    // public void setProductTypeId(int productTypeId) {
    //   this.testChange("productTypeId", productTypeId);
    //   this.productTypeId = productTypeId;
    // }

    public void setProgressAdvanceInspectionBy(String progressAdvanceInspectionBy) {
        this.testChange("progressAdvanceInspectionBy", progressAdvanceInspectionBy);
        this.progressAdvanceInspectionBy = progressAdvanceInspectionBy;
    }

    public void setRefiProductTypeId(int refiProductTypeId) {
        this.testChange("refiProductTypeId", refiProductTypeId);
        this.refiProductTypeId = refiProductTypeId;
    }

    public void setRequestStandardService(String requestStandardService) {
        this.testChange("requestStandardService", requestStandardService);
        this.requestStandardService = requestStandardService;
    }

    public void setSelfDirectedRRSP(String selfDirectedRRSP) {
        this.testChange("selfDirectedRRSP", selfDirectedRRSP);
        this.selfDirectedRRSP = selfDirectedRRSP;
    }

    // END - MI additions

    // Special update methods to handle MortgageInsuranceResponse (LONG field)
    //  -- By BILLY 25Feb2002
    public String getMIPolicyNumAIGUG()
    {
        return this.MIPolicyNumAIGUG;
    }
    public void setMIPolicyNumAIGUG(String value)
    {
        this.testChange("MIPolicyNumAIGUG", value);
        this.MIPolicyNumAIGUG = value;
    }
    public double getMIFeeAmount()
    {
        return this.MIFeeAmount;
    }
    public void setMIFeeAmount(double amt)
    {
        this.testChange("MIFeeAmount", amt);
        this.MIFeeAmount = amt; 
    }
    public String getMIPremiumErrorMessage()
    {
        return this.MIPremiumErrorMessage;
    }
    public void setMIPremiumErrorMessage(String value)
    {
        this.testChange("MIPremiumErrorMessage",value);
        this.MIPremiumErrorMessage = value;
    }
    //AAtcha ends

    // 4.3GR, Update Total Loan Amount -- start
    public double getMIPremiumAmountPrevious() {
        return this.miPremiumAmountPrevious;
    }

	public void setMIPremiumAmountPrevious(double miPremiumAmountPrevious) {
		testChange("miPremiumAmountPrevious", miPremiumAmountPrevious);
		this.miPremiumAmountPrevious = miPremiumAmountPrevious;
	}

	public String getMIPremiumAmountChangeNortified() {
		return this.miPremiumAmountChangeNortified;
	}

	public void setMIPremiumAmountChangeNortified(String miPremiumAmountChangeNortified) {
		testChange("miPremiumAmountChangeNortified",
				miPremiumAmountChangeNortified);
		this.miPremiumAmountChangeNortified = miPremiumAmountChangeNortified;
	}
	//4.3GR, Update Total Loan Amount -- end


    // Special update methods to handle MortgageInsuranceResponse (LONG field)
    //  -- By BILLY 25Feb2002
    public void updateMortgageInsuranceResponse()
    {
        updateMortgageInsuranceResponse(false);
    }
    public void updateMortgageInsuranceResponse(boolean allCopies)
    {
       // FXP25883 
        Clob clob = null;
        try{


            String tmpData = "";
            if(this.mortgageInsuranceResponse != null)
                tmpData = this.mortgageInsuranceResponse;

            PreparedStatement pstmt = null;

            if(allCopies == false)
            {
                String state = "update deal set mortgageinsuranceresponse = ? " +
                "where dealid = ? and copyid = ?";
                pstmt = this.jExec.getCon().prepareStatement(state);
//              ByteArrayInputStream strStream = new ByteArrayInputStream(tmpData.getBytes());
                pstmt.clearParameters();
//              pstmt.setAsciiStream(1, strStream, tmpData.length());
                clob = createClob(tmpData);
                pstmt.setClob(1, clob);
                pstmt.setInt(2, this.getDealId());
                pstmt.setInt(3, this.getCopyId());
                logger.debug("Deal.updateMortgageInsuranceResponse (only this copy): Dealid = " + this.getDealId() +
                        " Copyid = " + this.getCopyId());
                pstmt.executeUpdate();

            }
            else
            {
                //update sql without copyid changes all copies
                String propagateSql = "update deal set mortgageinsuranceresponse = ? where dealid = ? ";
                pstmt = this.jExec.getCon().prepareStatement(propagateSql);
                pstmt.clearParameters();
                pstmt.setClob(1, createClob(tmpData));
                pstmt.setInt(2, getDealId());
                int ret = pstmt.executeUpdate();
                logger.debug("Deal.updateMortgageInsuranceResponse (all copies) updated " + ret + " record(s) where dealid = " + getDealId());
            }
            pstmt.close();

        }
        catch(Exception e)
        {
            logger.error("Exception in Deal.updateMortgageInsuranceResponse : " + e.getMessage(), e);
        } finally {// FXP25883 
            try {
                if (clob != null) 
                    oracle.sql.CLOB.freeTemporary((oracle.sql.CLOB)clob);
            } catch (SQLException sqle) {
                logger.debug("could not release CLOB temp", sqle);
            }
        }
        
    }

    // Override the CopyChildren for special handling of MIResponse -- By BILLY 04April2002
    public void copyChildren(MasterDeal srcParent, MasterDeal destParent, String parentName, String parentKey, int copyId)  throws Exception
    {
        // Get the target Deal
        // added srk.getInstitutionId() for ML, July 6, 2007
        Deal targetDeal = new Deal(srk, null, destParent.getDealId(), copyId);

        targetDeal.setMortgageInsuranceResponse(this.getMortgageInsuranceResponse());
        targetDeal.updateMortgageInsuranceResponse();

        // Call the original copyChildren
        super.copyChildren(srcParent, destParent, parentName, parentKey, copyId);
    }

    //-- FXLink Phase II --//
    // For performance issue to avoid "Select * from Deal" (bad performance)
    //   -- By BILLY 17Nov2003
    public String getChannelMedia(int dealId, int copyId)throws RemoteException, FinderException
    {
        String theChannelMedia = "";
        boolean gotRecord = false;
        String sql = "Select channelmedia from Deal where dealId = " +
        dealId + " AND copyId = " + copyId;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                theChannelMedia = jExec.getString(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @getChannelMedia(dealid = " + dealId + ", copyid = " + copyId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - getChannelMedia() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return theChannelMedia;
    }
    //=======================================================

    //==========================  Catherine =============================
    /** Catherine for CV, pvcs #727, 18Nov2004
     * need to find if any of the copies have 'INSTRUCTIONPRODUCED'='YES'
     * INSTRUCTIONPRODUCED is set to 'YES' by ClosingActivityHandler.handleSubmit()
     */

    public boolean haveInstructionsBeenProduced() throws FinderException
    {
        String sFlag = "YES";
        boolean gotRecord = false;
        int count = 0;

        String sql = "SELECT COUNT(INSTRUCTIONPRODUCED) FROM DEAL WHERE DEALID = " +
        dealId + " AND INSTRUCTIONPRODUCED = " + "'" + sFlag + "'";

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                count = jExec.getInt(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @haveInstructionsBeenProduced(dealid = " + dealId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new Exception(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - haveInstructionsBeenProduced() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return ((count == 0) ? false : true);
    }
    //========================== Catherine end =============================

    /**
     * @return Returns the uploadSerialNumber.
     */
    public int getUploadSerialNumber() {
        return uploadSerialNumber;
    }
    /**
     * @param uploadSerialNumber The uploadSerialNumber to set.
     */
    public void setUploadSerialNumber(int uploadSerialNumber) {
        this.testChange ( "uploadSerialNumber", uploadSerialNumber ) ;
        this.uploadSerialNumber = uploadSerialNumber;
    }

    // Catherine, XD to MCAP, 20-Apr-05 start ---------

    // Catherine, #1918 - changed method signature
    public void updateFundingUploadDone(boolean allCopies, boolean isSet)
    {
        String attrName = "FUNDINGUPLOADDONE";
        updateBooleanAttribute(attrName, allCopies, isSet);
    }
    // Catherine, XD to MCAP, 20-Apr-05 end ---------

    // GE to FCT Link, Catherine, 1-Sep-05 --------- start ---------
    public void updateClosingUploadDone(boolean allCopies, boolean isSet){
        String attrName = "CLOSINGUPLOADDONE";
        updateBooleanAttribute(attrName, allCopies, isSet);
    }


    // XueBin 20070103 add this method to update CriticalChangeFlag for all copied.
    public void updateCriticalChangeFlagForAllCopies(boolean aYes)
    {    	
        String attributeName = "CRITICALCHANGEFLAG";
        updateBooleanAttribute(attributeName,true,aYes);    
    }   

    // CS, Hiro, 26-Feb-07
    public void updateOutCreditScoreResponseForAllCopies(boolean isOutCS)
    {
        String attrName = "OUTCREDITSCORERESPONSE";
        updateBooleanAttribute(attrName, true, isOutCS);
    }

    private void updateBooleanAttribute(String attributeName, boolean allCopies, boolean isSet){
        try{

            String newVal = (isSet ? "Y" : "N");

            StringBuffer sbSQL = new StringBuffer("UPDATE DEAL SET " + attributeName 
                    + " = '" + newVal + "' " +  "WHERE DEALID = ? ");
            StringBuffer sbTail = new StringBuffer(" AND COPYID = ?");

            String sql;
            if (allCopies) {
                sql = sbSQL.toString();   
            } else {
                sql = sbSQL.append(sbTail).toString();
            }

            logger.debug("Deal.update" + attributeName + ": SQL statement : " + sql);

            PreparedStatement pstmt = this.jExec.getCon().prepareStatement(sql);

            if(allCopies)
            {
                pstmt.clearParameters();
                pstmt.setInt(1, this.getDealId());
                pstmt.executeUpdate();
                logger.debug("Deal.update" + attributeName + " (all copies): Dealid = " + this.getDealId() 
                        + " " + attributeName + " = '" + newVal + "'");
            }
            else
            {
                pstmt.clearParameters();
                pstmt.setInt(1, this.getDealId());
                pstmt.setInt(2, this.getCopyId());
                pstmt.executeUpdate();

                logger.debug("Deal" + attributeName + " (only this copy): Dealid = " + this.getDealId() +
                        " Copyid = " + this.getCopyId() + " " + attributeName + " = '" + newVal + "'");
            }
            pstmt.close();

        }
        catch(Exception e)
        {
            logger.error("Exception in Deal.update" + attributeName + ": " + e.getMessage());
        }
    }

    public int getPaymentTermTypeId() throws FinderException
    {
        int ptTypeId = -1;
        boolean gotRecord = false;
        String sql = "Select paymentTerm.ptTypeID from paymentTerm, deal where deal.dealId = " +
        dealId + " AND deal.copyId = " + copyId + " and paymentTerm.paymentTermID = " + paymentTermId;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                ptTypeId = jExec.getInt(key, 1);
                // Should be only one
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "Deal Entity: @getPaymentTermType(dealid = " + dealId + ", copyid = " + copyId + "), record not found";

                if (getSilentMode() == false)
                    logger.warn(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal - getChannelMedia() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return ptTypeId;
    }

    // GE to FCT Link, Catherine, 1-Sep-05 --------- end --------- 



    /**
     *   gets the last Request associated with this entity
     *
     *   @return Collection of the last Request;
     */
    public Collection getCollectionOfLastRequest() throws Exception
    {
        return new Request(srk).findLastCollectionOfRequest(this.pk);
    }

    /**
     *   gets the last Request associated with this entity
     *
     *   @return Request;
     */
    public Request getLastRequest() throws Exception
    {
        return new Request(srk).findLastRequest(this.pk);
    }
    /**
     * Return the rateFloatDownTypeId associated to the deal
     * @return rateFloatDownTypeId
     * @throws Exception
     */
    public int getRateFloatDownTypeId() throws Exception
    {
        return new MtgProd(srk,dcm).findRfdTypeByPricingProfileId(this.getActualPricingProfileId());
    }

    /**
     * getFloatDownCompletedFlag
     * 	Returns floatDownCompletedFlag attribute value
     * 
     * @param none
     * 
     * @return String : the value of the getFloatDownCompletedFlag attribute
     * @author NBC/PP Implementation Team
     * @version 1.0 (Intial Version - 26 Jun 2006)
     */
    public String getFloatDownCompletedFlag() 
    { 
        return floatDownCompletedFlag; 
    }

    /**
     * setFloatDownCompletedFlag
     * 	Sets floatDownCompletedFlag attribute value
     * 
     * @param value: String
     * 
     * @return none
     * @author NBC/PP Implementation Team
     * @version 1.0 (Intial Version - 26 Jun 2006)
     */
    public void setFloatDownCompletedFlag ( String value ) 
    { 
        this.testChange("floatDownCompletedFlag", value);
        this.floatDownCompletedFlag = value; 
    }

    /**
     * getFloatDownTaskCreatedFlag
     * 	Returns floatDownTaskCreatedFlag attribute value
     * 
     * @param none
     * 
     * @return String : the value of the getFloatDownTaskCreatedFlag attribute
     * @author NBC/PP Implementation Team
     * @version 1.0 (Intial Version - 26 Jun 2006)
     */
    public String getFloatDownTaskCreatedFlag() 
    { 
        return floatDownTaskCreatedFlag; 
    }

    /**
     * setFloatDownTaskCreatedFlag
     * 	Sets floatDownTaskCreatedFlag attribute value
     * 
     * @param value: String
     * 
     * @return none
     * @author NBC/PP Implementation Team
     * @version 1.0 (Intial Version - 26 Jun 2006)
     */
    public void setFloatDownTaskCreatedFlag ( String value ) 
    { 
        this.testChange("floatDownTaskCreatedFlag", value);
        this.floatDownTaskCreatedFlag = value; 
    }

//  ***** Change by NBC Impl. Team - Version 1.10 - Start *****//

    /**
     * @return Returns the affiliationProgramId.
     */
    public int getAffiliationProgramId() {
        return affiliationProgramId;
    }

    /**
     * @param affiliationProgramId The affiliationProgramId to set.
     */
    public void setAffiliationProgramId(int affiliationProgramId) {
        this.testChange ( "affiliationProgramId", affiliationProgramId ) ;
        this.affiliationProgramId = affiliationProgramId;
    }

//  ***** Change by NBC Impl. Team - Version 1.10 - End *****//
    //  ***** Change by NBC Impl. Team - Version 1.11 - Start *****//
    /**
     * @return Returns the balanceAmountEndOfTerm.
     */
    public double getBalanceRemainingAtEndOfTerm() {
        return balanceRemainingAtEndOfTerm;
    }

    /**
     * @param balanceAmountEndOfTerm The balanceAmountEndOfTerm to set.
     */
    public void setBalanceRemainingAtEndOfTerm(double balanceRemainingAtEndOfTerm) {
        this.testChange("balanceRemainingAtEndOfTerm", balanceRemainingAtEndOfTerm);
        this.balanceRemainingAtEndOfTerm = balanceRemainingAtEndOfTerm;
    }

    /**
     * @return Returns the totalCostOfBorrowing.
     */
    public double getTotalCostOfBorrowing() {
        return totalCostOfBorrowing;
    }

    /**
     * @param totalCostOfBorrowing The totalCostOfBorrowing to set.
     */
    public void setTotalCostOfBorrowing(double totalCostOfBorrowing) {
        testChange("totalCostOfBorrowing", totalCostOfBorrowing);
        this.totalCostOfBorrowing = totalCostOfBorrowing;
    }

    //	***** Change by NBC Impl. Team - Version 1.11 - End *****//

    //	***** Change by NBC Impl. Team - Version 1.12 - Start *****//
    public int getLocRepaymentTypeId() {
        return locRepaymentTypeId;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.testChange("productTypeId", productTypeId);	 
        this.productTypeId = productTypeId;
    }

    public void setLocRepaymentTypeId(int repaymentTypeId) {
        this.testChange("locRepaymentTypeId", repaymentTypeId);
        locRepaymentTypeId = repaymentTypeId;
    }

    //	***** Change by NBC Impl. Team - Version 1.12 - End *****//

    /**
     *  @return Returns the MIFeeAmount.
     */
    /*	public double getMIFeeAmount() {
		return MIFeeAmount;
	}   

	/**
     * @param MIFeeAmount The MIFeeAmount to set.
     */
    /*	public void setMIFeeAmount(double MIFeeAmount) {
		testChange("MIFeeAmount", MIFeeAmount);
		this.MIFeeAmount = MIFeeAmount;
	}
     */
    //	  ***** Change by NBC Impl. Team - Version 1.13 - Start *****//
    /**
     * @return Returns the interestOverTerm.
     */
    public double getInterestOverTerm() {
        return interestOverTerm;
    }

    /**
     * @param interestOverTerm The interestOverTerm to set.
     */
    public void setInterestOverTerm(double interestOverTerm) {
        testChange("interestOverTerm", interestOverTerm);
        this.interestOverTerm = interestOverTerm;
    }

    /**
     * @return Returns the teaserRateInterestSaving.
     */
    public double getTeaserRateInterestSaving() {
        return teaserRateInterestSaving;
    }

    /**
     * @param teaserRateInterestSaving The teaserRateInterestSaving to set.
     */
    public void setTeaserRateInterestSaving(double teaserRateInterestSaving) {
        testChange("teaserRateInterestSaving", teaserRateInterestSaving);
        this.teaserRateInterestSaving = teaserRateInterestSaving;
    }

    //	  ***** Change by NBC Impl. Team - Version 1.13 - End *****//

    // XueBin 20060725 Outstanding Credit Scoring Response flag
    public String getOutCreditScoreResponse() {
        return this.outCreditScoreResponse;
    }

    public void setOutCreditScoreResponse(String outCreditScoreResponse) {
        this.testChange("outCreditScoreResponse", outCreditScoreResponse);
        this.outCreditScoreResponse = outCreditScoreResponse;
    }

    public String getCriticalChangeFlag() {
        return this.criticalChangeFlag;
    }

    public void setCriticalChangeFlag(String criticalChangeFlag){
        this.testChange("criticalChangeFlag", criticalChangeFlag);
        this.criticalChangeFlag = criticalChangeFlag;
    }

    //    ***** added for CR03 3.2 GEN - Start *****//
    /**
     * @return autoCalcCommitExpiryDate
     */
    public String getAutoCalcCommitExpiryDate()
    {
        return autoCalcCommitExpiryDate;
    }
    /**
     * @param autoCalcCommitExpiryDate
     */
    public void setAutoCalcCommitExpiryDate(String autoCalcCommitExpiryDate)
    {
        testChange("autoCalcCommitExpiryDate", autoCalcCommitExpiryDate);
        this.autoCalcCommitExpiryDate = autoCalcCommitExpiryDate;
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
    
    //    ***** added for CR03 3.2 GEN - End *****//
	  public String getMIPolicyNumPMI()
	  {
	     return this.MIPolicyNumPMI;
	  }
	  public void setMIPolicyNumPMI(String MIPolicyNumPMI)
	  {
	     this.testChange("MIPolicyNumPMI", MIPolicyNumPMI);
	     this.MIPolicyNumPMI = MIPolicyNumPMI;
	  }

    /***************MCM Impl team changes starts - XS_2.7 *******************/ 
    /**
     * getRefiAdditionalInformation
     * 	Gets refiAdditionalInformation attribute value
     * 
     * @return : String
     * @author MCM Impl Team
     * @version 1.0 (Intial Version - 10 Jun 2008)
     */
    public String getRefiAdditionalInformation() {
        return refiAdditionalInformation;
    }
    /**
     * setRefiAdditionalInformation
     * 	Sets refiAdditionalInformation attribute value
     * 
     * @param value: String
     * 
     * @author MCM Impl Team
     * @version 1.0 (Intial Version - 10 Jun 2008)
     */
    public void setRefiAdditionalInformation(String refiAdditionalInformation) {
        testChange("refiAdditionalInformation", refiAdditionalInformation);
        this.refiAdditionalInformation = refiAdditionalInformation;
    }
    /***************MCM Impl team changes ends - XS_2.7 *******************/

    //4.4 Submission Agent
    /**
     *   getS the sourceOfBusinessProfileSecondary associated with this entity
     *
     *   @return a int
     */
    public int getSourceOfBusinessProfile2ndaryId()
    {
        return this.sourceOfBusinessProfile2ndaryId ;
    }

    public void setSourceOfBusinessProfile2ndaryId(int id )
    {
        this.testChange ("sourceOfBusinessProfile2ndary", id ) ;
        this.sourceOfBusinessProfile2ndaryId = id;
    }
    
    /**
     *   getS the sourceOfBusinessProfileSecondary associated with this entity
     *
     *   @return a SourceOfBusinessProfile
     */
    public SourceOfBusinessProfile getSourceOfBusinessProfile2ndary()throws FinderException, RemoteException
    {
        return new SourceOfBusinessProfile(srk, this.getSourceOfBusinessProfile2ndaryId());
    }
    //4.4 Submission Agent - end
    
    
    //Qualify Rate
    public String getQualifyingOverrideFlag()
    {
        return this.qualifyingOverrideFlag ;
    }
    public String getQualifyingRateOverrideFlag()
    {
        return this.qualifyingRateOverrideFlag ;
    }
	
    public void setQualifyingOverrideFlag(String id )
    {
        this.testChange ("qualifyingOverrideFlag", id ) ;
        this.qualifyingOverrideFlag = id;
    }
    
	    public void setQualifyingRateOverrideFlag(String id )
    {
        this.testChange ("qualifyingRateOverrideFlag", id ) ;
        this.qualifyingRateOverrideFlag = id;
    }
	
    public int getOverrideQualProd()
    {
        return this.overrideQualProd ;
    }

    public void setOverrideQualProd(int id )
    {
        this.testChange ("overrideQualProd", id ) ;
        this.overrideQualProd = id;
    }
    //Qualify Rate
    
    
    public int getMIDealRequestNumber()
    {
        return this.MIDealRequestNumber ;
    }

    public void setMIDealRequestNumber(int number )
    {
        this.testChange ("MIDealRequestNumber", number ) ;
        this.MIDealRequestNumber = number;
    }
    
    private String readMortgageInsuranceResponse() {
        JdbcExecutor je = srk.getJdbcExecutor();
        Connection con = je.getCon();
        String outval = "";
        ResultSet rs = null;
        Statement stm = null;
        try {

            String stmt = "Select MortgageInsuranceResponse, dealid from Deal ";
            stmt += " where dealId = " + this.getDealId();

            oracle.sql.CLOB clob = null;

            stm = con.createStatement();
            rs = stm.executeQuery(stmt);

            if (rs.next()) {
                clob = (oracle.sql.CLOB) rs.getObject(1);

                if (clob == null)
                    return outval;

                if (clob.length() <= 0)
                    return outval;

                outval = readClob(clob);

            }

        } catch (Exception e) {
            logger.error("Deal: failed to read data from MortgageInsuranceResponse: "
                    + this.getPk());
            logger.info("In Transaction: " + je.isInTransaction());
            String msg = "";
            if (e.getMessage() == null)
                msg = "Unknown Error Occurred";
            logger.error("Deal: Reason for failure: " + msg);
            logger.error(e);

        }finally{
            try {
                if(rs != null) rs.close();
            } catch (Exception ignore){}
            try {
                if(stm != null) stm.close();
            } catch (Exception ignore){}
        }

        return outval;

    }
    /**
     * <p>
     * Description: Retrives the Components associated to the Deal 
     * </p>
     * 
     * @version 1.0 XS_11.11 & XS_11.6  10-Jun-2008 Initial Version
     * @return a Collection - Collection of Component entity Object's
     */
    public Collection<Component> getComponents() throws Exception {
    	//FXP23331, MCM/4.1GR, Jan 13, 2009 - start
        //return new Component(srk).findByDeal(this.pk);
        return new Component(srk, dcm).findByDeal(this.pk);
        //FXP23331, MCM/4.1GR, Jan 13, 2009 - end
    }
    
    /**
     * <p>
     * Description: Retrives the Components associated to the Deal 
     * </p>
     * 
     * @version 1.0 XS_11.11 & XS_11.6  10-Jun-2008 Initial Version
     * @return a Collection - Collection of Component entity Object's
     */
    public Collection getComponentSummary() throws Exception {
        return new ComponentSummary(srk).findByDeal(this.pk);
    }

    /**
     * @return Returns the financingWaiverDate.
     */
    public Date getFinancingWaiverDate() {
        return financingWaiverDate;
    }

    /**
     * @param financingWaiverDate The financingWaiverDate to set.
     */
    public void setFinancingWaiverDate(Date financingWaiverDate) {
        testChange("financingWaiverDate", financingWaiverDate);
        this.financingWaiverDate = financingWaiverDate;
    }
    
    /**
     * GR 3.2.2 find most recent copy of deal is approved
     * @return Deal
     * @throws Exception
     */
    public Deal findLastPreApproval() {

        String sql = "Select * from Deal where dealId = " + this.dealId;
        sql += " and specialfeatureid = " + Mc.SPECIAL_FEATURE_PRE_APPROVAL
                + " and statusid = " + Mc.DEAL_PRE_APPROVAL_OFFERED;
        sql += " order by copyid desc";

        boolean gotRecord = false;
        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                gotRecord = true;
                this.pk = pk;
                this.copyId = pk.getCopyId();
                setPropertiesFromQueryResult(key);
                pk.setCopyId(getCopyId());
                break; // find last one copy
            }
            /***** FXP23297 search for status approved start *****/
            if(!gotRecord) {
                sql = "Select * from Deal where dealId = " + this.dealId;
                sql += " and specialfeatureid = " + Mc.SPECIAL_FEATURE_PRE_APPROVAL
                + " and statusid = " + Mc.DEAL_APPROVED;
                sql += " order by copyid desc";
                
                key = jExec.execute(sql);
                for (; jExec.next(key);) {
                    gotRecord = true;
                    this.pk = pk;
                    this.copyId = pk.getCopyId();
                    setPropertiesFromQueryResult(key);
                    pk.setCopyId(getCopyId());
                    break; // find one copy
                }
            }
            /***** FXP23297 search for status approved start *****/
            jExec.closeData(key);
        } catch (Exception e) {
            logger.error(e);
        }
        return this;
    }

}
