package com.basis100.deal.entity;

import java.util.Date;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DCImagingReferencePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class DCImagingReference extends DealEntity{
  protected int dcImagingReferenceId;
  protected int dcFolderId;
  protected Date dateReceived;
  protected String dcDocumentViewed;
  protected Date dcDocumentViewedDate;
  protected String dcDocumentStatus;
  protected String dcDocumentDescription;
  protected int institutionId;

  private DCImagingReferencePK pk;

  public DCImagingReference(SessionResourceKit srk) throws RemoteException {
    super(srk);
  }

  public DCImagingReference(SessionResourceKit srk, int id) throws FinderException, RemoteException {
    super(srk);

    pk = new DCImagingReferencePK(id);
    findByPrimaryKey(pk);
  }

  public DCImagingReference findByPrimaryKey(DCImagingReferencePK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM DCIMAGINGREFERENCE " + pk.getWhereClause();
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break;                                                  // can only be one record
        }

        jExec.closeData(key);
        if (gotRecord == false)
        {
          String msg = "DCImagingReference: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("DCImagingReference.findByPrimaryKey() exception");
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);
          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }

  public DCImagingReference findByDcFolderId(int dcFolderId) throws FinderException {
    this.dcFolderId = dcFolderId;
    String sql = "SELECT * FROM DCIMAGINGREFERENCE WHERE DCFOLDERID = "  + dcFolderId;
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
            break;                                                  // can only be one record
        }

        jExec.closeData(key);
        if (gotRecord == false)
        {
          String msg = "DCImagingReference.findByDcFolderId(), FolderId = " + dcFolderId + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("DCImagingReference.findByDealId() exception. " + dcFolderId);
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);
          throw fe;
      }
      this.pk = pk;
      return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
       int index = 0;
       setDcImagingReferenceId(jExec.getInt(key,++index));
       setDcFolderId(jExec.getInt(key,++index));
       setDateReceived(jExec.getDate(key,++index));
       setDcDocumentViewed(jExec.getString(key,++index));
       setDcDocumentViewedDate(jExec.getDate(key,++index));
       setDcDocumentStatus(jExec.getString(key,++index));
       setDcDocumentDescription(jExec.getString(key,++index));
  }

  public DCImagingReference create(int dcFolderId, String folderCode, String folderStatusCode, String folderStatusDesc)
      throws RemoteException, CreateException
  {
    this.pk = this.createPrimaryKey();

     String comma = ", ";
     Date now = new Date();

     StringBuffer sqlbuf = new StringBuffer(
     "INSERT INTO DCIMAGINGREFERENCE " +
     "( " +
         " DCIMAGINGREFERENCEID, DCFOLDERID, DATERECEIVED ," +
         " DCDOCUMENTVIEWED, DCDOCUMENTVIEWEDDATE, " +
         " DCDOCUMENTSTATUS, DCDOCUMENTDESCRIPTION, " +
         " INSTITUTIONPROFILEID " +
     ")");
     sqlbuf.append(" VALUES (");
     sqlbuf.append(sqlStringFrom(pk.getId()) + ",");                                          // DCIMAGINGREFERENCEID
     sqlbuf.append(sqlStringFrom(dcFolderId)).append(comma);                                  // DCFOLDERID
     sqlbuf.append(sqlStringFrom(now)).append(comma);                                         // DATERECEIVED
     sqlbuf.append(sqlStringFrom('N')).append(comma);                                         // DCDOCUMENTVIEWED
     sqlbuf.append(sqlStringFrom(new String())).append(comma);  // should be null             // DCDOCUMENTVIEWEDDATE
     sqlbuf.append(sqlStringFrom(folderStatusCode)).append(comma);                            // DCDOCUMENTSTATUS
     sqlbuf.append(sqlStringFrom(folderStatusDesc)).append(comma);                            // DCDOCUMENTDESCRIPTION
     sqlbuf.append(srk.getExpressState().getDealInstitutionId());
     sqlbuf.append(")");

     String sql = new String(sqlbuf);

     try
     {
       jExec.executeUpdate(sql);
     }
     catch (Exception e)
     {
       CreateException ce = new CreateException("DCImagingReference.create() exception");
       logger.error(ce.getMessage());
       logger.error(e);

       throw ce;
     }

     this.pk = pk;
     this.dcImagingReferenceId  = pk.getId();
     this.dcFolderId  =  dcFolderId;
     this.dateReceived = now;
     this.dcDocumentViewed = "N";
     this.dcDocumentViewedDate = null;
     this.dcDocumentStatus = folderStatusCode;
     this.dcDocumentDescription = folderStatusDesc;

     srk.setModified(true);
     return this;
    }

  public DCImagingReferencePK createPrimaryKey() throws CreateException
  {
    String sql = "SELECT DCIMAGINGREFERENCESEQ.NEXTVAL FROM DUAL";
    int id = -1;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key);  )
        {
            id = jExec.getInt(key,1);  // can only be one record
        }
        jExec.closeData(key);
        if( id == -1 ) throw new Exception();
     }
     catch (Exception e)
     {
         CreateException ce = new CreateException("DCImagingReference.createPrimaryKey() exception getting ID from sequence");
         logger.error(ce.getMessage());
         logger.error(e);
         throw ce;
     }
     return new DCImagingReferencePK(id);
 }

  public String getEntityTableName(){
    return "DCImagingReference";
  }

  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }

  public int getClassId(){
    return ClassId.DEFAULT;
  }

  public String getStringValue(String fieldName)
  {
      return doGetStringValue(this, this.getClass(), fieldName);
  }

  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }

  protected int performUpdate()  throws Exception
  {
  Class clazz = this.getClass();
  int ret = doPerformUpdate(this, clazz);

  return ret;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof DCImagingReference )
      {
        DCImagingReference oa = ( DCImagingReference ) o;
        if  (  this.dcImagingReferenceId == oa.getDcImagingReferenceId () )
            return true;
      }
    return false;
  }


// ====================== properties getters ======================

  public int getDcImagingReferenceId () {
    return this.dcImagingReferenceId;
  }

  public int getDcFolderId () {
    return this.dcFolderId;
  }

  public Date getDateReceived () {
    return this.dateReceived;
  }

  public String getDcDocumentViewed () {
    return this.dcDocumentViewed;
  }

  public Date getDcDocumentViewedDate () {
    return this.dcDocumentViewedDate;
  }

  public String getDcFolderStatusCode () {
    return this.dcDocumentStatus;
  }

  public String getDcFolderStatusDescription () {
    return this.dcDocumentDescription;
  }

// ====================== properties setters ======================

  public void setDcImagingReferenceId (int dcImagingReferenceId) {
    this.testChange("dcImagingReferenceId", dcImagingReferenceId);
    this.dcImagingReferenceId = dcImagingReferenceId;
  }

  public void setDcFolderId (int dcFolderId) {
    this.testChange("dcFolderId", dcFolderId);
    this.dcFolderId = dcFolderId;
  }

  public void setDateReceived (Date dateReceived) {
    this.testChange("dateReceived", dateReceived);
    this.dateReceived = dateReceived;
  }

  public void setDcDocumentViewed (String dcDocumentViewed) {
    this.testChange("documentViewed", dcDocumentViewed);
    this.dcDocumentViewed = dcDocumentViewed;
  }

  public void setDcDocumentViewedDate (Date dcDocumentViewedDate) {
    this.testChange("documentViewedDate", dcDocumentViewedDate);
    this.dcDocumentViewedDate = dcDocumentViewedDate;
  }

  public void setDcDocumentStatus (String dcDocumentStatus) {
    this.testChange("dcDocumentStatus", dcDocumentStatus);
    this.dcDocumentStatus = dcDocumentStatus;
  }

  public void setDcDocumentDescription (String dcDocumentDescription) {
    this.testChange("dcDocumentDescription", dcDocumentDescription);
    this.dcDocumentDescription = dcDocumentDescription;
  }

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }
    
    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
        this.testChange("institutionProfileId", institutionId);
        this.institutionId = institutionId;
    }
    
}
