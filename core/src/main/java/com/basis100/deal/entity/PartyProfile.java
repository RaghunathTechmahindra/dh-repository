
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.PartyProfilePK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;

/**
 * PartyProfile:
 * 
 * An instance of the entity represents a row in the PARTYPROFILE table and
 * optionally one set of association attributes (members of table
 * PARTYDEALASSOC) - the associative properties, when used, relate the party
 * instance to a deal, or to a property (of a specified deal).
 * 
 * Note when association information is not available the association properties
 * (dealId and propertyId) must be set to value -1.
 * 
 * Some create methods allow specification of association information (e.g.
 * dealId or dealId and propertyId). These methods cause creation of a record in
 * table PARTYDEALASSOC to represent the association.
 * 
 * The remove method in addition to removing a record from PARTYPROFILE performs
 * the following operations: . removes the (logical) child CONTACT record
 * related to the entity (1) . removes the (logical) child ADDR record related
 * to the CONTACT record (1) . removes all PARTYDEALASSOC records related to the
 * entity removed
 * 
 * (1) PARTYPROFILE records requires a copyId field to satisfy the FK
 * relationship with the (logical) child CONTACT record. However, parties are
 * associated with master deal records not an instance of a deal copy. This
 * implies that the copyId field is not required from a programatic point of
 * view. For simplicity we shall assign value 1 to the copyId field of all party
 * records.
 * 
 * Hence the remove method performs all actions to complete remove the party
 * record and all of its associations from the database.
 * 
 * The class also contains associate() and disassociate() methods (various
 * overloaded versions of each) to allow a party to be associated or
 * disassociated with a deal (or property).
 */

public class PartyProfile extends DealEntity {

    // Filogix-IBC #753 - Sasha - Oct.10th, 2006
    // final static String SOLICITOR_TYPE = "\'50\'";

    protected int partyProfileId;
    protected String partyName;
    protected int profileStatusId;
    protected int contactId;
    protected int partyTypeId;
    protected String ptPShortName;
    protected String ptPBusinessId;
    protected String partyCompanyName;

    // Additional field -- by BILLY 09Jan2002
    protected int copyId;
    protected String notes;

    // --DJ_PT_CR--start--Additional fields//
    protected String GEBranchTransitNum;
    protected String CHMCBranchTransitNum;

    // --DJ_PT_CR--end//

    // --> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    // --> By Billy 20May2004
    protected String bankTransitNum;
    protected String bankAccNum;
    protected String bankName;

    // ====================================================================
    // ***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
    protected String location;
    // ***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//

    // For party association
    protected boolean haveAssoc;
    protected int dealId;
    protected int propertyId;
    protected int institutionProfileId;

    private PartyProfilePK pk;

    public PartyProfile(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    public PartyProfile(SessionResourceKit srk, int partyProfileId)
            throws RemoteException, FinderException {

        super(srk);

        pk = new PartyProfilePK(partyProfileId);

        findByPrimaryKey(pk);
    }

    //
    // create methods
    //
    // minimal create method

    public PartyProfile create(int pPartyType, int pProfileSTatus)
            throws RemoteException, CreateException {

        pk = createPrimaryKey();

        // Make the string save to save
        String sql = "Insert into PartyProfile(PARTYPROFILEID, COPYID, PARTYTYPEID, PROFILESTATUSID,INSTITUTIONPROFILEID ) "
                + "values( "
                + pk.getId()
                + ", 1, "
                + pPartyType
                + ","
                + pProfileSTatus
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PartyProfile Entity  - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        this.haveAssoc = false;
        this.dealId = -1;
        this.propertyId = -1;
        srk.setModified(true);

        return this;
    }

    //
    // create methods
    //
    // --DJ_PT_CR--start//
    // // The signature of the method has been changed since two new fields:
    // CHMCBranchTransitNum
    // // and branchTransitNumCE have been added

    public PartyProfile create(String partyName, int profileStatusId,
            int contactId, int partyTypeId, String ptPShortName,
            String ptPBusinessId, String partyCompanyName, String notes,
            String GEBranchTransitNum, String CHMCBranchTransitNum)
            throws RemoteException, CreateException {

        pk = createPrimaryKey();

        // Make the string save to save
        String sql = "Insert into PartyProfile(PARTYPROFILEID, COPYID, PARTYNAME, PROFILESTATUSID,INSTITUTIONPROFILEID"
                + ",CONTACTID,PARTYTYPEID,PTPSHORTNAME,PTPBUSINESSID,PARTYCOMPANYNAME,NOTES,GEBRANCHTRANSITNUM,CHMCBRANCHTRANSITNUM) "
                + "values("
                + pk.getId()
                + ", 1, '"
                + StringUtil.makeQuoteSafe(partyName)
                + "', "
                + profileStatusId
                + ", "
                + srk.getExpressState().getDealInstitutionId()
                + ","
                + contactId
                + ", "
                + partyTypeId
                + ",'"
                + StringUtil.makeQuoteSafe(ptPShortName)
                + "','"
                + ptPBusinessId
                + "', '"
                + StringUtil.makeQuoteSafe(partyCompanyName)
                + "', '"
                + StringUtil.makeQuoteSafe(notes)
                + "', '"
                + StringUtil.makeQuoteSafe(CHMCBranchTransitNum)
                + "', '"
                + StringUtil.makeQuoteSafe(GEBranchTransitNum) + "')";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PartyProfile Entity  - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        this.haveAssoc = false;
        this.dealId = -1;
        this.propertyId = -1;
        srk.setModified(true);

        return this;
    }

    // --DJ_PT_CR--//
    // // The signature of the method has been changed since two new fields:
    // CHMCBranchTransitNum
    // // and branchTransitNumCE have been added
    public PartyProfile createWithPropertyAssoc(String partyName,
            int profileStatusId, int contactId, int partyTypeId,
            String ptPShortName, String ptPBusinessId, String partyCompanyName,
            int dealId, int propertyId, String GEBranchTransitNum,
            String CHMCBranchTransitNum) throws RemoteException,
            CreateException {

        String sql = getInsertToAssocSQL(dealId, propertyId);

        create(partyName, profileStatusId, contactId, partyTypeId,
                ptPShortName, ptPBusinessId, partyCompanyName, "",
                GEBranchTransitNum, CHMCBranchTransitNum);

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PartyDeal Entity  - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        this.haveAssoc = true;
        this.dealId = dealId;
        this.propertyId = propertyId;
        srk.setModified(true);
        return this;
    }

    public PartyProfile createWithDealAssoc(String partyName,
            int profileStatusId, int contactId, int partyTypeId,
            String ptPShortName, String ptPBusinessId, String partyCompanyName,
            int dealId) throws RemoteException, CreateException {

        return createWithPropertyAssoc(partyName, profileStatusId, contactId,
                partyTypeId, ptPShortName, ptPBusinessId, partyCompanyName,
                dealId, -1, GEBranchTransitNum, CHMCBranchTransitNum);
    }

    public PartyProfilePK createPrimaryKey() throws CreateException {

        String sql = "Select PartyProfileseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1);
                break;
            }
            jExec.closeData(key);
            if (id == -1) {
                throw new CreateException();
            }

        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PartyProfile createPrimary Key() :exception: getting PartyProfileId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }
        return new PartyProfilePK(id);
    }

    // --------------------- DJ, pvcs # 957, Catherine -- begin
    // ---------------------
    /**
     * Catherine Rutgaizer, 4-May-05 This method returns a collection of
     * PartyTypeId(s)
     * 
     * @throws FinderException
     */
    public Set getPartyProfileSources() throws FinderException {

        HashSet lResult = new HashSet();

        String sql = "SELECT PARTYTYPEID FROM PARTYPROFILESOURCE";

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                lResult.add(new Integer(jExec.getInt(key, 1)));
            }
        } catch (Exception e) {
            throw new FinderException("Exception: " + e + ". Finder sql = "
                    + sql);
        }
        return lResult;
    }

    /**
     * Catherine Rutgaizer, 4-may-05 this method is used in DjDupeCheckProcessor
     * 
     * @param dealId
     * @return Map - a Map of PartyProfileId(s) mapped to their partyTypeId for
     *         a given Deal
     * @throws FinderException
     */
    public Map getTypedParties4Dj(int dealId) throws FinderException {

        Map lResult = new HashMap(5); // 5
        // records
        // is
        // enough
        // based
        // on
        // research

        String sql = "SELECT P.PARTYTYPEID, P.PARTYPROFILEID "
                + "FROM PARTYDEALASSOC A, "
                + "     PARTYPROFILE P  "
                + "WHERE P.PARTYPROFILEID = A.PARTYPROFILEID  "
                + "AND ( "
                + "    P.PARTYTYPEID IN (SELECT PARTYTYPEID FROM PARTYPROFILESOURCE) "
                + " OR P.PARTYTYPEID = " + Mc.PARTY_TYPE_ORIGINATION_BRANCH
                + "  ) " + " AND A.DEALID = " + dealId;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                lResult.put(new Integer(jExec.getInt(key, 1)), new Integer(
                        jExec.getInt(key, 2)));
            }

            jExec.closeData(key);

        } catch (Exception e) {

            throw new FinderException("PartyProfile@getTypedParties: error: "
                    + e + " . Finder SQL = " + sql);
        }

        return lResult;
    }

    // --------------------- DJ, pvcs # 957, Catherine -- end
    // ---------------------

    // --------------------- DJ, pvcs # 957, 6-Jul-05 Catherine -- begin
    // ---------------------
    /*
     * Returns a hash map of PartyProfile records associated to the given deal
     * through PARTYDEALASSOC table partyTypeId serves as a map key
     */
    public Map findByDealId(int dealId) throws RemoteException, FinderException {

        Map lResult = new HashMap(5); // 5
        // records
        // is
        // enough
        // based
        // on
        // research

        String sql = "SELECT p.PARTYTYPEID, a.PARTYPROFILEID FROM PARTYDEALASSOC A, PARTYPROFILE P "
                + " WHERE a.PARTYPROFILEID = p.PARTYPROFILEID AND a.DEALID = "
                + dealId;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {

                PartyProfile current = new PartyProfile(srk, jExec.getInt(key,
                        2));
                current.haveAssoc = true; // (?!)
                // Catherine:
                // not
                // sure
                // why
                // we
                // do
                // this

                lResult.put(new Integer(jExec.getInt(key, 1)), current);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger.error("Finder sql: " + sql);
            logger.error(e);
            throw new FinderException();
        }

        return lResult;
    }

    // --------------------- DJ, pvcs # 957, 6-Jul-05 Catherine -- end
    // ---------------------

    // 
    // remove methods
    //

    public void remove() throws RemoteException, RemoveException {

        try {
            jExec
                    .executeUpdate("delete from partydealassoc where partyprofile = "
                            + this.partyProfileId);

            // to remove a party record we must remove the contact record it
            // points
            // to, and the address record
            // pointed to be the contact record.

            Contact contact = new Contact(getSessionResourceKit(),
                    getContactId(), 1);

            jExec.executeUpdate("delete from addr where ADDRID = "
                    + contact.getAddrId());
            jExec.executeUpdate("delete from contact where CONTACTID = "
                    + contact.getContactId());

            jExec
                    .executeUpdate("delete from partyprofile where partyprofile = "
                            + this.partyProfileId);
        } catch (Exception e) {
            RemoveException ce = new RemoveException(
                    "PartyProfile Entity: remove() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }
    }

    /* Delete part of association record, related to given dealId and propertyId */
    // // What is it?
    public int removeAssoc(int dealId, int propertyId) {

        return 0;
    }

    //
    // finders
    //

    public PartyProfile findByPrimaryKey(PartyProfilePK pk)
            throws RemoteException, FinderException {
    	
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from PartyProfile " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: PartyProfile - @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity: PartyProfile - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        haveAssoc = false;
        dealId = -1;
        propertyId = -1;

        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public PartyProfile findByPartyProfileId(int id) throws RemoteException,
            FinderException {

        return findByPrimaryKey(new PartyProfilePK(id));
    }

    public String getPartyType() throws FinderException {

        String sql = "Select PTDESCRIPTION from PARTYTYPE where PARTYTYPEID="
                + getPartyTypeId();
        String partyType;
        try {
            int key = jExec.execute(sql);
            jExec.next(key);
            partyType = jExec.getString(key, 1);
            jExec.closeData(key);
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Party Profile Entity getPartyType() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }
        return partyType;
    }

    public PartyProfile findByDealSolicitor(int inDealId)
            throws FinderException {

        String sql = "Select p.*, "
                + "a.dealId, a.propertyId from PartyProfile p, PartyDealAssoc a where "
                + "a.dealId = "
                + inDealId
                + " and a.propertyId is NULL and p.partyprofileid = a.partyprofileid and "
                + "p.PARTYTYPEID = "
                + Integer.toString(Mc.PARTY_TYPE_SOLICITOR);
        // Filogix-IBC #753 - Sasha - Oct.10th, 2006
        // "p.PARTYTYPEID = " + SOLICITOR_TYPE;

        this.haveAssoc = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                haveAssoc = true;
                setPropertiesFromQueryResult(key);

                // technically possible to be more than one record but we take
                // the first
                break;
            }
            jExec.closeData(key);

            if (haveAssoc == false) {
                String msg = "Party Profile Deal Entity: findByDealSolicitor(), DealId= "
                        + inDealId + ", Solicitor not found";

                if (getSilentMode() == false) {
                    logger.error(msg);

                }
                throw new FinderException(msg);
            }

        } catch (Exception e) {
            if (haveAssoc == false && getSilentMode() == true) {
                throw (FinderException) e;
            }

            FinderException fe = new FinderException(
                    "Deal Entity - findByDealSolicitor() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        dealId = inDealId;
        propertyId = -1;

        this.pk = new PartyProfilePK(partyProfileId);
        return this;
    }

    public PartyProfile findByBusinessIdAndType(String theBusinessId, int pType)
            throws RemoteException, FinderException {

        String sql = "Select * from PartyProfile where  "
                + StringUtil.formFieldMatchExpression("ptpBusinessId",
                        theBusinessId, false) + " AND partyTypeId = " + pType;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                this.pk = new PartyProfilePK(this.partyProfileId);

                // take first only
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity PartyProfile: findByBusinessIdAndType = "
                        + theBusinessId + ":" + pType;
                logger.error(msg);
                throw new FinderException();
            }
        } catch (Exception e) {
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw new FinderException();
        }

        this.haveAssoc = false;
        this.dealId = -1;
        this.propertyId = -1;

        return this;
    }

    public PartyProfile findByShortName(String theShortName)
            throws RemoteException, FinderException {

        String sql = "Select * from PartyProfile where "
                + StringUtil.formFieldMatchExpression("ptPShortName",
                        theShortName, false);

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                this.pk = new PartyProfilePK(this.partyProfileId);

                // take first only
                break;
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity PartyProfile: findByShortName = "
                        + theShortName;
                logger.error(msg);
                throw new FinderException();
            }
        } catch (Exception e) {
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw new FinderException();
        }

        this.haveAssoc = false;
        this.dealId = -1;
        this.propertyId = -1;

        return this;
    }

    public int extractIngestionProfileStatus(int pSobCategory, int pPartyType)
            throws FinderException {

        String sql = " Select * from partyprofilesource where sourceofbusinesscategoryid = "
                + pSobCategory + " AND partytypeid = " + pPartyType;

        //boolean gotRecord = false;

        int rv = -1;
        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                rv = jExec.getInt(key, 3);
                //gotRecord = true;
                break;
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger
                    .error("Error in PartyProfile findByPropertyDealAndType - sql: "
                            + sql);
            logger.error(e);
            throw new FinderException();
        }

        return rv;
    }

    /**
     * 
     * Find all parties associated with a deal.
     * 
     * Args: typeId == -1 find all parties associated with the deal ==
     * partyTypeId - find only parties of the specified type (likely only one
     * party in this case) //ALERT THIS NEEDS REPAIR.....
     */

    public Collection<PartyProfile> findByDealAndType(DealPK pk, int typeId)
            throws RemoteException, FinderException {

        String sql = "Select p.PARTYPROFILEID, a.dealId, a.propertyId from PartyProfile p, PartyDealAssoc a where "
            + "p.institutionprofileid = a.institutionprofileid AND "    
            + "p.PARTYPROFILEID = a.PARTYPROFILEID AND a.dealId = "
                + pk.getId();

        if (typeId != -1) {
            sql = sql + " AND p.PARTYTYPEID = " + typeId;

        }

        Collection<PartyProfile> records = new ArrayList<PartyProfile>();
        Vector v = new Vector();
        try {
            int count = 0;
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                count++;
                CollectionResult cr = new CollectionResult();
                cr.partyProfileId = jExec.getInt(key, 1);
                cr.dealId = jExec.getInt(key, 2);
                cr.propertyId = jExec.getInt(key, 3);
                v.add(cr);
            }

            jExec.closeData(key);

            // now get the party entities
            for (int i = 0; i < count; ++i) {
                CollectionResult cr = (CollectionResult) v.elementAt(i);

                PartyProfile current = new PartyProfile(srk, cr.partyProfileId);
                current.setDealId(cr.dealId);
                current.setPropertyId(cr.propertyId);
                current.haveAssoc = true;
                records.add(current);
            }
        } catch (Exception e) {
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw new FinderException();
        }

        return records;
    }

    /**
     * 
     * Find all parties associated with a deal.
     * 
     * Args: typeId == -1 find all parties associated with the deal ==
     * partyTypeId - find only parties of the specified type (likely only one
     * party in this case) //ALERT THIS NEEDS REPAIR.....
     */

    public Collection<PartyProfile> findByDealAndType(int dealId, int typeId)
            throws RemoteException, FinderException {

        String sql = "Select p.PARTYPROFILEID, a.dealId, a.propertyId from PartyProfile p, PartyDealAssoc a where "
            + "p.institutionprofileid = a.institutionprofileid AND "       
            + "p.PARTYPROFILEID = a.PARTYPROFILEID AND a.dealId = "
                + dealId;

        if (typeId != -1) {
            sql = sql + " AND p.PARTYTYPEID = " + typeId;

        }

        Collection records = new ArrayList();
        Vector v = new Vector();
        try {
            int count = 0;
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                count++;
                CollectionResult cr = new CollectionResult();
                cr.partyProfileId = jExec.getInt(key, 1);
                cr.dealId = jExec.getInt(key, 2);
                cr.propertyId = jExec.getInt(key, 3);
                v.add(cr);
            }

            jExec.closeData(key);

            // now get the party entities
            for (int i = 0; i < count; ++i) {
                CollectionResult cr = (CollectionResult) v.elementAt(i);

                PartyProfile current = new PartyProfile(srk, cr.partyProfileId);
                current.setDealId(cr.dealId);
                current.setPropertyId(cr.propertyId);
                current.haveAssoc = true;
                records.add(current);
            }
        } catch (Exception e) {
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw new FinderException();
        }

        return records;
    }

    // SEAN issue #957 added to find by name and type id.
    public Collection findByNameAndTypeId(String pName, int pTypeId)
            throws RemoteException, FinderException {

        StringBuffer buf = new StringBuffer("Select * from partyProfile where ");
        buf.append(StringUtil.formFieldMatchExpression("trim(partyName)",
                pName, false));
        buf.append(" AND partyTypeId = " + pTypeId);
        Collection records = new ArrayList();
        String sql = buf.toString();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                PartyProfile current = new PartyProfile(srk);
                current.setPropertiesFromQueryResult(key);
                current.pk = new PartyProfilePK(current.getPartyProfileId());
                records.add(current);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger
                    .error("Error in PartyProfile findByPropertyDealAndType - sql: "
                            + sql);
            logger.error(e);
            throw new FinderException();
        }

        return records;
    }

    // SEAN issue #957 END

    public Collection findByNameCompanyNameAndTypeId(String pName,
            String pCompanyName, int pTypeId) throws RemoteException,
            FinderException {

        StringBuffer buf = new StringBuffer("Select * from partyProfile where ");
        buf.append(StringUtil.formFieldMatchExpression("trim(partyName)",
                pName, false));
        buf.append(" AND ");
        buf.append(StringUtil.formFieldMatchExpression(
                "trim(partyCompanyName)", pCompanyName, false));
        buf.append(" AND partyTypeId = " + pTypeId);

        Collection records = new ArrayList();

        String sql = buf.toString();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                PartyProfile current = new PartyProfile(srk);
                current.setPropertiesFromQueryResult(key);
                current.pk = new PartyProfilePK(current.getPartyProfileId());
                records.add(current);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger
                    .error("Error in PartyProfile findByPropertyDealAndType - sql: "
                            + sql);
            logger.error(e);
            throw new FinderException();
        }

        return records;
    }

    /**
     * 
     * Find all parties associated with a Property and it's parent Deal where
     * type is as given.
     * 
     * @param property -
     *            the associated property provides the associated DealId
     * @param typeId -
     *            the partyType
     * @return a Collection of Profiles
     */
    public Collection findByPropertyDealAndType(Property property, int typeId)
            throws RemoteException, FinderException {

        StringBuffer buf = new StringBuffer(
                "Select * from partyProfile where partyProfileId in(");
        buf
                .append(" Select partyProfileId from partydealassoc where propertyId = ");
        buf.append(property.getPropertyId()).append(" AND dealId = ");
        buf.append(property.getDealId()).append(") AND partyTypeId = ").append(
                typeId);

        Collection records = new ArrayList();

        String sql = buf.toString();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                PartyProfile current = new PartyProfile(srk);
                current.setPropertiesFromQueryResult(key);
                current.pk = new PartyProfilePK(current.getPartyProfileId());
                records.add(current);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger
                    .error("Error in PartyProfile findByPropertyDealAndType - sql: "
                            + sql);
            logger.error(e);
            throw new FinderException();
        }

        return records;
    }

    // Added partyProfileId as checking criteria -- By BILLY 08March2001
    public Collection findByDupeCheckCriteria(String firstName,
            String lastName, int partyTypeId) throws RemoteException,
            FinderException {

        StringBuffer sql = new StringBuffer("Select * from partyprofile where ");
        sql.append("partytypeid = " + partyTypeId + " AND ");
        sql.append("contactid in( select contactid from contact where ");
        sql.append(
                StringUtil.formFieldMatchExpression("contactFirstName",
                        firstName, false)).append(" AND");
        sql.append(
                StringUtil.formFieldMatchExpression("contactLastName",
                        lastName, false)).append(")");

        Collection records = new ArrayList();

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                PartyProfile current = new PartyProfile(srk);
                current.setPropertiesFromQueryResult(key);
                current.pk = new PartyProfilePK(current.getPartyProfileId());
                records.add(current);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            logger
                    .error("Error in PartyProfile findByDupeCheckCriteria - sql: "
                            + sql);
            logger.error(e);
            throw new FinderException();
        }

        return records;
    }

      /*
       * <p>findInstitionProfileIdByGEID</p>
       * <p> added for ML project to handle MIReply for DJ
       * DJ uses this value to identify institition.
       * to call this method, VPD MUST NOT SET and the connection should be able to 
       * addess to all institutions.
       * @param geid: String
       * @return institionProfileId: int
       * 
       */  
        public int findInstitionProfileIdByGEID(String geid) throws FinderException
        {
            return findInstitionProfileIdUniqueId("GEBRANCHTRANSITNUMBER", geid);          
        }
  /*
   * <p>findInstitionProfileIdUniqueId</p>
   * <p> added for ML project to handle MIReply. This method can seach 
   * InsutitionProfileId by any unique key
   * @param columnName: String
   * @param id: String
   * @return institionProfileId: int
   * 
   */  
    public int findInstitionProfileIdUniqueId(String columnName, String id) throws FinderException
    {
        String sql = "select InstitionProfileId from PARTYPROFILE where " +  columnName + " = '" + id + "'"; 
        int institutionId = -1;
        try
        {
          int key = jExec.execute(sql);
    
          for(; jExec.next(key); )
          {
            institutionId = jExec.getInt(key, "INSTITUTIONPROFILEID");
            break;
          }
    
          jExec.closeData(key);
          if (institutionId >= 0)
            return institutionId;
          else
            throw new FinderException("institionProfileId cannot find by GEID: " + id);
    
        }
        catch(Exception e)
        {
          logger.error("Error in PartyProfile findInstitionProfileIdUniqueId - sql: " + sql);
          logger.error(e);
          throw new FinderException();
        }
    
  }
  
  class PartyProfileSource
  {
        int ppsSOBCategoryId;
        int ppsPartyTypeId;
        int ppsProfileStatusId;
    }

    class CollectionResult {
        int partyProfileId;
        int dealId;
        int propertyId;
    }

    //
    // business methods
    //

    /**
     * associate:
     * 
     * The associate() methods allows a party to be associated with a deal or
     * with a property; in the latter case the party type is expected to be an
     * appraiser but this is not checked.
     * 
     * An associate method is expected to be called within the context of a
     * transaction established by the caller. As such the caller must perform
     * the ulntimate commit or rollback of the transaction.
     * 
     * public void associate(int dealId)
     * 
     * Assoicate the party with a deal. Method throws exception if party already
     * associated with the deal.
     * 
     * public void associate(int dealId, int propertyId)
     * 
     * Assoicate the party with a property (and deal). Method throws exception
     * if party already associated with the property.
     * 
     */

    public void associate(int dealId, int propertyId) throws RemoteException {

        String sql = getInsertToAssocSQL(dealId, propertyId);

        try {
            jExec.executeUpdate(sql);
        } catch (Exception e) {
            RemoteException ce = new RemoteException(
                    "PartyProfile Entity: associate() exception, dealId = "
                            + dealId + ", propertyId = " + propertyId);
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        // if entity does not represent an association then adopt association
        // just
        // set
        if (haveAssoc == false) {
            haveAssoc = true;
            this.dealId = dealId;
            this.propertyId = propertyId;
        }
    }

    public void associate(int dealId) throws RemoteException {

        associate(dealId, -1);
    }

    /**
     * disassociate()
     * 
     * The disassociate() methods allows a party to be disassociated from a deal
     * or from a property; in the latter case the party type is expected to be
     * an appraiser but this is not checked.
     * 
     * An disassociate method is expected to be called within the context of a
     * transaction established by the caller. As such the caller must perform
     * the untimate commit or rollback of the transaction.
     * 
     * public void disassociate()
     * 
     * Disassociate the patry from the deal or property it is associated based
     * upon the current values of the association properties (dealId and
     * propertyId).
     * 
     * public void associate(int dealId)
     * 
     * Disassoicate the party from a deal.
     * 
     * public void associate(int dealId, int propertyId)
     * 
     * Disassoicate the party from a property (and deal).
     * 
     */

    public void disassociate(int dealId, int propertyId) throws RemoteException {

        String sql = getRemoveFromAssocSQL(dealId, propertyId);

        try {
            jExec.executeUpdate(sql);
        } catch (Exception e) {
            RemoteException ce = new RemoteException(
                    "PartyProfile Entity: disassociate() exception, dealId = "
                            + dealId + ", propertyId = " + propertyId);
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        // check to see if have removed current association (if one)
        if (haveAssoc) {
            if (this.dealId == dealId && this.propertyId == propertyId) {
                haveAssoc = false;
                this.dealId = -1;
                this.propertyId = -1;
            }
        }
    }

    public void disassociate(int dealId) throws RemoteException {

        disassociate(dealId, -1);
    }

    public void disassociate() throws RemoteException {

        if (haveAssoc == false) {
            RemoteException ce = new RemoteException(
                    "PartyProfile Entity: disassociate() exception, entity does not contain association values");
            logger.error(ce.getMessage());

            throw ce;
        }

        disassociate(dealId, propertyId);
    }

//    public void removeAssocForDeal(DealPK pk) throws RemoteException,
    public void removeAssocForDeal(MasterDealPK pk) throws RemoteException,
            RemoveException {

        try {
            jExec.executeUpdate("Delete from partydealassoc where dealid = "
                    + pk.getId());
        } catch (Exception e) {

            RemoveException ce = new RemoveException(
                    "PartyDealAssoc remove() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }
    }

    public String getPartyStatus() throws FinderException {

        String sql = "Select psdescription from profilestatus where profilestatusid="
                + getProfileStatusId();
        String partyStatus;
        try {
            int key = jExec.execute(sql);
            jExec.next(key);
            partyStatus = jExec.getString(key, 1);
            jExec.closeData(key);
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Party Profile Entity getPartyStatus() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }
        return partyStatus;
    }

    public List getSecondaryParents() throws Exception {

        List sps = new ArrayList();

        Contact lContact = getContact();

        if (lContact != null) {
            sps.add(lContact);

        }
        return sps;
    }

    public Contact getContact() throws FinderException, RemoteException {

        return new Contact(srk, this.contactId, 1);
    }

    // GETTERS & SETTERS

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    public int getPartyProfileId() {

        return this.partyProfileId;
    }

    public String getPartyName() {

        return this.partyName;
    }

    public int getProfileStatusId() {

        return this.profileStatusId;
    }

    public int getContactId() {

        return this.contactId;
    }

    public int getPartyTypeId() {

        return this.partyTypeId;
    }

    public String getPtPShortName() {

        return this.ptPShortName;
    }

    public String getPtPBusinessId() {

        return this.ptPBusinessId;
    }

    public String getPartyCompanyName() {

        return this.partyCompanyName;
    }

    public boolean getHaveAssoc() {

        return this.haveAssoc;
    }

    public int getDealId() {

        return this.dealId;
    }

    public int getPropertyId() {

        return this.propertyId;
    }

    // Additional Fields -- By BILLY 09Jan2002
    public int getCopyId() {

        return this.copyId;
    }

    public String getNotes() {

        return this.notes;
    }

    // --> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    // --> By Billy 20May2004
    public String getBankAccNum() {

        return bankAccNum;
    }

    public String getBankName() {

        return bankName;
    }

    public String getBankTransitNum() {

        return bankTransitNum;
    }

    // =========================================================================

    // --DJ_PT_CR--start--Additional fields//
    public String getCHMCBranchTransitNum() {

        return this.CHMCBranchTransitNum;
    }

    public String getGEBranchTransitNum() {

        return this.GEBranchTransitNum;
    }

    // --DJ_PT_CR--end//

    /**
     * getLocation Returns location attribute value
     * 
     * @param none
     * 
     * @return String : the value of the location attribute
     * @author NBC/PP Implementation Team
     * @version 1.0 (Intial Version - 29 May 2006)
     */
    public String getLocation() {

        return this.location;
    }

    public void setPartyProfileId(int value) {

        this.partyProfileId = value;
        this.testChange("partyProfileId", value);
    }

    public void setPartyName(String value) {

        this.partyName = value;
        this.testChange("partyName", value);
    }

    public void setProfileStatusId(int value) {

        this.profileStatusId = value;
        this.testChange("profileStatusId", value);
    }

    public void setContactId(int value) {

        this.contactId = value;
        this.testChange("contactId", value);
    }

    public void setPartyTypeId(int value) {

        this.partyTypeId = value;
        this.testChange("partyTypeId", value);
    }

    public void setPtPShortName(String value) {

        this.ptPShortName = value;
        this.testChange("ptPShortName", value);
    }

    public void setPtPBusinessId(String value) {

        this.ptPBusinessId = value;
        this.testChange("ptPBusinessId", value);
    }

    public void setPartyCompanyName(String value) {

        this.partyCompanyName = value;
        this.testChange("partyCompanyName", value);
    }

    public void setDealId(int value) {

        this.dealId = value;
        if (dealId == -1) {
            this.testChange("dealId", value);
        }
    }

    public void setPropertyId(int value) {

        this.propertyId = value;
        if (propertyId != -1) {
            this.testChange("propertyId", value);
        }
    }

    // Additional Fields -- By BILLY 09Jan2002
    public void setCopyId(int value) {

        this.copyId = value;
        this.testChange("copyId", value);
    }

    public void setNotes(String value) {

        this.notes = value;
        this.testChange("notes", value);
    }

    // --> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    // --> By Billy 20May2004
    public void setBankAccNum(String value) {

        this.bankAccNum = value;
        this.testChange("bankAccNum", value);
    }

    public void setBankName(String value) {

        this.bankName = value;
        this.testChange("bankName", value);
    }

    public void setBankTransitNum(String value) {

        this.bankTransitNum = value;
        this.testChange("bankTransitNum", value);
    }

    // ========================================================================

    // --DJ_PT_CR--start--Additional fields//
    public void setCHMCBranchTransitNum(String value) {

        this.CHMCBranchTransitNum = value;
        this.testChange("CHMCBranchTransitNum", value);
    }

    public void setGEBranchTransitNum(String value) {

        this.GEBranchTransitNum = value;
        this.testChange("GEBranchTransitNum", value);
    }

    // --DJ_PT_CR--end//

    /**
     * setLocationR <br>
     * This method sets the value for the location attribute
     * 
     * @param value
     *            String <br>
     *            String representing the location field value on a JSP
     * 
     * @return none
     * @author NBC/PP Implementation Team
     * @version 1.0 (Initial Version - 29 May 2006)
     */
    public void setLocation(String value) {

        this.location = value;
        this.testChange("location", value);
    }

    public IEntityBeanPK getPk() {

        return this.pk;
    }

    //
    // implementation
    //

    private void setPropertiesFromQueryResult(int key) throws Exception {

        // --> Change to get the result by Colname ==> remove the dependance of
        // field order in DB
        // --> By Billy 20May2004
        this.setPartyProfileId(jExec.getInt(key, "partyProfileId"));
        this.setPartyName(jExec.getString(key, "partyName"));
        this.setProfileStatusId(jExec.getInt(key, "profileStatusId"));
        this.setContactId(jExec.getInt(key, "contactId"));
        this.setPartyTypeId(jExec.getInt(key, "partyTypeId"));
        this.setPtPShortName(jExec.getString(key, "ptPShortName"));
        this.setPtPBusinessId(jExec.getString(key, "ptPBusinessId"));
        this.setPartyCompanyName(jExec.getString(key, "partyCompanyName"));
        // Additional Fields -- By BILLY 09Jan2002
        this.setCopyId(jExec.getInt(key, "copyId"));
        this.setNotes(jExec.getString(key, "notes"));
        // --DJ_PT_CR--start--Additional fields//
        this.setGEBranchTransitNum(jExec.getString(key, "GEBranchTransitNum"));
        this.setCHMCBranchTransitNum(jExec.getString(key,
                "CHMCBranchTransitNum"));
        // --DJ_PT_CR--end//
        // --> Ticket#369 :: Change for Cervus :: added New Bank info for Party
        // --> By Billy 20May2004
        this.setBankTransitNum(jExec.getString(key, "bankTransitNum"));
        this.setBankAccNum(jExec.getString(key, "bankAccNum"));
        this.setBankName(jExec.getString(key, "bankName"));
        // ====================================================================
        // ***** Change by NBC/PP Implementation Team - Version 1.1 - Start
        // *****//
        this.setLocation(jExec.getString(key, "location"));
        // ***** Change by NBC/PP Implementation Team - Version 1.1 - End
        // *****//
        this.setInstitutionProfileId(jExec.getInt(key, "InstitutionProfileId"));
    }

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    /**
     * Gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public Object getFieldValue(String fieldName) {

        return doGetFieldValue(this, this.getClass(), fieldName);
    }

    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public String getEntityTableName() {

        return "PartyProfile";
    }

    public int getClassId() {

        return ClassId.PARTYPROFILE;
    }

    public boolean isAuditable() {

        return true;
    }

    private String getInsertToAssocSQL(int dealId, int propertyId) {

        String sql = "Insert into PartyDealAssoc(partyprofileid, dealId, propertyId,institutionProfileId ) VALUES ("
                + partyProfileId
                + ", "
                + dealId
                + ", "
                + ((propertyId == -1) ? "NULL" : ("" + propertyId))
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        return sql;
    }

    private String getRemoveFromAssocSQL(int dealId, int propertyId) {

        String sql =

        "Delete from PartyDealAssoc " + "where partyprofileid = "
                + getPartyProfileId() + " and dealId = " + dealId;

        // -2 == remove associations (in theory itis possible for a single party
        // to be associated with a specific deal mutiple time (e.g. appraiser
        // for multiple properties) - -2 will remove all such associations.
        if (propertyId == -2) {
            return sql;
        }

        sql = sql +

        " and propertyId "
                + ((propertyId == -1) ? " IS NULL" : (" = " + propertyId));

        return sql;
    }
}
