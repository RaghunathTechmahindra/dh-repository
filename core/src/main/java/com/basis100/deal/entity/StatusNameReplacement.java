/**
 * <p>Title: DealEventNameReplacement.java</p>
 *
 * <p>Description: entity class for DealEventNameReplacement</p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 4-May-09
 *
 */
package com.basis100.deal.entity;

import java.io.Serializable;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * this is static table so that no create/update method is required
 * @author maida
 *
 */
public class StatusNameReplacement extends DealEntity implements
        Serializable {
    
    int statusNameReplacementId;
    String statusNameReplacement;
    

    /**
     * @param srk SessionResourceKit to use
     * @throws RemoteException
     */    
    public StatusNameReplacement (SessionResourceKit srk) throws RemoteException {
        super(srk);
    }

    /**
     * 
     * @param srk SessionResourceKit to us
     * @param id DealEventStatusSnapshot id
     * @throws RemoteException
     */
    public StatusNameReplacement (SessionResourceKit srk, int id) throws RemoteException {
        super(srk);
        statusNameReplacementId = id;
    }

    /**
     * Find the row corresponding to this PK and populate entity
     * @param pk The primary key represetning this row
     * @return This (now populated) entity
     */
    public StatusNameReplacement findByPrimaryKey(int id) 
        throws RemoteException, FinderException {
        
        String sql = "SELECT * FROM STATUSNAMEREPLACEMENT WHERE STATUSNAMEREPLACEMENTID = " + id;
        boolean gotRecord = false;
        
        try {
            int key = jExec.execute(sql);
            for(;jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                //break;
            }
            
            jExec.closeData(key);
            
            if(!gotRecord) {
                String msg = "STATUSNAMEREPLACEMENT Entity: @findByQuery(), key= " + id + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch(Exception e) {
            FinderException fe = new FinderException("STATUSNAMEREPLACEMENT findByPrimaryKey(): exception: " + e);
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
              
            throw fe;
        }
        
        return this;
    }

    
    private void setPropertiesFromQueryResult(int key) throws Exception {
        setStatusNameReplacementId(jExec.getInt(key, "StatusNameReplacementId"));
        setStatusNameReplacement(jExec.getString(key, "StatusNameReplacement"));
    }
    
    // getters and setters STARTS
    public int getStatusNameReplacementId() {
        return statusNameReplacementId;
    }
    public void setStatusNameReplacementId(int statusNameReplacementId) {
        this.statusNameReplacementId = statusNameReplacementId;
    }
    public String getStatusNameReplacement() {
        return statusNameReplacement;
    }
    public void setStatusNameReplacement(String statusNameReplacement) {
        this.statusNameReplacement = statusNameReplacement;
    }
    // getters and setters ENDS
}
