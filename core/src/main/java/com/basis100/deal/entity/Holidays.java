package com.basis100.deal.entity;

import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.deal.pk.*;

import com.basis100.deal.util.DBA;

/**
 * A class representing holiday entity specific for MOS.
 * When a holiday entity is created a record is inserted in the Holidays table.
 * The unique field holidaysId is populated with the holiday day number.
 *
 * @author  Bartek Jasionowski
 * @version
 * @see
 */
public class Holidays extends DealEntity {

   EntityContext ctx;

   protected int    holidayId;
   protected String holidayName;
   protected java.util.Date holidayDate;
   protected int startHour;
   protected int startMinute;
   protected int endHour;
   protected int endMinute;
   protected String statuaryHoliday;
   protected String recurring;
   protected String spanWeekend;

    // SEAN ticket #494: Add the variable province id.
    protected int provinceId = 0;
    // SEAN ticket #494 END

   private static Hashtable holidayTable = new Hashtable();

   HolidaysPK pk;

   // derived attributes: start and end times (minutes from beginning of day (set via calcStartEndTimes())
   private  int startTime;
   private  int endTime;

   public Holidays(){}

   public Holidays(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

    public Holidays(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      HolidaysPK pk = new HolidaysPK(id);

      findByPrimaryKey(pk);
   }


   //public Holidays create(int holidayId, int provinceId) throws RemoteException, CreateException
    public Holidays create(int holidayId) throws RemoteException, CreateException
    {
      pk = createPrimaryKey(holidayId);

      String sql = "Insert Into Holidays (HOLIDAYID, PROVINCEID) Values (" + pk.getId() + ", " + provinceId + ")";
      //String sql = "Insert Into Holidays (HOLIDAYID, HOLIDAYDATE, PROVINCEID) Values ("17001", '' ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("Holidays Entity - Holidays - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
      return this;
    }

 /**
    * Attempts to remove the record that corresponds to the holiday id.
    *
    * @param
    * @return
 */

    public void remove() throws RemoteException, RemoveException
    {
        String sql = "Delete from HOLIDAYS where holidayId = " + getHolidayId();
        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoveException re = new RemoveException("Holidays - remove() exception");

            logger.error(re.getMessage());
            logger.error("remove sql: " + sql);
            logger.error(e);

            throw re;
        }
    }


   /**
    * Attempts to find the record that corresponds to the description/ name disregarding Case
    * @param  a String representation of description
    * @return Holidays
    */

   public Holidays findByName(String description) throws RemoteException, FinderException
   {
      if (description == null)
        return null;

      description = description.toUpperCase();
      String sql = "Select * from Holidays where upper(holidayName) = '" + description + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Business Calendar: Holidays: @findbyName = " + description;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }
      return this;
   }



   /**
    * Attempts to find the record that corresponds to the primary key.
    * @param  a HolidaysPK primary key
    * @return this instance
    */
    public Holidays findByPrimaryKey(HolidaysPK pk) throws RemoteException, FinderException
   {
      String sql = "Select * from Holidays where " + pk.getName().toUpperCase()+ " = " + pk.getId() + "";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              /**can only be one record*/
              break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Business Calendar: Holidays: @findByPrimaryKey(), key= " + pk + ", holiday not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Business Calendar: Holidays - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return this;
    }


  /**
    *
    *
    * @param  a String representation of the name
    * @return Vector of holidays
    */

    public Vector findByHolidayId() throws RemoteException, FinderException
   {
      String sql = "Select * from Holidays";
      logger.debug("SQL Statement: " + sql);

      boolean gotRecord = false;
      Vector records = new Vector();

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              /**all records into hash table*/
              Holidays hol = new Holidays(srk);
              hol.setPropertiesFromQueryResult(key);

              // Bug fix -- set the key value with the formula
              holidayTable.put(new Integer(getDayNumber(hol.getHolidayDate())), hol);
              logger.debug("Added the following holiday to the holiday Table: ");
              logger.debug("    Holiday Name: " + hol.getHolidayName());
              logger.debug("    Holiday Date: " + hol.getHolidayDate());
              logger.debug("     Province ID: " + hol.getProvinceId());

              records.addElement(hol);
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Business Calendar: Holidays: @findByHolidayId()";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Business Calendar: Holidays - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return records;
    }

/**
  *   similar to a clone operation - however does not return a true clone of opject since
  *   the 'data' members copied (e.g. not concerned with base class members, session resource
  *   kit, etc). Assumption is that the 'copy' will be used only for data access/manipulation
  *   only, not for updating the entity to persistent storage.
  *
  *   @return an int
  */

    public Holidays privateCopy()
    {
      Holidays h = new Holidays();

      h.setStartHour(getStartHour());
      h.setStartMinute(getStartMinute());
      h.setEndHour(getEndHour());
      h.setEndMinute(getEndMinute());

      h.setHolidayId(getHolidayId());
      h.setHolidayName(getHolidayName());
      h.setHolidayDate(getHolidayDate());
      h.setStatuaryHoliday(getStatuaryHoliday());
      h.setRecurring(getRecurring());
      h.setSpanWeekend(getSpanWeekend());

      return h;
   }


/**
  * Convienence methods for adding units to various attributes; units to be 'added' may be
  * negative amounts.
  *
  * The result is a flag that indicates if the result of the operation is within the normal
  * integrity range for the attribute manipulated. Example:
  *
  *      assume startMinute = 30
  *
  *      addToStartMinute(10) == true (result 40 within range 0-59)
  *      addToStartMinute(40) == false (result 70 outside integrity range)
  *
  * Application has responsibility for ensuring ultimate integrity of results of an operation; e.g.
  * if adding to minutes causes an integrity problem application must correct.
  *
  **/

    public boolean addToStartHour (int units)
    {
      startHour += units;

      return hourIntegrity(startHour);
    }

    public boolean addToEndHour (int units)
    {
      endHour += units;

      return hourIntegrity(endHour);
    }

    public boolean addToStartAndEndHour (int units)
    {
      startHour += units;
      endHour += units;

      return hourIntegrity(startHour) && hourIntegrity(endHour);
    }

    public boolean addToStartMinute (int units)
    {
      startMinute += units;

      return minuteIntegrity(startMinute);
    }

    public boolean addToEndMinute (int units)
    {
      endMinute += units;

      return minuteIntegrity(endMinute);
    }

    public boolean addToStartAndEndMinute (int units)
    {
      startMinute += units;
      endMinute += units;

      return minuteIntegrity(startMinute) && minuteIntegrity(endMinute);
    }

/**
  * Calculate start and end times - minutes from beginning of day
  *
  **/

  public void calcStartEndTimes()
  {
    startTime = startHour * 60 + startMinute;
    endTime   = endHour   * 60 + endMinute;
  }

    /**
    * Methods implemented in Enterprise Bean Instances that have no remote object
    * counterparts - i.e. EJB container callback methods, and context related methods.
    **/
    public void setEntityContext(EntityContext ctx)
    {
        this.ctx = ctx;
    }

    public void unsetEntityContext()
    {
        ctx = null;
    }

    public void ejbActivate() throws RemoteException{;}
    public void ebjPassivate () throws RemoteException {;}

    public void ejbLoad() throws RemoteException
    {
        /**implementation not required for now!*/
    }



    public void ejbStore() throws RemoteException
    {
        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        String sql = "Update HOLIDAYS set "       +
                     "holidayId    = "            + getHolidayId() + ", " +
                     "holidayName  = '"           + getHolidayName() + "', " +
                     "holidayDate = "            + DBA.sqlStringFrom(getHolidayDate()) + ", " +
                     "businessDaystartHour = "    + getStartHour() + ", " +
                     "businessDaystartMinute = "  + getStartMinute() + ", " +
                     "businessDayendHour = "      + getEndHour() + ", " +
                     "businessDayendMinute = "    + getEndMinute() + ", " +
                     "statuaryHoliday  = '"       + getStatuaryHoliday() + "', " +
                     "recurring = '"              + getRecurring() + "', " +
                     "spanWeekend = '"            + getSpanWeekend() + "', " +
                     "provinceId = "              + getProvinceId() + " " +  // SEAN ticket #494: SQL statement
                     "Where HolidayId = "         + getHolidayId();

        logger.debug("SQL statement: " + sql);

        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoteException re = new RemoteException("Holidays Entity - ejbStore() exception");

            logger.error(re.getMessage());
            logger.error("ejbStore sql: " + sql);
            logger.error(e);

            throw re;
        }
    }

  /**
     *  gets the holidayTable associated with this entity
     *
     *   @return a Hashtable
     */

   public static Hashtable getHolidayTable ()
   {
    return holidayTable;
   }

    /**
     *   gets the holidayName associated with this entity
     *
     *   @return a String
     */

   public String getHolidayName () {
    return this.holidayName;
   }

    /**
     *   gets the holidayDate associated with this entity
     *
     *   @return a date
     */
   public java.util.Date getHolidayDate (){
    return this.holidayDate;
    }

      /**
     *   gets the dayStartHour associated with this entity
     *
     *   @return an int
     */
    public int getStartHour () {
      return this.startHour;
    }

    /**
     *   gets the Day startMinute associated with this entity
     *
     *   @return an int
     */

    public int getStartMinute () {
      return this.startMinute;
    }


    /**
     *   gets the day endHour associated with this entity
     *
     *   @return an int
     */
    public int getEndHour () {
      return this.endHour;
    }

    /**
     *   gets the day endMinute associated with this entity
     *
     *   @return an int
     */
    public int getEndMinute () {
      return this.endMinute;
    }

     /**
     *   gets the day start time (ninutes from beginning of day) associated with this entity
     *
     *   @return an int
     */
    public int getStartTime () {
      return startTime;
    }

     /**
     *   gets the day end time (ninutes from beginning of day) associated with this entity
     *
     *   @return an int
     */
    public int getEndTime () {
      return endTime;
    }

    /**
     *   gets the statuaryHoliday associated with this entity
     *
     *   @return a boolean
     */
    public String getStatuaryHoliday () {
      return this.statuaryHoliday;
    }



    /**
     *   gets the recurring char associated with this entity
     *
     *   @return a String
     */
    public String getRecurring () {
      return this.recurring;
    }



    /**
     *   gets the spanWeekend value associated with this entity
     *
     *   @return a boolean
     */
    public String getSpanWeekend () {
      return this.spanWeekend;
    }



    /**
     *   gets the holidayID associated with this entity
     *
     *   @return an int
     */
   public int getHolidayId()
     {
     return this.holidayId;
   }

    /**
     *   sets the holidayTable associated with this entity
     *
     *  @param  holidayTable
     *  @return
     */
  public void setHolidayTable(Hashtable holidayTable) {
    this.holidayTable = holidayTable;
  }

    /**
     *   sets the holidayId associated with this entity
     *
     *  @param  int id
     *  @return
     */
  public void setHolidayId(int id){
     this.holidayId = id;
  }

    // SEAN ticket #494: the getter and setter.
    /**
     * Get the value of the province id.
     */
    public int getProvinceId() {
        return this.provinceId;
    }

    /**
     * Set the value of the province id.
     */
    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }
    // SEAN ticket #494 EDN

  /**
     *   sets the holidayName associated with this entity
     *
     *  @param  String holidayName
     *  @return
     */
  public void setHolidayName (String holidayName) {
    this.holidayName = holidayName;
  }


  /**
     *  sets the holidayName associated with this entity
     *
     *  @param  java.util.Date holidayDate
     *  @return
     */
   public void setHolidayDate (java.util.Date holidayDate) {
    this.holidayDate = holidayDate;
  }


  /**
     *  sets the startHour associated with this entity
     *
     *  @param  int startHour
     *  @return
     */
   public void setStartHour (int startHour) {
    this.startHour = startHour;
  }

   /**
     *  sets the startMinute associated with this entity
     *
     *  @param  int startMinute
     *  @return
     */
   public void setStartMinute (int startMinute) {
    this.startMinute = startMinute;
  }


     /**
     *  sets the endHour associated with this entity
     *
     *  @param  int endHour
     *  @return
     */
    public void setEndHour (int endtHour) {
    this.endHour = endHour;
  }

     /**
     *  sets the endMinute associated with this entity
     *
     *  @param  int endMinute
     *  @return
     */
   public void setEndMinute (int endMinute) {
    this.endMinute = endMinute;
  }


   /**
     *  sets the statuaryHoliday associated with this entity
     *
     *  @param  boolean statuaryHoliday
     *  @return
     */
   public void setStatuaryHoliday (String statuaryHoliday) {
    this.statuaryHoliday = statuaryHoliday;
  }


  /**
     *  sets the setRecurring associated with this entity
     *
     *  @param  String recurring
     *  @return
  */
  public void setRecurring (String recurring) {
    this.recurring = recurring;
  }


  /**
     *  sets the spanWeekend associated with this entity
     *
     *  @param  boolean spanWeeekend
     *  @return
  */
  public void setSpanWeekend (String spanWeekend) {
    this.spanWeekend = spanWeekend;
  }

   /**
    *Implemetatiion
   */
   public void setPropertiesFromQueryResult(int key) throws Exception
   {
      setHolidayId(jExec.getInt(key,"HOLIDAYID"));
      setHolidayName(jExec.getString(key,"HOLIDAYNAME"));
      setHolidayDate(jExec.getDate(key,"HOLIDAYDATE"));
      setStartHour(jExec.getInt(key,"BUSINESSDAYSTARTHOUR"));
      setStartMinute(jExec.getInt(key,"BUSINESSDAYSTARTMINUTE"));
      setEndHour(jExec.getInt(key,"BUSINESSDAYENDHOUR"));
      setEndMinute(jExec.getInt(key,"BUSINESSDAYENDMINUTE"));
      setStatuaryHoliday(jExec.getString(key,"STATUARYHOLIDAY"));
      setRecurring(jExec.getString(key,"RECURRING"));
      setSpanWeekend(jExec.getString(key,"SPANWEEKEND"));
      // SEAN ticket #494: load provinceid from result set.
      setProvinceId(jExec.getInt(key, "PROVINCEID"));
      // SEAN ticket #494 END
   }


    /** Creates a unique primary key for the Holidays.
      *
      *
      * @param    int holidayId unique key based on the day of year calculation
      * @return   HolidaysPk
      *
    */
    public HolidaysPK createPrimaryKey(int holidayId)throws CreateException
    {
	String sql = "Select holidayId from Holidays where holidayId = " + holidayId + "";
        int id = -1;

        /** if the unique Id already exists throws an exception */
        try
        {
            int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           if( id != -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Holidays Entity create() exception getting holidayId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new HolidaysPK(holidayId);
    }

    private boolean hourIntegrity(int h)
    {
      if (h >= 0 && h <= 23)
        return true;

      return false;
    }

    private boolean minuteIntegrity(int m)
    {
      if (m >= 0 && m <= 60)
        return true;

      return false;
    }

    /**
  * Gets day number.  The day number is calculated based on the
  * increment of 400 for each year, starting on January 1, 1970
  * and the day of the current year
  * @param     date
  * @return int dayNumber
  **/

  public int getDayNumber (java.util.Date theDate)
   {
    Calendar theDateInCal = Calendar.getInstance();
    theDateInCal.setTime(theDate);
    int dayNumber = (theDateInCal.get(theDateInCal.YEAR) - 1970)*400 + theDateInCal.get(theDateInCal.DAY_OF_YEAR);

    return dayNumber;
  }
  
}
