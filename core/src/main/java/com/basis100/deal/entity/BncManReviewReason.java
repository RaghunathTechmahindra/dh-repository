
package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BncManReviewReasonPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: BncManReviewResason
 * </p>
 * <p>
 * Description: Entity class to handles all database communication to the
 * BncManReviewResason table
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */
public class BncManReviewReason extends DealEntity {
    private int responseId;
    private int institutionProfileId;
    private int manReviewReasonId;
    private String manReviewReasonCode;
    private String manReviewReasonDesc;
    private BncManReviewReasonPK pk;

    public static final String MANREVIEWREASONCODE = "manReviewReasonCode";
    public static final String MANREVIEWREASONDESC = "manReviewReasonDesc";

    public BncManReviewReason() {

    }

    public BncManReviewReason(SessionResourceKit srk) throws RemoteException {

        super(srk);
    }

    public int getResponseId() {

        return responseId;
    }

    public void setResponseId(int responseId) {

        testChange(BncManReviewReasonPK.RESPONSEID, responseId);
        this.responseId = responseId;
    }

    public int getManReviewReasonId() {

        return manReviewReasonId;
    }

    public void setManReviewReasonId(int manReviewReasonId) {

        testChange(BncManReviewReasonPK.MANREVIEWREASONID, manReviewReasonId);
        this.manReviewReasonId = manReviewReasonId;
    }

    public String getManReviewReasonCode() {

        return manReviewReasonCode;
    }

    public void setManReviewReasonCode(String manReviewReasonCode) {

        testChange(MANREVIEWREASONCODE, manReviewReasonCode);
        this.manReviewReasonCode = manReviewReasonCode;
    }

    public String getManReviewReasonDesc() {

        return manReviewReasonDesc;
    }

    public void setManReviewReasonDesc(String manReviewReasonDesc) {

        testChange(MANREVIEWREASONDESC, manReviewReasonDesc);
        this.manReviewReasonDesc = manReviewReasonDesc;
    }

    public void setPk(BncManReviewReasonPK primaryKey) {

        this.pk = primaryKey;
    }

    /**
     * search for BncManReviewResason record using primary key
     * 
     * @param pk
     *            BncManReviewResasonPK Primary key to be searched by
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws FinderException
     *             Thrown if unable to find a matching row
     * @return BncManReviewResason object that matches the primary key given
     */
    public BncManReviewReason findByPrimaryKey(BncManReviewReasonPK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from " + getEntityTableName()
                + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = getEntityTableName()
                        + ": @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(getEntityTableName()
                    + " Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * Set all properties of this object using the values that is returned by
     * the current database query
     * 
     * @param key
     *            int The index of the database query result list
     * @throws Exception
     *             Thrown if error
     */
    public void setPropertiesFromQueryResult(int key) throws Exception {

        setResponseId(jExec.getInt(key, BncManReviewReasonPK.RESPONSEID));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        setManReviewReasonId(jExec.getInt(key,
                BncManReviewReasonPK.MANREVIEWREASONID));
        setManReviewReasonCode(jExec.getString(key, MANREVIEWREASONCODE));
        setManReviewReasonDesc(jExec.getString(key, MANREVIEWREASONDESC));
    }

    /**
     * Create a new primaryky from the BncManReviewReason sequence.
     * 
     * @param responseId
     *            int Response Id
     * @throws CreateException
     *             Thrown if unable to create
     * @return BncManReviewReasonPK Primary key with the newest ID in the
     *         sequence
     */
    public BncManReviewReasonPK createPrimaryKey(int responseId)
            throws CreateException {

        String sql = "Select " + getEntityTableName() + "SEQ.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1);
                break;
            }
            jExec.closeData(key);
            if (id == -1) {
                throw new CreateException();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(getEntityTableName()
                    + "createPrimaryKey() :" + "exception: getting "
                    + BncManReviewReasonPK.MANREVIEWREASONID + " from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }
        return new BncManReviewReasonPK(responseId, id);
    }

    /**
     * Create and inserts a BncManReviewResason into the database
     * 
     * @param responseId
     *            int Response Id
     * @param code
     *            String Manual review reason code
     * @param description
     *            String Manual review reason description
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws CreateException
     *             Thrown if unable to create row
     * @return BncManReviewResason The BncManReviewResason object has has been
     *         inserted into the database
     */
    public BncManReviewReason create(int responseId, String code,
            String description) throws RemoteException, CreateException {

        BncManReviewReasonPK rpk = createPrimaryKey(responseId);
        int key = 0;
        StringBuffer sqlb = new StringBuffer("Insert into "
                + getEntityTableName() + "( " + BncManReviewReasonPK.RESPONSEID
                + ", " + BncManReviewReasonPK.MANREVIEWREASONID + ", "
                + MANREVIEWREASONCODE + ", " + MANREVIEWREASONDESC
                + ", INSTITUTIONPROFILEID) VALUES ( ");
        sqlb.append(sqlStringFrom(rpk.getId()) + ", ");
        sqlb.append(sqlStringFrom(rpk.getManReviewReasonId()) + ",");
        sqlb.append(sqlStringFrom(code) + ",");
        sqlb.append(sqlStringFrom(description) + ",");
        sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId())
                + ")");

        String sql = new String(sqlb);

        try {
            key = jExec.executeUpdate(sql);
            findByPrimaryKey(new BncManReviewReasonPK(rpk.getId(), rpk
                    .getManReviewReasonId()));
            this.trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(getEntityTableName()
                    + " Entity - " + getEntityTableName()
                    + " - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        } finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            } catch (Exception e) {
                logger.error(e.toString());
            }
        }
        srk.setModified(true);
        return this;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // ///////////////////// OVERWRITING BASE CLASS METHODS
    // /////////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);
        return ret;
    }

    public String getEntityTableName() {

        return "BncManReviewReason";
    }

    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public boolean equals(Object o) {

        if (o instanceof BncManReviewReason) {
            BncManReviewReason oa = (BncManReviewReason) o;
            if (this.pk.equals(oa.getPk())) {
                return true;
            }
        }
        return false;
    }

    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public Object clone() throws CloneNotSupportedException {

        throw new CloneNotSupportedException();
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}
