package com.basis100.deal.entity;

import java.util.Date;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DcFolderPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author  Catherine Rutgaizer
 * @version 1.0
 */

public class DcFolder extends DealEntity {
  protected int dcFolderId;
  protected String sourceApplicationId;
  protected String dcFolderCode;
  protected Date dateCreated;
  protected String dcFolderStatusCode;
  protected String dcFolderStatusDescription;
  protected int institutionId;

  private DcFolderPK pk;

  public DcFolder(SessionResourceKit srk) throws RemoteException {
    super(srk);
  }

  public DcFolder(SessionResourceKit srk, int id) throws FinderException, RemoteException {
    super(srk);

    pk = new DcFolderPK(id);
    findByPrimaryKey(pk);
  }

  public DcFolder findByPrimaryKey(DcFolderPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM DCFOLDER " + pk.getWhereClause();
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break;                                                  // can only be one record
        }

        jExec.closeData(key);
        if (gotRecord == false)
        {
          String msg = "DcFolder: @findByPrimaryKey(), key = " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("DcFolder.findByPrimaryKey() exception");
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);
          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }

  public DcFolder findBySourceAppId(String sourceAppId) throws FinderException {
    this.sourceApplicationId = sourceAppId;
    String sql = "SELECT * FROM DCFOLDER WHERE TRIM(SOURCEAPPLICATIONID) = '"  + sourceAppId.trim() + "'";
    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
            break;                                                  // can only be one record
        }

        jExec.closeData(key);
        if (gotRecord == false)
        {
          String msg = "DcFolder.findBySourceAppId(), sourceAppId = " + sourceAppId + ", entity not found";
          logger.trace(msg);
          throw new FinderException(msg);
        }
      }
      catch (FinderException fe){
        throw new FinderException(fe.getMessage());
      }
      catch (Exception e)
      {
          String msg = "DcFolder.findBySourceAppId() exception: " + e.getMessage();
          FinderException fe = new FinderException(msg);
          logger.error(msg);
          throw fe;
      }
      this.pk = pk;
      return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
       setDcFolderId(jExec.getInt(key, "DCFOLDERID"));
       setSourceApplicationId(jExec.getString(key, "SOURCEAPPLICATIONID"));
       setDcFolderCode(jExec.getString(key, "DCFOLDERCODE"));
       setDateCreated(jExec.getDate(key, "DATECREATED"));
       setDcFolderStatusCode(jExec.getString(key, "DCFOLDERSTATUSCODE"));
       setDcFolderStatusDescription(jExec.getString(key, "DCFOLDERSTATUSDESCRIPTION"));
  }

  public DcFolder create(String sourceAppId, String folderCode, String folderStatusCode, String folderStatusDesc)
      throws RemoteException, CreateException
  {
    this.pk = this.createPrimaryKey();

     String comma = ", ";
     Date now = new Date();

     StringBuffer sqlbuf = new StringBuffer(
     "INSERT INTO DCFOLDER " +
     "(" +
          " DCFOLDERID, " +
          " SOURCEAPPLICATIONID," +
          " DCFOLDERCODE," +
          " DATECREATED," +
          " DCFOLDERSTATUSCODE," +
          " DCFOLDERSTATUSDESCRIPTION," +
          " INSTITUTIONPROFILEID" +
     ")");
     sqlbuf.append(" VALUES (");
     sqlbuf.append(sqlStringFrom(pk.getId()) + ",");                                          // DCFOLDERID
     sqlbuf.append(sqlStringFrom(sourceAppId)).append(comma);                                 // SOURCEAPPLICATIONID
     sqlbuf.append(sqlStringFrom(folderCode)).append(comma);                                  // DCFOLDERCODE
     sqlbuf.append(sqlStringFrom(now)).append(comma);                                         // DATECREATED
     sqlbuf.append(sqlStringFrom(folderStatusCode)).append(comma);                            // DCFOLDERSTATUSCODE
     sqlbuf.append(sqlStringFrom(folderStatusDesc)+ ", ");                                          // DCFOLDERSTATUSDESCRIPTION
     sqlbuf.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId())); 
     sqlbuf.append(")");

     String sql = new String(sqlbuf);

     try
     {
       jExec.executeUpdate(sql);
     }
     catch (Exception e)
     {
       CreateException ce = new CreateException("DcFolder.create() exception");
       logger.error(ce.getMessage());
       logger.error(e);

       throw ce;
     }

     this.pk = pk;
     this.dcFolderId  = pk.getId();;
     this.sourceApplicationId = sourceAppId;
     this.dateCreated = now;
     this.dcFolderCode = folderCode;
     this.dcFolderStatusCode = folderStatusCode;
     this.dcFolderStatusDescription = folderStatusDesc;

     srk.setModified(true);
     return this;
    }

  public DcFolderPK createPrimaryKey() throws CreateException
  {
    String sql = "SELECT DCFOLDERSEQ.NEXTVAL FROM DUAL";
    int id = -1;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key);  )
        {
            id = jExec.getInt(key,1);  // can only be one record
        }
        jExec.closeData(key);
        if( id == -1 ) throw new Exception();
     }
     catch (Exception e)
     {
         CreateException ce = new CreateException("DcFolder.createPrimaryKey() exception getting ID from sequence");
         logger.error(ce.getMessage());
         logger.error(e);
         throw ce;
     }
     return new DcFolderPK(id);
 }

  public String getEntityTableName(){
    return "DcFolder";
  }

  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }

  public int getClassId(){
    return ClassId.DEFAULT;
  }

  public String getStringValue(String fieldName)
  {
      return doGetStringValue(this, this.getClass(), fieldName);
  }

  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }

  protected int performUpdate()  throws Exception
  {
  Class clazz = this.getClass();
  int ret = doPerformUpdate(this, clazz);

  return ret;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof DcFolder )
      {
        DcFolder oa = ( DcFolder ) o;
        if  (  this.dcFolderId == oa.getDcFolderId () )
            return true;
      }
    return false;
  }


// ====================== properties getters ======================

  public int getDcFolderId () {
    return dcFolderId;
  }

  public String getSourceApplicationId () {
    return sourceApplicationId;
  }

  public String getDcFolderCode () {
    return this.dcFolderCode;
  }

  public String getDcFolderStatusCode () {
    return this.dcFolderStatusCode;
  }

  public String getDcFolderStatusDescription () {
    return this.dcFolderStatusDescription;
  }

  public Date getDateCreated () {
    return dateCreated;
  }

// ====================== properties setters ======================

  public void setDcFolderId (int dcFolderId) {
    this.testChange("dcFolderId", dcFolderId);
    this.dcFolderId = dcFolderId;
  }

  public void setSourceApplicationId (String sourceApplicationId) {
    this.testChange("sourceApplicationId", sourceApplicationId);
    this.sourceApplicationId = sourceApplicationId;
  }

  public void setDcFolderCode (String dcFolderCode) {
    this.testChange("dcFolderCode", dcFolderCode);
    this.dcFolderCode = dcFolderCode;
  }

  public void setDateCreated (Date dateCreated) {
    this.testChange("dateCreated", dateCreated);
    this.dateCreated = dateCreated;
  }

  public void setDcFolderStatusCode (String dcFolderStatusCode) {
    this.testChange("dcFolderStatusCode", dcFolderStatusCode);
    this.dcFolderStatusCode = dcFolderStatusCode;
  }

  public void setDcFolderStatusDescription (String dcFolderStatusDescription) {
    this.testChange("dcFolderStatusDescription", dcFolderStatusDescription);
    this.dcFolderStatusDescription = dcFolderStatusDescription;
  }

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }
    
    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
        this.testChange("institutionProfileId", institutionId);
        this.institutionId = institutionId;
    }
}
