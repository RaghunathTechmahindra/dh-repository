package com.basis100.deal.entity;

import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.DocumentTrackingPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;

import com.basis100.deal.conditions.ConditionRegExParser;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: DocumentTracking
 * </p>
 * 
 * <p>
 * Description: DocumentTracking Entity
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * 
 * <p>
 * Company: Filogic Inc.
 * </p>
 * 
 * @author BL
 * @version 1.0
 * @author MCM Imp Team 
 * change log:
 * @version 1.1 11-July-2008 XS_16.17 Added new method for getting the component related condition 
 *                                    and modified the getForCommitment() method.
 * @author MCM Imp Team 
 * change log:
 * @version 1.2 06-Oct-2008 Bug Fix :FXP22542 Added new method loadSectionCode().                                     
 */
public class DocumentTracking extends DealEntity implements Xc {
    protected int dealId;

    protected int copyId;

    protected int applicationId;

    protected int documentTrackingId;

    protected int documentTrackingSourceId;

    protected Date documentRequestDate;

    protected int documentStatusId;

    protected Date dStatusChangeDate;

    protected Date documentDueDate;

    protected int documentResponsibilityId;

    protected int documentResponsibilityRoleId;

    protected int conditionId;

    protected int tagContextId;

    protected int tagContextClassId;
    
    protected String useDefaultChecked;
    
    protected String displayDefault;

    //FXP27162
    private List verb;

    private ConditionSection conditionSection;
    //End of FXP27162
    protected int 	instProfileId;

    public DocumentTrackingPK pk = null;

 	public static interface Ec { // #DG638 those are just local entity constants 
 		public static final String DOCUMENT_TRACKING = "DocumentTracking";
 	 	public static interface CL { // columns constants 
	    public static final String DEAL_ID = "dealId";
	    public static final String COPY_ID = "copyId";
	  	public static final String CONDITION_ID = "conditionId";
	  	public static final String D_STATUS_CHANGE_DATE = "dStatusChangeDate";
	  	public static final String DOCUMENT_TRACKING_SOURCE_ID = "documentTrackingSourceId";
	  	public static final String DOCUMENT_REQUEST_DATE = "documentRequestDate";
	  	public static final String DOCUMENT_DUE_DATE = "documentDueDate";
	  	public static final String APPLICATION_ID = "applicationId";
	  	public static final String TAG_CONTEXT_ID = "tagContextId";
	  	public static final String TAG_CONTEXT_CLASS_ID = "tagContextClassId";
	  	public static final String DOCUMENT_RESPONSIBILITY_ID = "documentResponsibilityId";
	  	public static final String DOCUMENT_RESPONSIBILITY_ROLE_ID = "documentResponsibilityRoleId";
	  	public static final String DOCUMENT_STATUS_ID = "documentStatusId";
	  	public static final String DOCUMENT_TRACKING_ID = "documentTrackingId";
 	 	
	  	// verbiage columns
	    public static final String DOCUMENT_LABEL = "documentLabel";
			public static final String LANGUAGE_PREFERENCE_ID = "languagePreferenceId";
			public static final String DOCUMENT_TEXT = "documentText";
 	 	}
 	}   

    /**
         * DocumentTracking
         * 
   * @param srk SessionResourceKit
         * @throws RemoteException
         * @throws FinderException
         */
    public DocumentTracking(SessionResourceKit srk) throws RemoteException,
	    FinderException {
	super(srk);
	verb = new ArrayList();		//#DG638
    }

    /**
         * DocumentTracking
         * 
         * @param srk
         *                SessionResourceKit
         * @param id
         *                int
         * @param copyId
         *                int
         * @throws RemoteException
         * @throws FinderException
         */
    public DocumentTracking(SessionResourceKit srk, int id, int copyId)
	    throws RemoteException, FinderException {
	this(srk);	//#DG638

	pk = new DocumentTrackingPK(id, copyId);

	findByPrimaryKey(pk);
    }

    /**
         * createPrimaryKey
         * 
         * @param copyId
         *                int
         * @throws CreateException
         * @return DocumentTrackingPK
         */
    private DocumentTrackingPK createPrimaryKey(int copyId)
	    throws CreateException {
	String sql = "Select DocumentTrackingseq.nextval from dual";
	int id = -1;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		id = jExec.getInt(key, 1); // can only be one record
	    }

	    jExec.closeData(key);

	    if (id == -1)
		throw new Exception();
	} catch (Exception e) {
		e = new CreateException("DocumentTracking Entity create() exception getting DocumentTrackingId from sequence:"+e);		//#DG638

		logger.error(e);
		throw (CreateException)e;
	}

	return new DocumentTrackingPK(id, copyId);
    }

    /**
         * getVerb
         * 
         * @return List
         */
    public List getVerb() {
	return this.verb;
    }

    /**
         * appendVerbiage
         * 
         * @param pTrack
         *                DocumentTracking
         * @throws RemoteException
         * @throws CreateException
         */
    public void appendVerbiage(DocumentTracking pTrack) throws RemoteException,
	    CreateException {
	List lVerbiage = pTrack.getVerb();
	Iterator it = lVerbiage.iterator();
	while (it.hasNext()) {
	    DocTrackingVerbiage lOneVerbiage = (DocTrackingVerbiage) it.next();
	    DocTrackingVerbiage outVerbiage = null;

	    outVerbiage = new DocTrackingVerbiage(this.srk, this
		    .getDocumentTrackingId(), this.getCopyId(), lOneVerbiage
		    .getLanguagePreferenceId(),
		    lOneVerbiage.getDocumentLabel(), lOneVerbiage
			    .getDocumentText());
	    outVerbiage.create(new DocumentTrackingPK(this
		    .getDocumentTrackingId(), this.getCopyId()));

       this.verb.add(outVerbiage);
     }
   }
  
  public void appendVerbiage(List verb, DocumentTracking dt) throws RemoteException,CreateException {
	  
	  if(verb != null && verb.size() > 0) {
		  Iterator it = verb.iterator();
		  while(it.hasNext()) {
			  DocTrackingVerbiage dtv = (DocTrackingVerbiage)it.next();
			  dtv.create((DocumentTrackingPK)dt.getPk());
		  }
	  }
	  
  }

    /** #DG638 rewritten
     * findByPrimaryKey
     * 
     * @param pka DocumentTrackingPK
     * @throws RemoteException
     * @throws FinderException
     * @return DocumentTracking
     */
    public DocumentTracking findByPrimaryKey(DocumentTrackingPK pka) throws FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pka)) return this; // 4.4 Entity Cache
		String sql = "Select * from DocumentTracking " + pka.getWhereClause();
		boolean gotRecord = false;
		try {
			int key = jExec.execute(sql);
   			gotRecord = jExec.next(key);
   			if(gotRecord) {	// only 0 or 1 record is expected
				this.copyId = pka.getCopyId();

				setPropertiesFromQueryResult(key);
			}

			jExec.closeData(key);
   			if (!gotRecord ) {
				String msg = "DocumentTracking Entity: @findByPrimaryKey(), key= " + pka + ", entity not found";
				throw new FinderException(msg);
			}

			loadVerbiage();
        } catch (Exception e) {
			e = new FinderException("DocumentTracking Entity - findByPrimaryKey() exception:"+e);		//#DG638
   			if (gotRecord || !getSilentMode() ) {
   				logger.error(e.getMessage());
				logger.error("finder sql: " + sql);
   			}
			throw (FinderException)e;
        }

		this.pk = pka;
		ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
		return this;
    }

    /**
     * findByLabel
     * 
     * @param pk
     *                DealPK
     * @param label
     *                String
     * @throws Exception
     * @return Collection
     */
    public Collection findByLabel(DealPK pk, String label) throws Exception {

      //select * from documenttracking where documenttrackingid in( select documenttrackingid from documenttrackingverbiage where documentlabel = 'Non Rental Unit')

      String sql = "Select * from documenttracking where documentTrackingid " 
      	+" in ( select documentTrackingid from "
      	+"documentTrackingVerbiage where documentlabel ="
      	+sqlStringFrom(label)+")";

      return findBySqlColl(sql.toString());
    }

    /**
         * findByDeal
         * 
         * @param pk
         *                DealPK
         * @throws Exception
         * @return Collection
         * change log :
         * @author MCM Impl Team 
         * @version 1.1 12-July-2008 XS_16.17 Modified order by clause,added tagcontextid and conditionid.
         */
    public Collection findByDeal(DealPK pk) throws Exception {

      String sql = "Select * from DocumentTracking " +
                    pk.getWhereClause() + " Order by DocumentResponsibilityRoleId,tagcontextid,conditionid ";

      return findBySqlColl(sql);
    }

    /**
         * findByDealAndSource
         * 
         * @param pk
         *                DealPK
         * @param source
         *                int
         * @throws Exception
         * @return Collection
         */
    public Collection findByDealAndSource(DealPK pk, int source)
	    throws Exception {

      String sql = "Select * from DocumentTracking " +
                    pk.getWhereClause() + " and documentTrackingSourceId = " +
                    source + " order by DocumentResponsibilityRoleId ";
      return findBySqlColl(sql);
    }

    /**
         * findForCommitmentLetter
         * 
         * @param pk
         *                DealPK
         * @throws Exception
         * @return Collection
         */
    public Collection findForCommitmentLetter(DealPK pk) throws Exception {
	return getForCommitment(pk, false);
    }
/**
 * 
 * This methos returns the collection of condition for the commitment letter.
 * @author MCM Impl Team 
 * @version 1.1 11-July-2008 XS_16.17 modified select query to select only those condition which are not having conditonsection other than 'MCM'
 * @version 1.2 26-Feb-2012 modified to check systemType for section code.  FXP33286 
 */
    protected Collection getForCommitment(DealPK pk, boolean includeDocTrackOnly)
    throws Exception {

        int dealId = pk.getId();
        int copyId = pk.getCopyId();
        
        StringBuffer sqlbuf = new StringBuffer(
        "select * from DocumentTracking where dealId = " + dealId);
        sqlbuf.append(" AND copyId = " + copyId).append(" AND ");
        sqlbuf
        .append("conditionId in( Select condition.conditionId from condition,conditionsection where ");
        sqlbuf.append("conditionTypeId in( ");
        sqlbuf.append(Mc.CONDITION_TYPE_COMMITMENT_ONLY).append(", ");
        if (includeDocTrackOnly) {
            sqlbuf.append(Mc.CONDITION_TYPE_DOC_TRACKING_ONLY).append(", ");
        }
        /**************************MCM Impl Team Changes XS_16.17 starts*************************************/
        /**
         *Selection only those condition which are not having conditiondescription as 'MCM'
         */
        sqlbuf.append(Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING).append(
                ") AND condition.institutionprofileid=conditionsection.institutionprofileid  " +
                "  AND condition.conditionId=conditionsection.conditionId " +
        "  AND sectioncode NOT IN('MCM')")
        .append(" AND systemTypeid in (select systemTypeId from deal where dealid = " + dealId)
        .append(" AND copyId = " + copyId)
        .append("))")
        .append(" AND " );
        /**************************MCM Impl Team Changes XS_16.17 ends*************************************/
        sqlbuf.append("documentTrackingSourceId in(");
        sqlbuf.append(Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM)
        .append(", ");
        sqlbuf.append(Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED)
        .append(", ");
        sqlbuf.append(Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED).append(
        ") AND ");
        sqlbuf.append("documentResponsibilityRoleId in(");
        sqlbuf.append(Mc.CRR_PARTY_TYPE_APPRAISER).append(", ");
        sqlbuf.append(Mc.CRR_PARTY_TYPE_SOLICITOR).append(", ");
        sqlbuf.append(Mc.CRR_USER_TYPE_APPLICANT).append(", ");
        //According to Express Commitment Letter Condition Ordering for CMLI, add ConditionResponsibilityRoleId=5 (Lender)  
        sqlbuf.append(Mc.CRR_PARTY_TYPE_LENDER).append(", ");  
        sqlbuf.append(Mc.CRR_SOURCE_OF_BUSINESS).append(") AND ");
        sqlbuf.append("documentStatusId in(");
        sqlbuf.append(Mc.DOC_STATUS_REQUESTED).append(", ");
        sqlbuf.append(Mc.DOC_STATUS_RECEIVED).append(", ");
        sqlbuf.append(Mc.DOC_STATUS_INSUFFICIENT).append(", ");
        sqlbuf.append(Mc.DOC_STATUS_CAUTION).append(", ");

        // ============================================================================
        // zivko removed the following line based on request from product team.
        // product team has to change spec for Commitment letter verbiage . doc
        // file
        // so the code and spec can be in sync. The request is that vaiwed
        // conditions
        // should be excluded from the list.
        // change has been related to issue #40 on basisXPress tracker list.
        // ==========================================================================
        // sqlbuf.append(Mc.DOC_STATUS_WAIVED).append(", ");
        sqlbuf.append(Mc.DOC_STATUS_WAIVE_REQUESTED).append(") ");
        sqlbuf
        .append("order by documentResponsibilityRoleId, documenttrackingid");

        return findBySqlColl(sqlbuf.toString());
    }
 /**************************MCM Impl Team Changes XS_16.17 starts*************************************/  
    /**
     * Description :This method is retuns the collection of conditions which are having condition section as 'MCM'
     * @author MCM Imp Team 
     * @version 1.1 11-July-2008 XS_16.17 Intial Version
     * @version 1.2 26-Feb-2012 modified to check systemType for section code.  FXP33286 
     */
public Collection getComponentCondition(DealPK pk,boolean includeDocTrackOnly)throws Exception{
  
    int dealId = pk.getId();
    int copyId = pk.getCopyId();

    StringBuffer sqlbuf1 = new StringBuffer(
    "select * from DocumentTracking where dealId = " + dealId);
    sqlbuf1.append(pk.getId()).append(" AND copyId = " + copyId);
    sqlbuf1
    .append(" AND conditionId in( Select condition.conditionId from condition,conditionsection where ");
    sqlbuf1.append("conditionTypeId in( ");
    sqlbuf1.append(Mc.CONDITION_TYPE_COMMITMENT_ONLY).append(", ");
    if (includeDocTrackOnly) {
        sqlbuf1.append(Mc.CONDITION_TYPE_DOC_TRACKING_ONLY).append(", ");
    }
    /***Selectig only those condition which are having conditiondescription as 'MCM'*/
    sqlbuf1.append(Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING)
    .append(") AND condition.institutionprofileid=conditionsection.institutionprofileid  ")
    .append(" AND condition.conditionId=conditionsection.conditionId ")
    .append(" AND sectioncode IN('MCM')")
    .append(" AND systemTypeid in (select systemTypeId from deal where dealId = " + dealId )
    .append(" AND copyid = " + copyId)
    .append("))");
    sqlbuf1.append(" AND documentTrackingSourceId in(");
    sqlbuf1.append(Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM).append(", ");
    sqlbuf1.append(Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED).append(", ");
    sqlbuf1.append(Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED).append( ") AND ");
    sqlbuf1.append("documentResponsibilityRoleId in(");
    sqlbuf1.append(Mc.CRR_PARTY_TYPE_APPRAISER).append(", ");
    sqlbuf1.append(Mc.CRR_PARTY_TYPE_SOLICITOR).append(", ");
    sqlbuf1.append(Mc.CRR_USER_TYPE_APPLICANT).append(", ");
    sqlbuf1.append(Mc.CRR_SOURCE_OF_BUSINESS).append(") AND ");
    sqlbuf1.append("documentStatusId in(");
    sqlbuf1.append(Mc.DOC_STATUS_REQUESTED).append(", ");
    sqlbuf1.append(Mc.DOC_STATUS_RECEIVED).append(", ");
    sqlbuf1.append(Mc.DOC_STATUS_INSUFFICIENT).append(", ");
    sqlbuf1.append(Mc.DOC_STATUS_CAUTION).append(", ");
    sqlbuf1.append(Mc.DOC_STATUS_WAIVE_REQUESTED).append(") ");
    sqlbuf1.append("order by conditionId, documentResponsibilityRoleId, documenttrackingid, tagcontextId");

   return findBySqlColl(sqlbuf1.toString());
    
}
/**************************MCM Impl Team Changes XS_16.17 ends*************************************/    
    public Collection findForClosingServices(DealPK pk) throws Exception {
	return getForCommitment(pk, true);
    }

    /**
         * findForCommitmentLetter - this is an overloaded method to satisfy
         * Cervus requirements for conditions on a commitment letter
         * 
         * @param pk
         *                DealPK
         * @param includeOpen
         *                boolean
         * @throws Exception
         * @return Collection
         */
    public Collection findForCommitmentLetter(DealPK pk, boolean includeOpen)
	    throws Exception {
	// StringBuffer sqlbuf = new StringBuffer("select * from
        // DocumentTracking where dealId = 19311");

	StringBuffer sqlbuf = new StringBuffer(
		"select * from DocumentTracking where dealId = ");
	sqlbuf.append(pk.getId()).append(" AND copyId = ").append(
		pk.getCopyId()).append(" AND ");
	sqlbuf
		.append("conditionId in( Select conditionId from condition where ");
	sqlbuf.append("conditionTypeId in( ");
	sqlbuf.append(Mc.CONDITION_TYPE_COMMITMENT_ONLY).append(", ");
	sqlbuf.append(Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING).append(
		")) AND ");
	sqlbuf.append("documentTrackingSourceId in(");
	sqlbuf.append(Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM)
		.append(", ");
	sqlbuf.append(Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED)
		.append(", ");
	sqlbuf.append(Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED).append(
		") AND ");
	sqlbuf.append("documentResponsibilityRoleId in(");
	sqlbuf.append(Mc.CRR_PARTY_TYPE_APPRAISER).append(", ");
	sqlbuf.append(Mc.CRR_PARTY_TYPE_SOLICITOR).append(", ");
	sqlbuf.append(Mc.CRR_USER_TYPE_APPLICANT).append(", ");
	sqlbuf.append(Mc.CRR_SOURCE_OF_BUSINESS).append(") AND ");
	sqlbuf.append("documentStatusId in(");
	if (includeOpen) {
	    // -----------------------------------------------
	    // Catherine: the following is special for Cervus
	    sqlbuf.append(Mc.DOC_STATUS_OPEN).append(", ");
	    // -----------------------------------------------
	}
	sqlbuf.append(Mc.DOC_STATUS_REQUESTED).append(", ");
	sqlbuf.append(Mc.DOC_STATUS_RECEIVED).append(", ");
	sqlbuf.append(Mc.DOC_STATUS_INSUFFICIENT).append(", ");
	sqlbuf.append(Mc.DOC_STATUS_CAUTION).append(", ");
	// ============================================================================
	// zivko removed the following line based on request from product team.
	// product team has to change spec for Commitment letter verbiage . doc
        // file
	// so the code and spec can be in sync. The request is that vaiwed
        // conditions
	// should be excluded from the list.
	// change has been related to issue #40 on basisXPress tracker list.
	// ==========================================================================
	// sqlbuf.append(Mc.DOC_STATUS_WAIVED).append(", ");
	sqlbuf.append(Mc.DOC_STATUS_WAIVE_REQUESTED).append(") ");
	sqlbuf
		.append("order by documentResponsibilityRoleId, documenttrackingid");

	return findBySqlColl(sqlbuf.toString());
    }

    /**
         * findByConditionId
         * 
         * @param conditionId
         *                int
         * @throws Exception
         * @return Collection
         */
    public Collection findByConditionId(int conditionId) throws Exception {

		String sql = "Select * from DocumentTracking where conditionId ="
			+ conditionId;

		return findBySqlColl(sql);
    }

    // --> In order to fix the bug that DocPrep creating the un-necessary
        // PACAgreementCondition when
    // --> Generating the Instruct Solicitor document.
    // --> By Billy 17Aug2004
    /**
         * Find all DoucmentTracking records for the Deal and corresponding
         * ConditionId
         * 
         * @param pk
         *                DealPK
         * @param conditionId
         *                int
         * @throws Exception
         * @return Collection (all matched DocumentTracking Entities)
         */
    public Collection findByDealAndConditionId(DealPK pk, int conditionId)
	    throws Exception {
		String sql = "Select * from DocumentTracking " + pk.getWhereClause()
			+ " and conditionId =" + conditionId
			+ " order by DocumentTrackingId ";

		return findBySqlColl(sql);
    }

	//#DG638 rewritten - does not seem to b used...
	public Vector findByMyCopies() throws RemoteException, FinderException  {

		String sql = "Select * from DocumentTracking where documentTrackingId = " +
			getDocumentTrackingId() + " AND copyId <> " + getCopyId();
		try {
			return (Vector) findBySqlColl(sql);
		} catch (Exception e) {
			throw (FinderException)e;
		}
	}  

 	/** #DG638 unified method to retrieve a list of conditions
	 *
	 * @param sql sql input command
	 * @return collection of documents
	 * @throws Exception
	 */
	public Collection findBySqlColl(String sql) throws Exception {
		
		Collection documents = new Vector();
		try		{

		  int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
          DocumentTracking dt = new DocumentTracking(srk);
          dt.setPropertiesFromQueryResult(key);
          dt.pk = new DocumentTrackingPK(dt.getDocumentTrackingId(),dt.getCopyId());

          documents.add(dt);
	      dt.loadVerbiage();
        }
        jExec.closeData(key);

		  /* loaded above
		  if(!documents.isEmpty())        {
          Iterator it = documents.iterator();
		    while(it.hasNext())          {


            DocumentTracking curr = (DocumentTracking)it.next();
            curr.loadVerbiage();
          }
		  }*/
      }
      catch (Exception e)
      {
			//since this method is called from other locations here, let's log caller method
		  e = new Exception("Exception DocumentTracking@findBySqlColl:", e);		//#DG638
		  if (!getSilentMode()) {
			  logger.error(e.getMessage());
			  logger.error("finder sql: " + sql);
		  }
		  throw e;
		}
      return documents;
   }

    private void setPropertiesFromQueryResult(int key) throws Exception {
	setDealId(jExec.getInt(key, "DEALID"));
	setCopyId(jExec.getInt(key, "COPYID"));
	setDocumentTrackingId(jExec.getInt(key, "DOCUMENTTRACKINGID"));
	setDocumentStatusId(jExec.getInt(key, "DOCUMENTSTATUSID"));
	setDocumentRequestDate(jExec.getDate(key, "DOCUMENTREQUESTDATE"));
	setConditionId(jExec.getInt(key, "CONDITIONID"));
	setDocumentDueDate(jExec.getDate(key, "DOCUMENTDUEDATE"));
	setDocumentResponsibilityId(jExec.getInt(key, "DOCUMENTRESPONSIBILITYID"));
	setDocumentResponsibilityRoleId(jExec.getInt(key, "DOCUMENTRESPONSIBILITYROLEID"));
	setApplicationId(jExec.getInt(key, "APPLICATIONID"));
	setDStatusChangeDate(jExec.getDate(key, "DSTATUSCHANGEDATE"));
	setDocumentTrackingSourceId(jExec.getInt(key, "DOCUMENTTRACKINGSOURCEID"));
	setTagContextId(jExec.getInt(key, "TAGCONTEXTID"));
	setTagContextClassId(jExec.getInt(key, "TAGCONTEXTCLASSID"));
	setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
	setUseDefaultChecked(jExec.getString(key, "USEDEFAULTCHECKED"));
	setDisplayDefault(jExec.getString(key, "DISPLAYDEFAULT"));
    }

    private void loadVerbiage() throws Exception {

    	//=================================================================
		//CLOB Performance Enhancement June 2010
			String sql = "Select LANGUAGEPREFERENCEID, DOCUMENTLABEL, " +
			"DOCUMENTTEXT, " +
			"DOCUMENTTEXTLITE " +
			"from DocumentTrackingVerbiage " + 
		//=================================================================
            "where DocumentTrackingId = " + pk.getId() +
            " and copyId = " + pk.getCopyId() + " order by LanguagePreferenceId";

      //#DG638 verb = new ArrayList();
      verb.clear();
      try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          	//=================================================================
    		//CLOB Performance Enhancement June 2010
    		String tmpDocText = jExec.getString(key, "DOCUMENTTEXTLITE");
    			if( null == tmpDocText) {
    				tmpDocText = TypeConverter.stringFromClob(jExec.getClob(key, 3));
    			}
    			final DocTrackingVerbiage dtv = new DocTrackingVerbiage(srk,
    				documentTrackingId, copyId, jExec.getInt(key, 1), jExec.getString(
    				key, 2), tmpDocText);
    		//=================================================================
			//#DG638 this.setVerbiagePropertiesFromQueryResult(key);
            verb.add(dtv);            
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
          e = new FinderException("DocmentTracking Entity - findByPrimaryKey() exception:"+e);		//#DG638
          if(!getSilentMode())	{
	          logger.error(e.getMessage());
          logger.error("finder sql: " + sql);
		  }
          throw e;
	  }
	}

	/* #DG638
    private void setVerbiagePropertiesFromQueryResult(int key) throws Exception {
	verb.add(new DocTrackingVerbiage(srk, this.documentTrackingId,
		this.copyId, jExec.getInt(key, 1), jExec.getString(key, 2),
		jExec.getString(key, 3)));

		}*/

    private void loadSection() throws Exception {
	StringBuffer sqlbuf = new StringBuffer(
		"Select  SECTIONDESCRIPTION, SECTIONCODE, SEQUENCEID ");
	sqlbuf.append("from ConditionSection where conditionId = ");
	sqlbuf.append(getConditionId()).append(" and SystemTypeId = ");
	sqlbuf.append("(Select systemTypeId from deal where dealId = ");
	sqlbuf.append(getDealId()).append(" and copyId = ");
	sqlbuf.append(getCopyId()).append(")");

	String sql = sqlbuf.toString();

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		conditionSection = new ConditionSection(
						jExec.getString(key, 1), jExec.getString(key, 2), jExec
								.getInt(key, 3)); 
	    }

	    jExec.closeData(key);
	} catch (Exception e) {
	    Exception fe = new Exception(
		    "DocmentTracking Entity - loadConditionSection() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}

    }

    /**
         * gets the value of instance fields as String. if a field does not
         * exist (or the type is not serviced) null is returned. if the field
         * exists (and the type is serviced) but the field is null an empty
         * String is returned.
         * 
         * @returns value of bound field as a String;
         * @param fieldName
         *                as String Other entity types are not yet serviced ie.
         *                You can't get the Province object for province.
         * @return String
         */
    public String getStringValue(String fieldName) {
	return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
         * sets the value of instance fields as String. if a field does not
         * exist or is not serviced an Exception is thrown if the field exists
         * (and the type is serviced) but the field is null an empty String is
         * returned.
         * 
         * @param fieldName
         *                as String
         * @param value
         *                value as a String Other entity types are not yet
         *                serviced ie. You can't set the Province object in
         *                Addr.
         * @throws Exception
         */
    public void setField(String fieldName, String value) throws Exception {
	doSetField(this, this.getClass(), fieldName, value);
    }

    /**
         * Overriddes ejbStore in DealEntity to ensure storage of verbiage
         * 
         * @throws RemoteException
         */
    public void ejbStore() throws RemoteException {

	    Iterator it = verb.iterator();
	    DocTrackingVerbiage cv = null;

	    while (it.hasNext()) {
		cv = (DocTrackingVerbiage) it.next();

		if (cv.isDataChanged()) {
		    try {
			cv.performUpdate();
			cv.trackEntityChange(); // Track all the change
		    } catch (Exception e) {
            e = new RemoteException(" DocumentTrackingVerbiage - ejbStore() exception - msg: \n" + e);		//#DG638
            logger.error(e.getMessage());		//#DG638
            throw (RemoteException)e;
		    }
		}
	    }


	if (this.isDataChanged()) {
	    try {
		this.performUpdate();
		this.trackEntityChange(); // Track all the change
	    } catch (Exception e) {
		RemoteException re = new RemoteException(
			" DocumentTracking - ejbStore() exception - msg: ");
		logger.error(re.getMessage() + "\n" + e.getMessage());
		logger.error(e);

		throw re;
	    }

	}
	
		//4.4 Entity Cache
		ThreadLocalEntityCache.writeToCache(this, this.getPk());
    }

    /**
         * Updates any Database field that has changed since the last
         * synchronization ie. the last findBy... call
         * 
         * @return the number of updates performed
         * @throws Exception
         */
    protected int performUpdate() throws Exception {
	Class clazz = this.getClass();

	return (doPerformUpdate(this, clazz));
    }

    /**
         * copyChildren
         * 
         * @param src
         *                MasterDeal
         * @param destParent
         *                MasterDeal
         * @param dtname
         *                String
         * @param dtkey
         *                String
         * @param copyId
         *                int
         * @throws Exception
         */
    public void copyChildren(MasterDeal src, MasterDeal destParent,
	    String dtname, String dtkey, int copyId) throws Exception {
	int dtkeyval = this.documentTrackingId;

	if (src.getDealId() != destParent.getDealId()) // copyfrom type
	    dtkeyval = Integer.parseInt(dtkey);

	this.copyVerbiage(dtkeyval, copyId);
    }

    private void copyVerbiage(int doctrackingId, int copyId) throws Exception {
	DocumentTrackingPK pk = new DocumentTrackingPK(doctrackingId, copyId);

	this.loadVerbiage();

	DocTrackingVerbiage dtv = null;

	Iterator it = verb.iterator();

	while (it.hasNext()) {
	    dtv = (DocTrackingVerbiage) it.next();

	    if (dtv != null)
		dtv.create(pk);
	}

    }

    /**
         * getEntityTableName
         * 
         * @return String
         */
    public String getEntityTableName() {
	return "DocumentTracking";
    }

    /**
         * gets the pk associated with this entity
         * 
         * @return a DocumentTrackingPK
         */
    public IEntityBeanPK getPk() {
	return (IEntityBeanPK) this.pk;
    }

    /**
         * createWithNoVerbiage
         * 
         * @param condition
         *                Condition
         * @param dealId
         *                int
         * @param copyId
         *                int
         * @param docStatusId
         *                int
         * @param dtsourceId
         *                int
         * @throws RemoteException
         * @throws CreateException
         * @return DocumentTracking
         */
    public DocumentTracking createWithNoVerbiage(Condition condition,
	    int dealId, int copyId, int docStatusId, int dtsourceId)
	    throws RemoteException, CreateException {
	try {
	    this.pk = createPrimaryKey(copyId);

	    StringBuffer ibuf = new StringBuffer(
		    "Insert into DocumentTracking( ");
	    ibuf.append("documentTrackingId, dealId, copyId, applicationId,  ");
	    ibuf.append("documentStatusId, conditionId,");
	    ibuf
		    .append("documentResponsibilityRoleId, documentTrackingSourceId, InstitutionProfileId ) Values ( ");
	    // should be 8 values

	    ibuf.append(pk.getId()).append(", ");
	    ibuf.append(dealId).append(", ");
	    ibuf.append(copyId).append(", ");
	    ibuf.append(dealId).append(", ");
	    ibuf.append(docStatusId).append(", ");
	    // =======================================================================
	    // this is bug fix instead of documentstatusid we should use
                // docStatusId
	    // ibuf.append(documentStatusId).append(", ");
	    // =======================================================================
	    ibuf.append(condition.getConditionId()).append(", ");
	    ibuf.append(condition.getConditionResponsibilityRoleId()).append(
		    ", ");
	    ibuf.append(dtsourceId).append(",")
        .append(srk.getExpressState().getDealInstitutionId()).append(" )");

	    int key = jExec.executeUpdate(ibuf.toString());

	    findByPrimaryKey(pk);
	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DocumentTracking Entity - DocumentTrackingType - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);

	    throw ce;
	}
	srk.setModified(true);
	return this;
    }

    /**
         * create a DocumentTracking record given an id (primary key) and a
         * description
         * 
         * @param condition
         *                Condition
         * @param dealId
         *                int
         * @param copyId
         *                int
         * @param docStatusId
         *                int
         * @param dtsourceId
         *                int
         * @throws RemoteException
         * @throws CreateException
         * @return DocumentTracking
         */
    public DocumentTracking create(Condition condition, int dealId, int copyId,
	    int docStatusId, int dtsourceId) throws RemoteException,
	    CreateException {

	try {
	    this.pk = createPrimaryKey(copyId);

	    StringBuffer ibuf = new StringBuffer(
		    "Insert into DocumentTracking( ");
	    ibuf.append("documentTrackingId, dealId, copyId, applicationId,  ");
	    ibuf.append("documentStatusId, conditionId,");
	    ibuf
		    .append("documentResponsibilityRoleId, documentTrackingSourceId, INSTITUTIONPROFILEID) Values ( ");
	    // should be 8 values

	    ibuf.append(pk.getId()).append(", ");
	    ibuf.append(dealId).append(", ");
	    ibuf.append(copyId).append(", ");
	    ibuf.append(dealId).append(", ");
	    ibuf.append(docStatusId).append(", ");
	    // =======================================================================
	    // this is bug fix instead of documentstatusid we should use
                // docStatusId
	    // ibuf.append(documentStatusId).append(", ");
	    // =======================================================================
	    ibuf.append(condition.getConditionId()).append(", ");
	    ibuf.append(condition.getConditionResponsibilityRoleId()).append(
		    ", ");
	    ibuf.append(dtsourceId).append(", ");
	    ibuf.append(srk.getExpressState().getDealInstitutionId()).append(" )");

	    int key = jExec.executeUpdate(ibuf.toString());

	    //#DG638 if (condition.hasVerbiage()) {
		int len = condition.verbiageSize();

		for (int i = 0; i < len; i++) {

		    DocTrackingVerbiage vbg = new DocTrackingVerbiage(srk, pk
			    .getId(), pk.getCopyId(), condition
			    .getLanguagePreferenceId(i), condition
			    .getConditionLabel(i), condition
			    .getConditionText(i));

		    vbg.create(this.pk);

		    verb.add(vbg);

		}
		//#DG638 }

             //Removed for CIBC performance improvement    
             //findByPrimaryKey(pk);
             //Added for CIBC performance improvement 
             this.setDocumentTrackingId(pk.getId());
             this.setDealId(dealId);
             this.setCopyId(copyId);
             this.setApplicationId(dealId);
             this.setDocumentStatusId(docStatusId);
             this.setConditionId(condition.getConditionId());
             this.setDocumentResponsibilityRoleId(condition.getConditionResponsibilityRoleId());
             this.setDocumentTrackingSourceId(dtsourceId); 
             
             this.setTagContextId(0);
             this.setTagContextClassId(0);
             this.setDocumentResponsibilityId(0);

             //Added for CIBC performance improvement end
	} catch (Exception e) {
	    CreateException ce = new CreateException(
		    "DocumentTracking Entity - DocumentTrackingType - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);

	    throw ce;
	}
	srk.setModified(true);
	return this;
    }

    // --Release2.1--//
    // // Product Team requests now to create a custom condition in a style
        // of Standard condition
    // // (two records in the DocumentTrackingVerbiage table for both
        // languages).
    // // Submit action on update of the documentText should propagate this
        // change
    // // to the BOTH records (English and French) nevertheless what the
        // session language is
    // // it at the moment.
    // // On the other hand for the new TD's ChangeRequest (joining
        // Condition
    // // and DocTracking screens) will require the the update of the
        // documentText
    // // based on the languageId. This method will provide this and SHOULD
        // STAY here.
    /**
         * public DocumentTracking create(Condition condition, int dealId, int
         * copyId, int docStatusId, int dtsourceId, int langId ) throws
         * RemoteException, CreateException { try { this.pk =
         * createPrimaryKey(copyId); StringBuffer ibuf = new
         * StringBuffer("Insert into DocumentTracking( ");
         * ibuf.append("documentTrackingId, dealId, copyId, applicationId, ");
         * ibuf.append("documentStatusId, conditionId,");
         * ibuf.append("documentResponsibilityRoleId, documentTrackingSourceId )
         * Values ( "); //should be 8 values ibuf.append(pk.getId()).append(",
         * "); ibuf.append(dealId).append(", "); ibuf.append(copyId).append(",
         * "); ibuf.append(dealId).append(", ");
         * ibuf.append(docStatusId).append(", ");
         * //======================================================================= //
         * this is bug fix instead of documentstatusid we should use docStatusId //
         * ibuf.append(documentStatusId).append(", ");
         * //=======================================================================
         * ibuf.append(condition.getConditionId()).append(", ");
         * ibuf.append(condition.getConditionResponsibilityRoleId()).append(",
         * "); ibuf.append(dtsourceId).append(" )"); int key =
         * jExec.executeUpdate(ibuf.toString()); if(condition.hasVerbiage()) {
         * int len = condition.verbiageSize(); for(int i = 0; i < len ; i++){
         * DocTrackingVerbiage vbg = new
         * DocTrackingVerbiage(srk,pk.getId(),pk.getCopyId(),
         * ////condition.getLanguagePreferenceId(i), langId,
         * condition.getConditionLabel(i), condition.getConditionText(i));
         * vbg.create(this.pk); if(verb == null) verb = new ArrayList();
         * verb.add(vbg); } } findByPrimaryKey(pk); } catch (Exception e) {
         * CreateException ce = new CreateException("DocumentTracking Entity -
         * DocumentTrackingType - create() exception");
         * logger.error(ce.getMessage()); logger.error(e); throw ce; }
         * srk.setModified(true); return this; }
         * 
         * @param value
         *                int
         */
    public void setDocumentTrackingId(int value) {
	this.testChange("documentTrackingId", value);
	this.documentTrackingId = value;
    }

    public int getDocumentTrackingId() {
	return this.documentTrackingId;
    }

    public void setDocumentStatusId(int value) {
	this.testChange("documentStatusId", value);
	this.documentStatusId = value;
    }

    public void setDocumentResponsibilityRoleId(int value) {
	this.testChange("documentResponsibilityRoleId", value);
	this.documentResponsibilityRoleId = value;
    }

    public int getDocumentResponsibilityRoleId() {
	return this.documentResponsibilityRoleId;
    }

    public void setDocumentResponsibilityId(int value) {
	this.testChange("documentResponsibilityId", value);
	this.documentResponsibilityId = value;
    }

    public int getDocumentResponsibilityId() {
	return this.documentResponsibilityId;
    }

    public void setTagContextClassId(int value) {
	this.testChange("tagContextClassId", value);
	this.tagContextClassId = value;
    }

    public int getTagContextClassId() {
	return this.tagContextClassId;
    }

    public void setTagContextId(int value) {
	this.testChange("tagContextId", value);
	this.tagContextId = value;
    }

    public int getTagContextId() {
	return this.tagContextId;
    }

    public void setApplicationId(int value) {
	this.testChange("applicationId", value);
	this.applicationId = value;
    }

    public int getApplicationId() {
	return this.applicationId;
    }

    public void setDocumentDueDate(Date value) {
	this.testChange("documentDueDate", value);
	this.documentDueDate = value;
    }

    public Date getDocumentDueDate() {
	return this.documentDueDate;
    }

    public int getDocumentStatusId() {
	return this.documentStatusId;
    }

    public void setDocumentRequestDate(Date value) {
	this.testChange("documentRequestDate", value);
	this.documentRequestDate = value;
    }

    public Date getDocumentRequestDate() {
	return this.documentRequestDate;
    }

    public void setDocumentTrackingSourceId(int value) {
	this.testChange("documentTrackingSourceId", value);
	this.documentTrackingSourceId = value;
    }

    public int getDocumentTrackingSourceId() {
	return this.documentTrackingSourceId;
    }

    public void setDStatusChangeDate(Date value) {
	this.testChange("dStatusChangeDate", value);
	this.dStatusChangeDate = value;
    }

    public Date getDStatusChangeDate() {
	return this.dStatusChangeDate;
    }

    public void setConditionId(int value) {
	this.testChange("conditionId", value);
	this.conditionId = value;
    }

    public int getConditionId() {
	return this.conditionId;
    }

    public void setDealId(int value) {
	this.testChange("dealId", value);
	this.dealId = value;
    }

    public int getDealId() {
	return this.dealId;
    }

    public void setCopyId(int value) {
	this.copyId = value;
    }

    public int getCopyId() {
	return this.copyId;
    }

    public String getDocumentText(int ndx) {
	try {
	    DocTrackingVerbiage cv = (DocTrackingVerbiage) verb.get(ndx);
	    return cv.getDocumentText();
	} catch (ArrayIndexOutOfBoundsException a) {
	    return null;
	} catch (NullPointerException np) {
	    return null;
	}
    }

    public void setDocumentText(int ndx, String text)
	    throws ArrayIndexOutOfBoundsException {

	DocTrackingVerbiage cv = null;

	cv = (DocTrackingVerbiage) verb.get(ndx);

	cv.setDocumentText(text);
    }

    public void setDocumentTextForLanguage(int lang, String text)
	    throws ArrayIndexOutOfBoundsException {

	DocTrackingVerbiage cv = null;

	for (int i = 0; i < verb.size(); i++) {
	    cv = (DocTrackingVerbiage) verb.get(i);
	    if (cv.getLanguagePreferenceId() == lang) {
		cv.setDocumentText(text);
		return;
	    }
	}
	return;
    }

    public String getDocumentLabel(int ndx) {

	try {
	    DocTrackingVerbiage dv = (DocTrackingVerbiage) verb.get(ndx);
	    return dv.getDocumentLabel();
	} catch (ArrayIndexOutOfBoundsException a) {
	    return null;
	} catch (NullPointerException np) {
	    return null;
	}
    }

    public void setDocumentLabel(int ndx, String label)
	    throws ArrayIndexOutOfBoundsException {

	DocTrackingVerbiage cv = null;

	cv = (DocTrackingVerbiage) verb.get(ndx);

	cv.setDocumentLabel(label);
    }

    public void setDocumentLabelForLanguage(int lang, String label) {
	if (label == null)
	    return;
	DocTrackingVerbiage cv = null;
	// logger.debug("DT@setDocumentLabelForLanguage::VerbSize: " +
        // verb.size());
	// logger.debug("DT@setDocumentLabelForLanguage::LangId: " + lang);

	for (int i = 0; i < verb.size(); i++) {
	    cv = (DocTrackingVerbiage) verb.get(i);
	    // logger.debug("DT@setDocumentLabelForLanguage::PreferenceLanguage:
                // " + cv.getLanguagePreferenceId());
	    // logger.debug("DT@setDocumentLabelForLanguage::DocText: " +
                // cv.getDocumentText());
	    // logger.debug("DT@setDocumentLabelForLanguage::DocTrackingId:
                // " + cv.getDocumentTrackingId());

	    if (cv.getLanguagePreferenceId() == lang) {
		cv.setDocumentLabel(label);
		return;
	    }
	}

	cv.setDocumentLabel(label);
    }

  public String getDocumentLabelByLanguage(int languagePref) {

		String isGenerateOnTheFly = PropertiesCache.getInstance().getInstanceProperty(
				"com.basis100.conditions.dtverbiage.generateonthefly", "N");
		try {
			if (null != isGenerateOnTheFly && "N".equals(isGenerateOnTheFly)) {
				DocTrackingVerbiage dv = null;

				Iterator i = verb.iterator();

				while (i.hasNext()) {
					dv = (DocTrackingVerbiage) i.next();
					if (dv.getLanguagePreferenceId() == languagePref) {
						return dv.getDocumentLabel();
					}
				}
			} else if (null != isGenerateOnTheFly
					&& "Y".equals(isGenerateOnTheFly)) {
				String text = this.hasLabel(languagePref);
				if (null != text) {
					return text;
				}
				return generateDocumentLabelOnTheFly(languagePref);

			}
		} catch (ArrayIndexOutOfBoundsException a) {
			return null;
		} catch (NullPointerException np) {
			return null;
		}

		return null;
	}






  public String getDocumentTextByLanguage(int languagePref) {
	  String isGenerateOnTheFly = PropertiesCache.getInstance().
					getInstanceProperty("com.basis100.conditions.dtverbiage.generateonthefly", "N");
		try {
			if (null != isGenerateOnTheFly && "N".equals(isGenerateOnTheFly)) {
				DocTrackingVerbiage dv = null;

				Iterator i = verb.iterator();

				while (i.hasNext()) {
					dv = (DocTrackingVerbiage) i.next();
					if (dv.getLanguagePreferenceId() == languagePref) {
						return dv.getDocumentText();
					}
				}
			} else if (null != isGenerateOnTheFly
					&& "Y".equals(isGenerateOnTheFly)) {

				String text = this.hasVerbiage(languagePref);
				// if verbiage exists - terurn it.
				if (text != null) {
					return text;
				}

				return generateDocumentTextVerbiageOnTheFly(languagePref);
			}
		} catch (ArrayIndexOutOfBoundsException a) {
			return null;
		} catch (NullPointerException np) {
			return null;
		}

		return null;
	}

    public String getDocumentTextAndRole(int languagePref) {
	try {
	    DocTrackingVerbiage dv = null;

	    Iterator i = verb.iterator();

	    while (i.hasNext()) {
		dv = (DocTrackingVerbiage) i.next();
		if (dv.getLanguagePreferenceId() == languagePref) {
		    // Get DocumentText
		    String finalStr = dv.getDocumentText();

		    String role = getCCRDescription();

		    finalStr = finalStr + " " + "( " + role + " )";

		    return finalStr;
		}
	    }
	} catch (ArrayIndexOutOfBoundsException a) {
	    return null;
	} catch (NullPointerException np) {
	    return null;
	}

	return null;

    }

    public int getSequenceId() throws Exception  
    {  
        if(conditionSection == null)  
            loadSection();  

        if(conditionSection == null)  
            return 0;  
        
        return conditionSection.sequenceId;  
    }  

    
    public String getCCRDescription() {
	int drr = this.getDocumentResponsibilityRoleId();
	String desc = PicklistData.getDescription(
		"ConditionResponsibilityRole", drr);

	return desc;
    }

    public String getCCRDescription(int lang) {
	int drr = this.getDocumentResponsibilityRoleId();
	String desc = PicklistData.getDescription(
		"ConditionResponsibilityRole", drr);

	com.basis100.deal.docprep.DocPrepLanguage.getInstance().getTerm(desc,
		lang);

	return desc;
    }

    public int getLanguagePreferenceId(int ndx) {
	try {
	    DocTrackingVerbiage dtv = (DocTrackingVerbiage) verb.get(ndx);
	    return dtv.getLanguagePreferenceId();
	} catch (ArrayIndexOutOfBoundsException a) {
	    return -1;
	} catch (NullPointerException np) {
	    return -1;
	}

    }

    // read only properties
    public String getSectionDescription() throws Exception {
	if (conditionSection == null)
	    loadSection();

	if (conditionSection == null)
	    return null;

	return conditionSection.sectionDescription;
    }

    public String getSectionCode() throws Exception {
	if (conditionSection == null)
	    loadSection();

	if (conditionSection == null)
	    return null;

	// zivko:marker: ticket number must be provided
	return conditionSection.sectionCode;
	// return conditionSection.sectionDescription;
    }

    public boolean isAuditable() {
	return true;
    }

    public int getClassId() {
	return ClassId.DOCUMENTTRACKING;
    }

    public boolean equals(Object o) {
	if (o instanceof DocumentTracking) {
	    DocumentTracking oa = (DocumentTracking) o;
	    if ((this.documentTrackingId == oa.getDocumentTrackingId())
		    && (this.copyId == oa.getCopyId()))
		return true;
	}
	return false;
    }

    /**
     * Business method for use by DocumentTracking entity
     * 
     * @return boolean
     */
    /*#DG638 public boolean hasVerbiage() {
	if (verb == null)
	    return false;

	return verb.isEmpty();
    }*/

    /**
         * Business method for use by DocumentTracking entity
         * 
         * @return int
         */
  public int verbiageSize()
  {
    /*#DG638 
    if(hasVerbiage())
     return verb.size();
    else
     return 0;*/
  	return verb.size();
  }
  
  public String hasVerbiage(int languagePref)
  {
    try
    {
      DocTrackingVerbiage dv = null;

      Iterator i = verb.iterator();

      while(i.hasNext())
      {
        dv = (DocTrackingVerbiage)i.next();
        if(dv.getLanguagePreferenceId() == languagePref)
        {
          return dv.getDocumentText();
        }
      }
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
      return null;
    }
     catch(NullPointerException np)
    {
      return null;
    }

    return null;
  }
  
  public String hasLabel(int languagePref)
  {
    try
    {
      DocTrackingVerbiage dv = null;

      Iterator i = verb.iterator();

      while(i.hasNext())
      {
        dv = (DocTrackingVerbiage)i.next();
        if(dv.getLanguagePreferenceId() == languagePref)
        {
          return dv.getDocumentLabel();
        }
      }
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
      return null;
    }
     catch(NullPointerException np)
    {
      return null;
    }

    return null;
  }

  /**
   * removeSons
   *
   * @throws Exception
   */
  protected void removeSons() throws Exception
  {
    if(verb.isEmpty()) return;

    DocTrackingVerbiage vb = (DocTrackingVerbiage)verb.get(0);
    vb.deleteRecords(this.pk);
  }
  
  public String generateDocumentTextVerbiageOnTheFly(int languageId) {
	  
	  int contextId = this.getTagContextId();
	  int classId = this.getTagContextClassId();
	  String text = "";
	  
	  try {
	      DealEntity dealEntity = EntityBuilder.constructEntity(classId, srk, null, contextId,copyId);
	      Condition condition = new Condition(srk, this.conditionId);
	      String conditionsText = condition.getConditionTextByLanguage(languageId);
	      ConditionRegExParser conditionRegExParser = new ConditionRegExParser(srk);
	      text = conditionRegExParser.replaceTages(conditionsText, dealEntity, languageId);
	  }
	  catch(Exception e) {
		  // TODO Serghei
		  return null;
	  }
	  
	  return text;
	  
  }
  
  public String generateDocumentLabelOnTheFly(int languagePref) {
	  
	  try {
	      Condition condition = new Condition(srk, this.conditionId);
	      return condition.getConditionLabel(languagePref);
	  }
	  catch(Exception e) {
		// TODO Serghei
		  return null; 
	  }
  }
  
  public void createMissingDTVerbiage(Condition condition) throws RemoteException,CreateException, JdbcTransactionException {
	  
	  int len = condition.verbiageSize();

      for(int i = 0; i < len ; i++) {
    	  
    	  String label = null;	  
    	  String text = this.hasVerbiage(i);
    	  
    	  if(null != text)
    		  return;
    	  
    	  text  = this.generateDocumentTextVerbiageOnTheFly(i);
    	  label = this.generateDocumentLabelOnTheFly(i);
    	  
    	  DocTrackingVerbiage dtv = new DocTrackingVerbiage(this.srk,this.getDocumentTrackingId(),
                  this.getCopyId(),i, label, text);
    	  dtv.create((DocumentTrackingPK)this.getPk());
    	  dtv.setDocumentLabel(label);
    	  dtv.setDocumentText(text);
    	  dtv.setLanguagePreferenceId(i);
    	  dtv.ejbStore();
    	  
    	  this.verb.add(dtv);
    	  
      }
	  
  }

  /**
   * <p>Title: </p>
   *
   * <p>Description: </p>
   *
   * <p>Copyright: Copyright (c) 2004</p>
   *
   * <p>Company: Filogix Inc.</p>
   * @author Billy
   * @version 1.0
   */
  public class DocTrackingVerbiage extends DealEntity
  {
    String documentText;
    String documentLabel;
    int languagePreferenceId;
    int documentTrackingId = -1;

	/**
         * DocTrackingVerbiage
         * 
         * @param srk
         *                SessionResourceKit
         * @param doctrackId
         *                int
         * @param cid
         *                int
         * @param lang
         *                int
         * @param lab
         *                String
         * @param verb
         *                String
         * @throws RemoteException
         */
	public DocTrackingVerbiage(SessionResourceKit srk, int doctrackId,
		int cid, int lang, String lab, String verb)
		throws RemoteException {
	    super(srk);
	    this.setDocumentText(verb);
	    this.setDocumentLabel(lab);
	    this.setLanguagePreferenceId(lang);
	    documentTrackingId = doctrackId;
	    copyId = cid;
	}
	
	//4.4 Entity Cache
	public DocTrackingVerbiage(DocTrackingVerbiage other){
	    this.setDocumentText(other.getDocumentText());
	    this.setDocumentLabel(other.getDocumentLabel());
	    this.setLanguagePreferenceId(other.getLanguagePreferenceId());
	    this.setDocumentTrackingId(other.getDocumentTrackingId());
	} 
	
	/**
         * setDocumentText
         * 
         * @param text
         *                String
         */
	public void setDocumentText(String text) {
	    this.testChange("documentText", text);
	    documentText = text;
	}

	/**
         * getDocumentText
         * 
         * @return String
         */
	public String getDocumentText() {
	    return documentText;
	}

	/**
         * setLanguagePreferenceId
         * 
         * @param id
         *                int
         */
	public void setLanguagePreferenceId(int id) {
	    this.testChange("languagePreferenceId", id);
	    languagePreferenceId = id;
	}

	/**
         * getLanguagePreferenceId
         * 
         * @return int
         */
	public int getLanguagePreferenceId() {
	    return languagePreferenceId;
	}

	/**
         * setDocumentLabel
         * 
         * @param label
         *                String
         */
	public void setDocumentLabel(String label) {
	    this.testChange("documentLabel", label);
	    documentLabel = label;
	}

	/**
         * getDocumentLabel
         * 
         * @return String
         */
	public String getDocumentLabel() {
	    return documentLabel;
	}

	/**
         * setDocumentTrackingId
         * 
         * @param id
         *                int
         */
	public void setDocumentTrackingId(int id) {
	    this.testChange("documentTrackingId", id);
	    documentTrackingId = id;
	}

	/**
         * getDocumentTrackingId
         * 
         * @return int
         */
	public int getDocumentTrackingId() {
	    return documentTrackingId;
	}

	// Modified to use PrepareStatement to handle the SQL problem with
        // String > 4K
	// -- By BILLY 25Feb2002
	/**
         * performUpdate
         * 
         * @throws Exception
         * @return int
         */
	protected int performUpdate() throws Exception {

		//================================================================
		//CLOB Performance Enhancement June 2010
			String sql = "Update DocumentTrackingVerbiage set documentLabel = ?, documentText = ?, documentTextLite = ?"
					+ " where documentTrackingId = ? and copyId = ? and languagePreferenceId = ?";
		//================================================================
	    
	    // FXP25883
	    Clob clob = null;
	    try {
		// Get the PrepareStatement
		PreparedStatement pstmt = jExec.getCon().prepareStatement(sql);

		// Set documentLabel
		// --Release2.1--//
		pstmt.setString(1, documentLabel);
		
      	//================================================================
		//CLOB Performance Enhancement June 2010
		if(null == documentText) {
			throw new Exception("documentText cannot be null.");
		} else if(documentText.length() > 4000) {
			clob = createClob(documentText);
			pstmt.setClob(2, clob);
			pstmt.setNull(3, java.sql.Types.VARCHAR);
		} else {
			pstmt.setNull(2, java.sql.Types.CLOB);
			pstmt.setString(3, documentText);
		}
		//================================================================
				
		// Set documentTrackingId
		pstmt.setInt(4, documentTrackingId);
		// Set CopyId
		pstmt.setInt(5, copyId);
		// Set languagePreferenceId
		pstmt.setInt(6, languagePreferenceId);

		int result = pstmt.executeUpdate();
		pstmt.close();
		return result;

		// return (jExec.executeUpdate(sql));
	    } catch (Exception s) {
    		throw new Exception(
    			"DocTrackingVerbiage - could not update record : SQL = "
    				+ sql + " :: " + s);
        } finally {// FXP25883 
            try {
                if (clob != null) 
                    oracle.sql.CLOB.freeTemporary((oracle.sql.CLOB)clob);
            } catch (SQLException sqle) {
                logger.debug("could not release CLOB temp");
                logger.error(sqle);
            }
        }
	}

	// Modified to use PrepareStatement to handle the SQL problem with
        // String > 4K
	// -- By BILLY 25Feb2002
	/**
         * create
         * 
         * @param pk
         *                DocumentTrackingPK
         * @throws CreateException
         */
	void create(DocumentTrackingPK pk) throws CreateException {
	    StringBuffer col = new StringBuffer(
		    "Insert into DocumentTrackingVerbiage( documentTrackingId, copyId  ");

	    // StringBuffer val = new StringBuffer(" values(
                // ").append(pk.getId());
	    // val.append(", ").append(pk.getCopyId());
	    StringBuffer val = new StringBuffer(" values( ?, ?");
	    
		//================================================================
		//CLOB Performance Enhancement June 2010
		int strLen = (null != documentText)?documentText.length():0;
		//================================================================
		
	    if (documentText != null) {
	    	//================================================================
			//CLOB Performance Enhancement June 2010
			if(strLen > 4000) {
				col.append(", documentText");
				val.append(", ? ");
			} else {
				col.append(", documentTextLite");
				val.append(", ?");
			}
			//================================================================
	    } else {
	    	throw new CreateException("DocumentText cannot be null.");
	    }
	    if (documentLabel != null) {
		col.append(", documentLabel");
		// val.append(",
                // '").append(StringUtil.makeQuoteSafe(documentLabel)).append("'
                // ");
		val.append(", ? ");
	    }
	    if (languagePreferenceId != -1) {
		col.append(",languagePreferenceId ");
		// val.append(", ").append(languagePreferenceId);
		val.append(", ? ");
	    }

	    col.append(",INSTITUTIONPROFILEID ");
	    val.append(",");
	    
	    val.append(srk.getExpressState().getDealInstitutionId()); 
	    
	    col.append(") ");
	    val.append(") ");
	    String sql = col.toString() + val.toString();
	    
	    // FXP25883
        Clob clob = null;

	    try {
		// Get the preparStatement
		// Get the PrepareStatement
		PreparedStatement pstmt = jExec.getCon().prepareStatement(sql);
		int index = 1;

		// Set documentTrackingId
		pstmt.setInt(index++, pk.getId());
		// Set Copyid
		pstmt.setInt(index++, pk.getCopyId());
		// Set documentText
		if (documentText != null) {
			//================================================================
			//CLOB Performance Enhancement June 2010
			if(strLen > 4000) {
				clob = createClob(documentText);
				pstmt.setClob(index++, clob);
			} else {
				pstmt.setString(index++, documentText);
			}
			//================================================================
		} else {
	    	throw new CreateException("DocumentText cannot be null.");
	    }
		// Set documentLabel
		if (documentLabel != null)
		    pstmt.setString(index++, documentLabel);
		// Set languagePreferenceId
		if (languagePreferenceId != -1)
		    pstmt.setInt(index++, languagePreferenceId);

		// logger.debug("DocTrackingVerbiage : the SQL : " + sql);

		pstmt.executeUpdate();
		// srk.getJdbcExecutor().executeUpdate(sql);
		pstmt.close();
		
		
	    } catch (Exception s) {
	    	logger.error("sql = " + sql);
	    	logger.error(s);
	    	throw new CreateException(
	    			"DocTrackingVerbiage - could not create record : "
	    			+ s.getMessage());
        } finally {// FXP25883 
            try {
                if (clob != null) 
                    oracle.sql.CLOB.freeTemporary((oracle.sql.CLOB)clob);
            } catch (SQLException sqle) {
                logger.debug("could not release CLOB temp");
                logger.error(sqle);
            }
        }
	}

	/**
         * deleteRecords
         * 
         * @param pk
         *                DocumentTrackingPK
         * @throws Exception
         */
	public void deleteRecords(DocumentTrackingPK pk) throws Exception {
	    StringBuffer rec = new StringBuffer(
		    "Delete from DocumentTrackingVerbiage where DocumentTrackingId = ");
	    rec.append(pk.getId()).append(" and copyId = ").append(
		    pk.getCopyId());

	    jExec.executeUpdate(rec.toString());
	}

    }

    /**
         * <p>
         * Title:
         * </p>
         * 
         * <p>
         * Description:
         * </p>
         * 
         * <p>
         * Copyright: Copyright (c) 2004
         * </p>
         * 
         * <p>
         * Company: Filogix Inc.
         * </p>
         * 
         * @author Billy
         * @version 1.0
         */
    class ConditionSection {
	String sectionDescription;

	String sectionCode;
	int sequenceId; 

	public ConditionSection(String desc, String code, int seqId) {
	    sectionDescription = desc;
	    sectionCode = code;
	    sequenceId = seqId; 
	}
    }

    /**
         * getEntityContext
         * 
         * @throws RemoteException
         * @return EntityContext
         */
    protected EntityContext getEntityContext() throws RemoteException {
	EntityContext ctx = this.entityContext;

	try {
	    ctx.setContextText("DocumentTracking");
	    ctx.setContextSource(String.valueOf(this.documentTrackingSourceId));

	    // Modified by Billy for performance tuning -- By BILLY
                // 28Jan2002
	    // Deal deal = new Deal( srk, null , this.dealId , this.copyId
                // );
	    // if ( deal == null )
	    // return ctx;
	    // String sc = String.valueOf ( deal.getScenarioNumber () ) +
                // deal.getCopyType();
	    Deal deal = new Deal(srk, null);
	    String sc = String.valueOf(deal.getScenarioNumber(this.dealId,
		    this.copyId))
		    + deal.getCopyType(this.dealId, this.copyId);
	    // ===================================================================================

	    ctx.setScenarioContext(sc);

	    ctx.setApplicationId(this.dealId);

	} catch (Exception e) {
	    if (e instanceof FinderException) {
		logger
			.warn("DocumentTracking.getEntityContext Parent Not Found. documentTrackingId="
				+ this.documentTrackingId);
		return ctx;
	    }

	    throw (RemoteException) e;
	}
	return ctx;
    }

    public int getInstProfileId() {
	return instProfileId;
    }

    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
    }
public String getUseDefaultChecked() {
		return useDefaultChecked;
	}

	public void setUseDefaultChecked(String useDefaultChecked) {
		this.testChange("useDefaultChecked", useDefaultChecked);
		this.useDefaultChecked = useDefaultChecked;
	}

	public String getDisplayDefault() {
		return displayDefault;
	}

	public void setDisplayDefault(String displayDefault) {
		this.testChange("displayDefault", displayDefault);
		this.displayDefault = displayDefault;
	}

	/***************MCM Impl Team Bug Fix :FXP22542 :added new method to get the section code Starts************************************/
    /**
     * @version 1.1 06-Oct-2008 This method will loads the section code for given condition.
     */
    public String loadSectionCode() throws Exception {
        StringBuffer sqlbuf = new StringBuffer(
          "Select  SECTIONDESCRIPTION, SECTIONCODE, SEQUENCEID ");
        sqlbuf.append("from ConditionSection where conditionId = ");
        sqlbuf.append(getConditionId());

        String sql = sqlbuf.toString();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
          conditionSection = new ConditionSection(
            jExec.getString(key, 1), jExec.getString(key, 2), jExec.getInt(key, 3)); 
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
              "DocmentTracking Entity - loadConditionSection() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        if (conditionSection == null)
            return null;

    
        return conditionSection.sectionCode;

          }
/*******************************Bug Fix :FXP22542 Ends*******************************************/
    
    /**
     * findForConditionUpdate
     * 
     * @param pk
     *                DealPK
     * @throws Exception
     * @return Collection
     * change log :
     * @version 1.0 April 22 2009
     */
    public Collection<DocumentTracking> findForConditionUpdate(DealPK pk) throws Exception {

        String sql = "select D.*  from DocumentTracking D, DocumentStatus S, Condition C " 
                + pk.getWhereClause() + " and D.DocumentStatusId = S.DocumentStatusId " 
                + " and D.institutionProfileId = C.institutionProfileId and D.ConditionId = C.conditionId "
                + " Order by S.SortOrder, C.ConditionTypeId ";

        return findBySqlColl(sql);
    }
    
    // added  comma delimited condition type list
    public Collection<DocumentTracking> findForConditionUpdate(DealPK pk, String conditionTypes) throws Exception {

        if (conditionTypes == null || conditionTypes.length()==0){
            return null;
        }
            
        StringBuffer sql = new StringBuffer("select D.*  from DocumentTracking D, DocumentStatus S, Condition C ")
            .append(pk.getWhereClause()).append(" and D.DocumentStatusId = S.DocumentStatusId ")
            .append(" and D.ConditionId = C.conditionId ")
            .append(" and C.conditionTypeId in (").append( conditionTypes ).append(") ")
            .append(" Order by S.SortOrder, C.ConditionTypeId ");

        return findBySqlColl(sql.toString());
    }

    //added for performance enchangement
    //before store CM request in Q, check the deal need condtion status update
    public boolean needStatusUpdate(DealPK pk, String conditionTypes, boolean statusCheck) throws Exception {

        if (conditionTypes == null || conditionTypes.length() ==0){
            return false;
        }
	    
        StringBuffer sqlb = new StringBuffer("select count(*)  from DocumentTracking D, DocumentStatus S, Condition C, Deal DE ")
            .append("where DE.dealid = ").append(pk.getId()).append(" and  DE.Copyid = ").append(pk.getCopyId()) 
            .append(" and DE.Dealid = D.dealid and DE.Copyid = D.copyid ")
            .append(" and D.DocumentStatusId = S.DocumentStatusId ")
            .append(" and D.ConditionId = C.conditionId ")
            .append(" and C.conditionTypeId in (").append( conditionTypes ).append(") ");
        if (statusCheck) {
            sqlb.append(" and (  D.DocumentStatusId <> ").append(Mc.DOC_STATUS_APPROVED)
               .append(" or (to_char(D.DSTATUSCHANGEDATE, 'YYYY-MON-DD') = to_char(SYSDATE, 'YYYY-MON-DD')")
               .append(" and to_char(DE.STATUSDATE, 'YYYY-MON-DD') = to_char(SYSDATE, 'YYYY-MON-DD')))");
        }
        
        String sql = sqlb.toString();
        int count = 0;
        
        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                count = jExec.getInt(key, 1);
        	break;
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
              "DocmentTracking Entity - existConditionWithConditionType() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return count > 0;
    }
    

}
