package com.basis100.deal.entity;

import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import oracle.jdbc.driver.OracleCallableStatement;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationResponsePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.pk.SAMResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

public class SAMResponse extends DealEntity{
	protected int           responseId;
	protected int	        samDecisionId;
	protected double	    samAmountApproved;
	protected String 		samResult;
	protected String 	    samXML;
    protected int         institutionProfileId;

	private SAMResponsePK pk;

	public SAMResponse(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public SAMResponse(SessionResourceKit srk, int id) throws RemoteException, FinderException
	{
		super(srk);
	    pk = new SAMResponsePK(id);
	    findByPrimaryKey(pk);
	  }

	/**  search for SAMResponse record using primary key
	*   @return a SAMResponse */
	public SAMResponse findByPrimaryKey(SAMResponsePK pk) throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from SAMResponse " + pk.getWhereClause();
	    boolean gotRecord = false;
	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }
	        jExec.closeData(key);
	        if (gotRecord == false)
	        {
	          String msg = "SAMResponse: @findByPrimaryKey(), key= " + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException("SAMResponse Entity - findByPrimaryKey() exception");
	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);
	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}

    public Collection findByResponseId(int resid) throws Exception
    {
      Collection samresponses = new ArrayList();

      String sql = "Select * from SAMResponses " ;
            sql += "where ResponseId = " + resid;

        try
        {
              int key = jExec.execute(sql);
              for (; jExec.next(key); )
              {
                SAMResponse samresponse = new SAMResponse(srk,jExec.getInt(key,"responseId"));
                samresponse.setPropertiesFromQueryResult(key);
                samresponse.pk = new SAMResponsePK(samresponse.getResponseId());
                samresponses.add(samresponse);
              }
              jExec.closeData(key);
          }
          catch (Exception e)
          {
            Exception fe = new Exception("Exception caught while getting Deal::SAMResponse");
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
          }
          return samresponses;
    }

    public Collection findByResponse(ResponsePK rk) throws Exception
    {
      Collection samresponses = new ArrayList();

      String sql = "Select * from SAMResponses " ;
            sql +=  rk.getWhereClause();

        try
        {
              int key = jExec.execute(sql);
              for (; jExec.next(key); )
              {
                SAMResponse samresponse = new SAMResponse(srk,jExec.getInt(key,"responseId"));
                samresponse.setPropertiesFromQueryResult(key);
                samresponse.pk = new SAMResponsePK(samresponse.getResponseId());
                samresponses.add(samresponse);
              }
              jExec.closeData(key);
          }
          catch (Exception e)
          {
            Exception fe = new Exception("Exception caught while getting Deal::SAMResponse");
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
          }
          return samresponses;
    }

    public Collection findByDeal(DealPK pk) throws Exception
    {
      Collection samresponses = new ArrayList();

      String sql = "Select sr.* from SAMResponse sr, Response r " ;
            sql += "where sr.responseId = r.responseId and r.dealId = ";
            sql += pk.getId() + " and r.copyId = " + pk.getCopyId();
            sql += " order by r.ResponseDate desc";
      try
      {

            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              SAMResponse samresponse = new SAMResponse(srk,jExec.getInt(key,"responseId"));
              samresponse.setPropertiesFromQueryResult(key);
              samresponse.pk = new SAMResponsePK(samresponse.getResponseId());
              samresponses.add(samresponse);
            }

            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::SAMResponse");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }
        return samresponses;
    }

	/**  sets properties for SAMResponse */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	  	this.setResponseId(jExec.getInt(key, "responseId"));
	  	this.setSAMDecisionId(jExec.getInt(key, "samDecisionId"));
	  	this.setSAMAmountApproved(jExec.getDouble(key, "samAmountApproved"));
	  	this.setSAMResult(readSAMResult());
	  	this.setSAMXML(readSAMXML());
        this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		String [] arrExcl = { "SAMRESULT","SAMXML" };

		int ret = doPerformUpdate(arrExcl);
	  	writeSAMResult();
	  	writeSAMXML();
		return ret;
	}


	public String getEntityTableName()
	{
	    return "SAMResponse"; 
	}

	/**  gets the pk associated with this entity
	 *   @return a SAMResponsePK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}	

	public int getResponseId()
	{ 
		return this.responseId;
	}

	public void setResponseId(int id)
	{
		this.testChange ("responseId", id);
		this.responseId = id;
	}

	public int getSAMDecisionId()
	{ 
		return this.samDecisionId;
	}

	public void setSAMDecisionId(int id)
	{
		this.testChange ("samDecisionId", id);
		this.samDecisionId = id;
	}

	public double getSAMAmountApproved()
	{ 
		return this.samAmountApproved;
	}

	public void setSAMAmountApproved(double amt)
	{
		this.testChange ("samAmountApproved", amt);
		this.samAmountApproved = amt;
	}

	
	public String getSAMResult()
	{ 
		return this.samResult;
	}

	public void setSAMResult(String res)
	{
		this.testChange ("samResult", res);
		this.samResult = res;
	}

	public String getSAMXML()
	{ 
		return this.samXML;
	}

	public void setSAMXML(String sxml)
	{
//		String samxml = parseHTML(sxml);
		this.testChange ("samXML", sxml);
		this.samXML = sxml;
	}

	/**  creates SAMResponse record into database
	 * 	 using ResponsePK - minimum fields
	 *   @return a SAMResponse */
	public SAMResponse create(ResponsePK rpk)throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into SAMResponse(" + rpk.getName() +  ", samResult, "
      + "samXML, institutionProfileId) Values ( " + rpk.getId() 
      + ", empty_clob(), empty_clob(), " 
      + srk.getExpressState().getDealInstitutionId() + ")";
		
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new SAMResponsePK(rpk.getId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("SAMResponse Entity - SAMResponse - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
	  	return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof SAMResponse)
		{
			SAMResponse oa = (SAMResponse) o;
			if  (  this.responseId == oa.getResponseId ()  )
				return true;
		}
		return false;
	}

	private String readSAMResult()
	{
		JdbcExecutor je =  srk.getJdbcExecutor();
		Connection con = je.getCon();
		String outval = "";
		ResultSet rs = null;
		Statement stm = null;
		OracleCallableStatement callableStmt = null;
		try
		{
			String stmt = "Select SAMResult from SAMResponse ";
			stmt += "where ResponseId = " + this.getResponseId();

			oracle.sql.CLOB clob = null;

			stm = con.createStatement() ;
			rs = stm.executeQuery(stmt);

			if (rs.next())
			{
				clob = (oracle.sql.CLOB)rs.getObject(1);

				if(clob == null)
					return outval;

				if(clob.length() <= 0)
					return outval;

          callableStmt =
           (OracleCallableStatement)con.prepareCall("begin dbms_LOB.read(?,?,?,?); end;");
          long totalLength = clob.length();
          long i = 0;             // index at which to read
          int chunk = 30*1024;    // size of the chunk to read :: Oracle limit is 30K
          long read_this_time = 0;

          while (i < totalLength)
          {
            callableStmt.clearParameters();
            callableStmt.setCLOB(1,clob);
            callableStmt.setLong(2,chunk); //size of chunk
            callableStmt.registerOutParameter(2,Types.NUMERIC); //Bytes received
            callableStmt.setLong(3, i+1); // begin reading at the offset
            callableStmt.registerOutParameter (4,Types.LONGVARCHAR);
            callableStmt.execute ();

            read_this_time = callableStmt.getLong (2);
            outval += callableStmt.getString(4);
            i += read_this_time;
          }
          callableStmt.close();
        }

        rs.close();
        stm.close();
	}
      catch(Exception e)
      {
        logger.error("SAMResponse::SAMResult: failed to read data from report: " + this.getPk());
        logger.info("In Transaction: " + je.isInTransaction());

        String msg = "";

        if(e.getMessage() == null)
         msg = "Unknown Error Occurred";
        logger.error("SAMResponse::SAMResult: Reason for failure: " + msg);
        logger.error(e);
        try{ rs.close();            }catch(Exception ea){;}
        try{ stm.close();           }catch(Exception eb){;}
        try{ callableStmt.close();  }catch(Exception ec){;}
      } finally {
          try {
              if(rs != null) rs.close();
          } catch (Exception ignore){}
          try {
              if(stm != null) stm.close();
          } catch (Exception ignore){}
          try {
              if(callableStmt != null) callableStmt.close();
          } catch (Exception ignore){}
      }
      return outval;
   	}

	private void writeSAMResult()
	{
	      if(this.samResult == null )
	       this.samResult = "";
	      srk.setCloseJdbcConnectionOnRelease(true);
	      JdbcExecutor je =  srk.getJdbcExecutor();
	      Connection con = je.getCon();
	      ResultSet rs = null;
	      Statement stm = null;
		  Writer writer = null;
	      try
	      {
	        oracle.sql.CLOB clob = null;

	        //get insert a new locator
	        stm = con.createStatement() ;
	        String st1 = "Update SAMResponse set samResult = empty_clob() where ResponseId = ";
	        st1 +=  getResponseId();
	        stm.executeUpdate(st1);
	        con.setAutoCommit(false);
	        String st2 = "Select samResult from SAMResponse ";
	        st2 += "where ResponseId = " + getResponseId();
	        st2 += " for update";
	        rs = stm.executeQuery(st2);
	        if (rs.next())
	        {
	          clob = (oracle.sql.CLOB)rs.getObject(1);
	          if(clob == null)
	           throw new Exception("SAMResponse::SAMResult: Could not obtain LOB locator: " + this.pk);
			  writer = clob.getCharacterOutputStream();
	        }
	        rs.close();
	        stm.close();
	        writer.write(getSAMResult());
			writer.flush();
			writer.close();
	        con.commit();
	      }
	      catch(Exception e)
	      {
	        e.printStackTrace();
	        logger.error("SAMResponse::SAMResult: failed to write data to report: " + this.getPk());
	        logger.info("In Transaction: " + je.isInTransaction());

	        String msg = "";
	        if(e.getMessage() == null)
	         msg = "Unknown Error Occurred";
	        logger.error("SAMResponse::SAMResult: Reason for failure: " + msg);
	        logger.error(e);
	        try{ rs.close();      }catch(Exception ea){;}
	        try{ stm.close();     }catch(Exception eb){;}
	        try{ writer.close();  }catch(Exception ec){;}
	      }finally{
	          try {
	              if(rs != null) rs.close();
	          } catch (Exception ignore){}
	          try {
	              if(stm != null) stm.close();
	          } catch (Exception ignore){}
	      }
	}

	private String readSAMXML()
	{
		JdbcExecutor je =  srk.getJdbcExecutor();
		Connection con = je.getCon();
		String outval = "";
		ResultSet rs = null;
		Statement stm = null;
		OracleCallableStatement callableStmt = null;
		try
		{
			String stmt = "Select SAMXML from SAMResponse ";
			stmt += "where ResponseId = " + this.getResponseId();

			oracle.sql.CLOB clob = null;

			stm = con.createStatement() ;
			rs = stm.executeQuery(stmt);

			if (rs.next())
			{
				clob = (oracle.sql.CLOB)rs.getObject(1);

				if(clob == null)
					return outval;

				if(clob.length() <= 0)
					return outval;

          callableStmt =
           (OracleCallableStatement)con.prepareCall("begin dbms_LOB.read(?,?,?,?); end;");
          long totalLength = clob.length();
          long i = 0;             // index at which to read
          int chunk = 30*1024;    // size of the chunk to read :: Oracle limit is 30K
          long read_this_time = 0;

          while (i < totalLength)
          {
            callableStmt.clearParameters();
            callableStmt.setCLOB(1,clob);
            callableStmt.setLong(2,chunk); //size of chunk
            callableStmt.registerOutParameter(2,Types.NUMERIC); //Bytes received
            callableStmt.setLong(3, i+1); // begin reading at the offset
            callableStmt.registerOutParameter (4,Types.LONGVARCHAR);
            callableStmt.execute ();

            read_this_time = callableStmt.getLong (2);
            outval += callableStmt.getString(4);
            i += read_this_time;
          }
          callableStmt.close();
        }

        rs.close();
        stm.close();
	}
      catch(Exception e)
      {
        logger.error("SAMResponse::SAMXML: failed to read data from report: " + this.getPk());
        logger.info("In Transaction: " + je.isInTransaction());

        String msg = "";

        if(e.getMessage() == null)
         msg = "Unknown Error Occurred";
        logger.error("SAMResponse::SAMXML: Reason for failure: " + msg);
        logger.error(e);
        try{ rs.close();            }catch(Exception ea){;}
        try{ stm.close();           }catch(Exception eb){;}
        try{ callableStmt.close();  }catch(Exception ec){;}
      } finally {
          try {
              if(rs != null) rs.close();
          } catch (Exception ignore){}
          try {
              if(stm != null) stm.close();
          } catch (Exception ignore){}
          try {
              if(callableStmt != null) callableStmt.close();
          } catch (Exception ignore){}
      }
      return outval;
   	}

	private void writeSAMXML()
	{
	      if(this.samXML == null )
	       this.samXML = "";
	      srk.setCloseJdbcConnectionOnRelease(true);
	      JdbcExecutor je =  srk.getJdbcExecutor();
	      Connection con = je.getCon();
	      ResultSet rs = null;
	      Statement stm = null;
		  Writer writer = null;
	      try
	      {
	        oracle.sql.CLOB clob = null;

	        //get insert a new locator
	        stm = con.createStatement() ;
	        String st1 = "Update SAMResponse set samXML = empty_clob() where ResponseId = ";
	        st1 +=  getResponseId();
	        stm.executeUpdate(st1);
	        con.setAutoCommit(false);
	        String st2 = "Select samXML from SAMResponse ";
	        st2 += "where ResponseId = " + getResponseId();
	        st2 += " for update";
	        rs = stm.executeQuery(st2);
	        if (rs.next())
	        {
	          clob = (oracle.sql.CLOB)rs.getObject(1);
	          if(clob == null)
	           throw new Exception("SAMResponse::SAMXML: Could not obtain LOB locator: " + this.pk);
			  writer = clob.getCharacterOutputStream();
	        }
	        rs.close();
	        stm.close();
			writer.write(getSAMXML());
			writer.flush();
			writer.close();
	        con.commit();
	      }
	      catch(Exception e)
	      {
	        e.printStackTrace();
	        logger.error("SAMResponse::SAMXML: failed to write data to report: " + this.getPk());
	        logger.info("In Transaction: " + je.isInTransaction());

	        String msg = "";
	        if(e.getMessage() == null)
	         msg = "Unknown Error Occurred";
	        logger.error("SAMResponse::SAMXML: Reason for failure: " + msg);
	        logger.error(e);
	        try{ rs.close();      }catch(Exception ea){;}
	        try{ stm.close();     }catch(Exception eb){;}
	        try{ writer.close();  }catch(Exception ec){;}
	      }finally{
	          try {
	              if(rs != null) rs.close();
	          } catch (Exception ignore){}
	          try {
	              if(stm != null) stm.close();
	          } catch (Exception ignore){}
	      }
	}

    public Collection findByAdjudicationResponse(AdjudicationResponsePK pk) throws Exception
    {
      Collection samresponses = new ArrayList();

      String sql = "Select * from SAMResponse " ;
            sql +=  pk.getWhereClause();
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              SAMResponse samresponse = new SAMResponse(srk);
              samresponse.setPropertiesFromQueryResult(key);
              samresponse.pk = new SAMResponsePK(samresponse.getResponseId());
              samresponses.add(samresponse);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting AdjudicationResponse::SAMResponse");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }
        return samresponses;
    }

    public Collection getSAMDispositions() throws Exception
    {
      return new SAMDisposition(srk).findBySAMResponse(this.pk);
    }	

    private void setPk(SAMResponsePK pk) {
        this.pk = pk;
    }
    
    /**
     * This object should only be cloned with a known new PK.
     */
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    
    public Object clone(ResponsePK newPk) {
        
        SAMResponse clonedResponse = null;
        
        try {              
            // first create the response to generate the new PK
            clonedResponse = new SAMResponse(srk);
            clonedResponse = clonedResponse.create(newPk);
                      
            // call Object's clone() which does a shallow copy
            clonedResponse = (SAMResponse)super.clone();
            
            // restore the pk
            clonedResponse.setPk(new SAMResponsePK(newPk.getId()));
            clonedResponse.setResponseId(newPk.getId());
            
            clonedResponse.doPerformUpdateForce();         
        }
        catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in SAMResponse.clone().  Trying to clone responseid = " + this.responseId + " " + ex.getMessage());
        }
        
        return clonedResponse;        
    }

    protected void removeSons() throws Exception
    {

 	   // -------- start of all SAMDisposition -------------
 	   Collection sdisps = this.getSAMDispositions();
 	   Iterator itDisp = sdisps.iterator () ;
 	   while ( itDisp.hasNext () )
 	   {
 		  SAMDisposition sdisp = (SAMDisposition) itDisp.next () ;
 		   if ( itDisp.hasNext () )
 		   {
 			   sdisp.ejbRemove ( false );
 		   }
 	       else{
 	    	   sdisp.ejbRemove ( true );
 	      }
 	   }
 	   // -------- end of all SAMDisposition -------------
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }   

}
