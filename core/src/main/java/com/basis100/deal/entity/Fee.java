package com.basis100.deal.entity;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.FeePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class Fee extends DealEntity
{
   /**
    *  The primary key ID for the entry in the table. Provides link to the
    *  applicant entry and is used for lookup purposes to obtain the applicable description.
    */
   public int    feeId;
   /**
    *   The description of a standard fee to be assessed per lender policy.
    *   Institution-defined values:
    *   01 = Application Fee, 02 = Appraisal Fee, 03 = Agent Referral Fee, 04 = CHMC Fee,
    *   05 = Mortgage Insurance Premium, 06 = GE Capital Fee, 07 = Inspection Fee
    */
   public String feeDescription;
   /**
    *  Standard charge defined to this fee entry.
    */
   public double standardAmount;
   /**
    *   The GL Account that this fee would be deposited to when collected.
    */
   public String GLAccount;

   protected	 int    feeTypeId;
   protected	 int    offSetingFeeTypeId;
   protected	 int    defaultPaymentMethodId;
   protected	 int    feePayorTypeId;
   protected	 String feeBusinessId;
   protected	 boolean  payableIndicator ;
   protected	 double defaultFeeAmount;
   protected	 int    defaultFeePaymentDateTypeId;
   protected	 int    feeGenerationTypeId;
   protected   String refundable;

   protected 	 String MITypeFlag;
   protected 	 String includeInTotalFeeAmount;
   protected     int 	instProfileId;
   protected	 Integer	FCXFeeTypeID;
   
   //AAtcha ends
   private FeePK pk;


   public Fee(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
   {
      super(srk,dcm);
   }


   public Fee(SessionResourceKit srk, CalcMonitor dcm, int id) throws RemoteException, FinderException
   {
      super(srk,dcm);

      pk = new FeePK(id);

      findByPrimaryKey(pk);
   }

   public Fee findByPrimaryKey(FeePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from Fee where " + pk.getName()+ " = '" + pk.getId() + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

   /**
    *   return the first fee record with the given <code>FeeType</code>
    *   @param type - the FeeType.feeTypeId
    *   @return this Fee object populated by found record data
    */
   public Fee findFirstByType(int type) throws RemoteException, FinderException
   {
      String sql = "Select * from Fee where feeTypeId = " + type;

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Fee Entity - findFirstByType() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        return this;
    }

  /**
  *
  *<pre>
  *	 Name                            Null?    Type
  *	 ------------------------------- -------- ----
  *	 FEEID                           NOT NULL NUMBER(4)
  *	 FEEDESCRIPTION                           VARCHAR2(35)
  *	 STANDARDAMOUNT                           NUMBER(13,2)
  *	 GLACCOUNT                                VARCHAR2(35)
  *	 FEETYPEID                                NUMBER(2)
  *	 OFFSETINGFEETYPEID                       NUMBER(2)
  *	 DEFAULTPAYMENTMETHODID                   NUMBER(2)
  *	 FEEPAYORTYPEID                           NUMBER(2)
  *	 FEEBUSINESSID                            VARCHAR2(35)
  *	 PAYABLEINDICATOR                         CHAR(1)
  *	 DEFAULTFEEAMOUNT                         NUMBER(13,2)
  *	 DEFAULTFEEPAYMENTDATETYPEID              NUMBER(2)
  *	 FEEGENERATIONTYPEID                      NUMBER(2)
  *  REFUNDABLE                               CHAR(1)
  *  FCXFEETYPEID							  NUMBER(4)
  *</pre>
  */
  private void setPropertiesFromQueryResult(int key) throws Exception
  {
      this.setFeeId(jExec.getInt(key,"FEEID"));
      this.setFeeDescription(jExec.getString(key,"FEEDESCRIPTION"));
      this.setStandardAmount(jExec.getDouble(key,"STANDARDAMOUNT"));
      this.setGLAccount(jExec.getString(key,"GLACCOUNT"));

      this.setFeeTypeId(jExec.getInt(key,"FEETYPEID"));
      this.setOffSetingFeeTypeId(jExec.getInt(key,"OFFSETINGFEETYPEID"));
      this.setDefaultPaymentMethodId(jExec.getInt(key,"DEFAULTPAYMENTMETHODID"));
      this.setFeePayorTypeId(jExec.getInt(key,"FEEPAYORTYPEID"));
      this.setFeeBusinessId(jExec.getString(key,"FEEBUSINESSID"));
      this.setPayableIndicator(jExec.getString(key,"PAYABLEINDICATOR"));
      this.setDefaultFeeAmount(jExec.getDouble(key,"DEFAULTFEEAMOUNT"));
      this.setDefaultFeePaymentDateTypeId(jExec.getInt(key,"DEFAULTFEEPAYMENTDATETYPEID"));
      this.setFeeGenerationTypeId(jExec.getInt(key,"FEEGENERATIONTYPEID"));
      this.setRefundable( jExec.getString(key,"REFUNDABLE") );
      this.setFCXFeeTypeID(jExec.getInteger(key, "FCXFEETYPEID"));
      //AAtcha starts
      this.setMITypeFlag(jExec.getString(key,"MITYPEFLAG"));
      this.setIncludeInTotalFeeAmount(jExec.getString(key,"INCLUDEINTOTALFEEAMOUNT"));
      this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
  }

  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "Fee";
   }

	/**
   *   gets the standardAmount associated with this entity
   *
   *   @return a double
   */
   public double getStandardAmount()
   {
      return this.standardAmount ;
   }
  /**
   *   gets the GLAccount associated with this entity
   *
   *   @return a String form of the GLAccount
   */
   public String getGLAccount()
   {
      return this.GLAccount ;
   }
	/**
   *   gets the feeId associated with this entity
   *
   *   @return the FeeId
   */
    public int getFeeId()
   {
      return this.feeId ;
   }

   public int getClassId(){
    return ClassId.FEE;
   }

   public	 int getFeeTypeId(){return this.feeTypeId; }
   public	 int getOffSetingFeeTypeId(){return this.offSetingFeeTypeId; }
   public	 int getDefaultPaymentMethodId(){return this.defaultPaymentMethodId; }
   public	 int getFeePayorTypeId(){return this.feePayorTypeId; }
   public	 String getFeeBusinessId(){return this.feeBusinessId; }
   public	 boolean  getPayableIndicator (){return this.payableIndicator; }
   public	 double getDefaultFeeAmount(){return this.defaultFeeAmount; }
   public	 int getDefaultFeePaymentDateTypeId(){return this.defaultFeePaymentDateTypeId; }
   public	 int getFeeGenerationTypeId(){return this.feeGenerationTypeId; }
   public        String getFeeDescription(){return this.feeDescription;}
   public String getRefundable(){return this.refundable;}

   public 	 String getMITypeFlag() {return this.MITypeFlag ;}
   public 	 String getIncludeInTotalFeeAmount() {return this.includeInTotalFeeAmount ;}
   //AAtcha ends
	/**
   *   gets the pk associated with this entity
   *
   *   @return a FeePK
   */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }


   public Fee deepCopy() throws CloneNotSupportedException
   {
      return (Fee)this.clone();
   }

   public FeePK createPrimaryKey()throws CreateException
  {
       String sql = "Select Feeseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           jExec.closeData(key);
           if( id == -1 ) throw new Exception();

        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Fee Entity create() exception getting FeeId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new FeePK(id);
  }
    /**
   * creates a Fee record
   */
   //AAtcha added MITypeFlag and includeInTotalFeeAmount
   public Fee create( String GLAccount,
                      int feeTypeId,
                      int offSetingFeeTypeId,
                      int defaultPaymentMethodId,
                      int feePayorTypeId,
                      String feeBusinessId,
                      String payableIndicator,
                      double defaultFeeAmount,
                      int defaultFeePaymentDateTypeId,
                      int feeGenerationTypeId,
                      String MITypeFlag,
                      String includeInTotalFeeAmount)throws RemoteException, CreateException
   {

        pk = createPrimaryKey();

        String sql =  "Insert into Fee(feeId,  GLAccount, feeTypeId, offSetingFeeTypeId,defaultPaymentMethodId, " +
                      " feePayorTypeId, feeBusinessId, payableIndicator,defaultFeeAmount, " +
                      " defaultFeePaymentDateTypeId, feeGenerationTypeId, MITypeFlag, includeInTotalFeeAmount, INSTITUTIONPROFILEID) Values ( " +
                      pk.getId() + ", " + "'" + GLAccount + "'" + ", " + feeTypeId + ", " +
                      offSetingFeeTypeId + ", " + defaultPaymentMethodId +
                      ", " + feePayorTypeId + ", " + "'" + feeBusinessId + "'" + ", " + "'" + payableIndicator + "'" +
                      ", " + defaultFeeAmount + ", " + defaultFeePaymentDateTypeId +
                      ", " + feeGenerationTypeId + ", " + MITypeFlag + ", " + includeInTotalFeeAmount + "," + srk.getExpressState().getDealInstitutionId() + ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          this.trackEntityCreate (); // Track the creation
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("Fee Entity - Fee - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
      srk.setModified(true);
      return this;
  }

  /**
   * creates an Fee using the FeeId - the mininimum (ie.non-null) fields
   *
   */
   public Fee create(FeePK pk)throws RemoteException, CreateException
   {
      String sql = "Insert into Fee(feeId) Values ( " + pk.getId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        this.trackEntityCreate (); // track the creation
      }
      catch (Exception e)
      {

        CreateException ce = new CreateException("Fee Entity - Fee - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
      srk.setModified(true);
      return this;
    }

  public void setFeeId(int id)
	{
     this.testChange ("feeId", id);
		 this.feeId = id;
  }

  public void setFeeDescription(String ad )
	{
    this.testChange ("feeDescription", ad );
		this.feeDescription = ad;
  }

  public void setStandardAmount(double av)
	{
     this.testChange ("standardAmount", av );
		 this.standardAmount = av;
  }

  public void setGLAccount(String acc)
	{
     this.testChange ("GLAccount", acc);
		 this.GLAccount = acc;
  }

  public void setFeeTypeId(int id)
	{
     this.testChange("feeTypeId", id);
		 this.feeTypeId = id;
  }
  public void setOffSetingFeeTypeId(int id)
	{
     this.testChange("offSetingFeeTypeId", id );
		 this.offSetingFeeTypeId = id;
  }

  public void setDefaultPaymentMethodId(int id)
	{
     this.testChange ("defaultPaymentMethodId", id );
		 this.defaultPaymentMethodId = id;
  }
  public void setFeePayorTypeId(int id)
	{
     this.testChange ("feePayorTypeId", id );
		 this.feePayorTypeId = id;
  }

  public void setFeeBusinessId(String ad)
	{
     this.testChange ("feeBusinessId", ad);
		 this.feeBusinessId = ad;
  }

  public void setPayableIndicator(String indicator)
	{
     this.testChange ("payableIndicator", indicator);
		 setPayableIndicator(TypeConverter.booleanFrom(indicator));
  }

  public void setPayableIndicator(boolean indicator)
	{
     this.testChange ("payableIndicator", indicator);
		 this.payableIndicator = indicator;
  }

  public void setDefaultFeeAmount(double amt)
	{
     this.testChange("defaultFeeAmount", amt);
		 this.defaultFeeAmount = amt;
  }

  public void setDefaultFeePaymentDateTypeId(int id)
	{
     this.testChange ("defaultFeePaymentDateTypeId", id );
		 this.defaultFeePaymentDateTypeId = id;
  }

  public void setFeeGenerationTypeId(int type)
	{
     this.testChange ( "feeGenerationTypeId", type) ;
		 this.feeGenerationTypeId = type;
  }

   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }

  public void setRefundable(String pRef)
	{
     this.testChange ("refundable", pRef);
		 this.refundable = pRef;
  }

 //AAtcha starts
  public void setMITypeFlag(String indicator)
  {
	  this.testChange("MITypeFlag",indicator);
	  this.MITypeFlag = indicator;
  }
  
  public void setIncludeInTotalFeeAmount(String indicator)
  {
	  this.testChange("includeInTotalFeeAmount",indicator);
	  this.includeInTotalFeeAmount = indicator;
  }

//AAtcha Ends
  
    public int getInstProfileId() {
    	return instProfileId;
    }
    
    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
    }


	public Integer getFCXFeeTypeID() {
		return this.FCXFeeTypeID;
	}


	public void setFCXFeeTypeID(Integer feeTypeID) {
		this.testChange ("FCXFeeTypeID", feeTypeID);
		this.FCXFeeTypeID = feeTypeID;
	}

}
