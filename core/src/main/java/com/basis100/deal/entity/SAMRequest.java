package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationRequestPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.SAMRequestPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * Corresponds to the SAMRequest DB table.
 * 
 * @author AServilla
 */

public class SAMRequest extends DealEntity{
	protected int           requestId;
	protected int	        samStepId;
	protected int           samApplicationTypeId;
	protected int           samPropertyTypeId;
	protected int           samProductTypeId;
	protected String        samRequestTypeCode;
	protected String        samCreditFileRequired;
	protected String        samGuarantorOccupancy;
    protected int         institutionProfileId;

	private SAMRequestPK pk;

	public SAMRequest(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public SAMRequest(SessionResourceKit srk, int id) 
    throws RemoteException, FinderException
	{
		super(srk);
	    pk = new SAMRequestPK(id);
	    findByPrimaryKey(pk);
	  }

	/**  search for SAMRequest record using primary key
	*   @return a SAMRequest */
	public SAMRequest findByPrimaryKey(SAMRequestPK pk) 
    throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from SAMRequest " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "SAMRequest: @findByPrimaryKey(), key= " + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException("SAMRequest Entity - findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}

    public Collection findByRequest(AdjudicationRequestPK pk) throws Exception
    {
      Collection samrequests = new ArrayList();

      String sql = "Select * from SAMRequest " ;
            sql +=  pk.getWhereClause();
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              SAMRequest samrequest = new SAMRequest(srk);
              samrequest.setPropertiesFromQueryResult(key);
              samrequest.pk = new SAMRequestPK(samrequest.getRequestId());
              samrequests.add(samrequest);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::Borrowers");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return samrequests;
    }
	
	/**  sets properties for SAMRequest */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	    this.setRequestId(jExec.getInt(key, "RequestId"));
	  	this.setSAMStepId(jExec.getInt(key, "SAMStepId"));
	  	this.setSAMApplicationTypeId(jExec.getInt(key, "SAMApplicationTypeId"));
	  	this.setSAMPropertyTypeId(jExec.getInt(key, "SAMPropertyTypeId"));
	  	this.setSAMProductTypeId(jExec.getInt(key, "SAMProductTypeId"));
	  	this.setSAMRequestTypeCode(jExec.getString(key, "SAMRequestTypeCode"));
	  	this.setSAMCreditFileRequired(jExec.getString(key, "SAMCreditFileRequired"));
	  	this.setSAMGuarantorOccupancy(jExec.getString(key, "SAMGuarantorOccupancy"));
          this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	public String getEntityTableName()
	{
	    return "SAMRequest"; 
	}

	/**  gets the pk associated with this entity
	 *   @return a SAMRequestPK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}	

	/**
	 * @return Returns the requestId
	 */
	public int getRequestId()
	{ 
		return this.requestId;
	}

	/**
	 * @param id The requestId is set.
	 */
	public void setRequestId(int id)
	{
		this.testChange ("requestId", id);
		this.requestId = id;
	}

	/**
	 * @return Returns the samStepId
	 */
	public int getSAMStepId()
	{ 
		return this.samStepId;
	}

	/**
	 * @param id The samStepId is set.
	 */
	public void setSAMStepId(int id)
	{
		this.testChange ("samStepId", id);
		this.samStepId = id;
	}

	/**
	 * @return Returns the samApplicationTypeId
	 */
	public int getSAMApplicationTypeId()
	{ 
		return this.samApplicationTypeId;
	}

	/**
	 * @param id The samApplicationTypeId is set.
	 */
	public void setSAMApplicationTypeId(int id)
	{
		this.testChange ("SAMApplicationTypeId", id);
		this.samApplicationTypeId = id;
	}

	/**
	 * @return Returns the samPropertyTypeId
	 */
	public int getSAMPropertyTypeId()
	{ 
		return this.samPropertyTypeId;
	}

	/**
	 * @param id The samPropertyTypeId is set.
	 */
	public void setSAMPropertyTypeId(int id)
	{
		this.testChange ("samPropertyTypeId", id);
		this.samPropertyTypeId = id;
	}

	/**
	 * @return Returns the samProductTypeId
	 */
	public int getSAMProductTypeId()
	{ 
		return this.samProductTypeId;
	}

	/**
	 * @param id The samProductTypeId is set.
	 */
	public void setSAMProductTypeId(int id)
	{
		this.testChange ("samProductTypeId", id);
		this.samProductTypeId = id;
	}

	/**
	 * @return Returns the samRequestTypeCode
	 */
	public String getSAMRequestTypeCode()
	{ 
		return this.samRequestTypeCode;
	}

	/**
	 * @param samreqcode The samRequestTypeCode is set.
	 */
	public void setSAMRequestTypeCode(String samreqcode)
	{
		this.testChange ("samRequestTypeCode", samreqcode);
		this.samRequestTypeCode = samreqcode;
	}

	/**
	 * @return Returns the samCreditFileRequired
	 */
	public String getSAMCreditFileRequired()
	{ 
		return this.samCreditFileRequired;
	}

	/**
	 * @param samcredit The samCreditFileRequired is set.
	 */
	public void setSAMCreditFileRequired(String samcredit)
	{
		this.testChange ("samCreditFileRequired", samcredit);
		this.samCreditFileRequired = samcredit;
	}

	/**
	 * @return Returns the getSAMGuarantorOccupancy
	 */
	public String getSAMGuarantorOccupancy()
	{ 
		return this.samGuarantorOccupancy;
	}

	/**
	 * @param guaroccupancy The getSAMGuarantorOccupancy is set.
	 */
	public void setSAMGuarantorOccupancy(String guaroccupancy)
	{
		this.testChange ("samGuarantorOccupancy", guaroccupancy);
		this.samGuarantorOccupancy = guaroccupancy;
	}

	/**  creates SAMRequest record into database 
	 *   minimum fields
	 *   @return a SAMRequest */
	public SAMRequest create(RequestPK rpk)throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into SAMRequest(" + rpk.getName() 
      +  ", InstitutionProfileId ) Values ( " + rpk.getId() + "," 
      + srk.getExpressState().getDealInstitutionId() + ")";
		
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new SAMRequestPK(rpk.getId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("SAMRequest Entity - SAMRequest "
                                               + " - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
	  	return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof SAMRequest)
		{
			SAMRequest oa = (SAMRequest) o;
			if  (  this.requestId == oa.getRequestId ()  )
				return true;
		}
		return false;
	}
    
    private void setPk(SAMRequestPK pk) {
        this.pk = pk;
    }
    
    /**
     * This object should only be cloned with a known new PK.
     */
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    
    public Object clone(RequestPK newPk) {
        
        SAMRequest clonedRequest = null;
        
        try {              
            // first create teh request to generate the new PK
            clonedRequest = new SAMRequest(srk);
            clonedRequest = clonedRequest.create(newPk);
                      
            // call Object's clone() which does a shallow copy
            clonedRequest = (SAMRequest)super.clone();
            
            // restore the pk
            clonedRequest.setPk(new SAMRequestPK(newPk.getId()));
            clonedRequest.setRequestId(newPk.getId());
            
            clonedRequest.doPerformUpdateForce();         
        }
        catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in SAMRequest.clone().  Trying to clone requestid = " + this.requestId + " " + ex.getMessage());
        }
        
        return clonedRequest;        
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
}
