package com.basis100.deal.entity;

import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.sql.Clob;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationApplicantResponsePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>Title: AdjudicationApplicantResponse</p>
 * <p>Description: Entity class to handles all database communication to the
 * AdjudicationApplicantResponse table</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */

public class AdjudicationApplicantResponse
	extends DealEntity
{
	private int responseId;
	private int applicantNumber;
	private int copyId;
	private int borrowerId;
	private int	instProfileId;
	private String applicantCurrentInd;
	private double riskRating;
	private int internalScore;
	private int creditBureauScore;
	public AdjudicationApplicantResponsePK pk;
	private Clob creditBureauReport;

	public static final String COPYID = "copyId";
	public static final String BORROWERID = "borrowerId";
	public static final String APPLICANTCURRENTIND = "applicantCurrentInd";
	public static final String RISKRATING = "riskRating";
	public static final String INTERNALSCORE = "internalScore";
	public static final String CREDITBUREAUSCORE = "creditBureauScore";
	public static final String CREDITBUREAUREPORT = "creditBureauReport";
	public static final String INSTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

	public AdjudicationApplicantResponse()
	{
	}

	public AdjudicationApplicantResponse(SessionResourceKit srk)
		throws RemoteException
	{
		super(srk);
	}

	public int getResponseId()
	{
		return responseId;
	}

	public void setResponseId(int responseId)
	{
		this.testChange(AdjudicationApplicantResponsePK.RESPONSEID, responseId);
		this.responseId = responseId;
	}

	public int getApplicantNumber()
	{
		return applicantNumber;
	}

	public void setApplicantNumber(int applicantNumber)
	{
		this.testChange(AdjudicationApplicantResponsePK.APPLICANTNUMBER,
						applicantNumber);
		this.applicantNumber = applicantNumber;
	}

	public int getCopyId()
	{
		return copyId;
	}

	public void setCopyId(int copyId)
	{
		this.testChange(COPYID, copyId);
		this.copyId = copyId;
	}

	public int getBorrowerId()
	{
		return borrowerId;
	}

	public void setBorrowerId(int borrowerId)
	{
		this.testChange(BORROWERID, borrowerId);
		this.borrowerId = borrowerId;
	}

	public String getApplicantCurrentInd()
	{
		return applicantCurrentInd;
	}

	public void setApplicantCurrentInd(String applicantCurrentInd)
	{
		this.testChange(RISKRATING, riskRating);
		this.applicantCurrentInd = applicantCurrentInd;
	}

	public double getRiskRating()
	{
		return riskRating;
	}

	public void setRiskRating(double riskRating)
	{
		this.testChange(RISKRATING, riskRating);
		this.riskRating = riskRating;
	}

	public int getInternalScore()
	{
		return internalScore;
	}

	public void setInternalScore(int internalScore)
	{
		this.testChange(INTERNALSCORE, internalScore);
		this.internalScore = internalScore;
	}

	public int getCreditBureauScore()
	{
		return creditBureauScore;
	}

	public void setCreditBureauScore(int creditBureauScore)
	{
		this.testChange(CREDITBUREAUSCORE, creditBureauScore);
		this.creditBureauScore = creditBureauScore;
	}

	public Clob getCreditBureauReport()
	{
		return creditBureauReport;
	}

	public void setCreditBureauReport(Clob creditBureauReport)
	{
		this.creditBureauReport = creditBureauReport;
	}

	public void setPk(AdjudicationApplicantResponsePK
					  primaryKey)
	{
		this.pk = primaryKey;
	}

	/**
	 * search for AdjudicationApplicantResponse record using primary key
	 * @param pk AdjudicationApplicantResponsePK Primary key to be searched by
	 * @throws RemoteException Thrown if error on connection
	 * @throws FinderException Thrown if unable to find a matching row
	 * @return AdjudicationApplicantResponse object that matches the primary key given
	 */
	public AdjudicationApplicantResponse findByPrimaryKey(
		AdjudicationApplicantResponsePK
		pk)
		throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from " + getEntityTableName() +
			pk.getWhereClause();

		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg =
					getEntityTableName() + ": @findByPrimaryKey(), key= " + pk +
					", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException(
				getEntityTableName() + " Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}
		this.pk = pk;
		ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
		return this;
	}

	/**
	 * Set all properties of this object using the values that is returned by
	 * the current database query
	 * @param key int The index of the database query result list
	 * @throws Exception Thrown if error
	 */
	public void setPropertiesFromQueryResult(int key)
		throws Exception
	{
		setResponseId(jExec.getInt(key,
								   AdjudicationApplicantResponsePK.
								   RESPONSEID));
		setApplicantNumber(jExec.getInt(key,
										AdjudicationApplicantResponsePK.
										APPLICANTNUMBER));
		setApplicantCurrentInd(jExec.getString(key, APPLICANTCURRENTIND));
		setBorrowerId(jExec.getInt(key, BORROWERID));
		setCopyId(jExec.getInt(key, COPYID));
		setCreditBureauReport(jExec.getClob(key, CREDITBUREAUREPORT));
		setCreditBureauScore(jExec.getInt(key, CREDITBUREAUSCORE));
		setInternalScore(jExec.getInt(key, INTERNALSCORE));
		setRiskRating(jExec.getDouble(key, RISKRATING));
		setInstProfileId(jExec.getInt(key, INSTITUTIONPROFILEID));
	}

	/**
	 * Create and inserts a AdjudicationApplicantResponse into the database
	 * @param rpk AdjudicationApplicantResponse Primary key
	 * @throws RemoteException Thrown if error on connection
	 * @throws CreateException Thrown if unable to create row
	 * @return AdjudicationApplicantResponse The AdjudicationApplicantResponse object has
	 * has been inserted into the database
	 */
	public AdjudicationApplicantResponse create(
		AdjudicationApplicantResponsePK rpk, int copyId, int borrowerId,
		String applicantCurrentInd)
		throws RemoteException, CreateException
	{

		int key = 0;
		// Create BorrowerResaponseAssoc record first
		StringBuffer sqla = new StringBuffer(
			"Insert into BorrowerResponseAssoc( responseId" +
			",copyId, borrowerId,INSTITUTIONPROFILEID) Values ( ");
		sqla.append(sqlStringFrom(rpk.getId()) + ", ");
		sqla.append(sqlStringFrom(copyId) + ", ");
		sqla.append(sqlStringFrom(borrowerId) + ", ");
		sqla.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");

		String sql = new String(sqla);

		try
		{
			key = jExec.executeUpdate(sql);
			this.trackEntityCreate(); // track the creation
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException(
				"AdjudicationApplicant Entity - AdjudicationApplicant - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			throw ce;
		}
		finally
		{
			try
			{
				if (key != 0)
				{
					jExec.closeData(key);
				}
			}
			catch (Exception e)
			{
				logger.error(e);
			}
		}

		key = 0;
		StringBuffer sqlb = new StringBuffer(
			"Insert into " + getEntityTableName() + "( " +
			AdjudicationApplicantResponsePK.RESPONSEID +
			" ," + AdjudicationApplicantResponsePK.APPLICANTNUMBER +
			" ," + COPYID +
			" ," + BORROWERID +
			" ," + APPLICANTCURRENTIND +
			" ," + CREDITBUREAUREPORT +
			" ," + INSTITUTIONPROFILEID +
			" ) VALUES ( ");
		sqlb.append(sqlStringFrom(rpk.getId()) + ", ");
		sqlb.append(sqlStringFrom(rpk.getApplicantNumber()) + ", ");
		sqlb.append(sqlStringFrom(copyId) + ", ");
		sqlb.append(sqlStringFrom(borrowerId) + ", ");
		sqlb.append(sqlStringFrom(applicantCurrentInd) + ", ");
		sqlb.append(" EMPTY_CLOB()" + ", ");
		sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");

		sql = new String(sqlb);

		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new AdjudicationApplicantResponsePK(rpk.getId(),
				rpk.getApplicantNumber()));
			this.trackEntityCreate(); // track the creation
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException(
				getEntityTableName() + " Entity - " + getEntityTableName() +
				" - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);

			throw ce;
		}
		finally
		{
			try
			{
				if (key != 0)
				{
					jExec.closeData(key);
				}
			}
			catch (Exception e)
			{
				logger.error(e);
			}
		}
		srk.setModified(true);
		return this;
	}

	public void saveReport(String report, AdjudicationApplicantResponsePK rpk)
		throws Exception
	{
		boolean isAutoCommit = jExec.getCon().getAutoCommit();
		jExec.getCon().setAutoCommit(false);
		String sql = "Select " + CREDITBUREAUREPORT + " from " +
			getEntityTableName() + rpk.getWhereClause() + " For Update";
		int key = jExec.execute(sql);

		for (; jExec.next(key); )
		{
			// Get the Blob locator and open output stream for the Blob
			Clob reportClob = jExec.getClob(key, CREDITBUREAUREPORT);

			Writer clobWriter = ((oracle.sql.CLOB)reportClob).
				getCharacterOutputStream();

			Reader reportReader = new StringReader(report);

			// Buffer to hold chunks of data to being written to the Clob.
			char[] cbuffer = new char[10 * 1024];

			// Read a chunk of data from the sample file input stream,
			// and write the chunk into the Clob column output stream.
			// Repeat till file has been fully read.
			int nread = 0;
			while ((nread = reportReader.read(cbuffer)) != -1)
			{
				clobWriter.write(cbuffer, 0, nread); // Write to Clob
			}
			// Close both streams
			reportReader.close();
			clobWriter.close();

			break; // can only be one record
		}
		jExec.getCon().setAutoCommit(isAutoCommit);
		jExec.closeData(key);

	}

	@Override
	protected int performUpdate() throws Exception {
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	@Override
	public String getEntityTableName() {
		return "AdjudicationApplicantResponse";
	}

	@Override
	public IEntityBeanPK getPk() {
		return (IEntityBeanPK) this.pk;
	}

	@Override
	public String getStringValue(String fieldName) {
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof AdjudicationApplicantResponse) {
			AdjudicationApplicantResponse oa = (AdjudicationApplicantResponse) o;
			if (this.pk.equals(oa.getPk())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setField(String fieldName, String value) throws Exception {
		doSetField(this, this.getClass(), fieldName, value);
	}

	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}

}
