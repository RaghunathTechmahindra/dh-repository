/**
 * 
 */
package com.basis100.deal.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.FCXLoanAppQueuePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * @author Phil.Yuan
 * This entity class is used to represent entries in FCXLOANAPPQUEUE table.
 */
public class FCXLoanAppQueue extends DealEntity {
	
	protected int fcxLoanAppQueueId;
	protected String senderMessageId;
	protected String senderChannel;
	protected String receiverChannel;
	protected String sourceApplicationId;
	protected String format;
	protected String version;
	protected Date receivedTimeStamp;
	protected String message;
	protected int fcxLoanAppStatusId;
	protected int retryCounter;
	
	protected FCXLoanAppQueuePK pk;
	
	public FCXLoanAppQueue() {
		super();
	}
	public FCXLoanAppQueue(SessionResourceKit srk, CalcMonitor calcMon)
			throws RemoteException {
		super(srk, calcMon);
	}
	public FCXLoanAppQueue(SessionResourceKit srk) throws RemoteException {
		super(srk);
	}
	
	/**
	 * Constructor for creating a new FCXLoanAppQueue object using an existing
	 * id.
	 * @param srk
	 * @param id
	 * @throws RemoteException
	 * @throws FinderException
	 */
    public FCXLoanAppQueue(SessionResourceKit srk, int id)
    throws RemoteException, FinderException {
    	super(srk);
    	pk = new FCXLoanAppQueuePK(id);
    	findByPrimaryKey(pk);
    }
    
    public IEntityBeanPK getPk()
    {
       return this.pk;
    }
    
    /**
     * Inserts an FCXLoanAppQueue into database table.
     * After insertion, 'this' object will contain the automatically generated fcxLoanAppQueueId.
     * @param senderMessageId
     * @param senderChannel
     * @param receiverChannel
     * @param sourceApplicationId
     * @param format
     * @param version
     * @param receivedTimeStamp
     * @param message
     * @param fcxLoanAppStatusId
     * @param retryCounter
     * @return FCXLoanAppQueue 'this' object that contains everything inserted including auto-generated fcxLoanAppQueueId.
     */
    public FCXLoanAppQueue create(	String senderMessageId,
    		String senderChannel,
    		String receiverChannel,
    		String sourceApplicationId,
    		String format,
    		String version,
    		Date receivedTimeStamp,
    		String message,
    		int fcxLoanAppStatusId,
    		int retryCounter ) 
    throws RemoteException, CreateException
    {
    	this.senderMessageId = senderMessageId;
    	this.senderChannel = senderChannel;
    	this.receiverChannel = receiverChannel;
    	this.sourceApplicationId = sourceApplicationId;
    	this.format = format;
    	this.version = version;
    	this.receivedTimeStamp = receivedTimeStamp;
    	this.message = message;
    	this.fcxLoanAppStatusId = fcxLoanAppStatusId;
    	this.retryCounter = retryCounter;

    	this.pk = this.createPrimaryKey();
    	this.fcxLoanAppQueueId = this.pk.getId();

    	StringBuffer sqlb = new StringBuffer ();
    	sqlb.append("INSERT INTO FCXLOANAPPQUEUE( ");
    	sqlb.append(" FCXLOANAPPQUEUEID, ");
    	sqlb.append(" SENDERMESSAGEID, ");
    	sqlb.append(" SENDERCHANNEL, ");
    	sqlb.append(" RECEIVERCHANNEL, ");
    	sqlb.append(" SOURCEAPPLICATIONID, ");
    	sqlb.append(" FORMAT, ");
    	sqlb.append(" VERSION, ");
    	sqlb.append(" RECEIVEDTIMESTAMP, ");
    	sqlb.append(" MESSAGE, ");
    	sqlb.append(" FCXLOANAPPSTATUSID, ");
    	sqlb.append(" RETRYCOUNTER");
    	sqlb.append(" ) VALUES ( "); 
    	sqlb.append(" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ");
    	sqlb.append(" )");
    	
    	PreparedStatement ps = null;
    	try {
    		ps = jExec.getCon().prepareStatement(sqlb.toString());
    		ps.clearParameters();
    		ps.setInt(1, this.pk.getId());

    		ps.setString(2, this.getSenderMessageId()) ;
    		ps.setString(3, this.getSenderChannel()) ;
    		ps.setString(4, this.getReceiverChannel()) ;
    		ps.setString(5, this.getSourceApplicationId()) ;                         
    		ps.setString(6, this.getFormat());
    		ps.setString(7, this.getVersion());
    		ps.setTimestamp(8, new Timestamp(receivedTimeStamp.getTime()));
    		ps.setClob(9, createClob(this.getMessage()));
    		ps.setInt(10, this.getFcxLoanAppStatusId());
    		ps.setInt(11, this.getRetryCounter());

    		ps.executeUpdate();
    		trackEntityCreate(); // track the creation

    	} catch (Exception e) {
    		CreateException ce = new CreateException("FCXLoanAppQueue Entity - create() exception");
    		logger.error(ce.getMessage());
    		logger.error(e);
    		throw ce;
    	} finally {
    		try {
    			if(ps != null) ps.close();
    		} catch (SQLException e) {
    			throw new CreateException(e);
    		}
    	}
    	srk.setModified(true);
    	return this;
    }
    
    /**
     * Generate the next primary key id from db.
     * @return The newly created FCXLoanAppQueuePK.
     * @throws CreateException
     */
    private FCXLoanAppQueuePK createPrimaryKey()throws CreateException
    {
       String sql = "Select FCXLOANAPPQUEUE_SEQ.nextval from dual";
       int id = -1;
       int key = 0;

       try
       {
          key = jExec.execute(sql);
          if(jExec.next(key))
          {
            id = jExec.getInt(key,1);  // can only be one record
          }
          jExec.closeData(key);
          if( id == -1 ) throw new CreateException("Error getting FCXLoanAppQueueId from sequence");
        } catch (Exception e) {
            CreateException ce = new CreateException("Error getting FCXLoanAppQueueId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        } finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch (Exception e) {
                logger.debug(e.toString());
            }
        }
        return new FCXLoanAppQueuePK(id);
    }
    
    /** 
    * Find the FCX Loan Application(s) by status. The result is sorted by TIMERECEIVED 
    * column in ascending order (older items are placed higher in result).
    * @param status the status of the records of interest. The values can be:
	* 0 - Application received and ready for processing
	* 1 - FCX Loan Application is in process
	* 2 - Successfully Processed, Archived Records
	* 3 - Data Power connectivity failure, network error
	* 4 - Other error (placeholder for new error categories)
	* 5 - Data Power 401 Authentication Error
	* 6 - Data Power 403 Authorization Error
	* 7 - Data Power 440 Invalid wsa:action
	* 8 - Data Power 441 Invalid transaction identifier
	* 9 - Data Power 451 Invalid receiver element
	*10 - Data Power 452 Invalid payload
	*11 - Data Power 453 Schema validation Error
	*12	- Data Power 454 XSLT failure
	*13	- Data Power 500 Internal System Error
    * @return Collection of qualified record with status, sorted by date (latest first).
    * @throws FinderException
    */

   public Collection<FCXLoanAppQueue> findByStatus(int status) throws FinderException {

	   Collection<FCXLoanAppQueue> loanApps = new ArrayList<FCXLoanAppQueue>();

       String sql = "select * from FCXLOANAPPQUEUE "
              		+ " where FCXLOANAPPSTATUSID = " + status 
              		+ " order by RECEIVEDTIMESTAMP asc";
      try
      {
           int key = jExec.execute(sql);
           while (jExec.next(key))
           {
        	 FCXLoanAppQueue loanApp = new FCXLoanAppQueue(srk);
             loanApp.setPropertiesFromQueryResult(key);
             loanApps.add(loanApp);
           }
           jExec.closeData(key);
       }
      
       catch (Exception e)
       {
         FinderException fe = new FinderException("Exception caught while getting FCXLoanAppQueue");
         logger.error(fe.getMessage());
         logger.error(e);

         throw fe;
       }

       return loanApps;
   }
   
   /**
    * Finds failed items that has the sourceAppId.
    * The records that meet the following criteria will be selected:
    * 1. whose SOURCEAPPLICATIONID equals sourceAppId
    * 2. whose FCXLOANAPPSTATUSID is not 0 or 3, OR
    * 3. whose FCXLOANAPPSTATUSID is 3 and RETRYCOUNTER >= 5.
    * If no record found, then a Collection<FCXLoanAppQueue> with 0 element is returned.
    * @param sourceAppId
    * @return the collection of records that meet the criteria.
    * @throws FinderException
    */
   public Collection<FCXLoanAppQueue> findFailedBySourceApplicationId(String sourceAppId) throws FinderException {

	   Collection<FCXLoanAppQueue> loanApps = new ArrayList<FCXLoanAppQueue>();
	   
	   if(null == sourceAppId || sourceAppId.trim().length() == 0) return loanApps;

       String sql = "select * from FCXLOANAPPQUEUE "
              		+ " where SOURCEAPPLICATIONID = " + "\'" + sourceAppId.trim() +"\'"  
              		+ " and FCXLOANAPPSTATUSID <> 0 and FCXLOANAPPSTATUSID <> 3" 
              		+ " or (FCXLOANAPPSTATUSID = 3 and RETRYCOUNTER >= 5) ";
      try
      {
           int key = jExec.execute(sql);
           while (jExec.next(key))
           {
        	 FCXLoanAppQueue loanApp = new FCXLoanAppQueue(srk);
             loanApp.setPropertiesFromQueryResult(key);
             loanApps.add(loanApp);
           }
           jExec.closeData(key);
       }
      
       catch (Exception e)
       {
         FinderException fe = new FinderException("Exception caught while getting FCXLoanAppQueue");
         logger.error(fe.getMessage());
         logger.error(e);

         throw fe;
       }

       return loanApps;
   }
   
   /**
    * Finds the next available record based on statusID and maxRetry columns.
    * Qualified record will contain statusID that matches any element in statusIds array.
    * Qualified record should also contain a maxRetry value that is less than the maxRetry being passed in.
    * If more than one record is qualified, the one with oldest (earliest) time stamp is returned.
    * @param statusIds
    * @param maxRetry
    * @return the qualified record.
    * @throws FinderException
    */
	public FCXLoanAppQueue findNextAvailable(int[] statusIds, int maxRetry) throws FinderException {
		
		if(null == statusIds || statusIds.length != 2) {
			throw new FinderException("Incorrect number of statusIds.");
		}
		
		String sql = "select * from (" + 
		"select * from FCXLOANAPPQUEUE where " + 
		"(FCXLOANAPPSTATUSID=" + statusIds[0] + " or FCXLOANAPPSTATUSID=" + statusIds[1] +  
		") and RETRYCOUNTER < " + maxRetry + " order by RECEIVEDTIMESTAMP asc) where rownum=1";

		boolean gotRecord = false;
		try
		{
          int key = jExec.execute(sql);
          
          if(jExec.next(key))
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key); // can only be one record
          }

          jExec.closeData(key);
		}
        catch (Exception e)
        {
        	String message = e.getMessage();
        	if(message.contains("entity not found"))
        		message = "Cannot find next available record";
            FinderException fe = new FinderException("FCXLoanAppQueue Entity - findNextAvailable() exception:" + message);

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

	    if (gotRecord == false)
	    {
	        String msg = "FCXLoanAppQueue Entity: @findNextAvailable(), no qualified entry found";
	        logger.debug(msg);
	        throw new FinderException(msg);
	    }
	    
        return this;
	}
    
    public FCXLoanAppQueue findByPrimaryKey(FCXLoanAppQueuePK pk) throws RemoteException, FinderException
    {
      String sql = "Select * from FCXLOANAPPQUEUE " + pk.getWhereClause();
      boolean gotRecord = false;
      try
      {
          int key = jExec.execute(sql);
          if(jExec.next(key))
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key); // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "FCXLoanAppQueue Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
        	String message = e.getMessage();
        	if(message.contains("entity not found"))
        		message = "Cannot find Entity with primary key " + pk.getId();
            FinderException fe = new FinderException("FCXLoanAppQueue Entity - findByPrimaryKey() exception:" + message);

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        return this;
    }
    
    private void setPropertiesFromQueryResult(int key) throws Exception {
    	this.setFcxLoanAppQueueId(jExec.getInt(key, "FCXLOANAPPQUEUEID"));
    	//Updating the pk reference.
    	this.pk = new FCXLoanAppQueuePK(this.getFcxLoanAppQueueId());
        this.setSenderMessageId(jExec.getString(key, "SENDERMESSAGEID"));
        this.setSenderChannel(jExec.getString(key, "SENDERCHANNEL"));
        this.setReceiverChannel(jExec.getString(key, "RECEIVERCHANNEL"));
        this.setSourceApplicationId(jExec.getString(key, "SOURCEAPPLICATIONID"));
        this.setFormat(jExec.getString(key, "FORMAT"));
        this.setVersion(jExec.getString(key, "VERSION"));
        this.setReceivedTimeStamp(jExec.getDate(key, "RECEIVEDTIMESTAMP"));
        this.setMessage(readClob(jExec.getClob(key, "MESSAGE")));
        this.setFcxLoanAppStatusId(jExec.getInt(key, "FCXLOANAPPSTATUSID"));
        this.setRetryCounter(jExec.getInt(key, "RETRYCOUNTER"));
    }
    
    public String getEntityTableName()
    {
      return "FCXLOANAPPQUEUE";
    }
	
    protected int performUpdate() throws Exception {
		String[] exclude = { "MESSAGE" };
		int ret = doPerformUpdate(exclude);
		forceUpdateDealXml();
		return ret;
	}
    
	private void forceUpdateDealXml() throws RemoteException {
		Connection connection = jExec.getCon();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(
					"UPDATE FCXLOANAPPQUEUE SET MESSAGE=? WHERE FCXLOANAPPQUEUEID=?");
			statement.setClob(1, createClob(this.message));
			statement.setInt(2, this.pk.getId());
			statement.executeUpdate();
		} catch (Exception e) {
			throw new RemoteException(e.getMessage()); 
		} finally {
			try {
				if(statement != null)
				statement.close();
			} catch (SQLException sqle) {
				logger.warn(sqle.getMessage());
			}
		}
	}
	//Getter methods and setter methods.
	public int getFcxLoanAppQueueId() {
		return this.fcxLoanAppQueueId;
	}
	/**
	 * Make sure to also update the pk object after calling this method.
	 * @param loanAppId
	 */
	private void setFcxLoanAppQueueId(int loanAppId) {
		this.testChange ("FCXLOANAPPQUEUEID", loanAppId);
		this.fcxLoanAppQueueId = loanAppId;
	}
	public String getSenderMessageId() {
		return senderMessageId;
	}
	public void setSenderMessageId(String senderMessageId) {
		this.testChange ("SENDERMESSAGEID", senderMessageId);
		this.senderMessageId = senderMessageId;
	}
	public String getSenderChannel() {
		return senderChannel;
	}
	public void setSenderChannel(String senderChannel) {
		this.testChange ("SENDERCHANNEL", senderChannel);
		this.senderChannel = senderChannel;
	}
	public String getReceiverChannel() {
		return receiverChannel;
	}
	public void setReceiverChannel(String receiverChannel) {
		this.testChange ("RECEIVERCHANNEL", receiverChannel);
		this.receiverChannel = receiverChannel;
	}
	public String getSourceApplicationId() {
		return sourceApplicationId;
	}
	public void setSourceApplicationId(String sourceApplicationId) {
		this.testChange ("SOURCEAPPLICATIONID", sourceApplicationId);
		this.sourceApplicationId = sourceApplicationId;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.testChange ("FORMAT", format);
		this.format = format;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.testChange ("VERSION", version);
		this.version = version;
	}
	public Date getReceivedTimeStamp() {
		return receivedTimeStamp;
	}
	public void setReceivedTimeStamp(Date receivedTimeStamp) {
		this.testChange ("RECEIVEDTIMESTAMP", receivedTimeStamp);
		this.receivedTimeStamp = receivedTimeStamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.testChange ("MESSAGE", message);
		this.message = message;
	}
	public int getFcxLoanAppStatusId() {
		return fcxLoanAppStatusId;
	}
	public void setFcxLoanAppStatusId(int statusId) {
		this.testChange ("FCXLOANAPPSTATUSID", statusId);
		this.fcxLoanAppStatusId = statusId;
	}
	public int getRetryCounter() {
		return retryCounter;
	}
	public void setRetryCounter(int retryCounter) {
		this.testChange ("RETRYCOUNTER", retryCounter);
		this.retryCounter = retryCounter;
	}
}
