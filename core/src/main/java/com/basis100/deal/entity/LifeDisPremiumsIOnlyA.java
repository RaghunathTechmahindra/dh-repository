package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.LDInsuranceRatesPK;
import com.basis100.deal.pk.LifeDisPremiumsIOnlyAPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


//--DJ_CR136--23Nov2004--//
// LifeDisPremiums for Insure Only Applicant type of Borrower.
public class LifeDisPremiumsIOnlyA extends DealEntity
{
   protected int      lifeDisPremiumsIOnlyAId;
   protected int      insureOnlyApplicantId;
   protected int      copyId;
   protected int      lenderProfileId;
   protected int      LDInsuranceRateId;
   protected int      LDInsuranceTypeId;
   protected int      disabilityStatusId;
   protected int      lifeStatusId;
   protected int      institutionProfileId;  

   protected LifeDisPremiumsIOnlyAPK pk;

   public LifeDisPremiumsIOnlyA(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

  public LifeDisPremiumsIOnlyA(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
  {
    super(srk,dcm);
  }

  public LifeDisPremiumsIOnlyA(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
  {
    super(srk,dcm);

    pk = new LifeDisPremiumsIOnlyAPK(id, copyId);

    findByPrimaryKey(pk);
  }

   public LifeDisPremiumsIOnlyA findByPrimaryKey(LifeDisPremiumsIOnlyAPK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from LifeDisPremiumsIOnlyA where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key, false);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public List findByLDInsuranceRateId(int id) throws Exception
    {
      List found = new ArrayList();
      StringBuffer sql = new StringBuffer("Select * from LifeDisPremiumsIOnlyA where ");
      sql.append("LDInsuranceRateId = " + id);

      try
      {
          int key = jExec.execute(sql.toString());

          for (; jExec.next(key); )
          {
              LifeDisPremiumsIOnlyA ldprem = new LifeDisPremiumsIOnlyA(srk);
              ldprem.setPropertiesFromQueryResult(key, false);
              found.add(ldprem);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("LifeDisPremiumsIOnlyA Entity - findByPricingProfileId() exception for " + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
   }

  public Collection findByInsureOnlyApplicant(InsureOnlyApplicantPK pk) throws Exception
  {
    Collection lifedispremiumsIOA = new ArrayList();

    String sql = "Select * from LifeDisPremiumsIOnlyA " ;
          sql +=  pk.getWhereClause();

     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           LifeDisPremiumsIOnlyA ldp = new LifeDisPremiumsIOnlyA(srk,dcm);
           ldp.setPropertiesFromQueryResult(key);
           ldp.pk = new LifeDisPremiumsIOnlyAPK(ldp.getLifeDisPremiumsIOnlyAId(),ldp.getCopyId());
           lifedispremiumsIOA.add(ldp);
        }
        jExec.closeData(key);
      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while fetching Borrower::LifeDisPremiumsIOnlyA");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

     return lifedispremiumsIOA;
  }

  public Vector findByMyCopies() throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from LifeDisPremiumsIOnlyA where LifeDisPremiumsIOnlyAId = " +
       getLifeDisPremiumsIOnlyAId() + " AND copyId <> " + getCopyId();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            LifeDisPremiumsIOnlyA iCpy = new LifeDisPremiumsIOnlyA(srk,dcm);

            iCpy.setPropertiesFromQueryResult(key);
            iCpy.pk = new LifeDisPremiumsIOnlyAPK(iCpy.getLifeDisPremiumsIOnlyAId(), iCpy.getCopyId());

            v.addElement(iCpy);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {

        FinderException fe = new FinderException("getLifeDisPremiumsIOnlyAId() - findByMyCopies exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
    return v;
  }

   private void setPropertiesFromQueryResult(int key, boolean deep) throws Exception
   {
      this.setLifeDisPremiumsIOnlyAId (jExec.getInt(key,"LIFEDISPREMIUMSIONLYAID"));
      this.setLDInsuranceRateId (jExec.getInt(key,"LDINSURANCERATEID"));
      this.setInsureOnlyApplicantId (jExec.getInt(key,"INSUREONLYAPPLICANTID"));
      this.setCopyId (jExec.getInt(key,"COPYID"));
      this.setDisabilityStatusId (jExec.getInt(key,"DISABILITYSTATUSID"));
      this.setLifeStatusId (jExec.getInt(key,"LIFESTATUSID"));
      this.setLenderProfileId (jExec.getInt(key,"LENDERPROFILEID"));
      this.setLDInsuranceTypeId (jExec.getInt(key,"LDINSURANCETYPEID"));
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
   }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setLifeDisPremiumsIOnlyAId (jExec.getInt(key,"LIFEDISPREMIUMSIONLYAID"));
      this.setLDInsuranceRateId (jExec.getInt(key,"LDINSURANCERATEID"));
      this.setInsureOnlyApplicantId (jExec.getInt(key,"INSUREONLYAPPLICANTID"));
      this.setCopyId (jExec.getInt(key,"COPYID"));
      this.setDisabilityStatusId (jExec.getInt(key,"DISABILITYSTATUSID"));
      this.setLifeStatusId (jExec.getInt(key,"LIFESTATUSID"));
      this.setLenderProfileId (jExec.getInt(key,"LENDERPROFILEID"));
      this.setLDInsuranceTypeId (jExec.getInt(key,"LDINSURANCETYPEID"));
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
   }

  /**
   *  gets the value of instace fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

   /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }

  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate() throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
   }

   public String getEntityTableName()
   {
      return "LifeDisPremiumsIOnlyA";
   }

   public int getClassId()
   {
     return ClassId.LIFEDISPREMIUMSIONLYA;
   }

   public boolean isAuditable()
   {
     return true;
   }


		/**
     *   gets the pk associated with this entity
     *
     *   @return a LifeDisPremiumsIOnlyAPK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

  /**
   * Should be changed to create the entity with all not nullable fields.
   * creates a LifeDisPremiumsIOnlyA using the LifeDisPremiumsIOnlyAId with mininimum fields
   *
   */
  public LifeDisPremiumsIOnlyA create(LifeDisPremiumsIOnlyAPK pk)throws RemoteException, CreateException
  {
      String sql = "Insert into LifeDisPremiumsIOnlyA(" + pk.getName() + ", copyID, INSTITUTIONPROFILEID ) Values ( " + pk.getId()+ "," + 
          pk.getCopyId() + "," + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("LifeDisPremiumsIOnlyA Entity - LifeDisPremiumsIOnlyA - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      this.pk = pk;

      return this;
    }

  /**
   * creates a LifeDisPremiumsIOnlyA using the LifeDisPremiumsIOnlyA primary keys and a borrower primary key
   *  - the mininimum (ie.non-null) fields
   *
   */
    public LifeDisPremiumsIOnlyA create(InsureOnlyApplicantPK ioapk, int lifeDisabilityInsTypeId)
      throws RemoteException, CreateException
    {
       pk = createPrimaryKey(ioapk.getCopyId());

       //// 1. Since DJ took a decision not to provide/update their data to populate
       //// the appropriate tables this method is currently hardcoded.

       //// 2. IMPORTANT:
       //// The rateId should be picked up (or passed via RMI) based on all
       //// dimentional tables such as: LDInsuranceAgeRanges, and LDInitialLoanRanges, etc.
       //// The suggested db addition for Life&Disability Insurance module allows
       //// either independant storage of all related data with BXP cals engine or keep track
       //// of data marshalling between BXP and external legacy system (DJ) via RMI, e.g.

       //// 3. All this strategy should be designed (and other entities finished) after getting
       //// at least initial documentation from DJ. It may take an update of the current Life/Disability db
       //// schema design addition. This is not a final Entity AT ALL in terms of methods, etc.

       //// 4. Add appropriate population from db and/or from DJ RMI process when
       //// the specs will be finalized!!!

       //// Currently hardcoded just to create entities for Deal Enty deal creation.
       int ldRateId = 1;   // Default for disability
       int lenderProfileId = 0;

       if (lifeDisabilityInsTypeId == 1)
          ldRateId = 26;
       else if (lifeDisabilityInsTypeId == 2)
          ldRateId =1;

logger.debug("LDP@CreateInsureOnlyApplicantPK::StampForType: " + lifeDisabilityInsTypeId);
logger.debug("LDP@CreateInsureOnlyApplicantPK::IOAID: " + ioapk.getId());
logger.debug("LDP@CreateInsureOnlyApplicantPK::IOACID: " + ioapk.getCopyId());


       String sql = "Insert into LifeDisPremiumsIOnlyA(LifeDisPremiumsIOnlyAId, InsureOnlyApplicantId, copyId, " +
                    "LDInsuranceRateId, LenderProfileId, LDInsuranceTypeId, INSTITUTIONPROFILEID ) Values ( " +
                    pk.getId() + "," + ioapk.getId() + "," + ioapk.getCopyId() + "," + ldRateId + "," +
                    lenderProfileId + "," + lifeDisabilityInsTypeId + ", " + srk.getExpressState().getDealInstitutionId() + ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("LifeDisPremiumsIOnlyA Entity - LifeDisPremiumsIOnlyA - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

       srk.setModified(true);
       return this;
    }



   private LifeDisPremiumsIOnlyAPK createPrimaryKey(int copyId)throws CreateException
   {
     String sql = "Select LifeDisPremiumsIOnlyASeq.nextval from dual";

     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }

         jExec.closeData(key);

         if( id == -1 ) throw new Exception();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("LifeDisPremiumsIOnlyA Entity create() exception getting LifeDisPremiumsIOnlyAId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
      }

      return new LifeDisPremiumsIOnlyAPK(id, copyId);
  }

  /**
   * creates a LifeDisPremiumsIOnlyA using the LifeDisPremiumsIOnlyAId with mininimum fields
   *
   */
  public LifeDisPremiumsIOnlyA create(LifeDisPremiumsIOnlyAPK pk, LDInsuranceRatesPK ppk)throws RemoteException, CreateException
  {
      String sql = "Insert into LifeDisPremiumsIOnlyA( LifeDisPremiumsIOnlyAID, LDInsuranceRatesId, INSTITUTIONPROFILEID ) Values ( " +
                         pk.getId() + ", " + ppk.getId() + ", " + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityChange();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("LifeDisPremiumsIOnlyA Entity - LifeDisPremiumsIOnlyA - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
      srk.setModified(true);
      return this;
    }

   //// Getters
	 public int getLifeDisPremiumsIOnlyAId()   {     return this.lifeDisPremiumsIOnlyAId;   }
   public int getInsureOnlyApplicantId()   {    return this.insureOnlyApplicantId;   }
   public int getCopyId()   {    return this.copyId;   }
   public int getLenderProfileId()   {    return this.lenderProfileId;   }
   public int getLDInsuranceRateId()   {    return this.LDInsuranceRateId;   }
   public int getLDInsuranceTypeId()   {    return this.LDInsuranceTypeId;   }
   public int getDisabilityStatusId()   {    return this.disabilityStatusId;   }
   public int getLifeStatusId()   {    return this.lifeStatusId;   }

   //// Setters
   public void setLifeDisPremiumsIOnlyAId(int val)
	 {
		 this.testChange("lifeDisPremiumsIOnlyAId", val);
		 this.lifeDisPremiumsIOnlyAId = val;
   }

   public void setInsureOnlyApplicantId(int val)
	 {
		 this.testChange("insureOnlyApplicantId", val);
		 this.insureOnlyApplicantId = val;
   }

   public void setCopyId(int val)
	 {
		 this.testChange("copyId", val);
		 this.copyId = val;
   }

   public void setLenderProfileId(int val)
	 {
		 this.testChange("lenderProfileId", val);
		 this.lenderProfileId = val;
   }

   public void setLDInsuranceRateId(int val)
	 {
		 this.testChange("LDInsuranceRateId", val);
		 this.LDInsuranceRateId = val;
   }

   public void setLDInsuranceTypeId(int val)
	 {
		 this.testChange("LDInsuranceTypeId", val);
		 this.LDInsuranceTypeId = val;
   }

   public void setDisabilityStatusId(int val)
	 {
		 this.testChange("disabilityStatusId", val);
		 this.disabilityStatusId = val;
   }

   public void setLifeStatusId(int val)
	 {
		 this.testChange("lifeStatusId", val);
		 this.lifeStatusId = val;
   }
   public void setInstitutionProfileId(int institutionProfileId) {
       testChange("institutionProfileId", institutionProfileId);
       this.institutionProfileId = institutionProfileId;
   }

   public int getInstitutionProfileId() {
       return institutionProfileId;
   }

}
