package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ScotiabankAdditionalDetailsPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class ScotiabankAdditionalDetails extends DealEntity {
	protected int           dealId;
	protected int	        copyId;
	protected Date			approvalDate;
	protected int			scotiabankApplicationNumber;
	protected String		branchName;
	protected int			servicingBranchNumber;
	protected int			standaloneMortgageNumber;
	protected int			standaloneSCLNumber;
	protected String		healthCrisisInsuranceStatus;
	protected String		scotiaLifeInsuranceStatus;
	protected int			bankCode;
	protected double		postedRate;
	protected int			transitNumber;
	protected double		committedRate;
	protected int			bankAccount;
	protected int			amortization;
	protected double		findersFeeTotal;
	protected Date			termStartDate;
	protected double		rateBuydown;
	protected double		refinanceNewMoney;
	protected double		rateBuydownAmount;
	protected String		switchCode;
	protected double		cashbackPercentage;
	protected int			existingAccountNumber;
	protected double		cashbackAmount;
	protected int			rateBuydownPayorId;
	protected String		mdm;
	protected double		holdbackAmount;
	protected String		otherMortgageDeductions;
	protected String		ppDescription;
	protected String		otherDeductionsDescription;
	protected String		sfDescription;
	protected String		otherDescription;
    protected int         institutionProfileId;

	private ScotiabankAdditionalDetailsPK pk;

	public ScotiabankAdditionalDetails(SessionResourceKit srk) 
    throws RemoteException
	{
		super(srk);
	}

	public ScotiabankAdditionalDetails(SessionResourceKit srk, int id,int copyid) throws RemoteException, FinderException
	{
		super(srk);
	    pk = new ScotiabankAdditionalDetailsPK(id,copyid);
	    findByPrimaryKey(pk);
	}

	/**  search for ScotiabankAdditionalDetails record using primary key
	*   @return a ScotiabankAdditionalDetails */
	public ScotiabankAdditionalDetails findByPrimaryKey(ScotiabankAdditionalDetailsPK pk) throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from ScotiabankAdditionalDetails " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "ScotiabankAdditionalDetails: @findByPrimaryKey(), key= " + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException("ScotiabankAdditionalDetails Entity - findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}


    /**  sets properties for ScotiabankAdditionalDetails */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	  	this.setDealId(jExec.getInt(key, "dealId"));
	  	this.setCopyId(jExec.getInt(key, "copyId"));
	  	this.setApprovalDate(jExec.getDate(key, "approvalDate"));
	  	this.setScotiabankApplicationNumber(jExec.getInt(key, "scotiabankApplicationNumber"));
	  	this.setBranchName(jExec.getString(key, "branchName"));
	  	this.setServicingBranchNumber(jExec.getInt(key, "servicingBranchNumber"));
	  	this.setStandaloneMortgageNumber(jExec.getInt(key, "standaloneMortgageNumber"));
	  	this.setStandaloneSCLNumber(jExec.getInt(key, "standaloneSCLNumber"));
	  	this.setHealthCrisisInsuranceStatus(jExec.getString(key, "healthCrisisInsuranceStatus"));
	  	this.setScotiaLifeInsuranceStatus(jExec.getString(key, "scotiaLifeInsuranceStatus"));
	  	this.setBankCode(jExec.getInt(key, "bankCode"));
	  	this.setPostedRate(jExec.getDouble(key, "postedRate"));
	  	this.setTransitNumber(jExec.getInt(key, "transitNumber"));
	  	this.setCommittedRate(jExec.getDouble(key, "committedRate"));
	  	this.setBankAccount(jExec.getInt(key, "bankAccount"));
	  	this.setAmortization(jExec.getInt(key, "amortization"));
	  	this.setFindersFeeTotal(jExec.getDouble(key, "findersFeeTotal"));
	  	this.setTermStartDate(jExec.getDate(key, "termStartDate"));
	  	this.setRateBuydown(jExec.getDouble(key, "rateBuydown"));
	  	this.setRefinanceNewMoney(jExec.getDouble(key, "refinanceNewMoney"));
	  	this.setRateBuydownAmount(jExec.getDouble(key, "rateBuydownAmount"));
	  	this.setSwitchCode(jExec.getString(key, "switchCode"));
	  	this.setCashbackPercentage(jExec.getDouble(key, "cashbackPercentage"));
	  	this.setExistingAccountNumber(jExec.getInt(key, "existingAccountNumber"));
	  	this.setCashbackAmount(jExec.getDouble(key, "cashbackAmount"));
	  	this.setRateBuydownPayorId(jExec.getInt(key, "rateBuydownPayorId"));
	  	this.setMDM(jExec.getString(key, "mdm"));
	  	this.setHoldbackAmount(jExec.getDouble(key, "holdbackAmount"));
	  	this.setOtherMortgageDeductions(jExec.getString(key, "otherMortgageDeductions"));
	  	this.setPPDescription(jExec.getString(key, "ppDescription"));
	  	this.setOtherDeductionsDescription(jExec.getString(key, "otherDeductionsDescription"));
	  	this.setSFDescription(jExec.getString(key, "sfDescription"));
	  	this.setOtherDescription(jExec.getString(key, "otherDescription"));
        this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));

	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	public String getEntityTableName()
	{
	    return "ScotiabankAdditionalDetails"; 
	}

	/**  gets the pk associated with this entity
	 *   @return a ScotiabankAdditionalDetailsPK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}	

	public int getDealId()
	{ 
		return this.dealId;
	}

	public void setDealId(int id)
	{
		this.testChange ("dealId", id);
		this.dealId = id;
	}

	public int getCopyId()
	{ 
		return this.copyId;
	}

	public void setCopyId(int id)
	{
		this.testChange ("copyId", id);
		this.copyId = id;
	}

	public Date getApprovalDate()
	{
		return this.approvalDate;
	}

	public void setApprovalDate(Date d)
	{
		this.testChange ("approvalDate", d);
		this.approvalDate = d;
	}
	
	public int getScotiabankApplicationNumber()
	{
		return this.scotiabankApplicationNumber;
	}

	public void setScotiabankApplicationNumber(int num)
	{
		this.testChange ("scotiabankApplicationNumber", num);
		this.scotiabankApplicationNumber = num;
	}
	
	public String getBranchName()
	{
		return this.branchName;
	}

	public void setBranchName(String str)
	{
		this.testChange ("branchName", str);
		this.branchName = str;
	}

	public int getServicingBranchNumber()
	{
		return this.servicingBranchNumber;
	}

	public void setServicingBranchNumber(int num)
	{
		this.testChange ("servicingBranchNumber", num);
		this.servicingBranchNumber = num;
	}

	public int getStandaloneMortgageNumber()
	{
		return this.standaloneMortgageNumber;
	}

	public void setStandaloneMortgageNumber(int num)
	{
		this.testChange ("standaloneMortgageNumber", num);
		this.standaloneMortgageNumber = num;
	}

	public int getStandaloneSCLNumber()
	{
		return this.standaloneSCLNumber;
	}

	public void setStandaloneSCLNumber(int num)
	{
		this.testChange ("standaloneSCLNumber", num);
		this.standaloneSCLNumber = num;
	}

	public String getHealthCrisisInsuranceStatus()
	{
		return this.healthCrisisInsuranceStatus;
	}

	public void setHealthCrisisInsuranceStatus(String str)
	{
		this.testChange ("healthCrisisInsuranceStatus", str);
		this.healthCrisisInsuranceStatus = str;
	}

	public String getScotiaLifeInsuranceStatus()
	{
		return this.scotiaLifeInsuranceStatus;
	}

	public void setScotiaLifeInsuranceStatus(String str)
	{
		this.testChange ("scotiaLifeInsuranceStatus", str);
		this.scotiaLifeInsuranceStatus = str;
	}

	public int getBankCode()
	{
		return this.bankCode;
	}

	public void setBankCode(int num)
	{
		this.testChange ("bankCode", num);
		this.bankCode = num;
	}

	public double getPostedRate()
	{
		return this.postedRate;
	}

	public void setPostedRate(double rate)
	{
		this.testChange ("postedRate", rate);
		this.postedRate = rate;
	}
	
	public int getTransitNumber()
	{
		return this.transitNumber;
	}

	public void setTransitNumber(int num)
	{
		this.testChange ("transitNumber", num);
		this.transitNumber = num;
	}

	public double getCommittedRate()
	{
		return this.committedRate;
	}

	public void setCommittedRate(double rate)
	{
		this.testChange ("committedRate", rate);
		this.committedRate = rate;
	}

	public int getBankAccount()
	{
		return this.bankAccount;
	}

	public void setBankAccount(int num)
	{
		this.testChange ("bankAccount", num);
		this.bankAccount = num;
	}

	public int getAmortization()
	{
		return this.amortization;
	}

	public void setAmortization(int num)
	{
		this.testChange ("amortization", num);
		this.amortization = num;
	}

	public double getFindersFeeTotal()
	{
		return this.findersFeeTotal;
	}

	public void setFindersFeeTotal(double amt)
	{
		this.testChange ("findersFeeTotal", amt);
		this.findersFeeTotal = amt;
	}

	public Date getTermStartDate()
	{
		return this.termStartDate;
	}

	public void setTermStartDate(Date d)
	{
		this.testChange ("termStartDate", d);
		this.termStartDate = d;
	}

	public double getRateBuydown()
	{
		return this.rateBuydown;
	}

	public void setRateBuydown(double rate)
	{
		this.testChange ("rateBuydown", rate);
		this.rateBuydown = rate;
	}

	public double getRefinanceNewMoney()
	{
		return this.refinanceNewMoney;
	}

	public void setRefinanceNewMoney(double amt)
	{
		this.testChange ("refinanceNewMoney", amt);
		this.refinanceNewMoney = amt;
	}

	public double getRateBuydownAmount()
	{
		return this.rateBuydownAmount;
	}

	public void setRateBuydownAmount(double amt)
	{
		this.testChange ("rateBuydownAmount", amt);
		this.rateBuydownAmount = amt;
	}

	public String getSwitchCode()
	{
		return this.switchCode;
	}

	public void setSwitchCode(String str)
	{
		this.testChange ("switchCode", str);
		this.switchCode = str;
	}
	
	public double getCashbackPercentage()
	{
		return this.cashbackPercentage;
	}

	public void setCashbackPercentage(double prcnt)
	{
		this.testChange ("cashbackPercentage", prcnt);
		this.cashbackPercentage = prcnt;
	}

	public int getExistingAccountNumber()
	{
		return this.existingAccountNumber;
	}

	public void setExistingAccountNumber(int num)
	{
		this.testChange ("existingAccountNumber", num);
		this.existingAccountNumber = num;
	}

	public double getCashbackAmount()
	{
		return this.cashbackAmount;
	}

	public void setCashbackAmount(double amt)
	{
		this.testChange ("cashbackAmount", amt);
		this.cashbackAmount = amt;
	}

	public int getRateBuydownPayorId()
	{
		return this.rateBuydownPayorId;
	}

	public void setRateBuydownPayorId(int id)
	{
		this.testChange ("rateBuydownPayorId", id);
		this.rateBuydownPayorId = id;
	}

	public String getMDM()
	{
		return this.mdm;
	}

	public void setMDM(String str)
	{
		this.testChange ("mdm", str);
		this.mdm = str;
	}

	public double getHoldbackAmount()
	{
		return this.holdbackAmount;
	}

	public void setHoldbackAmount(double amt)
	{
		this.testChange ("holdbackAmount", amt);
		this.holdbackAmount = amt;
	}

	public String getOtherMortgageDeductions()
	{
		return this.otherMortgageDeductions;
	}

	public void setOtherMortgageDeductions(String str)
	{
		this.testChange ("otherMortgageDeductions", str);
		this.otherMortgageDeductions = str;
	}

	public String getPPDescription()
	{
		return this.ppDescription;
	}

	public void setPPDescription(String str)
	{
		this.testChange ("ppDescription", str);
		this.ppDescription = str;
	}

	public String getOtherDeductionsDescription()
	{
		return this.otherDeductionsDescription;
	}

	public void setOtherDeductionsDescription(String str)
	{
		this.testChange ("otherDeductionsDescription", str);
		this.otherDeductionsDescription = str;
	}

	public String getSFDescription()
	{
		return this.sfDescription;
	}

	public void setSFDescription(String str)
	{
		this.testChange ("sfDescription", str);
		this.sfDescription = str;
	}

	public String getOtherDescription()
	{
		return this.otherDescription;
	}

	public void setOtherDescription(String str)
	{
		this.testChange ("otherDescription", str);
		this.otherDescription = str;
	}

	/**  creates ScotiabankAdditionalDetails record into database
	 *   @return a ScotiabankAdditionalDetails */
	public ScotiabankAdditionalDetails create(DealPK dpk, int payor)throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into ScotiabankAdditionalDetails(" + dpk.getName() 
      + "," + "copyId, rateBuydownPayorId, institutionProfileId) Values "
      + "( " + dpk.getId() + "," + dpk.getCopyId() + "," + payor + ", " 
      + srk.getExpressState().getDealInstitutionId() + ")";
		
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new ScotiabankAdditionalDetailsPK(dpk.getId(),dpk.getCopyId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("ScotiabankAdditionalDetails Entity - ScotiabankAdditionalDetails - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}
		srk.setModified(true);
		return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof ScotiabankAdditionalDetails)
		{
			ScotiabankAdditionalDetails oa = (ScotiabankAdditionalDetails) o;
	        if  ( ( dealId  == oa.getDealId () )
	                && ( copyId  ==  oa.getCopyId ()  )  )
				return true;
		}
		return false;
	}


    public Collection findByDeal(DealPK pk) throws Exception
    {
    	int key = 0;
    	Collection sads = new ArrayList();

    	String sql = "Select * from ScotiabankAdditionalDetails" ;
			sql +=  " where dealId = " + pk.getId();
			sql +=  " where copyId = " + pk.getCopyId();
      try
      {

            key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              ScotiabankAdditionalDetails sad = new ScotiabankAdditionalDetails(srk,pk.getId(),pk.getCopyId());
              sad.setPropertiesFromQueryResult(key);
              sad.pk = new ScotiabankAdditionalDetailsPK(sad.getDealId(),sad.getCopyId());
              sads.add(sad);
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::ScotiabankAdditionalDetails");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}
        return sads;
    }
    
    private void setPk(ScotiabankAdditionalDetailsPK pk) {
      this.pk = pk;
    }
    
    public Object clone() throws CloneNotSupportedException {
      throw new CloneNotSupportedException();
    }
    
    /**
     * Create a clone of this Scotiabank additional details entity. The clone is
     * identical but for the dealPk, and its members the deal ID and copy ID, 
     * which must be provided.
     * 
     * @param dealPk Deal for which the clone will be created.
     * @return The cloned Scotiabank additional details entity.
     * @throws CloneNotSupportedException
     */
    public Object clone(DealPK dealPk) throws CloneNotSupportedException {
    	ScotiabankAdditionalDetails clone = null;
    	
    	try {
    		clone = new ScotiabankAdditionalDetails(srk);
    		clone.create(dealPk, this.getRateBuydownPayorId());
        
        // copy other fields
        clone = (ScotiabankAdditionalDetails) super.clone();
        
        // restore PK
        clone.setDealId(dealPk.getId());
        clone.setCopyId(dealPk.getCopyId());
        clone.setPk(new ScotiabankAdditionalDetailsPK(dealPk.getId(), dealPk.getCopyId()));
        
        // persist entity
        clone.doPerformUpdateForce();
        
      } catch (Exception e) {          
        e.printStackTrace();
        logger.error("Error in ScotiabankAdditionalDetails.clone(): could not " +
            "clone SAD entity with dealId: " + dealId + " and copyId: " + copyId);
      }
      
    	return clone;
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
       this.institutionProfileId = institutionProfileId;
    }

}
