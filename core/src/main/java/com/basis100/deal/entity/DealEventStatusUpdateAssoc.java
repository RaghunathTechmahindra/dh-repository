/**
 * <p>Title: DealEventSttusUpdateAssoc.java</p>
 *
 * <p>Description: entity class for DealEventSttusUpdateAssoc</p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 30-Apr-09
 *
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class DealEventStatusUpdateAssoc extends DealEntity {

    protected int institutionProfileId;
    protected int systemtypeId;
    protected int dealEventStatusTypeId;
    protected int statusId;
    protected int statusNameReplacementId;
    private DealEventStatusUpdateAssocPK pk;
    
    /**
     * @param srk SessionResourceKit to use
     * @throws RemoteException
     */    
    public DealEventStatusUpdateAssoc (SessionResourceKit srk) throws RemoteException {
        super(srk);
    }
    
    /**
     * 
     * @param srk SessionResourceKit to us
     * @param id DealEventStatusSnapshot id
     * @throws RemoteException
     */
    public DealEventStatusUpdateAssoc (SessionResourceKit srk, int systemTypeId, int dealEventStatusTypeId, int statusId) throws RemoteException {
        super(srk);
        this.systemtypeId =  systemTypeId;
        this.dealEventStatusTypeId = dealEventStatusTypeId;
        this.statusId = statusId;
    }
    
    /**
     * Find the row corresponding to this PK and populate entity
     * @param pk The primary key represetning this row
     * @return This (now populated) entity
     */
    public DealEventStatusUpdateAssoc findByPrimaryKey(int systemTypeId, int dealEventStatusTypeId, int statusId) 
        throws RemoteException, FinderException {
        
        
        DealEventStatusUpdateAssocPK pk = new DealEventStatusUpdateAssocPK(systemTypeId, dealEventStatusTypeId, statusId);
        return findByPrimaryKey(pk);
    }
    
    public DealEventStatusUpdateAssoc findByPrimaryKey(DealEventStatusUpdateAssocPK pk) 
    throws RemoteException, FinderException {
    
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "SELECT * FROM DEALEVENTSTATUSUPDATEASSOC where SYSTEMTYPEID = " + pk.getSystemTypeId() 
            + " and DEALEVENTSTATUSTYPEID = " + pk.getDealEventStatusTypeId() + " and STATUSID = " + pk.getStatusId();
        boolean gotRecord = false;
        
        try {
            int key = jExec.execute(sql);
            for(;jExec.next(key);) {
                gotRecord = true;
                this.pk = pk;
                setPropertiesFromQueryResult(key);
                //break;
            }
            
            jExec.closeData(key);
            
            if(!gotRecord) {
                String msg = "DealEventSttusUpdateAssoc Entity: @findByQuery(), systemtypeId= " 
                    + pk.getSystemTypeId() + ", dealEvenetStatusId = " + pk.getDealEventStatusTypeId() + " entity not found";
                if (getSilentMode() == false)
                    logger.error(msg);
                
                throw new FinderException(msg);
            }
        } catch(Exception e) {
            
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("DEALEVENTSTATUSSNAPSHOT findByPrimaryKey(): exception: ");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * Find the row corresponding to the SystemType and populate entity
     */
    public Collection<DealEventStatusUpdateAssoc> findBySystemType(int systemtypeid) 
        throws RemoteException, FinderException {
            
        Collection<DealEventStatusUpdateAssoc> resultSet = new ArrayList<DealEventStatusUpdateAssoc>();
        
        String sql = "SELECT * FROM DealEventStatusUpdateAssoc" ;
        sql += " WHERE SYSTEMTYPEID = " + systemtypeid  ;
        
        try {
            int key = jExec.execute(sql);
            for(; jExec.next(key);) {
                DealEventStatusUpdateAssoc result = new DealEventStatusUpdateAssoc(srk);
                result.setPropertiesFromQueryResult(key);
                result.pk = new DealEventStatusUpdateAssocPK(result.getSystemtypeId(), 
                        result.getDealEventStatusTypeId(), result.getStatusId());
                resultSet.add(result);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException("DealEventStatusUpdateAssoc Entity - findBySystemType() exception");
            logger.error(fe.getMessage());            
            logger.error(e);
            
            throw fe;
        }
        return resultSet;
        }
    
    private void setPropertiesFromQueryResult(int key) throws Exception {
        setSystemtypeId(jExec.getInt(key, "SYSTEMTYPEID"));
        setDealEventStatusTypeId(jExec.getInt(key, "DEALEVENTSTATUSTYPEID"));
        setStatusId(jExec.getInt(key, "STATUSID"));
        setStatusNameReplacementId(jExec.getInt(key, "STATUSNAMEREPLACEMENTID"));
    }
    
    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }
    public int getSystemtypeId() {
        return systemtypeId;
    }
    public void setSystemtypeId(int systemtypeId) {
        this.systemtypeId = systemtypeId;
    }
    public int getDealEventStatusTypeId() {
        return dealEventStatusTypeId;
    }
    public void setDealEventStatusTypeId(int dealEventStatusTypeId) {
        this.dealEventStatusTypeId = dealEventStatusTypeId;
    }
    public int getStatusId() {
        return statusId;
    }
    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
    public int getStatusNameReplacementId() {
        return statusNameReplacementId;
    }
    public void setStatusNameReplacementId(int statusNameReplacementId) {
        this.statusNameReplacementId = statusNameReplacementId;
    }
    
    
}
