package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.LDInsuranceRatesPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

//--DJ_LDI_CR--//
//// The creation of this entity is not finished. It should be done after DJ or
//// any other clients gives specifications.

public class LDInsuranceRates extends DealEntity
{
   protected int      LDInsuranceRateId; //PK
   protected String   rateCode;
   protected Date     rateStartDate;
   protected Date     rateEndDate;
   protected Date     rateDisabledDate;
   protected Date     rateTimeStamp;
   protected double   ldRate;
   protected String   rateDescription;
   protected int      initialRangeId;
   protected int      insuranceRateStatusId;
   protected int      insuranceAgeId;
   protected int      insuranceTypeId; //PK
   protected int      smokeStatusId;
   protected int      institutionProfileId;
   
   protected LDInsuranceRatesPK pk;

   public LDInsuranceRates(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

   public LDInsuranceRates(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new LDInsuranceRatesPK(id);

      findByPrimaryKey(pk);
   }

    public LDInsuranceRates findByPrimaryKey(LDInsuranceRatesPK pk) throws RemoteException, FinderException
   {
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from LDInsuranceRates where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public List findByRateCode(String rateCode ) throws Exception
   {
      List found = new ArrayList();
      StringBuffer sql = new StringBuffer("Select * from LDInsuranceRates where ");
      sql.append("LDRateCode = \'" + rateCode.trim () + "\'");

      try
      {
          int key = jExec.execute(sql.toString());

          for (; jExec.next(key); )
          {
              LDInsuranceRates ldir = new LDInsuranceRates(srk);
              ldir.setPropertiesFromQueryResult(key);
              ldir.pk = new LDInsuranceRatesPK(ldir.getLDInsuranceRateId());
              found.add(ldir);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("LDInsuranceRates Entity - findByRateCode() exception for " + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
    }
    // End of Find by Rate Code

    public List findByRateCodeDescription(String rateCodeDescription ) throws Exception
    {
      List found = new ArrayList();
      StringBuffer sql = new StringBuffer("Select * from LDInsuranceRates where ");
      sql.append("LDRATEDESC = \'" + rateCodeDescription.trim () + "\'");

      try
      {
          int key = jExec.execute(sql.toString());

          for (; jExec.next(key); )
          {
              LDInsuranceRates ldir = new LDInsuranceRates(srk);
              ldir.setPropertiesFromQueryResult(key);
              ldir.pk = new LDInsuranceRatesPK(ldir.getLDInsuranceRateId());
              found.add(ldir);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("LDInsuranceRates Entity - findByRateCodeDescription() exception for " + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
    }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setLDInsuranceRateId(jExec.getInt(key,"LDINSURANCERATEID"));
      this.setLDRateCode(jExec.getString(key,"LDRATECODE"));
      this.setLDRateStartDate(jExec.getDate(key,"LDRATESTARTDATE"));
      this.setLDRateEndDate(jExec.getDate(key,"LDRATEENDDATE"));
      this.setLDRateDisabledDate(jExec.getDate(key,"LDRATEDISABLEDDATE"));
      this.setLDRateTimeStamp(jExec.getDate(key,"LDRATETIMESTAMP"));
      this.setLDRate(jExec.getDouble(key,"LDRATE"));
      this.setLDRateDescription(jExec.getString(key,"LDRATEDESC"));
      this.setInitialRangeId(jExec.getInt(key,"LDINITIALLOANRANGEID"));
      this.setInsuranceRateStatusId(jExec.getInt(key,"LDINSURANCERATESTATUSID"));
      this.setInsuranceAgeId(jExec.getInt(key,"LDINSURANCEAGEID"));
      this.setInsuranceTypeId(jExec.getInt(key,"LDINSURANCETYPEID"));
      this.setSmokeStatusId(jExec.getInt(key,"SMOKESTATUSID"));
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));


   }

  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

   /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }

  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return the number of updates performed
  */
  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();

    return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "LDInsuranceRates";
   }

  /**
   *   gets the pk associated with this entity
   *
   *   @return a LDInsuranceRatesPK
   */
   public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

   public LDInsuranceRatesPK createPrimaryKey()throws CreateException
   {
       String sql = "Select LDInsuranceRatesseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }

           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("LDInsuranceRates Entity create() exception getting LDInsuranceRateId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new LDInsuranceRatesPK(id);
  }

  /**
   * creates a LDInsuranceRates using the LDInsuranceRateId with mininimum fields
   *
   */

  public LDInsuranceRates create(LDInsuranceRatesPK pk)throws RemoteException, CreateException
  {
        String sql = "Insert into LDInsuranceRates(" + pk.getName() + " ) Values ( " + pk.getId() + ")";

        try
        {
          jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("LDInsuranceRates Entity - LDInsuranceRates - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

        this.pk = pk;
        srk.setModified(true);
        return this;
    }

   //// "Getters" and "Setters"
   public int getLDInsuranceRateId(){ return this.LDInsuranceRateId; }
   public String getLDRateCode(){ return this.rateCode; }
   public  Date     getLDRateStartDate(){return this.rateDisabledDate; }
   public  Date     getLDRateEndDate(){return this.rateDisabledDate; }
   public  Date     getLDRateDisabledDate(){return this.rateDisabledDate; }
   public  Date     getLDRateTimeStamp(){return this.rateDisabledDate; }
   public double getLDRate(){ return this.ldRate; }
   public String getLDRateDescription(){ return this.rateDescription; }
   public  int     getInitialRangeId(){return this.initialRangeId; }
   public  int     getInsuranceRateStatusId(){return this.insuranceRateStatusId; }
   public  int     getInsuranceAgeId(){return this.insuranceAgeId; }
   public  int     getInsuranceTypeId(){return this.insuranceTypeId; }
   public  int     getSmokeStatusId(){return this.smokeStatusId; }


   public void setLDInsuranceRateId(int value)
	 {
		 this.testChange("LDInsuranceRateId", value);
		 this.LDInsuranceRateId = value;
   }

   public void setLDRateCode(String value )
	 {
		 this.testChange("rateCode", value);
		 this.rateCode = value;
   }

   public void setLDRateStartDate(Date value)
   {
     this.testChange("rateStartDate", value);
     this.rateStartDate = value;
   }

   public void setLDRateEndDate(Date value)
   {
     this.testChange("rateEndDate", value);
     this.rateEndDate = value;
   }

   public void setLDRateDisabledDate(Date value)
   {
     this.testChange("rateDisableDate", value);
     this.rateDisabledDate = value;
   }

   public void setLDRateTimeStamp(Date value)
   {
     this.testChange("rateTimeStamp", value);
     this.rateTimeStamp = value;
   }

   public void setLDRate(double value)
	 {
		 this.testChange("ldRate", value);
		 this.ldRate = value;
   }

   public void setLDRateDescription(String value )
	 {
		 this.testChange("rateDescription", value);
		 this.rateDescription = value;
   }

   public void setInitialRangeId(int value)
   {
     this.testChange("initialRangeId", value);
     this.initialRangeId = value;
   }

   public void setInsuranceRateStatusId(int value)
   {
     this.testChange("insuranceRateStatusId", value);
     this.insuranceRateStatusId = value;
   }

   public void setInsuranceAgeId(int value)
   {
     this.testChange("insuranceAgeId", value);
     this.insuranceAgeId = value;
   }

   public void setInsuranceTypeId(int value)
   {
     this.testChange("insuranceTypeId", value);
     this.insuranceTypeId = value;
   }

   public void setSmokeStatusId(int value)
   {
     this.testChange("smokeStatusId", value);
     this.smokeStatusId = value;
   }

   public int getInstitutionProfileId() {
       return institutionProfileId;
   }

   public void setInstitutionProfileId(int institutionProfileId) {
       this.testChange("institutionProfileId", institutionProfileId);
       this.institutionProfileId = institutionProfileId;
   }

}
