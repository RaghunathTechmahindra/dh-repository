package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.pk.SAMDispositionPK;
import com.basis100.deal.pk.SAMResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class SAMDisposition extends DealEntity{

    public static final String INSTITUTIONPROFILEID = "INSTITUTIONPROFILEID";
    
	protected int           responseId;
	protected int	        samDispositionStatusId;
	protected int		    samDispositionCodeId;
	protected int	        userProfileId;
	protected Date	        submitDateTimeStamp;
	protected Date	        responseDateTimeStamp;
	protected int         institutionProfileId;

	private SAMDispositionPK pk;

	public SAMDisposition(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public SAMDisposition(SessionResourceKit srk, int id) 
    throws RemoteException, FinderException
	{
		super(srk);
	    pk = new SAMDispositionPK(id);
	    findByPrimaryKey(pk);
	  }

	/**  search for SAMDisposition record using primary key
	*   @return a SAMDisposition */
	public SAMDisposition findByPrimaryKey(SAMDispositionPK pk) 
    throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from SAMDisposition " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	            break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "SAMDisposition: @findByPrimaryKey(), key= " 
	            + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe 
              = new FinderException("SAMDisposition Entity - "
                                    + "findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}

    /**  sets properties for SAMDisposition */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	  	this.setResponseId(jExec.getInt(key, "responseId"));
	  	this.setSAMDispositionStatusId(jExec.getInt(key, "samDispositionStatusId"));
	  	this.setSAMDispositionCodeId(jExec.getInt(key, "samDispositionCodeId"));
	  	this.setUserProfileId(jExec.getInt(key, "userProfileId"));
	  	this.setSubmitDateTimeStamp(jExec.getDate(key, "submitDateTimeStamp"));
      this.setResponseDateTimeStamp(jExec.getDate(key, "responseDateTimeStamp"));
      this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	public String getEntityTableName()
	{
	    return "SAMDisposition"; 
	}

	/**  gets the pk associated with this entity
	 *   @return a SAMDispositionPK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}	

	public int getResponseId()
	{ 
		return this.responseId;
	}

	public void setResponseId(int id)
	{
		this.testChange ("responseId", id);
		this.responseId = id;
	}

	public int getSAMDispositionStatusId()
	{ 
		return this.samDispositionStatusId;
	}

	public void setSAMDispositionStatusId(int id)
	{
		this.testChange ("samDispositionStatusId", id);
		this.samDispositionStatusId = id;
	}

	public int getSAMDispositionCodeId()
	{ 
		return this.samDispositionCodeId;
	}

	public void setSAMDispositionCodeId(int id)
	{
		this.testChange ("samDispositionCodeId", id);
		this.samDispositionCodeId = id;
	}

	public int getUserProfileId()
	{ 
		return this.userProfileId;
	}

	public void setUserProfileId(int id)
	{
		this.testChange ("userProfileId", id);
		this.userProfileId = id;
	}

	public Date getSubmitDateTimeStamp()
	{ 
		return this.submitDateTimeStamp;
	}

	public void setSubmitDateTimeStamp(Date sdate)
	{
		this.testChange ("submitDateTimeStamp", sdate);
		this.submitDateTimeStamp = sdate;
	}
	
	public Date getResponseDateTimeStamp()
	{ 
		return this.responseDateTimeStamp;
	}

	public void setResponseDateTimeStamp(Date rdate)
	{
		this.testChange ("responseDateTimeStamp", rdate);
		this.responseDateTimeStamp = rdate;
	}

	/**  creates SAMDisposition record into database
	 *   @return a SAMDisposition */
	public SAMDisposition create(ResponsePK rpk)
    throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into SAMDisposition(" + rpk.getName() +  ", " 
      + INSTITUTIONPROFILEID + ") Values ( " + rpk.getId() + ", " 
      + srk.getExpressState().getDealInstitutionId() + ")";
		
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new SAMDispositionPK(rpk.getId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException
        ("SAMDisposition Entity - SAMDisposition - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
		return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof SAMDisposition)
		{
			SAMDisposition oa = (SAMDisposition) o;
			if  (  this.responseId == oa.getResponseId ()  )
				return true;
		}
		return false;
	}

    public Collection findBySAMResponse(SAMResponsePK pk) throws Exception
    {
      Collection samdispositions = new ArrayList();

      String sql = "Select * from SAMDisposition " ;
            sql +=  pk.getWhereClause();
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              SAMDisposition samdisposition = new SAMDisposition(srk);
              samdisposition.setPropertiesFromQueryResult(key);
              samdisposition.pk = new SAMDispositionPK(samdisposition.getResponseId());
              samdispositions.add(samdisposition);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting "
                                       + " SAMResponse::SAMDisposition");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }
        return samdispositions;
    }

    public int getInstitutionProfileId() {
      return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}
