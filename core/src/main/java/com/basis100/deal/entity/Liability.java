package com.basis100.deal.entity;

/**
 * 26.Jun.2009 DVG #DG862 LEN380950: 	Ruth Kiaupa DE-INIT business rule issue
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.LiabilityPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;

public class Liability extends DealEntity implements Xc
{
  protected int            liabilityId;
  protected int            liabilityTypeId;
  protected int            borrowerId;
  protected double         liabilityAmount;
  protected double         liabilityMonthlyPayment;
  protected boolean        includeInGDS;
  protected boolean        includeInTDS;
  protected String         liabilityDescription;
  protected int            liabilityPayOffTypeId;
  protected double         percentInGDS;
  protected double         percentInTDS;
  protected String         liabilityPaymentQualifier;
  protected double         percentOutGDS;
  protected double         percentOutTDS;
  protected int            copyId;
  protected int            institutionProfileId; 
  
  //FFATE
  protected double         creditLimit;
  protected Date           maturityDate;
  protected String         creditBureauRecordIndicator;

  private LiabilityPK pk;

  //--> It not used anywhere !!
  //--> Commented out by Billy 06Oct2004
  //protected boolean isAuditable = true;
  //===========================

  public Liability(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
  {
    super(srk,dcm);
  }

  public Liability(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
  {
    super(srk,dcm);

    pk = new LiabilityPK(id, copyId);

    findByPrimaryKey(pk);
  }

  public Liability findByPrimaryKey(LiabilityPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from Liability " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

          logger.error( fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }
      this.pk = pk;
      this.copyId = pk.getCopyId();
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }

  public Collection<Liability> findByBorrower(BorrowerPK pk) throws Exception
  {
    Collection<Liability> liabilities = new ArrayList<Liability>();

    String sql = "Select * from Liability "+  pk.getWhereClause();
     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           Liability l = new Liability(srk,dcm);
           l.setPropertiesFromQueryResult(key);
           l.pk = new LiabilityPK(l.getLiabilityId(),l.getCopyId());
           liabilities.add(l);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while fetching Borrower::Liabilities");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

     return liabilities;
    }

    /**
    *  Find Liability records not associated with an EmploymentHistory record for
    *  the given Borrower.
    */
    public Collection findByDeal(DealPK pk) throws Exception
    {
      Collection liabilities = new ArrayList();

      StringBuffer buf = new StringBuffer("Select * from Liability ");
      buf.append(" where borrowerId in( Select BorrowerId from Borrower where dealId = ");
      buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
      buf.append(") and copyId = ").append(pk.getCopyId());
      final String sql = buf.toString();
      
		try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           Liability l = new Liability(srk,dcm);
           l.setPropertiesFromQueryResult(key);
           l.pk = new LiabilityPK(l.getLiabilityId(),l.getCopyId());
           liabilities.add(l);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Liability Entity - findByPrimaryKey() exception");
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw e;
      }

    return liabilities;
   }

    public Collection findByDealAndPayoffTypes(DealPK pk, int[] payofftype) throws Exception
    {
      Collection liabilities = new ArrayList();

      StringBuffer inBuf = new StringBuffer(32);
      inBuf.append("( ");
      for(int i=0; i<payofftype.length; i++ ){
        inBuf.append(payofftype[i]);
        if(i < payofftype.length - 1){
          inBuf.append(",");
        }
      }
      inBuf.append(")");

      StringBuffer buf = new StringBuffer(256);
      buf.append("Select * from Liability ");
      buf.append(" where liabilityPayoffTypeId in  " );
      buf.append( inBuf.toString() );
      buf.append(" and ");
      buf.append(" borrowerId in( Select BorrowerId from Borrower where dealId = ");
      buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
      buf.append(") and copyId = ").append(pk.getCopyId());

      final String sql = buf.toString();
		try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           Liability l = new Liability(srk,dcm);
           l.setPropertiesFromQueryResult(key);
           l.pk = new LiabilityPK(l.getLiabilityId(),l.getCopyId());
           liabilities.add(l);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Liability Entity - findByPrimaryKey() exception");
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw e;
      }

    return liabilities;
   }


    /**
    *  Find Liability records associated with a given deal where the payofftypeId = to
    *  the provided id.
    * @param pk - the DealPK for the deal
    * @param payofftype - the LiabilityPayoffTypeId for the search
    * @return a collection of Liability entities
    */
    public Collection findByDealAndPayoffType(DealPK pk, int payofftype) throws Exception
    {
      Collection liabilities = new ArrayList();

      StringBuffer buf = new StringBuffer("Select * from Liability ");
      buf.append(" where liabilityPayoffTypeId = ").append(payofftype).append(" and ");
      buf.append(" borrowerId in( Select BorrowerId from Borrower where dealId = ");
      buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
      buf.append(") and copyId = ").append(pk.getCopyId());

      final String sql = buf.toString();
		try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           Liability l = new Liability(srk,dcm);
           l.setPropertiesFromQueryResult(key);
           l.pk = new LiabilityPK(l.getLiabilityId(),l.getCopyId());
           liabilities.add(l);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Liability Entity - findByPrimaryKey() exception");
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw e;
      }

    return liabilities;
   }

   /**
    *  Find Liability records not with non blank payoff not of type mortgage
    *  the given Borrower.
    */
    public Collection findNonMortgageTypes(DealPK pk) throws Exception
    {
      Collection liabilities = new ArrayList();

      StringBuffer buf = new StringBuffer("Select * from Liability ");
      buf.append(" where borrowerId in( Select BorrowerId from Borrower where dealId = ");
      buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
      buf.append(" and LiabilityPayoffTypeId <> 0 and LiabilityTypeId <> 0) and copyId = ").append(pk.getCopyId());

      final String sql = buf.toString();
		try
      {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           Liability l = new Liability(srk,dcm);
           l.setPropertiesFromQueryResult(key);
           l.pk = new LiabilityPK(l.getLiabilityId(),l.getCopyId());
           liabilities.add(l);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Liability Entity - findByPrimaryKey() exception");
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw e;
      }

    return liabilities;
   }


  public void setPropertiesFromQueryResult(int key) throws Exception
  {
       //int index = 0;
       this.setLiabilityId(jExec.getInt(key,"LIABILITYID"));
       this.setLiabilityTypeId(jExec.getInt(key,"LIABILITYTYPEID"), false);
       this.setBorrowerId(jExec.getInt(key,"BORROWERID"));
       this.setLiabilityAmount(jExec.getDouble(key,"LIABILITYAMOUNT"));
       this.setLiabilityMonthlyPayment(jExec.getDouble(key,"LIABILITYMONTHLYPAYMENT"));
       this.setIncludeInGDS(jExec.getString(key,"INCLUDEINGDS"));
       this.setIncludeInTDS(jExec.getString(key,"INCLUDEINTDS"));
       this.setLiabilityDescription(jExec.getString(key,"LIABILITYDESCRIPTION"));
       this.setLiabilityPayOffTypeId(jExec.getInt(key,"LIABILITYPAYOFFTYPEID"));
       this.setPercentInGDS(jExec.getDouble(key,"PERCENTINGDS"));
       this.setPercentInTDS(jExec.getDouble(key,"PERCENTINTDS"));
       this.setLiabilityPaymentQualifier(jExec.getString(key,"LIABILITYPAYMENTQUALIFIER"));
       this.setPercentOutGDS(jExec.getDouble(key,"PERCENTOUTGDS"));
       this.setPercentOutTDS(jExec.getDouble(key,"PERCENTOUTTDS"));
       this.copyId = jExec.getInt(key, "COPYID");
       this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));    
       this.setCreditLimit(jExec.getDouble(key,"CREDITLIMIT"));
       this.setMaturityDate(jExec.getDate(key,"MATURITYDATE"));
       this.setCreditBureauRecordIndicator(jExec.getString(key,"CREDITBUREAURECORDINDICATOR"));

  }

   /**
  *  gets the value of instance fields as String.
  *  if a field does not exist (or the type is not serviced)** null is returned.
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *  @returns value of bound field as a String;
  *  @param  fieldName as String
  *
  *  Other entity types are not yet serviced   ie. You can't get the Province object
  *  for province.
  */
  public String getStringValue(String fieldName)
  {
    return doGetStringValue(this, this.getClass(), fieldName);
  }

  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  @param  fieldName as String
  *  @param  the value as a String
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);

    // Here is an exception that when setting the up or changing the LiabilityType
    // we need to update the LiabilityPaymentQualifier as well -- By BILLY 14Nov2001
    if(fieldName != null && fieldName.equalsIgnoreCase("liabilityTypeId"))
    {
        // added inst id for ML
      String theQ  = PicklistData.getMatchingColumnValue(PKL_LIABILITY_TYPE, 
      		TypeConverter.intTypeFrom(value), "LIABILITYPAYMENTQUALIFIER");

      doSetField(this, this.getClass(), "liabilityPaymentQualifier", theQ);
    }
  }


  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return the number of updates performed
  */
  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();
    int ret = doPerformUpdate(this, clazz);

    // Optimization:
    // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;
  }


  public String getEntityTableName()
  {
    return Liability.class.getSimpleName();	//#DG862
  }

  /**
   *   gets the liabilityId associated with this entity
   *
   *   @return a int
   */
  public int getLiabilityId()
  {
    return this.liabilityId ;
  }

  /**
   *   gets the liabilityMonthlyPayment associated with this entity
   *
   *   @return a double
   */
  public double getLiabilityMonthlyPayment()
  {
    return this.liabilityMonthlyPayment ;
  }

  /**
   *   gets the liabilityTypeId associated with this entity
   *
   */
  public int getLiabilityTypeId()
  {
    return this.liabilityTypeId ;
  }



  /**
   *   gets the pk associated with this entity
   *
   *   @return a LiabilityPK
   */
  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }

  /**
   *   gets the borrowerId associated with this entity
   *
   *   @return a int
   */
  public int getBorrowerId()
  {
    return this.borrowerId ;
  }

  /**
  *   gets the liabilityAmount associated with this entity
  *
  *   @return a double
  */
  public double getLiabilityAmount()
  {
    return this.liabilityAmount ;
  }

  public boolean getIncludeInGDS()
  {
    return this.includeInGDS ;
  }

  public boolean getIncludeInTDS()
  {
    return this.includeInTDS;
  }
  public int getCopyId(){return copyId;}
  public double getPercentInGDS(){ return this.percentInGDS;}
  public double getPercentInTDS(){ return this.percentInTDS;}
  public double getPercentOutGDS(){ return this.percentOutGDS;}
  public double getPercentOutTDS(){ return this.percentOutTDS;}
  public int getLiabilityPayOffTypeId(){ return this.liabilityPayOffTypeId;}
  public String getLiabilityDescription(){ return this.liabilityDescription;}

  public String getLiabilityPaymentQualifier()
  {
   return this.liabilityPaymentQualifier;
  }

  private LiabilityPK createPrimaryKey(int copyId)throws CreateException
  {
     String sql = "Select Liabilityseq.nextval from dual";
     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }

         jExec.closeData(key);

         if( id == -1 ) throw new Exception();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("Liability Entity create() exception getting LiabilityId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
      }

     return new LiabilityPK(id, copyId);
  }


  /**
  * creates a Liability using the Liability primary key and a borrower primary key
  *  - the mininimum (ie.non-null) fields
  *
  */

  public Liability create(BorrowerPK bpk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(bpk.getCopyId());

    // currently not defaulted ....
    // protected int            percentOutTDS;

    // set default value for liability type and associated defaults

    int liabilityTypeId = 0;

    // added inst id for ML
    String percentInGDS  = PicklistData.getMatchingColumnValue(PKL_LIABILITY_TYPE, liabilityTypeId, "GDSINCLUSION");
    String includeInGDS = ("0".equals(percentInGDS)) ? "'N'" : "'Y'";

    // added inst id for ML
    String percentInTDS  = PicklistData.getMatchingColumnValue(PKL_LIABILITY_TYPE, liabilityTypeId, "TDSINCLUSION");
    String includeInTDS = ("0".equals(percentInTDS)) ? "'N'" : "'Y'";

    String percentOutGDS = "0.0"; // this value reflects the fact, that this is a liability from the liability
                                  // section. Actually, it could be created from the screen with 'Add Liability'
                                  // functionality only.

    //==> add some protections -- By Billy 22May2003
    // added inst id for ML
    String liabilityPaymentQualifier = PicklistData.getMatchingColumnValue(//#DG862 srk.getExpressState().getDealInstitutionId(),
   		 PKL_LIABILITY_TYPE, liabilityTypeId, "LIABILITYPAYMENTQUALIFIER");
    if(liabilityPaymentQualifier != null && liabilityPaymentQualifier.length()==1)
      liabilityPaymentQualifier = "'" + liabilityPaymentQualifier + "'";
    else
    {
      //Default to 'F'
      logger.warn("BILLY looking for this :: Problem @Liability.create ==> cannot get LIABILITYPAYMENTQUALIFIER ==> Default it to F !!");
      liabilityPaymentQualifier = "'F'";
    }
    //====================================================

    String sql = "Insert into Liability( " +
	    "LiabilityId, BorrowerId, copyId, liabilityAmount, liabilityMonthlyPayment, liabilityPayOffTypeId, " +
	    "liabilityTypeId, percentInGDS, includeInGDS, percentInTDS, includeInTDS, " +
	    "liabilityPaymentQualifier, percentOutGDS, InstitutionProfileID )" +
	    " Values ( " +
      pk.getId() + "," + bpk.getId() + "," + pk.getCopyId() + ", 0.0, 0.0, 0, " +
      liabilityTypeId + ", " + percentInGDS + ", " + includeInGDS + ", " + percentInTDS + ", " + includeInTDS + ", " +
      liabilityPaymentQualifier + ", " + percentOutGDS + "," + srk.getExpressState().getDealInstitutionId()+ ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      this.trackEntityCreate ();  // track the creation
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Liability Entity - Liability - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    srk.setModified(true);

    if(dcm != null)
        //dcm.inputEntity(this);
        dcm.inputCreatedEntity ( this );

    return this;
  }


  public Liability deepCopy() throws CloneNotSupportedException
  {
    return (Liability)this.clone();
  }

  public void setLiabilityId(int id)
  {
   this.testChange ("liabilityId", id ) ;
   this.liabilityId = id;
  }

  public void setLiabilityTypeId(int id)
  {
    this.setLiabilityTypeId(id, true);
  }
  public void setLiabilityTypeId(int id, boolean isSetQualifier)
  {
   this.testChange ("liabilityTypeId", id ) ;
   this.liabilityTypeId = id;

   if(isSetQualifier == true)
   {
       // added inst id for ML
       String theQ = PicklistData.getMatchingColumnValue(PKL_LIABILITY_TYPE, id, "LIABILITYPAYMENTQUALIFIER");
     this.setLiabilityPaymentQualifier(theQ);
   }

  }

  public void setBorrowerId(int id)
  {
   this.testChange ("borrowerId", id ) ;
   this.borrowerId = id;
  }
  public void setLiabilityAmount(double amt )
  {
   this.testChange ("liabilityAmount", amt ) ;
   this.liabilityAmount = amt;
  }

  public void setLiabilityMonthlyPayment(double payment )
  {
   this.testChange( "liabilityMonthlyPayment", payment ) ;
   this.liabilityMonthlyPayment = payment;
  }
  public void setIncludeInGDS(String c)
  {
   this.testChange ("includeInGDS", c ) ;
   this.includeInGDS = TypeConverter.booleanFrom(c);
  }
  public void setIncludeInGDS(char c)
  {
   this.testChange ("includeInGDS", c ) ;
   this.includeInGDS = TypeConverter.booleanFrom(String.valueOf(c));
  }

  public void setIncludeInTDS(String c)
  {
   this.testChange ("includeInTDS", c );
   this.includeInTDS = TypeConverter.booleanFrom(c);
  }
  public void setIncludeInTDS(char c)
  {
   this.testChange ("includeInTDS", c );
   this.includeInTDS = TypeConverter.booleanFrom(String.valueOf(c));
  }

  public void setPercentInGDS(double pct)
  {
   this.testChange ("percentInGDS", pct);
   this.percentInGDS = pct;
  }

  public void setPercentInTDS(double pct)
  {
   this.testChange ("percentInTDS", pct );
   this.percentInTDS = pct;
  }


  public void setLiabilityPayOffTypeId(int id)
  {
   this.testChange ("liabilityPayOffTypeId", id ) ;
   this.liabilityPayOffTypeId = id;
  }

  public void setLiabilityDescription(String desc)
  {
   this.testChange ("liabilityDescription", desc ) ;
   this.liabilityDescription = desc;
  }

  public void setLiabilityPaymentQualifier(String q)
  {
   this.testChange ("liabilityPaymentQualifier", q );
   this.liabilityPaymentQualifier = q;
  }

  public void setPercentOutGDS(double pct)
  {
   this.testChange ("percentOutGDS", pct ) ;
   this.percentOutGDS = pct;
  }

  public void setPercentOutTDS(double pct)
  {
   this.testChange ("percentOutTDS", pct ) ;
   this.percentOutTDS = pct;
  }

  public int getInstitutionProfileId() {
      return institutionProfileId;
  }

  public void setInstitutionProfileId(int institutionProfileId) {
      this.testChange("institutionProfileId", institutionProfileId); 
      this.institutionProfileId = institutionProfileId;
  }

  public double getCreditLimit() {
      return creditLimit;
  }

  public void setCreditLimit(double creditLimit) {
      this.testChange("creditLimit", creditLimit); 
      this.creditLimit = creditLimit;
  }
  
  public Date getMaturityDate() {
      return maturityDate;
  }

  public void setMaturityDate(Date maturityDate) {
      this.testChange("maturityDate", maturityDate); 
      this.maturityDate = maturityDate;
  }
  
  public String getCreditBureauRecordIndicator() {
      return creditBureauRecordIndicator;
  }

  public void setCreditBureauRecordIndicator(String creditBureauRecordIndicator) {
      this.testChange("creditBureauRecordIndicator", creditBureauRecordIndicator); 
      this.creditBureauRecordIndicator = creditBureauRecordIndicator;
  }
  
  public Vector findByMyCopies() throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from Liability where liabilityId = " +
       getLiabilityId() + " AND copyId <> " + getCopyId();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            Liability iCpy = new Liability(srk,dcm);

            iCpy.setPropertiesFromQueryResult(key);
            iCpy.pk = new LiabilityPK(iCpy.getLiabilityId(), iCpy.getCopyId());

            v.addElement(iCpy);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {

        FinderException fe = new FinderException("Liability - findByMyCopies exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }

    return v;
  }

  public int getClassId()
  {
    return ClassId.LIABILITY;
  }

  public boolean isAuditable()
  {
    return true;
  }


  public  boolean equals( Object o )
  {
    if ( o instanceof Liability )
      {
        Liability oa = ( Liability ) o;
        if  ( ( this.liabilityId == oa.getLiabilityId ()  )
          && ( this.copyId  ==  oa.getCopyId ()  )  )
            return true;
      }
    return false;
  }

  protected EntityContext getEntityContext() throws RemoteException
  {
    EntityContext ctx = this.entityContext  ;

    try
    {
      ctx.setContextText ( this.getLiabilityDescription () ) ;
      Borrower borrower = new Borrower( srk, dcm, this.borrowerId , this.copyId  );
      if ( borrower.getBorrowerFirstName () != null && borrower.getBorrowerLastName ()!= null )
        ctx.setContextSource (borrower.getNameForEntityContext());

      //Modified by Billy for performance tuning -- By BILLY 28Jan2002
      //Deal  deal = new Deal ( srk, dcm, this.dealId  , this.copyId ) ;
      //String sc = String.valueOf ( deal.getScenarioNumber ()) + deal.getCopyType () ;
      //if ( deal == null )
      //  return ctx;
      //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
      Deal deal = new Deal(srk, dcm);
      String  sc = String.valueOf (deal.getScenarioNumber(borrower.getDealId(), borrower.getCopyId ( ))) +
          deal.getCopyType(borrower.getDealId(), borrower.getCopyId ( ));
      // ===================================================================================

      ctx.setScenarioContext ( sc ) ;

      ctx.setApplicationId ( borrower.getDealId() ) ;

    } catch ( Exception e )
    {
      if ( e instanceof FinderException )
      {
        logger.warn("Liability.getEntityContext Parent Not Found. LiabilityId=" + this.liabilityId );
        return ctx;
      }
      throw ( RemoteException) e ;
    }
    return ctx ;
  }// ---------- End of getEntityContext() ---------------------------
}
