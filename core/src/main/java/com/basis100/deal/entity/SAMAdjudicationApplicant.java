package com.basis100.deal.entity;

import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import oracle.jdbc.driver.OracleCallableStatement;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationApplicantPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.SAMAdjudicationApplicantPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

//*************** WARNING ******************
//TODO: copyId is missing
// WHAT HAPPEN???
// commented Sep 7, 2007 Midori
//******************************************
public class SAMAdjudicationApplicant extends DealEntity{
	protected int         requestId;
	protected int	        applicantNumber;
	protected double	    samTotalMonthlyPayment;
	protected String	    samCreditFileReport;
  protected int         institutionProfileId;

	private SAMAdjudicationApplicantPK pk;

	public SAMAdjudicationApplicant(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public SAMAdjudicationApplicant(SessionResourceKit srk, int id, int appnum) 
    throws RemoteException, FinderException
	{
		super(srk);
    pk = new SAMAdjudicationApplicantPK(id, appnum);
    findByPrimaryKey(pk);
	}

	/**  search for SAMAdjudicationApplicant record using primary key
	*   @return a SAMAdjudicationApplicant */
	public SAMAdjudicationApplicant findByPrimaryKey(SAMAdjudicationApplicantPK pk) 
    throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from SAMAdjudicationApplicant " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "SAMAdjudicationApplicant: @findByPrimaryKey(), key= " 
              + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException
              ("SAMAdjudicationApplicant Entity - findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}


    /**  sets properties for SAMAdjudicationApplicant */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	  	this.setRequestId(jExec.getInt(key, "requestId"));
	  	this.setApplicantNumber(jExec.getInt(key, "applicantNumber"));
	  	this.setSAMTotalMonthlyPayment(jExec.getDouble(key, "samTotalMonthlyPayment"));
      this.setSAMCreditFileReport(readCreditFileReport());
      this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		String [] arrExcl = { "SAMCREDITFILEREPORT" };

		int ret = doPerformUpdate(arrExcl);
		writeCreditFileReport();
		return ret;
	}

	
	public String getEntityTableName()
	{
	    return "SAMAdjudicationApplicant"; 
	}

	/**  gets the pk associated with this entity
	 *   @return a SAMAdjudicationApplicantPK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}	

	public int getRequestId()
	{ 
		return this.requestId;
	}

	public void setRequestId(int id)
	{
		this.testChange ("requestId", id);
		this.requestId = id;
	}

	public int getApplicantNumber()
	{ 
		return this.applicantNumber;
	}

	public void setApplicantNumber(int id)
	{
		this.testChange ("applicantNumber", id);
		this.applicantNumber = id;
	}

	public double getSAMTotalMonthlyPayment()
	{ 
		return this.samTotalMonthlyPayment;
	}

	public void setSAMTotalMonthlyPayment(double amt)
	{
		this.testChange ("samTotalMonthlyPayment", amt);
		this.samTotalMonthlyPayment = amt;
	}

	public String getSAMCreditFileReport()
	{ 
		return this.samCreditFileReport;
	}

	public void setSAMCreditFileReport(String creditfilerep)
	{
		String crreport = parseHTML(creditfilerep);
		this.testChange ("samCreditFileReport", crreport);
		this.samCreditFileReport = crreport;
	}

	/**  creates SAMAdjudicationApplicant record into database
	 *   @return a SAMAdjudicationApplicant */
	public SAMAdjudicationApplicant create(RequestPK rpk, int appnum)
    throws RemoteException, CreateException
	{
		int key = 0;
		String sql = "Insert into SAMAdjudicationApplicant(" + rpk.getName() 
      +  ", applicantNumber, institutionProfileId) Values ( " + rpk.getId() 
      + "," + appnum + ", " + srk.getExpressState().getDealInstitutionId() +")";
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new SAMAdjudicationApplicantPK(rpk.getId(),appnum));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("SAMAdjudicationApplicant Entity - SAMAdjudicationApplicant - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}

		srk.setModified(true);
		return this;
	}

	/**  creates SAMAdjudicationApplicant record into database
	 *   @return a SAMAdjudicationApplicant */
	public SAMAdjudicationApplicant create(int reqid, int appnum)throws RemoteException, CreateException
	{
		int key = 0;
	  	StringBuffer sqlb = new StringBuffer
        ("Insert into SAMAdjudicationApplicant(requestId, applicantNumber, " 
         + "institutonProfileId) Values ( ");  
	  	
      sqlb.append(sqlStringFrom(reqid) + ", ");   
      sqlb.append(sqlStringFrom(appnum) + ", ");   
			sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");   							   							 
	  	
		String sql = new String(sqlb);
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new SAMAdjudicationApplicantPK(reqid,appnum));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("SAMAdjudicationApplicant Entity - SAMAdjudicationApplicant - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}
		srk.setModified(true);
		return this;
	}

	/**  creates SAMAdjudicationApplicant record into database
	 *   @return a SAMAdjudicationApplicant */
	public SAMAdjudicationApplicant create(AdjudicationApplicantPK aapk)
    throws RemoteException, CreateException
	{
		int key = 0;
	  	StringBuffer sqlb = new StringBuffer
        ("Insert into SAMAdjudicationApplicant (requestId, applicantNumber, "
         + "+insitutionProfileId) Values ( ");  
	  	
      sqlb.append(sqlStringFrom(aapk.getId()) + ", ");    
      sqlb.append(sqlStringFrom(aapk.getCopyId()) + ", ");    
			sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId() + ")"));   							   							 
	  	
		String sql = new String(sqlb);

		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new SAMAdjudicationApplicantPK(aapk.getId(),aapk.getCopyId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException
        ("SAMAdjudicationApplicant Entity - SAMAdjudicationApplicant - "
         + "create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}
		srk.setModified(true);
		return this;
	}

	private String readCreditFileReport()
	{
		JdbcExecutor je =  srk.getJdbcExecutor();
		Connection con = je.getCon();
		String outval = "";
		ResultSet rs = null;
		Statement stm = null;
		OracleCallableStatement callableStmt = null;
		try
		{
			String stmt = "Select SAMCreditFileReport from SAMAdjudicationApplicant ";
			stmt += "where RequestId = " + this.getRequestId();
			stmt += " and ApplicantNumber = " + this.getApplicantNumber();

			oracle.sql.CLOB clob = null;

			stm = con.createStatement() ;
			rs = stm.executeQuery(stmt);

			if (rs.next())
			{
				clob = (oracle.sql.CLOB)rs.getObject(1);

				if(clob == null)
					return outval;

				if(clob.length() <= 0)
					return outval;

          callableStmt =
           (OracleCallableStatement)con.prepareCall("begin dbms_LOB.read(?,?,?,?); end;");
          long totalLength = clob.length();
          long i = 0;             // index at which to read
          int chunk = 30*1024;    // size of the chunk to read :: Oracle limit is 30K
          long read_this_time = 0;

          while (i < totalLength)
          {
            callableStmt.clearParameters();
            callableStmt.setCLOB(1,clob);
            callableStmt.setLong(2,chunk); //size of chunk
            callableStmt.registerOutParameter(2,Types.NUMERIC); //Bytes received
            callableStmt.setLong(3, i+1); // begin reading at the offset
            callableStmt.registerOutParameter (4,Types.LONGVARCHAR);
            callableStmt.execute ();

            read_this_time = callableStmt.getLong (2);
            outval += callableStmt.getString(4);
            i += read_this_time;
          }
          callableStmt.close();
        }

        rs.close();
        stm.close();
      }
      catch(Exception e)
      {
        logger.error("SAMAdjudicationApplicant::SAMCreditFileReport: "
                     + "failed to read data from report: " + this.getPk());
        logger.info("In Transaction: " + je.isInTransaction());

        String msg = "";

        if(e.getMessage() == null)
         msg = "Unknown Error Occurred";
        logger.error("SAMAdjudicationApplicant::SAMCreditFileReport: Reason for failure: " + msg);
        logger.error(e);
        try{ rs.close();            }catch(Exception ea){;}
        try{ stm.close();           }catch(Exception eb){;}
        try{ callableStmt.close();  }catch(Exception ec){;}
      } finally {
          try {
              if(rs != null) rs.close();
          } catch (Exception ignore){}
          try {
              if(stm != null) stm.close();
          } catch (Exception ignore){}
          try {
              if(callableStmt != null) callableStmt.close();
          } catch (Exception ignore){}
      }
      return outval;
   	}

	private void writeCreditFileReport()
	{
	      if(this.samCreditFileReport == null )
	       this.samCreditFileReport = "";
	      srk.setCloseJdbcConnectionOnRelease(true);
	      JdbcExecutor je =  srk.getJdbcExecutor();
	      Connection con = je.getCon();
	      ResultSet rs = null;
	      Statement stm = null;
		  Writer writer = null;
	      try
	      {
	        oracle.sql.CLOB clob = null;

	        //get insert a new locator
	        stm = con.createStatement() ;
	        String st1 = "Update SAMAdjudicationApplicant set samCreditFileReport "
            + "= empty_clob() where RequestId = ";
	        st1 +=  getRequestId();
			st1 += " and ApplicantNumber = " + getApplicantNumber();
			stm.executeUpdate(st1);
	        con.setAutoCommit(false);
	        String st2 = "Select samCreditFileReport from SAMAdjudicationApplicant ";
	        st2 += "where RequestId = " + getRequestId();
			st2 += " and ApplicantNumber = " + getApplicantNumber();
	        st2 += " for update";
	        rs = stm.executeQuery(st2);
	        if (rs.next())
	        {
	          clob = (oracle.sql.CLOB)rs.getObject(1);
	          if(clob == null)
	           throw new Exception("SAMAdjudicationApplicant::SAMCreditFileReport: "
                                 + "Could not obtain LOB locator: " + this.pk);
			  writer = clob.getCharacterOutputStream();
	        }
	        rs.close();
	        stm.close();
			writer.write(getSAMCreditFileReport());
			writer.flush();
			writer.close();
			con.commit();
	      }
	      catch(Exception e)
	      {
	        e.printStackTrace();
	        logger.error("SAMAdjudicationApplicant::SAMCreditFileReport: "
                       + "failed to write data to report: " + this.getPk());
	        logger.info("In Transaction: " + je.isInTransaction());

	        String msg = "";
	        if(e.getMessage() == null)
	         msg = "Unknown Error Occurred";
	        logger.error("SAMAdjudicationApplicant::SAMCreditFileReport: Reason for failure: " + msg);
	        logger.error(e);
	        try{ rs.close();      }catch(Exception ea){;}
	        try{ stm.close();     }catch(Exception eb){;}
	        try{ writer.close();  }catch(Exception ec){;}
	      } finally {
	          try {
	              if(rs != null) rs.close();
	          } catch (Exception ignore){}
	          try {
	              if(stm != null) stm.close();
	          } catch (Exception ignore){}
	      }
	}
	
	public  boolean equals( Object o )
	{
		if ( o instanceof SAMAdjudicationApplicant)
		{
			SAMAdjudicationApplicant oa = (SAMAdjudicationApplicant) o;
			if  (  this.requestId == oa.getRequestId ()  )
				return true;
		}
		return false;
	}

    public Collection findByAdjudicationApplicant(AdjudicationApplicantPK pk) throws Exception
    {
      Collection adjapplicants = new ArrayList();

      String sql = "Select * from SAMAdjudicationApplicant " ;
            sql +=  pk.getWhereClause();
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
            	SAMAdjudicationApplicant adjapplicant = new SAMAdjudicationApplicant(srk);
            	adjapplicant.setPropertiesFromQueryResult(key);
            	adjapplicant.pk = new SAMAdjudicationApplicantPK
                (adjapplicant.getRequestId(),adjapplicant.getApplicantNumber());
            	adjapplicants.add(adjapplicant);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting "
                                       + "AdjudicationApplicant::"
                                       + "SAMAdjudicationApplicant");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return adjapplicants;
    }
    
    private void setPk(SAMAdjudicationApplicantPK pk) {
        this.pk = pk;  
    }
    
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    
    public Object clone(RequestPK rpk) throws CloneNotSupportedException
    {
        SAMAdjudicationApplicant clone = null;
        
        try { 
            clone = new SAMAdjudicationApplicant(srk);
            clone.create(rpk.getId(), getApplicantNumber());
 
            SAMAdjudicationApplicantPK apk = (SAMAdjudicationApplicantPK) clone.getPk();
            
            clone = (SAMAdjudicationApplicant) super.clone();
            
            clone.setPk(apk);
            clone.setRequestId(apk.getId());
            clone.setApplicantNumber(apk.getCopyId());
            
            clone.doPerformUpdateForce();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Error in SAMAdjudicationApplicant.clone(). Trying "
                         + "to clone RequestID = " + this.requestId 
                         + "; AppNumber: " + applicantNumber + e.getMessage());
        }
        
        return clone;
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
}
