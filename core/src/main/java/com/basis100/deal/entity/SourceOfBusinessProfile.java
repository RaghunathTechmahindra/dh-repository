package com.basis100.deal.entity;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;


public class SourceOfBusinessProfile extends DealEntity
{
   protected int      sourceOfBusinessProfileId;
   protected int      lengthOfService;
   protected int      contactId;
   protected int      profileStatusId;
   protected int      tierLevel;
   protected double   compensationFactor;
   protected String   SOBPShortName;
   protected String   SOBPBusinessId;
   protected int      sourceOfBusinessCategoryId;
   protected String   alternativeId;
   protected int      partyProfileId;
   protected String   notes;
   protected int      SOBRegionId;
   protected int      sourceFirmProfileId;
   protected int      systemTypeId;
   protected String   sourceOfBusinessCode;
   protected int      sourceOfBusinessPriorityId;
   protected int      institutionProfileId;
   protected String   SOBLicenseRegistrationNumber;


   protected SourceOfBusinessProfilePK pk;

   public SourceOfBusinessProfile() {}

   public SourceOfBusinessProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public SourceOfBusinessProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new SourceOfBusinessProfilePK(id);

      findByPrimaryKey(pk);
   }


   public SourceOfBusinessProfile findByPrimaryKey(SourceOfBusinessProfilePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from SourceOfBusinessProfile where " + pk.getName()+ " = '" + pk.getId() + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

  //-- FXLink Phase II --//
  public SourceOfBusinessProfile findBySystemTypeSFPAndSOBCode( int pSystemType, int pSourceFirmProfileId,String pSobCode ) throws FinderException
  {
      StringBuffer buf = new StringBuffer("SELECT * from sourceOfBusinessProfile WHERE ");
      buf.append(" systemtypeid = ").append(pSystemType).append(" AND ");
      buf.append(" sourceofbusinesscode = \'").append(pSobCode).append("\'").append(" AND sourcefirmprofileid = ").append(pSourceFirmProfileId) ;

      String sql = buf.toString();

      boolean gotRecord = false;
      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              this.pk = new SourceOfBusinessProfilePK(this.sourceOfBusinessProfileId);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Error in finder - findBySystemTypeSFPAndSOBCode : Type=" + pSystemType + " SourceFirmProfileId =" + pSourceFirmProfileId + " sourceofbusinesscode = " + pSobCode;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Error in finder - findBySystemTypeSFPAndSOBCode : " + buf.toString());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        return this;
  }


/*
  //-- FXLink Phase II --//
  public SourceOfBusinessProfile findBySystemTypeAndSOBCode( int pSystemType, String pSourceFirmCode, boolean ZivkoScrewedUp, boolean zzscrewup ) throws FinderException
  {
      List retList = new ArrayList();
      StringBuffer buf = new StringBuffer("SELECT * from sourceOfBusinessProfile WHERE ");
      buf.append(" systemtypeid = ").append(pSystemType).append(" AND ");
      buf.append(" sourceofbusinesscode = \'").append(pSourceFirmCode).append("\'");

      String sql = buf.toString();

      boolean gotRecord = false;
      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              this.pk = new SourceOfBusinessProfilePK(this.sourceOfBusinessProfileId);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Error in finder - findBySystemTypeAndSOBCode : Type=" + pSystemType + " SourceOfBusinessCode=" + pSourceFirmCode;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Error in finder - findBySystemTypeAndSOBCode : " + buf.toString());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        return this;
  }
*/

  /**
  * Attempts to find the record that corresponds to the description. The method
  * first attempts to find the Contact entity that matches the names given.
  * 
  * ***** WARING *****
  * THis sql does return more than one record but this method return first revord
  * Midori - Sep 11, 1007
  * ***************************
  * 
  * @returns this entity in sync with database record or null if no matching contact
  * can be found.
  */
   public SourceOfBusinessProfile findByName(String sobpShortName) throws RemoteException, FinderException
   {
      String sql = "Select * from sourceOfBusinessProfile where SOBPShortName = '" + sobpShortName + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               this.pk = new SourceOfBusinessProfilePK(this.sourceOfBusinessProfileId);

               break;

          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByName = " + sobpShortName;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }

      return this;
   }

 /**
  * Attempts to find the first record that corresponds to the description. Assumes
  * that each mailbox is associated with one and only one SourceOfBusiness.
  *
  * @returns this entity in sync with database record or null if no matching contact
  * can be found.
  */
  //-- FXLink Phase II --//
  //--> Modified to use the sourceOfBusinessCode field instead of SourceOfBusinessMailBox table
  //--> By Billy 05Nov2003
   public SourceOfBusinessProfile findByMailBox(String mailbox) throws RemoteException, FinderException
   {
      String sql = "Select SourceOfBusinessProfileId from SourceOfBusinessProfile where sourceOfBusinessCode = '"
                  + mailbox + "'"
                  + " Order by SourceOfBusinessProfileId";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               int id = jExec.getInt(key,1);
               this.pk = new SourceOfBusinessProfilePK(id);
               break;
          }

          jExec.closeData(key);

          if(gotRecord && pk != null) findByPrimaryKey(pk);

          if (gotRecord == false)
          {
            String msg = "SourceOfBusinessProfile: @findByMailBox = " + mailbox  ;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }

      return this;
   }

    //-- FXLink Phase II --//
    //--> Modified to use the sourceOfBusiness.sourceFirmProfileId field instead of SourceToFirmAssoc table
    //--> By Billy 05Nov2003
    public Collection findByAssociatedSourceFirm(SourceFirmProfilePK pk)
    {
      List retList = new ArrayList();

      StringBuffer buf = new StringBuffer("Select * from SourceOfBusinessProfile where");
      buf.append(" sourceFirmProfileId = ");
      buf.append(pk.getId());

      try
      {
          int key = jExec.execute(buf.toString());
          for (; jExec.next(key); )
          {
             SourceOfBusinessProfile profile = new SourceOfBusinessProfile(srk);
             profile.setPropertiesFromQueryResult(key);
             profile.pk = new SourceOfBusinessProfilePK(profile.getSourceOfBusinessProfileId());
             retList.add(profile);
          }
          jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger.error("Error in finder - SourceOfBusinessProfile : " + buf.toString());
            logger.error(e);
            return retList;
        }
      return retList;
    }

    //-- FXLink Phase II --//
    // New additional fields -- By Billy 05Nov2003
    private void setPropertiesFromQueryResult(int key) throws Exception
    {
      this.setSourceOfBusinessProfileId(jExec.getInt(key,"sourceOfBusinessProfileId"));
      this.setLengthOfService(jExec.getInt(key,"lengthOfService"));
      this.setContactId(jExec.getInt(key, "contactId"));
      this.setProfileStatusId(jExec.getInt(key, "profileStatusId"));
      this.setTierLevel(jExec.getInt(key, "tierLevel"));
      this.setCompensationFactor(jExec.getInt(key, "compensationFactor"));
      this.setSOBPShortName(jExec.getString(key, "SOBPShortName"));
      this.setSOBPBusinessId(jExec.getString(key, "SOBPBusinessId"));
      this.setSourceOfBusinessCategoryId(jExec.getInt(key, "sourceOfBusinessCategoryId"));
      this.setAlternativeId(jExec.getString(key, "alternativeId"));
      this.setPartyProfileId(jExec.getInt(key, "partyProfileId"));
      this.setNotes(jExec.getString(key, "notes"));
      this.setSOBRegionId(jExec.getInt(key, "SOBRegionId"));
      this.setSourceFirmProfileId(jExec.getInt(key, "sourceFirmProfileId"));
      this.setSystemTypeId(jExec.getInt(key, "systemTypeId"));
      this.setSourceOfBusinessCode(jExec.getString(key, "sourceOfBusinessCode"));
      this.setSourceOfBusinessPriorityId(jExec.getInt(key,"sourceOfBusinessPriorityId"));
      this.setInstitutionProfileId(jExec.getInt(key,"institutionProfileId"));
      this.setSobLicenseRegistrationNumber(jExec.getString(key, "sobLicenseRegistrationNumber"));
      //========================================================
    }

  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
    protected int performUpdate()  throws Exception
    {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
    }

    public String getEntityTableName()
    {
      return "SourceOfBusinessProfile";

    }

		/**
     *   gets the lengthOfService associated with this entity
     *
     */
    public int getLengthOfService()
   {
      return this.lengthOfService ;
   }

		/**
     *   gets the profileStatusId associated with this entity
     *
     */
    public int getProfileStatusId()
   {
      return this.profileStatusId ;
   }

    /**
    *   retrieves the Contact parent record associated with this entity
    *
    *   @return a Contact
    */
    public Contact getContact()throws FinderException,RemoteException
    {
      return new Contact(srk,this.getContactId(),1);
    }





		/**
     *   gets the pk associated with this entity
     *
     *   @return a SourceOfBusinessProfilePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }


   public SourceOfBusinessProfilePK createPrimaryKey()throws CreateException
   {
       String sql = "Select SourceOfBusinessProfileseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }

           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("SourceOfBusinessProfile Entity create() exception getting SourceOfBusinessProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new SourceOfBusinessProfilePK(id);
  }

  /**
   * creates a SourceOfBusinessProfile with default PK
   */
  public SourceOfBusinessProfile create()throws RemoteException, CreateException
  {
    pk = createPrimaryKey();
    return create(pk);
  }

  /**
   * creates a SourceOfBusinessProfile using a primary key
   */
  public SourceOfBusinessProfile create(SourceOfBusinessProfilePK pk)
    throws RemoteException, CreateException
  {
      String sql = "Insert into SourceOfBusinessProfile(" +
          "SourceOfBusinessProfileId, InstitutionProfileId  " +
         " ) Values ( " + pk.getId() + ", " 
         + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate (); // track the creation
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceOfBusinessProfile Entity - SourceOfBusinessProfile - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      try
      {
        Contact c = new Contact(srk);
        c.create();
        this.setContactId(c.getContactId());
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceOfBusinessProfile Entity - SourceOfBusinessProfile - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      srk.setModified(true);
      return this;
  }

  public List getSecondaryParents() throws Exception
  {
    List sps = new ArrayList();

    Object o = getContact();

    if(o != null)
    sps.add(o);

    return sps;
  }

  public void setSourceOfBusinessProfileId(int id )
  {
   this.testChange("sourceOfBusinessProfileId", id);
   this.sourceOfBusinessProfileId = id;

  }

  public int getSourceOfBusinessProfileId()
  {
   return this.sourceOfBusinessProfileId ;
  }

  public void setLengthOfService(int len)
  {
   this.testChange("lengthOfService", len);
  this.lengthOfService = len;
  }
  public void setContactId(int id )
  {
   this.testChange("contactId", id);
  this.contactId = id;
  }
  public int getContactId(){  return this.contactId;  }

  public void setProfileStatusId(int id )
  {
   this.testChange("profileStatusId",id);
   this.profileStatusId = id;
  }


  public void setSOBPShortName(String val )
  {
   this.testChange("SOBPShortName", val);
   this.SOBPShortName = val;
  }
  public String getSOBPShortName(){ return this.SOBPShortName;}

  public void setSOBPBusinessId(String ctr )
  {
   this.testChange("SOBPBusinessId", ctr);
   this.SOBPBusinessId = ctr;
  }

  public String getSOBPBusinessId(){ return this.SOBPBusinessId;}

  public void setTierLevel(int id )
  {
   this.testChange("tierLevel", id);
   this.tierLevel = id;
  }
  public int getTierLevel(){  return this.tierLevel;  }


  public void setCompensationFactor(double factor )
  {
   this.testChange("compensationFactor", factor);
   this.compensationFactor = factor;
  }
  public double getCompensationFactor(){  return this.compensationFactor;  }

  public void setSourceOfBusinessCategoryId(int ctr)
  {
   this.testChange("sourceOfBusinessCategoryId",ctr);
   this.sourceOfBusinessCategoryId = ctr;
  }

  public int getSourceOfBusinessCategoryId()
  {
   return this.sourceOfBusinessCategoryId;
  }

  public void setAlternativeId(String id)
  {
   this.testChange("alternativeId", id);
   this.alternativeId = id;
  }
  public String getAlternativeId(){  return this.alternativeId;  }

  public void setPartyProfileId(int id )
  {
    this.testChange("partyProfileId", id);
    this.partyProfileId = id;
  }
  public int getPartyProfileId(){  return this.partyProfileId;  }

  // Added Notes field -- By BILLY 11April2002
  public void setNotes(String value)
  {
    this.testChange("notes", value);
    this.notes = value;
  }
  public String getNotes(){  return this.notes;  }

  // Added Notes field -- By BILLY 17May2002
  public void setSOBRegionId(int value)
  {
    this.testChange("SOBRegionId", value);
    this.SOBRegionId = value;
  }
  public int getSOBRegionId(){  return this.SOBRegionId;  }

  //-- FXLink Phase II --//
  // New additional fields -- By Billy 05Nov2003
  public void setSourceFirmProfileId(int value)
  {
    this.testChange("sourceFirmProfileId", value);
    this.sourceFirmProfileId = value;
  }
  public int getSourceFirmProfileId(){  return this.sourceFirmProfileId;  }

  public void setSystemTypeId(int value)
  {
    this.testChange("systemTypeId", value);
    this.systemTypeId = value;
  }
  public int getSystemTypeId(){  return this.systemTypeId;  }

  public void setSourceOfBusinessCode(String value)
  {
    this.testChange("sourceOfBusinessCode", value);
    this.sourceOfBusinessCode = value;
  }
  public String getSourceOfBusinessCode(){  return this.sourceOfBusinessCode;  }
  //============================================

  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
  //--> Add new field to indicate the Priority of the SOB
  //--> By Billy 28Nov2003
  public void setSourceOfBusinessPriorityId(int value)
  {
    this.testChange("sourceOfBusinessPriorityId", value);
    this.sourceOfBusinessPriorityId = value;
  }
  public int getSourceOfBusinessPriorityId(){  return this.sourceOfBusinessPriorityId;  }
  //==========================================================

  // business source and mailbox/firm/user association methods
  //-- FXLink Phase II --//
  //--> Removed all MailBox and SOBtoFirm releated methods
  //--> By Billy 05Nov2003

  public void createSourceUserAssoc(int sourceUserProfileId, java.util.Date effectiveDate)throws Exception
  {
      String sql = "Insert into SourceToUserAssoc( SourceOfBusinessProfileId, userProfileId, EffectiveDate" +
         " , InstitutionProfileId ) Values ( " + pk.getId() + "," 
         + sourceUserProfileId + "," + sqlStringFrom(effectiveDate) + ", " 
         + srk.getExpressState().getDealInstitutionId() + ")";
      try
      {
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceOfBusinessProfile Entity - createSourceUserAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }
  public void updateSourceUserAssoc(int userProfileId, java.util.Date effectiveDate ) throws Exception
  {
      String sql = "Select userProfileId, effectiveDate from SourceToUserAssoc where SourceOfBusinessProfileId = " +
                 pk.getId();
      int  prevProfileId = 0;
      java.util.Date prevDate = null;
      boolean  gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for( ;jExec.next(key); )
          {
              prevProfileId = jExec.getInt(key,1);
              prevDate = jExec.getDate(key,2);

              if(prevProfileId == userProfileId  && !isNewDate( prevDate, effectiveDate))
              {
                  jExec.closeData(key);
                  return;
              }
              gotRecord = true;
              break;
          }
          jExec.closeData(key);

          if(gotRecord)
          {
               String updateSql = "Update SourceToUserAssoc set userProfileId = " +
                 userProfileId + ", effectiveDate = " + sqlStringFrom(effectiveDate) +
                 " where SourceOfBusinessProfileId = " + pk.getId();

               jExec.executeUpdate(updateSql);
          }
          else
          {
               createSourceUserAssoc(userProfileId, effectiveDate);
          }
      }
      catch (Exception e)
      {
        Exception ce = new Exception("SourceOfBusinessProfile Entity - updateSourceUserAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }

  public void deleteSourceUserAssoc() throws Exception
  {
      String deleteSql = "Delete from SourceToUserAssoc where SourceOfBusinessProfileId = " +
          pk.getId();
      try
      {
        jExec.executeUpdate(deleteSql);
      }
      catch(Exception e)
      {
        Exception ce = new Exception("SourceOfBusinessProfile Entity - deleteSourceUserAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }

  private boolean isNewDate(java.util.Date prevDate, java.util.Date effectiveDate)
  {
    //// SYNCADD. Bug if either date is NULL -- Fixed by BILLY 06June2002
    if(prevDate == null || effectiveDate == null)
      return true;
    //========================================================================

    // To compare only the Date only i.e. set time to 00:00:00
    java.util.Date thePrevDate = new java.util.Date(prevDate.getYear(), prevDate.getMonth(), prevDate.getDate());
    java.util.Date theEffDate = new java.util.Date(effectiveDate.getYear(), effectiveDate.getMonth(), effectiveDate.getDate());

    if(thePrevDate.compareTo(effectiveDate) != 0 )
      return true;

    return false;
  }

  //-- FXLink Phase II --//
  // Changed to check unique by SystemTypeId + sourceOfBusinessCode
  //--> By Billy 05Nov2003
  public boolean isDupeSystemCode(String systemCode, int SystemTypeId) throws RemoteException, FinderException
  {
    return isDupeSystemCode(systemCode, SystemTypeId, 0);
  }
  public boolean isDupeSystemCode(String systemCode, int SystemTypeId, int mySOBId) throws RemoteException, FinderException
   {
      String sql = "Select SourceOfBusinessProfileId from SourceOfBusinessProfile where "
                  + "SourceOfBusinessProfileId <> " + mySOBId
                  + " and sourceOfBusinessCode = '" + systemCode + "'"
                  + " and SystemTypeId = '" + SystemTypeId + "'";

      try
      {
          int key = jExec.execute(sql);
          for (; jExec.next(key); )
          {
logger.debug("BILLY==> isDupeSystemCode dupicated records found!!");
            jExec.closeData(key);
            return true;
          }

          jExec.closeData(key);
          return false;

        }
        catch (Exception e)
        {
          logger.error("Error in finder - isDupeSystemCode : " + sql);
          logger.error(e);
          return false;
        }
   }

  //-- FXLink Phase II --//
  //--> Modified to check the System Property instead
  //--> By Billy 06Nov2003
  public boolean isDupeBusId(String SOBPBusinessId, int SourceOfBusinessProfileId) throws RemoteException, FinderException
  {
    String sql = "Select SourceOfBusinessProfileId from sourceofbusinessprofile where "
                + "SourceOfBusinessProfileId <> " + SourceOfBusinessProfileId
                + " and sobpbusinessid = '" + SOBPBusinessId + "'";

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          jExec.closeData(key);
          return true;
        }
        jExec.closeData(key);
        return false;

      }
      catch (Exception e)
      {
        logger.error("Error in finder - isDupeBusId : " + sql);
        logger.error(e);
        return false;
      }
  }

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> new methods to handle SOB to Group association
  //--> By Billy 06Aug2003
   public void createSourceGroupAssoc(int groupProfileId, java.util.Date effectiveDate)throws Exception
  {
       /**
        * BEGIN FIX FOR FXP21176
        * Mohan V Premkumar
        * 04/11/2008
        */
      String sql = "Insert into SourceToGroupAssoc( SourceOfBusinessProfileId, "
        + "groupProfileId, EffectiveDate, InstitutionProfileId ) Values ( " 
        + pk.getId() + ", " + groupProfileId + ", " + sqlStringFrom(effectiveDate) + ", "
        + srk.getExpressState().getDealInstitutionId() + " )";
      /**
       * END FIX FOR FXP21176
       */
      try
      {
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("SourceOfBusinessProfile Entity - createSourceGroupAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }

  public void updateSourceGroupAssoc(int groupProfileId, java.util.Date effectiveDate ) throws Exception
  {
      String sql =
        "Select groupProfileId, effectiveDate from SourceToGroupAssoc where SourceOfBusinessProfileId = " + pk.getId();
      int  prevProfileId = 0;
      java.util.Date prevDate = null;
      boolean  gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for( ;jExec.next(key); )
          {
              prevProfileId = jExec.getInt(key,1);
              prevDate = jExec.getDate(key,2);

              if(prevProfileId == groupProfileId  && !isNewDate( prevDate, effectiveDate))
              {
                  jExec.closeData(key);
                  return;
              }
              gotRecord = true;
              break;
          }
          jExec.closeData(key);

          if(gotRecord)
          {
               String updateSql = "Update SourceToGroupAssoc set groupProfileId = " +
                 groupProfileId + ", effectiveDate = " + sqlStringFrom(effectiveDate) +
                 " where SourceOfBusinessProfileId = " + pk.getId();

               jExec.executeUpdate(updateSql);
          }
          else
          {
               createSourceGroupAssoc(groupProfileId, effectiveDate);
          }
      }
      catch (Exception e)
      {
        Exception ce = new Exception("SourceOfBusinessProfile Entity - updateSourceGroupAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }

  public void deleteSourceGroupAssoc() throws Exception
  {
      String deleteSql = "Delete from SourceToGroupAssoc where SourceOfBusinessProfileId = " +
          pk.getId();
      try
      {
        jExec.executeUpdate(deleteSql);
      }
      catch(Exception e)
      {
        Exception ce = new Exception("SourceOfBusinessProfile Entity - deleteSourceGroupAssoc() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
  }
  //====================================================================

  //-- FXLink Phase II --//
  // New method to get the ChannelMedia from SystemType
  //--> By Billy 05Nov2003
  // added SessionResourceKit as a parameter for ML
  static public String getChannelMedia(SessionResourceKit srkTemp, int systemTypeId)
  {
    String tmp = PicklistData.getMatchingColumnValue(srkTemp.getExpressState().getDealInstitutionId(),
                                                     "SystemType", systemTypeId, "ChannelMedia");
    if(tmp != null)
      return tmp;
    else
      return "";
  }
  //===================================================

  public int getInstitutionProfileId()
  {
    return institutionProfileId;
  }

  public void setInstitutionProfileId(int institutionProfileId)
  {
      testChange("institutionProfileId", institutionProfileId);
    this.institutionProfileId = institutionProfileId;
  }

  public String getSobLicenseRegistrationNumber()
  {
    return SOBLicenseRegistrationNumber;
  }

  public void setSobLicenseRegistrationNumber(String sobLcenseRegistrationNumber)
  {
      testChange("SOBLicenseRegistrationNumber", sobLcenseRegistrationNumber);
      this.SOBLicenseRegistrationNumber = sobLcenseRegistrationNumber;
  }
  
}
