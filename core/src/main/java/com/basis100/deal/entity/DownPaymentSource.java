package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.DownPaymentSourcePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class DownPaymentSource extends DealEntity
{
   protected int     downPaymentSourceId;
   protected int     downPaymentSourceTypeId;
   protected int     dealId;
   protected double  amount;
   protected String  DPSDescription;
   protected int     copyId;
   protected int 	instProfileId;
   protected DownPaymentSourcePK pk;

   public DownPaymentSource(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
   {
      super(srk,dcm);
   }

   public DownPaymentSource(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId ) throws RemoteException, FinderException
   {
      super(srk,dcm);

      pk = new DownPaymentSourcePK(id, copyId);

      findByPrimaryKey(pk);
   }



   private DownPaymentSourcePK createPrimaryKey(int copyId)throws CreateException
   {
     String sql = "Select DownPaymentSourceseq.nextval from dual";
     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }

         jExec.closeData(key);

         if( id == -1 ) throw new Exception();
      }
      catch (Exception e)
      {
            CreateException ce = new CreateException("DownPaymentSource Entity create() exception getting DownPaymentSourceId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;

      }

      return new DownPaymentSourcePK(id, copyId);
   }


   public DownPaymentSource findByPrimaryKey(DownPaymentSourcePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from DownPaymentSource " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
  }

  public Collection<DownPaymentSource> findByDeal(DealPK pk) throws Exception
  {

    Collection found = new ArrayList();

    String sql = "Select * from DownPaymentSource " ;
          sql +=  pk.getWhereClause();

     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          DownPaymentSource dps = new DownPaymentSource(srk,dcm);
          dps.setPropertiesFromQueryResult(key);
          dps.pk = new DownPaymentSourcePK(dps.getDownPaymentSourceId(),dps.getCopyId());

          found.add(dps);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while fetching Deal::DownPaymentSources");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

      return found;
  }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     this.setDownPaymentSourceId(jExec.getInt(key,"DOWNPAYMENTSOURCEID"));
     this.setDownPaymentSourceTypeId(jExec.getInt(key,"DOWNPAYMENTSOURCETYPEID"));
     this.setDealId(jExec.getInt(key,"DEALID"));
     this.setAmount(jExec.getDouble(key,"AMOUNT"));
     this.setDPSDescription(jExec.getString(key,"DPSDESCRIPTION"));
     this.copyId = jExec.getInt(key, "COPYID" );
     this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

    /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }


  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();
      int ret = doPerformUpdate(this, clazz);

      if(dcm != null && ret >0 ) dcm.inputEntity(this);

      return ret;
  }

  public String getEntityTableName()
  {
      return "DownPaymentSource";
  }


		/**
     *   gets the pk associated with this entity
     *
     *   @return a DownPaymentSourcePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }


    public int getDownPaymentSourceId()
   {
      return this.downPaymentSourceId ;
   }


   /**
    * <pre>
    *    create a DownPaymentSource record given a DownPaymentSourcePK primary key.
    *     these create methods assume a standard list of records and are therefore
    *     a utility for record entry.
    *    @param pk - the primary key for this record
    *    @return this instance
    * </pre>
    */
   public DownPaymentSource create(DealPK dpk)throws RemoteException, CreateException
   {
      pk = createPrimaryKey(dpk.getCopyId());

      String sql = "Insert into DownPaymentSource(" + pk.getName() +  " , " + dpk.getName() + ", copyId, INSTITUTIONPROFILEID) Values ( " +
       pk.getId() + ", " + dpk.getId() + ", " + pk.getCopyId() + ", " + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        this.trackEntityCreate (); // track the creation

      }
      catch (Exception e)
      {

        CreateException ce = new CreateException("DownPaymentSource Entity - DownPaymentSourceType - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      this.pk = pk;
      srk.setModified(true);

      if(dcm != null)
        dcm.inputEntity(this);

      return this;
  }

  public void setDealId(int id)
  {
   this.testChange ( "dealId", id ) ;
   this.dealId = id;
  }


  public void setDownPaymentSourceId(int id)
  {
   this.testChange ("downPaymentSourceId", id);
   this.downPaymentSourceId = id;
  }

  public void setDownPaymentSourceTypeId(int id)
  {
   this.testChange ("downPaymentSourceTypeId", id );
   this.downPaymentSourceTypeId = id;
  }
  public void setAmount(double amount)
  {
   this.testChange ("amount", amount);
   this.amount = amount;
  }

  public void setDPSDescription(String desc)
  {
   this.testChange ("DPSDescription", desc);
   this.DPSDescription = desc;
  }

  public int getDownPaymentSourceTypeId(){ return this.downPaymentSourceTypeId;}
  public double getAmount(){ return this.amount;}
  public String getDPSDescription(){ return this.DPSDescription;}
  public int getDealId(){ return this.dealId;}
  public int getCopyId(){ return this.copyId;}


  public Vector findByMyCopies() throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from DownPaymentSource where downPaymentSourceId = " +
       getDownPaymentSourceId() + " AND copyId <> " + getCopyId();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            DownPaymentSource iCpy = new DownPaymentSource(srk,dcm);

            iCpy.setPropertiesFromQueryResult(key);
            iCpy.pk = new DownPaymentSourcePK(iCpy.getDownPaymentSourceId(), iCpy.getCopyId());

            v.addElement(iCpy);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
        FinderException fe = new FinderException("DownPaymentSource - findByMyCopies exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
      return v;
  }

  public int getClassId()
  {
   return ClassId.DOWNPAYMENTSOURCE;
}

  public boolean isAuditable()
  {
    return true;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof DownPaymentSource )
      {
        DownPaymentSource oa = ( DownPaymentSource ) o;
        if  ( ( this.downPaymentSourceId  == oa.getDownPaymentSourceId () )
          && ( this.copyId  ==  oa.getCopyId ()  )  )
            return true;
      }
    return false;
  }

  protected EntityContext getEntityContext() throws RemoteException
  {
    EntityContext ctx = this.entityContext;
    try
    {
      ctx.setContextText ( this.DPSDescription ) ; // Downpayment Source Description
      ctx.setContextSource ( String.valueOf (  this.amount  ) ) ;

      //Modified by Billy for performance tuning -- By BILLY 28Jan2002
      //Deal deal = new Deal( srk, null , this.dealId , this.copyId  );
      //if ( deal == null )
      //  return ctx;
      //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
      Deal deal = new Deal(srk, null);
      String  sc = String.valueOf (deal.getScenarioNumber(this.dealId , this.copyId)) +
          deal.getCopyType(this.dealId , this.copyId);
      // ===================================================================================

      ctx.setScenarioContext ( sc );

      ctx.setApplicationId ( this.dealId ) ;
    } catch ( Exception e)
    {
      if ( e instanceof FinderException )
         return ctx;
      throw ( RemoteException) e;

    } // end of try.. catch ...
    return ctx;
  } // --------------- End of getEntityContext ----------------------
  
    public int getInstProfileId() {
    	return instProfileId;
    }
    
    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
    }
}
