package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.IncomePK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.util.StoredProcCopyMgt;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;


public class MasterDeal  extends DealEntity
{
  public static final int FXLINKSCHEMA_FCX_V_1_0=1;
  static int INIT_COPY_NUMBER = 1;  //one based copy scheme

  protected int dealId;
  protected int numberOfCopies;
  protected int goldCopyId = 1; //one based copy scheme
  protected int lastCopyId;
  protected int institutionId; // added for ML - July 6, 2007 Midori
  //Three new fields for Data Alignment
  protected String FXLinkPOSChannel;
  protected String FXLinkUWChannel;
  protected int FXLinkSchemaId;
  
  
  protected Map srcCopyValueCache = new HashMap();

  protected MasterDealPK pk;

  protected boolean goldSanityCheckEnabled = true;

  public void setGoldSanityCheckEnabled(boolean b)
  {
    goldSanityCheckEnabled = b;
  }



  public MasterDeal(SessionResourceKit srk, CalcMonitor calc)throws RemoteException, FinderException
  {
      super(srk,calc);
  }


  public MasterDeal(SessionResourceKit srk, CalcMonitor dcm, int id) throws RemoteException, FinderException
  {
      super(srk,dcm);

      pk = new MasterDealPK(id);

      findByPrimaryKey(pk);
  }

  /**
   *
   * !!!!! Entity specific version does not register as change in resource kit !!!!!!
   *
   * ejbStore stores the present state of the entity in the database. This accomplished
   * whith a call to the performUpdate method implentation of the calling class.
   *
   */
   public void ejbStore() throws RemoteException
   {
     logger.debug("MasterDeal::ejbStore:Stamp ");

      if ( !isDataChanged() ) return;

      //////////    srk.setModified(true);
      try
      {
        performUpdate();
        this.trackEntityChange ();  // Track all the change

        //4.4 Entity Cache
        ThreadLocalEntityCache.writeToCache(this, this.getPk());
      }
      catch (Exception e)
      {
        RemoteException re = new RemoteException(" MasterDeal Entity - ejbStore() exception - msg: ");
        logger.error(re.getMessage() + "\n" + e.getMessage());
        logger.error(e);

        throw re;
      }
  }



  /**
  *  creates a copy of the Deal tree identified by the goldCopyId.
  *  @return the copyId created - the lastCopyId
  */
  public int copy()throws Exception
  {
    return copy(this.getGoldCopyId());
  }

  /**
   *  creates a copy of the Deal tree identified by the parameter.
   *  @return the copyId created - the lastCopyId
   */
  public int copy(int sourceCopyId) throws DealCopyException
  {
    try
    {
		  //4.4 Copy management improvement, June 2010 -- start
		  if("Y".equals(PropertiesCache.getInstance().getProperty(
					institutionId, "com.basis100.deal.entity.copyWithStoredProc", "Y"))){
			  StoredProcCopyMgt.createCopy(srk, this, sourceCopyId, lastCopyId + 1);
		  }else{
      Deal deal = new Deal(srk,dcm,this.getDealId(),sourceCopyId);
      deal.copyEntity(this, this, this.getEntityTableName(), null, lastCopyId + 1);
		  }
		  //4.4 Copy management improvement, June 2010 -- end
		  
      setLastCopyId(lastCopyId + 1);
      setNumberOfCopies(numberOfCopies + 1);
    }
    catch(Exception e)
    {
    	logger.error(e);
		  throw new DealCopyException(e);
    }

    return lastCopyId;
  }

  public int copyFrom(MasterDeal srceMasterDeal, int srceCopyId)throws Exception
  {
    int destCid = -1;

    if(srceMasterDeal.getLastCopyId() > this.getLastCopyId())
      destCid = srceMasterDeal.getLastCopyId() + 1;
    else
      destCid = getLastCopyId() + 1;

	  //4.4 Copy management improvement, June 2010 -- start
	  if("Y".equals(PropertiesCache.getInstance().getProperty(
				institutionId, "com.basis100.deal.entity.copyWithStoredProc", "Y"))){
		  StoredProcCopyMgt.duplicateDealTree(
				  srk, srceMasterDeal, srceCopyId, this, destCid);
	  }else{
    Deal srceDeal = new Deal(srk, dcm,srceMasterDeal.getDealId(), srceCopyId);
    srceDeal.copyEntity(srceMasterDeal, this, this.getEntityTableName(), null ,destCid);
	  }
	  //4.4 Copy management improvement, June 2010 -- end
    setLastCopyId(destCid);
    setNumberOfCopies(numberOfCopies + 1);

    return destCid;
  }

  public void deleteCopy(int copyId) throws DealCopyException
  {
    try
    {
		  //4.4 Copy management improvement, June 2010 -- start
		  if("Y".equals(PropertiesCache.getInstance().getProperty(
					institutionId, "com.basis100.deal.entity.copyWithStoredProc", "Y"))){
			  StoredProcCopyMgt.deleteCopy(srk, this, copyId);
		  }else{
       Deal remDeal = new Deal(srk,dcm,dealId,copyId);
       remDeal.remove((numberOfCopies == 1));
		  }
		  //4.4 Copy management improvement, June 2010 -- end
       setNumberOfCopies(numberOfCopies - 1);
      }
      catch (Exception ex)
      {
        String msg = "Failed to delete (copy Id = " + copyId + ") from MasterDeal (deal Id = " + dealId + ")";
        logger.error(msg);
        logger.error(ex);
		  throw new DealCopyException(msg, ex);
      }
      
    /* 
    //LEN537179, Sep 13, 2011, start
    //1. Express doesn't have to delete MasterDeal
    //2. numberOfCopies is often wrong thus we cannot rely on it 
    if(this.numberOfCopies < 1)
    {
      try
      {
        this.ejbRemove();
      }
		  catch(RemoteException ex)
      {
        String msg = "MasterDeal@deleteCopy: Failed to remove self after last copy deleted. " +
                      "Possible Orphaned MasterDeal record deal Id = " + dealId;
        logger.error(msg);
        logger.error(ex);
			  throw new DealCopyException(msg, ex);
      }
    }
    // LEN537179, Sep 13, 2011, end 
    */
  }




  /**
   *  Removes this MasterDeal and all of its children from the data base.
   */
  public void removeDeal()throws Exception
  {
     Collection copies = getCopyIds();
     Iterator it = copies.iterator();
     Integer current = null;

     if(copies.size() != numberOfCopies)
      throw new Exception("Master Deal record data is corrupt - number of copies incorrect");

     try
     {
         while(it.hasNext())
         {
           current = (Integer)it.next();

           // following removes self after deleting last existing copy
           this.deleteCopy(current.intValue());
         }
     }
     catch (Exception e)
     {
        String msg = "MasterDeal@removeDeal: Failed to remove (entire deal)";
        logger.error(msg);
        logger.error(e);
        throw new Exception(msg);
     }
 }

/*
  public boolean hasCopy(int copyNumber)
  {
     if(copyNumber > lastCopyId || copyNumber < 1)
     {
       return false;
     }

     Deal copy = null;

     try
     {
       DealPK pk = new DealPK(this.dealId,copyNumber);
       copy = new Deal(this.srk);
       copy = copy.findByPrimaryKey(pk);
     }
     catch(Exception e)
     {
       return false;
     }

     if(copy != null) return true;

     return false;
  }
*/

  public Collection getCopyIdsSorted()
  {
     String sql = "select copyId from deal where dealId = " + this.dealId + " order by copyId " ;

     List copies = new ArrayList();

     try
     {
        int key = jExec.execute(sql);
        int current = 0;

        for (; jExec.next(key); )
        {
           current = jExec.getInt(key,1);
           copies.add(new Integer(current));
        }

        jExec.closeData(key);
     }
     catch (Exception e)
     {
        copies = new ArrayList();

        logger.error("query error - sql: " + sql);
        logger.error(e);
     }

     return copies;
  }
  /**
  * Gets a collection of Integers representing copyIds for
  * those deals in default schema only!
  *
  */
  public Collection<Integer> getCopyIds()
  {
     String sql = "select copyId from deal where dealId = " + this.dealId;

     List<Integer> copies = new ArrayList<Integer>();

     try
     {
        int key = jExec.execute(sql);
        int current = 0;

        for (; jExec.next(key); )
        {
           current = jExec.getInt(key,1);
           copies.add(new Integer(current));
        }

        jExec.closeData(key);
     }
     catch (Exception e)
     {
        copies = new ArrayList<Integer>();

        logger.error("query error - sql: " + sql);
        logger.error(e);
     }

     return copies;
  }


  public MasterDeal findByPrimaryKey(MasterDealPK pk) throws RemoteException, FinderException
  {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from " + "MasterDeal" + " " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "MasterDeal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("MasterDeal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
   }

  /**
   * Return the next available scenario number for the deal.
   **/

  public int nextScenarioNumber() throws RemoteException
  {
    String sql = "select max(scenarioNumber) from deal where dealId = " +
                  this.getDealId() + " AND copyType = 'S'";

    int scenarioNumber = 0;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        scenarioNumber = jExec.getInt(key, 1);
        break;
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
        RemoteException re = new RemoteException("MasterDeal Entity - nextScenarioNumber: exception");
        logger.error(re.getMessage());
        logger.error(e);
        throw re;
    }

    return scenarioNumber + 1;
  }

  public Deal create(MasterDealPK pk) throws RemoteException, CreateException
  {
    String sql = "Insert into MasterDeal(" + pk.getName() +
     ",numberOfCopies ,goldCopyId,lastCopyId, InstitutionProfileId, FXLinkPOSChannel, FXLinkUWChannel, FXLinkSchemaId) Values ( " +
     pk.getId() + ", " +
     INIT_COPY_NUMBER + ", " +
     INIT_COPY_NUMBER + ", " +
     INIT_COPY_NUMBER + ", " +
     srk.getExpressState().getDealInstitutionId() + 
     ", '', '', 0" +  " )"; //create new DA fields as empty string, empty string, zero.

    try
    {
      jExec.executeUpdate(sql);

      findByPrimaryKey(pk);

      this.pk = pk;
      DealPK dealPk =
         new DealPK(this.dealId, this.lastCopyId);

      Deal deal = new Deal(srk,dcm);
      return deal.create(dealPk);
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Master Entity - Create Exception");
      logger.error(ce.getMessage());
      logger.error(e);
      throw ce;
    }
  }

  /**
   * <p>createPrimaryKey</p>
   * modified dealseq to be by institutions for ML
   * XXX_deqlsel: XXX is institution short name
   * @param institutionProfileId
   * @return
   * @throws CreateException
   */
  public MasterDealPK createPrimaryKey(int institutionProfileId)throws CreateException
  {
    try
    {
      InstitutionProfile institution = new InstitutionProfile(srk);
      institution = institution.findByPrimaryKey(new InstitutionProfilePK(institutionProfileId));
      int nextId = getNextId(institution.getInstitutionAbbr());
      return new MasterDealPK(nextId);
    }
    catch(RemoteException re)
    {
      logger.error(re);
      throw new CreateException("failed to create MasterDealPK institutionProfileId = " + institutionProfileId);
    }
    catch(FinderException fe)
    {
      logger.error(fe);
      throw new CreateException("failed to create MasterDealPK institutionProfileId = " + institutionProfileId);
    }
  }
  
  private int getNextId(String institutionAbbr)throws CreateException
  {

    String sql = "Select DEAL_" + institutionAbbr + "_SEQ.nextval from dual";
     int id = -1;

     try
     {
       int key = jExec.execute(sql);

       for (; jExec.next(key);  )
       {
           id = jExec.getInt(key,1);
       }

       jExec.closeData(key);

       if( id == -1 ) throw new Exception();
    }
    catch (Exception e)
    {
        CreateException ce =
          new CreateException("MasterDeal Entity create() exception getting new PK from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
    }

    return id;
  }


  protected int performUpdate()  throws Exception
  {
      return (doPerformUpdate(this, this.getClass()));
  }

  public void checkCopyId( int copyId) throws DealCopyException
  {
      if( copyId <0 || copyId > lastCopyId )
      throw new DealCopyException("Copy ID is NOT Correct");
  }

  public int getDealId(){return dealId;}

  public void setDealId(int id )
  {
    this.testChange ("dealId", id ) ;
    this.dealId = id;
  }

  public IEntityBeanPK getPk(){return (IEntityBeanPK)this.pk;}

  public void setGoldCopyId( int goldCopyId ) throws Exception
  {
	  if (goldSanityCheckEnabled == true)
		  checkCopyId(goldCopyId);
	  this.testChange ("goldCopyId", goldCopyId ) ;
	  this.goldCopyId = goldCopyId;
  }

  public int getGoldCopyId()
  {
    return goldCopyId;
  }

  public void setLastCopyId( int lastCopyId )
  {
   this.testChange ("lastCopyId", lastCopyId ) ;
   this.lastCopyId = lastCopyId;
  }
  public int  getLastCopyId(){ return lastCopyId; }

  public void setNumberOfCopies(int numberOfCopies)
  {
    this.testChange ("numberOfCopies", numberOfCopies ) ;
    this.numberOfCopies = numberOfCopies;
  }

  public int  getNumberOfCopies(){return numberOfCopies;}


  public void ejbRemove() throws RemoteException
  {
    IEntityBeanPK pk = getPk();
    String sql = "Delete From MasterDeal " + pk.getWhereClause();

    try
    {
      removeSons();

      jExec.executeUpdate(sql);
    }
    catch (Exception e)
    {
      String msg =  "Exception removing MasterDeal record with deal id = " + this.getDealId();

      logger.error(msg);
      logger.error(e);

      throw new RemoteException(msg);
    }
  }

  protected void removeSons() throws RemoveException
  {
	  removeSons(srk, pk);
  }

  protected void removeSons(SessionResourceKit srk, MasterDealPK pk) throws RemoveException
  {
    try
    {
		  new DealNotes(srk).ejbRemove(pk, false);
		  new CreditBureauReport(srk).ejbRemove(pk, false);
		  new DealHistory(srk).ejbRemove(pk, false);
		  new PartyProfile(srk).removeAssocForDeal(pk);
		  new DisclosureQuestion(srk).ejbRemove(pk, false);	
		  new DocumentQueue(srk).ejbRemove(pk, false);	
		  new QualifyDetail(srk).ejbRemove(pk, false);
    }
    catch (Exception e)
    {
      String msg =  "Exception @MasterDeal.removeSons";
      logger.error(msg);
      logger.error(e);
		  throw new RemoveException(msg, e);
    }
  }

  //
  // Implementation
  //
  private void setPropertiesFromQueryResult(int key) throws Exception
  {
         setDealId(jExec.getInt(key, "dealId"));
         setNumberOfCopies(jExec.getInt(key, "numberOfCopies"));

         // ALERT -- there is a check when gold copy set to ensure number is not greater then
         //          last copy id - hence we must call in a specific order when loading to avoid
         //          an exception. Hence the convoluted code!. BJH

         int goldId = jExec.getInt(key, "goldCopyId");
         int lastId = jExec.getInt(key, "lastCopyId");
         int institutionId = jExec.getInt(key, "institutionProfileId");

         setLastCopyId           (lastId);
         setGoldCopyId           (goldId);
         setInstitutionId(institutionId);

         //new fields for Data Alignment...
         setFXLinkPOSChannel(jExec.getString(key, "FXLinkPOSChannel"));
         setFXLinkUWChannel(jExec.getString(key, "FXLinkUWChannel"));
         setFXLinkSchemaId(jExec.getInt(key, "FXLinkSchemaId"));
         
  }

  /**
   *  Creates a Deal tree with subtree components.
   *  @return the MasterDeal created
   */
  public static MasterDeal createDefaultTree(SessionResourceKit srk, CalcMonitor dcm) throws Exception
  {
    MasterDeal root = new MasterDeal(srk,dcm);
    MasterDealPK pk = root.createPrimaryKey(srk.getExpressState().getDealInstitutionId());

    Deal deal = root.create(pk);
    DealPK dealPK = (DealPK)deal.getPk();

    // Set default values for a new deal
    // Set AmortizationTerm to 25 Years
    deal.setAmortizationTerm(300);
    // Set ActualPaymentTerm to 5 Years
    deal.setActualPaymentTerm(60);

    deal.setNumberOfBorrowers(1);

    deal.setNumberOfProperties(1);

    deal.ejbStore();

    root.addDefaultChildren(dealPK);

    return root;
  }

  private void addDefaultChildren(DealPK dealPK)throws Exception
  {
      //create borrower subtree
    Borrower borrower = new Borrower(srk,dcm);
    borrower = borrower.create(dealPK);

    borrower.setPrimaryBorrowerFlag("Y");
    borrower.setBorrowerNumber(1);
    borrower.ejbStore();

    BorrowerPK bpk = (BorrowerPK)borrower.getPk();
////logger.debug("DealEntity@BorrowerPK: " + bpk.getId());

    BorrowerAddress baddr = new BorrowerAddress(srk,dcm);
    baddr = baddr.create(bpk);

    Asset asset = new Asset(srk,dcm);
    asset = asset.create(bpk);

    Liability liab = new Liability(srk, dcm);
    liab = liab.create(bpk);

    Income inc = new Income(srk,dcm);
    inc = inc.create(bpk);

    EmploymentHistory eh = new EmploymentHistory(srk);
    eh = eh.create((IncomePK)inc.getPk());

    //--DJ_LDI_CR--start--//
    //--> Bug fix : we don't need to create the default LifeDisabilityPremium
    //--> By Billy 13May2004
    /*
    //Create Life and Disability Premiums Entity
    LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk, dcm);
    ldp = ldp.create(bpk, 1);

////logger.debug("MasterDeal@LifeDisabilityInsCopyId_1: " + ldp.copyId);
////logger.debug("MasterDeal@LifeDisInsTableName_1: " + ldp.getEntityTableName());
////logger.debug("MasterDeal@LifeDisRateId_1: " + ldp.getLDInsuranceRateId());

    LifeDisabilityPremiums ldp2 = new LifeDisabilityPremiums(srk, dcm);
    ldp2 = ldp2.create(bpk, 2);

////logger.debug("MasterDeal@LifeDisabilityInsCopyId_2: " + ldp.copyId);
////logger.debug("MasterDeal@LifeDisInsTableName_2: " + ldp.getEntityTableName());
////logger.debug("MasterDeal@LifeDisRateId_2: " + ldp.getLDInsuranceRateId());
    */
    //--DJ_LDI_CR--end--//

    //property subtree
    Property property = new Property(srk,dcm);
    property = property.create(dealPK);

    property.setPrimaryPropertyFlag("Y");
    property.setPropertyOccurenceNumber(1);
    property.ejbStore();

    PropertyPK ppk = (PropertyPK)property.getPk();

    PropertyExpense pexp = new PropertyExpense(srk,dcm);
    pexp = pexp.create(ppk);


    //Other Deal sub entities
    DownPaymentSource dps = new DownPaymentSource(srk,dcm);
    dps = dps.create(dealPK);
  }


  public String getEntityTableName()
  {
     return "MasterDeal";
  }


  public void cacheSrcCopyValue(String field, String srcval, String destval)
  {
     srcCopyValueCache.put(field.toLowerCase() + srcval, destval);
  }

  public String getCachedCopyValue(String field, String srcval)
  {
    return (String)srcCopyValueCache.get(field.toLowerCase() + srcval);
  }



  public int getInstitutionId()
  {
    return institutionId;
  }

  public void setInstitutionId(int institutionId)
  {
    this.institutionId = institutionId;
  }

  //three new fields for DA...
  public String getFXLinkPOSChannel()
  {
	  return FXLinkPOSChannel;
  }
  
  public void setFXLinkPOSChannel(String POSchannel )
  {
    this.testChange ("FXLinkPOSChannel", POSchannel ) ;
    this.FXLinkPOSChannel = POSchannel;
  }

  public String getFXLinkUWChannel()
  {
	  return FXLinkUWChannel;
  }
  
  public void setFXLinkUWChannel(String UWchannel )
  {
    this.testChange ("FXLinkUWChannel", UWchannel ) ;
    this.FXLinkUWChannel = UWchannel;
  }
  public int getFXLinkSchemaId()
  {
	  return FXLinkSchemaId;
  }
  
  public void setFXLinkSchemaId(int schemaId )
  {
    this.testChange ("FXLinkSchemaId", schemaId ) ;
    this.FXLinkSchemaId = schemaId;
  }

}
