/*
 * ServiceResponseQueue.java
 * 
 * Entity class (connectivity with AVM services)
 * 
 * Created: 19-June-2006
 * Author:  Asif Atcha
 * 
 * TODO: test create method.
 *       
 */
package com.basis100.deal.entity;

import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ServiceResponseQueuePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

/**
 * Corresponds to the ServiceResponseQueue DB table.
 * 
 * @author AAtcha
 */
public class ServiceResponseQueue extends DealEntity {

  protected int    serviceResponseQueueId;
  protected int    dealId;
  protected int    copyId;
  protected int    serviceProductId;
  protected int    processStatusId;
  protected int    retryCounter;
  protected String responseXML;	   
  
  protected Date   responseDate;
  protected String channelTransactionKey;
  protected String requestType;
  protected int    institutionProfileId;
  protected Date lastProcessDate;  
 
  private ServiceResponseQueuePK pk;

  /**
   * @param srk SessionResourceKit to use
   * @throws RemoteException
   */
  public ServiceResponseQueue(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
  {
	    super(srk,dcm);
	  }
  /**
   * @param srk SessionResourceKit to use
   * @throws RemoteException
   */
  public ServiceResponseQueue(SessionResourceKit srk,CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
  {
      super(srk,dcm);

      pk = new ServiceResponseQueuePK(id);

      findByPrimaryKey(pk);
  }
  
  /**
   * @param srk SessionResourceKit to use
   * @param pk Primary Key corresponding to appropriate line in table
   * @throws RemoteException
   */
/*
  public ServiceResponseQueue(SessionResourceKit srk, int id) throws RemoteException, FinderException {
    super(srk);
    pk = new ServiceResponseQueuePK(id);
    findByPrimaryKey(pk);
  }
*/
  /**
   * Find the row corresponding to this PK and populate entity.
   * @param pk The primary key representing this row
   * @return This (now populated) entity.
   * @throws FinderException
   */
 public ServiceResponseQueue findByPrimaryKey(ServiceResponseQueuePK pk) 
      throws RemoteException, FinderException {
	 
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM SERVICERESPONSEQUEUE" + pk.getWhereClause();
     boolean gotRecord = false;
    
    try {
      int key = jExec.execute(sql);
      for (; jExec.next(key);) {
        gotRecord = true;
        this.pk = pk;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }
      
      jExec.closeData(key);
      
      if (gotRecord == false) {
        String msg = "ServiceResponseQueue Entity: @findByQuery(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("ServiceResponseQueue Entity - findByQuery() exception");
      
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);
      
      throw fe;
    }
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }

 
 
    private final static int SECONDS_IN_A_DAY = 24*60*60;
    private final static String NEXT_UNPROCESSED_SQL = "SELECT * FROM (SELECT * FROM SERVICERESPONSEQUEUE WHERE PROCESSSTATUSID = " 
        + ProcessStatus.AVAILABLE + " AND RETRYCOUNTER <= ? AND (LASTPROCESSDATE IS NULL OR ((SYSDATE - LASTPROCESSDATE) > (? / " + SECONDS_IN_A_DAY + "))) " +
        " ORDER BY SERVICERESPONSEQUEUEID ASC) WHERE ROWNUM = 1 ";

    public ServiceResponseQueue findNextUnprocessedResponse(int maxTry, int retryIntervalSec)
            throws Exception, FinderException {

        boolean gotRecord = false;

        try {
            PreparedStatement pstat = jExec.getPreparedStatement(NEXT_UNPROCESSED_SQL);
            pstat.setInt(1, maxTry);
            pstat.setInt(2, retryIntervalSec);
            
            int key = jExec.executePreparedStatement(pstat, NEXT_UNPROCESSED_SQL);
            
            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                this.pk = new ServiceResponseQueuePK(this.getServiceResponseQueueId());
                break;
            }
            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "findNextUnprocessedResponse, unprocessed record not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Exception caught while getting NextUnprocessedResponse");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + NEXT_UNPROCESSED_SQL);
            logger.error(e);
            throw fe;
        }

        return this;
    }
 
    public ServiceResponseQueue findLatestByDealid(int dealId, int serviceProductId) throws FinderException, RemoteException {

        boolean gotRecord = false;

        StringBuffer  sql = new StringBuffer();
        sql.append("SELECT * ");
        sql.append("  FROM SERVICERESPONSEQUEUE ");
        sql.append(" WHERE     DEALID = ").append(dealId);
        sql.append("       AND SERVICEPRODUCTID = ").append(serviceProductId);
        sql.append("       AND PROCESSSTATUSID = ").append(ProcessStatus.AVAILABLE);
        sql.append(" ORDER BY SERVICERESPONSEQUEUEID DESC ");  

        try {
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                this.pk = new ServiceResponseQueuePK(this.getServiceResponseQueueId());
                break;
            }
            jExec.closeData(key);

        } catch (Exception e) {
            String msg = "faild to find ServiceResponseQueue data sql=" + sql.toString();
            logger.error(msg, e);
            throw new RemoteException(e.getMessage());
        }

        if (gotRecord == false) {
            String msg = "no data found";
            logger.error(msg);
            throw new FinderException(msg);
        }
        return this;
    }
 
    public List<ServiceResponseQueue> findAvailableByDealIdAndServProdId(int dealId, int serviceProductId) throws RemoteException {

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * ");
        sql.append("  FROM SERVICERESPONSEQUEUE ");
        sql.append(" WHERE     DEALID = ").append(dealId);
        sql.append("       AND SERVICEPRODUCTID = ").append(serviceProductId);
        sql.append("       AND PROCESSSTATUSID = ").append(ProcessStatus.AVAILABLE);
        sql.append(" ORDER BY SERVICERESPONSEQUEUEID DESC ");

        List<ServiceResponseQueue> list = new ArrayList<ServiceResponseQueue>();

        try {
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {

                ServiceResponseQueue srq = new ServiceResponseQueue(srk, dcm);
                srq.setPropertiesFromQueryResult(key);
                srq.pk = new ServiceResponseQueuePK(srq.getServiceResponseQueueId());
                list.add(srq);
            }
            jExec.closeData(key);

        } catch (Exception e) {
            String msg = "faild to find ServiceResponseQueue data sql=" + sql.toString();
            logger.error(msg, e);
            throw new RemoteException(e.getMessage());
        }
        return list;
    }
 
 
 public int  resetLockedResponse() throws RemoteException { //David: changed R-->r; added s; retrn type void--> int
    
	 StringBuffer sqlb = new StringBuffer(
                   "UPDATE SERVICERESPONSEQUEUE set PROCESSSTATUSID = " + ProcessStatus.AVAILABLE + " where PROCESSSTATUSID = " + ProcessStatus.PROCESSING );
     int result = 0;
     
     try
     {
      String sql;  
      sql = sqlb.toString();  

    //   key = jExec.executeUpdate(sql);
       PreparedStatement pstmt = this.jExec.getCon().prepareStatement(sql);
       pstmt.clearParameters();
       result = pstmt.executeUpdate();
     //  this.trackEntityChange();
 //      this.trackEntityRemoval (); // Track the Removal
     }
     catch  ( Exception e )
     {
       String msg =  "Failed to change record " + pk.getWhereClause();
       RemoteException re = new RemoteException(msg);
       logger.error(re.getMessage());
       logger.error("change sql: ");
       logger.error(e);
 
       throw re;
     }
  
  //   srk.setModified(true);
     return result;
 
 }
 
 
 
 public Collection findByServiceResponseQueueId(int serviceresponsequeueid) throws Exception
 {
   Collection responses = new ArrayList();
   
   String sql = "Select * from ServiceResponseQueue " ;
   sql += "where ServiceResponseQueueId = " + serviceresponsequeueid;
   
   try
   {
     int key = jExec.execute(sql);
     for (; jExec.next(key); )
     {
       ServiceResponseQueue serviceresponse = new ServiceResponseQueue(srk,dcm);
       serviceresponse.setPropertiesFromQueryResult(key);
       serviceresponse.pk = new ServiceResponseQueuePK(serviceresponse.getServiceResponseQueueId());
       responses.add(serviceresponse);
     }
     jExec.closeData(key);
   }
   catch (Exception e)
   {
     Exception fe = new Exception("Exception caught while getting Deal::ServiceResponseQueue");
     logger.error(fe.getMessage());
     logger.error(e);
     throw fe;
   }
   return responses;
 }
 

// *** WARNING ***
// THis method is duplicated with findByPrimaryKey
// should be fixed or deleted.
// Midori Sep 10, 2007
 public Collection findByServiceResponse(ServiceResponseQueuePK rk) throws Exception
 {
   Collection responses = new ArrayList();
   
   String sql = "Select * from ServiceResponseQueue " ;
   sql +=  rk.getWhereClause();
   
   try
   {
     int key = jExec.execute(sql);
     for (; jExec.next(key); )
     {
       ServiceResponseQueue serviceresponse = new ServiceResponseQueue(srk,dcm);
       serviceresponse.setPropertiesFromQueryResult(key);
       serviceresponse.pk = new ServiceResponseQueuePK(serviceresponse.getServiceResponseQueueId());
       responses.add(serviceresponse);
     }
     jExec.closeData(key);
   }
   catch (Exception e)
   {
     Exception fe = new Exception("Exception caught while getting Deal::ServiceResponseQueue");
     logger.error(fe.getMessage());
     logger.error(e);
     throw fe;
   }
   return responses;
 }
 
 /**
  * Returns Collection of <code>ServiceResponseQueue</code> objects with the given service TypeId. 
  * 
  * @param serviceProductId The ServiceProductId to search for.
  * @return Collection of ServiceResponseQueues.
  * @throws Exception
  */
 public Collection findOpenByTransactionkey(String keyId, String requestType) throws Exception {
   Collection responses = new Vector();
   
   StringBuffer sql = new StringBuffer();
   sql.append("SELECT * FROM ServiceResponseQueue WHERE ").
       append("CHANNELTRANSACTIONKEY = '" + keyId + "' and RequestType = '" + requestType + "' and PROCESSSTATUSID = 1");
   
   try {
     int key = jExec.execute(sql.toString());
     
     for(; jExec.next(key); ) {
       ServiceResponseQueue resp = new ServiceResponseQueue(srk,dcm);
       
       resp.setPropertiesFromQueryResult(key);
       resp.pk = new ServiceResponseQueuePK(resp.getServiceResponseQueueId());
       
       responses.add(resp);
     }
     
     jExec.closeData(key);
   } catch (Exception e) {
     Exception fe = new Exception("Exception caught while fetching Responses.");
     
     logger.error(fe.getMessage());
     logger.error(e);

     throw fe;     
   }   
 
   return responses;
 } 
 /**
  * Returns Collection of <code>ServiceResponseQueue</code> objects with the given service TypeId. 
  * 
  * @param serviceProductId The ServiceProductId to search for.
  * @return Collection of ServiceResponseQueues.
  * @throws Exception
  */
 public Collection findByserviceProductId(int requestId) throws Exception {
   Collection responses = new Vector();
   
   StringBuffer sql = new StringBuffer();
   sql.append("SELECT * FROM ServiceResponseQueue WHERE ").
       append("SERVICEPRODUCTID = " + serviceProductId + " order by serviceProductId desc, serviceResponseQueueId desc");
   
   try {
     int key = jExec.execute(sql.toString());
     
     for(; jExec.next(key); ) {
       ServiceResponseQueue resp = new ServiceResponseQueue(srk,dcm);
       
       resp.setPropertiesFromQueryResult(key);
       resp.pk = new ServiceResponseQueuePK(resp.getServiceResponseQueueId());
       
       responses.add(resp);
     }
     
     jExec.closeData(key);
   } catch (Exception e) {
     Exception fe = new Exception("Exception caught while fetching Responses.");
     
     logger.error(fe.getMessage());
     logger.error(e);

     throw fe;     
   }   
 
   return responses;
 }
 
  /**
   * Create a new primarykey from the response sequence.
   * @return
   * @throws CreateException
   */
  public ServiceResponseQueuePK createPrimaryKey(int copyId) throws CreateException {
    String sql = "Select SERVICERESPONSEQUEUESEQ.nextval from dual";
    int id = -1;

    try {
      int key = jExec.execute(sql);

      for(; jExec.next(key); ) {
        id = jExec.getInt(key, 1);
        break;
      }
      jExec.closeData(key);
      if(id == -1) {
        throw new CreateException();
      }
    }
    catch(Exception e) {
      CreateException ce = new CreateException("ServiceResponseQueue createPrimaryKey() :" +
                              "exception: getting ServiceResponseQueueId from sequence");
      logger.error(ce.getMessage());
      logger.error(e);
      throw ce;
    }
   
    return new ServiceResponseQueuePK(id);
  }
  
  /**
   * Create a new ServiceResponseQueue object with minimum fields, insert it into Database, and return.
   * @param serviceProductId
   * @param status
   * @return The newly created ServiceResponseQueue.
   * @throws RemoteException
   * @throws CreateException
   */
  public ServiceResponseQueue create(DealPK dpk,int serviceProductId,
		  int processStatusId,int retryCounter,
          String responseXML, Date responseDate)
throws RemoteException, CreateException 
{
ServiceResponseQueuePK pk = createPrimaryKey(dpk.getCopyId());

return create(pk, dpk, copyId, serviceProductId, processStatusId, retryCounter, responseXML, responseDate);
}
  
  /**
   * Create a new  ServiceResponseQueue  object with minimum fields, insert it into Database, and return.
   * @param pk
   * @param serviceProductId
   * @param status
   * @return The newly created  Service Response Queue .
   * @throws RemoteException
   * @throws CreateException
   */
  public ServiceResponseQueue create(ServiceResponseQueuePK pk, DealPK dpk,int copyId, int serviceProductId,
                         int processStatusId, int retryCounter, String responseXML, Date responseDate)
      throws RemoteException, CreateException 
  {
//	String sql = "Insert into ServiceResponseQueue(" + pk.getName() +  ", serviceProductId ,ResponseXML,status) Values ( " + pk.getId() + ", serviceProductId, empty_clob(), status)";
		
	StringBuffer sqlb = new StringBuffer
    ("INSERT INTO ServiceResponseQueue (SERVICERESPONSEQUEUEID, DEALID, COPYID, "
     + "SERVICEPRODUCTID, PROCESSSTATUSID, RETRYCOUNTER, RESPONSEXML, "
     + "RESPONSEDATE, INSTITUTIONPROFILEID) VALUES (");
    sqlb.append(sqlStringFrom(pk.getId()) + ", ");
    sqlb.append(sqlStringFrom(dpk.getId()) + ", ");
    sqlb.append(sqlStringFrom(dpk.getCopyId()) + ", ");
    sqlb.append(sqlStringFrom(serviceProductId) + ", ");
    sqlb.append(sqlStringFrom(processStatusId) + ", ");
    sqlb.append(sqlStringFrom(retryCounter) + ", ");
    sqlb.append(" empty_clob() " + ", ");
    sqlb.append(" sysdate " + ", ");
    sqlb.append(srk.getExpressState().getDealInstitutionId() + ")");
    
    String sql = new String(sqlb);
    
    try {
    	
        jExec.executeUpdate(sql);
        
        findByPrimaryKey(pk);
        setResponseXML(responseXML);
        trackEntityCreate(); // track the creation
        
    }
    catch (Exception e) {
      CreateException ce = new CreateException("Service Response Queue Entity - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);
      
      throw ce;
    }
    
    srk.setModified(true);
    return this;      
  }
  
  
  /**
   * @return Returns the ServiceResponseQueue's primary key.
   */
  //public IEntityBeanPK getPk() {
  //  return pk;
 // }
  
  /**
   * Test for equality.
   * @param obj The (ServiceResponseQueue) object to test for equality.
   */
  public boolean equals(Object obj) {
    if (obj instanceof ServiceResponseQueue) {
      ServiceResponseQueue otherObj = (ServiceResponseQueue) obj;
      if  (this.serviceResponseQueueId == otherObj.getServiceResponseQueueId()) {
          return true;
      }
    }
    return false;   
  }
  
  private String readResponseXML()
	{
		JdbcExecutor je =  srk.getJdbcExecutor();
		Connection con = je.getCon();
		String outval = "";
		ResultSet rs = null;
		Statement stm = null;
		CallableStatement callableStmt = null;
		try
		{
			String stmt = "Select RESPONSEXML from ServiceResponseQueue ";
			stmt += "where serviceResponseQueueId = " + this.getServiceResponseQueueId();

			oracle.sql.CLOB clob = null;

			stm = con.createStatement() ;
			rs = stm.executeQuery(stmt);

			if (rs.next())
			{
				clob = (oracle.sql.CLOB)rs.getObject(1);

				if(clob == null)
					return outval;

				if(clob.length() <= 0)
					return outval;

        callableStmt =
         (CallableStatement)con.prepareCall("begin dbms_LOB.read(?,?,?,?); end;");
        long totalLength = clob.length();
        long i = 0;             // index at which to read
        int chunk = 30*1024;    // size of the chunk to read :: Oracle limit is 30K
        long read_this_time = 0;

        while (i < totalLength)
        {
          callableStmt.clearParameters();
          callableStmt.setClob(1,clob);
          callableStmt.setLong(2,chunk); //size of chunk
          callableStmt.registerOutParameter(2,Types.NUMERIC); //Bytes received
          callableStmt.setLong(3, i+1); // begin reading at the offset
          callableStmt.registerOutParameter (4,Types.LONGVARCHAR);
          callableStmt.execute ();

          read_this_time = callableStmt.getLong (2);
          outval += callableStmt.getString(4);
          i += read_this_time;
        }
        callableStmt.close();
      }

      rs.close();
      stm.close();
	}
    catch(Exception e)
    {
      logger.error("ServiceResponseQueue::responseXML: failed to read data from report: " + this.getPk());
      logger.info("In Transaction: " + je.isInTransaction());

      String msg = "";

      if(e.getMessage() == null)
       msg = "Unknown Error Occurred";
      logger.error("ServiceResponseQueue::responseXML: Reason for failure: " + msg);
      logger.error(e);
      try{ rs.close();            }catch(Exception ea){;}
      try{ stm.close();           }catch(Exception eb){;}
      try{ callableStmt.close();  }catch(Exception ec){;}
    } finally {
        try {
            if(rs != null) rs.close();
        } catch (Exception ignore){}
        try {
            if(stm != null) stm.close();
        } catch (Exception ignore){}
        try {
            if(callableStmt != null) callableStmt.close();
        } catch (Exception ignore){}
    }
    return outval;
 	}
  
  private void writeResponseXML()
	{
	      if(this.responseXML == null )
	       this.responseXML = "";
	      srk.setCloseJdbcConnectionOnRelease(true);
	      JdbcExecutor je =  srk.getJdbcExecutor();
	      Connection con = je.getCon();
	      ResultSet rs = null;
	      Statement stm = null;
	    //  java.io.OutputStream outstream = null;
	     Writer writer = null;
	      try
	      {
	        oracle.sql.CLOB clob = null;

	        //get insert a new locator
	        stm = con.createStatement() ;
	        String st1 = "Update ServiceResponseQueue set responseXML = empty_clob() where ServiceResponseQueueId = ";
	        st1 +=  getServiceResponseQueueId();
	        stm.executeUpdate(st1);
	        con.setAutoCommit(false);
	        String st2 = "Select responseXML from ServiceResponseQueue ";
	        st2 += "where ServiceResponseQueueId = " + getServiceResponseQueueId();
	        st2 += " for update";
	        rs = stm.executeQuery(st2);
	        if (rs.next())
	        {
	          clob = (oracle.sql.CLOB)rs.getObject(1);
	          if(clob == null)
	           throw new Exception("ServiceResponseQueue::responseXML: Could not obtain LOB locator: " + this.pk);
	     //     outstream = blob.getBinaryOutputStream();
	         writer = clob.getCharacterOutputStream();
	        }
	        rs.close();
	        stm.close();
		  //  outstream.write(Integer.parseInt(getResponseXML()));
		//    outstream.flush();
		//    outstream.close();
	       writer.write(getResponseXML());
			writer.flush();
			writer.close();
	        con.commit();
	      }
	      catch(Exception e)
	      {
	        e.printStackTrace();
	        logger.error("ServiceResponseQueue::responseXML: failed to write data to report: " + this.getPk());
	        logger.info("In Transaction: " + je.isInTransaction());

	        String msg = "";
	        if(e.getMessage() == null)
	         msg = "Unknown Error Occurred";
	        logger.error("ServiceResponseQueue::responseXML: Reason for failure: " + msg);
	        logger.error(e);
	        try{ rs.close();      }catch(Exception ea){;}
	        try{ stm.close();     }catch(Exception eb){;}
	       //   try{ outstream.close();  }catch(Exception ec){;}
		     
	          try{ writer.close();  }catch(Exception ec){;}
	      } finally {
	          try {
	              if(rs != null) rs.close();
	          } catch (Exception ignore){}
	          try {
	              if(stm != null) stm.close();
	          } catch (Exception ignore){}
	      }
	}
  
  
  
  /** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		String [] arrExcl = { "RESPONSEXML" };

		int ret = doPerformUpdate(arrExcl);
	  	writeResponseXML();
		return ret;
	}
	
	/**
	 * method to be called to update everything other than response XML so that
	 * its not cleared when updating the status.
	 * @return
	 * @throws Exception
	 */
	public int updateEntity() throws Exception{
	    
	    srk.setModified(true);
	    String [] arrExcl = { "RESPONSEXML" };

        int ret = doPerformUpdate(arrExcl);
        this.trackEntityChange ();
        //writeResponseXML();
        return ret;
	}
	 
   

  private void setPropertiesFromQueryResult(int key) throws Exception {
    setServiceResponseQueueId(jExec.getInt(key, "serviceResponseQueueId"));
    this.setDealId(jExec.getInt(key,"dealId"));
    this.copyId = jExec.getInt(key,"copyId");
    setServiceProductId(jExec.getInt(key, "serviceProductId"));
    setProcessStatusId(jExec.getInt(key,"processStatusId"));
    setRetryCounter(jExec.getInt(key,"retryCounter"));
    setResponseXML(readResponseXML());
    setResponseDate(jExec.getDate(key, "responseDate"));
    setChannelTransactionKey(jExec.getString(key,"channelTransactionKey"));
    setRequestType(jExec.getString(key,"requestType"));
    setInstitutionProfileId(jExec.getInt(key,"institutionProfileId"));
    setLastProcessDate(jExec.getDate(key, "LASTPROCESSDATE"));
  
  }

  /**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}
  
	
	/**
	   * Get Entity's table's name.
	   * @return Table name.
	   */
	  public String getEntityTableName() {
	    return "ServiceResponseQueue";
	  }
	
	
	//------ START: getters and setters --------------------------------------
  
 
  /**
   * @return Returns the copy ID.
   */
  public int getCopyId() {return this.copyId;}


  

  

  /**
   * @return Returns the serviceResponseQueue ID.
   */
  public int getServiceResponseQueueId() {
    return serviceResponseQueueId;
  }

  /**
   * @param serviceResponseQueueId The serviceResponseQueue ID to set.
   */
  public void setServiceResponseQueueId(int cbId) {
    this.testChange("serviceResponseQueueId", cbId);
    this.serviceResponseQueueId = cbId;
    
  }
  public int getDealId()
  {
     return this.dealId;
  }
  public void setDealId(int dealId)
  {
     this.testChange ("dealId", dealId);
     this.dealId = dealId;
  }
  
  
  /**
   * @return Returns the Service ProductId.
   */
  public int getServiceProductId() {
    return serviceProductId;
  }

  /**
   * @param serviceProductId The Service ProductId to set.
   */
  public void setServiceProductId(int productId) {
    this.testChange("serviceProductId", productId);
    this.serviceProductId = productId;
    
  }
  /**
   * @return Returns the Process StatusId.
   */
  public int getProcessStatusId() {
    return processStatusId;
  }

  /**
   * @param processStatusId The process StatusId to set.
   */
  public void setProcessStatusId(int pStatusId) {
    this.testChange("processStatusId", pStatusId);
    this.processStatusId = pStatusId;
    
  }
  /**
   * @return Returns the Retry Counter.
   */
  public int getRetryCounter() {
    return retryCounter;
  }

  /**
   * @param retryCounter The Retry Counter to set.
   */
  public void setRetryCounter(int rCntr) {
    this.testChange("retryCounter", rCntr);
    this.retryCounter = rCntr;
    
  }
  /**
   * @return Returns the Received Time Stamp.
   */
  public Date getResponseDate() {
    return responseDate;
    
  }
  /**
   * @param recievedTimestamp The received time stamp to set.
   */
  public void setResponseDate(Date rDate) {
    testChange("responseDate", rDate);
    this.responseDate = rDate;
  }
  
  public String getResponseXML()
  {
	  return responseXML;
  }

  public void setResponseXML(String xmlR)
	{
		this.testChange("responseXML",xmlR);
		this.responseXML = xmlR;
	}
 
	public void saveXML(String report)
		throws Exception
	{
		boolean isAutoCommit = jExec.getCon().getAutoCommit();
		jExec.getCon().setAutoCommit(false);
		String sql = "Select " + "responseXML" + " from " +
			getEntityTableName() + pk.getWhereClause() + " For Update";
		int key = jExec.execute(sql);

		for (; jExec.next(key); )
		{
			// Get the Blob locator and open output stream for the Blob
			Clob reportClob = jExec.getClob(key, "responseXML");

			Writer clobWriter = ((oracle.sql.CLOB)reportClob).
				getCharacterOutputStream();

			Reader reportReader = new StringReader(report);

			// Buffer to hold chunks of data to being written to the Clob.
			char[] cbuffer = new char[10 * 1024];

			// Read a chunk of data from the sample file input stream,
			// and write the chunk into the Clob column output stream.
			// Repeat till file has been fully read.
			int nread = 0;
			while ((nread = reportReader.read(cbuffer)) != -1)
			{
				clobWriter.write(cbuffer, 0, nread); // Write to Clob
			}
			// Close both streams
			reportReader.close();
			clobWriter.close();

			break; // can only be one record
		}
		jExec.getCon().setAutoCommit(isAutoCommit);
		jExec.closeData(key);

    }
    
    //5.0 MI -- start
    
    public Date getLastProcessDate() {
        return lastProcessDate;
    }

    public void setLastProcessDate(Date lastProcessDate) {
        testChange("lastProcessDate", lastProcessDate);
        this.lastProcessDate = lastProcessDate;
    }
    
    public void setLastProcessDateDBSystemDate() {
        this.setDataBaseSystemDate("lastProcessDate");
    }
    
    //5.0 MI -- end


  public String getChannelTransactionKey()
  {
	  return channelTransactionKey;
  }

  
  public void setChannelTransactionKey(String ctKey)
	{
		testChange("channelTransactionKey", ctKey);
		this.channelTransactionKey = ctKey;
	}
  
  public String getRequestType()
  {
	  return requestType;
  }


  public void setRequestType(String rType)
	{
		testChange("requestType", rType);
		this.requestType = rType;
	}

  public IEntityBeanPK getPk(){
      if(pk==null)
          pk= new ServiceResponseQueuePK(this.serviceResponseQueueId);
      return pk;
  }
  public int getInstitutionProfileId()
  {
    return institutionProfileId;
  }
  public void setInstitutionProfileId(int institutionProfileId)
  {
      testChange("institutionProfileId", institutionProfileId);
      this.institutionProfileId = institutionProfileId;
  }
  


  //------- END: getters and setters ---------------------------------------
  
  public Response getResponse() throws FinderException, RemoteException {
      
      Response response = new Response(srk);
      response.findChannelTransactionData(this);
      return response;
  }
  
}
