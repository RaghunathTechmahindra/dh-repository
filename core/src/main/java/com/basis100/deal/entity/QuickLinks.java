package com.basis100.deal.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.QuickLinksBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

/**
 * 
 * QuickLinks:
 * 
 * Enterprise bean entity representing a record MOS database table 'QUICKLINKS'.
 * 
 * The a quick link entity consists of the properties in the quicklinkstable
 * table consisting quick link urls for particular institution profile id and
 * branch profileid based on configurations.
 * 
 * QUICKLINKID is created the unique key field and is obtained as the next
 * available value of sequence QUICKLINKIDSEQ. This sequence must exist in the
 * database, e.g.: create sequence QUICKLINKIDSEQ;
 * 
 */
public class QuickLinks extends DealEntity {

	private final static Logger  logger = LoggerFactory.getLogger(QuickLinks.class);

	// Prepared query to insert record into quicklinks table
	private static final String insertQuery = "Insert into QuickLinks  (quicklinkId, linkNameEnglish, "
			+ "linkNameFrench,uRLLink, branchProfileId,institutionProfileId,sortorder,favicon ) "
			+ "values (?,?,?,?,?,?,?,?)";

	// Prepared query to update record into quicklinks table
	private static final String updateQuery = "update QuickLinks "
			+ " set linkNameEnglish =?, linkNameFrench=?,uRLLink=?,	sortorder = ?, favicon = ? ,branchProfileId =?"
			+ " where quicklinkId = ? and institutionProfileId =?";
	
	// Query to delete record from quicklinks table for branch and institutionid
	private static final String deleteBranchQuickLinkQuery = "delete from QuickLinks "
			+ " where branchprofileid = ? and institutionProfileId =?";

	private int quickLinkId;
	private String linkNameEnglish;
	private String linkNameFrench;
	private String uRLAddress;
	private int branchProfileId;
	private int institutionProfileId;
	private int sortOrder;
	private String favicon;

	/**
	 * Constructor: Accepts session resources object (access to system logger
	 * and Jdbc connection resources.
	 */
	public QuickLinks(SessionResourceKit srk) throws RemoteException {
		super(srk);
	}

	/**
	 * 
	 */
	public QuickLinks() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the quickLinkId
	 */
	public int getQuickLinkId() {
		return quickLinkId;
	}

	/**
	 * @param quickLinkId
	 *            the quickLinkId to set
	 */
	public void setQuickLinkId(int quickLinkId) {
		this.quickLinkId = quickLinkId;
	}

	/**
	 * @return the linkNameEnglish
	 */
	public String getLinkNameEnglish() {
		return linkNameEnglish;
	}

	/**
	 * @param linkNameEnglish
	 *            the linkNameEnglish to set
	 */
	public void setLinkNameEnglish(String linkNameEnglish) {
		this.linkNameEnglish = linkNameEnglish;
	}

	/**
	 * @return the linkNameFrench
	 */
	public String getLinkNameFrench() {
		return linkNameFrench;
	}

	/**
	 * @param linkNameFrench
	 *            the linkNameFrench to set
	 */
	public void setLinkNameFrench(String linkNameFrench) {
		this.linkNameFrench = linkNameFrench;
	}

	/**
	 * @return the uRLAddress
	 */
	public String getUrlAddress() {
		return uRLAddress;
	}

	/**
	 * @param uRLAddress
	 *            the uRLAddress to set
	 */
	public void setUrlAddress(String uRLAddress) {
		this.uRLAddress = uRLAddress;
	}

	/**
	 * @return the branchProfileId
	 */
	public int getBranchProfileId() {
		return branchProfileId;
	}

	/**
	 * @param branchProfileId
	 *            the branchProfileId to set
	 */
	public void setBranchProfileId(int branchProfileId) {
		this.branchProfileId = branchProfileId;
	}

	/**
	 * @return the sortOrder
	 */
	public int getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the favicon
	 */
	public String getFavicon() {
		return favicon;
	}

	/**
	 * @param favicon
	 *            the favicon to set
	 */
	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	/**
	 * @return the institutionProfileId
	 */
	public int getInstitutionProfileId() {
		return institutionProfileId;
	}

	/**
	 * @return the institutionProfileId
	 */
	public int setInstitutionProfileId(int institutionProfileId) {
		return this.institutionProfileId = institutionProfileId;
	}

	/**
     * <p>updateQuickLinks</p>
     * <p>Description: Updates quick links for given institutionProfileId and branchprofileid
     *    based on flag com.filogix.ingestion.quicklinks.enabled.ALLBranches = Y/N
     *    
     * @param quickLinksList: List <QuickLinks>
     * @version 1.0
     */
	public void updateQuickLinks(List<QuickLinks> quickLinksList, int oldBranchQuickLinks)
			throws SQLException {

		Connection connection = null;

		PreparedStatement insertPstmt = null;
		PreparedStatement updatePstmt = null;
		PreparedStatement deleteBranchPstmt = null;
		
		try {
			connection = srk.getConnection();
			
			insertPstmt = connection.prepareStatement(insertQuery);
			updatePstmt = connection.prepareStatement(updateQuery);
			
			deleteBranchPstmt = connection.prepareStatement(deleteBranchQuickLinkQuery);

			// Copy from branch needs to delete old quick link records.
			if(oldBranchQuickLinks != 0)
				deleteOldBranchQuickLinks(deleteBranchPstmt, oldBranchQuickLinks,srk.getExpressState().getUserInstitutionId());

	
			for (QuickLinks url : quickLinksList) {

				if (url.getQuickLinkId() == 0) {

					// New quick URL to be inserted to database
					insertPreperedStatement(insertPstmt, url);
				} else {
					// update the existing quick link URL
					updatePreperedStatement(updatePstmt, url);
				}
			}
			// update the batch scripts and execute

			insertPstmt.executeBatch();
			updatePstmt.executeBatch();

			logger.debug("updateQuickLinks: saved records to quick link table:");
		} catch (SQLException eSQL) {
			logger.error("updateQuickLinks: failed to rollback records: "
					+ eSQL.getMessage());
			throw eSQL;
			
		} finally {
			try {
				if (insertPstmt != null)
					insertPstmt.close();
				if (updatePstmt != null)
					updatePstmt.close();
				if (deleteBranchPstmt != null)
					deleteBranchPstmt.close();
			} catch (SQLException sqlE) {
				logger.error("updateQuickLinks: failed to release database resources" + sqlE.getMessage());
			}
		}
	}

	
	/**
     * <p>insertPreperedStatement</p>
     * <p>Description: insert record into quick links table
     *    
     * @param pStmtInsert: PreparedStatement
     * @param url: QuickLinks
     * @version 1.0
     */
	private void insertPreperedStatement(PreparedStatement pStmtInsert,
			QuickLinks url) throws SQLException {
		pStmtInsert.setInt(1, getNextQuickLinksId());
		pStmtInsert.setString(2, url.getLinkNameEnglish());
		pStmtInsert.setString(3, url.getLinkNameFrench());
		pStmtInsert.setString(4, url.getUrlAddress());

		if (url.getBranchProfileId() == -1)
			pStmtInsert.setNull(5, java.sql.Types.NULL);
		else
			pStmtInsert.setInt(5, url.getBranchProfileId());

		pStmtInsert.setInt(6, url.getInstitutionProfileId());
		pStmtInsert.setInt(7, url.getSortOrder());
		pStmtInsert.setString(8, url.getFavicon());
		pStmtInsert.addBatch();
	}

	/**
     * <p>updatePreperedStatement</p>
     * <p>Description: update record into quick links table
     *    
     * @param pStmtUpdate: PreparedStatement
     * @param url: QuickLinks
     * @version 1.0
     */
	private void updatePreperedStatement(PreparedStatement pStmtUpdate,
			QuickLinks url) throws SQLException {

		pStmtUpdate.setString(1, url.getLinkNameEnglish());
		pStmtUpdate.setString(2, url.getLinkNameFrench());
		pStmtUpdate.setString(3, url.getUrlAddress());
		pStmtUpdate.setInt(4, url.getSortOrder());
		pStmtUpdate.setString(5, url.getFavicon());
		// pStmtUpdate.setInt(6, url.getBranchProfileId());
		if (url.getBranchProfileId() == -1)
			pStmtUpdate.setNull(6, java.sql.Types.NULL);
		else
			pStmtUpdate.setInt(6, url.getBranchProfileId());
		pStmtUpdate.setInt(7, url.getQuickLinkId());
		pStmtUpdate.setInt(8, url.getInstitutionProfileId());

		pStmtUpdate.addBatch();
	}

	/**
     * <p>getNextQuickLinksId</p>
     * <p>Description: gets new sequence number for quicklinksId
     *  @param  quickLinkSequence Number  
     * @version 1.0
     */
	public int getNextQuickLinksId() {
		int result = -1;

		try {

			String theSQL = "SELECT QUICKLINKSSEQ.NEXTVAL FROM DUAL";
			JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute(theSQL);

			if (jExec.next(key)) {
				result = jExec.getInt(key, 1);
			}

			jExec.closeData(key);

		} catch (Exception e) {
			logger.error("Exception @QuickLinks.getNextQuickLinksId: Problem encountered in creating a new quicklinksid.");
			logger.error(e.getMessage());
		}
		return result;
	}

	/**
     * <p>findByPrimaryKey</p>
     * <p>Description: findbyPrimary Key
     *  @param  quickLinkSequence Number  
     * @version 1.0
     */
	public QuickLinks findByPrimaryKey(QuickLinksBeanPK pk)
			throws RemoteException, FinderException {
		StringBuilder sql = new StringBuilder();
		sql.append("Select * from QuickLinks where quicklinkId = ").append(pk.getQuickLinkId());

		boolean gotRecord = false;

		try {
			int key = jExec.execute(sql.toString());

			if(jExec.next(key)) {
				gotRecord = true;
				setPropertiesFromQueryResult(key);
			}

			jExec.closeData(key);

			if (gotRecord == false) {
				String msg = "QuickLinks Entity: @findByPrimaryKey(), key="
						+ pk + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		} catch (Exception e) {
			FinderException fe = new FinderException(
					"QuickLinks Entity - findByPrimaryKey() exception");
			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			throw fe;
		}

		return this;
	}
	
	/**
     * <p>deleteOldBranchData</p>
     * <p>Description: delete old record from quick links table
     *    
     * @param deletePStmt: PreparedStatement
     * @param branchProfileId: int
     * @param institutionId: int
     * @version 1.0
     */
	private void deleteOldBranchQuickLinks(PreparedStatement deletePStmt,
			int branchProfileId, int institutionProfileId) throws SQLException {

		deletePStmt.setInt(1, branchProfileId);
		deletePStmt.setInt(2, institutionProfileId);

		boolean update = deletePStmt.execute();
		logger.debug("deleteOldBranchData: deleted old records from quick link table for branch:"
						+ branchProfileId
						+ " and institutionId"
						+ institutionProfileId);
	}

	
	/**
     * <p>setPropertiesFromQueryResult</p>
     * <p>Description:  Method to set data from result set to object
     *  @param  key  int  
     * @version 1.0
     */

	private void setPropertiesFromQueryResult(int key) throws Exception {
		setQuickLinkId(jExec.getInt(key, "QUICKLINKID"));
		setLinkNameEnglish(jExec.getString(key, "LINKNAMEENGLISH"));
		setLinkNameFrench(jExec.getString(key, "LINKNAMEFRENCH"));
		setUrlAddress(jExec.getString(key, "URLLINK"));
		setBranchProfileId(jExec.getInt(key, "BRANCHPROFILEID"));
		setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
		setSortOrder(jExec.getInt(key, "SORTORDER"));
		setFavicon(jExec.getString(key, "FAVICON"));
	}

	
	/**
     * <p>toString</p>
     * <p>Description: toString method overridden
     *  @param  toString String representation of object  
     * @version 1.0
     */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QuickLinks [branchProfileId=");
		builder.append(branchProfileId);
		builder.append(", favicon=");
		builder.append(favicon);
		builder.append(", institutionProfileId=");
		builder.append(institutionProfileId);
		builder.append(", linkNameEnglish=");
		builder.append(linkNameEnglish);
		builder.append(", linkNameFrench=");
		builder.append(linkNameFrench);
		builder.append(", quickLinkId=");
		builder.append(quickLinkId);
		builder.append(", sortOrder=");
		builder.append(sortOrder);
		builder.append(", uRLAddress=");
		builder.append(uRLAddress);
		builder.append("]");
		return builder.toString();
	}

}
