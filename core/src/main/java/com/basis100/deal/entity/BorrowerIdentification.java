
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerIdentificationPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: BorrowerIdentification
 * </p>
 * 
 * <p>
 * Description: BREntity class for Borrower Identification
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * @author NBC Implementation Team
 * @version 1.3
 * @version 1.5 added getClassId()
 */
public class BorrowerIdentification extends DealEntity {

    protected int identificationId;

    protected String identificationNumber;

    protected int identificationTypeId;

    protected int borrowerId;

    protected int copyId;

    protected int institutionProfileId;

    protected String identificationCountry;

    private BorrowerIdentificationPK pk;

    /**
     * @param srk
     * @throws RemoteException
     */
    public BorrowerIdentification(SessionResourceKit srk)
            throws RemoteException {

        super(srk);
    }

    /**
     * @param srk
     * @param calcMon
     * @throws RemoteException
     */
    public BorrowerIdentification(SessionResourceKit srk, CalcMonitor calcMon)
            throws RemoteException {

        super(srk, calcMon);
    }

    /**
     * 
     * @param srk
     * @param id
     * @param copyId
     * @throws RemoteException
     * @throws FinderException
     */
    public BorrowerIdentification(SessionResourceKit srk, int id, int copyId)
            throws RemoteException, FinderException {

        super(srk);
        pk = new BorrowerIdentificationPK(id, copyId);
        findByPrimaryKey(pk);
    }

    /**
     * 
     * @param srk
     * @param dcm
     * @param id
     * @param copyId
     * @throws RemoteException
     * @throws FinderException
     */

    public BorrowerIdentification(SessionResourceKit srk, CalcMonitor dcm,
            int id, int copyId) throws RemoteException, FinderException {

        super(srk, dcm);

        pk = new BorrowerIdentificationPK(id, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * 
     * @param pk
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public BorrowerIdentification findByPrimaryKey(BorrowerIdentificationPK pk)
            throws RemoteException, FinderException {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from BorrowerIdentification "
                + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "BorrowerIdentification Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a BorrowerIdentificationPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * 
     * @param key
     * @throws Exception
     */
    private void setPropertiesFromQueryResult(int key) throws Exception {

        this.setIdentificationId(jExec.getInt(key, "IDENTIFICATIONID"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        this.setIdentificationNumber(jExec.getString(key,
                "IDENTIFICATIONNUMBER"));
        this.setIdentificationTypeId(jExec.getInt(key, "IDENTIFICATIONTYPEID"));
        this.setBorrowerId(jExec.getInt(key, "BORROWERID"));
        this.setCopyId(jExec.getInt(key, "COPYID"));
        this.setIdentificationCountry(jExec.getString(key,
                "IDENTIFICATIONCOUNTRY"));
    }

    /**
     * <p>
     * Gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced) null is returned. if the field exists (and
     * the type is serviced) but the field is null an empty String is returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public Object getFieldValue(String fieldName) {

        return doGetFieldValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    private BorrowerIdentificationPK createPrimaryKey(int copyId)
            throws CreateException {

        String sql = "Select BorrowerIdentificationseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {

            CreateException ce = new CreateException(
                    "BorrowerIdentification Entity create() exception "
                            + "getting BorrowerIdentificationId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }

        return new BorrowerIdentificationPK(id, copyId);
    }

    // ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    // This method returns the collection of borrowerIdentifications
    // corresponding to one
    // borrower
    public Collection findByBorrower(BorrowerPK pk) throws Exception {

        Collection borrowerIdentifications = new ArrayList();

        String sql = "Select * from BorrowerIdentification ";
        sql += pk.getWhereClause();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                BorrowerIdentification borrowerIdentification = new BorrowerIdentification(
                        srk, dcm);

                borrowerIdentification.setPropertiesFromQueryResult(key);
                borrowerIdentification.pk = new BorrowerIdentificationPK(
                        borrowerIdentification.getIdentificationId(),
                        borrowerIdentification.getCopyId());

                borrowerIdentifications.add(borrowerIdentification);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while getting Borrower :: "
                            + "BorrowerIdentification");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return borrowerIdentifications;
    }

    // ***** Change by NBC Impl. Team - Version 1.2 - End *****//
    /**
     * <p>
     * creates a BorrowerIdentification using the BorrowerIdentification primary
     * key and a borrower primary key - the mininimum (ie.non-null) fields
     * <p>
     * 
     * @param bpk
     * @return
     * @throws RemoteException
     * @throws CreateException
     */
    public BorrowerIdentification create(BorrowerPK bpk)
            throws RemoteException, CreateException {

        pk = createPrimaryKey(bpk.getCopyId());

        String sql = "Insert into BorrowerIdentification( "
                + "identificationId, borrowerId, copyId, INSTITUTIONPROFILEID)"
                + " Values ( " + pk.getId() + "," + bpk.getId() + ","
                + pk.getCopyId() + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // Track the create
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "BorrowerIdentification Entity - "
                            + "BorrowerIdentification - create() exception");

            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);

        if (dcm != null) {
            // dcm.inputEntity(this);
            dcm.inputCreatedEntity(this);
        }
        return this;
    }

    /**
     * 
     */
    public String getEntityTableName() {

        return "BorrowerIdentification";
    }

    /**
     * @return Returns the borrowerId.
     */
    public int getBorrowerId() {

        return borrowerId;
    }

    /**
     * @param borrowerId
     *            The borrowerId to set.
     */
    public void setBorrowerId(int borrowerId) {

        this.testChange("borrowerId", borrowerId);
        this.borrowerId = borrowerId;
    }

    /**
     * @return Returns the copyId.
     */
    public int getCopyId() {

        return copyId;
    }

    /**
     * @param copyId
     *            The copyId to set.
     */
    public void setCopyId(int copyId) {

        this.testChange("copyId", copyId);
        this.copyId = copyId;
    }

    /**
     * @return Returns the identificationCountry.
     */
    public String getIdentificationCountry() {

        return identificationCountry;
    }

    /**
     * @param identificationCountry
     *            The identificationCountry to set.
     */
    public void setIdentificationCountry(String identificationCountry) {

        this.testChange("identificationCountry", identificationCountry);
        this.identificationCountry = identificationCountry;
    }

    /**
     * @return Returns the identificationId.
     */
    public int getIdentificationId() {

        return identificationId;
    }

    /**
     * @param identificationId
     *            The identificationId to set.
     */
    public void setIdentificationId(int identificationId) {

        this.testChange("identificationId", identificationId);
        this.identificationId = identificationId;
    }

    /**
     * @return Returns the identificationNumber.
     */
    public String getIdentificationNumber() {

        return identificationNumber;
    }

    /**
     * @param identificationNumber
     *            The identificationNumber to set.
     */
    public void setIdentificationNumber(String identificationNumber) {

        this.testChange("identificationNumber", identificationNumber);
        this.identificationNumber = identificationNumber;
    }

    /**
     * @return Returns the identificationTypeid.
     */
    public int getIdentificationTypeId() {

        return identificationTypeId;
    }

    /**
     * @param identificationTypeid
     *            The identificationTypeid to set.
     */
    public void setIdentificationTypeId(int identificationTypeId) {

        this.testChange("identificationTypeId", identificationTypeId);
        this.identificationTypeId = identificationTypeId;
    }

    // ***** Change by NBC Impl. Team - Version 1.5 - Start *****//
    /**
     * getClassId - Returns the class id of the BORROWERIDENTIFICATION class
     * This is used for ingestion and calcs
     * 
     * @param None
     * 
     * @return int : the result of getClassId - Class id of
     *         BORROWERIDENTIFICATION entity <br>
     */
    public int getClassId() {

        return ClassId.BORROWERIDENTIFICATION;
    }

    // ***** Change by NBC Impl. Team - Version 1.5 - End*****//

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int instProfileId) {

        this.testChange("institutionProfileId", instProfileId);
        this.institutionProfileId = instProfileId;
    }
}
