package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ScotiabankStepMortgagePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class ScotiabankStepMortgage extends DealEntity{
	protected int           dealId;
	protected int	        copyId;
	protected int	        stepMortgageId;
	protected int			stepMortgageTypeId;
	protected int			stepProductId;
	protected int			stepMortgageNumber;
	protected double		stepMortgageGlobalLimit;
	protected double		stepMortgageRegistrationAmount;
	protected int			stepMortgageAccountLoanNumber;
	protected double		stepMortgageCreditLimit;
	protected double		stepMortgageAmountAdvanced;
	protected double		stepMortgagePostedRate;
	protected double		stepMortgageCommittedRate;
	protected int			stepMortgageTerm;
	protected int			stepMortgageAmortization;
	protected double		stepMortgagePayment;
	protected double		stepMortgageFindersFee;
	protected String		stepMortgageLOLP;
	protected String		stepMortgageHCP;
	protected double		stepMortgageCashbackPercentage;
	protected int			userProfileId;
	protected Date			dateTimeStamp;
    protected int         institutionProfileId;
	
	private ScotiabankStepMortgagePK pk;

	public ScotiabankStepMortgage(SessionResourceKit srk) throws RemoteException
	{
		super(srk);
	}

	public ScotiabankStepMortgage(SessionResourceKit srk, int id, int dealid, int copyid) throws RemoteException, FinderException
	{
		super(srk);
	    pk = new ScotiabankStepMortgagePK(id,dealid,copyid);
	    findByPrimaryKey(pk);
	}

	/**  search for ScotiabankStepMortgage record using primary key
	*   @return a ScotiabankStepMortgage */
	public ScotiabankStepMortgage findByPrimaryKey(ScotiabankStepMortgagePK pk) throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from ScotiabankStepMortgage " + pk.getWhereClause();

	    boolean gotRecord = false;

	    try
	    {
	        int key = jExec.execute(sql);

	        for (; jExec.next(key); )
	        {
	            gotRecord = true;
	            setPropertiesFromQueryResult(key);
	             break; // can only be one record
	        }

	        jExec.closeData(key);

	        if (gotRecord == false)
	        {
	          String msg = "ScotiabankStepMortgage: @findByPrimaryKey(), key= " + pk + ", entity not found";
	          logger.error(msg);
	          throw new FinderException(msg);
	        }
	      }
	      catch (Exception e)
	      {
	          FinderException fe = new FinderException("ScotiabankStepMortgage Entity - findByPrimaryKey() exception");

	          logger.error(fe.getMessage());
	          logger.error("finder sql: " + sql);
	          logger.error(e);

	          throw fe;
	      }
	      this.pk = pk;
	      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	      return this;
	}


    /**  sets properties for ScotiabankStepMortgage */
	private void setPropertiesFromQueryResult(int key) throws Exception
	{
	  	this.setDealId(jExec.getInt(key, "DEALID"));
	  	this.setCopyId(jExec.getInt(key, "COPYID"));
	  	this.setStepMortgageId(jExec.getInt(key, "STEPMORTGAGEID"));
	  	this.setStepMortgageTypeId(jExec.getInt(key, "STEPMORTGAGETYPEID"));
	  	this.setStepProductId(jExec.getInt(key, "STEPPRODUCTID"));
	  	this.setStepMortgageNumber(jExec.getInt(key, "STEPMORTGAGENUMBER"));
	  	this.setStepMortgageGlobalLimit(jExec.getDouble(key, "STEPMORTGAGEGLOBALLIMIT"));
	  	this.setStepMortgageRegistrationAmount(jExec.getDouble(key, "STEPMORTGAGEREGISTRATIONAMOUNT"));
	  	this.setStepMortgageAccountLoanNumber(jExec.getInt(key, "STEPMORTGAGEACCOUNTLOANNUMBER"));
	  	this.setStepMortgageCreditLimit(jExec.getDouble(key, "STEPMORTGAGECREDITLIMIT"));
	  	this.setStepMortgageAmountAdvanced(jExec.getDouble(key, "STEPMORTGAGEAMOUNTADVANCED"));
	  	this.setStepMortgagePostedRate(jExec.getDouble(key, "STEPMORTGAGEPOSTEDRATE"));
	  	this.setStepMortgageCommittedRate(jExec.getDouble(key, "STEPMORTGAGECOMMITTEDRATE"));
	  	this.setStepMortgageTerm(jExec.getInt(key, "STEPMORTGAGETERM"));
	  	this.setStepMortgageAmortization(jExec.getInt(key, "STEPMORTGAGEAMORTIZATION"));
	  	this.setStepMortgagePayment(jExec.getDouble(key, "STEPMORTGAGEPAYMENT"));
	  	this.setStepMortgageFindersFee(jExec.getDouble(key, "STEPMORTGAGEFINDERSFEE"));
	  	this.setStepMortgageLOLP(jExec.getString(key, "STEPMORTGAGELOLP"));
	  	this.setStepMortgageHCP(jExec.getString(key, "STEPMORTGAGEHCP"));
	  	this.setStepMortgageCashbackPercentage(jExec.getDouble(key, "STEPMORTGAGECASHBACKPERCENTAGE"));
	  	this.setUserProfileId(jExec.getInt(key, "USERPROFILEID"));
	  	this.setDateTimeStamp(jExec.getDate(key, "DATETIMESTAMP"));
        this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
      
	}

	/**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	public String getEntityTableName()
	{
	    return "ScotiabankStepMortgage"; 
	}

	/**  gets the pk associated with this entity
	 *   @return a ScotiabankStepMortgagePK */
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk ;
	}	

	public int getDealId()
	{ 
		return this.dealId;
	}

	public void setDealId(int id)
	{
		this.testChange ("dealId", id);
		this.dealId = id;
	}

	public int getCopyId()
	{ 
		return this.copyId;
	}

	public void setCopyId(int id)
	{
		this.testChange ("copyId", id);
		this.copyId = id;
	}

	public int getStepMortgageId()
	{ 
		return this.stepMortgageId;
	}

	public void setStepMortgageId(int id)
	{
		this.testChange ("stepMortgageId", id);
		this.stepMortgageId = id;
	}

	public int getStepMortgageTypeId()
	{ 
		return this.stepMortgageTypeId;
	}

	public void setStepMortgageTypeId(int id)
	{
		this.testChange ("stepMortgageTypeId", id);
		this.stepMortgageTypeId = id;
	}

	public int getStepProductId()
	{ 
		return this.stepProductId;
	}

	public void setStepProductId(int id)
	{
		this.testChange ("stepProductId", id);
		this.stepProductId = id;
	}

	public int getStepMortgageNumber()
	{ 
		return this.stepMortgageNumber;
	}

	public void setStepMortgageNumber(int num)
	{
		this.testChange ("stepMortgageNumber", num);
		this.stepMortgageNumber = num;
	}

	public double getStepMortgageGlobalLimit()
	{ 
		return this.stepMortgageGlobalLimit;
	}

	public void setStepMortgageGlobalLimit(double amt)
	{
		this.testChange ("stepMortgageGlobalLimit", amt);
		this.stepMortgageGlobalLimit = amt;
	}

	public double getStepMortgageRegistrationAmount()
	{ 
		return this.stepMortgageRegistrationAmount;
	}

	public void setStepMortgageRegistrationAmount(double amt)
	{
		this.testChange ("stepMortgageRegistrationAmount", amt);
		this.stepMortgageRegistrationAmount = amt;
	}

	public int getStepMortgageAccountLoanNumber()
	{ 
		return this.stepMortgageAccountLoanNumber;
	}

	public void setStepMortgageAccountLoanNumber(int num)
	{
		this.testChange ("stepMortgageAccountLoanNumber", num);
		this.stepMortgageAccountLoanNumber = num;
	}
	
	public double getStepMortgageCreditLimit()
	{ 
		return this.stepMortgageCreditLimit;
	}

	public void setStepMortgageCreditLimit(double amt)
	{
		this.testChange ("stepMortgageCreditLimit", amt);
		this.stepMortgageCreditLimit = amt;
	}

	public double getStepMortgageAmountAdvanced()
	{ 
		return this.stepMortgageAmountAdvanced;
	}

	public void setStepMortgageAmountAdvanced(double amt)
	{
		this.testChange ("stepMortgageAmountAdvanced", amt);
		this.stepMortgageAmountAdvanced = amt;
	}

	public double getStepMortgagePostedRate()
	{ 
		return this.stepMortgagePostedRate;
	}

	public void setStepMortgagePostedRate(double rate)
	{
		this.testChange ("stepMortgagePostedRate", rate);
		this.stepMortgagePostedRate = rate;
	}
	
	public double getStepMortgageCommittedRate()
	{ 
		return this.stepMortgageCommittedRate;
	}

	public void setStepMortgageCommittedRate(double rate)
	{
		this.testChange ("stepMortgageCommittedRate", rate);
		this.stepMortgageCommittedRate = rate;
	}
	
	public int getStepMortgageTerm()
	{ 
		return this.stepMortgageTerm;
	}

	public void setStepMortgageTerm(int num)
	{
		this.testChange ("stepMortgageTerm", num);
		this.stepMortgageTerm = num;
	}

	public int getStepMortgageAmortization()
	{ 
		return this.stepMortgageAmortization;
	}

	public void setStepMortgageAmortization(int num)
	{
		this.testChange ("stepMortgageAmortization", num);
		this.stepMortgageAmortization = num;
	}

	public double getStepMortgagePayment()
	{ 
		return this.stepMortgagePayment;
	}

	public void setStepMortgagePayment(double amt)
	{
		this.testChange ("stepMortgagePayment", amt);
		this.stepMortgagePayment = amt;
	}

	public double getStepMortgageFindersFee()
	{ 
		return this.stepMortgageFindersFee;
	}

	public void setStepMortgageFindersFee(double amt)
	{
		this.testChange ("stepMortgageFindersFee", amt);
		this.stepMortgageFindersFee = amt;
	}

	public String getStepMortgageLOLP()
	{ 
		return this.stepMortgageLOLP;
	}

	public void setStepMortgageLOLP(String str)
	{
		this.testChange ("stepMortgageLOLP", str);
		this.stepMortgageLOLP = str;
	}
	
	public String getStepMortgageHCP()
	{ 
		return this.stepMortgageHCP;
	}

	public void setStepMortgageHCP(String str)
	{
		this.testChange ("stepMortgageHCP", str);
		this.stepMortgageHCP = str;
	}

	public double getStepMortgageCashbackPercentage()
	{ 
		return this.stepMortgageCashbackPercentage;
	}

	public void setStepMortgageCashbackPercentage(double prcnt)
	{
		this.testChange ("stepMortgageCashbackPercentage", prcnt);
		this.stepMortgageCashbackPercentage = prcnt;
	}

	public int getUserProfileId()
	{ 
		return this.userProfileId;
	}

	public void setUserProfileId(int id)
	{
		this.testChange ("userProfileId", id);
		this.userProfileId = id;
	}
	
	public Date getDateTimeStamp()
	{ 
		return this.dateTimeStamp;
	}

	public void setDateTimeStamp(Date d)
	{
		this.testChange ("dateTimeStamp", d);
		this.dateTimeStamp = d;
	}
	
	/**  creates ScotiabankStepMortgage record into database
	 *   @return a ScotiabankStepMortgage */
	public ScotiabankStepMortgage create(DealPK dpk, int stepid,
									int mtgtypeid, int prodid, 
									int mtgnumber, double amtadv,
									int term, int amort, int userid)throws RemoteException, CreateException
	{
		int key = 0;
		
	  	StringBuffer sqlb = new StringBuffer
        ("Insert into ScotiabankStepMortgage(" 
         + dpk.getName() +  ", copyId, stepMortgageId, stepMortgageTypeId, "
         + "stepProductId, stepMortgageNumber, stepMortgageAmountAdvanced, "
         + "stepMortgageTerm, stepMortgageAmortization, userProfileId, "
         + "institutionProfileId) Values ("); 
	  	sqlb.append(sqlStringFrom(dpk.getId()) + ", "); 
	  	sqlb.append(sqlStringFrom(dpk.getCopyId()) + ", "); 
	  	sqlb.append(sqlStringFrom(stepid) + ", "); 
	  	sqlb.append(sqlStringFrom(mtgtypeid) + ", "); 
	  	sqlb.append(sqlStringFrom(prodid) + ", "); 
	  	sqlb.append(sqlStringFrom(mtgnumber) + ", "); 
	  	sqlb.append(sqlStringFrom(amtadv) + ", "); 
	  	sqlb.append(sqlStringFrom(term) + ", "); 
      sqlb.append(sqlStringFrom(amort) + ", "); 
      sqlb.append(sqlStringFrom(userid) + ", "); 
	  	sqlb.append(srk.getExpressState().getDealInstitutionId() + ")");

	  	String sql = new String(sqlb);
		try
		{
			key = jExec.executeUpdate(sql);
			findByPrimaryKey(new ScotiabankStepMortgagePK(stepid, dpk.getId(),dpk.getCopyId()));
			this.trackEntityCreate (); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException("ScotiabankStepMortgage Entity - ScotiabankStepMortgage - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			
			throw ce;
		}	
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}
	  	
		srk.setModified(true);
		return this;
	}

	public  boolean equals( Object o )
	{
		if ( o instanceof ScotiabankStepMortgage)
		{
			ScotiabankStepMortgage oa = (ScotiabankStepMortgage) o;
	        if  ( ( dealId  == oa.getDealId () )
	                && ( copyId  ==  oa.getCopyId ()  )  )
				return true;
		}
		return false;
	}

    public Collection findByDeal(DealPK pk) throws Exception
    {
    	int key = 0;
    	Collection steps = new ArrayList();

    	String sql = "Select * from ScotiabankStepMortgage" ;
		sql +=  " where dealId = " + pk.getId();
		sql +=  " where copyId = " + pk.getCopyId();
      try
      {
            key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              ScotiabankStepMortgage step = new ScotiabankStepMortgage(srk,jExec.getInt(key,"stepMortgageId"),pk.getId(),pk.getCopyId());
              step.setPropertiesFromQueryResult(key);
              step.pk = new ScotiabankStepMortgagePK(step.getStepMortgageId(),step.getDealId(),step.getCopyId());
              steps.add(step);
            }
        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::ScotiabankStepMortgage");
          logger.error(fe.getMessage());
          logger.error(e);
          throw fe;
        }
	  	finally {
	  		try {
	  			if (key != 0)
	  				jExec.closeData(key);
	  		}
	  		catch (Exception e) {
	  			logger.error(e.toString());
	  		}
	  	}
        return steps;
    }

    private void setPk(ScotiabankStepMortgagePK pk) {
      this.pk = pk;
    }
    public Object clone() throws CloneNotSupportedException {
      throw new CloneNotSupportedException();
    }
    
    /**
     * Create a clone of this Scotiabank step mortgage entity. The clone is
     * identical but for the dealPk, and its members the dealId and CopyId, 
     * which must be provided.
     * 
     * @param dealPk Deal for which the clone will be created.
     * @return The cloned Scotiabank step mortgage.
     * @throws CloneNotSupportedException
     */
    public Object clone(DealPK dealPk) throws CloneNotSupportedException {
      ScotiabankStepMortgage clone = null;
      
      try {
        clone = new ScotiabankStepMortgage(srk);
        // XXX: is stepMortgageId correct?
        clone.create(dealPk, this.stepMortgageId, this.stepMortgageTypeId,
            this.stepProductId, this.stepMortgageNumber, 
            this.stepMortgageAmountAdvanced, this.stepMortgageTerm,
            this.stepMortgageAmortization, this.userProfileId);
        
        int savedId = clone.getStepMortgageId();
        // copy other fields
        clone = (ScotiabankStepMortgage) super.clone();
        
        // restore PK
        clone.setDealId(dealPk.getId());
        clone.setCopyId(dealPk.getCopyId());
        clone.setStepMortgageId(savedId);
        clone.setPk(new ScotiabankStepMortgagePK(savedId, dealPk.getId(), 
            dealPk.getCopyId()));
        
        // persist entity
        clone.ejbStore();
        
      } catch (Exception e) {          
        e.printStackTrace();
        logger.error("Error in ScotiabanksStepMortgage.clone(): could not " +
            "clone SSM entity with dealId: " + dealId + " and copyId: " + copyId);
      }      
      return clone;      
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    } 
}
