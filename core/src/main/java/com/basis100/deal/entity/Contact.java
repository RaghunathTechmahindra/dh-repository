package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.List;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class Contact extends DealEntity
{
   protected int      contactId;
   protected int      copyId;
   protected String   contactFirstName;
   protected String   contactMiddleInitial;
   protected String   contactLastName;
   protected String   contactPhoneNumber;
   protected String   contactFaxNumber;
   protected String   contactEmailAddress;
   protected int      addrId;
   protected int      salutationId;
   protected String   contactJobTitle;
   protected String   contactPhoneNumberExtension;
   //--Release2.1--//
   //Added LanguagePreference ID for Multilingual support
   //--> By Billy 12Nov2002
   protected int      languagePreferenceId;
   //--DJ_PREFCOMMETHOD_CR--start--//
   protected int      preferredDeliveryMethodId;
   //--DJ_PREFCOMMETHOD_CR--end--//

   protected int      institutionId;
   
   protected ContactPK pk;
   //
   public Contact(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public Contact(SessionResourceKit srk, int id, int copyId) throws RemoteException, FinderException
   {
      super(srk);

      pk = new ContactPK(id, copyId);

      findByPrimaryKey(pk);
   }

  /**
  * @returns full contact name formatted as: lastName, firstName initial
  */
  public String getContactFullName()
  {
      String fullName = contactLastName + ", " + contactFirstName;

      if (contactMiddleInitial != null && (!contactMiddleInitial.equals("")))
      	return fullName + " " + contactMiddleInitial + ".";

      return fullName;
  }
  /**
  * Specially used by LogOnHandler ::
  * @returns full contact name formatted as: firstName initial. lastname
  */
  public String getContactFullName2()
  {
      String fullName = contactLastName + ", " + contactFirstName;

      if (contactMiddleInitial != null && (!contactMiddleInitial.equals("")))
        fullName = contactFirstName +" "+ contactMiddleInitial +". "+ contactLastName;
      else
        fullName = contactFirstName +" "+ contactLastName;

      return fullName;
  }


   public Contact findByName(String first, String middle, String last) throws RemoteException, FinderException
   {
      if(first == null) first = "";
      if(middle == null) middle = "";
      if(last == null) last = "";

      first = first.trim().toUpperCase();
      last = last.trim().toUpperCase();
      middle = middle.trim().toUpperCase();

      String sql = "Select * from Contact where " ;
      boolean and = false;

      if(!first.equals(""))
      {

        sql += "upper(ContactFirstName) = '" + first + "'";
        and = true;
      }
      if(!middle.equals(""))
      {
        if(and) sql += " AND ";
        sql += "upper(ContactMiddleInitial) = '" + middle + "'";
        and = true;
      }

      if(and) sql+= " AND ";

      sql+= "upper(ContactLastName) = '" + last + "'";


      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Contact Entity: failed to @findByName = " + sql;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
            logger.error("finder sql: " + sql);
            logger.error(e);
            return null;
        }

      return this;
   }

   public Contact findByPrimaryKey(ContactPK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from Contact where contactId = " + pk.getId() +
       " AND copyId = " + pk.getCopyId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Contact Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Contact Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

   public Contact findByUserProfileId(int userProfileId) throws RemoteException, FinderException
   {
   	  int id = 0;

  	  String selSql = "Select * from Contact where contactId = ";

  	  String pSql = "Select contactId from userProfile where userProfileId = " + userProfileId;
  	  String sql = selSql + id;

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(pSql);

          for (; jExec.next(key); )
          {
         	   id = jExec.getInt(key, 1);
               break; // can only be one record
          }

          jExec.closeData(key);

      	  sql = selSql + id;

          key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Contact Entity: @findByUserProfileId(), userProfileId = " + userProfileId + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Contact Entity: @findByUserProfileId(), userProfileId = " + userProfileId + ",exception");

          logger.error(fe.getMessage());
          logger.error("finder sql (1): " + pSql);
          logger.error("finder sql (2): " + sql);

          logger.error(e);

          throw fe;
        }
        return this;
    }

   /**
    * <p>findByUserProfileIdWithInstitutionId</p>
    * this method was created for iWorkQ and mWorkQ page that use user VPN
    * FXP23322
    * 
    * @param pk
    * @param institutionId
    * @return
    * @throws RemoteException
    * @throws FinderException
    */
   public Contact findByUserProfileIdWithInstitutionId(int userProfileId, int institutionId) 
   		throws RemoteException, FinderException
   {
  	  String sql = "Select c.* from Contact c, userprofile u where c.contactId = u.contactId and " 
  		  + "c.institutionProfileId = u.institutionProfileId and u.userprofileId = " + userProfileId 
  		  + " and u.institutionProfileId = " + institutionId;

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Contact Entity: @findByUserProfileId(), userProfileId = " + userProfileId + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Contact Entity: @findByUserProfileIdWithInstitutionId(), userProfileId = " 
        		  + userProfileId + " InstituitonId = " + institutionId + " ,exception");

          logger.error(fe.getMessage());
          logger.error("finder sql : " + sql);

          logger.error(e);

          throw fe;
        }
        return this;
    }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     this.setContactId(jExec.getInt(key,"CONTACTID"));
     this.copyId = jExec.getInt(key,"COPYID");
     this.setContactFirstName(jExec.getString(key,"CONTACTFIRSTNAME"));
     this.setContactMiddleInitial(jExec.getString(key,"CONTACTMIDDLEINITIAL"));
     this.setContactLastName(jExec.getString(key,"CONTACTLASTNAME"));
     this.setContactPhoneNumber(jExec.getString(key,"CONTACTPHONENUMBER"));
     this.setContactFaxNumber(jExec.getString(key,"CONTACTFAXNUMBER"));
     this.setContactEmailAddress(jExec.getString(key,"CONTACTEMAILADDRESS"));
     this.setAddrId(jExec.getInt(key,"ADDRID"));
     this.setSalutationId(jExec.getInt(key,"SALUTATIONID"));
     this.setContactJobTitle(jExec.getString(key,"CONTACTJOBTITLE"));
     this.setContactPhoneNumberExtension(jExec.getString(key,"CONTACTPHONENUMBEREXTENSION"));
     //--Release2.1--//
     //Added LanguagePreference ID for Multilingual support
     //--> By Billy 12Nov2002
     this.setLanguagePreferenceId(jExec.getInt(key,"LANGUAGEPREFERENCEID"));
     //--DJ_PREFCOMMETHOD_CR--start--//
     this.setPreferredDeliveryMethodId(jExec.getInt(key,"PREFERREDDELIVERYMETHODID"));
     //--DJ_PREFCOMMETHOD_CR--end--//
     this.setInstitutionId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
     
   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

  public Object getFieldValue(String fieldName){
    return doGetFieldValue(this, this.getClass(), fieldName);
  }

  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
    protected int performUpdate()  throws Exception
    {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
    }

    public String getEntityTableName()
    {
      return "Contact";
    }

    public int getClassId()
    {
      return ClassId.CONTACT ;
    }

    public List getSecondaryParents() throws Exception
    {
      List sps = new ArrayList();
      Addr addr = null;

      try
      {
        addr = getAddr();
      }
      catch(FinderException e)
      {
        return sps;
      }

      if(addr != null)
        sps.add(addr);

      return sps;
    }



  /**
   *   gets the contactMiddleInitial associated with this entity
   *
   *   @return a String
   */
    public String getContactMiddleInitial()
   {
      return this.contactMiddleInitial ;
   }
  /**
   *   gets the contactFirstName associated with this entity
   *
   *   @return a String
   */
    public String getContactFirstName()
   {
      return this.contactFirstName ;
   }
 	/**
   *   gets the contactFirstName associated with this entity
   *
   *   @return a String
   */
    public String getContactJobTitle()
   {
      return this.contactJobTitle ;
   }

		/**
     *   gets the addr associated with this entity
     *
     *   @return a Addr
     */
   public Addr getAddr() throws FinderException,RemoteException
   {
     return new Addr(srk,this.addrId,this.getCopyId());
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a ContactPK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

		/**
     *   gets the contactEmailAddress associated with this entity
     *
     *   @return a String
     */
    public String getContactEmailAddress()
   {
      return this.contactEmailAddress ;
   }

		/**
     *   gets the contactLastName associated with this entity
     *
     *   @return a String
     */
    public String getContactLastName()
   {
      return this.contactLastName ;
   }

		/**
     *   gets the addrId associated with this entity
     *
     *   @return a int
     */
    public int getAddrId()
   {
      return this.addrId ;
   }
  /**
  *   gets the copyId associated with this entity
  *
  *   @return a int
  */
  public int getCopyId()
  {
     return this.copyId ;
  }

  /**
   *   gets the contactPhoneNumber associated with this entity
   *
   *   @return a String
   */
   public String getContactPhoneNumber()
   {
      return this.contactPhoneNumber ;
   }

  /**
   *   gets the contactPhoneNumberExtension associated with this entity
   *
   *   @return a String
   */
   public String getContactPhoneNumberExtension()
   {
      return this.contactPhoneNumberExtension ;
   }

  /**
   *   gets the salutationId associated with this entity
   *
   *   @return int the salutation id
   */
   public int getSalutationId()
   {
      return this.salutationId ;
   }
		/**
     *   gets the contactFaxNumber associated with this entity
     *
     *   @return a String
     */
   public String getContactFaxNumber()
   {
      return this.contactFaxNumber ;
   }

		/**
     *   gets the contactId associated with this entity
     *
     *   @return a int
     */
   public int getContactId()
   {
      return this.contactId ;
   }


   private ContactPK createPrimaryKey(int copyId)throws CreateException
   {
     String sql = "Select Contactseq.nextval from dual";
     int id = -1;

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }

         jExec.closeData(key);
         if( id == -1 ) throw new Exception();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("Contact Entity create() exception getting ContactId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;

      }

      return new ContactPK(id,copyId);
  }


  public Contact create(AddrPK adpk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(adpk.getCopyId());

    String sql = "Insert into Contact( contactId, addrid, copyId, institutionProfileId ) Values ( "
                 + pk.getId() + "," + adpk.getId() + "," + pk.getCopyId() 
                 + "," + srk.getExpressState().getDealInstitutionId() + ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      this.trackEntityCreate (); // track the create
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Contact Entity - Contact - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);
    return this;
  }

  public Contact create()throws RemoteException, CreateException
  {
    return create(1);
  }

   /**
   * Creates a contact using the primary key of it's database parent
   *
   */
  public Contact create(int parentCopyId)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(parentCopyId);
    Addr add = null;

    try
    {
      add = new Addr(srk);
      add.create(pk);
      return this.create((AddrPK)add.getPk());
    }
    catch (Exception ex)
    {
      CreateException ce = new CreateException("Addr Entity - BorrowerAddress - create() exception");
      logger.error(ce.getMessage());
      logger.error(ex);

      throw ce;
    }
  }

    /**
    * Creates a contact using the primary key of it's database parent
    *
    */
   public Contact create(EmploymentHistoryPK epk)throws RemoteException, CreateException
   {
     int cid = epk.getCopyId();
     return create(cid);
   }


  /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }


   public Contact deepCopy() throws CloneNotSupportedException
   {
      return (Contact)this.clone();
   }

   public void setContactId(int id)
	 {
     this.testChange ("contactId", id ) ;
		 this.contactId = id;
   }

   public void setContactFirstName(String name )
	 {
     this.testChange ("contactFirstName", name ) ;
		 this.contactFirstName = name;
   }
   public void setContactMiddleInitial(String mi )
	 {
     this.testChange ("contactMiddleInitial", mi );
		 this.contactMiddleInitial = mi;
   }
   public void setContactLastName(String name )
	 {
     this.testChange ("contactLastName", name ) ;
		 this.contactLastName = name;
   }
   public void setContactPhoneNumber(String  num )
	 {
     this.testChange ("contactPhoneNumber", num );
		 this.contactPhoneNumber = num;
   }
   public void setContactFaxNumber(String fax)
	 {
     this.testChange ("contactFaxNumber", fax );
		 this.contactFaxNumber = fax;
   }
   public void setContactEmailAddress(String email )
	 {
     this.testChange ("contactEmailAddress", email );
		 this.contactEmailAddress = email;
   }
   public void setAddrId(int id )
	 {
     this.testChange("addrId", id );
		 this.addrId = id;
   }
   public void setSalutationId(int id)
	 {
     this.testChange ("salutationId", id );
		 this.salutationId = id;
   }
    public void setContactJobTitle(String title)
	 {
     this.testChange ("contactJobTitle", title);
		 this.contactJobTitle = title;
   }
   public void setContactPhoneNumberExtension(String phoneNumberExtension)
	 {
     this.testChange ("contactPhoneNumberExtension", phoneNumberExtension );
		 this.contactPhoneNumberExtension = phoneNumberExtension;
   }
   public void setCopyId(int id )
	 {
     this.testChange ("copyId", id ) ;
		 this.copyId = id;
   }

   public void ejbRemove() throws RemoteException
   {
    try
    {
      String sql = "Delete From Contact " + this.pk.getWhereClause();

      jExec.executeUpdate(sql);

      Addr addr = null;
      try
      {
        addr = this.getAddr ();
      }
      catch (Exception e)
      {
        logger.info("Contact " + this.pk + " - ejbRemove- no Addr found");
        addr = null;
      }

      if ( addr != null )
      {
        try
        {
          addr.ejbRemove (false);  // remvoe address should not trigger calculation
        }
        catch(Exception ex)
        {
          String msg = ex.getMessage();

          if(msg == null) msg = " Exception has null message: " + ex.getClass().getName();

          msg = "Contact " + this.pk + " - ejbRemove - Addr found "
                + " but couldn't be removed.";

          logger.error(msg);
          logger.error(ex);

          throw new RemoteException(msg);
        }
      }
    }
    catch (Exception e)
    {
      String msg =  "Exception removing Contact record: " + this.pk;

      logger.error(msg);
      logger.error(e);

      throw new RemoteException(msg);
    }

  }

  protected EntityContext getEntityContext() throws RemoteException
  {
    EntityContext ctx = this.entityContext ;

    try
    {
      //???
    } catch ( Exception e )
    {
      if ( e instanceof FinderException )
       return ctx;

      throw ( RemoteException ) e ;
    }
    return ctx ;
  } // --------------- End of getEntityContext ----------------------

  public  boolean equals( Object o )
  {
    if ( o instanceof Contact )
      {
        Contact oa = ( Contact )  o;
        if  ( ( contactId  == oa.getContactId () )
          && (  copyId  ==  oa.getCopyId ()  )  )
            return true;
      }
    return false;
  }

  //--Release2.1--//
  //Added LanguagePreference ID for Multilingual support
  //--> By Billy 12Nov2002
  public int getLanguagePreferenceId()
  {
     return this.languagePreferenceId;
  }

  public void setLanguagePreferenceId(int id )
  {
   this.testChange ("languagePreferenceId", id ) ;
   this.languagePreferenceId = id;
  }

  //--DJ_PREFCOMMETHOD_CR--start--//
  public int getPreferredDeliveryMethodId()
  {
     return this.preferredDeliveryMethodId;
  }

  public void setPreferredDeliveryMethodId(int id )
  {
   this.testChange ("preferredDeliveryMethodId", id ) ;
   this.preferredDeliveryMethodId = id;
  }
  //--DJ_PREFCOMMETHOD_CR--end--//


  /**
   * @return the institutionId
   */
  public int getInstitutionId()
  {
    return institutionId;
  }


  /**
   * @param institutionId the institutionId to set
   */
  public void setInstitutionId(int institutionId)
  { this.testChange("institutionProfileId", institutionId);
    this.institutionId = institutionId;
  }
}
