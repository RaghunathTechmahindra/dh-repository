//===========================================================================================
// Modifications :  Modified GDSInclusion, TDSInclusion to double type
//                    -- By BILLY 17Jan2001
//===========================================================================================

package com.basis100.deal.entity;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.LiabilityTypePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class LiabilityType extends DealEntity
{

  protected int            liabilityTypeId;
  protected double         GDSInclusion;
  protected double         TDSInclusion;
  protected String         LDescription;
  protected String         liabilityPaymentQualifier;

  private LiabilityTypePK pk;

  public LiabilityType(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }

  public LiabilityType(SessionResourceKit srk, int id) throws RemoteException, FinderException
  {
    super(srk);

    pk = new LiabilityTypePK(id);

    findByPrimaryKey(pk);
  }



  public LiabilityType findByPrimaryKey(LiabilityTypePK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from LiabilityType " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "LiabilityType: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("LiabilityType Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }

    srk.setModified(true);
    this.pk = pk;
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
     setLiabilityTypeId(jExec.getInt(key,"LIABILITYTYPEID"));
     setLDescription(jExec.getString(key,"LDESCRIPTION"));
     setGDSInclusion(jExec.getDouble(key,"GDSINCLUSION"));
     setTDSInclusion(jExec.getDouble(key,"TDSINCLUSION"));
     setLiabilityPaymentQualifier(jExec.getString(key,"LIABILITYPAYMENTQUALIFIER"));
  }

 
   /**
  *  gets the value of instance fields as String.
  *  if a field does not exist (or the type is not serviced)** null is returned.
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *  @returns value of bound field as a String;
  *  @param  fieldName as String
  *
  *  Other entity types are not yet serviced   ie. You can't get the Province object
  *  for province.
  */
  public String getStringValue(String fieldName)
  {
      return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  @param  fieldName as String
  *  @param  the value as a String
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }


  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return the number of updates performed
  */
  protected int performUpdate()  throws Exception
  {
  Class clazz = this.getClass();
  int ret = doPerformUpdate(this, clazz);

  return ret;
  }

  public String getEntityTableName()
  {
    return "LiabilityType"; 
  }



  /**
   *   gets the liabilityId associated with this entity
   *
   *   @return a int
   */
  public int getLiabilityTypeId()
  {
    return this.liabilityTypeId ;
  }


  /**
   *   gets the pk associated with this entity
   *
   *   @return a LiabilityTypePK
   */
  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }


  public double getGDSInclusion()
  {
    return this.GDSInclusion;
  }

  public double getTDSInclusion()
  {
    return this.TDSInclusion;
  }

  public String getLDescription(){ return this.LDescription;}
  
  public String getLiabilityPaymentQualifier()
  {
   return this.liabilityPaymentQualifier;
  }


  /**
  * creates a LiabilityType using the LiabilityType id
  *
 LIABILITYTYPEID                                       NOT NULL NUMBER(2)
 LDESCRIPTION                                                   VARCHAR2(35)
 GDSINCLUSION                                                   NUMBER(3)
 TDSINCLUSION                                                   NUMBER(3)
 LIABILITYPAYMENTQUALIFIER                                      VARCHAR2(1)
 */

  public LiabilityType create(int id, String desc, double gdsInc, double tdsInc, String lpq )throws RemoteException, CreateException
  {
      String sql = "Insert into LiabilityType (LIABILITYTYPEID, LDESCRIPTION, GDSINCLUSION, TDSINCLUSION, LIABILITYPAYMENTQUALIFIER) Values ( " +
       id + ",'" + desc + "'," + gdsInc + "," + tdsInc + ",'" + lpq + "')";

      pk = new LiabilityTypePK(id);

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        this.trackEntityCreate ();  // track the creation
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("LiabilityType Entity - LiabilityType - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      srk.setModified(true);

      if(dcm != null)
        dcm.inputEntity(this);

    return this;
  }

  public LiabilityType deepCopy() throws CloneNotSupportedException
  {
    return (LiabilityType)this.clone();
  }

  public void setLiabilityTypeId(int id)
  {
   this.testChange ("liabilityTypeId", id ) ;
   this.liabilityTypeId = id;
  }


  public void setGDSInclusion(double c)
  {
   this.testChange ("GDSInclusion", c ) ;
   this.GDSInclusion = c;
  }

  public void setTDSInclusion(double c)
  {
   this.testChange ("TDSInclusion", c );
   this.TDSInclusion = c;
  }

  public void setLDescription(String desc)
  {
   this.testChange ("LDescription", desc ) ;
   this.LDescription = desc;
  }

  public void setLiabilityPaymentQualifier(String q)
  {
   this.testChange ("liabilityPaymentQualifier", q );
   this.liabilityPaymentQualifier = q;
  }


  public int getClassId()
  {
    return ClassId.DEFAULT;
  }

  public void ejbRemove()throws RemoteException
  {
    ejbRemove(false);
  }

  public void ejbRemove(boolean calc) throws RemoteException
  {
      String sql = "Delete From LiabilityType " + pk.getWhereClause();

      try
      {
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        RemoteException re = new RemoteException("Failed to remove LiabilityType record" );

        logger.error(re.getMessage());
        logger.error("remove sql: " + sql);
        logger.error(e);

        throw re;
      }
      
      //4.4 Entity Cache
      ThreadLocalEntityCache.removeFromCache(this, pk);

  }

  public  boolean equals( Object o )
  {
    if ( o instanceof LiabilityType )
      {
        LiabilityType oa = ( LiabilityType ) o;
        if  (  this.liabilityTypeId == oa.getLiabilityTypeId ()  )
            return true;
      }
    return false;
  }

}
