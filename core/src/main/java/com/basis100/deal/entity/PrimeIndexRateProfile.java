
package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

// --Ticket#781--XD_ARM/VRM--19Apr2005--start--//

public class PrimeIndexRateProfile extends DealEntity {
    protected int primeIndexRateProfileId;
    protected String primeIndexName;
    protected String primeIndexAbrv;
    protected int armRateAdjustmentId;
    protected String primeIndexDescription;
    protected String piFinancialInstitution;
    protected String isTwoTermParameter;
    protected int primeIndexStatusId;
    protected int armCapsInventoryId;
    protected Date primeIndexDisDate;
    protected int armLoanTerm;
    protected int institutionProfileId;
    protected PrimeIndexRateProfilePK pk;

    public PrimeIndexRateProfile(SessionResourceKit srk)
            throws RemoteException, FinderException {

        super(srk);
    }

    public PrimeIndexRateProfile(SessionResourceKit srk, int id)
            throws RemoteException, FinderException {

        super(srk);

        pk = new PrimeIndexRateProfilePK(id);

        findByPrimaryKey(pk);
    }

    public PrimeIndexRateProfile findByPrimeIndexRateInventory(int priid)
            throws Exception {

        StringBuffer sql = new StringBuffer(
                "Select * from primeindexrateprofile where primeindexrateprofileid = "
                        + "(select primeindexrateprofileid from primeindexrateinventory "
                        + "where primeindexrateinventoryid = " + priid + ")");
        //boolean gotRecord = false;
        PrimeIndexRateProfile pp = null;
        try {
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {
                //gotRecord = true;
                pp = new PrimeIndexRateProfile(srk);
                pp.setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PrimeIndexRateProfile Entity - findByPrimeIndexRateInventory("
                            + priid + ") " + "exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return pp;
    }

    public PrimeIndexRateProfile findByPrimaryKey(PrimeIndexRateProfilePK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from PrimeIndexRateProfile where "
                + pk.getName() + " = " + pk.getId();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    // * Get PrimeIndex Rate Inventory related ( PricingProfileId )
    public Collection getPrimeIndexRateInventories() throws FinderException {

        return fetchPrimeIndexRateInventories();
    }

    public Collection fetchPrimeIndexRateInventories() throws FinderException {

        Collection primeIndexRateInventories = new Vector();

        String sql = "Select primeIndexRateInventoryID from PrimeIndexRateInventory "
                + pk.getWhereClause();
        sql += " order by PRIMEINDEXEFFDATE DESC";

        int key = 0;
        Vector v = new Vector();

        try {
            key = jExec.execute(sql);

            for (; jExec.next(key);) {
                v.add(new Integer(jExec.getInt(key, 1)));
            }
            jExec.closeData(key);

            int lim = v.size();

            for (int i = 0; i < lim; ++i) {
                int nextId = ((Integer) v.elementAt(i)).intValue();

                PrimeIndexRateInventory primeIndexRateInventory = null;

                try {
                    primeIndexRateInventory = new PrimeIndexRateInventory(srk,
                            nextId);
                    primeIndexRateInventories.add(primeIndexRateInventory);
                } catch (Exception epri) {
                    primeIndexRateInventory = null;

                    logger
                            .error("Error locating PrimeIndex Rate Inventory entity, id = "
                                    + nextId + " - ignored");
                    logger.error(epri);
                }
            }

        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Exception caught while fetching PrimeIndexRateProfile:PrimeIndexRateInventory");

            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return primeIndexRateInventories;
    }

    // ---------End of fecthPrimeIndexRateInventories ---------

    private void setPropertiesFromQueryResult(int key) throws Exception {

        int index = 0;
        this.setPrimeIndexRateProfileId(jExec.getInt(key, ++index));
        this.setPrimeIndexName(jExec.getString(key, ++index));
        this.setPrimeIndexAbrv(jExec.getString(key, ++index));
        this.setArmRateAdjustmentId(jExec.getInt(key, ++index));
        this.setPrimeIndexDescription(jExec.getString(key, ++index));
        this.setPIFinancialInstitution(jExec.getString(key, ++index));
        this.setIsTwoTermParameter(jExec.getString(key, ++index));
        this.setPrimeIndexStatusId(jExec.getInt(key, ++index));
        this.setArmCapsInventoryId(jExec.getInt(key, ++index));
        this.setPrimeIndexDisDate(jExec.getDate(key, ++index));
        this.setArmLoanTerm(jExec.getInt(key, ++index));
        this.setInstitutionProfileId(jExec.getInt(key, ++index));
    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    public String getEntityTableName() {

        return "PrimeIndexRateProfile";
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a PricingProfilePK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public int getPrimeIndexRateProfileId() {

        return this.primeIndexRateProfileId;
    }

    public String getPrimeIndexName() {

        return this.primeIndexName;
    }

    public Date getPrimeIndexDisDate() {

        return this.primeIndexDisDate;
    }

    public String getPrimeIndexAbrv() {

        return this.primeIndexAbrv;
    }

    public int getArmRateAdjustmentId() {

        return this.armRateAdjustmentId;
    }

    public String getPrimeIndexDescription() {

        return this.primeIndexDescription;
    }

    public String getPIFinancialInstitution() {

        return this.piFinancialInstitution;
    }

    public String getIsTwoTermParameter() {

        return this.isTwoTermParameter;
    }

    public int getPrimeIndexStatusId() {

        return this.primeIndexStatusId;
    }

    public int getArmCapsInventoryId() {

        return this.armCapsInventoryId;
    }

    public int getArmLoanTerm() {

        return this.armLoanTerm;
    }

    public PrimeIndexRateProfilePK createPrimaryKey() throws CreateException {

        String sql = "Select PrimeIndexRateProfileseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PrimeIndexRateProfile Entity create() exception getting PrimeIndexRateProfileId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        return new PrimeIndexRateProfilePK(id);
    }

    /**
     * creates a PrimeIndexRateProfile using the PrimeIndexRateProfileId with
     * minimum fields
     * 
     */

    public PrimeIndexRateProfile create(PrimeIndexRateProfilePK pk)
            throws RemoteException, CreateException {

        String sql = "Insert into PrimeIndexRateProfile(" + pk.getName()
                + ",INSTITUTIONPROFILEID ) Values ( " + pk.getId() + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PrimeIndexRateProfile Entity - PrimeIndexRateProfile - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        this.pk = pk;
        srk.setModified(true);
        return this;
    }

    /**
     * gets the primeIndexRateProfileId associated with this entity
     * 
     */
    public void setPrimeIndexRateProfileId(int value) {

        this.testChange("primeIndexRateProfileId", value);
        this.primeIndexRateProfileId = value;
    }

    public void setPrimeIndexName(String value) {

        this.testChange("primeIndexName", value);
        this.primeIndexName = value;
    }

    public void setPrimeIndexAbrv(String value) {

        this.testChange("primeIndexAbrv", value);
        this.primeIndexAbrv = value;
    }

    public void setArmRateAdjustmentId(int value) {

        this.testChange("armRateAdjustmentId", value);
        this.armRateAdjustmentId = value;
    }

    public void setPrimeIndexDescription(String value) {

        this.testChange("primeIndexDescription", value);
        this.primeIndexDescription = value;
    }

    public void setPIFinancialInstitution(String value) {

        this.testChange("piFinancialInstitution", value);
        this.piFinancialInstitution = value;
    }

    public void setIsTwoTermParameter(String value) {

        this.testChange("isTwoTermParameter", value);
        this.isTwoTermParameter = value;
    }

    public void setPrimeIndexStatusId(int value) {

        this.testChange("primeIndexStatusId", value);
        this.primeIndexStatusId = value;
    }

    public void setArmCapsInventoryId(int value) {

        this.testChange("armCapsInventoryId", value);
        this.armCapsInventoryId = value;
    }

    public void setPrimeIndexDisDate(Date value) {

        this.testChange("primeIndexDisDate", value);
        this.primeIndexDisDate = value;
    }

    public void setArmLoanTerm(int value) {

        this.testChange("armLoanTerm", value);
        this.armLoanTerm = value;
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
}
