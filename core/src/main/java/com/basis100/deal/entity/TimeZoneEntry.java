package com.basis100.deal.entity;

import java.util.Hashtable;
import java.util.TimeZone;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.TimeZoneEntryPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;


/**
 * A class representing time zone entry specific for MOS.
 * When a time zone entry is created a record is inserted in the TimeZoneEntry table. The table
 * maps Mos time zone id to the standard Java time zone id.
 *
 * Attributes:
 *
 *    timeZoneEntryId		  - System assigned (picklist) values (see MOS Data Dictionary)
 *    tzeDescription		  - Description
 *    tzeSysId			      - System/Java Time Zone Ids
 *    tzeCustomId         - Custom ID for display in MOS
 *
 *  Modification :
 *    Added new field tzeCustomId for diaplaying purpose -- By BILLY 29Nov2001
 *
 */

public class TimeZoneEntry extends DealEntity {

   EntityContext ctx;

   private int    timeZoneEntryId;
   private String tzeDescription;
   private String tzeSysId;
   private String tzeCustomId;

   TimeZoneEntryPK pk;

   private static Hashtable tzTable = new Hashtable();

   public TimeZoneEntry(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

   public TimeZoneEntry(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      TimeZoneEntryPK pk = new TimeZoneEntryPK(id);

      findByPrimaryKey(pk);
   }

   public TimeZoneEntry create(int timeZoneEntryId, String tzeDescription, String tzeSysId, String tzeCustomId) throws RemoteException, CreateException
   {
      String sql = "Insert Into TimeZoneEntry (timeZoneEntryId, tzeDescription, tzeSysId, tzeCustomId) Values (" +
                    timeZoneEntryId + ", '" + tzeDescription + "', '" + tzeSysId + "', '" + tzeCustomId +"')";

      pk = new TimeZoneEntryPK( timeZoneEntryId );

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate (); // track the creation
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("TimeZoneEntry Entity - TimeZoneEntry - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      return this;
    }

 /**
    * Attempts to remove the record that corresponds to the time zone id.
    *
    * @param
    * @return
 */

    public void remove() throws RemoteException, RemoveException
    {
        String sql = "Delete from TIMEZONEENTRY where timeZoneEntryId = " + getTimeZoneEntryId();
        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoveException re = new RemoveException("TimeZoneEntry Entity - remove() exception");

            logger.error(re.getMessage());
            logger.error("remove sql: " + sql);
            logger.error(e);

            throw re;
        }
    }


   /**
    * Attempts to find the record that corresponds to the primary key.
    * @param  a TimeZoneEntryPK primary key
    * @return this instance
    */
    public TimeZoneEntry findByPrimaryKey(TimeZoneEntryPK pk) throws RemoteException, FinderException
   {
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from TimeZoneEntry where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              /**can only be one record*/
              break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "TimeZoneEntry Entity: @findByPrimaryKey(), key= " + pk + ", time zone record not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("TimeZoneEntry Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    ///do not use!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //potential broken definitely stupid.
    public TimeZoneEntry findByDeal(DealPK pk) throws RemoteException, FinderException
    {

      StringBuffer sqlbuf = new StringBuffer("Select * from timeZoneEntry where ");
      sqlbuf.append("timeZoneEntryid = (Select timeZoneentryid from BranchProfile");
      sqlbuf.append(" where branchProfileId = (select branchProfileid from deal ");
      sqlbuf.append(" where dealid = ").append( pk.getId() ).append(" and copyId = " + pk.getCopyId() +"))");

      String sql = sqlbuf.toString();
      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              /**can only be one record*/
              break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "TimeZoneEntry Entity: @findByDeal(), dealPk= " + pk + ", time zone record not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("TimeZoneEntry Entity - findByDeal() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return this;
    }


  /**
    *
    * @return Vector of time zone entities
    */

    public Vector findByAll() throws RemoteException, FinderException
   {
      String sql = "Select * from TimeZoneEntry";

      boolean gotRecord = false;
      Vector records = new Vector();

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              TimeZoneEntry tze = new TimeZoneEntry(srk);
              tze.setPropertiesFromQueryResult(key);
              records.addElement(tze);

              // all records into hash table
              tzTable.put(new Integer(tze.getTimeZoneEntryId()), tze);
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "TimeZoneEntry: @findByAll() :: no records found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("TimeZoneEntry - findByAll() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return records;
    }


    /**
    * Methods implemented in Enterprise Bean Instances that have no remote object
    * counterparts - i.e. EJB container callback methods, and context related methods.
    **/
    public void setEntityContext(EntityContext ctx)
    {
        this.ctx = ctx;
    }

    public void unsetEntityContext()
    {
        ctx = null;
    }

    public void ejbActivate() throws RemoteException{;}
    public void ebjPassivate () throws RemoteException {;}

    public void ejbLoad() throws RemoteException
    {
        /**implementation not required for now!*/
    }


    public void ejbStore() throws RemoteException
    {
        String sql = "Update TIMEZONEENTRY set "       +
                     "timeZoneEntryId    = "            + getTimeZoneEntryId() + ", " +
                     "tzeDescription  = '"              + getTzeDescription() + "', " +
                     "tzeSysId = '"                     + getTzeSysId() + "' " +
                     "tzeCustomId = '"                  + getTzeCustomId() + "' " +
                     "Where timeZoneEntryId = "         + getTimeZoneEntryId();

        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoteException re = new RemoteException("TimeZoneEntry Entity - ejbStore() exception");

            logger.error(re.getMessage());
            logger.error("ejbStore sql: " + sql);
            logger.error(e);

            throw re;
        }
        
        //4.4 Entity Cache
        ThreadLocalEntityCache.writeToCache(this, this.getPk());
    }

  /**
     *  gets the tzTable associated with this entity
     *
     *   @return a Hashtable
     */

   public static Hashtable getTzTable ()
   {
    return tzTable;
   }

   	/**
     *   gets the holidayID associated with this entity
     *
     *   @return an int
     */
   public int getTimeZoneEntryId()
	 {
     return timeZoneEntryId;
   }

   	/**
     *   gets the tzeDescription associated with this entity
     *
     *   @return a String
     */

   public String getTzeDescription () {
    return tzeDescription;
   }

    /**
     *   gets the tzeSysId value associated with this entity
     *
     *   @return a String
     */
    public String getTzeSysId () {
      return tzeSysId;
    }


    /**
     *   gets the tzeCustomId value associated with this entity
     *
     *   @return a String
     */
    public String getTzeCustomId() {
      return tzeCustomId;
    }


   	/**
     *   sets the tzTable associated with this entity
     *
     *  @param  tzTable
     *  @return
     */
  public void setTzTable(Hashtable tzTable) {
    this.tzTable = tzTable;
  }

 	/**
     *   sets the timeZoneEntryId associated with this entity
     *
     *  @param  int id
     *  @return
     */
  public void setTimeZoneEntryId(int id){
     timeZoneEntryId = id;
  }


  /**
     *   sets the tzeDescription associated with this entity
     *
     *  @param  String tzeDescription
     *  @return
     */
  public void setTzeDescription (String desc) {
    tzeDescription = desc;
  }



  /**
     *  sets the tzeSysId associated with this entity
     *
     *  @param
     *  @return
  */
  public void setTzeSysId (String sysId) {
    tzeSysId = sysId;
  }

  /**
     *  sets the tzeCustomId associated with this entity
     *
     *  @param
     *  @return
  */
  public void setTzeCustomId(String custId) {
    tzeCustomId = custId;
  }

   /**
    *Implemetatiion
   */
   public void setPropertiesFromQueryResult(int key) throws Exception
   {
      setTimeZoneEntryId(jExec.getInt(key,1));
      setTzeDescription(jExec.getString(key,2));
      setTzeSysId(jExec.getString(key,3));
      setTzeCustomId(jExec.getString(key,4));
   }

   // New method to get the Custom TimeZone (i.e. timezone with CustomId as TimeZoneId) -- By BILLY 29Nov2001
   public TimeZone getCustTimeZone()
   {
      TimeZone tz = TimeZone.getTimeZone(tzeSysId);
      tz.setID(tzeCustomId);
      return tz;
   }
}
