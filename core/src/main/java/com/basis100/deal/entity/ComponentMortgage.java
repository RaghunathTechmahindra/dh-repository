/*
 * @(#)ComponentMortgage.java May 1, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: ComponentMortgage
 * </p>
 * <p>
 * Description: This is an entity class representing ComponentMortgage. This
 * class provides accessor and mutator methods for all the entity attributes.
 * This class will map the entity and database fields. This class will save the
 * appropriate data into the ComponentMortgage Table.
 * </p>
 * @version 1.0 Initial Version
 * @version 1.1 XS_11.6 10-Jun-2008 - Added getEntityTableName(), getClassId(),
 *          getPk(), performUpdate() methods, Added constructor's and added code
 *          comments - Added testChange(...) method for all setter methods.
 * @version 1.2 19-Jun-2008 Added findByComponent() for copy management
 * @version 1.3 3 -July-2008 fixed to avoid to set empty for checkBox, i.e. miAllocationFlag, 
 *                           PropertytaxAllocatdFlag, cachBackOverride 
 * @version 1.4 XS_2.26 14-Jul-2008 added create(int componentId, int copyId) method
 * @version 1.5 XS_2.26 14-Jul-2008 deleted create(int componentId, int copyId,int paymentFrequencyId, 
 * 									int prepaymentOptionsId,int privilegePaymementId, String cashBackAmountOverride
 * 									,String rateLock) as paymentFrequencyId, prepaymentOptionsId,privilegePaymementId
 * 									cashBackAmountOverride , rateLock are defaulted in the DB 
 */
public class ComponentMortgage extends DealEntity
{
    protected int componentId;

    protected int copyId;

    protected int institutionProfileId;

    protected int paymentFrequencyId;

    protected int privilegePaymentId;

    protected int prePaymentOptionsId;

    protected int actualPaymentTerm;

    protected double additionalPrincipal;

    protected double advanceHold;

    protected int amortizationTerm;

    protected double balanceRemainingAtEndOfTerm;

    protected double buyDownRate;

    protected double cashBackAmount;

    protected String cashBackAmountOverride;

    protected double cashBackPercent;

    protected String commissionCode;

    protected double discount;

    protected int effectiveAmortizationMonths;

    protected String existingAccountIndicator;

    protected String existingAccountNumber;

    protected Date firstPaymentDate;

    protected Date firstPaymentDateMonthly;

    protected int iadNumberOfDays;

    protected double interestAdjustmentAmount;

    protected Date interestAdjustmentDate;

    protected Date maturityDate;

    protected String miAllocateFlag;

    protected double netInterestRate;

    protected double pAndIPaymentAmount;

    protected double pAndIPaymentAmountMonthly;

    protected double perDiemInterestAmount;

    protected double premium;

    protected String propertyTaxAllocateFlag;

    protected double propertyTaxEscrowAmount;

    protected int rateGuaranteePeriod;

    protected String rateLock;

    protected double totalMortgageAmount;

    protected double totalPaymentAmount;

    protected double mortgageAmount;

    protected ComponentMortgagePK pk;

    /**
     * <p>
     * Description:Default constructor.
     * </p>
     * @version 1.0 Initial Version
     */
    public ComponentMortgage()
    {

    }

    /**
     * <p>
     * Description: Creates the ComponentMortgage entity object by taking
     * session resource kit object and calls the super class constructor by
     * passing the session resource kit object.
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @param srk
     *            stores the session level information.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentMortgage(SessionResourceKit srk) throws RemoteException,
            FinderException
    {
        super(srk);
    }

    /**
     * <p>
     * Description: Creates the ComponentMortgage entity object by taking
     * session resource kit and calc monitor objects and calls the super class
     * constructor by passing the session resource kit and calc monitor objects.
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @param srk
     *            stores the session level information.
     * @param dcm
     *            runs the calculation of this ComponentMortgage entity.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentMortgage(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException
    {
        super(srk, dcm);
    }

    /**
     * <p>
     * Description: Creates the ComponentMortgage entity object by taking
     * session resource kit, component id and the copy id of the existing
     * Component.Calls the super class constructor and creates the primary key
     * for the ComponentMortgage entity and calls the findByPrimaryKey() method.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.6 Modified id to componentId in the constructor
     * @param srk
     *            stores the session level information.
     * @param componentId
     *            component id value passed to the entity PK class.
     * @param copyId
     *            copy id of the Component.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     * @see com.basis100.deal.entity.ComponentMortgage#findByPrimaryKey(com.basis100.deal.pk.ComponentMortgagePK )
     */
    public ComponentMortgage(SessionResourceKit srk, int componentId, int copyId)
            throws RemoteException, FinderException
    {
        super(srk);

        pk = new ComponentMortgagePK(componentId, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * <p>
     * Description: Creates the ComponentMortgage entity object by taking
     * session resource kit and calc monitor objects, component id and the copy
     * id of the existing Component.Calls the super class constructor and
     * creates the primary key for the ComponentMortgage entity and calls the
     * findByPrimaryKey() method.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.6 Modified id to componentId in the constructor
     * @param srk
     *            stores the session level information.
     * @param componentId
     *            component id value passed to the entity PK class.
     * @param dcm
     *            runs the calculation of this ComponentMortgage entity.
     * @param copyId
     *            copy id of the Component.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     * @see com.basis100.deal.entity.ComponentMortgage#findByPrimaryKey(com.basis100.deal.pk.ComponentMortgagePK )
     */
    public ComponentMortgage(SessionResourceKit srk, CalcMonitor dcm,
            int componentId, int copyId) throws RemoteException,
            FinderException
    {
        super(srk, dcm);

        pk = new ComponentMortgagePK(componentId, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * <p>
     * Description: Retrieves a row from the database using the primary key
     * passed in.
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @param pk
     *            primary key object of the ComponentMortgage entity.
     * @return ComponentMortgage Component Mortgage object.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     */
    public ComponentMortgage findByPrimaryKey(ComponentMortgagePK pk)
            throws RemoteException, FinderException
    {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from ComponentMortgage where componentId = "
                + pk.getId() + " AND copyId = " + pk.getCopyId();

        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "ComponentMortgage Entity: @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                if(getSilentMode() == false)
                    logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException(
                    "ComponentMortgage Entity - findByPrimaryKey() exception");
            if(getSilentMode() == false){
                logger.error(fe.getMessage());
                logger.error("finder sql: " + sql);
                logger.error(e);
            }
            throw fe;
        }
        this.copyId = pk.getCopyId();
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * @param key
     * @throws Exception
     */
    private void setPropertiesFromQueryResult(int key) throws Exception
    {
        this.setComponentId(jExec.getInt(key, "COMPONENTID"));
        this.setCopyId(jExec.getInt(key, "COPYID"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        this.setPaymentFrequencyId(jExec.getInt(key, "PAYMENTFREQUENCYID"));
        this.setPrivilegePaymentId(jExec.getInt(key, "PRIVILEGEPAYMENTID"));
        this.setPrePaymentOptionsId(jExec.getInt(key, "PREPAYMENTOPTIONSID"));
        this.setActualPaymentTerm(jExec.getInt(key, "ACTUALPAYMENTTERM"));
        this
                .setAdditionalPrincipal(jExec.getDouble(key,
                        "ADDITIONALPRINCIPAL"));
        this.setAdvanceHold(jExec.getDouble(key, "ADVANCEHOLD"));
        this.setAmortizationTerm(jExec.getInt(key, "AMORTIZATIONTERM"));
        this.setBalanceRemainingAtEndOfTerm(jExec.getDouble(key,
                "BALANCEREMAININGATENDOFTERM"));
        this.setBuyDownRate(jExec.getDouble(key, "BUYDOWNRATE"));
        this.setCashBackAmount(jExec.getDouble(key, "CASHBACKAMOUNT"));
        this.setCashBackAmountOverride(jExec.getString(key,
                "CASHBACKAMOUNTOVERRIDE"));
        this.setCashBackPercent(jExec.getDouble(key, "CASHBACKPERCENT"));
        this.setCommissionCode(jExec.getString(key, "COMMISSIONCODE"));
        this.setDiscount(jExec.getDouble(key, "DISCOUNT"));
        this.setEffectiveAmortizationMonths(jExec.getInt(key,
                "EFFECTIVEAMORTIZATIONMONTHS"));
        this.setExistingAccountIndicator(jExec.getString(key,
                "EXISTINGACCOUNTINDICATOR"));
        this.setExistingAccountNumber(jExec.getString(key,
                "EXISTINGACCOUNTNUMBER"));
        this.setFirstPaymentDate(jExec.getDate(key, "FIRSTPAYMENTDATE"));
        this.setFirstPaymentDateMonthly(jExec.getDate(key,
                "FIRSTPAYMENTDATEMONTHLY"));
        this.setIadNumberOfDays(jExec.getInt(key, "IADNUMBEROFDAYS"));
        this.setInterestAdjustmentAmount(jExec.getDouble(key,
                "INTERESTADJUSTMENTAMOUNT"));
        this.setInterestAdjustmentDate(jExec.getDate(key,
                "INTERESTADJUSTMENTDATE"));
        this.setMaturityDate(jExec.getDate(key, "MATURITYDATE"));
        this.setMiAllocateFlag(jExec.getString(key, "MIALLOCATEFLAG"));
        this.setNetInterestRate(jExec.getDouble(key, "NETINTERESTRATE"));
        this.setPAndIPaymentAmount(jExec.getDouble(key, "PANDIPAYMENTAMOUNT"));
        this.setPAndIPaymentAmountMonthly(jExec.getDouble(key,
                "PANDIPAYMENTAMOUNTMONTHLY"));
        this.setPerDiemInterestAmount(jExec.getDouble(key,
                "PERDIEMINTERESTAMOUNT"));
        this.setPremium(jExec.getDouble(key, "PREMIUM"));
        this.setPropertyTaxAllocateFlag(jExec.getString(key,
                "PROPERTYTAXALLOCATEFLAG"));
        this.setPropertyTaxEscrowAmount(jExec.getDouble(key,
                "PROPERTYTAXESCROWAMOUNT"));
        this.setRateGuaranteePeriod(jExec.getInt(key, "RATEGUARANTEEPERIOD"));
        this.setRateLock(jExec.getString(key, "RATELOCK"));
        this
                .setTotalMortgageAmount(jExec.getDouble(key,
                        "TOTALMORTGAGEAMOUNT"));
        this.setTotalPaymentAmount(jExec.getDouble(key, "TOTALPAYMENTAMOUNT"));
        this.setMortgageAmount(jExec.getDouble(key, "MORTGAGEAMOUNT"));

    }

    /**
     * @param componentId
     * @param copyId
     * @param paymentFrequencyId
     * @param prepaymentOptionsId
     * @param privilegePaymementId
     * @param cashBackAmountOverride
     * @param rateLock
     * @return
     * @throws RemoteException
     * @throws CreateException
     */
    public ComponentMortgage create(int componentId, int copyId) throws RemoteException, CreateException
    {

        pk = new ComponentMortgagePK(componentId, copyId);
        // SQL
        String sql = "Insert into ComponentMortgage( componentId, copyId, institutionProfileId) Values ( "
                + componentId
                + ","
                + copyId
                + ","
                + srk.getExpressState().getDealInstitutionId()
                + ")";

        // Execute
        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the create
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "ComponentMortgage Entity - Component - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        if (dcm != null)
            dcm.inputCreatedEntity(this);

        return this;
    }

    /**
     * <p>
     * Description: Returns the Component id value.
     * </p>
     * @version 1.0 Initial Version
     * @return the componentId
     */
    public int getComponentId()
    {
        return componentId;
    }

    /**
     * <p>
     * Description: Sets the Component id value.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 11-Jun-2008 - Added testChange method.
     * @param componentId -
     *            the componentId to set
     */
    public void setComponentId(int componentId)
    {
        this.testChange("componentId", componentId);
        this.componentId = componentId;
    }

    /**
     * <p>
     * Description: Returns the copyId.
     * </p>
     * @version 1.0 Initial Version
     * @return the copyId
     */
    public int getCopyId()
    {
        return copyId;
    }

    /**
     * <p>
     * Description: Sets the copyId value.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 11-Jun-2008 - Added testChange method.
     * @param copyId
     *            the copyId to set
     */
    public void setCopyId(int copyId)
    {
        this.testChange("copyId", copyId);
        this.copyId = copyId;
    }

    /**
     * <p>
     * Description: Returns the institutionProfileId value.
     * </p>
     * @version 1.0 Initial Version
     * @return the institutionProfileId
     */
    public int getInstitutionProfileId()
    {
        return institutionProfileId;
    }

    /**
     * <p>
     * Description: Sets institutionProfile Id.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param institutionProfileId -
     *            the institutionProfileId to set
     */
    public void setInstitutionProfileId(int institutionProfileId)
    {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    /**
     * <p>
     * Description: Returns the paymentFrequency id.
     * </p>
     * @version 1.0 Initial Version
     * @return the institutionProfileId
     */
    public int getPaymentFrequencyId()
    {
        return paymentFrequencyId;
    }

    /**
     * <p>
     * Description: Sets the paymentFrequencyId.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param paymentFrequencyId
     *            the paymentFrequencyId to set
     */
    public void setPaymentFrequencyId(int paymentFrequencyId)
    {
        this.testChange("paymentFrequencyId", paymentFrequencyId);
        this.paymentFrequencyId = paymentFrequencyId;
    }

    /**
     * <p>
     * Description: Returns the privilegePaymentId value.
     * </p>
     * @version 1.0 Initial Version
     * @return the privilegePaymentId
     */
    public int getPrivilegePaymentId()
    {
        return privilegePaymentId;
    }

    /**
     * <p>
     * Description: Sets the privilegePaymentId value.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param privilegePaymentId
     *            the privilegePaymentId to set
     */
    public void setPrivilegePaymentId(int privilegePaymentId)
    {
        this.testChange("privilegePaymentId", privilegePaymentId);
        this.privilegePaymentId = privilegePaymentId;
    }

    /**
     * <p>
     * Description: Returns the prePaymentOptionsId.
     * </p>
     * @version 1.0 Initial Version
     * @return the prePaymentOptionsId
     */
    public int getPrePaymentOptionsId()
    {
        return prePaymentOptionsId;
    }

    /**
     * <p>
     * Description: Sets the prePaymentOptions id value.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param prePaymentOptionsId
     *            the prePaymentOptionsId to set
     */
    public void setPrePaymentOptionsId(int prePaymentOptionsId)
    {
        this.testChange("prePaymentOptionsId", prePaymentOptionsId);
        this.prePaymentOptionsId = prePaymentOptionsId;
    }

    /**
     * <p>
     * Description: Returns the actualPaymentTerm.
     * </p>
     * @version 1.0 Initial Version
     * @return the actualPaymentTerm
     */
    public int getActualPaymentTerm()
    {
        return actualPaymentTerm;
    }

    /**
     * <p>
     * Description: Sets the actualPaymentTerm.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param actualPaymentTerm
     *            the actualPaymentTerm to set
     */
    public void setActualPaymentTerm(int actualPaymentTerm)
    {
        this.testChange("actualPaymentTerm", actualPaymentTerm);
        this.actualPaymentTerm = actualPaymentTerm;
    }

    /**
     * <p>
     * Description: Returns the additionalPrincipal.
     * </p>
     * @version 1.0 Initial Version
     * @return the additionalPrincipal
     */
    public double getAdditionalPrincipal()
    {
        return additionalPrincipal;
    }

    /**
     * <p>
     * Description: Sets the additionalPrincipal.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param additionalPrincipal
     *            the additionalPrincipal to set
     */
    public void setAdditionalPrincipal(double additionalPrincipal)
    {
        this.testChange("additionalPrincipal", additionalPrincipal);
        this.additionalPrincipal = additionalPrincipal;
    }

    /**
     * <p>
     * Description: Returns the advanceHold.
     * </p>
     * @version 1.0 Initial Version
     * @return the advanceHold
     */
    public double getAdvanceHold()
    {
        return advanceHold;
    }

    /**
     * <p>
     * Description: Sets the advanceHold.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param advanceHold
     *            the advanceHold to set
     */
    public void setAdvanceHold(double advanceHold)
    {
        this.testChange("advanceHold", advanceHold);
        this.advanceHold = advanceHold;
    }

    /**
     * <p>
     * Description: Returns the amortizationTerm.
     * </p>
     * @version 1.0 Initial Version
     * @return the amortizationTerm
     */
    public int getAmortizationTerm()
    {
        return amortizationTerm;
    }

    /**
     * <p>
     * Description: Sets the amortizationTerm.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param amortizationTerm
     *            the amortizationTerm to set
     */
    public void setAmortizationTerm(int amortizationTerm)
    {
        this.testChange("amortizationTerm", amortizationTerm);
        this.amortizationTerm = amortizationTerm;
    }

    /**
     * <p>
     * Description: Returns the balanceRemainingAtEndOfTerm.
     * </p>
     * @version 1.0 Initial Version
     * @return the balanceRemainingAtEndOfTerm
     */
    public double getBalanceRemainingAtEndOfTerm()
    {
        return balanceRemainingAtEndOfTerm;
    }

    /**
     * <p>
     * Description: Sets the balanceRemainingAtEndOfTerm.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param balanceRemainingAtEndOfTerm
     *            the balanceRemainingAtEndOfTerm to set
     */
    public void setBalanceRemainingAtEndOfTerm(
            double balanceRemainingAtEndOfTerm)
    {
        this.testChange("balanceRemainingAtEndOfTerm",
                balanceRemainingAtEndOfTerm);
        this.balanceRemainingAtEndOfTerm = balanceRemainingAtEndOfTerm;
    }

    /**
     * <p>
     * Description: Returns the buyDownRate.
     * </p>
     * @version 1.0 Initial Version
     * @return the buyDownRate
     */
    public double getBuyDownRate()
    {
        return buyDownRate;
    }

    /**
     * <p>
     * Description: Sets the buyDownRate.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param buyDownRate
     *            the buyDownRate to set
     */
    public void setBuyDownRate(double buyDownRate)
    {
        this.testChange("buyDownRate", buyDownRate);
        this.buyDownRate = buyDownRate;
    }

    /**
     * <p>
     * Description: Returns the cashBackAmount.
     * </p>
     * @version 1.0 Initial Version
     * @return the cashBackAmount
     */
    public double getCashBackAmount()
    {
        return cashBackAmount;
    }

    /**
     * <p>
     * Description: Sets the cashBackAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param cashBackAmount
     *            the cashBackAmount to set
     */
    public void setCashBackAmount(double cashBackAmount)
    {
        this.testChange("cashBackAmount", cashBackAmount);
        this.cashBackAmount = cashBackAmount;
    }

    /**
     * <p>
     * Description: Returns the cashBackAmountOverride.
     * </p>
     * @version 1.0 Initial Version
     * @return the cashBackAmountOverride
     */
    public String getCashBackAmountOverride()
    {

        return cashBackAmountOverride;
    }

    /**
     * <p>
     * Description: Sets the cashBackAmountOverride.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param cashBackAmountOverride
     *            the cashBackAmountOverride to set
     */
    public void setCashBackAmountOverride(String cashBackAmountOverride)
    {
        String value = "N"; 
        if ("Y".equalsIgnoreCase(cashBackAmountOverride)) {
            value = "Y";
        }
        this.testChange("cashBackAmountOverride", value);
        this.cashBackAmountOverride = value;
    }

    /**
     * <p>
     * Description: Returns the cashBackPercent.
     * </p>
     * @version 1.0 Initial Version
     * @return the cashBackPercent
     */
    public double getCashBackPercent()
    {
        return cashBackPercent;
    }

    /**
     * <p>
     * Description: Sets the cashBackPercent.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param cashBackPercent
     *            the cashBackPercent to set
     */
    public void setCashBackPercent(double cashBackPercent)
    {
        this.testChange("cashBackPercent", cashBackPercent);
        this.cashBackPercent = cashBackPercent;
    }

    /**
     * <p>
     * Description: Returns the commissionCode.
     * </p>
     * @version 1.0 Initial Version
     * @return the commissionCode
     */
    public String getCommissionCode()
    {
        return commissionCode;
    }

    /**
     * <p>
     * Description: Sets the commissionCode.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param commissionCode
     *            the commissionCode to set
     */
    public void setCommissionCode(String commissionCode)
    {
        this.testChange("commissionCode", commissionCode);
        this.commissionCode = commissionCode;
    }

    /**
     * <p>
     * Description: Returns the discount.
     * </p>
     * @version 1.0 Initial Version
     * @return the discount
     */
    public double getDiscount()
    {
        return discount;
    }

    /**
     * <p>
     * Description: Sets the discount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param discount
     *            the discount to set
     */
    public void setDiscount(double discount)
    {
        this.testChange("discount", discount);
        this.discount = discount;
    }

    /**
     * <p>
     * Description: Returns the effectiveAmortizationMonths.
     * </p>
     * @version 1.0 Initial Version
     * @return the effectiveAmortizationMonths
     */
    public int getEffectiveAmortizationMonths()
    {
        return effectiveAmortizationMonths;
    }

    /**
     * <p>
     * Description: Sets the effectiveAmortizationMonths.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param effectiveAmortizationMonths
     *            the effectiveAmortizationMonths to set
     */
    public void setEffectiveAmortizationMonths(int effectiveAmortizationMonths)
    {
        this.testChange("effectiveAmortizationMonths",
                effectiveAmortizationMonths);
        this.effectiveAmortizationMonths = effectiveAmortizationMonths;
    }

    /**
     * <p>
     * Description: Returns the existingAccountIndicator.
     * </p>
     * @version 1.0 Initial Version
     * @return the existingAccountIndicator
     */
    public String getExistingAccountIndicator()
    {
        return existingAccountIndicator;
    }

    /**
     * <p>
     * Description: Sets the existingAccountIndicator.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param existingAccountIndicator
     *            the existingAccountIndicator to set
     */
    public void setExistingAccountIndicator(String existingAccountIndicator)
    {
        this.testChange("existingAccountIndicator", existingAccountIndicator);
        this.existingAccountIndicator = existingAccountIndicator;
    }

    /**
     * <p>
     * Description: Returns the existingAccountNumber.
     * </p>
     * @version 1.0 Initial Version
     * @return the existingAccountNumber
     */
    public String getExistingAccountNumber()
    {
        return existingAccountNumber;
    }

    /**
     * <p>
     * Description: Sets the existingAccountNumber.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param existingAccountNumber
     *            the existingAccountNumber to set
     */
    public void setExistingAccountNumber(String existingAccountNumber)
    {
        this.testChange("existingAccountNumber", existingAccountNumber);
        this.existingAccountNumber = existingAccountNumber;
    }

    /**
     * <p>
     * Description: Returns the firstPaymentDate.
     * </p>
     * @version 1.0 Initial Version
     * @return the firstPaymentDate
     */
    public Date getFirstPaymentDate()
    {
        return firstPaymentDate;
    }

    /**
     * <p>
     * Description: Sets the firstPaymentDate.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param firstPaymentDate
     *            the firstPaymentDate to set
     */
    public void setFirstPaymentDate(Date firstPaymentDate)
    {
        this.testChange("firstPaymentDate", firstPaymentDate);
        this.firstPaymentDate = firstPaymentDate;
    }

    /**
     * <p>
     * Description: Returns the firstPaymentDateMonthly.
     * </p>
     * @version 1.0 Initial Version
     * @return the firstPaymentDateMonthly
     */
    public Date getFirstPaymentDateMonthly()
    {
        return firstPaymentDateMonthly;
    }

    /**
     * <p>
     * Description: Sets the firstPaymentDateMonthly.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param firstPaymentDateMonthly
     *            the firstPaymentDateMonthly to set
     */
    public void setFirstPaymentDateMonthly(Date firstPaymentDateMonthly)
    {
        this.testChange("firstPaymentDateMonthly", firstPaymentDateMonthly);
        this.firstPaymentDateMonthly = firstPaymentDateMonthly;
    }

    /**
     * <p>
     * Description: Returns the iadNumberOfDays.
     * </p>
     * @version 1.0 Initial Version
     * @return the iadNumberOfDays
     */
    public int getIadNumberOfDays()
    {
        return iadNumberOfDays;
    }

    /**
     * <p>
     * Description: Sets the iadNumberOfDays.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param iadNumberOfDays
     *            the iadNumberOfDays to set
     */
    public void setIadNumberOfDays(int iadNumberOfDays)
    {
        this.testChange("iadNumberOfDays", iadNumberOfDays);
        this.iadNumberOfDays = iadNumberOfDays;
    }

    /**
     * <p>
     * Description: Returns the interestAdjustmentAmount.
     * </p>
     * @version 1.0 Initial Version
     * @return the interestAdjustmentAmount
     */
    public double getInterestAdjustmentAmount()
    {
        return interestAdjustmentAmount;
    }

    /**
     * <p>
     * Description: Sets the interestAdjustmentAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param interestAdjustmentAmount
     *            the interestAdjustmentAmount to set
     */
    public void setInterestAdjustmentAmount(double interestAdjustmentAmount)
    {
        this.testChange("interestAdjustmentAmount", interestAdjustmentAmount);
        this.interestAdjustmentAmount = interestAdjustmentAmount;
    }

    /**
     * <p>
     * Description: Returns the interestAdjustmentDate.
     * </p>
     * @version 1.0 Initial Version
     * @return the interestAdjustmentDate
     */
    public Date getInterestAdjustmentDate()
    {
        return interestAdjustmentDate;
    }

    /**
     * <p>
     * Description: Sets the interestAdjustmentDate.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param interestAdjustmentDate
     *            the interestAdjustmentDate to set
     */
    public void setInterestAdjustmentDate(Date interestAdjustmentDate)
    {
        this.testChange("interestAdjustmentDate", interestAdjustmentDate);
        this.interestAdjustmentDate = interestAdjustmentDate;
    }

    /**
     * <p>
     * Description: Returns the maturityDate.
     * </p>
     * @version 1.0 Initial Version
     * @return the maturityDate
     */
    public Date getMaturityDate()
    {
        return maturityDate;
    }

    /**
     * <p>
     * Description: Sets the maturityDate.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param maturityDate
     *            the maturityDate to set
     */
    public void setMaturityDate(Date maturityDate)
    {
        this.testChange("maturityDate", maturityDate);
        this.maturityDate = maturityDate;
    }

    /**
     * <p>
     * Description: Returns the miAllocateFlag.
     * </p>
     * @version 1.0 Initial Version
     * @return the miAllocateFlag
     */
    public String getMiAllocateFlag()
    {
        return miAllocateFlag;
    }

    /**
     * <p>
     * Description: Sets the miAllocateFlag.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param miAllocateFlag
     *            the miAllocateFlag to set
     */
    public void setMiAllocateFlag(String miAllocateFlag)
    {
        String value = "N"; 
        if ("Y".equalsIgnoreCase(miAllocateFlag)) {
            value = "Y";
        }
        this.testChange("miAllocateFlag", value);
        this.miAllocateFlag = value;
    }

    /**
     * <p>
     * Description: Returns the netInterestRate.
     * </p>
     * @version 1.0 Initial Version
     * @return the netInterestRate
     */
    public double getNetInterestRate()
    {
        return netInterestRate;
    }

    /**
     * <p>
     * Description: Sets the netInterestRate.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param netInterestRate
     *            the netInterestRate to set
     */
    public void setNetInterestRate(double netInterestRate)
    {
        this.testChange("netInterestRate", netInterestRate);
        this.netInterestRate = netInterestRate;
    }

    /**
     * <p>
     * Description: Returns the pAndIPaymentAmount.
     * </p>
     * @version 1.0 Initial Version
     * @return the pAndIPaymentAmount
     */
    public double getPAndIPaymentAmount()
    {
        return pAndIPaymentAmount;
    }

    /**
     * <p>
     * Description: Sets the pAndIPaymentAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param andIPaymentAmount
     *            the pAndIPaymentAmount to set
     */
    public void setPAndIPaymentAmount(double andIPaymentAmount)
    {
        this.testChange("pAndIPaymentAmount", andIPaymentAmount);
        pAndIPaymentAmount = andIPaymentAmount;
    }

    /**
     * <p>
     * Description: Returns the pAndIPaymentAmountMonthly.
     * </p>
     * @version 1.0 Initial Version
     * @return the pAndIPaymentAmountMonthly
     */
    public double getPAndIPaymentAmountMonthly()
    {
        return pAndIPaymentAmountMonthly;
    }

    /**
     * <p>
     * Description: Sets the pAndIPaymentAmountMonthly.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param pAndIPaymentAmountMonthly
     *            the pAndIPaymentAmountMonthly to set
     */
    public void setPAndIPaymentAmountMonthly(double pAndIPaymentAmountMonthly)
    {
        this.testChange("pAndIPaymentAmountMonthly", pAndIPaymentAmountMonthly);
        this.pAndIPaymentAmountMonthly = pAndIPaymentAmountMonthly;
    }

    /**
     * <p>
     * Description: Returns the perDiemInterestAmount.
     * </p>
     * @version 1.0 Initial Version
     * @return the perDiemInterestAmount
     */
    public double getPerDiemInterestAmount()
    {
        return perDiemInterestAmount;
    }

    /**
     * <p>
     * Description: Sets the perDiemInterestAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param perDiemInterestAmount
     *            the perDiemInterestAmount to set
     */
    public void setPerDiemInterestAmount(double perDiemInterestAmount)
    {
        this.testChange("perDiemInterestAmount", perDiemInterestAmount);
        this.perDiemInterestAmount = perDiemInterestAmount;
    }

    /**
     * <p>
     * Description: Returns the premium.
     * </p>
     * @version 1.0 Initial Version
     * @return the premium
     */
    public double getPremium()
    {
        return premium;
    }

    /**
     * <p>
     * Description: Sets the premium.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param premium
     *            the premium to set
     */
    public void setPremium(double premium)
    {
        this.testChange("premium", premium);
        this.premium = premium;

    }

    /**
     * <p>
     * Description: Returns the propertyTaxAllocateFlag.
     * </p>
     * @version 1.0 Initial Version
     * @return the propertyTaxAllocateFlag
     */
    public String getPropertyTaxAllocateFlag()
    {
        return propertyTaxAllocateFlag;
    }

    /**
     * <p>
     * Description: Sets the propertyTaxAllocateFlag.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param propertyTaxAllocateFlag
     *            the propertyTaxAllocateFlag to set
     */
    public void setPropertyTaxAllocateFlag(String propertyTaxAllocateFlag)
    {
        String value = "N"; 
        if ("Y".equalsIgnoreCase(propertyTaxAllocateFlag)) {
            value = "Y";
        }
        this.testChange("propertyTaxAllocateFlag", value);
        this.propertyTaxAllocateFlag = value;
    }

    /**
     * <p>
     * Description: Returns the propertyTaxEscrowAmount.
     * </p>
     * @version 1.0 Initial Version
     * @return the propertyTaxEscrowAmount
     */
    public double getPropertyTaxEscrowAmount()
    {
        return propertyTaxEscrowAmount;
    }

    /**
     * <p>
     * Description: Sets the propertyTaxEscrowAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param propertyTaxEscrowAmount
     *            the propertyTaxEscrowAmount to set
     */
    public void setPropertyTaxEscrowAmount(double propertyTaxEscrowAmount)
    {
        this.testChange("propertyTaxEscrowAmount", propertyTaxEscrowAmount);
        this.propertyTaxEscrowAmount = propertyTaxEscrowAmount;
    }

    /**
     * <p>
     * Description: Returns the rateGuaranteePeriod.
     * </p>
     * @version 1.0 Initial Version
     * @return the rateGuaranteePeriod
     */
    public int getRateGuaranteePeriod()
    {
        return rateGuaranteePeriod;
    }

    /**
     * <p>
     * Description: Sets the rateGuaranteePeriod.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param rateGuaranteePeriod
     *            the rateGuaranteePeriod to set
     */
    public void setRateGuaranteePeriod(int rateGuaranteePeriod)
    {
        this.testChange("rateGuaranteePeriod", rateGuaranteePeriod);
        this.rateGuaranteePeriod = rateGuaranteePeriod;
    }

    /**
     * <p>
     * Description: Returns the rateLock.
     * </p>
     * @version 1.0 Initial Version
     * @return the rateLock
     */
    public String getRateLock()
    {
        return rateLock;
    }

    /**
     * <p>
     * Description: Sets the rateLock.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param rateLock
     *            the rateLock to set
     */
    public void setRateLock(String rateLock)
    {
        this.testChange("rateLock", rateLock);
        this.rateLock = rateLock;
    }

    /**
     * <p>
     * Description: Returns the totalMortgageAmount.
     * </p>
     * @version 1.0 Initial Version
     * @return the totalMortgageAmount
     */
    public double getTotalMortgageAmount()
    {
        return totalMortgageAmount;
    }

    /**
     * <p>
     * Description: Sets the totalMortgageAmount.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     * @param totalMortgageAmount
     *            the totalMortgageAmount to set
     */
    public void setTotalMortgageAmount(double totalMortgageAmount)
    {
        this.testChange("totalMortgageAmount", totalMortgageAmount);
        this.totalMortgageAmount = totalMortgageAmount;
    }

    /**
     *<p>
     *Description: Returns the totalPaymentAmount.
     *</p>
     *@version 1.0 Initial Version
     *@return the totalPaymentAmount
     */
    public double getTotalPaymentAmount()
    {
        return totalPaymentAmount;
    }

    /**
     *<p>
     *Description: Sets the totalPaymentAmount.
     *</p>
     *@version 1.0 Initial Version
     *@version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     *@param totalPaymentAmount the totalPaymentAmount to set
     */
    public void setTotalPaymentAmount(double totalPaymentAmount)
    {
        this.testChange("totalPaymentAmount", totalPaymentAmount);
        this.totalPaymentAmount = totalPaymentAmount;
    }

    /**
     *<p>
     *Description: Returns the mortgageAmount.
     *</p>
     *@version 1.0 Initial Version
     *@return the mortgageAmount
     */
    public double getMortgageAmount()
    {
        return mortgageAmount;
    }

    /**
     *<p>
     *Description: Sets the mortgageAmount.
     *</p>
     *@version 1.0 Initial Version
     *@version 1.1 XS_11.3 11-Jun-2008 - Added testChange method.
     *@param mortgageAmount the mortgageAmount to set
     */
    public void setMortgageAmount(double mortgageAmount)
    {
        this.testChange("mortgageAmount", mortgageAmount);
        this.mortgageAmount = mortgageAmount;
    }

    /**
     * <p>
     * Description: Performs updates on the ComponentMortgage entity
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @return int returns the no of rows updated.
     */
    protected int performUpdate() throws Exception
    {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0)
        {
            dcm.inputEntity(this);
        }

        return ret;
    }

    /**
     * <p>
     * Description: Returns the pk object
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @return IEntityBeanPK ComponentMortgage entity pk object
     */
    public IEntityBeanPK getPk()
    {
        return (IEntityBeanPK) this.pk;
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage entity table name
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @return String - table name of the entity
     */
    public String getEntityTableName()
    {
        return "ComponentMortgage";
    }

    /**
     * <p>
     * Description: Returns the class id of the ComponentMortgage entity
     * </p>
     * @version 1.0 XS_11.6 Initial Version
     * @return int - classid of ComponentMortgage entity.
     */
    public int getClassId()
    {

        return ClassId.COMPONENTMORTGAGE;
    }

    /**
     * <p>
     * Description: Gets a collection of ComponentMortgages for the component
     * </p>
     * @version 1.0 
     * @param ComponentPK
     *  
     */
    public Collection findByComponent(ComponentPK pk) throws Exception
    {

        StringBuffer sqlbuf = new StringBuffer(
                "Select * from ComponentMortgage where ");
        sqlbuf.append("componentId = ").append(pk.getId());
        sqlbuf.append(" and copyId = ").append(pk.getCopyId());

        Collection records = new ArrayList();

        try
        {
            int key = jExec.execute(sqlbuf.toString());

            for ( ; jExec.next(key); )
            {
                ComponentMortgage mortgage = new ComponentMortgage(srk, dcm);
                mortgage.setPropertiesFromQueryResult(key);
                mortgage.pk = new ComponentMortgagePK(
                        mortgage.getComponentId(), mortgage.getCopyId());

                records.add(mortgage);
            }

            jExec.closeData(key);

        }
        catch (Exception e)
        {
            logger.error("finder sql: " + sqlbuf.toString());
            logger.error(e);
            return null;
        }

        return records;
    }
    
    /**
     *   <p>isAuditable</p>
     *   <p>Descriptio: gets if this entity is auditable or not
     *   @version 1.0 XS_2.30
     *   @return true
     */
    public boolean isAuditable() {

        return true;
    }

    /**
     * returns EntityContext. 
     * @since 4.0 MCM, Oct 28, 2008, FXP23168, Hiro
     */
    protected EntityContext getEntityContext() throws RemoteException {

        EntityContext ctx = this.entityContext;
        try {
            Component comp = new Component(srk, componentId, copyId);
            ctx.setContextText(this.getEntityTableName());
            int langId = srk.getLanguageId();
            int instId = srk.getExpressState().getDealInstitutionId();
            String description = BXResources.getPickListDescription(instId,
                    "COMPONENTTYPE", comp.getComponentTypeId(), langId);
            ctx.setContextSource(description);
            ctx.setApplicationId(comp.getDealId());

        } catch (FinderException e) {
            logger.error(getClass().getSimpleName() + ".getEntityContext " +
                    "FinderException caught. ComponentId= " + componentId + " CopyId=" + copyId);
        }
        return ctx;
    } 

}
