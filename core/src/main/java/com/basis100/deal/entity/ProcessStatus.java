/*
 * ProcessStatus.java
 * 
 * Entity class (connectivity with AVM services)
 * 
 * Created: 19-June-2006
 * Author:  Asif Atcha
 * 
 * TODO: test create method.
 *       
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ProcessStatusPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * Corresponds to the ProcessStatus DB table.
 * 
 * @author AAtcha
 */
public class ProcessStatus extends DealEntity {
	
  public static final int AVAILABLE = 0;
  public static final int PROCESSING =1;
  public static final int PROCESSED =2;
  public static final int ERROR =3;
  public static final int RESPONSE_EXPIRED =4; //UNSOLICITED response
  
  protected int    processStatusId;
  protected String processStatusDesc;
 
  private ProcessStatusPK pk;

  /**
   * @param srk SessionResourceKit to use
   * @throws RemoteException
   */
  public ProcessStatus(SessionResourceKit srk) throws RemoteException {
    super(srk);
  }
  /**
   * @param srk SessionResourceKit to use
   * @throws RemoteException
   */
  public ProcessStatus(SessionResourceKit srk, CalcMonitor clm) throws RemoteException {
    super(srk,clm);
  }
  
  /**
   * @param srk SessionResourceKit to use
   * @param pk Primary Key corresponding to appropriate line in table
   * @throws RemoteException
   */
  public ProcessStatus(SessionResourceKit srk, int id) throws RemoteException, FinderException {
    super(srk);
    pk = new ProcessStatusPK(id);
    findByPrimaryKey(pk);
  }

  /**
   * Find the row corresponding to this PK and populate entity.
   * @param pk The primary key representing this row
   * @return This (now populated) entity.
   * @throws FinderException
   */
 public ProcessStatus findByPrimaryKey(ProcessStatusPK pk) 
      throws RemoteException, FinderException {    
	 
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM ProcessStatus" + pk.getWhereClause();
    
    boolean gotRecord = false;
    
    try {
      int key = jExec.execute(sql);
      
      for (; jExec.next(key);) {
        gotRecord = true;
        this.pk = pk;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }
      
      jExec.closeData(key);
      
      if (gotRecord == false) {
        String msg = "ProcessStatus Entity: @findByQuery(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("ProcessStatus Entity - findByQuery() exception");
      
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);
      
      throw fe;
    }
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }
 
 public Collection findByprocessStatusId(int processstatusid) throws Exception
 {
   Collection responses = new ArrayList();
   
   String sql = "Select * from ProcessStatus " ;
   sql += "where ProcessStatusId = " + processstatusid;
   
   try
   {
     int key = jExec.execute(sql);
     for (; jExec.next(key); )
     {
       ProcessStatus callbackresponse = new ProcessStatus(srk,jExec.getInt(key,"processStatusId"));
       callbackresponse.setPropertiesFromQueryResult(key);
       callbackresponse.pk = new ProcessStatusPK(callbackresponse.getProcessStatusId());
       responses.add(callbackresponse);
     }
     jExec.closeData(key);
   }
   catch (Exception e)
   {
     Exception fe = new Exception("Exception caught while getting Deal::ProcessStatus");
     logger.error(fe.getMessage());
     logger.error(e);
     throw fe;
   }
   return responses;
 }
 
 
 
  /**
   * Create a new primarykey from the response sequence.
   * @return
   * @throws CreateException
   */
  public ProcessStatusPK createPrimaryKey() throws CreateException {
    String sql = "Select PROCESSSTATUSSEQ.nextval from dual";
    int id = -1;

    try {
      int key = jExec.execute(sql);

      for(; jExec.next(key); ) {
        id = jExec.getInt(key, 1);
        break;
      }
      jExec.closeData(key);
      if(id == -1) {
        throw new CreateException();
      }
    }
    catch(Exception e) {
      CreateException ce = new CreateException("ProcessStatus createPrimaryKey() :" +
                              "exception: getting processStatusId from sequence");
      logger.error(ce.getMessage());
      logger.error(e);
      throw ce;
    }
   
    return new ProcessStatusPK(id);
  }
  
  
  
  /**
   * Create a new Event Callback Response object with minimum fields, insert it into Database, and return.
   * @param serviceTypeId
   * @param status
   * @return The newly created Event Callback Request.
   * @throws RemoteException
   * @throws CreateException
   */
  public ProcessStatus create(String processStatusDesc)
throws RemoteException, CreateException 
{
ProcessStatusPK pk = createPrimaryKey();

return create(pk, processStatusDesc);
}
  
  /**
   * Create a new Event Callback Response object with minimum fields, insert it into Database, and return.
   * @param pk
   * @param serviceTypeId
   * @param status
   * @return The newly created Event Callback Request.
   * @throws RemoteException
   * @throws CreateException
   */
  public ProcessStatus create(ProcessStatusPK pk, String processStatusDesc)
      throws RemoteException, CreateException 
  {
//	String sql = "Insert into ProcessStatus(" + pk.getName() +  ", serviceTypeId ,XmlResponse,processStatusDesc) Values ( " + pk.getId() + ", serviceTypeId, empty_clob(), processStatusDesc)";
		
	StringBuffer sqlb = new StringBuffer(
                 "INSERT INTO ProcessStatus (PROCESSSTATUSID, " +
                 "PROCESSSTATUSDESC) VALUES (");
    sqlb.append(sqlStringFrom(pk.getId()) + ", "); 
     sqlb.append(sqlStringFrom(processStatusDesc)+ ")");
    
    String sql = new String(sqlb);
    
    try {
    	
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate(); // track the creation
    }
    catch (Exception e) {
      CreateException ce = new CreateException("Process Status Entity - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);
      
      throw ce;
    }
    
    srk.setModified(true);
    return this;      
  }
  
  
  /**
   * @return Returns the ProcessStatus's primary key.
   */
  public IEntityBeanPK getPk() {
    return pk;
  }
  
  /**
   * Test for equality.
   * @param obj The (ProcessStatus) object to test for equality.
   */
  public boolean equals(Object obj) {
    if (obj instanceof ProcessStatus) {
      ProcessStatus otherObj = (ProcessStatus) obj;
      if  (this.processStatusId == otherObj.getProcessStatusId()) {
          return true;
      }
    }
    return false;   
  }
  
 
  /** Updates any Database field that has changed since the last synchronization
	*  ie. the last findBy... call
	*  @return the number of updates performed */
	protected int performUpdate()  throws Exception
	{
		String [] arrExcl = { "XMLRESPONSE" };

		int ret = doPerformUpdate(arrExcl);
	
		return ret;
	}
   

  private void setPropertiesFromQueryResult(int key) throws Exception {
    setProcessStatusId(jExec.getInt(key, "processStatusId"));
    setProcessStatusDesc(jExec.getString(key, "processStatusDesc"));
  }

  /**
	*  gets the value of instance fields as String.
	*  if a field does not exist (or the type is not serviced)** null is returned.
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*  @returns value of bound field as a String;
	*  @param  fieldName as String
	*
	*  Other entity types are not yet serviced   ie. You can't get the Province object
	*  for province.
	*/
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	*  if a field does not exist or is not serviced an Exception is thrown
	*  if the field exists (and the type is serviced) but the field is null an empty
	*  String is returned.
	*
	*  @param  fieldName as String
	*  @param  the value as a String
	*
	*  Other entity types are not yet serviced   ie. You can't set the Province object
	*  in Addr. */
	public void setField(String fieldName, String value) throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}
  
	
	/**
	   * Get Entity's table's name.
	   * @return Table name.
	   */
	  public String getEntityTableName() {
	    return "ProcessStatus";
	  }
	
	
	//------ START: getters and setters --------------------------------------
  
 
  /**
   * @return Returns the copy ID.
   */
  public int getCopyId() {
    return -1;
  }


  

  

  /**
   * @return Returns the callback ID.
   */
  public int getProcessStatusId() {
    return processStatusId;
  }

  /**
   * @param processStatusId The Process Status ID to set.
   */
  public void setProcessStatusId(int psId) {
    this.testChange("processStatusId", psId);
    this.processStatusId = psId;
    
  }
 
  
 
 
 
  public String getProcessStatusDesc()
  {
	  return processStatusDesc;
  }

  public void setProcessStatusDesc(String st)
	{
		this.testChange("processStatusDesc",st);
		this.processStatusDesc = st;
	}
  


  //------- END: getters and setters ---------------------------------------
}
