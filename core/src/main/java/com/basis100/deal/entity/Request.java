/*
 * Request.java
 * 
 * Request: general purpose request entity class.
 * 
 * created: 06-FEB-2006
 * author:  Edward Steel
 * 
 * TODO: test FindByDealIdAndCopyId : Request
 */

package com.basis100.deal.entity;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * Request class.
 * 
 * @author ESteel
 */
public class Request extends DealEntity {
    
    protected int    requestId;
    
    protected Date   requestDate;
    protected String statusMessage;
    protected Date   statusDate;
    protected int    dealId;
    protected int    copyId;
    protected int    serviceProductId;
    protected int    channelId;
    protected int    payloadTypeId;
    protected int    serviceTransactionTypeId;
    protected int    requestStatusId;
    protected int    userProfileId;
    
    //FXP27162
    private int    requestTypeId;
    private String requestTypeShortName;
    //End of FXP27162
  
    protected String channelTransactionKey;
  protected int     institutionProfileId;     
    private RequestPK   pk;

    /**
     * @param srk The SessionResourceKit to use.
     * @throws RemoteException
     */
    public Request(SessionResourceKit srk) throws RemoteException {
        super(srk);
    }
    
    public Request(SessionResourceKit srk, int id, int copyId)
            throws RemoteException, FinderException {

        super(srk);
        
        pk = new RequestPK(id, copyId);
        findByPrimaryKey(pk);
    }

    public RequestPK createPrimaryKey(int copyId) throws CreateException  {

        String sql = "Select Requestseq.nextval from dual";
        int id = -1;
        int key = 0;
        
        try {
          key = jExec.execute(sql);
    
          for(; jExec.next(key); ) {
            id = jExec.getInt(key, 1);
            break;
          }
          jExec.closeData(key);
          if(id == -1) {
            throw new CreateException();
          }
        }
        catch(Exception e) {
          CreateException ce = new CreateException(
            "Request createPrimaryKey() :exception: getting RequestId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }
        finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch (Exception e) {
                logger.debug(e.toString());
            }
        }
        
        return new RequestPK(id, copyId);
    }
  
    /**
     * Create a new Request object, insert it into Database, and return.
     * @param requestDate
     * @param dealId
     * @param copyId
     * @param requestStatusId
     * @param statusDate
     * @param userProfileId
     * @return The newly created Request.
     * @throws RemoteException
     * @throws CreateException
     */
    public Request create(DealPK dpk, int requestStatusId, Date statusDate,
                          int userProfileId)
        throws RemoteException, CreateException {
        this.pk = createPrimaryKey(dpk.getCopyId());
        
     
        StringBuffer sqlb = new StringBuffer (
            "INSERT INTO REQUEST( REQUESTID" 
            + ", REQUESTDATE"
            + ", DEALID, COPYID"
            + ", REQUESTSTATUSID, STATUSDATE, USERPROFILEID, INSTITUTIONPROFILEID "
            + " ) VALUES ( ");  
        
        sqlb.append(sqlStringFrom(pk.getId()) + ", ");      
        sqlb.append(sqlStringFrom(statusDate) + ", ");
        sqlb.append(sqlStringFrom(dpk.getId()) + ", ");
        sqlb.append(sqlStringFrom(dpk.getCopyId()) + ", ");
        sqlb.append(sqlStringFrom(requestStatusId) + ", ");                         
        sqlb.append(sqlStringFrom(statusDate) + ",");
        sqlb.append(sqlStringFrom(userProfileId) + ",");                                  
        sqlb.append(srk.getExpressState().getDealInstitutionId() + ")");
    
        String sql = new String(sqlb);
        
        int key = 0;
        try {
            key = jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate (); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException("Request Entity - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            
            throw ce;
        } finally {
            
            try {
                if (key != 0)
                    jExec.closeData(key);
            } catch (Exception e) {
                logger.error(e.toString());
            }
        }
        
        srk.setModified(true);
        return this;
    }
  
    
    /**
     * Get a Request's details from the database using the given primary key,
     * and populate this Request.
     * @param pk Primary key that identifies the Request.
     * @return This, with all details set from database with given primary key.
     * @throws FinderException
     */
    public Request findByPrimaryKey(RequestPK pk) throws FinderException {

      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "SELECT * FROM REQUEST" + pk.getWhereClause();
    
      boolean gotRecord = false;
      int key = 0;
  
      try {
        key = jExec.execute(sql);
  
        for (; jExec.next(key);) {
          gotRecord = true;
          this.pk = pk;
          setPropertiesFromQueryResult(key);
          break; // can only be one record
        }
  
        if (gotRecord == false) {
          String msg = "Request Entity: @findByQuery(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      } catch (Exception e) {
        FinderException fe = new FinderException("Request Entity - findByQuery() exception");
  
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);
  
        throw fe;
      }
      finally  {
          try {
              if(key != 0) {
                  jExec.closeData(key);
              }
          }
          catch (Exception e) {
              logger.error(e.toString());
          }
      }
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
    }
  
  /**
   * !!!!!!DO NOT USE : Use the method findByChannelTranKeyAndServProduct instead!!!!!!!!
   * <p>findByChannelTransactionKey</p>
   * <p>retreive Request record from Request table.
   * this method should not required VPD set since it will be used from WS, 
   * when asynchronous response comes back from Datx
   * Express don't know institutionId</p>
   * 
   * @param ctk
   * @return
   * @throws Exception
   * @deprecated
   */
  public Request findByChannelTransactionKey(String ctk) throws Exception
  {
    boolean gotRecord = false;

        if (ctk == null)
        {
            String msg = "Request Entity: @findByChannelTransactionKey(), key is NULL";
            logger.error(msg);
            throw new Exception(msg);
        }     

            /* assumes that higher requestdate would be the
             * most current
             */
            String sql = "Select * from Request where ChannelTransactionKey =" + sqlStringFrom(ctk) + " Order by requestdate desc";
          
             try
             {
                 int key = jExec.execute(sql);
                  for (; jExec.next(key); )
                  {
                gotRecord = true;
                     setPropertiesFromQueryResult(key);           
                  break;
                  }
                  jExec.closeData(key);
            if (gotRecord == false)
            {
                String msg =
                    "Request Entity: @findByChannelTransactionKey(), key= " +
                    ctk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }

              }
              catch (Exception e)
              {
                Exception fe = new Exception("Exception caught  while getting Deal::Borrowers");
                logger.error(fe.getMessage());
                logger.error(e);

                throw fe;
              }
               return this;
          }
     
  /**
   * Find Request by channel transaction key and serviceProductname
   * @param ctk
   * @param serviceProductName
   * @return
   * @throws Exception
   */
  public Request findByChannelTranKeyAndServProduct(String ctk,
            String serviceProductName) throws Exception {
        boolean gotRecord = false;

        if (ctk == null) {
            String msg = "Request Entity: @findByChannelTransactionKey(), key is NULL";
            logger.error(msg);
            throw new Exception(msg);
        }

        /*
         * assumes that higher requestdate would be the most current
         */
        String sql = "Select Request.* from Request, ServiceProduct where Request.ChannelTransactionKey ="
                + sqlStringFrom(ctk)
                + " and Request.serviceProductId = ServiceProduct.serviceProductId and"
                + " ServiceProduct.serviceProductName = "
                + sqlStringFrom(serviceProductName)
                + " Order by requestdate desc";
        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break;
            }
            jExec.closeData(key);
            if (gotRecord == false) {
                String msg = "Request Entity: @findByChannelTransactionKey(), key= "
                        + ctk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught  while getting Deal::Borrowers");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }
        return this;
    } 
    /**
     * Find Request using DealPK
     * 
     * @param dealPK
     *            DealPK to find by.
     * @return Collection of Requests
     * @throws Exception
     */
    public Collection findByDeal(DealPK pk) throws Exception {

      Collection requests = new ArrayList();

      /* assumes that higher numbered requestId would be the
       * most current
       */
      String sql = "Select * from Request" ;
            sql +=  " where dealId = " + pk.getId();
            sql +=  " order by requestId desc";
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              Request request = new Request(srk);
              request.setPropertiesFromQueryResult(key);
              request.pk = new RequestPK(request.getRequestId(), request.getCopyId());
              requests.add(request);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::Borrowers");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return requests;
    }


    /**
     * Find Request using DealPK
     *
     * @param dealPK DealPK to find by.
     * @return Collection of Requests
     * @throws Exception
     */
    public Collection findRequestsByDealIdAndCopyId(DealPK pk) throws Exception {

      Collection requests = new ArrayList();

      /* assumes that higher numbered requestId would be the
       * most current
       */
      String sql = "Select * from Request" ;
            sql +=  " where dealId = " + pk.getId();
            sql +=  " and copyId = " + pk.getCopyId();
            sql +=  " order by requestId desc";
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              Request request = new Request(srk);
              request.setPropertiesFromQueryResult(key);
              request.pk = new RequestPK(request.getRequestId(), request.getCopyId());
              requests.add(request);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::Borrowers");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return requests;
    }

    /** Find the last Request using DealPK
     *
     * @param pk
     * @return Collection of the most current Request
     * @throws Exception
     */

    public Collection findLastCollectionOfRequest(DealPK pk) throws Exception {

      Collection requests = new ArrayList();

      /* assumes that higher numbered requestId would be the
       * most current
       */
      /*String sql = "Select * from Request" ;
            sql +=  " where dealId = " + pk.getId();
            sql +=  " and rownum = 1";
            sql +=  " order by requestId desc";
      */
            // Venkata N - 26OCT2006
            // Commented the above query and modified to fetch the most recent Request
            // for a given dealId and CopyId
            // as the above query returns the row with physical rownum = 1 for the given dealId
            String sql = "select * from (select * from request " ;
                    sql += " where dealId = " + pk.getId();
                    sql += " order by requestdate desc) where rownum = 1";
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              Request request = new Request(srk);
              request.setPropertiesFromQueryResult(key);
              request.pk = new RequestPK(request.getRequestId(), request.getCopyId());
              requests.add(request);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Deal::Borrowers");
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return requests;
    }

    /** Uses findLastCollectionOfRequest() to return a Request object
     *
     * @param DealPK
     * @return Request
     * @throws Exception
     */
    public Request findLastRequest(DealPK pk) throws Exception {

        Collection c = null;
        Request r = null;

        c = findLastCollectionOfRequest(pk);
        if (c.size() > 0)
        {
            r = (Request)c.toArray()[0];
        }
        return r;
    }

    
    
    /**
     * Find all Requests by requestId.
     * 
     * @param requestId requestId to find by.
     * @return Request from DB row matching IDs.
     * @throws FinderException
     */
    public Collection findRequestsByRequestId(int requestId)
        throws FinderException
    {

      Collection requests = new ArrayList();

      String sql = "SELECT * from REQUEST" ;
            sql +=  " WHERE REQUESTID = " + requestId;
            sql +=  " ORDER BY REQUESTID DESC";
       try
       {
            int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              Request request = new Request(srk);
              request.setPropertiesFromQueryResult(key);
              request.pk = new RequestPK(request.getRequestId(), request.getCopyId());
              requests.add(request);
            }
            jExec.closeData(key);

        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Exception caught while getting Request RequestId:" + requestId);
          logger.error(fe.getMessage());
          logger.error(e);

          throw fe;
        }

        return requests;
    }

    /**
     * Find Final Request by requestId.
     * 
     * @param requestId requestId to find by.
     * @return Request from DB row matching IDs.
     * @throws FinderException
     */
    public Request findLastRequestByRequestId(int requestId)
        throws FinderException
    {

      StringBuffer sqlb = new StringBuffer();
      sqlb.append("SELECT * FROM REQUEST WHERE ");
      sqlb.append(" REQUESTID = ");
      sqlb.append(sqlStringFrom(requestId));
      sqlb.append(" AND ");
      sqlb.append(" COPYID = (SELECT MAX(COPYID) FROM REQUEST WHERE REQUESTID = ");
      sqlb.append(sqlStringFrom(requestId));
      sqlb.append(")");
      
      boolean gotRecord = false;
      String sql = new String(sqlb);
      int key = 0;
      try
      {
        key = jExec.execute(sql);

        for (; jExec.next(key);)
        {
          gotRecord = true;
          setPropertiesFromQueryResult(key);
          break;
        }
            jExec.closeData(key);

      }
      catch (Exception e)
      {
        if (gotRecord == false && getSilentMode() == true) throw (FinderException) e;

        FinderException fe = new FinderException(
            "Request Entity - findLastRequestByRequestId() Exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
      finally
      {
        try
        {
          if (key != 0)
          {
            jExec.closeData(key);
          }
        }
        catch (Exception e)
        {
          logger.debug(e.toString());
        }
      }

      if (gotRecord == false)
      {
        String msg = "Request entity: @findLastRequestByRequestId(), requestId = "
            + requestId + ", not found.";
        if (getSilentMode() == false) logger.error(msg);

        throw new FinderException(msg);
      }

      this.pk = new RequestPK(this.requestId, this.copyId);

      return this;
    }
/**
     * Find Final Request by serviceProductId, dealId and copyId.  Exclude the request
     * that is represented by the given filterRequestPK.
     *
     * @param serviceProductId serviceProductId to find by.
     * @param dealPK Primary key for the Deal table.
     * @param filterRequestPK Nullable value.  Exclude the request represented by this object from the search.
     * @return Request from DB row matching IDs.
     * @throws FinderException
     */
    public Request findLastRequestByServiceProductId(int serviceProductId,
        DealPK dealPK, RequestPK filterRequestPK)
        throws FinderException
    {

        StringBuffer sqlb = new StringBuffer();
        sqlb.append("SELECT * FROM REQUEST WHERE ");
        sqlb.append(" SERVICEPRODUCTID = ");
        sqlb.append(sqlStringFrom(serviceProductId));
//remove by nbc      
//        sqlb.append(" AND ");
//        sqlb.append(" COPYID = ");
//        sqlb.append(sqlStringFrom(dealPK.getCopyId()));
        sqlb.append(" AND DEALID = ");
        sqlb.append(sqlStringFrom(dealPK.getId()));
        if (filterRequestPK != null)
        {
            sqlb.append(" AND NOT REQUESTID = ");
            sqlb.append(filterRequestPK.getId());
        }
        sqlb.append(" ORDER BY REQUESTID DESC");

        boolean gotRecord = false;
        String sql = new String(sqlb);
        int key = 0;
        try
        {
            key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break;
            }

        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
            {
                throw (FinderException)e;
            }

            FinderException fe = new FinderException(
                "Request Entity - findLastRequestByServiceProductId() Exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        finally
        {
            try
            {
                if (key != 0)
                {
                    jExec.closeData(key);
                }
            }
            catch (Exception e)
            {
                logger.debug(e.toString());
            }
        }

        if (gotRecord == false)
        {
            String msg =
                "Request entity: @findLastRequestByServiceProductId(), requestId = "
                + requestId + ", not found.";
            if (getSilentMode() == false)
            {
                logger.error(msg);

            }
            throw new FinderException(msg);
        }

        this.pk = new RequestPK(this.requestId, this.copyId);

        return this;
    }
    /**
     * Find Request by Deal and Copy IDs.
     * 
     * @param dealId Deal ID to find by.
     * @param copyId Copy ID to find by.
     * @return Request from DB row matching IDs.
     * @throws RemoteException
     * @throws FinderException
     */
    public Request findByDealIdAndCopyId(int dealId, int copyId)
        throws RemoteException, FinderException {


        StringBuffer sqlb = new StringBuffer();
        
        sqlb.append("SELECT * FROM REQUEST WHERE DEALID = ");
        sqlb.append(sqlStringFrom(dealId));
        sqlb.append(" AND COPYID = ");
        sqlb.append(sqlStringFrom(copyId));
        sqlb.append(" order by requestdate desc");
        
        boolean gotRecord = false;
        String sql = new String(sqlb);
        int key = 0;
        try {
            key = jExec.execute(sql);
            
            for(; jExec.next(key); ) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break;              
            }
            if (gotRecord == false) {
                String msg = "Request entity: @findByDealIdAndCopyId(), dealId = " +
                                         dealId + ", copyId = " + copyId + ", not found.";
                if (getSilentMode() == false)
                    logger.error(msg);
                
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException) e;
            
            FinderException fe = new FinderException("Request Entity - findByDealIdAndCopyId() Exception");
            
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch (Exception e) {
                logger.debug(e.toString());
            }
        }
        
        this.pk = new RequestPK(this.requestId, this.copyId);
        
        return this;
    }
    
    /**
     * find the latest request based on serviceTypeId and dealId and CopyId
     * @return Collection
     * @throws Exception
     * @since 4.2 DSU
     */
    public Request findLastRequestByDealIdandCopyIdandServiceSubTypeId(int dealId, int copyId, int serviceSubTypeId)
        throws RemoteException, FinderException {
        
        StringBuffer sqlb = new StringBuffer();
        
        sqlb.append("SELECT * FROM REQUEST R, DEAL D WHERE R.DEALID = ");
        sqlb.append(sqlStringFrom(dealId));
        sqlb.append(" AND D.SCENARIORECOMMENDED='Y'");
        sqlb.append(" AND R.DEALID=D.DEALID AND R.COPYID = D.COPYID");
        sqlb.append(" AND SERVICEPRODUCTID in ( ");
        sqlb.append("select serviceProductId from ServiceProduct where serviceSubTypeId = ")
            .append(sqlStringFrom(serviceSubTypeId)).append(")");
        sqlb.append(" order by requestdate desc");

        boolean gotRecord = false;
        String sql = new String(sqlb);
        int key = 0;
        try {
            key = jExec.execute(sql);
            
            for(; jExec.next(key); ) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break;              
            }
            if (gotRecord == false) {
                String msg = "Request entity: @findByDealIdandCopyIdandProductTypeId(), dealId = " +
                                         dealId + ", copyId = " + copyId + ", not found.";
                if (getSilentMode() == false)
                    logger.error(msg);
                
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException) e;
            
            FinderException fe = new FinderException("Request Entity - findByDealIdandCopyIdandProductTypeId() Exception");
            
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch (Exception e) {
                logger.debug(e.toString());
            }
        }
        
        this.pk = new RequestPK(this.requestId, this.copyId);

        return this;
    }
    
    public int findContactId() throws Exception {

        return findContactId(requestId, copyId);
    }

    
    /**
     * Find the ContactId associated with the given request.
     * @param requestId
     * @param copyId
     * @return The Contact ID.
     * @throws FinderException If the Contact ID cannot be found.
     */
    public int findContactId(int requestId, int copyId)
        throws FinderException {

        StringBuffer sqlb = new StringBuffer("SELECT C.serviceRequestContactId FROM ");
        sqlb.append("REQUEST R, BORROWERREQUESTASSOC A, SERVICEREQUESTCONTACT C ");
        sqlb.append("WHERE R.requestId = " + requestId + " AND ");
        sqlb.append("R.copyId = " + copyId + " AND ");
        sqlb.append("R.requestId = A.requestId AND ");
        sqlb.append("R.copyId = A.copyId AND ");
        sqlb.append("A.requestId = C.requestId AND ");
        sqlb.append("A.copyId = C.copyId");
        
        String sql = new String(sqlb);
        int key = 0;
        int result = -1;
        boolean gotRecord = false;
        
        try {
            key = jExec.execute(sql);
            
            for(; jExec.next(key);) {
                gotRecord = true;
                result = jExec.getInt(key, "serviceRequestContactId");
                break;
            }
            
            if (gotRecord == false) {
                String msg = "Request entity: @findContactId(), requestId = " +
                        requestId + ", copyId = " + copyId;
                if(getSilentMode() == false)
                    logger.error(msg);
                
                throw new FinderException(msg);
            }
        }
        catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true) {
                throw (FinderException) e;              
            }
            FinderException fe = new FinderException("Request Entity - findContactId() Exception");
            
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch (Exception e) {
                logger.debug(e.toString());
            }
        }
        
        return result;
    }
    
    
    public int findDatxTimeout() throws Exception {

        return findDatxTimeout(serviceProductId, channelId); 
    }
    
    /**
     * Find the DatX Timeout from the TimeoutDefinitionAssoc table.
     * @param serviceProductId
     * @param channelId
     * @return The DatX Timeout.
     * @throws FinderException If the timeout cannot be found.
     */
    public int findDatxTimeout(int serviceProductId, int channelId)
        throws FinderException{

        StringBuffer sqlb = new StringBuffer("SELECT T.datxTimeout FROM ");
        sqlb.append("REQUEST R, SERVICEPRODUCTREQUESTASSOC A, SERVICEPRODUCT S, ");
        sqlb.append("TIMEOUTDEFINITIONASSOC T WHERE ");
        sqlb.append("R.serviceProductId = " + serviceProductId + " AND ");
        sqlb.append("R.channelId = " + channelId + " AND ");
        sqlb.append("R.serviceProductId = A.serviceProductId AND ");
        sqlb.append("R.channelId = A.channelId AND ");
        sqlb.append("A.serviceProductId = S.serviceProductId AND ");
        sqlb.append("S.serviceProductId = T.serviceProductId AND ");
        sqlb.append("T.channelId = " + channelId);
        
        String sql = new String(sqlb);
        int key = 0;
        int result = -1;
        boolean gotRecord = false;
        
        try {
            key = jExec.execute(sql);
            
            for(; jExec.next(key); ) {
                gotRecord = true;
                result = jExec.getInt(key, "datxTimeout");
                break;
            }
            
            if (gotRecord == false) {
                String msg = "Request entity: @findDatxTimeout(), serviceProductId = " +
                        serviceProductId + ", channelId = " + channelId;
                if(getSilentMode() == false)
                    logger.error(msg);
                
                throw new FinderException(msg);
            }
        }
        catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true) {
                throw (FinderException) e;              
            }
            FinderException fe = new FinderException("Request Entity - findDatxTimeOut() Exception");
            
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch (Exception e) {
                logger.debug(e.toString());
            }
        }
        
        return result;
    }
    
    
    public int findDocumentTypeId() throws Exception {

        return findDocumentTypeId(payloadTypeId);
    }
    
    /**
     * Find this request's document type by querying the payloadType table.
     * This assumes the primary key of the entity has already been set.
     * @param payloadTypeId
     * @return This request's document's type ID.
     */
  // TODO Catherine: make SQL conditional: ChannelId, PayloadTypeId, serviceTransactionTypeId might be NULL in Request table!!
    public int findDocumentTypeId(int payloadTypeId) throws FinderException {

        StringBuffer sqlb = new StringBuffer("SELECT P.documentTypeId FROM ");
        sqlb.append("REQUEST R, SERVICEPRODUCTREQUESTASSOC A, PAYLOADTYPE P ");
        sqlb.append("WHERE R.CHANNELID = A.CHANNELID ");
        sqlb.append(" AND R.PAYLOADTYPEID = A.PAYLOADTYPEID");
        sqlb.append(" AND R.SERVICETRANSACTIONTYPEID = A.SERVICETRANSACTIONTYPEID");
        sqlb.append(" AND A.PAYLOADTYPEID = P.PAYLOADTYPEID");
        sqlb.append(" AND R.PAYLOADTYPEID = " + payloadTypeId);
        sqlb.append(" AND R.REQUESTID = " + pk.getId());
        
        String sql = new String(sqlb);
        int key = 0;
        int result = -1;
        boolean gotRecord = false;
        
        try {
            key = jExec.execute(sql);
            
            
            for(; jExec.next(key); ) {
                gotRecord = true;
                result = jExec.getInt(key, "documentTypeId");
                break;              
            }
            if (gotRecord == false) {
                String msg = "Request entity: @findDocumentTypeId(), payloadTypeId = " +
                payloadTypeId;
                if (getSilentMode() == false)
                    logger.error(msg);
                
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException) e;
            
            FinderException fe = new FinderException("Request Entity - findDocumentTypeId() Exception");
            
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            }
            catch (Exception e) {
                logger.debug(e.toString());
            }
        }
        
        return result;
    }
    
  // ------------ Catherine: begin ------------ 
  
    public String getRequestTypeShortName(int requestId) throws FinderException {
        // before fixing for #4863, 
        // returns the last request
        return getRequestTypeShortName(requestId, 0 ); 
    }

    public String getRequestTypeShortName(int requestId, int copyId) throws FinderException
    {
        StringBuffer sqlb = new StringBuffer(
                      " SELECT A.REQUESTTYPESHORTNAME ");
          sqlb.append(" FROM SERVICEPRODUCTREQUESTASSOC A,");
          sqlb.append("      REQUEST R");
          sqlb.append(" WHERE A.SERVICEPRODUCTID = R.SERVICEPRODUCTID"); //-- nullable in R; // Catherine: assuming that it's not null!
        if (this.payloadTypeId != 0) {    
          sqlb.append(" AND A.PAYLOADTYPEID = R.PAYLOADTYPEID");         // -- nullable in R
        }
        if (this.channelId != 0){
          sqlb.append(" AND A.CHANNELID = R.CHANNELID");                 // -- nullable in R
        }  
        if (this.serviceTransactionTypeId != 0){
          sqlb.append(" AND  A.SERVICETRANSACTIONTYPEID = R.SERVICETRANSACTIONTYPEID"); // -- nullable in R
        }  
            
          sqlb.append(" AND R.REQUESTID = " + requestId);
    
        // added for #4863 start - Midori
        if (copyId == 0)
            sqlb.append(" order by  requestDate desc");
        else
            sqlb.append(" AND R.COPYID = " + copyId);
        // added for #4863 end
        
        String sql = new String(sqlb);
        int key = 0;
        String result = null;
        boolean gotRecord = false;
        
        try {
          key = jExec.execute(sql);
          
          
          for(; jExec.next(key); ) {
            gotRecord = true;
            result = jExec.getString(key, "requestTypeShortName");
            break;        
          }
          if (gotRecord == false) {
            String msg = "Failed to find RequestId = " +  requestId;
            if (getSilentMode() == false)
              logger.error(msg);
            
            throw new FinderException(msg);
          }
        } catch (Exception e) {
          if (gotRecord == false && getSilentMode() == true){
            throw (FinderException) e;
          }
          
          FinderException fe = new FinderException("Request Entity - getRequestTypeShortName() Exception");
          
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);
          
          throw fe;
        }
        finally {
          try {
            if (key != 0) {
              jExec.closeData(key);
            }
          }
          catch (Exception e) {
            logger.debug(e.toString());
          }
        }
        
        return result;
    }
  
    /**
     * 
     * @return requestTypeId from SERVICEPRODUCTREQUESTASSOC table
     * @throws FinderException 
     */
    public int getRequestTypeId() throws FinderException{
      StringBuffer sqlb = new StringBuffer(
                    " SELECT A.REQUESTTYPEID ");
        sqlb.append(" FROM SERVICEPRODUCTREQUESTASSOC A,");
        sqlb.append("      REQUEST R");
        sqlb.append(" WHERE A.SERVICEPRODUCTID = R.SERVICEPRODUCTID"); // --  nullable in R; Catherine: assuming that it's  not null!
      if (this.payloadTypeId != 0) {
        sqlb.append(" AND A.PAYLOADTYPEID = R.PAYLOADTYPEID");       // -- nullable in R
      }
      if (this.channelId != 0) {
        sqlb.append(" AND A.CHANNELID = R.CHANNELID");               // -- nullable in R
      }
      if (this.serviceTransactionTypeId != 0) {
        sqlb.append(" AND  A.SERVICETRANSACTIONTYPEID = R.SERVICETRANSACTIONTYPEID"); // -- nullable in R
      }
        sqlb.append(" AND R.REQUESTID = " + pk.getId());
  
      String sql = new String(sqlb);
      int key = 0;
      int result = 0;
      boolean gotRecord = false;
  
      try {
        key = jExec.execute(sql);
  
        for (; jExec.next(key);) {
          gotRecord = true;
          result = jExec.getInt(key, "requestTypeId");
          break;
        }
        if (gotRecord == false) {
          String msg = "Cannot find RequestTypeId: @getRequestTypeId(), requestId = " + pk.getId();
          if (getSilentMode() == false)
            logger.error(msg);
  
          throw new FinderException(msg);
        }
      } catch (Exception e) {
        if (gotRecord == false && getSilentMode() == true) {
          throw (FinderException) e;
        }
  
        FinderException fe = new FinderException("Request Entity - getRequestTypeShortName() Exception");
  
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);
  
        throw fe;
      } finally {
        try {
          if (key != 0) {
            jExec.closeData(key);
          }
        } catch (Exception e) {
          logger.debug(e.toString());
        }
      }
  
      return result;
    }
  // ------------ Catherine: end ------------ 

    private void setPropertiesFromQueryResult(int key) throws Exception {
        this.setRequestId(jExec.getInt(key, "REQUESTID"));
        this.setRequestDate(jExec.getDate(key, "REQUESTDATE"));
        this.setDealId(jExec.getInt(key, "DEALID"));
        this.setCopyId(jExec.getInt(key, "COPYID"));
        this.setServiceProductId(jExec.getInteger(key, "SERVICEPRODUCTID"));
        this.setChannelId(jExec.getInteger(key, "CHANNELID"));
        this.setPayloadTypeId(jExec.getInteger(key, "PAYLOADTYPEID"));
        this.setServiceTransactionTypeId(jExec.getInteger(key, "SERVICETRANSACTIONTYPEID"));
        this.setRequestStatusId(jExec.getInt(key, "REQUESTSTATUSID"));
        this.setStatusMessage(jExec.getString(key, "STATUSMESSAGE"));
        this.setStatusDate(jExec.getDate(key, "STATUSDATE"));
        this.setUserProfileId(jExec.getInt(key, "USERPROFILEID"));
        this.setChannelTransactionKey(jExec.getString(key, "CHANNELTRANSACTIONKEY"));
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }
  
    /**
     * Tests whether the Request is equal to the given (request) object.
     * @param obj The Object to test for equality.
     */
    public boolean equals(Object obj) {
      if (obj instanceof Request) {
        Request otherObj = (Request) obj;
        if  (this.requestId == otherObj.getRequestId()) {
            return true;
        }
      }
      return false;   
    }
    
    /**
     *  Gets the value of instance fields as String.
     *  If a field does not exist (or the type is not serviced)** null is returned.
     *  If the field exists (and the type is serviced) but the field is null an empty
     *  String is returned.
     *  @returns value of bound field as a String;
     *  @param  fieldName as String
     *
     *  Other entity types are not yet serviced   ie. You can't get the Province object
     *  for province.
     */
    public String getStringValue(String fieldName)
    {
       return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * Performs database update.
     * 
     * @throws Exception
     */
    protected int performUpdate() throws Exception {
      Class clazz = this.getClass();
  
      int result = doPerformUpdate(this, clazz);
  
      return result;
    }

    /**
     * @return Returns the name of the DB Table this class represents.
     */
    public String getEntityTableName() {
      return "Request";
    }

    /**
     * @return Returns the Request's primary Key.
     */
    public IEntityBeanPK getPk() {
      return pk;
    }

    /**
     * @return Returns the channel ID.
     */
    public int getChannelId() {
        return channelId;
    }

    /**
     * @param channelId The channel ID to set.
     */
    public void setChannelId(int channelId) {

        setChannelId(new Integer(channelId));
    }
    public void setChannelId(Integer channelId) {

        testChange("channelId", channelId);
        if (channelId != null) {
            this.channelId = channelId.intValue();
        }
    }

    /**
     * @return Returns the copy ID.
     */
    public int getCopyId() {
        return copyId;
    }

    /**
     * @param copyId The copy ID to set.
     */
    public void setCopyId(int copyId) {

        testChange("copyId", copyId);  
        this.copyId = copyId;
    }

    /**
     * @return Returns the deal ID.
     */
    public int getDealId() {
        return dealId;
    }

    /**
     * @param dealId The deal ID to set.
     */
    public void setDealId(int dealId) {

        testChange("dealId", dealId);
        this.dealId = dealId;
    }

    /**
     * @return Returns the payload type ID.
     */
    public int getPayloadTypeId() {
        return payloadTypeId;
    }

    /**
     * @param payloadTypeId The payload type ID to set.
     */
    public void setPayloadTypeId(int payloadTypeId) {

        setPayloadTypeId(new Integer(payloadTypeId));
    }
    public void setPayloadTypeId(Integer payloadTypeId) {

        testChange("payloadTypeId", payloadTypeId);
        if (payloadTypeId != null) {
            this.payloadTypeId = payloadTypeId.intValue();
        }
    }

    /**
     * @return Returns the request date.
     */
    public Date getRequestDate() {

        return requestDate;
    }

    /**
     * @param requestDate The request date to set.
     */
    public void setRequestDate(Date requestDate) {

        testChange("requestDate", requestDate);  
        this.requestDate = requestDate;
    }

    /**
     * @return Returns the request ID.
     */
    public int getRequestId() {
        return requestId;
    }

    /**
     * @param requestId The request ID to set.
     */
    public void setRequestId(int requestId) {

        testChange("requestId", requestId);
        this.requestId = requestId;
    }

    /**
     * @return Returns the request status ID.
     */
    public int getRequestStatusId() {

        return requestStatusId;
    }

    /**
     * @param requestStatusId The request status ID to set.
     */
    public void setRequestStatusId(int requestStatusId) {

        testChange("requestStatusId", requestStatusId);  
        this.requestStatusId = requestStatusId;
    }

    /**
     * @return Returns the service product ID.
     */
    public int getServiceProductId() {
        return serviceProductId;
    }

    /**
     * @param serviceProductId The service Product ID to set.
     */
    public void setServiceProductId(int serviceProductId) {

        setServiceProductId(new Integer(serviceProductId));
    }
    public void setServiceProductId(Integer serviceProductId) {

        testChange("serviceProductId", serviceProductId);
        if (serviceProductId != null) {
            this.serviceProductId = serviceProductId.intValue();
        }
    }

    /**
     * return the service product entity.
     */
    public ServiceProduct getServiceProduct()
        throws RemoteException, FinderException {

        return new ServiceProduct(srk, getServiceProductId());
    }

    /**
     * @return Returns the status date.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * @param statusDate The status date to set.
     */
    public void setStatusDate(Date statusDate) {

        testChange("statusDate", statusDate);  
        this.statusDate = statusDate;
    }

    /**
     * @return Returns the status message.
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The status message to set.
     */
    public void setStatusMessage(String statusMessage) {
        testChange("statusMessage", statusMessage);
        this.statusMessage = statusMessage;
    }

    /**
     * @return Returns the transaction type ID.
     */
    public int getServiceTransactionTypeId() {
        return serviceTransactionTypeId;
    }

    /**
     * @param serviceTransactionTypeId The transaction type ID to set.
     */
    public void setServiceTransactionTypeId(int serviceTransactionTypeId) {

        setServiceTransactionTypeId(new Integer(serviceTransactionTypeId));
    }
    public void setServiceTransactionTypeId(Integer serviceTransactionTypeId) {

        testChange("SERVICETRANSACTIONTYPEID", serviceTransactionTypeId);
        if (serviceTransactionTypeId != null) {
            this.serviceTransactionTypeId =
                serviceTransactionTypeId.intValue();
        }
    }

    /**
     * @return Returns the user profile ID.
     */
    public int getUserProfileId() {
        return userProfileId;
    }

    /**
     * @param userProfileId The user profile ID to set.
     */
    public void setUserProfileId(int userProfileId) {

        testChange("userProfileId", userProfileId);  
        this.userProfileId = userProfileId;
    }
    
    public int getRequestTypeInt() {
        return requestTypeId;
    }

    /**
     * @param userProfileId The user profile ID to set.
     */
    public void setRequestTypeId(int requestTypeId) {

        testChange("requestTypeId", requestTypeId);
        this.requestTypeId = requestTypeId;
    }

    /**
     *   getS the AdjudicationRequests associated with this entity
     *
     *   @return a Collection of AdjudicationRequests;
     */
    public Collection getAdjudicationRequests() throws Exception
    {
      return new AdjudicationRequest(srk).findByRequest(this.pk);
    }

    /**
     *   getS the AdjudicationApplicantRequests associated with this entity
     *
     *   @return a Collection of AdjudicationApplicantRequests;
     */
    public Collection getAdjudicationApplicantRequests() throws Exception
    {
      return new AdjudicationApplicantRequest(srk).findByRequest(this.pk);
    }

    /**
     *   getS the responses associated with this entity
     *
     *   @return a Collection of Responses;
     */
    public Collection getResponses() throws Exception
    {
       return new Response(srk).findByRequestId(getRequestId());
    }
 
    /**
     *   getS the AdjudicationApplicants associated with this entity
     *
     *   @return a Collection of AdjudicationApplicant;
     */
    public Collection getAdjudicationApplicants() throws Exception
    {
      return new AdjudicationApplicant(srk).findByRequest(this.pk);
    }
    /**
     * @return Returns the Channel TransactionKey.
     */
    public String getChannelTransactionKey() {
        return channelTransactionKey;
    }

    /**
     * @param statusMessage The status message to set.
     */
    public void setChannelTransactionKey(String tKey) {
        testChange("channelTransactionKey", tKey);
        this.channelTransactionKey = tKey;
    }
 
    private void setPk(RequestPK pk) {
      this.pk = pk;
    }
 
    /**
     * copy child assocs.
     */
    protected void copyChildAssocs(int newCopyId) throws Exception {

        String findSql = "SELECT BorrowerId FROM BorrowerRequestAssoc " +
            "WHERE requestid = "
            + getRequestId() + " AND copyid = " + getCopyId();
        logger.info("Request -> copyChildAssocs : Select stmt - " + findSql);
        try {
            int key = jExec.execute(findSql);
            int borrowerId = -1;
            
            String sql = "INSERT INTO BorrowerRequestAssoc "
                + "(requestId, copyId, borrowerId, institutionProfileId) VALUES ("
                + getRequestId() + ", " + newCopyId + ", ?, " 
                + srk.getExpressState().getDealInstitutionId() + ")";
            logger.info("Request -> copyChildAssocs : Insert statement - " + sql);
            PreparedStatement pstmt = this.jExec.getCon().prepareStatement(sql);
           
            for (; jExec.next(key);) {
                borrowerId = jExec.getInt(key, "BorrowerId");
                pstmt.setInt(1,borrowerId);
                pstmt.executeUpdate();
                //break;
            }
            pstmt.close();
            jExec.closeData(key);

            /*
            if (borrowerId > 0) {
                String sql = "INSERT INTO BorrowerRequestAssoc "
                    + "(requestId, copyId, borrowerId) VALUES ("
                    + getRequestId() + ", " + newCopyId + ", "
                    + borrowerId + ")";
                jExec.executeUpdate(sql);
            }
             */
            
        } catch (Exception e) {
            logger.error("Error on remove sons: ");
            throw new Exception("Error on remove sons: ", e);
        }
    }

    /**
     * For TransactionCopy, parentKey has dealId value
     * For ResurrectDeal, parentKey has new RequestId value
     */
    protected void copyChildAssocs(String parentKey, int newCopyId) throws Exception {

        int intRequestId = getRequestId();
        String findSql = "SELECT BorrowerId FROM BorrowerRequestAssoc " +
            "WHERE requestid = "
            + intRequestId + " AND copyid = " + getCopyId();
        logger.info("Request -> copyChildAssocs : Select stmt - " + findSql);
        try {
            
            //PVCS Ticket # 1809, 1810 - Begin//
            //Added the condition to differentiate between Transaction Copying and
            // & Resurrect deal
             if(Integer.parseInt(parentKey) != getRequestId() && 
                     Integer.parseInt(parentKey) != getDealId())
             {
                 Request req = new Request(srk);
                 req.findByPrimaryKey(new RequestPK(Integer.parseInt(parentKey),newCopyId));
                 findSql = "Select * from Borrower " +
                            "WHERE dealid = " + req.getDealId() + " and copyId = " + req.getCopyId() +
                            " order by PrimaryBorrowerFlag desc , BORROWERID asc";
                 intRequestId = req.getRequestId();
             }
            //PVCS Ticket # 1809, 1810 - End//
             
            int key = jExec.execute(findSql);
            int borrowerId = -1;
           
            String sql = "INSERT INTO BorrowerRequestAssoc "
                + "(requestId, copyId, borrowerId, institutionProfileId) VALUES ("
                + intRequestId + ", " + newCopyId + ", ?, "
                + srk.getExpressState().getDealInstitutionId() + ")";
            logger.info("Request -> copyChildAssocs : Insert statement - " + sql);
            PreparedStatement pstmt = this.jExec.getCon().prepareStatement(sql);
           
            for (; jExec.next(key);) {
                borrowerId = jExec.getInt(key, "BorrowerId");
                pstmt.setInt(1,borrowerId);
                pstmt.executeUpdate();
                //break;
            }
            pstmt.close();
            jExec.closeData(key);
            
        } catch (Exception e) {
            logger.error("Error on remove sons: ");
            throw new Exception("Error on remove sons: ", e);
        }
    }

    /**
     * remove child assocs.
     */
    public void removeChildAssocs() throws Exception {

        String sql = "DELETE FROM BorrowerRequestAssoc WHERE requestid = "
            + getRequestId() + " AND copyid = " + getCopyId();

        try {
            jExec.executeUpdate(sql);
        } catch (Exception e) {
            logger.error("Error on remove sons: ");
            throw new Exception("Error on remove sons: ", e);
        }
    }

    /**
     * create child assoc.
     */
    public void createChildAssocs(int borrowerId) throws Exception {

        String assocSql = "INSERT INTO BorrowerRequestAssoc " +
            "(requestId, copyId, borrowerId, institutionProfileId) VALUES ("   +
            getRequestId() + ", " + copyId +
            ", " + borrowerId + ", "
            + srk.getExpressState().getDealInstitutionId() + ")";
        int key = jExec.executeUpdate(assocSql);
        jExec.closeData(key);
    }

    /**
     * Clones everything except for the PK.  Inserts the new record into the DB.
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
      Request clonedRequest = null;
 
      try {
        // first create teh request to generate the new PK
        clonedRequest = new Request(srk);
        clonedRequest.create(new DealPK(this.getDealId(), this.getCopyId()), 
                          this.getRequestStatusId(), this.getStatusDate(), this.getUserProfileId());
 
        // now save the newly generated request PK before cloning
        RequestPK newRequestPk = (RequestPK)clonedRequest.getPk();
 
        // call Object's clone() which does a shallow copy
        clonedRequest = (Request)super.clone();
 
        // restore the pk
        clonedRequest.setPk(newRequestPk);
        clonedRequest.setRequestId(newRequestPk.getId());
        
        clonedRequest.doPerformUpdateForce();
      }
      catch (Exception ex) {
        ex.printStackTrace();
        logger.error("Error in Request.clone().  Trying to clone requestid = " + this.requestId + " " + ex.getMessage());
      }
 
      return clonedRequest;
    }

    protected void removeSons() throws Exception {
 
        // -------- start of all AdjudicationRequest -------------
        Collection areqs = this.getAdjudicationRequests();
        Iterator itAR = areqs.iterator () ;
        while ( itAR.hasNext () )
        {
            AdjudicationRequest areq = (AdjudicationRequest) itAR.next () ;
            if ( itAR.hasNext () )
            {
                areq.ejbRemove ( false );
            }
            else{
                areq.ejbRemove ( true );
           }
        }
        // -------- end of all AdjudicationRequest -------------
 
        // Catherine: for AVM ------- start ---------
        Collection col = this.getServiceRequests ();
        Iterator itReqs = col.iterator () ;
        while ( itReqs.hasNext () )
        {
          ServiceRequest srvReq = ( ServiceRequest ) itReqs.next () ;
            srvReq.ejbRemove ( false );
        }
        // Catherine: for AVM ------- end ---------
   
        for (Iterator i = getServiceRequestContacts().iterator();
             i.hasNext(); ) {
   
            ServiceRequestContact contact = (ServiceRequestContact) i.next();
            contact.ejbRemove(false);
        }
   
        //-------- start of all AdjudicationApplicantRequest-------------
        Collection adjAppReqs = this.getAdjudicationApplicantRequests();
        Iterator itAdjAppReqs = adjAppReqs.iterator () ;
        while ( itAdjAppReqs.hasNext () )
        {
            AdjudicationApplicantRequest adjAppReq = (AdjudicationApplicantRequest) itAdjAppReqs.next () ;
            if ( itAdjAppReqs.hasNext () )
            {
                adjAppReq.ejbRemove ( false );
            }
            else{
                adjAppReq.ejbRemove ( true );
           }
        }
        //-------- end of all AdjudicationApplicantRequest-------------
        
   
        removeChildAssocs();
   
        // -------- start of all AdjudicationApplicant -------------
        Collection aapps = this.getAdjudicationApplicants();
        Iterator itAApp = aapps.iterator () ;
        while ( itAApp.hasNext () )
        {
            AdjudicationApplicant aapp = (AdjudicationApplicant) itAApp.next () ;
            if ( itAApp.hasNext () )
            {
                aapp.ejbRemove ( false );
            }
            else{
                aapp.ejbRemove ( true );
           }
        }
        // -------- end of all AdjudicationApplicant -------------
    }

    /**
     * return all children as List.
     * Added AdjudicationApplicantRequests to the Children list
     */
    public List getChildren() throws Exception {
      List children = new ArrayList();
 
      children.addAll(this.getServiceRequests());
      children.addAll(this.getServiceRequestContacts());
      children.addAll(this.getAdjudicationApplicantRequests());
 
      return children;
    }

    /**
     * return the service request.
     */
    public ServiceRequest getServiceRequest() {

        try {
            return new ServiceRequest(srk, getRequestId(), getCopyId());
        } catch (Exception re) {
            return null;
        }
    }
    
    /**
     * get all servicerequest related to this requst. should be only one.
     */
    protected Collection getServiceRequests() throws Exception {
      return new ServiceRequest(srk).findByRequest(this.pk);
    }

    /**
     * return all contact related to this request. should be only one!
     */
    protected Collection getServiceRequestContacts() throws Exception {

        return new ServiceRequestContact(srk).findByRequest(this.pk);
    }

    /**
     * returns the service request contact.
     */
    public ServiceRequestContact getServiceRequestContact() {

        try {
            Collection contacts = getServiceRequestContacts();
            if (contacts != null && !(contacts.isEmpty())) {
                return (ServiceRequestContact) contacts.iterator().next();
            }
        } catch (Exception e) {
            // do nothing for now.
        }

        return null;
    }

    public boolean isRequestForAnotherScenario(int dealId, int copyId) throws Exception {

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT request.* ").
            append(" FROM REQUEST request, SERVICEPRODUCT product, ").
            append("SERVICESUBTYPE subtype, SERVICEREQUEST servicerequest").
            append(" WHERE request.institutionprofileid = product.institutionprofileid").
            append(" AND request.SERVICEPRODUCTID = product.SERVICEPRODUCTID").
            append(" AND product.SERVICESUBTYPEID = subtype.SERVICESUBTYPEID").
            append(" AND subtype.SERVICELOCKABLE = 'Y'").
            append(" AND request.institutionprofileid = servicerequest.institutionprofileid").
            append(" AND request.REQUESTID = servicerequest.REQUESTID").
            append(" AND request.DEALID = ").append(dealId).
            append(" AND request.copyid <> ").append(copyId).
            append(" AND NOT (request.REQUESTSTATUSID IN (").
            append(ServiceConst.REQUEST_STATUS_BLANK).append(", ").
            append(ServiceConst.REQUEST_STATUS_NOT_SUBMITTED).append(") OR").
            append(" (request.REQUESTSTATUSID IN (").
            append(ServiceConst.REQUEST_STATUS_SYSTEM_ERROR).append(", ").
            append(ServiceConst.REQUEST_STATUS_PROCESSING_ERROR).append(", ").
            append(ServiceConst.REQUEST_STATUS_FAILED).append(") ").
            append("AND servicerequest.SERVICEPROVIDERREFNO IS NULL))");
        logger.info("query sql: " + sql.toString());

        boolean gotRecord = false;
        try {
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key); ) {
                gotRecord = true;
                break;
            }
            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception("Exception caught from isRequestForAnotherScenario(..)");
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }

        logger.debug("Request.isRequestForAnotherScenario(..) called, sql = " + sql);
        logger.debug("Return value = " + gotRecord);

        return gotRecord;
    }
    public int getInstitutionProfileId()
    {
      return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId)
    {
      this.testChange("institutionProfileId", institutionProfileId);
      this.institutionProfileId = institutionProfileId;
    }
}
