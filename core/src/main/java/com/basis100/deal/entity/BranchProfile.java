package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class BranchProfile extends DealEntity {
    protected int branchProfileId;

    protected String branchName;

    protected int regionProfileId;

    protected int branchManagerUserId;

    protected String costCentre;

    protected String servicingCentre;

    protected int contactId;

    protected int profileStatusId;

    protected String BPShortName;

    protected String BPBusinessId;

    protected int timeZoneEntryId;

    protected int partyProfileId;

    protected String GEBranchTransitNumber;

    protected String CMHCBranchTransitNumber;

    protected int branchTypeId;

    // SEAN Outbound Faxing Nov 28, 2005
    protected String faxNotificationEmail;

    protected int faxNotificationPreferenceId;

    // SEAN Outbound Faxing END
    
    //5.0 MI, QC286
    protected String faxCertificate;

    protected BranchProfilePK pk;

    protected String AIGUGBranchTransitNumber; // AAtcha
   protected String PMIBranchTransitNumber;

    protected int institutionProfileId;

    public BranchProfile(SessionResourceKit srk) throws RemoteException,
            FinderException {
        super(srk);
    }

    public BranchProfile(SessionResourceKit srk, int id)
            throws RemoteException, FinderException {
        super(srk);

        pk = new BranchProfilePK(id);

        findByPrimaryKey(pk);
    }

    public BranchProfile findByPrimaryKey(BranchProfilePK pk)
            throws RemoteException, FinderException {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from " + "branchProfile" + " where " + pk.getName()
                + " = " + pk.getId();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key, false);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    private void setPropertiesFromQueryResult(int key, boolean deep)
            throws Exception {
        this.setBranchProfileId(jExec.getInt(key, "branchProfileId"));
        this.setBranchName(jExec.getString(key, "branchName"));
        this.setRegionProfileId(jExec.getInt(key, "regionProfileId"));
        this.setBranchManagerUserId(jExec.getInt(key, "branchManagerUserId"));
        this.setCostCentre(jExec.getString(key, "costCentre"));
        this.setServicingCentre(jExec.getString(key, "servicingCentre"));
        this.setContactId(jExec.getInt(key, "contactId"));
        this.setProfileStatusId(jExec.getInt(key, "profileStatusId"));
        this.setBPShortName(jExec.getString(key, "BPShortName"));
        this.setBPBusinessId(jExec.getString(key, "BPBusinessId"));
        this.setTimeZoneEntryId(jExec.getInt(key, "timeZoneEntryId"));
        this.setPartyProfileId(jExec.getInt(key, "partyProfileId"));
        this.setGEBranchTransitNumber(jExec.getString(key,
                "GEBranchTransitNumber"));
        this.setCMHCBranchTransitNumber(jExec.getString(key,
                "CMHCBranchTransitNumber"));
        this.setBranchTypeId(jExec.getInt(key, "branchTypeId"));
        // SEAN Outbound Faxing Nov 28, 2005
        this.setFaxNotificationEmail(jExec.getString(key,
                "faxNotificationEmail"));
        this.setFaxNotificationPreferenceId(jExec.getInt(key,
                "faxNotificationPreferenceId"));
        // SEAN Outbound Faxing END
        this.setAIGUGBranchTransitNumber(jExec.getString(key,
                "AIGUGBranchTransitNumber")); // AAtcha
        this.setInstitutionProfileId(jExec
                .getInt(key, "institutionProfileId")); // for ML, July 11,
                          // 2007 - MAida
        this.setPMIBranchTransitNumber(jExec.getString(key,"PMIBranchTransitNumber"));
        
        this.setFaxCertificate(jExec.getString(key, "faxCertificate"));
    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {
        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {
        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    public List getSecondaryParents() throws Exception {
        List sps = new ArrayList();

        Object o = getContact();

        if (o != null)
            sps.add(o);

        return sps;
    }

    public String getEntityTableName() {
        return "BranchProfile";
    }

    public int getProfileStatusId() {
        return this.profileStatusId;
    }

    public int getRegionProfileId() {
        return this.regionProfileId;
    }

    public int getBranchManagerUserId() {
        return this.branchManagerUserId;
    }

    public String getCostCentre() {
        return this.costCentre;
    }

    public String getServicingCentre() {
        return this.servicingCentre;
    }

    public int getContactId() {
        return this.contactId;
    }

    /**
     * retrieves the Contact parent record associated with this entity
     * 
     * @return a Contact
     */
    public Contact getContact() throws FinderException, RemoteException {
        return new Contact(srk, this.getContactId(), 1);
    }

    public String getBPShortName() {
        return this.BPShortName;
    }

    public String getBPBusinessId() {
        return this.BPBusinessId;
    }

    public int getTimeZoneEntryId() {
        return this.timeZoneEntryId;
    }

    public int getPartyProfileId() {
        return this.partyProfileId;
    }

    public String getGEBranchTransitNumber() {
        return this.GEBranchTransitNumber;
    }

    public String getCMHCBranchTransitNumber() {
        return this.CMHCBranchTransitNumber;
    }

    public int getBranchTypeId() {
        return this.branchTypeId;
    }

    // SEAN Outbound Faxing Nov 28, 2005
    public String getFaxNotificationEmail() {
        return this.faxNotificationEmail;
    }

    public int getFaxNotificationPreferenceId() {
        return this.faxNotificationPreferenceId;
    }

    // SEAN Outbound Faxing END

    // AAtcha starts
    public String getAIGUGBranchTransitNumber() {
        return this.AIGUGBranchTransitNumber;
    }

    // AAtcha ends
    /**
     * gets the branchProfileId associated with this entity
     * 
     * @return a short
     */
    public int getBranchProfileId() {
        return this.branchProfileId;
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a BranchProfilePK
     */
    public IEntityBeanPK getPk() {
        return (IEntityBeanPK) this.pk;
    }

    /**
     * gets the branchName associated with this entity
     * 
     * @return a String
     */
    public String getBranchName() {
        return this.branchName;
    }

    public BranchProfilePK createPrimaryKey() throws CreateException {
        String sql = "Select BranchProfileseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1)
                throw new Exception();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "BranchProfile Entity create() exception getting BranchProfileId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }

        return new BranchProfilePK(id);
    }

    /**
     * creates a BranchProfile using the BranchProfileId with mininimum fields
     * 
     */

    public BranchProfile create(BranchProfilePK pk) throws RemoteException,
            CreateException {
        // TODO: need to add institutionProfileId - Midori July 18, 2007
        String sql = "Insert into BranchProfile(" + pk.getName() + ", "
                + "INSTITUTIONPROFILEID " + " ) Values ( " + pk.getId() + ", "
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            this.pk = pk;
            this.findByPrimaryKey(pk);
            this.trackEntityCreate(); // Track the create
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "BranchProfile Entity - BranchProfile - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);
        return this;
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {
        doSetField(this, this.getClass(), fieldName, value);
    }

    public void setBranchProfileId(int id) {
        this.testChange("branchProfileId", id);
        this.branchProfileId = id;
    }

    public void setRegionProfileId(int id) {
        this.testChange("regionProfileId", id);
        this.regionProfileId = id;
    }

    public void setBranchName(String name) {
        this.testChange("branchName", name);
        this.branchName = name;
    }

    public void setBranchManagerUserId(int id) {
        this.testChange("branchManagerUserId", id);
        this.branchManagerUserId = id;
    }

    public void setCostCentre(String ctr) {
        this.testChange("costCentre", ctr);
        this.costCentre = ctr;
    }

    public void setServicingCentre(String ctr) {
        this.testChange("servicingCentre", ctr);
        this.servicingCentre = ctr;
    }

    public void setBPShortName(String val) {
        this.testChange("BPShortName", val);
        this.BPShortName = val;
    }

    public void setBPBusinessId(String ctr) {
        this.testChange("BPBusinessId", ctr);
        this.BPBusinessId = ctr;
    }

    public void setContactId(int id) {
        this.testChange("contactId", id);
        this.contactId = id;
    }

    public void setProfileStatusId(int id) {
        this.testChange("profileStatusId", id);
        this.profileStatusId = id;
    }

    public void setTimeZoneEntryId(int id) {
        this.testChange("timeZoneEntryId", id);
        this.timeZoneEntryId = id;
    }

    public void setPartyProfileId(int id) {
        this.testChange("partyProfileId", id);
        this.partyProfileId = id;
    }

    public void setGEBranchTransitNumber(String num) {
        this.testChange("GEBranchTransitNumber", num);
        this.GEBranchTransitNumber = num;
    }

    public void setCMHCBranchTransitNumber(String num) {
        this.testChange("CMHCBranchTransitNumber", num);
        this.CMHCBranchTransitNumber = num;
    }

    public void setBranchTypeId(int id) {
        this.testChange("branchTypeId", id);
        this.branchTypeId = id;
    }

    // SEAN Outbound Faxing Nov 28, 2005
    public void setFaxNotificationEmail(String email) {
        this.testChange("faxNotificationEmail", email);
        this.faxNotificationEmail = email;
    }

    public void setFaxNotificationPreferenceId(int id) {
        this.testChange("faxNotificationPreferenceId", id);
        this.faxNotificationPreferenceId = id;
    }

    // SEAN Outbound Faxing END
    // AAtcha start
    public void setAIGUGBranchTransitNumber(String num) {
        this.testChange("AIGUGBranchTransitNumber", num);
        this.AIGUGBranchTransitNumber = num;
    }

    // AAtcha ends
	public String getPMIBranchTransitNumber(){
		return this.PMIBranchTransitNumber; 
	}
	
	public void setPMIBranchTransitNumber(String PMIBranchTransitNumber)
	{
		this.testChange("PMIBranchTransitNumber",PMIBranchTransitNumber);
		this.PMIBranchTransitNumber = PMIBranchTransitNumber;
	}
	
    /**
     * <p>
     * findByUniqueKey
     * </p>
     * get DealEntity entity found by any unique key with the table. the col
     * doesn't have unique constraint though
     * 
     * @param String
     *            colName: column name that have unique key, such as CMHCID,
     *            GEID,,,
     * @param String
     *            id: unique id for the column
     * @return BranchProfile entity
     */
    public BranchProfile findByUniqueKey(String colName, String id)
            throws RemoteException, FinderException {
        String sql = "Select * from " + "branchProfile" + " where " + colName + "= '"
                + id + "'";
        boolean gotRecord = false;
        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key, false); // 2nd parm is
                                                            // deep/shallow
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "branchProfile" + ": @findByUniqueKey(), colName = "
                        + colName + " key= " + id + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException("branchProfile"
                    + " - findByUniqueKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return this;
    }

    public BranchProfile findByCMHCBranchTransitNumber(String transitNumber)
            throws RemoteException, FinderException {
        return findByUniqueKey("CMHCBranchTransitNumber", transitNumber);
    }

    public BranchProfile findByGEBranchTransitNumber(String transitNumber)
            throws RemoteException, FinderException {
        return findByUniqueKey("GEBranchTransitNumber", transitNumber);
    }

    public BranchProfile findByAIGUGBranchTransitNumber(String transitNumber)
            throws RemoteException, FinderException {
        return findByUniqueKey("AIGUGBranchTransitNumber", transitNumber);
    }

    public String getTableName() {
        return "branchProfile";
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

	public String getFaxCertificate() {
		return faxCertificate;
	}

	public void setFaxCertificate(String faxCertificate) {
		this.testChange("faxCertificate", faxCertificate);
		this.faxCertificate = faxCertificate;
	}

}
