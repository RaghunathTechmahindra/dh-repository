package com.basis100.deal.entity;



import java.util.ArrayList;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.GroupProfilePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class GroupProfile extends DealEntity
{
   protected int      groupProfileId;
   protected String   groupName;
   protected int      branchProfileId;
   protected int      supervisorUserId;
   protected int      contactId;
   protected int      profileStatusId;
   protected int      instProfileId;

   protected GroupProfilePK pk;


   public GroupProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public GroupProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new GroupProfilePK(id);

      findByPrimaryKey(pk);
   }

   public GroupProfile findByPrimaryKey(GroupProfilePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from GroupProfile where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setGroupProfileId(jExec.getInt(key,"GROUPPROFILEID"));
      this.setGroupName(jExec.getString(key,"GROUPNAME"));
      this.setBranchProfileId(jExec.getInt(key,"BRANCHPROFILEID"));
      this.setSupervisorUserId(jExec.getInt(key,"SUPERVISORUSERID"));
      this.setContactId( jExec.getInt(key,"CONTACTID"));
      this.setProfileStatusId(jExec.getInt(key,"PROFILESTATUSID"));
      this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }



  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   * <pre>
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */

  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

  public List getSecondaryParents() throws Exception
  {
    List sps = new ArrayList();

    Object o = getContact();

    if(o != null)
    sps.add(o);

    return sps;
  }


   public String getEntityTableName()
   {
      return "GroupProfile";
   }

		/**
     *   gets the profileStatusId associated with this entity
     */
    public int getProfileStatusId(){return this.profileStatusId ;}

    public int getBranchProfileId(){ return this.branchProfileId;}
    public int getSupervisorUserId(){ return this.supervisorUserId;}

    public int getContactId(){ return this.contactId;}

    /**
    *   retrieves the Contact parent record associated with this entity
    *
    *   @return a Contact
    */
    public Contact getContact()throws FinderException,RemoteException
    {
      return new Contact(srk,this.getContactId(),1);
    }


		/**
     *   gets the groupProfileId associated with this entity
     */
    public int getGroupProfileId(){return this.groupProfileId ;}


    /**
     *   gets the pk associated with this entity
     *
     *   @return a GroupProfilePK
     */
    public IEntityBeanPK getPk(){  return (IEntityBeanPK)this.pk ; }

		/**
     *   gets the groupName associated with this entity
     *
     *   @return a String
     */
    public String getGroupName(){return this.groupName ; }


  public GroupProfilePK createPrimaryKey()throws CreateException
  {
       String sql = "Select GroupProfileseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }

           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("GroupProfile Entity create() exception getting GroupProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new GroupProfilePK(id);
  }



  /**
   * creates a GroupProfile using the GroupProfileId with mininimum fields
   *
   */

  public GroupProfile create()throws RemoteException, CreateException
  {
      GroupProfilePK pk = createPrimaryKey();

      String sql = "Insert into GroupProfile(" + pk.getName() + ", INSTITUTIONPROFILEID" + " ) Values ( " + pk.getId() + "," + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate();
      }
      catch (Exception e)
      {

        CreateException ce = new CreateException("GroupProfile Entity - GroupProfile - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      this.pk = pk;
      srk.setModified(true);
      return this;
    }

  /**
   * BEGIN FIX FOR A problem occured message is shown when creating New User Group[FXP-21051]
   * Mohan V Premkumar
   * 04/01/2008
   */
  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Method to create Group profile with name input
  //--> By Billy 07Aug2003
  public GroupProfile create(int branchId, String gpName, int institutionProfileId)throws RemoteException, CreateException
  {
    GroupProfilePK pk = createPrimaryKey();

    String sql = "Insert into GroupProfile(" +
        pk.getName() + ", " + "groupName" + ", " + "branchProfileId, ProfileStatusId, InstitutionProfileId " + 
        " ) Values ( " +
        pk.getId() + ", " + sqlStringFrom(gpName) + ", " + sqlStringFrom(branchId) + ", 0, " + institutionProfileId + ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("GroupProfile Entity - GroupProfile - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    this.pk = pk;
    srk.setModified(true);
    return this;
  }
  //===========================================================  
  /**
   * END FIX FOR A problem occured message is shown when creating New User Group[FXP-21051]
   */
  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Method to create Group profile with name input
  //--> By Billy 07Aug2003
  public GroupProfile create(int branchId, String gpName)throws RemoteException, CreateException
  {
    GroupProfilePK pk = createPrimaryKey();

    String sql = "Insert into GroupProfile(" +
        pk.getName() + ", " + "groupName" + ", " + "branchProfileId" +
        " ) Values ( " +
        pk.getId() + ", " + sqlStringFrom(gpName) + ", " + sqlStringFrom(branchId) + ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("GroupProfile Entity - GroupProfile - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    this.pk = pk;
    srk.setModified(true);
    return this;
  }
  //===========================================================

   public void setGroupProfileId(int value)
	 {
		 this.testChange("groupProfileId", value);
		 this.groupProfileId = value;
   }

   public void setBranchProfileId(int value)
	 {
		 this.testChange("branchProfileId", value);
		 this.branchProfileId = value;
   }

   public void setGroupName(String value )
	 {
		 this.testChange("groupName", value);
		 this.groupName = value;
   }
   public void setSupervisorUserId(int value )
	 {
		 this.testChange("supervisorUserId", value);
		 this.supervisorUserId = value;
   }

   public void setContactId(int value )
	 {
		 this.testChange("contactId", value);
		 this.contactId = value;
   }
   public void setProfileStatusId(int value )
	 {
		 this.testChange("profileStatusId", value);
		 this.profileStatusId = value;
   }

   /**
    * =================================================
    * --> CRF#37 :: Source/Firm to Group routing for TD
    * =================================================
    * --> Method to check if Group name is duplicated
    * --> By Billy 07Aug2003
    * 
    * @version 1.1 04-Sep-2009 FXP25074: add branchId
    */
  public boolean isDupGroupName(String gpName, int branchId)
  {
    String sql = "Select * from GroupProfile where GroupName = " + sqlStringFrom(gpName) +
            " and branchProfileId = " + branchId;
    
    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);
      for (; jExec.next(key); )
      {
          gotRecord = true;
          break; // can only be one record
      }

      jExec.closeData(key);

    }
    catch (Exception e)
    {
      logger.error("Deal Entity - isDupGroupName() exception : " + e);
    }

    return gotRecord;
  }
  //======================================================================
  
    public int getInstProfileId() {
    	return instProfileId;
    }
    
    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
    }

}
