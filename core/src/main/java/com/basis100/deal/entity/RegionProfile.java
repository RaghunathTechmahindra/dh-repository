package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RegionProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class RegionProfile extends DealEntity
{
   protected int    regionProfileId;
   protected String   regionName;
   protected int      regionManagerUserId;
   protected int      contactId;
   protected int      profileStatusId;
   protected String   RPShortName;
   protected String   RPBusinessId;
   protected int      institutionId;

   protected RegionProfilePK pk;
   
   public RegionProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public RegionProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new RegionProfilePK(id);

      findByPrimaryKey(pk);
   }

    public RegionProfile findByPrimaryKey(RegionProfilePK pk) throws RemoteException, FinderException
   {
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from RegionProfile where " + pk.getName()+ " = " + pk.getId();
        
      boolean gotRecord = false;
        
      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setRegionProfileId(jExec.getInt(key,"REGIONPROFILEID"));
      this.setRegionName(jExec.getString(key,"REGIONNAME"));
      this.setRegionManagerUserId(jExec.getInt(key,"REGIONMANAGERUSERID"));
      this.setContactId(jExec.getInt(key,"CONTACTID"));
      this.setProfileStatusId(jExec.getInt(key,"PROFILESTATUSID"));
      this.setRPShortName(jExec.getString(key,"RPSHORTNAME"));
      this.setRPBusinessId(jExec.getString(key,"RPBUSINESSID"));
      this.setInstitutionId(jExec.getInt(key,"INSTITUTIONPROFILEID"));

   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
 
 protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "RegionProfile"; 
   }

		/**
     *   gets the profileStatusId associated with this entity
     *
     */
    public int getProfileStatusId(){return this.profileStatusId ;}

    public int getRegionProfileId(){ return this.regionProfileId;}
    public int getRegionManagerUserId(){ return this.regionManagerUserId;}
    public int getContactId(){ return this.contactId;}
    public String getRPShortName(){ return this.RPShortName;}
    public String getRPBusinessId(){ return this.RPBusinessId;}


    /**
     *   gets the pk associated with this entity
     *
     *   @return a RegionProfilePK
     */
    public IEntityBeanPK getPk(){  return (IEntityBeanPK)this.pk ; }

		/**
     *   gets the regionName associated with this entity
     *
     *   @return a String
     */
    public String getRegionName(){return this.regionName ; }


    public RegionProfilePK createPrimaryKey()throws CreateException
   {
       String sql = "Select RegionProfileseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }

           jExec.closeData(key);
           
           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("RegionProfile Entity create() exception getting RegionProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new RegionProfilePK(id);
  }



  /**
   * creates a RegionProfile using the RegionProfileId with mininimum fields
   *
   */

  public RegionProfile create(RegionProfilePK pk)throws RemoteException, CreateException
  {
    
        String sql = "Insert into RegionProfile(" + pk.getName() + " INSTITUTIONPROFILEID) Values ( " 
          + pk.getId() + ", " + srk.getExpressState().getDealInstitutionId() + ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate (); // track the creation

        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("RegionProfile Entity - RegionProfile - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }
      srk.setModified(true);
      return this;
   }

    public Contact getContact() throws FinderException, RemoteException
    {
      return new Contact(srk,this.contactId,1);
    }


  /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }


   public void setRegionProfileId(int id)
	 {
		 this.testChange("regionProfileId",id);
		 this.regionProfileId = id;
   }

   public void setRegionName(String name )
	 {
		 this.testChange("regionName", name);
		 this.regionName = name;
   }
   public void setRegionManagerUserId(int id )
	 {
		 this.testChange("regionManagerUserId",id);
		 this.regionManagerUserId = id;
   }
 
   public void setRPShortName(String val )
	 {
		 this.testChange("RPShortName",val);
		 this.RPShortName = val;
   }

   public void setRPBusinessId(String ctr )
	 {
		 this.testChange("RPBusinessId",ctr);
		 this.RPBusinessId = ctr;
   }

   public void setContactId(int id)
	 {
		 this.testChange("contactId", id);
		 this.contactId = id;
   }
   public void setProfileStatusId(int id )
	 {
		 this.testChange("profileStatusId", id);
		 this.profileStatusId = id;
   }


  /**
   * @return the institutionId
   */
  public int getInstitutionId() {
    return institutionId;
  }


  /**
   * @param institutionId the institutionId to set
   */
  public void setInstitutionId(int institutionId) {
    this.testChange("institutionProfileId", institutionId);
    this.institutionId = institutionId;
  }

}
