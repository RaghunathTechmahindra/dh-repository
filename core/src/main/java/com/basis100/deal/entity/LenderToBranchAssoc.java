package com.basis100.deal.entity;

import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.pk.*;

public class LenderToBranchAssoc extends DealEntity
{
   protected int      lenderProfileId;
   protected int      branchProfileId;
   protected int      contactId;

   protected String   ELPhoneNumber;
   protected String   ELFaxNumber;
   protected String   ETPhoneNumber;
   protected String   ETFaxNumber;
   protected String   FLPhoneNumber;
   protected String   FLFaxNumber;
   protected String   FTPhoneNumber;
   protected String   FTFaxNumber;
   protected int      institutionProfileId;  

   private LenderToBranchAssocPK pk;

   public LenderToBranchAssoc(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

 
   public LenderToBranchAssoc findByPrimaryKey(LenderToBranchAssocPK pk) throws RemoteException, FinderException
   {
      String sql = "Select * from LenderToBranchAssoc  " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        return this;
   }

  public Collection findByBranchAndLender(BranchProfilePK bpk, LenderProfilePK lpk) throws RemoteException, FinderException
  {

    String sql = "Select LenderProfileId, BranchProfileId, contactId from " +
                 "LenderToBranchAssoc  where branchProfileId = " + bpk.getId() +
                 " and lenderProfileId = " + lpk.getId();

    boolean gotRecord = false;
    LenderToBranchAssoc current = null;
    Collection associations = new ArrayList();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          gotRecord = true;
          int lid = jExec.getInt(key,1);
          int bid = jExec.getInt(key,2);
          int cid = jExec.getInt(key,3);

          current = new LenderToBranchAssoc(srk);
          current = current.findByPrimaryKey(new LenderToBranchAssocPK(lid,bid,cid));

          associations.add(current);
        }

         jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

     return associations;
   }



   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setLenderProfileId(jExec.getInt(key,"LENDERPROFILEID"));
      this.setBranchProfileId(jExec.getInt(key,"BRANCHPROFILEID"));
      this.setContactId( jExec.getInt(key,"CONTACTID"));
      this.setELPhoneNumber(jExec.getString(key,"ELPHONENUMBER"));
      this.setELFaxNumber(jExec.getString(key,"ELFAXNUMBER"));
      this.setETPhoneNumber(jExec.getString(key,"ETPHONENUMBER"));
      this.setETFaxNumber(jExec.getString(key,"ETFAXNUMBER"));
      this.setFLPhoneNumber(jExec.getString(key,"FLPHONENUMBER"));
      this.setFLFaxNumber(jExec.getString(key,"FLFAXNUMBER"));
      this.setFTPhoneNumber(jExec.getString(key,"FTPHONENUMBER"));
      this.setFTFaxNumber(jExec.getString(key,"FTFAXNUMBER"));
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));

   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
 protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "LenderToBranchAssoc"; 

   }	


  /**
  *   retrieves the Contact parent record associated with this entity
  *
  *   @return a Contact
  */
  public Contact getContact()throws FinderException,RemoteException
  {
    return new Contact(srk,this.getContactId(),1);
  }


	

  public LenderToBranchAssocPK getAssocPk()
  {
    return this.pk ;
  }

  /**
   *   gets the lenderProfileId associated with this entity
   *
   *   @return a int
   */
  public int getLenderProfileId()
  {
    return this.lenderProfileId ;
  }

  /**
   *   gets the lenderProfileId associated with this entity
   *
   *   @return a int
   */
  public int getBranchProfileId()
  {
    return this.branchProfileId ;
  }
  /**
   *   gets the contactId associated with this entity
   *
   *   @return a int
   */
  public int getContactId()
  {
    return this.contactId ;
  }


  public String getELPhoneNumber(){return this.ELPhoneNumber; }
  public String getELFaxNumber(){return this.ELFaxNumber; }
  public String getETPhoneNumber(){return this.ETPhoneNumber; }
  public String getETFaxNumber(){return this.ETFaxNumber; }
  public String getFLPhoneNumber(){return this.FLPhoneNumber; }
  public String getFLFaxNumber(){return this.FLFaxNumber; }
  public String getFTPhoneNumber(){return this.FTPhoneNumber; }
  public String getFTFaxNumber(){return this.FTFaxNumber; }



  /**
   * creates a LenderProfile using the LenderProfileId with mininimum fields
   *
   */

  public LenderToBranchAssoc create(LenderProfilePK lpk,BranchProfilePK bpk)
    throws RemoteException, CreateException
  {
        String sql = "Insert into LenderToBranchAssoc(" + lpk.getName() + " , " +
                     bpk.getName() + ", INSTITUTIONPROFILEID ) Values ( " +
                     lpk.getId() + " , " + bpk.getId() + ","+srk.getExpressState().getDealInstitutionId()+")";

        try
        {
          jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("LenderToBranchAssoc Entity - LenderToBranchAssoc - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

      this.lenderProfileId = lpk.getId();
      this.branchProfileId = bpk.getId();
      this.pk = pk;
      srk.setModified(true);
      return this;
  }

  public void setLenderProfileId(int value)
  {
    this.testChange("lenderProfileId", value);
    this.lenderProfileId = value;
  }

  public void setBranchProfileId(int value)
  {
    this.testChange("branchProfileId", value);
    this.branchProfileId = value;
  }

  public void setContactId(int value )
  {
    this.testChange("contactId", value);
    this.contactId = value;
  }

  public void setELPhoneNumber(String value)
  {
    this.testChange("ELPhoneNumber", value);
    this.ELPhoneNumber = value;
  }

  public void setELFaxNumber(String value)
  {
    this.testChange("ELFaxNumber", value);
    this.ELFaxNumber = value;
  }

  public void setETPhoneNumber(String value)
  {
    this.testChange("ETPhoneNumber", value);
    this.ETPhoneNumber = value;
  }

  public void setETFaxNumber(String value)
  {
    this.testChange("ETFaxNumber", value);
    this.ETFaxNumber = value;
  }
  public void setFLPhoneNumber(String value)
  {
    this.testChange("FLPhoneNumber", value);
    this.FLPhoneNumber = value;
  }

  public void setFLFaxNumber(String value)
  {
    this.testChange("FLFaxNumber", value);
    this.FLFaxNumber = value;
  }

  public void setFTPhoneNumber(String value)
  {
    this.testChange("FTPhoneNumber", value);
    this.FTPhoneNumber = value;
  }

  public void setFTFaxNumber(String value)
  {
    this.testChange("FTFaxNumber", value);
    this.FTFaxNumber = value;
  }


  public int getInstitutionProfileId() {
      return institutionProfileId;
  }


  public void setInstitutionProfileId(int institutionProfileId) {
      this.testChange("institutionProfileId", institutionProfileId); 
      this.institutionProfileId = institutionProfileId;
  }


}
