package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.LDInsuranceRatesPK;
import com.basis100.deal.pk.LifeDisabilityPremiumsPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;



public class LifeDisabilityPremiums extends DealEntity
{
   protected int      lifeDisabilityPremiumsId;
   protected int      borrowerId;
   protected int      copyId;
   protected int      lenderProfileId;
   protected int      LDInsuranceRateId;
   protected int      LDInsuranceTypeId;
   protected int      disabilityStatusId;
   protected int      lifeStatusId;
   protected int      institutionProfileId; 

   protected LifeDisabilityPremiumsPK pk;

   public LifeDisabilityPremiums(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

  public LifeDisabilityPremiums(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
  {
    super(srk,dcm);
  }

  public LifeDisabilityPremiums(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
  {
    super(srk,dcm);

    pk = new LifeDisabilityPremiumsPK(id, copyId);

    findByPrimaryKey(pk);
  }

   public LifeDisabilityPremiums findByPrimaryKey(LifeDisabilityPremiumsPK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from LifeDisabilityPremiums where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key, false);
              break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public List findByLDInsuranceRateId(int id) throws Exception
    {
      List found = new ArrayList();
      StringBuffer sql = new StringBuffer("Select * from LifeDisabilityPremiums where ");
      sql.append("LDInsuranceRateId = " + id);

      try
      {
          int key = jExec.execute(sql.toString());

          for (; jExec.next(key); )
          {
              LifeDisabilityPremiums ldprem = new LifeDisabilityPremiums(srk);
              ldprem.setPropertiesFromQueryResult(key, false);
              found.add(ldprem);
          }

          jExec.closeData(key);
        }
        catch (Exception e)
        {
            Exception fe = new Exception("LifeDisabilityPremiums Entity - findByPricingProfileId() exception for " + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
   }

  public Collection findByBorrower(BorrowerPK pk) throws Exception
  {
    Collection lifedisabilitypremiums = new ArrayList();

    String sql = "Select * from LifeDisabilityPremiums " ;
          sql +=  pk.getWhereClause();

     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk,dcm);
           ldp.setPropertiesFromQueryResult(key);
           ldp.pk = new LifeDisabilityPremiumsPK(ldp.getLifeDisabilityPremiumsId(),ldp.getCopyId());
           lifedisabilitypremiums.add(ldp);
        }
        jExec.closeData(key);
      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while fetching Borrower::LifeDisabilityPremiums");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

     return lifedisabilitypremiums;
  }

  //--DJ_CR136--23Nov2004--start--//
  public Collection findByInsureOnlyApplicant(InsureOnlyApplicantPK pk) throws Exception
  {
    Collection lifedisabilitypremiums = new ArrayList();

    String sql = "Select * from LifeDisabilityPremiums " ;
          sql +=  pk.getWhereClause();

     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
           LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk,dcm);
           ldp.setPropertiesFromQueryResult(key);
           ldp.pk = new LifeDisabilityPremiumsPK(ldp.getLifeDisabilityPremiumsId(),ldp.getCopyId());
           lifedisabilitypremiums.add(ldp);
        }
        jExec.closeData(key);
      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while fetching InsureOnlyApplicant::LifeDisabilityPremiums");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

     return lifedisabilitypremiums;
  }
  //--DJ_CR136--23Nov2004--end--//

  public Vector findByMyCopies() throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from LifeDisabilityPremiums where LifeDisabilityPremiumsId = " +
       getLifeDisabilityPremiumsId() + " AND copyId <> " + getCopyId();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            LifeDisabilityPremiums iCpy = new LifeDisabilityPremiums(srk,dcm);

            iCpy.setPropertiesFromQueryResult(key);
            iCpy.pk = new LifeDisabilityPremiumsPK(iCpy.getLifeDisabilityPremiumsId(), iCpy.getCopyId());

            v.addElement(iCpy);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {

        FinderException fe = new FinderException("getLifeDisabilityPremiumsId() - findByMyCopies exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
    return v;
  }

   private void setPropertiesFromQueryResult(int key, boolean deep) throws Exception
   {
      this.setLifeDisabilityPremiumsId (jExec.getInt(key,"LIFEDISABILITYPREMIUMSID"));
      this.setLDInsuranceRateId (jExec.getInt(key,"LDINSURANCERATEID"));
      this.setBorrowerId (jExec.getInt(key,"BORROWERID"));
      this.setCopyId (jExec.getInt(key,"COPYID"));
      this.setDisabilityStatusId (jExec.getInt(key,"DISABILITYSTATUSID"));
      this.setLifeStatusId (jExec.getInt(key,"LIFESTATUSID"));
      this.setLenderProfileId (jExec.getInt(key,"LENDERPROFILEID"));
      this.setLDInsuranceTypeId (jExec.getInt(key,"LDINSURANCETYPEID"));
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
   }

   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setLifeDisabilityPremiumsId (jExec.getInt(key,"LIFEDISABILITYPREMIUMSID"));
      this.setLDInsuranceRateId (jExec.getInt(key,"LDINSURANCERATEID"));
      this.setBorrowerId (jExec.getInt(key,"BORROWERID"));
      this.setCopyId (jExec.getInt(key,"COPYID"));
      this.setDisabilityStatusId (jExec.getInt(key,"DISABILITYSTATUSID"));
      this.setLifeStatusId (jExec.getInt(key,"LIFESTATUSID"));
      this.setLenderProfileId (jExec.getInt(key,"LENDERPROFILEID"));
      this.setLDInsuranceTypeId (jExec.getInt(key,"LDINSURANCETYPEID"));
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
   }

  /**
   *  gets the value of instace fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

   /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }

  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate() throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
   }

   public String getEntityTableName()
   {
      logger.debug("LDPEntity@GetEntityTableStamp");
      return "LifeDisabilityPremiums";
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a LifeDisabilityPremiumsPK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

  /**
   * Should be changed to create the entity with all not nullable fields.
   * creates a LifeDisabilityPremiums using the LifeDisabilityPremiumsId with mininimum fields
   *
   */
  public LifeDisabilityPremiums create(LifeDisabilityPremiumsPK pk)throws RemoteException, CreateException
  {
      String sql = "Insert into LifeDisabilityPremiums(" + pk.getName() + ", copyID, INSTITUTIONPROFILEID ) Values ( " + pk.getId() + "," + 
          pk.getCopyId() + "," + srk.getExpressState().getDealInstitutionId() +")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("LifeDisabilityPremiums Entity - LifeDisabilityPremiums - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      this.pk = pk;

      return this;
    }

  /**
   * creates a LifeDisabilityPremiums using the LifeDisabilityPremiums primary keys and a borrower primary key
   *  - the mininimum (ie.non-null) fields
   *
   */

    public LifeDisabilityPremiums create(BorrowerPK bpk, int lifeDisabilityInsTypeId)throws RemoteException, CreateException
    {
       pk = createPrimaryKey(bpk.getCopyId());

       //// 1. Since DJ took a decision not to provide/update their data to populate
       //// the appropriate tables this method is currently hardcoded.

       //// 2. IMPORTANT:
       //// The rateId should be picked up (or passed via RMI) based on all
       //// dimentional tables such as: LDInsuranceAgeRanges, and LDInitialLoanRanges, etc.
       //// The suggested db addition for Life&Disability Insurance module allows
       //// either independant storage of all related data with BXP cals engine or keep track
       //// of data marshalling between BXP and external legacy system (DJ) via RMI, e.g.

       //// 3. All this strategy should be designed (and other entities finished) after getting
       //// at least initial documentation from DJ. It may take an update of the current Life/Disability db
       //// schema design addition. This is not a final Entity AT ALL in terms of methods, etc.

       //// 4. Add appropriate population from db and/or from DJ RMI process when
       //// the specs will be finalized!!!

       //// Currently hardcoded just to create entities for Deal Enty deal creation.
       int ldRateId = 1;   // Default for disability
       int lenderProfileId = 0;

       if (lifeDisabilityInsTypeId == 1)
          ldRateId = 26;
       else if (lifeDisabilityInsTypeId == 2)
          ldRateId =1;

logger.debug("LDP@CreateBorrowerPK::StampForType: " + lifeDisabilityInsTypeId);

       String sql = "Insert into LifeDisabilityPremiums(LifeDisabilityPremiumsId, BorrowerId, copyId, " +
                    "LDInsuranceRateId, LenderProfileId, LDInsuranceTypeId , INSTITUTIONPROFILEID) Values ( " +
                    pk.getId() + "," + bpk.getId() + "," + bpk.getCopyId() + "," + ldRateId + "," +
                    lenderProfileId + "," + lifeDisabilityInsTypeId + "," + srk.getExpressState().getDealInstitutionId()+ ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("LifeDisabilityPremiums Entity - LifeDisabilityPremiums - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

       srk.setModified(true);
       return this;
    }

    //--DJ_CR136--23Nov2004--start--//
    public LifeDisabilityPremiums create(InsureOnlyApplicantPK ioapk, int lifeDisabilityInsTypeId)throws RemoteException, CreateException
    {
       pk = createPrimaryKey(ioapk.getCopyId());

       //// 1. Since DJ took a decision not to provide/update their data to populate
       //// the appropriate tables this method is currently hardcoded.

       //// 2. IMPORTANT:
       //// The rateId should be picked up (or passed via RMI) based on all
       //// dimentional tables such as: LDInsuranceAgeRanges, and LDInitialLoanRanges, etc.
       //// The suggested db addition for Life&Disability Insurance module allows
       //// either independant storage of all related data with BXP cals engine or keep track
       //// of data marshalling between BXP and external legacy system (DJ) via RMI, e.g.

       //// 3. All this strategy should be designed (and other entities finished) after getting
       //// at least initial documentation from DJ. It may take an update of the current Life/Disability db
       //// schema design addition. This is not a final Entity AT ALL in terms of methods, etc.

       //// 4. Add appropriate population from db and/or from DJ RMI process when
       //// the specs will be finalized!!!

       //// Currently hardcoded just to create entities for Deal Enty deal creation.
       int ldRateId = 1;   // Default for disability
       int lenderProfileId = 0;

       if (lifeDisabilityInsTypeId == 1)
          ldRateId = 26;
       else if (lifeDisabilityInsTypeId == 2)
          ldRateId =1;

logger.debug("LDP@CreateInsureOnlyApplPK::StampForType: " + lifeDisabilityInsTypeId);

       String sql = "Insert into LifeDisabilityPremiums(LifeDisabilityPremiumsId, BorrowerId, copyId, " +
                    "LDInsuranceRateId, LenderProfileId, LDInsuranceTypeId, INSTITUTIONPROFILEID ) Values ( " +
                    pk.getId() + "," + ioapk.getId() + "," + ioapk.getCopyId() + "," + ldRateId + "," +
                    lenderProfileId + "," + lifeDisabilityInsTypeId + "," + srk.getExpressState().getDealInstitutionId()+ ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("LifeDisabilityPremiums Entity - LifeDisabilityPremiums - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

       srk.setModified(true);
       return this;
    }
    //--DJ_CR136--23Nov2004--end--//

   //// Not in use now.
   public LifeDisabilityPremiums create( BorrowerPK bpk,
                                         int lifeDisabilityPremiumsId,
                                         int borrowerId,
                                         int copyId,
                                         int lenderProfileId,
                                         int LDInsuranceRateId,
                                         int LDInsuranceTypeId,
                                         int disabilityStatusId,
                                         int lifeStatusId)throws RemoteException, CreateException
   {

        copyId = bpk.getCopyId();
        pk = createPrimaryKey(copyId);
        borrowerId = bpk.getId();

        String sql =  "Insert into Fee(lifeDisabilityPremiumsId,  borrowerId, copyId, lenderProfileId, " +
                      "LDInsuranceRateId, " +
                      "LDInsuranceTypeId, disabilityStatusId, disabilityStatusId, dlifeStatusId, INSTITUTIONPROFILEID) Values ( " +
                      pk.getId() + ", " + borrowerId + ", " + copyId + ", " +
                      lenderProfileId + ", " + LDInsuranceRateId +
                      ", " + LDInsuranceTypeId + ", " + disabilityStatusId + ", " + lifeStatusId + ", " + srk.getExpressState().getDealInstitutionId()+ ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          this.trackEntityCreate (); // Track the creation
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("LifeDisabilityPremiums Entity - LifeDisabilityPremiums - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
      srk.setModified(true);
      return this;
  }

   private LifeDisabilityPremiumsPK createPrimaryKey(int copyId)throws CreateException
   {
     String sql = "Select LifeDisabilityPremiumsSeq.nextval from dual";

     int id = -1;

//logger.debug("LDP@CreateCopyId::StampCreatePK");

     try
     {
         int key = jExec.execute(sql);

         for (; jExec.next(key);  )
         {
             id = jExec.getInt(key,1);  // can only be one record
         }

         jExec.closeData(key);

         if( id == -1 ) throw new Exception();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("LifeDisabilityPremiums Entity create() exception getting LifeDisabilityPremiumsId from sequence");
        logger.error(ce.getMessage());
        logger.error(e);
        throw ce;
      }

      return new LifeDisabilityPremiumsPK(id, copyId);
  }

  /**
   * creates a LifeDisabilityPremiums using the LifeDisabilityPremiumsId with mininimum fields
   *
   */
  public LifeDisabilityPremiums create(LifeDisabilityPremiumsPK pk, LDInsuranceRatesPK ppk)throws RemoteException, CreateException
  {
      String sql = "Insert into LifeDisabilityPremiums( LifeDisabilityPremiumsID, LDInsuranceRatesId, INSTITUTIONPROFILEID ) Values ( " +
                         pk.getId() + ", " + ppk.getId() + ", " + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityChange();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("LifeDisabilityPremiums Entity - LifeDisabilityPremiums - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
      srk.setModified(true);
      return this;
    }

   //// Getters

	 public int getLifeDisabilityPremiumsId()   {     return this.lifeDisabilityPremiumsId;   }
   public int getBorrowerId()   {    return this.borrowerId;   }
   public int getCopyId()   {    return this.copyId;   }
   public int getLenderProfileId()   {    return this.lenderProfileId;   }
   public int getLDInsuranceRateId()   {    return this.LDInsuranceRateId;   }
   public int getLDInsuranceTypeId()   {    return this.LDInsuranceTypeId;   }
   public int getDisabilityStatusId()   {    return this.disabilityStatusId;   }
   public int getLifeStatusId()   {    return this.lifeStatusId;   }

   //// Setters
   public void setLifeDisabilityPremiumsId(int val)
	 {
		 this.testChange("lifeDisabilityPremiumsId", val);
		 this.lifeDisabilityPremiumsId = val;
   }

   public void setBorrowerId(int val)
	 {
		 this.testChange("borrowerId", val);
		 this.borrowerId = val;
   }

   public void setCopyId(int val)
	 {
		 this.testChange("copyId", val);
		 this.copyId = val;
   }

   public void setLenderProfileId(int val)
	 {
		 this.testChange("lenderProfileId", val);
		 this.lenderProfileId = val;
   }

   public void setLDInsuranceRateId(int val)
	 {
		 this.testChange("LDInsuranceRateId", val);
		 this.LDInsuranceRateId = val;
   }

   public void setLDInsuranceTypeId(int val)
	 {
		 this.testChange("LDInsuranceTypeId", val);
		 this.LDInsuranceTypeId = val;
   }

   public void setDisabilityStatusId(int val)
	 {
		 this.testChange("disabilityStatusId", val);
		 this.disabilityStatusId = val;
   }

   public void setLifeStatusId(int val)
	 {
		 this.testChange("lifeStatusId", val);
		 this.lifeStatusId = val;
   }

   public void setInstitutionProfileId(int institutionProfileId) {
       testChange("institutionProfileId", institutionProfileId);
       this.institutionProfileId = institutionProfileId;
   }

   public int getInstitutionProfileId() {
       return institutionProfileId;
   }

}
