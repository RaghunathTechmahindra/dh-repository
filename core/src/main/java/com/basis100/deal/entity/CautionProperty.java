package com.basis100.deal.entity;


import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.SQLException;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData ;

public class CautionProperty extends DealEntity
{
  protected int     CPProvinceId;
  protected String  CPCity;
  protected String  CPPostalFSA;
  protected String  CPPostalLDU;
  protected String  CPStreetName;
  protected String  CPStreetNumber;
  protected int     CPStreetDirectionId;
  protected int     CPStreetTypeId;
  protected String  cautionReason;
  
  protected int     institutionProfileId;
  protected int     cautionPropertiesId;

  private CautionPropertyPK pk;

  public CautionProperty(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }


  public List findByAddressComponents(  String CPStreetNumber,
                                        String CPStreetName,
                                        String CPCity ) throws RemoteException, FinderException
  {

    List result = new ArrayList();
    // if incoming values are null they will not match property

    StringBuffer sql = new StringBuffer("Select * from CautionProperties where ");

    sql.append(StringUtil.formFieldMatchExpression("CPStreetNumber", CPStreetNumber, false));
    sql.append("AND");
    sql.append(StringUtil.formFieldMatchExpression("CPStreetName", CPStreetName, false));
    sql.append("AND");
    sql.append(StringUtil.formFieldMatchExpression("CPCity", CPCity, false));

    String sqlString = sql.toString();

    try
    {
        int key = jExec.execute(sqlString);

        for (; jExec.next(key); )
        {
           CautionProperty p = new CautionProperty(srk);

           p.setPropertiesFromQueryResult(key);

           result.add(p);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
        FinderException fe = new FinderException("Deal Entity - findByPrimaryCautionProperty() exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }

    return result;
  }


  //Alert presently has no primary key.
 /* public CautionProperty findByPrimaryKey(CautionPropertyPK pk) throws RemoteException, FinderException
  {
    String sql = "Select * from CautionProperty " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
        FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");
        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
     
      this.pk = pk;

      return this;
  }*/

  public void setPropertiesFromQueryResult(int key) throws Exception
  {

    setCPProvinceId( jExec.getInt(key, "CPPROVINCEID"));
    setCPCity( jExec.getString(key, "CPCITY"));
    setCPPostalFSA(jExec.getString(key, "CPPOSTALFSA"));
    setCPPostalLDU(jExec.getString(key, "CPPOSTALLDU"));
    setCPStreetName(jExec.getString(key, "CPSTREETNAME"));
    setCPStreetNumber(jExec.getString(key, "CPSTREETNUMBER"));
    setCPStreetDirectionId(jExec.getInt(key, "CPSTREETDIRECTIONID"));
    setCPStreetTypeId(jExec.getInt(key, "CPSTREETTYPEID"));
    setCautionReason(jExec.getString(key, "CAUTIONREASON"));
    setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    setCautionPropertiesId(jExec.getInt(key, "CAUTIONPROPERTIESID"));
  }



  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

    /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }


  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();
    int ret = doPerformUpdate(this, clazz);

    // Optimization:
    // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;
  }

   // CautionProperty getter/setters
   //

   public String getEntityTableName()
   {
      return "CautionProperty";
   }
 
    /**
     *   gets the CPStreetDirectionId associated with this entity
     *
     *   @return a int
     */
    public int getCPStreetDirectionId()
    {
      return this.CPStreetDirectionId ;
    }


    /**
     *   gets the cautionReason associated with this entity
     *
     *   @return a String
     */
    public String getCautionReason()
    {
      return this.cautionReason ;
    }


    /**
     *   gets the pk associated with this entity
     *
     *   @return a CautionPropertyPK
     */
    public IEntityBeanPK getPk()
    {
      return (IEntityBeanPK)this.pk ;
    }


      /**
     *   gets the CPCity associated with this entity
     *
     *   @return a String
     */
    public String getCPCity()
    {
      return this.CPCity ;
    }

    /**
     *   gets the CPPostalFSA associated with this entity
     *
     *   @return a String
     */
    public String getCPPostalFSA()
    {
      return this.CPPostalFSA ;
    }


    public int getCPStreetTypeId()
    {
      return this.CPStreetTypeId ;
    }


	
    /**
     *   gets the CPProvinceId associated with this entity
     *
     *   @return a int
     */
    public int getCPProvinceId()
    {
      return this.CPProvinceId ;
    }


    public String getCPStreetName() {return this.CPStreetName;}

    public String getCPStreetNumber() {return this.CPStreetNumber;}

    public String getCPPostalLDU(){  return this.CPPostalLDU; }




  /**
   * creates a CautionProperty using the CautionPropertyId with mininimum fields
   *
   */
   @Deprecated
   public CautionProperty create(int CPProvinceId,
                                 String  CPCity,
                                 String  CPPostalFSA,
                                 String  CPPostalLDU,
                                 String  CPStreetName,
                                 String  CPStreetNumber,
                                 int     CPStreetDirectionId,
                                 int     CPStreetTypeId,
                                 String  cautionReason,
                                 int     cautionPropertiesId)throws RemoteException, CreateException
   {
      StringBuffer sql = new StringBuffer();
      sql.append( "Insert into CautionProperties( ");
      sql.append(" CPProvinceId, CPCity, CPPostalFSA, CPPostalLDU, CPStreetName, ");
      sql.append(" CPStreetNumber, CPStreetDirectionId, CPStreetTypeId, cautionReason, cautionPropertiesId, institutionProfileId ) ");
      sql.append("values( ");
      sql.append(CPProvinceId).append(", ");
      sql.append(sqlStringFrom(CPCity)).append(", ");
      sql.append(sqlStringFrom(CPPostalFSA)).append(", ");
      sql.append(sqlStringFrom(CPPostalLDU)).append(", ");
      sql.append(sqlStringFrom(CPStreetName)).append(", ");
      sql.append(sqlStringFrom(CPStreetNumber)).append(", ");
      sql.append(CPStreetDirectionId).append(", ");
      sql.append(CPStreetTypeId).append(", ");
      sql.append(sqlStringFrom(cautionReason)).append(", ");
      sql.append(cautionPropertiesId).append(", ");
      sql.append(srk.getExpressState().getDealInstitutionId()).append(" )");
      try
      {
        jExec.executeUpdate(sql.toString());
        
        this.trackEntityCreate (); // track the creation
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("CautionProperty Entity - CautionProperty - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }

      srk.setModified(true);
      return this;
   }

 
   public void setCPStreetDirectionId(int id  )
	 {
     this.testChange ("CPStreetDirectionId", id  );
		 this.CPStreetDirectionId = id;
   }


   public void setCPProvinceId(int id )
	 {
     this.testChange ("CPProvinceId", id ) ;
     this.CPProvinceId = id;
   }

   public void setCPStreetName(String val )
	 {
     this.testChange ("CPStreetName", val) ;
		 this.CPStreetName = val;
   }

   public void setCPPostalFSA(String val )
	 {
     this.testChange ("CPPostalFSA", val) ;
		 this.CPPostalFSA = val;
   }
   public void setCPCity(String val )
	 {
    this.testChange ("CPCity", val) ;
		this.CPCity = val;
   }

   public void setCautionReason(String reason)
   {
      this.testChange ("cautionReason", reason ) ;
      this.cautionReason = reason ;
   }

   public void setCPStreetTypeId( int d )
	 {
    this.testChange ("CPStreetTypeId", d ) ;
		this.CPStreetTypeId = d;
   }


  public void setCPStreetNumber(String value)
  {
     this.testChange("CPStreetNumber", value);
		 this.CPStreetNumber = value;
  }


  public void setCPPostalLDU(String value)
  {
     this.testChange("CPPostalLDU", value );
		 this.CPPostalLDU = value;
  }


  /**
   * @return the cautionPropertiesId
   */
  public int getCautionPropertiesId() {
    return cautionPropertiesId;
  }


  /**
   * @param cautionPropertiesId the cautionPropertiesId to set
   */
  public void setCautionPropertiesId(int cautionPropertiesId) {
    this.testChange("CAUTIONPROPERTIESID", cautionPropertiesId);
    this.cautionPropertiesId = cautionPropertiesId;
  }


  /**
   * @return the institutionProfileId
   */
  public int getInstitutionProfileId() {
    return institutionProfileId;
  }


  /**
   * @param institutionProfileId the institutionProfileId to set
   */
  public void setInstitutionProfileId(int institutionProfileId) {
    this.testChange("INSTITUTIONPROFILEID", institutionProfileId);
    this.institutionProfileId = institutionProfileId;
  }
}
