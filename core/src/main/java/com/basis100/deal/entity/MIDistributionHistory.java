/**
 * <p>Title: MIDistributionHistory.java</p>
 *
 * <p>MIDistributionHistory: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Hiroyuki Kobayashi
 * @version 1.0 (Initial Version � Jun 21, 2007)
 *
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.MIDistributionHistoryPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class MIDistributionHistory
    extends DealEntity
{
   private static final Log logger = LogFactory
      .getLog(MIDistributionHistory.class);
  
  protected int miDistributionHistoryId;
  protected int mortgageInsurerId;
  protected int dealId;
  protected int institutionProfileId;
  
  

  protected MIDistributionHistoryPK pk;

  public MIDistributionHistory(SessionResourceKit srk)
      throws RemoteException, FinderException
  {
    super(srk);
  }

  public MIDistributionHistory(SessionResourceKit srk, int id)
      throws RemoteException, FinderException
  {
    super(srk);

    pk = new MIDistributionHistoryPK(id);
    findByPrimaryKey(pk);
  }

  public MIDistributionHistory findByPrimaryKey(MIDistributionHistoryPK pk)
      throws RemoteException, FinderException
  {
	  
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from MIDistributionHistory " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "MIDistributionHistory: @findByPrimaryKey(), key= "
            + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException(
          "MIDistributionHistory Entity - findByPrimaryKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
    this.pk = pk;
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
    int index = 0;
    setMiDistributionHistoryId(jExec.getInt(key, ++index));
    setDealId(jExec.getInt(key, ++index));
    setInstitutionProfileId(jExec.getInt(key, ++index));
    setMortgageInsurerId(jExec.getInt(key, ++index));

  }

  protected MIDistributionHistoryPK createPrimaryKey() throws CreateException
  {
    String sql = "Select MIDISTRIBUTIONHISTORYSEQ.nextval from dual";
    int id = -1;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        id = jExec.getInt(key, 1); // can only be one record
      }

      jExec.closeData(key);
      if (id == -1) throw new Exception();

    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("fail @createPrimaryKey");
      logger.error(ce.getMessage());
      logger.error(e);
      throw ce;

    }

    return new MIDistributionHistoryPK(id);
  }

  public MIDistributionHistory create(int institutionProfileId, int dealId, int mortgageInsurerId)
      throws RemoteException, CreateException
  {
    pk = createPrimaryKey();

    String sql = "Insert into MIDistributionHistory ( "
        + " MIDISTRIBUTIONHISTORYID, DEALID, INSTITUTIONPROFILEID, MORTGAGEINSURERID ) Values ( "
        + pk.getId() + "," + dealId + "," + institutionProfileId + "," + mortgageInsurerId + ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("exception @create");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    srk.setModified(true);
    return this;
  }

  public void setMiDistributionHistoryId(int miDistHistId)
  {
    this.testChange("miDistributionHistoryId", miDistHistId);
    this.miDistributionHistoryId = miDistHistId;
  }

  public void setMortgageInsurerId(int miid)
  {
    this.testChange("mortgageInsurerId", miid);
    this.mortgageInsurerId = miid;
  }

  public void setDealId(int deal)
  {
    this.testChange("dealId", deal);
    this.dealId = deal;
  }

  public int getMiDistributionHistoryId()
  {
    return miDistributionHistoryId;
  }

  public int getMortgageInsurerId()
  {
    return mortgageInsurerId;
  }

  public int getDealId()
  {
    return dealId;
  }

  public String getStringValue(String fieldName)
  {
    return doGetStringValue(this, this.getClass(), fieldName);
  }

  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }

  /**
   * Updates any Database field that has changed since the last synchronization
   * ie. the last findBy... call
   *
   * @return the number of updates performed
   */
  protected int performUpdate() throws Exception
  {
    Class clazz = this.getClass();

    return (doPerformUpdate(this, clazz));
  }

  public String getEntityTableName()
  {
    return "MIDistributionHistory";
  }

  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK) this.pk;
  }

  public String toString()
  {
    // for debug
    String str = "";
    str += "miDistributionHistoryId=" + miDistributionHistoryId;
    str += ",mortgageInsurerId=" + mortgageInsurerId;
    str += ",dealId=" + dealId;
    str += ",institutionProfileId=" + institutionProfileId;
    return str;
  }

public int getInstitutionProfileId() {
	return institutionProfileId;
}

public void setInstitutionProfileId(int institutionProfileId) {
	this.testChange("institutionProfileId", institutionProfileId);
	this.institutionProfileId = institutionProfileId;
}

}
