/**
 * <p>Title: DealEventStatusSnapshot.java</p>
 *
 * <p>Description: entity class for DealEventStatusSnapshot</p>
 *
 * <p>Copyright: Filogix Ltd. Partnership (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 * 
 * @author Midori Aida
 * @version 1.0(Initial Version � Apr 28, 2009)
 *
 */
package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealEventStatusSnapshotPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class DealEventStatusSnapshot extends DealEntity {
    
    protected int institutionProfileId;
    protected int dealId;
    protected int systemTypeId;
    protected int dealEventStatusTypeId;
    protected int statusNameReplacementId;
    
    private DealEventStatusSnapshotPK pk;

    /**
     * @param srk SessionResourceKit to use
     * @throws RemoteException
     */    
    public DealEventStatusSnapshot (SessionResourceKit srk) throws RemoteException {
        super(srk);
    }
    
    /**
     * 
     * @param srk SessionResourceKit to us
     * @param id DealEventStatusSnapshot id
     * @throws RemoteException
     */
    public DealEventStatusSnapshot (SessionResourceKit srk, int id, int typeId) throws RemoteException {
        super(srk);
        pk = new DealEventStatusSnapshotPK(id, typeId);
    }
    
    /**
     * Find the row corresponding to this PK and populate entity
     * @param pk The primary key represetning this row
     * @return This (now populated) entity
     */
    public DealEventStatusSnapshot findByPrimaryKey(DealEventStatusSnapshotPK pk) 
        throws RemoteException, FinderException {
        
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "SELECT * FROM DEALEVENTSTATUSSNAPSHOT " + pk.getWhereClause();
        boolean gotRecord = false;
        
        try {
            int key = jExec.execute(sql);
            for(;jExec.next(key);) {
                gotRecord = true;
                this.pk = pk;
                setPropertiesFromQueryResult(key);
                //break;
            }
            
            jExec.closeData(key);
            
            if(!gotRecord) {
                String msg = "DEALEVENTSTATUSSNAPSHOT Entity: @findByQuery(), key= " + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch(Exception e) {
            FinderException fe = new FinderException("DEALEVENTSTATUSSNAPSHOT findByPrimaryKey(): exception: " + e);
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
              
            throw fe;
        }
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }
    /**
     * Create a new primary key from the status event queue sequence
     * @return StatusEventQueuePK
     * @throws CreateException
     */
    public DealEventStatusSnapshotPK createPrimaryKey(int typeId) throws CreateException
    {
        String sql = "SELECT DealEventStatusSnapshotSEQ.nextval from dual";
        int id = -1;
        
        try {
            int key = jExec.execute(sql);
            for(;jExec.next(key);) {
                id = jExec.getInt(key, 1);
                break;
            }
            
            jExec.closeData(key);
            
            if(id < 0) {
                throw new CreateException("DEALEVENTSTATUSSNAPSHOT createPrimaryKey(): no DealEventStatusSnapshotSEQ found");
            }
        }
        catch(Exception e) {
            CreateException ce = new CreateException("DEALEVENTSTATUSSNAPSHOT createPrimaryKey(): exception: " + e );
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }
        
        return new DealEventStatusSnapshotPK(id, typeId);
    }
    
    /**
     * Create a new StatusEventQueue object with mininum fields, insert into database and return.
     * @param Deal deal
     * @param int dealEventStatusTypeId
     * @param int statusNameReplacementId
     * @return The newly created DealEventStatusSnapshot Object
     * @throws RemoteException
     * @throws CreateException
     */
    public DealEventStatusSnapshot create(Deal deal,
            int dealEventStatusTypeId,
            int statusNameReplacementId)            
        throws CreateException {
        
        StringBuffer sqlb = new StringBuffer(
        "INSERT INTO DEALEVENTSTATUSSNAPSHOT (" +
        "institutionProfileId, dealId, systemTypeId, dealEventStatusTypeId, statusNameReplacementId " +
        ") VALUES (" );
        
        sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ", ");
        sqlb.append(sqlStringFrom(deal.getDealId()) + ", ");
        sqlb.append(sqlStringFrom(deal.getSystemTypeId()) + ", ");
        sqlb.append(sqlStringFrom(dealEventStatusTypeId) + ", ");
        sqlb.append(sqlStringFrom(statusNameReplacementId) + ")");
        
        try {
            jExec.execute(sqlb.toString());
            pk = new DealEventStatusSnapshotPK(deal.getDealId(), dealEventStatusTypeId);
            findByPrimaryKey(pk);
//            trackEntityCreate();
        } catch(Exception e) {
            CreateException ce = new CreateException("DealEventStatusSnapshot create(): exception: " + e);
            logger.error(ce.getMessage());
            logger.error("creater sql: " + sqlb.toString());
            logger.error(e);              
            throw ce;
        }
        srk.setModified(true);
        
        return this;
    }
    
    /**
     * Update any database field that has changed sine the last synchroniztion
     * @return the number of updates performed
     */
    public int performUpdate () throws Exception {
        Class thisClass = this.getClass();
        int ret = doPerformUpdate(this, thisClass);
        
        return ret;
    }
    
    private void setPropertiesFromQueryResult(int key) throws Exception {
        setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
        setDealId(jExec.getInt(key, "dealId"));
        setSystemTypeId(jExec.getInt(key, "systemTypeId"));
        setDealEventStatusTypeId(jExec.getInt(key, "dealEventStatusTypeId"));
        setStatusNameReplacementId(jExec.getInt(key, "statusNameReplacementId"));
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
        
    }
    
    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
    public int getDealId() {
        return dealId;
    }
    public void setDealId(int dealId) {
        this.testChange("dealId", dealId);
        this.dealId = dealId;
    }
    public int getSystemTypeId() {
        return systemTypeId;
    }
    public void setSystemTypeId(int systemTypeId) {
        this.testChange("systemTypeId", systemTypeId);
        this.systemTypeId = systemTypeId;
    }
    public int getDealEventStatusTypeId() {
        return dealEventStatusTypeId;
    }
    public void setDealEventStatusTypeId(int dealEventStatusTypeId) {
    	this.testChange("dealEventStatusTypeId", dealEventStatusTypeId);
        this.dealEventStatusTypeId = dealEventStatusTypeId;
    }
    public int getStatusNameReplacementId() {
        return statusNameReplacementId;
    }
    public void setStatusNameReplacementId(int statusNameReplacementId) {
    	this.testChange("statusNameReplacementId", statusNameReplacementId);
        this.statusNameReplacementId = statusNameReplacementId;
    }
    
    /**
     * @return Returns the name of the DB Table this class represents.
     */
    public String getEntityTableName() {
      return "DealEventStatusSnapshot";
    }

    /**
     * @return Returns the Request's primary Key.
     */
    public IEntityBeanPK getPk() {
      return pk;
    }
    

    /*
     * 
     */
    /**
     * find by eventstuatustype
     * @param pk
     * @param typeId
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public DealEventStatusSnapshot findByEventStatusType(DealEventStatusSnapshotPK pk, int typeId) 
    throws RemoteException, FinderException {
    
    String sql = "SELECT * FROM DEALEVENTSTATUSSNAPSHOT "
                + pk.getWhereClause() + " AND DEALEVENTSTATUSTYPEID = "
                + typeId;
    boolean gotRecord = false;
    
    try {
        int key = jExec.execute(sql);
        for(;jExec.next(key);) {
            gotRecord = true;
            this.pk = pk;
            setPropertiesFromQueryResult(key);
            //break;
        }
        
        jExec.closeData(key);
        
        if(!gotRecord) {
            String msg = "DEALEVENTSTATUSSNAPSHOT Entity: @findByQuery(), key= " + pk + ", entity not found";
            if (getSilentMode() == false)
                logger.error(msg);
            
            throw new FinderException(msg);
        }
    } catch(Exception e) {
        
        if (gotRecord == false && getSilentMode() == true)
            throw (FinderException)e;

        FinderException fe = new FinderException("DEALEVENTSTATUSSNAPSHOT findByPrimaryKey(): exception: ");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
    }
    
    return this;
}
}
