/*
 * Response.java
 * 
 * Entity class (connectivity with AVM services)
 * 
 * Created: 07-FEB-2006
 * Author:  Edward Steel
 * 
 * TODO: test create method.
 *           test findByRequestId : Collection
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * Corresponds to the Response DB table.
 * 
 * @author ESteel
 */
public class Response extends DealEntity {

    protected int    responseId;
    
    protected Date   responseDate;
    protected Date   statusDate;
    protected String statusMessage;
    protected String channelTransactionKey;
    
    protected int    requestId;
    protected int    dealId;
    protected int    responseStatusId;
    protected int    institutionProfileId;

    private ResponsePK pk;

    /**
     * @param srk SessionResourceKit to use
     * @throws RemoteException
     */
    public Response(SessionResourceKit srk) throws RemoteException {
        super(srk);
    }
    
    /**
     * @param srk SessionResourceKit to use
     * @param pk Primary Key corresponding to appropriate line in table
     * @throws RemoteException
     */
    public Response(SessionResourceKit srk, int id) 
            throws RemoteException, FinderException {
        super(srk);
        pk = new ResponsePK(id);
        
        findByPrimaryKey(pk);
    }

    /**
     * Create a new primaryky from the response sequence.
     * @return
     * @throws CreateException
     */
    public ResponsePK createPrimaryKey() throws CreateException {
        String sql = "Select RESPONSESEQ.nextval from dual";
        int id = -1;
    
        try {
            int key = jExec.execute(sql);
      
            for(; jExec.next(key); ) {
                id = jExec.getInt(key, 1);
                break;
            }
            jExec.closeData(key);
            if(id == -1) {
                throw new CreateException();
            }
        } catch(Exception e) {
            CreateException ce 
                = new CreateException("Response createPrimaryKey() :" 
                                      + "exception: getting ResponseId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }
     
        return new ResponsePK(id);
    }
  
    /**
     * Create a new Response object, insert it into Database, and return.
     * @param responseDate
     * @param requestId
     * @param dealId
     * @param responseStatusId
     * @param statusDate
     * @param statusMessage
     * @param channelTransactionKey
     * @return The newly created Request.
     * @throws RemoteException
     * @throws CreateException
     */
    public Response create(Date responseDate,
                           int requestId,
                           DealPK dealPk,
                           Date statusDate,
                           int responseStatusId)
        throws RemoteException, CreateException  {
        ResponsePK pk = createPrimaryKey();
        
        return create(pk, responseDate, requestId, dealPk.getId(),
                      statusDate, responseStatusId);
    }
  
    /**
     * Create a new Response object, insert it into Database, and return.
     * @param pk
     * @param responseDate
     * @param requestId
     * @param dealId
     * @param statusDate
     * @param responseStatusId
     * @return The newly created Request.
     * @throws RemoteException
     * @throws CreateException
     */
    public Response create(ResponsePK pk, Date responseDate, int requestId,
            int dealId, Date statusDate, int responseStatusId)
          throws RemoteException, CreateException {
      
        StringBuffer sqlb = new StringBuffer(
                                 "INSERT INTO RESPONSE (RESPONSEID, RESPONSEDATE, "
                                 + "REQUESTID, DEALID, STATUSDATE, RESPONSESTATUSID, "
                                 + "INSTITUTIONPROFILEID) VALUES (");
        sqlb.append(sqlStringFrom(pk.getId()) + ", "); 
        sqlb.append(sqlStringFrom(responseDate) + ", ");
        sqlb.append(sqlStringFrom(requestId) + ", ");
        sqlb.append(sqlStringFrom(dealId) + ", " );
        sqlb.append(sqlStringFrom(statusDate) + ", ");
        sqlb.append(sqlStringFrom(responseStatusId) + ", ");
        sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");
        
        String sql = new String(sqlb);
        
        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce 
              = new CreateException("Response Entity - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);
            
            throw ce;
        }
        
        srk.setModified(true);
        return this;        
    }
  
    
    /**
     * @return Returns the Response's primary key.
     */
    public IEntityBeanPK getPk() {
        return pk;
    }
    
    /**
     * Test for equality.
     * @param obj The (Response) object to test for equality.
     */
    public boolean equals(Object obj) {
        if (obj instanceof Response) {
            Response otherObj = (Response) obj;
            if  (this.responseId == otherObj.getResponseId()) {
                return true;
            }
        }
        return false;   
    }
    
    /**
     * Get Entity's table's name.
     * @return Table name.
     */
    public String getEntityTableName() {
        return "Response";
    }
    
    /**
     *  Gets the value of instance fields as String.
     *  If a field does not exist (or the type is not serviced)** null is returned.
     *  If the field exists (and the type is serviced) but the field is null an empty
     *  String is returned.
     *  @returns value of bound field as a String;
     *  @param  fieldName as String
     *
     *  Other entity types are not yet serviced   ie. You can't get the Province object
     *  for province.
     */
    public String getStringValue(String fieldName){
        return doGetStringValue(this, this.getClass(), fieldName);
    }
  
    /**
     * Performs database update.
     * 
     * @throws Exception
     */
    protected int performUpdate() throws Exception {
        Class clazz = this.getClass();
    
        int result = doPerformUpdate(this, clazz);
    
        return result;
    }
    
  
    /**
     * Find the row corresponding to this PK and populate entity.
     * @param pk The primary key representing this row
     * @return This (now populated) entity.
     * @throws FinderException
     */
    public Response findByPrimaryKey(ResponsePK pk) 
            throws FinderException {    
    	
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "SELECT * FROM RESPONSE" + pk.getWhereClause();
        
        boolean gotRecord = false;
        
        try {
            int key = jExec.execute(sql);
            
            for (; jExec.next(key);) {
                gotRecord = true;
                this.pk = pk;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }
            
            jExec.closeData(key);
            
            if (gotRecord == false) {
                String msg = "Response Entity: @findByQuery(), key= " + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException("Response Entity - findByQuery() exception");
            
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }
    
    /**
     * Returns Collection of <code>Response</code> objects with the given request ID. 
     * 
     * @param requestId The RequestId to search for.
     * @return Collection of responses.
     * @throws Exception
     */
    public Collection findByRequestId(int requestId) throws Exception {
        Collection responses = new Vector();
        
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * FROM RESPONSE WHERE ").
            append("REQUESTID = " + requestId).
            append(" ORDER BY STATUSDATE DESC");
        
        try {
            int key = jExec.execute(sql.toString());
            
            for(; jExec.next(key); ) {
                Response resp = new Response(srk);
                
                resp.setPropertiesFromQueryResult(key);
                resp.pk = new ResponsePK(resp.getResponseId());
                
                responses.add(resp);
            }
            
            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception("Exception caught while fetching Responses.");
            
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;           
        }       
    
        return responses;
    }
  
  /**
   * Returns instance of <code>Response</code> 
   * objects with the given request ID and Max Response ID. 
   * 
   * @param requestId The RequestId to search for.
   * @return respons.
   * @throws FinderException
   */
    public Response findLastResponseByRequestId(int requestId) 
      throws FinderException {
    
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * ");
        sql.append("  FROM RESPONSE ");
        sql.append(" WHERE RESPONSEID = ");
        sql.append("  (SELECT MAX(RESPONSEID) ");
        sql.append("     FROM RESPONSE ");
        sql.append("    WHERE REQUESTID = ").append(sqlStringFrom(requestId));
        sql.append("  )");
    
        boolean gotRecord = false;
        int key = 0;
        try {
            key = jExec.execute(sql.toString());
      
            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break;
            }
        } catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true) {
                throw (FinderException) e;
            }
            FinderException fe = new FinderException(
                "Request Entity - findLastRequestByRequestId() Exception");
      
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
      
            throw fe;
        } finally {
            try {
                if (key != 0) {
                  jExec.closeData(key);
                }
            } catch (Exception e) {
                logger.debug(e.toString());
            }
        }
    
        if (gotRecord == false) {
            String msg = "Response entity: @findLastResponseByRequestId(), requestId = "
                + requestId + ", not found.";
            if (getSilentMode() == false) logger.error(msg);
      
            throw new FinderException(msg);
        }
    
        this.pk = new ResponsePK(this.responseId);
    
        return this;
      
    }
    /**
     **************  WARNING  *****************
     * THIS METHOD DOESNOT MAKE SENSE
     * PASSING DEAL PK, i.e requestId and copyId 
     * copyId isnot included in respose table
     * SHOULD BE DLETED
     * SEP 6, 2007 Midori
     * ****************************************
     * Find Response using DealPK
     * 
     * @param dealPK DealPK to find by.
     * @return Collection of Responses from DB row matching IDs.
     * @throws RemoteException
     * @throws FinderException
     */
    public Collection findByDeal(DealPK pk) throws Exception {

        Collection responses = new ArrayList();
  
        String sql = "Select * from Response " ;
        sql +=  pk.getWhereClause();
        sql +=  " order by ResponseDate desc";
        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key); ) {
                Response response = new Response(srk);
                response.setPropertiesFromQueryResult(key);
                response.pk = new ResponsePK(response.getResponseId());
                responses.add(response);
            }
            jExec.closeData(key);
  
        } catch (Exception e) {
            Exception fe = new Exception("Exception caught while getting Deal::Borrowers");
            logger.error(fe.getMessage());
            logger.error(e);
  
            throw fe;
        }
        return responses;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
        setResponseId(jExec.getInt(key, "responseId"));
        setResponseDate(jExec.getDate(key, "responseDate"));
        setRequestId(jExec.getInt(key, "requestId"));
        setDealId(jExec.getInt(key, "dealId"));
        setResponseStatusId(jExec.getInt(key, "responseStatusId"));
        setStatusDate(jExec.getDate(key, "statusDate"));
        setStatusMessage(jExec.getString(key, "statusMessage"));
        setChannelTransactionKey(jExec.getString(key, "channelTransactionKey"));
        setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
    }


    //------ START: getters and setters --------------------------------------
    /**
     * @return Returns the channel transaction key.
     */
    public String getChannelTransactionKey() {
        return channelTransactionKey;
    }

    /**
     * @param channelTransactionKey The channel transaction key to set.
     */
    public void setChannelTransactionKey(String channelTransactionKey) {
        testChange("channelTransactionKey", channelTransactionKey);
        this.channelTransactionKey = channelTransactionKey;
    }

    /**
     * @return Returns the copy ID.
     */
    public int getCopyId() {
        return -1;
    }


    /**
     * @return Returns the deal ID.
     */
    public int getDealId() {
        return dealId;
    }

    /**
     * @param dealId The deal ID to set.
     */
    public void setDealId(int dealId) {
        testChange("dealId", dealId);
        this.dealId = dealId;
    }


    /**
     * @return Returns the request ID.
     */
    public int getRequestId() {
        return requestId;
    }

    /**
     * @param requestId The request ID to set.
     */
    public void setRequestId(int requestId) {
        testChange("requestId", requestId);
        this.requestId = requestId;
    }

    /**
     * @return Returns the response date.
     */
    public Date getResponseDate() {
        return responseDate;
    }

    /**
     * @param responseDate The response date to set.
     */
    public void setResponseDate(Date responseDate) {
        testChange("responseDate", responseDate);
        this.responseDate = responseDate;
    }

    /**
     * @return Returns the response ID.
     */
    public int getResponseId() {
        return responseId;
    }

    /**
     * @param responseId The response ID to set.
     */
    public void setResponseId(int responseId) {
        testChange("responseId", responseId);
        this.responseId = responseId;
    }

    /**
     * @return Returns the response status ID.
     */
    public int getResponseStatusId() {
        return responseStatusId;
    }

    /**
     * @param responseStatusId The response status ID to set.
     */
    public void setResponseStatusId(int responseStatusId) {
        testChange("responseStatusId", responseStatusId);
        this.responseStatusId = responseStatusId;
    }

    /**
     * @return Returns the status date.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * @param statusDate The status date to set.
     */
    public void setStatusDate(Date statusDate) {
        testChange("statusDate", statusDate);
        this.statusDate = statusDate;
    }

    /**
     * @return Returns the status message.
     */
    public String getStatusMessage() {
        return statusMessage;
    }
    /**
     * !!!!!!DO NOT USE : Use the method findByChannelTranKeyAndServProduct instead!!!!!!!!
     * @param ctk
     * @return
     * @throws Exception
     * @deprecated
     */
    public Response findByChannelTransactionKey(String ctk) throws Exception {
        /* assumes that higher responsedate would be the
         * most current
         */
        String sql = "Select * from Response where ChannelTransactionKey =" 
          + sqlStringFrom(ctk) + " Order by responsedate desc";
      
        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key); ) {
               setPropertiesFromQueryResult(key);   
//               LZhou Sept. 25, 2006, update pk
               this.pk = new ResponsePK(this.getResponseId());  
               break;
              }
              jExec.closeData(key);
        } catch (Exception e) {
            Exception fe 
              = new Exception("Exception caught  while getting Deal::Borrowers");
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }
        return this;
    }
    

    
    /**
     * @param statusMessage The status message to set.
     */
    public void setStatusMessage(String statusMessage) {
        testChange("statusMessage", statusMessage);
        this.statusMessage = statusMessage;
    }
    //------- END: getters and setters ---------------------------------------

    /**
     **************  WARNING  *****************
     * THIS METHOD DOESNOT MAKE SENSE
     * PASSING REQEUEST PK, i.e requestId and copyId 
     * copyId isnot included in respose table
     * SHOULD BE DLETED
     * SEP 6, 2007 Midori
     * ****************************************
     */
    public Collection findByRequest(RequestPK pk) throws Exception {

        Collection responses = new ArrayList();

        String sql = "Select * from Response " ;
        sql += pk.getWhereClause();

        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key); ) {
                Response response = new Response(srk);
                response.setPropertiesFromQueryResult(key);
                response.pk = new ResponsePK(response.getResponseId());
                responses.add(response);
            }
            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception("Exception caught while getting Deal::Borrowers");
            logger.error(fe.getMessage());
            logger.error(e);
  
            throw fe;
        }

        return responses;
    }

    private void setPk(ResponsePK pk) {
      this.pk = pk;
    }

    public Collection getAdjudicationResponses() throws Exception {
        return new AdjudicationResponse(srk).findByResponse(this.pk);
    }   
    
    public Collection getAvmSummarys() throws Exception {
        return new AvmSummary(srk).findByResponse(this.pk);
    }   

    public Collection getAppraisalSummarys() throws Exception {
        return new AppraisalSummary(srk).findByResponse(this.pk);
    }   

    /**
     * Clones everything except for the PK.  Inserts the new record into the DB.
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
      Response clonedResponse = null;

        try {
            // first create teh response to generate the new PK
            clonedResponse = new Response(srk);
            clonedResponse.create(this.getResponseDate(), this.getRequestId(), 
                    new DealPK(this.getDealId(), this.getCopyId()), 
                    this.getStatusDate(), this.getResponseStatusId());
    
            // now save the newly generated request PK before cloning
            ResponsePK newResponsePk = (ResponsePK)clonedResponse.getPk();
    
            // call Object's clone() which does a shallow copy
            clonedResponse = (Response)super.clone();
    
            // restore the pk
            clonedResponse.setPk(newResponsePk);
            clonedResponse.setResponseId(newResponsePk.getId());
            
            clonedResponse.doPerformUpdateForce();
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in Response.clone().  Trying to clone responseid = "
                    + this.responseId + " " + ex.getMessage());
        }
  
        return clonedResponse;
    }

    protected void removeSons() throws Exception {

       // -------- start of all AdjudicationResponse -------------
       Collection aresps = this.getAdjudicationResponses();
       Iterator itResp = aresps.iterator () ;
       while ( itResp.hasNext () )
       {
           AdjudicationResponse aresp = (AdjudicationResponse) itResp.next () ;
           if ( itResp.hasNext () ) {
               aresp.ejbRemove ( false );
           } else {
               aresp.ejbRemove ( true );
           }
       }
       // -------- end of all AdjudicationResponse -------------

       // -------- start of all AvmSummary -------------
       Collection avmsumms = this.getAvmSummarys();
       Iterator itAVM = avmsumms.iterator () ;
        while ( itAVM.hasNext () ) {
            AvmSummary avmsumm = (AvmSummary) itAVM.next () ;
            if ( itAVM.hasNext () ) {
                avmsumm.ejbRemove ( false );
            } else{
                avmsumm.ejbRemove ( true );
            }
        }
        // -------- end of all AvmSummary -------------

        // -------- start of all AppraisalSummary -------------
        Collection appsumms = this.getAppraisalSummarys();
        Iterator itApp = appsumms.iterator () ;
        while ( itApp.hasNext () ) {
            AppraisalSummary appsumm = (AppraisalSummary) itApp.next () ;
            if ( itApp.hasNext () ) {
                appsumm.ejbRemove ( false );
            } else {
                appsumm.ejbRemove ( true );
            }
        }
        // -------- end of all AdjudicationResponse -------------
    }

    // Catherine: AVM --------- begin --------------
    
    public List getChildren() throws Exception {
        List result = new ArrayList();
      
        result.addAll(getAvmSummarys());
        result.addAll(getAppraisalSummarys());
        result.addAll(getAdjudicationResponses());

        return result;
    }
    // Catherine: AVM --------- begin --------------
    
    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    public Response findChannelTransactionData(ServiceResponseQueue srq) throws FinderException{
        return findChannelTransactionData(srq.getChannelTransactionKey(), srq.getDealId(), srq.getServiceProductId());
    }
    
    public Response findChannelTransactionData(String channelTransactionKey, int dealId, int  serviceProductId) throws FinderException{

        boolean gotRecord = false;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT   response.* ");
        sql.append("  FROM   response ");
        sql.append("         JOIN request ");
        sql.append("         ON       response.requestid = request.requestid ");
        sql.append("         JOIN serviceproduct ");
        sql.append("         ON       request.serviceproductid = serviceproduct.serviceproductid ");
        sql.append(" WHERE   response.channeltransactionkey    = ").append(sqlStringFrom(channelTransactionKey));
        sql.append("   AND   request.dealid                    = ").append(dealId);
        sql.append("   AND   serviceproduct.serviceproductid   = ").append(serviceProductId);
        sql.append(" ORDER BY response.responsedate DESC");

        try {
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                this.pk = new ResponsePK(this.getResponseId());  
                break;
            }
            jExec.closeData(key);
            
        } catch (Exception e) {
            String msg = "faild to find response data";
            logger.error(msg, e);
            throw new FinderException(msg, e);
        }

        if (gotRecord == false) {
            String msg = "no data found";
            logger.error(msg);
            throw new FinderException(msg);
        }
        return this;
    }
    
    public Request getRequest() throws FinderException, RemoteException {
        Request request = new Request(srk, this.requestId, this.getCopyId());
        return request;
    }
}
