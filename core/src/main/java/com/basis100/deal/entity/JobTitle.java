package com.basis100.deal.entity;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.JobTitlePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class JobTitle extends DealEntity
{

  protected int            jobTitleId;
  protected int            industrySectorId;
  protected int            occupationId;
  protected String         jobTitleDescription;


  private JobTitlePK pk;

  public JobTitle(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }

  public JobTitle(SessionResourceKit srk, int id) throws RemoteException, FinderException
  {
    super(srk);

    pk = new JobTitlePK(id);

    findByPrimaryKey(pk);
  }



  public JobTitle findByPrimaryKey(JobTitlePK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from JobTitle " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "JobTitle: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("JobTitle Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }


   public JobTitle findByName(String description) throws RemoteException, FinderException
   {
      if (description == null)
        return null;

      description = description.toUpperCase();

      // zivko 28-XI-2003  bug fix quote was open
      //String sql = "Select * from JobTitle where upper(JobTitleDescription) = '" + description ;
      String sql = "Select * from JobTitle where upper(JobTitleDescription) = '" + description + "'" ;

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               this.pk = new JobTitlePK(this.jobTitleId);
               break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "JobTitle Entity: @findByName = " + description;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          throw new FinderException("Exception while trying to find Jobtitle from description");
        }
      return this;
   }

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
       setJobTitleId(jExec.getInt(key,"JOBTITLEID"));
       setIndustrySectorId(jExec.getInt(key,"INDUSTRYSECTORID"));
       setOccupationId(jExec.getInt(key,"OCCUPATIONID"));
       setJobTitleDescription(jExec.getString(key,"JOBTITLEDESCRIPTION"));
  }

  /**
  *  gets the value of instance fields as String.
  *  if a field does not exist (or the type is not serviced)** null is returned.
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *  @returns value of bound field as a String;
  *  @param  fieldName as String
  *
  *  Other entity types are not yet serviced   ie. You can't get the Province object
  *  for province.
  */
  public String getStringValue(String fieldName)
  {
      return doGetStringValue(this, this.getClass(), fieldName);
  }

  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  @param  fieldName as String
  *  @param  the value as a String
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }


  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return the number of updates performed
  */
  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();
    int ret = doPerformUpdate(this, clazz);

    return ret;
  }

  public String getEntityTableName()
  {
    return "JobTitle";
  }



  /**
   *   gets the assetId associated with this entity
   *
   *   @return a int
   */
  public int getJobTitleId()
  {
    return this.jobTitleId ;
  }

  public int getIndustrySectorId()
  {
    return this.industrySectorId ;
  }


  /**
   *   gets the pk associated with this entity
   *
   *   @return a JobTitlePK
   */
  public IEntityBeanPK getPk()
  {
    return (IEntityBeanPK)this.pk ;
  }


  public int getOccupationId()
  {
    return this.occupationId;
  }

  public String getJobTitleDescription(){ return this.jobTitleDescription;}

  public JobTitle create()throws RemoteException, CreateException
  {
      CreateException ce = new CreateException("JobTitle Entity - JobTitle - create() exception");
      logger.error(ce.getMessage());
      logger.error(ce);

      throw ce;
  }

  public void setJobTitleId(int id)
  {
   this.testChange ("jobTitleId", id ) ;
   this.jobTitleId = id;
  }

  public void setIndustrySectorId(int id)
  {
   this.testChange ("industrySectorId", id ) ;
   this.industrySectorId = id;
  }

  public void setOccupationId(int c)
  {
   this.testChange ("occupationId", c ) ;
   this.occupationId = c;
  }


  public void setJobTitleDescription(String desc)
  {
   this.testChange ("jobTitleDescription", desc ) ;
   this.jobTitleDescription = desc;
  }


  public int getClassId()
  {
    return ClassId.DEFAULT;
  }

  public void ejbRemove() throws RemoteException
  {
     String sql = "Delete From JobTitle " + pk.getWhereClause();

      try
      {
        jExec.executeUpdate(sql);
      }
      catch (Exception e)
      {
        RemoteException re = new RemoteException("Failed to remove JobTitle record" );

        logger.error(re.getMessage());
        logger.error("remove sql: " + sql);
        logger.error(e);

        throw re;
        }

  }

  public  boolean equals( Object o )
  {
    if ( o instanceof JobTitle )
      {
        JobTitle oa = ( JobTitle ) o;
        if  (  this.jobTitleId == oa.getJobTitleId ()
               && this.industrySectorId == oa.getIndustrySectorId()
               && this.occupationId == oa.getOccupationId() )
            return true;
      }
    return false;
  }

}
