package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.IncomePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class EmploymentHistory extends DealEntity {
    protected int employmentHistoryId;
    protected int occupationId;
    protected int industrySectorId;
    protected String employerName;
    protected int borrowerId;
    protected int monthsOfService;
    protected int employmentHistoryNumber;
    protected int contactId;
    protected int employmentHistoryTypeId;
    protected int incomeId;
    protected int employmentHistoryStatusId;
    protected String jobTitle; // deprecated (to be deleted from db
    protected int copyId;
    protected int jobTitleId;
    protected int instProfileId;
    
    private EmploymentHistoryPK pk;


    public EmploymentHistory(SessionResourceKit srk) throws RemoteException,
	    FinderException {
	super(srk);
    }

    public EmploymentHistory(SessionResourceKit srk, int id, int copyId)
	    throws RemoteException, FinderException {
	super(srk);

	pk = new EmploymentHistoryPK(id, copyId);

	findByPrimaryKey(pk);
    }

    // Added by BILLY -- needed to do calc if removed (i.e. The attached
    // Income removed)
    public EmploymentHistory(SessionResourceKit srk, CalcMonitor dcm)
	    throws RemoteException, FinderException {
	super(srk, dcm);
    }

    public EmploymentHistory(SessionResourceKit srk, CalcMonitor dcm, int id,
	    int copyId) throws RemoteException, FinderException {
	super(srk, dcm);

	pk = new EmploymentHistoryPK(id, copyId);

	findByPrimaryKey(pk);
    }

    // ======================

    public EmploymentHistory findByPrimaryKey(EmploymentHistoryPK pk)
	    throws RemoteException, FinderException {
    	
    if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
	String sql = "Select * from EmploymentHistory " + pk.getWhereClause();

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
			+ ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "EmploymentHistory Entity - findByPrimaryKey() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}

	this.pk = pk;
	ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	return this;
    }

    public EmploymentHistory findByCurrentFullTimeEmployment(BorrowerPK pk)
	    throws RemoteException, FinderException {
	String sql = "Select * from EmploymentHistory " + pk.getWhereClause()
		+ " and EmploymentHistoryStatusId = "
		+ Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT
		+ " and employmenthistorytypeid = "
		+ Mc.EMPLOYMENT_HISTORY_TYPE_FULLTIME;

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
			+ ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "EmploymentHistory Entity - findByPrimaryKey() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}
	this.pk = new EmploymentHistoryPK(employmentHistoryId, copyId);
	return this;
    }

    public EmploymentHistory findByCurrentEmployment(BorrowerPK pk)
	    throws RemoteException, FinderException {
	String sql = "Select * from EmploymentHistory " + pk.getWhereClause()
		+ " and EmploymentHistoryStatusId = "
		+ Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
			+ ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "EmploymentHistory Entity - findByPrimaryKey() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}
	this.pk = new EmploymentHistoryPK(employmentHistoryId, copyId);
	return this;
    }

    public EmploymentHistory findByCurrentEmployment(IncomePK pk)
	    throws RemoteException, FinderException {
	String sql = "Select * from EmploymentHistory " + pk.getWhereClause()
		+ " and EmploymentHistoryStatusId = "
		+ Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
			+ ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "EmploymentHistory Entity - findByPrimaryKey() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}

	this.pk = new EmploymentHistoryPK(this.employmentHistoryId, this.copyId);
	return this;
    }

    public Collection<EmploymentHistory> findByBorrower(BorrowerPK pk) throws Exception {
	Collection histories = new ArrayList();
	String sql = "Select * from EmploymentHistory ";
	sql += pk.getWhereClause();
	sql += " order by employmentHistoryStatusId";
	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		// needed to do calc if removed (i.e. The attached Income
		// removed)
		EmploymentHistory eh = new EmploymentHistory(srk, dcm);
		eh.setPropertiesFromQueryResult(key);
		eh.pk = new EmploymentHistoryPK(eh.getEmploymentHistoryId(), eh
			.getCopyId());

		histories.add(eh);
	    }

	    jExec.closeData(key);

	} catch (Exception e) {
	    Exception fe = new Exception(
		    "Exception caught while fetching Borrower::EmploymentHistories");
	    logger.error(fe.getMessage());
	    logger.error(e);

	    throw fe;
	}

	return histories;
    }

    /**
         * Find EmploymentHistory records not associated with an
         * EmploymentHistory record for the given Borrower.
         */
    public Collection findByDeal(DealPK pk) throws Exception {
	Collection histories = new ArrayList();

	StringBuffer buf = new StringBuffer("Select * from EmploymentHistory ");
	buf
		.append(" where borrowerId in( Select BorrowerId from Borrower where dealId = ");
	buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
	buf.append(") and copyId = ").append(pk.getCopyId());

	try {

	    int key = jExec.execute(buf.toString());

	    for (; jExec.next(key);) {
		EmploymentHistory eh = new EmploymentHistory(srk);
		eh.setPropertiesFromQueryResult(key);
		eh.pk = new EmploymentHistoryPK(eh.getEmploymentHistoryId(), eh
			.getCopyId());

		histories.add(eh);
	    }

	    jExec.closeData(key);

	} catch (Exception e) {
	    Exception fe = new Exception(
		    "EmploymentHistory Entity - findByPrimaryKey() exception");
	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + buf.toString());
	    logger.error(e);

	    throw e;
	}

	return histories;
    }

    /**
         * Find EmploymentHistory records not associated with an
         * EmploymentHistory record for the given Borrower.
         */
    public Collection findAllCurrent(DealPK pk) throws Exception {
	Collection histories = new ArrayList();

	StringBuffer buf = new StringBuffer("Select * from EmploymentHistory ");
	buf
		.append(" where borrowerId in( Select BorrowerId from Borrower where dealId = ");
	buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
	buf.append(") and  EmploymentHistoryStatusId = ").append(
		Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT);
	buf.append(" and copyId = ").append(pk.getCopyId());

	try {

	    int key = jExec.execute(buf.toString());

	    for (; jExec.next(key);) {
		EmploymentHistory eh = new EmploymentHistory(srk);
		eh.setPropertiesFromQueryResult(key);
		eh.pk = new EmploymentHistoryPK(eh.getEmploymentHistoryId(), eh
			.getCopyId());

		histories.add(eh);
	    }

	    jExec.closeData(key);

	} catch (Exception e) {
	    Exception fe = new Exception(
		    "EmploymentHistory Entity - findAllByCurrent() exception");
	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + buf.toString());
	    logger.error(e);

	    throw e;
	}

	return histories;
    }

    public Collection findByCurrentEmployments(BorrowerPK pk) throws RemoteException, FinderException
    {
       String sql = "Select * from EmploymentHistory " + pk.getWhereClause() 
       	+ " and EmploymentHistoryStatusId = " + Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;
       return findBySqlColl(sql, null);
    }
    public Collection findBySqlColl(String sql, CalcMonitor dcm) throws FinderException {
		
		Collection histories = new ArrayList();
		try
		{
			int key = jExec.execute(sql);
			for (; jExec.next(key);) {
            EmploymentHistory eh = new EmploymentHistory(srk, dcm);
            eh.setPropertiesFromQueryResult(key);
            eh.pk = new EmploymentHistoryPK(eh.getEmploymentHistoryId(),eh.getCopyId());

            histories.add(eh);
			}
			jExec.closeData(key);			
		}
		catch(Exception e)
		{
			FinderException fe = new FinderException("Entity - findBySqlColl() exception:"+sql, e);
			if (!silentMode) 
				logger.error(fe);
			throw fe;
		}
		return histories;
	}
    
    public EmploymentHistory findByIncome(IncomePK pk) throws RemoteException,
	    FinderException {
	String sql = "Select * from EmploymentHistory " + pk.getWhereClause();

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
			+ ", entity not found";

		if (this.getSilentMode() == false)
		    logger.error(msg);

		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    if (gotRecord == false && getSilentMode() == true)
		throw (FinderException) e;

	    FinderException fe = new FinderException(
		    "EmploymentHistory Entity - findByIncome() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}
	this.pk = new EmploymentHistoryPK(this.employmentHistoryId, this.copyId);
	return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

	this.setEmploymentHistoryId(jExec.getInt(key, "EMPLOYMENTHISTORYID"));
	this.setOccupationId(jExec.getShort(key, "OCCUPATIONID"));
	this.setIndustrySectorId(jExec.getShort(key, "INDUSTRYSECTORID"));
	this.setEmployerName(jExec.getString(key, "EMPLOYERNAME"));
	this.setBorrowerId(jExec.getInt(key, "BORROWERID"));
	this.setMonthsOfService(jExec.getShort(key, "MONTHSOFSERVICE"));
	this.setEmploymentHistoryNumber(jExec.getInt(key,
		"EMPLOYMENTHISTORYNUMBER"));
	this.setContactId(jExec.getInt(key, "CONTACTID"));
	this.setEmploymentHistoryTypeId(jExec.getInt(key,
		"EMPLOYMENTHISTORYTYPEID"));
	this.setIncomeId(jExec.getInt(key, "INCOMEID"));
	this.setEmploymentHistoryStatusId(jExec.getInt(key,
		"EMPLOYMENTHISTORYSTATUSID"));
	this.setJobTitle(jExec.getString(key, "JOBTITLE"));
	this.setCopyId(jExec.getInt(key, "COPYID"));
	// Debug -- to populate JobTitleId -- by BILLY 16Jan2001
	this.setJobTitleId(jExec.getInt(key, "JOBTITLEID"));
	this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));

    }

    /**
         * gets the value of instance fields as String. if a field does not
         * exist (or the type is not serviced)** null is returned. if the field
         * exists (and the type is serviced) but the field is null an empty
         * String is returned.
         * 
         * @returns value of bound field as a String;
         * @param fieldName
         *                as String
         * 
         * Other entity types are not yet serviced ie. You can't get the
         * Province object for province.
         */
    public String getStringValue(String fieldName) {
	return doGetStringValue(this, this.getClass(), fieldName);
    }

    public Object getFieldValue(String fieldName) {
	return doGetFieldValue(this, this.getClass(), fieldName);
    }

    /**
         * sets the value of instance fields as String. if a field does not
         * exist or is not serviced an Exception is thrown if the field exists
         * (and the type is serviced) but the field is null an empty String is
         * returned.
         * 
         * @param fieldName
         *                as String
         * @param the
         *                value as a String
         * 
         * Other entity types are not yet serviced ie. You can't set the
         * Province object in Addr.
         */
    public void setField(String fieldName, String value) throws Exception {
	doSetField(this, this.getClass(), fieldName, value);
    }

    /**
         * Updates any Database field that has changed since the last
         * synchronization ie. the last findBy... call
         * 
         * @return the number of updates performed
         */
    protected int performUpdate() throws Exception {
	Class clazz = this.getClass();

	return (doPerformUpdate(this, clazz));
    }

    public List getSecondaryParents() throws Exception {
	List sps = new ArrayList();

	sps.add(getContact());

	return sps;
    }

    private EmploymentHistoryPK createPrimaryKey(int copyId)
	    throws CreateException {
	String sql = "Select EmploymentHistoryseq.nextval from dual";
	int id = -1;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		id = jExec.getInt(key, 1); // can only be one record
	    }

	    jExec.closeData(key);

	    if (id == -1)
		throw new Exception();
	} catch (Exception e) {

	    CreateException ce = new CreateException(
		    "EmploymentHistory Entity create() exception getting EmploymentHistoryId from sequence");
	    logger.error(ce.getMessage());
	    logger.error(e);
	    throw ce;

	}

	return new EmploymentHistoryPK(id, copyId);
    }

    /**
         * creates a EmploymentHistory using the EmploymentHistory primary key
         * and a borrower primary key - the mininimum (ie.non-null) fields
         * 
         */
    public EmploymentHistory create(BorrowerPK bpk) throws RemoteException,
	    CreateException {
	pk = createPrimaryKey(bpk.getCopyId());

	int contId = 0;

	try {
	    Contact c = new Contact(srk);
	    c.create(pk);
	    contId = c.getContactId();
	} catch (Exception ex) {
	    CreateException ce = new CreateException(
		    "Contact Entity in EmploymentHistory - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(ex);

	    throw ce;
	}

	// Check if the current Employment exists. If yes => default Empolyment
	// type to PREVIOUS
	// -- By BILLY 18Jan2001
	int theEmployType;
	if (isCurrentEmployment(bpk))
	    theEmployType = Mc.EMPLOYMENT_HISTORY_STATUS_PREVIOUS;
	else
	    theEmployType = Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;

	String sql = "Insert into EmploymentHistory(" + pk.getName() + ","
		+ bpk.getName()
		+ ", copyId, contactId, employmentHistoryStatusId, INSTITUTIONPROFILEID) Values ( "
		+ pk.getId() + "," + bpk.getId() + "," + pk.getCopyId() + ","
		+ contId + "," + theEmployType +"," + srk.getExpressState().getDealInstitutionId() + ")";

	try {
	    jExec.executeUpdate(sql);
	    this.findByPrimaryKey(pk);
	    this.trackEntityCreate(); // track the creation
	} catch (Exception e) {

	    CreateException ce = new CreateException(
		    "EmploymentHistory Entity - EmploymentHistory - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);

	    throw ce;
	}

	srk.setModified(true);

	if (dcm != null)
	    // dcm.inputEntity(this);
	    dcm.inputCreatedEntity(this);

	return this;
    }

    public EmploymentHistory create(IncomePK ipk) throws RemoteException,
	    CreateException {

	int contId = 0;
	int borrId = 0;
	int copyId = ipk.getCopyId();
	pk = createPrimaryKey(copyId);

	try {
	    Contact c = new Contact(srk);
	    c.create(pk);
	    contId = c.getContactId();
	} catch (Exception ex) {
	    CreateException ce = new CreateException(
		    "Contact Entity in EmploymentHistory - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(ex);

	    throw ce;
	}

	try {
	    Income inc = new Income(srk, dcm);
	    inc = inc.findByPrimaryKey(ipk);
	    borrId = inc.getBorrowerId();

	} catch (Exception ex) {
	    CreateException ce = new CreateException(
		    "Contact Entity in EmploymentHistory - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(ex);

	    throw ce;
	}

	// Check if the current Employment exists. If yes => default Empolyment
	// type to PREVIOUS
	// -- By BILLY 18Jan2001
	int theEmployType;
	if (isCurrentEmployment(new BorrowerPK(borrId, copyId)))
	    theEmployType = Mc.EMPLOYMENT_HISTORY_STATUS_PREVIOUS;
	else
	    theEmployType = Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;

	String sql = "Insert into EmploymentHistory( EmploymentHistoryId ,BorrowerId, IncomeId,"
		+ " copyId, contactId, employmentHistoryStatusId, institutionProfileId ) Values ( "
		+ pk.getId()
		+ ", "
		+ borrId
		+ ","
		+ ipk.getId()
		+ ","
		+ copyId
		+ "," + contId + "," + theEmployType + "," + srk.getExpressState().getDealInstitutionId() + ")";

	try {
	    jExec.executeUpdate(sql);
	    this.findByPrimaryKey(pk);
	    this.trackEntityCreate(); // track the creation
	} catch (Exception e) {

	    CreateException ce = new CreateException(
		    "EmploymentHistory Entity - EmploymentHistory - create() exception");
	    logger.error(ce.getMessage());
	    logger.error(e);

	    throw ce;
	}

	srk.setModified(true);

	if (dcm != null)
	    // dcm.inputEntity(this);
	    dcm.inputCreatedEntity(this);

	return this;
    }

    public String getEntityTableName() {
	return "EmploymentHistory";
    }

    public int getClassId() {
	return ClassId.EMPLOYMENTHISTORY;
    }

    /**
         * gets the contact associated with this entity
         * 
         * @return a Contact
         */
    public Contact getContact() throws FinderException, RemoteException {
	return new Contact(srk, this.getContactId(), this.getCopyId());
    }

    /**
         * gets the pk associated with this entity
         * 
         * @return a EmploymentHistoryPK
         */
    public IEntityBeanPK getPk() {
	return (IEntityBeanPK) this.pk;
    }

    public Income getIncome() throws FinderException, RemoteException {
	return new Income(srk, dcm, this.getIncomeId(), this.getCopyId());
    }

    public void setEmploymentHistoryId(int id) {
	this.testChange("employmentHistoryId", id);
	this.employmentHistoryId = id;
    }

    public int getEmploymentHistoryId() {
	return this.employmentHistoryId;
    }

    public void setOccupationId(int id) {
	this.testChange("occupationId", id);
	this.occupationId = id;
    }

    public int getOccupationId() {
	return this.occupationId;
    }

    public void setIndustrySectorId(int id) {
	this.testChange("industrySectorId", id);
	this.industrySectorId = id;
    }

    public int getIndustrySectorId() {
	return this.industrySectorId;
    }

    public void setBorrowerId(int id) {
	this.testChange("borrowerId", id);
	this.borrowerId = id;
    }

    public int getBorrowerId() {
	return this.borrowerId;
    }

    public void setEmployerName(String name) {
	this.testChange("employerName", name);
	this.employerName = name;
    }

    public String getEmployerName() {
	return this.employerName;
    }

    public void setMonthsOfService(int months) {
	this.testChange("monthsOfService", months);
	this.monthsOfService = months;
    }

    public int getMonthsOfService() {
	return this.monthsOfService;
    }

    public void setEmploymentHistoryNumber(int n) {
	this.testChange("employmentHistoryNumber", n);
	this.employmentHistoryNumber = n;
    }

    public int getEmploymentHistoryNumber() {
	return this.employmentHistoryNumber;
    }

    public void setEmploymentHistoryTypeId(int id) {
	// Added testChange -- by BILLY 18Jan2001
	this.testChange("employmentHistoryTypeId", id);
	this.employmentHistoryTypeId = id;
    }

    public int getEmploymentHistoryTypeId() {
	return this.employmentHistoryTypeId;
    }

    public void setContactId(int id) {
	this.testChange("contactId", id);
	this.contactId = id;
    }

    public int getContactId() {
	return this.contactId;
    }

    public void setIncomeId(int id) {
	this.testChange("incomeId", id);
	this.incomeId = id;
    }

    public int getIncomeId() {
	return this.incomeId;
    }

    public void setCopyId(int id) {
	this.testChange("copyId", id);
	this.copyId = id;
    }

    public int getCopyId() {
	return this.copyId;
    }

    public void setEmploymentHistoryStatusId(int id) {
	// Added testChange -- by BILLY 18Jan2001
	this.testChange("employmentHistoryStatusId", id);
	this.employmentHistoryStatusId = id;
    }

    public int getEmploymentHistoryStatusId() {
	return this.employmentHistoryStatusId;
    }

    public void setJobTitle(String jobTitle) {
	this.testChange("jobTitle", jobTitle);
	this.jobTitle = jobTitle;
    }

    public String getJobTitle() {
	return this.jobTitle;
    }

    public void setJobTitleId(int job) {
	this.testChange("jobTitleId", job);
	this.jobTitleId = job;
    }

    public int getJobTitleId() {
	return this.jobTitleId;
    }

    public Vector findByMyCopies() throws RemoteException, FinderException {
	Vector v = new Vector();

	String sql = "Select * from EmploymentHistory where EmploymentHistoryId = "
		+ getEmploymentHistoryId() + " AND copyId <> " + getCopyId();

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		EmploymentHistory iCpy = new EmploymentHistory(srk);

		iCpy.setPropertiesFromQueryResult(key);
		iCpy.pk = new EmploymentHistoryPK(
			iCpy.getEmploymentHistoryId(), iCpy.getCopyId());

		v.addElement(iCpy);
	    }

	    jExec.closeData(key);
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "EmploymentHistory - findByMyCopies exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}

	return v;
    }

    public boolean isAuditable() {
	return true;
    }

    public boolean equals(Object o) {
	if (o instanceof EmploymentHistory) {
	    EmploymentHistory oa = (EmploymentHistory) o;
	    if ((this.employmentHistoryId == oa.getEmploymentHistoryId())
		    && (this.copyId == oa.getCopyId()))
		return true;
	}
	return false;
    }

    public void ejbRemove() throws RemoteException {
	ejbRemove(true);
    }

    public void ejbRemove(boolean calc) throws RemoteException {

	try {
	    Income relatedIncome = null;
	    try {
		relatedIncome = this.getIncome();
	    } catch (Exception e) {
		logger.info("EmploymentHistory " + this.pk
			+ " - ejbRemove- no related Income found.");
		relatedIncome = null;
	    }

	    String sql = "Delete From EmploymentHistory "
		    + this.pk.getWhereClause();
	    jExec.executeUpdate(sql);

	    Contact cont = null;
	    try {
		cont = this.getContact();
	    } catch (Exception e) {
		logger.info("EmploymentHistory " + this.pk
			+ " - ejbRemove - noContact found.");
		cont = null;
	    }

	    if (cont != null) {
		try {
		    cont.ejbRemove(); // remove address should not trigger
		    // calculation
		} catch (Exception ex) {
		    String msg = ex.getMessage();

		    if (msg == null)
			msg = " Exception has null message: "
				+ ex.getClass().getName();

		    msg = "EmploymentHistory " + this.pk
			    + " - ejbRemove - Contact found "
			    + " but couldn't be removed.";

		    logger.error(msg);
		    logger.error(ex);

		    throw new RemoteException(msg);
		}

	    }

	    if (relatedIncome != null) {
		try {
		    relatedIncome.ejbRemove(calc);
		} catch (Exception ex) {
		    String msg = ex.getMessage();

		    if (msg == null)
			msg = " Exception has null message: "
				+ ex.getClass().getName();

		    msg = "EmploymentHistory " + this.pk
			    + " - ejbRemove - related Income found "
			    + " but couldn't be removed.";

		    logger.error(msg);
		    logger.error(ex);

		    throw new RemoteException(msg);
		}
	    }
	} catch (Exception e) {
	    String msg = "Exception removing EmploymentHistory record: "
		    + this.pk;

	    logger.error(msg);
	    logger.error(e);

	    throw new RemoteException(msg);
	}

	    //4.4 Entity Cache
	    ThreadLocalEntityCache.removeFromCache(this, pk);
    
    }

    protected EntityContext getEntityContext() throws RemoteException {
	EntityContext ctx = this.entityContext;

	try {
	    ctx.setContextText(this.getEmployerName());
	    Borrower borrower = new Borrower(srk, null);
	    borrower.findByPrimaryKey(new BorrowerPK(this.getBorrowerId(), this
		    .getCopyId()));
	    if (borrower == null)
		return ctx;
	    if ((borrower.getBorrowerFirstName() != null && borrower.getBorrowerFirstName().length() >= 1)
		    && borrower.getBorrowerLastName() != null)
		ctx.setContextSource(borrower.getBorrowerFirstName().substring(
			0, 1)
			+ " " + borrower.getBorrowerLastName());

	    // Modified by Billy for performance tuning -- By BILLY
	    // 28Jan2002
	    // Deal deal = new Deal( srk, null);
	    // deal = deal.findByPrimaryKey( new DealPK( borrower.getDealId
	    // () , borrower.getCopyId () ) ) ;
	    // if ( deal == null )
	    // return ctx;
	    // String sc = String.valueOf ( deal.getScenarioNumber () ) +
	    // deal.getCopyType();
	    Deal deal = new Deal(srk, null);
	    String sc = String.valueOf(deal.getScenarioNumber(borrower
		    .getDealId(), borrower.getCopyId()))
		    + deal.getCopyType(borrower.getDealId(), borrower
			    .getCopyId());
	    // ===================================================================================

	    ctx.setScenarioContext(sc);

	    ctx.setApplicationId(borrower.getDealId());

	} catch (Exception e) {
	    if (e instanceof FinderException) {
		logger
			.error("EmploymentHistory.getEntityContext Parent Not Found. EmploymentHistoryId="
				+ this.employmentHistoryId);
		return ctx;
	    } else
		throw (RemoteException) e;
	}// =---= end of try .. catch ....

	return ctx;
    } // --------- End of getEntityContext ---------------------------

    // New method to check if the Current Employment exist -- By BILLY
    // 18Jan2001
    public boolean isCurrentEmployment(BorrowerPK pk) {
	String sql = "Select * from EmploymentHistory " + pk.getWhereClause()
		+ " and EmploymentHistoryStatusId = "
		+ Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		break; // can only be one record
	    }

	    jExec.closeData(key);

	} catch (Exception e) {
	    gotRecord = true;
	}

	return gotRecord;
    }
    
	public int getInstProfileId() {
		return instProfileId;
	}

    public void setInstProfileId(int institutionProfileId) {
    	this.testChange("institutionProfileId", institutionProfileId);
    	this.instProfileId = institutionProfileId;
	}

}
