/*
 * ServiceProvider.java
 * 
 * ServiceProvider Entity Class (Connectivity to AVM services)
 * 
 * created: 06-FEB-2006
 * author:  Edward Steel
 */
package com.basis100.deal.entity;

import java.util.Date;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.ServiceProviderPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


/**
 * ServiceProvider entity class.
 * 
 * @author ESteel
 */
public class ServiceProvider extends DealEntity {
	
  protected int     serviceProviderId;
  protected int			copyId;

  protected String  serviceProviderName;
  protected Date    startDate;
  protected Date    endDate;
  
  protected int     contactId;
  protected String  receivingPartyCode;   // Catherine: 20-Mar-06 
    protected int         institutionProfileId;
  
  private ServiceProviderPK pk;

	/**
	 * Constructor. Creates a blank entity.
	 * @param srk SessionResourceKit to use.
	 * @throws RemoteException
	 */
  public ServiceProvider(SessionResourceKit srk) throws RemoteException {
		super(srk);
	}
	
  /**
   * Constructor that populates entity. Creates an entity populated by 
   * information from the database.
   * @param srk SessionResourceKit to use.
   * @param id sessionResourceId from which to find the information in the
   * database table.
   * 
   * @throws RemoteException
   * @throws FinderException
   */
	public ServiceProvider(SessionResourceKit srk, int id) 
			throws RemoteException, FinderException {
		super(srk);
		
		pk = new ServiceProviderPK(id);
		findByPrimaryKey(pk);
	}

	/**
	 * Tests whether this ServiceProvider is equal to the given (ServiceProvider)
	 * object.
	 * @param obj The Object to test for equality.
	 */
	public boolean equals(Object obj) {
		if (obj instanceof ServiceProvider) {
	    ServiceProvider otherObj = (ServiceProvider) obj;
	    if  (this.serviceProviderId == otherObj.getServiceProviderId()) {
	        return true;
	    }
	  }
		return false;  	
	}

	/**
	 * Populates entity from database.
	 * @param pk Primary Key for the relevant row in the ServiceProvider table. 
	 * @return This (populated) <code>ServiceProvider</code>.
	 * @throws FinderException
	 */
	public ServiceProvider findByPrimaryKey(ServiceProviderPK pk) 
			throws FinderException {  	
		
    if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
  	String sql = "SELECT * FROM SERVICEPROVIDER" + pk.getWhereClause();

    boolean gotRecord = false;

    try {
      int key = jExec.execute(sql);

      for (; jExec.next(key);) {
        gotRecord = true;
        this.pk = pk;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "ServiceProvider Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("ServiceProvider Entity - findByPrimaryKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
	ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }
	
	/**
	 * Returns the name of this entity.
	 * @return Name of Entity.
	 */
	public String getEntityTableName() {
		return "ServiceProvider";
	}
	
	/**
	 * @return Returns this ServiceProvider's primary key (may be null).
	 */
	public IEntityBeanPK getPk() {
		return pk;
	}
	
	/**
	 *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName) {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  
  private void setPropertiesFromQueryResult(int key) throws Exception{
    setServiceProviderId(jExec.getInt(key, "SERVICEPROVIDERID"));
    setCopyId(jExec.getInt(key, "COPYID"));
    setContactId(jExec.getInt(key, "CONTACTID"));
    setServiceProviderName(jExec.getString(key, "SERVICEPROVIDERNAME"));
    setStartDate(jExec.getDate(key, "STARTDATE"));
    setEndDate(jExec.getDate(key, "ENDDATE"));
    setReceivingPartyCode(jExec.getString(key, "RECEIVINGPARTYCODE")); // Catherine: 20-Mar-06 
    this.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
  }
	
	//-------START: getters and setters-------------------------------------------	
	/**
	 * @return Returns the contact ID.
	 */
	public int getContactId() {
		return contactId;
	}
	
	/**
	 * @return Returns the copy ID.
	 */
	public int getCopyId() {
		return copyId;
	}

	/**
	 * @return Returns the end date.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @return Returns the service provider ID.
	 */
	public int getServiceProviderId() {
		return serviceProviderId;
	}

	/**
	 * @return Returns the service provider name.
	 */
	public String getServiceProviderName() {
		return serviceProviderName;
	}

	/**
	 * @return Returns the start date.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param contactId The contact ID to set.
	 */
	public void setContactId(int contactId) {
		testChange("contactId", contactId);
		this.contactId = contactId;
	}
	
	/**
	 * @param copyId The copy ID to set.
	 */
	public void setCopyId(int copyId) {
		testChange("copyId", copyId);
		this.copyId = copyId;
	}

	/**
	 * @param endDate The end date to set.
	 */
	public void setEndDate(Date endDate) {
		testChange("endDate", endDate);
		this.endDate = endDate;
	}

	/**
	 * @param serviceProviderId The service provider ID to set.
	 */
	public void setServiceProviderId(int serviceProviderId) {
		testChange("serviceProviderId", serviceProviderId);
		this.serviceProviderId = serviceProviderId;
	}

	/**
	 * @param serviceProviderName The service provider name to set.
	 */
	public void setServiceProviderName(String serviceProviderName) {
		testChange("serviceProviderName", serviceProviderName);
		this.serviceProviderName = serviceProviderName;
	}

	/**
	 * @param startDate The start date to set.
	 */
	public void setStartDate(Date startDate) {
		testChange("startDate", startDate);
		this.startDate = startDate;
	}	
	//--------END: getters and setters--------------------------------------------

  // Catherine: 20-Mar-06 --------------------- begin --------------------
  public String getReceivingPartyCode() {
    return receivingPartyCode;
  }

  public void setReceivingPartyCode(String receivingPartyCode) {
    testChange("receivingPartyCode", receivingPartyCode);
    this.receivingPartyCode = receivingPartyCode;
  }
  // Catherine: 20-Mar-06 --------------------- begin --------------------

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    
    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}
