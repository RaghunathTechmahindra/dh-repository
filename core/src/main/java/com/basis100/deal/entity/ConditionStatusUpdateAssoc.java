package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ConditionStatusUpdateAssocPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * Corresponds to the ConditionStatusUpdateAssoc DB table
 * 
 * @author Albert.Tse
 *
 */
public class ConditionStatusUpdateAssoc extends DealEntity {
    
    protected int    systemTypeId;
    protected int    documentStatusId;
    protected int    institutionProfileId;
    
    private ConditionStatusUpdateAssocPK pk;
    
    /**
     * @param srk SessionResourceKit to use
     * @throws RemoteException
     */
    
    public ConditionStatusUpdateAssoc (SessionResourceKit srk) 
    	throws RemoteException {
    	
        super(srk);
    }
    
    /**
     * Find the row corresponding to this PK and populate entity
     */
    public ConditionStatusUpdateAssoc findByPrimaryKey (ConditionStatusUpdateAssocPK pk) 
    	throws RemoteException, FinderException {
    	
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "SELECT * FROM CONDITIONSTATUSUPDATEASSOC" + pk.getWhereClause();
        boolean gotRecord = false;
        
        try {
            int key = jExec.execute(sql);
            for(; jExec.next(key); ) {
                gotRecord = true;
                this.pk = pk;
                setPropertiesFromQueryResult (key);
                break;
            }
            
            jExec.closeData(key);
            
            if(!gotRecord) {
                String msg = "ConditionStatusUpdateAssoc Entity: @findByQuery(), key= " + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch(Exception e) {
            FinderException fe = new FinderException("ConditionStatusUpdateAssoc Entity - findByQuery() exception");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            
            throw fe;
        }
        
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }
    
    /**
     * Find the row corresponding to the SystemType and populate entity
     */
    public Collection<ConditionStatusUpdateAssoc> findBySystemType (int systemtypeid) 
    	throws RemoteException, FinderException {
    	
        Collection<ConditionStatusUpdateAssoc> resultSet = new ArrayList<ConditionStatusUpdateAssoc>();
        
        String sql = "SELECT * FROM CONDITIONSTATUSUPDATEASSOC" ;
        sql += " WHERE SYSTEMTYPEID = " + systemtypeid  ;
        
        try {
            int key = jExec.execute(sql);
            for(; jExec.next(key);) {
                ConditionStatusUpdateAssoc result = new ConditionStatusUpdateAssoc(srk);
                result.setPropertiesFromQueryResult(key);
                result.pk = new ConditionStatusUpdateAssocPK(result.getDocumentStatusId(), result.getSystemTypeId());
                resultSet.add(result);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException("ConditionStatusUpdateAssoc Entity - findByQuery() exception");
            logger.error(fe.getMessage());            
            logger.error(e);
            
            throw fe;
        }
        return resultSet;
    }
    
    /**
     * Return the SystemTypeId property
     */
    public int getSystemTypeId() {
        return systemTypeId;
    }
    
    /**
     * Set SystemTypeId property
     * @param systemTypeId
     */
    public void setSystemTypeId(int systemTypeId) {
        this.systemTypeId = systemTypeId;
    }
    
    /**
     * Return the DocumentStatusId property
     */
    public int getDocumentStatusId() {
        return documentStatusId;
    }    
    
    /**
     * Set DocumentStatusId property
     * @param documentStatusId
     */
    public void setDocumentStatusId(int documentStatusId) {
        this.documentStatusId = documentStatusId;
    }
    
    /**
     * Return the institutionProfileId property
     * @return
     */
    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    
    /**
     * Set the institutionProfileId property
     * @param institutionProfileId
     */
    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }
    
    private void setPropertiesFromQueryResult(int key) throws Exception {
        setSystemTypeId(jExec.getInt(key, "systemTypeId"));
        setDocumentStatusId(jExec.getInt(key, "documentStatusId"));
        setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
    }
    

    /**
     * Find the row corresponding to the SystemType and populate entity
     */
    public Collection<Integer> finddocumentStatusIdBySystemType (int systemtypeid) 
        throws RemoteException, FinderException {
        
        Collection<Integer> resultSet = new ArrayList<Integer>();
        
        String sql = "SELECT documentStatusId FROM CONDITIONSTATUSUPDATEASSOC" ;
        sql += " WHERE SYSTEMTYPEID = " + systemtypeid  ;
        
        try {
            int key = jExec.execute(sql);
            for(; jExec.next(key);) {
                resultSet.add(new Integer(jExec.getInt(key, "documentStatusId")));
            }
        } catch (Exception e) {
            FinderException fe = new FinderException("finddocumentStatusIdBySystemType Entity - findByQuery() exception");
            logger.error(fe.getMessage());            
            logger.error(e);
            
            throw fe;
        }
        return resultSet;
    }

}
