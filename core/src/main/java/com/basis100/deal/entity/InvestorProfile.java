package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.InvestorProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;


public class InvestorProfile extends DealEntity
{
   protected int     investorProfileId;
   protected String  investorProfileName;
   protected char    defaultInvestor;
   protected int     profileStatusId;
   protected int     contactId;
   protected String  IPShortName;
   protected String  IPBusinessId;
   protected int     institutionProfileId;

   private InvestorProfilePK pk;

   public InvestorProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public InvestorProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new InvestorProfilePK(id);

      findByPrimaryKey(pk);
   }

    public InvestorProfile findByPrimaryKey(InvestorProfilePK pk) throws RemoteException, FinderException
   {
    	
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from InvestorProfile where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

 
   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setInvestorProfileId(jExec.getInt(key,"INVESTORPROFILEID"));
      this.setInvestorProfileName(jExec.getString(key,"INVESTORPROFILENAME"));
      this.setDefaultInvestor(jExec.getString(key,"DEFAULTINVESTOR"));
      this.setProfileStatusId(jExec.getInt(key,"PROFILESTATUSID"));
      this.setContactId(jExec.getInt(key,"CONTACTID"));
      this.setIPShortName(jExec.getString(key,"IPSHORTNAME"));
      this.setIPBusinessId(jExec.getString(key,"IPBUSINESSID"));
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));

   }

  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

  public List getSecondaryParents() throws Exception
  {
    List sps = new ArrayList();

    Object o = getContact();

    if(o != null)
    sps.add(o);

    return sps;
  }

   public String getEntityTableName()
   {
     return "InvestorProfile"; 
   }                        	

		/**
     *   gets the profileStatusId associated with this entity
     *
     */
    public int getProfileStatusId()
   {
      return this.profileStatusId ;
   }

		/**
     *   gets the contact associated with this entity
     *
     *   @return a Contact
     */
    public Contact getContact()throws FinderException, RemoteException
   {
      return new Contact(srk,contactId,1);
   }

		/**
     *   gets the investorProfileName associated with this entity
     *
     *   @return a String
     */
    public String getInvestorName()
   {
      return this.investorProfileName ;
   }

		/**
     *   gets the defaultInvestor associated with this entity
     *
     *   @return a String
     */
    public char getDefaultInvestor()
   {
      return this.defaultInvestor ;
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a InvestorProfilePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

		/**
     *   gets the investorProfileId associated with this entity
     *
     *   @return a int
     */
    public int getInvestorProfileId()
   {
      return this.investorProfileId ;
   }

	/**
   *   gets the contactId associated with this entity
   *
   *   @return a int
   */
   public int getContactId()
   {
      return this.contactId ;
   }

   public String getIPShortName(){ return this.IPShortName;}
   public String getIPBusinessId(){ return this.IPBusinessId;}

   public InvestorProfilePK createPrimaryKey()throws CreateException
   {
       String sql = "Select InvestorProfileseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           
           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("InvestorProfile Entity create() exception getting InvestorProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new InvestorProfilePK(id);
  }



  /**
   * creates a InvestorProfile using the InvestorProfileId with mininimum fields
   *
   */

  public InvestorProfile create(InvestorProfilePK pk)throws RemoteException, CreateException
  {
        String sql = "Insert into InvestorProfile(" + pk.getName() + ",INSTITUTIONPROFILEID ) Values ( " + pk.getId() +","+srk.getExpressState().getDealInstitutionId()+ ")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("InvestorProfile Entity - InvestorProfile - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }
     srk.setModified(true);
     return this;
  }

   public void setInvestorProfileId(int value)
	{
		 this.testChange("investorProfileId", value);
		 this.investorProfileId = value;
  }
   public void setDefaultInvestor(String value)
	{
		 this.testChange("defaultInvestor", value);

     if(value != null && value.length()==1)
		    this.defaultInvestor = value.charAt(0);
  }
   public void setInvestorProfileName(String value )
	{
		 this.testChange("investorProfileName", value);
		 this.investorProfileName = value;
  }
   public void setContactId(int value )
	{
		 this.testChange("contactId", value);
		 this.contactId = value;
  }

  public void setProfileStatusId(int value )
	{
	  this.testChange("profileStatusId", value);
	  this.profileStatusId = value;
  }

  public void setIPShortName(String value)
	{
	 this.testChange("IPShortName", value);
	 this.IPShortName = value;
  }

   public void setIPBusinessId(String value)
	 {
		 this.testChange("IPBusinessId", value);
		 this.IPBusinessId = value;
   }


public int getInstitutionProfileId() {
    return institutionProfileId;
}


public void setInstitutionProfileId(int institutionProfileId) {
    this.testChange("institutionProfileId", institutionProfileId);
    this.institutionProfileId = institutionProfileId;
}

}
