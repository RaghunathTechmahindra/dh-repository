package com.basis100.deal.entity;

public final class DealCopyException extends Exception
{
	private static final long serialVersionUID = 1L;
	
    public DealCopyException()
    {
        super();
    }

    public DealCopyException(String message)
    {
        super(message);
    }

	public DealCopyException(Throwable cause) {
		super(cause);
	}

	public DealCopyException(String message, Throwable cause) {
		super(message, cause);
	}
}