package com.basis100.deal.entity.simplecache;

import java.util.Set;

public class SimpleEntityCacheConfig {

	private Set<String> targetEntities;

	public Set<String> getTargetEntities() {
		return targetEntities;
	}

	public void setTargetEntities(Set<String> targetEntities) {
		this.targetEntities = targetEntities;
	}

}
