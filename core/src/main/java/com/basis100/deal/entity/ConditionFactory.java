package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.FilogixCache;

/**
 * 
 * @modified Maida
 * @date July 29, 2009
 * @description: added institutionPrefileId as cache name
 */
public class ConditionFactory {
	private static ConditionFactory instance = null;
	private static Log logger = LogFactory.getLog(ConditionFactory.class);

	private ConditionFactory() {

	}

	public static ConditionFactory getInstance() {
		if (null == instance) {
			synchronized (ConditionFactory.class) {
				instance = new ConditionFactory();
				logger.debug("New instance for Condition Factory");
			}
		}
		return instance;
	}

	public Condition getCondition(SessionResourceKit srk, int id) throws RemoteException,
			FinderException {
		Condition conditionToReturn = null;
		// TODO REMOVE this
		// get the cache manager
		FilogixCache cache = FilogixCache.getInstance();
		// try to retrieve from the cache
		//added institutionProfileId for 4.2GR
		Condition currentConditionEntity = (Condition) cache.get(id, 
				Condition.class.getName() + "_" + srk.getExpressState().getDealInstitutionId());
		if (null == currentConditionEntity) {
			// its not yet been cached so get it from the database and then
			// cache it
			conditionToReturn = new Condition(srk, id);
			logger.debug("Fetching from Database conditionId --> " + id);
			if (null == conditionToReturn) {
				return null;
			} else {
				// unknown if storing the SRK Object could cause DB issues so
				// setting it to null
				conditionToReturn.setSessionResourceKit(null);
				// now cache it in the FilogixCache
				cache.put(id, conditionToReturn, 
						Condition.class.getName() + "_" + srk.getExpressState().getDealInstitutionId());
				// will be returned at the end of the method.
			}
		} else {
			// it is cached so just set the cached condition to current
			logger.debug("Found in the Cache conditionId --> " + id);
			conditionToReturn = (Condition) currentConditionEntity.clone();
		}
		// do a passthrough with the SRK.
		conditionToReturn.setSessionResourceKit(srk);
		return conditionToReturn;
	}

}
