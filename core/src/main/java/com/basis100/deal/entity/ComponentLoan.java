/*
 * @(#)ComponentLoan.java Apr 30, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.entity;


import java.util.Date;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
/**
 * <p>
 * Title: ComponentLan
 * </p>
 * <p>
 * Description: This is an entity class representing ComponentLoan. This class
 * provides accessor and mutator methods for all the entity attributes. This
 * class will map the entity and database fields. This class will save the
 * appropriate data into the ComponentLoan Table.
 * </p>
 * @version 1.0 Initial Version
 * @version 1.1 XS_11.5 25-Jun-2008 Added  constructor's 
 * @version 1.2 XS_2.39 10 July-2008 fixed PandI setter typo
 * @version 1.3 XS_11.13 10-July-2008 Added pAndIPaymentAmountMonthly variable and respective setter ,getter methods.
 * @version 1.4 XS_2.26 14-Jul-2008 added create(int componentId, int copyId) method
 * @version 1.5 XS_2.26 14-Jul-2008 deleted create(int componentId, int copyId,int paymentFrequencyId) 
 * 									as paymentFrequencyId is defaulted in the DB 
 */
public class ComponentLoan extends DealEntity
{
    protected int componentId;

    protected int copyId;

    protected int institutionProfileId;

    protected int actualPaymentTerm;

    protected double discount;

    protected Date firstPaymentDate;

    protected int iadNumberOfDays;

    protected double interestAdjustmentAmount;

    protected Date interestAdjustmentDate;

    protected double loanAmount;

    protected Date maturityDate;

    protected double netInterestRate;

    protected double pAndIPaymentAmount;

    protected int paymentFrequencyId;

    protected double perDiemInterestAmount;

    protected double premium;
    
    //V1.3 10-July-2008 XS_11.13
    protected double pAndIPaymentAmountMonthly;
    //V1.3 10-July-2008 XS_11.13

    protected ComponentLoanPK pk;
   
    /**
     * <p>
     * Description:Default constructor.
     * </p>
     * @version 1.0 Initial Version
     */
    public ComponentLoan()
    {

    }
    
    /**
     * @param srk
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentLoan(SessionResourceKit srk) throws RemoteException,
            FinderException
    {
        super(srk);
    }

    /**
     * <p>
     * Description: Creates the ComponentLoan entity object by taking session
     * resource kit and calc monitor objects, component id and the copy id of
     * the existing Component.Calls the super class constructor and creates the
     * primary key for the ComponentLoan entity and calls the findByPrimaryKey()
     * method.
     * </p>
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @param srk
     *            stores the session level information.
     * @param dcm
     *            runs the calculation of this ComponentLoan entity.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     * @see com.basis100.deal.entity.ComponentLoan#findByPrimaryKey(com.basis100.deal.pk.ComponentLoanPK )
     */
    public ComponentLoan(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException
    {
        super(srk, dcm);
    }

    /**
     * @param srk
     * @param id
     * @param copyId
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentLoan(SessionResourceKit srk, int id, int copyId)
            throws RemoteException, FinderException
    {
        super(srk);

        pk = new ComponentLoanPK(id, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * <p>
     * Description: Creates the ComponentLoan entity object by taking session
     * resource kit and calc monitor objects, component id and the copy id of
     * the existing Component.Calls the super class constructor and creates the
     * primary key for the ComponentLoan entity and calls the findByPrimaryKey()
     * method.
     * </p>
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @param srk
     *            stores the session level information.
     * @param componentId
     *            component id value passed to the entity PK class.
     * @param dcm
     *            runs the calculation of this ComponentLoan entity.
     * @param copyId
     *            copy id of the Component.
     * @throws RemoteException
     *             exception that may occur during the execution of a remote
     *             method call.
     * @throws FinderException
     *             exception that may occurs if the entity is not in exist.
     * @see com.basis100.deal.entity.ComponentLoan#findByPrimaryKey(com.basis100.deal.pk.ComponentLoanPK )
     */
    public ComponentLoan(SessionResourceKit srk, CalcMonitor dcm,
            int componentId, int copyId) throws RemoteException,
            FinderException
    {
        super(srk, dcm);

        pk = new ComponentLoanPK(componentId, copyId);

        findByPrimaryKey(pk);
    }

    /**
     * @param key
     * @throws Exception
     * @version 1.0 Initial version
     * @version 1.1 Added 'PANDIPAYMENTAMOUNTMONTHLY'
     */
    private void setPropertiesFromQueryResult(int key) throws Exception
    {
        this.setComponentId(jExec.getInt(key, "COMPONENTID"));
        this.copyId = jExec.getInt(key, "COPYID");
        this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        this.setActualPaymentTerm(jExec.getInt(key, "ACTUALPAYMENTTERM"));
        this.setDiscount(jExec.getDouble(key, "DISCOUNT"));
        this.setFirstPaymentDate(jExec.getDate(key, "FIRSTPAYMENTDATE"));
        this.setIadNumberOfDays(jExec.getInt(key, "IADNUMBEROFDAYS"));
        this.setInterestAdjustmentAmount(jExec.getDouble(key,
                "INTERESTADJUSTMENTAMOUNT"));
        this.setInterestAdjustmentDate(jExec.getDate(key,
                "INTERESTADJUSTMENTDATE"));
        this.setLoanAmount(jExec.getDouble(key, "LOANAMOUNT"));
        this.setMaturityDate(jExec.getDate(key, "MATURITYDATE"));
        this.setNetInterestRate(jExec.getDouble(key, "NETINTERESTRATE"));
        this.setPAndIPaymentAmount(jExec.getDouble(key, "PANDIPAYMENTAMOUNT"));
        this.setPaymentFrequencyId(jExec.getInt(key, "PAYMENTFREQUENCYID"));
        this.setPerDiemInterestAmount(jExec.getDouble(key,
                "PERDIEMINTERESTAMOUNT"));
        this.setPremium(jExec.getDouble(key, "PREMIUM"));
        //V1.1 0-July-2008 XS_11.13
        this.setPAndIPaymentAmountMonthly(jExec.getDouble(key,
        "PANDIPAYMENTAMOUNTMONTHLY"));
        //V1.1 10-July-2008 XS_11.13
    }

    /**
     * @param pk
     * @return
     * @throws RemoteException
     * @throws FinderException
     */
    public ComponentLoan findByPrimaryKey(ComponentLoanPK pk)
            throws RemoteException, FinderException
    {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from ComponentLoan where componentId = "
                + pk.getId() + " AND copyId = " + pk.getCopyId();

        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "ComponentLoan Entity: @findByPrimaryKey(), key= "
                        + pk + ", entity not found";
                if(getSilentMode() == false)
                    logger.error(msg);
                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException(
                    "ComponentLoan Entity - findByPrimaryKey() exception");

            if(getSilentMode() == false){
                logger.error(fe.getMessage());
                logger.error("finder sql: " + sql);
                logger.error(e);
            }
            throw fe;
        }
        this.copyId = pk.getCopyId();
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * @param copyId
     * @return
     * @throws CreateException
     */
    private ComponentPK createPrimaryKey(int copyId) throws CreateException
    {

        String sql = "Select ComponentLoanseq.nextval from dual";
        int id = -1;

        try
        {
            int key = jExec.execute(sql);

            for ( ; jExec.next(key); )
            {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);
            if (id == -1)
                throw new Exception();
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "ComponentLoan Entity create() exception getting ContactId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }

        return new ComponentPK(id, copyId);
    }

    /**
     * @param componentId
     * @param copyId
     * @return ComponentLoan
     * @throws RemoteException
     * @throws CreateException
     */
    public ComponentLoan create(int componentId, int copyId) throws RemoteException, CreateException
    {

        pk = new ComponentLoanPK(componentId, copyId);
        // SQL
        String sql = "Insert into ComponentLoan( componentId, copyId, institutionProfileId) Values ( "
                + componentId
                + ","
                + copyId
                + ","
                + srk.getExpressState().getDealInstitutionId()+ ")";

        // Execute
        try
        {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the create
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException(
                    "ComponentLoan Entity - Component - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        return this;
    }

    /**
     * @return the componentId
     */
    public int getComponentId()
    {
        return componentId;
    }

    /**
     * @param componentId
     *            the componentId to set
     */
    public void setComponentId(int componentId)
    {
        this.testChange("componentId", componentId);
        this.componentId = componentId;
    }

    /**
     * @return the copyId
     */
    public int getCopyId()
    {
        return copyId;
    }

    /**
     * @param copyId
     *            the copyId to set
     */
    public void setCopyId(int copyId)
    {
        this.testChange("copyId", copyId);
        this.copyId = copyId;
    }

    /**
     * @return the institutionProfileId
     */
    public int getInstitutionProfileId()
    {
        return institutionProfileId;
    }

    /**
     * @param institutionProfileId
     *            the institutionProfileId to set
     */
    public void setInstitutionProfileId(int institutionProfileId)
    {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    /**
     * @return the actualPaymentTerm
     */
    public int getActualPaymentTerm()
    {
        return actualPaymentTerm;
    }

    /**
     * @param actualPaymentTerm
     *            the actualPaymentTerm to set
     */
    public void setActualPaymentTerm(int actualPaymentTerm)
    {
        this.testChange("actualPaymentTerm", actualPaymentTerm);
        this.actualPaymentTerm = actualPaymentTerm;
    }

    /**
     * @return the discount
     */
    public double getDiscount()
    {
        return discount;
    }

    /**
     * @param discount
     *            the discount to set
     */
    public void setDiscount(double discount)
    {
        this.testChange("discount", discount);
        this.discount = discount;
    }

    /**
     * @return the firstPaymentDate
     */
    public Date getFirstPaymentDate()
    {
        return firstPaymentDate;
    }

    /**
     * @param firstPaymentDate
     *            the firstPaymentDate to set
     */
    public void setFirstPaymentDate(Date firstPaymentDate)
    {
        this.testChange("firstPaymentDate", firstPaymentDate);
        this.firstPaymentDate = firstPaymentDate;
    }

    /**
     * @return the iadNumberOfDays
     */
    public int getIadNumberOfDays()
    {
        return iadNumberOfDays;
    }

    /**
     * @param iadNumberOfDays
     *            the iadNumberOfDays to set
     */
    public void setIadNumberOfDays(int iadNumberOfDays)
    {
        this.testChange("iadNumberOfDays", iadNumberOfDays);
        this.iadNumberOfDays = iadNumberOfDays;
    }

    /**
     * @return the interestAdjustmentAmount
     */
    public double getInterestAdjustmentAmount()
    {
        return interestAdjustmentAmount;
    }

    /**
     * @param interestAdjustmentAmount
     *            the interestAdjustmentAmount to set
     */
    public void setInterestAdjustmentAmount(double interestAdjustmentAmount)
    {
        this.testChange("interestAdjustmentAmount", interestAdjustmentAmount);
        this.interestAdjustmentAmount = interestAdjustmentAmount;
    }

    /**
     * @return the interestAdjustmentDate
     */
    public Date getInterestAdjustmentDate()
    {
        return interestAdjustmentDate;
    }

    /**
     * @param interestAdjustmentDate
     *            the interestAdjustmentDate to set
     */
    public void setInterestAdjustmentDate(Date interestAdjustmentDate)
    {
        this.testChange("interestAdjustmentDate", interestAdjustmentDate);
        this.interestAdjustmentDate = interestAdjustmentDate;
    }

    /**
     * @return the loanAmount
     */
    public double getLoanAmount()
    {
        return loanAmount;
    }

    /**
     * @param loanAmount
     *            the loanAmount to set
     */
    public void setLoanAmount(double loanAmount)
    {
        this.testChange("loanAmount", loanAmount);
        this.loanAmount = loanAmount;
    }

    /**
     * @return the maturityDate
     */
    public Date getMaturityDate()
    {
        return maturityDate;
    }

    /**
     * @param maturityDate
     *            the maturityDate to set
     */
    public void setMaturityDate(Date maturityDate)
    {
        this.testChange("maturityDate", maturityDate);
        this.maturityDate = maturityDate;
    }

    /**
     * @return the netInterestRate
     */
    public double getNetInterestRate()
    {
        return netInterestRate;
    }

    /**
     * @param netInterestRate
     *            the netInterestRate to set
     */
    public void setNetInterestRate(double netInterestRate)
    {
        this.testChange("netInterestRate", netInterestRate);
        this.netInterestRate = netInterestRate;
    }

    /**
     * @return the pAndIPaymentAmount
     */
    public double getPAndIPaymentAmount()
    {
        return pAndIPaymentAmount;
    }

    /**
     * @param andIPaymentAmount
     *            the pAndIPaymentAmount to set
     * @version 1.2 MCM Team: fixed typo           
     */
    public void setPAndIPaymentAmount(double pandIPaymentAmount)
    {
        this.testChange("pAndIPaymentAmount", pandIPaymentAmount);
        this.pAndIPaymentAmount = pandIPaymentAmount;
    }

    /**
     * @return the paymentFrequencyId
     */
    public int getPaymentFrequencyId()
    {
        return paymentFrequencyId;
    }

    /**
     * @param paymentFrequencyId
     *            the paymentFrequencyId to set
     */
    public void setPaymentFrequencyId(int paymentFrequencyId)
    {
        this.testChange("paymentFrequencyId", paymentFrequencyId);
        this.paymentFrequencyId = paymentFrequencyId;
    }

    /**
     * @return the perDiemInterestAmount
     */
    public double getPerDiemInterestAmount()
    {
        return perDiemInterestAmount;
    }

    /**
     * @param perDiemInterestAmount
     *            the perDiemInterestAmount to set
     */
    public void setPerDiemInterestAmount(double perDiemInterestAmount)
    {
        this.testChange("perDiemInterestAmount", perDiemInterestAmount);
        this.perDiemInterestAmount = perDiemInterestAmount;
    }

    /**
     * @return the premium
     */
    public double getPremium()
    {
        return premium;
    }

    /**
     * @param premium
     *            the premium to set
     */
    public void setPremium(double premium)
    {
        this.testChange("premium", premium);
        this.premium = premium;
    }
    /**
     * <p>
     * Description: Returns the pAndIPaymentAmountMonthly.
     * </p>
     * @version 1.0 XS_11.13 10-Jul-2008 Initial Version
     * @return the pAndIPaymentAmountMonthly
     */
    public double getPAndIPaymentAmountMonthly()
    {
        return pAndIPaymentAmountMonthly;
    }
    /**
     * <p>
     * Description: Sets the pAndIPaymentAmountMonthly.
     * </p>
     * @version 1.0 XS_11.13 10-Jul-2008 Initial Version
     * @param pAndIPaymentAmountMonthly
     *            the pAndIPaymentAmountMonthly to set
     */
    public void setPAndIPaymentAmountMonthly(double pAndIPaymentAmountMonthly)
    {
        this.testChange("pAndIPaymentAmountMonthly", pAndIPaymentAmountMonthly);
        this.pAndIPaymentAmountMonthly = pAndIPaymentAmountMonthly;
    }

    /**
     * <p>
     * Description: Performs updates on the ComponentLoan entity
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return int returns the no of rows updated.
     */
    protected int performUpdate() throws Exception
    {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0)
        {
            dcm.inputEntity(this);
        }

        return ret;
    }

    /**
     * getS the pk associated with this entity
     * @return a DealPK
     */
    public IEntityBeanPK getPk()
    {
        return (IEntityBeanPK) this.pk;
    }

    /**
     * <p>
     * Description: Returns the ComponentLoan entity table name
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return String - table name of the entity
     */
    public String getEntityTableName()
    {
        return "ComponentLoan";
    }
    /**
     *   <p>isAuditable</p>
     *   <p>Descriptio: gets if this entity is auditable or not
     *   @version 1.0 XS_2.30
     *   @return true
     */
    public boolean isAuditable() {

        return true;
    }
    /**
     * <p>
     * Description: Returns the class id of the ComponentLoan entity
     * </p>
     * @version 1.0 XS_11.5 10-Jul-2008 Initial Version
     * @return int - classid of ComponentLoan entity.
     */
    public int getClassId()
    {
        return ClassId.COMPONENTLOAN;
    }

    /**
     * returns EntityContext. 
     * @since 4.0 MCM, Oct 28, 2008, FXP23168, Hiro
     */
    protected EntityContext getEntityContext() throws RemoteException {

        EntityContext ctx = this.entityContext;
        try {
            Component comp = new Component(srk, componentId, copyId);
            ctx.setContextText(this.getEntityTableName());
            int langId = srk.getLanguageId();
            int instId = srk.getExpressState().getDealInstitutionId();
            String description = BXResources.getPickListDescription(instId,
                    "COMPONENTTYPE", comp.getComponentTypeId(), langId);
            ctx.setContextSource(description);
            ctx.setApplicationId(comp.getDealId());

        } catch (FinderException e) {
            logger.error(getClass().getSimpleName() + ".getEntityContext " +
                    "FinderException caught. ComponentId= " + componentId + " CopyId=" + copyId);
        }
        return ctx;
    } 
}
