package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceRequestPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * @author CRutgaizer
 * @author ESteel
 */
public class ServiceRequest extends DealEntity {
  protected int    requestId; 
  
  protected String specialInstructions;
  protected String serviceProviderRefNo;
  
  protected int    propertyId;
  protected int    copyId;
  
  protected int institutionProfileId;
  
  private ServiceRequestPK pk;

  public IEntityBeanPK getPk() {
    return pk;
  }
  
  /**
   * Default constructor. Creates a new, empty entity, with the provided
   * Session Resource Kit.
   * @param srk <code>SessionResourceKit</code> containing all the required
   * resources for this entity.
   * @throws RemoteException If the entity cannot be created.
   */
  public ServiceRequest(SessionResourceKit srk) 
        throws RemoteException {
    super(srk);
  }
  
  /**
   * Constructor. Creates an empty entity and then populates it from the database.
   * Data is retrieved from the row with this <code>requestId</code>.
   * @param srk  <code>SessionResourceKit</code> containing all the required
   * resources for this entity.
   * @param id Identifying request ID.
   * @throws RemoteException If the entity cannot be created.
   * @throws FinderException If the record with this ID cannot be found.
   */
  public ServiceRequest(SessionResourceKit srk, int id, int cid) 
        throws RemoteException, FinderException {
    super(srk);
    pk = new ServiceRequestPK(id, cid);
    findByPrimaryKey(pk);   
  }
  
  /**
   * Creates a new database row with the given request ID.
   * @param rid Desired request ID.
   * @return This, fetched from the newly created row.
   * @throws CreateException if the row cannot be created, or if the record
   * cannot subsequently be retrieved.
   */
  public ServiceRequest create(int rid, int copyId) throws CreateException {
    
    String sql = "INSERT INTO SERVICEREQUEST (REQUESTID, COPYID, "
      + "INSTITUTIONPROFILEID) VALUES (" + rid + ", " + copyId + ", " 
      + srk.getExpressState().getDealInstitutionId() + ")";
    try {
        jExec.executeUpdate(sql);
        findByPrimaryKey(new ServiceRequestPK(rid, copyId));
        trackEntityCreate();
    }
    catch (Exception e) {
        CreateException ce = new CreateException("ServiceRequest Entity - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);
        
        throw ce;
    }
    srk.setModified(true);
    return this;
  }
  
  /**
     * @return Returns the name of the DB Table Entity this class represents.
     */
    public String getEntityTableName() {
      return "ServiceRequest";
    }

    /**
     *  Gets the value of instance fields as String.
     *  If a field does not exist (or the type is not serviced) null is returned.
     *  If the field exists (and the type is serviced) but the field is null an empty
     *  String is returned.
     *  @returns value of bound field as a String;
     *  @param  fieldName as String
     *
     *  Other entity types are not yet serviced   ie. You can't get the Province object
     *  for province.
     */
    public String getStringValue(String fieldName)
    {
       return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * Tests whether the ServiceRequest is equal to the given (sr) object.
     * @param obj The Object to test for equality.
     */
    public boolean equals(Object obj) {
        if (obj instanceof ServiceRequest) {
        ServiceRequest otherObj = (ServiceRequest) obj;
        if  (this.requestId == otherObj.getRequestId()) {
            return true;
        }
      }
        return false;   
    }

    /**
     * Retrieve this <code>ServiceRequest</code> from the database.
     * @param pk The Primary Key to find the record.
     * @return This, populated from the database.
     * @throws FinderException If the record cannot be found.
     */
    public ServiceRequest findByPrimaryKey(ServiceRequestPK pk) 
        throws FinderException {    
    
    if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "SELECT * FROM SERVICEREQUEST" + pk.getWhereClause();
    this.pk = pk;
    findByQuery(sql);
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
    }
    
    /**
     * Find Service Requests by Property ID.
     * Returns a Collection of <code>ServiceRequest</code> objects.
     * @param propertyId The Property Id to search by.
     * @return A collection of Service Requests.
     * @throws Exception if an exception occurs querying the database.
     */
    public Collection findByPropertyId(int propertyId) throws Exception {
        
        Collection requests = new Vector();
        
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * FROM SERVICEREQUEST WHERE ").
                append("PROPERTYID = " + propertyId);
        
        try { 
            int key = jExec.execute(sql.toString());
            
            for(; jExec.next(key);) {
                ServiceRequest req = new ServiceRequest(srk);
                
                req.setPropertiesFromQueryResult(key);
                req.pk = new ServiceRequestPK(req.getRequestId(), req.getCopyId());
                
                requests.add(req);
            }
            
            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception ("Exception caught while fetching ServiceRequests");
            
            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }
        
        return requests;
    }

    public Collection findByRequest(RequestPK pk) throws Exception
    {
      Collection servreqs = new ArrayList();

    String sql = "Select * from ServiceRequest ";
    sql += pk.getWhereClause();
       try
       {
      int key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
              ServiceRequest servreq = new ServiceRequest(srk);
              servreq.setPropertiesFromQueryResult(key);
              servreq.pk = new ServiceRequestPK(servreq.getRequestId(), servreq.getCopyId());
              servreqs.add(servreq);
      }
      jExec.closeData(key);

      }
        catch (Exception e)
        {
          Exception fe = new Exception("Exception caught while getting Request::ServiceRequest");
      logger.error(fe.getMessage());
      logger.error(e);

      throw fe;
    }

        return servreqs;
  }
    
    
    private ServiceRequest findByQuery(String sql) throws FinderException {

    boolean gotRecord = false;

    try {
      int key = jExec.execute(sql);

      for (; jExec.next(key);) {
        gotRecord = true;
        this.pk = pk;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false) {
        String msg = "ServiceRequest Entity: @findByQuery(), key= " + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    } catch (Exception e) {
      FinderException fe = new FinderException("ServiceRequest Entity - findByQuery() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }

    return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception {
    this.setRequestId(jExec.getInt(key, "REQUESTID"));
    this.setPropertyId(jExec.getInteger(key, "PROPERTYID"));
    this.setCopyId(jExec.getInt(key, "COPYID"));
    this.setSpecialInstructions(jExec.getString(key, "SPECIALINSTRUCTIONS"));
    this.setServiceProviderRefNo(jExec.getString(key, "SERVICEPROVIDERREFNO"));
    this.setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
  }

  /**
   * Performs a database update.
   */
  protected int performUpdate() throws Exception {
    Class clazz = this.getClass();

    int ret = doPerformUpdate(this, clazz);

    return ret;
  }

  // GETTERS/SETTERS -----------------------------------------
  public int getPropertyId() {
    return propertyId;
  }
  public void setPropertyId(int propertyId) {

      setPropertyId(new Integer(propertyId));
  }
  public void setPropertyId(Integer propertyId) {

    testChange("propertyId", propertyId);
    if (propertyId != null) {
        this.propertyId = propertyId.intValue();
    }
  }
  public int getCopyId() {
    return copyId;
  }
  public void setCopyId(int copyId) {
    testChange("copyId", copyId);
    this.copyId = copyId;
  }
  public int getRequestId() {
    return requestId;
  }
  public void setRequestId(int requestId) {
    testChange("requestId", requestId);
    this.requestId = requestId;
  }
  public String getSpecialInstructions() {
    return specialInstructions;
  }
  public void setSpecialInstructions(String specialInstructions) {
    testChange("specialInstructions", specialInstructions);
    this.specialInstructions = specialInstructions;
  }
  public String getServiceProviderRefNo() {
     return serviceProviderRefNo;
  }
  public void setServiceProviderRefNo(String serviceProviderRefNo) {
    testChange("serviceProviderRefNo", serviceProviderRefNo);
    this.serviceProviderRefNo = serviceProviderRefNo;
  }

  // Catherine: for AVM ------- start ---------
  public Collection findByPropertyIdAndCopyId(PropertyPK pk) throws Exception {
    Collection list = new ArrayList();

    String sql = "SELECT * FROM SERVICEREQUEST";
    sql += " WHERE PROPERTYID = " + pk.getId();
    sql += " AND COPYID = " + pk.getCopyId();
    sql += " ORDER BY REQUESTID DESC";
    try {
      int key = jExec.execute(sql);
      for (; jExec.next(key);) {
        ServiceRequest srvcReq = new ServiceRequest(srk);
        srvcReq.setPropertiesFromQueryResult(key);
        srvcReq.pk = new ServiceRequestPK(srvcReq.getRequestId(), srvcReq.getCopyId());
        list.add(srvcReq);
      }
      jExec.closeData(key);

    } catch (Exception e) {
      Exception fe = new Exception("Exception caught while getting Deal::Borrowers");
      logger.error(fe.getMessage());
      logger.error(e);

      throw fe;
    }

    return list;
  }
  // Catherine: for AVM ------- end ---------

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    
    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
  
}
