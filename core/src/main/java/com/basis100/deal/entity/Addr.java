package com.basis100.deal.entity;

import java.util.StringTokenizer;

import MosSystem.Mc;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.BorrowerAddressPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

public class Addr extends DealEntity
{
  protected int      addrId;
  protected int      copyId;
  protected int      provinceId;
  protected String   addressLine1;
  protected String   addressLine2;
  protected String   city;
  protected String   postalFSA;
  protected String   postalLDU;
  protected String 	streetNumber;
  protected String 	streetName;
  protected int 	streetTypeId;
  protected int 	streetDirectionId;
  protected String 	unitNumber;
  private static final int maxAddressLineSize = 35; 
  protected int 	instProfileId;

  private AddrPK pk;

  public Addr(SessionResourceKit srk) throws RemoteException, FinderException
  {
    super(srk);
  }

  public Addr(SessionResourceKit srk, int id, int copyId) throws RemoteException, FinderException
  {
    super(srk);

    pk = new AddrPK(id, copyId);

    findByPrimaryKey(pk);
  }

  private AddrPK createPrimaryKey(int copyId)throws CreateException
  {
     String sql = "Select Addrseq.nextval from dual";
     int id = -1;

     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key);  )
        {
          id = jExec.getInt(key,1);  // can only be one record
        }

        jExec.closeData(key);
        if( id == -1 ) throw new Exception();

      }
      catch (Exception e)
      {
          CreateException ce = new CreateException("Addr Entity create() exception getting AddrId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;

      }

      return new AddrPK(id, copyId);
  }

  /**
  * creates a Addr using the AddrId with mininimum fields
  *
  */
  protected Addr create(BorrowerAddressPK bapk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(bapk.getCopyId());

    String sql = "Insert into Addr( AddrId,copyId,INSTITUTIONPROFILEID) Values ( "
        + pk.getId() + ","+ pk.getCopyId() + "," + srk.getExpressState().getDealInstitutionId() +  ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate ();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Addr Entity - Addr - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);
    return this;
  }

  /**
  * creates a Addr using the default copy id
  *
  */
  public Addr create()throws RemoteException, CreateException
  {
    pk = createPrimaryKey(DEFAULT_COPY_VALUE);

    String sql = "Insert into Addr( AddrId,copyId,INSTITUTIONPROFILEID) Values ( "
                  + pk.getId() + ","+ pk.getCopyId() + "," + srk.getExpressState().getDealInstitutionId() +  ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate ();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Addr Entity - Addr - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    srk.setModified(true);
    return this;
  }

  /**
  * creates a Addr using the copyId from the parent Contact entity via ContactPK
  * @return this Addr instance synchroized with the db
  */
  protected Addr create(ContactPK cpk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(cpk.getCopyId());

    String sql = "Insert into Addr( addrId,copyId,INSTITUTIONPROFILEID) Values ( "
    	+ pk.getId() + ","+ pk.getCopyId() + "," + srk.getExpressState().getDealInstitutionId() +  ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate ();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Addr Entity - Addr - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);
    return this;
  }

  public Addr findByPrimaryKey(AddrPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
	  	
    String sql = "Select * from addr " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            gotRecord = true;
            setPropertiesFromQueryResult(key);
             break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "Addr Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("Addr Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }
  
  /**
   * setPropertiesFromQueryResult
   * This method sets the values in entity to save data in database.
   *
   * @param key int <br>
   *
   * @throws Exception : If an DB exception has occured<br>
   * @version 1.2 (Unit Test passed � 28, June 2006) <br>
   * Date: 06/28/2006 <br>
   * Author: NBC/PP Implementation Team <br>
   * Change:  <br>
   *	Added the new fields setters <br>
   */
  private void setPropertiesFromQueryResult(int key) throws Exception
  {
    setAddrId( jExec.getInt(key, "ADDRID"));
    setCopyId( jExec.getInt(key, "COPYID"));
    setProvinceId(jExec.getInt(key, "PROVINCEID"));
    setAddressLine1(jExec.getString(key, "ADDRESSLINE1"));
    setAddressLine2(jExec.getString(key, "ADDRESSLINE2"));
    setCity(jExec.getString(key, "CITY"));
    setPostalFSA(jExec.getString(key, "POSTALFSA"));
    setPostalLDU(jExec.getString(key, "POSTALLDU"));
    setStreetNumber(jExec.getString(key, "STREETNUMBER"));
    setStreetName(jExec.getString(key, "STREETNAME"));
    setStreetTypeId(jExec.getInt(key, "STREETTYPEID"));
    setStreetDirectionId(jExec.getInt(key, "STREETDIRECTIONID"));
    setUnitNumber(jExec.getString(key, "UNITNUMBER"));
    setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
  }


  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();

    return (doPerformUpdate(this, clazz));
  }

  public String getEntityTableName()
  {
    return "Addr";
  }

  /**
  *  gets the value of instance fields as String.
  *  if a field does not exist (or the type is not serviced)** null is returned.
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *  @returns value of bound field as a String;
  *  @param  fieldName as String
  *
  *  Other entity types are not yet serviced   ie. You can't get the Province object
  *  for province.
  */
  public String getStringValue(String fieldName)
  {
   return doGetStringValue(this, this.getClass(), fieldName);
  }

  public Object getFieldValue(String fieldName){
    return doGetFieldValue(this, this.getClass(), fieldName);
  }


  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  @param  fieldName as String
  *  @param  the value as a String
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }


  /**
  *   gets the pk associated with this entity
  *
  *   @return a AddrPK
  */
  public IEntityBeanPK getPk()
  {
    return this.pk ;
  }

  private void setAddrId(int id)
  {
   this.testChange ("addrId", id );
   this.addrId = id;
  }

  public int getAddrId()
  {
    return this.addrId ;
  }

  private void setCopyId(int id)
  {
    this.testChange ("copyId", id );
   this.copyId = id;
  }

  public int getCopyId()
  {
    return this.copyId ;
  }

  public void setProvinceId(int id )
  {
    this.testChange ("provinceId", id);
   this.provinceId = id;
  }

  public int getProvinceId()
  {
    return this.provinceId ;
  }

  public void setAddressLine1(String line1)
  {
    this.testChange ("addressLine1", line1);
   this.addressLine1 = line1;
  }

  public String getAddressLine1()
  {
    return this.addressLine1 ;
  }

  public void setAddressLine2(String line2)
  {
    this.testChange("addressLine2", line2);
   this.addressLine2 = line2;
  }

  public String getAddressLine2()
  {
    return this.addressLine2 ;
  }

  public void setCity(String city)
  {
	  if(city != null && city.length() > 20)
		  city = city.substring(0, 20);
    this.testChange ("city", city );
   this.city = city;
  }

  public String getCity()
  {
    return this.city ;
  }

  public void setPostalFSA(String fsa )
  {
    this.testChange("postalFSA", fsa);
   this.postalFSA = fsa;
  }

  public String getPostalFSA()
  {
    return this.postalFSA ;
  }

  public void setPostalLDU(String ldu )
  {
    this.testChange("postalLDU", ldu);
   this.postalLDU = ldu;
  }

  public String getPostalLDU()
  {
    return this.postalLDU ;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof Addr )
      {
        Addr oa = ( Addr )  o;
        if  ( ( addrId  == oa.getAddrId () )
          && (  copyId  ==  oa.getCopyId ()  )  )
            return true;
      }
    return false;
  }

  /**
 * getStreetDirectionId
 * @return Returns the streetDirectionId.
 */
public int getStreetDirectionId() {
	return streetDirectionId;
}

/**
 * setStreetDirectionId
 * @param streetDirectionId The streetDirectionId to set.
 */
public void setStreetDirectionId(int streetDirectionId) {
	this.testChange("streetDirectionId",streetDirectionId);
	this.streetDirectionId = streetDirectionId;
}

/**
 * getStreetName
 * @return Returns the streetName.
 */
public String getStreetName() {
	return streetName;
}

/**
 * setStreetName
 * @param streetName The streetName to set.
 */
public void setStreetName(String streetName) {
	this.testChange("streetName",streetName);
	this.streetName = streetName;
}

/**
 * getStreetNumber
 * @return Returns the streetNumber.
 */
public String getStreetNumber() {
	return streetNumber;
}

/**
 * setStreetNumber
 * @param streetNumber The streetNumber to set.
 */
public void setStreetNumber(String streetNumber) {
	this.testChange("streetNumber",streetNumber);
	this.streetNumber = streetNumber;
}

/**
 * getStreetTypeId
 * @return Returns the streetTypeId.
 */
public int getStreetTypeId() {
	return streetTypeId;
}

/**
 * setStreetTypeId
 * @param streetTypeId The streetTypeId to set.
 */
public void setStreetTypeId(int streetTypeId) {
	this.testChange("streetTypeId",streetTypeId);
	this.streetTypeId = streetTypeId;
}

/**
 * getUnitNumber
 * @return Returns the unitNumber.
 */
public String getUnitNumber() {
	return unitNumber;
}

/**
 * setUnitNumber
 * @param unitNumber The unitNumber to set.
 */
public void setUnitNumber(String unitNumber) {
	this.testChange("unitNumber",unitNumber);
	this.unitNumber = unitNumber;
}

/**
 * ejbStore 
 * This method overrides ejbStore to store the updated addressline1 value 
 * in Addr Entity <br>
 * @throws RemoteException <br>
 */
public void ejbStore() throws RemoteException
{
	updateAddressField();
	super.ejbStore();
}

/**
 * updateAddressField 
 * This method updates addressline1 value with new field values. It truncates the 
 * addressLineStringValue, if its length exceeds max length <br>
 */

	private void updateAddressField(){
		String addressLineStringValue=null;
		addressLineStringValue = getConcatenatedAddressLine ();
		
		if (addressLineStringValue != null )
		{
			if (addressLineStringValue.length()>maxAddressLineSize)
			{
                // Truncating the ending characters from address, if AddressLine exceeds maxAddressLineSize , i.e. 35 chars
                String[] stringLines = torncatAddress(addressLineStringValue);
                setAddressLine1(stringLines[0]);
                setAddressLine2(stringLines[1]);
			}
			else
            {
				setAddressLine1(addressLineStringValue);
                setAddressLine2(null);
            }
		}
	}
  
  /** 
   * <p>Truncating address using space, comma, or dash
   */
	public String[] torncatAddress(String addressLine)
  {
    String[] addressLines = null;
    addressLines = torncatBy(addressLine, ",");
    if (addressLines == null)
      addressLines = torncatBy(addressLine, " ");
    if (addressLines == null)
      addressLines = torncatBy(addressLine, "-");
    if (addressLines == null)
    {
      addressLines = new String[2];
      addressLines[0] = addressLine.substring(0, maxAddressLineSize);
      if (addressLine.substring(maxAddressLineSize).length()>maxAddressLineSize)
        addressLines[1] = addressLine.substring(maxAddressLineSize, maxAddressLineSize*2);
      else
        addressLines[1] = addressLine.substring(maxAddressLineSize);
    }
    
   return addressLines;
  }
  
  public String[] torncatBy(String addressLine, String splitBy)
  {
    
    String line1 = null;
    String line2 = null;
    
   StringTokenizer tokens = new StringTokenizer(addressLine, splitBy, true);
   int countLength = 0;
   if (tokens.countTokens() > 1)
   {
     while (tokens.hasMoreTokens())
     {
       String token = tokens.nextToken();
       if (countLength + token.length() < maxAddressLineSize)
       {
         countLength += token.length();
       }
       else
       {
         break;
       }
     }
     line1 = addressLine.substring(0, countLength);
     line2 = addressLine.substring(countLength);
     if (line2.length() > maxAddressLineSize)
       line2 = line2.substring(0, maxAddressLineSize);
     String[] stringLines = {line1, line2};
     return stringLines;
   }
   return null;
  }
  
  
/**
 * <p>getConcatenatedAddressLine </p>
 * <p>This method gets the values for address fields and concatenates the addressline in 
 * format "Unit-Street# StreetName Type Direction" by getting following values from Entity</p>
 * 
 *  <p>-unitNumber</p>
 *  <p>-streetNumber</p>
 *	<p>-streetName</p>
 *  <p>-streetTypeId</p>
 *  <p>-directionId</p>
 *  
 *  <p>History: changed to use lang id - Dec 1, 2006 Midori
 *  
 * @return String addressLineStringValue , new addressline string in format "Unit-Street# StreetName Type Direction" <br>
 */
  public String getConcatenatedAddressLine()
  {
    //   Set English as default
    if (Mc.PROVINCE_QUEBEC == getProvinceId())
    {
      return getConcatenatedAddressLine(Mc.LANGUAGE_PREFERENCE_FRENCH);
    }
    else
    {
      return getConcatenatedAddressLine(Mc.LANGUAGE_PREFERENCE_ENGLISH);
    }
  }
  
    private String getConcatenatedAddressLine(int lang)
    {
    	String unitNumber			= getUnitNumber();
    	String streetNumber		    = getStreetNumber();
    	String streetName			= getStreetName();
    	String streetType			= BXResources.getPickListDescription
                            (srk.getExpressState().getDealInstitutionId(), "STREETTYPE", getStreetTypeId(), lang );
    	String direction			= BXResources.getPickListDescription
                            (srk.getExpressState().getDealInstitutionId(), "STREETDIRECTION", getStreetDirectionId(), lang );
                
    	StringBuffer addressLine = new StringBuffer("");
        boolean getOriginalAddrLine = true;
        // The format for value to be displayed in AddressLine1 is :
        // "Unit-Street# StreetName Type Direction"    
        if ( unitNumber != null && !unitNumber.trim().equals("0") && unitNumber.trim().length() > 0)
        {
            addressLine.append(unitNumber+"-");
            getOriginalAddrLine = false;
        }
        if (streetNumber != null && streetNumber.trim().length() > 0)
        {
        	addressLine.append(streetNumber+" ");
          getOriginalAddrLine = false;
        }
        if (Mc.LANGUAGE_PREFERENCE_ENGLISH == lang)
        {
          if (streetName != null && streetName.trim().length() > 0)
          {
            addressLine.append(streetName+" ");
            getOriginalAddrLine = false;
          }
          if (streetType != null && streetType.trim().length() > 0)
          {
            addressLine.append(streetType+" ");
            getOriginalAddrLine = false;
          }      
        }
        else
        {
          if (streetType != null && streetType.trim().length() > 0)
          {
            addressLine.append(streetType+" ");
            getOriginalAddrLine = false;
          }      
          if (streetName != null && streetName.trim().length() > 0)
          {
            addressLine.append(streetName+" ");
            getOriginalAddrLine = false;
          }
        }
        if (direction != null && direction.trim().length() > 0)
        {
          addressLine.append(direction);
          getOriginalAddrLine = false;
        }
    		
        if (getOriginalAddrLine)
        {
          return StringUtil.concat( StringUtil.concat(getAddressLine1(), " "), 
                                    getAddressLine2()).toString().trim();
    //    return getAddressLine1();
        }
        else if (getAddressLine2() != null && getAddressLine2().trim().length() > 0)
        {
          return addressLine.toString().trim() + " " + getAddressLine2().trim();
        }
        else 
        {
          return addressLine.toString().trim();
        }
	}	

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}
}

