package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * @version 1.2 <br>
 * @author: NBC/PP Implementation Team <br>
 *          Date: 06/28/2006 <br>
 *          Change: <br>
 *          added a new method to get MtgProd based on MpBusinessId. <br> -
 *          public MtgProd getMtgProdByMPBusinessId(String mpBusinessId)<br> -
 *          added new fields.
 * @version 1.3 XS_16.7 <br>
 * @author: MCM Implementation Team <br>
 *          Date: 13/06/2006 <br>
 *          Change: <br>
 *          added a new componentEligibleFlagfields . <br> - updated
 *          setPropertiesFromQueryResult method <br>
 * @version 1.4 <br>
 * @author: MCM Implementation Team <br>
 *          Date: 20/06/2006 <br>
 *          Change: <br>
 *          Checking in just to increase the version to force build to take new
 *          file <br>
 * @version 1.5 <br>
 * @author: MCM Implementation Team <br>
 *          Date: 24/06/2006 <br>
 *          Change: <br>
 *          added a new UnderwriteAsTypeId . <br> - updated
 *          setPropertiesFromQueryResult method <br> *
 * @version 1.6 <br>
 * @author: MCM Implementation Team <br>
 *          Date: 1/08/2008 <br>
 *          Change: <br>
 *          added a new componentTypeId, respective getter, setters . <br> -
 *          updated setPropertiesFromQueryResult method <br>
 * @version 1.7 <br>
 * @author: MCM Implementation Team <br>
 *          Date: 23/08/2008 <br>
 *          Change: <br>
 *          added a new repaymentTypeId, respective getter, setters . <br> -
 *          updated setPropertiesFromQueryResult method artf767017<br>         
 */
public class MtgProd extends DealEntity
{
   protected int      mtgProdId;
   protected int      interestTypeId;
   protected String   mtgProdName;
   protected double   minimumAmount;
   protected double   maximumAmount;
   protected double   minimumLTV;
   protected double   maximumLTV;
   protected int      paymentTermId;
   protected int      pricingProfileId;
   protected int      lineOfBusinessId;
   protected String   commitmentTerm;
   protected String   mpBusinessId;
   protected int      prePaymentOptionsId;
   protected int      privilegePaymentId;
   //--Ticket#1736--18July2005--start--//
   //protected double   teaserDiscount;
   //protected int      teaserTerm;
   //--Ticket#1736--18July2005--end--//

   protected String   mpShortName;
   protected int      interestCompoundingId;
   protected double   miscellaneousRate;
   protected int   rateGuaranteePeriod;
   protected int   advanceRateFloatDownPeriod;
   protected int   rateFloatDownTypeId;

   // ***** Change by NBC Impl. Team - Version 1.2  
   protected double maxCashBackAmount;
   protected double maxCashBackPercentage;
   protected int minimumAmortization;
   protected int maximumAmortization;
   protected int affiliationProgramId; 
   protected int sourceOfBusinessCategoryId;
   
   /*****************MCM Impl Team Starts XS_16.7*******************/
   protected String componentEligibleFlag;

   /*****************MCM Impl Team Ends XS_16.7*******************/
   /*****************MCM Impl Team STARTS XS_11.2*******************/
   protected int underwriteAsTypeId;
   /*****************MCM Impl Team Ends XS_11.2*******************/
   /*******************Mcm Imp Team Starts XS_16.17***************/
   protected int producttypeId;
   /*******************Mcm Imp Team Starts XS_16.17***************/
   
   /*******************Mcm Imp Team Starts XS_16.23***************/
   protected int componentTypeId;
   /*******************Mcm Imp Team Starts XS_16.23***************/
   // ***** Change by NBC Impl. Team - Version 1.2
   protected MtgProdPK pk;
   
   protected int institutionProfileId;
   /*******************Mcm Imp Team Starts version1.7***************/
   protected int repaymentTypeId;
   /*******************Mcm Imp Team Ends version1.7***************/
   
   //Qualify Rate
   protected String qualifyingRateEligibleFlag;
   
   
   public MtgProd(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
   {
      super(srk,dcm);
   }
 

   public MtgProd(SessionResourceKit srk,CalcMonitor dcm, int id) throws RemoteException, FinderException
   {
      super(srk,dcm);

      pk = new MtgProdPK(id);

      findByPrimaryKey(pk);
   }

   /**
    * This method returns MtgProd for given mpBusinessId <br>
    * @param mpBusinessId - mortgage product business id <br>
    * @return - returns <code>MtgProd</code> corresponding to mpBusinessId<br>
    * @throws RemoteException - If any remote exception occurs <br>
    * @throws FinderException - If it fails to find the <code>MtgProd</code> entity for given mpBusinessId<br>
    */
   public MtgProd findByMPBusinessId(String mpBusiness) throws RemoteException, FinderException
   {
	  //**** Sasha - #564 from Expert-IBC
	  //StringBuffer buf = new StringBuffer();
	  //buf.append("Select * from MtgProd where mpBusinessId = ");
      //buf.append(sqlStringFrom(mpBusiness));
      
      String sql = "Select * from MtgProd where mpBusinessId = " + sqlStringFrom(mpBusiness);
      //**** End of Sasha - #564 from Expert-IBC

      boolean gotRecord = false;

      try
      {
    	  //**** Sasha - #564 from Expert-IBC
          //int key = jExec.execute(buf.toString());
          int key = jExec.execute(sql);
          //**** End of Sasha - #564 from Expert-IBC

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "MtgProd Entity: @findByMPBusinessId(), mpBusinessId= " + mpBusiness + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("MtgProd Entity - @findByMPBusinessId(mpBusinessId) exception");

          logger.error(fe.getMessage());
          
          //**** Sasha - #564 from Expert-IBC
          //logger.error("finder sql: " + buf.toString());
          logger.error("finder sql: " + sql);
          //**** End of Sasha - #564 from Expert-IBC
          
          logger.error(e);

          throw fe;
        }

     this.pk = new MtgProdPK(this.getMtgProdId());
     return this;
     
   } 

   public MtgProd findByPrimaryKey(MtgProdPK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from MtgProd where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }
//=========================XS_16.7===========================================================//
   public MtgProd findByPrimaryKey(int dealId,int institutionId) throws RemoteException, FinderException
   {
      String sql = "Select * from MtgProd where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        this.pk = pk;

        return this;
    }
//===========================================================================================//
    public Collection findByLenderAndPaymentTerm(int lenderProfileId, int paymentTermId) throws Exception
    {
      // select m.mtgProdid mtgProdId from mtgprod m, mtgprodlenderinvestorassoc a where a.lenderprofileid = 1 and a.mtgprodid = m.mtgprodid and m.paymentTermid = 1
      StringBuffer buf = new StringBuffer();
      buf.append("select * from mtgprod m, ");
      buf.append("mtgprodlenderinvestorassoc a where a.mtgprodid = m.mtgprodid and ");
      buf.append("a.lenderprofileid = ").append(lenderProfileId).append(" and ");
      buf.append("m.paymentTermid = " ).append(paymentTermId);

      List prods = new ArrayList();
      try
      {

        int key = jExec.execute(buf.toString());

        for (; jExec.next(key); )
        {
          MtgProd mp = new MtgProd(srk,dcm);
          mp.setPropertiesFromQueryResult(key);
          mp.pk = new MtgProdPK(mp.getMtgProdId());
          prods.add(mp);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {

        Exception fe = new Exception("MtgProd - findByLenderAndPaymentTerm");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + buf.toString());
        logger.error(e);

        throw fe;
      }
      return prods;
   }

    public Collection findByPricingProfile(int pricingProfileId) throws Exception
    {
      // select m.mtgProdid mtgProdId from mtgprod m, mtgprodlenderinvestorassoc a where a.lenderprofileid = 1 and a.mtgprodid = m.mtgprodid and m.paymentTermid = 1
      StringBuffer buf = new StringBuffer();
      buf.append("Select * from MtgProd where pricingProfileId =  " + pricingProfileId);

      List prods = new ArrayList();

      try
      {

        int key = jExec.execute(buf.toString());

        for (; jExec.next(key); )
        {
          MtgProd mp = new MtgProd(srk,dcm);
          mp.setPropertiesFromQueryResult(key);
          mp.pk = new MtgProdPK(mp.getMtgProdId());
          prods.add(mp);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {

        Exception fe = new Exception("MtgProd - findByLenderAndPaymentTerm");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + buf.toString());
        logger.error(e);

        throw fe;
      }
      return prods;
   }


   public Collection findByLenderAndInterestType(int lenderProfileId, int intType) throws Exception
   {
      StringBuffer buf = new StringBuffer();
      buf.append("select * from mtgprod m, ");
      buf.append("mtgprodlenderinvestorassoc a where a.mtgprodid = m.mtgprodid and ");
      buf.append("a.lenderprofileid = ").append(lenderProfileId).append(" and ");
      buf.append("m.interestTypeId =  ").append(intType);

      List prods = new ArrayList();

      try
      {
        int key = jExec.execute(buf.toString());

        for (; jExec.next(key); )
        {
          MtgProd mp = new MtgProd(srk,dcm);
          mp.setPropertiesFromQueryResult(key);
          mp.pk = new MtgProdPK(mp.getMtgProdId());
          prods.add(mp);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("MtgProd - findByLenderAndPaymentTerm");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + buf.toString());
        logger.error(e);

        throw fe;
      }
      return prods;
   }

   public MtgProd findDefaultFor(LenderProfile prof)throws RemoteException, FinderException
   {

      StringBuffer buf = new StringBuffer();
      buf.append("Select * from MtgProd where mtgProdId = ");
      buf.append("(Select mtgprodid from mtgprodlenderinvestorassoc where lenderprofileid = ");
      //buf.append("lenderprofileid = ").append(prof.getLenderProfileId());
      // zivko changed to fix the error in SQL statement
      buf.append( prof.getLenderProfileId() );
      buf.append(" and defaultProduct = 'Y' ");
      buf.append(" and institutionprofileid = " +  prof.getInstitutionProfileId() + " ) ");
      

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(buf.toString());

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
          FinderException fe = new FinderException("Deal Entity - findDefaultFor(LenderProfile) exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + buf.toString());
          logger.error(e);

          throw fe;
        }

     this.pk = new MtgProdPK(this.getMtgProdId());

     return this;

  }

   /**
    * Find the rateFloatDownTypeId associated to the pricingProfileId
    * 
    * @param ppid
    * @return rateFloatDownTypeId
    * @throws Exception
    */
   public int findRfdTypeByPricingProfileId(int ppid) throws Exception
   {
       int rfdtype = 0;
       
       String sql = "select rateFloatDownTypeId from mtgProd where pricingProfileId = " + ppid;

       try
       {
           int key = jExec.execute(sql);
           for (; jExec.next(key); )
           {
               rfdtype = jExec.getInt(key,"rateFloatDownTypeId");
               break;
           }
               jExec.closeData(key);

       }
       catch (Exception e)
       {
           Exception fe = new Exception("MtgProd - findRfdTypeByPricingProfileId");
           logger.error(fe.getMessage());
           logger.error("finder sql: " + sql);
           logger.error(e);

           throw fe;
       }
       return rfdtype;
   }
   
   /**
     * <p>
     * Set properties from query result
     * </p>
     * @version 1.2 <br>
     *          Date: 06/28/2006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br> - setting the new fields<br>
     * @version 1.2 <br>
     *          Date: 13/06/2008 <br>
     *          Author: MCM Implementation Team <br>
     *          Change: <br> - setting the new COMPONENTELIGIBLEFLAG field<br> *
     * @version 1.3 <br>
     *          Date: 1/08/2008 <br>
     *          Author: MCM Implementation Team <br>
     *          Change: <br> - setting the new COMPONENTTYPEID field<br>
     */
   private void setPropertiesFromQueryResult(int key) throws Exception
   {
     this.setInterestTypeId(jExec.getInt(key,"INTERESTTYPEID"));
     this.setMtgProdId(jExec.getInt(key,"MTGPRODID"));
     this.setMtgProdName(jExec.getString(key,"MTGPRODNAME"));
     this.setMinimumAmount(jExec.getDouble(key,"MINIMUMAMOUNT"));
     this.setMaximumAmount(jExec.getDouble(key,"MAXIMUMAMOUNT"));
     this.setMinimumLTV(jExec.getDouble(key,"MINIMUMLTV"));
     this.setMaximumLTV(jExec.getDouble(key,"MAXIMUMLTV"));
     this.setPaymentTermId(jExec.getInt(key,"PAYMENTTERMID"));
     this.setPricingProfileId(jExec.getInt(key,"PRICINGPROFILEID"));
     this.setLineOfBusinessId(jExec.getInt(key,"LINEOFBUSINESSID"));
     this.setCommitmentTerm(jExec.getString(key,"COMMITMENTTERM"));
     this.setMPBusinessId(jExec.getString(key,"MPBUSINESSID"));
     this.setPrePaymentOptionsId(jExec.getInt(key,"PREPAYMENTOPTIONSID"));
     this.setPrivilegePaymentId(jExec.getInt(key,"PRIVILEGEPAYMENTID"));
     //--Ticket#1736--18July2005--start--//
     //this.setTeaserDiscount(jExec.getDouble(key,++index));
     //this.setTeaserTerm(jExec.getInt(key,++index));
     //--Ticket#1736--18July2005--end--//
     this.setMPShortName(jExec.getString(key,"MPSHORTNAME"));
     this.setInterestCompoundingId(jExec.getInt(key,"INTERESTCOMPOUNDINGID"));
     this.setMiscellaneousRate(jExec.getDouble(key,"MISCELLANEOUSRATE"));
     this.setRateGuaranteePeriod(jExec.getInt(key,"RATEGUARANTEEPERIOD"));
     this.setAdvanceRateFloatDownPeriod(jExec.getInt(key,"ADVANCERATEFLOATDOWNPERIOD"));
     this.setRateFloatDownTypeId(jExec.getInt(key,"RATEFLOATDOWNTYPEID"));
     
//   ***** Change by NBC Impl. Team - Version 1.2 - Start *****//   
     this.setMaxCashBackAmount(jExec.getDouble(key,"MAXCASHBACKAMOUNT"));
     this.setMaxCashBackPercentage(jExec.getDouble(key,"MAXCASHBACKPERCENTAGE"));
     this.setMinimumAmortization(jExec.getInt(key,"MINIMUMAMORTIZATION"));
     this.setMaximumAmortization(jExec.getInt(key,"MAXIMUMAMORTIZATION"));
     this.setAffiliationProgramId(jExec.getInt(key,"AFFILIATIONPROGRAMID"));
     this.setSourceOfBusinessCategoryId(jExec.getInt(key,"SOURCEOFBUSINESSCATEGORYID"));
//   ***** Change by NBC Impl. Team - Version 1.2 - End *****//
     
     this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
     /************MCM Impl Team Starts XS_16.7*************************/
     this.setComponentEligibleFlag(jExec.getString(key,"COMPONENTELIGIBLEFLAG"));
     /************MCM Impl Team Starts XS_16.7*************************/
     /************MCM Impl Team Starts XS_11.2*************************/
     this.setUnderwriteAsTypeId(jExec.getInt(key,"UNDERWRITEASTYPEID"));
     /************MCM Impl Team Starts XS_11.2*************************/
     
     /************MCM Impl Team Starts XS_11.2*************************/
     this.setProductTypeId(jExec.getInt(key,"PRODUCTTYPEID"));
     /************MCM Impl Team Starts XS_11.2*************************/
     
     /************MCM Impl Team Starts XS_16.23*************************/
     this.setComponentTypeId(jExec.getInt(key,"COMPONENTTYPEID"));
     /************MCM Impl Team Starts XS_16.23*************************/
     
     /*******************Mcm Imp Team Starts version1.7***************/
     this.setRepaymentTypeId(jExec.getInt(key, "REPAYMENTTYPEID"));
     /*******************Mcm Imp Team Starts version1.7***************/
     
     //Qualify Rate
     this.setQualifyingRateEligibleFlag(jExec.getString(key, "QUALIFYINGRATEELIGIBLEFLAG"));
     
   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

   public String getEntityTableName()
   {
      return "MtgProd";
   }

		/**
     *   gets the minimumLTV associated with this entity
     *
     *   @return a double
     */
    public double getMinimumLTV()
   {
      return this.minimumLTV ;
   }

		/**
     *   gets the paymentTermId associated with this entity
     *
     */
    public int getPaymentTermId()
   {
      return this.paymentTermId ;
   }


		/**
     *   gets the pricingProfile associated with this entity
     *
     *   @return a PricingProfile
     */
   public PricingProfile getPricingProfile()throws RemoteException,FinderException
   {
      return new PricingProfile(srk,pricingProfileId);
   }


		/**
     *   gets the pricingProfileId associated with this entity
     *
     *   @return a int
     */
    public int getPricingProfileId()
   {
      return this.pricingProfileId ;
   }

		/**
     *   gets the mtgProdId associated with this entity
     *
     *   @return a int
     */
    public int getMtgProdId()
   {
      return this.mtgProdId ;
   }


		/**
     *   gets the mtgProdName associated with this entity
     *
     *   @return a String
     */
    public String getMtgProdName()
   {
      return this.mtgProdName ;
   }

		/**
     *   gets the maximumLTV associated with this entity
     *
     *   @return a double
     */
    public double getMaximumLTV()
   {
      return this.maximumLTV ;
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a MtgProdPK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

		/**
     *   gets the minimumAmount associated with this entity
     *
     *   @return a double
     */
    public double getMinimumAmount()
   {
      return this.minimumAmount ;
   }

		/**
     *   gets the lineOfBusinessId associated with this entity
     *
     *   @return a int
     */
    public int getLineOfBusinessId()
   {
      return this.lineOfBusinessId ;
   }
    /**
     *   gets the maximumAmount associated with this entity
     *
     *   @return a double
     */
    public double getMaximumAmount()
   {
      return this.maximumAmount ;
   }

		/**
     *   gets the interestTypeId associated with this entity
     *
     *   @return a int
     */
    public int getInterestTypeId()
   {
      return this.interestTypeId ;
   }


   public String getMPShortName(){return this.mpShortName; }
   public String getMPBusinessId(){return this.mpBusinessId; }
   public String getCommitmentTerm(){return this.commitmentTerm; }
   public int getPrivilegePaymentId(){return this.privilegePaymentId ;}
   public int getPrePaymentOptionsId(){return this.prePaymentOptionsId ;}
   public int getInterestCompoundingId(){ return this.interestCompoundingId ;}


  public MtgProdPK createPrimaryKey()throws CreateException
  {
       String sql = "Select MtgProdseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }

           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("MtgProd Entity create() exception getting MtgProdId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;

        }

        return new MtgProdPK(id);
  }

  /**
   * creates a MtgProd using the MtgProdId with mininimum fields
   *
   */

  public MtgProd create(MtgProdPK pk)throws RemoteException, CreateException
  {
        String sql = "Insert into MtgProd(" + pk.getName() + ", INSTITUTIONPROFILEID ) Values ( " + pk.getId() + ", " +  srk.getExpressState().getDealInstitutionId()+")";

        try
        {
          jExec.executeUpdate(sql);
          findByPrimaryKey(pk);
          trackEntityCreate();
        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("MtgProd Entity - MtgProd - create() exception");
          logger.error(ce.getMessage());
          logger.error(e);

          throw ce;
        }

      srk.setModified(true);
       return this;
  }


  public List getSecondaryParents() throws Exception
  {
    List sps = new ArrayList();

    Object o = getPricingProfile();

    if(o != null)
      sps.add(o);

    return sps;
  }

   public void setMtgProdId(int value)
	{
		 this.testChange("mtgProdId", value);
		this.mtgProdId = value;
  }
   public void setInterestTypeId(int value)
	{
		 this.testChange("interestTypeId", value);
		this.interestTypeId = value;
  }

   public void setMtgProdName(String  value )
	{
		 this.testChange("mtgProdName", value);
		this.mtgProdName = value;
  }
   public void setMinimumAmount(double value )
	{
		 this.testChange("minimumAmount", value);
		this.minimumAmount = value;
  }
   public void setMaximumAmount(double value )
	{
		 this.testChange("maximumAmount", value);
		this.maximumAmount = value;
  }
   public void setMinimumLTV(double value)
	{
		 this.testChange("minimumLTV", value);
		 this.minimumLTV = value;
  }

   public void setMaximumLTV(double value )
	{
		 this.testChange("maximumLTV", value);
		 this.maximumLTV = value;
  }

   public void setPaymentTermId(int value)
	{
		 this.testChange("paymentTermId", value);
		 this.paymentTermId = value;
  }

  public void setPrePaymentOptionsId(int value)
	{
		 this.testChange("prePaymentOptionsId", value);
		 this.prePaymentOptionsId = value;
  }

  public void setPrivilegePaymentId(int value)
	{
		 this.testChange("privilegePaymentId", value);
		 this.privilegePaymentId = value;
  }
   public void setPricingProfileId(int value )
	{
		 this.testChange("pricingProfileId", value);
		 this.pricingProfileId = value;
  }

   public void setLineOfBusinessId(int value )
	{
		 this.testChange("lineOfBusinessId", value);
		 this.lineOfBusinessId = value;
  }

  public void setCommitmentTerm(String  value)
	{
		 this.testChange("commitmentTerm", value);
		this.commitmentTerm = value;
  }

   public void setMPShortName(String value)
	{
	 this.testChange("mpShortName", value);
	 this.mpShortName = value;
  }

  public void setMPBusinessId(String value)
	{
	 this.testChange("mpBusinessId", value);
	 this.mpBusinessId = value;
  }
  //--Ticket#1736--18July2005--start--//
  //public int getTeaserTerm(){return teaserTerm;}
  //public void setTeaserTerm(int value)
  //{
  // this.testChange("teaserTerm", value);
  // this.teaserTerm = value;
  //}

  //public double getTeaserDiscount(){return teaserDiscount;}
  //public void setTeaserDiscount(double value)
  //{
  // this.testChange("teaserDiscount", value);
  // this.teaserDiscount = value;
  //}
  //--Ticket#1736--18July2005--end--//

  public void setInterestCompoundingId(int value)
  {
   this.testChange("interestCompoundingId", value);
   this.interestCompoundingId = value;
  }

  public double getMiscellaneousRate(){return miscellaneousRate;}
  public void setMiscellaneousRate(double value)
  {
   this.testChange("miscellaneousRate", value);
   this.miscellaneousRate = value;
  }

  public int getRateGuaranteePeriod()
  {
      return this.rateGuaranteePeriod;
  }
  
  public void setRateGuaranteePeriod(int rateguarperiod)
  {
      this.testChange("rateGuaranteePeriod", rateguarperiod);
      this.rateGuaranteePeriod = rateguarperiod;
  }

  public int getAdvanceRateFloatDownPeriod()
  {
      return this.advanceRateFloatDownPeriod;
  }
  
  public void setAdvanceRateFloatDownPeriod(int advanceRfd)
  {
      this.testChange("advanceRateFloatDownPeriod", advanceRfd);
      this.advanceRateFloatDownPeriod = advanceRfd;
  }

  public int getRateFloatDownTypeId()
  {
      return this.rateFloatDownTypeId;
  }
  
  public void setRateFloatDownTypeId(int rfdId)
  {
      this.testChange("rateFloatDownTypeId", rfdId);
      this.rateFloatDownTypeId = rfdId;
  }

  
  
	//***** Change by NBC Impl. Team - Version 1.2 - End *****//   
	/**
	 * @return Returns the sourceOfBusinessCategoryId.
	 */
	public int getSourceOfBusinessCategoryId() {
		return sourceOfBusinessCategoryId;
	}
	
	
	/**
	 * @param sourceOfBusinessCategoryId The sourceOfBusinessCategoryId to set.
	 */
	public void setSourceOfBusinessCategoryId(int sourceOfBusinessCategoryId) 
	{
		this.testChange("sourceOfBusinessCategoryId", sourceOfBusinessCategoryId);
		this.sourceOfBusinessCategoryId = sourceOfBusinessCategoryId;
	}
	
	
	/**
	 * @return Returns the affiliationProgramId.
	 */
	public int getAffiliationProgramId() {
        return affiliationProgramId;
	}
	
	
	/**
	 * @param affiliationProgramId The affiliationProgramId to set.
	 */
	public void setAffiliationProgramId(int affiliationProgramId) {
		this.testChange("affiliationProgramId", affiliationProgramId);
        this.affiliationProgramId = affiliationProgramId;
	}
	
	
	/**
	 * @return Returns the maxCashBackAmount.
	 */
	public double getMaxCashBackAmount() {
		return maxCashBackAmount;
	}
	
	
	/**
	 * @param maxCashBackAmount The maxCashBackAmount to set.
	 */
	public void setMaxCashBackAmount(double maxCashBackAmount) {
		this.testChange("maxCashBackAmount", maxCashBackAmount);
		this.maxCashBackAmount = maxCashBackAmount;
	}
	
	
	/**
	 * @return Returns the maxCashBackPercentage.
	 */
	public double getMaxCashBackPercentage() {
		return maxCashBackPercentage;
	}
	
	
	/**
	 * @param maxCashBackPercentage The maxCashBackPercentage to set.
	 */
	public void setMaxCashBackPercentage(double maxCashBackPercentage) {
		this.testChange("maxCashBackPercentage", maxCashBackPercentage);
		this.maxCashBackPercentage = maxCashBackPercentage;
	}
	
	
	/**
	 * @return Returns the maximumAmortization.
	 */
	public int getMaximumAmortization() {
		return maximumAmortization;
	}
	
	
	/**
	 * @param maximumAmortization The maximumAmortization to set.
	 */
	public void setMaximumAmortization(int maximumAmortization) {
		this.testChange("maximumAmortization", maximumAmortization);
		this.maximumAmortization = maximumAmortization;
	}
	
	
	/**
	 * @return Returns the minimumAmortization.
	 */
	public int getMinimumAmortization() {
		return minimumAmortization;
	}
	
	
	/**
	 * @param minimumAmortization The minimumAmortization to set.
	 */
	public void setMinimumAmortization(int minimumAmortization) {
		this.testChange("minimumAmortization", minimumAmortization);
		this.minimumAmortization = minimumAmortization;
	}
	//***** Change by NBC Impl. Team - Version 1.2 - End *****//   
    
    //***** Change by NBC Impl. Team - Version 1.0 - Start *****//   

    /**
     * Determines whether the product is a valid rate float down type
     * 
     * @return boolean
     *          - true - if product is a valid rate float down type
     *          - false - if product is not a valid rate float down type
     */
    public boolean isValidRateFloatDown()
    {
        boolean isRfd = false;

        if ((rateFloatDownTypeId == Mc.RATE_FLOAT_DOWN_TYPE_UCURVE || rateFloatDownTypeId == Mc.RATE_FLOAT_DOWN_TYPE_VCURVE) &&
                rateGuaranteePeriod > 0)
        {
            isRfd = true;
        }
        
        return isRfd;
    }
    //***** Change by NBC Impl. Team - Version 1.0 - End *****//   


    public int getInstitutionProfileId() {
        return institutionProfileId;
    }


    public void setInstitutionProfileId(int institutionProfileId) {
        testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
    
    /**************************MCM Impl Team Starts XS_16.7*************************/
    public void setComponentEligibleFlag(String componentEligibleFlag) {
        testChange("componentEligibleFlag", componentEligibleFlag);
        this.componentEligibleFlag = componentEligibleFlag;
    }
    public String getComponentEligibleFlag() {
		return componentEligibleFlag;
	}
    /**************************MCM Impl Team Ends XS_16.7*************************/
    
    /**************************MCM Impl Team Starts XS_11.2*************************/
    public void setUnderwriteAsTypeId(int underwriteAsTypeId) {
        testChange("underwriteAsTypeId", underwriteAsTypeId);
        this.underwriteAsTypeId = underwriteAsTypeId;
    }
    public int getUnderwriteAsTypeId() {
    return underwriteAsTypeId;
  }
    /**************************MCM Impl Team Ends XS_11.2*************************/
    
    /**************************MCM Impl Team Starts XS_16.17*************************/
    public void setProductTypeId(int producttypeId) {
        testChange("producttypeId", producttypeId);
        this.producttypeId = producttypeId;
    }
    public int getProductTypeId() {
    return producttypeId;
  }
    /**************************MCM Impl Team Ends XS_16.17*************************/

    /**************************MCM Impl Team Ends XS_16.23*************************/    

    /**
     * <p>
     * Description: Returns the component Type Id.
     * </p>
     * @version 1.0 XS_16.23 Initial Version
     * @return the componentTypeId
     */
    public int getComponentTypeId()
    {
        return componentTypeId;
    }

    /**
     * <p>
     * Description: Sets the component Type.
     * </p>
     * @version 1.0 XS_16.23 Initial Version
     * @param componentTypeId
     *            the component Type Id to set
     */
    public void setComponentTypeId(int componentTypeId)
    {
        this.testChange("componentTypeId", componentTypeId);
        this.componentTypeId = componentTypeId;
    }
    /**************************MCM Impl Team Ends XS_16.23*************************/
    
    /*******************Mcm Imp Team Starts version1.7***************/
    
    /**
     * <p>
     * Description: Returns the Repayment Type Id.
     * </p>
     * @version 1.0 Initial Version
     * @return the repaymentTypeId
     */
    public int getRepaymentTypeId()
    {
      return repaymentTypeId;
    }
    
    /**
     * <p>
     * Description: Sets the Repayment Type Id.
     * </p>
     * @version 1.0 Initial Version
     * @param repaymentId
     *            the Repayment Type Id to set
     */
    public void setRepaymentTypeId(int repaymentId)
    {
      this.testChange("repaymentTypeId", repaymentId);
      this.repaymentTypeId = repaymentId;
    }
    /*******************Mcm Imp Team Ends version1.7***************/
    
    
    //Qualify Rate  
    public String getQualifyingRateEligibleFlag()
    {
      return qualifyingRateEligibleFlag;
    }

    public void setQualifyingRateEligibleFlag(String flag)
    {
      this.testChange("qualifyingRateEligibleFlag", flag);
      this.qualifyingRateEligibleFlag = flag;
    }
}
