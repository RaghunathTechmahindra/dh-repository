
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PrimeIndexRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

// --Ticket#781--XD_ARM/VRM--19Apr2005--start--//

public class PrimeIndexRateInventory extends DealEntity {

    protected int primeIndexRateInventoryId;
    protected int primeIndexRateProfileId;
    protected double primeIndexRate;
    protected double primeIndexMargin;
    protected Date primeIndexEffDate;
    protected Date primeIndexExpDate;
    protected int institutionProfileId;
    protected PrimeIndexRateInventoryPK pk;

    public PrimeIndexRateInventory(SessionResourceKit srk)
            throws RemoteException, FinderException {

        super(srk);
    }

    public PrimeIndexRateInventory(SessionResourceKit srk, int id)
            throws RemoteException, FinderException {

        super(srk);

        pk = new PrimeIndexRateInventoryPK(id);

        findByPrimaryKey(pk);
    }

    public PrimeIndexRateInventory findByPrimaryKey(PrimeIndexRateInventoryPK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from PrimeIndexRateInventory where "
                + pk.getName() + " = " + pk.getId();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key, false);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public List findByPrimeIndexRateProfileId(int id) throws Exception {

        List found = new ArrayList();
        StringBuffer sql = new StringBuffer(
                "Select * from PrimeIndexRateInventory where ");
        sql.append("primeIndexRateProfileId = " + id);

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                PrimeIndexRateInventory pri = new PrimeIndexRateInventory(srk);
                pri.setPropertiesFromQueryResult(key, false);
                found.add(pri);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "PrimeIndexRateInventory Entity - findByPrimeRateProfileId() exception for "
                            + this.getClass().getName());

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);
            throw fe;
        }

        return found;
    }

    private void setPropertiesFromQueryResult(int key, boolean deep)
            throws Exception {
        this.setPrimeIndexRateInventoryId(jExec.getInt(key,
                "PrimeIndexRateInventoryId"));
        this.setPrimeIndexRateProfileId(jExec.getInt(key,
                "PrimeIndexRateProfileId"));
        this.setPrimeIndexRate(jExec.getDouble(key, "PrimeIndexRate"));
        this.setPrimeIndexMargin(jExec.getDouble(key, "PrimeIndexMargin"));
        this.setPrimeIndexEffDate(jExec.getDate(key, "PrimeIndexEffDate"));
        this.setPrimeIndexExpDate(jExec.getDate(key, "PrimeIndexExpDate"));
        this.setInstitutionProfileId(jExec.getInt(key, "InstitutionProfileId"));
    }

    /**
     * gets the value of instace fields as String. if a field does not exist (or
     * the type is not serviced)** null is returned. if the field exists (and
     * the type is serviced) but the field is null an empty String is returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();

        return (doPerformUpdate(this, clazz));
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    public String getEntityTableName() {

        return "PrimeIndexRateInventory";
    }

    /** all the gets */
    public double getPrimeIndexRate() {

        return this.primeIndexRate;
    }

    public double getPrimeIndexMargin() {

        return this.primeIndexMargin;
    }

    public int getPrimeIndexRateProfileId() {

        return this.primeIndexRateProfileId;
    }

    public int getPrimeIndexRateInventoryId() {

        return this.primeIndexRateInventoryId;
    }

    public Date getPrimeIndexEffDate() {

        return this.primeIndexEffDate;
    }

    public Date getPrimeIndexExpDate() {

        return this.primeIndexExpDate;
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a PrimeIndexRateInventoryPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public PrimeIndexRateInventoryPK createPrimaryKey() throws CreateException {

        String sql = "Select PrimeIndexRateInventoryseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PrimeIndexRateInventory Entity create() exception getting PrimeIndexRateInventoryId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        return new PrimeIndexRateInventoryPK(id);
    }

    /**
     * creates a PrimeIndexRateInventory using the PrimeIndexRateInventoryId
     * with minimum fields
     * 
     */

    public PrimeIndexRateInventory create(PrimeIndexRateInventoryPK pk)
            throws RemoteException, CreateException {

        String sql = "Insert into PrimeIndexRateInventory(" + pk.getName()
                + ",INSTITUTIONPROFILEID ) Values ( " + pk.getId() + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityCreate();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PrimeIndexRateInventory Entity - PrimeIndexRateInventory - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        this.pk = pk;

        return this;
    }

    /**
     * creates a PrimeIndexRateInventory using the PrimeIndexRateInventoryPK and
     * PrimeIndexRateProfilePK with minimal number of fields.
     * 
     */
    public PrimeIndexRateInventory create(PrimeIndexRateInventoryPK pk,
            PrimeIndexRateProfilePK ppk) throws RemoteException,
            CreateException {

        String sql = "Insert into PrimeIndexRateInventory( primeIndexRateInventoryID, primeIndexRateProfileID,INSTITUTIONPROFILEID ) Values ( "
                + pk.getId()
                + ", "
                + ppk.getId()
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            trackEntityChange();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "PrimeIndexRateInventory Entity - PrimeIndexRateInventory - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }
        srk.setModified(true);
        return this;
    }

    // ---------------- All the sets ------------------------------------
    //
    public void setPrimeIndexRate(double val) {

        this.testChange("primeIndexRate", val);
        this.primeIndexRate = val;
    }

    public void setPrimeIndexMargin(double val) {

        this.testChange("primeIndexMargin", val);
        this.primeIndexMargin = val;
    }

    public void setPrimeIndexRateProfileId(int val) {

        this.testChange("primeIndexRateProfileId", val);
        this.primeIndexRateProfileId = val;
    }

    public void setPrimeIndexRateInventoryId(int val) {

        this.testChange("primeIndexRateInventoryId", val);
        this.primeIndexRateInventoryId = val;
    }

    public void setPrimeIndexEffDate(Date val) {

        this.testChange("primeIndexEffDate", val);
        this.primeIndexEffDate = val;
    }

    public void setPrimeIndexExpDate(Date val) {

        this.testChange("primeIndexExpDate", val);
        this.primeIndexExpDate = val;
    }
}
