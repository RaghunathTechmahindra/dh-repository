/**
 * <p>Title: MortgageInsurer.java</p>
 *
 * <p>MortgageInsurer: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Hiroyuki Kobayashi
 * @version 1.0 (Initial Version � Jun 20, 2007)
 *
 */

package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.MortgageInsurerPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class MortgageInsurer
    extends DealEntity
{
  private static final Log _log = LogFactory.getLog(MortgageInsurer.class);

  protected int mortgageInsurerId;
  protected String mortgageInsurerName;
  protected String datxMIPremiumSupported;
  protected String datxMISupported;
  protected int serviceProviderId;
  protected int distributionPercentage;
  protected String supportForXTDPayload;

  private MortgageInsurerPK pk;

  public MortgageInsurer(SessionResourceKit srk)
      throws RemoteException, FinderException
  {
    super(srk);
  }

  public MortgageInsurer(SessionResourceKit srk, int id)
      throws RemoteException, FinderException
  {
    super(srk);

    pk = new MortgageInsurerPK(id);

    findByPrimaryKey(pk);
  }

  public MortgageInsurer findByPrimaryKey(MortgageInsurerPK pk)
      throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from MortgageInsurer " + pk.getWhereClause();

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key);)
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if (gotRecord == false)
      {
        String msg = "MortgageInsurer: @findByPrimaryKey(), key= "
            + pk + ", entity not found";
        _log.error(msg);
        throw new FinderException(msg);
      }
    }
    catch (Exception e)
    {
      FinderException fe = new FinderException(
          "MortgageInsurer Entity - findByPrimaryKey() exception");

      _log.error(fe.getMessage());
      _log.error("finder sql: " + sql);
      _log.error(e);

      throw fe;
    }
    this.pk = pk;
    ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
    return this;
  }

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
    int index = 0;
    setMortgageInsurerId(jExec.getInt(key, ++index));
    setMortgageInsurerName(jExec.getString(key, ++index));
    setDatxMIPremiumSupported(jExec.getString(key, ++index));
    setDatxMISupported(jExec.getString(key, ++index));
    setServiceProviderId(jExec.getInt(key, ++index));
    setDistributionPercentage(jExec.getInt(key, ++index));
    setSupportForXTDPayload(jExec.getString(key, ++index));
  }

//  private MortgageInsurerPK createPrimaryKey() throws CreateException
//  {
//    String sql = "Select MORTGAGEINSURERSEQ.nextval from dual";
//    int id = -1;
//
//    try
//    {
//      int key = jExec.execute(sql);
//
//      for (; jExec.next(key);)
//      {
//        id = jExec.getInt(key, 1); // can only be one record
//      }
//
//      jExec.closeData(key);
//      if (id == -1) throw new Exception();
//
//    }
//    catch (Exception e)
//    {
//      CreateException ce = new CreateException(
//          "MortgageInsurer Entity create() exception getting MortgageInsurerId from sequence");
//      logger.error(ce.getMessage());
//      logger.error(e);
//      throw ce;
//
//    }
//
//    return new MortgageInsurerPK(id);
//  }

  public String getDatxMIPremiumSupported()
  {
    return datxMIPremiumSupported;
  }

  public void setDatxMIPremiumSupported(String datxMIPremiumSupported)
  {
    this.testChange("datxMIPremiumSupported", datxMIPremiumSupported);
    this.datxMIPremiumSupported = datxMIPremiumSupported;
  }

  public String getDatxMISupported()
  {
    return datxMISupported;
  }

  public void setDatxMISupported(String datxMISupported)
  {
    this.testChange("datxMISupported", datxMISupported);
    this.datxMISupported = datxMISupported;
  }

  public int getDistributionPercentage()
  {
    return distributionPercentage;
  }

  public void setDistributionPercentage(int distributionPercentage)
  {
    this.testChange("distributionPercentage", distributionPercentage);
    this.distributionPercentage = distributionPercentage;
  }

  public int getMortgageInsurerId()
  {
    return mortgageInsurerId;
  }

  public void setMortgageInsurerId(int mortgageInsurerId)
  {
    this.testChange("mortgageInsurerId", mortgageInsurerId);
    this.mortgageInsurerId = mortgageInsurerId;
  }

  public String getMortgageInsurerName()
  {
    return mortgageInsurerName;
  }

  public void setMortgageInsurerName(String mortgageInsurerName)
  {
    this.testChange("mortgageInsurerName", mortgageInsurerName);
    this.mortgageInsurerName = mortgageInsurerName;
  }

  public int getServiceProviderId()
  {
    return serviceProviderId;
  }

  public void setServiceProviderId(int serviceProviderId)
  {
    this.testChange("serviceProviderId", serviceProviderId);
    this.serviceProviderId = serviceProviderId;
  }
  
  public String getSupportForXTDPayload() {
      return supportForXTDPayload;
  }

  public void setSupportForXTDPayload(String supportForXTDPayload) {
      this.testChange("supportForXTDPayload", supportForXTDPayload);
      this.supportForXTDPayload = supportForXTDPayload;
  }

  public Collection findMortgageInsurersNotUsed(int dealId) throws Exception
  {
    
    ArrayList list = new ArrayList();
    StringBuffer sql = new StringBuffer();
    sql.append("SELECT ");
    sql.append("    MI.MORTGAGEINSURERID, MI.MORTGAGEINSURERNAME, ");
    sql.append("    MI.DATXMIPREMIUMSUPPORTED, MI.DATXMISUPPORTED, ");
    sql.append("    MI.SERVICEPROVIDERID, MI.DISTRIBUTIONPERCENTAGE ");
    sql.append("  FROM ");
    sql.append("    MORTGAGEINSURER MI");
    sql.append("  WHERE ");
    sql.append("    NOT EXISTS (");
    sql.append("      SELECT 1 ");
    sql.append("        FROM MIDISTRIBUTIONHISTORY MDH ");
    sql.append("       WHERE MDH.MORTGAGEINSURERID = MI.MORTGAGEINSURERID ");
    sql.append("         AND MDH.DEALID = ").append(dealId);
    sql.append("    ) AND MI.DISTRIBUTIONPERCENTAGE > 0 ");
    sql.append("  ORDER BY MI.MORTGAGEINSURERID ");
    
    try
    {
      int key = jExec.execute(sql.toString());
      for (; jExec.next(key);)
      {
        MortgageInsurer mi = new MortgageInsurer(srk);
        mi.setPropertiesFromQueryResult(key);
        mi.pk = new MortgageInsurerPK(mi.getMortgageInsurerId());
        list.add(mi);
      }
      jExec.closeData(key);

    }
    catch (Exception e)
    {
      _log.error(e.getMessage());
      _log.error("Error in collection finder - sql: " + sql);
      _log.error(e);

      throw e;
    }
    return list;
  }

  //5.0 MI
    public List<Integer> findMITypesByMortgageInsurer(int mortgageInsurerID)
            throws Exception {
        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT MORTGAGEINSURANCETYPEID ");
        sql.append("  FROM MORTGAGEINSURANCETYPEASSOC ");
        sql.append(" WHERE MORTGAGEINSURERID = ").append(mortgageInsurerID);
        sql.append(" ORDER BY MORTGAGEINSURANCETYPEID");

        List<Integer> list = new ArrayList<Integer>();
        try {

            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {
                Integer mitypeid = jExec.getInteger(key, 1);
                if (mitypeid != null) {
                    list.add(mitypeid);
                }
            }
            jExec.closeData(key);

        } catch (Exception e) {
            _log.error(e.getMessage());
            _log.error("Error happened - sql: " + sql.toString());
            _log.error(e);
            throw e;
        }
        return list;
    }
  
  public String toString()
  {
    //for debug
    String str = "mortgageInsurerId=" + mortgageInsurerId;
    str = str + ",mortgageInsurerName=" + mortgageInsurerName;
    str = str + ",datxMIPremiumSupported=" + datxMIPremiumSupported;
    str = str + ",datxMISupported=" + datxMISupported;
    str = str + ",serviceProviderId=" + serviceProviderId;
    str = str + ",distributionPercentage=" + distributionPercentage;
    str = str + ",supportForXTDPayload=" + supportForXTDPayload;
    
    return str;
  }


  
  
}
