package com.basis100.deal.entity;

import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Vector;

import oracle.jdbc.driver.OracleCallableStatement;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.CreditBureauReportPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;


public class CreditBureauReport extends DealEntity
{
   protected int     creditBureauReportId;
   protected String  creditReport;
   protected int     dealId;
   
   protected int     institutionId;

   private CreditBureauReportPK pk;

   public CreditBureauReport(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }


   public CreditBureauReport(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new CreditBureauReportPK(id);

      findByPrimaryKey(pk);
   }


   private CreditBureauReportPK createPrimaryKey()throws CreateException
   {
       String sql = "Select CreditBureauReportseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
               this.trackEntityCreate ();
           }

           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {
            CreateException ce = new CreateException("CreditBureauReport Entity create() exception getting CreditBureauReportId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;

        }

        return new CreditBureauReportPK(id);
   }


  public CreditBureauReport findByPrimaryKey(CreditBureauReportPK pk) throws RemoteException, FinderException
  {
	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
    String sql = "Select * from CreditBureauReport where " + pk.getName()+ " = " + pk.getId();

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          gotRecord = true;
          setPropertiesFromQueryResult(key);
          break; // can only be one record
        }

        jExec.closeData(key);

        if (gotRecord == false)
        {
          String msg = "CreditBureauReport Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
          logger.error(msg);
          throw new FinderException(msg);
        }
      }
      catch (Exception e)
      {
        FinderException fe = new FinderException("CreditBureauReport Entity - findByPrimaryKey() exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
      this.pk = pk;
      ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
      return this;
  }

  public Collection findByDealid(int dealid) throws Exception
  {
    Collection reports = new Vector();

    String sql = "Select * from CreditBureauReport where dealId = " ;
        sql +=  dealid;

    try
    {

      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        CreditBureauReport cb = new CreditBureauReport(srk);

        cb.setPropertiesFromQueryResult(key);
        cb.pk = new CreditBureauReportPK(cb.getCreditBureauReportId());

        reports.add(cb);
      }


      jExec.closeData(key);

    }
    catch (Exception e)
    {
      Exception fe = new Exception("Exception caught while fetching Deal::Properties");

      logger.error(fe.getMessage());
      logger.error(e);

      throw fe;
    }

    return reports;
  }

  public Collection findByDeal(DealPK pk) throws Exception
  {

    Collection reports = new Vector();

    String sql = "Select * from CreditBureauReport where dealId = " ;
        sql +=  pk.getId();

    try
    {

      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        CreditBureauReport cb = new CreditBureauReport(srk);

        cb.setPropertiesFromQueryResult(key);
        cb.pk = new CreditBureauReportPK(cb.getCreditBureauReportId());

        reports.add(cb);
      }


      jExec.closeData(key);

    }
    catch (Exception e)
    {
      Exception fe = new Exception("Exception caught while fetching Deal::Properties");

      logger.error(fe.getMessage());
      logger.error(e);

      throw fe;
    }

    return reports;
  }


   private void setPropertiesFromQueryResult(int key) throws Exception
   {
         this.setCreditBureauReportId(jExec.getInt(key, "CREDITBUREAUREPORTID"));
         this.setDealId(jExec.getInt(key, "DEALID"));
         //this.setCreditReport(readReport());
         this.setCreditReport(readClob(jExec.getClob(key, "CREDITREPORT"))); 
         this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID")); 
   }


  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

  /**
  *  Updates any Database field that has changed since the last synchronization
  *  ie. the last findBy... call
  *
  *  @return the number of updates performed
  */
  protected int performUpdate()  throws Exception
  {
    writeReport();
    return 1;
  }

   public String getEntityTableName()
   {
     return "CreditBureauReport";
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a CreditBureauReportPK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

  /**
   *   gets the creditBureauReportId associated with this entity
   *
   *   @return the id of this creditReport
   */
    public int getCreditBureauReportId()
   {
      return this.creditBureauReportId ;
   }
  /**
   *   gets the creditReport associated with this entity
   *
   *   @return a String credit report
   */
    public String getCreditReport()
   {
      return this.creditReport ;
   }

   /**
   *   gets the dealId associated with this entity
   *
   *   @return the dealid as int
   */
    public int getDealId()
   {
      return this.dealId;
   }

    /**
    *   <pre>
    *    create a CreditBureauReport record given an id (primary key) and a description
    *    @param id the primary key for this record
    *    @param desc the description of this CreditBureauReport object
    *    @return this instance
    *   </pre>
    */
   public CreditBureauReport create(DealPK dpk)throws RemoteException, CreateException
   {
      pk = createPrimaryKey();

      String sql = "Insert into CreditBureauReport(creditBureauReportId , dealId, creditReport, institutionProfileId ) ";
      sql += " Values ( " + pk.getId() + ", " + dpk.getId() + ", " + "empty_clob()" + ", " 
             + srk.getExpressState().getDealInstitutionId() + ")";

      try
      {
        jExec.executeUpdate(sql);
        findByPrimaryKey(pk);
        trackEntityCreate();
      }
      catch (Exception e)
      {
        CreateException ce = new CreateException("CreditBureauReport Entity - CreditBureauReportType - create() exception");
        logger.error(ce.getMessage());
        logger.error(e);

        throw ce;
      }
      srk.setModified(true);
      return this;
   }

   /**
    *  Writes the current creditReport String to the database column. Method logic is
    *  predicated on a read having been performed during findBy or create.
    */
   private void writeReport()
   {
     //no report is the same as an empty report?
      if(this.creditReport == null )
       this.creditReport = "";

      //========================================================================
      // bug fix - dealing with crditbureaureport created opened cursors in the
      // database. The following statement makes sure that the connection closes.
      //========================================================================
      srk.setCloseJdbcConnectionOnRelease(true);

      JdbcExecutor je =  srk.getJdbcExecutor();
      Connection con = je.getCon();
      ResultSet rs = null;
      Statement stm = null;
	  // SEAN Ticket #1381 Sept 19, 2005: 
	  Writer writer = null;
	  // SEAN Ticket #1381 END

      try
      {
        oracle.sql.CLOB clob = null;

        //get insert a new locator
        stm = con.createStatement() ;
        //String st1 = "Update creditbureaureport set creditReport = empty_clob()";
        String st1 = "Update creditbureaureport set creditReport = empty_clob() where CreditBureauReportId = ";
        st1 +=  getCreditBureauReportId();
        st1 += " AND dealId = " + getDealId();

        stm.executeUpdate(st1);

        String st2 = "Select creditReport from CreditBureauReport ";
        st2 += "where CreditBureauReportId = " + getCreditBureauReportId();
        st2 += " AND dealId = " + getDealId() + " FOR UPDATE";

        rs = stm.executeQuery(st2);

        if (rs.next())
        {
          clob = (oracle.sql.CLOB)rs.getObject(1);

          if(clob == null)
           throw new Exception("CreditBureauReport: Could not obtain LOB locator: " + this.pk);

		  // SEAN Ticket #1381 Sept. 19, 2005: Change from Ascii stream to Unicode Stream. to keep the report format.
		  writer = clob.getCharacterOutputStream();
		  // SEAN Ticket #1381 END
        }

         rs.close();
         stm.close();

		 // SEAN Ticket #1381 Sept. 19, 2005
		 writer.write(getCreditReport());
		 writer.flush();
		 writer.close();
		 // SEAN Ticket #1381 END
      }
      catch(Exception e)
      {
        e.printStackTrace();
        logger.error("CreditBureauReport: failed to write data to report: " + this.getPk());
        logger.info("In Transaction: " + je.isInTransaction());

        String msg = "";
        if(e.getMessage() == null)
         msg = "Unknown Error Occurred";
        logger.error("CreditBureauReport: Reason for failure: " + msg);

        logger.error(e);

        try{ rs.close();      }catch(Exception ea){;}
        try{ stm.close();     }catch(Exception eb){;}
        try{ writer.close();  }catch(Exception ec){;}
      } finally {
          try {
              if(rs != null) rs.close();
          } catch (Exception ignore){}
          try {
              if(stm != null) stm.close();
          } catch (Exception ignore){}
      }

   }

   @Deprecated
   private String readReport()
   {
      JdbcExecutor je =  srk.getJdbcExecutor();
      Connection con = je.getCon();
      String outval = "";
      ResultSet rs = null;
      Statement stm = null;
      OracleCallableStatement callableStmt = null;

      try
      {

        String stmt = "Select creditReport, dealid from CreditBureauReport ";
        stmt += "where CreditBureauReportId = " + this.getCreditBureauReportId();
        stmt += " AND dealId = " + this.getDealId();

        oracle.sql.CLOB clob = null;

        stm = con.createStatement() ;
        rs = stm.executeQuery(stmt);

        if (rs.next())
        {
          clob = (oracle.sql.CLOB)rs.getObject(1);

          if(clob == null)
           return outval;

          if(clob.length() <= 0)
            return outval;

          //BUG fix ==> There is the limitation the return value of Stored Procedure < 32K
          //      Modified to read data in a loop of 30K at a time.
          //By Billy 07Jan2003
          //--> Bug fix by Billy 14Feb2003
          callableStmt =
           (OracleCallableStatement)con.prepareCall("begin dbms_LOB.read(?,?,?,?); end;");
          long totalLength = clob.length();
          long i = 0;             // index at which to read
          int chunk = 30*1024;    // size of the chunk to read :: Oracle limit is 30K
          long read_this_time = 0;

          while (i < totalLength)
          {
            callableStmt.clearParameters();
            callableStmt.setCLOB(1,clob);
            callableStmt.setLong(2,chunk); //size of chunk
            callableStmt.registerOutParameter(2,Types.NUMERIC); //Bytes received
            callableStmt.setLong(3, i+1); // begin reading at the offset
            callableStmt.registerOutParameter (4,Types.LONGVARCHAR);
            callableStmt.execute ();

            read_this_time = callableStmt.getLong (2);
            outval += callableStmt.getString(4);
            i += read_this_time;
          }
          callableStmt.close();
        }

        rs.close();
        stm.close();
      }
      catch(Exception e)
      {
        logger.error("CreditBureauReport: failed to read data from report: " + this.getPk());
        logger.info("In Transaction: " + je.isInTransaction());

        String msg = "";

        if(e.getMessage() == null)
         msg = "Unknown Error Occurred";

        logger.error("CreditBureauReport: Reason for failure: " + msg);

        logger.error(e);

      }finally{
          try {
              if(rs != null) rs.close();
          } catch (Exception ignore){}
          try {
              if(stm != null) stm.close();
          } catch (Exception ignore){}
          try {
              if(callableStmt != null) callableStmt.close();
          } catch (Exception ignore){}
      }

     return outval;

  }


     /**
   *  sets the value of instance fields as String.
   *  if a field does not exist or is not serviced an Exception is thrown
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *
   *  @param  fieldName as String
   *  @param  the value as a String
   *
   *  Other entity types are not yet serviced   ie. You can't set the Province object
   *  in Addr.
   */
   public void setField(String fieldName, String value) throws Exception
   {
      doSetField(this, this.getClass(), fieldName, value);
   }


   public void setCreditBureauReportId(int id  )
	 {
     this.testChange ("creditBureauReportId", id );
		 this.creditBureauReportId = id;
   }

   public void setCreditReport(String report)
	 {
     this.testChange ("creditReport", report );
		 this.creditReport = report;
   }

   public void appendCreditReport(String report)
	 {

     String value = this.getCreditReport();

     if(value == null)
      value = "";

     value += /*System.getProperty("line.separator")+ */ report;

     this.testChange ("creditReport", value );

		 this.creditReport = value;
   }

   public void setDealId(int id)
	 {
     this.testChange ("dealId", id );
		 this.dealId = id;
   }

   public int getClassId(){
    return ClassId.CREDITBUREAUREPORT;
   }


  /**
   * @return the institutionId
   */
  public int getInstitutionId()
  {
    return institutionId;
  }


  /**
   * @param institutionId the institutionId to set
   */
  public void setInstitutionId(int institutionId)
  { this.testChange("institutionProfileId", institutionId);
    this.institutionId = institutionId;
  }
}
