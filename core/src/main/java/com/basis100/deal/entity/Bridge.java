package com.basis100.deal.entity;

import java.util.Date;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BridgePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;

public class Bridge extends DealEntity {
    protected int bridgeId;

    protected int investorProfileId;

    protected int lenderProfileId;

    protected int bridgePurposeId;

    protected int interestTypeId;

    protected int paymentTermId;

    protected int pricingProfileId;

    protected double discount;

    protected int actualPaymentTerm;

    protected double postedInterestRate;

    protected double netInterestRate;

    protected double netLoanAmount;

    protected double postedRate;

    protected Date maturityDate;

    protected Date rateDate;

    protected double premium;

    protected int dealId;

    protected int copyId;
    
    protected int institutionId;

    private BridgePK pk;

    public Bridge(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException {
        super(srk, dcm);
    }

    public Bridge(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId)
            throws RemoteException, FinderException {
        super(srk, dcm);

        pk = new BridgePK(id, copyId);

        findByPrimaryKey(pk);
    }

    public Bridge findByPrimaryKey(BridgePK pk) throws RemoteException,
            FinderException {
    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from Bridge " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Bridge Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Bridge Entity: @findByPrimaryKey(), key= " + pk
                            + ", entity not found");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public Bridge findByDeal(DealPK pk) throws Exception {

        String sql = "Select * from Bridge " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);

                this.pk = new BridgePK(this.getBridgeId(), this.getCopyId());

                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Bridge Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                return null;
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Bridge Entity: Exception @findByPrimaryKey(), key= " + pk
                            + ", entity not found");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

        this.setBridgeId(jExec.getInt(key, "BRIDGEID"));
        this.setInvestorProfileId(jExec.getInt(key, "INVESTORPROFILEID"));
        this.setLenderProfileId(jExec.getInt(key, "LENDERPROFILEID"));
        this.setBridgePurposeId(jExec.getInt(key, "BRIDGEPURPOSEID"));
        this.setInterestTypeId(jExec.getInt(key, "INTERESTTYPEID"));
        this.setPaymentTermId(jExec.getInt(key, "PAYMENTTERMID"));
        this.setPricingProfileId(jExec.getInt(key, "PRICINGPROFILEID"));
        this.setDiscount(jExec.getDouble(key, "DISCOUNT"));
        this.setActualPaymentTerm(jExec.getInt(key, "ACTUALPAYMENTTERM"));
        this.setPostedInterestRate(jExec.getDouble(key, "POSTEDINTERESTRATE"));
        this.setNetInterestRate(jExec.getDouble(key, "NETINTERESTRATE"));
        this.setNetLoanAmount(jExec.getDouble(key, "NETLOANAMOUNT"));
        this.setPostedRate(jExec.getDouble(key, "POSTEDRATE"));
        this.setMaturityDate(jExec.getDate(key, "MATURITYDATE"));
        this.setRateDate(jExec.getDate(key, "RATEDATE"));
        this.setPremium(jExec.getDouble(key, "PREMIUM"));
        this.setDealId(jExec.getInt(key, "DEALID"));
        this.copyId = jExec.getInt(key, "COPYID");
        this.setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {
        return doGetStringValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {
        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */

    protected int performUpdate() throws Exception {
        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);
        if (dcm != null && ret > 0)
            dcm.inputEntity(this);
        return ret;
    }

    public String getEntityTableName() {
        return "Bridge";
    }

    public IEntityBeanPK getPk() {
        return this.pk;
    }

    public int getBridgeId() {
        return this.bridgeId;
    }

    public int getActualPaymentTerm() {
        return this.actualPaymentTerm;
    }

    public int getBridgePurposeId() {
        return this.bridgePurposeId;
    }

    public double getDiscount() {
        return this.discount;
    }

    public double getPostedInterestRate() {
        return this.postedInterestRate;
    }

    public int getInterestTypeId() {
        return this.interestTypeId;
    }

    public int getInvestorProfileId() {
        return this.investorProfileId;
    }

    public int getLenderProfileId() {
        return this.lenderProfileId;
    }

    public Date getMaturityDate() {
        return this.maturityDate;
    }

    public double getNetInterestRate() {
        return this.netInterestRate;
    }

    public double getNetLoanAmount() {
        return this.netLoanAmount;
    }

    public int getPaymentTermId() {
        return this.paymentTermId;
    }

    public int getPricingProfileId() {
        return this.pricingProfileId;
    }

    public double getPostedRate() {
        return this.postedRate;
    }

    public Date getRateDate() {
        return this.rateDate;
    }

    public double getPremium() {
        return this.premium;
    }

    public int getCopyId() {
        return this.copyId;
    }

    public int getDealId() {
        return this.dealId;
    }

    BridgePK createPrimaryKey(int copyId) throws CreateException {
        String sql = "Select Bridgeseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1)
                throw new Exception();
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "Bridge Entity create() exception getting BridgeId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        return new BridgePK(id, copyId);
    }

    /**
     * creates a EmploymentHistory using the EmploymentHistory primary key and a
     * borrower primary key - the mininimum (ie.non-null) fields
     * 
     */

    public Bridge create(DealPK dpk) throws RemoteException, CreateException {
        pk = createPrimaryKey(dpk.getCopyId());

        String sql = "Insert into Bridge ( dealId, bridgeId, copyId,  "
                + " institutionProfileId ) Values ( "
                + dpk.getId()
                + ", "
                + pk.getId()
                + ", "
                + pk.getCopyId()
                + ", "
                + srk.getExpressState().getDealInstitutionId()
                + " )";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // Track the create
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "Bridge Entity - Bridge  - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);

        if (dcm != null)
            // dcm.inputEntity(this);
            dcm.inputCreatedEntity(this);

        return this;
    }

    // -------------All the sets --------------------------------
    public void setBridgeId(int id) {
        this.testChange("bridgeId", id);
        this.bridgeId = id;
    }

    public void setActualPaymentTerm(int id) {
        this.testChange("actualPaymentTerm", id);
        this.actualPaymentTerm = id;
    }

    public void setBridgePurposeId(int id) {
        this.testChange("bridgePurposeId", id);
        this.bridgePurposeId = id;
    }

    public void setDiscount(double id) {
        this.testChange("discount", id);
        this.discount = id;
    }

    public void setPostedInterestRate(double id) {
        this.testChange("postedInterestRate", id);
        this.postedInterestRate = id;
    }

    public void setInterestTypeId(int id) {
        this.testChange("interestTypeId", id);
        this.interestTypeId = id;
    }

    public void setInvestorProfileId(int id) {
        this.testChange("investorProfileId", id);
        this.investorProfileId = id;
    }

    public void setLenderProfileId(int id) {
        this.testChange("lenderProfileId", id);
        this.lenderProfileId = id;
    }

    public void setMaturityDate(Date id) {
        this.testChange("maturityDate", id);
        this.maturityDate = id;
    }

    public void setNetInterestRate(double id) {
        this.testChange("netInterestRate", id);
        this.netInterestRate = id;
    }

    public void setNetLoanAmount(double id) {
        this.testChange("netLoanAmount", id);
        this.netLoanAmount = id;
    }

    public void setPaymentTermId(int id) {
        this.testChange("paymentTermId", id);
        this.paymentTermId = id;
    }

    public void setPricingProfileId(int id) {
        this.testChange("pricingProfileId", id);
        this.pricingProfileId = id;
    }

    public void setPostedRate(double id) {
        this.testChange("postedRate", id);
        this.postedRate = id;
    }

    public void setRateDate(Date id) {
        this.testChange("rateDate", id);
        this.rateDate = id;
    }

    public void setPremium(double id) {
        this.testChange("premium", id);
        this.premium = id;
    }

    public void setDealId(int id) {
        this.testChange("dealId", id);
        this.dealId = id;
    }

    public int getClassId() {
        return ClassId.BRIDGE;
    }

    public boolean isAuditable() {
        return true;
    }

    public boolean equals(Object o) {
        if (o instanceof Bridge) {
            Bridge oa = (Bridge) o;
            if ((bridgeId == oa.getBridgeId()) && (copyId == oa.getCopyId()))
                return true;
        }
        return false;
    }

    protected EntityContext getEntityContext() throws RemoteException {
        EntityContext ctx = this.entityContext;

        try {
            ctx.setContextText(PicklistData.getDescription("BridgePurpose",
                    this.getBridgePurposeId()));
            ctx.setContextSource(String.valueOf(this.netLoanAmount)); // ??
                                                                        // BridgeAmount

            // Modified by Billy for performance tuning -- By BILLY 28Jan2002
            // Deal deal = new Deal( srk, null , this.dealId , this.copyId );
            // if ( deal == null )
            // return ctx;
            // String sc = String.valueOf ( deal.getScenarioNumber () ) +
            // deal.getCopyType();
            Deal deal = new Deal(srk, null);
            String sc = String.valueOf(deal.getScenarioNumber(this.dealId,
                    this.copyId))
                    + deal.getCopyType(this.dealId, this.copyId);

            ctx.setScenarioContext(sc);

            ctx.setApplicationId(this.dealId);
        } catch (Exception e) {
            if (e instanceof FinderException) {
                logger
                        .warn("Bridge.getEntityContext Parent Not Found. BridgeID= "
                                + this.bridgeId);
                return ctx;
            }
            throw (RemoteException) e;
        }
        return ctx;
    } // ------------------ End of getEntityContext
        // ---------------------------

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
        this.testChange("institutionProfileId", institutionId);
        this.institutionId = institutionId;
    }
}
