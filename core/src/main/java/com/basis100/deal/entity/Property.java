
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;

public class Property extends DealEntity {

    protected int dealId;
    protected int propertyId;
    protected int copyId;
    protected int streetTypeId;
    protected int propertyTypeId;
    protected int propertyUsageId;
    protected int dwellingStyleId;
    protected int occupancyTypeId;
    protected String propertyStreetNumber;
    protected String propertyStreetName;
    protected int heatTypeId;
    protected String propertyAddressLine2;
    protected String propertyCity;
    protected String propertyPostalFSA;
    protected String propertyPostalLDU;
    protected String legalLine1;
    protected String legalLine2;
    protected String legalLine3;
    protected int numberOfBedrooms;
    protected int yearBuilt;
    protected boolean insulatedWithUFFI;
    protected int provinceId;
    protected double monthlyCondoFees;
    protected double purchasePrice;
    protected int municipalityId;
    protected double landValue;
    protected String propertyValueSource;
    protected int appraisalSourceId;
    protected double equityAvailable;
    protected int newConstructionId;
    protected double estimatedAppraisalValue;
    protected double actualAppraisalValue;
    protected double loanToValue;
    protected String unitNumber;
    protected int lotSize;
    protected char primaryPropertyFlag;
    protected Date appraisalDateAct;
    protected int lotSizeDepth;
    protected int lotSizeFrontage;
    protected int lotSizeUnitOfMeasureId;
    protected int numberOfUnits;
    protected int propertyLocationId;
    protected int propertyOccurenceNumber;
    protected String propertyReferenceNumber;
    protected int sewageTypeId;
    protected int streetDirectionId;
    protected int waterTypeId;
    protected int livingSpace;
    protected int livingSpaceUnitOfMeasureId;
    protected int structureAge;
    protected int dwellingTypeId;
    protected double lendingValue;
    protected double totalPropertyExpenses;
    protected double totalAnnualTaxAmount;
    protected int zoning;
    // Additional fields for CMHC modification -- By BILLY 12Feb2002
    protected String MLSListingFlag;
    protected int garageSizeId;
    protected int garageTypeId;
    protected String builderName;
    // =============================================================

    // transient properties - not physical table columns
    //FXP27162
    private String provinceName; // from provinceId
    private String provinceAbbreviation; // from provinceId
    //End of FXP27162
    // ***** Change by NBC Impl. Team - Version 1.2 - End*****//
    protected int requestAppraisalId;
    // ***** Change by NBC Impl. Team - Version 1.2 - End*****//
    private boolean propagateChanges = false;

    private PropertyPK pk;

    // START - MI Additions
    protected int tenureTypeId;
    protected String miEnergyEfficiency;
    protected String onReserveTrustAgreementNumber;
    protected String subdivisionDiscount;
    // END - MI Additions

    protected int institutionProfileId;

    // SEAN issue #957
    public Property(SessionResourceKit srk) throws RemoteException,
            FinderException {

        super(srk);
    }

    // SEAN issue #957 END

    public Property(SessionResourceKit srk, CalcMonitor dcm)
            throws RemoteException, FinderException {

        super(srk, dcm);
    }

    public Property(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId)
            throws RemoteException, FinderException {

        super(srk, dcm);

        pk = new PropertyPK(id, copyId);

        findByPrimaryKey(pk);
    }

    public Collection<Property> findByDeal(DealPK pk) throws Exception {

        Collection<Property> properties = new Vector<Property>();

        String sql = "Select * from Property ";
        sql += pk.getWhereClause();
        sql += " order by PropertyOccurenceNumber";

        try {

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Property prop = new Property(srk, dcm);

                prop.setPropertiesFromQueryResult(key);
                prop.pk = new PropertyPK(prop.getPropertyId(), prop.getCopyId());

                properties.add(prop);
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while fetching Deal::Properties");

            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return properties;
    }

    // SEAN issue #957 added to find by deal id and primary property flag.
    public Collection findByDealIdAndPrimaryFlag(int dealId, char primaryFlag)
            throws Exception {

        Collection properties = new Vector();

        StringBuffer sql = new StringBuffer();
        sql.append("Select * from Property where ")
                .append("DEALID = " + dealId).append(
                        " and PRIMARYPROPERTYFLAG = '" + primaryFlag + "'");

        try {
            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {
                Property prop = new Property(srk, dcm);

                prop.setPropertiesFromQueryResult(key);
                prop.pk = new PropertyPK(prop.getPropertyId(), prop.getCopyId());

                properties.add(prop);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "Exception caught while fetching Deal::Properties");

            logger.error(fe.getMessage());
            logger.error(e);
            throw fe;
        }

        return properties;
    }

    // SEAN issue #957 END

    /**
     * 
     */
    public Collection findByDupeCheckCriteria(String propertyStreetNumber,
            String propertyStreetName, int streetTypeId, int streetDirectionId,
            String unitNumber, String city, int provinceId, Date minDate)
            throws Exception {

        // Modified to use table joint instead of IN(...) for better performance
        // -- By BILLY 27Feb2002
        StringBuffer sql = new StringBuffer(
                "Select p.* from Property p, Deal d where ");

        if (propertyStreetNumber == null) {
            sql.append(" p.propertyStreetNumber is null");
        } else {
            sql.append(" upper(p.propertyStreetNumber) = ").append(
                    sqlStringFrom(propertyStreetNumber.toUpperCase()));
        }

        if (propertyStreetName == null) {
            sql.append(" and p.propertyStreetName is null");
        } else {
            sql.append(" and upper(p.propertyStreetName) = ").append(
                    sqlStringFrom(propertyStreetName.toUpperCase()));
        }

        sql.append(" and p.streetTypeId  =  ").append(
                sqlStringFrom(streetTypeId));

        sql.append(" and p.streetDirectionId = ").append(
                sqlStringFrom(streetDirectionId));

        if (unitNumber == null) {
            sql.append(" and p.unitNumber is null");
        } else {
            sql.append(" and upper(p.unitNumber)  =  ").append(
                    sqlStringFrom(unitNumber.toUpperCase()));
        }

        if (city == null) {
            sql.append(" and p.propertyCity  is null");
        } else {
            sql.append(" and upper(p.propertyCity)  = ").append(
                    sqlStringFrom(city.toUpperCase()));
        }

        sql.append(" and p.provinceId =  ").append(provinceId);

        // sql.append(" AND dealId in( Select dealid from deal where copyType =
        // 'G' ");

        sql.append(" AND p.dealId = d.dealid and d.copyType = 'G' ");

        if (minDate != null) // if a date is supplied
        {
            sql.append(" AND  d.applicationDate > ").append(
                    sqlStringFrom(minDate));
        }

        sql.append(" order by p.dealid");

        List result = new ArrayList();

        String sqlString = sql.toString();

        try {
            int key = jExec.execute(sqlString);

            for (; jExec.next(key);) {
                Property prop = new Property(srk, dcm);

                prop.setPropertiesFromQueryResult(key);
                prop.pk = new PropertyPK(prop.getPropertyId(), prop.getCopyId());

                result.add(prop);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "Deal Entity - findByDupeCheckCriteria() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return result;
    }

    public Collection findByGenXDupeCheck(String propertyStreetNumber,
            String propertyStreetName, int streetTypeId, int streetDirectionId,
            String unitNumber, String city, int provinceId, Date minDate)
            throws Exception {

        StringBuffer sql = new StringBuffer(
                "Select p.* from Property p, Deal d where ");
        if (propertyStreetNumber == null) {
            sql.append(" p.propertyStreetNumber is null");
        } else {
            sql.append(" upper(p.propertyStreetNumber) = ").append(
                    sqlStringFrom(propertyStreetNumber.toUpperCase()));
        }

        if (propertyStreetName == null) {
            sql.append(" and p.propertyStreetName is null");
        } else {
            sql.append(" and upper(p.propertyStreetName) = ").append(
                    sqlStringFrom(propertyStreetName.toUpperCase()));
        }

        sql.append(" and p.streetTypeId  =  ").append(
                sqlStringFrom(streetTypeId));

        sql.append(" and p.streetDirectionId = ").append(
                sqlStringFrom(streetDirectionId));

        if (unitNumber == null) {
            sql.append(" and p.unitNumber is null");
        } else {
            sql.append(" and upper(p.unitNumber)  =  ").append(
                    sqlStringFrom(unitNumber.toUpperCase()));
        }

        if (city == null) {
            sql.append(" and p.propertyCity  is null");
        } else {
            sql.append(" and upper(p.propertyCity)  = ").append(
                    sqlStringFrom(city.toUpperCase()));
        }

        sql.append(" and p.provinceId =  ").append(provinceId);

        sql
                .append(" AND p.dealId = d.dealid and ( ( d.copyType = 'G' or d.copyType = 'S' ) and d.scenariorecommended = 'Y') ");

        if (minDate != null) // if a date is supplied
        {
            sql.append(" AND  d.applicationDate > ").append(
                    sqlStringFrom(minDate));
        }

        sql.append(" order by p.dealid");

        List result = new ArrayList();

        String sqlString = sql.toString();

        try {
            int key = jExec.execute(sqlString);

            for (; jExec.next(key);) {
                Property prop = new Property(srk, dcm);

                prop.setPropertiesFromQueryResult(key);
                prop.pk = new PropertyPK(prop.getPropertyId(), prop.getCopyId());

                result.add(prop);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            Exception fe = new Exception(
                    "Deal Entity - findByDupeCheckCriteria() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return result;
    }

    public Property findByPrimaryKey(PropertyPK pk) throws RemoteException,
            FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from Property " + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.copyId = pk.getCopyId();
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public Property findByPrimaryProperty(int dealId, int copyId,
            int institutionId) throws RemoteException, FinderException {

        String sql = "Select * from Property where dealId = " + dealId
                + " AND primaryPropertyFlag = 'Y'" + " and copyId = " + copyId;

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Deal Entity: @findByPrimaryProperty(), dealId = "
                        + dealId + ", entity not found";

                if (getSilentMode() == false) {
                    logger.error(msg);
                }

                throw new FinderException(msg);
            }
        } catch (Exception e) {
            if (gotRecord == false && getSilentMode() == true) {
                throw (FinderException) e;
            }

            FinderException fe = new FinderException(
                    "Deal Entity - findByPrimaryProperty() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        this.copyId = copyId;
        this.pk = new PropertyPK(propertyId, copyId);

        return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {

        int index = 0; // ::todo check this entity for physical order and
        // change to setters.

        setDealId(jExec.getInt(key, "DealId"));
        setPropertyId(jExec.getInt(key, "PropertyId"));
        copyId = jExec.getInt(key, "copyId");
        setStreetTypeId(jExec.getInt(key, "StreetTypeId"));
        setPropertyTypeId(jExec.getInt(key, "PropertyTypeId"));
        setPropertyUsageId(jExec.getInt(key, "PropertyUsageId"));
        setDwellingStyleId(jExec.getInt(key, "DwellingStyleId"));
        setOccupancyTypeId(jExec.getInt(key, "OccupancyTypeId"));
        setPropertyStreetNumber(jExec.getString(key, "PropertyStreetNumber"));
        setPropertyStreetName(jExec.getString(key, "PropertyStreetName"));
        setHeatTypeId(jExec.getInt(key, "HeatTypeId"));
        setPropertyAddressLine2(jExec.getString(key, "PropertyAddressLine2"));
        setPropertyCity(jExec.getString(key, "PropertyCity"));
        setPropertyPostalFSA(jExec.getString(key, "PropertyPostalFSA"));
        setPropertyPostalLDU(jExec.getString(key, "PropertyPostalLDU"));
        setLegalLine1(jExec.getString(key, "LegalLine1"));
        setLegalLine2(jExec.getString(key, "LegalLine2"));
        setLegalLine3(jExec.getString(key, "LegalLine3"));
        setNumberOfBedrooms(jExec.getInt(key, "NumberOfBedrooms"));
        setYearBuilt(jExec.getInt(key, "YearBuilt"));
        setInsulatedWithUFFI(jExec.getString(key, "InsulatedWithUFFI"));
        setProvinceId(jExec.getInt(key, "ProvinceId"));
        setMonthlyCondoFees(jExec.getDouble(key, "MonthlyCondoFees"));
        setPurchasePrice(jExec.getDouble(key, "PurchasePrice"));
        setMunicipalityId(jExec.getInt(key, "MunicipalityId"));
        setLandValue(jExec.getDouble(key, "LandValue"));
        setPropertyValueSource(jExec.getString(key, "PropertyValueSource"));
        setAppraisalSourceId(jExec.getInt(key, "AppraisalSourceId"));
        setEquityAvailable(jExec.getDouble(key, "EquityAvailable"));
        setNewConstructionId(jExec.getInt(key, "NewConstructionId"));
        ++index;// URBANSURBAN (ALERT - removed but still present in database
        setEstimatedAppraisalValue(jExec.getDouble(key,
                "EstimatedAppraisalValue"));
        setActualAppraisalValue(jExec.getDouble(key, "ActualAppraisalValue"));
        setLoanToValue(jExec.getDouble(key, "LoanToValue"));
        setUnitNumber(jExec.getString(key, "UnitNumber"));
        setLotSize(jExec.getInt(key, "LotSize"));
        setPrimaryPropertyFlag(jExec.getString(key, "PrimaryPropertyFlag"));
        setAppraisalDateAct(jExec.getDate(key, "AppraisalDateAct"));

        setLotSizeDepth(jExec.getInt(key, "LotSizeDepth"));
        setLotSizeFrontage(jExec.getInt(key, "LotSizeFrontage"));
        setLotSizeUnitOfMeasureId(jExec.getInt(key, "LotSizeUnitOfMeasureId"));
        setNumberOfUnits(jExec.getInt(key, "NumberOfUnits"));
        setPropertyLocationId(jExec.getInt(key, "PropertyLocationId"));
        setPropertyOccurenceNumber(jExec.getInt(key, "PropertyOccurenceNumber"));
        setPropertyReferenceNumber(jExec.getString(key,
                "PropertyReferenceNumber"));
        setSewageTypeId(jExec.getInt(key, "SewageTypeId"));
        setStreetDirectionId(jExec.getInt(key, "StreetDirectionId"));
        setWaterTypeId(jExec.getInt(key, "WaterTypeId"));

        setLivingSpace(jExec.getInt(key, "LivingSpace"));
        setLivingSpaceUnitOfMeasureId(jExec.getInt(key,
                "LivingSpaceUnitOfMeasureId"));
        setStructureAge(jExec.getInt(key, "StructureAge"));
        setDwellingTypeId(jExec.getInt(key, "DwellingTypeId"));

        setLendingValue(jExec.getDouble(key, "LendingValue"));
        setTotalPropertyExpenses(jExec.getDouble(key, "TotalPropertyExpenses"));
        setTotalAnnualTaxAmount(jExec.getDouble(key, "TotalAnnualTaxAmount"));
        setZoning(jExec.getInt(key, "Zoning"));

        // Additional fields for CMHC modification -- By BILLY 12Feb2002
        setMLSListingFlag(jExec.getString(key, "MLSListingFlag"));
        setGarageSizeId(jExec.getInt(key, "GarageSizeId"));
        setGarageTypeId(jExec.getInt(key, "GarageTypeId"));
        setBuilderName(jExec.getString(key, "BuilderName"));
        // =============================================================

        // START - MI Additions
        setTenureTypeId(jExec.getInt(key, "TenureTypeId"));
        setMiEnergyEfficiency(jExec.getString(key, "MiEnergyEfficiency"));
        setOnReserveTrustAgreementNumber(jExec.getString(key,
                "OnReserveTrustAgreementNumber"));
        setSubdivisionDiscount(jExec.getString(key, "SubdivisionDiscount"));
        // END - MI Addition

        setRequestAppraisalId(jExec.getInt(key, "RequestAppraisalId"));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

    public Collection<PropertyExpense> getPropertyExpenses() throws Exception {

        return (new PropertyExpense(srk, dcm)).findByProperty(this.pk);

    }

    /**
     * gets the value of instance fields as String. if a field does not exist
     * (or the type is not serviced)** null is returned. if the field exists
     * (and the type is serviced) but the field is null an empty String is
     * returned.
     * 
     * @returns value of bound field as a String;
     * @param fieldName
     *            as String
     * 
     * Other entity types are not yet serviced ie. You can't get the Province
     * object for province.
     */
    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public Object getFieldValue(String fieldName) {

        return doGetFieldValue(this, this.getClass(), fieldName);
    }

    /**
     * sets the value of instance fields as String. if a field does not exist or
     * is not serviced an Exception is thrown if the field exists (and the type
     * is serviced) but the field is null an empty String is returned.
     * 
     * @param fieldName
     *            as String
     * @param the
     *            value as a String
     * 
     * Other entity types are not yet serviced ie. You can't set the Province
     * object in Addr.
     */
    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    /**
     * Updates any Database field that has changed since the last
     * synchronization ie. the last findBy... call
     * 
     * @return the number of updates performed
     */
    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);

        // Optimization:
        // After Calculation, when no real change to the database happened,
        // Don't attach the entity to Calc. again
        if (dcm != null && ret > 0) {
            dcm.inputEntity(this);
        }

        return ret;
    }

    //
    // Transients
    //

    public String getProvinceName() {

        return this.provinceName;
    }

    public void setProvinceName(String prov) {

        this.provinceName = prov;
    }

    public String getProvinceAbbreviation() {

        return this.provinceAbbreviation;
    }

    public void setProvinceAbbreviation(String pr) {

        this.provinceAbbreviation = pr;
    }

    //
    // Property getter/setters
    //

    public String getEntityTableName() {

        return "Property";
    }

    public List getChildren() throws Exception {

        List children = new ArrayList();
        // do not alter the order of the calls this method:
        // Mercator relies on the order that results!!!!
        // any new calls to addAll should go at the end.
        children.addAll(this.getPropertyExpenses());

        // --> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
        // --> By Billy 12Dec2003
        // Add AppraisalOrder to the Children list if any
        AppraisalOrder theAO = getAppraisalOrder();
        if (theAO != null) {
            children.add(theAO);
            // =================================================================
        }

        return children;
    }

    // Catherine: for AVM
    protected Collection getServiceRequests() throws Exception {

        return new ServiceRequest(srk).findByPropertyIdAndCopyId(this.pk);
    }

    public int getCopyId() {

        return this.copyId;
    }

    public char getPrimaryPropertyFlag() {

        return this.primaryPropertyFlag;
    }

    public boolean isPrimaryProperty() {

        return TypeConverter.booleanFrom(String
                .valueOf(this.primaryPropertyFlag));
    }

    /**
     * gets the propertyPostalLDU associated with this entity
     * 
     * @return a String
     */
    public String getPropertyPostalLDU() {

        return this.propertyPostalLDU;
    }

    /**
     * gets the purchasePrice associated with this entity
     * 
     * @return a double
     */
    public double getPurchasePrice() {

        return this.purchasePrice;
    }

    /**
     * gets the monthlyCondoFees associated with this entity
     * 
     * @return a double
     */
    public double getMonthlyCondoFees() {

        return this.monthlyCondoFees;
    }

    /**
     * gets the appraisalSourceId associated with this entity
     * 
     * @return a String
     */
    public int getAppraisalSourceId() {

        return this.appraisalSourceId;
    }

    /**
     * gets the propertyAddressLine2 associated with this entity
     * 
     * @return a String
     */
    public String getPropertyAddressLine2() {

        return this.propertyAddressLine2;
    }

    /**
     * gets the dealId associated with this entity
     * 
     * @return a int
     */
    public int getDealId() {

        return this.dealId;
    }

    /**
     * gets the propertyValueSource associated with this entity
     * 
     * @return a String
     */
    public String getPropertyValueSource() {

        return this.propertyValueSource;
    }

    /**
     * gets the loanToValue associated with this entity
     * 
     * @return a double
     */
    public double getLoanToValue() {

        return this.loanToValue;
    }

    /**
     * gets the streetTypeId associated with this entity
     * 
     * @return a int
     */
    public int getStreetTypeId() {

        return this.streetTypeId;
    }

    public String getStreetTypeDescription() throws Exception {

        String sql = "Select STDESCRIPTION from STREETTYPE where STREETTYPEID = "
                + this.streetTypeId;

        boolean gotRecord = false;

        String result = null;
        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                result = jExec.getString(key, 1);
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = "Property Entity: @getStreetTypeDescription(), streetTypeId = "
                        + this.streetTypeId + ", Street Type not found";
                logger.error(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Property Entity - getStreetTypeDescription() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return result;
    }

    /**
     * gets the propertyStreetName associated with this entity
     * 
     * @return a String
     */
    public String getPropertyStreetName() {

        return this.propertyStreetName;
    }

    /**
     * gets the actualAppraisalValue associated with this entity
     * 
     * @return a int
     */
    public double getActualAppraisalValue() {

        return this.actualAppraisalValue;
    }

    /**
     * gets the numberOfBedrooms associated with this entity
     * 
     * @return a int
     */
    public int getNumberOfBedrooms() {

        return this.numberOfBedrooms;
    }

    /**
     * gets the propertyUsageId associated with this entity
     * 
     * @return a int
     */
    public int getPropertyUsageId() {

        return this.propertyUsageId;
    }

    /**
     * gets the zoning associated with this entity
     * 
     * @return a String
     */
    public int getZoning() {

        return this.zoning;
    }

    /**
     * gets the propertyTypeId associated with this entity
     * 
     * @return a int
     */
    public int getPropertyTypeId() {

        return this.propertyTypeId;
    }

    /**
     * gets the equityAvailable associated with this entity
     * 
     * @return a boolean
     */
    public double getEquityAvailable() {

        return this.equityAvailable;
    }

    /**
     * gets the propertyId associated with this entity
     * 
     * @return a int
     */
    public int getPropertyId() {

        return this.propertyId;
    }

    /**
     * gets the provinceId associated with this entity
     * 
     * @return a int
     */
    public int getProvinceId() {

        return this.provinceId;
    }

    /**
     * gets the landValue associated with this entity
     * 
     * @return a double
     */
    public double getLandValue() {

        return this.landValue;
    }

    /**
     * gets the pk associated with this entity
     * 
     * @return a PropertyPK
     */
    public IEntityBeanPK getPk() {

        return this.pk;
    }

    /**
     * gets the estimatedAppraisalValue associated with this entity
     * 
     * @return a int
     */
    public double getEstimatedAppraisalValue() {

        return this.estimatedAppraisalValue;
    }

    /**
     * gets the legalLine3 associated with this entity
     * 
     * @return a String
     */
    public String getLegalLine3() {

        return this.legalLine3;
    }

    /**
     * gets the heatTypeId associated with this entity
     * 
     * @return a int
     */
    public int getHeatTypeId() {

        return this.heatTypeId;
    }

    /**
     * gets the legalLine2 associated with this entity
     * 
     * @return a String
     */
    public String getLegalLine2() {

        return this.legalLine2;
    }

    /**
     * gets the legalLine1 associated with this entity
     * 
     * @return a String
     */
    public String getLegalLine1() {

        return this.legalLine1;
    }

    /**
     * gets the yearBuilt associated with this entity
     * 
     * @return a Date
     */
    public int getYearBuilt() {

        return this.yearBuilt;
    }

    /**
     * gets the propertyStreetNumber associated with this entity
     * 
     * @return a String
     */
    public String getPropertyStreetNumber() {

        return this.propertyStreetNumber;
    }

    /**
     * gets the propertyOccurenceNumber associated with this entity
     * 
     * @return a int
     */
    public int getPropertyOccurenceNumber() {

        return this.propertyOccurenceNumber;
    }

    /**
     * gets the propertyPostalFSA associated with this entity
     * 
     * @return a String
     */
    public String getPropertyPostalFSA() {

        return this.propertyPostalFSA;
    }

    /**
     * gets the dwellingStyleId associated with this entity
     * 
     * @return a int
     */
    public int getDwellingStyleId() {

        return this.dwellingStyleId;
    }

    /**
     * gets the occupancyTypeId associated with this entity
     * 
     * @return a int
     */
    public int getOccupancyTypeId() {

        return this.occupancyTypeId;
    }

    /**
     * gets the municipalityId associated with this entity
     * 
     * @return a int
     */
    public int getMunicipalityId() {

        return this.municipalityId;
    }

    /**
     * gets the propertyCity associated with this entity
     * 
     * @return a String
     */
    public String getPropertyCity() {

        return this.propertyCity;
    }

    /**
     * gets the newConstruction associated with this entity
     * 
     * @return a boolean
     */
    public int getNewConstructionId() {

        return this.newConstructionId;
    }

    // ===========================================================================
    // Method added because the new construction field has been changed and
    // we have evaluation of new construction all over the software.
    // ===========================================================================
    public boolean isNewConstruction() {

        switch (newConstructionId) {
            case Mc.CONSTRUCTION_CONSTRUCTION: {
                return true;
            }
            case Mc.CONSTRUCTION_NEW: {
                return true;
            }
            default: {
                return false;
            }
        }
    }

    /**
     * gets the insulatedWithUFFI associated with this entity
     * 
     * @return a boolean
     */
    public boolean getInsulatedWithUFFI() {

        return this.insulatedWithUFFI;
    }

    public String getUnitNumber() {

        return this.unitNumber;
    }

    public int getLotSize() {

        return this.lotSize;
    }

    public Date getAppraisalDateAct() {

        return this.appraisalDateAct;
    }

    public int getLotSizeDepth() {

        return this.lotSizeDepth;
    }

    public int getLotSizeFrontage() {

        return this.lotSizeFrontage;
    }

    public int getLotSizeUnitOfMeasureId() {

        return this.lotSizeUnitOfMeasureId;
    }

    public int getNumberOfUnits() {

        return this.numberOfUnits;
    }

    public int getPropertyLocationId() {

        return this.propertyLocationId;
    }

    public String getPropertyReferenceNumber() {

        return this.propertyReferenceNumber;
    }

    public int getSewageTypeId() {

        return this.sewageTypeId;
    }

    public int getStreetDirectionId() {

        return this.streetDirectionId;
    }

    public int getWaterTypeId() {

        return this.waterTypeId;
    }

    // START - MI Additions
    public String getMiEnergyEfficiency() {

        return miEnergyEfficiency;
    }

    public String getOnReserveTrustAgreementNumber() {

        return onReserveTrustAgreementNumber;
    }

    public String getSubdivisionDiscount() {

        return subdivisionDiscount;
    }

    public int getTenureTypeId() {

        return tenureTypeId;
    }

    // END - MI Additions

    private PropertyPK createPrimaryKey(int copyId) throws CreateException {

        String sql = "Select Propertyseq.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1); // can only be one record
            }

            jExec.closeData(key);

            if (id == -1) {
                throw new Exception();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "Property Entity create() exception getting PropertyId from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }
        return new PropertyPK(id, copyId);
    }

    /**
     * creates a Property using the PropertyId with mininimum fields
     * 
     */

    public Property create(DealPK dpk) throws RemoteException, CreateException {

        pk = createPrimaryKey(dpk.getCopyId());

        String sql = "Insert into Property(PropertyId, DealId, copyId,INSTITUTIONPROFILEID ) Values ( "
                + pk.getId()
                + ","
                + dpk.getId()
                + ","
                + pk.getCopyId()
                + ","
                + srk.getExpressState().getDealInstitutionId() + ")";

        try {
            jExec.executeUpdate(sql);
            findByPrimaryKey(pk);
            this.trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(
                    "Property Entity - Property - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        }

        srk.setModified(true);

        if (dcm != null) {
            // dcm.inputEntity(this);
            dcm.inputCreatedEntity(this);
        }

        return this;
    }

    public void setPropertyId(int propertyId) {

        this.testChange("propertyId", propertyId);
        this.propertyId = propertyId;
    }

    public void setDealId(int id) {

        this.testChange("dealId", id);
        this.dealId = id;
    }

    public void setStreetTypeId(int id) {

        this.testChange("streetTypeId", id);
        this.streetTypeId = id;
    }

    public void setPropertyTypeId(int id) {

        this.testChange("propertyTypeId", id);
        this.propertyTypeId = id;
    }

    public void setPropertyUsageId(int id) {

        this.testChange("propertyUsageId", id);
        this.propertyUsageId = id;
    }

    public void setDwellingStyleId(int id) {

        this.testChange("dwellingStyleId", id);
        this.dwellingStyleId = id;
    }

    public void setOccupancyTypeId(int id) {

        this.testChange("occupancyTypeId", id);
        this.occupancyTypeId = id;
    }

    public void setPropertyStreetNumber(String n) {

        this.testChange("propertyStreetNumber", n);
        this.propertyStreetNumber = n;
    }

    public void setPropertyStreetName(String name) {

        this.testChange("propertyStreetName", name);
        this.propertyStreetName = name;
    }

    public void setHeatTypeId(int id) {

        this.testChange("heatTypeId", id);
        this.heatTypeId = id;
    }

    public void setPropertyAddressLine2(String line2) {

        this.testChange("propertyAddressLine2", line2);
        this.propertyAddressLine2 = line2;
    }

    public void setPropertyCity(String city) {

        // ticket 4190
        if (city != null && city.length() > 20) {
            city = city.substring(0, 20);
        }
        this.testChange("propertyCity", city);
        this.propertyCity = city;
    }

    public void setPropertyPostalFSA(String fsa) {

        this.testChange("propertyPostalFSA", fsa);
        this.propertyPostalFSA = fsa;
    }

    public void setPropertyPostalLDU(String ldu) {

        this.testChange("propertyPostalLDU", ldu);
        this.propertyPostalLDU = ldu;
    }

    public void setLegalLine1(String line1) {

        this.testChange("legalLine1", line1);
        this.legalLine1 = line1;
    }

    public void setLegalLine2(String line2) {

        this.testChange("legalLine2", line2);
        this.legalLine2 = line2;
    }

    public void setLegalLine3(String line3) {

        this.testChange("legalLine3", line3);
        this.legalLine3 = line3;
    }

    public void setNumberOfBedrooms(int n) {

        this.testChange("numberOfBedrooms", n);
        this.numberOfBedrooms = n;
    }

    public void setYearBuilt(int d) {

        this.testChange("yearBuilt", d);
        this.yearBuilt = d;
    }

    public void setInsulatedWithUFFI(String uffi) {

        this.testChange("insulatedWithUFFI", uffi);
        this.insulatedWithUFFI = TypeConverter.booleanFrom(uffi, "N");
    }

    public void setProvinceId(int id) {

        this.testChange("provinceId", id);
        this.provinceId = id;
    }

    public void setMonthlyCondoFees(double condo) {

        this.testChange("monthlyCondoFees", condo);
        this.monthlyCondoFees = condo;
    }

    /**
     * Sets and stores the purchase price for this Property. Causes the parent
     * Deal to updateTotalPurchasePrice.
     */
    public void setPurchasePrice(double pp) {

        this.testChange("purchasePrice", pp);
        this.purchasePrice = pp;
    }

    public void setMunicipalityId(int id) {

        this.testChange("municipalityId", id);
        this.municipalityId = id;
    }

    public void setLandValue(double value) {

        this.testChange("landValue", value);
        this.landValue = value;
    }

    public void setPropertyValueSource(String src) {

        this.testChange("propertyValueSource", src);
        this.propertyValueSource = src;
    }

    public void setAppraisalSourceId(int src) {

        this.testChange("appraisalSourceId", src);
        this.appraisalSourceId = src;
    }

    public void setEquityAvailable(double equity) {

        this.testChange("equityAvailable", equity);
        this.equityAvailable = equity;
    }

    public void setNewConstructionId(int nc) {

        this.testChange("newConstructionId", nc);
        this.newConstructionId = nc;
    }

    public void setZoning(int zone) {

        this.testChange("zoning", zone);
        this.zoning = zone;
    }

    public void setEstimatedAppraisalValue(double val) {

        this.testChange("estimatedAppraisalValue", val);
        this.estimatedAppraisalValue = val;
    }

    public void setActualAppraisalValue(double actual) {

        this.testChange("actualAppraisalValue", actual);
        this.actualAppraisalValue = actual;
    }

    public void setLoanToValue(double lv) {

        this.testChange("loanToValue", lv);
        this.loanToValue = lv;
    }

    public void setUnitNumber(String unitNum) {

        this.testChange("unitNumber", unitNum);
        this.unitNumber = unitNum;
    }

    public void setLotSize(int size) {

        this.testChange("lotSize", size);
        this.lotSize = size;
    }

    public void setPrimaryPropertyFlag(String flag) {

        this.testChange("primaryPropertyFlag", flag);
        if (flag != null && flag.length() == 1) {
            this.primaryPropertyFlag = flag.charAt(0);
        }
    }

    public void setNumberOfUnits(int value) {

        this.testChange("numberOfUnits", value);
        this.numberOfUnits = value;
    }

    public void setLotSizeUnitOfMeasureId(int value) {

        this.testChange("lotSizeUnitOfMeasureId", value);
        this.lotSizeUnitOfMeasureId = value;
    }

    public void setSewageTypeId(int value) {

        this.testChange("sewageTypeId", value);
        this.sewageTypeId = value;
    }

    public void setStreetDirectionId(int value) {

        this.testChange("streetDirectionId", value);
        this.streetDirectionId = value;
    }

    public void setWaterTypeId(int value) {

        this.testChange("waterTypeId", value);
        this.waterTypeId = value;
    }

    public void setAppraisalDateAct(Date d) {

        this.testChange("appraisalDateAct", d);
        this.appraisalDateAct = d;
    }

    public void setLotSizeDepth(int value) {

        this.testChange("lotSizeDepth", value);
        this.lotSizeDepth = value;
    }

    public void setLotSizeFrontage(int value) {

        this.testChange("lotSizeFrontage", value);
        this.lotSizeFrontage = value;
    }

    public void setPropertyLocationId(int value) {

        this.testChange("propertyLocationId", value);
        this.propertyLocationId = value;
    }

    public void setPropertyOccurenceNumber(int value) {

        this.testChange("propertyOccurenceNumber", value);
        this.propertyOccurenceNumber = value;
    }

    public void setPropertyReferenceNumber(String value) {

        this.testChange("propertyReferenceNumber", value);
        this.propertyReferenceNumber = value;
    }

    // START - MI Additions
    public void setMiEnergyEfficiency(String energyEfficiency) {

        this.testChange("miEnergyEfficiency", energyEfficiency);
        miEnergyEfficiency = energyEfficiency;
    }

    public void setOnReserveTrustAgreementNumber(
            String onReserveTrustAgreementNumber) {

        this.testChange("onReserveTrustAgreementNumber",
                onReserveTrustAgreementNumber);
        this.onReserveTrustAgreementNumber = onReserveTrustAgreementNumber;
    }

    public void setSubdivisionDiscount(String subdivisionDiscount) {

        this.testChange("subdivisionDiscount", subdivisionDiscount);
        this.subdivisionDiscount = subdivisionDiscount;
    }

    public void setTenureTypeId(int tenureTypeId) {

        this.testChange("tenureTypeId", tenureTypeId);
        this.tenureTypeId = tenureTypeId;
    }

    // END - MI Additions

    public int getLivingSpace() {

        return this.livingSpace;
    }

    public void setLivingSpace(int value) {

        this.testChange("livingSpace", value);
        this.livingSpace = value;
    }

    public int getLivingSpaceUnitOfMeasureId() {

        return this.livingSpaceUnitOfMeasureId;
    }

    public void setLivingSpaceUnitOfMeasureId(int value) {

        this.testChange("livingSpaceUnitOfMeasureId", value);
        this.livingSpaceUnitOfMeasureId = value;
    }

    public int getStructureAge() {

        return this.structureAge;
    }

    public void setStructureAge(int value) {

        this.testChange("structureAge", value);
        this.structureAge = value;
    }

    public int getDwellingTypeId() {

        return this.dwellingTypeId;
    }

    public void setDwellingTypeId(int value) {

        this.testChange("dwellingTypeId", value);
        this.dwellingTypeId = value;
    }

    public double getLendingValue() {

        return this.lendingValue;
    }

    public void setLendingValue(double value) {

        this.testChange("lendingValue", value);
        this.lendingValue = value;
    }

    public double getTotalPropertyExpenses() {

        return this.totalPropertyExpenses;
    }

    public void setTotalPropertyExpenses(double value) {

        this.testChange("totalPropertyExpenses", value);
        this.totalPropertyExpenses = value;
    }

    public double getTotalAnnualTaxAmount() {

        return this.totalAnnualTaxAmount;
    }

    public void setTotalAnnualTaxAmount(double value) {

        this.testChange("totalAnnualTaxAmount", value);
        this.totalAnnualTaxAmount = value;
    }

    // Additional fields for CMHC modifications -- By BILLY12Feb2002
    public String getMLSListingFlag() {

        return this.MLSListingFlag;
    }

    public void setMLSListingFlag(String value) {

        this.testChange("MLSListingFlag", value);
        this.MLSListingFlag = value;
    }

    public int getGarageSizeId() {

        return this.garageSizeId;
    }

    public void setGarageSizeId(int value) {

        this.testChange("garageSizeId", value);
        this.garageSizeId = value;
    }

    public int getGarageTypeId() {

        return this.garageTypeId;
    }

    public void setGarageTypeId(int value) {

        this.testChange("garageTypeId", value);
        this.garageTypeId = value;
    }

    public String getBuilderName() {

        return this.builderName;
    }

    public void setBuilderName(String value) {

        this.testChange("builderName", value);
        this.builderName = value;
    }

    // ***** Change by NBC Impl. Team - Version 1.2 - Start*****//

    /**
     * @return Returns the requestAppraisalId.
     */
    public int getRequestAppraisalId() {

        return requestAppraisalId;
    }

    /**
     * @param requestAppraisalId
     *            The requestAppraisalId to set.
     */
    public void setRequestAppraisalId(int requestAppraisal) {

        this.testChange("requestAppraisalId", requestAppraisal);
        this.requestAppraisalId = requestAppraisal;
    }

    // ***** Change by NBC Impl. Team - Version 1.2 - End*****//
    // ==============================================================

    public Vector findByMyCopies() throws RemoteException, FinderException {

        Vector v = new Vector();

        String sql = "Select * from Property where propertyId = "
                + getPropertyId() + " AND copyId <> " + getCopyId();

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                Property iCpy = new Property(srk, dcm);

                iCpy.setPropertiesFromQueryResult(key);
                iCpy.pk = new PropertyPK(iCpy.getPropertyId(), iCpy.getCopyId());

                v.addElement(iCpy);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Property - findByMyCopies exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        return v;
    }

    public int getClassId() {

        return ClassId.PROPERTY;
    }

    public boolean isAuditable() {

        return true;
    }

    public String formPropertyAddress() {

        String addr = "";
        if (this.getPropertyStreetNumber() != null) {
            addr += this.getPropertyStreetNumber() + " ";
        }

        if (this.getPropertyStreetName() != null) {
            addr += this.getPropertyStreetName() + " ";
        }

        addr += PicklistData.getDescription("StreetType", this
                .getStreetTypeId());

        return addr;
    }

    /**
     * A fast track business method db call/query that gets the first
     * partyCompanyName<br/> for appraisers associated with this property. (<i>Note:
     * PartyProfile.findByPropertyDealAndType could have been used but <br/>
     * returns a collection of fully loaded profiles.</i>)
     * 
     * @return a String containing the partyName or null if none is found
     */
    public String fetchFirstAppraiserCompanyName() throws Exception {

        String sql = "select partyCompanyName from PartyProfile r, PartyDealAssoc a "
                + "where a.propertyId = "
                + this.propertyId
                + " and r.partyProfileId = "
                + " a.partyprofileId and r.partytypeid = "
                + Mc.PARTY_TYPE_APPRAISER + " order by r.partyProfileId";

        String name = null;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                name = jExec.getString(key, 1);
                break; // get first record
            }

            jExec.closeData(key);

        } catch (Exception e) {
            Exception ne = new Exception(
                    "Exception caught while fetching Appraiser - PartyCompanyName");

            logger.error(ne.getMessage());

            if (e.getMessage() != null) {
                logger.error(e.getMessage());
            }

            logger.error(e);

            throw ne;
        }

        return name;
    }

    protected void removeSons() throws Exception { // Remove sons

        try {
            Collection propertyExpenses = this.getPropertyExpenses();
            Iterator itPE = propertyExpenses.iterator();

            while (itPE.hasNext()) {
                PropertyExpense propertyExpense = (PropertyExpense) itPE.next();
                if (itPE.hasNext()) {
                    propertyExpense.ejbRemove(false);
                } else {
                    propertyExpense.ejbRemove(true);
                }
            }

            // --> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
            // --> By Billy 12Dec2003
            // Remove the AppraisalOrder Record if any
            AppraisalOrder theAO = getAppraisalOrder();
            if (theAO != null) {
                theAO.ejbRemove();
                // =================================================================
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public boolean equals(Object o) {

        if (o instanceof Property) {
            Property oa = (Property) o;
            if ((this.propertyId == oa.getPropertyId())
                    && (this.copyId == oa.getCopyId())) {
                return true;
            }
        }
        return false;
    }

    protected EntityContext getEntityContext() throws RemoteException {

        EntityContext ctx = this.entityContext;
        try {
            ctx.setContextText("");
            ctx.setContextSource(this.propertyStreetNumber
                    + " "
                    + this.propertyStreetName
                    + " "
                    + PicklistData.getDescription("StreetType",
                            this.streetTypeId));

            // Modified by Billy for performance tuning -- By BILLY 28Jan2002
            // Deal deal = new Deal( srk, null , this.dealId , this.copyId );
            // if ( deal == null )
            // return ctx;
            // String sc = String.valueOf ( deal.getScenarioNumber () ) +
            // deal.getCopyType();
            Deal deal = new Deal(srk, null);
            String sc = String.valueOf(deal.getScenarioNumber(this.dealId,
                    this.copyId))
                    + deal.getCopyType(this.dealId, this.copyId);
            // ===================================================================================

            ctx.setScenarioContext(sc);

            ctx.setApplicationId(this.dealId);

        } catch (Exception e) {
            if (e instanceof FinderException) {
                logger
                        .warn("Property.getEntityContext Parent Not Found. PropertyId="
                                + this.propertyId);
                return ctx;
            }
            throw (RemoteException) e;
        }
        return ctx;
    } // -------------- End of getEntityContext() ----------------------

    // --> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    // --> By Billy 12Dec2003

    /**
     * Method to get the AppraisalOrder record if any - return null if not find.
     * 
     * @throws Exception
     * @return AppraisalOrder - if record find null - if not find
     */
    public AppraisalOrder getAppraisalOrder() throws Exception {

        return (new AppraisalOrder(srk, dcm)).findByProperty(this.pk);
    }

    public String getStreetDirectionDescription() throws Exception {

        {
            String sql = "Select sdDescription from StreetDirection where streetDirectionId = "
                    + this.streetDirectionId;

            boolean gotRecord = false;

            String result = null;
            try {
                int key = jExec.execute(sql);

                for (; jExec.next(key);) {
                    gotRecord = true;
                    result = jExec.getString(key, 1);
                    break;
                }

                jExec.closeData(key);

                if (gotRecord == false) {
                    String msg = "Property Entity: @getStreetDirectionDescription(), streetDirectionId = "
                            + this.streetDirectionId
                            + ", Street Direction not found";
                    logger.error(msg);
                }
            } catch (Exception e) {
                FinderException fe = new FinderException(
                        "Property Entity - getStreetDirectionDescription() exception");

                logger.error(fe.getMessage());
                logger.error("finder sql: " + sql);
                logger.error(e);

                throw fe;
            }

            return result;
        }
    }

    public Collection findCopyIdByPropertyId(int propertyId)
            throws FinderException {

        Collection copies = new ArrayList();

        String sql = "SELECT * from PROPERTY";
        sql += " WHERE PROPERTYID = " + propertyId;
        sql += " ORDER BY PROPERTY.PROPERTYOCCURENCENUMBER DESC";

        try {
            int key = jExec.execute(sql);
            for (; jExec.next(key);) {
                Integer copyId = new Integer(jExec.getInt(key, "COPYID"));
                copies.add(copyId);
            }
            jExec.closeData(key);

        } catch (Exception e) {
            FinderException fe = new FinderException(
                    "Exception caught while getting Property propertyId:"
                            + propertyId);
            logger.error(fe.getMessage());
            logger.error(e);

            throw fe;
        }

        return copies;
    }

    // =================================================================

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }
}
