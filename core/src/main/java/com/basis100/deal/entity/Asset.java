package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AssetPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;

public class Asset extends DealEntity
{
   protected int      assetId;
   protected int      borrowerId;
   protected String   assetDescription;
   protected double   assetValue;
   protected int      assetTypeId;
   protected boolean  includeInNetWorth;
   protected int      percentInNetWorth;
   protected int      copyId;
   protected int      instProfileId;

   private AssetPK pk;

   public Asset(SessionResourceKit srk, CalcMonitor dcm) throws RemoteException, FinderException
   {
      super(srk,dcm);
   }

   public Asset(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException
   {
      super(srk,dcm);

      pk = new AssetPK(id, copyId);

      findByPrimaryKey(pk);
   }

   /**
    * Attempts to find the record that corresponds to the description/ name disregarding Case
    * @param  a String representation of the name
    * @return this instance
    */

    // BJH - July 2001
    //
    // Note this method is highly suspect - searches on combination of asset description and copy id!!
    //
    // However it does not appear to be used at present.

   public Asset findByName(String description, int copyId) throws RemoteException, FinderException
   {
      String sql = "Select * from Asset where " +
                    StringUtil.formFieldMatchExpression("AssetDescription", description, false) +
                    " and copyId = " + copyId;

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               this.pk = new AssetPK(this.assetId, this.copyId);
               break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByName = " + description;
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }
      return this;
   }

  public Collection<Asset> findByBorrower(BorrowerPK pk) throws Exception
  {
    Collection<Asset> assets = new ArrayList<Asset>();

    String sql = "Select * from Asset " ;
          sql +=  pk.getWhereClause();

     try
     {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          Asset asset =  new Asset(srk,dcm);
          asset.setPropertiesFromQueryResult(key);
          asset.pk = new AssetPK(asset.getAssetId(),asset.getCopyId());
          assets.add(asset);
        }

        jExec.closeData(key);

      }
      catch (Exception e)
      {
        Exception fe = new Exception("Exception caught while getting Borrower::Assets");
        logger.error(fe.getMessage());
        logger.error(e);

        throw fe;
      }

    return assets;
  }


   public Asset findByPrimaryKey(AssetPK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from asset " + pk.getWhereClause();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            gotRecord = true;
            setPropertiesFromQueryResult(key);

            break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Deal Entity: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Deal Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

     /**
    *  Find income records not associated with an EmploymentHistory record for
    *  the given Borrower.
    */
   public Collection findByDeal(DealPK pk) throws Exception
   {
      Collection assets = new ArrayList();      //assetid, copyId

      StringBuffer buf = new StringBuffer("Select * from asset ");
      buf.append(" where borrowerId in( Select BorrowerId from Borrower where dealId = ");
      buf.append(pk.getId()).append(" and copyId = ").append(pk.getCopyId());
      buf.append(") and copyId = ").append(pk.getCopyId());

      try
      {
        int key = jExec.execute(buf.toString());

        for (; jExec.next(key); )
        {
          Asset a = new Asset(srk,dcm);
          a.setPropertiesFromQueryResult(key);
          a.pk = new AssetPK(a.getAssetId(),a.getCopyId());
          assets.add(a);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
        logger.error(e.getMessage());
        logger.error(e);

        throw e;
      }

    return assets;
   }

   private void setPropertiesFromQueryResult(int key) throws Exception {
		this.setAssetId(jExec.getInt(key, "ASSETID"));
		this.setBorrowerId(jExec.getInt(key, "BORROWERID"));
		this.setAssetDescription(jExec.getString(key, "ASSETDESCRIPTION"));
		this.setAssetValue(jExec.getDouble(key, "ASSETVALUE"));
		this.setAssetTypeId(jExec.getInt(key, "ASSETTYPEID"));
		this.setIncludeInNetWorth(jExec.getString(key, "INCLUDEINNETWORTH"));
		this.setPercentInNetWorth(jExec.getInt(key, "PERCENTINNETWORTH"));
		this.copyId = jExec.getInt(key, "COPYID");
		this.setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
	}


  /**
	 * gets the value of instance fields as String. if a field does not exist
	 * (or the type is not serviced)** null is returned. if the field exists
	 * (and the type is serviced) but the field is null an empty String is
	 * returned.
	 * 
	 * @returns value of bound field as a String;
	 * @param fieldName
	 *            as String
	 * 
	 * Other entity types are not yet serviced ie. You can't get the Province
	 * object for province.
	 */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }

  protected int performUpdate()  throws Exception
  {
    Class clazz = this.getClass();
    int ret = doPerformUpdate(this, clazz);
   // Optimization:
   // After Calculation, when no real change to the database happened, Don't attach the entity to Calc. again
    if(dcm != null && ret >0 ) dcm.inputEntity(this);

    return ret;
  }


  public String getEntityTableName()
  {
      return "Asset";
  }

		/**
     *   gets the assetValue associated with this entity
     *
     *   @return a double
     */
    public double getAssetValue()
   {
      return this.assetValue ;
   }

		/**
     *   gets the assetId associated with this entity
     *
     *   @return a int
     */
    public int getAssetId()
   {
      return this.assetId ;
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a AssetPK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

		/**
     *   gets the borrowerId associated with this entity
     *
     *   @return a int
     */
    public int getBorrowerId()
   {
      return this.borrowerId ;
   }

		/**
     *   gets the assetDescription associated with this entity
     *
     *   @return a String
     */
   public String getAssetDescription()
   {
      return this.assetDescription ;
   }

   public boolean getIncludeInNetWorth()
   {
		 return this.includeInNetWorth;
   }

   public int getPercentInNetWorth()
   {
		 return this.percentInNetWorth;
   }
   public int getAssetTypeId()
	 {
     return assetTypeId;
   }
   public int getCopyId()
   {
     return copyId;
   }
   public Asset deepCopy() throws CloneNotSupportedException
   {
      return (Asset)this.clone();
   }

   private AssetPK createPrimaryKey(int copyId)throws CreateException
   {
       String sql = "Select Assetseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
              id = jExec.getInt(key,1);  // can only be one record
           }

           jExec.closeData(key);

           if( id == -1 ) throw new Exception();

        }
        catch (Exception e)
        {
          CreateException ce = new CreateException("Asset Entity create() exception getting AssetId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;
        }

        return new AssetPK(id,copyId);
  }


  /**
  * creates a Asset using the Asset primary key and a borrower primary key
  *  - the mininimum (ie.non-null) fields
  *
  */
  public Asset create(BorrowerPK bpk)throws RemoteException, CreateException
  {
    pk = createPrimaryKey(bpk.getCopyId());

    // set default value for asset type and associated defaults  -- by BILLY 11Jan2001
    // assetType default to option 0 (Savings)
    int assetTypeId = 0;

    // added inst id for ML: -1 is no institutionnized table inst Id
    String percentInNetworth 
        = PicklistData.getMatchingColumnValue(-1, "AssetType", assetTypeId, 
                                              "networthinclusion");
    String includeInNetworth = ("0".equals(percentInNetworth)) ? "'N'" : "'Y'";

    String sql = "Insert into Asset( " +
    "AssetId, BorrowerId, copyId, assetvalue, " +
    "assettypeid, percentinnetworth, includeinnetworth, INSTITUTIONPROFILEID)" +
    " Values ( " +
      pk.getId() + "," + bpk.getId() + "," + pk.getCopyId() + ", 0.0, " +
      assetTypeId + ", " + percentInNetworth + ", " + includeInNetworth + "," + 
      srk.getExpressState().getDealInstitutionId() +
    ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      this.trackEntityCreate ();      // Track the create
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("Asset Entity - Asset - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }

    srk.setModified(true);

    if(dcm != null)
    {
      //dcm.inputEntity(this);
      dcm.inputCreatedEntity ( this );
    }

    return this;
  }

  /**
  *  sets the value of instance fields as String.
  *  if a field does not exist or is not serviced an Exception is thrown
  *  if the field exists (and the type is serviced) but the field is null an empty
  *  String is returned.
  *
  *  @param  fieldName as String
  *  @param  the value as a String
  *
  *  Other entity types are not yet serviced   ie. You can't set the Province object
  *  in Addr.
  */
  public void setField(String fieldName, String value) throws Exception
  {
    doSetField(this, this.getClass(), fieldName, value);
  }


  public void setAssetId(int id)
	{
		 this.testChange ("assetId", id);
		 this.assetId = id;
  }
  public void setBorrowerId(int id)
	{
     this.testChange ("borrowerId", id);
		 this.borrowerId = id;
  }
  public void setAssetDescription(String ad)
	{
	  this.testChange ("assetDescription", ad);
		this.assetDescription = ad;
  }
  public void setAssetValue(double av)
	{
     this.testChange ("assetValue", av);
		 this.assetValue = av;
  }


  public void setIncludeInNetWorth(String inc)
	{
		setIncludeInNetWorth(TypeConverter.booleanFrom(inc));
  }

  public void setIncludeInNetWorth(boolean inc)
	{
    this.testChange ("includeInNetWorth", inc ) ;
		this.includeInNetWorth = inc;
  }

  public void setAssetTypeId(int id)
	{
    this.testChange( "assetTypeId", id );
    this.assetTypeId = id;
  }

  public void setPercentInNetWorth(int inc)
	{
    this.testChange ("percentInNetWorth", inc);
		this.percentInNetWorth = inc;
  }

  public Vector findByMyCopies() throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from Asset where assetId = " +
       getAssetId() + " AND copyId <> " + getCopyId();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
          Asset iCpy = new Asset(srk,dcm);

          iCpy.setPropertiesFromQueryResult(key);
          iCpy.pk = new AssetPK(iCpy.getAssetId(), iCpy.getCopyId());

          v.addElement(iCpy);
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
        FinderException fe = new FinderException("Asset - findByMyCopies exception");

        logger.error(fe.getMessage());
        logger.error("finder sql: " + sql);
        logger.error(e);

        throw fe;
      }
      return v;
   }

  public int getClassId()
  {
    return ClassId.ASSET;
  }

  public boolean isAuditable()
  {
    return true;
  }

  public  boolean equals( Object o )
  {
    if ( o instanceof Asset )
      {
        Asset oa = ( Asset ) o;
        if  ( ( this.assetId == oa.getAssetId () )
          && ( this.copyId  ==  oa.getCopyId ()  ))
            return true;
      }
    return false;
  }

 protected EntityContext getEntityContext()  throws RemoteException
 {
  EntityContext ctx = this.entityContext ;
  try
  {
    ctx.setContextText( this.assetDescription) ;
    Borrower borrower = new Borrower(srk, null , this.getBorrowerId () , this.getCopyId () ) ;
    if ( borrower == null )
      return ctx;

    ctx.setContextSource(borrower.getNameForEntityContext());

    //Modified by Billy for performance tuning -- By BILLY 28Jan2002
    //Deal deal = new Deal( srk, null);
    //deal = deal.findByPrimaryKey( new DealPK( borrower.getDealId () , borrower.getCopyId () ) ) ;
    //if ( deal == null )
    //  return ctx;
    //String  sc = String.valueOf ( deal.getScenarioNumber () ) + deal.getCopyType();
    Deal deal = new Deal(srk, null);
    String  sc = String.valueOf (deal.getScenarioNumber(borrower.getDealId () , borrower.getCopyId ())) +
        deal.getCopyType(borrower.getDealId () , borrower.getCopyId ());
    // ===================================================================================

    ctx.setScenarioContext ( sc);

    ctx.setApplicationId ( borrower.getDealId() ) ;
  } catch ( Exception e )
  {
    if ( e instanceof FinderException )
    {
      logger.warn( "Asset.getEntityContext Parent Not Found. AssetId=" + this.assetId );
      return ctx;
    }
    throw ( RemoteException ) e ;
  }
  return ctx;
 } ///////// End of getEntityContext /////////////////////////////////////////

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}

}
