
package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BncAutoDeclineReasonPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: BncAutoDeclineReason
 * </p>
 * <p>
 * Description: Entity class to handles all database communication to the
 * BncAutoDeclineReason table
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */
public class BncAutoDeclineReason extends DealEntity {
    private int responseId;
    private int institutionProfileId;
    private int autoDeclineReasonId;
    private String autoDeclineReasonCode;
    private String autoDeclineReasonDesc;
    private BncAutoDeclineReasonPK pk;

    public static final String AUTODECLINEREASONCODE = "autoDeclineReasonCode";
    public static final String AUTODECLINEREASONDESC = "autoDeclineReasonDesc";

    public BncAutoDeclineReason() {

    }

    public BncAutoDeclineReason(SessionResourceKit srk) throws RemoteException {

        super(srk);
    }

    public int getResponseId() {

        return responseId;
    }

    public void setResponseId(int responseId) {

        testChange(BncAutoDeclineReasonPK.RESPONSEID, responseId);
        this.responseId = responseId;
    }

    public int getAutoDeclineReasonId() {

        return autoDeclineReasonId;
    }

    public void setAutoDeclineReasonId(int autoDeclineReasonId) {

        testChange(BncAutoDeclineReasonPK.AUTODECLINEREASONID,
                autoDeclineReasonId);
        this.autoDeclineReasonId = autoDeclineReasonId;
    }

    public String getAutoDeclineReasonCode() {

        return autoDeclineReasonCode;
    }

    public void setAutoDeclineReasonCode(String autoDeclineReasonCode) {

        testChange(AUTODECLINEREASONCODE, autoDeclineReasonCode);
        this.autoDeclineReasonCode = autoDeclineReasonCode;
    }

    public String getAutoDeclineReasonDesc() {

        return autoDeclineReasonDesc;
    }

    public void setAutoDeclineReasonDesc(String autoDeclineReasonDesc) {

        testChange(AUTODECLINEREASONDESC, autoDeclineReasonDesc);
        this.autoDeclineReasonDesc = autoDeclineReasonDesc;
    }

    public void setPk(BncAutoDeclineReasonPK primaryKey) {

        this.pk = primaryKey;
    }

    /**
     * search for BncAutoDeclineReason record using primary key
     * 
     * @param pk
     *            BncAutoDeclineReasonPK Primary key to be searched by
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws FinderException
     *             Thrown if unable to find a matching row
     * @return BncAutoDeclineReason object that matches the primary key given
     */
    public BncAutoDeclineReason findByPrimaryKey(BncAutoDeclineReasonPK pk)
            throws RemoteException, FinderException {

    	if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
        String sql = "Select * from " + getEntityTableName()
                + pk.getWhereClause();

        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                gotRecord = true;
                setPropertiesFromQueryResult(key);
                break; // can only be one record
            }

            jExec.closeData(key);

            if (gotRecord == false) {
                String msg = getEntityTableName()
                        + ": @findByPrimaryKey(), key= " + pk
                        + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch (Exception e) {
            FinderException fe = new FinderException(getEntityTableName()
                    + " Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * Set all properties of this object using the values that is returned by
     * the current database query
     * 
     * @param key
     *            int The index of the database query result list
     * @throws Exception
     *             Thrown if error
     */
    public void setPropertiesFromQueryResult(int key) throws Exception {

        setResponseId(jExec.getInt(key, BncAutoDeclineReasonPK.RESPONSEID));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        setAutoDeclineReasonId(jExec.getInt(key,
                BncAutoDeclineReasonPK.AUTODECLINEREASONID));
        setAutoDeclineReasonCode(jExec.getString(key, AUTODECLINEREASONCODE));
        setAutoDeclineReasonDesc(jExec.getString(key, AUTODECLINEREASONDESC));
    }

    /**
     * Create a new primaryky from the BncAutoDeclineReason sequence.
     * 
     * @param responseId
     *            int Response Id
     * @throws CreateException
     *             Thrown if unable to create
     * @return BncAutoDeclineReasonPK Primary key with the newest ID in the
     *         sequence
     */
    public BncAutoDeclineReasonPK createPrimaryKey(int responseId)
            throws CreateException {

        String sql = "Select " + getEntityTableName() + "SEQ.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                id = jExec.getInt(key, 1);
                break;
            }
            jExec.closeData(key);
            if (id == -1) {
                throw new CreateException();
            }
        } catch (Exception e) {
            CreateException ce = new CreateException(getEntityTableName()
                    + "createPrimaryKey() :" + "exception: getting "
                    + BncAutoDeclineReasonPK.AUTODECLINEREASONID
                    + " from sequence");
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }
        return new BncAutoDeclineReasonPK(responseId, id);
    }

    /**
     * Create and inserts a BncAutoDeclineReason into the database
     * 
     * @param responseId
     *            int Response Id
     * @param code
     *            String Auto decline reason code
     * @param description
     *            String Auto decline reason description
     * @throws RemoteException
     *             Thrown if error on connection
     * @throws CreateException
     *             Thrown if unable to create row
     * @return BncAutoDeclineReason The BncAutoDeclineReason object has has been
     *         inserted into the database
     */
    public BncAutoDeclineReason create(int responseId, String code,
            String description) throws RemoteException, CreateException {

        BncAutoDeclineReasonPK rpk = createPrimaryKey(responseId);
        int key = 0;
        StringBuffer sqlb = new StringBuffer("Insert into "
                + getEntityTableName() + "( "
                + BncAutoDeclineReasonPK.RESPONSEID + ", "
                + BncAutoDeclineReasonPK.AUTODECLINEREASONID + ", "
                + AUTODECLINEREASONCODE + ", " + AUTODECLINEREASONDESC
                + ", INSTITUTIONPROFILEID) VALUES ( ");
        sqlb.append(sqlStringFrom(rpk.getId()) + ", ");
        sqlb.append(sqlStringFrom(rpk.getAutoDeclineReasonId()) + ",");
        sqlb.append(sqlStringFrom(code) + ",");
        sqlb.append(sqlStringFrom(description) + ",");
        sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId())
                + ")");

        String sql = new String(sqlb);

        try {
            key = jExec.executeUpdate(sql);
            findByPrimaryKey(new BncAutoDeclineReasonPK(rpk.getId(), rpk
                    .getAutoDeclineReasonId()));
            this.trackEntityCreate(); // track the creation
        } catch (Exception e) {
            CreateException ce = new CreateException(getEntityTableName()
                    + " Entity - " + getEntityTableName()
                    + " - create() exception");
            logger.error(ce.getMessage());
            logger.error(e);

            throw ce;
        } finally {
            try {
                if (key != 0) {
                    jExec.closeData(key);
                }
            } catch (Exception e) {
                logger.error(e.toString());
            }
        }
        srk.setModified(true);
        return this;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // ///////////////////// OVERWRITING BASE CLASS METHODS
    // /////////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    protected int performUpdate() throws Exception {

        Class clazz = this.getClass();
        int ret = doPerformUpdate(this, clazz);
        return ret;
    }

    public String getEntityTableName() {

        return "BncAutoDeclineReason";
    }

    public IEntityBeanPK getPk() {

        return this.pk;
    }

    public String getStringValue(String fieldName) {

        return doGetStringValue(this, this.getClass(), fieldName);
    }

    public boolean equals(Object o) {

        if (o instanceof BncAutoDeclineReason) {
            BncAutoDeclineReason oa = (BncAutoDeclineReason) o;
            if (this.pk.equals(oa.getPk())) {
                return true;
            }
        }
        return false;
    }

    public void setField(String fieldName, String value) throws Exception {

        doSetField(this, this.getClass(), fieldName, value);
    }

    public Object clone() throws CloneNotSupportedException {

        throw new CloneNotSupportedException();
    }

    public int getInstitutionProfileId() {

        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {

        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

}
