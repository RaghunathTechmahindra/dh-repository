package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class LenderProfile extends DealEntity
{
   protected int      lenderProfileId;
   protected char     defaultLender;
   protected String   lenderName;
   protected int      contactId;
   protected int      profileStatusId;
   protected String   lpShortName;
   protected String   lpBusinessId;
   protected boolean  upfrontMIAllowed;
   protected int      defaultInvestorId;
   protected String   privateLabelIndicator;
   protected String   registrationName;
   protected int      interestCompoundId;
   //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
   protected String supportRateFloatDown;
   //protected String SUPPORT_RATE_FLOAT_DOWN_FLAG = "Y";//Quick Fix Moved to Mc.java. Need to move to mossys later
   //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
   
   protected int       institutionProfileId; 
   
   private LenderProfilePK pk;

   public LenderProfile(SessionResourceKit srk) throws RemoteException, FinderException
   {
      super(srk);
   }

   public LenderProfile(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      pk = new LenderProfilePK(id);

      findByPrimaryKey(pk);
   }

   public LenderProfile findByPrimaryKey(LenderProfilePK pk) throws RemoteException, FinderException
   {
	  if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from LenderProfile where " + pk.getName()+ " = " + pk.getId();

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "LenderProfile: @findByPrimaryKey(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = pk;
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    public LenderProfile findByDefaultLender() throws RemoteException, FinderException
    {
      String sql = "Select * from LenderProfile where defaultLender = 'Y'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
               break; // can only be one record
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "LenderProfile: @findByDefaultLender(), key= " + pk + ", entity not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("LenderProfile Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        this.pk = new LenderProfilePK(this.lenderProfileId);
        return this;
    }


   public Collection findByShortName(String shortName) throws Exception
   {
      String sql = "Select * from LenderProfile where lpShortName = '" + shortName + "'";

      LenderProfile profile = null;
      Collection profiles = new ArrayList();

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            profile = new LenderProfile(srk);
            profile.setPropertiesFromQueryResult(key);
            profile.pk = new LenderProfilePK(profile.getLenderProfileId());
            profiles.add(profile);
          }

          jExec.closeData(key);


        }
        catch (Exception e)
        {
          Exception fe = new Exception("Lender Profile Entity - findByPrimaryKey() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }

        return profiles;
    }


   public Collection findByBranch(BranchProfilePK pk) throws Exception
   {
      String sql = "Select * from LenderToBranchAssoc where " + pk.getName()+ " = " + pk.getId();

      LenderProfile profile = null;
      Collection profiles = new ArrayList();

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            profile = new LenderProfile(srk);
            profile.setPropertiesFromQueryResult(key);
            profile.pk = new LenderProfilePK(profile.getLenderProfileId());
            profiles.add(profile);
          }

          jExec.closeData(key);


        }
        catch (Exception e)
        {
          Exception fe = new Exception("LenderProfile Entity - findByBranch() exception");

          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);

          throw fe;
        }
        
        return profiles;
    }

   /**
    * setPropertiesFromQueryResult
    * 
    * @param key int <br>
    * @return none<br>
    * @version 1.1 <br>
    * Date: 06/26/2006 <br>
    * Author: NBC/PP Implementation Team <br>
    * Change: <br>
    * 	Added call to set supportRateFloatDown attribute
    *
    */
   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      this.setLenderProfileId(jExec.getInt(key,"LENDERPROFILEID"));
      this.setDefaultLender(jExec.getString(key,"DEFAULTLENDER"));
      this.setLenderName(jExec.getString(key,"LENDERNAME"));
      this.setContactId( jExec.getInt(key,"CONTACTID"));
      this.setProfileStatusId(jExec.getInt(key,"PROFILESTATUSID"));
      this.setLPShortName(jExec.getString(key,"LPSHORTNAME"));
      this.setLPBusinessId(jExec.getString(key,"LPBUSINESSID"));
      this.setUpfrontMIAllowed(jExec.getString(key,"UPFRONTMIALLOWED"));
      this.setDefaultInvestorId(jExec.getInt(key,"DEFAULTINVESTORID"));
      this.setPrivateLabelIndicator(jExec.getString(key,"PRIVATELABELINDICATOR"));
      this.setRegistrationName(jExec.getString(key,"REGISTRATIONNAME"));
      this.setInterestCompoundId(jExec.getInt(key,"INTERESTCOMPOUNDINGID"));
      //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//      
      this.setSupportRateFloatDown(jExec.getString(key,"SUPPORTRATEFLOATDOWN"));
      //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//   
      this.setInstitutionProfileId(jExec.getInt(key,"INSTITUTIONPROFILEID"));
   }

   /**
    * isSupportRateFloatDown
    * 
    * @param none <br>
    * @return boolean<br>
    * @version 1.1 (Initial version - 26 Jun 2006)
    * @author NBC/PP Implementation Team
    */
   public boolean isSupportRateFloatDown() 
   {
	   // SUPPORT_RATE_FLOAT_DOWN_FLAG, global constant
	   
   		boolean returnValue = supportRateFloatDown.equalsIgnoreCase(Mc.SUPPORT_RATE_FLOAT_DOWN_FLAG);//Quick Fix Need to move to mossys later
   		return returnValue;
   }
     
  /**
   *  gets the value of instance fields as String.
   *  if a field does not exist (or the type is not serviced)** null is returned.
   *  if the field exists (and the type is serviced) but the field is null an empty
   *  String is returned.
   *  @returns value of bound field as a String;
   *  @param  fieldName as String
   *
   *  Other entity types are not yet serviced   ie. You can't get the Province object
   *  for province.
   */
  public String getStringValue(String fieldName)
  {
     return doGetStringValue(this, this.getClass(), fieldName);
  }
  /**
   *  Updates any Database field that has changed since the last synchronization
   *  ie. the last findBy... call
   *
   *  @return the number of updates performed
   */
  protected int performUpdate()  throws Exception
  {
      Class clazz = this.getClass();

      return (doPerformUpdate(this, clazz));
  }

  public List getSecondaryParents() throws Exception
  {
    List sps = new ArrayList();

    Object o = getContact();

    if(o != null)
    sps.add(o);

    return sps;
  }

   public String getEntityTableName()
   {
      return "LenderProfile" ;
   }	

  /**
   *   gets the profileStatusId associated with this entity
   */
   public int getProfileStatusId()
   {
      return this.profileStatusId ;
   }

   /**
    *   retrieves the Contact parent record associated with this entity
    *
    *   @return a Contact
    */
    public Contact getContact()throws FinderException,RemoteException
    {
      return new Contact(srk,this.getContactId(),1);
    }


		/**
     *   gets the lenderName associated with this entity
     *
     *   @return a String
     */
    public String getLenderName()
   {
      return this.lenderName ;
   }

		/**
     *   gets the defaultLender associated with this entity
     *
     *   @return a String
     */
    public char getDefaultLender()
   {
      return this.defaultLender ;
   }

		/**
     *   gets the pk associated with this entity
     *
     *   @return a LenderProfilePK
     */
    public IEntityBeanPK getPk()
   {
      return (IEntityBeanPK)this.pk ;
   }

		/**
     *   gets the lenderProfileId associated with this entity
     *
     *   @return a int
     */
    public int getLenderProfileId()
   {
      return this.lenderProfileId ;
   }

		/**
     *   gets the contactId associated with this entity
     *
     *   @return a int
     */
    public int getContactId()
   {
      return this.contactId ;
   }
   
   public String getLPShortName(){return this.lpShortName; }
   public String getLPBusinessId(){return this.lpBusinessId; }
   public boolean getUpfrontMIAllowed(){return this.upfrontMIAllowed; }
   public int    getDefaultInvestorId(){return this.defaultInvestorId; }
   public String getPrivateLabelIndicator(){return this.privateLabelIndicator; }
   public String getRegistrationName(){return this.registrationName; }
   public int    getInterestCompoundId(){return this.interestCompoundId; }

   /**
    * getSupportRateFloatDown
    * 	Returns supportRateFloatDown attribute value
    * 
    * @param none
    * 
    * @return String : the value of the supportRateFloatDown attribute
    * @author NBC/PP Implementation Team
    * @version 1.0 (Intial Version - 26 Jun 2006)
    */
   public String getSupportRateFloatDown () 
   { 
 	  return supportRateFloatDown; 
   }

  public LenderProfilePK createPrimaryKey()throws CreateException
  {
       String sql = "Select LenderProfileseq.nextval from dual";
       int id = -1;

       try
       {
           int key = jExec.execute(sql);

           for (; jExec.next(key);  )
           {
               id = jExec.getInt(key,1);  // can only be one record
           }
           
           jExec.closeData(key);

           if( id == -1 ) throw new Exception();
        }
        catch (Exception e)
        {

          CreateException ce = new CreateException("LenderProfile Entity create() exception getting LenderProfileId from sequence");
          logger.error(ce.getMessage());
          logger.error(e);
          throw ce;

        }

        return new LenderProfilePK(id);
  }



  /**
   * creates a LenderProfile using the LenderProfileId with mininimum fields
   *
   */

  public LenderProfile create(LenderProfilePK pk)throws RemoteException, CreateException
  {
    String sql = "Insert into LenderProfile(" + pk.getName() + ", InstitutionProfileID ) Values ( " + pk.getId() + ","+srk.getExpressState().getDealInstitutionId()+ ")";

    try
    {
      jExec.executeUpdate(sql);
      findByPrimaryKey(pk);
      trackEntityCreate();
    }
    catch (Exception e)
    {
      CreateException ce = new CreateException("LenderProfile Entity - LenderProfile - create() exception");
      logger.error(ce.getMessage());
      logger.error(e);

      throw ce;
    }
    srk.setModified(true);
    return this;
  }

   public void setLenderProfileId(int value)
	{
		 this.testChange("lenderProfileId", value);
		 this.lenderProfileId = value;
  }
   public void setDefaultLender(String lender)
	{

     if(lender != null && lender.length()==1)
		    this.defaultLender = lender.charAt(0);

      this.testChange("defaultLender", defaultLender);
  }
   public void setLenderName(String value )
	{
		 this.testChange("lenderName", value);
		 this.lenderName = value;
  }
   public void setContactId(int value )
	{
		 this.testChange("contactId", value);
		 this.contactId = value;
  }

  public void setProfileStatusId(int value )
	{
		 this.testChange("profileStatusId", value);
		this.profileStatusId = value;
  }

  public void setLPShortName(String value)
	{
	 this.testChange("lpShortName", value);
	 this.lpShortName = value;
  }

  public void setLPBusinessId(String value)
	{
	 this.testChange("lpBusinessId", value);
	 this.lpBusinessId = value;
  }

  public void setUpfrontMIAllowed(boolean value)
	{
	 this.testChange("upfrontMIAllowed", value);
	 this.upfrontMIAllowed = value;
  }
  public void setUpfrontMIAllowed(String value)
  {
    this.testChange("upfrontMIAllowed", value);
    this.upfrontMIAllowed = TypeConverter.booleanFrom(value);
  }
   public void setDefaultInvestorId(int value )
	{
		 this.testChange("defaultInvestorId", value);
		 this.defaultInvestorId = value;
  }

  public void setPrivateLabelIndicator(String value)
	{
	 this.testChange("privateLabelIndicator", value);
	 this.privateLabelIndicator = value;
  }
  
  public void setRegistrationName(String value)
	{
	 this.testChange("RegistrationName", value);
	 this.registrationName = value;
  }

  public void setInterestCompoundId(int value )
	{
		 this.testChange("interestCompoundId", value);
		 this.interestCompoundId = value;
  }

  /**
   * setSupportRateFloatDown
   * 	Sets supportRateFloatDown attribute value
   * 
   * @param value: String
   * 
   * @return none
   * @author NBC/PP Implementation Team
   * @version 1.0 (Intial Version - 26 Jun 2006)
   */
  public void setSupportRateFloatDown (String value) 
  { 
	  this.testChange("supportRateFloatDown", value);
	  this.supportRateFloatDown = value; 
  }

  public int getInstitutionProfileId() {
      return institutionProfileId;
  }

  public void setInstitutionProfileId(int institutionProfileId) {
      this.testChange("institutionProfileId", institutionProfileId); 
      this.institutionProfileId = institutionProfileId;
  }
 
 

}
