package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.AdjudicationApplicantRequestPK;
import com.basis100.deal.pk.BncAdjudicationApplRequestPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class AdjudicationApplicantRequest extends DealEntity
{
	protected int requestId;
	protected int applicantNumber;
	protected int copyId;
	protected int borrowerId;
	protected int instProfileId;
	protected String applicantCurrentInd;

	public AdjudicationApplicantRequestPK pk;

	public AdjudicationApplicantRequest(SessionResourceKit srk)
		throws RemoteException
	{
		super(srk);
	}

	public AdjudicationApplicantRequest(SessionResourceKit srk, int id, int applicantNumber, int copyId)
		throws RemoteException, FinderException
	{
		super(srk);
		pk = new AdjudicationApplicantRequestPK(id, applicantNumber, copyId );
		findByPrimaryKey(pk);
	}

	/**  search for AdjudicationApplicantRequest record using primary key
	 *   @return a AdjudicationApplicantRequest */
	public AdjudicationApplicantRequest findByPrimaryKey(AdjudicationApplicantRequestPK pk)
		throws RemoteException, FinderException
	{
		if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
		String sql = "Select * from AdjudicationApplicantRequest " + pk.getWhereClause();

		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg =
					"AdjudicationApplicantRequest: @findByPrimaryKey(), key= " + pk +
					", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException(
				"AdjudicationApplicantRequest Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}
		this.pk = pk;
		ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
		return this;
	}

	/**  sets properties for AdjudicationApplicantRequest */
	public void setPropertiesFromQueryResult(int key)
		throws Exception
	{
		setRequestId(jExec.getInt(key, "requestid"));
		setApplicantNumber(jExec.getInt(key, "applicantNumber"));
		setCopyId(jExec.getInt(key, "copyid"));
		setBorrowerId(jExec.getInt(key, "borrowerid"));
		setApplicantCurrentInd(jExec.getString(key, "applicantCurrentInd"));
		setInstProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
	}

	/**
	 *  gets the value of instance fields as String.
	 *  if a field does not exist (or the type is not serviced)** null is returned.
	 *  if the field exists (and the type is serviced) but the field is null an empty
	 *  String is returned.
	 *  @returns value of bound field as a String;
	 *  @param  fieldName as String
	 *
	 *  Other entity types are not yet serviced   ie. You can't get the Province object
	 *  for province.
	 */
	@Override
	public String getStringValue(String fieldName)
	{
		return doGetStringValue(this, this.getClass(), fieldName);
	}

	/**sets the value of instance fields as String.
	 *  if a field does not exist or is not serviced an Exception is thrown
	 *  if the field exists (and the type is serviced) but the field is null an empty
	 *  String is returned.
	 *
	 *  @param  fieldName as String
	 *  @param  the value as a String
	 *
	 *  Other entity types are not yet serviced   ie. You can't set the Province object
	 *  in Addr. */
	@Override
	public void setField(String fieldName, String value)
		throws Exception
	{
		doSetField(this, this.getClass(), fieldName, value);
	}

	/** Updates any Database field that has changed since the last synchronization
	 *  ie. the last findBy... call
	 *  @return the number of updates performed */
	@Override
	protected int performUpdate()
		throws Exception
	{
		Class clazz = this.getClass();
		int ret = doPerformUpdate(this, clazz);
		return ret;
	}

	@Override
	public String getEntityTableName()
	{
		return "AdjudicationApplicantRequest";
	}

	/**  gets the pk associated with this entity
	 *   @return a AdjudicationApplicantrequestPK */
	@Override
	public IEntityBeanPK getPk()
	{
		return (IEntityBeanPK)this.pk;
	}

	public String getApplicantCurrentInd()
	{
		return this.applicantCurrentInd;
	}

	public void setApplicantCurrentInd(String ind)
	{
		this.testChange("applicantCurrentInd", ind);
		this.applicantCurrentInd = ind;
	}

	public int getApplicantNumber()
	{
		return this.applicantNumber;
	}

	public void setApplicantNumber(int id)
	{
		this.testChange("applicantNumber", id);
		this.applicantNumber = id;
	}

	public int getBorrowerId()
	{
		return this.borrowerId;
	}

	public void setBorrowerId(int id)
	{
		this.testChange("borrowerId", id);
		this.borrowerId = id;
	}

	public int getCopyId()
	{
		return this.copyId;
	}

	public void setCopyId(int id)
	{
		this.testChange("copyId", id);
		this.copyId = id;
	}

	public int getRequestId()
	{
		return this.requestId;
	}

	public void setRequestId(int id)
	{
		this.testChange("requestId", id);
		this.requestId = id;
	}

	/**  creates AdjudicationApplicantRequest record into database
	 *   @return a AdjudicationApplicantRequest */
	public AdjudicationApplicantRequest create(RequestPK rpk, BorrowerPK bpk,
										int appnum, String appcurrind)
		throws RemoteException, CreateException
	{
		int key = 0;
		// Create BORROWERREQUESTASSOC record first
		StringBuffer sqla = new StringBuffer(
			"Insert into BorrowerRequestAssoc( requestId" +
			",copyId, borrowerId,INSTITUTIONPROFILEID) Values ( ");
		sqla.append(sqlStringFrom(rpk.getId()) + ", ");
		sqla.append(sqlStringFrom(bpk.getCopyId()) + ", ");
		sqla.append(sqlStringFrom(bpk.getId()) + ", ");
		sqla.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");

		String sql = new String(sqla);

		try
		{
			key = jExec.executeUpdate(sql);
			this.trackEntityCreate(); // track the creation
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException(
				"AdjudicationApplicantRequest Entity - AdjudicationApplicantRequest - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);
			throw ce;
		}
		finally
		{
			try
			{
				if (key != 0)
				{
					jExec.closeData(key);
				}
			}
			catch (Exception e)
			{
				logger.error(e.toString());
			}
		}

		key = 0;
		// Insert ADJUDICATIONAPPLICANT record next
		StringBuffer sqlb = new StringBuffer(
			"Insert into AdjudicationApplicantRequest( requestId" +
			",applicantNumber, copyId, borrowerId, applicantCurrentInd,INSTITUTIONPROFILEID) Values ( ");
		sqlb.append(sqlStringFrom(rpk.getId()) + ", ");
		sqlb.append(sqlStringFrom(appnum) + ", ");
		sqlb.append(sqlStringFrom(bpk.getCopyId()) + ", ");
		sqlb.append(sqlStringFrom(bpk.getId()) + ", ");
		sqlb.append(sqlStringFrom(appcurrind) + ", ");
		sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ")");

		String sql2 = new String(sqlb);

		try
		{
			key = jExec.executeUpdate(sql2);
			findByPrimaryKey(new AdjudicationApplicantRequestPK(rpk.getId(), appnum, rpk.getCopyId()));
			this.trackEntityCreate(); // track the creation
		}
		catch (Exception e)
		{

			CreateException ce = new CreateException(
				"AdjudicationApplicantRequest Entity - AdjudicationApplicantRequest - create() exception");
			logger.error(ce.getMessage());
			logger.error(e);

			throw ce;
		}
		finally
		{
			try
			{
				if (key != 0)
				{
					jExec.closeData(key);
				}
			}
			catch (Exception e)
			{
				logger.error(e.toString());
			}
		}

		srk.setModified(true);
		return this;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof AdjudicationApplicantRequest)
		{
			AdjudicationApplicantRequest oa = (AdjudicationApplicantRequest)o;
			if (this.requestId == oa.getRequestId())
			{
				return true;
			}
		}
		return false;
	}
	
	public AdjudicationApplicantRequest findByBorrowerAndRequestId(RequestPK requestPK, BorrowerPK borrowerPK) throws Exception {
		AdjudicationApplicantRequest adjapplicant = null;
		
		String sql = "Select * from AdjudicationApplicantRequest ";
		sql += borrowerPK.getWhereClause();
		sql += " and requestid=" + requestPK.getId();
		try
		{
			int key = jExec.execute(sql);
			for (; jExec.next(key); )
			{
				adjapplicant = new AdjudicationApplicantRequest(
					srk);
				adjapplicant.setPropertiesFromQueryResult(key);
				adjapplicant.pk = new AdjudicationApplicantRequestPK(adjapplicant.
					getRequestId(), adjapplicant.getApplicantNumber(), adjapplicant.getCopyId());
			}
			jExec.closeData(key);

		}
		catch (Exception e)
		{
			Exception fe = new Exception(
				"Exception caught while getting Borrower::AdjudicationApplicantRequest");
			logger.error(fe.getMessage());
			logger.error(e);

			throw fe;
		}

		return adjapplicant;
	}

	public Collection findByBorrower(BorrowerPK pk)
		throws Exception
	{
		Collection adjapplicants = new ArrayList();

		String sql = "Select * from AdjudicationApplicantRequest ";
		sql += pk.getWhereClause();
		try
		{
			int key = jExec.execute(sql);
			for (; jExec.next(key); )
			{
				AdjudicationApplicantRequest adjapplicant = new AdjudicationApplicantRequest(
					srk);
				adjapplicant.setPropertiesFromQueryResult(key);
				adjapplicant.pk = new AdjudicationApplicantRequestPK(adjapplicant.
					getRequestId(), adjapplicant.getApplicantNumber(), adjapplicant.getCopyId());
				adjapplicants.add(adjapplicant);
			}
			jExec.closeData(key);

		}
		catch (Exception e)
		{
			Exception fe = new Exception(
				"Exception caught while getting Borrower::AdjudicationApplicantRequest");
			logger.error(fe.getMessage());
			logger.error(e);

			throw fe;
		}

		return adjapplicants;
	}

	public Collection findByRequest(RequestPK pk)
		throws Exception
	{
		Collection adjapplicants = new ArrayList();

		String sql = "Select * from AdjudicationApplicantRequest ";
		sql += pk.getWhereClause();
		try
		{
			int key = jExec.execute(sql);
			for (; jExec.next(key); )
			{
				AdjudicationApplicantRequest adjapplicant = new AdjudicationApplicantRequest(srk);
				adjapplicant.setPropertiesFromQueryResult(key);
				adjapplicant.pk = new AdjudicationApplicantRequestPK(adjapplicant.getRequestId(),
					adjapplicant.getApplicantNumber(), adjapplicant.getCopyId());
				adjapplicants.add(adjapplicant);
			}
			jExec.closeData(key);

		}
		catch (Exception e)
		{
			Exception fe = new Exception(
				"Exception caught while getting Borrower::AdjudicationApplicantRequest");
			logger.error(fe.getMessage());
			logger.error(e);

			throw fe;
		}

		return adjapplicants;
	}

	public BncAdjudicationApplRequest getBncAdjudicationApplRequest()
		throws Exception
	{
		BncAdjudicationApplRequestPK bncAdjApplReqPK = new BncAdjudicationApplRequestPK(pk.getId(), this.applicantNumber ,this.copyId);
		return new BncAdjudicationApplRequest(srk).findByPrimaryKey(bncAdjApplReqPK);
	}


	public void setPk(AdjudicationApplicantRequestPK pk)
	{
		this.pk = pk;
	}

	public Object clone()
		throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}

	/**
	 * Clones everything except for the PK. Inserts the new record into the DB.
	 */
	public Object clone(RequestPK rpk)
		throws CloneNotSupportedException
	{
		AdjudicationApplicantRequest clone = null;

		try
		{
			// create applicant to generate the new PK
			clone = new AdjudicationApplicantRequest(srk);
			clone.create(rpk,
						 new BorrowerPK(this.getBorrowerId(), 
                            this.getCopyId()),
						 this.getApplicantNumber(), this.getApplicantCurrentInd());

			// save newly created PK before cloning.
			AdjudicationApplicantRequestPK aPK = (AdjudicationApplicantRequestPK)clone.getPk();

			// call object's clone (create shallow copy).
			clone = (AdjudicationApplicantRequest)super.clone();

			// restore PK
			clone.setPk(aPK);
			clone.setApplicantNumber(aPK.getAppNum());
			clone.setCopyId(aPK.getCopyId());
			clone.setRequestId(aPK.getId());

			clone.doPerformUpdateForce();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			logger.error(
				"Error in AdjudicationApplicantRequest.clone(). Trying to clone " +
				"applicantId = " + applicantNumber + "; requestId = " +
				requestId);
		}

		return clone;

	}

	/**
	 * We need to override this because we also need to delete the borrowerrequestassociation as well as the adj applicant
	 * @param needCalculation boolean
	 * @throws RemoteException
	 */
	public void ejbRemove(boolean needCalculation)
		throws RemoteException
	{
		String sql = null;
		IEntityBeanPK pk = getPk();

		try
		{
			this.removeChild();

			// first delete the record as normal
			super.ejbRemove(needCalculation);

        //don not need this -- this is being taken care in the Request.removeChildAssoc()
			/*
			// then delete the association
			sql = "Delete From BORROWERREQUESTASSOC where REQUESTID = " +
				this.getRequestId() + " and BORROWERID = " + this.getBorrowerId();
			jExec.executeUpdate(sql);
		    */	
		}
		catch (Exception e)
		{
			String msg = "Failed to remove record " + pk.getWhereClause();
			RemoteException re = new RemoteException(msg);
			logger.error(re.getMessage());
			logger.error("remove sql: " + sql);
			logger.error(e);

			throw re;
		}

	}

	protected void removeChild() throws Exception 
	{
		BncAdjudicationApplRequest bncAdjApplRequest = null;
		try {
			bncAdjApplRequest = getBncAdjudicationApplRequest();
		} catch (Exception e) {
			//swallow the exception because the child does not exist.
		}
		if (bncAdjApplRequest != null)
				bncAdjApplRequest.ejbRemove();
	}
	
	/**
     * return all children as List.
     * there is only one children type BncAdjudicationApplRequest
     */
    public List getChildren() throws Exception {
      List children = new ArrayList();
 
      children.add(this.getBncAdjudicationApplRequest());
 
      return children;
    }

	public int getInstProfileId() {
		return instProfileId;
	}

	public void setInstProfileId(int instProfileId) {
		this.testChange("INSTITUTIONPROFILEID", instProfileId);
		this.instProfileId = instProfileId;
	}
    
}
