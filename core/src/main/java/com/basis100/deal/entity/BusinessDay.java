package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.BusinessDayPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;

/**
 * A class representing business day entity specific for MOS.
 * Business day entity stores one record defining business week
 * start and end days, start and end times
 *
 * @author  Bartek Jasionowski
 * @version
 * @see
 * @date
*/
public class BusinessDay extends DealEntity{

   EntityContext ctx;

   protected  int businessDayId;
   protected  String weekStart;
   protected  String weekEnd;
   protected  int startHour;
   protected  int startMinute;
   protected  int endHour;
   protected  int endMinute;


   // derived attributes: start and end times (minutes from beginning of day (set via calcStartEndTimes())
   private  int startTime;
   private  int endTime;

   // derived attributes: day number for start week day and end week day respectively (set via
   // setWeekStart() and setWeekEnd())
   private  int weekStartDayNum;
   private  int weekEndDayNum;

   protected int institutionId;
   protected String baseTimeZone;

   /**
   * @return the baseTimeZone
   */
  public String getBaseTimeZone() {
    return baseTimeZone;
  }

  /**
   * @param baseTimeZone the baseTimeZone to set
   */
  public void setBaseTimeZone(String baseTimeZone) {
    this.baseTimeZone = baseTimeZone;
  }

  /**
   * @return the institutionid
   */
  public int getInstitutionId() {
    return institutionId;
  }

  /**
   * @param institutionid the institutionid to set
   */
  public void setInstitutionId(int institutionId) {
    this.testChange("institutionProfileId", institutionId);
    this.institutionId = institutionId;
  }

  private BusinessDay()  // to support privateCopy()
   {
   }

   public BusinessDay(SessionResourceKit srk) throws RemoteException, FinderException {
     super(srk);

     this.findByName();
   }


   public BusinessDay(SessionResourceKit srk, int id) throws RemoteException, FinderException
   {
      super(srk);

      BusinessDayPK pk = new BusinessDayPK(id);

      this.findByPrimaryKey(pk);
   }

  /**
   * creates a BusinessDay  using the BusinessDayId
   *
   * In the current release only one business day entity is allowed, and this record must be added
   * manually to the database. Hence this method simply throws an exception.
   *
   * @param
   * @return BusinessDay
   */
   public BusinessDay create() throws RemoteException, CreateException
   {
      CreateException ce = new CreateException("BusinessDay Entity - create() exception :: create() not allowed!");
      logger.error(ce.getMessage());

      throw ce;
    }



  /**
   * remove a BusinessDay entity
   *
   * In the current release only one business day entity is allowed, and this record must be added
   * manually to the database. Hence this method simply throws an exception.
   *
   * @param
   * @return
   */
    public void remove() throws RemoteException, RemoveException
    {
        RemoveException re = new RemoveException("BusinesdDay Entity - remove() exception");

        logger.error(re.getMessage());

        throw re;
   }



   /**
    * Attempts to find the record that corresponds to the description/ name disregarding Case
    * @param  a String representation of the name
    * @return this instance
    */
   public BusinessDay findByName() throws RemoteException, FinderException
   {
      String sql = "Select * from BusinessDays";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
               gotRecord = true;
               setPropertiesFromQueryResult(key);
               break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Business Calendar: BusinessDays: Select * = ";
            logger.error(msg);
            return null;
          }
        }
        catch (Exception e)
        {
          logger.error("finder sql: " + sql);
          logger.error(e);
          return null;
        }
      return this;
   }

   /**
    * Attempts to find the record that corresponds to primary key
    * @param  a BusinessDayPK
    * @return BusinessDay
    */
    public BusinessDay findByPrimaryKey(BusinessDayPK pk) throws RemoteException, FinderException
   {
      if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
      String sql = "Select * from BusinessDays where " + pk.getName()+ " = '" + pk.getId() + "'";

      boolean gotRecord = false;

      try
      {
          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
              gotRecord = true;
              setPropertiesFromQueryResult(key);
              /** can only be one record */
               break;
          }

          jExec.closeData(key);

          if (gotRecord == false)
          {
            String msg = "Business Calendar: BusinessDays: @findByPrimaryKey(), key= " + pk + ", holiday not found";
            logger.error(msg);
            throw new FinderException(msg);
          }
        }
        catch (Exception e)
        {
            FinderException fe = new FinderException("Business Calendar: BusinessDays - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }
        ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
        return this;
    }

    /**
     * Methods implemented in Enterprise Bean Instances that have no remote object
     * counterparts - i.e. EJB container callback methods, and context related methods.
    */
    public void setEntityContext(EntityContext ctx)
    {
        this.ctx = ctx;
    }

    public void unsetEntityContext()
    {
        ctx = null;
    }

    public void ejbActivate() throws RemoteException{;}

    public void ebjPassivate () throws RemoteException {;}

    public void ebjLoad() throws RemoteException
    {
        /** implementation not required for now! */
    }


    public void ejbStore() throws RemoteException
    {
        String sql = "Update BusinessDays set "  +
                     "businessDayId    = "            + getBusinessDayId() + ", " +
                     "weekStart  = '"                 + getWeekStart() + "', " +
                     "weekEnd = '"                    + getWeekEnd() + "', " +
                     "dayStartHour  = '"              + getStartHour() + "', " +
                     "dayStartMinute = "              + getStartMinute() + ", " +
                     "dayEndHour = "                  + getEndHour() + ", " +
                     "dayEndMinute = "                + getEndMinute() + ", " +
                     "Where businessDayId = "        + getBusinessDayId();

        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoteException re = new RemoteException("BusinessDay Entity - ejbStore() exception");

            logger.error(re.getMessage());
            logger.error("ejbStore sql: " + sql);
            logger.error(e);

            throw re;
        }
        
        //4.4 Entity Cache
        ThreadLocalEntityCache.writeToCache(this, this.getPk());
    }

/**
  *   similar to a clone operation - however does not return a true clone of opject since
  *   the 'data' members copied (e.g. not concerned with base class members, session resource
  *   kit, etc). Assumption is that the 'copy' will be used only for data access/manipulation
  *   only, not for updating the entity to persistent storage.
  *
  *   @return an int
  */

    public BusinessDay privateCopy()
    {
      BusinessDay b = new BusinessDay();

      b.setBusinessDayId(getBusinessDayId());
      b.setWeekStart(getWeekStart());
      b.setWeekEnd(getWeekEnd());
      b.setStartHour(getStartHour());
      b.setStartMinute(getStartMinute());
      b.setEndHour(getEndHour());
      b.setEndMinute(getEndMinute());

      return b;
   }

/**
  * Convienence methods for adding units to various attributes; units to be 'added' may be
  * negative amounts.
  *
  * The result is a flag that indicates if the result of the operation is within the normal
  * integrity range for the attribute manipulated. Example:
  *
  *      assume startMinute = 30
  *
  *      addToStartMinute(10) == true (result 40 within range 0-59)
  *      addToStartMinute(40) == false (result 70 outside integrity range)
  *
  * Application has responsibility for ensuring ultimate integrity of results of an operation; e.g.
  * if adding to minutes causes an integrity problem application must correct.
  *
  **/

    public boolean addToStartHour (int units)
    {
      startHour += units;

      return hourIntegrity(startHour);
    }

    public boolean addToEndHour (int units)
    {
      endHour += units;

      return hourIntegrity(endHour);
    }

    public boolean addToStartAndEndHour (int units)
    {
      startHour += units;
      endHour += units;

      return hourIntegrity(startHour) && hourIntegrity(endHour);
    }

    public boolean addToStartMinute (int units)
    {
      startMinute += units;

      return minuteIntegrity(startMinute);
    }

    public boolean addToEndMinute (int units)
    {
      endMinute += units;

      return minuteIntegrity(endMinute);
    }

    public boolean addToStartAndEndMinute (int units)
    {
      startMinute += units;
      endMinute += units;

      return minuteIntegrity(startMinute) && minuteIntegrity(endMinute);
    }

   public String toString()
   {
     String sm = "00" + startMinute;
     sm = sm.substring(sm.length()-2);

     String em = "00" + endMinute;
     em = em.substring(em.length()-2);

     return "BusinessDay (id=" + businessDayId + ") Week Start/End " +  weekStart + "/" + weekEnd +
            ", Day Start/End " + startHour + ":" + sm + "/" + endHour + ":" + em;
   }


/**
  * Calculate start and end times - minutes from beginning of day
  *
  **/

  public void calcStartEndTimes()
  {
    startTime = startHour * 60 + startMinute;
    endTime   = endHour   * 60 + endMinute;
  }

	  /**
     *   gets the startHour associated with this entity
     *
     *   @return an int
     */
    public int getStartHour () {
      return this.startHour;
    }



    /**
     *   gets the Day startMinute associated with this entity
     *
     *   @return an int
     */

    public int getStartMinute () {
      return this.startMinute;
    }


    /**
     *   gets the day endHour associated with this entity
     *
     *   @return an int
     */
    public int getEndHour () {
      return this.endHour;
    }

     /**
     *   gets the day endMinute associated with this entity
     *
     *   @return an int
     */
    public int getEndMinute () {
      return this.endMinute;
    }

     /**
     *   gets the day start time (ninutes from beginning of day) associated with this entity
     *
     *   @return an int
     */
    public int getStartTime () {
      return startTime;
    }

     /**
     *   gets the day end time (ninutes from beginning of day) associated with this entity
     *
     *   @return an int
     */
    public int getEndTime () {
      return endTime;
    }

    /**
     *   gets the BusinessDayId associated with this entity
     *
     *   @return an int
     */
   public int getBusinessDayId()
	 {
     return businessDayId;
   }

   /**
     *   gets the weekStart associated with this entity
     *
     *   @return a String
     */
   public String getWeekStart()
   {
    return weekStart;
   }


   /**
     *   gets the weekEnd associated with this entity
     *
     *   @return a String
     */
   public String getWeekEnd()
   {
    return weekEnd;
   }

   /**
     *   gets the weekStartDayNum associated with this entity
     *
     *   @return a String
     */
   public int getWeekStartDayNum()
   {
    return weekStartDayNum;
   }


   /**
     *   gets the weekEndDayNum associated with this entity
     *
     *   @return a String
     */
   public int getWeekEndDayNum()
   {
    return weekEndDayNum;
   }




  /**
     *  sets the weekStart associated with this entity
     *
     *  @param  String weekStart
     *  @return
  */
  public void setWeekStart (String weekStart) {
    this.weekStart = weekStart;

    weekStartDayNum = getDay(weekStart);
  }

  /**
     *  sets the weekEnd associated with this entity
     *
     *  @param  String weekEnd
     *  @return
  */
  public void setWeekEnd (String weekEnd) {
    this.weekEnd = weekEnd;

    weekEndDayNum = getDay(weekEnd);
  }


  /**
    *  sets the startHour associated with this entity
    *
    *  @param  int startHour
    *  @return
  */
  public void setStartHour (int startHour) {
    this.startHour = startHour;
  }


  /**
    *  sets the startMinute associated with this entity
    *
    *  @param  int startMinute
    *  @return
  */
  public void setStartMinute (int startMinute) {
    this.startMinute = startMinute;
  }


  /**
    *  sets the endHour associated with this entity
    *
    *  @param  int endHour
    *  @return
  */
  public void setEndHour (int endHour) {
    this.endHour = endHour;
  }


  /**
    *  sets the endMinute associated with this entity
    *
    *  @param  int endMinute
    *  @return
  */
  public void setEndMinute (int endMinute) {
    this.endMinute = endMinute;
  }


  /**
     *  sets the businessDayId associated with this entity
     *
     *  @param  int id
     *  @return
  */
  public void setBusinessDayId(int id)
	{
    this.businessDayId = id;
  }

  /**
   * Implementation
  */
   private void setPropertiesFromQueryResult(int key) throws Exception
   {
      setBusinessDayId(jExec.getInt(key, "BUSINESSDAYID"));
      setWeekStart(jExec.getString(key, "WEEKSTART"));
      setWeekEnd(jExec.getString(key, "WEEKEND"));
      setStartHour(jExec.getInt(key, "DAYSTARTHOUR"));
      setStartMinute(jExec.getInt(key, "DAYSTARTMINUTE"));
      setEndHour(jExec.getInt(key, "DAYENDHOUR"));
      setEndMinute(jExec.getInt(key,"DAYENDMINUTE"));
      setBaseTimeZone(jExec.getString(key, "BASETIMEZONE"));
      setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
   }

    private boolean hourIntegrity(int h)
    {
      if (h >= 0 && h <= 23)
        return true;

      return false;
    }

    private boolean minuteIntegrity(int m)
    {
      if (m >= 0 && m <= 60)
        return true;

      return false;
    }

/**
  *  Gets the int from a given day of the week. Sunday = 1 ... Saturday = 7
  *
  *  WARNING
  *
  *  Assumes english day names.
  *
  *  @param     dayOfWeek  String representation of the day of week
  *  @return dayNumber int corresponding to a day of the week
  **/

  public int getDay(String dayOfWeek)
   {
    if (dayOfWeek == null)
      return 0;

    dayOfWeek = dayOfWeek.toUpperCase();

    int day = 0;

    if (dayOfWeek.startsWith("MO")) {
      day = 2;
    }
    else if (dayOfWeek.startsWith("TU")) {
      day = 3;
    }
    else if (dayOfWeek.startsWith("WE")) {
      day = 4;
    }
    else if (dayOfWeek.startsWith("TH")) {
      day = 5;
    }
    else if (dayOfWeek.startsWith("FR")) {
      day = 6;
    }
    else if (dayOfWeek.startsWith("SA")) {
      day = 7;
    }
    else if (dayOfWeek.startsWith("SU")) {
      day = 1;
    }

    return day;
  }


}