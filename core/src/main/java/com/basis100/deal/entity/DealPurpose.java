/**
 * <p>Title: DealPurpose.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jun 5, 2006)
 *
 */

package com.basis100.deal.entity;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPurposePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class DealPurpose extends DealEntity {
    protected int dealPurposeId;
    protected String dpDescription;
    protected boolean refinanceFlag;

    private DealPurposePK pk;
    

    public DealPurpose(SessionResourceKit srk) throws RemoteException,
	    FinderException {
	super(srk);
    }

    public DealPurpose(SessionResourceKit srk, int id) throws RemoteException,
	    FinderException {
	super(srk);

	pk = new DealPurposePK(id);

	findByPrimaryKey(pk);
    }

    public DealPurpose findByPrimaryKey(DealPurposePK pk)
	    throws RemoteException, FinderException {
    	
    if (ThreadLocalEntityCache.readFromCache(this, pk)) return this; // 4.4 Entity Cache
	String sql = "Select * from DealPurpose " + pk.getWhereClause();

	boolean gotRecord = false;

	try {
	    int key = jExec.execute(sql);

	    for (; jExec.next(key);) {
		gotRecord = true;
		setPropertiesFromQueryResult(key);
		break; // can only be one record
	    }

	    jExec.closeData(key);

	    if (gotRecord == false) {
		String msg = "AssetType: @findByPrimaryKey(), key= " + pk
			+ ", entity not found";
		logger.error(msg);
		throw new FinderException(msg);
	    }
	} catch (Exception e) {
	    FinderException fe = new FinderException(
		    "AssetType Entity - findByPrimaryKey() exception");

	    logger.error(fe.getMessage());
	    logger.error("finder sql: " + sql);
	    logger.error(e);

	    throw fe;
	}
	this.pk = pk;
	ThreadLocalEntityCache.writeToCache(this, pk); // 4.4 Entity Cache
	return this;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
	setDealPurposeId(jExec.getInt(key, "DEALPURPOSEID"));
	setDpDescription(jExec.getString(key, "DPDESCRIPTION"));
	String refinanceFlg = jExec.getString(key, "REFINANCEFLAG");
	if (YES.equals(refinanceFlg)) {
	    setRefinanceFlag(true);
	} else if (NO.equals(refinanceFlg)) {
	    setRefinanceFlag(false);
	}
    }

    public void setDealPurposeId(int id) {
	this.dealPurposeId = id;
    }

    public void setDpDescription(String desc) {
	this.dpDescription = desc;
    }

    public void setRefinanceFlag(boolean yn) {
	this.refinanceFlag = yn;
    }

    public int getDealPurposeId() {
	return dealPurposeId;
    }

    public String getDpDescription() {
	return dpDescription;
    }

}
