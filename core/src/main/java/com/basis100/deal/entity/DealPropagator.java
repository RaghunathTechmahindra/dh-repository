package com.basis100.deal.entity;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

/**
 * this class provides the ability of updating deal records without specifying
 * copy-id. if CalcMoniter is set,it attaches all copies of deal to it.
 * 
 * @author Hiro
 * @since 5.0
 * @version 1.0, Nov 14, 2011
 * 
 */
public class DealPropagator extends Deal {

    private static final long serialVersionUID = 1L;
    private final static Logger logger = LoggerFactory.getLogger(DealPropagator.class);

    public DealPropagator(Deal deal, CalcMonitor dcm) throws RemoteException, FinderException {
        this(deal.srk, dcm, deal.getDealId(), deal.getCopyId());
    }

    public DealPropagator(SessionResourceKit srk, CalcMonitor dcm, int id, int copyId) throws RemoteException, FinderException {
        super(srk, dcm, id, copyId);
    }

    /**
     * this is the same as super.doPerformUpdate except it execute update SQL
     * without copyid
     */
    @SuppressWarnings("rawtypes")
    @Override
    protected int doPerformUpdate(Object me, Class meInfo) throws Exception {

        StringBuffer buffer = new StringBuffer();
        buffer.append("Update " + getEntityTableName() + " set ");

        if (formUpdateFieldsList(buffer, true) == null) {
            return 0;
        }

        DealPK dealPk = (DealPK) super.getPk();
        // this class doesn't use copyId so that data change happens on all
        // copies
        buffer.append(dealPk.getWhereClauseDealIdOnly());

        String sql = buffer.toString();
        int ret = jExec.executeUpdate(sql);

        logger.debug("Propagation SQL updated " + ret + " record(s). SQL=" + sql);

        return ret;
    }

    /**
     * this method called by ejbStore. this method tries as follows, 1. updates
     * mortgageInsuranceResponse in all copies 2. execute cracked
     * doPerformUpdate so that propagating SQL is executed 3. when 'dcm' exists,
     * attaches all copies so that calc runs on all copies
     */
    @SuppressWarnings("unchecked")
    @Override
    protected int performUpdate() throws Exception {

        if (isValueChange("mortgageInsuranceResponse")) {
            updateMortgageInsuranceResponse(true);// all copies
            resetChange("mortgageInsuranceResponse");
        }

        int ret = doPerformUpdate(null, null);
        if (ret > 0 && dcm != null) {
            for (Deal deal : findAllCopies()) {
                // let each deal know the fields that has been changed in 'this'
                // instance
                deal.changeValueMap = cloneChangeValueMap(this.changeValueMap, deal.getCopyId());
                // single dcm can distinguish each deal copy because deal.equals
                // method can
                dcm.inputEntity(deal);
            }
        }

        return ret;
    }

    /**
     * this class wants to update all the copies. even though 'this' copy's
     * value in some field is the same as the value assigned, this class wants
     * to include such fields in the update SQL because other copy might have
     * different value. Thus, when new value is assigned, this method always
     * sets 'true' to the changeFlag
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void doTestChange(String propertyName, String newValue) {

        ChangeMapValue changeMapValue = (ChangeMapValue) (this.changeValueMap.get(propertyName));

        if (changeMapValue == null) // First Time , initial it
        {
            changeMapValue = new ChangeMapValue(propertyName, newValue);
            changeMapValue.setChangeFlag(false);
            this.changeValueMap.put(propertyName, changeMapValue);
        } else {
            // force change whenever it's updated
            changeMapValue.setCurValue(newValue);
            changeMapValue.setChangeFlag(true); // force change!!
        }
    }

    private static Map<String, ChangeMapValue> cloneChangeValueMap(Map<String, ChangeMapValue> cvMap, int copyId) {

        Map<String, ChangeMapValue> clone = new HashMap<String, ChangeMapValue>();

        for (String key : cvMap.keySet()) {
            ChangeMapValue val = cvMap.get(key);
            clone.put(key, val.duplicate());
        }
        // just in case, change value of copyid, we don't really need this
        ChangeMapValue cmvCopyId = clone.get("copyId");
        cmvCopyId.setCurValue(String.valueOf(copyId));
        cmvCopyId.setOrigValue(String.valueOf(copyId));
        cmvCopyId.setChangeFlag(false);

        return clone;
    }
}