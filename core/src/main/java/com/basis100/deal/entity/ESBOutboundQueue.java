package com.basis100.deal.entity;

import java.sql.PreparedStatement;
import java.util.Date;

import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ServiceConst;

public class ESBOutboundQueue extends DealEntity {

    protected int esbOutboundQueueId;
    protected int institutionProfileId;
    protected int serviceTypeId;
    protected int dealId;
    protected int systemTypeId;
    protected int userProfileId;
    protected int attemptCounter = 0 ;
    protected Date retryTimeStamp;
    protected int transactionStatusId;
    protected String transactionStatusDescription;
    protected Date timeStamp;
    //2 new fields for Data Alignment....
    protected int serviceSubTypeid;
    //protected int esbQueueStatusCategoryId;

    private ESBOutboundQueuePK pk;

    /**
     * @param srk SessionResourceKit to use
     * @throws RemoteException
     */
    public ESBOutboundQueue (SessionResourceKit srk) throws RemoteException {
        super(srk);
    }

    /**
     *
     * @param srk SessionResourceKit to us
     * @param id StatusEventQueue id
     * @throws RemoteException
     * @throws FinderException 
     */
    public ESBOutboundQueue (SessionResourceKit srk, int id) throws RemoteException, FinderException {
        super(srk);
        pk = new ESBOutboundQueuePK(id);
        this.findByPrimaryKey(pk);
    }

    /**
     * Find the row corresponding to this PK and populate entity
     * @param pk The primary key represetning this row
     * @return This (now populated) entity
     */
    public ESBOutboundQueue findByPrimaryKey(ESBOutboundQueuePK pk)
        throws RemoteException, FinderException {

    	String sql = "SELECT * FROM ESBOUTBOUNDQUEUE" + pk.getWhereClause();
        boolean gotRecord = false;

        try {
            int key = jExec.execute(sql);
            for(;jExec.next(key);) {
                gotRecord = true;
                this.pk = pk;
                setPropertiesFromQueryResult(key);
                //break;
            }

            jExec.closeData(key);

            if(!gotRecord) {
                String msg = "ESBOUTBOUNDQUEUE Entity: @findByQuery(), key= " + pk + ", entity not found";
                logger.error(msg);
                throw new FinderException(msg);
            }
        } catch(Exception e) {
            FinderException fe = new FinderException("ESBOUTBOUNDQUEUE findByPrimaryKey(): exception: " + e);
            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        return this;
    }

    /**
     * Create a new primary key from the status event queue sequence
     * @return StatusEventQueuePK
     * @throws CreateException
     */
    public ESBOutboundQueuePK createPrimaryKey() throws CreateException
    {
        String sql = "SELECT ESBOUTBOUNDQUEUESEQ.nextval from dual";
        int id = -1;

        try {
            int key = jExec.execute(sql);
            for(;jExec.next(key);) {
                id = jExec.getInt(key, 1);
                break;
            }

            jExec.closeData(key);

            if(id < 0) {
                throw new CreateException("StatusEventQueue createPrimaryKey(): no ESBOUTBOUNDQUEUESEQ found");
            }
        }
        catch(Exception e) {
            CreateException ce = new CreateException("ESBOUTBOUNDQUEUESEQ createPrimaryKey(): exception: " + e );
            logger.error(ce.getMessage());
            logger.error(e);
            throw ce;
        }

        return new ESBOutboundQueuePK(id);
    }

    /**
     * Create a new ESBOUTBOUNDQUEUE object with minimum fields, insert into database and return.
     * @param pk
     * @param institutionProfileId
     * @param serviceTypeId
     * @param dealId
     * @param systemTypeId
     * @param userProfileId
     * @return The newly created ESBOUTBOUNDQUEUE Object
     * @throws RemoteException
     * @throws CreateException
     * 
     * modification for data alignment: new records are created with "available" transaction status
     */
    public ESBOutboundQueue create(ESBOutboundQueuePK pk,
            int serviceTypeId,
            int dealId,
            int systemTypeId,
            int userProfileId)
        throws RemoteException, CreateException {

        StringBuffer sqlb = new StringBuffer(
        "INSERT INTO ESBOutboundQueue (" +
        "ESBOutboundQueueId, institutionProfileId, serviceTypeId, dealId, systemTypeId, " +
        "userProfileId, attemptCounter, timeStamp, transactionStatusId" +
        ") VALUES (" );

        sqlb.append(sqlStringFrom(pk.getId()) + ", ");
        sqlb.append(sqlStringFrom(srk.getExpressState().getDealInstitutionId()) + ", ");
        sqlb.append(sqlStringFrom(serviceTypeId) + ", ");
        sqlb.append(sqlStringFrom(dealId) + ", ");
        sqlb.append(sqlStringFrom(systemTypeId) + ", ");
        sqlb.append(sqlStringFrom(userProfileId) + ", ");
        sqlb.append(sqlStringFrom(0) + ", ");
        sqlb.append(" sysdate " + ", ");
        sqlb.append(sqlStringFrom(ServiceConst.ESBOUT_TRANSACTIONSTATUS_AVALIABLE) + ")" );

        try {
            jExec.execute(sqlb.toString());
            findByPrimaryKey(pk);
            trackEntityCreate();
        } catch(Exception e) {
            CreateException ce = new CreateException("StatusEventQueue create(): exception: " + e);
            logger.error(ce.getMessage());
            logger.error("creater sql: " + sqlb.toString());
            logger.error(e);
            throw ce;
        }
        srk.setModified(true);

        return this;
    }

    /**
     * check  if unprocessed data with same dealid and sameServiceTYpeId exists.
     * @param serviceTypeId
     * @param dealId
     * @return if unprocessed data with same dealid and sameServiceTYpeId exists
     * @throws Exception
     * 
     * added for performance enhancement
     */
    public boolean existUnprocessed(int dealId, int serviceTypeId)
        throws Exception {
    	
    	String sql = "SELECT * FROM ESBOUTBOUNDQUEUE WHERE DEALID = " + dealId 
            + " AND SERVICETYPEID = " + serviceTypeId  
            + " AND TRANSACTIONSTATUSID = " + ServiceConst.ESBOUT_TRANSACTIONSTATUS_AVALIABLE;
    	
        boolean gotRecord = false;

        int key = jExec.execute(sql);
        for (; jExec.next(key);) {
            gotRecord = true;
            break;
        }
        jExec.closeData(key);

        return  gotRecord;
    }

    
    /**
     * Update any database field that has changed since the last synchroniztion
     * @return the number of updates performed
     */
    public int performUpdate () throws Exception {
        Class thisClass = this.getClass();
        int ret = doPerformUpdate(this, thisClass);

        return ret;
    }

    private void setPropertiesFromQueryResult(int key) throws Exception {
        setESBOutboundQueueId(jExec.getInt(key, "esbOutboundQueueId"));
        setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
        setServiceTypeId(jExec.getInt(key, "serviceTypeId"));
        setDealId(jExec.getInt(key, "dealId"));
        setSystemTypeId(jExec.getInt(key, "systemTypeId"));
        setUserProfileId(jExec.getInt(key, "userProfileId"));
        setAttemptCounter(jExec.getInt(key, "attemptCounter"));
        setRetryTimeStamp(jExec.getDate(key, "retryTimeStamp"));
        setTransactionStatusId(jExec.getInt(key, "transactionStatusId"));
        setTransactionStatusDescription(jExec.getString(key, "transactionStatusDescription"));
        setTimeStamp(jExec.getDate(key, "timeStamp"));
        setServiceSubTypeid(jExec.getInt(key, "serviceSubTypeid"));
//        setESBQueueStatusCategoryId(jExec.getInt(key, "esbQueueStatusCategoryId"));
    }

    /**
     * @return the StatusEventQueueId
     */
    public int getESBOutboundQueueId() {
        return esbOutboundQueueId;
    }

    /**
     *
     * @param statusEventQueueId The status event queue id to set
     */
    public void setESBOutboundQueueId(int esbOutboundQueueId) {
        this.testChange("esbOutboundQueueId", esbOutboundQueueId);
        this.esbOutboundQueueId = esbOutboundQueueId;
    }

    /**
     * @return the InstitutionProfileId
     */
    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    /**
     *
     * @param institutionProfileId The institution profile id to set
     */
    public void setInstitutionProfileId(int institutionProfileId) {
        this.testChange("institutionProfileId", institutionProfileId);
        this.institutionProfileId = institutionProfileId;
    }

    /**
     * @return the service type id
     */
    public int getServiceTypeId() {
        return serviceTypeId;
    }

    /**
     * @param serviceTypeId the serivce type id to set
     */
    public void setServiceTypeId(int serviceTypeId) {
        this.testChange("serviceTypeId", serviceTypeId);
        this.serviceTypeId = serviceTypeId;
    }

    /**
     * @return the deal id
     */
    public int getDealId() {
        return dealId;
    }

    /**
     * @param dealId the deal id to set
     */
    public void setDealId(int dealId) {
        this.testChange("dealId", dealId);
        this.dealId = dealId;
    }

    /**
     * @return the system type id
     */
    public int getSystemTypeId() {
        return systemTypeId;
    }

    /**
     * @param systemTypeId the system type  id to set
     */
    public void setSystemTypeId(int systemTypeId) {
        this.testChange("systemTypeId", systemTypeId);
        this.systemTypeId = systemTypeId;
    }

    /**
     * @return the user profile id
     */
    public int getUserProfileId() {
        return userProfileId;
    }

    /**
     * @param userProfileId the user profile id to set
     */
    public void setUserProfileId(int userProfileId) {
        this.testChange("userProfileId", userProfileId);
        this.userProfileId = userProfileId;
    }

    /**
     * @return the attempt counter
     */
    public int getAttemptCounter() {
        return attemptCounter;
    }

    /**
     * @param attemptCounter the attempt counter  to set
     */
    public void setAttemptCounter(int attemptCounter) {
        this.testChange("attemptCounter", attemptCounter);
        this.attemptCounter = attemptCounter;
    }

    /**
     * @return the retry time stamp
     */
    public Date getRetryTimeStamp() {
        return retryTimeStamp;
    }

    /**
     * @param retryTimeStamp the retry time stamp  to set
     */
    public void setRetryTimeStamp(Date retryTimeStamp) {
        this.testChange("retryTimeStamp", retryTimeStamp);
        this.retryTimeStamp = retryTimeStamp;
    }

    public void setRetryTimeStampDBSystemDate() {
        this.setDataBaseSystemDate("retryTimeStamp");
    }
    
    /**
     * @return the transaction status id
     */
    public int getTransactionStatusId() {
        return transactionStatusId;
    }

    /**
     * @param transactionStatusId the transaction status id to set
     */
    public void setTransactionStatusId(int transactionStatusId) {
        this.testChange("transactionStatusId", transactionStatusId);
        this.transactionStatusId = transactionStatusId;
    }

    /**
     * @return the transaction status description
     */
    public String getTransactionStatusDescription() {
        return transactionStatusDescription;
    }

    /**
     * @param transactionStatusDescription the transaction status description to set
     */
    public void setTransactionStatusDescription(String transactionStatusDescription) {
        this.testChange("transactionStatusDescription", transactionStatusDescription);
        this.transactionStatusDescription = transactionStatusDescription;
    }

    /**
     * @return the time stamp
     */
    public Date getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp the time stamp to set
     */
    public void setTimeStamp(Date timeStamp) {
        this.testChange("timeStamp", timeStamp);
        this.timeStamp = timeStamp;
    }

    /**
     * @return the service sub type
     */
    public int getServiceSubTypeId() {
        return serviceSubTypeid;
    }

    /**
     * @param subType the Service Sub Type to set
     */
    public void setServiceSubTypeid(int subType) {
        this.testChange("serviceSubTypeid", subType);
        this.serviceSubTypeid = subType;
    }

    /**
     * @return the status category
     */
/*    public int getESBQueueStatusCategoryId() {
        return esbQueueStatusCategoryId;
    }*/

    /**
     * @param category the esbQueueStatusCategoryId to set
     */
/*    public void setESBQueueStatusCategoryId(int category) {
        this.testChange("esbQueueStatusCategoryId", category);
        this.esbQueueStatusCategoryId = category;
    }*/

    /**
     * Return the Primary Key of this entity
     */
    public IEntityBeanPK getPk() {
        if(pk == null)
        {
              pk= new ESBOutboundQueuePK(this.esbOutboundQueueId);
        }

        return pk;
    }

    /**
     * Get Entity's table's name.
     * @return Table name.
     */
    public String getEntityTableName() {
        return "ESBOUTBOUNDQUEUE";
    }

    

    private final static int SECONDS_IN_A_DAY = 24*60*60;
    private final static String NEXT_UNPROCESSED_SQL = "SELECT * FROM (SELECT * FROM ESBOUTBOUNDQUEUE WHERE TRANSACTIONSTATUSID = "
        + ServiceConst.ESBOUT_TRANSACTIONSTATUS_AVALIABLE
        + " AND ATTEMPTCOUNTER <= ? " 
        + " AND ( RETRYTIMESTAMP IS NULL OR ((SYSDATE - RETRYTIMESTAMP) > ( ? / " + SECONDS_IN_A_DAY + ")) ) " 
        + " ORDER BY ESBOUTBOUNDQUEUEID) WHERE ROWNUM <= 1";

    public ESBOutboundQueue findNextUnprocessedEvent(int maxRetry, int retryIntervalSec) throws FinderException {

        try {
            PreparedStatement pstat = jExec.getPreparedStatement(NEXT_UNPROCESSED_SQL);

            pstat.setInt(1, maxRetry);
            pstat.setInt(2, retryIntervalSec);

            int key = jExec.executePreparedStatement(pstat, NEXT_UNPROCESSED_SQL);

            if (jExec.next(key)) {
                setPropertiesFromQueryResult(key);
            }
            jExec.closeData(key);
            
        } catch (Exception e) {
            FinderException fe = new FinderException("Exception caught while getting NextUnprocessedResponse");
            logger.error(fe.getMessage());
            logger.error("finder sql: " + NEXT_UNPROCESSED_SQL);
            logger.error(e);
            throw fe;
        }
        return this;
    }

    public static void placeOutboundRequest(SessionResourceKit srk, Deal deal,
        int serviceTypeid) throws RemoteException, CreateException {

        ESBOutboundQueue queue = new ESBOutboundQueue(srk);
        queue.create(queue.createPrimaryKey(), serviceTypeid,
                deal.getDealId(), deal.getSystemTypeId(), deal.getInstitutionProfileId());
        queue.ejbStore();

    }

}
