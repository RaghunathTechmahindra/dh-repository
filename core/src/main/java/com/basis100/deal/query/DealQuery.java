package com.basis100.deal.query;

import java.util.Vector;

import com.basis100.deal.util.DBA;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;

/**
*
*  Query methods for finding deals by various relationships - e.g. find deal by borrower name,
*  address, etc.
*
*  Most methods return collections (vectors) of DealQueryResult objects.
*
**/


public class DealQuery
{
    private SessionResourceKit srk;
    private SysLogger logger;
    private JdbcExecutor jExec;

    private static final int INITIAL = 0;
    private static final int JOIN    = 1;
    private static final int FILL    = 2;

    private String maxRecNum = "100";
    


    public DealQuery(SessionResourceKit srk)
    {
        this.srk = srk;
        logger = srk.getSysLogger();
        jExec = srk.getJdbcExecutor();
        maxRecNum = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),
                                                              Xc.Max_Search_Records_LIMIT, "100" );

    }

   //FXP20294
   private static final String QUERY_="SELECT d.dealid, d.copyid, d.copytype, d.scenariorecommended, d.statusid, d.sourceapplicationid, d.denialreasonid, d.servicingmortgagenumber, d.institutionprofileid, b.borrowerid, b.borrowerfirstname, b.borrowermiddleinitial, b.borrowerlastname, b.borrowerhomephonenumber, b.borrowerworkphonenumber, b.borrowerworkphoneextension, b.primaryborrowerflag, b.suffixid, a.addressline1, p.propertystreetnumber, p.propertystreetname, p.propertycity, s.stdescription, sb.sobpshortname, cs.contactFirstName as cscontactFirstName, cs.contactLastName as cscontactLastName, cu.contactfirstname as cucontactfirstname, cu.contactmiddleinitial as cucontactmiddleinitial, cu.contactlastname as cucontactlastname, ca.contactfirstname as cacontactfirstname, ca.contactmiddleinitial as cacontactmiddleinitial, ca.contactlastname as cacontactlastname, cf.contactfirstname as cfcontactfirstname, cf.contactmiddleinitial as cfcontactmiddleinitial, cf.contactlastname as cfcontactlastname FROM deal d "+
    "INNER JOIN borrower b on (d.dealid=b.dealid and d.copyid=b.copyid and d.institutionprofileid=b.institutionprofileid) INNER JOIN borroweraddress ba on (b.institutionprofileid=ba.institutionprofileid and b.borrowerid=ba.borrowerid and b.copyid=ba.copyid) INNER JOIN addr a on (ba.institutionprofileid=a.institutionprofileid and ba.addrid=a.addrid and ba.copyid=a.copyid) LEFT JOIN property p on (d.dealid=p.dealid and d.copyid=p.copyid and d.institutionprofileid=p.institutionprofileid) LEFT JOIN streettype s ON (p.streettypeid=s.streettypeid) INNER JOIN sourceofbusinessprofile sb ON (d.institutionprofileid=sb.institutionprofileid AND (d.sourceofbusinessprofileid=sb.sourceofbusinessprofileid or d.sourceOfBusinessProfile2ndary = sb.sourceofbusinessprofileid)) INNER JOIN contact cs ON (sb.institutionprofileid=cs.institutionprofileid AND sb.contactid=cs.contactid) INNER JOIN userprofile uu ON (d.institutionprofileid=uu.institutionprofileid AND d.underwriteruserid=uu.userprofileid) LEFT JOIN userprofile ua ON (d.institutionprofileid=ua.institutionprofileid AND d.administratorid=ua.userprofileid) LEFT JOIN userprofile uf ON (d.institutionprofileid=uf.institutionprofileid AND d.funderprofileid=uf.userprofileid) INNER JOIN contact cu ON (uu.institutionprofileid=cu.institutionprofileid AND uu.contactid=cu.contactid) LEFT JOIN contact ca ON (ua.institutionprofileid=ca.institutionprofileid AND ua.contactid=ca.contactid) LEFT JOIN contact cf ON (uf.institutionprofileid=cf.institutionprofileid AND uf.contactid=cf.contactid) WHERE ROWNUM<=100 ";
    
   private static final String _QUERY="ORDER BY d.dealid, d.copyid";

   public Vector<DealQueryResult> query(boolean searchFlags[], boolean isPrimOnly, String bLastName, String bFirstName,  String bMailAddr, String bHomePhone, String bWorkPhone, String bStreetNumber, String pStreetName, String pCity, String sourceName, String uwName, String adminName, String funderName, String appId, String sourceAppId, String servicingMortgageNum) throws Exception {
      StringBuffer query = new StringBuffer(QUERY_);
      
      //FXP23355
      //query.append("AND d.copyType='G' ");
      query.append("AND d.scenariorecommended='Y' AND d.copytype<>'T' AND ba.borroweraddresstypeId=0 ");

      if(isPrimOnly == true)
         query.append("AND b.PRIMARYBORROWERFLAG='Y' ");     
      if (bLastName != null && bLastName.length() > 0)
         query.append("AND UPPER(b.borrowerLastName) LIKE UPPER(" + wildcardIfAbsent(bLastName) + ") ");
      if (bFirstName != null && bFirstName.length() > 0)
         query.append("AND UPPER(b.borrowerFirstName) LIKE UPPER(" + wildcardIfAbsent(bFirstName) + ") ");
      if (bHomePhone != null && bHomePhone.length() > 0)
         query.append("AND UPPER(b.borrowerHomePhoneNumber) LIKE UPPER(" + wildcardIfAbsent(bHomePhone) + ") ");
      if (bWorkPhone != null && bWorkPhone.length() > 0)
         query.append("AND UPPER(b.borrowerWorkPhoneNumber) LIKE UPPER(" + wildcardIfAbsent(bWorkPhone) + ") ");
      if (bMailAddr != null && bMailAddr.length() > 0)
         query.append("AND UPPER(a.addressLine1) LIKE UPPER(" + wildcardIfAbsent(bMailAddr) + ") ");

      if (bStreetNumber != null && bStreetNumber.length() > 0)
         query.append("AND UPPER(p.propertyStreetNumber) LIKE UPPER(" + wildcardIfAbsent(bStreetNumber) + ") ");
      if (pStreetName != null && pStreetName.length() > 0)
         query.append("AND UPPER(p.propertyStreetName) LIKE UPPER(" + wildcardIfAbsent(pStreetName) + ") ");
      if (pCity != null && pCity.length() > 0)
         query.append("AND UPPER(p.propertyCity) LIKE UPPER(" + wildcardIfAbsent(pCity) + ") ");

      if (sourceName != null && sourceName.length() > 0)
         query.append("AND UPPER(sb.SOBPShortName) LIKE UPPER(" + wildcardIfAbsent(sourceName) + ") ");

      if(appId != null && appId.trim().length() > 0)
         query.append("AND UPPER(d.dealid) LIKE UPPER(" + wildcardIfAbsent(appId) + ") ");
      if(sourceAppId != null && sourceAppId.trim().length() > 0)
         query.append("AND UPPER(d.sourceapplicationid) LIKE UPPER(" + wildcardIfAbsent(sourceAppId) + ") ");
      if(servicingMortgageNum != null && servicingMortgageNum.trim().length() > 0)
         query.append("AND UPPER(d.servicingmortgagenumber) LIKE UPPER(" + wildcardIfAbsent(servicingMortgageNum) + ") ");

      if(uwName != null && uwName.trim().length() > 0)
         query.append("AND UPPER(cu.contactLastName) LIKE UPPER(" + wildcardIfAbsent(uwName) + ") ");
      if(adminName != null && adminName.trim().length() > 0)
          query.append("AND UPPER(ca.contactLastName) LIKE UPPER(" + wildcardIfAbsent(adminName) + ") ");
      if(funderName != null && funderName.trim().length() > 0)
          query.append("AND UPPER(cf.contactLastName) LIKE UPPER(" + wildcardIfAbsent(funderName) + ") ");

      query.append(_QUERY);

      int key = jExec.execute(query.toString());
      Vector<DealQueryResult> result = new Vector<DealQueryResult>();
      for (; jExec.next(key); ) {
         DealQueryResult dqr = new DealQueryResult();
         dqr.dealId = jExec.getInt(key, "dealId");
         dqr.copyId = jExec.getInt(key, "copyId");
         dqr.copyType = jExec.getString(key, "copyType");
         if (dqr.copyType == null) dqr.copyType = "?";
         if ("Y".equals(jExec.getString(key, "scenariorecommended"))) dqr.setRecommendedScenario(true);
         dqr.statusId = jExec.getInt(key, "statusId");
         dqr.sourceApplicationId = jExec.getString(key, "sourceapplicationid");
         dqr.denialReasonId = jExec.getInt(key, "denialreasonid");
         dqr.servicingMortgageNum = jExec.getString(key, "servicingmortgagenumber");
         dqr.institutionProfileId = jExec.getInt(key, "institutionProfileId");

         dqr.borrowerId = jExec.getInt(key, "borrowerId");
         dqr.borrowerFirstName = jExec.getString(key, "borrowerFirstName");
         dqr.borrowerMiddleInitial = jExec.getString(key, "borrowerMiddleInitial");
         dqr.borrowerLastName = jExec.getString(key, "borrowerLastName");
         dqr.borrowerHomePhoneNumber = jExec.getString(key, "borrowerHomePhoneNumber");
         dqr.borrowerWorkPhoneNumber = jExec.getString(key, "borrowerWorkPhoneNumber");
         dqr.borrowerWorkPhoneExtension = jExec.getString(key, "borrowerWorkPhoneExtension");
         dqr.primaryBorrowerFlag = jExec.getString(key, "primaryBorrowerFlag");
         dqr.addressLine1 = jExec.getString(key, "addressLine1");
         dqr.suffixId = jExec.getInt(key, "suffixId");

         dqr.propertyStreetNumber = jExec.getString(key, "propertyStreetNumber");
         dqr.propertyStreetName = jExec.getString(key, "propertyStreetName");
         dqr.streetTypeDesc = jExec.getString(key, "stdescription");
         dqr.properyAddress = formPropertyAddress(dqr.propertyStreetNumber, dqr.propertyStreetName, dqr.streetTypeDesc);

         dqr.source_contactFirstName = jExec.getString(key, "cscontactFirstName");
         dqr.source_contactLastName = jExec.getString(key, "cscontactLastName");

         dqr.uw_contactFirstName = jExec.getString(key, "cucontactFirstName");
         dqr.uw_contactMiddleInitial = jExec.getString(key, "cucontactMiddleInitial");
         dqr.uw_contactLastName = jExec.getString(key, "cucontactLastName");
         dqr.uw_fullname = formFullName(dqr.uw_contactFirstName, dqr.uw_contactMiddleInitial, dqr.uw_contactLastName);

         dqr.ad_contactFirstName = jExec.getString(key, "cacontactFirstName");
         dqr.ad_contactMiddleInitial = jExec.getString(key, "cacontactMiddleInitial");
         dqr.ad_contactLastName = jExec.getString(key, "cacontactLastName");
         dqr.ad_fullname = formFullName(dqr.ad_contactFirstName, dqr.ad_contactMiddleInitial, dqr.ad_contactLastName);

         dqr.fn_contactFirstName = jExec.getString(key, "cfcontactFirstName");
         dqr.fn_contactMiddleInitial = jExec.getString(key, "cfcontactMiddleInitial");
         dqr.fn_contactLastName = jExec.getString(key, "cfcontactLastName");
         dqr.fn_fullname = formFullName(dqr.fn_contactFirstName, dqr.fn_contactMiddleInitial, dqr.fn_contactLastName);

         result.add(dqr);
      }        
      return result;
   }
    /**
    *
    * flags[0] = have borrower search criteria
    * flags[1] = have property search criteria
    * flags[2] = have source search criteria
    * flags[3] = have underwriter search criteria
    * flags[4] = have deal (application number)  search criteria
    * flags[5] = have Administrator search criteria
    * flags[6] = have Funder search criteria
    *
    * Modifications : Support "All Borrowers" Search -- BY BILLY 01March2002
    *
    **/
    //=========================================================
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    /*public Vector query(boolean searchFlags[],
      boolean isPrimOnly, String bLastName, String bFirstName,  String bMailAddr, String bHomePhone, String bWorkPhone,
      String bStreetNumber, String pStreetName, String pCity,
      String sourceName,
      String uwName,
      String adminName,
      String funderName,
      String appId,
      String sourceAppId,
      String servicingMortgageNum) throws Exception
    {
        Vector results = new Vector();

        int queryType = INITIAL;

        // --- //

        if (searchFlags[0] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = queryBorrowerDetails(results, queryType, isPrimOnly, bLastName, bFirstName, bMailAddr, bHomePhone, bWorkPhone);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

        if (results.size() == 0)
          return null;   // there are no matches
        }

        // --- //

        if (searchFlags[1] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = queryPropertyDetails(results, queryType, bStreetNumber, pStreetName, pCity);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

        if (results.size() == 0)
          return null;   // there are no matches
        }

        // --- //

        if (searchFlags[4] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          //=========================================================
          //--> Ticket#129 -- Support search by Servicing Mortgage #
          //--> By Billy 26Nov2003
          results = queryDealDetails(results, queryType, appId, sourceAppId, servicingMortgageNum);
          //========================================================

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

        if (results.size() == 0)
          return null;   // there are no matches
        }

        // --- //

        if (searchFlags[2] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = querySourceDetails(results, queryType, sourceName);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

        if (results.size() == 0)
          return null;   // there are no matches
        }


        // --- //

        if (searchFlags[3] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = queryUWDetails(results, queryType, uwName);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

        if (results.size() == 0)
          return null;   // there are no matches

        }

        if (searchFlags[5] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = queryAdminDetails(results, queryType, adminName);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

          if (results.size() == 0)
          return null;   // there are no matches
        }

        if (searchFlags[6] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = queryFunderDetails(results, queryType, funderName);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

          if (results.size() == 0)
          return null;   // there are no matches
        }

        if (results.size() == 0)
          return null;   // there are no matches

        // perform fill queries for information not included in search criteria

        String dealIds = formDealIds(results);

        // --- //

        if (searchFlags[0] == false)
        {
          // Modified to handle All Borrowers search -- By BILLY 04March2002
          queryBorrowerDetailsByDeals(results, FILL, isPrimOnly, dealIds);
        }

        if (searchFlags[1] == false)
        {
          queryPropertyDetailsByDeals(results, FILL, dealIds);
        }

        if (searchFlags[2] == false)
        {
          querySourceDetailsByDeals(results, FILL, dealIds);
        }

        if (searchFlags[3] == false)
        {
          queryUWDetailsByDeals(results, FILL, dealIds);
        }

        if (searchFlags[5] == false)
        {
          queryAdminDetailsByDeals(results, FILL, dealIds);
        }

        if (searchFlags[6] == false)
        {
          queryFunderDetailsByDeals(results, FILL, dealIds);
        }

      return results;
    }*/

    //=========================================================
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    public Vector queryDealDetails(Vector results, int queryType, String appId,
      String sourceAppId, String servicingMortgageNum) throws Exception
    {
      // Added sourceapplicationId search -- By BILLY 08Nov2001
      //// SYNCADD.
      // Added DenialReasonId in the result display -- By Billy 21Aug2002
      String sql =
      "select dealId, copyId, copyType, scenariorecommended, statusId, sourceapplicationid, denialreasonid, servicingmortgagenumber, institutionProfileId " +
      "from deal WHERE " +
      "copyType = 'G' AND ";

      // Check if search by DealId
      boolean isDealIdSearch = false;
      if(appId != null && appId.trim().length() > 0)
      {
        sql += "UPPER(DEALID) LIKE UPPER(" + wildcardIfAbsent(appId) + ")";
        isDealIdSearch = true;
      }

      // Check if search by SourceApplicationId
      if(sourceAppId != null && sourceAppId.trim().length() > 0)
      {
        if(isDealIdSearch)
          sql += " AND ";
        sql += "UPPER(sourceapplicationid) LIKE UPPER(" + wildcardIfAbsent(sourceAppId) + ")";
      }

      // Check if search by servicingmortgagenumber
      if(servicingMortgageNum != null && servicingMortgageNum.trim().length() > 0)
      {
        if(isDealIdSearch)
          sql += " AND ";
        sql += "UPPER(servicingmortgagenumber) LIKE UPPER(" + wildcardIfAbsent(servicingMortgageNum) + ")";
      }

      sql = sql + " AND ROWNUM <= " + maxRecNum;
      
      sql += " ORDER BY dealId";

      return executeDealQuery(results, queryType, sql, jExec);
    }

    //===============================================================================
    // Modified to support All Borrowers Search -- BY BILLY 01March2002
    //===============================================================================
    public Vector queryBorrowerDetails(Vector results, int queryType,
              boolean isPrimOnly, String bLastName, String bFirstName,
              String bMailAddr, String bHomePhone, String bWorkPhone) throws Exception
    {
        String sql = null;

        sql = formBorrowerSearchSelectSQL();

        sql = sql + formBorrowerSearchWhereSQL(false, isPrimOnly, bLastName, bFirstName, bMailAddr, bHomePhone, bWorkPhone);

        return executeBorrowerQuery(results, queryType, sql, jExec, isPrimOnly);
    }

    public Vector queryBorrowerDetailsByDeals(Vector results, int queryType, boolean isPrimOnly, String dealIds) throws Exception
    {
        String sql = null;

        sql = formBorrowerSearchSelectSQL();

        sql = sql + formBorrowerSearchWhereSQLByDeals(false, isPrimOnly, dealIds);

        return executeBorrowerQuery(results, queryType, sql, jExec, isPrimOnly);
    }

    public Vector queryPropertyDetails(Vector results,  int queryType, String pStreetNumber, String pStreetName,
                                             String pCity) throws Exception
    {
        String sql = null;

        sql = formPropertySearchSelectSQL();

        sql = sql + formPropertySearchWhereSQL(false, pStreetNumber, pStreetName, pCity);

        Vector golds = executePropertyQuery(results, queryType, sql, jExec);

        return golds;
    }

    public Vector queryPropertyDetailsByDeals(Vector results, int queryType, String dealIds) throws Exception
    {
        String sql = null;

        sql = formPropertySearchSelectSQL();

        sql = sql + formPropertySearchWhereSQLByDeals(false, dealIds);

        return executePropertyQuery(results, queryType, sql, jExec);
    }

    public Vector querySourceDetails(Vector results, int queryType, String sourceName) throws Exception
    {
        String sql = null;

        sql = formSourceSelectSQL();

        sql = sql + formSourceSearchWhereSQL(false, sourceName);

        return executeSourceQuery(results, queryType, sql, jExec);
    }

    public Vector querySourceDetailsByDeals(Vector results, int queryType, String dealIds) throws Exception
    {
        String sql = null;

        sql = formSourceSelectSQL();

        sql = sql + formSourceSearchWhereSQLByDealIds(false, dealIds);

        return executeSourceQuery(results, queryType, sql, jExec);
    }

    public Vector queryUWDetails(Vector results, int queryType, String uwName) throws Exception
    {
        String sql = null;

        sql = formUWSelectSQL();

        sql = sql + formUWSearchWhereSQL(false, uwName);

        return executeUWQuery(results, queryType, sql, jExec);
    }

    public Vector queryUWDetailsByDeals(Vector results, int queryType, String dealIds) throws Exception
    {
        String sql = null;

        sql = formUWSelectSQL();

        sql = sql + formUWWhereSQLByDeals(false, dealIds);

        return executeUWQuery(results, queryType, sql, jExec);
    }

  private Vector executeDealQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      logger.debug("Search SQL: " + sql);
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          // pure deal id (application number) query consists of only common fields (deal identification)
          standardResultRowHandling(results, queryType, jExec, key);
      }

      jExec.closeData(key);

      return results;
  }

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  //=========================================================
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> Modified to use Field Name instead of Index
  //--> By Billy 26Nov2003
  private Vector executeBorrowerQuery(Vector results, int queryType, String sql, JdbcExecutor jExec, boolean isPrimOnly) throws Exception
  {
      logger.debug("Search SQL: " + sql);
      int key = jExec.execute(sql);
      int curBorrowerId = 0;

      for (; jExec.next(key); )
      {
          curBorrowerId = jExec.getInt(key, "borrowerId");
          Vector matchedResults = standardResultRowHandling(results, queryType, jExec, key);

          if (matchedResults == null || matchedResults.isEmpty())
            continue;

          DealQueryResult dqr = null;
          // -- //

          boolean isCreateResult = true;

          int lim = matchedResults.size();
          for (int i = 0; i < lim; ++i)
          {
            dqr = (DealQueryResult)matchedResults.elementAt(i);
            //index = 7; // standard handling retrieve first 6 fields
            if(dqr.getBorrowerId() == 0)
            {
              dqr.setBorrowerId(jExec.getInt(key, "borrowerId"));
              dqr.setBorrowerNumber(jExec.getInt(key, "borrowerNumber"));
              dqr.setBorrowerTypeId(jExec.getInt(key, "borrowerTypeId"));
              dqr.setBorrowerFirstName(jExec.getString(key, "borrowerFirstName"));
              dqr.setBorrowerMiddleInitial(jExec.getString(key, "borrowerMiddleInitial"));
              dqr.setBorrowerLastName(jExec.getString(key, "borrowerLastName"));
              dqr.setBorrowerHomePhoneNumber(jExec.getString(key, "borrowerHomePhoneNumber"));
              dqr.setBorrowerWorkPhoneNumber(jExec.getString(key, "borrowerWorkPhoneNumber"));
              dqr.setBorrowerWorkPhoneExtension(jExec.getString(key, "borrowerWorkPhoneExtension"));
              dqr.setBorrowerFaxNumber(jExec.getString(key, "borrowerFaxNumber"));
              dqr.setBorrowerEmailAddress(jExec.getString(key, "borrowerEmailAddress"));
              dqr.setPrimaryBorrowerFlag(jExec.getString(key, "primaryBorrowerFlag"));
              dqr.setAddressLine1(jExec.getString(key, "addressLine1"));
              dqr.setCity(jExec.getString(key, "city"));
              dqr.setProvinceId(jExec.getInt(key, "provinceId"));
              dqr.setPostalFSA(jExec.getString(key, "postalFSA"));
              dqr.setPostalLDU(jExec.getString(key, "postalLDU"));
              
              dqr.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
              
              isCreateResult = false;
            }
            else if(dqr.getBorrowerId() == curBorrowerId)
            {
              // should never happened -- Just in case to prevent creating dupicated results
              isCreateResult = false;
            }
          } // for

          // Create seperated results for All borrowers
          if(isPrimOnly == false && isCreateResult == true)
          {
            // Make a copy of the last matched record
            DealQueryResult newDqr = new DealQueryResult();
            newDqr.copy(dqr);

            // Set data of the current borrower
            //index = 7; // standard handling retrieve first 6 fields
            newDqr.setBorrowerId(jExec.getInt(key, "borrowerId"));
            newDqr.setBorrowerNumber(jExec.getInt(key, "borrowerNumber"));
            newDqr.setBorrowerTypeId(jExec.getInt(key, "borrowerTypeId"));
            newDqr.setBorrowerFirstName(jExec.getString(key, "borrowerFirstName"));
            newDqr.setBorrowerMiddleInitial(jExec.getString(key, "borrowerMiddleInitial"));
            newDqr.setBorrowerLastName(jExec.getString(key, "borrowerLastName"));
            newDqr.setBorrowerHomePhoneNumber(jExec.getString(key, "borrowerHomePhoneNumber"));
            newDqr.setBorrowerWorkPhoneNumber(jExec.getString(key, "borrowerWorkPhoneNumber"));
            newDqr.setBorrowerWorkPhoneExtension(jExec.getString(key, "borrowerWorkPhoneExtension"));
            newDqr.setBorrowerFaxNumber(jExec.getString(key, "borrowerFaxNumber"));
            newDqr.setBorrowerEmailAddress(jExec.getString(key, "borrowerEmailAddress"));
            newDqr.setPrimaryBorrowerFlag(jExec.getString(key, "primaryBorrowerFlag"));
            newDqr.setAddressLine1(jExec.getString(key, "addressLine1"));
            newDqr.setCity(jExec.getString(key, "city"));
            newDqr.setProvinceId(jExec.getInt(key, "provinceId"));
            newDqr.setPostalFSA(jExec.getString(key, "postalFSA"));
            newDqr.setPostalLDU(jExec.getString(key, "postalLDU"));
            
            newDqr.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
            newDqr.setSuffixId(jExec.getInt(key, "suffixId"));
            
            // Insert it to the results in the correct position
            int pos = results.lastIndexOf(newDqr);
            results.add(pos+1, newDqr);
           }

      } // Main for loop

      jExec.closeData(key);

      return results;
  }

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  //=========================================================
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> Modified to use Field Name instead of Index
  //--> By Billy 26Nov2003
  private Vector executePropertyQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      logger.debug("Search SQL: " + sql);
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          Vector matchedResults = standardResultRowHandling(results, queryType, jExec, key);

          if (matchedResults == null || matchedResults.isEmpty())
            continue;

          //int index;
          DealQueryResult dqr = null;
          // -- //

          int lim = matchedResults.size();
          for (int i = 0; i < lim; ++i)
          {
            dqr = (DealQueryResult)matchedResults.elementAt(i);
            //index = 7; // standard handling retrieve first 6 fields

            dqr.propertyId = jExec.getInt(key, "propertyId");
            dqr.propertyStreetNumber = jExec.getString(key, "propertyStreetNumber");
            dqr.propertyStreetName = jExec.getString(key, "propertyStreetName");
            dqr.streetTypeId = jExec.getInt(key, "streetTypeId");
            dqr.streetTypeDesc = jExec.getString(key, "stdescription");
            
            dqr.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));

            dqr.properyAddress = formPropertyAddress(dqr.propertyStreetNumber, dqr.propertyStreetName, dqr.streetTypeDesc);
          }
      }

      jExec.closeData(key);

      return results;
  }

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  //=========================================================
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> Modified to use Field Name instead of Index
  //--> By Billy 26Nov2003
  private Vector executeSourceQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      logger.debug("Search SQL: " + sql);
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          Vector matchedResults = standardResultRowHandling(results, queryType, jExec, key);

          if (matchedResults == null || matchedResults.isEmpty())
            continue;

          //int index;
          DealQueryResult dqr = null;
          // -- //

          int lim = matchedResults.size();
          for (int i = 0; i < lim; ++i)
          {
            dqr = (DealQueryResult)matchedResults.elementAt(i);
            //index = 7; // standard handling retrieve first 6 fields

            dqr.sourceOfBusinessProfileId = jExec.getInt(key, "sourceOfBusinessProfileId");
            dqr.SOBPShortName = jExec.getString(key, "SOBPShortName");
            dqr.source_contactFirstName = jExec.getString(key, "contactFirstName");
            dqr.source_contactMiddleInitial = jExec.getString(key, "contactMiddleInitial");
            dqr.source_contactLastName = jExec.getString(key, "contactLastName");
            
            dqr.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));

            dqr.source_fullname = formFullName(dqr.source_contactFirstName, dqr.source_contactMiddleInitial, dqr.source_contactLastName);
          }
      }

      jExec.closeData(key);

      return results;
  }

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  //=========================================================
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> Modified to use Field Name instead of Index
  //--> By Billy 26Nov2003
  private Vector executeUWQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      logger.debug("Search SQL: " + sql);
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          Vector matchedResults = standardResultRowHandling(results, queryType, jExec, key);

          if (matchedResults == null || matchedResults.isEmpty())
            continue;

          //int index;
          DealQueryResult dqr = null;
          // -- //

          int lim = matchedResults.size();
          for (int i = 0; i < lim; ++i)
          {
            dqr = (DealQueryResult)matchedResults.elementAt(i);
            //index = 7; // standard handling retrieve first 6 fields

            dqr.underwriterUserId = jExec.getInt(key, "userProfileId");
            /// dqr.SOBPShortName = jExec.getString(key, ++index);
            dqr.uw_contactFirstName = jExec.getString(key, "contactFirstName");
            dqr.uw_contactMiddleInitial = jExec.getString(key, "contactMiddleInitial");
            dqr.uw_contactLastName = jExec.getString(key, "contactLastName");
            
            dqr.setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));

            dqr.uw_fullname = formFullName(dqr.uw_contactFirstName, dqr.uw_contactMiddleInitial, dqr.uw_contactLastName);
          }
      }


      jExec.closeData(key);

      return results;
  }

  //
  // Common sql --> select fields portions of statements to be executed
  //

  private String formBorrowerSearchSelectSQL()
  {
      // alias:
      //
      //  borrower a, borroweraddress b,  addr c,  deal d

      String sql =
      //--> Ticket#129 -- Support search by Servicing Mortgage #
      //--> By Billy 26Nov2003
      "select distinct d.dealId, d.copyId, d.copyType, d.scenariorecommended, d.statusId, d.sourceapplicationid, " +
      "d.denialreasonid, d.servicingmortgagenumber, d.institutionProfileId, " +
      //========================================================
      "a.borrowerId, a.borrowerNumber, a.borrowerTypeId, a.borrowerFirstName, " +
      "a.borrowerMiddleInitial, a.borrowerLastName, a.borrowerHomePhoneNumber, " +
      "a.borrowerWorkPhoneNumber, a.borrowerWorkPhoneExtension, a.borrowerFaxNumber, " +
      "a.borrowerEmailAddress, a.primaryBorrowerFlag, " +

      "c.addressLine1, c.city, c.provinceId, c.postalFSA, c.postalLDU " +

      "FROM borrower a, borroweraddress b, addr c, deal d ";

      return sql;
  }

  private String formPropertySearchSelectSQL()
  {
      // alias:
      //
      //  property a, streettype b, deal d

      String sql =
      //--> Ticket#129 -- Support search by Servicing Mortgage #
      //--> By Billy 26Nov2003
      "select distinct d.dealId, d.copyId, d.copyType, d.scenariorecommended, d.statusId, d.sourceapplicationid, " +
      "d.denialreasonid, d.servicingmortgagenumber, d.institutionProfileId, " +
      //=======================================================
      "a.propertyId, a.propertyStreetNumber, a.propertyStreetName, a.streetTypeId, b.stdescription " +

      "FROM property a, streettype b, deal d ";

      return sql;
  }

  private String formSourceSelectSQL()
  {
      // alias:
      //
      //  sourceofbusinessprofile a, contact b, deal d

      String sql =
      //--> Ticket#129 -- Support search by Servicing Mortgage #
      //--> By Billy 26Nov2003
      "select distinct d.dealId, d.copyId, d.copyType, d.scenariorecommended, d.statusId, d.sourceapplicationid, " +
      "d.denialreasonid, d.servicingmortgagenumber, d.institutionProfileId, " +
      //=======================================================
      "a.sourceOfBusinessProfileId, a.SOBPShortName, b.contactFirstName, " +

      "b.contactMiddleInitial, b.contactLastName " +

      "FROM sourceofbusinessprofile a, contact b,  deal d ";

      return sql;
  }

  private String formUWSelectSQL()
  {
      // alias:
      //
      //  userprofile a, contact b, deal d

      String sql =
      //--> Ticket#129 -- Support search by Servicing Mortgage #
      //--> By Billy 26Nov2003
      "select distinct d.dealId, d.copyId, d.copyType, d.scenariorecommended, d.statusId, d.sourceapplicationid, " +
      "d.denialreasonid, d.servicingmortgagenumber, d.institutionProfileId, " +
      //========================================================
      "a.userProfileId, b.contactFirstName, " +

      "b.contactMiddleInitial, b.contactLastName " +

      "FROM userProfile a, contact b,  deal d ";

      return sql;
  }

  //
  // Criteria sql --> where clauses formation
  //

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  private String formBorrowerSearchWhereSQL(boolean getRecommendedScenarios, boolean isPrimOnly,
      String bLastName, String bFirstName, String bMailAddr, String bHomePhone, String bWorkPhone)
  {
      String sql =

      " WHERE " +

      // JOINS
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.dealId = a.dealId and " +
      "d.copyId = a.copyId and " +
      "a.institutionProfileId = b.institutionprofileId and " +
      "a.borrowerId = b.borrowerId and " +
      "a.copyId = b.copyId and " +
      "b.institutionProfileId = c.institutionProfileId and " +
      "b.addrId = c.addrId and " +
      "b.copyId = c.copyId and " +
      // FIXED CRITERIA (address type current)
      "b.borrowerAddressTypeId = 0 and ";   //LL 04/27/01
      // Modified to support All Borrowers Search -- BY BILLY 01March2002
      if(isPrimOnly == true)
      {
        sql = sql + "a.PRIMARYBORROWERFLAG='Y' and ";
      }
      //=================================================================

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy


      boolean some = false;

      if (bLastName != null && bLastName.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(a.borrowerLastName) LIKE UPPER(" + wildcardIfAbsent(bLastName) + ")";
      }

      if (bFirstName != null && bFirstName.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(a.borrowerFirstName) LIKE UPPER(" + wildcardIfAbsent(bFirstName) + ")";
      }

      if (bHomePhone != null && bHomePhone.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(a.borrowerHomePhoneNumber) LIKE UPPER(" + wildcardIfAbsent(bHomePhone) + ")";
      }

      if (bWorkPhone != null && bWorkPhone.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(a.borrowerWorkPhoneNumber) LIKE UPPER(" + wildcardIfAbsent(bWorkPhone) + ")";
      }


      if (bMailAddr != null && bMailAddr.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(c.addressLine1) LIKE UPPER(" + wildcardIfAbsent(bMailAddr) + ")";
      }
      sql = sql + " AND ROWNUM <= " + maxRecNum;

      sql = sql + " ORDER BY a.borrowerLastName, a.borrowerFirstName";

      return sql;

  }

  private String formBorrowerSearchWhereSQLByDeals(boolean getRecommendedScenarios, boolean isPrimOnly, String dealIds)
  {
      String sql =
      // alias:
      //
      //  borrower a, borroweraddress b,  addr c,  deal d

      " WHERE " +

      // JOINS
      "d.institutionprofileId = a.institutionprofileId AND " +
      "d.dealId = a.dealId and " +
      "d.copyId = a.copyId and " +
      "a.institutionprofileId = b.institutionprofileId AND " +
      "a.borrowerId = b.borrowerId and " +
      "a.copyId = b.copyId and " +
      "b.institutionprofileId = c.institutionprofileId AND " +
      "b.addrId = c.addrId and " +
      "b.copyId = c.copyId and " +

      // FIXED CRITERIA (address type current)

      "b.borrowerAddressTypeId = 0 and ";

      // Modified to support All Borrowers search -- By BILLY 04March2002
      if(isPrimOnly == true)
        sql = sql + "a.PRIMARYBORROWERFLAG='Y' and ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + " (d.dealId, d.institutionProfileId) IN (" + dealIds + ")";

      return sql;
  }

  // -- //

  private String formPropertySearchWhereSQL(boolean getRecommendedScenarios, String bStreetNumber, String pStreetName, String pCity)
  {
      String sql =

      " WHERE " +

      // JOINS
      "d.institutionprofileId = a.institutionprofileId AND " +
      "a.dealid = d.dealid AND a.streettypeid = b.streettypeid AND ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy


      boolean some = false;

      if (bStreetNumber != null && bStreetNumber.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(a.propertyStreetNumber) LIKE UPPER(" + wildcardIfAbsent(bStreetNumber) + ")";
      }

      if (pStreetName != null && pStreetName.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(a.propertyStreetName) LIKE UPPER(" + wildcardIfAbsent(pStreetName) + ")";
      }

      if (pCity != null && pCity.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(a.propertyCity) LIKE UPPER(" + wildcardIfAbsent(pCity) + ")";
      }

      sql = sql + " AND ROWNUM <= " + maxRecNum; 

      sql = sql + " ORDER BY a.propertyStreetNumber, a.propertyStreetName";

      return sql;
  }


  private String formPropertySearchWhereSQLByDeals(boolean getRecommendedScenarios, String dealIds)
  {
      String sql =

      " WHERE " +

      // JOINS
      
      "a.dealid = d.dealid AND a.streettypeid = b.streettypeid AND " +
      "a.institutionProfileId = d.institutionProfileId AND ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + " (d.dealId, d.institutionProfileId) IN (" + dealIds + ")";

      return sql;
  }

  // -- //

  private String formSourceSearchWhereSQL(boolean getRecommendedScenarios, String sourcename)
  {
      String sql =

      " WHERE " +

      // JOINS

      "(a.sourceOfBusinessProfileId = d.sourceOfBusinessProfileId or a.sourceOfBusinessProfileId = d.sourceOfBusinessProfile2ndary) " +
      "AND a.contactId = b.contactId AND " + 
      "d.institutionProfileId = b.institutionProfileId and " + 
      "d.institutionProfileId = a.institutionProfileId and ";


      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + " UPPER(a.SOBPShortName) LIKE UPPER(" + wildcardIfAbsent(sourcename) + ")";

      sql = sql + " AND ROWNUM <= " + maxRecNum; 

      sql = sql + " ORDER BY a.SOBPShortName";

      return sql;
  }

  private String formSourceSearchWhereSQLByDealIds(boolean getRecommendedScenarios, String dealIds)
  {
      String sql =

      " WHERE " +

      // JOINS

      "(a.sourceOfBusinessProfileId = d.sourceOfBusinessProfileId or a.sourceOfBusinessProfileId = d.sourceOfBusinessProfile2ndary) " +
      "AND a.contactId = b.contactId AND " +
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.institutionProfileId = b.institutionProfileId and ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + " (d.dealId, d.institutionProfileId) IN (" + dealIds + ")";

      return sql;
  }

  // -- //

  private String formUWSearchWhereSQL(boolean getRecommendedScenarios, String uwname)
  {
      String sql =

      " WHERE " +

      // JOINS

      "a.userProfileId = d.underwriterUserId AND a.contactId = b.contactId AND " +
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.institutionProfileId = b.institutionProfileId and ";


      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

     ///// ALERT --- should be based on UW short name

      sql = sql + " UPPER(b.contactLastName) LIKE UPPER(" + wildcardIfAbsent(uwname) + ")";

      sql = sql + " AND ROWNUM <= " + maxRecNum;

      return sql;
  }

  private String formUWWhereSQLByDeals(boolean getRecommendedScenarios, String dealIds)
  {
      String sql =

      " WHERE " +

      // JOINS

      "a.userProfileId = d.underwriterUserId AND a.contactId = b.contactId AND " +
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.institutionProfileId = b.institutionProfileId and ";


      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + " (d.dealId, d.institutionProfileId) IN (" + dealIds + ")";

      return sql;
  }

  //
  // Query support
  //
  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  //=========================================================
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> Modified to use Field Name instead of Index
  //--> By Billy 26Nov2003
  private Vector standardResultRowHandling(Vector results, int queryType, JdbcExecutor jExec, int key) throws Exception
  {
      Vector matchedResults = new Vector();

      int dealId = jExec.getInt(key, "dealId");
      int copyId = jExec.getInt(key, "copyId");
      int institutionId = jExec.getInt(key, "institutionProfileId");
      DealQueryResult dqr = null;

      if (queryType == INITIAL)
      {
        dqr = new DealQueryResult(dealId, copyId, institutionId);

        dqr.copyType = jExec.getString(key, "copyType");
        if (dqr.copyType == null)
          dqr.copyType = "?";

        String s = jExec.getString(key, "scenariorecommended");
        if (s != null && s.equals("Y"))
          dqr.setRecommendedScenario(true);

        dqr.setStatusId(jExec.getInt(key, "statusId"));

        dqr.sourceApplicationId = jExec.getString(key, "sourceapplicationid");

        //// SYNCADD.
        // Added DenialReasonId in the result display -- By Billy 21Aug2002
        dqr.setDenialReasonId(jExec.getInt(key, "denialreasonid"));
        //=================================================================

        //--> Ticket#129 -- Support search by Servicing Mortgage #
        //--> By Billy 26Nov2003
        dqr.setServicingMortgageNum(jExec.getString(key, "servicingmortgagenumber"));
        //=================================================================
        
        results.add(dqr);

        matchedResults.add(dqr);

      }
      else
      {
        // join or fill - these are essentially identical (match an existing result entry,
        // update and set touched - for fill this is not necessary but no need to differientiate)

        matchedResults = findResultMatch(dealId, copyId, institutionId, results);

        if (matchedResults == null || matchedResults.isEmpty())
          return null;
      }

      return matchedResults;
  }

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  private Vector findResultMatch(int dealId, int copyId, int institutionId, Vector results)
  {
    Vector matchedResults = new Vector();

    DealQueryResult dqr = null;

    int lim = results.size();

    for (int i = 0; i < lim; ++i)
    {
        dqr = (DealQueryResult)results.elementAt(i);

        if (dqr.dealId == dealId && dqr.copyId == copyId && dqr.institutionProfileId == institutionId)
        {
            dqr.touched = true;  // mark touched on match (support for JOIN, na for FILL)
            matchedResults.add(dqr);
        }
    }

    return matchedResults;
  }

  private Vector eliminateUnjoinedResults(Vector results)
  {
      Vector joinedResults = new Vector();

      int lim = results.size();

      for (int i = 0; i < lim; ++i)
      {

        DealQueryResult dqr = (DealQueryResult)results.elementAt(i);
        if (dqr.touched == true)
          joinedResults.add(dqr);
      }

    return joinedResults;
  }


  private void setResultsUnjoined(Vector results)
  {
      if (results == null || results.size() == 0)
        return;

      int lim = results.size();

      for (int i = 0; i < lim; ++i)
      {
        ((DealQueryResult)results.elementAt(i)).touched = false;
      }
  }

  //
  // simple utility
  //

  private String formFullName(String f, String i, String l)
  {
      if (f == null) f = "";
      if (l == null) l = "";

      String fullName = f + ", " + l;

      if (fullName.equals(", ")) fullName = "";

      if (i != null && (!i.equals("")))
      	return fullName + " " + i + ".";

      return fullName;
  }

  private String formPropertyAddress(String stNumber, String stName, String stDesc)
  {
    if (stNumber == null || stNumber.trim().length() == 0)
      stNumber = "";
    else
      stNumber = stNumber.trim() + " ";

    if (stName == null || stName.trim().length() == 0)
      stName = "";
    else
      stName = stName.trim() + " ";

    if (stDesc == null || stDesc.trim().length() == 0)
      stDesc = "";
    else
      stDesc = stDesc.trim();

      return stNumber + stName + stDesc;
  }

  // prepare string field for sql statement:
  //
  //   . adds wildcard character to end of string if not already present
  //   . nestes the string between enclosing 's (and allow for embedded 's)

  private String wildcardIfAbsent(String s)
  {
      if (s.lastIndexOf('%') != (s.length() - 1))
         s += "%";

      return DBA.sqlStringFrom(s);
  }

  private String formDealIds(Vector v)
  {
      StringBuffer dealIds = new StringBuffer();
      int lim = v.size() - 1;

      DealQueryResult dqr = null;

      int i = 0;
      for (; i < lim; ++i)
      {
          dqr = (DealQueryResult)v.elementAt(i);

          dealIds.append("(" + dqr.dealId + ", " + dqr.institutionProfileId + "),");
      }

      dqr = (DealQueryResult)v.elementAt(i);
      dealIds.append("(" + dqr.dealId + ", " + dqr.institutionProfileId + ")");

      return dealIds.toString();
  }

  // ==========================================================================
  // New requirement to add Administrator Search -- by BILLY 06Nov2001
  // ==========================================================================
  public Vector queryAdminDetails(Vector results, int queryType, String adminName) throws Exception
  {
      String sql = null;

      sql = formAdminSelectSQL();

      sql = sql + formAdminSearchWhereSQL(false, adminName);

      return executeAdminQuery(results, queryType, sql, jExec);
  }

  public Vector queryAdminDetailsByDeals(Vector results, int queryType, String dealIds) throws Exception
  {
      String sql = null;

      sql = formAdminSelectSQL();

      sql = sql + formAdminWhereSQLByDeals(false, dealIds);

      return executeAdminQuery(results, queryType, sql, jExec);
  }

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  //=========================================================
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> Modified to use Field Name instead of Index
  //--> By Billy 26Nov2003
  private Vector executeAdminQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          Vector matchedResults = standardResultRowHandling(results, queryType, jExec, key);

          if (matchedResults == null || matchedResults.isEmpty())
            continue;

          //int index;
          DealQueryResult dqr = null;
          // -- //

          int lim = matchedResults.size();
          for (int i = 0; i < lim; ++i)
          {
            dqr = (DealQueryResult)matchedResults.elementAt(i);
            //index = 7; // standard handling retrieve first 6 fields

            dqr.adminUserId = jExec.getInt(key, "userProfileId");
            /// dqr.SOBPShortName = jExec.getString(key, ++index);
            dqr.ad_contactFirstName = jExec.getString(key, "contactFirstName");
            dqr.ad_contactMiddleInitial = jExec.getString(key, "contactMiddleInitial");
            dqr.ad_contactLastName = jExec.getString(key, "contactLastName");

            dqr.ad_fullname = formFullName(dqr.ad_contactFirstName, dqr.ad_contactMiddleInitial, dqr.ad_contactLastName);
            dqr.institutionProfileId = jExec.getInt(key, "institutionProfileId");
          }
      }

      jExec.closeData(key);

      return results;
  }

  private String formAdminSearchWhereSQL(boolean getRecommendedScenarios, String adminName)
  {
      String sql =

      " WHERE " +

      // JOINS

      "a.userProfileId = d.administratorId AND a.contactId = b.contactId AND " +
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.institutionProfileId = b.institutionProfileId and ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

     ///// ALERT --- should be based on UW short name

      sql = sql + " UPPER(b.contactLastName) LIKE UPPER(" + wildcardIfAbsent(adminName) + ")";

      return sql;
  }

  private String formAdminWhereSQLByDeals(boolean getRecommendedScenarios, String dealIds)
  {
      String sql =

      " WHERE " +

      // JOINS

      "a.userProfileId = d.administratorId AND a.contactId = b.contactId AND " +
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.institutionProfileId = b.institutionProfileId and ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + "( d.dealId, d.institutionProfileId) IN (" + dealIds + ")";

      return sql;
  }

  private String formAdminSelectSQL()
  {
      // alias:
      //
      //  userprofile a, contact b, deal d

      String sql =
      //--> Ticket#129 -- Support search by Servicing Mortgage #
      //--> By Billy 26Nov2003
      "select distinct d.dealId, d.copyId, d.copyType, d.scenariorecommended, d.statusId, d.sourceapplicationid, " +
      "d.denialreasonid, d.servicingmortgagenumber, d.institutionProfileId, " +
      //========================================================
      "a.userProfileId, b.contactFirstName, " +

      "b.contactMiddleInitial, b.contactLastName " +

      "FROM userProfile a, contact b,  deal d ";

      return sql;
  }
  // ================== End Adding Admin name search =================================

  // ==========================================================================
  // New requirement to add Funder Name Search -- by BILLY 14Jan2002
  // ==========================================================================
  public Vector queryFunderDetails(Vector results, int queryType, String funderName) throws Exception
  {
      String sql = null;

      sql = formFunderSelectSQL();

      sql = sql + formFunderSearchWhereSQL(false, funderName);

      return executeFunderQuery(results, queryType, sql, jExec);
  }

  public Vector queryFunderDetailsByDeals(Vector results, int queryType, String dealIds) throws Exception
  {
      String sql = null;

      sql = formFunderSelectSQL();

      sql = sql + formFunderWhereSQLByDeals(false, dealIds);

      return executeFunderQuery(results, queryType, sql, jExec);
  }

  //===============================================================================
  // Modified to support All Borrowers Search -- BY BILLY 01March2002
  //===============================================================================
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> Modified to use Field Name instead of Index
  //--> By Billy 26Nov2003
  private Vector executeFunderQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      int key = jExec.execute(sql);
      for (; jExec.next(key); )
      {
          Vector matchedResults = standardResultRowHandling(results, queryType, jExec, key);

          if (matchedResults == null || matchedResults.isEmpty())
            continue;

          //int index;
          DealQueryResult dqr = null;
          // -- //

          int lim = matchedResults.size();
          for (int i = 0; i < lim; ++i)
          {
            dqr = (DealQueryResult)matchedResults.elementAt(i);
            //index = 7; // standard handling retrieve first 6 fields

            dqr.funderUserId = jExec.getInt(key, "userProfileId");
            dqr.fn_contactFirstName = jExec.getString(key, "contactFirstName");
            dqr.fn_contactMiddleInitial = jExec.getString(key, "contactMiddleInitial");
            dqr.fn_contactLastName = jExec.getString(key, "contactLastName");

            dqr.fn_fullname = formFullName(dqr.fn_contactFirstName, dqr.fn_contactMiddleInitial, dqr.fn_contactLastName);
            dqr.institutionProfileId = jExec.getInt(key, "institutionProfileId");
          }
      }

      jExec.closeData(key);

      return results;
  }

  private String formFunderSearchWhereSQL(boolean getRecommendedScenarios, String funderName)
  {
      String sql =

      " WHERE " +

      // JOINS

      "a.userProfileId = d.funderProfileId AND a.contactId = b.contactId AND " +
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.institutionProfileId = b.institutionProfileId and ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + " UPPER(b.contactLastName) LIKE UPPER(" + wildcardIfAbsent(funderName) + ")";

      return sql;
  }

  private String formFunderWhereSQLByDeals(boolean getRecommendedScenarios, String dealIds)
  {
      String sql =

      " WHERE " +

      // JOINS

      "a.userProfileId = d.funderProfileId AND a.contactId = b.contactId AND " +
      "d.institutionProfileId = a.institutionProfileId and " +
      "d.institutionProfileId = b.institutionProfileId and ";

      // VARIABLE

      if (getRecommendedScenarios == true)
          sql = sql + "d.scenarioRecommended = 'Y' and ";
      else
          sql = sql + "d.copyType = 'G' and ";  // get gold copy

      sql = sql + "( d.dealId, d.institutionProfileId) IN (" + dealIds + ")";

      return sql;
  }

  private String formFunderSelectSQL()
  {
      // alias:
      //
      //  userprofile a, contact b, deal d

      String sql =
      //--> Ticket#129 -- Support search by Servicing Mortgage #
      //--> By Billy 26Nov2003
      "select distinct d.dealId, d.copyId, d.copyType, d.scenariorecommended, d.statusId, d.sourceapplicationid, " +
      "d.denialreasonid, d.servicingmortgagenumber, d.institutionProfileId, " +
      //========================================================
      "a.userProfileId, b.contactFirstName, " +

      "b.contactMiddleInitial, b.contactLastName " +

      "FROM userProfile a, contact b,  deal d ";

      return sql;
  }
  // ================== End Adding Funder name search =================================
}
