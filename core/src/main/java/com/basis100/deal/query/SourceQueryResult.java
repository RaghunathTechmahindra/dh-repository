package com.basis100.deal.query;

import java.util.*;
import java.io.Serializable;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import com.basis100.deal.pk.*;

/**
*
* Query result object for maintaining information about a deal.
*
**/

public class SourceQueryResult implements Serializable
{
    public SourceQueryResult(int sobId, int sfId)
    {
       this.sourceOfBusinessProfileId = sobId;
       this.sourceFirmProfileId       = sfId;
    }

    // -- //

    boolean touched = false;

    // from sourceOfBusinessProfile  and associated records (contact/addr)

    public int      sourceOfBusinessProfileId;

    public int      sourceProfileStatusId;
    public String   sourceProfileStatus;

    public int      source_contactId;
    public String   source_contactFirstName;
    public String   source_contactMiddleInitial;
    public String   source_contactLastName;
    public String   source_fullname; // formed
    public String   source_phoneNumber;
    public String   source_faxNumber;

    public String   sourceAddrId;
    public String   source_city;
    public String   source_province;

    // from sourceFirmProfile
    public int    sourceFirmProfileId;
    public String sourceFirmFullName;
    public int    sourceSystemTypeId;

    // from sourceOfBusinessMailbox
    public String   sourceBusinessMailboxNbr;
    public String   source_phoneNumberextension;
    public String   source_formphonenumandext; // formed
    public String   source_formfaxnum; // formed

    // additional fields for new requirements - Billy 19Nov2001
    public int      source_BusinessCategoryId;
    public String   source_BusinessCategoryDesc;
    public String   sourceSystemTypeDesc;

// for testing purposes

    public String getSource_fullname()
    {
      return this.source_fullname;
    }

    public String getSource_fullphonenumber()
    {
      return this.source_formphonenumandext;
    }


}
