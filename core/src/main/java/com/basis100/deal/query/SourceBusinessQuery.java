package com.basis100.deal.query;

import java.util.Vector;

import com.basis100.deal.util.DBA;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

/**
*
*  Query methods for finding source of businesses by various relationships -
*  e.g. find source by source name, source firm, source city, etc.
*
*  Most methods return collections (vectors) of SourceQueryResult objects.
*
**/


public class SourceBusinessQuery
{
    private SessionResourceKit srk;
    private SysLogger logger;
    private JdbcExecutor jExec;

    private static final int INITIAL = 0;
    private static final int JOIN    = 1;
    private static final int FILL    = 2;


    public SourceBusinessQuery(SessionResourceKit srk)
    {
        this.srk = srk;
        logger = srk.getSysLogger();
        jExec = srk.getJdbcExecutor();
    }


    /**
    *
    * flags[0] = have source of business search criteria
    * flags[1] = have firm search criterion
    *
    **/

    public Vector query(boolean searchFlags[],

      String sourceNameFirst, String sourceNameInitial, String sourceNameLast,
      String sourceCity,  String sourceProvince, String sourcePhone, String sourceFax,
      String sourceFirm) throws Exception
      {
        Vector results = new Vector();

        int queryType = INITIAL;

        // --- //

        if (searchFlags[0] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = querySourceOfBusiness(results, queryType, sourceNameFirst, sourceNameInitial, sourceNameLast,
                                          sourceCity, sourceProvince, sourcePhone, sourceFax);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

        if (results.size() == 0)
          return null;   // there are no matches
        }

        // --- //

        if (searchFlags[1] == true)
        {
          if (queryType == JOIN)
            setResultsUnjoined(results);

          results = querySourceFirm(results, queryType, sourceFirm);

          if (queryType == INITIAL)
            queryType = JOIN;
          else
            results = eliminateUnjoinedResults(results);

        if (results.size() == 0)
          return null;   // there are no matches
        }


        // --- //

        if (results.size() == 0)
          return null;   // there are no matches

        // perform fill queries for information not included in search criteria

        // --- //

        if (searchFlags[0] == false)
        {
            querySourceOfBusiness_Fill(results, formIds(results, 1));
        }

        if (searchFlags[1] == false)
        {
            querySourceFirm_Fill(results, formIds(results, 0));
        }

      return results;
    }

    // --- //


    private Vector querySourceOfBusiness(Vector results, int queryType, String sourceNameFirst, String sourceNameInitial, String sourceNameLast,
     String sourceCity, String sourceProvince, String sourcePhone, String sourceFax) throws Exception
    {
        String sql = null;
        sql = formSourceOfBusinessSelectSQL();
        sql = sql + formSourceOfBusinessWhereSQL(sourceNameFirst, sourceNameInitial, sourceNameLast,
                                                 sourceCity, sourceProvince, sourcePhone, sourceFax);
        return executeSourceOfBusinessQuery(results, queryType, sql, jExec);
    }

    private Vector querySourceOfBusiness_Fill(Vector results, String sfIds) throws Exception
    {
        String sql = null;
        sql = formSourceOfBusinessSelectSQL();
        sql = sql + formSourceOfBusinessWhereSQL_Fill(sfIds);

        return executeSourceOfBusinessQuery(results, FILL, sql, jExec);
    }


    private Vector querySourceFirm(Vector results, int queryType, String sourceFirm) throws Exception
    {
        String sql = null;
        sql = formSourceFirmSelectSQL();
        sql = sql + formSourceFirmWhereSQL(sourceFirm);

        return executeSourceFirmQuery(results, queryType, sql, jExec);
    }

    private Vector querySourceFirm_Fill(Vector results, String sobIds) throws Exception
    {
        String sql = null;
        sql = formSourceFirmSelectSQL();
        sql = sql + formSourceFirmWhereSQL_Fill(sobIds);

        return executeSourceFirmQuery(results, FILL, sql, jExec);
    }

    //
    // Implementation
    //

  private Vector executeSourceOfBusinessQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          SourceQueryResult sqr = standardResultRowHandling(results, queryType, jExec, key);

          if (sqr == null)
            continue;

          int index = 2; // standard handling retrieve first 2 fields

          // -- //
          sqr.sourceProfileStatusId = jExec.getInt(key, ++index);
          sqr.sourceProfileStatus = jExec.getString(key, ++index);

          // -- //
          sqr.source_contactId = jExec.getInt(key, ++index);
          sqr.source_contactFirstName = jExec.getString(key, ++index);
          sqr.source_contactMiddleInitial = jExec.getString(key, ++index);
          sqr.source_contactLastName = jExec.getString(key, ++index);
          sqr.source_phoneNumber = jExec.getString(key, ++index);
          sqr.source_faxNumber = jExec.getString(key, ++index);

          // -- //
          sqr.sourceAddrId = jExec.getString(key, ++index);
          sqr.source_city = jExec.getString(key, ++index);
          sqr.source_province = jExec.getString(key, ++index);

          // -- //
          sqr.sourceBusinessMailboxNbr = jExec.getString(key, ++index);
          sqr.source_phoneNumberextension = jExec.getString(key, ++index);

          // -- //
          sqr.source_fullname = formFullName(sqr.source_contactFirstName, sqr.source_contactMiddleInitial, sqr.source_contactLastName);
          sqr.source_formphonenumandext = formFullPhoneNum(sqr.source_phoneNumber, sqr.source_phoneNumberextension);
          sqr.source_formfaxnum = formatFaxNumber(sqr.source_faxNumber);

          // added SourceCategory Description -- Billy 19Nov2001
          sqr.source_BusinessCategoryId = jExec.getInt(key, ++index);
          sqr.source_BusinessCategoryDesc = jExec.getString(key, ++index);
      }

      jExec.closeData(key);

      return results;
  }

  private Vector executeSourceFirmQuery(Vector results, int queryType, String sql, JdbcExecutor jExec) throws Exception
  {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
          SourceQueryResult sqr = standardResultRowHandling(results, queryType, jExec, key);

          if (sqr == null)
            continue;

          int index = 2; // standard handling retrieve first 2 fields

          // -- //

          sqr.sourceFirmFullName = jExec.getString(key, ++index);
//        addition
          sqr.sourceSystemTypeId = jExec.getInt(key, ++index);
          // added SystemType Description -- Billy 19Nov2001
          sqr.sourceSystemTypeDesc = jExec.getString(key, ++index);
          // ===============================================
      }

      jExec.closeData(key);

      return results;
  }

  //
  // Common sql --> select fields portions of statements to be executed
  //
  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Modified to use SOB.SystemTypeId instead of Firm.SystemTypeId table
  //--> By Billy 11Nov2003
  private String formSourceOfBusinessSelectSQL()
  {
      String sql =
      "select  sa.sourceofbusinessprofileid, sa.sourcefirmprofileid,  " +
      "sa.profilestatusid, ps.psdescription, " +
      "co.contactid, co.contactfirstname, co.contactmiddleinitial, " +
      "co.contactlastname, co.contactphonenumber, co.contactfaxnumber, " +

      //--Release2.1--//
      //// The search is done now on ProvinceId, not province name.
      ////"ad.addrid, ad.city, pr.provincename, smb.sourcesystemmailboxnbr,co.contactphonenumberextension, " +
      "ad.addrid, ad.city, pr.provinceid, sa.sourceofbusinesscode,co.contactphonenumberextension, " +

       // added SourceCategory Description -- Billy 19Nov2001
       "sa.sourceofbusinesscategoryid, scat.sobcdescription " +

      "FROM sourceofbusinessprofile sa, " +
      "contact co, addr ad, province pr,  profilestatus ps, sourceofbusinesscategory scat";

      return sql;
  }

  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Modified to use SOB.SystemTypeId instead of Firm.SystemTypeId table
  //--> By Billy 11Nov2003
  private String formSourceFirmSelectSQL()
  {
      // added SystemType Description -- Billy 19Nov2001
      String sql =
      "select  sa.sourceofbusinessprofileid, sb.sourcefirmprofileid,  " +
      "sb.sourcefirmname, sb.systemtypeid, st.systemtypedescription " +

      "FROM sourcefirmprofile sb, sourceofbusinessprofile sa, systemtype st ";

      return sql;
  }

  //
  // Criteria sql --> where clauses formation
  //
  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Modified to use SOB.SystemTypeId instead of Firm.SystemTypeId table
  //--> By Billy 11Nov2003
  private String formSourceOfBusinessWhereSQL(String sourceNameFirst, String sourceNameInitial, String sourceNameLast,
                                              String sourceCity, String sourceProvince, String sourcePhone,
                                              String sourceFax)
  {
      String sql =

      " WHERE " +

      // JOINS
      "co.institutionProfileId = sa.institutionProfileId AND " +
      "ad.institutionProfileId = sa.institutionProfileId AND " +
      "scat.institutionProfileId = sa.institutionProfileId AND " +      
      "co.contactid = sa.contactid AND " +
      "co.addrid = ad.addrid AND " +
      "ad.provinceid = pr.provinceid AND " +
      "sa.profilestatusid = ps.profilestatusid AND " +
      // added SourceCategory Description -- Billy 19Nov2001
      "sa.sourceofbusinesscategoryid = scat.sourceofbusinesscategoryid (+) AND " ;

      // VARIABLE
      boolean some = false;

      if (sourceNameFirst != null && sourceNameFirst.length() > 0)
      {
        if (some == true)
          sql = sql + " OR";
        else
          sql = sql + " (";

        some = true;

        sql = sql + " UPPER(co.contactfirstname) LIKE UPPER(" + containWildcardIfAbsent(sourceNameFirst) + ")";
      }

      if (sourceNameInitial != null && sourceNameInitial.length() > 0)
      {
        if (some == true)
          sql = sql + " OR";
        else
          sql = sql + " (";

        some = true;

        sql = sql + " UPPER(co.contactmiddleinitial) LIKE UPPER(" + containWildcardIfAbsent(sourceNameInitial) + ")";
      }

      if (sourceNameLast != null && sourceNameLast.length() > 0)
      {
        if (some == true)
          sql = sql + " OR";
        else
          sql = sql + " (";

        some = true;

        sql = sql + " UPPER(co.contactlastname) LIKE UPPER(" + containWildcardIfAbsent(sourceNameLast) + "))";
      }

      if (sourceCity != null && sourceCity.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(ad.city) LIKE UPPER(" + wildcardIfAbsent(sourceCity) + ")";
      }

      //--> The input sourceProvince is the ID
      //--> Bug fixed by Billy 01April2003
      if (sourceProvince != null && sourceProvince.length() > 0)
      {
       //-->if(! sourceProvince.equalsIgnoreCase("other"))
       if(! sourceProvince.equals("0"))
       {
        if (some == true)
          sql = sql + " AND";

        some = true;

        //--Release2.1--//
        //// The search in JATO could be done now for provinceId, not province name.
        ////sql = sql + " UPPER(pr.provincename) LIKE UPPER(" + wildcardIfAbsent(sourceProvince) + ")";
        //-->sql = sql + " pr.provinceId LIKE UPPER(" + wildcardIfAbsent(sourceProvince) + ")";
        sql = sql + " pr.provinceId = " + sourceProvince;
       }
      }

      if (sourcePhone != null && sourcePhone.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(co.contactphonenumber) LIKE UPPER(" + wildcardIfAbsent(sourcePhone) + ")";
      }


      if (sourceFax != null && sourceFax.length() > 0)
      {
        if (some == true)
          sql = sql + " AND";

        some = true;

        sql = sql + " UPPER(co.contactfaxnumber) LIKE UPPER(" + wildcardIfAbsent(sourceFax) + ")";
      }

      sql = sql + " ORDER BY co.contactfirstname, co.contactmiddleinitial, co.contactlastname";

      return sql;
  }

  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Modified to use SOB.SystemTypeId instead of Firm.SystemTypeId table
  //--> By Billy 11Nov2003
  private String formSourceOfBusinessWhereSQL_Fill(String sfIds)
  {
      String sql =

      " WHERE " +

      // JOINS
      "co.institutionProfileId = sa.institutionProfileId AND " +
      "ad.institutionProfileId = sa.institutionProfileId AND " +
      "scat.institutionProfileId = sa.institutionProfileId AND " +      
      "co.contactid = sa.contactid AND " +
      "co.addrid = ad.addrid AND " +
      "ad.provinceid = pr.provinceid AND " +
      "sa.profilestatusid = ps.profilestatusid AND " +
      // added SourceCategory Description -- Billy 19Nov2001
      "sa.sourceofbusinesscategoryid = scat.sourceofbusinesscategoryid (+) AND " ;

      sql = sql + " sa.sourcefirmprofileid IN (" + sfIds + ")";

      return sql;
  }

  // -- //

  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Modified to use SOB.SystemTypeId instead of Firm.SystemTypeId table
  //--> By Billy 11Nov2003
  private String formSourceFirmWhereSQL(String sourceFirmName)
  {
      String sql =

      " WHERE " +

      // JOINS
//      "sb.institutionProfileId = sa.institutionProfileId AND " +
//      "sb.institutionProfileId = st.institutionProfileId AND " +
      "sb.sourcefirmprofileid = sa.sourcefirmprofileid (+) AND " +
      // added SystemType Description -- Billy 19Nov2001
      "sb.systemtypeid = st.systemtypeid (+) AND " +
      // ===============================================
      " UPPER(sb.sourcefirmname) LIKE UPPER(" + containWildcardIfAbsent(sourceFirmName) + ") "  +

      "Order by sb.sourcefirmname";

      return sql;
  }

   //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Modified to use SOB.SystemTypeId instead of Firm.SystemTypeId table
  //--> By Billy 11Nov2003
  private String formSourceFirmWhereSQL_Fill(String sobIds)
  {
      String sql =

      " WHERE " +

      // JOINS
//      "sb.institutionProfileId = sa.institutionProfileId AND " +
//      "sb.institutionProfileId = st.institutionProfileId AND " +
      "sb.sourcefirmprofileid = sa.sourcefirmprofileid (+) AND " +
      // added SystemType Description -- Billy 19Nov2001
      "sb.systemtypeid = st.systemtypeid (+) AND " +
      // ===============================================
      " sa.sourceofbusinessprofileid IN (" + sobIds + ")";

      return sql;
  }

  //
  // Query support
  //

  private SourceQueryResult standardResultRowHandling(Vector results, int queryType, JdbcExecutor jExec, int key) throws Exception
  {
      int sobId = jExec.getInt(key, 1);
      int sfId  = jExec.getInt(key, 2);

      SourceQueryResult sqr = null;

      if (queryType == INITIAL)
      {
        sqr = new SourceQueryResult(sobId, sfId);
        results.add(sqr);
      }
      else
      {
        // join or fill - these are essentially identical (match an existing result entry,
        // update and set touched - for fill this is not necessary but no need to differientiate)

        sqr = findResultMatch(sobId, sfId, results);

        if (sqr == null)
          return null;
      }

      sqr.sourceOfBusinessProfileId = sobId;
      sqr.sourceFirmProfileId       = sfId;

      return sqr;
  }


  private SourceQueryResult findResultMatch(int sobId, int sfId, Vector results)
  {
    SourceQueryResult sqr = null;

    int lim = results.size();

    for (int i = 0; i < lim; ++i)
    {
        sqr = (SourceQueryResult)results.elementAt(i);

        if (sqr.sourceOfBusinessProfileId == sobId && sqr.sourceFirmProfileId == sfId)
        {
            sqr.touched = true;  // mark touched on match (support for JOIN, na for FILL)
            return sqr;
        }
    }

    return null;
  }

  private Vector eliminateUnjoinedResults(Vector results)
  {
      Vector joinedResults = new Vector();

      int lim = results.size();

      for (int i = 0; i < lim; ++i)
      {

        SourceQueryResult sqr = (SourceQueryResult)results.elementAt(i);
        if (sqr.touched == true)
          joinedResults.add(sqr);
      }

    return joinedResults;
  }


  private void setResultsUnjoined(Vector results)
  {
      if (results == null || results.size() == 0)
        return;

      int lim = results.size();

      for (int i = 0; i < lim; ++i)
      {
        ((SourceQueryResult)results.elementAt(i)).touched = false;
      }
  }

  //
  // simple utility
  //

  private String formFullName(String f, String i, String l)
  {
      if (f == null) f = "";
      if (l == null) l = "";
      if (i == null) i = "";

      String fullName = f + " " + i + " " + l;

      if(! f.equals("") && ! i.equals("") && ! l.equals("") )
        return fullName;

      if(i.equals("") )
          fullName = f + " " + l;

      return fullName;
  }

  private String formFullPhoneNum(String phStr, String ext)
  {
    String fullPhoneNumber = "";
  	if (phStr == null || phStr.trim().length() == 0)
    {
      if(fullPhoneNumber.length() < 7)
  			return fullPhoneNumber;
    }

    if (phStr.length() == 7)
	  {
  		fullPhoneNumber = phStr.substring(0, 3) + "-" + phStr.substring(3, 7);
	  	if(ext != null && ext.trim().length() != 0 )
		  	fullPhoneNumber = fullPhoneNumber + "x" + ext;
  		return  fullPhoneNumber;
  	}

	  if (phStr.trim().length() == 10)
    {
  		fullPhoneNumber = "(" +  phStr.substring(0, 3) + ")" + phStr.substring(3, 6) + "-" + phStr.substring(6, 10);
		  if (ext != null && ext.trim().length() != 0)
			  return fullPhoneNumber + "x" + ext;
    }

    return fullPhoneNumber;
  }


  public String formatFaxNumber(String phStr)
  {
    String fullPhoneNumber = "";
  	if (phStr == null || phStr.trim().length() == 0)
    {
      if(fullPhoneNumber.length() < 7)
  			return fullPhoneNumber;
    }

    if (phStr.length() == 7)
	{
  		fullPhoneNumber = phStr.substring(0, 3) + "-" + phStr.substring(3, 7);
  		return  fullPhoneNumber;
  	}

	if (phStr.trim().length() == 10)
    {
  		fullPhoneNumber = "(" +  phStr.substring(0, 3) + ")" + phStr.substring(3, 6) + "-" + phStr.substring(6, 10);
	    return fullPhoneNumber;
    }

    return fullPhoneNumber;
  }


  private String wildcardIfAbsent(String s)
  {
      if (s.lastIndexOf('%') != (s.length() - 1))
         s += "%";

      return DBA.sqlStringFrom(s);
  }

  private String containWildcardIfAbsent(String s)
  {
      // Modified to support String Contains search -- BILLY 07Dec2001
      if (!s.startsWith("%"))
         s = "%" + s;

      if (s.lastIndexOf('%') != (s.length() - 1))
         s += "%";

      return DBA.sqlStringFrom(s);
  }

  // type - 0 : Form sob ids
  //        1 : form sf ids
  private String formIds(Vector v, int type)
  {
      StringBuffer ids = new StringBuffer();
      int lim = v.size() - 1;

     //For this release the number of entries is restriced by 999
     //to escape oralce error: "ORA-01795 maximum number of expressions in a list is 1000".
     //From user's perspective even with this restriction system builds in memory
     // up to 100 pages which is more than enough for the scrolling.
     // Product team has approved this.

      if (lim >= 1000)
        lim = 998;

      SourceQueryResult sqr = null;

      int i = 0;
      for (; i < lim; ++i)
      {

          sqr = (SourceQueryResult)v.elementAt(i);

          if (type == 0)
            ids.append("" + sqr.sourceOfBusinessProfileId + ", ");
          else
            ids.append("" + sqr.sourceFirmProfileId + ", ");
      }

      sqr = (SourceQueryResult)v.elementAt(i);

      if (type == 0)
        ids.append("" + sqr.sourceOfBusinessProfileId);
      else
        ids.append("" + sqr.sourceFirmProfileId);


      return ids.toString();
  }
}


