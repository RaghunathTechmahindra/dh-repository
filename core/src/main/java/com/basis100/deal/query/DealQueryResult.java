package com.basis100.deal.query;

import java.util.*;
import java.io.Serializable;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import com.basis100.deal.pk.*;

/**
*
* Query result object for maintaining information about a deal.
*
**/

public class DealQueryResult implements Serializable
{
    boolean touched = false;

    // from deal

    public int              dealId;
    public int              copyId;
    public String           copyType;
    public boolean          recommendedScenario;
    public int              statusId;
    public String 			    statusDesc;
    public String 			    applicationId;
    public String 			    sourceApplicationId;
    //// SYNCADD.
    //Added DenialReason in the query results
    //-- By Billy 21Aug2002
    public int              denialReasonId;
    public String 			    denialReasonDesc;
    //=======================================
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    public String           servicingMortgageNum;
    //=======================================
    public int              institutionProfileId;

    // from borrower

    public int              borrowerId;
    public int              borrowerNumber;
    public int              borrowerTypeId;
    public String           borrowerFirstName;
    public String           borrowerMiddleInitial;
    public String           borrowerLastName;
    public String           borrowerFullName;
    public String           borrowerHomePhoneNumber;
    public String           borrowerWorkPhoneNumber;
    public String           borrowerWorkPhoneExtension;
    public String           borrowerFaxNumber;
    public String           borrowerEmailAddress;
    public String           primaryBorrowerFlag;
    public int              suffixId;

    // from addr (via borroweraddress)

    public String           addressLine1;
    public String           city;
    public int              provinceId;
    public String           postalFSA;
    public String           postalLDU;

    // from property
    public int      propertyId;
    public String   propertyStreetNumber;
    public String   propertyStreetName;
    public int      streetTypeId;
    public String   streetTypeDesc; // from street type
    public String   properyAddress; // formed

    // from sourceFirmProfile
    public String sourceFirmFullName;
    public int    sourceFirmContactId;
    public int    sourceFirmProfileId;
    public int    sourceFirmProfileStatusId;
    public String SFShortName;

    // from sourceOfBusinessProfile  and associated contact
    public int      sourceOfBusinessProfileId;
    public String   SOBPShortName;
    public String   sourceSOBPstatus;

    // ... from source contact info
    public String   source_contactFirstName;
    public String   source_contactMiddleInitial;
    public String   source_contactLastName;
    public String   source_fullname; // formed
    public String   source_phoneNumber;
    public String   source_faxNumber;
    public int      source_institutionProfileId;
    
    // from source addr
    public String source_city;

    // from province
    public String source_province;

    // from userProfile (underwriter)
    public int      underwriterUserId;
    // public String   SOBPShortName; // no short name!
    // ... from source contact info
    public String   uw_contactFirstName;
    public String   uw_contactMiddleInitial;
    public String   uw_contactLastName;
    public String   uw_fullname;      // formed

    // New requirement to add Administrator Search -- by BILLY 06Nov2001
    public int      adminUserId;
    public String   ad_contactFirstName;
    public String   ad_contactMiddleInitial;
    public String   ad_contactLastName;
    public String   ad_fullname;      // formed

    // New requirement to add Funder Search -- by BILLY 15Jan2002
    public int      funderUserId;
    public String   fn_contactFirstName;
    public String   fn_contactMiddleInitial;
    public String   fn_contactLastName;
    public String   fn_fullname;      // formed
    

    public DealQueryResult(int dealId, int copyId, int institutionId)
    {
       this.dealId = dealId;
       this.copyId = copyId;
       this.institutionProfileId = institutionId;
    }
    public DealQueryResult()
    {
       this.dealId = 0;
       this.copyId = 0;
       this.institutionProfileId = 0;
    }

    // -- //

    public int getDealId()
    {
      return dealId;
    }

    public int getCopyId()
    {
      return copyId;
    }

    public boolean getRecommendedScenario()
    {
      return recommendedScenario;
    }

    public int getStatusId()
    {
      return statusId;
    }

    public String getStatusDesc()
    {
      return statusDesc;
    }

    public String getApplicationId()
    {
      return applicationId;
    }

    //// SYNCADD.
    //Added DenialReason in the query results
    //-- By Billy 21Aug2002
    public int getDenialReasonId()
    {
      return denialReasonId;
    }

    public String getDenialReasonDesc()
    {
      return denialReasonDesc;
    }
    //=========================================

    // -- //

    public int getBorrowerTypeId()
    {
      return this.borrowerTypeId;
    }

    public int getBorrowerId()
    {
      return this.borrowerId ;
    }


    public int getBorrowerNumber()
    {
      return this.borrowerNumber ;
    }


    public String getBorrowerFirstName()
    {
      return this.borrowerFirstName ;
    }

    public String getBorrowerMiddleInitial()
    {
      return this.borrowerMiddleInitial ;
    }

    public String getBorrowerLastName()
    {
      return this.borrowerLastName ;
    }

    public String getBorrowerHomePhoneNumber()
    {
      return this.borrowerHomePhoneNumber ;
    }

    public String getBorrowerWorkPhoneNumber()
    {
      return this.borrowerWorkPhoneNumber ;
    }

    public String getBorrowerWorkPhoneExtension()
    {
      return this.borrowerWorkPhoneExtension;
    }

    public String getBorrowerFaxNumber()
    {
      return this.borrowerFaxNumber ;
    }

    public String getBorrowerEmailAddress()
    {
      return this.borrowerEmailAddress ;
    }

    public String getPrimaryBorrowerFlag()
    {
      return this.primaryBorrowerFlag ;
    }
    
    public int getSuffixId()
    {
      return this.suffixId ;
    }

    public String getAddressLine1()
    {
      return this.addressLine1 ;
    }

    public String getCity()
    {
      return this.city ;
    }

    public int getProvinceId()
    {
      return this.provinceId ;
    }

    public String getPostalFSA()
    {
      return this.postalFSA ;
    }

    public String getPostalLDU()
    {
      return this.postalLDU ;
    }

    // -- //


    public void setRecommendedScenario(boolean b)
    {
      recommendedScenario = b;
    }

    public void setStatusId(int id)
    {
      statusId = id;
    }

    public void setStatusDesc(String s)
    {
      statusDesc = s;
    }

    public void getApplicationId(String s)
    {
      applicationId = s;
    }

    //// SYNCADD.
    //Added DenialReason in the query results
    //-- By Billy 21Aug2002
    public void setDenialReasonId(int id)
    {
      denialReasonId = id;
    }

    public void setDenialReasonDesc(String s)
    {
      denialReasonDesc = s;
    }
    //=======================================

    public void setBorrowerId(int id)
    {
      this.borrowerId = id;
    }

    public void setBorrowerNumber(int num)
    {
      this.borrowerNumber = num;
    }

    public void setBorrowerTypeId(int id)
    {
      this.borrowerTypeId = id;
    }

    public void setBorrowerFirstName(String name )
    {
      this.borrowerFirstName = name;
    }

    public void setBorrowerMiddleInitial(String mi )
    {
      this.borrowerMiddleInitial = mi;
    }

    public void setBorrowerLastName(String name )
    {
      this.borrowerLastName = name;
    }

    public void setBorrowerHomePhoneNumber(String number)
    {
      this.borrowerHomePhoneNumber = number;
    }

    public void setBorrowerWorkPhoneNumber(String number)
    {
      this.borrowerWorkPhoneNumber = number;
    }

    public void setBorrowerWorkPhoneExtension(String value)
    {
      this.borrowerWorkPhoneExtension = value;
    }

    public void setBorrowerFaxNumber(String number)
    {
      this.borrowerFaxNumber = number;
    }

    public void setBorrowerEmailAddress(String email)
    {
      this.borrowerEmailAddress = email;
    }

    public void setPrimaryBorrowerFlag(String flag)
	  {
      this.primaryBorrowerFlag = flag;
    }

    public void setSuffixId(int id)
	{
        this.suffixId = id;
    }
    
    public void setAddressLine1(String line1)
    {
      this.addressLine1 = line1;
    }

    public void setCity(String city)
    {
      this.city = city;
    }

    public void setProvinceId(int id )
    {
      this.provinceId = id;
    }

    public void setPostalFSA(String fsa )
    {
      this.postalFSA = fsa;
    }


    public void setPostalLDU(String ldu )
    {
      this.postalLDU = ldu;
    }

    //=========================================================
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    public String getServicingMortgageNum()
    {
      return servicingMortgageNum;
    }

    public void setServicingMortgageNum(String data)
    {
      servicingMortgageNum = data;
    }
    //=========================================================
    
    /**
     * 
     */
    public int getInstitutionProfileId() {
        return institutionProfileId;
    }
    
    /**
     * 
     * @param institutionProfileId
     */
    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }

    public void copy(DealQueryResult source)
    {
      this.touched = source.touched;
      // from deal
      this.dealId = source.dealId;
      this.copyId = source.copyId;
      this.copyType = source.copyType;
      this.recommendedScenario = source.recommendedScenario;
      this.statusId = source.statusId;
      this.statusDesc = source.statusDesc;
      //// SYNCADD.
      //Added DenialReason -- Billy 21Aug2002
      this.denialReasonId = source.denialReasonId;
      this.denialReasonDesc = source.denialReasonDesc;
      //=====================================
      this.applicationId = source.applicationId;
      this.sourceApplicationId = source.sourceApplicationId;
      // from borrower
      this.borrowerId = source.borrowerId;
      this.borrowerNumber = source.borrowerNumber;
      this.borrowerTypeId = source.borrowerTypeId;
      this.borrowerFirstName = source.borrowerFirstName;
      this.borrowerMiddleInitial = source.borrowerMiddleInitial;
      this.borrowerLastName = source.borrowerLastName;
      this.borrowerFullName = source.borrowerFullName;
      this.borrowerHomePhoneNumber = source.borrowerHomePhoneNumber;
      this.borrowerWorkPhoneNumber = source.borrowerWorkPhoneNumber;
      this.borrowerWorkPhoneExtension = source.borrowerWorkPhoneExtension;
      this.borrowerFaxNumber = source.borrowerFaxNumber;
      this.borrowerEmailAddress = source.borrowerEmailAddress;
      this.primaryBorrowerFlag = source.primaryBorrowerFlag;
      this.suffixId = source.suffixId;
      // from addr (via borroweraddress)
      this.addressLine1 = source.addressLine1;
      this.city = source.city;
      this.provinceId = source.provinceId;
      this.postalFSA = source.postalFSA;
      this.postalLDU = source.postalLDU;
      // from property
      this.propertyId = source.propertyId;
      this.propertyStreetNumber = source.propertyStreetNumber;
      this.propertyStreetName = source.propertyStreetName;
      this.streetTypeId = source.streetTypeId;
      this.streetTypeDesc = source.streetTypeDesc; // from street type
      this.properyAddress = source.properyAddress; // formed
      // from sourceFirmProfile
      this.sourceFirmFullName = source.sourceFirmFullName;
      this.sourceFirmContactId = source.sourceFirmContactId;
      this.sourceFirmProfileId = source.sourceFirmProfileId;
      this.sourceFirmProfileStatusId = source.sourceFirmProfileStatusId;
      this.SFShortName = source.SFShortName;
      // from sourceOfBusinessProfile  and associated contact
      this.sourceOfBusinessProfileId = source.sourceOfBusinessProfileId;
      this.SOBPShortName = source.SOBPShortName;
      this.sourceSOBPstatus = source.sourceSOBPstatus;
      // ... from source contact info
      this.source_contactFirstName = source.source_contactFirstName;
      this.source_contactMiddleInitial = source.source_contactMiddleInitial;
      this.source_contactLastName = source.source_contactLastName;
      this.source_fullname = source.source_fullname; // formed
      this.source_phoneNumber = source.source_phoneNumber;
      this.source_faxNumber = source.source_faxNumber;
      // from source addr
      this.source_city = source.source_city;
      // from province
      this.source_province = source.source_province;
      // from userProfile (underwriter)
      this.underwriterUserId = source.underwriterUserId;
      // public String   SOBPShortName; // no short name!
      // ... from source contact info
      this.uw_contactFirstName = source.uw_contactFirstName;
      this.uw_contactMiddleInitial = source.uw_contactMiddleInitial;
      this.uw_contactLastName = source.uw_contactLastName;
      this.uw_fullname = source.uw_fullname;      // formed
      // New requirement to add Administrator Search -- by BILLY 06Nov2001
      this.adminUserId = source.adminUserId;
      this.ad_contactFirstName = source.ad_contactFirstName;
      this.ad_contactMiddleInitial = source.ad_contactMiddleInitial;
      this. ad_contactLastName = source.ad_contactLastName;
      this.ad_fullname = source.ad_fullname;      // formed
      // New requirement to add Funder Search -- by BILLY 15Jan2002
      this.funderUserId = source.funderUserId;
      this.fn_contactFirstName = source.fn_contactFirstName;
      this.fn_contactMiddleInitial = source.fn_contactMiddleInitial;
      this.fn_contactLastName = source.fn_contactLastName;
      this.fn_fullname = source.fn_fullname;      // formed
      //===========================================================
      //--> Ticket#129 -- Support search by Servicing Mortgage #
      //--> By Billy 26Nov2003
      this.servicingMortgageNum = source.servicingMortgageNum;
      //===========================================================
      this.institutionProfileId = source.institutionProfileId;
    }

    public boolean equals(Object object)
    {
      if(this == object)
        return true;
      else if(object == null || getClass() != object.getClass() )
        return false;

     DealQueryResult other  = (DealQueryResult)object;
      boolean retval = false;

      if(this.getDealId() == other.getDealId() && this.getCopyId() == other.getCopyId() 
                      && this.getInstitutionProfileId() == other.getInstitutionProfileId()) retval = true;

      return retval;
    }
    

}
