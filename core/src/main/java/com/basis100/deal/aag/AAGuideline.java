package com.basis100.deal.aag;

import java.util.*;

import MosSystem.Mc;
import MosSystem.Sc;
import com.filogix.express.legacy.mosapp.ISessionStateModel;
import mosApp.MosSystem.PageEntry;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.util.thread.*;
import com.basis100.deal.validation.*;
import com.basis100.deal.security.*;
import com.basis100.picklist.BXResources;

/*
 * AAGuideline - Approval Authority Guidline Module
 * 
 * This module provides the following services:
 *  . Deal Pre-Qualification, Adjudication Escalation, and External Approval
 * Testing
 * 
 * This service contains the logic to quailify a underwriter for sufficient
 * approval authority when he/she approves a deal. Underwriter approval
 * authority information is stored as records in the APPROVALAUTHORITY table -
 * see dictionary for details. Additionally this services exercises the AAG -
 * PreQual Business Rules that capture additional pre-qualification criteria.
 * 
 * Adjudication escaltion involves locating a higher or joint approver to judge
 * a deal when the current underwriter does not have sufficient approval
 * authority (as per deal pre-qualification). Selection of higher/joint approver
 * is performed on a deterministic basis dictated by the relationships
 * established by underwriter user profile records. Specifically, for a given
 * underwriter:
 *  . Higher Approver is identified by contents of SECONDAPPROVERUSERID field in
 * underwriter�s user profile record.
 *  . Joint Approver is identified by contents of JOINTAPPROVERUSERID field in
 * underwriter�s user profile record.
 * 
 * It is expected that only one of SECONDAPPROVERUSERID and JOINTAPPROVERUSERID
 * would be populated for a given profile record - however, when selecting a
 * subsequent approver precedence is given to the higher approver specification.
 * 
 * The rules with respects to higher/joint approvals are as follows:
 *  . if the user approving the deal was selected as a joint approver then no
 * further escalation occurs. It is possible that joint approver does not have
 * sufficient approval authority. When this is the case the MOS interface will
 * disallow approval of the deal with the appropriate message(s). Changes to the
 * deal, or to the user approval limit will be required to permit approval in
 * this case.
 *  . if the user approving the deal was not selected as a joint approver (this
 * always includes the underwriter assigned to the deal) then:
 *  . if fewer than four (4) higher approvals have occurred:
 *  . locate 'next' active higher approver starting from higher approver
 * indicated in the current approvers' profile - next step applied on failure to
 * locate an approver
 *  . if four (4) higher approval have occured or if an 'next' active higher
 * cannot be located
 *  . locate an active joint approver. Generally the join approver is located by
 * tranversing higher links starting from the current approvers' profile until a
 * profile without a higher approver is located (i.e. end of the higher approver
 * chain) - this profile is expected to point to a joint approver. However, if a
 * joint approver link is located in any propfile record while transversing the
 * higher approver links it is used.
 *  . if an additional approver (next higher or joint) is not located after the
 * steps above then we attempt to locate a joint approver by:
 *  . selecting all active underwriters (indirectly) designated as joint
 * approvers in the in the same group as the underwriter assigned to the deal.
 * The joint selected is then:
 *  . the single approver isolated if only one was found. . the least loaded
 * approver from the qualifying group (using standard load balancing algorithm)
 *  . if this fails to locate a joint approver a similar procedure is repeated
 * for:
 *  . active joint approvers from the same branch as the underwriter assigned to
 * the deal
 * 
 * Should the above steps fail to isolate an additional approver (unlikely!) the
 * MOS interface will not allow approval of the deal by the current approver.
 * 
 * The application of the rules dictate that a given deal will never require
 * more that six (6) distinct approvals (e.g. worst case --> original uw, four
 * higher approvals and finally approval by a joint approver). Fewer approval
 * are required whenever an approver has sufficient approval authority, or when
 * a joint approver is indicated by a profile record linkage prior the 4th
 * higher approver assignment.
 * 
 * AAG - PostQual Business Rules encapsulate the critera for testing the need to
 * have an underwriter review a deal for external approval (e.g. need to forward
 * a deal to a lender's own underwriting staff for the final approval decision.
 * These rule are only exercised if approval of the deal is permitted -
 * underwriter has sufficient authority to approve the deal without escalation.
 * 
 * Methods -------
 *  . int qualifyApproval(int uwProfileId, int dealId, SessionResourceKit srk)
 * 
 * This is the entry point for services described above.
 * 
 * Result: {next deal status code with values:
 * 
 * -1 - Exception or deal cannot be approved nor esclated for approval. Caller
 * can obtain an Active Message (confirming type - no OK button) for display to
 * the user via a call to getUserDialog().
 * 
 * Mc.DEAL_APPROVED - Deal can be approved without further intervention. Caller
 * can proceed with final approval actions (e.g. generate commitment offer
 * letter, etc).
 * 
 * Mc.DEAL_APPROVAL_RECOMMENDED - Escalation of approval is required. Caller can
 * obtain an Active Message (dialog type - generates server OK event) for
 * display to the user via a call to getUserDialog(). Upon the OK event the
 * approval recommended status should be set (in the deal) and the opertion
 * completed via notifying the workflow engine; a external task completion
 * object is available via a call to getExtTaskCompletionObj() in this case - it
 * must be communicated to the engine prior to activation.
 * 
 * {any other code} - Typically Mc.DEAL_EXTERNAL_APPROVAL_REVIEW indicating a
 * need for an external approval review. In general the code return can be set
 * as the deal status then proceeding with normal handler completion actions
 * (e.g. trigger workflow engine, etc.).
 * 
 *  
 */

/*
 * public class AAGExtTaskCompletionObject
 */

public class AAGuideline {
	private SessionResourceKit srk;

	private SysLogger logger;

	private JdbcExecutor jExec;

	private ActiveMessage userDialog = null;

	private WFEExternalTaskCompletion extTaskCompletionObj = null;

	public AAGuideline(SessionResourceKit srk) {
		this.srk = srk;
		logger = srk.getSysLogger();
		jExec = srk.getJdbcExecutor();
	}

	public ActiveMessage getUserDialog() {
		return userDialog;
	}

	public WFEExternalTaskCompletion getExtTaskCompletionObj() {
		return extTaskCompletionObj;
	}

	/*
	 * qualifyApproval: See class documentation
	 *  
	 */

	////public int qualifyApproval(SessionState theSession, PageEntry pe, SessionResourceKit srk, Deal deal) throws Exception
	public int qualifyApproval(ISessionStateModel theSession, PageEntry pe,
			SessionResourceKit srk, Deal deal) throws Exception {
		boolean sufficientLOBAuthority;
		boolean aagRulesMessages;

		// defaults - assume uw sufficient authority

		// create default dialog for exception condition
		//--Release2.1--//
		//Modified to pass LanguageId for Multilingual handling
		//--> By Billy 12Nov2002
		userDialog = ActiveMsgFactory.getActiveMessage(
				ActiveMsgFactory.ISCUSTOMCONFIRM, theSession.getLanguageId());
		userDialog.setDialogMsg(BXResources.getSysMsg(
				"TASK_RELATED_UNEXPECTED_FAILURE", theSession.getLanguageId()));

		logger.trace("AAG: qualifyApproval()");

		// get id of the approver
		int uwProfileId = theSession.getSessionUserId();

		logger.trace("AAG: current approver id = " + uwProfileId);

		double totalLoanAmount = deal.getTotalLoanAmount();
		int lineOfBusinessId = deal.getLineOfBusinessId();

		logger.trace("AAG: check approval auth: amt = " + totalLoanAmount
				+ ", LOB = " + lineOfBusinessId);

		// determine if approver has sufficient approval authority
        // added instituionProfileId from sessionState
		UserProfile appProfile = getApproverProfile(uwProfileId, 
                                                    srk.getExpressState().getDealInstitutionId());
		if (appProfile == null)
			return -1;

		sufficientLOBAuthority = appProfile.hasApprovalAuthorityForLOB(
				totalLoanAmount, lineOfBusinessId);

		for (int iii = 0; iii < 5; ++iii) {
			logger.trace("AAG: type/value/limit = "
					+ appProfile.getApprovalAuthorityTypeId(iii) + "/"
					+ appProfile.getApprovalAuthorityTypeValue(iii) + "/"
					+ appProfile.getApprovalLimit(iii));
		}

		// run AAG business rules
		PassiveMessage pm = new PassiveMessage();
		BusinessRuleExecutor brExec = new BusinessRuleExecutor();

		pm = brExec.BREValidator(theSession, pe, srk, pm, Mc.AAG_RULE_PREFIX
				+ "-%");

		aagRulesMessages = pm.getNumMessages() > 0;

		// user can approve
		if (sufficientLOBAuthority == true && aagRulesMessages == false) {
			// check need for external approver review
			if (null != brExec.BREValidator(theSession, pe, srk, null,
					Mc.AGP_RULE_PREFIX + "-%")) {
				return Mc.DEAL_EXTERNAL_APPROVAL_REVIEW;
			}

			// final approval reached
			return Mc.DEAL_APPROVED;
		}

		// insufficient ....

		// if current approve is joint approver we do not attempt to escalate
		// further
		if (deal.getJointApproverId() != 0) {
			//--Release2.1--//
			//Modified to pass LanguageId for Multilingual handling
			//--> By Billy 12Nov2002
			userDialog = formMessageObject(false, sufficientLOBAuthority, pm,
					theSession.getLanguageId());
			return -1;
		}

		// find next approver

		UserProfileQuery upq = new UserProfileQuery();

		UserProfile nextAppProfile = null;

		if (deal.higherApproverAllowed()
				&& appProfile.getSecondApproverUserId() > 0) {
			logger.trace("AAG: checking for higher approver");

			// select higher approver (if possible)
			nextAppProfile = upq.get2ndApproverUserProfile(appProfile
					.getSecondApproverUserId(), deal.getInstitutionProfileId(), true, true, srk );

			if (nextAppProfile != null
					&& nextAppProfile.getProfileStatusId() == Sc.PROFILE_STATUS_ACTIVE) {
				logger.trace("AAG: higher approver found, id = "
						+ nextAppProfile.getUserProfileId());
				// have next higher approver
				extTaskCompletionObj = new AAGTaskCompletionObj(
						Mc.DEAL_HIGHER_APPROVAL_REQUESTED, nextAppProfile
								.getUserProfileId());
				//--Release2.1--//
				//Modified to pass LanguageId for Multilingual handling
				//--> By Billy 12Nov2002
				userDialog = formMessageObject(true, nextAppProfile,
						sufficientLOBAuthority, pm, theSession.getLanguageId());
				return Mc.DEAL_APPROVAL_RECOMMENDED;
			}
		}

		// exhausted higher approvals or couldn't find a 'next' active higher
		// approver - get joint approver

		logger.trace("AAG: checking for joint approver");

		// first get to end of higher approver chain
		nextAppProfile = upq.getLast2ndApproverUserProfile(uwProfileId, deal.getInstitutionProfileId(), srk);

		// get joint approver id from end node (if one)
		if (nextAppProfile != null
				&& nextAppProfile.getJointApproverUserId() > 0) {
			//  get joint approvers' profile - ensure active transversing joint
			// chain if necessary
			nextAppProfile = upq.getJointApproverUserProfile(nextAppProfile
					.getJointApproverUserId(), deal.getInstitutionProfileId(), true, true, srk);

			if (nextAppProfile != null
					&& nextAppProfile.getProfileStatusId() == Sc.PROFILE_STATUS_ACTIVE) {
				logger.trace("AAG: joint approver id = "
						+ nextAppProfile.getUserProfileId());
				// have joint approver
				extTaskCompletionObj = new AAGTaskCompletionObj(
						Mc.DEAL_JOINT_APPROVAL_REQUESTED, nextAppProfile
								.getUserProfileId());
				//--Release2.1--//
				//Modified to pass LanguageId for Multilingual handling
				//--> By Billy 12Nov2002
				userDialog = formMessageObject(true, nextAppProfile,
						sufficientLOBAuthority, pm, theSession.getLanguageId());
				return Mc.DEAL_APPROVAL_RECOMMENDED;
			}
		}

		// here if couldn't locate active higher or joint approver

		logger.trace("AAG: failed to find joint approver - fall-back method");

		//
		// fall-back processing
		//

		UserProfile dealUW = upq.getUserProfile(deal.getUnderwriterUserId(), deal.getInstitutionProfileId(),
				false, false, upq.ANY_USER_TYPE, srk);

		// find active joint approvers
		Vector v = upq.getUsers(dealUW.getGroupProfileId(),  
				UserProfile.JOINT_APPROVERS, true, srk, deal.getInstitutionProfileId());

		WorkflowAssistBean wab = new WorkflowAssistBean();

		nextAppProfile = wab.getLeastLoadedProfile(v, srk, deal.getInstitutionProfileId());

		if (nextAppProfile == null) {
			// can't escalate
			//--Release2.1--//
			//Modified to pass LanguageId for Multilingual handling
			//--> By Billy 12Nov2002
			userDialog = formMessageObject(false, sufficientLOBAuthority, pm,
					theSession.getLanguageId());
			return -1;
		}

		logger.trace("AAG: joint approver id = "
				+ nextAppProfile.getUserProfileId());
		// have (fall-back) joint approver
		extTaskCompletionObj = new AAGTaskCompletionObj(
				Mc.DEAL_JOINT_APPROVAL_REQUESTED, nextAppProfile
						.getUserProfileId());
		//--Release2.1--//
		//Modified to pass LanguageId for Multilingual handling
		//--> By Billy 12Nov2002
		userDialog = formMessageObject(true, nextAppProfile,
				sufficientLOBAuthority, pm, theSession.getLanguageId());
		return Mc.DEAL_APPROVAL_RECOMMENDED;
	}

	
	/**
	 * This is a pageless version of the qualifyApproval method.
	 * 
	 * Note:  The language will be assumbed to be english for now.
	 * @param srk
	 * @param deal
	 * @return
	 * @throws Exception
	 */
	public int pagelessQualifyApproval(SessionResourceKit srk, Deal deal) throws Exception{
		
		boolean sufficientLOBAuthority;
		boolean aagRulesMessages;

		// defaults - assume uw sufficient authority

		// create default dialog for exception condition
		//--Release2.1--//
		//Modified to pass LanguageId for Multilingual handling
		//--> By Billy 12Nov2002
		
		
		userDialog = ActiveMsgFactory.getActiveMessage(
				ActiveMsgFactory.ISCUSTOMCONFIRM, 0);
		userDialog.setDialogMsg(BXResources.getSysMsg(
				"TASK_RELATED_UNEXPECTED_FAILURE", 0));
			
		
		logger.trace("AAG: pagelessQualifyApproval()");

		// get id of the approver
		int uwProfileId = srk.getExpressState().getUserProfileId();
		logger.trace("AAG: current approver id = " + uwProfileId);

		double totalLoanAmount = deal.getTotalLoanAmount();
		int lineOfBusinessId = deal.getLineOfBusinessId();

		logger.trace("AAG: check approval auth: amt = " + totalLoanAmount
				+ ", LOB = " + lineOfBusinessId);

		// determine if approver has sufficient approval authority
		UserProfile appProfile = getApproverProfile(uwProfileId, 
                                                    srk.getExpressState().getDealInstitutionId());
		if (appProfile == null)
			return -1;

		sufficientLOBAuthority = appProfile.hasApprovalAuthorityForLOB(
				totalLoanAmount, lineOfBusinessId);

		for (int iii = 0; iii < 5; ++iii) {
			logger.trace("AAG: type/value/limit = "
					+ appProfile.getApprovalAuthorityTypeId(iii) + "/"
					+ appProfile.getApprovalAuthorityTypeValue(iii) + "/"
					+ appProfile.getApprovalLimit(iii));
		}

		// run AAG business rules
		PassiveMessage pm = new PassiveMessage();
		BusinessRuleExecutor brExec = new BusinessRuleExecutor();

		pm = brExec.BREValidator((DealPK)deal.getPk(), srk, pm, Mc.AAG_RULE_PREFIX
				+ "-%");

		aagRulesMessages = pm.getNumMessages() > 0;

		// user can approve
		if (sufficientLOBAuthority == true && aagRulesMessages == false) {
			// check need for external approver review
			if (null != brExec.BREValidator((DealPK)deal.getPk(), srk, null,
					Mc.AGP_RULE_PREFIX + "-%")) {
				return Mc.DEAL_EXTERNAL_APPROVAL_REVIEW;
			}

			// final approval reached
			return Mc.DEAL_APPROVED;
		}

		// insufficient ....

		// if current approve is joint approver we do not attempt to escalate
		// further
		if (deal.getJointApproverId() != 0) {
			//--Release2.1--//
			//Modified to pass LanguageId for Multilingual handling
			//--> By Billy 12Nov2002
			
			userDialog = formMessageObject(false, sufficientLOBAuthority, pm,
					0);			
			return -1;
		}

		// find next approver

		UserProfileQuery upq = new UserProfileQuery();

		UserProfile nextAppProfile = null;

		if (deal.higherApproverAllowed()
				&& appProfile.getSecondApproverUserId() > 0) {
			logger.trace("AAG: checking for higher approver");

			// select higher approver (if possible)
			nextAppProfile = upq.get2ndApproverUserProfile(appProfile
					.getSecondApproverUserId(), deal.getInstitutionProfileId(), true, true, srk);

			if (nextAppProfile != null
					&& nextAppProfile.getProfileStatusId() == Sc.PROFILE_STATUS_ACTIVE) {
				logger.trace("AAG: higher approver found, id = "
						+ nextAppProfile.getUserProfileId());
				// have next higher approver
				extTaskCompletionObj = new AAGTaskCompletionObj(
						Mc.DEAL_HIGHER_APPROVAL_REQUESTED, nextAppProfile
								.getUserProfileId());
				//--Release2.1--//
				//Modified to pass LanguageId for Multilingual handling
				//--> By Billy 12Nov2002
				
				userDialog = formMessageObject(true, nextAppProfile,
						sufficientLOBAuthority, pm, 0);				
				return Mc.DEAL_APPROVAL_RECOMMENDED;
			}
		}

		// exhausted higher approvals or couldn't find a 'next' active higher
		// approver - get joint approver

		logger.trace("AAG: checking for joint approver");

		// first get to end of higher approver chain
		nextAppProfile = upq.getLast2ndApproverUserProfile(uwProfileId, deal.getInstitutionProfileId(), srk);

		// get joint approver id from end node (if one)
		if (nextAppProfile != null
				&& nextAppProfile.getJointApproverUserId() > 0) {
			//  get joint approvers' profile - ensure active transversing joint
			// chain if necessary
			nextAppProfile = upq.getJointApproverUserProfile(nextAppProfile
					.getJointApproverUserId(), deal.getInstitutionProfileId(), true, true, srk);

			if (nextAppProfile != null
					&& nextAppProfile.getProfileStatusId() == Sc.PROFILE_STATUS_ACTIVE) {
				logger.trace("AAG: joint approver id = "
						+ nextAppProfile.getUserProfileId());
				// have joint approver
				extTaskCompletionObj = new AAGTaskCompletionObj(
						Mc.DEAL_JOINT_APPROVAL_REQUESTED, nextAppProfile
								.getUserProfileId());
				//--Release2.1--//
				//Modified to pass LanguageId for Multilingual handling
				//--> By Billy 12Nov2002
				
				userDialog = formMessageObject(true, nextAppProfile,
						sufficientLOBAuthority, pm, 0);				
				return Mc.DEAL_APPROVAL_RECOMMENDED;
			}
		}

		// here if couldn't locate active higher or joint approver

		logger.trace("AAG: failed to find joint approver - fall-back method");

		//
		// fall-back processing
		//

		UserProfile dealUW = upq.getUserProfile(deal.getUnderwriterUserId(), deal.getInstitutionProfileId(),
				false, false, UserProfileQuery.ANY_USER_TYPE, srk);

		// find active joint approvers
		Vector v = upq.getUsers(dealUW.getGroupProfileId(), 
				UserProfile.JOINT_APPROVERS, true, srk, deal.getInstitutionProfileId());

		WorkflowAssistBean wab = new WorkflowAssistBean();

		nextAppProfile = wab.getLeastLoadedProfile(v, srk, deal.getInstitutionProfileId());

		if (nextAppProfile == null) {
			// can't escalate
			//--Release2.1--//
			//Modified to pass LanguageId for Multilingual handling
			//--> By Billy 12Nov2002
			
			userDialog = formMessageObject(false, sufficientLOBAuthority, pm,
					0);
			return -1;
		}

		logger.trace("AAG: joint approver id = "
				+ nextAppProfile.getUserProfileId());
		// have (fall-back) joint approver
		extTaskCompletionObj = new AAGTaskCompletionObj(
				Mc.DEAL_JOINT_APPROVAL_REQUESTED, nextAppProfile
						.getUserProfileId());
		//--Release2.1--//
		//Modified to pass LanguageId for Multilingual handling
		//--> By Billy 12Nov2002
		
		userDialog = formMessageObject(true, nextAppProfile,
				sufficientLOBAuthority, pm, 0);
		return Mc.DEAL_APPROVAL_RECOMMENDED;
	}
	
	//--Release2.1--//
	//Modified to pass LanguageId for Multilingual handling
	//--> By Billy 12Nov2002
	private ActiveMessage formMessageObject(boolean escalation,
			boolean sufficientLOBAuthority, PassiveMessage pm, int languageId) {
		return formMessageObject(escalation, null, sufficientLOBAuthority, pm,
				languageId);
	}

	private ActiveMessage formMessageObject(boolean escalation,
			UserProfile nextAppProfile, boolean sufficientLOBAuthority,
			PassiveMessage pm, int languageId) {
		ActiveMessage am = null;

		if (escalation) {
			//--Release2.1--//
			//Modified to pass LanguageId for Multilingual handling
			//--> By Billy 12Nov2002
			am = ActiveMsgFactory.getActiveMessage(
					ActiveMsgFactory.ISCUSTOMDIALOG, languageId);
			am.setDialogMsg(formEscalationDialogMessage(nextAppProfile,
					languageId));
		} else {
			//--Release2.1--//
			//Modified to pass LanguageId for Multilingual handling
			//--> By Billy 12Nov2002
			am = ActiveMsgFactory.getActiveMessage(
					ActiveMsgFactory.ISCUSTOMCONFIRM, languageId);
			am.setDialogMsg(BXResources.getSysMsg("DEAL_APPROVE_CANT_ESCALATE",
					languageId));
		}

		am.setInfoMsg(BXResources.getSysMsg("DEAL_APPROVE_CONDITIONS_INFO",
				languageId));

		if (pm != null)
			am.copyMsgs(pm);

		if (sufficientLOBAuthority == false)
			am.addMsg(BXResources.getSysMsg(
					"DEAL_APPROVE_INSUFFICIENT_LOB_LIMIT", languageId),
					am.CRITICAL);

		return am;
	}

	//--Release2.1--//
	//--> Added LanguageId for Multilingual support
	//--> By Billy 10Dec2002
	private String formEscalationDialogMessage(UserProfile nextAppProfile,
			int languageId) {
		if (nextAppProfile == null)
			return BXResources.getSysMsg("DEAL_APPROVE_ESCALATE", languageId);

		// get contact information for next approver

		Contact contact = null;

		try {
			SessionResourceKit srk = nextAppProfile.getSessionResourceKit();

			contact = new Contact(srk);

			contact = contact.findByPrimaryKey(new ContactPK(nextAppProfile
					.getContactId(), 1));
		} catch (Exception e) {
			;
		} // ignore problem (if one)

		// if problems locating contact information ignore and go with standard
		// message

		if (contact == null)
			return BXResources.getSysMsg("DEAL_APPROVE_ESCALATE", languageId);

		String approverName = contact.getContactFullName();

		String escalationMessage = ActiveMessage.replaceString("^", BXResources
				.getSysMsg("DEAL_APPROVE_ESCALATE_WITH_USER", languageId),
				approverName);

		return escalationMessage;
	}

	public UserProfile getApproverProfile(int userProfileId, int institutionId) {
		SysLogger logger = srk.getSysLogger();

		UserProfile approverProfile = null;

		try {
			approverProfile = new UserProfile(srk);
			approverProfile = approverProfile
					.findByPrimaryKey(new UserProfileBeanPK(userProfileId, institutionId));
		} catch (Exception e) {
			logger
					.error("AAGGuidline::getapproverProfile: Error locating profile");
			return null;
		}

		return approverProfile;
	}
}

