package com.basis100.deal.aag;

import java.io.Serializable;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.DBA;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.workflow.WFEExternalTaskCompletion;
import com.basis100.workflow.entity.AssignedTask;

/**
 * . escalate()/getTaskCompletionCode()
 *
 *   escalate() is called by the interface method getTaskCompletionCode(); getTaskCompletionCode()
 *   is called by the Workflow Engine to determine task completion when the external decision
 *   code is returned - i.e. the case for many of the Deal approval tasks such as Decision Deal,
 *   Higher Approval Requested and Joint Approval Requested.
 *
 *   escalate() assigns a higher/joint approver to the deal. Details of the approver (e.g.
 *   user profile id) are those pre-determined by the AAG module during approval testing.
 *   The status of the Deal is also updated.
 *
 *   Deal update activity is performed in the caller's transaction context - that is, it is
 *   expected that the caller has established a proper transaction context and that all changes
 *   will be committed (or rolled-back) by the caller. Note, this does not necessarilly mean
 *   the the WFE (known caller) must establish the transaction context - the user of the
 *   WFE (i.e. MOS application) typically controls transaction context (i.e. context
 *   established before calling WFE).
 *
 *   See Consolidated Design for details on the meanings of the task completion codes and
 *   in particular external decision completion code and WFEExternalTaskCompletion interface.
 *
 **/

public class AAGTaskCompletionObj implements WFEExternalTaskCompletion, Serializable
{
    private int nextStatus;
    private int approverId;

    // default task completion - regression to same stage since the workflow organizes the potentially
    // iterative approval tasks in a single stage.

    private int taskCompletionCode    = Sc.TASK_WORKFLOW_REGRESSION;
    private int regressionBackToStage = 0;

    public AAGTaskCompletionObj(int nextStatus, int nextApprover)
    {
      this.nextStatus       = nextStatus;
      this.approverId       = nextApprover;
    }

    public AAGTaskCompletionObj(int nextStatus, int nextApprover,
                                int taskCompletionCode, int regressionBackToStage)
    {
      this.nextStatus       = nextStatus;
      this.approverId       = nextApprover;
      this.taskCompletionCode     = taskCompletionCode;
      this.regressionBackToStage  = regressionBackToStage;
    }

    public String getName()
    {
        return Mc.AAG_RULE_PREFIX;
    }

    public int getTaskCompletionCode(SessionResourceKit srk, AssignedTask aTask, Deal deal, DealUserProfiles dealUserProfiles) throws Exception
    {
        //SysLogger logger = aTask.getLogger();

        escalate(srk, deal, dealUserProfiles);

        return taskCompletionCode;
    }

    public int getRegressionBackToStage()
    {
        return regressionBackToStage;
    }

    private void escalate(SessionResourceKit srk, Deal deal, DealUserProfiles dealUserProfiles) throws Exception
    {
        deal = DBA.recordDealStatus(srk, deal, nextStatus, approverId);

        // if precaution only - should never happen ... but
        if (approverId != -1)
          dealUserProfiles.setCurrentApproverProfile(approverId);
        else
          taskCompletionCode = Sc.TASK_NOT_DONE;

        deal.ejbStore();
    }
}

