/*
 * 19/Apr/2007 DVG #DG598 dupeCheck engine refactoring
 * Created on 4-May-2005
 *
 * Catherine Rutgaizer:
 * This is a copy if GENXDupeCheckProcessor, adapted for DJ
 * Please note that 99% of the code is exactly the same as in GENX;  
 * Moving common code to an ancestor class is a part of an upcoming project
 * 
 */
package com.basis100.deal.duplicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import MosSystem.Mc;

import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.Property;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.util.Xc;

public final class DjDupeCheckProcessor extends DupeCheckProcessor implements Xc
{
  // ------------------ Catherine pvcs #957 for DJ, May 2005 --- begin ------------------  
  protected void doSpecialProcessingOnReplace(int lExistPrimPropId) throws RemoteException, FinderException {
    writeLog("doSpecialCompare() entered.");
      
    // 1) fix PARTYDEALASSOC for appraiser, if exists
      Property newPrimProp = new Property(srk, null);
      int exDealId = existingDeal.getDealId();
      newPrimProp = newPrimProp.findByPrimaryProperty(exDealId, existingDeal.getCopyId(), existingDeal.getInstitutionProfileId());
      
      if (newPrimProp != null) {
        int newPrimPropId = newPrimProp.getPropertyId();
        
        updateAppraiserPartyAssoc(exDealId, lExistPrimPropId, newPrimPropId);
      }
      
    // 2) update Parties for received deal
      int lStat = existingDeal.getStatusId();
   	  logger.debug("[DUPECHECK] DjDupeCheckProcessor@performReplace(): existing deal status is " + 
   	  		((lStat == Mc.DEAL_RECEIVED) ? "DEAL_RECEIVED" : "NOT DEAL_RECEIVED" ));
      
   	  if (lStat == Mc.DEAL_RECEIVED ){
      	updatePartyProfileAssoc();
      }
  }
  // ------------------ Catherine pvcs #957 for DJ, May 2005 --- end --------------------  

  // -------- Catherine, pvcs #957 for DJ ----------------- begin ----------------------- 
  protected int getExistingPrimPropertyId() throws RemoteException, FinderException {
    // to fix a bug that does not update PARTYDEALASSOC with new propertyId
    Property lProp = new Property(srk, null);
    lProp = lProp.findByPrimaryProperty(existingDeal.getDealId(), existingDeal.getCopyId(), existingDeal.getInstitutionProfileId());
    int lExistPrimPropId = 0;

    if (lProp != null) {
      lExistPrimPropId = lProp.getPropertyId();
    }
    return lExistPrimPropId;
  }
  // -------- Catherine, pvcs #957 for DJ ----------------- end ----------------------- 

  private void updateAppraiserPartyAssoc(int dealId, int existPropId, int newPropId) throws RemoteException, FinderException {
    PartyProfile pp = new PartyProfile(srk);
    Collection col = pp.findByDealAndType(dealId, Mc.PARTY_TYPE_APPRAISER);
    Iterator it = col.iterator();
    while( it.hasNext() ){
      pp = (PartyProfile) it.next();
      if ((pp.getPropertyId() != 0) && (pp.getPropertyId() == existPropId)) {
        logger.debug("[DUPECHECK]@updateAppraiserPartyAssoc() disassociating partyProfileId = " + pp.getPartyProfileId() + ", propertyId = " + existPropId);
        pp.disassociate(dealId, existPropId);

        logger.debug("[DUPECHECK]@updateAppraiserPartyAssoc() associating PARTYDEALASSOC with new propertyId: partyProfileId = " + pp.getPartyProfileId() 
            + ", dealId = " + dealId + ", propertyId = " + newPropId);
        pp.associate(dealId, newPropId);
      }
    }
  }

  // --------------------- DJ, pvcs #957, Catherine -- begin --------------------- 

  /**
   * DJ, pvcs #957, 
   * Catherine Rutgaizer, 4-May-05,
   * The following method is used in DupeCheck process for DJ
   * This method replaces existing PartyDealAssoc records 
   * where partyTypeId = 60 or is in PARTYPROFILESOURCE 
   * @throws FinderException
   * @throws RemoteException
   */
  protected void updatePartyProfileAssoc() throws RemoteException, FinderException {
    writeLog("updating PartyDealAssoc records");
	  PartyProfile curProfile = new PartyProfile(srk);
	  PartyProfile exProfile = new PartyProfile(srk); 
	  
	  Map newPp = curProfile.getTypedParties4Dj(deal.getDealId());
	  if (newPp.isEmpty() || newPp == null) {
	    logger.debug("[DUPECHECK] DjDupeCheckProcessor@updatePartyProfileAssoc(): No PARTYDEALASSOC records on new deal");
	    return; 
	  }
	  
	  Map existingPp = curProfile.getTypedParties4Dj(existingDeal.getDealId());
      logger.debug("[DUPECHECK] Existing Deal #" + existingDeal.getDealId() + " existingParties = " + existingPp);
      logger.debug("[DUPECHECK]      New Deal #" + deal.getDealId() + " newParties      = " + newPp);

	  // Set stProfiles = curProfile.getPartyProfileSources();

	  Set st = newPp.keySet();
	  Iterator it = st.iterator();
	  int curPartyProfileId; 
	  Integer key;
	    
	  while (it.hasNext()){
	    key = (Integer)it.next(); 
	    curPartyProfileId = ((Integer)newPp.get(key)).intValue();
	    curProfile = curProfile.findByPartyProfileId(curPartyProfileId);
	    int propId = -1;
	      
	    if (key.intValue() == Mc.PARTY_TYPE_APPRAISER) {
	      Property prop = new Property(srk, null);
	      prop = prop.findByPrimaryProperty(existingDeal.getDealId(), existingDeal.getCopyId(), existingDeal.getInstitutionProfileId());
	      // prop = prop.findByPrimaryProperty(deal.getDealId(), deal.getCopyId());
	       
	      if (prop != null) {
	        propId = prop.getPropertyId();
	        logger.debug("[DUPECHECK] updatePartyProfileAssoc(): primary property Id = " + propId 
	            + " for deal = " + deal.getDealId());
	      }
	    }
	    
	    if ( !existingPp.isEmpty() ) {
		  if (existingPp.containsKey(key)) {
		    logger.debug("[DUPECHECK] Existing deal has PARTYDEALASSOC record of type " + key);
		    // replace 
		    int exPartyProfileId = ((Integer)existingPp.get(key)).intValue();
		    exProfile = exProfile.findByPartyProfileId(exPartyProfileId);
		    if (exProfile != null){
		      if ((exPartyProfileId == curPartyProfileId) && key.intValue() != Mc.PARTY_TYPE_APPRAISER) {
 	            logger.debug("[DUPECHECK] ProfileIds are the same, skipping replacement");
	            continue;                                                           // skip for the same profile for types other than appraiser. Because PartyProfile.getProfileIs() returns -1, have to replace Appraiser associations
	          }
		      
		      logger.debug("[DUPECHECK] Deleting PARTYDEALASSOC for deal = " 
		          + existingDeal.getDealId()+ ", partytypeid = " + exProfile.getPartyProfileId());
		      exProfile.disassociate(existingDeal.getDealId(), -2);                 // remove all records for given profile disregarding propertyId
		    }
		  }
		  // if an existing deal does not have party of this type, adopt an incoming deal's PARTYDEALASSOC record
		}

        // curProfile.disassociate(deal.getDealId(), propId);  // Catherine: 28-Oct-05 - should not be doing this; collapsed deal should have all paries
	    curProfile.associate(existingDeal.getDealId(), propId);
	     
	    logger.debug("[DUPECHECK] updatePartyProfileAssoc(): Associating partyProfileId = " + curPartyProfileId 
	        + " with deal = " + existingDeal.getDealId());
	  }
	}
   // --------------------- DJ, pvcs #957, Catherine -- end --------------------- 

      // --------------------- DJ, pvcs #957, 6-Jul-05 Catherine -- begin ---------------------
  protected void doSpecialCompare(DealCompare dealCompare) {
    writeLog("Calling doSpecialCompare() entered.");
    
    // --------------------- pvcs # 1683, spec v2, 21-Sep-05 Catherine --------------------- 
    dealCompare.compareInsureOnlyApplicants();             // this is a DJ-specific comparison  
    // --------------------- spec v2 ---------------------  end

    dealCompare.compareParties();                          // this is a DJ-specific comparison                                         
  }
 // --------------------- DJ, pvcs #957, 6-Jul-05 Catherine -- end ----------------------- 
}
