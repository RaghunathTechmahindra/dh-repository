package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.security.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.picklist.BXResources;
import com.basis100.log.*;


/**
 *   The NoteBuilder class has a single public method that takes a duplicate search result
 *   and returns a String representation of the appropriate DealNote entry.
 */
public class SourceMessageBuilder
{

  //--Release2.1--//
  //// Set of methods with ranslated messages for pre-defined (by Ingestion) languageId.
  //// Note, that the Static Labels on this PassiveMessage page could not be toggled by the user.
  //// This set of methods has been replaced the original one.

  public static PassiveMessage getMessage(int institutionProfileId, SearchResult result, int languageId)
  {
     String clazzname = result.getClass().getName();
     PassiveMessage pm = new PassiveMessage();
     int sc = result.getDealStatusCategory();
     String theMsg;

    if(clazzname.equals("com.basis100.deal.duplicate.PartySearchResult"))
    {
       theMsg = setupPartyWarningMsg(institutionProfileId, result, languageId);
       pm.addMsg(theMsg,pm.NONCRITICAL);
       return pm;
    }
    if(clazzname.equals("com.basis100.deal.duplicate.PropertySearchResult"))
    {
       theMsg = setupPropertyWarningMsg(institutionProfileId, result, languageId);
       pm.addMsg(theMsg,pm.NONCRITICAL);
       return pm;
    }
    else if(clazzname.equals("com.basis100.deal.duplicate.BorrowerSearchResult"))
    {
       theMsg = setupBorrowerWarningMsg(institutionProfileId, result, languageId);
       pm.addMsg(theMsg,pm.NONCRITICAL);
       return pm;

    }
    else if(clazzname.equals("com.basis100.deal.duplicate.CautionPropertySearchResult"))
    {
       theMsg = setupCautionPropertyWarningMsg(institutionProfileId, result, languageId);
       pm.addMsg(theMsg,pm.NONCRITICAL);
       return pm;
    }
    else if(clazzname.equals("com.basis100.deal.duplicate.DealSearchResult"))
    {
      // This will only be used for Deal Ingestion
      if(!result.isMatch() || result.getAction() != result.ARCHIVE) return pm;

      if(result.preApp && !result.isPropertyDelta())
      {
        String msgPreApp = BXResources.getGenericMsg("PRE_APPROVED_DEAL_LABEL", languageId);

        pm.addMsg(msgPreApp, pm.NONCRITICAL);
      }

      if(sc == SearchResult.FD && !result.isDealDelta())
      {
       String msgFinal = BXResources.getGenericMsg("FINALIZED_DEAL_LABEL", languageId) +
                              " " +
                              BXResources.getGenericMsg("DO_NOT_RESUBMIT_DEAL_LABEL", languageId);

        pm.addMsg(msgFinal,pm.NONCRITICAL);
      }

      if((sc == result.POST )||(sc == result.D) || (sc == result.PD))
      {
         String msgPostActive = BXResources.getGenericMsg("DEAL_POST_ACTIVE_LABEL", languageId) +
                                " " +
                                BXResources.getGenericMsg("DO_NOT_RESUBMIT_DEAL_LABEL", languageId);

         pm.addMsg(msgPostActive, pm.NONCRITICAL);
      }
    }

    return pm;
  }

  private static String setupPartyWarningMsg(int institutionProfileId, SearchResult result, int languageId)
  {
    String theMsg = "";

    // Setup Warning messages Header
    theMsg = BXResources.getGenericMsg("PARTY_WITH_THE_APPLICANT_EXIST_LABEL", languageId) + "<br><br>";

    // Display the Parties Info. found as said in spec.
    List theMatched = result.getAllMatchedEntries();
    ListIterator li = theMatched.listIterator();
    PartyProfile foundParty = null;

    String partyName = BXResources.getGenericMsg("PARTY_NAME_LABEL", languageId);
    String companyName = BXResources.getGenericMsg("COMPANY_NAME_LABEL", languageId);
    String type = BXResources.getGenericMsg("TYPE_LABEL", languageId);
    String status = BXResources.getGenericMsg("STATUS_LABEL", languageId);
    String undefined = BXResources.getGenericMsg("UNDEFINED_LABEL", languageId);

    String partyShortName = "";
    String partyCompanyName = "";

    while(li.hasNext())
    {
      foundParty = (PartyProfile)li.next();

      if(foundParty.getPtPShortName() == null) {
        partyShortName = BXResources.getGenericMsg("NULL_LABEL", languageId);
      }
      else {
        partyShortName = foundParty.getPtPShortName();
      }

      if(foundParty.getPartyCompanyName() == null) {
        partyCompanyName = BXResources.getGenericMsg("NULL_LABEL", languageId);
      }
      else {
        partyCompanyName = foundParty.getPartyCompanyName();
      }

      theMsg += "<b>" + partyName + " : </b>" + partyShortName + " | ";
      theMsg += "<b>" + companyName + " : </b>" + partyCompanyName + " | ";
      try
      {
        ////theMsg += "<b>" + type + " : </b>" + foundParty.getPartyType() + " | ";
        theMsg += "<b>" + type + " : </b>" +
                  BXResources.getPickListDescription(institutionProfileId, "PARTYTYPE", foundParty.getPartyTypeId(), languageId) + " | ";
      }
      catch(Exception e)
      {
        theMsg += "<b>" + type + ": </b>" + undefined + " | ";
      }
      try
      {
        ////theMsg += "<b>" + status + " : </b>" + foundParty.getPartyStatus() + "<br>";
        theMsg += "<b>" + status + " : </b>" +
                  BXResources.getPickListDescription(institutionProfileId, "PROFILESTATUS", foundParty.getProfileStatusId(), languageId) + "<br>";

      }
      catch(Exception e)
      {
        theMsg += "<b>" + status + ": </b>" + undefined + "<br><br>";
      }
    }
    return theMsg;
  }

  private static String setupPropertyWarningMsg(int institutionProfileId, SearchResult result, int languageId)
  {
    String theMsg = "";

    // Setup Warning messages Header
    theMsg = BXResources.getGenericMsg("DEAL_WITH_THE_PROPERTY_EXIST_LABEL", languageId) + "<br><br>";

    // Display the Parties Info. found as said in spec.
    List theMatched = result.getAllMatchedEntries();
    ListIterator li = theMatched.listIterator();
    Deal foundDeal = null;

    String dealNumber = BXResources.getGenericMsg("DEAL_NUMBER_LABEL", languageId);
    String dealStatus = BXResources.getGenericMsg("DEAL_STATUS_LABEL", languageId);
    String dealStatusDate = BXResources.getGenericMsg("DEAL_STATUS_DATE_LABEL", languageId);
    String dealPurpose = BXResources.getGenericMsg("DEAL_PURPOSE_LABEL", languageId);
    String specialFeature = BXResources.getGenericMsg("SPECIAL_FEATURE_LABEL", languageId);

    while(li.hasNext())
    {
      foundDeal = (Deal)li.next();
      theMsg += "<b>" + dealNumber + " : </b>" + foundDeal.getApplicationId() + " | ";
      ////theMsg += "<b>" + dealStatus + " : </b>" + PicklistData.getDescription("Status",foundDeal.getStatusId()) + " | ";
      theMsg += "<b>" + dealStatus + " : </b>" +
                BXResources.getPickListDescription(institutionProfileId, "STATUS", foundDeal.getStatusId(), languageId) + " | ";

      theMsg += "<b>" + dealStatusDate + " : </b>" + foundDeal.getStatusDate() + " | ";
      ////theMsg += "<b>" + dealPurpose + " : </b>" + PicklistData.getDescription("dealpurpose", foundDeal.getDealPurposeId()) + " | ";
      theMsg += "<b>" + dealPurpose + " : </b>" +
                BXResources.getPickListDescription(institutionProfileId, "DEALPURPOSE", foundDeal.getDealPurposeId(), languageId) + " | ";

      ////theMsg += "<b>" + specialFeature + " : </b>" + PicklistData.getDescription("specialfeature", foundDeal.getSpecialFeatureId()) + "<br><br>";
      theMsg += "<b>" + specialFeature + " : </b>" +
                BXResources.getPickListDescription(institutionProfileId, "SPECIALFEATURE", foundDeal.getSpecialFeatureId(), languageId) + "<br><br>";

    }
    return theMsg;
  }

  private static String setupBorrowerWarningMsg(int institutionProfileId, SearchResult result, int languageId)
  {
    String theMsg = "";

    // Setup Warning messages Header
    theMsg = BXResources.getGenericMsg("DEAL_WITH_THE_APPLICANT_EXIST_LABEL", languageId) + "<br><br>";

    // Display the Parties Info. found as said in spec.
    List theMatched = result.getAllMatchedEntries();
    ListIterator li = theMatched.listIterator();
    Deal foundDeal = null;

    String dealNumber = BXResources.getGenericMsg("DEAL_NUMBER_LABEL", languageId);
    String dealStatus = BXResources.getGenericMsg("DEAL_STATUS_LABEL", languageId);
    String dealStatusDate = BXResources.getGenericMsg("DEAL_STATUS_DATE_LABEL", languageId);
    String dealPurpose = BXResources.getGenericMsg("DEAL_PURPOSE_LABEL", languageId);
    String specialFeature = BXResources.getGenericMsg("SPECIAL_FEATURE_LABEL", languageId);

    while(li.hasNext())
    {
      foundDeal = (Deal)li.next();
      theMsg += "<b>" + dealNumber + " : </b>" + foundDeal.getApplicationId() + " | ";
      ////theMsg += "<b>" + dealStatus + " : </b>" + PicklistData.getDescription("Status",foundDeal.getStatusId()) + " | ";

      theMsg += "<b>" + dealStatus + " : </b>" +
                BXResources.getPickListDescription(institutionProfileId, "STATUS",foundDeal.getStatusId(), languageId) + " | ";
      theMsg += "<b>" + dealStatusDate + " : </b>" + foundDeal.getStatusDate() + " | ";

      ////theMsg += "<b>" + dealPurpose + " : </b>" + PicklistData.getDescription("dealpurpose", foundDeal.getDealPurposeId()) + " | ";
      theMsg += "<b>" + dealPurpose + " : </b>" +
                BXResources.getPickListDescription(institutionProfileId, "DEALPURPOSE", foundDeal.getDealPurposeId(), languageId) + " | ";

      ////theMsg += "<b>" + specialFeature + " : </b>" + PicklistData.getDescription("specialfeature", foundDeal.getSpecialFeatureId()) + "<br><br>";
      theMsg += "<b>" + specialFeature + " : </b>" +
                BXResources.getPickListDescription(institutionProfileId, "SPECIALFEATURE", foundDeal.getSpecialFeatureId(), languageId) + "<br><br>";

    }
    return theMsg;
  }

  private static String setupCautionPropertyWarningMsg(int institutionProfileId, SearchResult result, int languageId)
  {
    String theMsg = "";

    // Setup Warning messages Header
    theMsg = BXResources.getGenericMsg("PROPERTY_ADDED_TO_CAUTION_LIST", languageId) + "<br><br>";

    // Display the Parties Info. found as said in spec.
    List theMatched = result.getAllMatchedEntries();
    ListIterator li = theMatched.listIterator();
    CautionProperty foundCP = null;

    String propertyAddress = BXResources.getGenericMsg("PROPERTY_ADDRESS_LABEL", languageId);
    String city = BXResources.getGenericMsg("CITY_LABEL", languageId);
    String province = BXResources.getGenericMsg("PROVINCE_LABEL", languageId);
    String postalCode = BXResources.getGenericMsg("POSTAL_CODE_LABEL", languageId);
    String cautionReason = BXResources.getGenericMsg("CAUTION_REASON_LABEL", languageId);

    while(li.hasNext())
    {
       foundCP = (CautionProperty)li.next();
       theMsg += "<b>" + propertyAddress + " : </b>" + foundCP.getCPStreetNumber() + " " +
                  foundCP.getCPStreetName() + " " +
                  BXResources.getPickListDescription(institutionProfileId, "STREETTYPE", foundCP.getCPStreetTypeId(), languageId) + " " +
                  BXResources.getPickListDescription(institutionProfileId, "STREETDIRECTION", foundCP.getCPStreetDirectionId(), languageId) + " | ";

       theMsg += "<b>" + city + " : </b>" + foundCP.getCPCity() + " | ";
       theMsg += "<b>" + province + " : </b>" +
                  BXResources.getPickListDescription(institutionProfileId, "PROVINCE", foundCP.getCPProvinceId(), languageId) + " | ";

       theMsg += "<b>" + postalCode + " : </b>" + foundCP.getCPPostalFSA() + " " + foundCP.getCPPostalLDU() + "<br>";
       theMsg += "<b>" + cautionReason + " : </b>" + foundCP.getCautionReason() + "<br><br>";

    }
    return theMsg;
  }
}
