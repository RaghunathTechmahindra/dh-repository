package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;

public class DuplicateSourceFinder extends DuplicateFinder
{

  public DuplicateSourceFinder(SessionResourceKit kit)
  {
     srk = kit;
     logger = srk.getSysLogger();
  }

  protected Collection obtainDplSourceSystemMailBoxNbr(Deal parDeal, String sourceMailBoxNbr,Date minDate)throws SearchException
  {
     try
     {
        Deal tofind = new Deal(srk,null);   //::calcmon
        List found = (List)tofind.findByDCSourceMailBox(sourceMailBoxNbr,minDate);

        ListIterator li = found.listIterator();
        Deal current = null;

        while(li.hasNext())
        {
          current = (Deal)li.next();

          //remove the current deal from the found list
          if(current.getDealId() == parDeal.getDealId())
           li.remove();
        }

        return found;
     }
     catch(Exception e)
     {
       String msg = "Source Search Failed for Deal: " + parDeal;
       logger.error(msg);
       logger.error(e);
       throw new SearchException(msg);
     }
  }

  private Collection extractUnion(List firstList, List secList)
  {
    Iterator firstIterator = firstList.iterator();
    Iterator secIterator;

    List retList = new ArrayList();
    Deal aDeal;
    Deal pDeal;
    while( firstIterator.hasNext() ){
      aDeal = (Deal)firstIterator.next();
      secIterator = secList.iterator();
      while( secIterator.hasNext() ){
        pDeal = (Deal) secIterator.next();
        if( aDeal.getDealId() == pDeal.getDealId() ){
          retList.add(pDeal);
          secIterator.remove();
        }
      }
    }
    return retList;
  }

  /**
   *  Using the supplied Deal search for a match (by specification criteria)
   *  on the deal source.
   */
  public SearchResult search(DealEntity de,  Date minDate) throws SearchException
  {
     Deal mostRecent = null;
     Deal deal = (Deal)de;

     String sourceMailBoxNbr = deal.getSourceSystemMailBoxNBR();

     List foundMailBox;

     //=========================================================================
     // step 1.
     //  search for match on sourcesystem mail box number
     //=========================================================================
     if(sourceMailBoxNbr != null){
       foundMailBox = (List)obtainDplSourceSystemMailBoxNbr(deal, sourceMailBoxNbr, minDate );
       if( foundMailBox.isEmpty() ){
         return new SourceSearchResult(srk);
       }
       //=======================================================================
       // step 2
       // find the list with the applicant match
       //=======================================================================
       DuplicateBorrowerFinder app = new DuplicateBorrowerFinder(srk);
       List appMatchList = (List)app.findDealListMatch(de,minDate);
       //=======================================================================
       // step 3
       // find the list with the property match
       //=======================================================================
       DuplicatePropertyFinder prp = new DuplicatePropertyFinder(srk);
       List propMatchList = (List)prp.findDealListMatch(de,minDate);
       //=======================================================================
       // step 4.
       // find the union of these two lists.
       // Request has changed on Sep 06, 2001: Originally we had to find duplicate
       // borrower AND duplicate property. After that we searched for union, but
       // now if we do not find duplicate on BOTH we take the list of duplicates
       // on BORROWER or PROPERTY.
       //=======================================================================
       List appPropUnionList;
       if(appMatchList.isEmpty() && propMatchList.isEmpty()){
         // do not bother. it is not match on source
         return new SourceSearchResult(srk);
       }else if( !(appMatchList.isEmpty()) && propMatchList.isEmpty()){
         appPropUnionList = appMatchList;
       }else if( appMatchList.isEmpty() && !(propMatchList.isEmpty()) ){
         appPropUnionList = propMatchList;
       } else {
         appPropUnionList = (List)extractUnion(appMatchList, propMatchList);
        }
       //=======================================================================
       // step 5
       // find the union list between appPropUnionList and foundMailBox list
       //=======================================================================
       List mbAppPropUnionList = (List)extractUnion(foundMailBox, appPropUnionList);
       if( mbAppPropUnionList.isEmpty() ){
         return new SourceSearchResult(srk);
       }else if( mbAppPropUnionList.size() == 1 ){
         mostRecent = (Deal)mbAppPropUnionList.get(0);
         return new SourceSearchResult(srk,mostRecent,deal);
       }else {
         mostRecent = getMostRecentGoldDealId(mbAppPropUnionList);
         return new SourceSearchResult(srk,mostRecent,deal);
       }
     }
     // there is no match
     return new SourceSearchResult(srk);
  }
}

/*
  public SearchResult search(DealEntity de,  Date minDate) throws SearchException
  {
     Deal mostRecent = null;
     Deal deal = (Deal)de;

     String sourceAppId = deal.getSourceApplicationId();
     String sourceMailBoxNbr = deal.getSourceSystemMailBoxNBR();


     try
     {
        Deal tofind = new Deal(srk,null);   //::calcmon
        //List found = (List)tofind.findByDupeCheckCriteria(sourceMailBoxNbr,sourceAppId,minDate);
        List found = (List)tofind.findByDCSourceApplicationId(sourceMailBoxNbr,sourceAppId,minDate);

        ListIterator li = found.listIterator();
        Deal current = null;

        //if(found.isEmpty())   //didn't find a match
        // return new SourceSearchResult(srk);

        while(li.hasNext())
        {
          current = (Deal)li.next();

          //remove the current deal from the found list
          if(current.getDealId() == deal.getDealId())
           li.remove();
        }

        if(found.isEmpty())   //found a match on current deal only
         return new SourceSearchResult(srk);

        //sort the found deals from most recent to last
        mostRecent = getMostRecentGoldDeal(found);
     }
     catch(Exception e)
     {
       String msg = "Source Search Failed for Deal: " + deal;
       logger.error(msg);
       logger.error(e);
       throw new SearchException(msg);
     }



     //create the result
     return new SourceSearchResult(srk,mostRecent,deal);

  }

*/