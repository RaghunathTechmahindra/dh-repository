package com.basis100.deal.duplicate;

/**
 * 12/Oct/2007 DVG #DG640 FXP18697: DupeChcek - BDN Express - The note generated in Deal Y during the Deal Compare Process is NOT correct 
 * 28/Sep/2007 DVG #DG628 FXP18531: BDN Express - DupeCheck - Deal Comparing Header is always showing even when there is no change in the resubmission 
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 */

import java.util.*;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;

public class DupCheckAction implements Xc
{

  protected SessionResourceKit srk;
  protected SysLogger logger;
  protected DealPK storedDealPK;


  public DupCheckAction(SessionResourceKit srk)
  {
     this.srk = srk;
     logger = srk.getSysLogger();
  }

  /**
   *  Executes the appropriate set of actions for a given SearchResult based on
   *  search result type and context.
   *  @return true if ingested file archival is required.
   */

  //--Release2.1--//
  //// This methods is re-written to provide the translation lookup.
  public PassiveMessage execute(SearchResult searchResult, int langId)throws DupCheckActionException
  {

    storedDealPK = null;
    //#DG600 improve
    //String clazzname = searchResult.getClass().getName();
    PassiveMessage message = new PassiveMessage();

//    if(clazzname.equals("com.basis100.deal.duplicate.PropertySearchResult") ||
//     clazzname.equals("com.basis100.deal.duplicate.BorrowerSearchResult")||
//     clazzname.equals("com.basis100.deal.duplicate.PartySearchResult"))
    if(searchResult instanceof com.basis100.deal.duplicate.PropertySearchResult ||
    		searchResult instanceof com.basis100.deal.duplicate.BorrowerSearchResult||
    		searchResult instanceof com.basis100.deal.duplicate.PartySearchResult)
    {
       return SourceMessageBuilder.getMessage(srk.getExpressState().getDealInstitutionId(), searchResult, langId);
    }
    //if(clazzname.equals("com.basis100.deal.duplicate.CautionPropertySearchResult"))
    if(searchResult instanceof com.basis100.deal.duplicate.CautionPropertySearchResult)
    {
       // Perform Set Note if Caution Property matched
       if(searchResult.isMatch())
          performSetNoteOnly(searchResult);
       return SourceMessageBuilder.getMessage(srk.getExpressState().getDealInstitutionId(), searchResult, langId);
    }
    //else if(clazzname.equals("com.basis100.deal.duplicate.DealSearchResult"))
    else if(searchResult instanceof com.basis100.deal.duplicate.DealSearchResult)
    {
      int actid = searchResult.getAction();

      if(actid == SearchResult.ARCHIVE)
      {
        performArchive(searchResult);
      }
      else if(actid == SearchResult.REPLACE)
      {
        performReplace(searchResult);
      }
      else if(actid == SearchResult.CREATE_WITH_EXISTING)
      {
        this.performCreateWithExisting(searchResult);
      }
      else if(actid == SearchResult.CREATE_WITH_NEW)
      {
        this.performCreateWithNew(searchResult);
      }
      else if(actid == SearchResult.CREATE_XREF_DEALS)
      {
        this.performCreateXRefDeals(searchResult);
      }
    }

    return message;
  }

  /**
   *  The performArchive action removes the ingested deal.
   */
  private void performArchive(SearchResult sr)throws DupCheckActionException
  {
    try
    {
      Deal searchDeal = sr.getSearchDeal();
      Deal existingDeal = sr.getDuplicateDeal();

      DupeCheck.debug("Archive Deal: " + searchDeal.getDealId());

      //note to existing insertion
      DealNotes dealNota = new DealNotes(srk);
      Formattable nota = getNote(sr);
      dealNota.create(existingDeal, Sc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG640

      existingDeal.setCheckNotesFlag("Y");
      existingDeal.ejbStore();

      //#DG600 DupeCheck.debug(nota.toString());
      DupeCheck.debug(String.format("%s", nota));

      MasterDeal md = new MasterDeal(srk,null);
      md.findByPrimaryKey(new MasterDealPK(searchDeal.getDealId()));
      md.removeDeal();
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: Archive action failed";
      //#DG600 srk.getSysLogger().error(msg);
      SysLog.error(getClass(), e, msg);
      throw new DupCheckActionException(msg);
    }

    storedDealPK = null;
  }

  /**
   * Replace the existing deal with the incoming deal keeping the original deal number. <br>
   * Any manual changes made to the deal before being replaced will not be carried <br>
   * forward to the replaced deal.
   * The performReplace action removes the ingested deal.  <br>
   */
  private void performReplace(SearchResult sr)throws DupCheckActionException
  {

    Deal existingDeal = sr.getDuplicateDeal();
    Deal incomingDeal = sr.getSearchDeal();


    // preserve required fiels.
    int lUnderwriterUserId = existingDeal.getUnderwriterUserId();
    int lAdministratorId = existingDeal.getAdministratorId();
    int lBranchProfileId = existingDeal.getBranchProfileId();
    DupeCheck.debug("Replace Deal: " + existingDeal.getDealId());

    MasterDeal existingMD = null;
    MasterDeal incomingMD = null;

    try
    {

      existingMD = new MasterDeal(srk,null,existingDeal.getDealId());  //::calcmon
      incomingMD = new MasterDeal(srk,null,incomingDeal.getDealId());  //::calcmon

      Collection ids = null;

      ids = existingMD.getCopyIds();

      int gid = existingMD.copyFrom(incomingMD,incomingMD.getGoldCopyId());

      if(ids != null)
      {
        Iterator it = ids.iterator();
        Integer n = null;

        while(it.hasNext())
        {
          n = (Integer)it.next();
          existingMD.deleteCopy(n.intValue());
        }
      }

      existingMD.setGoldCopyId(gid);
      existingMD.ejbStore();

      DealPK pk = new DealPK(existingMD.getDealId(), gid);
      existingDeal = existingDeal.findByPrimaryKey(pk);

      DealNotes dealNota = new DealNotes(srk);
      Formattable nota = getNote(sr);
      dealNota.create(existingDeal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,0,nota);		//#DG640

      existingDeal.setCheckNotesFlag("Y");
      existingDeal.ejbStore();

      existingDeal.setUnderwriterUserId( lUnderwriterUserId );
      existingDeal.setAdministratorId( lAdministratorId );
      existingDeal.setBranchProfileId( lBranchProfileId );
      existingDeal.ejbStore();

      incomingMD.removeDeal();

      //#DG600 DupeCheck.debug(nota.toString());
      DupeCheck.debug(String.format("%s", nota));
      storedDealPK = pk;
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: Replace action failed";
      //#DG600 srk.getSysLogger().error(msg);
      SysLog.error(getClass(), e, msg);
      throw new DupCheckActionException(msg);
    }

  }

  private Deal findDealToCompare(MasterDeal md, Deal ed){
    Deal retDeal;
    List dealCopies = (List)md.getCopyIdsSorted();

    if(dealCopies.isEmpty() ){
      return null;
    }
    if(ed.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL ){
      if(ed.getCopyId() == ((Integer)dealCopies.get(0)).intValue() ){
        return null;
      }
      try{
        retDeal = new Deal( srk,null,ed.getDealId(), ((Integer)dealCopies.get(0)).intValue() );
        return retDeal;
      }catch(Exception exc){
        return null;
      }
    }else{
      Iterator it = dealCopies.iterator();
      while(it.hasNext()){
        int lCopyId = ((Integer)it.next()).intValue();
        if( lCopyId >= ed.getCopyId() ){
          return null;
        }
        try{
          Deal compDeal = new Deal(srk,null,ed.getDealId(),lCopyId);
          if(compDeal.getSpecialFeatureId() !=  Mc.SPECIAL_FEATURE_PRE_APPROVAL ){
            return compDeal;
          }
        }catch(Exception exc1){
          return null;
        }
      }
    }
    return null;
  }

  private void performCreateXRefDeals(SearchResult sr) throws DupCheckActionException
  {
    DupeCheck.debug("Create New Deal With Cross Reference");
    Deal existingDeal = sr.getDuplicateDeal();
    Deal incomingDeal = sr.getSearchDeal();

    MasterDeal existingMD = null;
    //MasterDeal incomingMD = null;

    try{
      existingMD = new MasterDeal(srk,null,existingDeal.getDealId());
      //DealPK pk = (DealPK)existingDeal.getPk();

      existingDeal.setReferenceDealNumber("" + incomingDeal.getDealId() );
      existingDeal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_RESUBMITTED_AS_DEAL);

      incomingDeal.setStatusId(Mc.DEAL_COLLAPSED);
      incomingDeal.setReferenceDealNumber("" + existingDeal.getDealId() );
      incomingDeal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_ORIGINAL_DEAL_RESUBMIT);
      //========================================================================
      // Call dealCompare module
      //========================================================================
      Deal dealToCompare = this.findDealToCompare(existingMD,existingDeal);
      DealCompare dealCompare;
      if( dealToCompare == null ){
        dealCompare = new DealCompare(existingDeal,incomingDeal,srk);
      }else{
        dealCompare = new DealCompare(dealToCompare,incomingDeal,srk);
      }
      dealCompare.compareAll();

      /*#DG600 StringBuffer notePrefix = new StringBuffer("");
      String strDateVal;*/
      Date curDate = new Date();
      Formattable nota;
      if( dealCompare.getTotalDifferences() > 0){
        /*strDateVal = DocPrepLanguage.getInstance().getFormattedDate(new Date(), Mc.LANGUAGE_PREFERENCE_ENGLISH );
        notePrefix.append("A resubmission has been received for this deal and was saved as deal : ");
        notePrefix.append(incomingDeal.getDealId()).append(" on ").append(strDateVal).append(": ");
        dealCompare.writeNoteToDeal(notePrefix.toString(),existingDeal) */;
        nota = BXResources.newFormata(DUP_CHEK_RESUBMISIONRCVD, incomingDeal.getDealId(), curDate);
        dealCompare.writeNoteToDeal(nota, existingDeal);		//#DG640
      }else{
        /*strDateVal = DocPrepLanguage.getInstance().getFormattedDate(new Date(), Mc.LANGUAGE_PREFERENCE_ENGLISH );
        notePrefix.append("A resubmission has been received for this deal and was saved as deal : ");
        notePrefix.append(incomingDeal.getDealId()).append(" on ").append(strDateVal).append(": ");*/
        nota = BXResources.newFormata(DUP_CHEK_RESUBMISIONRCVDA, incomingDeal.getDealId(), curDate);	//#DG640 provide date just in case...
        //#DG628 if no differences then it's just a simple deal note to be written 
        //dealCompare.writeNoteToDeal(nota, existingDeal);
        DealNotes dealNota = new DealNotes(srk);
  			dealNota.create(existingDeal, DEAL_NOTES_CATEGORY_GENERAL, 0, nota);		//#DG640
        //==========================================================================
      }
      existingDeal.setCheckNotesFlag("Y");
      existingDeal.setResubmissionFlag("Y");
      //ALERT:ZR:
      // could not find the property to set for redecesion deal flag.
      incomingDeal.setCheckNotesFlag("Y");
      incomingDeal.ejbStore();
      existingDeal.ejbStore();
    }catch( Exception exc ){
      String msg = "Duplicate Deal Check: CreateXRefDeals action failed.";
      srk.getSysLogger().error(msg + "  Original exception message:" + exc );
      //SysLog.error(getClass(), e, msg);
      throw new DupCheckActionException(msg);
    }
  }

 /**
   * Create a new deal ( copy ) and appends it to the existing MasterDeal.<br>
   * The new deal object should have a status of "Received",
   * and a deal object type of "Deal".
   * If the createNew type is Preapp gone firm <br>
   *    - Updates deal Special Feature to 'blank'
   *    - Sets the old copy to
   */
  private void performCreateWithExisting(SearchResult sr)throws DupCheckActionException
  {
    DupeCheck.debug("Create New Deal With Existing DealId");

    Deal existingDeal = sr.getDuplicateDeal();
    Deal incomingDeal = sr.getSearchDeal();

    MasterDeal existingMD = null;
    MasterDeal incomingMD = null;

    try
    {

      //#DG640 DealPK pk = (DealPK)existingDeal.getPk();

      existingMD = new MasterDeal(srk,null,existingDeal.getDealId()); //::calcmon
      // Update the deal object locked status to "Yes".
      existingDeal.setScenarioLocked("Y");
      existingDeal.setScenarioRecommended("N");
      existingDeal.setCopyType("S");

      //  Create a new deal object with the existing deal number.
      incomingMD = new MasterDeal(srk,null,incomingDeal.getDealId());  //::calcmon

      // The new deal object should have a status of "Received",
      // Update deal Special Feature to 'blank'

      incomingDeal.setStatusId(Mc.DEAL_RECEIVED);
      incomingDeal.setSpecialFeatureId(0);
      incomingDeal.setScenarioRecommended("Y");
      incomingDeal.setCopyType("G");
      incomingDeal.setScenarioLocked("N");

      int gid = existingMD.copyFrom(incomingMD,incomingMD.getGoldCopyId());

      existingMD.setGoldCopyId(gid);
      existingMD.ejbStore();


      //insert the note to existing deal ...
      DealNotes dealNota = new DealNotes(srk);
      Formattable nota = getNote(sr);
      dealNota.create(existingDeal,Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG640
      existingDeal.setCheckNotesFlag("Y");

      incomingDeal.ejbStore();
      existingDeal.ejbStore();

      setStoredDealPK(existingDeal.getDealId(), gid);

      //#DG600 DupeCheck.debug(nota.toString());
      DupeCheck.debug(String.format("%s", nota));
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: CreateNew action failed";

      //#DG600 srk.getSysLogger().error(msg);
      SysLog.error(getClass(), e, msg);
      throw new DupCheckActionException(msg);
    }

  }


  /**
   * Create a new deal ( copy ) and appends it to the existing MasterDeal.<br>
   * The new deal object should have a status of "Received",
   * and a deal object type of "Deal".
   * If the createNew type is Preapp gone firm <br>
   *    - Updates deal Special Feature to 'blank'
   *    - Sets the old copy to
   */
  private void performCreateWithNew(SearchResult sr)throws DupCheckActionException
  {
    DupeCheck.debug("Create New Deal with New DealId");

    Deal existingDeal = sr.getDuplicateDeal();
    Deal incomingDeal = sr.getSearchDeal();

    //MasterDeal existingMD = null;
    //MasterDeal incomingMD = null;

    try
    {
      // The new deal object should have a status of "Received",
      // Update deal Special Feature to 'blank'

      int existingStatus = existingDeal.getStatusId();

      if(existingStatus == Mc.DEAL_DENIED && !( sr.isSoftDenial() ))
         incomingDeal.setPreviouslyDeclined("Y");

      incomingDeal.setStatusId(Mc.DEAL_RECEIVED);

      setStoredDealPK(incomingDeal.getDealId(),incomingDeal.getCopyId());

      //insert the note to existing deal ...
      DealNotes dealNota = new DealNotes(srk);
      Formattable nota = getNote(sr);
      dealNota.create(incomingDeal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG640
      incomingDeal.setCheckNotesFlag("Y");

      incomingDeal.ejbStore();

    	//#DG600 DupeCheck.debug(nota.toString());
      DupeCheck.debug(String.format("%s", nota));
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: CreateNew action failed";

    	//#DG600 srk.getSysLogger().error(msg);
      SysLog.error(getClass(), e, msg);
      throw new DupCheckActionException(msg);
    }

  }

  private void performSetNoteOnly(SearchResult sr)throws DupCheckActionException
  {

    Deal incomingDeal = sr.getSearchDeal();

    try
    {
      //#DG640 DealPK pk = (DealPK)incomingDeal.getPk();
      //insert the note...
      DealNotes dealNota = new DealNotes(srk);
      Formattable nota = getNote(sr);

      dealNota.create(incomingDeal,Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG640
      incomingDeal.setCheckNotesFlag("Y");
    	//#DG600 DupeCheck.debug(nota.toString());
      DupeCheck.debug(String.format("%s", nota));
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: SetNoteOnly action failed";

    	//#DG600 srk.getSysLogger().error(msg);
      SysLog.error(getClass(), e, msg);
      throw new DupCheckActionException(msg);
    }

  }


  public DealPK getStoredDealPK()
  {
    return storedDealPK;
  }

  private void setStoredDealPK(int dealId, int copyId)
  {
    storedDealPK = new DealPK(dealId, copyId);
  }

  /*#DG600
   * since this class is used only here let's move it here then*/
  //class NoteBuilder implements Xc   {
  private static boolean isSourceMatch = false;
  private static boolean isApplicantMatch = false;
  private static boolean isPropertyMatch = false;

  /*private static String UPDATED     = "This deal was electronically updated by ";
    private static String ATTEMPTED   = " attempted to electronically update this deal. ";
    private static String NOT_CHANGED = "The deal has not been changed because the current deal status is " ;
    private static String FD          = "in Final Disposition and the deal attibutes have not changed";
    private static String GONE_FIRM   = " and has now gone firm.";
    private static String APPLICANT   = " isApplicantMatch ";
    private static String PROPERTY    = " isPropertyMatch ";
    private static String SRC_CHANGE  = "the source has changed";
    private static String APPLICANT_MATCH  = "the isApplicantMatch information has matched.";
    private static String PROPERTY_MATCH = "the isPropertyMatch information has matched.";
    private static String CAUTION_PROP = "The isPropertyMatch in this deal matches a isPropertyMatch that has been added to a caution list.";
   */

  private static void init() {
    isSourceMatch = false;
    isApplicantMatch = false;
    isPropertyMatch = false;
  }
	
  public String buildMatrixNote(SearchResult result)
  {
  	init();
  	return getMatrixNote(result);
  }
  private String getMatrixNote(SearchResult result)
  {
  	return "";
  }

  //#DG600 rewritten
  /**
   * @param result - a SearchResult object produced as a result of a DupCheck search
   * @return a String representation of the appropriate DealNote entry.
   * /
    public static String buildNote(SearchResult result)
    {
       return new NoteBuilder().getNote(result);
    } */

  /** 
   * 
   * builds the list of formattables to produce deal notes text 
   * 
   * @param srchRes 
   * @return
   */
  public Formattable getNote(SearchResult srchRes)
  {
  	if(!srchRes.isMatch())return null;

  	if(srchRes.getLevelOneCode() == SearchResult.CP)
  		return BXResources.newDPIGMsg(DUP_CHEK_CAUTION_PROP);

  	/*//#DG600 
       String sob = srchRes.getSOBShortName();
       String sf = srchRes.getSFShortName();
       String status = srchRes.getDealStatus();*/
  	Object sob = srchRes.getSOBShortName();
  	Object sf = srchRes.getSFShortName();
  	Formattable dealStatus = srchRes.getDealStatus();

  	if(srchRes.getAction() == SearchResult.REPLACE)
  	{
  		//return updatedBy(sob,sf);
  		return BXResources.newFormata(DUP_CHEK_UPDATED, sob, sf);
  	}

  	if(srchRes.isPreApp() & srchRes.isPropertyDelta())
  	{
  		//return this.goneFirm(sob,sf);
  		return BXResources.newFormata(DUP_CHEK_GONE_FIRM, sob, sf);
  	}

  	if(srchRes.getDealStatusCategory() == SearchResult.FD & !srchRes.isDealDelta())
  	{
  		//return this.finalDisposition(sob,sf);
  		return BXResources.newFormata(DUP_CHEK_ATTEMPTEDFD, sob, sf);
  	}

  	isSourceMatch = srchRes.isSourceMatch();
  	isApplicantMatch = srchRes.isApplicantMatch();
  	isPropertyMatch = srchRes.isPropertyMatch();

  	if(srchRes.getDealStatusCategory() == SearchResult.FD & srchRes.isDealDelta())
  	{
  		//return this.matchedExists(srchRes.getDuplicateId(), "Final Disposition");
  		return matchedExists(srchRes.getDuplicateId());
  	}


  	if(srchRes.getLevelOneCode() ==  SearchResult.S || srchRes.getLevelOneCode() ==  SearchResult.SAP)
  	{
  		if((srchRes.getDealStatusCategory() == SearchResult.POST )||
  				(srchRes.getDealStatusCategory() == SearchResult.D) ||
  				(srchRes.isPreApp() & !srchRes.isPropertyDelta()))
  		{
  			//return this.statusOnly(sob,sf,status);
  			return BXResources.newFormata(DUP_CHEK_ATTEMPTED, sob, sf, dealStatus);
  		}
  	}

  	if(srchRes.getLevelOneCode() !=  SearchResult.S & srchRes.getLevelOneCode() !=  SearchResult.SAP)
  	{
  		if(srchRes.getDealStatusCategory() != SearchResult.FD)
  			//return status(sob,sf,status);
  			return statusMsg(sob,sf,dealStatus);
  	}
  	return null;
  }

  /*  private String sourceFromFirm(String sobShorName, String sfshortName)    {
      return sobShorName + " from " + sfshortName;
    }
    private String updatedBy(String sobShorName, String sfshortName)    {
      return  UPDATED + sourceFromFirm(sobShorName,sfshortName);
    }
    private String goneFirm(String sobShorName, String sfshortName)    {
      return updatedBy(sobShorName,sfshortName) + GONE_FIRM;
    }
    private String statusOnly(String sobShorName, String sfshortName, String dealStatus)    {
       return sourceFromFirm(sobShorName,sfshortName) + ATTEMPTED + NOT_CHANGED + dealStatus + ".";
    }
    private String status(String sobShorName, String sfshortName, String dealStatus)    {
      String val = statusOnly(sobShorName, sfshortName, dealStatus );
      val = val.substring(0,val.length() - 1);

      if((!isSourceMatch & !(isPropertyMatch || isApplicantMatch)) || (!isSourceMatch & isPropertyMatch & isApplicantMatch))
      val += " and ";
      else if(!isSourceMatch & ((!isPropertyMatch & isApplicantMatch) || (isPropertyMatch & !isApplicantMatch)))//isSourceMatch changed and only..
      val += ", ";
      else if(isSourceMatch & ((!isPropertyMatch & isApplicantMatch) || (isPropertyMatch & !isApplicantMatch)))
      val += "";

      if(!isSourceMatch)
       val+= SRC_CHANGE;
      if(isApplicantMatch & !isPropertyMatch)
       val+= " and only " + APPLICANT_MATCH;
      else if(isPropertyMatch & !isApplicantMatch)
       val+= " and only " + PROPERTY_MATCH;

      if(!val.endsWith(".")) val += ".";

      return val;
    }
    private String finalDisposition( String sobShorName, String sfshortName )    {
       return statusOnly(sobShorName,sfshortName,FD);
    }
    private String matchedExists(int dealid, String dealStatus)    {
      String val = "A deal with matching";

       if(isApplicantMatch & !isPropertyMatch)
        val+= APPLICANT;
       else if(isPropertyMatch & !isApplicantMatch)
        val+= PROPERTY;
       else if(isApplicantMatch && isPropertyMatch)
        val+= APPLICANT + "and" + PROPERTY;

       val += " details already exists, with the deal number " + dealid;
       val += " and deal status of " + dealStatus + ".";

       return val;
    }  */   

  /*#DG600
   * this is ugly but there's not much of another option for now :(
   * the final localization will have to be made by composing the key
   * with a suffix depending on the situation */
  private static Formattable statusMsg(Object sob, Object sf, Formattable dealStatus)
  {
  	StringBuffer keya = new StringBuffer(DUP_CHEK_ATTEMPTED);

  	if(!isSourceMatch & (!(isPropertyMatch || isApplicantMatch)) || (isPropertyMatch & isApplicantMatch))
  		keya.append('a');  //add " and "
  	else if(!isSourceMatch & ((!isPropertyMatch & isApplicantMatch) || (isPropertyMatch & !isApplicantMatch)))//isSourceMatch changed and only..
  		keya.append('c');  //add ", "
  	else //if(isSourceMatch & ((!isPropertyMatch & isApplicantMatch) || (isPropertyMatch & !isApplicantMatch)))
  		keya.append('n');  //no ending "."

  	if(!isSourceMatch)
  		keya.append('s');  //     val+= SRC_CHANGE;
  	if(isApplicantMatch & !isPropertyMatch)
  		keya.append('l');  //     val+= " and only " + APPLICANT_MATCH;
  	else if(isPropertyMatch & !isApplicantMatch)
  		keya.append('p');  //     val+= " and only " + PROPERTY_MATCH;

  	return BXResources.newFormata(keya.toString(), sob, sf, dealStatus);
  }

  private static Formattable matchedExists(int dealid)
  {
  	String keya = DUP_CHEK_MATCHEDDEAL;
  	if(isApplicantMatch & !isPropertyMatch) {
  		keya = DUP_CHEK_MATCHEDDEALAPL;
  	} else if(isPropertyMatch & !isApplicantMatch)
  		keya = DUP_CHEK_MATCHEDDEALPROP;
  	else if(isApplicantMatch && isPropertyMatch)
  		keya = DUP_CHEK_MATCHEDDEALAPL_PROP;

  	return BXResources.newFormata(keya, dealid);
  }
}
