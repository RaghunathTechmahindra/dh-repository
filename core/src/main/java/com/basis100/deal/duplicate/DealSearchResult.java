package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;


public class DealSearchResult extends SearchResult
{

  public DealSearchResult(SessionResourceKit srk, Deal found, Deal searchWith,
                            int resultcode, int deltaType)
  {
     if(found == null)
     {
       match = false;
     }
     else
     {
       this.srk = srk;
       this.levelOneCode = resultcode;
       this.found = found;
       this.searchDeal = searchWith;
       this.match = true;
       this.deltaType = deltaType;
       init();
     }
  }

  public DealSearchResult(SessionResourceKit srk)
  {
    match = false;
  }

}