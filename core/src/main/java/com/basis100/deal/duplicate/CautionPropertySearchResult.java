package com.basis100.deal.duplicate;

import java.util.*;
//import com.basis100.deal.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;



public class CautionPropertySearchResult extends SearchResult
{


  public CautionPropertySearchResult(SessionResourceKit srk, CautionProperty foundCP, Deal searchWith)
  {
    if(foundCP == null)
    {
      this.match = false;
    }
    else
    {
      this.srk = srk;
      cautionPFound = foundCP;
      searchDeal = searchWith;
      match = true;
    }
    
  }

  public CautionPropertySearchResult(SessionResourceKit srk, CautionProperty foundCP, Deal searchWith, List allMatches)
  {
    if(foundCP == null)
    {
      this.match = false;
    }
    else
    {
      this.srk = srk;
      cautionPFound = foundCP;
      searchDeal = searchWith;
      MatchedEntries.addAll(allMatches);
      match = true;
    }
    
  }

  /**
   *  returns an int indicating the appropriate action for this search result.
   *  @return the appropriate action or -1 if none is required.
   */
  public int getAction()
  {
     return this.SET_NOTE_ONLY;
  }


  public int getLevelOneCode()
  {
    return 99;
  }
} 