package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

public class PropertySearchResult extends SearchResult
{
  public PropertySearchResult(SessionResourceKit srk, Deal found, Deal searchWith)
  {
     if(found == null)
     {
       match = false;
     }
     else
     {
       this.srk = srk;
       levelOneCode = SearchResult.P;
       this.found = found;
       searchDeal = searchWith;
       match = true;

       init();
     }
  }

  public PropertySearchResult(SessionResourceKit srk, Deal found, Deal searchWith, List allDeals)
  {
     if(found == null)
     {
       match = false;
     }
     else
     {
       this.srk = srk;
       levelOneCode = SearchResult.P;
       this.found = found;
       searchDeal = searchWith;
       MatchedEntries.addAll(allDeals);
       match = true;

       init();
     }
  }

  public PropertySearchResult(SessionResourceKit srk)
  {
    this.srk = srk;
    match = false;
  }

  /**
   *  returns an int indicating the appropriate action for this search result.
   *  @return the appropriate action or -1 if none is required.
   */
  public int getAction()
  {
     return -1;
  }


} 