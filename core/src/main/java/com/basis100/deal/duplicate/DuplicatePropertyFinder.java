package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

public class DuplicatePropertyFinder extends DuplicateFinder
{

  public DuplicatePropertyFinder(SessionResourceKit kit)
  {
     srk = kit;
     logger = srk.getSysLogger();
  }

  public Collection findDealListMatch(DealEntity de, Date minDate) throws SearchException
  {
    List found = new ArrayList();

    int clsid = de.getClassId();
    Collection appProperties = null;
    Deal deal = null;

    if(clsid == ClassId.DEAL)
    {
      deal = (Deal)de;

      try
      {
        appProperties =  deal.getProperties();
      }
      catch(Exception e)
      {
        String msg = "DuplicatePropertyFinder - Failed to fetch properties for: " + de;
        logger.error(msg);
        logger.error(e);
        throw new SearchException(msg);
      }
    }
    if(clsid == ClassId.PROPERTY)
    {
      Property prop = (Property)de;
      try
      {
        deal = new Deal(srk,null,prop.getDealId(),prop.getCopyId());
      }
      catch(Exception fe)
      {
        throw new SearchException("Error occured while searching for duplicate Properties");
      }
      appProperties = new ArrayList();
      appProperties.add(prop);
    }

    Iterator li = appProperties.iterator();
    Property current = null;

    while(li.hasNext())
    {

      current = (Property)li.next();

      Set des = matchProperty(current,minDate);

      if(!(des == null || des.isEmpty()))
       found.addAll(des);
    }

    List allDeals = getAllGoldDealId(found);

    return allDeals;
  }


   /**
   *   <pre>
   *  Gets the Deal that is the most recent gold copy result of a search using
   *  Property match criteria as defined by the Dupecheck specification.
   *  <br> Namely the collateral Property details:
   *  <b>Street Number, Street Name, Street type, Street Direction,
   *  Unit, City, Province </b>  for each and every Property associated with a Deal.
   *  If a single Property on a deal does not match the search fails.
   *  </pre>
   *  @return most recent matched Deal or null if none is found
   *  @param deal - the incoming Deal
   *
   */
  public SearchResult search(DealEntity de,  Date minDate)throws SearchException
  {
    List found = new ArrayList();

    int clsid = de.getClassId();
    Collection appProperties = null;
    Deal deal = null;

    if(clsid == ClassId.DEAL)
    {
      deal = (Deal)de;

      try
      {
        appProperties =  deal.getProperties();
      }
      catch(Exception e)
      {
        String msg = "DuplicatePropertyFinder - Failed to fetch properties for: " + de;
        logger.error(msg);
        logger.error(e);
        throw new SearchException(msg);
      }
    }
    if(clsid == ClassId.PROPERTY)
    {
      Property prop = (Property)de;
      try
      {
        deal = new Deal(srk,null,prop.getDealId(),prop.getCopyId());
      }
      catch(Exception fe)
      {
        throw new SearchException("Error occured while searching for duplicate Properties");
      }
      appProperties = new ArrayList();
      appProperties.add(prop);
    }

    Iterator li = appProperties.iterator();
    Property current = null;

    while(li.hasNext())
    {

      current = (Property)li.next();

      Set des = matchProperty(current,minDate);

      if(!(des == null || des.isEmpty()))
       found.addAll(des);
    }

    List allDeals = getAllGoldDealId(found);

    if(allDeals.isEmpty())
     return new PropertySearchResult(srk);

    return new PropertySearchResult(srk,(Deal)allDeals.get(0),deal, allDeals);
  }


  /**
   *   <pre>
   *  Gets the set of Integers representing those DealIds where the parameter
   *  Property matches a Property from an existing Deal as defined by
   *  the Dupecheck specification.  <br> Namely the collateral Property details:
   *  <b>Street Number, Street Name, Street type, Street Direction,
   *  Unit, City, Province </b>
   *  </pre>
   *  @return a Set of Integers
   *  @param  current - the property to compare to all existing properties.
   */
  public Set matchProperty(Property current, Date minDate)
  {
    Set matchingDeals = new HashSet();

    if(current == null) return matchingDeals;

    String propertyStreetNumber = current.getPropertyStreetNumber();
    String propertyStreetName = current.getPropertyStreetName();
    String unitNumber = current.getUnitNumber();
    String city = current.getPropertyCity();
    int streetTypeId = current.getStreetTypeId();
    int streetDirectionId = current.getStreetDirectionId();
    int provinceId = current.getProvinceId();

    List result = null;

    try
    {
      Property p = new Property(srk,null); //::calcmon
      result = (List)p.findByDupeCheckCriteria(propertyStreetNumber,propertyStreetName,streetTypeId,
                             streetDirectionId,unitNumber,city,provinceId,minDate);
    }
    catch(Exception fie)
    {
      return null;
    }

    if(result.isEmpty()) return null;

    ListIterator li = result.listIterator();
    Property pr = null;

    while(li.hasNext())
    {
      pr = (Property)li.next();

      int dealId = pr.getDealId();

      if(dealId != current.getDealId())
        matchingDeals.add(new Integer(dealId));
    }

    return matchingDeals;
  }

  public boolean isCautionMatch(Property current)
  {
    String propertyStreetNumber = current.getPropertyStreetNumber();
    String propertyStreetName = current.getPropertyStreetName();
    String city = current.getPropertyCity();

    StringBuffer sql = new StringBuffer("Select * from CautionProperty where ");
    sql.append(" CPStreetNumber  =  \'" + propertyStreetNumber + "\' ");
    sql.append(" and CPStreetName  =  \'" +   propertyStreetName + "\' ");
    sql.append(" and CPCity  =  \'" +   city + "\' ");

    String sqlString = sql.toString();
    JdbcExecutor jExec = srk.getJdbcExecutor();
    boolean result = false;

    try
    {

        int key = jExec.execute(sqlString);

        for (; jExec.next(key); )
        {
          result = true;
          break;
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
        return false;
      }


    return result;

  }

}
