package com.basis100.deal.duplicate;

import java.util.*;
//import com.basis100.deal.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;



public class PartySearchResult extends SearchResult
{

  public PartySearchResult(SessionResourceKit srk,PartyProfile found, PartyProfile searchWith)
  {
     if(found == null)
     {
       match = false;
     }
     else
     {
       this.srk = srk;
       levelOneCode = SearchResult.A;
       this.foundParty = found;
       searchParty = searchWith;
       match = true;

       init();
     }
  }

  public PartySearchResult(SessionResourceKit srk,PartyProfile found, PartyProfile searchWith, List allMatches)
  {
     if(found == null)
     {
       match = false;
     }
     else
     {
       this.srk = srk;
       levelOneCode = SearchResult.A;
       this.foundParty = found;
       searchParty = searchWith;
       MatchedEntries.addAll(allMatches);
       match = true;

       init();
     }
  }

  /**
   *  returns an int indicating the appropriate action for this search result.
   *  @return the appropriate action or -1 if none is required.
   */
  public int getAction()
  {
     return -1;
  }


} 