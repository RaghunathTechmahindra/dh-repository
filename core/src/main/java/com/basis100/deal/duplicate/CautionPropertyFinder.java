package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;



public class CautionPropertyFinder extends DuplicateFinder
{

  SessionResourceKit srk;
  SysLogger logger;
  
  public CautionPropertyFinder(SessionResourceKit kit)
  {
     srk = kit;
     logger = srk.getSysLogger();
  }


  public SearchResult search(DealEntity de, Date afterDate)throws SearchException
  {
    int clsid = de.getClassId();
    Collection appProperties = null;
    List duplicates = new ArrayList();
    Deal deal = null;

    if(clsid == ClassId.DEAL)
    {
      deal = (Deal)de;

      try
      {
        appProperties =  deal.getProperties();
      }
      catch(Exception e)
      {
        String msg = "DuplicatePropertyFinder - Failed to fetch properties for: " + de;
        logger.error(msg);
        logger.error(e);
        throw new SearchException(msg);
      }
    }
    if(clsid == ClassId.PROPERTY)
    {
      Property prop = (Property)de;

      try
      {
        deal = new Deal(srk,null,prop.getDealId(),prop.getCopyId());
      }
      catch(Exception fe)
      {
        throw new SearchException("Error occured while searching for duplicate Properties");
      }
      appProperties = new ArrayList();
      appProperties.add(prop);
    }

    Iterator li = appProperties.iterator();
    Property current = null;

    while(li.hasNext())
    {
      current = (Property)li.next();
      duplicates.addAll(matchCP(current));
    }

    if(!duplicates.isEmpty())
        return new CautionPropertySearchResult(srk,(CautionProperty)duplicates.get(0), deal, duplicates);
    else
        return new CautionPropertySearchResult(srk, null, deal);
  }


  public List matchCP(Property de)throws SearchException
  {
    List duplicates = new ArrayList();
    Property theProperty = (Property)de;
    try
    {
      CautionProperty cp = new CautionProperty(srk);
      duplicates = (List)cp.findByAddressComponents(theProperty.getPropertyStreetNumber(),
                theProperty.getPropertyStreetName(), theProperty.getPropertyCity());
    }
    catch(Exception fe)
    {
      throw new SearchException("Error occured while searching for Caution Property");
    }

    return duplicates;

  }
 
}
