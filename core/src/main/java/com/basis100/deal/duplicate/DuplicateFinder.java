package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.util.*;
import com.basis100.deal.util.collections.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.picklist.*;

/**
 * The abstract superclass of all Duplicate Finder types. Each Finder subclass is responsible
 * for it's own search semantics. In turn each type returns a relative SearchResult type from
 * which search information may be gleaned.
 *
 */
public abstract class DuplicateFinder
{

  protected SessionResourceKit srk;
  protected SysLogger logger;
  protected Deal duplicate;
  private int sysDependantCutOffDate = 90;

  public DuplicateFinder(){};

  public abstract SearchResult search(DealEntity deal, Date minDate)throws SearchException;

  /**
   *  Function to return a sorted (based on the ApplicationDate) List of deals
   *    based on input collection of dealids
   */
  public List getAllGoldDealId(Collection dealIds, SessionResourceKit srk)
  {
    ArrayList found = new ArrayList(dealIds);

    Iterator fit = found.iterator();

    List deals = new ArrayList();

    try
    {
      //Deal currentDeal = new Deal(srk,null);

      while(fit.hasNext())
      {
        Integer currentId = (Integer)fit.next();
        int dealId = currentId.intValue();
        try
        {
          Deal foundDeal = new Deal(srk, null);
          foundDeal = foundDeal.findByGoldCopy(dealId);
          deals.add(foundDeal);
        }
        catch(FinderException fe)
        {
          continue;
        }
      }
    }
    catch(Exception e)
    {
      ;// not bloody likely
    }

    if(deals.isEmpty())
     return deals;

    Collections.sort(deals,new DealApplicationDateComparator());

    return deals;
  }

  public List getAllGoldDealId(Collection dealIds)
  {
    return getAllGoldDealId(dealIds, this.srk);
  }

  public Deal getMostRecentGoldDealId(Collection found)
  {
      return getMostRecentGoldDealId(found,this.srk);
  }
   /**
   *  Given a collection of DealId Integers get goldCopy Deal with the most
   *  recent applicationDate. When any two Deals are compared if both have null
   *  application dates the one with the greater DealId is considered to be most
   *  recent.
   *  If only one of the compared Deals has a null application date the one with the
   *  non-null application date is considered to be most recent.
   */
  public Deal getMostRecentGoldDealId(Collection dealIds, SessionResourceKit srk)
  {
    List deals = getAllGoldDealId(dealIds, srk);

    Deal out = null;

    if(deals != null && !deals.isEmpty())
     out = (Deal)deals.get(0);

    return out;
  }

  /**
   *  given a collection of Deals sort them by DealId and then find the one
   *  with the most recent application date. If the application date is null
   *  the deal is considered to be older than any Deal with a non null application date.
   */
  public Deal getMostRecentDeal(Collection found)
  {

    if(found.isEmpty()) return null;

    ArrayList list = new ArrayList(found);
    Collections.sort(list,new DealApplicationDateComparator());

    return (Deal)list.get(0);
  }

   /**
   *  given a collection of Deals sort them by DealId and then find the one
   *  with the most recent application date. If the application date is null
   *  the deal is considered to be older than any Deal with a non null application.
   */
  public Deal getMostRecentGoldDeal(Collection found)
  {

    if(found.isEmpty()) return null;

    ArrayList list = new ArrayList(found);

    Iterator fit = found.iterator();
    Deal current = null;
    MasterDeal md = null;
    MasterDealPK pk = null;

    ArrayList deals = new ArrayList();

    while(fit.hasNext())
    {
      current = (Deal)fit.next();
      int dealId = current.getDealId();

      try
      {
         md = new MasterDeal(srk,null,dealId);  //::calcmon
         pk = new MasterDealPK(dealId);
         md.findByPrimaryKey(pk);
         int copy = md.getGoldCopyId();

         if(copy == current.getCopyId())
          deals.add(current);
         else
          deals.add(new Deal(srk,null,dealId,copy)); //::calcmon
      }
      catch(Exception e)
      {
        continue;
      }
    }


    Collections.sort(list,new DealApplicationDateComparator());

    return (Deal)list.get(0);
  }


  /**
   *  returns a value based on presence of any property values for
   *  propertyStreetNumber or propertyStreetName
   */
  public int propertyDeltaType(Collection ps)
  {
     //=========================================================================
     // Zivko made this change on 2002-01-18 because the case when new deal does
     // not have properties was not covered.
     //=========================================================================
     if(ps.isEmpty()){return SearchResult.DELTA_TYPE_NONE; }

     Iterator it = ps.iterator();
     while(it.hasNext())
     {
        Property curr = (Property)it.next();

        String f1 = curr.getPropertyStreetNumber();
        String f2 = curr.getPropertyStreetName();

        if( f1 == null || f2 == null || f1.trim().length()==0 || f2.trim().length()== 0)
        {
          return SearchResult.DELTA_TYPE_NONE;
        }
     }

     return SearchResult.DELTA_TYPE_PROPERTY;
  }

  //============================================================================
  // ZR: utility method called by  detectDealFieldDelta
  //============================================================================
  private boolean isCBAttachmentMatch(Deal ex, Deal in)
  {
    Collection exBorColl ;
    Collection inBorColl;
    try{
      exBorColl = ex.getBorrowers();
      inBorColl = in.getBorrowers();
    } catch(Exception exc){
      return false;
      // ALERT: handle this exception
    }

    Borrower primBorEx;
    Borrower primBorIn;

    boolean exHasAttachment = false;
    boolean inHasAttachment = false;

    boolean exPrimBorFound = false;
    boolean inPrimBorFound = false;

    // find the primary borrower for existing deal;
    Iterator it = exBorColl.iterator();
    while(it.hasNext()){
      primBorEx = (Borrower)it.next();
      if(primBorEx.isPrimaryBorrower()){
        exPrimBorFound = true;
        if( primBorEx.getBureauAttachment() == null){
          continue;
        }
        if( primBorEx.getBureauAttachment().equalsIgnoreCase("y") ){
          exHasAttachment = true;
        }
        break;
      }
    }

    // find the primary borrower for new deal
    it = inBorColl.iterator();
    while( it.hasNext() ){
      primBorIn = (Borrower)it.next();
      if(primBorIn.isPrimaryBorrower()){
        inPrimBorFound = true;
        if(primBorIn.getBureauAttachment() == null){
          continue;
        }
        if( primBorIn.getBureauAttachment().equalsIgnoreCase("y") ){
          inHasAttachment = true;
        }
        break;
      }
    }

    if( exPrimBorFound && inPrimBorFound){
      // now decide about the match
      if( exHasAttachment == true  && inHasAttachment == true ) { return true; }
      if( exHasAttachment == true  && inHasAttachment == false ){ return true; }
      if( exHasAttachment == false  && inHasAttachment == true ){ return false; }
      if( exHasAttachment == false  && inHasAttachment == false ){ return true; }
    }

    return false;
  }

  public int detectDealFieldDelta(Deal ex, Deal in)
  {
    if(ex.getTotalLoanAmount() != in.getTotalLoanAmount())
      return SearchResult.DELTA_TYPE_DEAL;
    if(ex.getTotalPurchasePrice() != in.getTotalPurchasePrice())
      return SearchResult.DELTA_TYPE_DEAL;
    if(ex.getNumberOfBorrowers() !=  in.getNumberOfBorrowers())
      return SearchResult.DELTA_TYPE_DEAL;
    if(ex.getNumberOfProperties() != in.getNumberOfProperties())
      return SearchResult.DELTA_TYPE_DEAL;
    if(ex.getCombinedTotalIncome() != in.getCombinedTotalIncome())
      return SearchResult.DELTA_TYPE_DEAL;
    if(ex.getCombinedTotalLiabilities() != in.getCombinedTotalLiabilities())
      return SearchResult.DELTA_TYPE_DEAL;
    if( !isCBAttachmentMatch( ex, in  ) )
      return SearchResult.DELTA_TYPE_DEAL;

    Date exEstClosing = ex.getEstimatedClosingDate();
    Date inEstClosing = in.getEstimatedClosingDate();

    if((exEstClosing != null && inEstClosing == null) ||
           (exEstClosing == null && inEstClosing != null))
      return SearchResult.DELTA_TYPE_DEAL;

    if(exEstClosing != null && inEstClosing != null)
      if(!exEstClosing.equals(inEstClosing))
         return SearchResult.DELTA_TYPE_DEAL;


    return SearchResult.DELTA_TYPE_NONE;
  }

  public int findStatusCategory(Deal mostRecent)
  {
    int dsc = -1;

     if(mostRecent != null)
       dsc = mostRecent.getStatusCategoryId();


    return dsc;

  }
  /**
   * Gets the system assigned cutoff date for searches
   * @param srk - Session resources
   * @param appDate - the Application date to count back from. <br>If null then the current date is used.
   * @returns system assigned search cut-off as a java.util.Date
   */
  public static Date getMinSearchDate(SessionResourceKit srk, Date appDate)
  {
      int mos  = 0;

      try
      {
        InstitutionProfile ip = new InstitutionProfile(srk);
        ip = ip.findByFirst();

        mos = ip.getDupeCheckTimeFrame();
      }
      catch(Exception e)
      {
        ; //if can't get institution profile then dupecheck is off.
        return new Date();
      }

      Calendar appCal = Calendar.getInstance();

      if(appDate != null)
       appCal.setTime(appDate);
      else
       appCal.setTime(new Date());


      appCal.add(appCal.MONTH, (mos * -1));

      return appCal.getTime();
  }


}
