package com.basis100.deal.duplicate;

public class DcpConstants {
  static final int DUPECHECK_MESSAGE_1  = 101; 
  static final int DUPECHECK_MESSAGE_2  = 102; 
  static final int DUPECHECK_MESSAGE_3  = 103; 
  static final int DUPECHECK_MESSAGE_4  = 104; 
  static final int DUPECHECK_MESSAGE_5  = 105; 
  static final int DUPECHECK_MESSAGE_6  = 106; 
  static final int DUPECHECK_MESSAGE_7  = 107; 
  static final int DUPECHECK_MESSAGE_8  = 108; 
  static final int DUPECHECK_MESSAGE_9  = 109; 
  static final int DUPECHECK_MESSAGE_10 = 110; 
  static final int DUPECHECK_MESSAGE_11 = 111; 
  static final int DUPECHECK_MESSAGE_12 = 112; 
}
