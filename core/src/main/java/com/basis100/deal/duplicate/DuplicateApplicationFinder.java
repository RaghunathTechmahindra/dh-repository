package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import MosSystem.Mc;

public class DuplicateApplicationFinder extends DuplicateFinder
{

  public DuplicateApplicationFinder(SessionResourceKit kit)
  {
     srk = kit;
     logger = srk.getSysLogger();
  }



  protected Collection obtainDplSourceApplicationId(Deal parDeal, String sourceAppId,Date minDate)throws SearchException
  {
     try
     {
        Deal tofind = new Deal(srk,null);   //::calcmon
        //List found = (List)tofind.findByDCSourceApplicationId(sourceAppId,minDate);
        // Ticket #458
        int lSystemTypeId;
        if( parDeal.getSystemTypeId() > 50 ){
          lSystemTypeId = parDeal.getSystemTypeId() - 50;
        }else{
          lSystemTypeId = parDeal.getSystemTypeId();
        }
        //List found = (List)tofind.findBySouAppIdAndSysType( sourceAppId, parDeal.getSystemTypeId(), minDate );
        List found = (List)tofind.findBySouAppIdAndSysType( sourceAppId, lSystemTypeId, minDate );

        // Ticket #458
        // if the duplicate could not be found we have to go again and search
        // for same loan code but with system type 8 (expert)
        //--> Bug fix : To keep searching with Expert type (8) even if there is some deal found
        //--> By Billy 27July2004
        if(parDeal.getSystemTypeId() > 50  ){
          List found2 = (List)tofind.findBySouAppIdAndSysType( sourceAppId, Mc.SYSTEM_TYPE_EXPERT, minDate );

          if(!found2.isEmpty())
            found.addAll(found2);
        }
        //=========== End Billy's fixes 27July2004 ==============================

        ListIterator li = found.listIterator();
        Deal current = null;

        while(li.hasNext())
        {
          current = (Deal)li.next();
          //remove the current deal from the found list
          if(current.getDealId() == parDeal.getDealId())
           li.remove();
        }

        // Ticket #458 -- change the SystemID to 8 if it's > 50
        //--> By Billy 26July2004
        if(parDeal.getSystemTypeId() > 50)
        {
          boolean isInTran = parDeal.getSessionResourceKit().isInTransaction();
          if(isInTran == false)
            parDeal.getSessionResourceKit().beginTransaction();

          parDeal.setSystemTypeId(Mc.SYSTEM_TYPE_EXPERT);
          parDeal.ejbStore();

          if(isInTran == false)
            parDeal.getSessionResourceKit().commitTransaction();
        }
        //=====================================================

        return found;
     }
     catch(Exception e)
     {
       String msg = "Source Search Failed for Deal: " + parDeal;
       logger.error(msg);
       logger.error(e);
       throw new SearchException(msg);
     }
  }

  /**
   *  Using the supplied Deal search for a match (by specification criteria)
   *  on the deal source.
   */
  public SearchResult search(DealEntity de,  Date minDate) throws SearchException
  {
     Deal mostRecent = null;
     Deal deal = (Deal)de;

     String sourceAppId = deal.getSourceApplicationId();

     List foundAppl;

     // step 1. search for match on sourceapplicationid
     if( sourceAppId != null ){
      foundAppl = (List)obtainDplSourceApplicationId(deal, sourceAppId, minDate );
      if( foundAppl.size() > 0 ){
        mostRecent = getMostRecentGoldDeal(foundAppl);
        //return new SourceSearchResult(srk,mostRecent,deal);
        return new SourceSearchResult( srk,mostRecent,deal, foundAppl );
      }
     }

     // there is no match
     return new SourceSearchResult(srk);
  }


}

