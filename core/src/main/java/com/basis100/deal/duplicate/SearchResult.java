package com.basis100.deal.duplicate;

/**
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import MosSystem.Mc;
import com.filogix.util.Xc;

public abstract class SearchResult implements Xc
{
  public static final int PD          = Mc.DEAL_STATUS_CATEGORY_PRE_DECISION;
  public static final int D           = Mc.DEAL_STATUS_CATEGORY_DECISION;
  public static final int POST        = Mc.DEAL_STATUS_CATEGORY_POST_DECISION;
  public static final int FD          = Mc.DEAL_STATUS_CATEGORY_FINAL_DISPOSITION;

  public boolean preApp = false;

  public static final int ARCHIVE                 = 1;
  public static final int CREATE_WITH_EXISTING    = 2;
  public static final int CREATE_WITH_NEW         = 3;
  public static final int REPLACE           = 4;
  public static final int SET_NOTE_ONLY     = 5;
  public static final int CREATE_XREF_DEALS = 6;
  //public static final int MERGE             = 6;

  public static final int S = 1;
  public static final int A = 3;
  public static final int P = 7;
  public static final int SOU_APPL = 15;
  public static final int SA = 4; //S+A
  public static final int SP = 8; //S+P
  public static final int AP = 10;//A+P
  public static final int SAP = 11;//S+A+P

  public static final int CP = 99;//S+A+P

  protected int levelOneCode;
  /*protected String SFShortName = "Unknown Source Firm";
  protected String SOBShortName = "Unknown Source Of Business";
  protected String dealStatus;*/
  protected Object SFShortName = BXResources.newDPIGMsg(DUP_CHEK_UNKNOWN_SOURCE_FIRM);
  protected Object SOBShortName = BXResources.newDPIGMsg(DUP_CHEK_UNKNOWN_SOURCE_OF_BUSINESS);
  protected Formattable dealStatus;

  public static final int DELTA_TYPE_PROPERTY = 2;
  public static final int DELTA_TYPE_DEAL = 4;
  public static final int DELTA_TYPE_PROPERTY_DEAL = 6;
  public static final int DELTA_TYPE_NONE = 0;
  int deltaType = -1;

  protected boolean match = false;
  protected int action = -1;
  protected int dsc = -1;

  protected boolean softDenial = false;

  protected Deal found;         // Store the most recent Deal found
  protected Deal searchDeal;    // Store the Deal in question

  // Added by BILLY to handel party checking result
  protected PartyProfile foundParty;  // Store the First Party found
  protected PartyProfile searchParty; // Store the Party in question
  // Added by BILLY to handel Caution Property checking result
  protected CautionProperty cautionPFound;  // Store the First Caution Porperty found

  // To store all matched DealEntry records
  protected List MatchedEntries = new ArrayList();

  protected SessionResourceKit srk;

  protected void init()
  {
    if(!match) return;

    loadSource();
    loadDSC();
  }


 /**
 *  Populates the given Search Result with details from its Deal instance.
 */
  private void loadSource()
  {
     if(srk == null)
      return;

     if(searchDeal == null)
     {
       /*#DG600
       setSFShortName(DUP_CHEK_UNKNOWN_SOURCE_FIRM);
       setSOBShortName(DUP_CHEK_UNKNOWN_SOURCE_OF_BUSINESS);*/
       setSFShortName(BXResources.newDPIGMsg(DUP_CHEK_UNKNOWN_SOURCE_FIRM));
       setSOBShortName(BXResources.newDPIGMsg(DUP_CHEK_UNKNOWN_SOURCE_OF_BUSINESS));
       return;
     }

     int sfp = searchDeal.getSourceFirmProfileId();
     int sob = searchDeal.getSourceOfBusinessProfileId();

     try
     {
        SourceFirmProfile firm = new SourceFirmProfile(srk,sfp);
        //#DG600 String firmName = firm.getSfShortName();
        Object firmName = firm.getSfShortName();

        if(firmName == null) firmName = firm.getSourceFirmName();
        if(firmName == null) 
        	//#DG600 firmName = "Unnamed Source Firm";
        	firmName = BXResources.newDPIGMsg(DUP_CHEK_UNNAMED_SOURCE_FIRM);

        setSFShortName(firmName);
     }
     catch(Exception e)
     {
    	 //	#DG600 setSFShortName(DUP_CHEK_UNKNOWN_SOURCE_FIRM);
        setSFShortName(BXResources.newDPIGMsg(DUP_CHEK_UNKNOWN_SOURCE_FIRM));
     }
     try
     {
        SourceOfBusinessProfile bus = new SourceOfBusinessProfile(srk,sob);
      	//#DG600 String sobName = bus.getSOBPShortName();
        Object sobName = bus.getSOBPShortName();

        if(sobName == null)
        {
        	//#DG600 sobName = "Unnamed Source Of Business";
          sobName = BXResources.newDPIGMsg(DUP_CHEK_UNNAMED_SOURCE_OF_BUSINESS);
        }

        setSOBShortName(sobName);
     }
     catch(Exception e)
     {
    		//#DG600 setSOBShortName("Unknown Source of Business");
        setSOBShortName(BXResources.newDPIGMsg(DUP_CHEK_UNKNOWN_SOURCE_OF_BUSINESS));
     }

  }


  private void loadDSC()
  {
    if(found == null)
     return;

    int status = found.getStatusId();
    //#DG600 this.dealStatus = PicklistData.getDescription("Status",status);
    dealStatus = BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_STATUS, status);

    if(status == Mc.DEAL_PRE_APPROVAL_OFFERED)
    {
       dsc = POST;
       preApp = true;
    }
    else
    {
       dsc = found.getStatusCategoryId();
    }
  }

  private int actOnSourceAppMatch(){
    int retVal = -1;
    switch(dsc){
      case PD :{
        retVal = REPLACE;
        break;
      }
      case D :{
        retVal = ARCHIVE;
        break;
      }
      case POST :{
        if(preApp){
          if(isPropertyFieldDelta(deltaType)){
            //retVal = REPLACE;
            retVal = CREATE_WITH_EXISTING;
          }else{
            retVal = ARCHIVE;
          }
        }else{
          retVal = CREATE_XREF_DEALS;
        }
        break;
      }
      case FD :{
        if(deltaType == SearchResult.DELTA_TYPE_NONE || deltaType == SearchResult.DELTA_TYPE_PROPERTY){
           retVal =  ARCHIVE;
        } else if( deltaType == SearchResult.DELTA_TYPE_DEAL || deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL){
           retVal = CREATE_WITH_NEW;
        }
        break;
      }
    }
    return retVal;
  }

  private int actOnSAPMatch(){
    int retVal = -1;
    switch(dsc){
      case PD :{
        retVal = REPLACE;
        break;
      }
      case D :{
        retVal = ARCHIVE;
        break;
      }
      case POST :{
        if(preApp){
          if(isPropertyFieldDelta(deltaType)){
            //retVal = REPLACE;
            retVal = CREATE_WITH_EXISTING;
          }else{
            retVal = ARCHIVE;
          }
        }else{
          retVal = ARCHIVE;
        }
        break;
      }
      case FD :{
        if(deltaType == SearchResult.DELTA_TYPE_NONE || deltaType == SearchResult.DELTA_TYPE_PROPERTY){
           retVal =  ARCHIVE;
        } else if( deltaType == SearchResult.DELTA_TYPE_DEAL || deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL){
           retVal = CREATE_WITH_NEW;
        }
        break;
      }
    }
    return retVal;
  }

  private int actOnAPMatch(){
    int retVal = -1;
    switch(dsc){
      case PD :{
        retVal = ARCHIVE;
        break;
      }
      case D :{
        retVal = ARCHIVE;
        break;
      }
      case POST :{
        retVal = ARCHIVE;
        break;
      }
      case FD :{
        if(deltaType == SearchResult.DELTA_TYPE_NONE || deltaType == SearchResult.DELTA_TYPE_PROPERTY){
           retVal =  ARCHIVE;
        } else if( deltaType == SearchResult.DELTA_TYPE_DEAL || deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL){
           retVal = CREATE_WITH_NEW;
        }
        break;
      }
    }
    return retVal;
  }

  private int actOnSAMatch(){
    int retVal = -1;
    switch(dsc){
      case PD :{
        retVal = ARCHIVE;
        break;
      }
      case D :{
        retVal = ARCHIVE;
        break;
      }
      case POST :{
        if(preApp){
          if(isPropertyFieldDelta(deltaType)){
            //retVal = REPLACE;
            retVal = CREATE_WITH_EXISTING;
          }else{
            retVal = ARCHIVE;
          }
        }else{
          retVal = ARCHIVE;
        }
        break;
      }
      case FD :{
        if(deltaType == SearchResult.DELTA_TYPE_NONE || deltaType == SearchResult.DELTA_TYPE_PROPERTY){
           retVal =  ARCHIVE;
        } else if( deltaType == SearchResult.DELTA_TYPE_DEAL || deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL){
           retVal = CREATE_WITH_NEW;
        }
        break;
      }
    }
    return retVal;
  }

  private int actOnAMatch(){
    int retVal = -1;
    switch(dsc){
      case PD :{
        retVal = ARCHIVE;
        break;
      }
      case D :{
        retVal = ARCHIVE;
        break;
      }
      case POST :{
        retVal = ARCHIVE;
        break;
      }
      case FD :{
        if(deltaType == SearchResult.DELTA_TYPE_NONE || deltaType == SearchResult.DELTA_TYPE_PROPERTY){
           retVal =  ARCHIVE;
        } else if( deltaType == SearchResult.DELTA_TYPE_DEAL || deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL){
           retVal = CREATE_WITH_NEW;
        }
        break;
      }
    }
    return retVal;
  }

  private int actOnSPMatch(){
    int retVal = -1;
    switch(dsc){
      case PD :{
        retVal = ARCHIVE;
        break;
      }
      case D :{
        retVal = ARCHIVE;
        break;
      }
      case POST :{
        retVal = ARCHIVE;
        break;
      }
      case FD :{
        if(deltaType == SearchResult.DELTA_TYPE_NONE || deltaType == SearchResult.DELTA_TYPE_PROPERTY){
           retVal =  ARCHIVE;
        } else if( deltaType == SearchResult.DELTA_TYPE_DEAL || deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL){
           retVal = CREATE_WITH_NEW;
        }
        break;
      }
    }
    return retVal;
  }

  private int actOnPMatch(){
    int retVal = -1;
    switch(dsc){
      case PD :{
        retVal = ARCHIVE;
        break;
      }
      case D :{
        retVal = ARCHIVE;
        break;
      }
      case POST :{
        retVal = ARCHIVE;
        break;
      }
      case FD :{
        if(deltaType == SearchResult.DELTA_TYPE_NONE || deltaType == SearchResult.DELTA_TYPE_PROPERTY){
           retVal =  ARCHIVE;
        } else if( deltaType == SearchResult.DELTA_TYPE_DEAL || deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL){
           retVal = CREATE_WITH_NEW;
        }
        break;
      }
    }
    return retVal;
  }

  public int getAction()
  {
    int retVal = -1;

    if( softDenial ){
      return CREATE_WITH_NEW;
    }

    switch ( levelOneCode ){
      case SOU_APPL :{
        retVal = actOnSourceAppMatch();
        break;
      }// end case
      case SAP :{
        retVal = actOnSAPMatch();
        break;
      }// end case
      case AP :{
        retVal = actOnAPMatch();
        break;
      }// end case
      case SA :{
        retVal = actOnSAMatch();
        break;
      }// end case
      case A :{
        retVal = actOnAMatch();
        break;
      }// end case
      case SP :{
        retVal = actOnSPMatch();
        break;
      }// end case
      case P :{
        retVal = actOnPMatch();
        break;
      } // end case
    } // end switch

    return retVal;
  }

  /**
   *  returns an int indicating the appropriate action for this search result.
   *  Action types include one of: create a new deal, archive the incoming deal, or replace
   *  an existing deal.
   *  @return the appropriate action or -1 if none is required.
   */
   /*
  public int getAction()
  {

     if(!match) return -1;

     if(dsc == FD)
     {
        if(deltaType == SearchResult.DELTA_TYPE_NONE ||
           deltaType == SearchResult.DELTA_TYPE_PROPERTY)
        {
         return ARCHIVE;
        }
        else if(deltaType == SearchResult.DELTA_TYPE_DEAL ||
                deltaType == SearchResult.DELTA_TYPE_PROPERTY_DEAL)
        {
         return CREATE_WITH_NEW;
        }
     }
     else if(dsc == D)
     {
        return ARCHIVE;
     }
     else if(dsc == PD)
     {
       if(levelOneCode != S && levelOneCode != SAP)
        return ARCHIVE;
       else
        return REPLACE;
     }
     else if(dsc == POST)
     {
        if(levelOneCode == S || levelOneCode == SA || levelOneCode == SAP)
        {
            if(preApp && isPropertyFieldDelta(deltaType))
              return CREATE_WITH_EXISTING;
            else if(preApp && !isPropertyFieldDelta(deltaType))
              return ARCHIVE;
            else if(!preApp)
              return ARCHIVE;
        }
        else
        {
           return ARCHIVE;
        }

    }


     return -1;
  }
*/

  /**
   *  the 'Level One Code' is an int representing the types of matches obtained.
   *  For instance: if positive matches are found for source and property the method
   *  returns the unique value = 8
   *  @ return an int -  the level one unique code
   */
  public static int getLevelOneCodeFor(boolean s, boolean a, boolean p)
  {
    int val = 0;

    if(s)
     val += 1;
    if(a)
     val += 3;
    if(p)
     val += 7;

    return val;
  }

  public int getLevelOneCode()
  {
    return levelOneCode;
  }

	//#DG600 public String getSOBShortName()
  public Object getSOBShortName()
  {
    return SOBShortName;
  }

  /*#DG600 
  public void setSOBShortName(Formattable sob)  {
    if(sob == null || sob.trim().length() == 0) return;
    this.SOBShortName = sob;
  }*/
  public void setSOBShortName(Object sob)
  {
  	if(sob == null) 
  		return;  	
  	if(sob instanceof String) {
  		if(((String)sob).trim().length() == 0) 
  			return;
  	}
    this.SOBShortName = sob;
  }

	//#DG600 public String getSFShortName()
  public Object getSFShortName()
  {
    return SFShortName;
  }

  /*#DG600 public void setSFShortName(String sfs)  {
    if(sfs == null || sfs.trim().length() == 0) return;
    this.SFShortName = sfs;
  }*/
  public void setSFShortName(Object sfs)  // can be a string or a formattable
  {
  	if(sfs == null) 
  		return;
  	if(sfs instanceof String) {
  		if(((String)sfs).trim().length() == 0) 
  			return;
  	}
    this.SFShortName = sfs;
  }

  //#DG600 public String getDealStatus()
  public Formattable getDealStatus()
  {
    return dealStatus;
  }

  public int getDealStatusCategory()
  {
    return dsc;
  }

  public Deal getDuplicateDeal()
  {
    return found;
  }

  public PartyProfile getDuplicateParty()
  {
    return foundParty;
  }

  public int getDuplicateId()
  {
    if(found != null)
     return found.getDealId();
    else
     return -1;
  }

  public boolean isPropertyDelta()
  {
     return SearchResult.isPropertyFieldDelta(this.deltaType);
  }

  public boolean isDealDelta()
  {
     return SearchResult.isDealFieldDelta(this.deltaType);
  }


  public static boolean isPropertyFieldDelta(int delta)
  {
    if(delta == SearchResult.DELTA_TYPE_PROPERTY
             || delta == SearchResult.DELTA_TYPE_PROPERTY_DEAL )
      return true;

    return false;
  }

  public static boolean isDealFieldDelta(int delta)
  {
    if(delta == SearchResult.DELTA_TYPE_DEAL
             || delta == SearchResult.DELTA_TYPE_PROPERTY_DEAL )
      return true;

    return false;
  }

  public boolean isMatch()
  {
     return match;
  }

  public boolean isPreApp()
  {
     return this.preApp;
  }

  public Deal getSearchDeal()
  {
    return this.searchDeal;
  }

  public PartyProfile getSearchParty()
  {
    return this.searchParty;
  }

  public CautionProperty getCPFound()
  {
    return this.cautionPFound;
  }

  public List getAllMatchedEntries()
  {
    return this.MatchedEntries;
  }

  public boolean isSourceMatch()
  {
    int val = getLevelOneCode();

    if((val == 8) || (val == 4) || (val == 1) || (val == 11))
     return true;

    return false;
  }

  public boolean isPropertyMatch()
  {
    int val = getLevelOneCode();

    if((val == 8) || (val == 10) || (val == 7) || (val == 11))
     return true;

    return false;
  }

  public boolean isApplicantMatch()
  {
    int val = getLevelOneCode();

    if((val == 3) || (val == 4) || (val == 10) || (val == 11))
     return true;

    return false;
  }


  public String toString()
  {
    StringBuffer out = new StringBuffer("DUPECHECK: SearchResult - ");

    out.append(" Match: ").append(match);
    out.append(" MatchType: ").append(getMatchType(getLevelOneCode()));
    try
    {
      if(match)
      {
        out.append(" Duplicate: ").append(getDuplicateDeal().getDealId());
        out.append(" Search Deal: ").append(getSearchDeal().getDealId());
        out.append(" Action: ").append(getActionString(getAction()));
      	//#DG600 out += " Status: " + getDealStatus();
        out.append(" Status: ").append(String.format("%s", getDealStatus()));
        out.append(" StatusCategoryId: ").append(getDealStatusCategory());

        if(isPreApp() && isPropertyDelta())
         out.append(" - Note: PreApproval gone firm. ");

        if(isDealDelta())
         out.append(" - Key fields HAVE changed. ");
      }
    }
    catch(Exception e)
    {
      //blow it off
    }

    return out.toString();
  }

  //Helper methods for toString
  private String getActionString(int action)
  {
    String actStr = String.valueOf(action);

    switch(action)
    {
      case 1:
       actStr = "ARCHIVE";
       break;

      case 2:
       actStr = "CREATE_WITH_EXISTING";
       break;

      case 3:
       actStr = "CREATE_WITH_NEW";
       break;

      case 4:
       actStr = "REPLACE";
       break;

      case 5:
       actStr = "SET_NOTE_ONLY";
       break;

      default:
         ;
    }


    return actStr;

  }

  private String getMatchType(int loc)
  {
    String out = String.valueOf(loc);

    switch(loc)
    {
      case 1:
       out = "Source Only";
       break;

      case 3:
       out = "Applicant Only";
       break;

      case 7:
       out = "Property Only";
       break;

      case 4:
       out = "Source and Applicant";
       break;

      case 8:
       out = "Source and Property";
       break;

      case 10:
       out = "Applicant and Property";
       break;

      case 11:
       out = "All";
       break;

      default:
         ;
    }


    return out;

  }

  public void setSoftDenial(boolean softOrNot){
    softDenial = softOrNot;
  }

  public boolean isSoftDenial(){
    return softDenial;
  }

}
