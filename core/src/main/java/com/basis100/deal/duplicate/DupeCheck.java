package com.basis100.deal.duplicate;

import com.basis100.resources.*;
import com.basis100.deal.util.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.security.*;

public class DupeCheck
{
  private SessionResourceKit srk;
  private PassiveMessage message;

  private DealEntity found;
  private CautionProperty cautionProperty;

  private boolean duplicateFound;
  private boolean cautionFound;

  public static boolean debug = true;

  private DupeCheck(SessionResourceKit srk)
  {
    this.srk = srk;
    this.found = null;
    this.cautionProperty = null;
    this.message = new PassiveMessage();
  }

  //--Release2.1--//
  //// The whole set of methods in this class is re-written to provide the
  //// translation lookup.
  public static DupeCheck check(SessionResourceKit srk, Deal deal, Date afterDate, int langId) throws SearchException
  {
    DupeCheck dc = new DupeCheck(srk);

    // check if DepeCheck flag enable
    if( !isChecking(srk) )
        return dc;

    try
    {
      //check for caution properties
      CautionPropertyFinder cpf = new CautionPropertyFinder(srk);
      SearchResult csr = cpf.search(deal, afterDate);

      if(csr.isMatch())
      {
        handleCautionFound(csr, dc, srk, langId);
      }
      // no caution properties so now search for matches on prop, bor, src
      DuplicateDealFinder ddf = new DuplicateDealFinder(srk);
      SearchResult sr = ddf.search(deal, afterDate);

      return handleResult(sr, dc, srk, langId);
    }
    catch (Exception e)
    {
       srk.getSysLogger().error(e);
       throw new SearchException("Error in DupeCheck module: " + e.getMessage());
    }
  }

  public static DupeCheck check(SessionResourceKit srk, Property prop, Date afterDate, int langId) throws SearchException
  {
    DupeCheck dc = new DupeCheck(srk);

    // check if DepeCheck flag enable
    if( !isChecking(srk) )
        return dc;

    try
    {
      CautionPropertyFinder cpf = new CautionPropertyFinder(srk);
      SearchResult csr = cpf.search(prop, afterDate);

      if(csr.isMatch())
      {
        handleCautionFound(csr,dc,srk,langId);
      }

      DuplicatePropertyFinder dpf = new DuplicatePropertyFinder(srk);
      SearchResult sr = dpf.search(prop, afterDate);

      return handleResult(sr, dc, srk, langId);
    }
    catch (Exception e)
    {
       srk.getSysLogger().error(e);
       throw new SearchException("Error in DupeCheck module");
    }

  }


  public static DupeCheck check(SessionResourceKit srk, Borrower borrower, Date afterDate, int langId)
  throws SearchException
  {
    DupeCheck dc = new DupeCheck(srk);

    // check if DepeCheck flag enable
    if( !isChecking(srk) )
        return dc;

    try
    {
      DuplicateBorrowerFinder dbf = new DuplicateBorrowerFinder(srk);
      SearchResult sr = dbf.search(borrower, afterDate);

      return handleResult(sr, dc, srk, langId);
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e);
      throw new SearchException("Error in DupeCheck module");
    }

  }

  public static DupeCheck checkParty(SessionResourceKit srk, PartyProfile pp, Date afterDate, int langId)
  throws SearchException
  {
    DupeCheck dc = new DupeCheck(srk);

    // check if DepeCheck flag enable
    if( !isChecking(srk) )
        return dc;

    try
    {
      DuplicatePartyFinder dpf = new DuplicatePartyFinder(srk);
      SearchResult sr = dpf.search(pp, afterDate);

      return handleResult(sr,dc,srk,langId);
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e);
      throw new SearchException("Error checking for Duplicate Parties in DupeCheck module");
    }
  }

  public static DupeCheck checkCautionProperties(SessionResourceKit srk, Property prop, Date afterDate, int langId)
  throws SearchException
  {
    DupeCheck dc = new DupeCheck(srk);

    // check if DepeCheck flag enable
    if( !isChecking(srk) )
        return dc;

    try
    {
      CautionPropertyFinder dpf = new CautionPropertyFinder(srk);
      SearchResult sr = dpf.search(prop, afterDate);

      dc.handleCautionFound(sr, dc, srk, langId);
      return dc;
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e);
      throw new SearchException("Error checking for CautionProperties in DupeCheck module");
    }
  }

  public static DupeCheck checkCautionProperties(SessionResourceKit srk, Deal deal, Date afterDate, int langId)
  throws SearchException
  {
    DupeCheck dc = new DupeCheck(srk);

    // check if DepeCheck flag enable
    if( !isChecking(srk) )
        return dc;

    try
    {
      CautionPropertyFinder dpf = new CautionPropertyFinder(srk);
      SearchResult sr = dpf.search(deal, afterDate);
      return handleResult(sr, dc, srk, langId);
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e);
      throw new SearchException("Error checking for CautionProperties in DupeCheck module");
    }

  }

  private static DupeCheck handleResult(SearchResult sr, DupeCheck dc, SessionResourceKit srk, int langId)
  throws SearchException
  {
    try
    {
      if(sr.isMatch())
      {
        String clazzname = sr.getClass().getName();
        if(clazzname.equals("com.basis100.deal.duplicate.PartySearchResult"))
          dc.found = sr.getDuplicateParty();
        else
          dc.found = sr.getDuplicateDeal();
        DupCheckAction a = new DupCheckAction(srk);
        dc.message.addAllMessages(a.execute(sr, langId));
        dc.duplicateFound = true;
      }
      else
      {
        dc.duplicateFound = false;
      }
    }
    catch(Exception e)
    {
       srk.getSysLogger().error(e);
       throw new SearchException("Error handling SearchResult in DupeCheck module");
    }

    return dc;
  }

  private static void handleCautionFound(SearchResult csr, DupeCheck dc, SessionResourceKit srk, int langId)
  throws SearchException
  {
    try
    {
        CautionPropertySearchResult cpsr  = (CautionPropertySearchResult)csr;
        dc.found = cpsr.getDuplicateDeal();
        dc.cautionProperty = cpsr.getCPFound();
        DupCheckAction a = new DupCheckAction(srk);
        dc.message.addAllMessages(a.execute(cpsr, langId));
        dc.cautionFound = true;
    }
    catch(Exception e)
    {
       srk.getSysLogger().error(e);
       throw new SearchException("Error handling SearchResult in DupeCheck module");
    }
  }

  public PassiveMessage getMessage()
  {
    return message;
  }

  public DealEntity getFound()
  {
    return found;
  }

  public boolean isDuplicateFound()
  {
    return duplicateFound;
  }

  public DealEntity getCautionProperty()
  {
    return cautionProperty;
  }

  public boolean isCautionFound()
  {
    return cautionFound;
  }

  public static String debug(String in)
  {
    if(debug)
    {
     System.out.println(in);
     ResourceManager.getSysLogger("0").debug(in);
    }

    return in;
  }

  public static boolean isChecking(SessionResourceKit srk)
  {
    try
    {
      InstitutionProfile ip = new InstitutionProfile(srk);
      ip = ip.findByFirst();

      return(ip.isDupeCheckEnabled());
    }
    catch(Exception e)
    {
      // return default to False
      return(false);
    }
  }

}