package com.basis100.deal.duplicate;

import java.util.*;
//import com.basis100.deal.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;



public class DuplicatePartyFinder extends DuplicateFinder
{

 
  public DuplicatePartyFinder(SessionResourceKit kit)
  {
     srk = kit;
     logger = srk.getSysLogger();
  }

   /**
   *   <pre>
   *  Gets the Deal that is the most recent gold copy result of a search using
   *  
   *  </pre>
   *  @return most recent matched Deal or null if none is found
   *  @param deal - the incoming Deal
   *
   */
  public SearchResult search(DealEntity de,  Date minDate)throws SearchException
  {
    int clsid = de.getClassId();
    List duplicates = new ArrayList();

    if(clsid == ClassId.PARTYPROFILE)
    {
      PartyProfile pp = (PartyProfile)de;
      try
      {
        Contact contact = new Contact(srk,pp.getContactId(),1);
        duplicates = (List)pp.findByDupeCheckCriteria(contact.getContactFirstName(),
                  contact.getContactLastName(), pp.getPartyTypeId());

        // Check and elimate the Party that we looking at
        ListIterator li = duplicates.listIterator();
        PartyProfile current = null;
        while(li.hasNext())
        {
          current = (PartyProfile)li.next();

          //remove the current deal from the found list
          if(current.getPartyProfileId() == pp.getPartyProfileId())
           li.remove();
        }

        if(!duplicates.isEmpty())
          return new PartySearchResult(srk,(PartyProfile)duplicates.get(0),pp,duplicates);
      }
      catch(Exception fe)
      {
        throw new SearchException("Error occured while searching for duplicate Parties");
      }
    }
    return new PartySearchResult(srk, null, null);

  }


 
}
