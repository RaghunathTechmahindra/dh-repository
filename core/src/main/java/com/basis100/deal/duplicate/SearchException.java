package com.basis100.deal.duplicate;

public class SearchException extends Exception
{

 /**
  * Constructs a <code>SearchException</code> with no specified detail message.
  */
  public SearchException()
  {
	  super();
  }

 /**
  * Constructs a <code>SearchException</code> with the specified message.
  * @param   message  the related message.
  */
  public SearchException(String message)
  {
    super(message);
  }
}

