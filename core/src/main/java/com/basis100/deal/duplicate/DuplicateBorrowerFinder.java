package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.entity.*;
import com.basis100.resources.*;

public class DuplicateBorrowerFinder extends DuplicateFinder
{

  public DuplicateBorrowerFinder(SessionResourceKit kit)
  {
     srk = kit;
     logger = srk.getSysLogger();
  }

  public Collection findDealListMatch(DealEntity de, Date minDate) throws SearchException
  {
    int clsid = de.getClassId();
    Collection appBorrowers = null;
    Set found = new HashSet();
    Deal deal = null;

    if(clsid == ClassId.DEAL)
    {
      deal = (Deal)de;

      try
      {
        appBorrowers =  deal.getBorrowers();
      }
      catch(Exception e)
      {
        String msg = "DuplicateBorrowerFinder - Failed to fetch borrowers for: " + deal;
        logger.error(msg);
        logger.error(e);
        throw new SearchException(msg);
      }
    }
    if(clsid == ClassId.BORROWER)
    {
      Borrower bor = (Borrower)de;

      try
      {
         deal = new Deal(srk,null,bor.getDealId(),bor.getCopyId());
      }
      catch(Exception e)
      {

      }
      appBorrowers = new ArrayList();
      appBorrowers.add(bor);
    }

    Iterator li = appBorrowers.iterator();
    Borrower current = null;
    try
    {
      while(li.hasNext())
      {
        current = (Borrower)li.next();

        Set deals = matchBorrower(current,minDate);
        if(  !(deals.isEmpty()) ){
          found.addAll(deals);
        }
      }
    }
    catch(Exception e)
    {
      String msg = "DuplicateBorrowerFinder - Error matching borrowers for: " + deal;
      logger.error(msg);
      logger.error(e);
      throw new SearchException(msg);
    }

    List allDeals = getAllGoldDealId(found);
    return allDeals;
  }

/**
 *   <pre>
 *  Gets the Deal that is the most recent gold copy result of a search using
 *  Property match criteria as defined by the Dupecheck specification.
 *  <br> Namely the collateral Property details:
 *  <b>Street Number, Street Name, Street type, Street Direction,
 *  Unit, City, Province </b>  for each and every Property associated with a Deal.
 *  If a single Property on a deal does not match the search fails.
 *  </pre>
 *  @return most recent matched Deal or null if none is found
 *  @param deal - the incoming Deal
 *  @param limit - if the minimum search date.
 *
 */
  public SearchResult search(DealEntity de, Date minDate)throws SearchException
  {
    int clsid = de.getClassId();
    Collection appBorrowers = null;
    Set found = new HashSet();
    Deal deal = null;


    if(clsid == ClassId.DEAL)
    {
      deal = (Deal)de;

      try
      {
        appBorrowers =  deal.getBorrowers();
      }
      catch(Exception e)
      {
        String msg = "DuplicateBorrowerFinder - Failed to fetch borrowers for: " + deal;
        logger.error(msg);
        logger.error(e);
        throw new SearchException(msg);
      }
    }
    if(clsid == ClassId.BORROWER)
    {
      Borrower bor = (Borrower)de;

      try
      {
         deal = new Deal(srk,null,bor.getDealId(),bor.getCopyId());
      }
      catch(Exception e)
      {

      }
      appBorrowers = new ArrayList();
      appBorrowers.add(bor);
    }


    Iterator li = appBorrowers.iterator();
    Borrower current = null;
    try
    {
      while(li.hasNext())
      {
        current = (Borrower)li.next();

        Set deals = matchBorrower(current,minDate);
        if(deals.isEmpty())
           return new BorrowerSearchResult(srk); // all borrowers must match
                                                                    // and since one doesn't - return

        found.addAll(deals);
      }
    }
    catch(Exception e)
    {
      String msg = "DuplicateBorrowerFinder - Error matching borrowers for: " + deal;
      logger.error(msg);
      logger.error(e);
      throw new SearchException(msg);
    }


    List allDeals = getAllGoldDealId(found);

    if(allDeals.isEmpty())
      return new BorrowerSearchResult(srk);

    return new BorrowerSearchResult(srk, (Deal)allDeals.get(0), deal, allDeals);
  }



   /**
   *
   */
  public Set matchBorrower(Borrower current, Date minDate)throws Exception
  {
    Set matchingDeals = new HashSet();
    List result = new ArrayList();
    String sin = current.getSocialInsuranceNumber();

    if(sin != null && !sin.trim().equals(""))
    {

       try
       {
          Borrower b = new Borrower(srk,null);
          result.addAll(b.findByDupeCheckSin(sin,minDate));
       }
       catch(Exception e)
       {
         ;  // go on to demographic check
       }
    }

    try
    {
      String borrowerFirstName = current.getBorrowerFirstName();
      String borrowerMiddleInitial = current.getBorrowerMiddleInitial();
      String borrowerLastName = current.getBorrowerLastName();
      Date borrowerBirthDate = current.getBorrowerBirthDate();


      Borrower b = new Borrower(srk,null);
//logger.debug("BILLY ==> before check CheckCriteria : borrowerName = " + borrowerFirstName + " " +
//          borrowerFirstName + " " + borrowerLastName + " borrowerBirthDate = " + borrowerBirthDate);
      result.addAll( b.findByDupeCheckCriteria(borrowerFirstName,borrowerMiddleInitial,
                                            borrowerLastName,borrowerBirthDate, minDate));
    }
    catch(FinderException fie)
    {
      return matchingDeals;
    }
    catch(RemoteException re)
    {
      return matchingDeals;
    }


    ListIterator li = result.listIterator();
    Borrower br = null;

    while(li.hasNext())
    {
      br = (Borrower)li.next();
      int dealId = br.getDealId();

      if(dealId != current.getDealId())
       matchingDeals.add(new Integer(dealId));
    }

    return matchingDeals;
  }


}
