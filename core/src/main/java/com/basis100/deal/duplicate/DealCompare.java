package com.basis100.deal.duplicate;

/**
 * 14/Nov/2007 DVG #DG658 FXP19094  DupeCheck - BDN - Express - Dollar signs($) not being displayed for currency fields in Deal notes 
 * 24/Oct/2007 DVG #DG648 FXP18790: DupeCheck - BDN Express - The fields "City" and "Employment History Status"'s Names are not correct in Deal Compare Notes
 * 19/Oct/2007 DVG #DG644 FXP18792  DupeCheck - BDN Express - The property type info is missing in the Deal Compare Note
 * 15/Oct/2007 DVG #DG642 FXP18698: DupeCheck - BDN Express - Some fields showing with the index instead of the item values in Deal Compare notes 
 * 04/Oct/2007 DVG #DG634 FXP18630: DupeCheck - BDN Express - some changes won't be able to show up in Deal Compare Notes 
 * 28/Sep/2007 DVG #DG628 FXP18531: BDN Express - DupeCheck - Deal Comparing Header is always showing even when there is no change in the resubmission 
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 * 01/Aug/2006 DVG #DG476 #3892  CR315-Field XP68 LOB must be one the criteria in comparison window for resubmission task
 * 02/Nov/2008 MCM FXP23237: changed currency format to Ratio format.
 * 03/Nov/2008 MCM FXP23275: added missing label.
 */

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import MosSystem.Mc;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.*;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.*;
import com.filogix.util.Xc;

import config.Config;

/**
 * Compares 2 deals and makes a text note which can later be written to a deal's
 * DealNotes.
 * <p>
 * This class provides a facility for stopping comparison after a certain number
 * of differences exist.  If set to anything less than 1, maximum allowed
 * differences is unlimited.
 * <p>
 * To facilitate the reuse of the object, methods exist to reset the total
 * differences and max differences via <code>resetTotalDifferences()</code> and
 * <code>setMaxDifferences(int)</code>, respectively.  There is also a facility
 * to reset the note's contents via <code>resetNote</code>.
 * <p>
 * To allow for easier and dynamic formatting, a delimiter exists in between
 * each field.  A couple of parsing routines are built into this class which
 * format the new line and delimiter characters to a specific format
 * (HTML, TEXT).
 * <p>
 * Example usage:
 * <p><code><blockquote><pre>
 *     SessionResourceKit srk = new SessionResourceKit("0");
 *
 *     Deal deal1 = new Deal(srk, null, 80240, 32);
 *     Deal deal2 = new Deal(srk, null, 80240, 38);
 *
 *     //  Creates a DealCompare object, initialized with 2 deals
 *     DealCompare dc = new DealCompare(deal1, deal2);
 *
 *     //  Performs the comparison of the deals
 *     dc.compareDeals();
 *
 *     //  Performs the comparison of the primary properties on the deals
 *     dc.comparePrimaryProperties();
 *
 *     //  Formats the difference text
 *     String str = dc.formatNote();
 *
 * </pre></blockquote></code>
 * <p>
 * In addition to comparison methods, this class also has a number of useful
 * static methods for printing key fields in objects: <br/>
 * ---- change to non-static method due to different institution may has different bxresources.
 * we have to get the institution at run time.
 * <p><code><blockquote><b><pre>
 *     printAsset(Asset)
 *     printBorrower(Borrower)
 *     printBorrowerAddress(BorrowerAddress)
 *     printContact(Contact)
 *     printDownPaymentSource(DownPaymentSource)
 *     printEmploymentHistory(EmploymentHistory)
 *     printIncome(Income)
 *     printLiability(Liability)
 * </pre></b></blockquote></code>
 * <p>
 * This class has a <code>main()</code> that handles command line arguments.
 * They are expected in the following format in order to be handled:
 * <p>
 * <code>DealCompare [-deal1 deal_num1] -deal2 [deal_num2] [-writenote]</code>
 * <p>
 * <code><blockquote><pre>
 *     -deal 1 deal_num1 - first deal to compare
 *     -deal 2 deal_num2 - second deal to compare
 *     -writenote - write the comparison note to the deals after comparison
 * </pre></blockquote></code>
 * 
 *  * @version 1.1  <br>
 * Date: 06/30/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Ability to compare parties of that type. <br>
 *	- added method setpPartyProfile( PartyProfile profile)<br>
 *	- added method getPartyProfile( )<br>
 *	- added method comparePartiesByType( int type )<br>
 *	- modified method comparePartyProfiles( PartyProfile p1, PartyProfile  p2) <br>
 *  
 *  Modified compareDeals method. This method compares DEAL.PRICINGPROFILEID <br>
 *   -   public void compareDeals()
 * @version 1.2  <br>
 * Date: 08/14/2008 <br>
 * Author: MCM Implementation Team <br>
 * Change:  <br>
 *	Ability to compare Components of Deals. <br>
 *	- added method doComponentListCompare( )<br>
 *	- added method compareComponentMortgages( )<br>
 *	- added method compareComponentLOCs( )<br>
 *  - added method compareComponentLoans( )<br>
 *  - added method compareComponentCreditCards( )<br>
 *  - added method compareComponentOverDrafts( )<br>
 *	- modified method compareDeals( ) to call doComponentListCompare( )<br>
 *   */
public class DealCompare implements Xc
{
	protected SysLogger logger;
	/**
	 * The two deals to compare.
	 */
	protected Deal deal1, deal2;

	/**
	 * Resource kit used throughout the class.
	 */
	protected SessionResourceKit srk = null;

	/**
	 * PartyProfile for lookups
	 */

	protected PartyProfile partyProfile;

	/**
	 * The total amount of differences at any given time in the comparison.
	 */
	protected int totalDifferences = 0;

	/**
	 * The maximum allowed differences before processing stops.  Setting this to
	 * Integer.MAX_VALUE allows unlimited changes.
	 * 
	 */
	//#DG600 protected int maxDifferences = 0;
	protected int maxDifferences = Integer.MAX_VALUE;
	final String lowAlfaVal = "zzz";//String.valueOf(Character.MAX_VALUE);

	/**
	 * The note to be added as a Deal Note.
	 */
	protected FmtblNoteList dealNotesList = new FmtblNoteList();	//#DG600

	/**
	 * The deal note header.
	 */
	protected FmtblNoteList dealNotesHdrList = new FmtblNoteList();	//#DG600

	/**
	 * Delimiter used for aligning text when producing a more readable format.
	 */
//	#DG600 public static final String DELIMITER = "�";

	/**
	 * New line character.
	 */
	//#DG600 public static final String NEW_LINE = "\n";
	public static final String NEW_LINE = System.getProperty("line.separator");

	/**
	 * Offset values used as tabstops when formatting the text into
	 * something more readable.
	 */
	//#DG600 protected int tabStops[] = { 35, 65, 95 };
	static final int tab1 = 35, tab2 = 65, tab3 = 95 ;
	static final String 
		UNDERLIN = "---------------------------------", 
		UNDERLINA = "----------------------------", 
		UNDERLINB = UNDERLINA, UNDERLINC = UNDERLINA;

	/**
	 * Constant used when the user wants the note to be formatted in a certain
	 * way.
	 */
	public static final int PARSE_TEXT = 0;
	public static final int PARSE_HTML = 1;

	/**
	 * Whether or not the warning has been displayed indicating the maximum
	 * differences has been reached.  This flag is used to prevent multiple
	 * warning notes from being written.
	 */
	protected boolean maxWarningDisplayed = false;

	//#DG600 protected EntityComparator comp = new EntityComparator();

	/**
	 * Constructor that takes the original and new deals for comparison and the
	 * maximum allowed differences.
	 */
	public DealCompare(Deal originalDeal, Deal newDeal, int maxDiff, SessionResourceKit srk)
	{
		deal1 = originalDeal;
		deal2 = newDeal;
		maxDifferences = maxDiff;
		this.srk = srk;
		//#DG600 logger = this.srk.getSysLogger();
		logger = new SysLogger(this, "DupChek");
		resetNote();
	}

	/**
	 * Constructor that takes the original and new deals for comparison and
	 * allows unlimited differences.
	 */
	public DealCompare(Deal originalDeal, Deal newDeal, SessionResourceKit srk)
	{
		//#DG600 this(originalDeal, newDeal, 0, srk);
		this(originalDeal, newDeal, Integer.MAX_VALUE, srk);
	}

	/**
	 * Writes the current deal note data to a deal note on <code>d</code>.
	 * This current code is nasty because it creates one note for each line that
	 * is in the comparison note.
	 *
	 * @param prefix a string prefix to prepend to the note
	 * @param d the <code>Deal</code> to write the note to
	 *
	 * @return whether the write was successful
	 */
	public boolean writeNoteToDeal(Formattable prefix, Deal d)
	{		
		if(totalDifferences <= 0)	   //#DG628 just to be on the safe side... 
			return false;
		try
		{
			DealNotes dealNota = new DealNotes(srk);

			int catID = Mc.DEAL_NOTES_CATEGORY_GENERAL;

			// could not reuse dealNotesHdrList cuz this method may be called 2x  :|
			// 			once for incoming and other for existing deal ...
			FmtblNoteList fmtblNoteList = new FmtblNoteList();
			if(prefix != null)
				fmtblNoteList.add(prefix);

			//#DG628 moved from 'resetNote' and rewritten
			if(dealNotesHdrList.size() <= 0) 
				buildHeader();
			
			fmtblNoteList.addAll(dealNotesHdrList);
			fmtblNoteList.addAll(dealNotesList);

			dealNota.create(d, catID, 0, fmtblNoteList);		//#DG634

			return true;
		}
		catch (Exception e)
		{
			logger.error("@writeNoteToDeal:");
			logger.error(e);
		}

		return false;
	}

	//#DG628 moved from 'resetNote' and rewritten	
	/**
	 * 
	 */
	private void buildHeader() {
		String deal1Str = deal1.getDealId() + ", " + deal1.getCopyId();
		String deal2Str = deal2.getDealId() + ", " + deal2.getCopyId();
		//noteHeader.addElement("Comparing Deal (" + str1 + ") and (" + str2 + ")...");
		Formattable nota = BXResources.newFormata(DUP_CHEK_COMPARINGDEAL_HD, deal1Str,deal2Str);
		dealNotesHdrList.addElement(nota);
		nota = newTabedLine(DUP_CHEK_ITEM_DESCR, 
				BXResources.newFormata(DUP_CHEK_DEAL_CP_TAG, deal1Str), 
				BXResources.newFormata(DUP_CHEK_DEAL_CP_TAG, deal2Str), 
				BXResources.newDPIGMsg(DUP_CHEK_DIFFERENCE));
		dealNotesHdrList.addElement(nota);

		nota = newTabedLine(BXResources.newStrFormata(UNDERLIN),
				UNDERLINA, UNDERLINB, UNDERLINC);
		dealNotesHdrList.addElement(nota);
	}

	/**
	 * Returns whether we have reached the maximum allowed differences.
	 */
	private boolean maximumReached()
	{
		//  Allow unlimited differences
		if (//#DG600 maxDifferences > 0 &&
				totalDifferences >= maxDifferences)
		{
			if (!maxWarningDisplayed)
			{
				//appendNote("***  Maximum allowed differences reached (" + maxDifferences + ")  ***");
				//#DG600 appendNote("*** " +  BXResources.getDocPrepIngMsg("MAXIMUM_ALLOWED_DIFFERENCES_REACHED", lang) +" (" + maxDifferences + ")  ***");
				dealNotesList.addElement(BXResources.newFormata(DUP_CHEK_MAXDIFFRCHD, maxDifferences));
				maxWarningDisplayed = true;
			}
			logger.trace("Maximum reached. Returning...");

			return true;
		}
		return false;
	}

	/**
	 * Resets the note and total difference count.
	 */
	public void reset()
	{
		resetNote();
		resetTotalDifferences();
	}

	/**
	 * To facilitate the reuse of this object, provide the user a way to reset
	 * the note.
	 */
	public void resetNote()
	{
		dealNotesList.clear();
		dealNotesHdrList.clear();

		//#DG628 moved ...
		//...String deal1Str = deal1.getDealId() + ", " + deal1.getCopyId();
	}

	/**
	 * To facilitate the reuse of this object, provide the user a way to reset
	 * the total differences.  Since the total differences is reset to zero,
	 * the flag that is used to determine whether the max warning has been
	 * displayed is reset as well.
	 */
	public void resetTotalDifferences()
	{
		maxWarningDisplayed = false;
		totalDifferences = 0;
	}

	/**
	 * To facilitate the reuse of this object, provide the user a way to
	 * set the maximum allowed differences.  Anything less than or equal to
	 * zero is considered unlimited.
	 *
	 * @param iDiff the new allowable maximum difference value
	 *
	 */
	public void setMaxDifferences(int iDiff)
	{
		maxWarningDisplayed = false;
		maxDifferences = iDiff;
	}

	/**
	 * To facilitate the reuse of this object, provide the user a way to
	 * set deal 1.
	 *
	 * @param d the deal to set the internal <code>deal1</code> to.
	 *
	 */
	public void setDeal1(Deal d)
	{
		deal1 = d;
	}

	/**
	 * To facilitate the reuse of this object, provide the user a way to
	 * set deal 2.
	 *
	 * @param d the deal to set the internal <code>deal2</code> to.
	 *
	 */
	public void setDeal2(Deal d)
	{
		deal2 = d;
	}


	/**
	 * Sets the PartyProfile entity for lookup
	 *
	 * @param profile Entity for lookup <br>
	 *
	 */

	protected void setPartyProfile( PartyProfile profile ) 
	{
		partyProfile = profile;
	}


	/**
	 * Gets PartyProfile entity for lookup
	 *
	 * @return PartyProfile Entity for lookup <br>
	 * @throws RemoteException Unable to instantiate entity 
	 * @throws FinderException Unable to find entity
	 *
	 */    
	protected PartyProfile getPartyProfile() throws RemoteException , FinderException 
	{
		if ( partyProfile == null )
			partyProfile = new PartyProfile( srk );
		return partyProfile;
	}

	/**
	 * The following fields are compared on each <code>Deal</code>:
	 * <p><code><blockquote><pre>
	 *    ACTUALPAYMENTTERM
	 *    AMORTIZATIONTERM
	 *    DEALTYPEID
	 *    ESTIMATEDCLOSINGDATE
	 *    EXISTINGLOANAMOUNT
	 *    LIENPOSITIONID
	 *    MIEXISTINGPOLICYNUMBER
	 *    MIINDICATORID
	 *    MIUPFRONT			Tracker #2070 - Sasha
	 *    MTGPRODID
	 *    NETLOANAMOUNT
	 *    NUMBEROFBORROWERS
	 *    NUMBEROFGUARANTORS
	 *    PAPURCHASEPRICE
	 *    PAYMENTTERMID
	 *    REFICURMORTGAGEHOLDER
	 *    REFICURRENTBAL
	 *    REFIIMPROVEMENTAMOUNT
	 *    REFIIMPROVEMENTDESC
	 *    REFIORIGMTGAMOUNT
	 *    REFIORIGPURCHASEPRICE
	 *    REFIPURPOSE
	 *    TOTALPURCHASEPRICE
	 *    --------------------------------> Rel 3.1 Sasha Start
	 *    PRODUCTTYPEID
	 *    SELFDIRECTEDRSSP
	 *    CMHCPRODUCTTRACKERIDENTIFIER
	 *    LOCREPAYMENTTYPEID
	 *    --------------------------------> Rel 3.1 Sasha End
	 *    ADMINFEE //Added for Iteration 4 on 26-Jul-2006
	 * </pre></blockquote></code>
	 * <p>
	 * After each comparison, we check to see if we are at our comparison limit.  If
	 * so, we immediately stop processing.  Users of this class can then call
	 * maximumReached() to determine the comparison process' success status.
	 * <p>
	 * TODO:  I have hard-coded the 'friendly' names for the deal columns we are
	 * interested in.  It would be alot better to extract these names from some
	 * source to make naming them dynamic.  This would also allow for locale
	 * support.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @version 1.2 14-Aug-2008 Added call for doComponentListCompare for XS_1.8
	 */
	public void compareDeals() throws RemoteException, FinderException
	{
		logger.trace(": Method compareDeals entered. totalDifferences="+totalDifferences);		//#DG634
		int tempTotal = totalDifferences;
		//int place = appendNote("* " +  BXResources.getDocPrepIngMsg("DEAL_DIFFERENCES",lang) +"  *");
		int hdPos = dealNotesList.size();
		Formattable nota;

		//  In case the crazy user tries to do a compare again...
		if (maximumReached())
			return;

		String val1, val2;
		double dval1, dval2;
		int ival1, ival2;

		//* ACTUALPAYMENTTERM
		logger.trace( ": Method compareDeals - ACTUALPAYMENTTERM");		//#DG634
    ival1 = deal1.getActualPaymentTerm();
		ival2 = deal2.getActualPaymentTerm();
		if (comparInt(DUP_CHEK_ACTUAL_PAYMENT_TERM, ival1, ival2))
			return;

		//* AMORTIZATIONTERM
		logger.trace( ": Method compareDeals - AMORTIZATIONTERM");		//#DG634
		ival1 = deal1.getAmortizationTerm();
		ival2 = deal2.getAmortizationTerm();
		if(comparYrMth(DUP_CHEK_AMORTIZATION_PERIOD, ival1, ival2))
			return;

		//* DEALTYPEID
		logger.trace( ": Method compareDeals - DEALTYPEID");		//#DG634
		ival1 = deal1.getDealTypeId();
		ival2 = deal2.getDealTypeId();
		/*#DG600
				if (ival1 != ival2) {
          val1  = BXResources.getPickListDescription("DealType",  int1, lang);
          val2  = BXResources.getPickListDescription("DealType",  int2, lang);
					appendNote(BXResources.getDocPrepIngMsg("DEAL_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_DEAL_TYPE, PKL_DEAL_TYPE, ival1, ival2))
			return;

		//* ESTIMATEDCLOSINGDATE
		logger.trace( ": Method compareDeals - ESTIMATEDCLOSINGDATE");		//#DG634
		Date tval1 = deal1.getEstimatedClosingDate();
		Date tval2 = deal2.getEstimatedClosingDate();
		/*#DG600
				if (!isEqual(tval1, tval2)) {
            val1 = date1 == null ? "" : df.format(date1);
            val2 = date2 == null ? "" : df.format(date2);
            appendNote(BXResources.getDocPrepIngMsg("ESTIMATED_CLOSING_DATE",lang) + DELIMITER +  val1 + DELIMITER + val2);        }
        if (maximumReached()) */
		if(comparDate(DUP_CHEK_ESTIMATED_CLOSING_DATE, tval1, tval2))
			return;

		//* EXISTINGLOANAMOUNT
		logger.trace( ": Method compareDeals - EXISTINGLOANAMOUNT");		//#DG634
		dval1 = deal1.getExistingLoanAmount();
		dval2 = deal2.getExistingLoanAmount();
		/*#DG600
				if (dval1 != dval2) {
            val1 = formatCurrency(vald1);
            val2 = formatCurrency(vald2);
            dDiff = vald2 - vald1;
            appendNote(BXResources.getDocPrepIngMsg("EXISTING_LOAN_AMOUNT",lang) + DELIMITER +
                       val1 + DELIMITER +
                       val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_EXISTING_LOAN_AMOUNT, dval1, dval2))
			return;

		//* LIENPOSITIONID
		logger.trace( ": Method compareDeals - LIENPOSITIONID");		//#DG634
		ival1 = deal1.getLienPositionId();
		ival2 = deal2.getLienPositionId();
		/*#DG600
				if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("LienPosition",   deal1.getLienPositionId(), lang);
            val2  = BXResources.getPickListDescription("LienPosition",   deal2.getLienPositionId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("CHARGE",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_CHARGE, PKL_LIEN_POSITION, ival1, ival2))
			return;

		//* MIEXISTINGPOLICYNUMBER
		logger.trace( ": Method compareDeals - MIEXISTINGPOLICYNUMBER");		//#DG634
		val1 = deal1.getMIExistingPolicyNumber();
		val2 = deal2.getMIExistingPolicyNumber();
		/*#DG600
				if (!isEqual(val1, val2)) {
            appendNote(BXResources.getDocPrepIngMsg("MI_EXISTING_POLICY_NUMBER",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached()) */
		if (comparStr(DUP_CHEK_MI_EXISTING_POL_NR, val1, val2))
			return;

		//* MIINDICATORID
		logger.trace( ": Method compareDeals - MIINDICATORID");		//#DG634
		ival1 = deal1.getMIIndicatorId();
		ival2 = deal2.getMIIndicatorId();
		/*#DG600
				if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("MIIndicator",   deal1.getMIIndicatorId(), lang);
            val2  = BXResources.getPickListDescription("MIIndicator",   deal2.getMIIndicatorId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("MORTGAGE_INSURANCE_INDICATOR",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached())*/
		if(comparPkL(DUP_CHEK_MORTGAGE_INSURANCE_IND, PKL_MIINDICATOR, ival1, ival2))
			return;

		// MIUPFRONT, Tracker #2707 - Sasha
		//-----------------------------------------------------------------
		logger.trace( ": Method compareDeals - MIUPFRONT");
		val1 = deal1.getMIUpfront();
		val2 = deal2.getMIUpfront();
		/*#DG600
				if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("MIUPFRONT",lang) + DELIMITER +
            		deal1.getMIUpfront() + DELIMITER +
            		deal2.getMIUpfront() + DELIMITER );
        }
        // -----------------------------------------------------------------
        //End of Tracker #2707
        if (maximumReached())*/
		if(comparStr(DUP_CHEK_MIUPFRONT, val1, val2))
			return;

		//#DG476
		//* LineOfBusinessId
		logger.trace( ":" +" Method compareDeals : LineOfBusinessId");
		ival1 = deal1.getLineOfBusinessId();
		ival2 = deal2.getLineOfBusinessId();
		/*#DG600
				if(ival1 != ival2) {
          val1  = BXResources.getPickListDescription("LineOfBusiness",   deal1.getLineOfBusinessId(), lang);
          val2  = BXResources.getPickListDescription("LineOfBusiness",   deal2.getLineOfBusinessId(), lang);
          appendNote(BXResources.getDocPrepIngMsg("LineOfBusiness",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached())*/
		if(comparPkL(DUP_CHEK_LINE_OF_BUSINESS, PKL_LINE_OF_BUSINESS, ival1, ival2))
			return;
		//#DG476 end

		//* MTGPRODID
		logger.trace( ": Method compareDeals - MTGPRODID");
		ival1 = deal1.getMtgProdId();
		ival2 = deal2.getMtgProdId();
		/* tkt FXP19081
		if (ival1 != ival2)		{
			/ *#DG600
            val1 = null;
            val2 = null;
            try {val1 = deal1.getMtgProd().getMtgProdName();}
            catch (Exception e) {}
            try {val2 = deal2.getMtgProd().getMtgProdName();}
            catch (Exception e) {}
            appendNote(BXResources.getDocPrepIngMsg("PRODUCT",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			MtgProd mtgProda = new MtgProd(srk,null);
			try {val1 = mtgProda.findByPrimaryKey(new MtgProdPK(ival1)).getMtgProdName();}
			catch (Exception e) {}
			try {val2 = mtgProda.findByPrimaryKey(new MtgProdPK(ival2)).getMtgProdName();}
			catch (Exception e) {}
			nota = newTabedLine(DUP_CHEK_PRODUCT, val1, val2, null);
			addFormtb(nota);
		}
		if (maximumReached()) */ 
		if(comparPkL(DUP_CHEK_PRODUCT, PKL_MTGPROD, ival1, ival2))
			return;

		//* NETLOANAMOUNT
		logger.trace( ": Method compareDeals - NETLOANAMOUNT");
		dval1 = deal1.getNetLoanAmount();
		dval2 = deal2.getNetLoanAmount();
		/*#DG600
				if (dval1 != dval2) {
            val1 = formatCurrency(deal1.getNetLoanAmount());
            val2 = formatCurrency(deal2.getNetLoanAmount());
            dDiff = deal2.getNetLoanAmount() - deal1.getNetLoanAmount();
            appendNote(BXResources.getDocPrepIngMsg("LOAN",lang) + DELIMITER + 
            		   val1 + DELIMITER + 
            		   val2 + DELIMITER + formatCurrency(dDiff));
          totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_LOAN, dval1, dval2))
			return;

		//* NUMBEROFBORROWERS
		int numBorow1 = 0;
		int numBorow2 = 0;
		int numGuarant1 = 0;
		int numGuarant2 = 0;
		logger.trace( ": Method compareDeals - NUMBEROFBORROWERS");
		try {
			/* #DG600 rewritten - let's take the opportunity and count guarantors 2
          List list = (List)deal1.getBorrowers();
            Iterator i = list.iterator();
            Borrower b;
            while (i.hasNext()) {
                b = (Borrower)i.next();
                if (b.getBorrowerTypeId() == Mc.BT_BORROWER)
                   numBorow1++;
            }
            list = (List)deal2.getBorrowers();
            i = list.iterator();
            while (i.hasNext()) {
                b = (Borrower)i.next();
                if (b.getBorrowerTypeId() == Mc.BT_BORROWER)
                   numBorow2++;
            }*/
			Collection<Borrower> borows;
			borows = deal1.getBorrowers();
			for (Borrower b: borows) {
				switch (b.getBorrowerTypeId()) {
				case Mc.BT_BORROWER: {
					numBorow1++;
					break;
				}
				case Mc.BT_GUARANTOR: {
					numGuarant1++;
					break;
				}
				}
			}
			borows = deal2.getBorrowers();
			for (Borrower b: borows) {
				switch (b.getBorrowerTypeId()) {
				case Mc.BT_BORROWER: {
					numBorow2++;
					break;
				}
				case Mc.BT_GUARANTOR: {
					numGuarant2++;
					break;
				}
				}
			}
		}
		catch (Exception e) {}

		/*#DG600
        if (numBorow1 != numBorow2) {
            appendNote( BXResources.getDocPrepIngMsg("NUMBER_OF_BORROWERS",lang) + DELIMITER + 
            		    numBorow1 + DELIMITER + 
            		    numBorow2 +  DELIMITER + (numBorow2 - numBorow1));
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_NR_BOROWERS, numBorow1, numBorow2))
			return;

		//* NUMBEROFGUARANTORS
		//numBorow1 = 0;
		//numBorow2 = 0;
		logger.trace( ": Method compareDeals - NUMBEROFGUARANTORS");
		/*#DG600
        try {
            List list = (List)deal1.getBorrowers();
            Iterator i = list.iterator();
            Borrower b;

            while (i.hasNext()) {
                b = (Borrower)i.next();
                if (b.getBorrowerTypeId() == Mc.BT_GUARANTOR) 
                   numBorow1++;
            }

            list = (List)deal2.getBorrowers();
            i = list.iterator();

            while (i.hasNext()) {
                b = (Borrower)i.next();
                if (b.getBorrowerTypeId() == Mc.BT_GUARANTOR)
                   numBorow2++;
            }
        }
        catch (Exception e) {}
        if (numBorow1 != numBorow2) {
            appendNote( BXResources.getDocPrepIngMsg("NUMBER_OF_GUARANTORS",lang) + DELIMITER + 
            		    numBorow1 + DELIMITER + 
            		    numBorow2 + DELIMITER + (numBorow2 - numBorow1));
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_NR_GUARANTS, numGuarant1, numGuarant2))
			return;

		//* PAPURCHASEPRICE
		logger.trace( ": Method compareDeals - PAPURCHASEPRICE");
		dval1 = deal1.getPAPurchasePrice();
		dval2 = deal2.getPAPurchasePrice();
		/*#DG600
        if (dval1 != dval2) {
            val1 = formatCurrency(deal1.getPAPurchasePrice());
            val2 = formatCurrency(deal2.getPAPurchasePrice());
            dDiff = deal2.getPAPurchasePrice() - deal1.getPAPurchasePrice();
            appendNote(BXResources.getDocPrepIngMsg("PRE-APP_ESTIMATED_PURCHASE_PRICE",lang) + DELIMITER +
                       val1 + DELIMITER +
                       val2 + DELIMITER + formatCurrency(dDiff));
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PRE_AP_EST_PUR_PRIC, dval1, dval2))
			return;

		//* PAYMENTTERMID
		logger.trace( ": Method compareDeals - PAYMENTTERMID");
		ival1 = deal1.getPaymentTermId();
		ival2 = deal2.getPaymentTermId();
		/*#DG600
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("PaymentTerm", deal1.getPaymentTermId(), lang);
            val2  = BXResources.getPickListDescription("PaymentTerm", deal2.getPaymentTermId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("PAYMENT_TERM_DESCRIPTION",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PAYMENT_TERM_DESCR, PKL_PAYMENTTERM, ival1, ival2))
			return;

		//* REFICURMORTGAGEHOLDER
		logger.trace( ": Method compareDeals - REFICURMORTGAGEHOLDER");
		val1 = deal1.getRefiCurMortgageHolder();
		val2 = deal2.getRefiCurMortgageHolder();
		/*#DG600
				if (!isEqual(val1, val2)) {
            appendNote(BXResources.getDocPrepIngMsg("MORTGAGE_HOLDER",lang) + DELIMITER + 
            		deal1.getRefiCurMortgageHolder() + DELIMITER + 
            		deal2.getRefiCurMortgageHolder());
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_MORTGAGE_HOLDER, val1, val2))
			return;

		//* REFICURRENTBAL
		logger.trace( ": Method compareDeals - REFICURRENTBAL");
		dval1 = deal1.getRefiCurrentBal();
		dval2 = deal2.getRefiCurrentBal();
		/*#DG600
        if (dval1 != dval2) {
            val1 = formatCurrency(deal1.getRefiCurrentBal());
            val2 = formatCurrency(deal2.getRefiCurrentBal());
            dDiff = deal2.getRefiCurrentBal() - deal1.getRefiCurrentBal();
            appendNote( BXResources.getDocPrepIngMsg("OUTSTANDING_MTG_AMOUNT",lang) + DELIMITER + 
            		    val1 + DELIMITER + 
            		    val2 + DELIMITER + formatCurrency(dDiff));
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_OUTS_MTG_AMT, dval1, dval2))
			return;

		//* REFIIMPROVEMENTAMOUNT
		logger.trace( ": Method compareDeals - REFIIMPROVEMENTAMOUNT");
		dval1 = deal1.getRefiImprovementAmount();
		dval2 = deal2.getRefiImprovementAmount();
		/*#DG600
        if(dval1 != dval2)
  			    val1 = formatCurrency(deal1.getRefiImprovementAmount());
  			    val2 = formatCurrency(deal2.getRefiImprovementAmount());
  			    dDiff = deal2.getRefiImprovementAmount() - deal1.getRefiImprovementAmount();
  			    appendNote( BXResources.getDocPrepIngMsg("VALUE_OF_IMPROVEMENTS",lang) + DELIMITER + 
  			    		    val1 + DELIMITER + 
  			    		    val2 + DELIMITER + formatCurrency(dDiff));
  				totalDifferences++;
  			}
    		if (maximumReached()) */        
		if(comparDbl(DUP_CHEK_VALUE_IMPROVEM, dval1, dval2))
			return;

		//* REFIIMPROVEMENTDESC
		logger.trace( ": Method compareDeals - REFIIMPROVEMENTDESC");
		val1 = deal1.getRefiImprovementsDesc();
		val2 = deal2.getRefiImprovementsDesc();
		/*#DG600
        if (!isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("IMPROVEMENTS",lang) + DELIMITER + 
            		    deal1.getRefiImprovementsDesc() + DELIMITER + 
            		    deal2.getRefiImprovementsDesc());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_IMPROVEMENTS, val1, val2))
			return;

		//* REFIORIGMTGAMOUNT
		logger.trace( ": Method compareDeals - REFIORIGMTGAMOUNT");
		dval1 = deal1.getRefiOrigMtgAmount();
		dval2 = deal2.getRefiOrigMtgAmount();
		/*#DG600
        if (dval1 != dval2) {
            val1 = formatCurrency(deal1.getRefiOrigMtgAmount());
            val2 = formatCurrency(deal2.getRefiOrigMtgAmount());
            dDiff = deal2.getRefiOrigMtgAmount() - deal1.getRefiOrigMtgAmount();
            appendNote( BXResources.getDocPrepIngMsg("ORIGINAL_MTG_AMOUNT",lang) + DELIMITER + 
            		    val1 + DELIMITER + 
            		    val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_ORIG_MTG_AMT, dval1, dval2))
			return;

		//* REFIORIGPURCHASEPRICE
		logger.trace( ": Method compareDeals - REFIORIGPURCHASEPRICE");
		dval1 = deal1.getRefiOrigPurchasePrice();
		dval2 = deal2.getRefiOrigPurchasePrice();
		/*#DG600
        if (dval1 != dval2) {
            val1 = formatCurrency(deal1.getRefiOrigPurchasePrice());
            val2 = formatCurrency(deal2.getRefiOrigPurchasePrice());
            dDiff = deal2.getRefiOrigPurchasePrice() - deal1.getRefiOrigPurchasePrice();
            appendNote( BXResources.getDocPrepIngMsg("ORIGINAL_PURCHASE_PRICE",lang) + DELIMITER +
            		    val1 + DELIMITER + 
            		    val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_ORIG_PURC_PRICE, dval1, dval2))
			return;

		//* REFIPURPOSE
		logger.trace( ": Method compareDeals - REFIPURPOSE");
		val1 = deal1.getRefiPurpose();
		val2 = deal2.getRefiPurpose();
		/*#DG600
       	if (!isEqual(deal1.getRefiPurpose(), deal2.getRefiPurpose()))
        {
            appendNote(BXResources.getDocPrepIngMsg("PURPOSE",lang) + DELIMITER +
                       deal1.getRefiPurpose() + DELIMITER +
                       deal2.getRefiPurpose());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PURPOSE, val1, val2))
			return;

		//* TOTALPURCHASEPRICE
		logger.trace( ": Method compareDeals - TOTALPURCHASEPRICE");
		dval1 = deal1.getTotalPurchasePrice();
		dval2 = deal2.getTotalPurchasePrice();
		/*#DG600
        if (dval1 != dval2) {
            val1 = formatCurrency(deal1.getTotalPurchasePrice());
            val2 = formatCurrency(deal2.getTotalPurchasePrice());
            dDiff = deal2.getTotalPurchasePrice() - deal1.getTotalPurchasePrice();
            appendNote(BXResources.getDocPrepIngMsg("TOTAL_PURCHASE_PRICE",lang) + DELIMITER +
                       val1 + DELIMITER +
                       val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_TOTAL_PURC_PRICE, dval1, dval2))
			return;

		// MIUPFRONT, Tracker #2707 - Sasha
		//-----------------------------------------------------------------
		logger.trace( ": Method compareDeals : MIUPFRONT");
		val1 = deal1.getMIUpfront();
		val2 = deal2.getMIUpfront();
		/*#DG600
        if (!isEqual(val1, val2)) {
            appendNote(BXResources.getDocPrepIngMsg("MI_UPFRONT",lang) + DELIMITER +
            		val1 + DELIMITER +
            		val1 + DELIMITER );
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_MI_UPFRONT, val1, val2))
			return;        
		// -----------------------------------------------------------------
		//End of Tracker #2707

		// Rel 3.1 Sasha
		// -----------------------------------------------------------------
		//* PRODUCTTYPEID
		logger.trace( ": Method compareDeals - PRODUCTTYPEID");
		ival1 = deal1.getProductTypeId();
		ival2 = deal2.getProductTypeId();
		/*#DG600 
        if(ival1 != ival2) {
        	val1 = BXResources.getPickListDescription("ProductType",deal1.getProductTypeId(), lang);
        	val2 = BXResources.getPickListDescription("ProductType",deal2.getProductTypeId(), lang);
        	appendNote( BXResources.getDocPrepIngMsg("PRODUCT_TYPE_ID", lang) + DELIMITER + val1 + DELIMITER + val2);       
        }
        if( maximumReached()) */
		if(comparPkL(DUP_CHEK_PRODUCT_TYPE_ID, PKL_PRODUCT_TYPE, ival1, ival2))
			return;

		//* SELFDIRECTEDRRSP
		logger.trace( ": Method compareDeals - SELFDIRECTEDRRSP");
		val1 = deal1.getSelfDirectedRRSP();
		val2 = deal2.getSelfDirectedRRSP();
		/*#DG600 
        if(!isEqual(val1, val2)) {
        	appendNote( BXResources.getDocPrepIngMsg("SELF_DIRECTED_RRSP", lang) + DELIMITER +
        			    deal1.getSelfDirectedRRSP() + DELIMITER +
        			    deal2.getSelfDirectedRRSP());
        	totalDifferences++;
        }
        if( maximumReached()) */
		if(comparStr(DUP_CHEK_SELF_DIRECTED_RRSP, val1, val2))
			return;

		//* CMHCPRODUCTTRACKERIDENTIFIER
		logger.trace( ": Method compareDeals - CMHCPRODUCTTRACKERIDENTIFIER");
		val1 = deal1.getCmhcProductTrackerIdentifier();
		val2 = deal2.getCmhcProductTrackerIdentifier();
		/*#DG600 
        if(deal1.getCmhcProductTrackerIdentifier() != deal2.getCmhcProductTrackerIdentifier()) {
        	appendNote( BXResources.getDocPrepIngMsg("CMHC_PRODUCT_TRACKER_IDENTIFIER", lang) + DELIMITER +
        			    deal1.getCmhcProductTrackerIdentifier() + DELIMITER +
        			    deal2.getCmhcProductTrackerIdentifier());
        	totalDifferences++;
        }
        if( maximumReached()) */
		if(comparStr(DUP_CHEK_CMHC_PROD_TRAK_ID, val1, val2))
			return;


		//* LOCREPAYMENTTYPEID
		logger.trace( ": Method compareDeals - LOCREPAYMENTTYPEID");
		ival1 = deal1.getLocRepaymentTypeId();
		ival2 = deal2.getLocRepaymentTypeId();
		/*#DG600 
        if(ival1 != ival2) {
        	val1 = BXResources.getPickListDescription("LocRepaymentType", deal1.getLocRepaymentTypeId(), lang);
        	val2 = BXResources.getPickListDescription("LocRepaymentType", deal2.getLocRepaymentTypeId(), lang);
        	appendNote( BXResources.getDocPrepIngMsg("LOC_REPAYMENT_TYPE_ID", lang) + DELIMITER + val1 + DELIMITER + val2);
        	totalDifferences++;
        }
        if( maximumReached()) */
		if(comparPkL(DUP_CHEK_LOC_REPAY_TYPE_ID, PKL_LOC_REPAYMENT_TYPE, ival1, ival2))
			return;
		// -----------------------------------------------------------------
		// End of Rel 3.1 Sasha        

		//* RATES
		logger.trace( ":" +" Method compareDeals - compareRates");
		if(compareRates())
			//if (maximumReached()) 
			return;

//		//  If no changes were made then remove label.
//		if (tempTotal == totalDifferences)
//		dealNotesList.remove(place);
		//  If any changes add a label
		if (tempTotal < totalDifferences) {
			nota = BXResources.newFormata(DUP_CHEK_DEAL_DIFFERENCES);
			dealNotesList.insertElementAt(nota, hdPos);
		}

		//*
		//* DOWNPAYMENTSOURCE
		//* -----------------
		//*
		logger.trace( ": Method compareDeals : DOWNPAYMENTSOURCE");
		try
		{
			//#DG600 List l1 = (List)deal1.getDownPaymentSources();
			//#DG600 List l2 = (List)deal2.getDownPaymentSources();
			List<DownPaymentSource> dp1 = (List<DownPaymentSource>) deal1.getDownPaymentSources();
			List<DownPaymentSource> dp2 = (List<DownPaymentSource>) deal2.getDownPaymentSources();
			doDownPaymentSourceListCompare(dp1, dp2);
		}
		catch (Exception e) {}

		//*
		//* BORROWERS
		//* ---------
		//*
		logger.trace( ":" +" Method compareDeals : BORROWERS");
		try
		{
			//#DG600 List l1 = (List)deal1.getBorrowers();
			//#DG600 List l2 = (List)deal2.getBorrowers();
			List<Borrower> l1 = (List<Borrower>)deal1.getBorrowers();
			List<Borrower> l2 = (List<Borrower>)deal2.getBorrowers();
			doBorrowerListCompare(l1, l2);
		}
		catch (Exception e) {}

		// ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
		//		 ***** Change by MCM Impl. Team - XS_1.8- Start *****//
		logger.trace( ":" +" Method compareDeals : Components");
		try
		{
			List<Component> lComp11 = (List<Component>)deal1.getComponents();
			List<Component> lComp12 = (List<Component>)deal2.getComponents();
			doComponentListCompare(lComp11, lComp12);
		}
		catch (Exception e) {
		  logger.trace(":"+ e.getMessage());
		}
		//		 ***** Change by MCM Impl. Team - XS_1.8 - End *****//
		// * CASHBACKAMOUNT
		logger.trace( ": Method compareDeals : 27");
		dval1 = deal1.getCashBackAmount();
		dval2 = deal2.getCashBackAmount();
		/*#DG600 			
		if(dval1 != dval2) {
			val1 = formatCurrency(deal1.getCashBackAmount());
			val2 = formatCurrency(deal2.getCashBackAmount());
			dDiff = deal2.getCashBackAmount() - deal1.getCashBackAmount();
			appendNote(BXResources.getDocPrepIngMsg("CASH_BACK_AMOUNT", lang)
					+ DELIMITER + val1 + DELIMITER + val2 + DELIMITER
					+ formatCurrency(dDiff));

			totalDifferences++;
    }
		if (maximumReached())*/
		if(comparDbl(DUP_CHEK_CASH_BACK_AMOUNT, dval1, dval2))
			return;

		// * CASHBACKPERCENT
		logger.trace( ": Method compareDeals : 28");
		dval1 = deal1.getCashBackPercent();
		dval2 = deal2.getCashBackPercent();
		/*#DG600 
		if (dval1 != dval2) {
			val1 = formatCurrency(deal1.getCashBackPercent());
			val2 = formatCurrency(deal2.getCashBackPercent());
			dDiff = deal2.getCashBackPercent() - deal1.getCashBackPercent();
			appendNote(BXResources.getDocPrepIngMsg("CASH_BACK_PERCENT", lang)
					+ DELIMITER + val1 + DELIMITER + val2 + DELIMITER
					+ formatCurrency(dDiff));

			totalDifferences++;
		}
		if (maximumReached())*/
		//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
		//if(comparDbl(DUP_CHEK_CASH_BACK_PERCENT, dval1, dval2))
		if(comparDbl(DUP_CHEK_CASH_BACK_PERCENT, RATIO_FORMAT, dval1, dval2))
			return;

		// * PROGRESSADVANCE
		logger.trace( ":Method compareDeals : 29");
		val1 = deal1.getProgressAdvance();
		val2 = deal2.getProgressAdvance();	//#DG632
		/*#DG600 
		if (!isEqual(val1, val2)) {
			appendNote(BXResources.getDocPrepIngMsg("PROGRESS_ADVANCE", lang)
					+ DELIMITER + deal1.getProgressAdvance() + DELIMITER
					+ deal2.getProgressAdvance());

			totalDifferences++;
		}
		if (maximumReached())*/
		if(comparStr(DUP_CHEK_PROGRESS_ADVANCE, val1, val2))
			return;

		/**
		 * National Bank Implementation /PP
		 * 
		 * @date 03-Aug-07
		 * @author NBC/PP Implementation Team
		 * @iteration:4
		 * @story Name:Update Deal Compare with Admin Fee
		 * @version 1.2
		 * @return void
		 * @param srk
		 *            as SessionResourceKit object The following code has been
		 *            added to compare the AdminFee associated with the Deal
		 *            entity
		 * @increments the totalDifferences and appends deal notes if there is
		 *             difference in AdminFee
		 */

		// ***** Change by NBC/PP Implementation Team -version 1.2- Start
		// *****//
		// * NBC ADMIN FEE

		logger.trace( ":Method compareDeals : 30");

		try {

			EscrowPayment ep = new EscrowPayment(srk, null);
			/*#DG600 rewritten 
			int typeId1 = ep.getEscrowTypeId(deal1.getDealId(), deal1.getCopyId());
			int typeId2 = ep.getEscrowTypeId(deal2.getDealId(), deal2.getCopyId());
			if (typeId1 == Mc.ESCROWTYPE_ADMIN_FEE && typeId2 == Mc.ESCROWTYPE_ADMIN_FEE) {
				if (deal1.getAdminFeeAmount(srk) != deal2
						.getAdminFeeAmount(srk)) {
					val1 = formatCurrency(deal1.getAdminFeeAmount(srk));
					val2 = formatCurrency(deal2.getAdminFeeAmount(srk));
					dDiff = deal2.getAdminFeeAmount(srk)
							- deal1.getAdminFeeAmount(srk);

					appendNote(BXResources.getDocPrepIngMsg("NBC_ADMIN_FEE",
							lang)
							+ DELIMITER
							+ val1
							+ DELIMITER
							+ val2
							+ DELIMITER
							+ formatCurrency(dDiff));

					totalDifferences++;
				}
				if (maximumReached())
					return;
			}*/
			Collection eps;

			eps = ep.findByDealAndType(new DealPK(deal1.getDealId(), deal1.getCopyId()), Mc.ESCROWTYPE_ADMIN_FEE);
			dval1 = eps == null || eps.isEmpty()? 0: 
				((EscrowPayment)eps.iterator().next()).getEscrowPaymentAmount();

			eps = ep.findByDealAndType(new DealPK(deal2.getDealId(), deal2.getCopyId()), Mc.ESCROWTYPE_ADMIN_FEE);
			dval2 = eps == null || eps.isEmpty()? 0: 
				((EscrowPayment)eps.iterator().next()).getEscrowPaymentAmount();

			if(comparDbl(DUP_CHEK_ESCROW_AMOUNT, dval1, dval2))
				return;
		} catch (Exception e) {
			logger.error(":Error comparing the Admin Fee value for the reason : "+ e);		//#DG634
		}

		//**************** Defect #1205 fix 17-Oct-2006  Start***************************//

		//* AFFILIATIONPROGRAM
		logger.trace( ": Method compareDeals - AFFILIATIONPROGRAMID");
		ival1 = deal1.getAffiliationProgramId();
		ival2 = deal2.getAffiliationProgramId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("AFFILIATIONPROGRAM", deal1.getAffiliationProgramId(), lang);
            val2  = BXResources.getPickListDescription("AFFILIATIONPROGRAM", deal2.getAffiliationProgramId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("AFFILIATIONPROGRAM",lang) + DELIMITER + val1 + DELIMITER + val2);

            totalDifferences++;
        }
        if (maximumReached())*/
		if(comparPkL(DUP_CHEK_AFFILIATIONPROGRAM, PKL_AFFILIATIONPROGRAM, ival1, ival2))
			return;

		//** DISCOUNT        
		logger.trace( ": Method compareDeals : DISCOUNT");
		dval1 = deal1.getDiscount();
		dval2 = deal2.getDiscount();
		/*#DG600 
				if (dval1 != dval2) {
					 double dis1 = deal1.getDiscount();
					 double dis2 = deal2.getDiscount();
					 dDiff = dis2 - dis1;
					 appendNote(BXResources.getDocPrepIngMsg("DISCOUNT", lang)
							+ DELIMITER + dis1 + DELIMITER + dis2 + DELIMITER
							+ dDiff);

					 totalDifferences++;
				}
				if (maximumReached())*/
		if(comparDbl(DUP_CHEK_DISCOUNT, RATIO_FORMAT, dval1, dval2))
			return;

		//* BUYDOWNRATE	
		logger.trace( ":"
				+ " Method compareDeals : BUYDOWNRATE");
		dval1 = deal1.getBuydownRate();
		dval2 = deal2.getBuydownRate();
		/*#DG600 
				if (dval1 != dval2) {
					double bdr1 = deal1.getBuydownRate();
					double bdr2 = deal2.getBuydownRate();
					dDiff = bdr2 - bdr1;
					appendNote(BXResources.getDocPrepIngMsg("BUYDOWNRATE", lang)
							+ DELIMITER + bdr1 + DELIMITER + bdr2 + DELIMITER
							+ dDiff);

					totalDifferences++;
				}
				if (maximumReached())*/
		if(comparDbl(DUP_CHEK_BUYDOWNRATE, RATIO_FORMAT, dval1, dval2))
			return;


		//* PREMIUM

		logger.trace( ":"
				+ " Method compareDeals : PREMIUM");
		dval1 = deal1.getPremium();
		dval2 = deal2.getPremium();
		/*#DG600 
				if (dval1 != dval2) {
					double prm1 = deal1.getPremium();
					double prm2 = deal2.getPremium();
					dDiff = prm2 - prm1;
					appendNote(BXResources.getDocPrepIngMsg("PREMIUM", lang)
							+ DELIMITER + prm1 + DELIMITER + prm2 + DELIMITER
							+ dDiff);

					totalDifferences++;
				}
				if (maximumReached())*/
		//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
		//if(comparDbl(DUP_CHEK_PREMIUM, dval1, dval2))
		if(comparDbl(DUP_CHEK_PREMIUM, RATIO_FORMAT, dval1, dval2))
			return;

		//* RATEGUARANTEEPERIOD

		logger.trace( ":"
				+ " Method compareDeals : RATEGUARANTEEPERIOD");
		ival1 = deal1.getRateGuaranteePeriod();
		ival2 = deal2.getRateGuaranteePeriod();
		/*#DG600 
				if (ival1 != ival2) {
					int rgp1 = deal1.getRateGuaranteePeriod();
					int rgp2 = deal2.getRateGuaranteePeriod();
					dDiff = rgp2 - rgp1;
					appendNote(BXResources.getDocPrepIngMsg("RATEGUARANTEEPERIOD", lang)
							+ DELIMITER + rgp1 + DELIMITER + rgp2 + DELIMITER
							+ dDiff);
					totalDifferences++;
				}
				if (maximumReached())*/
		if(comparInt(DUP_CHEK_RATEGUARANTEPERIOD, ival1, ival2))
			return;


		//**************** Defect #1205 fix 17-Oct-2006  End**************************//

		// ***** Change by NBC/PP Implementation Team -version 1.2- End *****//
        
        // ***** FFATE - Start *****//
        //* FinancingWaiverDate
        logger.trace( ": Method compareDeals - FinancingWaiverDate");
        tval1 = deal1.getFinancingWaiverDate();
        tval2 = deal2.getFinancingWaiverDate();

        if(comparDate(DUP_CHEK_FINANCING_WAIVER, tval1, tval2))
            return;

        // ***** FFATE - End *****//
		logger.trace( ": Method compareDeals finished ");
	}

	/**
	 * Compares parties with the same party type. 
	 *
	 * @param  partyTypeId<br>
	 *   Id to use to compare PartyProfiles<br>
	 *  
	 */


	public void comparePartiesByType( int partyTypeId ) {
		if ( maximumReached( ) ) 
		{
			logger.trace("DealCompare@comparePartiesByType(int): maximum reached, return");
			return; 
		}
		logger.trace("DealCompare@comparePartiesByType( int ) comparing party profiles of type " + partyTypeId );


		// get parties with specified type for both deals
		try 
		{
			PartyProfile partyProfile = getPartyProfile( );
			// get existing and new 
			List currentPartiesList = ( List ) partyProfile.findByDealAndType( deal1.getDealId( ) , partyTypeId );         
			List newPartiesList = ( List ) partyProfile.findByDealAndType( deal2.getDealId( ) , partyTypeId );            
			// if no parties set to null
			PartyProfile currentPartyProfile = currentPartiesList.size( ) > 0 ? ( PartyProfile ) currentPartiesList.get( 0 ) : null;
			PartyProfile newPartyProfile = newPartiesList.size( ) > 0 ? ( PartyProfile ) newPartiesList.get( 0 ) : null;	
			// if parties are exactly the same, no comparison
			if ( currentPartyProfile != null && newPartyProfile != null && currentPartyProfile.getPartyProfileId( ) == newPartyProfile.getPartyProfileId( ) ) 
			{
				logger.trace( "DealCompare@comparePartiesByType( int ): ProfileId(s) for parties type " + partyTypeId + " are the same" );
			}	// if none found, no comparison
			else if ( currentPartyProfile == null && newPartyProfile == null ) 
			{
				logger.trace( "DealCompare@comparePartiesByType( int ): No Party Profiles found to compare." );
			}
			// if either one was found only, create note
			else 
			{		
				comparePartyProfiles( currentPartyProfile , newPartyProfile );
				totalDifferences++;
			}			
		} 
		catch ( Exception e ) {
			logger.trace( "Failed to get parties in comparePartiesByType( int ) for deals = " + deal1.getDealId( ) + ",  " + deal2.getDealId( ) );
			logger.trace( StringUtil.stack2string( e ) );
		}

		logger.trace( "@comparePartiesByType( int ) end");
	}


	/**
	 * Compares key fields in a pair of Properties.
	 * <P>
	 * The following fields are compared on each <code>Property</code>:
	 * <p><code><blockquote><pre>
	 *    ACTUALAPPRAISALVALUE
	 *    APPRAISALDATEACT
	 *    BUILDERNAME
	 *    DWELLINGSTYLEID
	 *    DWELLINGTYPEID
	 *    EQUITYAVAILABLE
	 *    GARAGESIZEID
	 *    GARAGETYPEID
	 *    HEATTYPEID
	 *    INSULATEDWITHUFFI
	 *    LANDVALUE
	 *    LEGALLINE1
	 *    LEGALLINE2
	 *    LEGALLINE3
	 *    LIVINGSPACE
	 *    LIVINGSPACEUNITOFMEASUREID
	 *    LOTSIZE
	 *    LOTSIZEDEPTH
	 *    LOTSIZEFRONTAGE
	 *    LOTSIZEUNITOFMEASUREID
	 *    MLSLISTINGFLAG
	 *    MONTHLYCONDOFEES
	 *    MUNICIPALITYID
	 *    NEWCONSTRUCTIONID
	 *    NUMBEROFBEDROOMS
	 *    NUMBEROFUNITS
	 *    OCCUPANCYTYPEID
	 *    PROPERTYADDRESSLINE2
	 *    PROPERTYCITY
	 *    PROPERTYLOCATIONID
	 *    PROPERTYPOSTALFSA
	 *    PROPERTYPOSTALLDU
	 *    PROPERTYSTREETNAME
	 *    PROPERTYSTREETNUMBER
	 *    PROPERTYTYPEID
	 *    PROPERTYUSAGEID
	 *    PROVINCEID
	 *    PURCHASEPRICE
	 *    SEWAGETYPEID
	 *    STREETDIRECTIONID
	 *    STREETTYPEID
	 *    STRUCTUREAGE
	 *    UNITNUMBER
	 *    WATERTYPEID
	 *    YEARBUILT
	 *    ZONING
	 *    ----------------------------------------- Rel 3.1 Sasha
	 *    TENURETYPEID
	 *    MIENERGYEFFICIENCY
	 *    ONRESERVETRUSTAGREEMENTNUMBER
	 *    SUBDIVISIONDISCOUNT
	 *    ----------------------------------------- End of Rel 3.1 Sasha
	 *    
	 * </pre></blockquote></code>
	 */
	public void compareProperties(Property prop1, Property prop2)
	{
		logger.trace( ": Method compareProperties entered ");
		int tempTotal = totalDifferences;

		//#DG600 int place = appendNote("* " + BXResources.getDocPrepIngMsg("PROPERTY_DIFFERENCES",lang) + " *");
		int hdPos = dealNotesList.size();

		if (maximumReached()) return;

		String val1, val2;
		//#DG600 double dDiff;
		//#DG600 int iDiff;
		Formattable nota;

		//* ACTUALAPPRAISALVALUE
		logger.trace( ": Method compareProperties - ACTUALAPPRAISALVALUE");
		double dval1 = prop1.getActualAppraisalValue();
		double dval2 = prop2.getActualAppraisalValue();
		/*#DG600 
        if (dval1 != dval2) {
            val1 = formatCurrency(prop1.getActualAppraisalValue());
            val2 = formatCurrency(prop2.getActualAppraisalValue());
            dDiff = prop2.getActualAppraisalValue() - prop1.getActualAppraisalValue();
            appendNote(BXResources.getDocPrepIngMsg("ACTUAL_APPRAISED_VALUE",lang) + DELIMITER +
                       val1 + DELIMITER +
                       val2 + DELIMITER +
                       formatCurrency(dDiff));

            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_ACT_APRAISED_VAL, dval1, dval2))
			return;

		//* APPRAISALDATEACT
		logger.trace(": Method compareProperties - APPRAISALDATEACT");
		final Date tval1 = prop1.getAppraisalDateAct();
		final Date tval2 = prop2.getAppraisalDateAct();
		/* #DG658
		if (!isEqual(tval1, tval2)) {
			/ *#DG600
            val1 = date1 == null ? "" : df.format(date1);
            val2 = date2 == null ? "" : df.format(date2);
            appendNote(BXResources.getDocPrepIngMsg("APPRAISAL_DATE",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			nota = newTabedLine(DUP_CHEK_APPRAISAL_DATE, 
					tval1 == null? "": BXResources.newFormata(DATE_FORMAT, tval1),		  
					tval2 == null? "": BXResources.newFormata(DATE_FORMAT, tval2), null);
			addFormtb(nota);
			totalDifferences++;
		}
		if (maximumReached()) */ 
		if(comparDate(DUP_CHEK_APPRAISAL_DATE, tval1, tval2))
			return;

		//* BUILDERNAME
		logger.trace(": Method compareProperties - BUILDERNAME");
		val1 = prop1.getBuilderName();
		val2 = prop2.getBuilderName();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            appendNote(BXResources.getDocPrepIngMsg("BUILDER_NAME",lang) + DELIMITER +
                       prop1.getBuilderName() + DELIMITER +
                       prop2.getBuilderName());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_BUILDER_NAME, val1, val2))
			return;

		//* DWELLINGSTYLEID
		logger.trace( ": Method compareProperties - DWELLINGSTYLEID");
		int ival1 = prop1.getDwellingStyleId();
		int ival2 = prop2.getDwellingStyleId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("DwellingStyle", prop1.getDwellingStyleId(), lang);
            val2  = BXResources.getPickListDescription("DwellingStyle", prop2.getDwellingStyleId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("DWELLING_STYLE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_DWELLING_STYLE, PKL_DWELLING_STYLE, ival1, ival2))
			return;

		//* DWELLINGTYPEID
		logger.trace(": Method compareProperties - DWELLINGTYPEID");
		ival1 = prop1.getDwellingTypeId();
		ival2 = prop2.getDwellingTypeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("DwellingType", prop1.getDwellingTypeId(), lang);
            val2  = BXResources.getPickListDescription("DwellingType", prop2.getDwellingTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("DWELLING_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_DWELLING_TYPE, PKL_DWELLING_TYPE, ival1, ival2))
			return;

		//* EQUITYAVAILABLE
		logger.trace(": Method compareProperties - EQUITYAVAILABLE");
		dval1 = prop1.getEquityAvailable();
		dval2 = prop2.getEquityAvailable();
		/*#DG600 
        if (dval1 != dval2) {
            val1 = formatCurrency(prop1.getEquityAvailable());
            val2 = formatCurrency(prop2.getEquityAvailable());
            dDiff = prop2.getEquityAvailable() - prop1.getEquityAvailable();
            appendNote( BXResources.getDocPrepIngMsg("EQUITY_AVAILABLE",lang) + DELIMITER + 
            		    val1 + DELIMITER + 
            		    val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_EQUITY_AVAILABLE, dval1, dval2))
			return;

		//* GARAGESIZEID
		logger.trace( ": Method compareProperties - GARAGESIZEID");
		ival1 = prop1.getGarageSizeId();
		ival2 = prop2.getGarageSizeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("GarageSize", prop1.getGarageSizeId(), lang);
            val2  = BXResources.getPickListDescription("GarageSize", prop2.getGarageSizeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("GARAGE_SIZE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_GARAGE_SIZE, PKL_GARAGE_SIZE, ival1, ival2))
			return;

		//* GARAGETYPEID
		logger.trace( ": Method compareProperties - GARAGETYPEID");
		ival1 = prop1.getGarageTypeId();
		ival2 = prop2.getGarageTypeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("GarageType", prop1.getGarageTypeId(), lang);
            val2  = BXResources.getPickListDescription("GarageType", prop2.getGarageTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("GARAGE_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_GARAGE_TYPE, PKL_GARAGE_TYPE, ival1, ival2))
			return;

		//* HEATTYPEID
		logger.trace( ": Method compareProperties - HEATTYPEID");
		ival1 = prop1.getHeatTypeId();
		ival2 = prop2.getHeatTypeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("HeatType", prop1.getHeatTypeId(), lang);
            val2  = BXResources.getPickListDescription("HeatType", prop2.getHeatTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("HEAT_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_HEAT_TYPE, PKL_HEAT_TYPE, ival1, ival2))
			return;

		//* INSULATEDWITHUFFI
		logger.trace( ": Method compareProperties - INSULATEDWITHUFFI");
		boolean bval1 = prop1.getInsulatedWithUFFI();
		boolean bval2 = prop2.getInsulatedWithUFFI();
		/* #DG658
		if (bval1 != bval2) {
			/ *#DG600 
            val1 = prop1.getInsulatedWithUFFI() ? "Yes" : NO_LABEL;
            val2 = prop2.getInsulatedWithUFFI() ? "Yes" : NO_LABEL;
            appendNote(BXResources.getDocPrepIngMsg("UFFI_INSULATION",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			nota  = newTabedLine(DUP_CHEK_UFFI_INSULATION,
					BXResources.newGenMsg(bval1? YES_LABEL : NO_LABEL),
					BXResources.newGenMsg(bval2? YES_LABEL : NO_LABEL), 
					null);            
			totalDifferences++;
		}
		if (maximumReached()) */
		if(comparBool(DUP_CHEK_UFFI_INSULATION, bval1, bval2))
			return;

		//* LANDVALUE
		logger.trace( ": Method compareProperties - LANDVALUE");
		dval1 = prop1.getLandValue();
		dval2 = prop2.getLandValue();
		/*#DG600 
        if (dval1 != dval2) {
            val1 = formatCurrency(prop1.getLandValue());
            val2 = formatCurrency(prop2.getLandValue());
            dDiff = prop2.getLandValue() - prop1.getLandValue();
            appendNote( BXResources.getDocPrepIngMsg("LAND_VALUE",lang) + DELIMITER + 
            		    val1 + DELIMITER + 
            		    val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_LAND_VALUE, dval1, dval2))
			return;

		//* LEGALLINE1
		logger.trace( ": Method compareProperties - LEGALLINE1");
		val1 = prop1.getLegalLine1();
		val2 = prop2.getLegalLine1();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            val1 = prop1.getLegalLine1();
            val2 = prop2.getLegalLine1();
            appendNote(BXResources.getDocPrepIngMsg("LEGAL_DESCRIPTION_LINE_1",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_LEGAL_DESCR1, val1, val2))
			return;

		//* LEGALLINE2
		logger.trace( ": Method compareProperties - LEGALLINE2");
		val1 = prop1.getLegalLine2();
		val2 = prop2.getLegalLine2();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            val1 = prop1.getLegalLine2();
            val2 = prop2.getLegalLine2();
            appendNote(BXResources.getDocPrepIngMsg("LEGAL_DESCRIPTION_LINE_2",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_LEGAL_DESCR2, val1, val2))
			return;

		//* LEGALLINE3
		logger.trace( ": Method compareProperties - LEGALLINE3");
		val1 = prop1.getLegalLine3();
		val2 = prop2.getLegalLine3();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            val1 = prop1.getLegalLine3();
            val2 = prop2.getLegalLine3();
            appendNote(BXResources.getDocPrepIngMsg("LEGAL_DESCRIPTION_LINE_2",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_LEGAL_DESCR2, val1, val2))
			return;

		//* LIVINGSPACE
		logger.trace( ": Method compareProperties - LIVINGSPACE");
		ival1 = prop1.getLivingSpace();
		ival2 = prop2.getLivingSpace();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getLivingSpace() - prop1.getLivingSpace();
            appendNote( BXResources.getDocPrepIngMsg("LIVING_SPACE",lang) + DELIMITER + 
            		    prop1.getLivingSpace() + DELIMITER +
                        prop2.getLivingSpace() + DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_LIVING_SPACE, ival1, ival2))
			return;

		//* LIVINGSPACEUNITOFMEASUREID
		logger.trace( ": Method compareProperties - LIVINGSPACEUNITOFMEASUREID");
		ival1 = prop1.getLivingSpaceUnitOfMeasureId();
		ival2 = prop2.getLivingSpaceUnitOfMeasureId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("LivingSpaceUnitOfMeasure", prop1.getLivingSpaceUnitOfMeasureId(), lang);
            val2  = BXResources.getPickListDescription("LivingSpaceUnitOfMeasure", prop2.getLivingSpaceUnitOfMeasureId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("LIVING_SPACE_UNIT_OF_MEASUREMENT",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_LIVIN_UNIT_MEASUR, PKL_LIVING_SP_UNIT_MEASURE, ival1, ival2))
			return;

		//* LOTSIZE
		logger.trace( ": Method compareProperties : 16");
		ival1 = prop1.getLotSize();
		ival2 = prop2.getLotSize();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getLotSize() - prop1.getLotSize();
            appendNote( BXResources.getDocPrepIngMsg("LOT_SIZE_FRONTAGE",lang) + DELIMITER + 
            		    prop1.getLotSize() + DELIMITER +
                        prop2.getLotSize() + DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_LOT_SIZE_FRONTAGE, ival1, ival2))
			return;

		//* LOTSIZEDEPTH
		logger.trace( ": Method compareProperties - LOTSIZEDEPTH");
		ival1 = prop1.getLotSizeDepth();
		ival2 = prop2.getLotSizeDepth();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getLotSizeDepth() - prop1.getLotSizeDepth();
            appendNote( BXResources.getDocPrepIngMsg("LOT_SIZE_DEPTH",lang) + DELIMITER + 
            		    prop1.getLotSizeDepth() + DELIMITER +
                        prop2.getLotSizeDepth() + DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_LOT_SIZE_DEPTH, ival1, ival2))
			return;

		//* LOTSIZEFRONTAGE
		logger.trace( ": Method compareProperties - LOTSIZEFRONTAGE");
		ival1 = prop1.getLotSizeFrontage();
		ival2 = prop2.getLotSizeFrontage();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getLotSizeFrontage() - prop1.getLotSizeFrontage();
            appendNote( BXResources.getDocPrepIngMsg("LOT_SIZE_FRONTAGE",lang) + DELIMITER + 
            		    prop1.getLotSizeFrontage() + DELIMITER +
                        prop2.getLotSizeFrontage() + DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_LOT_SIZE_FRONTAGE, ival1, ival2))
			return;

		//* LOTSIZEUNITOFMEASUREID
		logger.trace( ": Method compareProperties - LOTSIZEUNITOFMEASUREID");
		ival1 = prop1.getLotSizeUnitOfMeasureId();
		ival2 = prop2.getLotSizeUnitOfMeasureId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("LotSizeUnitOfMeasure", prop1.getLotSizeUnitOfMeasureId(), lang);
            val2  = BXResources.getPickListDescription("LotSizeUnitOfMeasure", prop2.getLotSizeUnitOfMeasureId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("LOT_SIZE_UNIT_OF_MEASUREMENT",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_LOT_UNIT_MEASUR, PKL_LOT_SIZE_UNIT_MEASURE, ival1, ival2))
			return;

		//* MLSLISTINGFLAG
		logger.trace( ": Method compareProperties - MLSLISTINGFLAG");
		val1 = prop1.getMLSListingFlag();
		val2 = prop2.getMLSListingFlag();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("MLS",lang) + DELIMITER + 
            		    prop1.getMLSListingFlag() +
                        DELIMITER + prop2.getMLSListingFlag());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_MLS, val1, val2))
			return;

		//* MONTHLYCONDOFEES
		logger.trace( ": Method compareProperties - MONTHLYCONDOFEES");
		dval1 = prop1.getMonthlyCondoFees();
		dval2 = prop2.getMonthlyCondoFees();
		/*#DG600 
        if (dval1 != dval2) {
            val1 = formatCurrency(prop1.getMonthlyCondoFees());
            val2 = formatCurrency(prop2.getMonthlyCondoFees());
            dDiff = prop2.getMonthlyCondoFees() - prop1.getMonthlyCondoFees();
            appendNote( BXResources.getDocPrepIngMsg("MONTHLY_CONDO_FEES",lang) + DELIMITER + 
            		    val1 + DELIMITER + 
            		    val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_MONTHLY_CONDO_FEES, dval1, dval2))
			return;

		//* MUNICIPALITYID
		logger.trace( ": Method compareProperties - MUNICIPALITYID");
		ival1 = prop1.getMunicipalityId();
		ival2 = prop2.getMunicipalityId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("Municipality", prop1.getMunicipalityId(), lang);
            val2  = BXResources.getPickListDescription("Municipality", prop2.getMunicipalityId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("MUNICIPALITY",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_MUNICIPALITY, PKL_MUNICIPALITY, ival1, ival2))
			return;

		//* NEWCONSTRUCTIONID
		ival1 = prop1.getNewConstructionId();
		ival2 = prop2.getNewConstructionId();
		/*#DG600 
        if (ival1 != ival2) {
            logger.trace( ": Method compareProperties - NEWCONSTRUCTIONID");
            val1  = BXResources.getPickListDescription("NewConstruction", prop1.getNewConstructionId(), lang);
            val2  = BXResources.getPickListDescription("NewConstruction", prop2.getNewConstructionId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("NEW_CONSTRUCTION",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_NEW_CONSTRUCTION, PKL_NEW_CONSTRUCTION, ival1, ival2))
			return;

		//* NUMBEROFBEDROOMS
		logger.trace( ": Method compareProperties - NUMBEROFBEDROOMS");
		ival1 = prop1.getNumberOfBedrooms();
		ival2 = prop2.getNumberOfBedrooms();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getNumberOfBedrooms() - prop1.getNumberOfBedrooms();
            appendNote(BXResources.getDocPrepIngMsg("NUMBER_OF_BEDROOMS",lang) + DELIMITER + prop1.getNumberOfBedrooms() +
                                         DELIMITER + prop2.getNumberOfBedrooms() +
                                         DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_NUMBER_OF_BEDROOMS, ival1, ival2))
			return;

		//* NUMBEROFUNITS
		logger.trace( ": Method compareProperties - NUMBEROFUNITS");
		ival1 = prop1.getNumberOfUnits();
		ival2 = prop2.getNumberOfUnits();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getNumberOfUnits() - prop1.getNumberOfUnits();
            appendNote(BXResources.getDocPrepIngMsg("NUMBER_OF_UNITS",lang) + DELIMITER + prop1.getNumberOfUnits() +
                                      DELIMITER + prop2.getNumberOfUnits() +
                                      DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_NUMBER_OF_UNITS, ival1, ival2))
			return;

		//* OCCUPANCYTYPEID
		logger.trace( ": Method compareProperties - OCCUPANCYTYPEID");
		ival1 = prop1.getOccupancyTypeId();
		ival2 = prop2.getOccupancyTypeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("OccupancyType", prop1.getOccupancyTypeId(), lang);
            val2  = BXResources.getPickListDescription("OccupancyType", prop2.getOccupancyTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("OCCUPANCY_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_OCCUPANCY_TYPE, PKL_OCCUPANCY_TYPE, ival1, ival2))
			return;

		//* PROPERTYADDRESSLINE2
		logger.trace( ": Method compareProperties - PROPERTYADDRESSLINE2");
		val1 = prop1.getPropertyAddressLine2();
		val2 = prop2.getPropertyAddressLine2();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_ADDRESS_LINE_2",lang) + DELIMITER + 
            		    prop1.getPropertyAddressLine2() + DELIMITER + 
            		    prop2.getPropertyAddressLine2());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROP_ADDR_2, val1, val2))
			return;

		//* PROPERTYCITY
		logger.trace( ": Method compareProperties : 27");
		val1 = prop1.getPropertyCity();
		val2 = prop2.getPropertyCity();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_CITY",lang) + DELIMITER + 
            		    prop1.getPropertyCity() + DELIMITER + 
            		    prop2.getPropertyCity());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROPERTY_CITY, val1, val2))
			return;

		//* PROPERTYLOCATIONID
		logger.trace( ": Method compareProperties - PROPERTYLOCATIONID");
		ival1 = prop1.getPropertyLocationId();
		ival2 = prop2.getPropertyLocationId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("PropertyLocation", prop1.getPropertyLocationId(), lang);
            val2  = BXResources.getPickListDescription("PropertyLocation", prop2.getPropertyLocationId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("PROPERTY_LOCATION",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PROPERTY_LOCATION, PKL_PROPERTY_LOCATION, ival1, ival2))
			return;

		//* PROPERTY POSTAL CODE
		logger.trace( ": Method compareProperties - PROPERTY POSTAL CODE");
		val1 = prop1.getPropertyPostalFSA() == null && prop1.getPropertyPostalFSA() == null?
				"null": prop1.getPropertyPostalFSA() + " " + prop1.getPropertyPostalLDU();
		val2 = prop2.getPropertyPostalFSA() == null && prop2.getPropertyPostalFSA() == null?
				"null": prop2.getPropertyPostalFSA() + " " + prop2.getPropertyPostalLDU();
		/*#DG600 
        val1 = prop1.getPropertyPostalFSA() + " " + prop1.getPropertyPostalLDU();
        val2 = prop2.getPropertyPostalFSA() + " " + prop2.getPropertyPostalLDU();
        if (val1.equals("null null")) val1 = null;
        if (val2.equals("null null")) val2 = null;
        if (!isEqual(val1, val2)) {
            appendNote(BXResources.getDocPrepIngMsg("PROPERTY_POSTAL_CODE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROP_POSTAL, val1, val2))
			return;


		//* PROPERTYSTREETNAME
		logger.trace( ": Method compareProperties - PROPERTYSTREETNAME");
		val1 = prop1.getPropertyStreetName();
		val2 = prop2.getPropertyStreetName();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_STREET_NAME",lang) + DELIMITER + 
            		    prop1.getPropertyStreetName() + DELIMITER + 
            		    prop2.getPropertyStreetName());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROP_STR_NAME, val1, val2))
			return;

		//* PROPERTYSTREETNUMBER
		logger.trace( ": Method compareProperties - PROPERTYSTREETNUMBER");
		val1 = prop1.getPropertyStreetNumber();
		val2 = prop2.getPropertyStreetNumber();	//#DG634
		/*#DG600 
    if (!isEqual(val1, val2)) {
        appendNote( BXResources.getDocPrepIngMsg("PROPERTY_STREET_NUMBER",lang) + DELIMITER + 
        		    prop1.getPropertyStreetNumber() + DELIMITER + 
        		    prop2.getPropertyStreetNumber());
        totalDifferences++;
    }
    if (maximumReached()) */
		//#DG644 rewritten - this portion was incorrectly commented out
		if(comparStr(DUP_CHEK_PROP_STR_NBR, val1, val2))   
			return;

    //* PROPERTYTYPEID
    logger.trace( ": Method compareProperties - PROPERTYTYPEID");
    ival1 = prop1.getPropertyTypeId();
    ival2 = prop2.getPropertyTypeId();
    /*#DG600 
    if (ival1 != ival2) {
        val1  = BXResources.getPickListDescription("PropertyType", prop1.getPropertyTypeId(), lang);
        val2  = BXResources.getPickListDescription("PropertyType", prop2.getPropertyTypeId(), lang);
        appendNote(BXResources.getDocPrepIngMsg("PROPERTY_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
        totalDifferences++;
    }
    if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PROPERTY_TYPE, PKL_PROPERTY_TYPE, ival1, ival2))
			return;

		//* PROPERTYUSAGEID
		logger.trace( ": Method compareProperties - PROPERTYUSAGEID");
		ival1 = prop1.getPropertyUsageId();
		ival2 = prop2.getPropertyUsageId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("PropertyUsage", prop1.getPropertyUsageId(), lang);
            val2  = BXResources.getPickListDescription("PropertyUsage", prop2.getPropertyUsageId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("PROPERTY_USAGE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PROPERTY_USAGE, PKL_PROPERTY_USAGE, ival1, ival2))
			return;

		//* PROVINCEID
		logger.trace( ": Method compareProperties - PROVINCEID");
		ival1 = prop1.getProvinceId();
		ival2 = prop2.getProvinceId();
		/*#DG600 
        if (ival1 != ival2) {
            val1 = "";
            val2 = "";
            try {
                Province prov1 = new Province(srk, prop1.getProvinceId());
                val1 = prov1.getProvinceName();
            }
            catch (Exception e) {}
            try {
                Province prov2 = new Province(srk, prop2.getProvinceId());
                val2 = prov2.getProvinceName();
            }
            catch (Exception e) {}            
            appendNote(BXResources.getDocPrepIngMsg("PROPERTY_PROVINCE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PROPERTY_PROVINCE, PKL_PROVINCE, ival1, ival2))
			return;

		//* PURCHASEPRICE
		logger.trace( ": Method compareProperties : 35");
		dval1 = prop1.getPurchasePrice();
		dval2 = prop2.getPurchasePrice();
		/*#DG600 
        if (dval1 != dval2) {
            val1 = formatCurrency(prop1.getPurchasePrice());
            val2 = formatCurrency(prop2.getPurchasePrice());
            dDiff = prop2.getPurchasePrice() - prop1.getPurchasePrice();
            appendNote( BXResources.getDocPrepIngMsg("PURCHASE_PRICE",lang) + DELIMITER + 
            		    val1 + DELIMITER + 
            		    val2 + DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PURCHASE_PRICE, dval1, dval2))
			return;

		//* SEWAGETYPEID
		logger.trace( ": Method compareProperties - SEWAGETYPEID");
		ival1 = prop1.getSewageTypeId();
		ival2 = prop2.getSewageTypeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("SewageType", prop1.getSewageTypeId(), lang);
            val2  = BXResources.getPickListDescription("SewageType", prop2.getSewageTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("SEWAGE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_SEWAGE, PKL_SEWAGE_TYPE, ival1, ival2))
			return;

		//* STREETDIRECTIONID
		logger.trace( ": Method compareProperties - STREETDIRECTIONID");
		ival1 = prop1.getStreetDirectionId();
		ival2 = prop2.getStreetDirectionId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("StreetDirection", prop1.getStreetDirectionId(), lang);
            val2  = BXResources.getPickListDescription("StreetDirection", prop2.getStreetDirectionId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("PROPERTY_STREET_DIRECTION",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PROP_STR_DIR, PKL_STREET_DIRECTION, ival1, ival2))
			return;

		//* STREETTYPEID
		logger.trace( ": Method compareProperties - STREETTYPEID");
		ival1 = prop1.getStreetTypeId();
		ival2 = prop2.getStreetTypeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("StreetType", prop1.getStreetTypeId(), lang);
            val2  = BXResources.getPickListDescription("StreetType", prop2.getStreetTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("PROPERTY_STREET_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PROP_STR_TYP, PKL_STREET_TYPE, ival1, ival2))
			return;

		//* STRUCTUREAGE
		logger.trace( ": Method compareProperties - STRUCTUREAGE");
		ival1 = prop1.getStructureAge();
		ival2 = prop2.getStructureAge();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getStructureAge() - prop1.getStructureAge();
            appendNote( BXResources.getDocPrepIngMsg("STRUCTURE_AGE",lang) + DELIMITER + 
            		    prop1.getStructureAge() + DELIMITER + 
            		    prop2.getStructureAge() + DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_STRUCTURE_AGE, ival1, ival2))
			return;

		//* UNITNUMBER
		logger.trace( ": Method compareProperties - UNITNUMBER");
		val1 = prop1.getUnitNumber();
		val2 = prop2.getUnitNumber();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_UNIT_NUMBER",lang) + DELIMITER + 
            		    prop1.getUnitNumber() + DELIMITER + 
            		    prop2.getUnitNumber());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROP_UNIT_NR, val1, val2))
			return;

		//* WATERTYPEID
		logger.trace( ": Method compareProperties - WATERTYPEID");
		ival1 = prop1.getWaterTypeId();
		ival2 = prop2.getWaterTypeId();
		/*#DG600 
        if (ival1 != ival2) {
            val1  = BXResources.getPickListDescription("WaterType", prop1.getWaterTypeId(), lang);
            val2  = BXResources.getPickListDescription("WaterType", prop2.getWaterTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("WATER",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_WATER, PKL_WATER_TYPE, ival1, ival2))
			return;

		//* YEARBUILT
		logger.trace( ": Method compareProperties - YEARBUILT");
		ival1 = prop1.getYearBuilt();
		ival2 = prop2.getYearBuilt();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getYearBuilt() - prop1.getYearBuilt();
            appendNote( BXResources.getDocPrepIngMsg("YEAR_BUILT",lang) + DELIMITER + 
            		    prop1.getYearBuilt() + DELIMITER + 
            		    prop2.getYearBuilt() + DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_YEAR_BUILT, ival1, ival2))
			return;

		//* ZONING
		logger.trace( ": Method compareProperties - ZONING");
		ival1 = prop1.getZoning();
		ival2 = prop2.getZoning();
		/*#DG600 
        if (ival1 != ival2) {
            iDiff = prop2.getZoning() - prop1.getZoning();
            appendNote( BXResources.getDocPrepIngMsg("ZONING",lang) + DELIMITER + 
            		    prop1.getZoning() + DELIMITER + 
            		    prop2.getZoning() + DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_ZONING, ival1, ival2))
			return;

		// Rel 3.1 Sasha
		// --------------------------------------------------------
		//* TENURETYPEID
		logger.trace( ": Method compareProperties - TENURETYPEID");
		ival1 = prop1.getTenureTypeId();
		ival2 = prop2.getTenureTypeId();
		/*#DG600 
        if (ival1 != ival2) {
        	val1 = BXResources.getPickListDescription("TenureType", prop1.getTenureTypeId(), lang);
        	val2 = BXResources.getPickListDescription("TenureType", prop2.getTenureTypeId(), lang);
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_TENURE_TYPE_ID",lang) + DELIMITER + val1 + DELIMITER + val2);            
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_PROP_TENURE_TYP_ID, PKL_TENURE_TYPE, ival1, ival2))
			return;

		//* MIENERGYEFFICIENCY
		logger.trace( ": Method compareProperties - MIENERGYEFFICIENCY");
		val1 = prop1.getMiEnergyEfficiency();
		val2 = prop2.getMiEnergyEfficiency();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_MI_ENERGY_EFFICIENCY",lang) + DELIMITER + 
            		    prop1.getMiEnergyEfficiency() + DELIMITER + 
            		    prop2.getMiEnergyEfficiency());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROP_MI_ENERG_EFIC, val1, val2))
			return;

		//* ONRESERVETRUSTAGREEMENTNUMBER
		logger.trace( ": Method compareProperties - ONRESERVETRUSTAGREEMENTNUMBER");
		val1 = prop1.getOnReserveTrustAgreementNumber();
		val2 = prop2.getOnReserveTrustAgreementNumber();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2)) {
        if (prop1.getOnReserveTrustAgreementNumber() != prop2.getOnReserveTrustAgreementNumber()) {
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_ON_RESERVE_TRUST_AGREEMENT_NUMBER",lang) + DELIMITER + 
            		    prop1.getOnReserveTrustAgreementNumber() + DELIMITER + 
            		    prop2.getOnReserveTrustAgreementNumber());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROP_RES_TR_AGR_NR, val1, val2))
			return;

		//* SUBDIVISIONDISCOUNT
		logger.trace( ": Method compareProperties - SUBDIVISIONDISCOUNT");
		val1 = prop1.getSubdivisionDiscount();
		val2 = prop2.getSubdivisionDiscount();	//#DG634
		/*#DG600 
        if ( !isEqual(val1, val2)) {
            appendNote( BXResources.getDocPrepIngMsg("PROPERTY_SUBDIVISION_DISCOUNT",lang) + DELIMITER + 
            		    prop1.getSubdivisionDiscount() + DELIMITER + 
            		    prop2.getSubdivisionDiscount());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_PROP_SUBDIV_DISC, val1, val2))
			return;
		// --------------------------------------------------------
		// End of Rel 3.1 Sasha


		//* PROPERTY EXPENSES
		logger.trace( ": Method compareProperties - PROPERTY EXPENSES");
		try {
			List<PropertyExpense> l1 = (List<PropertyExpense>)prop1.getPropertyExpenses();
			List<PropertyExpense> l2 = (List<PropertyExpense>)prop2.getPropertyExpenses();

			doPropertyExpenseListCompare(l1, l2);
		}
		catch (Exception e) {}

		// ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
		// * REQUESTAPPRAISAL
		// SCR #1744: save values in variables val1, val2 to compare and use in notes
		/*#DG600 
        val1 = String.valueOf(prop1.getRequestAppraisalId());
        val2 = String.valueOf(prop2.getRequestAppraisalId());
        logger.trace( ":" + " Method compareProperties : REQUEST APPRAISAL");
        if (! val1.equals(val2)) {							
        	appendNote(BXResources.getDocPrepIngMsg("REQUESTAPPRAISAL",
        			lang)
        			+ DELIMITER
        			+ ""
        			+ BXResources.getPickListDescription("REQUESTAPPRAISAL",
        					val1, lang)
        					+ DELIMITER
        					+ ""
        					+ BXResources.getPickListDescription("REQUESTAPPRAISAL",
        							val2, lang));

        	totalDifferences++;
        } */
		ival1 = prop1.getRequestAppraisalId();
		ival2 = prop2.getRequestAppraisalId();
		if(comparPkL(DUP_CHEK_REQUESTAPPRAISAL, PKL_REQUESTAPPRAISAL, ival1, ival2))
			return;

		// ***** Change by NBC Impl. Team - Version 1.6 - End *****//


		//  If no changes were made then remove label.
		/*#DG600 
        if (tempTotal == totalDifferences)
           dealNotesList.remove(place);*/
		if (tempTotal < totalDifferences) {
			nota = BXResources.newFormata(DUP_CHEK_PROP_DIFF);
			dealNotesList.insertElementAt(nota, hdPos);
		}

		logger.trace( ": Method compareProperties finished ");
	}

	/**
	 * The following fields are printed on the <code>Borrower</code>:
	 * <p><code><blockquote><pre>
	 *    TOTALLIABILITYPAYMENTS
	 *    TOTALLIABILITYAMOUNT
	 *    TOTALINCOMEAMOUNT
	 *    TOTALASSETAMOUNT
	 *    SOCIALINSURANCENUMBER
	 *    NUMBEROFTIMESBANKRUPT
	 *    NUMBEROFDEPENDENTS
	 *    MARITALSTATUSID
	 *    LANGUAGEPREFERENCEID
	 *    FIRSTTIMEBUYER
	 *    CREDITSCORE
	 *    CREDITBUREAUSUMMARY
	 *    CREDITBUREAUONFILEDATE
	 *    CREDITBUREAUNAMEID
	 *    CITIZENSHIPTYPEID
	 *    BUREAUATTACHMENT
	 *    BORROWERWORKPHONENUMBER
	 *    BORROWERWORKPHONEEXTENSION
	 *    BORROWERTYPEID
	 *    BORROWERMIDDLEINITIAL
	 *    BORROWERLASTNAME
	 *    BORROWERHOMEPHONENUMBER
	 *    BORROWERGENERALSTATUSID
	 *    BORROWERFIRSTNAME
	 *    BORROWERFAXNUMBER
	 *    BORROWEREMAILADDRESS
	 *    BORROWERBIRTHDATE
	 *    BANKRUPTCYSTATUSID
	 *    AGE
	 * </pre></blockquote></code>
	 *
	 * @param b the <code>Borrower</code> to use as a source.
	 */
	/*#DG600 not used ?
    public static String printBorrower(Borrower b)
    {
        String val;
        StringBuffer buff = new StringBuffer();

        //* BORROWERFIRSTNAME
        if (!isEmpty(b.getBorrowerFirstName()))
           //buff.append("First Name:" + DELIMITER + b.getBorrowerFirstName() + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("FIRST_NAME",lang)+ ":" + DELIMITER + b.getBorrowerFirstName() + NEW_LINE);

        //* BORROWERMIDDLEINITIAL
        if (!isEmpty(b.getBorrowerMiddleInitial()))
           //buff.append("Middle Initial:" + DELIMITER + b.getBorrowerMiddleInitial() + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("MIDDLE_INITIAL",lang) + ":" + DELIMITER + b.getBorrowerMiddleInitial() + NEW_LINE);

        //* BORROWERLASTNAME
        if (!isEmpty(b.getBorrowerLastName()))
           //buff.append("Last Name:" + DELIMITER + b.getBorrowerLastName() + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("LAST_NAME:",lang)+ ":" + DELIMITER + b.getBorrowerLastName() + NEW_LINE);

        //* TOTALLIABILITYPAYMENTS
        val = formatCurrency(b.getTotalLiabilityPayments());
        //buff.append("Total Applicant Liabilities:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("TOTAL_APPLICANT_LIABILITIES",lang)+ ":" + DELIMITER + val + NEW_LINE);

        //* TOTALLIABILITYAMOUNT
        val = formatCurrency(b.getTotalLiabilityAmount());
        //buff.append("Monthly Liabilities Payments:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("MONTHLY_LIABILITIES_PAYMENTS",lang)+":" + DELIMITER + val + NEW_LINE);

        //* TOTALINCOMEAMOUNT
        val = formatCurrency(b.getTotalIncomeAmount());
        //buff.append("Total Income:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("TOTAL_INCOME",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* TOTALASSETAMOUNT
        val = formatCurrency(b.getTotalAssetAmount());
        //buff.append("Total Asset:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("TOTAL_ASSET",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* SOCIALINSURANCENUMBER
        if (!isEmpty(b.getSocialInsuranceNumber()))
           //buff.append("S.I.N.:" + DELIMITER + StringUtil.formatSIN(b.getSocialInsuranceNumber()) + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("SIN",lang)+ ":" + DELIMITER + StringUtil.formatSIN(b.getSocialInsuranceNumber()) + NEW_LINE);

        //* NUMBEROFTIMESBANKRUPT
        //buff.append("# of Times Bankrupt:" + DELIMITER + b.getNumberOfTimesBankrupt() + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("NUMBER_OF_TIMES_BANKRUPT",lang)+ ":" + DELIMITER + b.getNumberOfTimesBankrupt() + NEW_LINE);

        //* NUMBEROFDEPENDENTS
        //buff.append("# of Dependents:" + DELIMITER + b.getNumberOfDependents() + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("NUMBER_OF_DEPENDENTS",lang) + ":" + DELIMITER + b.getNumberOfDependents() + NEW_LINE);

        //* MARITALSTATUSID
        //val = PicklistData.getDescription("MaritalStatus", b.getMaritalStatusId() + NEW_LINE);
        //ALERT:BX:ZR Release2.1.docprep
        val  = BXResources.getPickListDescription(MARITAL_STATUS,  b.getMaritalStatusId(), lang);

        //buff.append("Marital Status:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("MARITAL_STATUS",lang)+":" + DELIMITER + val + NEW_LINE);

        //* LANGUAGEPREFERENCEID
        //val = PicklistData.getDescription("LanguagePreference", b.getLanguagePreferenceId() + NEW_LINE);
        //ALERT:BX:ZR Release2.1.docprep
        val  = BXResources.getPickListDescription(LANGUAGE_PREFERENCE,  b.getLanguagePreferenceId(), lang);

        //buff.append("Language Preference:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("LANGUAGE_PREFERENCE",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* FIRSTTIMEBUYER
        //buff.append("First Time Buyer:" + DELIMITER + b.getFirstTimeBuyer() + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("FIRST_TIME_BUYER",lang) + ":" + DELIMITER + b.getFirstTimeBuyer() + NEW_LINE);

        //* CREDITSCORE
        //buff.append("Credit Score:" + DELIMITER + b.getCreditScore() + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("CREDIT_SCORE",lang) + ":" + DELIMITER + b.getCreditScore() + NEW_LINE);

        //* CREDITBUREAUSUMMARY
        if (!isEmpty(b.getCreditBureauSummary()))
           //buff.append("Credit Bureau Summary:" + DELIMITER + convertCR(b.getCreditBureauSummary()) + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("CREDIT_BUREAU_SUMMARY",lang) + ":" + DELIMITER + convertCR(b.getCreditBureauSummary()) + NEW_LINE);

        //* CREDITBUREAUONFILEDATE
        val = b.getCreditBureauOnFileDate() == null ?
                 null :
                 df.format(b.getCreditBureauOnFileDate());

        //buff.append("Credit Bureau on File Date:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("CREDIT_BUREAU_ON_FILE_DATE",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* CREDITBUREAUNAMEID
        //val = PicklistData.getDescription("CreditBureauName", b.getCreditBureauNameId());
        //ALERT:BX:ZR Release2.1.docprep
        val  = BXResources.getPickListDescription(CREDIT_BUREAU_NAME,  b.getCreditBureauNameId(), lang);

        //buff.append("Credit Bureau Name:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("CREDIT_BUREAU_NAME",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* CITIZENSHIPTYPEID
        //val = PicklistData.getDescription("CitizenshipType", b.getCitizenshipTypeId());
        //ALERT:BX:ZR Release2.1.docprep
        val  = BXResources.getPickListDescription(CITIZENSHIP_TYPE,  b.getCreditBureauNameId(), lang);

        //buff.append("Citizenship Status:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("CITIZENSHIP_STATUS",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* BUREAUATTACHMENT
        //buff.append("Bureau Attachment:" + DELIMITER + b.getBureauAttachment() + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("BUREAU_ATTACHMENT",lang) + ":" + DELIMITER + b.getBureauAttachment() + NEW_LINE);

        //* BORROWERWORKPHONENUMBER
        val = null;

        try
        {
            val = StringUtil.getAsFormattedPhoneNumber(b.getBorrowerWorkPhoneNumber(), null);
        }
        catch (Exception e) {}

        if (!isEmpty(val))
           //buff.append("Work Phone #:" + DELIMITER + val + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("WORK_PHONE_NUMBER",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* BORROWERWORKPHONEEXTENSION
        if (!isEmpty(b.getBorrowerWorkPhoneExtension()))
           //buff.append("Borrower Work Phone Extension:" + DELIMITER + b.getBorrowerWorkPhoneExtension() + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("BORROWER_WORK_PHONE_EXTENSION",lang) + ":" + DELIMITER + b.getBorrowerWorkPhoneExtension() + NEW_LINE);

        //* BORROWERTYPEID
        //val = PicklistData.getDescription("BorrowerType", b.getBorrowerTypeId());
        //ALERT:BX:ZR Release2.1.docprep
        val  = BXResources.getPickListDescription(BORROWER_TYPE,  b.getBorrowerTypeId(), lang);

        //buff.append("Type:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("TYPE",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* BORROWERHOMEPHONENUMBER
        val = null;

        try
        {
            val = StringUtil.getAsFormattedPhoneNumber(b.getBorrowerHomePhoneNumber(), null);
        }
        catch (Exception e) {}

        if (!isEmpty(val))
           //buff.append("Home Phone #:" + DELIMITER + val + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("HOME_PHONE_NUMBER",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* BORROWERGENERALSTATUSID
        //val = PicklistData.getDescription("BorrowerGeneralStatus", b.getBorrowerGeneralStatusId());
        //ALERT:BX:ZR Release2.1.docprep
        val  = BXResources.getPickListDescription(BORROWER_GENERAL_STATUS,  b.getBorrowerGeneralStatusId(), lang);

        //buff.append("Borrower General Status:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("BORROWER_GENERAL_STATUS",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* BORROWERFAXNUMBER
        val = null;

        try
        {
            val = StringUtil.getAsFormattedPhoneNumber(b.getBorrowerFaxNumber(), null);
        }
        catch (Exception e) {}

        if (!isEmpty(val))
           //buff.append("Fax #:" + DELIMITER + val + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("FAX_NUMBER",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* BORROWEREMAILADDRESS
        if (!isEmpty(b.getBorrowerEmailAddress()))
           //buff.append("Email Address:" + DELIMITER + b.getBorrowerEmailAddress() + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("EMAIL_ADDRESS",lang) + ":" + DELIMITER + b.getBorrowerEmailAddress() + NEW_LINE);

        //* BORROWERBIRTHDATE
        val = b.getBorrowerBirthDate() == null ?
                 null :
                 df.format(b.getBorrowerBirthDate());

        if (val != null)
           //buff.append("Date of Birth:" + DELIMITER + val + NEW_LINE);
           buff.append(BXResources.getDocPrepIngMsg("DATE_OF_BIRTH",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* BANKRUPTCYSTATUSID
        //val = PicklistData.getDescription("BankruptcyStatus", b.getBankruptcyStatusId());
        //ALERT:BX:ZR Release2.1.docprep
        val  = BXResources.getPickListDescription(BANKRUPTCY_STATUS,  b.getBankruptcyStatusId(), lang);

        //buff.append("Bankruptcy Status:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("BANKRUPTCY_STATUS",lang) + ":" + DELIMITER + val + NEW_LINE);

        //* AGE
        //buff.append("Age" + DELIMITER + b.getAge() + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("AGE",lang) + ":" + DELIMITER + b.getAge() + NEW_LINE);

        return buff.toString();
    }*/

	/**
	 * Compares key fields in a pair of Borrowers.
	 * <P>
	 * The following fields are compared on each <code>Borrower</code>:
	 * <p><code><blockquote><pre>
	 *    TOTALLIABILITYPAYMENTS
	 *    TOTALLIABILITYAMOUNT
	 *    TOTALINCOMEAMOUNT
	 *    TOTALASSETAMOUNT
	 *    SOCIALINSURANCENUMBER
	 *    NUMBEROFTIMESBANKRUPT
	 *    NUMBEROFDEPENDENTS
	 *    MARITALSTATUSID
	 *    LANGUAGEPREFERENCEID
	 *    FIRSTTIMEBUYER
	 *    CREDITSUMMARY
	 *    CREDITSCORE
	 *    CREDITBUREAUSUMMARY
	 *    CREDITBUREAUONFILEDATE
	 *    CREDITBUREAUNAMEID
	 *    CITIZENSHIPTYPEID
	 *    BUREAUATTACHMENT
	 *    BORROWERWORKPHONENUMBER
	 *    BORROWERWORKPHONEEXTENSION
	 *    BORROWERTYPEID
	 *    BORROWERMIDDLEINITIAL
	 *    BORROWERLASTNAME
	 *    BORROWERHOMEPHONENUMBER
	 *    BORROWERGENERALSTATUSID
	 *    BORROWERFIRSTNAME
	 *    BORROWERFAXNUMBER
	 *    BORROWEREMAILADDRESS
	 *    BORROWERBIRTHDATE
	 *    BANKRUPTCYSTATUSID
	 *    AGE
	 * </pre></blockquote></code>
	 */
	public void compareBorrowers(Borrower b1, Borrower b2)
	{
		logger.trace( ": Method compareBorrowers entered ");
		if (maximumReached()) 
			return;

		int diffCount = 0;
		String val1, val2;
		/*#DG600 
        double dDiff;
        int iDiff; */
		Formattable nota;

		//* BORROWERFIRSTNAME
		logger.trace( ": Method compareBorrowers : BORROWERFIRSTNAME ");
		val1 = b1.getBorrowerFirstName();
		val2 = b2.getBorrowerFirstName();
		if (!isEqual(val1, val2))
		{
			/*#DG600 
            appendNote(BXResources.getDocPrepIngMsg("FIRST_NAME",lang) + DELIMITER + b1.getBorrowerFirstName() +
                                      DELIMITER + b2.getBorrowerFirstName());
        	totalDifferences++;                                      */
			nota = newTabedLine(DUP_CHEK_FIRST_NAME, val1, val2, null);
			addFormtb(nota);
			diffCount++;
		}
		if (maximumReached()) 
			return;

		//* BORROWERLASTNAME
		logger.trace( ": Method compareBorrowers : BORROWERLASTNAME ");
		val1 = b1.getBorrowerLastName();
		val2 = b2.getBorrowerLastName();
		if (!isEqual(val1, val2))
		{
			/*#DG600 
            appendNote(BXResources.getDocPrepIngMsg("LAST_NAME",lang) + DELIMITER + b1.getBorrowerLastName() +
                                     DELIMITER + b2.getBorrowerLastName());
        	totalDifferences++;                                     */
			nota = newTabedLine(DUP_CHEK_LAST_NAME, val1, val2, null);
			addFormtb(nota);
			diffCount++;
		}

		if (diffCount >= 2)
		{
			//#DG600 appendNote(BXResources.getDocPrepIngMsg("BORROWERS_ASSUMED_DIFFERENT",lang));
			nota = BXResources.newFormata(DUP_CHEK_BORROWERS_ASSUMED_DIFFERENT);
			dealNotesList.addElement(nota);    			
			return;
		}

		if (maximumReached()) return;

		//* BORROWERMIDDLEINITIAL
		logger.trace( ": Method compareBorrowers : BORROWERMIDDLEINITIAL ");
		val1 = b1.getBorrowerMiddleInitial();
		val2 = b2.getBorrowerMiddleInitial();
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("MIDDLE_INITIAL",lang) + DELIMITER + b1.getBorrowerMiddleInitial() +
                                          DELIMITER + b2.getBorrowerMiddleInitial());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_MIDDLE_INITIAL, val1, val2))
			return;

		//*    TOTALLIABILITYPAYMENTS
		logger.trace( ": Method compareBorrowers : TOTALLIABILITYPAYMENTS ");
		double dval1 = b1.getTotalLiabilityPayments();
		double dval2 = b2.getTotalLiabilityPayments();
		/*#DG600 
    if (dval1 != dval2)
    {
        val1 = formatCurrency(b1.getTotalLiabilityPayments());
        val2 = formatCurrency(b2.getTotalLiabilityPayments());
        dDiff = b2.getTotalLiabilityPayments() - b1.getTotalLiabilityPayments();
        appendNote(BXResources.getDocPrepIngMsg("TOTAL_LIABILITY_PAYMENTS",lang) + DELIMITER +
                   val1 + DELIMITER +
                   val2 + DELIMITER +
                   formatCurrency(dDiff));
        totalDifferences++;
    }
    if (maximumReached()) */
		if(comparDbl(DUP_CHEK_TOTAL_LIABILITY_PAYMENTS, dval1, dval2))
			return;

		//*    TOTALLIABILITYAMOUNT
		logger.trace( ": Method compareBorrowers : TOTALLIABILITYAMOUNT ");
		dval1 = b1.getTotalLiabilityAmount();
		dval2 = b2.getTotalLiabilityAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(b1.getTotalLiabilityAmount());
            val2 = formatCurrency(b2.getTotalLiabilityAmount());
            dDiff = b2.getTotalLiabilityAmount() - b1.getTotalLiabilityAmount();
            appendNote(BXResources.getDocPrepIngMsg("TOTAL_LIABILITY_AMOUNT",lang) + DELIMITER +
                       val1 + DELIMITER +
                       val2 + DELIMITER +
                       formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_TOTAL_LIABILITY_AMOUNT, dval1, dval2))
			return;

		//*    TOTALINCOMEAMOUNT
		logger.trace( ": Method compareBorrowers : TOTALINCOMEAMOUNT ");
		dval1 = b1.getTotalIncomeAmount();
		dval2 = b2.getTotalIncomeAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(b1.getTotalIncomeAmount());
            val2 = formatCurrency(b2.getTotalIncomeAmount());
            dDiff = b2.getTotalIncomeAmount() - b1.getTotalIncomeAmount();
            appendNote(BXResources.getDocPrepIngMsg("TOTAL_INCOME_AMOUNT",lang) + DELIMITER +
                       val1 + DELIMITER +
                       val2 + DELIMITER +
                       formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_TOTAL_INCOME_AMOUNT, dval1, dval2))
			return;

		//*    TOTALASSETAMOUNT
		logger.trace( ": Method compareBorrowers : TOTALASSETAMOUNT ");
		dval1 = b1.getTotalAssetAmount();
		dval2 = b2.getTotalAssetAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(b1.getTotalAssetAmount());
            val2 = formatCurrency(b2.getTotalAssetAmount());
            dDiff = b2.getTotalAssetAmount() - b1.getTotalAssetAmount();
            appendNote(BXResources.getDocPrepIngMsg("TOTAL_ASSET_AMOUNT",lang) + DELIMITER +
                       val1 + DELIMITER +
                       val2 + DELIMITER +
                       formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_TOTAL_ASSET_AMOUNT, dval1, dval2))
			return;

		//* SOCIALINSURANCENUMBER
		logger.trace( ": Method compareBorrowers : SOCIALINSURANCENUMBER ");
		val1 = b1.getSocialInsuranceNumber();
		val2 = b2.getSocialInsuranceNumber();
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("SIN",lang) + DELIMITER + StringUtil.formatSIN(b1.getSocialInsuranceNumber()) +
                                  DELIMITER + StringUtil.formatSIN(b2.getSocialInsuranceNumber()));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_SIN, StringUtil.formatSIN(val1), StringUtil.formatSIN(val2)))
			return;

		//* NUMBEROFTIMESBANKRUPT
		logger.trace( ": Method compareBorrowers : NUMBEROFTIMESBANKRUPT ");
		int ival1 = b1.getNumberOfTimesBankrupt();
		int ival2 = b2.getNumberOfTimesBankrupt();
		/*#DG600 
        if (ival1 != ival2)
        {
            iDiff = b2.getNumberOfTimesBankrupt() - b1.getNumberOfTimesBankrupt();
            appendNote(BXResources.getDocPrepIngMsg("NUMBER_OF_TIMES_BANKRUPT",lang) + DELIMITER + b1.getNumberOfTimesBankrupt() +
                                               DELIMITER + b2.getNumberOfTimesBankrupt() +
                                               DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_NUMBER_OF_TIMES_BANKRUPT, ival1, ival2))
			return;

		//* NUMBEROFDEPENDENTS
		logger.trace( ": Method compareBorrowers : NUMBEROFDEPENDENTS ");
		ival1 = b1.getNumberOfDependents();
		ival2 = b2.getNumberOfDependents();
		/*#DG600 
        if (ival1 != ival2)
        {
            iDiff = b2.getNumberOfDependents() - b1.getNumberOfDependents();
            appendNote(BXResources.getDocPrepIngMsg("NUMBER_OF_DEPENDENTS",lang) + DELIMITER + b1.getNumberOfDependents() +
                                           DELIMITER + b2.getNumberOfDependents() +
                                           DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_NUMBER_OF_DEPENDENTS, ival1, ival2))
			return;

		//* MARITALSTATUSID
		logger.trace( ": Method compareBorrowers : MARITALSTATUSID ");
		ival1 = b1.getMaritalStatusId();
		ival2 = b2.getMaritalStatusId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("MaritalStatus",  b1.getMaritalStatusId(), lang);
            val2  = BXResources.getPickListDescription("MaritalStatus",  b2.getMaritalStatusId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("MARITAL_STATUS",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_MARITAL_STATUS, PKL_MARITAL_STATUS, ival1, ival2))
			return;

		//* LANGUAGEPREFERENCEID
		logger.trace( ": Method compareBorrowers : LANGUAGEPREFERENCEID ");
		ival1 = b1.getLanguagePreferenceId();
		ival2 = b2.getLanguagePreferenceId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("LanguagePreference",  b1.getLanguagePreferenceId(), lang);
            val2  = BXResources.getPickListDescription("LanguagePreference",  b2.getLanguagePreferenceId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("LANGUAGE_PREFERENCE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_LANGUAGE_PREFERENCE, PKL_LANGUAGE_PREFERENCE, ival1, ival2))
			return;

		//* FIRSTTIMEBUYER
		logger.trace( ": Method compareBorrowers : FIRSTTIMEBUYER ");
		val1 = b1.getFirstTimeBuyer();
		val2 = b2.getFirstTimeBuyer();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("FIRST_TIME_BUYER",lang) + DELIMITER + b1.getFirstTimeBuyer() +
                                            DELIMITER + b2.getFirstTimeBuyer());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_FIRST_TIME_BUYER, val1, val2))
			return;

		//* CREDITSCORE
		logger.trace( ": Method compareBorrowers : CREDITSCORE ");
		ival1 = b1.getCreditScore();
		ival2 = b2.getCreditScore();
		/*#DG600 
        if (ival1 != ival2)
        {
            iDiff = b2.getCreditScore() - b1.getCreditScore();
            appendNote(BXResources.getDocPrepIngMsg("CREDIT_SCORE",lang) + DELIMITER + b1.getCreditScore() +
                                        DELIMITER + b2.getCreditScore() +
                                        DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_CREDIT_SCORE, ival1, ival2))
			return;

		//* CREDITBUREAUSUMMARY
		logger.trace( ": Method compareBorrowers : CREDITBUREAUSUMMARY ");
		val1 = b1.getCreditBureauSummary();
		val2 = b2.getCreditBureauSummary();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("CREDIT_BUREAU_SUMMARY",lang) + DELIMITER +
               convertCR(b1.getCreditBureauSummary()) + DELIMITER +
               convertCR(b2.getCreditBureauSummary()));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_CREDIT_BUREAU_SUMMARY, val1, val2))
			return;

		//* CREDITBUREAUONFILEDATE
		logger.trace( ": Method compareBorrowers : CREDITBUREAUONFILEDATE ");
		Date tval1 = b1.getCreditBureauOnFileDate();
		Date tval2 = b2.getCreditBureauOnFileDate();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            val1 = b1.getCreditBureauOnFileDate() == null ? "" : df.format(b1.getCreditBureauOnFileDate());
            val2 = b2.getCreditBureauOnFileDate() == null ? "" : df.format(b2.getCreditBureauOnFileDate());
            appendNote(BXResources.getDocPrepIngMsg("CREDIT_BUREAU_ON_FILE_DATE",lang) + DELIMITER + val1 + DELIMITER + val2);
        	totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDate(DUP_CHEK_CRED_BUR_FILE_DATE, tval1, tval2))
			return;

		//* CREDITBUREAUNAMEID
		logger.trace( ": Method compareBorrowers : CREDITBUREAUNAMEID ");
		ival1 = b1.getCreditBureauNameId();
		ival2 = b2.getCreditBureauNameId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("CreditBureauName",  b1.getCreditBureauNameId(), lang);
            val2  = BXResources.getPickListDescription("CreditBureauName",  b2.getCreditBureauNameId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("CREDIT_BUREAU_NAME",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_CREDIT_BUREAU_NAME, PKL_CREDIT_BUREAU_NAME, ival1, ival2))
			return;

		//* CITIZENSHIPTYPEID
		logger.trace( ": Method compareBorrowers : CITIZENSHIPTYPEID ");
		ival1 = b1.getCitizenshipTypeId();
		ival2 = b2.getCitizenshipTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("CitizenshipType",  b1.getCitizenshipTypeId(), lang);
            val2  = BXResources.getPickListDescription("CitizenshipType",  b2.getCitizenshipTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("CITIZENSHIP_STATUS",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_CITIZENSHIP_STATUS, PKL_CITIZENSHIP_TYPE, ival1, ival2))
			return;

		//* BUREAUATTACHMENT
		logger.trace( ": Method compareBorrowers : BUREAUATTACHMENT ");
		val1 = b1.getBureauAttachment();
		val2 = b2.getBureauAttachment();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("BUREAU_ATTACHMENT",lang) + DELIMITER + b1.getBureauAttachment() +
                                             DELIMITER + b2.getBureauAttachment());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_BUREAU_ATTACHMENT, val1, val2))
			return;

		//* BORROWERWORKPHONENUMBER
		logger.trace( ": Method compareBorrowers : BORROWERWORKPHONENUMBER ");
		val1 = b1.getBorrowerWorkPhoneNumber();
		val2 = b2.getBorrowerWorkPhoneNumber();	//#DG634
		if (!isEqual(val1, val2))
		{
			//#DG600 
			try
			{
				val1 = StringUtil.getAsFormattedPhoneNumber(val1, null);
			}
			catch (Exception e) {}

			try
			{
				val2 = StringUtil.getAsFormattedPhoneNumber(val2, null);
			}
			catch (Exception e) {}
			//appendNote(BXResources.getDocPrepIngMsg("WORK_PHONE_NUMBER",lang) + DELIMITER + val1 + DELIMITER + val2);
			//totalDifferences++;
			nota = newTabedLine(DUP_CHEK_WORK_PHONE_NUMBER, val1, val2, null);
			addFormtb(nota);
		}

		if (maximumReached()) return;

		//* BORROWERWORKPHONEEXTENSION
		logger.trace( ": Method compareBorrowers : BORROWERWORKPHONEEXTENSION ");
		val1 = b1.getBorrowerWorkPhoneExtension();
		val2 = b2.getBorrowerWorkPhoneExtension();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("BORROWER_WORK_PHONE_EXTENSION",lang) + DELIMITER + b1.getBorrowerWorkPhoneExtension() +
                                                DELIMITER + b2.getBorrowerWorkPhoneExtension());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_BORROWER_WORK_PHONE_EXT, val1, val2))
			return;

		//* BORROWERTYPEID
		logger.trace( ": Method compareBorrowers : BORROWERTYPEID ");
		ival1 = b1.getBorrowerTypeId();
		ival2 = b2.getBorrowerTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("BorrowerType",  b1.getBorrowerTypeId(), lang);
            val2  = BXResources.getPickListDescription("BorrowerType",  b2.getBorrowerTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_TYPE, PKL_BORROWER_TYPE, ival1, ival2))
			return;

		//* BORROWERHOMEPHONENUMBER
		logger.trace( ": Method compareBorrowers : BORROWERHOMEPHONENUMBER ");
		//#DG600 
		val1 = b1.getBorrowerHomePhoneNumber();
		val2 = b2.getBorrowerHomePhoneNumber();
		if (!isEqual(val1 ,val2))
		{
			try
			{
				val1 = StringUtil.getAsFormattedPhoneNumber(val1, null);
			}
			catch (Exception e) {}

			try
			{
				val2 = StringUtil.getAsFormattedPhoneNumber(val2, null);
			}
			catch (Exception e) {}
			//appendNote(BXResources.getDocPrepIngMsg("HOME_PHONE_NUMBER",lang) + DELIMITER + val1 + DELIMITER + val2);
			//totalDifferences++;
			nota = newTabedLine(DUP_CHEK_HOME_PHONE_NUMBER,val1, val2, null);
			addFormtb(nota);
		}
		if (maximumReached()) return;

		//* BORROWERGENERALSTATUSID
		logger.trace( ": Method compareBorrowers : BORROWERGENERALSTATUSID ");
		ival1 = b1.getBorrowerGeneralStatusId();
		ival2 = b2.getBorrowerGeneralStatusId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("BorrowerGeneralStatus",  b1.getBorrowerGeneralStatusId(), lang);
            val2  = BXResources.getPickListDescription("BorrowerGeneralStatus",  b2.getBorrowerGeneralStatusId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("BORROWER_GENERAL_STATUS",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_BORROWER_GENERAL_STATUS, PKL_BORROWER_GENERAL_STATUS, ival1, ival2))
			return;

		//* BORROWERFAXNUMBER
		logger.trace( ": Method compareBorrowers : BORROWERFAXNUMBER ");
		val1 = b1.getBorrowerFaxNumber();
		val2 = b2.getBorrowerFaxNumber();	//#DG634
		if (!isEqual(val1, val2))
		{
			//#DG600 
			try
			{
				val1 = StringUtil.getAsFormattedPhoneNumber(val1, null);
			}
			catch (Exception e) {}

			try
			{
				val2 = StringUtil.getAsFormattedPhoneNumber(val2, null);
			}
			catch (Exception e) {}

			//appendNote(BXResources.getDocPrepIngMsg("FAX_NUMBER",lang) + DELIMITER + val1 + DELIMITER + val2);
			//totalDifferences++;
			nota = newTabedLine(DUP_CHEK_FAX_NUMBER,val1, val2, null);
			addFormtb(nota);
		}
		if (maximumReached()) return;

		//* BORROWEREMAILADDRESS
		logger.trace( ": Method compareBorrowers : BORROWEREMAILADDRESS ");
		val1 = b1.getBorrowerEmailAddress();
		val2 = b2.getBorrowerEmailAddress();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("EMAIL_ADDRESS",lang) + DELIMITER + b1.getBorrowerEmailAddress() +
                                         DELIMITER + b2.getBorrowerEmailAddress());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_EMAIL_ADDRESS, val1, val2))
			return;

		//* BORROWERBIRTHDATE
		logger.trace( ": Method compareBorrowers : BORROWERBIRTHDATE ");
		tval1 = b1.getBorrowerBirthDate();
		tval2 = b2.getBorrowerBirthDate();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            val1 = b1.getBorrowerBirthDate() == null ? "" : df.format(b1.getBorrowerBirthDate());
            val2 = b2.getBorrowerBirthDate() == null ? "" : df.format(b2.getBorrowerBirthDate());
            appendNote(BXResources.getDocPrepIngMsg("DATE_OF_BIRTH",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDate(DATE_OF_BIRTH, tval1, tval2))
			return;

		//* BANKRUPTCYSTATUSID
		logger.trace( ": Method compareBorrowers : BANKRUPTCYSTATUSID ");
		ival1 = b1.getBankruptcyStatusId();
		ival2 = b2.getBankruptcyStatusId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("BankruptcyStatus",  b1.getBankruptcyStatusId(), lang);
            val2  = BXResources.getPickListDescription("BankruptcyStatus",  b2.getBankruptcyStatusId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("BANKRUPTCY_STATUS",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_BANKRUPTCY_STATUS, PKL_BANKRUPTCY_STATUS, ival1, ival2))
			return;

		//* AGE
		logger.trace( ": Method compareBorrowers : AGE ");
		ival1 = b1.getAge();
		ival2 = b2.getAge();
		/*#DG600 
        if (ival1 != ival2)
        {
            iDiff = b2.getAge() - b1.getAge();
            appendNote(BXResources.getDocPrepIngMsg("AGE",lang) + DELIMITER + b1.getAge() + DELIMITER + b2.getAge() +
                               DELIMITER + iDiff);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparInt(DUP_CHEK_AGE, ival1, ival2))
			return;

		//*
		//* BORROWERADDRESS
		//* ---------------
		//*
		logger.trace( ": Method compareBorrowers : BORROWERADDRESS ");
		try
		{
			//#DG600 List l1 = (List)b1.getBorrowerAddresses();
			//#DG600 List l2 = (List)b2.getBorrowerAddresses();
			List<BorrowerAddress> l1 = (List<BorrowerAddress>)b1.getBorrowerAddresses();
			List<BorrowerAddress> l2 = (List<BorrowerAddress>)b2.getBorrowerAddresses();

			doBorrowerAddressListCompare(l1, l2);
		}
		catch (Exception e) {}

		//*
		//* EMPLOYMENTHISTORY
		//* -----------------
		//*
		logger.trace( ": Method compareBorrowers : EMPLOYMENTHISTORY ");
		try
		{
			/*#DG600 
            List l1 = (List)b1.getEmploymentHistories();
            List l2 = (List)b2.getEmploymentHistories();*/
			List<EmploymentHistory> l1 = (List)b1.getEmploymentHistories();
			List<EmploymentHistory> l2 = (List)b2.getEmploymentHistories();

			doEmploymentHistoryListCompare(l1, l2);
		}
		catch (Exception e) {}

		//*
		//* INCOME
		//* ------
		//*
		logger.trace( ": Method compareBorrowers : INCOME ");
		try
		{
			/*#DG600 
            List l1 = (List)b1.getIncomes();
            List l2 = (List)b2.getIncomes();*/
			List<Income> l1 = (List)b1.getIncomes();
			List<Income> l2 = (List)b2.getIncomes();
			doIncomeListCompare(l1, l2);
		}
		catch (Exception e) {}

		//*
		//* LIABILITY
		//* ---------
		//*
		logger.trace( ": Method compareBorrowers : LIABILITY ");
		try
		{
			/*#DG600
            List l1 = (List)b1.getLiabilities();
            List l2 = (List)b2.getLiabilities();*/
			List<Liability> l1 = (List)b1.getLiabilities();
			List<Liability> l2 = (List)b2.getLiabilities();

			doLiabilityListCompare(l1, l2);
		}
		catch (Exception e) {}

		//*
		//* ASSETS
		//* ------
		//*
		logger.trace( ": Method compareBorrowers : ASSETS ");
		try
		{
			/*#DG600
            List l1 = (List)b1.getAssets();
            List l2 = (List)b2.getAssets();*/
			List<Asset> l1 = (List)b1.getAssets();
			List<Asset> l2 = (List)b2.getAssets();

			doAssetListCompare(l1, l2);
		}
		catch (Exception e) {}

		// --------------------- DJ, pvcs # 1201, Catherine -- begin ---------------------
		if (PropertiesCache.getInstance()
				.getProperty(srk.getExpressState().getDealInstitutionId(), 
				             COM_BASIS100_CALC_ISDJCLIENT, "N").equals("Y")){
			compareBorrowerA(b1, b2);
      if (maximumReached()) 
        return;
			// -----------------------
		}
		// --------------------- DJ, pvcs # 1201, Catherine -- end ---------------------

		//----------------NBC ,pvcs # 780, 5-Oct-05----Start
		// ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
		// * BORROWERGENDERID
		// * ------------
		logger.trace( ": Method compareBorrowers : BORROWERGENDERID ");

		/*#DG600 rewritten           
			val1 = "" + b1.getBorrowerGenderId();
			val2 = "" + b2.getBorrowerGenderId();
			if (!val1.equals(val2)) {
				appendNote(BXResources.getDocPrepIngMsg("BORROWERGENDERID",
						lang)
						+ DELIMITER
						+ ""
						+ BXResources.getPickListDescription("BORROWERGENDER",
								val1, lang)
						+ DELIMITER
						+ ""
						+ BXResources.getPickListDescription("BORROWERGENDER",
								val2, lang));

				totalDifferences++;

				if (maximumReached()) {
					logger.trace("Maximum reached. Returning...");
					return;
				}
			}*/
		ival1 =  b1.getBorrowerGenderId();           
		ival2 =  b2.getBorrowerGenderId();           
		if(comparPkL(DUP_CHEK_BORROWERGENDERID, PKL_BORROWERGENDER, ival1, ival2))
			return;

		// * STAFFOFLENDER 
		logger.trace( ": Method compareBorrowers : STAFFOFLENDER ");
		//#DG658
		boolean bval1 = b1.getStaffOfLender();
		boolean bval2 = b2.getStaffOfLender();
		/*
		if(b1.getStaffOfLender() != b2.getStaffOfLender()) {
			/ *#DG600
		  String label;          
		  // getStaffOfLender returns boolean value; write as yes or no in deal notes
          label = (b1.getStaffOfLender() ? YES_LABEL : NO_LABEL);
          val1  = BXResources.getGenericMsg(label, lang);          
          label = (b2.getStaffOfLender() ? YES_LABEL : NO_LABEL);
          val2  = BXResources.getGenericMsg(label, lang);                    
				appendNote(BXResources
						.getDocPrepIngMsg("STAFF_OF_LENDER", lang)
						+ DELIMITER
					+ val1
						+ DELIMITER
					+ val2);
				totalDifferences++;* /
			val1 = b1.getStaffOfLender() ? YES_LABEL : NO_LABEL;
			val2 = b2.getStaffOfLender() ? YES_LABEL : NO_LABEL;						
			nota = newTabedLine(DUP_CHEK_STAFF_OF_LENDER, 
					BXResources.newGenMsg(val1), 
					BXResources.newGenMsg(val2), null);				
			addFormtb(nota);				
			if (maximumReached()) 
				return;
		}*/
		if(comparBool(DUP_CHEK_STAFF_OF_LENDER, bval1, bval2))
			return;        

		// *
		// * BORROWERIDENTIFICATION
		// * ---------------
		// *
		logger.trace( ":"
				+ " Method compareBorrowersIdentifications : BORROWERIDENTIFICATIONS ");
		try {
			//#DG600 List l1 = (List) b1.getBorrowerIdentifications();
			//#DG600 List l2 = (List) b2.getBorrowerIdentifications();
			List<BorrowerIdentification> l1 = (List<BorrowerIdentification>) b1.getBorrowerIdentifications();
			List<BorrowerIdentification> l2 = (List<BorrowerIdentification>) b2.getBorrowerIdentifications();
			doBorrowerIdentificationListCompare(l1, l2);
		} catch (Exception e) {
			logger.error("DealCompare@@compareBorrowers::exception " + e);	//#DG634 
		}

		// * SOLICITATIONID
		// * ------------
		logger.trace( ": Method compareBorrowers : SOLICITATIONID ");
		/*#DG600
			val1 = "" + b1.getSolicitationId();
			val2 = "" + b2.getSolicitationId();
			if (!val1.equals(val2)) {
				appendNote(BXResources.getDocPrepIngMsg("SOLICITATIONID", lang)
						+ DELIMITER
						+ ""
						+ BXResources.getPickListDescription("SOLICITATION",
								val1, lang)
						+ DELIMITER
						+ ""
						+ BXResources.getPickListDescription("SOLICITATION",
								val2, lang));            
            totalDifferences++;            
            if (maximumReached()) {
              logger.trace("Maximum reached. Returning...");
              return;
            }
          }*/
		ival1 = b1.getSolicitationId();
		ival2 = b2.getSolicitationId();
		if(comparPkL(DUP_CHEK_SOLICITATIONID, PKL_SOLICITATION, ival1, ival2))
			return;

		// * EMPLOYEENUMBER
		logger.trace( ": Method compareBorrowers : EMPLOYEENUMBER ");
		val1 = b1.getEmployeeNumber();
		val2 = b2.getEmployeeNumber();	//#DG634
		/*#DG600 
			if (!isEqual(val1, val2)) {
				appendNote(BXResources.getDocPrepIngMsg("EMPLOYEENUMBER", lang)
						+ DELIMITER + b1.getEmployeeNumber() + DELIMITER
						+ b2.getEmployeeNumber());

				totalDifferences++;
			}
			if (maximumReached())*/
		if(comparStr(DUP_CHEK_EMPLOYEENUMBER, val1, val2))
			return;

		// * PREFCONTACTMETHODID
		// * ------------
		logger.trace( ": Method compareBorrowers : PREFCONTACTMETHODID ");

		/*#DG600 
			val1 = "" + b1.getPrefContactMethodId();
			val2 = "" + b2.getPrefContactMethodId();
			if (!val1.equals(val2)) { 
				appendNote(BXResources.getDocPrepIngMsg("PREFCONTACTMETHODID",
						lang)
						+ DELIMITER
						+ ""
						+ BXResources.getPickListDescription(
								"PREFMETHODOFCONTACT", val1, lang)
						+ DELIMITER
						+ ""
						+ BXResources.getPickListDescription(
								"PREFMETHODOFCONTACT", val2, lang));

				totalDifferences++;

				if (maximumReached()) {
					logger.trace("Maximum reached. Returning...");
					return;
				}
			} */
		ival1 = b1.getPrefContactMethodId();
		ival2 = b2.getPrefContactMethodId();
		if(comparPkL(DUP_CHEK_PREFCONTACTMETH_ID, PKL_PREFMETHODOFCONTACT, ival1, ival2))
			return;

		//* SMOKESTATUSID
		//* ---------------------
		logger.trace( ": Method compareBorrowers : SMOKESTATUSID ");

		/*#DG600 
        val1 = ""+ b1.getSmokeStatusId();           
        val2 = ""+ b2.getSmokeStatusId();
        if (!val1.equals(val2)) {
        			appendNote(BXResources.getDocPrepIngMsg("SMOKESTATUSID", lang)
					+ DELIMITER
					+ ""
					+ BXResources.getPickListDescription("BORROWERSMOKER",
							val1, lang)
					+ DELIMITER
					+ ""
					+ BXResources.getPickListDescription("BORROWERSMOKER",
							val2, lang));

          totalDifferences++;

          if (maximumReached()) {
            logger.trace("Maximum reached. Returning...");
            return;
          }
        }*/
		ival1 = b1.getSmokeStatusId();
		ival2 = b2.getSmokeStatusId();
		if(comparPkL(DUP_CHEK_SMOKESTATUSID, PKL_SMOKESTATUS, ival1, ival2))
			return;


		// ***** Change by NBC Impl. Team - Version 1.6 - End *****//
		// ----------------NBC ,pvcs # 780, 5-Oct-05-------End

        logger.trace( ": Method compareBorrowers : BorrowerCellPhoneNumber ");
		//* BorrowerCellPhoneNumber
        val1 = b1.getBorrowerCellPhoneNumber();
        val2 = b2.getBorrowerCellPhoneNumber();
        if(comparStr(DUP_CHEK_BORROWERCELLPHONE, val1, val2))
            return;
        
        logger.trace( ": Method compareBorrowers : SuffixId ");
        //* SuffixId
        ival1 = b1.getSuffixId();
        ival2 = b2.getSuffixId();
        if(comparPkL(DUP_CHEK_SUFFIXID, PKL_SUFFIX, ival1, ival2))
            return;
        
        logger.trace( ": Method compareBorrowers : CreditBureauAuthorizationDate ");
        //* CreditBureauAuthorizationDate
        tval1 = b1.getCbAuthorizationDate();
        tval2 = b2.getCbAuthorizationDate();
        if(comparDate(DUP_CHEK_CREDITBUREAUAUTHORIZATIONDATE, tval1, tval2))
            return;
        
        logger.trace( ": Method compareBorrowers : CreditBureauAuthorizationMethod ");
        //* CreditBureauAuthorizationMethod
        val1 = b1.getCbAuthorizationMethod();
        val2 = b2.getCbAuthorizationMethod();
        if(comparStr(DUP_CHEK_CREDITBUREAUAUTHORIZATIONMETHOD, val1, val2))
            return;
        
		logger.trace( ": Method compareBorrowers finished ");
	}

	/** #DG658 those additional fields are optionally compared
	 * 
	 * @param b1 borrower 1
	 * @param b2 borrower 2
	 */
	private void compareBorrowerA(Borrower b1, Borrower b2) {
		String val1;
		String val2;
		double dval1;
		double dval2;
		int ival1;
		int ival2;
		//*
		//* GUARANTOROTHERLOANS
		//* -------------------
		//*
		logger.trace( ": Method compareBorrowers : GUARANTOROTHERLOANS ");

		val1 = b1.getGuarantorOtherLoans();
		val2 = b2.getGuarantorOtherLoans();
		//#DG658
		boolean bval1 = val1.equalsIgnoreCase("Y");
		boolean bval2 = val2.equalsIgnoreCase("Y");
		/* 
		if (!(val1.equalsIgnoreCase(val2))) {
			/ *String label;            
		      label = (val1.equalsIgnoreCase("Y") ? YES_LABEL : NO_LABEL);
		      val1  = BXResources.getGenericMsg(label, lang);            
		      label = (val2.equalsIgnoreCase("Y") ? YES_LABEL : NO_LABEL);
		      val2  = BXResources.getGenericMsg(label, lang);
		      appendNote(BXResources.getDocPrepIngMsg("GUARANTOR_OTHER_LOANS", lang) + DELIMITER + val1 + DELIMITER + val2);
		      totalDifferences++;
			 * /
			val1 = val1.equalsIgnoreCase("Y") ? YES_LABEL : NO_LABEL;
			val2 = val2.equalsIgnoreCase("Y") ? YES_LABEL : NO_LABEL;						
			nota = newTabedLine(DUP_CHEK_GUAR_OTHER_LOANS, 
					BXResources.newGenMsg(val1), 
					BXResources.newGenMsg(val2), null);				
			addFormtb(nota);				
			if (maximumReached()) 
				return;
		}*/
		if(comparBool(DUP_CHEK_GUAR_OTHER_LOANS, bval1, bval2))
			return;    
		// --------------------- DJ, pvcs # 1683, 6-Jul-05 Catherine -- begin ---------------------

		//* INSURANCEPROPORTIONSID
		//* ----------------------
		logger.trace( ": Method compareBorrowers : INSURANCEPROPORTIONSID ");
		ival1 = b1.getInsuranceProportionsId();
		ival2 = b2.getInsuranceProportionsId();
		if(comparPkL(DUP_CHEK_INSUR_PROPORT_ID, PKL_INSURANCEPROPORTIONS, ival1, ival2))
			return;

		//* LIFEPERCENTCOVERAGEREQ
		//* ----------------------
		logger.trace( ": Method compareBorrowers : LIFEPERCENTCOVERAGEREQ ");
		dval1 = b1.getLifePercentCoverageReq();
		dval2 = b2.getLifePercentCoverageReq();
		if(comparDbl(DUP_CHEK_LIFEPERC_COVER_REQ, RATIO_FORMAT, dval1, dval2))            
			return;

		//* DISABILITYSTATUSID
		//* ------------------
		logger.trace( ": Method compareBorrowers : DISABILITYSTATUSID ");
		ival1 = b1.getDisabilityStatusId();
		ival2 = b2.getDisabilityStatusId();
		if(comparPkL(DUP_CHEK_DISAB_STATID, PKL_DISABILITYSTATUS, ival1, ival2))
			return;

		//* LIFESTATUSID
		//* ------------
		logger.trace( ": Method compareBorrowers : LIFESTATUSID ");
		ival1 = b1.getLifeStatusId();             
		ival2 = b2.getLifeStatusId();
		if(comparPkL(DUP_CHEK_LIFESTATUSID, PKL_LIFESTATUS, ival1, ival2))
			return;

		//* SMOKESTATUSID
		//* ---------------------
		logger.trace( ": Method compareBorrowers : SMOKESTATUSID ");
		/*#DG600 rewritten           
    val1 =  ""+ b1.getSmokeStatusId();           
    val2 = ""+ b2.getSmokeStatusId();
    if (!val1.equals(val2)) {
      switch (lang) {
      case 1:
        val1 = (b1.getSmokeStatusId()== 1 ? "O" : "N");
        val2 = (b2.getSmokeStatusId()== 1 ? "O" : "N");
        break;

      default:
        val1 = (b1.getSmokeStatusId()== 1 ? "Y" : "N");
        val2 = (b2.getSmokeStatusId()== 1 ? "Y" : "N");
        break;
      }
      //----------------NBC ,pvcs # 780, 23-Oct-06----Start            
      appendNote(BXResources.getDocPrepIngMsg("SMOKESTATUSID",lang)+ DELIMITER+ ""
				+ BXResources.getPickListDescription("BORROWERSMOKER",val1, lang)+ DELIMITER+ ""
				+ BXResources.getPickListDescription("BORROWERSMOKER",val2, lang));
      // ----------------NBC ,pvcs # 780, 23-Oct-06----End
      totalDifferences++;
      if (maximumReached()) {
        logger.trace("Maximum reached. Returning...");
        return;
      }
    }*/
		ival1 =  b1.getSmokeStatusId();           
		ival2 =  b2.getSmokeStatusId();
		/* #DG658
		bval1 = ival1 == 1;
		bval2 = ival2 == 1;
		if (ival1 != ival2) {
			val1 =  ival1 == 1 ? YES_LABEL : NO_LABEL;           
			val2 =  ival2 == 1 ? YES_LABEL : NO_LABEL;
			nota = newTabedLine(DUP_CHEK_SMOKESTATUSID, 
					BXResources.newGenMsg(val1), 
					BXResources.newGenMsg(val2), null);				
			addFormtb(nota);				
			if (maximumReached()) 
				return;
		}
		if(comparBool(DUP_CHEK_SMOKESTATUSID, bval1, bval2)) */
		if(comparPkL(DUP_CHEK_SMOKESTATUSID, PKL_SMOKESTATUS, ival1, ival2))
			return;

		// --------------------- DJ, pvcs # 1683, 6-Jul-05 Catherine -- end
	}

	// --------------------- DJ, pvcs # 1683, spec v2, 21-Sep-05 Catherine -- begin ---------------------
	// both deals must be known!
	public void compareInsureOnlyApplicants() {

		logger.trace(": Method compareInsureOnlyApplicants() entered");
		try {
			List<InsureOnlyApplicant> l1 = (List<InsureOnlyApplicant>) deal1.getInsureOnlyApplicants();
			List<InsureOnlyApplicant> l2 = (List<InsureOnlyApplicant>) deal2.getInsureOnlyApplicants();

			doInsureOnlyApplicantsListCompare(l1, l2);
		} catch (Exception e) {
			logger.trace( ": " + e.getMessage());
		}

		logger.trace( ": Method compareInsureOnlyApplicants() finished ");
	}

	protected void doInsureOnlyApplicantsListCompare(List<InsureOnlyApplicant> l1, List<InsureOnlyApplicant> l2){
		if (l1 == null || l2 == null)
			return;

		logger.trace( ": Method doInsureOnlyApplicantsListCompare() entered");
		InsureOnlyApplicant a1, a2;
		boolean found;
		//#DG600 String name; 
		Formattable nota;

		for (int i=0; i < l1.size(); i++)
		{
			a1 = (InsureOnlyApplicant)l1.get(i);
			//#DG600 name = a1.getInsureOnlyApplicantFirstName() + " " + a1.getInsureOnlyApplicantLastName();
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				a2 = (InsureOnlyApplicant)l2.get(j);

				if (isEqual(a1.getInsureOnlyApplicantFirstName(), a2.getInsureOnlyApplicantFirstName()) &&
						isEqual(a1.getInsureOnlyApplicantLastName(), a2.getInsureOnlyApplicantLastName()))
				{

					boolean bHasChanged;
					bHasChanged = 
						(!isEqual(a1.getInsureOnlyApplicantFirstName(), a2.getInsureOnlyApplicantFirstName()) ||
								!isEqual(a1.getInsureOnlyApplicantLastName(), a2.getInsureOnlyApplicantLastName()) ||
								!isEqual(a1.getInsureOnlyApplicantBirthDate(), a2.getInsureOnlyApplicantBirthDate()) ||
								(a1.getInsureOnlyApplicantGenderId() != a2.getInsureOnlyApplicantGenderId()) ||
								(a1.getInsuranceProportionsId() != a2.getInsuranceProportionsId()) || 
								(a1.getLifePercentCoverageReq() != a2.getLifePercentCoverageReq()) ||
								(a1.getDisabilityStatusId() != a2.getDisabilityStatusId()) ||
								(a1.getLifeStatusId() !=  a2.getLifeStatusId()) ||
								(a1.getSmokeStatusId() != a2.getSmokeStatusId()));

					if (bHasChanged)
					{
						/*#DG600
    					appendNote(NEW_LINE + "***  Insured Spouse '" + a1.getInsureOnlyApplicantFirstName() +
    							" " + a1.getInsureOnlyApplicantLastName() + "' Changed  ***");*/          
						nota = BXResources.newFormata(DUP_CHEK_INSUR_SPOU_CHANGED, 
								a1.getInsureOnlyApplicantFirstName(), a1.getInsureOnlyApplicantLastName());
						dealNotesList.addElement(nota);    		

						compareInsureOnlyApplicants(a1, a2);

						/*#DG600
    					appendNote(NEW_LINE + "***  End Insured Spouse '" + a1.getInsureOnlyApplicantFirstName() +
    							" " + a1.getInsureOnlyApplicantLastName() + "' Changed  ***");*/          
						nota = BXResources.newFormata(DUP_CHEK_INSUR_SPOE_CHANGED, 
								a1.getInsureOnlyApplicantFirstName(), a1.getInsureOnlyApplicantLastName());
						dealNotesList.addElement(nota);    		
					}

					//  We want to decrement the outer loop since the item was removed
					//  and the loop will increment.  The inner loop will begin again,
					//  so it doesn't matter what we do to it.
					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				/*#DG600
    			appendNote(NEW_LINE + "***  Insured Spouse  '" + name + "' Deleted  ***");*/          
				nota = BXResources.newFormata(DUP_CHEK_INSUR_SPOU_DELETED, 
						a1.getInsureOnlyApplicantFirstName(), a1.getInsureOnlyApplicantLastName());
				dealNotesList.addElement(nota);    		
				totalDifferences++;
			}
		}

		//  All remaining items in list 2 are considered new
		for(InsureOnlyApplicant a: l2)
		{
			/*#DG600
    		a = (InsureOnlyApplicant)i.next();
    		name = a.getInsureOnlyApplicantFirstName() + " " + a.getInsureOnlyApplicantLastName();
    		appendNote(NEW_LINE + "***  Insured Spouse  '" + name + "' Added  ***");*/          
			nota = BXResources.newFormata(DUP_CHEK_INSUR_SPOU_ADDED, 
					a.getInsureOnlyApplicantFirstName(), a.getInsureOnlyApplicantLastName());
			dealNotesList.addElement(nota);    		
			totalDifferences++;
		}
		logger.trace( ":" + " Method doInsureOnlyApplicantsListCompare() end");
	}

	private void compareInsureOnlyApplicants(InsureOnlyApplicant a1, InsureOnlyApplicant a2) {
		logger.trace( ": Method compareInsureOnlyApplicants entered ");

		String val1, val2;
		Formattable nota;		//#DG600 

		if (maximumReached()) 
			return;

		//* FIRSTNAME
		logger.trace( ": Method compareInsureOnlyApplicants : FIRSTNAME ");
		val1 = a1.getInsureOnlyApplicantFirstName();
		val2 = a2.getInsureOnlyApplicantFirstName();
		/*#DG600
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("FIRST_NAME", lang) + DELIMITER + val1 +  DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached())*/
		if(comparStr(DUP_CHEK_FIRST_NAME, val1, val2)) 
			return;

		//* LASTNAME
		logger.trace( ":" + " Method compareInsureOnlyApplicants : LASTNAME ");
		val1 = a1.getInsureOnlyApplicantLastName();
		val2 = a2.getInsureOnlyApplicantLastName();
		/*#DG600
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("LAST_NAME", lang) + DELIMITER + val1 +  DELIMITER + val2);

            totalDifferences++;
        }
        if (maximumReached())*/
		if(comparStr(DUP_CHEK_LAST_NAME, val1, val2)) 
			return;

		//* BIRTHDATE
		logger.trace( ":" + " Method compareInsureOnlyApplicants : BIRTHDATE ");
		Date tval1 = a1.getInsureOnlyApplicantBirthDate();
		Date tval2 = a2.getInsureOnlyApplicantBirthDate();
		/*#DG600
        val1 = a1.getInsureOnlyApplicantBirthDate() == null ? "" : df.format(a1.getInsureOnlyApplicantBirthDate());
        val2 = a2.getInsureOnlyApplicantBirthDate() == null ? "" : df.format(a2.getInsureOnlyApplicantBirthDate());
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("DATE_OF_BIRTH",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDate(DATE_OF_BIRTH, tval1, tval2))
			return;

		//* GENDER
		logger.trace( ":" + " Method compareInsureOnlyApplicants : GENDER ");
		int ival1 = a1.getInsureOnlyApplicantGenderId();
		int ival2 = a2.getInsureOnlyApplicantGenderId();
		/*#DG600
 				val1 = getGenderLetter(ival1, lang);
				val2 = getGenderLetter(ival2, lang);
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("GENDER", lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) 
        {
          logger.trace("Maximum reached. Returning...");
          return;
        }*/
		if (ival1 != ival2)  {
			nota = newTabedLine(GENDER, 
					newGenderFormata(ival1), newGenderFormata(ival2), null);
			addFormtb(nota);   		
			if (maximumReached()) 
				return;
		}


		//* INSURANCEPROPORTIONSID
		//* ----------------------
		logger.trace( ": Method compareInsureOnlyApplicants : INSURANCEPROPORTIONSID ");
		/*#DG600
        val1 = ""+ a1.getInsuranceProportionsId();   
        val2 = ""+ a2.getInsuranceProportionsId();
        if (!val1.equals(val2)) {

          val1 = "" + BXResources.getPickListDescription("INSURANCEPROPORTIONS", a1.getInsuranceProportionsId(), lang);
          val2 = "" + BXResources.getPickListDescription("INSURANCEPROPORTIONS", a2.getInsuranceProportionsId(), lang);

          appendNote(BXResources.getDocPrepIngMsg("INSURANCEPROPORTIONSID", lang) + DELIMITER + val1 + DELIMITER + val2);

          totalDifferences++;

          if (maximumReached()) {
            logger.trace("Maximum reached. Returning...");
            return;
          }
        }*/
		ival1 = a1.getInsuranceProportionsId();   
		ival2 = a2.getInsuranceProportionsId();
		if(comparPkL(DUP_CHEK_INSUR_PROPORT_ID, PKL_INSURANCEPROPORTIONS, ival1, ival2)) 
			return;

		//* LIFEPERCENTCOVERAGEREQ
		//* ----------------------
		logger.trace( ": Method compareInsureOnlyApplicants : LIFEPERCENTCOVERAGEREQ ");
		/*#DG600 
        val1 = formatRatio(a1.getLifePercentCoverageReq());   
        val2 = formatRatio(a2.getLifePercentCoverageReq());
        if (!val1.equals(val2)) {
          appendNote(BXResources.getDocPrepIngMsg("LIFEPERCENTCOVERAGEREQ", lang) + DELIMITER + val1 + DELIMITER + val2);

          totalDifferences++;

          if (maximumReached()) {
            logger.trace("Maximum reached. Returning...");
            return;
          }
        }*/
		double dval1 = a1.getLifePercentCoverageReq();   
		double dval2 = a2.getLifePercentCoverageReq();
		if(comparDbl(DUP_CHEK_LIFEPERC_COVER_REQ, RATIO_FORMAT, dval1, dval2))  
			return;

		//* DISABILITYSTATUSID
		//* ------------------
		logger.trace( ": Method compareInsureOnlyApplicants : DISABILITYSTATUSID ");
		/*#DG600 
        val1 = ""+ a1.getDisabilityStatusId();       
        val2 = ""+ a2.getDisabilityStatusId();
        if (!val1.equals(val2)) {
          appendNote(BXResources.getDocPrepIngMsg("DISABILITYSTATUSID", lang) + DELIMITER 
              + "" + BXResources.getPickListDescription("DISABILITYSTATUS", val1, lang) + DELIMITER 
              + "" + BXResources.getPickListDescription("DISABILITYSTATUS", val2, lang));

          totalDifferences++;

          if (maximumReached()) {
            logger.trace("Maximum reached. Returning...");
            return;
          }
        }*/
		ival1 = a1.getDisabilityStatusId();   
		ival2 = a2.getDisabilityStatusId();
		if(comparPkL(DUP_CHEK_DISAB_STATID, PKL_DISABILITYSTATUS, ival1, ival2)) 
			return;

		//* LIFESTATUSID
		//* ------------
		logger.trace( ": Method compareInsureOnlyApplicants : LIFESTATUSID ");
		/*#DG600 
        val1 = ""+ a1.getLifeStatusId();             
        val2 = ""+ a2.getLifeStatusId();
        if (!val1.equals(val2)) {
          appendNote(BXResources.getDocPrepIngMsg("LIFESTATUSID", lang) + DELIMITER 
              + "" + BXResources.getPickListDescription("LIFESTATUS", val1, lang) + DELIMITER 
              + "" + BXResources.getPickListDescription("LIFESTATUS", val2, lang));

          totalDifferences++;

          if (maximumReached()) {
            logger.trace("Maximum reached. Returning...");
            return;
          }
        }*/
		ival1 = a1.getLifeStatusId();   
		ival2 = a2.getLifeStatusId();
		if(comparPkL(DUP_CHEK_LIFESTATUSID, PKL_LIFESTATUS, ival1, ival2)) 
			return;


		//* SMOKESTATUSID
		//* ---------------------
		logger.trace( ": Method compareInsureOnlyApplicants : SMOKESTATUSID ");
		/*#DG600 
        val1 =  ""+ a1.getSmokeStatusId();           
        val2 = ""+ a2.getSmokeStatusId();
        if (!val1.equals(val2)) {
          switch (lang) {
          case 1:
            val1 = (a1.getSmokeStatusId()== 1 ? "O" : "N");
            val2 = (a2.getSmokeStatusId()== 1 ? "O" : "N");
            break;

          default:
            val1 = (a1.getSmokeStatusId()== 1 ? "Y" : "N");
            val2 = (a2.getSmokeStatusId()== 1 ? "Y" : "N");
            break;
          }          
          appendNote(BXResources.getDocPrepIngMsg("SMOKESTATUSID", lang) + DELIMITER + val1 + DELIMITER + val2);          
          totalDifferences++;          
          if (maximumReached()) {
            logger.trace("Maximum reached. Returning...");
            return;
          }
        }*/
		ival1 = a1.getSmokeStatusId();   
		ival2 = a2.getSmokeStatusId();
		if(comparPkL(DUP_CHEK_SMOKESTATUSID, PKL_SMOKESTATUS, ival1, ival2))
			return;
	}

	/*#DG600
	 * returns the abreviature for the gender for that language
    public String getGenderLetter(int genderId, int lang)
    {
      String val = "";
      switch (genderId) {
          case Mc.INSURER_ONLY_APPLICANT_GENDER_MALE : {
            val = ((lang == Mc.LANGUAGE_PREFERENCE_FRENCH) ? "H" : "M");
            break;
        }   
            case Mc.INSURER_ONLY_APPLICANT_GENDER_FEMALE : {
                val = "F";
                break;
          } 
      }
      return val;
    }   */
	public Formattable newGenderFormata(int genderId)
	{
		Formattable forma;
		switch (genderId) {
		case Mc.INSURER_ONLY_APPLICANT_GENDER_MALE : {
			forma = BXResources.newFormata(MALE_ABREVIAT);
			break;
		}   
		case Mc.INSURER_ONLY_APPLICANT_GENDER_FEMALE : {
			forma = BXResources.newFormata(FEMALE_ABREVIAT);
			break;
		} 
		default:
			forma = BXResources.newStrFormata("");
		break;
		}
		return forma;
	}   
	// --------------------- DJ, pvcs # 1683, spec v2, 21-Sep-05 Catherine -- end ---------------------

	/**
	 * The following fields are printed on the <code>BorrowerAddress</code>:
	 * <p><code><blockquote><pre>
	 *    BORROWERADDRESSTYPEID
	 *    MONTHSATADDRESS
	 *    RESIDENTIALSTATUSID
	 *
	 *    ADDR
	 *    ----
	 *
	 *    ADDRESSLINE1
	 *    ADDRESSLINE2
	 *    CITY
	 *    POSTALFSA
	 *    POSTALLDU
	 *    PROVINCEID
	 * </pre></blockquote></code>
	 *
	 * @param ba the <code>BorrowerAddress</code> to use a source.
	 * @param offset determines which column to print the information to.
	 *		1 for the first data column, otherwise second column.
	 */
	public Vector<Formattable> printBorrowerAddress(BorrowerAddress ba, int offset)
	{
		String val;
		Addr addr;
		//#DG600 StringBuffer buff = new StringBuffer();
		Formattable nota;	
		Vector<Formattable> dnl = new Vector<Formattable>();

		//* BORROWERADDRESSTYPEID
		/*#DG600
        val  = BXResources.getPickListDescription("BorrowerAddressType",  ba.getBorrowerAddressTypeId(), lang);
        //buff.append("Borrower Address Type:" + DELIMITER + val + NEW_LINE);
        buff.append(BXResources.getDocPrepIngMsg("BORROWER_ADDRESS_TYPE",lang) + DELIMITER + val + NEW_LINE);*/
		int ival = ba.getBorrowerAddressTypeId();
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_BORROWER_ADDRESS_TYPE, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_BORROWER_ADDRESS_TYPE, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_BORROWER_ADDRESS_TYPE, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_BORROWER_ADDRESS_TYPE, ival), null);
			
		dnl.addElement(nota);

		//*
		//* ADDR
		//* ----
		//*

		try
		{
			addr = ba.getAddr();

			//* ADDRESSLINE1
			val = addr.getAddressLine1();
			if (!isEmpty(val)) {
				//buff.append("Address Line 1:" + DELIMITER + val + NEW_LINE);
				//#DG600 buff.append(BXResources.getDocPrepIngMsg("ADDRESS_LINE_1",lang) + DELIMITER + val + NEW_LINE);
				if (offset == 1)
					nota = newTabedLine(DUP_CHEK_ADDRESS_LINE_1, val, null, null);
				else
					nota = newTabedLine(DUP_CHEK_ADDRESS_LINE_1, null, val, null);
				dnl.addElement(nota);
			}

			//* ADDRESSLINE2
			val = addr.getAddressLine2();
			if (!isEmpty(val))	{
				//buff.append("Address Line 2:" + DELIMITER + val + NEW_LINE);
				//#DG600 buff.append(BXResources.getDocPrepIngMsg("ADDRESS_LINE_2",lang) + DELIMITER + val + NEW_LINE);
				if (offset == 1)
					nota = newTabedLine(DUP_CHEK_ADDRESS_LINE_2, val, null, null);
				else
					nota = newTabedLine(DUP_CHEK_ADDRESS_LINE_2, null, val, null);					
				dnl.addElement(nota);
			}

			//* CITY
			val = addr.getCity();
			if (!isEmpty(val))	{
				//buff.append("City:" + DELIMITER + addr.getCity() + NEW_LINE);
				//#DG600 buff.append(BXResources.getDocPrepIngMsg("CITY",lang) + DELIMITER + addr.getCity() + NEW_LINE);
				if (offset == 1)
					nota = newTabedLine(DUP_CHEK_CITY, val, null, null);
				else
					nota = newTabedLine(DUP_CHEK_CITY, null, val, null);
				dnl.addElement(nota);
			}

			//* POSTAL CODE
			val = addr.getPostalFSA();
			if (val != null)
				val += " " + addr.getPostalLDU();
			if (!isEmpty(val))	{
				//buff.append("Postal Code:" + DELIMITER + val + NEW_LINE);
				//#DG600 buff.append(BXResources.getDocPrepIngMsg("POSTAL_CODE",lang) + DELIMITER + val + NEW_LINE);
				if (offset == 1)
					nota = newTabedLine(DUP_CHEK_POSTAL_CODE, val, null, null);
				else
					nota = newTabedLine(DUP_CHEK_POSTAL_CODE, null, val, null);
					
				dnl.addElement(nota);
			}
			//* PROVINCEID
			ival = addr.getProvinceId();
			/*#DG600 
            Province prov = new Province(srk, ival);
            val = prov.getProvinceName();
            //buff.append("Province:" + DELIMITER + val + NEW_LINE);
          	buff.append(BXResources.getDocPrepIngMsg("PROVINCE",lang) + DELIMITER + val + NEW_LINE);*/
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_PROVINCE, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PROVINCE, ival), null, null);
			else
				nota = newTabedLine(DUP_CHEK_PROVINCE, null, 
						BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PROVINCE, ival), null);
				
			dnl.addElement(nota);
		}
		catch (Exception e)
		{
			logger.error(DealCompare.class+":Exception in printBorrowerAddresses(): ");
			logger.error(e);
		}

		//* MONTHSATADDRESS
		ival = ba.getMonthsAtAddress();
		/*#DG600 
      	buff.append("Time at:" + DELIMITER +
           formatYearsandMonths(ba.getMonthsAtAddress()) + NEW_LINE);*/
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_TIME_AT, 
				BXResources.newYearsMonths(ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_TIME_AT, null, 
				BXResources.newYearsMonths(ival), null);
			
		dnl.addElement(nota);

		//* RESIDENTIALSTATUSID
		ival = ba.getResidentialStatusId();
		/*#DG600 
        val  = BXResources.getPickListDescription(RESIDENTIAL_STATUS,  ba.getResidentialStatusId(), lang);
        //buff.append("Residential Status:" + DELIMITER + val + NEW_LINE);
      	buff.append(BXResources.getDocPrepIngMsg("RESIDENTIAL_STATUS",lang) + DELIMITER + val + NEW_LINE);*/
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_RESIDENTIAL_STATUS, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_RESIDENTIAL_STATUS, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_RESIDENTIAL_STATUS, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_RESIDENTIAL_STATUS, ival), null);
			
		dnl.addElement(nota);
		return dnl;
	}

	/**
	 * Compares key fields in a pair of BorrowerAddresses.
	 * <P>
	 * The following fields are compared on each <code>BorrowerAddress</code>:
	 * <p><code><blockquote><pre>
	 *    BORROWERADDRESSTYPEID
	 *    MONTHSATADDRESS
	 *    RESIDENTIALSTATUSID
	 *
	 *    ADDR
	 *    ----
	 *
	 *    ADDRESSLINE1
	 *    ADDRESSLINE2
	 *    CITY
	 *    POSTALFSA
	 *    POSTALLDU
	 *    PROVINCEID
	 * </pre></blockquote></code>
	 */
	public void compareBorrowerAddresses(BorrowerAddress ba1, BorrowerAddress ba2)
	{
		if (maximumReached()) return;

		String val1, val2;

		//* BORROWERADDRESSTYPEID
		int ival1 = ba1.getBorrowerAddressTypeId();
		int ival2 = ba2.getBorrowerAddressTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("BorrowerAddressType", ba1.getBorrowerAddressTypeId(), lang);
            val2  = BXResources.getPickListDescription("BorrowerAddressType", ba2.getBorrowerAddressTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("BORROWER_ADDRESS_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_BORROWER_ADDRESS_TYPE, PKL_BORROWER_ADDRESS_TYPE, ival1, ival2))
			return;

		//* MONTHSATADDRESS
		ival1 = ba1.getMonthsAtAddress();
		ival2 = ba2.getMonthsAtAddress();
		/*#DG600 
        if (ival1 != ival2)
        {
            iDiff = ba2.getMonthsAtAddress() - ba1.getMonthsAtAddress();
            appendNote(BXResources.getDocPrepIngMsg("TIME_AT",lang) + DELIMITER + formatYearsandMonths(ba1.getMonthsAtAddress()) +
                                   DELIMITER + formatYearsandMonths(ba2.getMonthsAtAddress()) +
                                   DELIMITER + formatYearsandMonths(iDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparYrMth(DUP_CHEK_TIME_AT, ival1, ival2))
			return;

		//* RESIDENTIALSTATUSID
		ival1 = ba1.getResidentialStatusId();
		ival2 = ba2.getResidentialStatusId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("ResidentialStatus", ba1.getResidentialStatusId(), lang);
            val2  = BXResources.getPickListDescription("ResidentialStatus", ba2.getResidentialStatusId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("RESIDENTIAL_STATUS",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_RESIDENTIAL_STATUS, PKL_RESIDENTIAL_STATUS, ival1, ival2))
			return;

		//*
		//* ADDR
		//* ----
		//*
		Addr addr1 = null, addr2 = null;

		try
		{
			addr1 = ba1.getAddr();
		}
		catch (Exception e) {}

		try
		{
			addr2 = ba2.getAddr();
		}
		catch (Exception e) {}

		//* ADDRESSLINE1
		val1 = addr1.getAddressLine1();
		val2 = addr2.getAddressLine1();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("ADDRESS_LINE_1",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_ADDRESS_LINE_1, val1, val2))
			return;

		//* ADDRESSLINE2
		val1 = addr1.getAddressLine2();
		val2 = addr2.getAddressLine2();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("ADDRESS_LINE_2",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_ADDRESS_LINE_2, val1, val2))
			return;

		//* CITY
		val1 = addr1.getCity();
		val2 = addr2.getCity();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("CITY",lang) + DELIMITER + addr1.getCity() +
                                DELIMITER + addr2.getCity());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_CITY, val1, val2))		//#DG648
			return;

		//* PROPERTY POSTAL CODE
		/*#DG600 rewritten
        val1 = addr1.getPostalFSA() + " " + addr1.getPostalLDU();
        val2 = addr2.getPostalFSA() + " " + addr2.getPostalLDU();
        if (val1.equals("null null"))
           val1 = null;
        if (val2.equals("null null"))
           val2 = null;*/
		val1 = addr1.getPostalFSA() == null && addr1.getPostalFSA() == null?
				"null": addr1.getPostalFSA() + " " + addr1.getPostalLDU();
		val2 = addr2.getPostalFSA() == null && addr2.getPostalFSA() == null?
				"null": addr2.getPostalFSA() + " " + addr2.getPostalLDU();

		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("POSTAL_CODE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_POSTAL_CODE, val1, val2))
			return;

		//* PROVINCEID
		ival1 = addr1.getProvinceId();
		ival2 = addr2.getProvinceId();
		/*#DG600 
        if (ival1 != ival2)
        {
        	Province prov1=null, prov2=null;
        	try
        	{
        		prov1 = new Province(srk, addr1.getProvinceId());
        	}
        	catch (Exception e) {}
        	try
        	{
        		prov2 = new Province(srk, addr2.getProvinceId());
        	}
        	catch (Exception e) {}
        	val1 = prov1 != null ? prov1.getProvinceName() : null;
        	val2 = prov2 != null ? prov2.getProvinceName() : null;

        	appendNote(BXResources.getDocPrepIngMsg("PROVINCE",lang) + DELIMITER + val1 + DELIMITER + val2);
        	totalDifferences++;
        }*/
		if(comparPkL(DUP_CHEK_PROVINCE, PKL_PROVINCE, ival1, ival2))
			return;
	}

	/**
	 * The following fields are printed on the <code>EmploymentHistory</code>:
	 * <p><code><blockquote><pre>
	 *    EMPLOYERNAME
	 *    EMPLOYMENTHISTORYSTATUSID
	 *    EMPLOYMENTHISTORYTYPEID
	 *    INDUSTRYSECTORID
	 *    JOBTITLE
	 *    JOBTITLEID
	 *    MONTHSOFSERVICE
	 *    OCCUPATIONID
	 * </pre></blockquote></code>
	 *
	 * @param eh the <code>EmploymentHistory</code> to use a source.
	 */
	//#DG600 
	public Vector<Formattable> printEmploymentHistory(EmploymentHistory eh, int offset)
	{
		String val;
		//#DG600 StringBuffer buff = new StringBuffer();
		Vector<Formattable> dnl = new Vector<Formattable>();	
		Formattable nota;	 

		val = eh.getEmployerName();
		//* EMPLOYERNAME
		if (!isEmpty(val)) {
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_EMPLOYER_NAME, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_EMPLOYER_NAME, null, val, null);
			dnl.addElement(nota);
		}
		//* EMPLOYMENTHISTORYSTATUSID
		/*#DG600 
    	val  = BXResources.getPickListDescription(EMPLOYMENT_HISTORY_STATUS, eh.getEmploymentHistoryStatusId(), lang);
    	//buff.append("Employment History Status:" + DELIMITER + val + NEW_LINE);
    	buff.append(BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY_STATUS",lang) + DELIMITER + val + NEW_LINE);*/
		int ival = eh.getEmploymentHistoryStatusId();
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_EMPLOY_HIST_STATUS, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_EMPLOYMENT_HISTORY_STATUS, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_EMPLOY_HIST_STATUS, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_EMPLOYMENT_HISTORY_STATUS, ival), null);
		dnl.addElement(nota);


		//* EMPLOYMENTHISTORYTYPEID
		/*#DG600 
    	val  = BXResources.getPickListDescription(EMPLOYMENT_HISTORY_STATUS, eh.getEmploymentHistoryTypeId(), lang);
    	buff.append(BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY_TYPE",lang) + DELIMITER + val + NEW_LINE);*/
		ival = eh.getEmploymentHistoryTypeId();
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_EMPLOY_HIST_TYPE, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_EMPLOYMENT_HISTORY_STATUS, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_EMPLOY_HIST_TYPE, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_EMPLOYMENT_HISTORY_STATUS, ival), null);
		dnl.addElement(nota);

		//* INDUSTRYSECTORID
		/*#DG600 
    	val  = BXResources.getPickListDescription(INDUSTRY_SECTOR, eh.getIndustrySectorId(), lang);
    	buff.append(BXResources.getDocPrepIngMsg("INDUSTRY_SECTOR",lang) + DELIMITER + val + NEW_LINE);*/
		ival = eh.getIndustrySectorId();
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_INDUSTRY_SECTOR, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INDUSTRY_SECTOR, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_INDUSTRY_SECTOR, null,
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INDUSTRY_SECTOR, ival), null);
		dnl.addElement(nota);

		//* JOBTITLE
		val = eh.getJobTitle();
		if (!isEmpty(val)) {
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_JOB_TITLE, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_JOB_TITLE, null, val, null);
			dnl.addElement(nota);
		}
		//* JOBTITLEID
		/*#DG600 
    	val  = BXResources.getPickListDescription(JOB_TITLE, eh.getJobTitleId(), lang);
    	buff.append(BXResources.getDocPrepIngMsg("JOB_TITLE_CATEGORY",lang) + DELIMITER + val + NEW_LINE);*/
		ival = eh.getJobTitleId();
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_JOB_TITLE_CATEGORY, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_JOB_TITLE, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_JOB_TITLE_CATEGORY, null, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_JOB_TITLE, ival), null);
		dnl.addElement(nota);

		//* MONTHSOFSERVICE
		/*#DG600 
    	buff.append(BXResources.getDocPrepIngMsg("TIME_AT_JOB",lang) + DELIMITER + 
    		formatYearsandMonths(eh.getMonthsOfService()) + NEW_LINE);*/
		ival = eh.getMonthsOfService();
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_TIME_AT_JOB, 
				BXResources.newYearsMonths(ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_TIME_AT_JOB, null, 
					BXResources.newYearsMonths(ival), null);
		dnl.addElement(nota);

		//* OCCUPATIONID
		ival = eh.getOccupationId();
		if (offset == 1)
			nota = newTabedLine(DUP_CHEK_OCCUPATION, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_OCCUPATION, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_OCCUPATION, null, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_OCCUPATION, ival), null);
		dnl.addElement(nota);

		//*
		//* CONTACT
		//* -------
		//*
		try
		{
			dnl.addAll(printContact(eh.getContact(), offset));
		}
		catch (Exception e) {}

		//*
		//* INCOME
		//* -------
		//*
		try
		{
			//#DG600 buff.append(printIncome(eh.getIncome()));
			dnl.addAll(printIncome(eh.getIncome(), offset));
		}
		catch (Exception e) {}
		//#DG600 return buff.toString();
		return dnl;
	}

	/**
	 * Compares key fields in a pair of EmploymentHistories.
	 * <P>
	 * The following fields are compared on each <code>EmploymentHistory</code>:
	 * <p><code><blockquote><pre>
	 *    EMPLOYERNAME
	 *    EMPLOYMENTHISTORYSTATUSID
	 *    EMPLOYMENTHISTORYTYPEID
	 *    INDUSTRYSECTORID
	 *    JOBTITLE
	 *    JOBTITLEID
	 *    MONTHSOFSERVICE
	 *    OCCUPATIONID
	 * </pre></blockquote></code>
	 */
	public void compareEmploymentHistories(EmploymentHistory eh1, EmploymentHistory eh2)
	{
		if (maximumReached()) return;

		String val1, val2;

		//* EMPLOYERNAME
		val1 = eh1.getEmployerName();
		val2 = eh2.getEmployerName();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("EMPLOYER_NAME",lang) + DELIMITER + eh1.getEmployerName() +
                                         DELIMITER + eh2.getEmployerName());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_EMPLOYER_NAME, val1, val2))
			return;

		//* EMPLOYMENTHISTORYSTATUSID
		int ival1 = eh1.getEmploymentHistoryStatusId();
		int ival2 = eh2.getEmploymentHistoryStatusId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription(EMPLOYMENT_HISTORY_STATUS, eh1.getEmploymentHistoryStatusId(), lang);
            val2  = BXResources.getPickListDescription(EMPLOYMENT_HISTORY_STATUS, eh2.getEmploymentHistoryStatusId(), lang);
            //appendNote("Employment History Status" + DELIMITER + val1 + DELIMITER + val2);
            appendNote(BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY_STATUS",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_EMPLOY_HIST_STATUS, PKL_EMPLOYM_HIST_STAT, ival1, ival2))
			return;

		//* EMPLOYMENTHISTORYTYPEID
		ival1 = eh1.getEmploymentHistoryTypeId();
		ival2 = eh2.getEmploymentHistoryTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("EmploymentHistoryType", eh1.getEmploymentHistoryTypeId(), lang);
            val2  = BXResources.getPickListDescription("EmploymentHistoryType", eh2.getEmploymentHistoryTypeId(), lang);
            //appendNote("Employment History Type" + DELIMITER + val1 + DELIMITER + val2);
            appendNote(BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_EMPLOY_HIST_TYPE, PKL_EMPLOYMENT_HISTORY_TYPE, ival1, ival2))
			return;

		//* INDUSTRYSECTORID
		ival1 = eh1.getIndustrySectorId();
		ival2 = eh2.getIndustrySectorId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("IndustrySector", eh1.getIndustrySectorId(), lang);
            val2  = BXResources.getPickListDescription("IndustrySector", eh2.getIndustrySectorId(), lang);
            //appendNote("Industry Sector" + DELIMITER + val1 + DELIMITER + val2);
            appendNote(BXResources.getDocPrepIngMsg("INDUSTRY_SECTOR",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_INDUSTRY_SECTOR, PKL_INDUSTRY_SECTOR, ival1, ival2))
			return;

		//* JOBTITLE
		val1 = eh1.getJobTitle();
		val2 = eh2.getJobTitle();
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("JOB_TITLE",lang) + DELIMITER + eh1.getJobTitle() +
                                     DELIMITER + eh2.getJobTitle());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_JOB_TITLE, val1, val2))
			return;

		//* JOBTITLEID
		ival1 = eh1.getJobTitleId();
		ival2 = eh2.getJobTitleId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("JobTitle", eh1.getJobTitleId(), lang);
            val2  = BXResources.getPickListDescription("JobTitle", eh2.getJobTitleId(), lang);
            //appendNote("Job Title Category" + DELIMITER + val1 + DELIMITER + val2);
            appendNote(BXResources.getDocPrepIngMsg("JOB_TITLE_CATEGORY",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_JOB_TITLE_CATEGORY, PKL_JOB_TITLE, ival1, ival2))
			return;

		//* MONTHSOFSERVICE
		ival1 = eh1.getMonthsOfService();
		ival2 = eh2.getMonthsOfService();
		/*#DG600 
        if (ival1 != ival2)
        {
            iDiff = eh2.getMonthsOfService() - eh1.getMonthsOfService();
            appendNote(BXResources.getDocPrepIngMsg("TIME_AT_JOB",lang) + DELIMITER + formatYearsandMonths(eh1.getMonthsOfService()) +
                                       DELIMITER + formatYearsandMonths(eh2.getMonthsOfService()) +
                                       DELIMITER + formatYearsandMonths(iDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparYrMth(DUP_CHEK_TIME_AT_JOB, ival1, ival2))
			return;

		//* OCCUPATIONID
		ival1 = eh1.getOccupationId();
		ival2 = eh2.getOccupationId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("Occupation", eh1.getOccupationId(), lang);
            val2  = BXResources.getPickListDescription("Occupation", eh2.getOccupationId(), lang);
            //appendNote("Occupation" + DELIMITER + val1 + DELIMITER + val2);
            appendNote(BXResources.getDocPrepIngMsg("OCCUPATION",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_OCCUPATION, PKL_OCCUPATION, ival1, ival2))
			return;

		//*
		//* CONTACT
		//* -------
		//*
		try
		{
			compareContacts(eh1.getContact(), eh2.getContact());
		}
		catch (Exception e) {}
	}

	/**
	 * The following fields are printed on the <code>Contact</code>:
	 * <p><code><blockquote><pre>
	 *    CONTACTEMAILADDRESS
	 *    CONTACTFAXNUMBER
	 *    CONTACTFIRSTNAME
	 *    CONTACTJOBTITLE
	 *    CONTACTLASTNAME
	 *    CONTACTMIDDLEINITIAL
	 *    CONTACTPHONENUMBER
	 *    CONTACTPHONENUMBEREXTENSION
	 * </pre></blockquote></code>
	 *
	 * @param c the <code>Contact</code> to use as a source.
	 * @param offset determines which column to print the information to.
	 *		1 for the first data column, otherwise second column.
	 */
	public Vector<Formattable> printContact(Contact c, int offset)
	{ 
		String val;
		//#DG600 StringBuffer buff = new StringBuffer();
		Formattable nota;	
		Vector<Formattable> dnl = new Vector<Formattable>();

		//* CONTACTFIRSTNAME
		val = c.getContactFirstName();
		if (!isEmpty(val)) {
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_FIRST_NAME, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_FIRST_NAME, null, val, null);
			dnl.addElement(nota);
		}

		//* CONTACTMIDDLEINITIAL
		val = c.getContactMiddleInitial();
		if (!isEmpty(val)) {
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_MID_INITIAL, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_MID_INITIAL, null, val, null);
			dnl.addElement(nota);
		}
		//* CONTACTLASTNAME
		val = c.getContactLastName();
		if (!isEmpty(val))	{
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_LAST_NAME, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_LAST_NAME, null, val, null);
			dnl.addElement(nota);
		}

		//* CONTACTPHONENUMBER
		val = null;
		try
		{
			val = StringUtil.getAsFormattedPhoneNumber(c.getContactPhoneNumber(), null);
		}
		catch (Exception e) {}

		if (!isEmpty(val))	{
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_PHONE_NR, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_PHONE_NR, null, val, null);
			dnl.addElement(nota);
		}
		//* CONTACTPHONENUMBEREXTENSION
		val = c.getContactPhoneNumberExtension();
		if (!isEmpty(val))	{
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_PHONE_EXT, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_PHONE_EXT, null, val, null);
			dnl.addElement(nota);
		}
		//* CONTACTJOBTITLE
		val = c.getContactJobTitle();
		if (!isEmpty(val))	{
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_JOB_TIT, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_JOB_TIT, null, val, null);
				
			dnl.addElement(nota);
		}

		//* CONTACTEMAILADDRESS
		val = c.getContactEmailAddress();
		if (val != null)	{
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_EMAIL, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_EMAIL, null, val, null);
				
			dnl.addElement(nota);
		}
		//* CONTACTFAXNUMBER
		val = null;
		try
		{
			val = StringUtil.getAsFormattedPhoneNumber(c.getContactFaxNumber(), null);
		}
		catch (Exception e) {}

		if (!isEmpty(val))	{
			if (offset == 1)
				nota = newTabedLine(DUP_CHEK_CONT_FAX_NR, val, null, null);
			else
				nota = newTabedLine(DUP_CHEK_CONT_FAX_NR, null, val, null);
				
			dnl.addElement(nota);
		}
		return dnl;
	}

	/**
	 * Compares key fields in a pair of Contacts.
	 * <P>
	 * The following fields are compared on each <code>Contact</code>:
	 * <p><code><blockquote><pre>
	 *    CONTACTEMAILADDRESS
	 *    CONTACTFAXNUMBER
	 *    CONTACTFIRSTNAME
	 *    CONTACTJOBTITLE
	 *    CONTACTLASTNAME
	 *    CONTACTMIDDLEINITIAL
	 *    CONTACTPHONENUMBER
	 *    CONTACTPHONENUMBEREXTENSION
	 * </pre></blockquote></code>
	 */
	public void compareContacts(Contact c1, Contact c2)
	{
		Formattable nota;	//#DG600 
		logger.trace( ": Method compareContacts entered ");
		if (c1 == null)
		{
			//#DG600 appendNote ("Contact 1 has no information");
			nota = BXResources.newFormata(DUP_CHEK_CONTACT_NO_INFO, "1");
			return;
		}

		if (c2 == null)
		{
			//#DG600 appendNote ("Contact 2 has no information");
			nota = BXResources.newFormata(DUP_CHEK_CONTACT_NO_INFO, "2");
			return;
		}

		if (maximumReached()) return;
		logger.trace( ": Method compareContacts : 1");
		String val1, val2;

		//* CONTACTEMAILADDRESS
		val1 = c1.getContactEmailAddress();
		val2 = c2.getContactEmailAddress();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("CONTACT_EMAIL_ADDRESS",lang) + DELIMITER + c1.getContactEmailAddress() +
                                                 DELIMITER + c2.getContactEmailAddress());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_CONT_EMAIL, val1, val2))
			return;

		//* CONTACTFAXNUMBER
		logger.trace( ": Method compareContacts : 2");
		val1 = c1.getContactFaxNumber();
		val2 = c2.getContactFaxNumber();	//#DG634
		if (!isEqual(val1, val2))
		{
			val1 = null;
			val2 = null;
			try
			{
				val1 = StringUtil.getAsFormattedPhoneNumber(c1.getContactFaxNumber(), null);
			}
			catch (Exception e) {}
			try
			{
				val2 = StringUtil.getAsFormattedPhoneNumber(c2.getContactFaxNumber(), null);
			}
			catch (Exception e) {}

			//#DG600 
			/*appendNote(BXResources.getDocPrepIngMsg("CONTACT_FAX_NUMBER",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;*/            
			nota = newTabedLine(DUP_CHEK_CONT_FAX_NR,val1, val2, null);
			addFormtb(nota);
		}
		if (maximumReached()) return;

		//* CONTACTPHONENUMBER
		logger.trace( ": Method compareContacts : 3");
		val1 = c1.getContactPhoneNumber();
		val2 = c2.getContactPhoneNumber();	//#DG634
		if (!isEqual(val1, val2))
		{
			val1 = null;
			val2 = null;

			try
			{
				val1 = StringUtil.getAsFormattedPhoneNumber(c1.getContactPhoneNumber(), null);
			}
			catch (Exception e) {}

			try
			{
				val2 = StringUtil.getAsFormattedPhoneNumber(c2.getContactPhoneNumber(), null);
			}
			catch (Exception e) {}
			//#DG600 
			/*appendNote(BXResources.getDocPrepIngMsg("CONTACT_PHONE_NUMBER",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;*/
			nota = newTabedLine(DUP_CHEK_CONT_PHONE_NR,val1, val2, null);
			addFormtb(nota);
		}
		if (maximumReached()) return;

		//* CONTACTPHONENUMBEREXTENSION
		logger.trace( ": Method compareContacts : 4");
		val1 = c1.getContactPhoneNumberExtension();
		val2 = c2.getContactPhoneNumberExtension();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("CONTACT_PHONE_NUMBER_EXTENSION",lang) + DELIMITER + c1.getContactPhoneNumberExtension() +
                                                          DELIMITER + c2.getContactPhoneNumberExtension());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_CONT_PHONE_EXT, val1, val2))
			return;
		logger.trace( ": Method compareContacts finished ");
	}

	// ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
	/**
	 * The following fields are printed on the
	 * <code>BorrowerIdentification</code>:
	 * <p>
	 * <code><blockquote><pre>
	 *   IDENTIFICATIONTYPEID				
	 * 	IDENTIFICATIONNUMBER
	 * 	IDENTIFICATIONCOUNTRY
	 * </pre></blockquote></code>
	 * 
	 * @param bi
	 *            the <code>BorrowerIdentification</code> to be used as a
	 *            source.
	 * @param offset determines which column to print the information to.
	 *		1 for the first data column, otherwise second column.
	 */
	public Vector<Formattable> printBorrowerIdentification(BorrowerIdentification bi, int offset) {
		// logger.trace( ":" +" Method
		// printBorrowerIdentification entered ");
		String val;
		//#DG600 StringBuffer buff = new StringBuffer();
		Vector<Formattable> dnl = new Vector<Formattable>();	
		Formattable nota;	 

		// * IDENTIFICATIONTYPEID
		int ival = bi.getIdentificationTypeId();
		if (offset ==1)
			nota = newTabedLine(DUP_CHEK_ID_TYPE, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_IDENTIFICATIONTYPE, ival), null, null);
		else
			nota = newTabedLine(DUP_CHEK_ID_TYPE, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_IDENTIFICATIONTYPE, ival), null);
		dnl.addElement(nota);

		// * IDENTIFICATIONNUMBER
		val = bi.getIdentificationNumber();
		if (offset ==1)
			nota = newTabedLine(DUP_CHEK_ID_NUMBER, val, null, null);
		else
			nota = newTabedLine(DUP_CHEK_ID_NUMBER, null, val, null);
		dnl.addElement(nota);

		// * IDENTIFICATIONCOUNTRY
		val = bi.getIdentificationCountry();
		if (offset ==1)
			nota = newTabedLine(DUP_CHEK_ID_COUNTRY, val, null, null);
		else
			nota = newTabedLine(DUP_CHEK_ID_COUNTRY, null, val, null);
		dnl.addElement(nota);
		
		return dnl;
	}

	/**
	 * Compares key fields in a pair of Borroweridentifications.
	 * <P>
	 * The following fields are compared on each
	 * <code>Borrower Identification</code>:
	 * <p>
	 * <code><blockquote><pre>
	 *   IDENTIFICATIONTYPEID				
	 * 	IDENTIFICATIONNUMBER
	 * 	IDENTIFICATIONCOUNTRY
	 * </pre></blockquote></code>
	 */
	public void compareBorrowerIdentifications(BorrowerIdentification bi1,
			BorrowerIdentification bi2) {
		logger.trace( ": Method compareBorrowerIdentifications entered ");
		Formattable nota;	//#DG600 

		if (bi1 == null) { 
			/*#DG600
			appendNote("BorrowerIdentification 1 has no information");*/
			nota = BXResources.newFormata(DUP_CHEK_BOR_ID_NO_INFO, "1");
			dealNotesList.addElement(nota);
			return;
		}

		if (bi2 == null) {
			/*#DG600
			appendNote("BorrowerIdentification 2 has no information");*/
			nota = BXResources.newFormata(DUP_CHEK_BOR_ID_NO_INFO, "2");
			dealNotesList.addElement(nota);
			return;
		}

		if (maximumReached())
			return;
		logger.trace( ": Method compareBorrowerIdentifications : 1");
		String val1, val2;

		// * IDENTIFICATIONTYPEID
		int ival1 = bi1.getIdentificationTypeId();
		int ival2 = bi2.getIdentificationTypeId();
		/*#DG600 
		if (ival1 != ival2) {
			val1 = BXResources.getPickListDescription("IDENTIFICATIONTYPE", bi1
					.getIdentificationTypeId(), lang);
			val2 = BXResources.getPickListDescription("IDENTIFICATIONTYPE", bi2
					.getIdentificationTypeId(), lang);
			appendNote(BXResources
					.getDocPrepIngMsg("IDENTIFICATION_TYPE", lang)
					+ DELIMITER + val1 + DELIMITER + val2);
			totalDifferences++;
		}
		if (maximumReached())*/
		if(comparPkL(DUP_CHEK_ID_TYPE, PKL_IDENTIFICATIONTYPE, ival1, ival2))		
			return;

		// * IDENTIFICATIONNUMBER
		val1 = bi1.getIdentificationNumber();
		val2 = bi2.getIdentificationNumber();	//#DG634
		/*#DG600 
		if (!isEqual(val1, val2)) {
			appendNote(BXResources.getDocPrepIngMsg("IDENTIFICATION_NUMBER",
					lang)
					+ DELIMITER
					+ bi1.getIdentificationNumber()
					+ DELIMITER
					+ bi2.getIdentificationNumber());
			totalDifferences++;
		}
		if (maximumReached())*/
		if(comparStr(DUP_CHEK_ID_NUMBER, val1, val2))		
			return;

		// * IDENTIFICATIONCOUNTRY
		val1 = bi1.getIdentificationCountry();
		val2 = bi2.getIdentificationCountry();	//#DG634
		/*#DG600 
		if (!isEqual(val1, val2)) {
			appendNote(BXResources.getDocPrepIngMsg("IDENTIFICATION_COUNTRY",
					lang)
					+ DELIMITER
					+ bi1.getIdentificationCountry()
					+ DELIMITER
					+ bi2.getIdentificationCountry());
			totalDifferences++;
		}
		if (maximumReached())*/
		if(comparStr(DUP_CHEK_ID_COUNTRY, val1, val2))			
			return;
	}

	// ***** Change by NBC Impl. Team - Version 1.6 - End *****//
	/**
	 * The following fields are printed on the <code>Income</code>:
	 * <p><code><blockquote><pre>
	 *    ANNUALINCOMEAMOUNT
	 *    INCOMEAMOUNT
	 *    INCOMEDESCRIPTION
	 *    INCOMEPERIODID
	 *    INCOMETYPEID
	 *    INCPERCENTINGDS
	 *    INCPERCENTINTDS
	 *    INCPERCENTOUTGDS
	 *    INCPERCENTOUTTDS
	 *    MONTHLYINCOMEAMOUNT
	 * </pre></blockquote></code>
	 *
	 * @param inc the <code>Income</code> to use as a source.
	 */
	public Vector<Formattable> printIncome(Income inc, int offset)
	{
		Formattable nota;
		Vector<Formattable> dealNotesList = new Vector<Formattable>();	//#DG600
		//logger.trace( "DealCompare :" +" Method printIncome entered ");
		String val;
		//StringBuffer buff = new StringBuffer();
		//logger.trace( ": Method printIncome entered");
		//* INCOMEDESCRIPTION
		val = inc.getIncomeDescription();
		if (!isEmpty(val))	{
			if (offset == 1)
				nota  = newTabedLine(DUP_CHEK_INCOME_DESCRIPTION, val, null, null);
			else
				nota  = newTabedLine(DUP_CHEK_INCOME_DESCRIPTION, null, val, null);
			dealNotesList.addElement(nota);
		}

		//* ANNUALINCOMEAMOUNT
		if (offset == 1)
			nota  = newTabedLine(ANNUAL_INCOME_AMOUNT, 
				BXResources.newFormata(CURRENCY_FORMAT, inc.getAnnualIncomeAmount()), null, null);
		else
			nota  = newTabedLine(ANNUAL_INCOME_AMOUNT, null, 
				BXResources.newFormata(CURRENCY_FORMAT, inc.getAnnualIncomeAmount()), null);
		dealNotesList.addElement(nota);

		//* INCOMEAMOUNT
		if (offset == 1)
			nota  = newTabedLine(INCOME_AMOUNT, 
				BXResources.newFormata(CURRENCY_FORMAT, inc.getIncomeAmount()), null, null);
		else
			nota  = newTabedLine(INCOME_AMOUNT, null, 
				BXResources.newFormata(CURRENCY_FORMAT, inc.getIncomeAmount()), null);
		dealNotesList.addElement(nota);

		//* INCOMEPERIODID
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_INCOME_PERIOD, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INCOME_PERIOD, inc.getIncomePeriodId()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_INCOME_PERIOD, null, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INCOME_PERIOD, inc.getIncomePeriodId()), null);
		dealNotesList.addElement(nota);

		//* INCOMETYPEID
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_INCOME_TYPE, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INCOME_TYPE, inc.getIncomeTypeId()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_INCOME_TYPE, null, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INCOME_TYPE, inc.getIncomeTypeId()), null);
		dealNotesList.addElement(nota);

		//* INCPERCENTINGDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_GDS,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentInGDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_GDS, null,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentInGDS()), null);
		dealNotesList.addElement(nota);

		//* INCPERCENTINTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_TDS,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentInTDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_TDS, null,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentInTDS()), null);
		dealNotesList.addElement(nota);

		//* INCPERCENTOUTGDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_GDS,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentOutGDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_GDS, null,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentOutGDS()), null);
		dealNotesList.addElement(nota);

		//* INCPERCENTOUTTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_TDS,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentOutTDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_TDS, null,  
				BXResources.newFormata(RATIO_FORMAT, (double)inc.getIncPercentOutTDS()), null);
		dealNotesList.addElement(nota);

		//* MONTHLYINCOMEAMOUNT
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_MONTHLY_INCOME_AMOUNT, 
				BXResources.newFormata(CURRENCY_FORMAT, inc.getMonthlyIncomeAmount()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_MONTHLY_INCOME_AMOUNT, null, 
				BXResources.newFormata(CURRENCY_FORMAT, inc.getMonthlyIncomeAmount()), null);
		dealNotesList.addElement(nota);

		//logger.trace( ": Method printIncome finished ");
		//return buff.toString();
		return dealNotesList;
	}

	/**
	 * Compares key fields in a pair of Incomes.
	 * <P>
	 * The following fields are compared on each <code>Income</code>:
	 * <p><code><blockquote><pre>
	 *    ANNUALINCOMEAMOUNT
	 *    INCOMEAMOUNT
	 *    INCOMEDESCRIPTION
	 *    INCOMEPERIODID
	 *    INCOMETYPEID
	 *    INCPERCENTINGDS
	 *    INCPERCENTINTDS
	 *    INCPERCENTOUTGDS
	 *    INCPERCENTOUTTDS
	 *    MONTHLYINCOMEAMOUNT
	 * </pre></blockquote></code>
	 */
	public void compareIncomes(Income inc1, Income inc2)
	{
		logger.trace( ": Method compareIncomes entered ");
		if (maximumReached()) return;

		String val1, val2;

		//* ANNUALINCOMEAMOUNT
		logger.trace( ": Method compareIncomes : 1");
		double dval1 = inc1.getAnnualIncomeAmount();
		double dval2 = inc2.getAnnualIncomeAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(inc1.getAnnualIncomeAmount());
            val2 = formatCurrency(inc2.getAnnualIncomeAmount());
            dDiff = inc2.getAnnualIncomeAmount() - inc1.getAnnualIncomeAmount();
            appendNote(BXResources.getDocPrepIngMsg("ANNUAL_INCOME_AMOUNT",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                                DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }

        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_ANUAL_INCOME_AMT, dval1, dval2))
			return;

		//* INCOMEAMOUNT
		logger.trace( ": Method compareIncomes : 2");
		dval1 = inc1.getIncomeAmount();
		dval2 = inc2.getIncomeAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(inc1.getIncomeAmount());
            val2 = formatCurrency(inc2.getIncomeAmount());
            dDiff = inc2.getIncomeAmount() - inc1.getIncomeAmount();
            appendNote(BXResources.getDocPrepIngMsg("INCOME_AMOUNT",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                         DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(INCOME_AMOUNT, dval1, dval2))
			return;

		//* INCOMEDESCRIPTION
		logger.trace( ": Method compareIncomes : 3");
		val1 = inc1.getIncomeDescription();
		val2 = inc2.getIncomeDescription();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("INCOME_DESCRIPTION",lang) + DELIMITER + inc1.getIncomeDescription() +
                                              DELIMITER + inc2.getIncomeDescription());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_INCOME_DESCRIPTION, val1, val2))
			return;

		//* INCOMEPERIODID
		logger.trace( ": Method compareIncomes : 4");
		int ival1 = inc1.getIncomePeriodId();
		int ival2 = inc2.getIncomePeriodId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("IncomePeriod", inc1.getIncomePeriodId(), lang);
            val2  = BXResources.getPickListDescription("IncomePeriod", inc2.getIncomePeriodId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("INCOME_PERIOD",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_INCOME_PERIOD, PKL_INCOME_PERIOD, ival1, ival2))
			return;

		//* INCOMETYPEID
		logger.trace( ": Method compareIncomes : 5");
		ival1 = inc1.getIncomeTypeId();
		ival2 = inc2.getIncomeTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription(INCOME_TYPE, inc1.getIncomeTypeId(), lang);
            val2  = BXResources.getPickListDescription(INCOME_TYPE, inc2.getIncomeTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("INCOME_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_INCOME_TYPE, PKL_INCOME_TYPE, ival1, ival2))		//#DG642
			return;

		//* INCPERCENTINGDS
		logger.trace( ": Method compareIncomes : 6");
		dval1 = inc1.getIncPercentInGDS();
		dval2 = inc2.getIncPercentInGDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(inc1.getIncPercentInGDS());
            val2 = formatRatio(inc2.getIncPercentInGDS());
            dDiff = inc2.getIncPercentInGDS() - inc1.getIncPercentInGDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_IN_GDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                    DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERCENT_IN_GDS, RATIO_FORMAT, dval1, dval2))
			return;

		//* INCPERCENTINTDS
		logger.trace( ": Method compareIncomes : 7");
		dval1 = inc1.getIncPercentInTDS();
		dval2 = inc2.getIncPercentInTDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(inc1.getIncPercentInTDS());
            val2 = formatRatio(inc2.getIncPercentInTDS());
            dDiff = inc2.getIncPercentInTDS() - inc1.getIncPercentInTDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_IN_TDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                    DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERCENT_IN_TDS, RATIO_FORMAT, dval1, dval2))
			return;

		//* INCPERCENTOUTGDS
		logger.trace( ": Method compareIncomes : 8");
		dval1 = inc1.getIncPercentOutGDS();
		dval2 = inc2.getIncPercentOutGDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(inc1.getIncPercentOutGDS());
            val2 = formatRatio(inc2.getIncPercentOutGDS());
            dDiff = inc2.getIncPercentOutGDS() - inc1.getIncPercentOutGDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_OUT_OF_GDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                        DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERCENT_OUT_OF_GDS, RATIO_FORMAT, dval1, dval2))
			return;

		//* INCPERCENTOUTTDS
		logger.trace( ": Method compareIncomes : 9");
		dval1 = inc1.getIncPercentOutTDS();
		dval2 = inc2.getIncPercentOutTDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(inc1.getIncPercentOutTDS());
            val2 = formatRatio(inc2.getIncPercentOutTDS());
            dDiff = inc2.getIncPercentOutTDS() - inc1.getIncPercentOutTDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_OUT_OF_TDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                        DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERCENT_OUT_OF_TDS, RATIO_FORMAT, dval1, dval2))
			return;

		//* MONTHLYINCOMEAMOUNT
		logger.trace( ": Method compareIncomes : 10");
		dval1 = inc1.getMonthlyIncomeAmount();
		dval2 = inc2.getMonthlyIncomeAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(inc1.getMonthlyIncomeAmount());
            val2 = formatCurrency(inc2.getMonthlyIncomeAmount());
            dDiff = inc2.getMonthlyIncomeAmount() - inc1.getMonthlyIncomeAmount();
            appendNote(BXResources.getDocPrepIngMsg("MONTHLY_INCOME_AMOUNT",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                                 DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }*/
		if(comparDbl(DUP_CHEK_MONTHLY_INCOME_AMOUNT, dval1, dval2))
			return;
		logger.trace( ": Method compareIncomes finished ");
	}

	/*#DG600 not used
    private String printLiabilities(Borrower b)
    {
        logger.trace( ": Method printLiabilities entered ");
        StringBuffer buff = new StringBuffer();

        try
        {
            List l = (List)b.getLiabilities();
            Iterator i = l.iterator();
            Liability liab;

            Collections.sort(l, comp);

            while (i.hasNext())
            {
                liab = (Liability)i.next();
                buff.append(printLiability(liab)).append("\n");
            }
        }
        catch (Exception e) {}
        logger.trace( ": Method printLiabilities finished ");
        return buff.toString();
    } */

	/**
	 * The following fields are printed on the <code>Liability</code>:
	 * <p><code><blockquote><pre>
	 *    INCLUDEINGDS
	 *    INCLUDEINTDS
	 *    LIABILITYAMOUNT
	 *    LIABILITYDESCRIPTION
	 *    LIABILITYMONTHLYPAYMENT
	 *    LIABILITYPAYMENTQUALIFIER
	 *    LIABILITYPAYOFFTYPEID
	 *    LIABILITYTYPEID
	 *    PERCENTGDS
	 *    PERCENTTDS
	 *    PERCENTOUTGDS
	 *    PERCENTOUTTDS
	 * </pre></blockquote></code>
	 *
	 * @param l the <code>Liability</code> to use as a source.
	 */
	//#DG600 
	public Vector<Formattable> printLiability(Liability l, int offset)
	{
		Vector<Formattable> dnl = new Vector<Formattable>();	//#DG600
		Formattable nota;	//#DG600 
		String val;

		//* LIABILITYDESCRIPTION
		val = l.getLiabilityDescription();
		if (!isEmpty(val)) {
			if (offset == 1)
				nota  = newTabedLine(DESCRIPTION, trimDescription(val), null, null);
			else
				nota  = newTabedLine(DESCRIPTION, null, trimDescription(val), null);
			dnl.addElement(nota);
		}

		//* LIABILITYAMOUNT
		if (offset == 1)
			nota  = newTabedLine(AMOUNT, 
				BXResources.newFormata(CURRENCY_FORMAT, l.getLiabilityAmount()), null, null);
		else
			nota  = newTabedLine(AMOUNT, null, 
				BXResources.newFormata(CURRENCY_FORMAT, l.getLiabilityAmount()), null);
		dnl.addElement(nota);

		//* LIABILITYMONTHLYPAYMENT
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_MTHLY_PAY, 
				BXResources.newFormata(CURRENCY_FORMAT, l.getLiabilityMonthlyPayment()), null,null);
		else
			nota  = newTabedLine(DUP_CHEK_MTHLY_PAY, null, 
				BXResources.newFormata(CURRENCY_FORMAT, l.getLiabilityMonthlyPayment()), null);
		dnl.addElement(nota);

		//* LIABILITYPAYMENTQUALIFIER
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_LIAB_PAY_QUAL, l.getLiabilityPaymentQualifier(), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_LIAB_PAY_QUAL, null, l.getLiabilityPaymentQualifier(), null);
		dnl.addElement(nota);

		//* LIABILITYPAYOFFTYPEID
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_LIAB_PAYOFF_TYPE, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_LIABILITY_PAY_OFF_TYPE, 
					l.getLiabilityPayOffTypeId()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_LIAB_PAYOFF_TYPE, null, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_LIABILITY_PAY_OFF_TYPE, 
					l.getLiabilityPayOffTypeId()), null);
		dnl.addElement(nota);

		//* LIABILITYTYPEID
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_LIAB_TYPE,
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_LIABILITY_TYPE, l.getLiabilityTypeId()),null, null);
		else
			nota  = newTabedLine(DUP_CHEK_LIAB_TYPE, null,
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_LIABILITY_TYPE, l.getLiabilityTypeId()), null);
		dnl.addElement(nota);

		//* INCLUDEINGDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_GDS,
				BXResources.newGenMsg(l.getIncludeInGDS()? YES_LABEL : NO_LABEL),null, null);
		else
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_GDS, null,
				BXResources.newGenMsg(l.getIncludeInGDS()? YES_LABEL : NO_LABEL), null);
		dnl.addElement(nota);

		//* INCLUDEINTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_TDS,
				BXResources.newGenMsg(l.getIncludeInTDS()? YES_LABEL : NO_LABEL),null, null);
		else
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_TDS, null,
				BXResources.newGenMsg(l.getIncludeInTDS()? YES_LABEL : NO_LABEL),null);
		dnl.addElement(nota);

		//* PERCENTGDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_GDS,  
				BXResources.newFormata(RATIO_FORMAT, l.getPercentInGDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_GDS, null,  
				BXResources.newFormata(RATIO_FORMAT, l.getPercentInGDS()), null);
		dnl.addElement(nota);

		//* PERCENTTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_TDS, 
				BXResources.newFormata(RATIO_FORMAT, l.getPercentInTDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_TDS, null, 
				BXResources.newFormata(RATIO_FORMAT, l.getPercentInTDS()), null);
		dnl.addElement(nota);

		//* PERCENTOUTGDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_GDS, 
				BXResources.newFormata(RATIO_FORMAT, l.getPercentOutGDS()),null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_GDS, null, 
				BXResources.newFormata(RATIO_FORMAT, l.getPercentOutGDS()), null);
		dnl.addElement(nota);

		//* PERCENTOUTTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_TDS, 
				BXResources.newFormata(RATIO_FORMAT, l.getPercentOutTDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_TDS, null, 
				BXResources.newFormata(RATIO_FORMAT, l.getPercentOutTDS()), null);
		dnl.addElement(nota);

		//return buff.toString();
		return dnl;
	}

	/**
	 * The following fields are printed on the <code>PropertyExpense</code>:
	 * <p><code><blockquote><pre>
	 *    PROPERTYEXPENSEPERIODID
	 *    PROPERTYEXPENSETYPEID
	 *    PROPERTYEXPENSEAMOUNT
	 *    PROPERTYEXPENSEDESCRIPTION
	 *    MONTHLYEXPENSEAMOUNT
	 *    PEINCLUDEINGDS
	 *    PEINCLUDEINTDS
	 *    PEPERCENTINGDS
	 *    PEPERCENTINTDS
	 *    PEPERCENTOUTGDS
	 *    PEPERCENTOUTTDS
	 * </pre></blockquote></code>
	 *
	 * @param pe the <code>PropertyExpense</code> to use as a source.
	 */
	//#DG600 
	public Vector<Formattable> printPropertyExpense(PropertyExpense pe, int offset)
	{
		Vector<Formattable> dnl = new Vector<Formattable>();	//#DG600
		Formattable nota;	//#DG600 
		//StringBuffer buff = new StringBuffer();
		String val;

		val = pe.getPropertyExpenseDescription();
		//*    PROPERTYEXPENSEDESCRIPTION
		if (!isEmpty(val)) {
			if (offset == 1)
				nota  = newTabedLine(DESCRIPTION, val, null, null);
			else
				nota  = newTabedLine(DESCRIPTION, null, val, null);
			dnl.addElement(nota);
		}

		//*    PROPERTYEXPENSEPERIODID
		if (offset == 1)
			nota  = newTabedLine(PERIOD, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PROPERTY_EXPENSE_PERIOD, pe.getPropertyExpensePeriodId()), null, null);
		else
			nota  = newTabedLine(PERIOD, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PROPERTY_EXPENSE_PERIOD, pe.getPropertyExpensePeriodId()), null);
		dnl.addElement(nota);

		//*    PROPERTYEXPENSETYPEID
		if (offset == 1)
			nota  = newTabedLine(TYPE, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PROPERTY_EXPENSE_TYPE, pe.getPropertyExpenseTypeId()), null, null);
		else
			nota  = newTabedLine(TYPE, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PROPERTY_EXPENSE_TYPE, pe.getPropertyExpenseTypeId()), null);
		dnl.addElement(nota);

		//*    PROPERTYEXPENSEAMOUNT
		if (offset == 1)
			nota  = newTabedLine(AMOUNT, 
				BXResources.newFormata(CURRENCY_FORMAT, pe.getPropertyExpenseAmount()), null, null);
		else
			nota  = newTabedLine(AMOUNT, null, 
				BXResources.newFormata(CURRENCY_FORMAT, pe.getPropertyExpenseAmount()), null);
		dnl.addElement(nota);

		//*    MONTHLYEXPENSEAMOUNT
		if (offset == 1)
			nota  = newTabedLine(MTHLY_PAY, 
				BXResources.newFormata(CURRENCY_FORMAT, pe.getMonthlyExpenseAmount()), null, null);
		else
			nota  = newTabedLine(MTHLY_PAY, null, 
				BXResources.newFormata(CURRENCY_FORMAT, pe.getMonthlyExpenseAmount()), null);
		dnl.addElement(nota);

		//*    PEINCLUDEINGDS
		if (offset == 1)
			nota  = newTabedLine(TYPE, 
				BXResources.newGenMsg(pe.getPeIncludeInGDS() ? YES_LABEL : NO_LABEL), null, null);
		else
			nota  = newTabedLine(TYPE, null, 
				BXResources.newGenMsg(pe.getPeIncludeInGDS() ? YES_LABEL : NO_LABEL), null);
		dnl.addElement(nota);

		//*    PEINCLUDEINTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_TDS, 
				BXResources.newGenMsg(pe.getPeIncludeInTDS() ? YES_LABEL : NO_LABEL), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_TDS, null, 
				BXResources.newGenMsg(pe.getPeIncludeInTDS() ? YES_LABEL : NO_LABEL), null);
		dnl.addElement(nota);

		//*    PEPERCENTINGDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_GDS, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentInGDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_GDS, null, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentInGDS()), null);
		dnl.addElement(nota);

		//*    PEPERCENTINTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_TDS, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentInTDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_IN_TDS, null, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentInTDS()), null);
		dnl.addElement(nota);

		//*    PEPERCENTOUTGDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_GDS, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentOutGDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_GDS, null, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentOutGDS()), null);
		dnl.addElement(nota);

		//*    PEPERCENTOUTTDS
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_TDS, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentOutTDS()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERCENT_OUT_OF_TDS, null, 
				BXResources.newFormata(RATIO_FORMAT, (double)pe.getPePercentOutTDS()), null);
		dnl.addElement(nota);

		return dnl;
	}

	/**
	 * Compares key fields in a pair of Liabilities.
	 * <P>
	 * The following fields are compared on each <code>Liability</code>:
	 * <p><code><blockquote><pre>
	 *    BORROWERID
	 *    INCLUDEINGDS
	 *    INCLUDEINTDS
	 *    LIABILITYAMOUNT
	 *    LIABILITYDESCRIPTION
	 *    LIABILITYMONTHLYPAYMENT
	 *    LIABILITYPAYMENTQUALIFIER
	 *    LIABILITYPAYOFFTYPEID
	 *    LIABILITYTYPEID
	 *    PERCENTGDS
	 *    PERCENTTDS
	 *    PERCENTOUTGDS
	 *    PERCENTOUTTDS
	 * </pre></blockquote></code>
	 */
	public void compareLiabilities(Liability l1, Liability l2)
	{
		logger.trace( ": Method compareLiabilities entered ");
		if (maximumReached()) return;

		String val1, val2;

		//* LIABILITYDESCRIPTION
		logger.trace( ": Method compareLiabilities : LIABILITYDESCRIPTION ");
		val1 = trimDescription(l1.getLiabilityDescription());
		val2 = trimDescription(l2.getLiabilityDescription());
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("DESCRIPTION",lang) + DELIMITER +  val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DESCRIPTION, val1, val2))
			return;

		//* INCLUDEINGDS
		logger.trace( ": Method compareLiabilities : INCLUDEINGDS ");
		//#DG658
		boolean bval1 = l1.getIncludeInGDS();
		boolean bval2 = l2.getIncludeInGDS();
		/*
		if (l1.getIncludeInGDS() != l2.getIncludeInGDS())
		{
			val1 = l1.getIncludeInGDS() ? YES_LABEL : NO_LABEL;
			val2 = l2.getIncludeInGDS() ? YES_LABEL : NO_LABEL;
			/ *#DG600 
            appendNote(BXResources.getDocPrepIngMsg("INCLUDE_IN_GDS",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_GDS,
					BXResources.newGenMsg(val1),BXResources.newGenMsg(val2), null);
			dealNotesList.addElement(nota);
			totalDifferences++;
			if (maximumReached()) 
				return;
		}*/
		if(comparBool(DUP_CHEK_INCLUDE_IN_GDS, bval1, bval2))
			return;

		//* INCLUDEINTDS
		logger.trace( ": Method compareLiabilities : INCLUDEINTDS ");
		//#DG658
		bval1 = l1.getIncludeInTDS();
		bval2 = l2.getIncludeInTDS();
		/*if (l1.getIncludeInTDS() != l2.getIncludeInTDS())
		{
			val1 = l1.getIncludeInTDS() ? YES_LABEL : NO_LABEL;
			val2 = l2.getIncludeInTDS() ? YES_LABEL : NO_LABEL;
			/ *#DG600 
            appendNote(BXResources.getDocPrepIngMsg("INCLUDE_IN_TDS",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_TDS,
					BXResources.newGenMsg(val1),BXResources.newGenMsg(val2), null);
			dealNotesList.addElement(nota);
			totalDifferences++;
			if (maximumReached()) 
				return;
		}*/
		if(comparBool(DUP_CHEK_INCLUDE_IN_TDS, bval1, bval2))
			return;

		//* LIABILITYAMOUNT
		logger.trace( ": Method compareLiabilities : LIABILITYAMOUNT ");
		double dval1 = l1.getLiabilityAmount();
		double dval2 = l2.getLiabilityAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(l1.getLiabilityAmount());
            val2 = formatCurrency(l2.getLiabilityAmount());
            dDiff = l2.getLiabilityAmount() - l1.getLiabilityAmount();
            appendNote(BXResources.getDocPrepIngMsg("AMOUNT",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                  DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(AMOUNT, dval1, dval2))
			return;

		//* LIABILITYMONTHLYPAYMENT
		logger.trace( ": Method compareLiabilities : LIABILITYMONTHLYPAYMENT ");
		dval1 = l1.getLiabilityMonthlyPayment();
		dval2 = l2.getLiabilityMonthlyPayment();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(l1.getLiabilityMonthlyPayment());
            val2 = formatCurrency(l2.getLiabilityMonthlyPayment());
            dDiff = l2.getLiabilityMonthlyPayment() - l1.getLiabilityMonthlyPayment();
            appendNote(BXResources.getDocPrepIngMsg("MTHLY_PAY",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                     DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_MTHLY_PAY, dval1, dval2))
			return;

		//* LIABILITYPAYMENTQUALIFIER
		logger.trace( ": Method compareLiabilities : LIABILITYPAYMENTQUALIFIER ");
		val1 = l1.getLiabilityPaymentQualifier();
		val2 = l2.getLiabilityPaymentQualifier();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("LIABILITY_PAYMENT_QUALIFIER",lang) + DELIMITER + l1.getLiabilityPaymentQualifier() +
                                                       DELIMITER + l2.getLiabilityPaymentQualifier());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_LIAB_PAY_QUAL, val1, val2))
			return;

		//* LIABILITYPAYOFFTYPEID
		logger.trace( ": Method compareLiabilities : LIABILITYPAYOFFTYPEID ");
		int ival1 = l1.getLiabilityPayOffTypeId();
		int ival2 = l2.getLiabilityPayOffTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("LiabilityPayOffType", l1.getLiabilityPayOffTypeId(), lang);
            val2  = BXResources.getPickListDescription("LiabilityPayOffType", l2.getLiabilityPayOffTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("LIABILITY_PAYOFF_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_LIAB_PAYOFF_TYPE, PKL_LIABILITY_PAY_OFF_TYPE, ival1, ival2))
			return;

		//* LIABILITYTYPEID
		logger.trace( ": Method compareLiabilities : LIABILITYTYPEID ");
		ival1 = l1.getLiabilityTypeId();
		ival2 = l2.getLiabilityTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("LiabilityType", l1.getLiabilityTypeId(), lang);
            val2  = BXResources.getPickListDescription("LiabilityType", l2.getLiabilityTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_LIAB_TYPE, PKL_LIABILITY_TYPE, ival1, ival2))
			return;

		//* PERCENTGDS
		logger.trace( ": Method compareLiabilities : PERCENTGDS ");
		dval1 = l1.getPercentInGDS();
		dval2 = l2.getPercentInGDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(l1.getPercentInGDS());
            val2 = formatRatio(l2.getPercentInGDS());
            dDiff = l2.getPercentInGDS() - l1.getPercentInGDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_IN_GDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                    DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERCENT_IN_GDS, RATIO_FORMAT, dval1, dval2))
			return;

		//* PERCENTTDS
		logger.trace( ": Method compareLiabilities : PERCENTTDS ");
		dval1 = l1.getPercentInTDS();
		dval2 = l2.getPercentInTDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(l1.getPercentInTDS());
            val2 = formatRatio(l2.getPercentInTDS());
            dDiff = l2.getPercentInTDS() - l1.getPercentInTDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_IN_TDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                    DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERCENT_IN_TDS, RATIO_FORMAT, dval1, dval2))
			return;

		//* PERCENTOUTGDS
		logger.trace( ": Method compareLiabilities : PERCENTOUTGDS ");
		dval1 = l1.getPercentOutGDS();
		dval2 = l2.getPercentOutGDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(l1.getPercentOutGDS());
            val2 = formatRatio(l2.getPercentOutGDS());
            dDiff = l2.getPercentOutGDS() - l1.getPercentOutGDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_OUT_OF_GDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                     DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERCENT_OUT_OF_GDS, RATIO_FORMAT, dval1, dval2))
			return;

		//* PERCENTOUTTDS
		logger.trace( ": Method compareLiabilities : PERCENTOUTTDS ");
		dval1 = l1.getPercentOutTDS();
		dval2 = l2.getPercentOutTDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(l1.getPercentOutTDS());
            val2 = formatRatio(l2.getPercentOutTDS());
            dDiff = l2.getPercentOutTDS() - l1.getPercentOutTDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_OUT_OF_TDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                     DELIMITER + formatRatio(dDiff));
            totalDifferences++;
        }*/
		if(comparDbl(DUP_CHEK_PERCENT_OUT_OF_TDS, RATIO_FORMAT, dval1, dval2))
			return;

        /***** FFATE start *****/
        //* CreditLimit
        dval1 = l1.getCreditLimit();
        dval2 = l2.getCreditLimit();

        if(comparDbl(DUP_CHEK_CREDIT_LIMIT, dval1, dval2))
            return;
        
        //* MaturityDate
        Date mval1 = l1.getMaturityDate();
        Date mval2 = l2.getMaturityDate();

        if(comparDate(DUP_CHEK_MATURITY_DATE , mval1, mval2))
            return;
        
        //* CreditBureauRecordIndicator
        String sval1 = l1.getCreditBureauRecordIndicator();
        String sval2 = l2.getCreditBureauRecordIndicator();

        if(comparStr(DUP_CHEK_CREDITBUREAURECORDINDICATOR , sval1, sval2))
            return;
        /***** FFATE end *****/
        
		logger.trace( ": Method compareLiabilities finished ");
	}

	/**
	 * The following fields are printed on the <code>Asset</code>:
	 * <p><code><blockquote><pre>
	 *    ASSETDESCRIPTION
	 *    ASSETTYPEID
	 *    ASSETVALUE
	 *    BORROWERID
	 *    INCLUDEINNETWORTH
	 *    PERCENTINNETWORTH
	 * </pre></blockquote></code>
	 *
	 * @param a the <code>Asset</code> to be used as a source.
	 * @param offset determines which column to print the information to.
	 * 		1 for the first data column, otherwise second column.
	 */
	public Vector<Formattable> printAsset(Asset a, int offset) //#DG600 
	{
		//logger.trace( ": Method printAsset entered ");
		String val;
		//StringBuffer buff = new StringBuffer();
		Formattable nota;	//#DG600 
		Vector<Formattable> dnl = new Vector<Formattable>();	//#DG600

		//* ASSETDESCRIPTION
		val = a.getAssetDescription();
		if (!isEmpty(val))	{
			//buff.append(BXResources.getDocPrepIngMsg("ASSET_DESCRIPTION",lang) + DELIMITER + trimDescription(a.getAssetDescription()) + NEW_LINE);
			if (offset == 1)
				nota  = newTabedLine(DUP_CHEK_ASSET_DESCRIPTION, trimDescription(val), null, null);
			else
				nota  = newTabedLine(DUP_CHEK_ASSET_DESCRIPTION, null, trimDescription(val), null);
			dnl.addElement(nota);
		}
		//* ASSETTYPEID
		/*#DG600
        val  = BXResources.getPickListDescription("AssetType", a.getAssetTypeId(), lang);
        buff.append(BXResources.getDocPrepIngMsg("ASSET_TYPE",lang) + DELIMITER + val + NEW_LINE);*/
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_ASSET_TYPE, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_ASSET_TYPE, a.getAssetTypeId()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_ASSET_TYPE, null, 
					BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_ASSET_TYPE, a.getAssetTypeId()), null);
			
		dnl.addElement(nota);

		//* ASSETVALUE
		/*#DG600
        val = formatCurrency(a.getAssetValue());
        buff.append(BXResources.getDocPrepIngMsg("ASSET_VALUE",lang) + DELIMITER + val + NEW_LINE);*/
		//#DG658
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_ASSET_VALUE, 
				BXResources.newFormata(CURRENCY_FORMAT, a.getAssetValue()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_ASSET_VALUE, null, 
					BXResources.newFormata(CURRENCY_FORMAT, a.getAssetValue()), null);
			
		dnl.addElement(nota);

		//* INCLUDEINNETWORTH
		/*#DG600
        val = a.getIncludeInNetWorth() ? YES_LABEL : NO_LABEL;
        buff.append(BXResources.getDocPrepIngMsg("INCLUDE_IN_NET_WORTH",lang) + DELIMITER + val + NEW_LINE);*/
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_INC_IN_NET_WORTH,
				BXResources.newGenMsg(a.getIncludeInNetWorth() ? YES_LABEL : NO_LABEL), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_INC_IN_NET_WORTH, null,
					BXResources.newGenMsg(a.getIncludeInNetWorth() ? YES_LABEL : NO_LABEL), null);
			
		dnl.addElement(nota);

		//* PERCENTINNETWORTH
		/*#DG600
        val = formatRatio(a.getPercentInNetWorth());
        buff.append(BXResources.getDocPrepIngMsg("PERCENT_INCLUDED_IN_NET_WORTH",lang) + DELIMITER + val + NEW_LINE);*/
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_PERC_INC_NET_WORTH,  
				BXResources.newFormata(RATIO_FORMAT, (double)a.getPercentInNetWorth()),	null, null);
		else
			nota  = newTabedLine(DUP_CHEK_PERC_INC_NET_WORTH, null,  
					BXResources.newFormata(RATIO_FORMAT, (double)a.getPercentInNetWorth()), null);
		dnl.addElement(nota);

		//logger.trace( ": Method printAsset finished ");
		//return buff.toString();
		return dnl;
	}

	/**
	 * Compares key fields in a pair of Assets.
	 * <P>
	 * The following fields are compared on each <code>Asset</code>:
	 * <p><code><blockquote><pre>
	 *    ASSETDESCRIPTION
	 *    ASSETTYPEID
	 *    ASSETVALUE
	 *    BORROWERID
	 *    INCLUDEINNETWORTH
	 *    PERCENTINNETWORTH
	 * </pre></blockquote></code>
	 */
	public void compareAssets(Asset a1, Asset a2)
	{
		logger.trace( ": Method compareAssets entered ");
		if (maximumReached()) return;

		String val1, val2;

		//* ASSETDESCRIPTION
		logger.trace( ": Method compareAssets : 1 ");
		val1 = trimDescription(a1.getAssetDescription());
		val2 = trimDescription(a2.getAssetDescription());
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("DESCRIPTION",lang) + DELIMITER +  val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DESCRIPTION, val1, val2))
			return;

		//* ASSETTYPEID
		logger.trace( ": Method compareAssets : 2 ");
		int ival1 = a1.getAssetTypeId();
		int ival2 = a2.getAssetTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("AssetType",a1.getAssetTypeId(), lang);
            val2  = BXResources.getPickListDescription("AssetType",a2.getAssetTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("ASSET_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_ASSET_TYPE, PKL_ASSET_TYPE, ival1, ival2))
			return;

		//* ASSETVALUE
		logger.trace( ": Method compareAssets : 3 ");
		double dval1 = a1.getAssetValue();
		double dval2 = a2.getAssetValue();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(a1.getAssetValue());
            val2 = formatCurrency(a2.getAssetValue());
            dDiff = a2.getAssetValue() - a1.getAssetValue();
            appendNote(BXResources.getDocPrepIngMsg("ASSET_VALUE",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                       DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_ASSET_VALUE, dval1, dval2))
			return;

		//* INCLUDEINNETWORTH
		logger.trace( ": Method compareAssets : 4 ");
		boolean bval1 = a1.getIncludeInNetWorth();
		boolean bval2 = a2.getIncludeInNetWorth();
		/* #DG658
		if (bval1 != bval2)
		{
			val1 = bval1 ? YES_LABEL : NO_LABEL;
			val2 = bval2 ? YES_LABEL : NO_LABEL;
			/ *#DG600 
            appendNote(BXResources.getDocPrepIngMsg("INCLUDE_IN_NET_WORTH",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			nota  = newTabedLine(DUP_CHEK_INC_IN_NET_WORTH,
					BXResources.newGenMsg(val1),BXResources.newGenMsg(val2), null);
			dealNotesList.addElement(nota);
			totalDifferences++;
			if (maximumReached()) 
				return;
		}*/
		if(comparBool(DUP_CHEK_INC_IN_NET_WORTH, bval1, bval2))
			return;

		//* PERCENTINNETWORTH
		logger.trace( ": Method compareAssets : 5 ");
		dval1 = a1.getPercentInNetWorth();
		dval2 = a2.getPercentInNetWorth();
		/*#DG600 
	  if (dval1 != dval2)	  {
	      val1 = formatRatio(a1.getPercentInNetWorth());
	      val2 = formatRatio(a2.getPercentInNetWorth());
	      iDiff = a2.getPercentInNetWorth() - a1.getPercentInNetWorth();
	      appendNote(BXResources.getDocPrepIngMsg("PERCENT_INCLUDED_IN_NET_WORTH",lang) + DELIMITER + val1 + DELIMITER + val2 +
	                                             DELIMITER + formatRatio(iDiff));
	      totalDifferences++;
	  }*/
		if(comparDbl(DUP_CHEK_PERC_INC_NET_WORTH, RATIO_FORMAT, dval1, dval2))
			return;

		logger.trace( ": Method compareAssets finished ");
	}

	/**
	 * The following fields are printed on each <code>DownPaymentSource</code>:
	 * <p><code><blockquote><pre>
	 *    AMOUNT
	 *    DOWNPAYMENTSOURCETYPEID
	 *    DPSDESCRIPTION
	 * </pre></blockquote></code>
	 *
	 * @param dps the <code>DownPaymentSource</code> to use as a source.
	 */
	/*#DG600 rewritten
    public static String printDownPaymentSource(DownPaymentSource dps)
    {
        StringBuffer buff = new StringBuffer();
        String val;
        //* DPSDESCRIPTION
        if (!isEmpty(dps.getDPSDescription()))
           buff.append(BXResources.getDocPrepIngMsg("DOWN_PAYMENT_DESCRIPTION",lang) + DELIMITER + dps.getDPSDescription() + NEW_LINE);
        //* DOWNPAYMENTSOURCETYPEID
        val  = BXResources.getPickListDescription(DOWN_PAY_SRC_TYPE,dps.getDownPaymentSourceTypeId(), lang);
        buff.append(BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE",lang) + DELIMITER + val + NEW_LINE);
        //* AMOUNT
        val = formatCurrency(dps.getAmount());
        buff.append(BXResources.getDocPrepIngMsg("DOWN_PAYMENT",lang) + DELIMITER + val + NEW_LINE);
        return buff.toString();
    }*/
	public Vector<Formattable> printDownPaymentSource(DownPaymentSource dps, int offset)
	{
		Formattable nota;
		Vector<Formattable> dealNotesList = new Vector<Formattable>();	//#DG600
		//* DPSDESCRIPTION
		if (!isEmpty(dps.getDPSDescription())) {
			if (offset == 1)
				nota  = newTabedLine(DUP_CHEK_DOWN_PAY_DESCR, dps.getDPSDescription(),null,null);
			else
				nota  = newTabedLine(DUP_CHEK_DOWN_PAY_DESCR, null, dps.getDPSDescription(),null);
			dealNotesList.addElement(nota);
		}    	
		//* DOWNPAYMENTSOURCETYPEID
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_DOWN_PAY_SRC, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_DOWN_PAY_SRC_TYPE,dps.getDownPaymentSourceTypeId()), null,null);
		else
			nota  = newTabedLine(DUP_CHEK_DOWN_PAY_SRC, null, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_DOWN_PAY_SRC_TYPE,dps.getDownPaymentSourceTypeId()),null);
		dealNotesList.addElement(nota);
		//* AMOUNT
		if (offset == 1)
			nota  = newTabedLine(DUP_CHEK_DOWN_PAYMENT, 
				BXResources.newFormata(CURRENCY_FORMAT,dps.getAmount()), null, null);
		else
			nota  = newTabedLine(DUP_CHEK_DOWN_PAYMENT, null, 
				BXResources.newFormata(CURRENCY_FORMAT,dps.getAmount()), null);
		dealNotesList.addElement(nota);
		return dealNotesList;
	}

	/**
	 * Compares key fields in a pair of DownPaymentSources.
	 * <P>
	 * The following fields are compared on each <code>DownPaymentSource</code>:
	 * <p><code><blockquote><pre>
	 *    AMOUNT
	 *    DOWNPAYMENTSOURCEID
	 *    DOWNPAYMENTSOURCETYPEID
	 *    DPSDESCRIPTION
	 * </pre></blockquote></code>
	 */
	public boolean compareDownPaymentSources(DownPaymentSource dps1, DownPaymentSource dps2)
	{
		logger.trace( ": Method compareDownPaymentSources entered ");
		if (maximumReached()) 
			return true;

		//#DG600 double dDiff;	
		String val1;  	
		String val2;

		//* DPSDESCRIPTION
		val1 = dps1.getDPSDescription();	
		val2 = dps2.getDPSDescription();
		/*#DG600 
        if (!isEqual(val1, val2))
        {
        	appendNote(BXResources.getDocPrepIngMsg("DOWN_PAYMENT_DESCRIPTION",lang) + DELIMITER + dps1.getDPSDescription() + DELIMITER + dps2.getDPSDescription());
        	appendNote(nota);
        	totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DUP_CHEK_DOWN_PAY_DESCR, val1, val2))
			return true;

		//* AMOUNT
		double dval1 = dps1.getAmount();
		double dval2 = dps2.getAmount();
		/*#DG600 
      if (dval1 != dval2)
      {
          val1 = formatCurrency(dps1.getAmount());
          val2 = formatCurrency(dps2.getAmount());
          dDiff = dps2.getAmount() - dps1.getAmount();
          appendNote(BXResources.getDocPrepIngMsg("DOWN_PAYMENT",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                      DELIMITER + formatCurrency(dDiff));
          totalDifferences++;
      }
      if (maximumReached()) */
		if(comparDbl(DUP_CHEK_DOWN_PAYMENT, dval1, dval2))
			return true;

		//* DOWNPAYMENTSOURCETYPEID
		int ival1 = dps1.getDownPaymentSourceTypeId();
		int ival2 = dps2.getDownPaymentSourceTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("DownPaymentSourceType",dps1.getDownPaymentSourceTypeId(), lang);
            val2  = BXResources.getPickListDescription("DownPaymentSourceType", dps2.getDownPaymentSourceTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
            totalDifferences++;
        }
        logger.trace( ": Method compareDownPaymentSources finished ");
		 */
		if(comparPkL(DUP_CHEK_DOWN_PAY_SRC_TYPE, PKL_DOWN_PAY_SRC_TYPE, ival1, ival2))
			return true;
		return false;
	}

	/**
	 * Compares key fields in a pair of PropertyExpenses.
	 * <P>
	 * The following fields are compared on each <code>PropertyExpense</code>:
	 * <p><code><blockquote><pre>
	 *    PROPERTYEXPENSEPERIODID
	 *    PROPERTYEXPENSETYPEID
	 *    PROPERTYEXPENSEAMOUNT
	 *    PROPERTYEXPENSEDESCRIPTION
	 *    MONTHLYEXPENSEAMOUNT
	 *    PEINCLUDEINGDS
	 *    PEINCLUDEINTDS
	 *    PEPERCENTINGDS
	 *    PEPERCENTINTDS
	 *    PEPERCENTOUTGDS
	 *    PEPERCENTOUTTDS
	 * </pre></blockquote></code>
	 */
	public void comparePropertyExpenses(PropertyExpense pe1, PropertyExpense pe2)
	{
		logger.trace( ": Method comparePropertyExpenses started ");
		if (maximumReached()) return;

		String val1, val2;
		/*double dDiff;
        int iDiff;*/

		//*    PROPERTYEXPENSEDESCRIPTION
		val1 = pe1.getPropertyExpenseDescription();
		val2 = pe2.getPropertyExpenseDescription();	//#DG634
		/*#DG600 
        if (!isEqual(val1, val2))
        {
            appendNote(BXResources.getDocPrepIngMsg("DESCRIPTION",lang) + DELIMITER + pe1.getPropertyExpenseDescription() +
                                       DELIMITER + pe2.getPropertyExpenseDescription());
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparStr(DESCRIPTION, val1, val2))
			return;

		//*    PROPERTYEXPENSEPERIODID
		int ival1 = pe1.getPropertyExpensePeriodId();
		int ival2 = pe2.getPropertyExpensePeriodId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("PropertyExpensePeriod", pe1.getPropertyExpensePeriodId(), lang);
            val2  = BXResources.getPickListDescription("PropertyExpensePeriod", pe2.getPropertyExpensePeriodId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("EXPENSE_PERIOD",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_EXPENSE_PERIOD, PKL_PROPERTY_EXPENSE_PERIOD, ival1, ival2))
			return;

		//*    PROPERTYEXPENSETYPEID
		ival1 = pe1.getPropertyExpenseTypeId();
		ival2 = pe2.getPropertyExpenseTypeId();
		/*#DG600 
        if (ival1 != ival2)
        {
            val1  = BXResources.getPickListDescription("PropertyExpenseType", pe1.getPropertyExpenseTypeId(), lang);
            val2  = BXResources.getPickListDescription("PropertyExpenseType", pe2.getPropertyExpenseTypeId(), lang);
            appendNote(BXResources.getDocPrepIngMsg("EXPENSE_TYPE",lang) + DELIMITER + val1 + DELIMITER + val2);
        }
        if (maximumReached()) */
		if(comparPkL(DUP_CHEK_EXPENSE_TYPE, PKL_PROPERTY_EXPENSE_TYPE, ival1, ival2))
			return;

		//*    PROPERTYEXPENSEAMOUNT
		double dval1 = pe1.getPropertyExpenseAmount();
		double dval2 = pe2.getPropertyExpenseAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(pe1.getPropertyExpenseAmount());
            val2 = formatCurrency(pe2.getPropertyExpenseAmount());
            dDiff = pe2.getPropertyExpenseAmount() - pe1.getPropertyExpenseAmount();
            appendNote(BXResources.getDocPrepIngMsg("AMOUNT",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                  DELIMITER + formatCurrency(dDiff));

            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(AMOUNT, dval1, dval2))
			return;

		//*    MONTHLYEXPENSEAMOUNT
		dval1 = pe1.getMonthlyExpenseAmount();
		dval2 = pe2.getMonthlyExpenseAmount();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatCurrency(pe1.getMonthlyExpenseAmount());
            val2 = formatCurrency(pe2.getMonthlyExpenseAmount());
            dDiff = pe2.getMonthlyExpenseAmount() - pe1.getMonthlyExpenseAmount();
            appendNote(BXResources.getDocPrepIngMsg("MONTHLY_EXPENSE",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                           DELIMITER + formatCurrency(dDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_MONTHLY_EXPENSE, dval1, dval2))
			return;

		//*    PEINCLUDEINGDS
		boolean bval1 = pe1.getPeIncludeInGDS();
		boolean bval2 = pe2.getPeIncludeInGDS();
		/* #DG658
		if (bval1 != bval2)
		{
			val1 = bval1 ? YES_LABEL : NO_LABEL;
			val2 = bval2 ? YES_LABEL : NO_LABEL;
			/ *#DG600 
            appendNote(BXResources.getDocPrepIngMsg("INCLUDE_IN_GDS",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_GDS,
					BXResources.newGenMsg(val1),BXResources.newGenMsg(val2), null);
			dealNotesList.addElement(nota);
			totalDifferences++;
			if (maximumReached()) 
				return;
		}*/
		if(comparBool(DUP_CHEK_INCLUDE_IN_GDS, bval1, bval2))
			return;

		bval1 = pe1.getPeIncludeInTDS();
		bval2 = pe2.getPeIncludeInTDS();
		//*    PEINCLUDEINTDS
		/* #DG658
		if (bval1 != bval2)
		{
			val1 = bval1 ? YES_LABEL : NO_LABEL;
			val2 = bval2 ? YES_LABEL : NO_LABEL;
			/ *#DG600 
            appendNote(BXResources.getDocPrepIngMsg("INCLUDE_IN_TDS",lang) + DELIMITER + val1 + DELIMITER + val2);* /
			nota  = newTabedLine(DUP_CHEK_INCLUDE_IN_TDS,
					BXResources.newGenMsg(val1),BXResources.newGenMsg(val2), null);
			dealNotesList.addElement(nota);
			totalDifferences++;
			if (maximumReached()) 
				return;
		}*/
		if(comparBool(DUP_CHEK_INCLUDE_IN_TDS, bval1, bval2))
			return;

		//*    PEPERCENTINGDS
		dval1 = pe1.getPePercentInGDS();
		dval2 = pe2.getPePercentInGDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(pe1.getPePercentInGDS());
            val2 = formatRatio(pe2.getPePercentInGDS());
            iDiff = pe2.getPePercentInGDS() - pe1.getPePercentInGDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_INCLUDED_IN_GDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                             DELIMITER + formatRatio(iDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERC_INC_IN_GDS, RATIO_FORMAT, dval1, dval2))
			return;

		//*    PEPERCENTINTDS
		dval1 = pe1.getPePercentInTDS();
		dval2 = pe2.getPePercentInTDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(pe1.getPePercentInTDS());
            val2 = formatRatio(pe2.getPePercentInTDS());
            iDiff = pe2.getPePercentInTDS() - pe1.getPePercentInTDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_INCLUDED_IN_TDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                             DELIMITER + formatRatio(iDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERC_INC_IN_TDS, RATIO_FORMAT, dval1, dval2))
			return;

		//*    PEPERCENTOUTGDS
		dval1 = pe1.getPePercentOutGDS();
		dval2 = pe2.getPePercentOutGDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(pe1.getPePercentOutGDS());
            val2 = formatRatio(pe2.getPePercentOutGDS());
            iDiff = pe2.getPePercentOutGDS() - pe1.getPePercentOutGDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_INCLUDED_OUT_GDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                              DELIMITER + formatRatio(iDiff));
            totalDifferences++;
        }
        if (maximumReached()) */
		if(comparDbl(DUP_CHEK_PERC_INC_OUT_GDS, RATIO_FORMAT, dval1, dval2))
			return;

		//*    PEPERCENTOUTTDS
		dval1 = pe1.getPePercentOutTDS();
		dval2 = pe2.getPePercentOutTDS();
		/*#DG600 
        if (dval1 != dval2)
        {
            val1 = formatRatio(pe1.getPePercentOutTDS());
            val2 = formatRatio(pe2.getPePercentOutTDS());
            iDiff = pe2.getPePercentOutTDS() - pe1.getPePercentOutTDS();
            appendNote(BXResources.getDocPrepIngMsg("PERCENT_INCLUDED_OUT_TDS",lang) + DELIMITER + val1 + DELIMITER + val2 +
                                              DELIMITER + formatRatio(iDiff));
            totalDifferences++;
        }*/
		if(comparDbl(DUP_CHEK_PERC_INC_OUT_TDS, RATIO_FORMAT, dval1, dval2))
			return;

		logger.trace( ": Method comparePropertyExpenses finished ");
	}

	/**
	 * Compares the primary properties of a deal.
	 */
	public void comparePrimaryProperties()
	{
		if (maximumReached()) return;

		Property oProp = getPrimaryProperty(deal1);
		Property nProp = getPrimaryProperty(deal2);

		compareProperties(oProp, nProp);
	}

	/**
	 * Compares using all the comparison logic present in this class.  This includes all
	 * parts of a deal and primary properties.
	 * @throws FinderException 
	 * @throws RemoteException 
	 */
	public void compareAll() throws RemoteException, FinderException
	{
		logger.trace( ": Method compareAll entered ");
		if (maximumReached()) return;

		Property oProp = getPrimaryProperty(deal1);
		Property nProp = getPrimaryProperty(deal2);

		compareProperties(oProp, nProp);

		if (totalDifferences > 0)
			//#DG600 appendNote("");
			dealNotesList.addElement(BXResources.newStrFormata(""));        	

		compareDeals();
		logger.trace( ": Method compareAll finished ");
	}

	/*#DG600 not used
    /**
	 * Output the current note in text format
	 *
	 * @return the note in text format
	 * /
    public String formatNote()
    {
        return formatNote(PARSE_TEXT);
    }

    /**
	 * Output the current note text to a specific format.  Text and HTML are
	 * only supported at this point, but the framework for adding new
	 * formats exists.
	 *
	 * @param type the format to output the note's text to.
	 * @return the formatted note.
	 * /
    public String formatNote(int type)
    {
        if (dealNotesList.size() == 0)
            return "";

        StringBuffer newStr = new StringBuffer();
        int ind;

        Vector<Formattable> totalNotes = new Vector<Formattable>(dealNotesHdrList);
        totalNotes.addAll(dealNotesList);

        Iterator i = totalNotes.iterator();
        StringTokenizer st;
        String str, line;
        int index = 0;

        switch (type)
        {
            case PARSE_TEXT:
            {
                while(i.hasNext())
                {
                    st = new StringTokenizer((String)i.next(), DELIMITER);
                    index = 0;
                    line = "";

                    while (st.hasMoreTokens())
                    {
                        str = st.nextToken();
                        line += str;

                        try
                        {
                            if (line.length() <= tabStops[index])
                            {
                                while (line.length() <= tabStops[index])
                                    line += " ";
                            }
                            //  The string is longer than the following tabstop
                            else
                                line += "   ";
                        }
                        //  This exception will occur when trying to get
                        //  a tabstop when it doesn't exist.  This is ok, as we'll
                        //  just append 3 spaces to the end.
                        catch (ArrayIndexOutOfBoundsException e)
                        {
                            line += "   ";
                        }
                        index++;
                    }
                    newStr.append(line).append("\n");
                }

                break;
            }

            case PARSE_HTML:
            {
                while(i.hasNext())
                {
                    st = new StringTokenizer((String)i.next(), DELIMITER);
                    index = 0;
                    line = "";

                    while (st.hasMoreTokens())
                    {
                        str = st.nextToken();
                        line += str;

                        try
                        {
                            if (line.length() <= tabStops[index])
                            {
                                while (line.length() <= tabStops[index])
                                    line += "&nbsp;";
                            }
                            //  The string is longer than the following tabstop
                            else
                                line += "&nbsp;&nbsp;&nbsp;";
                        }
                        //  This exception will occur when trying to get
                        //  a tabstop when it doesn't exist.  This is ok, as we'll
                        //  just append 3 spaces to the end.
                        catch (ArrayIndexOutOfBoundsException e)
                        {
                            line += "&nbsp;&nbsp;&nbsp;";
                        }
                        index++;
                    }
                    newStr.append(line).append("<BR>");
                }

                break;
            }

            default:  break;
        }

        return newStr.toString();
    } 

    /**
	 * Utility method that formats <code>months</code> using the current doc language
	 * settings.
	 *
	 * @param months number to format into the relevant year/month style
	 *
	 * @return The formatted number.
	 * /
    protected static String formatYearsandMonths(int months)
    {
        return DocPrepLanguage.getInstance().getYearsAndMonths(months, lang);
    }

    /**
	 * Utility method that formats <code>amt</code> using the current doc language
	 * settings.
	 *
	 * @param amt number to format into the relevant currency style
	 *
	 * @return The formatted number.
	 * /
    protected static String formatCurrency(double amt)
    {
        return DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang);
    }

    /**
	 * Utility method that formats <code>ratio</code> using the current doc language
	 * settings.
	 *
	 * @param ratio number to format into the relevant ratio style
	 *
	 * @return The formatted number.
	 * /
    protected static String formatRatio(double ratio)
    {
        return DocPrepLanguage.getInstance().getFormattedRatio(ratio, lang);
    }

    /**
	 * Utility method that formats <code>rate</code> using the current doc language
	 * settings.
	 *
	 * @param ratio number to format into the relevant interest rate style
	 *
	 * @return The formatted rate.
	 * /
    protected static String formatInterestRate(double rate)
    {
        return DocPrepLanguage.getInstance().getFormattedInterestRate(rate, lang);
    }*/

	/**
	 * Utility method that grabs the primary property for <code>d</code>.
	 *
	 * @param d the <code>Deal</code> to grab the primary property from.
	 * @return the primary property. <code>null</code> is returned if one
	 *         can't be found, or an error occurs.
	 */
	protected Property getPrimaryProperty(Deal d)
	{
		Property pProperty = null;

		try
		{
			pProperty = new Property(srk,null);

			pProperty = pProperty.findByPrimaryProperty(d.getDealId(), 
			                                            d.getCopyId(), d.getInstitutionProfileId());
		}
		catch (Exception e) {}

		return pProperty;
	}

	/**
	 * Adds each line of <code>str</code> to <code>v</code>, delimited by carriage returns.
	 *
	 * @param v the <code>Vector</code> to add the lines to.
	 * @param str the carriage return delimited <code>String</code> that is
	 *            to be broken up into <code>Vector</code> elements.
	 * @return 
	 *
	 * @return whether the transference was successful.
	 */
	/*#DG600 phased out
     protected boolean addTo(Vector v, String str)
    {
        try
        {
            StringTokenizer st = new StringTokenizer(str, NEW_LINE);
            String temp;

            while (st.hasMoreTokens())
            {
                v.addElement(st.nextToken());
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }*/

	/**
	 * Performs the actual compare of Assets, indicating whether they are
	 * new, deleted or changed.
	 *
	 * @param l1 The first <code>Asset</code> list to use for comparison.
	 * @param l2 The second <code>Asset</code> list to use for comparison.
	 *
	 *Dave made public, from protected
	 */
	public void doAssetListCompare(List<Asset> l1, List<Asset> l2)
	{
		if (l1 == null || l2 == null)
			return;

		class Asc implements Comparator<Asset> {
			public int compare(Asset a1, Asset a2) {
				String val1 = a1.getAssetDescription(); 
				String val2 = a2.getAssetDescription(); 
				if(val1 == null) 
					val1 = lowAlfaVal;
				if(val2 == null) 
					val2 = lowAlfaVal;
				if (val1.equals(val2))
				{
					final int ival1 = a1.getAssetTypeId();
					final int ival2 = a2.getAssetTypeId();
					if (ival1 == ival2)
						return Double.compare(a1.getAssetValue(), a2.getAssetValue());
					else
						return (ival1<ival2 ? -1 : (ival1==ival2 ? 0 : 1));
				}
				return val1.compareTo(val2);
			}
		};     
		Asc asca = new Asc();

		Collections.sort(l1, asca);
		Collections.sort(l2, asca);

		Asset a1, a2;
		boolean found;
		String desc1, desc2;
		Formattable nota;	//#DG600 

		for (int i=0; i < l1.size(); i++)
		{
			a1 = (Asset)l1.get(i);
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				a2 = (Asset)l2.get(j);
				desc1 = trimDescription(a1.getAssetDescription());
				desc2 = trimDescription(a2.getAssetDescription());

				if (isEqual(desc1, desc2))
				{
					if (a1.getAssetTypeId() != a2.getAssetTypeId() ||
							a1.getAssetValue() != a2.getAssetValue() ||
							a1.getIncludeInNetWorth() != a2.getIncludeInNetWorth() ||
							a1.getPercentInNetWorth() != a2.getPercentInNetWorth())
					{
						//appendNote(NEW_LINE + "*  Asset '" + desc1 + "' Changed  *");
						/*#DG600
                        appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("ASSET",lang) +"'" + 
                        	desc1 + "'"+ BXResources.getDocPrepIngMsg("CHANGED",lang) +" *");*/          
						nota = BXResources.newFormata(DUP_CHEK_ASSET_CHANGED, desc1);
						dealNotesList.addElement(nota);    			    					
						compareAssets(a1, a2); 
					}

					//  We want to decrement the outer loop since the item was removed
					//  and the loop will increment.  The inner loop will begin again,
					//  so it doesn't matter what we do to it.
					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				//appendNote(NEW_LINE + "*  Asset Deleted  *");
				/*appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("ASSET_DELETED",lang) +" *");
                addTo(dealNotesList, printAsset(a1));*/                
				nota = BXResources.newFormata(DUP_CHEK_ASSET_DELETED);
				dealNotesList.addElement(nota);    			                
				dealNotesList.addAll(printAsset(a1, 1));                
				totalDifferences++;
			}
		}

		//  All remaining items in list 2 are considered new
		for (Asset asset : l2) {
			//appendNote(NEW_LINE + "*  Asset Added *");
			/*appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("ASSET_ADDED",lang) +" *");
            addTo(dealNotesList, printAsset((Asset)i.next()));*/
			nota = BXResources.newFormata(DUP_CHEK_ASSET_ADDED);
			dealNotesList.addElement(nota);
			dealNotesList.addAll(printAsset(asset, 2));                
			totalDifferences++;
		}
	}

	// ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
	/**
	 * Performs the actual compare of BorrowerIdentification, indicating whether
	 * they are new, deleted or changed.
	 * 
	 * @param l1
	 *            The first <code>BorrowerIdentification</code> list to use
	 *            for comparison.
	 * @param l2
	 *            The second <code>BorrowerIdentification</code> list to use
	 *            for comparison.
	 * 
	 */
	protected void doBorrowerIdentificationListCompare(
			List<BorrowerIdentification> l1, List<BorrowerIdentification> l2) {
		if (l1 == null || l2 == null)
			return;

		//#DG600 Comparator c = new BorrowerIdentificationComparator();
		class Bic implements Comparator<BorrowerIdentification> {
			public int compare(BorrowerIdentification bi1, BorrowerIdentification bi2) {
				final int ival1 = bi1.getIdentificationTypeId();
				final int ival2 = bi2.getIdentificationTypeId();
				return (ival1<ival2 ? -1 : (ival1==ival2 ? 0 : 1));
			}
		};     
		Bic bica = new Bic();		

		Collections.sort(l1, bica);
		Collections.sort(l2, bica);

		BorrowerIdentification bi1, bi2;
		boolean found;
		// String desc1, desc2;
		Formattable nota;	//#DG600 

		for (int i = 0; i < l1.size(); i++) {
			bi1 = (BorrowerIdentification) l1.get(i);
			found = false;

			for (int j = 0; j < l2.size(); j++) {
				bi2 = (BorrowerIdentification) l2.get(j);

				if (bi1.getIdentificationTypeId() == bi2
						.getIdentificationTypeId()) {
					if (!(bi1.getIdentificationNumber().equalsIgnoreCase(bi2
							.getIdentificationNumber()))
							|| (!bi1.getIdentificationCountry()
									.equalsIgnoreCase(
											bi2.getIdentificationCountry()))) {

						/*#DG600
						appendNote(NEW_LINE
								+ "* "
								+ BXResources.getDocPrepIngMsg(
										"BORROWERIDENTIFICATION", lang) + "'"
								+ bi1.getIdentificationNumber() + "'"
								+ BXResources.getDocPrepIngMsg("CHANGED", lang)
								+ " *");*/ // "'"+ bi1.getIdentificationNumber() + "'");
						nota = BXResources.newFormata(DUP_CHEK_BOROWERID_CHANGED,  bi1.getIdentificationNumber());
						dealNotesList.addElement(nota);   			    					

						compareBorrowerIdentifications(bi1, bi2);
					}

					// We want to decrement the outer loop since the item was
					// removed
					// and the loop will increment. The inner loop will begin
					// again,
					// so it doesn't matter what we do to it.
					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			// If the item wasn't found in List 2, then it has been deleted
			if (!found) {
				//#DG600 
				/*appendNote(NEW_LINE
						+ "* "
						+ BXResources.getDocPrepIngMsg(
								"BORROWERIDENTIFICATION_DELETED", lang) + " *");
				addTo(dealNotesList, printBorrowerIdentification(bi1));*/				
				nota = BXResources.newFormata(DUP_CHEK_BOROWERID_DELETED);
				dealNotesList.addElement(nota);    			                
				dealNotesList.addAll(printBorrowerIdentification(bi1, 1));				
				totalDifferences++;
			}
		}

		for (BorrowerIdentification bi : l2) {
			/*#DG600 
                appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("LIABILITY_DELETED",lang) + "  *");
			addTo(dealNotesList, printBorrowerIdentification((BorrowerIdentification) i
					.next()));*/
			nota = BXResources.newFormata(DUP_CHEK_BOROWERID_ADDED);
			dealNotesList.addElement(nota);    			                
			dealNotesList.addAll(printBorrowerIdentification(bi, 2));				
			totalDifferences++;
		}
	}

	// ***** Change by NBC Impl. Team - Version 1.6 - Start *****//

	/**
	 * Performs the actual compare of BorrowerAddresses, indicating whether they are
	 * new, deleted or changed.
	 *
	 * @param l1 The first <code>BorrowerAddress</code> list to use for comparison.
	 * @param l2 The second <code>BorrowerAddress</code> list to use for comparison.
	 *
	 */
	protected void doBorrowerAddressListCompare
	(List<BorrowerAddress> l1, List<BorrowerAddress> l2)	//#DG600 
	{
		if (l1 == null || l2 == null)
			return;

		Comparator<BorrowerAddress> c = new CurrentBorrowerAddressComparator();

		Collections.sort(l1, c);
		Collections.sort(l2, c);

		BorrowerAddress ba1, ba2;
		Addr a1, a2;
		boolean found;
		Formattable nota, notb;		//#DG600 

		for (int i=0; i < l1.size(); i++)
		{
			ba1 = (BorrowerAddress)l1.get(i);
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				ba2 = (BorrowerAddress)l2.get(j);

				try
				{
					a1 = ba1.getAddr();
				}
				catch (Exception e)
				{
					a1 = null;
				}

				try
				{
					a2 = ba2.getAddr();
				}
				catch (Exception e)
				{
					a2 = null;
				}

				if ((ba1.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT &&
						ba2.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT) ||
						isEqual(a1.getAddressLine1(), a2.getAddressLine1()))
				{
					if (ba1.getBorrowerAddressTypeId() != ba2.getBorrowerAddressTypeId() ||
							ba1.getMonthsAtAddress() != ba2.getMonthsAtAddress() ||
							ba1.getResidentialStatusId() != ba2.getResidentialStatusId() ||
							(a1 == null && a1 != a2) ||
							(a1 != null &&
									!isEqual(a1.getAddressLine1(), a2.getAddressLine1()) ||
									!isEqual(a1.getAddressLine2(), a2.getAddressLine2()) ||
									!isEqual(a1.getCity(), a2.getCity()) ||
									!isEqual(a1.getPostalFSA(), a2.getPostalFSA()) ||
									!isEqual(a1.getPostalLDU(), a2.getPostalLDU()) ||
									a1.getProvinceId() != a2.getProvinceId()))
					{
						int ival1 = ba1.getBorrowerAddressTypeId();
						int ival2 = ba2.getBorrowerAddressTypeId();
						if (ival1 == Mc.BT_ADDR_TYPE_CURRENT &&
								ival2 == Mc.BT_ADDR_TYPE_CURRENT)
							/*#DG600
                            desc = "(" + PicklistData.getDescription("BorrowerAddressType",
                               ba1.getBorrowerAddressTypeId()) + ")";*/
                               notb = BXResources.newStrFormata("(", BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_BORROWER_ADDRESS_TYPE,ival1),")");
                               else
                              	 //#DG600 desc = "'" + a1.getAddressLine1() + "'";
						notb = BXResources.newStrFormata("'" + a1.getAddressLine1() + "'");

						//#DG600 appendNote(NEW_LINE + "***" + BXResources.getDocPrepIngMsg("BORROWER_ADDRESS",lang) + " + desc + " + BXResources.getDocPrepIngMsg("CHANGED",lang)  +" ***");
						nota = BXResources.newFormata(DUP_CHEK_BOR_ADR_CHANGED, notb);
						dealNotesList.addElement(nota);    			
						compareBorrowerAddresses(ba1, ba2);
					}

					//  We want to decrement the outer loop since the item was removed
					//  and the loop will increment.  The inner loop will begin again,
					//  so it doesn't matter what we do to it.
					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				//#DG600 appendNote(NEW_LINE + "* "+ BXResources.getDocPrepIngMsg("BORROWER_ADDRESS_DELETED",lang) +" *");
				nota = BXResources.newFormata(DUP_CHEK_BOR_ADR_DELETED);
				dealNotesList.addElement(nota);    			                
				//#DG600 addTo(dealNotesList, printBorrowerAddress(ba1));
				dealNotesList.addAll(printBorrowerAddress(ba1, 1));
				totalDifferences++;
			}
		}

		Iterator i = l2.iterator();

		//  All remaining items in list 2 are considered new
		while (i.hasNext())
		{
			//#DG600 appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("BORROWER_ADDRESS_ADDED",lang) +" *");
			nota = BXResources.newFormata(DUP_CHEK_BOR_ADR_ADDED);
			dealNotesList.addElement(nota);    			            
			//#DG600 addTo(dealNotesList, printBorrowerAddress((BorrowerAddress)i.next()));
			dealNotesList.addAll(printBorrowerAddress((BorrowerAddress)i.next(), 2));
			totalDifferences++;
		}
	}

	/**
	 * Performs the actual compare of Borrowers, indicating whether they are
	 * new, deleted or changed.
	 *
	 * @param l1 The first <code>Borrower</code> list to use for comparison.
	 * @param l2 The second <code>Borrower</code> list to use for comparison.
	 *
	 */
	protected void doBorrowerListCompare(List<Borrower> l1, List<Borrower> l2)
	{
		if (l1 == null || l2 == null)
			return;

		// Catherine, for #1683 ------------------------ start ----------
		/*
        Comparator c = new PrimaryBorrowerComparator();
		 */
		// Catherine, for #1683 ------------------------ end ----------

		Comparator<Borrower> borComp = new DJBorrowerOrderComparator();

		Collections.sort(l1, borComp);
		Collections.sort(l2, borComp);

		Formattable nota;		//#DG600 
		Borrower b1, b2;
		boolean found;
		String name;
		List incList1, incList2;

		for (int i=0; i < l1.size(); i++)
		{
			b1 = l1.get(i);

			//#DG600 name = b1.getBorrowerFirstName() + " " + b1.getBorrowerLastName();
			name = b1.getBorrowerMiddleInitial();
			name = isEmpty(name)? "":name+' ';
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				b2 = (Borrower)l2.get(j);

				if ((isEqual(b1.getSocialInsuranceNumber(), b2.getSocialInsuranceNumber()) &&
						!isZeroes(b1.getSocialInsuranceNumber()) &&
						b1.getSocialInsuranceNumber() != null) ||
						(isEqual(b1.getBorrowerFirstName(), b2.getBorrowerFirstName()) &&
								isEqual(b1.getBorrowerLastName(), b2.getBorrowerLastName())))
				{
					try
					{
						incList1 = (List)b1.getIncomes();
					}
					catch (Exception e) { incList1 = null; }

					try
					{
						incList2 = (List)b2.getIncomes();
					}
					catch (Exception e) { incList2 = null; }

					// --------------------- DJ, pvcs # 1201, Catherine -- begin --------------------- 
					boolean bHasChanged;
					bHasChanged = 
						(!isEqual(b1.getBorrowerFirstName(), b2.getBorrowerFirstName()) ||
								!isEqual(b1.getBorrowerLastName(), b2.getBorrowerLastName()) ||
								b1.getTotalLiabilityPayments() != b2.getTotalLiabilityPayments() ||
								b1.getTotalLiabilityAmount() != b2.getTotalLiabilityAmount() ||
								b1.getTotalIncomeAmount() != b2.getTotalIncomeAmount() ||
								b1.getTotalAssetAmount() != b2.getTotalAssetAmount() ||
								!isEqual(b1.getSocialInsuranceNumber(), b2.getSocialInsuranceNumber()) ||
								b1.getNumberOfTimesBankrupt() != b2.getNumberOfTimesBankrupt() ||
								b1.getNumberOfDependents() != b2.getNumberOfDependents() ||
								b1.getMaritalStatusId() != b2.getMaritalStatusId() ||
								b1.getLanguagePreferenceId() != b2.getLanguagePreferenceId() ||
								!isEqual(b1.getFirstTimeBuyer(), b2.getFirstTimeBuyer()) ||
								!isEqual(b1.getCreditSummary(), b2.getCreditSummary()) ||
								b1.getCreditScore() != b2.getCreditScore() ||
								!isEqual(b1.getCreditBureauSummary(), b2.getCreditBureauSummary()) ||
								!isEqual(b1.getCreditBureauOnFileDate(), b2.getCreditBureauOnFileDate()) ||
								b1.getCreditBureauNameId() != b2.getCreditBureauNameId() ||
								b1.getCitizenshipTypeId() != b2.getCitizenshipTypeId() ||
								!isEqual(b1.getBureauAttachment(), b2.getBureauAttachment()) ||
								!isEqual(b1.getBorrowerWorkPhoneNumber(), b2.getBorrowerWorkPhoneNumber()) ||
								!isEqual(b1.getBorrowerWorkPhoneExtension(), b2.getBorrowerWorkPhoneExtension()) ||
								b1.getBorrowerTypeId() != b2.getBorrowerTypeId() ||
								!isEqual(b1.getBorrowerMiddleInitial(), b2.getBorrowerMiddleInitial()) ||
								!isEqual(b1.getBorrowerHomePhoneNumber(), b2.getBorrowerHomePhoneNumber()) ||
								b1.getBorrowerGeneralStatusId() != b2.getBorrowerGeneralStatusId() ||
								!isEqual(b1.getBorrowerFaxNumber(), b2.getBorrowerFaxNumber()) ||
								!isEqual(b1.getBorrowerEmailAddress(), b2.getBorrowerEmailAddress()) ||
								!isEqual(b1.getBorrowerBirthDate(), b2.getBorrowerBirthDate()) ||
								b1.getBankruptcyStatusId() != b2.getBankruptcyStatusId() ||
								b1.getAge() != b2.getAge() ||
								b1.getPrefContactMethodId() != b2.getPrefContactMethodId() ||
								b1.getBorrowerGenderId() != b2.getBorrowerGenderId() ||
								b1.getSmokeStatusId() != b2.getSmokeStatusId() ||
								b1.getSolicitationId() != b2.getSolicitationId() ||
								b1.getStaffOfLender() !=  b2.getStaffOfLender() ||
								b1.getEmployeeNumber() != b2.getEmployeeNumber() ||
								!isEqual(incList1, incList2));

					if (!bHasChanged && (PropertiesCache.getInstance().getProperty(-1, COM_BASIS100_CALC_ISDJCLIENT, "N").equals("Y")))
					{  // if Desjardins, add getGuarantorOtherLoans to the list of fields to check
						bHasChanged = (!(b1.getGuarantorOtherLoans().equalsIgnoreCase(b2.getGuarantorOtherLoans())) ||
								// --------------------- DJ, pvcs # 1683, 6-Jul-05 Catherine -- begin ---------------------
								b1.getInsuranceProportionsId() != b2.getInsuranceProportionsId() || 
								b1.getLifePercentCoverageReq() != b2.getLifePercentCoverageReq() ||
								b1.getDisabilityStatusId() != b2.getDisabilityStatusId() ||
								b1.getLifeStatusId() !=  b2.getLifeStatusId() ||
								b1.getSmokeStatusId() != b2.getSmokeStatusId()
						);
						// --------------------- DJ, pvcs # 1683, 6-Jul-05 Catherine -- end -----------------------

					}

					// --------------------- DJ, pvcs # 1201, Catherine -- end --------------------- 

					if (bHasChanged)
					{
						/*#DG600
              appendNote(NEW_LINE + "***  Borrower '" + b1.getBorrowerFirstName() +
                           " " + b1.getBorrowerLastName() + "' Changed  ***");*/
						nota = BXResources.newFormata(DUP_CHEK_BORROWER_CHANGED, 
								b1.getBorrowerFirstName(), name, 
								b1.getBorrowerLastName());
						dealNotesList.addElement(nota);    			

						compareBorrowers(b1, b2);
						/*#DG600
    					appendNote(NEW_LINE + "***  End Borrower '" + b1.getBorrowerFirstName() +
    							" " + b1.getBorrowerLastName() + "' Changed  ***");*/
						nota = BXResources.newFormata(DUP_CHEK_BORROWER_END, 
								b1.getBorrowerFirstName(), name, 
								b1.getBorrowerLastName());
						dealNotesList.addElement(nota);    			
					}

					//  We want to decrement the outer loop since the item was removed
					//  and the loop will increment.  The inner loop will begin again,
					//  so it doesn't matter what we do to it.
					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				//#DG600 appendNote(NEW_LINE + "***  Borrower  '" + name + "' Deleted  ***");
				nota = BXResources.newFormata(DUP_CHEK_BORROWER_DELETED, 
						b1.getBorrowerFirstName(), name, b1.getBorrowerLastName());
				dealNotesList.addElement(nota);    			
				totalDifferences++;
			}
		}

		//  All remaining items in list 2 are considered new
		for(Borrower b: l2) {
			//#DG600 name = b.getBorrowerFirstName() + " " + b.getBorrowerLastName();
			//#DG600 appendNote(NEW_LINE + "***  Borrower  '" + name + "' Added  ***");
			name = b.getBorrowerMiddleInitial();
			name = isEmpty(name)? "": name+' ';
			nota = BXResources.newFormata(DUP_CHEK_BORROWER_ADDED, 
					b.getBorrowerFirstName(), name, b.getBorrowerLastName());
			addFormtb(nota);    			
			//totalDifferences++;
		}
	}

	/**
	 * Performs the actual compare of DownPaymentSources, indicating whether they are
	 * new, deleted or changed.
	 *
	 * @param dpl1 The first <code>DownPaymentSource</code> list to use for comparison.
	 * @param dpl2 The second <code>DownPaymentSource</code> list to use for comparison.
	 *
	 */
	/*#DG600 rewritten
    protected void doDownPaymentSourceListCompare(List dp1, List dp2)
    {
        if (dp1 == null || dp2 == null)
           return;

        Collections.sort(dp1, comp);
        Collections.sort(dp2, comp);

        DownPaymentSource dps1, dps2;
        boolean found;
        String desc;

        for (int i=0; i < dp1.size(); i++)
        {
            dps1 = (DownPaymentSource)dp1.get(i);
            found = false;

            for (int j=0; j < dp2.size(); j++)
            {
                dps2 = (DownPaymentSource)dp2.get(j);

                if (dps1.getDownPaymentSourceTypeId() == dps2.getDownPaymentSourceTypeId())
                {
                    if (!isEqual(dps1.getDPSDescription(), dps2.getDPSDescription()) ||
                        dps1.getAmount() != dps2.getAmount())
                    {
                        desc  = BXResources.getPickListDescription("DownPaymentSourceType",  dps1.getDownPaymentSourceTypeId(), lang);
                        appendNote(NEW_LINE + "* "+ BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE",lang)+" '" + desc + "'" + BXResources.getDocPrepIngMsg("CHANGED",lang) +" *");
                        compareDownPaymentSources(dps1, dps2);
                    }

                    //  We want to decrement the outer loop since the item was removed
                    //  and the loop will increment.  The inner loop will begin again,
                    //  so it doesn't matter what we do to it.
                    dp1.remove(i--);
                    dp2.remove(j);

                    found = true;
                    break;
                }
            }

            //  If the item wasn't found in List 2, then it has been deleted
            if (!found)
            {
                appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE_DELETED",lang)+" *");
                addTo(dealNotesList, printDownPaymentSource(dps1));
                totalDifferences++;
            }
        }

        Iterator i = dp2.iterator();

        //  All remaining items in list 2 are considered new
        while (i.hasNext())
        {
            //appendNote(NEW_LINE + "*  Down Payment Source Added  *");
            appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE_ADDED",lang)+" *");
            addTo(dealNotesList, printDownPaymentSource((DownPaymentSource)i.next()));
            totalDifferences++;
        }
    } */

	protected void doDownPaymentSourceListCompare(List<DownPaymentSource> dpl1, List<DownPaymentSource> dpl2)
	{
		if (dpl1 == null || dpl2 == null)
			return;

		class Dpc implements Comparator<DownPaymentSource> {
			public int compare(DownPaymentSource dps1, DownPaymentSource dps2) {
				final int ival1 = dps1.getDownPaymentSourceTypeId();
				final int ival2 = dps2.getDownPaymentSourceTypeId();
				if (ival1 == ival2)
				{
					String val1 = dps1.getDPSDescription(); 
					String val2 = dps2.getDPSDescription(); 
					if(val1 == null) 
						val1 = lowAlfaVal;
					if(val2 == null) 
						val2 = lowAlfaVal;
					if (val1.equals(val2))
						return Double.compare(dps1.getAmount(), dps2.getAmount());
					else
						return val1.compareTo(val2);
				}
				return (ival1<ival2 ? -1 : (ival1==ival2 ? 0 : 1));
			}
		};     
		Dpc dpca = new Dpc();

		Collections.sort(dpl1, dpca);
		Collections.sort(dpl2, dpca);

		DownPaymentSource dps1, dps2;
		boolean found;
		Formattable nota;

		for (int i=0; i < dpl1.size(); i++)
		{
			dps1 = dpl1.get(i);
			found = false;

			for (int j=0; j < dpl2.size(); j++)
			{
				dps2 = dpl2.get(j);

				if (dps1.getDownPaymentSourceTypeId() == dps2.getDownPaymentSourceTypeId())
				{
					if (!isEqual(dps1.getDPSDescription(), dps2.getDPSDescription()) ||
							dps1.getAmount() != dps2.getAmount())
					{
						//desc  = BXResources.getPickListDescription("DownPaymentSourceType",  dps1.getDownPaymentSourceTypeId(), lang);
						//appendNote(NEW_LINE + "* "+ BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE",lang)+" '" + desc + "'" + BXResources.getDocPrepIngMsg("CHANGED",lang) +" *");
						nota = BXResources.newFormata(DUP_CHEK_DOWN_PAY_CHANGED, 
								BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_DOWN_PAY_SRC_TYPE, dps1.getDownPaymentSourceTypeId()));
						dealNotesList.addElement(nota);
						compareDownPaymentSources(dps1, dps2);
					}

					//  We want to decrement the outer loop since the item was removed
					//  and the loop will increment.  The inner loop will begin again,
					//  so it doesn't matter what we do to it.
					dpl1.remove(i--);
					dpl2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE_DELETED",lang)+" *");
				nota = BXResources.newFormata(DUP_CHEK_DOWN_PAY_DELETED);
				dealNotesList.addElement(nota);    			
				//#DG600 addTo(dealNotesList, printDownPaymentSource(dps1));
				dealNotesList.addAll(printDownPaymentSource(dps1, 1));
				totalDifferences++;
			}
		}

		//  All remaining items in list 2 are considered new
		for (DownPaymentSource dps: dpl2) {
			//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE_ADDED",lang)+" *");
			nota = BXResources.newFormata(DUP_CHEK_DOWN_PAY_ADDED);
			dealNotesList.addElement(nota);    			
			//addTo(dealNotesList, printDownPaymentSource((DownPaymentSource)i.next()));
			dealNotesList.addAll(printDownPaymentSource(dps, 2));
			totalDifferences++;
		}
	}    

	/**
	 * Performs the actual compare of EmploymentHistories, indicating whether they are
	 * new, deleted or changed.
	 *
	 * @param l1 The first <code>EmploymentHistory</code> list to use for comparison.
	 * @param l2 The second <code>EmploymentHistory</code> list to use for comparison.
	 *
	 */
	/*#DG600 rewritten
    protected void doEmploymentHistoryListCompare(List l1, List l2)
    {
        if (l1 == null || l2 == null)
           return;

        Comparator c = new CurrentEmploymentComparator();

        Collections.sort(l1, c);
        Collections.sort(l2, c);

        EmploymentHistory eh1, eh2;
        Contact c1 = null, c2 = null;
        boolean found;
        String desc;

        for (int i=0; i < l1.size(); i++)
        {
            eh1 = (EmploymentHistory)l1.get(i);
            found = false;

            for (int j=0; j < l2.size(); j++)
            {
                eh2 = (EmploymentHistory)l2.get(j);

                //  Let's compare to see if the 2 items are labeled as the current
                //  employer or have the same employer name.  Since the list is sorted
                //  by current employer, this method should be fine.
                if ((eh1.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT &&
                    eh2.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT) ||
                    isEqual(eh1.getEmployerName(), eh2.getEmployerName()))
                {
                    try
                    {
                        c1 = eh1.getContact();
                    }
                    catch (Exception e) { c1 = null;}

                    try
                    {
                        c2 = eh2.getContact();
                    }
                    catch (Exception e) { c2 = null;}

                    if (!isEqual(eh1.getEmployerName(), eh2.getEmployerName()) ||
                        eh1.getEmploymentHistoryStatusId() != eh2.getEmploymentHistoryStatusId() ||
                        eh1.getEmploymentHistoryTypeId() != eh2.getEmploymentHistoryTypeId() ||
                        eh1.getIndustrySectorId() != eh2.getIndustrySectorId() ||
                        eh1.getJobTitleId() != eh2.getJobTitleId() ||
                        !isEqual(eh1.getJobTitle(), eh2.getJobTitle()) ||
                        eh1.getMonthsOfService() != eh2.getMonthsOfService() ||
                        eh1.getOccupationId() != eh2.getOccupationId() ||
                        (c1 == null && c1 != c2) ||
                        (c1 != null &&
                        !isEqual(c1.getContactEmailAddress(), c2.getContactEmailAddress()) ||
                        !isEqual(c1.getContactFaxNumber(), c2.getContactFaxNumber()) ||
                        !isEqual(c1.getContactFirstName(), c2.getContactFirstName()) ||
                        !isEqual(c1.getContactJobTitle(), c2.getContactJobTitle()) ||
                        !isEqual(c1.getContactLastName(), c2.getContactLastName()) ||
                        !isEqual(c1.getContactMiddleInitial(), c2.getContactMiddleInitial()) ||
                        !isEqual(c1.getContactPhoneNumber(), c2.getContactPhoneNumber()) ||
                        !isEqual(c1.getContactPhoneNumberExtension(), c2.getContactPhoneNumberExtension())))
                    {
                        if (eh1.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT &&
                            eh2.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT)
                               desc = "(" + BXResources.getPickListDescription("EmploymentHistoryStatus",eh1.getEmploymentHistoryStatusId(),lang) + ")";

//                            desc = "(" + PicklistData.getDescription("EmploymentHistoryStatus",
//                               eh1.getEmploymentHistoryStatusId()) + ")";

                        else
                            desc = "'" + eh1.getEmployerName() + "'";

                        //appendNote(NEW_LINE + "*  Employment History " + desc + " Changed  *");
                        appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY",lang)  +  desc + BXResources.getDocPrepIngMsg("CHANGED",lang) + " *");
                        compareEmploymentHistories(eh1, eh2);
                    }

                    l1.remove(i--);
                    l2.remove(j);

                    found = true;
                    break;
                }
            }

            //  If the item wasn't found in List 2, then it has been deleted
            if (!found)
            {
                //appendNote(NEW_LINE + "*  EmploymentHistory Deleted  *");
                appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("EMPLOYMENTHISTORY_DELETED",lang) +  " *");
                addTo(dealNotesList, printEmploymentHistory(eh1));
                totalDifferences++;
            }
        }

        Iterator i = l2.iterator();

        //  All remaining items in list 2 are considered new
        while (i.hasNext())
        {
            //appendNote(NEW_LINE + "*  EmploymentHistory Added  *");
            appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY_ADDED",lang) + " *");
            addTo(dealNotesList, printEmploymentHistory((EmploymentHistory)i.next()));
            totalDifferences++;
        }
    }*/
	protected void doEmploymentHistoryListCompare(
			List<EmploymentHistory> l1, List<EmploymentHistory> l2)	//#DG600 
	{
		if (l1 == null || l2 == null)
			return;

		Comparator<EmploymentHistory> curec = new CurrentEmploymentComparator();

		Collections.sort(l1, curec);
		Collections.sort(l2, curec);

		EmploymentHistory eh1, eh2;
		Contact c1 = null, c2 = null;
		boolean found;
		Formattable nota;	//#DG600 

		for (int i=0; i < l1.size(); i++)
		{
			eh1 = (EmploymentHistory)l1.get(i);
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				eh2 = (EmploymentHistory)l2.get(j);

				//  Let's compare to see if the 2 items are labeled as the current
				//  employer or have the same employer name.  Since the list is sorted
				//  by current employer, this method should be fine.
				if ((eh1.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT &&
						eh2.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT) ||
						isEqual(eh1.getEmployerName(), eh2.getEmployerName()))
				{
					try
					{
						c1 = eh1.getContact();
					}
					catch (Exception e) { c1 = null;}

					try
					{
						c2 = eh2.getContact();
					}
					catch (Exception e) { c2 = null;}

					if (!isEqual(eh1.getEmployerName(), eh2.getEmployerName()) ||
							eh1.getEmploymentHistoryStatusId() != eh2.getEmploymentHistoryStatusId() ||
							eh1.getEmploymentHistoryTypeId() != eh2.getEmploymentHistoryTypeId() ||
							eh1.getIndustrySectorId() != eh2.getIndustrySectorId() ||
							eh1.getJobTitleId() != eh2.getJobTitleId() ||
							!isEqual(eh1.getJobTitle(), eh2.getJobTitle()) ||
							eh1.getMonthsOfService() != eh2.getMonthsOfService() ||
							eh1.getOccupationId() != eh2.getOccupationId() ||
							(c1 == null && c1 != c2) ||
							(c1 != null &&
									!isEqual(c1.getContactEmailAddress(), c2.getContactEmailAddress()) ||
									!isEqual(c1.getContactFaxNumber(), c2.getContactFaxNumber()) ||
									!isEqual(c1.getContactFirstName(), c2.getContactFirstName()) ||
									!isEqual(c1.getContactJobTitle(), c2.getContactJobTitle()) ||
									!isEqual(c1.getContactLastName(), c2.getContactLastName()) ||
									!isEqual(c1.getContactMiddleInitial(), c2.getContactMiddleInitial()) ||
									!isEqual(c1.getContactPhoneNumber(), c2.getContactPhoneNumber()) ||
									!isEqual(c1.getContactPhoneNumberExtension(), c2.getContactPhoneNumberExtension())))
					{
						Formattable notb;	//#DG600 
						int ival1 = eh1.getEmploymentHistoryStatusId();
						int ival2 = eh2.getEmploymentHistoryStatusId();
						if (ival1 == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT &&
								ival2 == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT)
							/*#DG600
    						desc = "(" + BXResources.getPickListDescription("EmploymentHistoryStatus",eh1.getEmploymentHistoryStatusId(),lang) + ")";*/
							notb = BXResources.newStrFormata("(", BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_EMPLOYM_HIST_STAT,ival1),")");
						else
							//#DG600 desc = "'" + eh1.getEmployerName() + "'";
							notb = BXResources.newStrFormata("'" + eh1.getEmployerName() + "'");

						//appendNote(NEW_LINE + "*  Employment History " + desc + " Changed  *");
						//#DG600 appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY",lang)  +  desc + BXResources.getDocPrepIngMsg("CHANGED",lang) + " *");
						nota = BXResources.newFormata(DUP_CHEK_EMPLOY_HIST_CHANGED, notb);
						dealNotesList.addElement(nota);    			    					
						compareEmploymentHistories(eh1, eh2);
					}

					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				//appendNote(NEW_LINE + "*  EmploymentHistory Deleted  *");
				//#DG600 appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("EMPLOYMENTHISTORY_DELETED",lang) +  " *");
				nota = BXResources.newFormata(DUP_CHEK_EMPLOY_HIST_DELETED);
				dealNotesList.addElement(nota);    			                
				dealNotesList.addAll(printEmploymentHistory(eh1, 1));
				totalDifferences++;
			}
		}

		//  All remaining items in list 2 are considered new
		for (EmploymentHistory eh : l2) {
			//appendNote(NEW_LINE + "*  EmploymentHistory Added  *");
			//#DG600 appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY_ADDED",lang) + " *");
			nota = BXResources.newFormata(DUP_CHEK_EMPLOY_HIST_ADDED);
			dealNotesList.addElement(nota);    			                
			//#DG600 addTo(dealNotesList, printEmploymentHistory((EmploymentHistory)i.next()));
			dealNotesList.addAll(printEmploymentHistory(eh, 2));
			totalDifferences++;
		}
	}

	/**
	 * Performs the actual compare of Incomes, indicating whether they are
	 * new, deleted or changed.  This method is more complicated than the other
	 * compare methods since we have to match up the income with its
	 * <code>EmploymentHistory</code> object, then do the comparison.
	 *
	 * @param l1 The first <code>Income</code> list to use for comparison.
	 * @param l2 The second <code>Income</code> list to use for comparison.
	 *
	 */
	//#DG600 
	protected void doIncomeListCompare(List<Income> l1, List<Income> l2)
	{
		if (l1 == null || l2 == null)
			return;

		//#DG600 
		class Incc implements Comparator<Income> {
			public int compare(Income inc1, Income inc2) {
				final int ival1 = inc1.getIncomeTypeId();			
				final int ival2 = inc2.getIncomeTypeId();
				if (ival1 == ival2)
				{
					final double dval1 = inc1.getIncomeAmount();
					final double dval2 = inc2.getIncomeAmount();
					if (dval1 == dval2)
					{
						String val1 = inc1.getIncomeDescription(); 
						String val2 = inc2.getIncomeDescription(); 
						if(val1 == null) 
							val1 = lowAlfaVal;
						if(val2 == null) 
							val2 = lowAlfaVal;
						return val1.compareTo(val2);
					}
					else
						return Double.compare(dval1, dval2);
				}
				return (ival1<ival2 ? -1 : (ival1==ival2 ? 0 : 1));
			}
		};     
		Incc incca = new Incc();        

		Collections.sort(l1, incca);
		Collections.sort(l2, incca);

		Income inc1, inc2;
		boolean found;
		EmploymentHistory eh1 = null, eh2 = null;
		boolean inc1HasEH = false, inc2HasEH = false;
		//#DG600 String prefix, suffix, val;
		Formattable nota, prefix, suffix, val;

		try
		{
			eh1 = new EmploymentHistory(srk);
			eh2 = new EmploymentHistory(srk);
		}
		catch (Exception e)
		{
			logger.error(e);
			return;
		}

		IncomeType lIncomeType;
		for (int i=0; i < l1.size(); i++)
		{
			inc1 = (Income)l1.get(i);
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				inc2 = (Income)l2.get(j);

				try
				{
					lIncomeType = new IncomeType( srk,inc1.getIncomeTypeId() );
					try
					{
						if( lIncomeType.getEmploymentRelated().equalsIgnoreCase("y") ){
							eh1.findByIncome((IncomePK)inc1.getPk());
							inc1HasEH = true;
						}else{
							inc1HasEH = false;
						}
					}
					catch (Exception e) {
						inc1HasEH = false;   // set it to false and continue
					}

					lIncomeType = new IncomeType( srk,inc2.getIncomeTypeId() );
					try
					{
						if( lIncomeType.getEmploymentRelated().equalsIgnoreCase("y") ){
							eh2.findByIncome((IncomePK)inc2.getPk());
							inc2HasEH = true;
						}else{
							inc2HasEH = false;
						}
					}
					catch (Exception e) {
						inc2HasEH = false;  // set it to false and continue
					}

					//  If both incomes don't have EH associated with them AND
					//  they are both the same income TYPE, then compare OTHERWISE
					//  both incomes require an associated EH AND they can either
					//  both be the current employment OR have the same employer name
					//  for further comparison.
					if ((!inc1HasEH && !inc2HasEH && inc1.getIncomeTypeId() == inc2.getIncomeTypeId()) ||
							(inc1HasEH && inc2HasEH &&
									((eh1.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT &&
											eh2.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT) ||
											(eh1.getEmploymentHistoryStatusId() == eh2.getEmploymentHistoryStatusId() &&
													isEqual(eh1.getEmployerName(), eh2.getEmployerName())))))
					{
						if (!isEqual(inc1, inc2))
						{
							//#DG600 val  = BXResources.getPickListDescription(INCOME_TYPE,  inc1.getIncomeTypeId(), lang);
							val  = BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INCOME_TYPE,inc1.getIncomeTypeId());

							//  Need an additional indicator showing whether the incomes are
							//  current, previous, or have changed between incomes.
							if (inc1HasEH && inc2HasEH)
							{
								int ival1 = eh1.getEmploymentHistoryStatusId();	
								int ival2 = eh2.getEmploymentHistoryStatusId();
								if (ival1 == ival2)
								{
									/*#DG600
                            prefix  = BXResources.getPickListDescription("IncomeType",  inc1.getIncomeTypeId(), lang);*/
									prefix  = val;
								}
								else
								{
									/*#DG600
                            prefix = BXResources.getPickListDescription("IncomeType",  inc1.getIncomeTypeId(), lang) + "->" +
                                       BXResources.getPickListDescription("IncomeType",  inc2.getIncomeTypeId(), lang);*/
									prefix = BXResources.newStrFormata(
											val, 
											"->",
											BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_INCOME_TYPE,  inc2.getIncomeTypeId())); 
								}
							}
							else
								//#DG600 prefix = "Other";
								prefix = BXResources.newFormata(DUP_CHEK_TYPE_OTHER);

							/*#DG600
                          suffix = (inc1.getIncomeTypeId() == Mc.INCOME_OTHER) ?
                                   "(" + inc1.getIncomeDescription() + ")" :
                                   "";*/
							suffix = (inc1.getIncomeTypeId() == Mc.INCOME_OTHER) ?
									BXResources.newStrFormata("(", inc1.getIncomeDescription(), ")") :
										BXResources.newStrFormata("");

									//appendNote(NEW_LINE + "* " + prefix + " Income '" + val + "' Changed " + suffix + " *");
									/*#DG600
                        appendNote(NEW_LINE + "* " + prefix + BXResources.getDocPrepIngMsg("INCOME",lang) + " '" 
                        		+ val + "'" + BXResources.getDocPrepIngMsg("CHANGED",lang)+  suffix + " *");*/
									nota = BXResources.newFormata(DUP_CHEK_INCOME_CHANGED, prefix, val, suffix);
									dealNotesList.addElement(nota);    			    					

									compareIncomes(inc1, inc2);
						}

						l1.remove(i--);
						l2.remove(j);

						found = true;
						break;
					}
				}
				catch (Exception e){}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			//  We only want to print the deleted incomes that aren't associated
			//  with an EmploymentHistory.  Deleted incomes that have an associated
			//  EmploymentHistory are already printed out in
			//  doEmploymentHistoryListCompare().
			if (!found)
			{
				try
				{
					eh1.findByIncome((IncomePK)inc1.getPk());
				}
				catch (Exception e)
				{
					//appendNote(NEW_LINE + "*  Income Deleted  *");
					//#DG600 
					/*appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("INCOME_DELETED",lang) + " *");
                    addTo(dealNotesList, printIncome(inc1));*/
					nota = BXResources.newFormata(DUP_CHEK_INCOME_DELETED);
					dealNotesList.addElement(nota);    			                
					dealNotesList.addAll(printIncome(inc1, 1));
					totalDifferences++;
				}
			}
		}

		//  All remaining items in list 2 are considered new
		//  We only want to print the new incomes that aren't associated
		//  with an EmploymentHistory.  New incomes that have an associated
		//  EmploymentHistory are already printed out in
		//  doEmploymentHistoryListCompare().
		for (Income inca: l2) {

			try
			{
				eh1.findByIncome((IncomePK)inca.getPk());
			}
			catch (Exception e)
			{
				//appendNote(NEW_LINE + "*  Income Added  *");
				/*#DG600 
                appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("INCOME_ADDED",lang)  + " *");
                addTo(dealNotesList, printIncome(inc1));*/
				nota = BXResources.newFormata(DUP_CHEK_INCOME_ADDED);
				dealNotesList.addElement(nota);    			                
				dealNotesList.addAll(printIncome(inca, 2));
				totalDifferences++;
			}
		}
	}

	/**
	 * Performs the actual compare of Liabilities, indicating whether they are
	 * new, deleted or changed.
	 *
	 * @param l1 The first <code>Liability</code> list to use for comparison.
	 * @param l2 The second <code>Liability</code> list to use for comparison.
	 *
	 */
	protected void doLiabilityListCompare(List<Liability> l1, List<Liability> l2)
	{
		if (l1 == null || l2 == null)
			return;

		//#DG600 
		class Liabc implements Comparator<Liability> {
			public int compare(Liability liab1, Liability liab2) {
				String val1 = liab1.getLiabilityDescription(); 
				String val2 = liab2.getLiabilityDescription(); 
				if(val1 == null) 
					val1 = lowAlfaVal;
				if(val2 == null) 
					val2 = lowAlfaVal;
				if (val1.equals(val2))
				{
					final double dval1 = liab1.getLiabilityAmount();
					final double dval2 = liab2.getLiabilityAmount();
					if (dval1 == dval2)
					{
						final int ival1 = liab1.getLiabilityTypeId();
						final int ival2 = liab2.getLiabilityTypeId();
						return (ival1<ival2 ? -1 : (ival1==ival2 ? 0 : 1));
					}
					else
					{
						return Double.compare(dval1, dval2);
					}
				}
				return val1.compareTo(val2);
			}
		};     
		Liabc liabca = new Liabc();                

		Collections.sort(l1, liabca);
		Collections.sort(l2, liabca);

		Liability liab1, liab2;
		boolean found;
		String desc1, desc2;

		Formattable nota;
		for (int i=0; i < l1.size(); i++)
		{
			liab1 = (Liability)l1.get(i);
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				liab2 = (Liability)l2.get(j);
				desc1 = trimDescription(liab1.getLiabilityDescription());
				desc2 = trimDescription(liab2.getLiabilityDescription());

				if (isEqual(desc1, desc2))
				{
					if (liab1.getIncludeInGDS() != liab2.getIncludeInGDS() ||
							liab1.getIncludeInTDS() != liab2.getIncludeInTDS() ||
							liab1.getLiabilityAmount() != liab2.getLiabilityAmount() ||
							liab1.getLiabilityMonthlyPayment() != liab2.getLiabilityMonthlyPayment() ||
							!isEqual(liab1.getLiabilityPaymentQualifier(), liab2.getLiabilityPaymentQualifier()) ||
							liab1.getLiabilityPayOffTypeId() != liab2.getLiabilityPayOffTypeId() ||
							liab1.getLiabilityTypeId() != liab2.getLiabilityTypeId() ||
							liab1.getPercentInGDS() != liab2.getPercentInGDS() ||
							liab1.getPercentInTDS() != liab2.getPercentInTDS() ||
							liab1.getPercentOutGDS() != liab2.getPercentOutGDS() ||
							liab1.getPercentOutTDS() != liab2.getPercentOutTDS())
					{
						//appendNote(NEW_LINE + "*  Liability '" + desc1 + "' Changed  *");
						/*#DG600
                        appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("LIABILITY",lang) + " '" + desc1 + "' " + BXResources.getDocPrepIngMsg("CHANGED",lang) + " *");*/
                        nota = BXResources.newFormata(DUP_CHEK_LIABILITY_CHANGED, desc1);
                        dealNotesList.addElement(nota);    			    					
                        compareLiabilities(liab1, liab2);
					}

					//  We want to decrement the outer loop since the item was removed
					//  and the loop will increment.  The inner loop will begin again,
					//  so it doesn't matter what we do to it.
					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				//appendNote(NEW_LINE + "*  Liability Deleted  *");
				/*#DG600
                appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("LIABILITY_DELETED",lang) + "  *");
                addTo(dealNotesList, printLiability(liab1));*/                
				nota = BXResources.newFormata(DUP_CHEK_LIABILITY_DELETED);
				dealNotesList.addElement(nota);    			                
				dealNotesList.addAll(printLiability(liab1, 1));              	
				totalDifferences++;
			}
		}

		//  All remaining items in list 2 are considered new
		for (Liability liab : l2) {
			//appendNote(NEW_LINE + "*  Liability Added  *");
			/*#DG600
			 * appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("LIABILITY_ADDED",lang)+ " *");
	            addTo(dealNotesList, printLiability((Liability)i.next()));*/            
			nota = BXResources.newFormata(DUP_CHEK_LIABILITY_ADDED);
			dealNotesList.addElement(nota);    			                
			dealNotesList.addAll(printLiability(liab, 2));            
			totalDifferences++;
		}
	}

	/**
	 * Performs the actual compare of PropertyExpenses, indicating whether they are
	 * new, deleted or changed.
	 *
	 * @param l1 The first <code>PropertyExpense</code> list to use for comparison.
	 * @param l2 The second <code>PropertyExpense</code> list to use for comparison.
	 *
	 */
	protected void doPropertyExpenseListCompare(List<PropertyExpense> l1, List<PropertyExpense> l2)
	{
		if (l1 == null || l2 == null)
			return;

		//#DG600 
		class Proexpc implements Comparator<PropertyExpense> {
			public int compare(PropertyExpense pe1, PropertyExpense pe2) {

				final int ival1 = pe1.getPropertyExpenseTypeId();
				final int ival2 = pe2.getPropertyExpenseTypeId();
				if (ival1 == ival2)
				{
					final double dval1 = pe1.getPropertyExpenseAmount();
					final double dval2 = pe2.getPropertyExpenseAmount();
					if (dval1 == dval2)
					{
						String val1 = pe1.getPropertyExpenseDescription(); 
						String val2 = pe2.getPropertyExpenseDescription(); 
						if(val1 == null) 
							val1 = lowAlfaVal;
						if(val2 == null) 
							val2 = lowAlfaVal;
						return val1.compareTo(val2);
					}
					else
						return Double.compare(dval1, dval2);
				}
				return (ival1<ival2 ? -1 : (ival1==ival2 ? 0 : 1));
			}
		};     
		Proexpc proexpca = new Proexpc();        
		/*
        Collections.sort(l1, comp);
        Collections.sort(l2, comp);*/
		Collections.sort(l1, proexpca);
		Collections.sort(l2, proexpca);

		PropertyExpense pe1, pe2;
		boolean found;
		Formattable desc1;	//#DG600 
		//String desc1;

		Formattable nota;
		for (int i=0; i < l1.size(); i++)
		{
			pe1 = (PropertyExpense)l1.get(i);
			found = false;

			for (int j=0; j < l2.size(); j++)
			{
				pe2 = (PropertyExpense)l2.get(j);

				if (pe1.getPropertyExpenseTypeId() == pe2.getPropertyExpenseTypeId())
				{
					if (pe1.getPropertyExpensePeriodId() != pe2.getPropertyExpensePeriodId() ||
							pe1.getMonthlyExpenseAmount() != pe2.getMonthlyExpenseAmount() ||
							!isEqual(pe1.getPropertyExpenseDescription(), pe2.getPropertyExpenseDescription()) ||
							pe1.getPeIncludeInGDS() != pe2.getPeIncludeInGDS() ||
							pe1.getPeIncludeInTDS() != pe2.getPeIncludeInTDS() ||
							pe1.getPePercentInGDS() != pe2.getPePercentInGDS() ||
							pe1.getPePercentInTDS() != pe2.getPePercentInTDS() ||
							pe1.getPePercentOutGDS() != pe2.getPePercentOutGDS() ||
							pe1.getPePercentOutTDS() != pe2.getPePercentOutTDS())
					{
						//desc1 = PicklistData.getDescription("PropertyExpenseType", pe1.getPropertyExpenseTypeId());
						//ALERT:BX:ZR Release2.1.docprep
						//#DG600 desc1  = BXResources.getPickListDescription(PROPERTY_EXPENSE_TYPE,  pe1.getPropertyExpenseTypeId(), lang);
						desc1  = BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PROPERTY_EXPENSE_TYPE,  pe1.getPropertyExpenseTypeId()); 

						/*#DG600
                        appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("PROPERTY_EXPENSE",lang) + " '"  + "'" + BXResources.getDocPrepIngMsg("CHANGED",lang) + " *");*/
						nota = BXResources.newFormata(DUP_CHEK_PROP_EXP_CHANGED, desc1);
						dealNotesList.addElement(nota);    			    					

						comparePropertyExpenses(pe1, pe2);
					}

					//  We want to decrement the outer loop since the item was removed
					//  and the loop will increment.  The inner loop will begin again,
					//  so it doesn't matter what we do to it.
					l1.remove(i--);
					l2.remove(j);

					found = true;
					break;
				}
			}

			//  If the item wasn't found in List 2, then it has been deleted
			if (!found)
			{
				/*#DG600
                appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("PROPERTY_EXPENSE_DELETED",lang) + "  *");
                addTo(dealNotesList, printPropertyExpense(pe1));
      					appendNote(nota);*/    			    					
				nota = BXResources.newFormata(DUP_CHEK_PROP_EXP_DELETED);
				dealNotesList.addElement(nota); //FXP23275, MCM Nov 1, 2008. 
				dealNotesList.addAll(printPropertyExpense(pe1, 1));              	
				totalDifferences++;
			}
		}

		//  All remaining items in list 2 are considered new
		for (PropertyExpense pe : l2) {
			/*#DG600
            appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("PROPERTY_EXPENSE_ADDED",lang) + " *");
            addTo(dealNotesList, printPropertyExpense((PropertyExpense)i.next()));*/    			    					
			nota = BXResources.newFormata(DUP_CHEK_PROP_EXP_ADDED);
			dealNotesList.addElement(nota); //FXP23275, MCM Nov 1, 2008.
			dealNotesList.addAll(printPropertyExpense(pe, 2));
			totalDifferences++;
		}
	}

	/**
	 * Compares <code>str1</code> to <code>str2</code>, taking into account
	 * possible <code>null</code> values being passed in.
	 *
	 * @param str1 The first <code>String</code> to compare
	 * @param str2 The second <code>String</code> to compare
	 *
	 * @return whether the two strings were equal
	 */
	public static boolean isEqual(String str1, String str2)
	{
		return (str1 == null && str1 == str2) ||
		(str1 != null && str1.equals(str2));
	}

	/**
	 * Compares <code>date1</code> to <code>date2</code>, taking into account
	 * possible <code>null</code> values being passed in.  This method only compares
	 * the day, month and year, not any of the time values.
	 *
	 * @param date1 The first <code>Date</code> to compare
	 * @param date2 The second <code>Date</code> to compare
	 *
	 * @return whether the two dates were equal
	 */
	protected static boolean isEqual(Date date1, Date date2)
	{
		if (date1 == null)
			return (date1 == date2);
		else if (date2 == null)
			return false;

		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();

		cal1.setTime(date1);
		cal2.setTime(date2);

		return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
				cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
				cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH));
	}

	/**
	 * Compares <code>inc1</code> to <code>inc2</code>, using important fields.
	 *
	 * @param inc1 The first <code>Income</code> to compare
	 * @param inc2 The second <code>Income</code> to compare
	 *
	 * @return whether the two incomes were equal
	 */
	public static boolean isEqual(Income inc1, Income inc2)
	{
		return (inc1.getIncomeTypeId() == inc2.getIncomeTypeId() &&
				inc1.getAnnualIncomeAmount() == inc2.getAnnualIncomeAmount() &&
				inc1.getIncomeAmount() == inc2.getIncomeAmount() &&
				inc1.getIncomePeriodId() == inc2.getIncomePeriodId() &&
				isEqual(inc1.getIncomeDescription(), inc2.getIncomeDescription()) &&
				inc1.getIncPercentInGDS() == inc2.getIncPercentInGDS() &&
				inc1.getIncPercentInTDS() == inc2.getIncPercentInTDS() &&
				inc1.getIncPercentOutGDS() == inc2.getIncPercentOutGDS() &&
				inc1.getIncPercentOutTDS() == inc2.getIncPercentOutTDS() &&
				inc1.getMonthlyIncomeAmount() == inc2.getMonthlyIncomeAmount());
	}

	/**
	 * Compares 2 income lists to see if they are the same.
	 */
	public static boolean isEqual(List incList1, List incList2)
	{
		if (incList1 == null)
			return (incList2 == null);
		else if (incList2 == null)
			return (incList1 == null);

		Income inc1, inc2;

		for (int i=0; i < incList1.size(); i++)
		{
			inc1 = (Income)incList1.get(i);

			for (int j=0; j < incList2.size(); j++)
			{
				inc2 = (Income)incList2.get(j);

				if (isEqual(inc1, inc2))
				{
					incList1.remove(i--);
					incList2.remove(j);
				}
			}
		}

		return (incList1.size() == 0 && incList2.size() == 0);
	}

	protected static boolean isZeroes(String str)
	{
		try
		{
			return (Integer.parseInt(str) == 0);
		}
		catch (Exception e) {}

		return false;
	}

	protected static String trimDescription(String desc)
	{
		if (desc == null)
			return null;

		String newDesc = desc;

		int index = newDesc.indexOf("     ");

		if (index != -1)
			newDesc = newDesc.substring(0, index);

		return newDesc;
	}

	public static boolean isEmpty(String str)
	{
		return (str == null || str.trim().length() == 0);
	}

	public int getTotalDifferences()
	{
		return totalDifferences;
	}

	/*#DG600 not used 
    public static String convertCR(String pStr)
    {
        if (pStr == null)
           return pStr;

        int ind;
        StringBuffer retBuf = new StringBuffer("");

        while( (ind = pStr.indexOf("�")) != -1 )
        {
            retBuf.append(pStr.substring(0,ind));
            retBuf.append( "\n" );
            pStr = pStr.substring(ind + 1);
        }

        retBuf.append(pStr);
        return retBuf.toString();
    }*/

	/**
	 * Determines whether 2 deals are significantly different from each other.
	 * This is determined by comparing the following fields on each deal:
	 * <p><code><blockquote><pre>
	 *    Total loan amount
	 *    Total purchase price
	 *    # of borrowers
	 *    # of properties
	 *    Combined total income
	 *    Combined total liabilities
	 *    Credit bureau attachments
	 *    Estimated closing date
	 * </pre></blockquote></code>
	 *
	 * @param d1 the first deal to compare
	 * @param d2 the second deal to compare
	 *
	 * @return whether the 2 deals are significantly different from each other.
	 */
	public static boolean hasSignificantChanges(Deal d1, Deal d2)
	{
		return d1.getTotalLoanAmount() != d2.getTotalLoanAmount() ||
		d1.getCombinedTotalAssets() != d2.getCombinedTotalAssets() ||
		d1.getTotalPurchasePrice() != d2.getTotalPurchasePrice() ||
		d1.getNumberOfBorrowers() !=  d2.getNumberOfBorrowers() ||
		d1.getNumberOfProperties() != d2.getNumberOfProperties() ||
		d1.getCombinedTotalIncome() != d2.getCombinedTotalIncome() ||
		d1.getCombinedTotalLiabilities() != d2.getCombinedTotalLiabilities() ||
		!isCBAttachmentEqual( d1, d2  ) ||
		(d1.getEstimatedClosingDate() == null && d2.getEstimatedClosingDate() != null) ||
		(d1.getEstimatedClosingDate() != null && !d1.getEstimatedClosingDate().equals(d2.getEstimatedClosingDate())) ||
		// Rel 3.1 Sasha
		d1.getProductTypeId() != d2.getProductTypeId() ||
		d1.getLocRepaymentTypeId() != d2.getLocRepaymentTypeId();
		// End of Rel 3.1 Sasha 
	}

	/**
	 * Compares rates that ingestion puts in the deal's notes.
	 * The first instance of a string indicating a net rate is searched for
	 * in the deal note.  Upon finding it, the rate is extracted and compared to
	 * the other deal's notes.  They are compared and difference text is added
	 * to the main note, if applicable.
	 */
	/*#DG600 (also, current implementation is too expensive: rewritten using clob)
    protected boolean compareRates()
    {
        String note1 = null, note2 = null;
        String prefix = "Net rate from ingestion:";
        try        {
            List list = (List)deal1.getDealNotes();
            DealNotesComparator dnc = new DealNotesComparator();
            DealNotes dn;
            Iterator i;
            String tempNote;
            //  ENSURES that the notes are sorted by date.
            Collections.sort(list, dnc);
            i = list.iterator();
            //  Look for string match in first deal notes list
            while (i.hasNext())            {
                dn = (DealNotes)i.next();
                tempNote = dn.getDealNotesText();
                if (tempNote != null && tempNote.startsWith(prefix) &&
                    dn.getDealNotesUserId() == 0)                {
                    note1 = tempNote;
                    break;
                }
            }
            list = (List)deal2.getDealNotes();
            Collections.sort(list, dnc);
            i = list.iterator();
            //  Look for string match in second deal notes list
            while (i.hasNext())            {
                dn = (DealNotes)i.next();
                tempNote = dn.getDealNotesText();
                if (tempNote != null && tempNote.startsWith(prefix) &&
                    dn.getDealNotesUserId() == 0)                {
                    note2 = tempNote;
                    break;
                }
            }
        }
        catch (Exception e){}
        //  Both of the prefixes have been found, so now we have to extract
        //  the actual rate and see if it's changed.  If it has, then
        //  we add it to the comparison deal note.
        if (note1 != null && note2 != null)        {
            int index = note1.indexOf(":");
            //  Trim off everything up, and including, the ':'
            //  We can use the same index in note2 as note1 since
            //  the prefix length is the same for both.
            note1 = note1.substring(index+1);
            note2 = note2.substring(index+1);
            //  Here, we need to do 2 separate searches, since the space
            //  after the rate is not guaranteed to be in the same place
            index = note1.indexOf(" ");
            if (index != -1)
               note1 = note1.substring(0, index);
            index = note2.indexOf(" ");
            if (index != -1)
               note2 = note2.substring(0, index);
            //  Once we get to here, we should only have the rate left,
            //  so all that's left to do is convert it to a double, compare it,
            //  and add the comparison text to the note.
            double rate1 = -1;
            double rate2 = -1;
            double diff;
            try            {
                rate1 = Double.parseDouble(note1);
                rate2 = Double.parseDouble(note2);
            }
            catch (NumberFormatException nfe)            {
                //#DG600 System.out.println("*** ERROR->DealCompare.compareRates(): " +nfe.getMessage());
                logger.error(this, nfe);
            }
            if (rate1 != rate2)            {
                diff = rate2 - rate1;
                appendNote(BXResources.getDocPrepIngMsg("REQUESTED_RATE",lang) + DELIMITER +
                   formatInterestRate(rate1) + DELIMITER +
                   formatInterestRate(rate2) + DELIMITER +
                   formatInterestRate(diff));
                totalDifferences++;
            }
        }*/
	protected boolean compareRates()
	{
		try {
			DealNotes dealNota = new DealNotes(srk);

			String[] tmpRts;					
			String prefix;
			//try all languages
	 		found: {
				for (Entry<Integer, Locale> intLoc: BXResources.getLocales()) {
					Locale locala = intLoc.getValue();
					prefix = BXResources.getDocPrepIngMsg(DUP_CHEK_NET_RATE_FROM_ING,locala);

					tmpRts = dealNota.findRateNotes(deal1, deal2, prefix);					
					if (tmpRts[0] != null || tmpRts[1] != null)
						break found;
				}
				return false;
			}
			
			double dval1 = Double.MAX_VALUE;
			double dval2 = Double.MAX_VALUE;
			Formattable nota, frmtb1=null, frmtb2=null, frmtbDiff;

			String str = tmpRts[0];
			if (str != null) {
				try            {
					dval1 = getDbl(prefix, str);
					frmtb1 = BXResources.newFormata(RATIO_FORMAT, dval1);
				}
				catch (Exception nfe)            {
					logger.warning("@compareRates-1:"+nfe);
				}
			}
			str = tmpRts[1];
			if (str != null) {
				try            {
					dval2 = getDbl(prefix, str);
					frmtb2 = BXResources.newFormata(RATIO_FORMAT, dval2);
				}
				catch (Exception nfe)            {
					logger.warning("@compareRates-1:"+nfe);
				}
			}
			if (dval1 == dval2)
				return false;

			frmtbDiff = frmtb1 == null || frmtb2 == null? 	
					null:
					BXResources.newFormata(RATIO_FORMAT, dval2 - dval1);
			nota = newTabedLine(DUP_CHEK_REQUESTED_RATE, frmtb1, frmtb2, frmtbDiff);
			addFormtb(nota);
			return maximumReached();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return false;
	}

	private double getDbl(String prefix, String str) {
		int ind, fin;
		// find digit
		for (ind = prefix.length(); ind < str.length(); ind++) 
			if (Character.isDigit(str.charAt(ind)))
				break;
		// find space
		for (fin = ind; fin < str.length(); fin++) 
			if (Character.isWhitespace(str.charAt(fin)))
				break;
		return Double.parseDouble(str.substring(ind, fin));
	}

	/**
	 * Determines whether the credit bureau attachments for the
	 * primary borrowers on each of the given deals are equal.
	 *
	 * @param d1 the first deal to compare
	 * @param d2 the second deal to compare
	 *
	 * @return whether the primary borrower from each deal's credit bureau
	 * attachments are equal.
	 */
	public static boolean isCBAttachmentEqual(Deal d1, Deal d2)
	{
		Collection d1BorColl, d2BorColl;

		try
		{
			d1BorColl = d1.getBorrowers();
			d2BorColl = d2.getBorrowers();
		}
		catch(Exception exc)
		{
			return false;
			// ALERT: handle this exception
		}

		Borrower b;

		boolean d1HasAttachment = false;
		boolean d2HasAttachment = false;

		boolean d1PrimBorFound = false;
		boolean d2PrimBorFound = false;

		// find the primary borrower for existing deal;
		Iterator it = d1BorColl.iterator();

		while (it.hasNext())
		{
			b = (Borrower)it.next();

			if (b.isPrimaryBorrower())
			{
				d1PrimBorFound = true;

				if (b.getBureauAttachment() != null &&
						b.getBureauAttachment().equalsIgnoreCase("y") )
				{
					d1HasAttachment = true;
				}
				break;
			}
		}

		// find the primary borrower for new deal
		it = d2BorColl.iterator();

		while( it.hasNext() )
		{
			b = (Borrower)it.next();

			if (b.isPrimaryBorrower())
			{
				d2PrimBorFound = true;

				if (b.getBureauAttachment() != null &&
						b.getBureauAttachment().equalsIgnoreCase("y") )
				{
					d2HasAttachment = true;
				}
				break;
			}
		}

		if (d1PrimBorFound && d2PrimBorFound)
		{
			// now decide about the match
			return (d1HasAttachment) ? true : !d2HasAttachment;
		}

		return false;
	}

	public void compareParties()
	{
		if (maximumReached()) 
			return; 

		logger.trace("DealCompare@compareParties() comparing party profiles for deals:  " + deal1.getDealId() + ", " + deal2.getDealId());

		PartyProfile curPp, newPp;

		// 1) get all parties for both deals
		try {
			curPp = new PartyProfile(srk);

			// 1) get all parties for both deals
			Map curPartiesMap = curPp.findByDealId(deal1.getDealId());            // existing deal
			Map newPartiesMap = curPp.findByDealId(deal2.getDealId());            // new deal

			if ((curPartiesMap.isEmpty() || curPartiesMap == null) && (newPartiesMap.isEmpty() || newPartiesMap == null)) {
				logger.trace("DealCompare@compareParties(): No PARTYDEALASSOC records on both deals.");
				curPp = null;
				return; 
			}

			newPp = new PartyProfile(srk);
			Object removed;

			Set st = curPartiesMap.keySet();
			Set processed = null;

			Iterator it = st.iterator();
			int curPartyProfileId, newPartyProfileId; 
			Integer key;

			// 2) find and compare changed parties (parties of the same type) 
			while (it.hasNext()){
				key = (Integer)it.next(); 
				curPp = (PartyProfile)curPartiesMap.get(key);

				if ( !newPartiesMap.isEmpty() ) {
					if (newPartiesMap.containsKey(key)) {
						logger.trace("DealCompare@compareParties(): Incoming deal has PARTYDEALASSOC record of type " + key);

						// compare partyProfileIds 
						curPartyProfileId = curPp.getPartyProfileId();

						newPp = (PartyProfile)newPartiesMap.get(key);
						if (newPp != null){
							newPartyProfileId = newPp.getPartyProfileId();
							if (newPartyProfileId == curPartyProfileId) {
								logger.trace("DealCompare@compareParties(): ProfileId(s) for parties type " + key + " are the same");
							} else {
								comparePartyProfiles(curPp, newPp);
							}

							// save processed keys to delete them later
							if (processed == null) {
								processed = new HashSet();
							}
							processed.add(key);
						}
					}
				}
			}
			// delete from maps and set
			if (processed != null) {                                  // Catherine for DJ #957 bug fix, 5-Aug-05
				it = null;
				it = processed.iterator();
				while (it.hasNext()) {
					key = (Integer)it.next(); 

					removed = curPartiesMap.remove((Integer)key);
					if (removed == null) {logger.trace("DealCompare@compareParties():cannot remove from the map, key = " + key);}

					removed = newPartiesMap.remove((Integer)key);
					if (removed == null) {logger.trace("DealCompare@compareParties():cannot remove from the map, key = " + key);}

				}
			}
			st = null;

			// 3) process the rest of the parties

			// removed parties
			logger.trace("DealCompare@compareParties(): adding deal notes for removed parties" );

			Collection vals = curPartiesMap.values(); 
			it = vals.iterator();
			while (it.hasNext()) {
				curPp = (PartyProfile)(it.next());

				comparePartyProfiles(curPp, null);
			}
			vals = null;

			// new parties
			logger.trace("DealCompare@compareParties(): adding deal notes for new parties" );
			vals = newPartiesMap.values();
			it = vals.iterator();
			while (it.hasNext()) {
				newPp = (PartyProfile)it.next();

				comparePartyProfiles(null, newPp);
			}

		} catch (Exception e) {
			logger.trace("Failed to get parties in compareParties() for deals = " + deal1.getDealId() + ",  " + deal2.getDealId());
			logger.trace(StringUtil.stack2string(e));
		}

		logger.trace( "@compareParties() end");
	}





	/**
	 * Prepares note after party profile comparison
	 *
	 * @param pp1 Current PartyProfile <br>
	 * @param pp2 New PartyProfile<br>
	 *
	 * @version 1.1  <br>
	 * Date: 06/30/2006 <br>
	 * Author: NBC/PP Implementation Team <br>
	 * Change:  <br>
	 *	- Add a new switch case to handle service branches.<br>
	 */

	public void comparePartyProfiles(PartyProfile pp1, PartyProfile pp2)
	{
		if (maximumReached()) {
			logger.trace("DealCompare@compareParties(): maximum reached, return");
			return;
		}   

		int partyTypeId;

		if (pp1 != null) {
			partyTypeId = pp1.getPartyTypeId();
		} else {                                                       // if one of partyprofiles == null
			if (pp2 != null) {
				partyTypeId = pp2.getPartyTypeId();
			} else {                             // if (pp2 == null)
				logger.trace("DealCompare@comparePartyProfiles(): Error! Both party profiles == null");
				return;
			}
		}
		logger.trace( ":comparePartyProfiles() : comparing for partyTypeId = " + partyTypeId);

		Formattable stLabel;			//#DG600 
		String val1 = "", val2 = "", tmp = "";//, stLabel = "";

		switch (partyTypeId) {

		case Mc.PARTY_TYPE_APPRAISER:{                              // 51

			if (pp1 != null) {
				val1 = pp1.getPartyName().trim();
				val1 = (val1 == null) ? "" : val1;
			}
			if (pp2 != null) {
				val2 = pp2.getPartyName().trim();
				val2 = (val2 == null) ? "" : val2;
			}

			//#DG600 stLabel = BXResources.getDocPrepIngMsg("APPRAISER", lang);
			stLabel = BXResources.newFormata(PKL_APPRAISER);
			break;
		}

		case Mc.PARTY_TYPE_ORIGINATION_BRANCH:{                     // 60
			if (pp1 != null) {
				val1 = pp1.getPtPBusinessId().toString();
				val1 = (val1 == null) ? "" : val1;
			}
			if (pp2 != null) {
				val2 = pp2.getPtPBusinessId().toString();
				val2 = (val2 == null) ? "" : val2;
			}

			//#DG600 stLabel = BXResources.getDocPrepIngMsg("TRANSIT_NUMBER", lang);
			stLabel = BXResources.newFormata(TRANSIT_NUMBER);
			break;
		}
		//        ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		case Mc.PARTY_TYPE_SERVICE_BRANCH : {    // 62
			if ( pp1 != null ) {
				val1 = pp1.getPtPBusinessId( ).toString( );
				val1 = ( val1 == null ) ? "" : val1;
			}
			if ( pp2 != null ) {
				val2 = pp2.getPtPBusinessId( ).toString( );
				val2 = ( val2 == null ) ? "" : val2;
			}

			//#DG600 stLabel = BXResources.getDocPrepIngMsg("TRANSIT_NUMBER", lang);
			stLabel = BXResources.newFormata(TRANSIT_NUMBER);
			break;
		}
		//        ***** Change by NBC Impl. Team - Version 1.1 - End*****//

		default:{                                                   // others

			if (pp1 != null) {
				val1 = pp1.getPartyName().trim();
				val1 = (val1 == null) ? "" : val1;

				tmp = pp1.getPartyCompanyName();
				tmp = (tmp == null) ? "" : tmp;

				val1 = val1 + ", " + tmp;
			}

			if (pp2 != null) {
				val2 = pp2.getPartyName().trim();                      // partyName is CHAR(150), so we have to trim it
				val2 = (val2 == null) ? "" : val2;

				tmp = pp2.getPartyCompanyName();
				tmp = (tmp == null) ? "" : tmp;

				val2 = val2 + ", " + tmp;
			}

			//#DG600 stLabel = BXResources.getPickListDescription("PARTYTYPE", partyTypeId, lang);
			stLabel = BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_PARTYTYPE, partyTypeId);
			break;
		}
		}

		/*#DG600 
      	appendNote(stLabel + DELIMITER +  val1 + DELIMITER + val2);
        totalDifferences++;*/
		Formattable nota = newTabedLine(stLabel, val1, val2, null);
		addFormtb(nota);        
	}
	// --------------------- DJ, pvcs # 957, 6-Jul-05 Catherine -- end ---------------------

	//#DG600 phased out
	/*protected class EntityComparator implements Comparator
    {
	 *//**
	 * Used to sort Assets, Liabilities, Incomes, DownPaymentSources,
	 * and PropertyExpenses
	 *//*
        public int compare(Object o1, Object o2)
        {
            String val1 = "", val2 = "";

            if (o1 instanceof Liability && o2 instanceof Liability)
            {
                Liability l1 = (Liability) o1;
                Liability l2 = (Liability) o2;

                val1 = (l1.getLiabilityDescription() == null) ? "zzz" : l1.getLiabilityDescription();
                val2 = (l2.getLiabilityDescription() == null) ? "zzz" : l2.getLiabilityDescription();

                if (val1.equals(val2))
                {
                    if (l1.getLiabilityAmount() == l2.getLiabilityAmount())
                    {
                        val1 = Integer.toString(l1.getLiabilityTypeId());
                        val2 = Integer.toString(l2.getLiabilityTypeId());
                    }
                    else
                    {
                        val1 = Double.toString(l1.getLiabilityAmount());
                        val2 = Double.toString(l2.getLiabilityAmount());
                    }
                }
            }
            else if (o1 instanceof Asset && o2 instanceof Asset)
            {
                Asset a1 = (Asset) o1;
                Asset a2 = (Asset) o2;

                val1 = (a1.getAssetDescription() == null) ? "zzz" : a1.getAssetDescription();
                val2 = (a2.getAssetDescription() == null) ? "zzz" : a2.getAssetDescription();

                if (val1.equals(val2))
                {
                    if (a1.getAssetTypeId() == a2.getAssetTypeId())
                    {
                        val1 = Double.toString(a1.getAssetValue());
                        val2 = Double.toString(a2.getAssetValue());
                    }
                    else
                    {
                        val1 = Integer.toString(a1.getAssetTypeId());
                        val2 = Integer.toString(a2.getAssetTypeId());
                    }
                }
            }
            else if (o1 instanceof Income && o2 instanceof Income)
            {
                Income inc1 = (Income) o1;
                Income inc2 = (Income) o2;

                if (inc1.getIncomeTypeId() == inc2.getIncomeTypeId())
                {
                    if (inc1.getIncomeAmount() == inc2.getIncomeAmount())
                    {
                        val1 = (inc1.getIncomeDescription() == null) ? "zzz" : inc1.getIncomeDescription();
                        val2 = (inc2.getIncomeDescription() == null) ? "zzz" : inc2.getIncomeDescription();
                    }
                    else
                    {
                        val1 = Double.toString(inc1.getIncomeAmount());
                        val2 = Double.toString(inc2.getIncomeAmount());
                    }
                }
                else
                {
                    val1 = Integer.toString(inc1.getIncomeTypeId());
                    val2 = Integer.toString(inc2.getIncomeTypeId());
                }
            }
            else if (o1 instanceof DownPaymentSource && o2 instanceof DownPaymentSource)
            {
                DownPaymentSource dps1 = (DownPaymentSource) o1;
                DownPaymentSource dps2 = (DownPaymentSource) o2;

                if (dps1.getDownPaymentSourceTypeId() == dps2.getDownPaymentSourceTypeId())
                {
                    val1 = (dps1.getDPSDescription() == null) ? "zzz" : dps1.getDPSDescription();
                    val2 = (dps2.getDPSDescription() == null) ? "zzz" : dps2.getDPSDescription();

                    if (val1.equals(val2))
                    {
                        val1 = Double.toString(dps1.getAmount());
                        val2 = Double.toString(dps2.getAmount());
                    }
                }
                else
                {
                    val1 = Integer.toString(dps1.getDownPaymentSourceTypeId());
                    val2 = Integer.toString(dps2.getDownPaymentSourceTypeId());
                }
            }
            else if (o1 instanceof PropertyExpense && o2 instanceof PropertyExpense)
            {
                PropertyExpense pe1 = (PropertyExpense) o1;
                PropertyExpense pe2 = (PropertyExpense) o2;

                if (pe1.getPropertyExpenseTypeId() == pe2.getPropertyExpenseTypeId())
                {
                    if (pe1.getPropertyExpenseAmount() == pe1.getPropertyExpenseAmount())
                    {
                        val1 = (pe1.getPropertyExpenseDescription() == null) ?
                             "zzz" : pe1.getPropertyExpenseDescription();
                        val2 = (pe1.getPropertyExpenseDescription() == null) ?
                             "zzz" : pe1.getPropertyExpenseDescription();
                    }
                    else
                    {
                        val1 = Double.toString(pe1.getPropertyExpenseAmount());
                        val2 = Double.toString(pe2.getPropertyExpenseAmount());
                    }
                }
                else
                {
                    val1 = Integer.toString(pe1.getPropertyExpenseTypeId());
                    val2 = Integer.toString(pe1.getPropertyExpenseTypeId());
                }
            }
            else if (o1 instanceof Borrower && o2 instanceof Borrower)
            {
                Borrower b1 = (Borrower)o1;
                Borrower b2 = (Borrower)o2;

                if (isEqual(b1.getBorrowerLastName(), b2.getBorrowerLastName()))
                {
                    if (isEqual(b1.getBorrowerFirstName(), b2.getBorrowerFirstName()))
                    {
                        val1 = (b1.getBorrowerMiddleInitial() == null) ?
                             "zzz" : b1.getBorrowerMiddleInitial();
                        val2 = (b2.getBorrowerMiddleInitial() == null) ?
                             "zzz" : b2.getBorrowerMiddleInitial();
                    }
                    else
                    {
                        val1 = (b1.getBorrowerFirstName() == null) ?
                             "zzz" : b1.getBorrowerFirstName();
                        val2 = (b2.getBorrowerFirstName() == null) ?
                             "zzz" : b2.getBorrowerFirstName();
                    }
                }
                else
                {
                    val1 = (b1.getBorrowerLastName() == null) ?
                         "zzz" : b1.getBorrowerLastName();
                    val2 = (b2.getBorrowerLastName() == null) ?
                         "zzz" : b2.getBorrowerLastName();
                }
            }

            return val1.compareTo(val2);
        }
    }*/

	// default will be currency
	private boolean comparDbl(final String keya, double dval1, double dval2) {			 
		return comparDbl(keya, CURRENCY_FORMAT, dval1, dval2); 
	}

	// possible values for typeFmt are: DupChekRATIO_FORMAT, DupChekCURRENCY_FORMAT
	private boolean comparDbl(final String keya, final String typeFmt, double dval1, double dval2) {
		if (dval1 == dval2) 
			return false;

		Formattable nota = newTabedLine(keya, 
				BXResources.newFormata(typeFmt, dval1), 
				BXResources.newFormata(typeFmt, dval2), 
				BXResources.newFormata(typeFmt, dval2 - dval1));				
		addFormtb(nota);
		return maximumReached();
	}

	private boolean comparStr(final String keya, String val1, String val2) {
		if (isEqual(val1, val2)) 
			return false;

		Formattable nota = newTabedLine(keya, val1, val2, null);
		addFormtb(nota);
		return maximumReached();
	}

	private boolean comparInt(final String keya, int ival1, int ival2) {
		if (ival1 == ival2) 
			return false;

		Formattable nota = newTabedLine(keya, ival1, ival2, ival2-ival1);
		addFormtb(nota);
		return maximumReached();
	}

	private boolean comparYrMth(final String keya, int ival1, int ival2) {
		if (ival1 == ival2) 
			return false;

		Formattable nota = newTabedLine(keya, 
				BXResources.newYearsMonths(ival1), 
				BXResources.newYearsMonths(ival2), 
				BXResources.newYearsMonths(ival2 - ival1));				
		addFormtb(nota);
		return maximumReached();
	}

	private boolean comparPkL(final String keya, final String pkLTable, int ival1, int ival2) {
		if (ival1 == ival2) 
			return false;

		Formattable nota = newTabedLine(keya, 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), pkLTable, ival1), 
				BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), pkLTable, ival2), null);				
		addFormtb(nota);				
		return maximumReached();
	}

	private boolean comparDate(final String keya, final Date tval1, final Date tval2) {
		if (isEqual(tval1, tval2)) 
			return false;

		Formattable nota = newTabedLine(keya, 
				tval1 == null? "": BXResources.newFormata(DATE_FORMAT, tval1),  
				tval2 == null? "": BXResources.newFormata(DATE_FORMAT, tval2), null);
		addFormtb(nota);				
		return maximumReached();
	}

	//#DG658
	private boolean comparBool(final String keya, final boolean bval1, final boolean bval2) {
		if (bval1 == bval2) 
			return false;
		
		Formattable nota = newTabedLine(keya,
				BXResources.newGenMsg(bval1 ? YES_LABEL : NO_LABEL),
				BXResources.newGenMsg(bval2 ? YES_LABEL : NO_LABEL), null);
		addFormtb(nota);				
		return maximumReached();
	}
	
	private void addFormtb(Formattable nota) {
		dealNotesList.addElement(nota);
		totalDifferences++;
	}

	/*
	 * produces localized result in 4 tabbed columns
	 * header			item1			item2		difference
	 * 
	 * eg.
	 * 
	 * 
	 */
	public static Formattable newTabedLine(final Formattable head, final Object val1, final Object val2, final Object diff) {
		return new Formattable() {
			public void formatTo(Formatter fmt, int flagas, int width, int precision) {

				int ofset;
				Locale locala = fmt.locale();
				/*StringBuffer sb = new StringBuffer();
  				sb.append(String.format(locala, "%s", head));
  				for (i = sb.length(); i < tab1; i++) 
  					sb.append(' ');
  				if(val1 != null)
  					sb.append(String.format(locala, "%s", val1));
  				for (i = sb.length(); i < tab2; i++) 
  					sb.append(' ');
  				if(val2 != null)
  					sb.append(String.format(locala, "%s", val2));
  				if(diff != null) {
    				for (i = sb.length(); i < tab3; i++) 
    					sb.append(' ');
  					sb.append(String.format(locala, "%s", diff));
  				}
  				fmt.format("%s", sb.toString());*/
				Appendable sb = fmt.out();
				String str;
				try {
					str = String.format(locala, "%s", head);
					sb.append(str);
					ofset = str.length();
					for (; ofset < tab1; ofset++) 
						sb.append(' ');

					if(val1 != null)	{
						str = String.format(locala, "%s", val1);
						sb.append(str);
						ofset += str.length();
					}
					for (; ofset < tab2; ofset++) 
						sb.append(' ');

					if(val2 != null)	{
						str = String.format(locala, "%s", val2);
						sb.append(str);
						ofset += str.length();
					}

					if(diff != null) {
						for (; ofset < tab3; ofset++) 
							sb.append(' ');
						sb.append(String.format(locala, "%s", diff));
					}
				} catch (IOException e) {
					SysLog.error(getClass(), e, "newTabedLine");
				}
			}
			//@Override #DG634 just for logging purpose ...
			public String toString() {
				return "TabedLine:header:"+head+";val1="+val1+";val2="+val2+";diff="+diff;
			}
		};
	}

	/*
	 * having to create a version where the 1st arg. (head) is a Formattable
	 * just for that one call :|
	 */
	public static Formattable newTabedLine(final String keya, final Object val1, final Object val2, final Object diff) {
		return newTabedLine(BXResources.newDPIGMsg(keya), val1, val2, diff);
	}

	/*
	 * iterates all items of this collection localizing and outputting it
	 */
	class FmtblNoteList extends Vector<Formattable> implements Formattable 
	{
		private static final long serialVersionUID = 1L;

		public void formatTo(Formatter fmt, int flagas, int width, int precision) {
			for(Formattable itema: this) {
				try {
					fmt.format("%s%n", itema);
					IOException ioException = fmt.ioException();
					if (ioException != null)
						throw ioException;
				} catch (Exception e) {
					final String msg = "[item="+itema+':'+e+']';
					logger.error(msg);
					fmt.format("%s%n", msg);	//#DG634 just for logging purpose ... 
				}
			}				
		}

	};

	private void printLiabs(final Locale locala, Deal deal1, StringBuffer str, int offset) 
	throws Exception {
		int index = 1;
		for(Borrower bor: (Collection<Borrower>)deal1.getBorrowers())
		{
			//str += "Borrower " + index++ + "\n";
			str.append(BXResources.getDocPrepIngMsg("BORROWER",locala)).append(" ")
			.append(index++).append("\n");

			List<Liability> lbs = (List<Liability>)bor.getLiabilities();
			for (Liability liab : lbs) {
				for(Formattable item: printLiability(liab, offset))             
					str.append(String.format(locala, "%s%n", item));
			}
		}
	}
	//#DG600 end

	/**
	 * Returns a <code>String</code> representation of the deal note, complete with
	 * DELIMITERs in between the fields.
	 *
	 * @return the deal note in text format.
	 */
	public String toString()
	{
		if (dealNotesList.size() == 0)
			return "";
		// is this really ever used?? let's assume the srk language ...
		final Locale locala = BXResources.getLocale(srk.getLanguageId());
		StringBuffer buffer = new StringBuffer(256);
		buffer.append(String.format(locala, "%s", dealNotesHdrList));
		buffer.append(String.format(locala, "%n%s", dealNotesList));

		return buffer.toString();
	}

	/**
	 * Prints information on how to use this class' main() with parameters.
	 */
	private static void printUsage()
	{
		System.err.println("usage: DealCompare [-deal1 deal_num1] [-deal2 deal_num2] [-writenote]");
	}

	// --------------------- DJ, pvcs # 957, 6-Jul-05 Catherine -- begin ---------------------

	public static void main (String args[])
	{
		String marka = null; 
		try
		{
			final String baseDir = "../ADMIN/";
			final String loginId = "DeCompTest";
			Config.SYS_PROPERTIES_LOCATION_DEV = baseDir;
			SysLogger loger = SysLog.getSysLogger(loginId);
			loger.setClass(DealCompare.class);
			loger.info("\n*****\n*****              DealCompare TEST START\n*****");
			PropertiesCache.addPropertiesFile(baseDir +"mossys.properties");
			ResourceManager.init(baseDir, "MOSDev");
			SysLog.init(baseDir);
			new BXResources();

			final Locale locala = Locale.ENGLISH;

			Deal deal1, deal2;

			int d1 = 645;
			int d2 = 648;

			boolean bWriteNote = false;

			//  If deal numbers were provided to the class, use them
			for (int i=0; i < args.length; i++)
			{
				if (args[i].equalsIgnoreCase("-deal1"))
				{
					try
					{
						d1 = Integer.parseInt(args[++i]);
					}
					catch (Exception e)
					{
						System.out.println("Error parsing argument 1 (" + args[0] + "): " + e);
						printUsage();
						return;
					}
				}
				else if (args[i].equalsIgnoreCase("-deal2"))
				{
					try
					{
						d2 = Integer.parseInt(args[++i]);
					}
					catch (Exception e)
					{
						System.out.println("Error parsing argument 2 (" + args[1] + "): " + e);
						printUsage();
						return;
					}
				}
				else if (args[i].equalsIgnoreCase("-writenote"))
				{
					bWriteNote = true;
				}
			}
			System.out.println("----->>>Comparing deals " + d1 + " and " + d2 + "...");
			
			SessionResourceKit srk = new SessionResourceKit(loginId);

			marka = "retrieving deal1"; 
			deal1 = new Deal(srk, null).findByGoldCopy(d1);

			marka = "retrieving deal2"; 
			deal2 = new Deal(srk, null).findByGoldCopy(d2);

			marka = "comparing"; 
			DealCompare dc = new DealCompare(deal1, deal2, srk);
			dc.reset();

			boolean debuggingLiabilities = false;
			boolean debuggingOther = false;
			StringBuffer str = new StringBuffer();
			srk.beginTransaction();

			if (debuggingOther)
			{
				System.out.println("Has significant changes: " + dc.hasSignificantChanges(deal1, deal2));
			}
			else if (debuggingLiabilities)
			{
				str.append("DEAL 1\n\n");
				dc.printLiabs(locala, deal1, str, 1);

				str.append("DEAL 2\n\n");
				dc.printLiabs(locala, deal2, str, 2);

			}
			else
			{
				dc.compareAll();
				dc.compareInsureOnlyApplicants();
				dc.compareParties();
				dc.comparePartiesByType( Mc.PARTY_TYPE_SERVICE_BRANCH ); 

				dc.buildHeader();
				str.append(String.format(locala, "%s", dc.dealNotesHdrList)).append('\n');
				str.append(String.format(locala, "%s", dc.dealNotesList));

				System.out.println(str);
				loger.info("DealCompare(" +
						deal1.getDealId() + "," + deal1.getCopyId() + ")X(" +
						deal2.getDealId() + "," + deal2.getCopyId() + ")");

			}
			loger.info(str.toString());

			if (bWriteNote)
			{
				dc.writeNoteToDeal(BXResources.newStrFormata("my test prefix"), deal1);
				dc.writeNoteToDeal(null, deal2);
			}

			System.out.println("\nTotal number of differences: " + dc.getTotalDifferences());

			srk.rollbackTransaction();
			//srk.commitTransaction();
			srk.freeResources();
			ResourceManager.shutdown();

			System.out.println("End:");
		}
		catch (Exception e)
		{
			System.err.println(marka+"\n");
			e.printStackTrace();
		}
	}
	
	
	/**
     * <p>
     * Description: This method compare two List consisting of Component
     * entities
     * </p>
     * @version 1.0 XS_1.8 14-Aug-2008 Initial Version
     * @param lComp1
     *            List<Component> Object.
     * @param lComp2
     *            List<Component> Object
     */
  protected void doComponentListCompare(List<Component> lComp1,
	  List<Component> lComp2)
  {
	logger.info("Start:doComponentListCompare");
	if (lComp1 == null || lComp2 == null)
	  return;

	class CompareComponent implements Comparator<Component>
	{
	  public int compare(Component comp1, Component comp2)
	  {
		logger.info("doComponentListCompare:CompareComponent Inner Class compare method");
		final int icompId1 = comp1.getComponentId();
		final int icompId2 = comp2.getComponentId();
		if (icompId1 == icompId2)
		  return Double.compare(comp1.getComponentId(), comp2.getComponentId());
		else
		  return (icompId1 < icompId2 ? -1 : (icompId1 == icompId2 ? 0 : 1));

	  }
	};

	CompareComponent compareComps = new CompareComponent();
	Collections.sort(lComp1, compareComps);
	Collections.sort(lComp2, compareComps);
	Component comp1, comp2;
	boolean found;
	Formattable nota;

	for ( int outerFlag = 0; outerFlag < lComp1.size(); outerFlag++ )
	{
	  comp1 = lComp1.get(outerFlag);
	  found = false;
	  logger.info("doComponentListCompare: first for loop");
	  for ( int innerFlag = 0; innerFlag < lComp2.size(); innerFlag++ )
	  {
		comp2 = lComp2.get(innerFlag);
		if (comp1.getComponentTypeId() == comp2.getComponentTypeId()
			&& comp1.getMtgProdId() == comp2.getMtgProdId())
		{
		  if (comp1.getComponentTypeId() == 1)
		  {
			compareComponentMortgages(comp1, comp2);
		  }
		  else if (comp1.getComponentTypeId() == 2)
		  {
			compareComponentLOCs(comp1, comp2);
		  }
		  else if (comp1.getComponentTypeId() == 3)
		  {
			compareComponentLoans(comp1, comp2);
		  }
		  else if (comp1.getComponentTypeId() == 4)
		  {
			compareComponentCreditCards(comp1, comp2);
		  }
		  else
		  {
			compareComponentOverDrafts(comp1, comp2);
		  }
		  lComp1.remove(outerFlag--);
		  lComp2.remove(innerFlag);

		  found = true;
		  break;
		}

	  }

	  if (!found)
	  {
		nota = BXResources.newFormata(DUP_CHECK_COMPONENT_DELETED, BXResources
			.newPkList(srk.getExpressState().getDealInstitutionId(),
				PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
			.newPkList(srk.getExpressState().getDealInstitutionId(),
				PKL_MTGPROD, comp1.getMtgProdId()));
		dealNotesList.addElement(nota);
     //*****************Bugfix23339 : After adding the new component printing Components details -Starts************************************/
     if (comp1.getComponentTypeId() == 5)
         dealNotesList.addAll(printOverdraftComponent(comp1, 1));
        else if (comp1.getComponentTypeId() == 4)
             dealNotesList.addAll(printCreditCardComponent(comp1, 1));
        else if (comp1.getComponentTypeId() == 3)
             dealNotesList.addAll(printLoanComponent(comp1, 1));
        else if (comp1.getComponentTypeId() == 2)
             dealNotesList.addAll(printLOCComponent(comp1, 1));
        else
             dealNotesList.addAll(printMortgageComponent(comp1, 1));
     //*****************Bugfix23339 : After adding the new component printing Components details -Ends************************************/
		totalDifferences++;
	  }
	  logger.info("End:doComponentListCompare");
	}
  
  //*****************Bugfix23339 : After adding the new component printing Components details -Starts************************************/

  for (Component new_comp: lComp2)
  {
  	  nota = BXResources.newFormata(DUP_CHECK_COMPONENT_ADDED, BXResources
		  .newPkList(srk.getExpressState().getDealInstitutionId(),
			  PKL_COMPONENT_TYPE, new_comp.getComponentTypeId()), BXResources
		  .newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
       new_comp.getMtgProdId()));
	     addFormtb(nota);
       
   if (new_comp.getComponentTypeId() == 5)
    dealNotesList.addAll(printOverdraftComponent(new_comp, 2));
   else if (new_comp.getComponentTypeId() == 4)
        dealNotesList.addAll(printCreditCardComponent(new_comp, 2));
   else if (new_comp.getComponentTypeId() == 3)
        dealNotesList.addAll(printLoanComponent(new_comp, 2));
   else if (new_comp.getComponentTypeId() == 2)
        dealNotesList.addAll(printLOCComponent(new_comp, 2));
   else
        dealNotesList.addAll(printMortgageComponent(new_comp, 2));
    totalDifferences++;
	}
  }
  //*****************Bugfix23339 : After adding the new component printing Components details -Ends************************************/
  /**
     * <p>
     * Description: This method compare two ComponentMortgage entities
     * </p>
     * @version 1.0 XS_1.8 14-Aug-2008 Initial Version
     * @param comp1
     *            Component Object.
     * @param comp2
     *            Component Object
     */
  private void compareComponentMortgages(Component comp1, Component comp2)
  {
	boolean compHasChanged;
	Formattable nota;
	logger.info("Start:compareComponentMortgages");
	ComponentMortgage compMtg1 = comp1.getComponentMortgage();
	ComponentMortgage compMtg2 = comp2.getComponentMortgage();
	compHasChanged = (compMtg1.getActualPaymentTerm() != compMtg2
		.getActualPaymentTerm()
		|| compMtg1.getAmortizationTerm() != compMtg2.getAmortizationTerm()
		|| compMtg1.getBuyDownRate() != compMtg2.getBuyDownRate()
		|| compMtg1.getCashBackAmount() != compMtg2.getCashBackAmount()
		|| compMtg1.getCashBackPercent() != compMtg2.getCashBackPercent()
		|| compMtg1.getPremium() != compMtg2.getPremium()
		|| compMtg1.getDiscount() != compMtg2.getDiscount()
		|| compMtg1.getMortgageAmount() != compMtg2.getMortgageAmount() || compMtg1
		.getRateGuaranteePeriod() != compMtg2.getRateGuaranteePeriod());
	if (!compHasChanged)
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_CHANGED, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	if (maximumReached())
	  return;
	double dval1, dval2;
	int ival1, ival2;
	ival1 = compMtg1.getActualPaymentTerm();
	ival2 = compMtg2.getActualPaymentTerm();
	logger.info("compareComponentMortgages:Actualpaymentterm");
	if (comparInt(DUP_CHEK_ACTUAL_PAYMENT_TERM, ival1, ival2))
	  return;
	ival1 = compMtg1.getAmortizationTerm();
	ival2 = compMtg2.getAmortizationTerm();
	logger.info("compareComponentMortgages:AmortizationTerm");
	if (comparYrMth(DUP_CHEK_AMORTIZATION_PERIOD, ival1, ival2))
	  return;
	dval1 = compMtg1.getBuyDownRate();
	dval2 = compMtg2.getBuyDownRate();
	logger.info("compareComponentMortgages:Buydownrate");
	if (comparDbl(DUP_CHEK_BUYDOWNRATE, RATIO_FORMAT, dval1, dval2))
	  return;
	dval1 = compMtg1.getCashBackAmount();
	dval2 = compMtg2.getCashBackAmount();
	logger.info("compareComponentMortgages:CashBackAmount");
	if (comparDbl(DUP_CHEK_CASH_BACK_AMOUNT, dval1, dval2))
	  return;
	dval1 = compMtg1.getCashBackPercent();
	dval2 = compMtg2.getCashBackPercent();
	logger.info("compareComponentMortgages:CashBackPercent");
	//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
	//if (comparDbl(DUP_CHEK_CASH_BACK_PERCENT, dval1, dval2))
	if (comparDbl(DUP_CHEK_CASH_BACK_PERCENT, RATIO_FORMAT, dval1, dval2))
	  return;
	dval1 = compMtg1.getPremium();
	dval2 = compMtg2.getPremium();
	logger.info("compareComponentMortgages:Premium");
	//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
	//if (comparDbl(DUP_CHEK_PREMIUM, dval1, dval2))
	if (comparDbl(DUP_CHEK_PREMIUM, RATIO_FORMAT, dval1, dval2))
	  return;
	dval1 = compMtg1.getDiscount();
	dval2 = compMtg2.getDiscount();
	logger.info("compareComponentMortgages:Discount");
	//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT
	//if (comparDbl(DUP_CHEK_DISCOUNT, dval1, dval2))
	if (comparDbl(DUP_CHEK_DISCOUNT, RATIO_FORMAT, dval1, dval2))
	  return;
	dval1 = compMtg1.getMortgageAmount();
	dval2 = compMtg2.getMortgageAmount();
	logger.info("compareComponentMortgages:MortgageAmount");
	if (comparDbl(DUP_CHEK_MORTGAGE_AMOUNT, dval1, dval2))
	  return;
	ival1 = compMtg1.getRateGuaranteePeriod();
	ival2 = compMtg2.getRateGuaranteePeriod();
	logger.info("compareComponentMortgages:Rateguaranteeperiod");
	if (comparInt(DUP_CHEK_RATEGUARANTEPERIOD, ival1, ival2))
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_END, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	logger.info("End:compareComponentMortgages");
  }

  /**
     * <p>
     * Description: This method compare two ComponentLOC entities
     * </p>
     * @version 1.0 XS_1.8 14-Aug-2008 Initial Version
     * @param comp1
     *            Component Object.
     * @param comp2
     *            Component Object
     */
  private void compareComponentLOCs(Component comp1, Component comp2)
  {
	boolean compHasChanged;
	Formattable nota;
	logger.info("Start:compareComponentLOCs");
	ComponentLOC compLoc1 = comp1.getComponentLOC();
	ComponentLOC compLoc2 = comp2.getComponentLOC();
	compHasChanged = (compLoc1.getPremium() != compLoc2.getPremium()
		|| compLoc1.getDiscount() != compLoc2.getDiscount() || compLoc1
		.getLocAmount() != compLoc2.getLocAmount());
	if (!compHasChanged)
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_CHANGED, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	if (maximumReached())
	  return;
	double dval1, dval2;
	dval1 = compLoc1.getPremium();
	dval2 = compLoc2.getPremium();
	logger.info("compareComponentLOCs:Premium");
	//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
	//if (comparDbl(DUP_CHEK_PREMIUM, dval1, dval2))
	if (comparDbl(DUP_CHEK_PREMIUM, RATIO_FORMAT, dval1, dval2))
	  return;

	dval1 = compLoc1.getDiscount();
	dval2 = compLoc2.getDiscount();
	logger.info("compareComponentLOCs:Discount");
	//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
	//if (comparDbl(DUP_CHEK_DISCOUNT, dval1, dval2))
	if (comparDbl(DUP_CHEK_DISCOUNT, RATIO_FORMAT, dval1, dval2))
	  return;

	dval1 = compLoc1.getLocAmount();
	dval2 = compLoc2.getLocAmount();
	logger.info("compareComponentLOCs: LocAmount");
	if (comparDbl(DUP_CHEK_LINE_OF_CREDIT_AMOUNT, dval1, dval2))
	  return;

	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_END, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	logger.info("End:compareComponentLOCs");
  }

  /**
     * <p>
     * Description: This method compare two ComponentLoan entities
     * </p>
     * @version 1.0 XS_1.8 14-Aug-2008 Initial Version
     * @param comp1
     *            Component Object.
     * @param comp2
     *            Component Object
     */
  private void compareComponentLoans(Component comp1, Component comp2)
  {
	boolean compHasChanged;
	Formattable nota;
	logger.info("Start:compareComponentLoans");
	ComponentLoan compLoan1, compLoan2;
	try
	{
	  compLoan1 = comp1.getComponentLoan();
	}
	catch (Exception e)
	{
	  compLoan1 = null;
	}

	try
	{
	  compLoan2 = comp2.getComponentLoan();
	}
	catch (Exception e)
	{
	  compLoan2 = null;
	}
	if (compLoan1 == null || compLoan2 == null)
	  return;
	compHasChanged = (compLoan1.getActualPaymentTerm() != compLoan2
		.getActualPaymentTerm()
		|| compLoan1.getPremium() != compLoan2.getPremium()
		|| compLoan1.getDiscount() != compLoan2.getDiscount() || compLoan1
		.getLoanAmount() != compLoan2.getLoanAmount());
	if (!compHasChanged)
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_CHANGED, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	if (maximumReached())
	  return;
	double dval1, dval2;
	int ival1, ival2;
	ival1 = compLoan1.getActualPaymentTerm();
	ival2 = compLoan2.getActualPaymentTerm();
	logger.info("compareComponentLoans:ActualPaymentTerm");
	if (comparInt(DUP_CHEK_ACTUAL_PAYMENT_TERM, ival1, ival2))
	  return;
	dval1 = compLoan1.getPremium();
	dval2 = compLoan2.getPremium();
	logger.info("compareComponentLoans:Premium");
	//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
    //if (comparDbl(DUP_CHEK_PREMIUM, dval1, dval2))
	if (comparDbl(DUP_CHEK_PREMIUM, RATIO_FORMAT, dval1, dval2))
	      return;
	dval1 = compLoan1.getDiscount();
	dval2 = compLoan2.getDiscount();
	logger.info("compareComponentLoans:Discount");
	//FXP23237, MCM Nov 02 2008,  changed CURRENCY_FORMAT to RATIO_FORMAT 
	//if (comparDbl(DUP_CHEK_DISCOUNT, dval1, dval2))
	if (comparDbl(DUP_CHEK_DISCOUNT, RATIO_FORMAT, dval1, dval2))
	  return;
	dval1 = compLoan1.getLoanAmount();
	dval2 = compLoan2.getLoanAmount();
	logger.info("compareComponentLoans:LoanAmount");
	if (comparDbl(DUP_CHEK_LOAN_AMOUNT, dval1, dval2))
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_END, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	logger.info("End:compareComponentLoans");
  }

  /**
     * <p>
     * Description: This method compare two ComponentCreditCard entities
     * </p>
     * @version 1.0 XS_1.8 14-Aug-2008 Initial Version
     * @param comp1
     *            Component Object.
     * @param comp2
     *            Component Object
     */
  private void compareComponentCreditCards(Component comp1, Component comp2)
  {
	boolean compHasChanged;
	Formattable nota;
	logger.info("Start:compareComponentCreditCards");
	ComponentCreditCard compCC1 = comp1.getComponentCreditCard();
	ComponentCreditCard compCC2 = comp2.getComponentCreditCard();
	compHasChanged = compCC1.getCreditCardAmount() != compCC2
		.getCreditCardAmount();
	if (!compHasChanged)
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_CHANGED, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	if (maximumReached())
	  return;
	double dval1, dval2;
	dval1 = compCC1.getCreditCardAmount();
	dval2 = compCC2.getCreditCardAmount();
	logger.info("compareComponentCreditCards: CreditCardAmount");
	if (comparDbl(DUP_CHEK_CREDIT_CARD_AMOUNT, dval1, dval2))
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_END, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	logger.info("End:compareComponentCreditCards");
  }

  /**
     * <p>
     * Description: This method compare two ComponentOverdraft entities
     * </p>
     * @version 1.0 XS_1.8 14-Aug-2008 Initial Version
     * @param comp1
     *            Component Object.
     * @param comp2
     *            Component Object
     */
  private void compareComponentOverDrafts(Component comp1, Component comp2)
  {

	boolean compHasChanged;
	Formattable nota;
	logger.info("Start:compareComponentOverDrafts");
	ComponentOverdraft compOverDft1, compOverDft2;;
	try
	{
	  compOverDft1 = comp1.getComponentOverdraft();
	}
	catch (Exception e)
	{
	  compOverDft1 = null;
	}
	try
	{
	  compOverDft2 = comp2.getComponentOverdraft();
	}
	catch (Exception e)
	{
	  compOverDft2 = null;
	}
	if (compOverDft1 == null || compOverDft2 == null)
	  return;
	compHasChanged = compOverDft1.getOverDraftAmount() != compOverDft2
		.getOverDraftAmount();
	if (!compHasChanged)
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_CHANGED, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	if (maximumReached())
	  return;
	double dval1, dval2;
	dval1 = compOverDft1.getOverDraftAmount();
	dval2 = compOverDft2.getOverDraftAmount();
	logger.info("compareComponentOverDrafts: OverDraftAmount");
	if (comparDbl(DUP_CHEK_OVERDRAFT_AMOUNT, dval1, dval2))
	  return;
	nota = BXResources.newFormata(DUP_CHECK_COMPONENT_END, BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(),
			PKL_COMPONENT_TYPE, comp1.getComponentTypeId()), BXResources
		.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD,
			comp1.getMtgProdId()));
	dealNotesList.addElement(nota);
	logger.info("End:compareComponentOverDrafts");
  }
 //********************************Bug Fix***************************************//
  /**
   * <p>
   * Description: This method to print the componentOverdraft entities
   * </p>
   */
private  Vector<Formattable> printOverdraftComponent(Component comp2, int offset)
{
    Formattable nota;
    Vector<Formattable> dealNotesList = new Vector<Formattable>();
    logger.info("Start:PrintComponentOverDrafts");
    ComponentOverdraft compOverDft2;;
    try
    {
        compOverDft2 = comp2.getComponentOverdraft();
    }
    catch (Exception e)
    {
        compOverDft2 = null;
    }
  
    double dval2;
    dval2 = compOverDft2.getOverDraftAmount();
    
    logger.info("compareComponentOverDrafts: OverDraftAmount");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_OVERDRAFT_AMOUNT, BXResources.newFormata(CURRENCY_FORMAT, dval2), null, null);
    else
    	nota = newTabedLine(DUP_CHEK_OVERDRAFT_AMOUNT,null,BXResources.newFormata(CURRENCY_FORMAT, dval2),null );
    dealNotesList.addElement(nota);
    
    logger.info("End:PrintComponentOverDrafts");
    return dealNotesList;
}
/**
 * <p>
 * Description: This method to print the  ComponentCreditCard entities
 * </p>
 */
private Vector<Formattable> printCreditCardComponent(Component comp2, int offset)
{

    Formattable nota;
    Vector<Formattable> dealNotesList = new Vector<Formattable>();
    logger.info("Start:PrintComponentOverDrafts");
    ComponentCreditCard componentCreditCard;;
    try
    {
        componentCreditCard = comp2.getComponentCreditCard();
    }
    catch (Exception e)
    {
        componentCreditCard = null;
    }

    double dval2;
    dval2 = componentCreditCard.getCreditCardAmount();
    logger.info("compareComponentCreditcard: CreditcardAmount");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_CREDIT_CARD_AMOUNT,BXResources.newFormata(CURRENCY_FORMAT, dval2),null,null );
    else
    	nota = newTabedLine(DUP_CHEK_CREDIT_CARD_AMOUNT,null,BXResources.newFormata(CURRENCY_FORMAT, dval2),null );
    	
    dealNotesList.addElement(nota);
    logger.info("End:PrintComponentOverDrafts");
    return dealNotesList;
}
/**
 * <p>
 * Description: This method to print the  ComponentLoan entities
 * </p>
 */
private Vector<Formattable> printLoanComponent(Component comp2, int offset)
{
 
    Formattable nota;
    Vector<Formattable> dealNotesList = new Vector<Formattable>();
    
    logger.info("Start:PrintComponentLoan");
    ComponentLoan componentLoan;;
    try
    {
        componentLoan = comp2.getComponentLoan();
    }
    catch (Exception e)
    {
        componentLoan = null;
    }

    double dval2;
   
    dval2 = componentLoan.getActualPaymentTerm();
    logger.info("compareComponentLoans:ActualPaymentTerm");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_ACTUAL_PAYMENT_TERM,dval2,null,null );
    else
    	nota = newTabedLine(DUP_CHEK_ACTUAL_PAYMENT_TERM,null,dval2,null );
    dealNotesList.addElement(nota);
    
    dval2 = componentLoan.getPremium();
    logger.info("compareComponentLoans:Premium");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_PREMIUM,BXResources.newFormata(RATIO_FORMAT, dval2),null,null );
    else
    	nota = newTabedLine(DUP_CHEK_PREMIUM,null,BXResources.newFormata(RATIO_FORMAT, dval2),null );
    dealNotesList.addElement(nota);
    
    dval2 = componentLoan.getDiscount();
    logger.info("compareComponentLoans:Discount");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_DISCOUNT,BXResources.newFormata(RATIO_FORMAT, dval2),null,null );
    else
    	nota = newTabedLine(DUP_CHEK_DISCOUNT,null,BXResources.newFormata(RATIO_FORMAT, dval2),null );
    dealNotesList.addElement(nota);
    
    dval2 = componentLoan.getLoanAmount();
    logger.info("compareComponentLoans:LoanAmount");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_LOAN_AMOUNT,BXResources.newFormata(CURRENCY_FORMAT, dval2),null,null );
    else
    	nota = newTabedLine(DUP_CHEK_LOAN_AMOUNT,null,BXResources.newFormata(CURRENCY_FORMAT, dval2),null );
    	
    dealNotesList.addElement(nota);
    
    logger.info("End:PrintComponentLoan");
    
    return dealNotesList;
}
/**
 * <p>
 * Description: This method to print the  ComponentLOC entities
 * </p>
 */
private Vector<Formattable> printLOCComponent(Component comp2, int offset)
{

    Vector<Formattable> dealNotesList = new Vector<Formattable>();
    Formattable nota;
    logger.info("Start:PrintComponentLOC");
    ComponentLOC componentLOC;;
    try
    {
        componentLOC = comp2.getComponentLOC();
    }
    catch (Exception e)
    {
        componentLOC = null;
    }
    double dval2;
    
    dval2 = componentLOC.getPremium();
    logger.info("compareComponentLOCs:Premium");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_PREMIUM,BXResources.newFormata(RATIO_FORMAT, dval2),null,null );
    else
    	nota = newTabedLine(DUP_CHEK_PREMIUM,null,BXResources.newFormata(RATIO_FORMAT, dval2),null );
    dealNotesList.addElement(nota);
    
    dval2 = componentLOC.getDiscount();
    logger.info("compareComponentLOC:Discount");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_DISCOUNT,BXResources.newFormata(RATIO_FORMAT, dval2),null,null );
    else
    	nota = newTabedLine(DUP_CHEK_DISCOUNT,null,BXResources.newFormata(RATIO_FORMAT, dval2),null );
    dealNotesList.addElement(nota);
    
    dval2 = componentLOC.getLocAmount();
    logger.info("compareComponentLOC:LocAmount");
    if (offset == 1)
    	nota = newTabedLine(DUP_CHEK_LOAN_AMOUNT,BXResources.newFormata(CURRENCY_FORMAT, dval2),null,null );
    else
    	nota = newTabedLine(DUP_CHEK_LOAN_AMOUNT,null,BXResources.newFormata(CURRENCY_FORMAT, dval2),null );
    dealNotesList.addElement(nota);
    
    logger.info("End:PrintComponentLOC");
    
    return dealNotesList;
}
/**
 * <p>
 * Description: This method to print the  ComponentMortgage entities
 * </p>
 */
private Vector<Formattable> printMortgageComponent(Component comp2, int offset)
{

    Formattable nota;
    Vector<Formattable> dealNotesList = new Vector<Formattable>();
    logger.info("Start:PrintComponentMortgage");
    ComponentMortgage compMtg2;
    try
    {
        compMtg2 = comp2.getComponentMortgage();
    }
    catch (Exception e)
    {
        compMtg2 = null;
    }

    double dval2;
    int ival2;

    
      ival2 = compMtg2.getAmortizationTerm();
      logger.info("compareComponentMortgages:AmortizationTerm");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_AMORTIZATION_PERIOD, BXResources.newYearsMonths(ival2), null, null);
      else
    	  nota = newTabedLine(DUP_CHEK_AMORTIZATION_PERIOD,null, BXResources.newYearsMonths(ival2),null);
      dealNotesList.addElement(nota);
      
      dval2 = compMtg2.getBuyDownRate();
      logger.info("compareComponentMortgages:Buydownrate");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_BUYDOWNRATE, BXResources.newFormata(RATIO_FORMAT, dval2), null, null);
      else
    	  nota = newTabedLine(DUP_CHEK_BUYDOWNRATE, null, BXResources.newFormata(RATIO_FORMAT, dval2), null);
      dealNotesList.addElement(nota);
     
      dval2 = compMtg2.getCashBackAmount();
      logger.info("compareComponentMortgages:CashBackAmount");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_CASH_BACK_AMOUNT, BXResources.newFormata(CURRENCY_FORMAT, dval2), null, null);
      else
    	  nota = newTabedLine(DUP_CHEK_CASH_BACK_AMOUNT, null, BXResources.newFormata(CURRENCY_FORMAT, dval2), null);
      dealNotesList.addElement(nota);
      
      dval2 = compMtg2.getCashBackPercent();
      logger.info("compareComponentMortgages:CashBackPercent");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_CASH_BACK_PERCENT, BXResources.newFormata(RATIO_FORMAT, dval2), null, null);
      else
    	  nota = newTabedLine(DUP_CHEK_CASH_BACK_PERCENT, null, BXResources.newFormata(RATIO_FORMAT, dval2), null);
      dealNotesList.addElement(nota);
      
      dval2 = compMtg2.getPremium();
      logger.info("compareComponentMortgages:Premium");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_PREMIUM, BXResources.newFormata(RATIO_FORMAT, dval2), null, null);
      else
    	  nota = newTabedLine(DUP_CHEK_PREMIUM, null, BXResources.newFormata(RATIO_FORMAT, dval2), null);
      dealNotesList.addElement(nota);

      dval2 = compMtg2.getDiscount();
      logger.info("compareComponentMortgages:Discount");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_DISCOUNT,BXResources.newFormata(RATIO_FORMAT, dval2),null,null );
      else
    	  nota = newTabedLine(DUP_CHEK_DISCOUNT,null,BXResources.newFormata(RATIO_FORMAT, dval2),null );
      dealNotesList.addElement(nota);
   
      dval2 = compMtg2.getMortgageAmount();
      logger.info("compareComponentMortgages:MortgageAmount");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_MORTGAGE_AMOUNT,BXResources.newFormata(CURRENCY_FORMAT, dval2),null,null );
      else
    	  nota = newTabedLine(DUP_CHEK_MORTGAGE_AMOUNT,null,BXResources.newFormata(CURRENCY_FORMAT, dval2),null );
      dealNotesList.addElement(nota);
        
      ival2 = compMtg2.getRateGuaranteePeriod();
      logger.info("compareComponentMortgages:Rateguaranteeperiod");
      if (offset == 1)
    	  nota = newTabedLine(DUP_CHEK_RATEGUARANTEPERIOD,ival2,null,null );
      else
    	  nota = newTabedLine(DUP_CHEK_RATEGUARANTEPERIOD,null,ival2,null );
      dealNotesList.addElement(nota);
      
      logger.info("End:PrintComponentMortgage");
      
      return dealNotesList;
}
 //******************************************************************************// 
  
} 
