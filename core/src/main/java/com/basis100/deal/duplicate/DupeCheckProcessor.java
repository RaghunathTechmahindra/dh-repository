package com.basis100.deal.duplicate;

/**
 * 12/Oct/2007 DVG #DG640 FXP18697: DupeChcek - BDN Express - The note generated in Deal Y during the Deal Compare Process is NOT correct 
 * 10/Oct/2007 DVG #DG636 FXP18688: DupeCheck - BDN Express - The deal Note in Deal Y is not correct when the active deal (deal X) has preApproval offered status, and the incoming deal Y is the firm up deal 
 * 28/Sep/2007 DVG #DG628 FXP18531: BDN Express - DupeCheck - Deal Comparing Header is always showing even when there is no change in the resubmission 
 * 24/Aug/2007 DVG #DG618 LEN197782: Christine Cieslak dupe check not working? 
	- dupecheck comparison is case insensitive for key fields
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 * 19/Apr/2007 DVG #DG598 dupeCheck engine refactoring
 * 09/Apr/2007 DVG #DG596 FXP16467: PIVOTAL_QA_Deals not assigned to Underwriter 
 * 08/Aug/2008 MCM Impl Team XS_1.1 Modified performCreateWithExisting(),performReplace() and Added processQualifyDetails(..)
 * 10/Oct/2008 FXP22820 : Modified findContextId and added findComponentId and isComponentMatch methods
 */

import java.util.*;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.DLM;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;

import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.message.ClientMessage;
import com.basis100.util.message.ClientMessageHandler;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.xml.DomUtil;
import com.filogix.util.Xc;

// #DG598 no more abstract, totally replaces 'com/basis100/deal/duplicate/GENXDupeCheckProcessor.java'
 
 
public class DupeCheckProcessor implements Xc
{
  //#DG600 protected int requiredAction = -1;
  protected boolean duplicate = false;
  protected DealPK storedDealPK;
  protected SysLogger logger;
  protected StringBuffer msgBuf;
  protected SessionResourceKit srk;
  protected int notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL;
  protected int lockedDealId;
  protected ArrayList<ClientMessage> messagesForBroker;
  protected boolean capLinkRequired = false;
  protected boolean capLinkSensitive = false;
  protected boolean cervusUploadRequired = false;

  //#DG598 --------------------------- Dupe Check refactoring start --------------------------------------------
  protected static final class DUPECHECK_MSG {
  	static final int PostFundPreApp  = 101; 
	  static final int PostFund  = 102; 
	  static final int FundPreApp  = 103; 
	  static final int Fund  = 104; 
	  static final int ToServicPreApp  = 105; 
	  static final int ToServic  = 106; 
	  static final int FundRevPreApp  = 107; 
	  static final int FundRev  = 108; 
	  static final int ColapsPreApp  = 109; 
	  static final int Colaps = 110; 
	  static final int DeniedPreApp = 111; 
	  static final int Denied = 112; 
  }  
  protected Deal deal;
  protected Deal existingDeal;
  //#DG600 protected ArrayList dealNotes;
  protected List<Deal> matchedEntries;
  protected org.w3c.dom.Document dom;
  protected SearchResult souapplr;
  // --------------------------- Dupe Check refactoring end ---------------------------------------------

  // ------------------------------------ Abstract methods begin -----------------------
  // this is called from processDuplicate() to set various upload flags, like 
  // capLinkRequired, cervusUploadRequired and others, if exist 
  protected void processUploadFlags(){
    writeLog("processUploadFlags(): no implementation in base class");
  }
  
  // this is called from process() to set sensitivity flags (ie. capLinkSensitive)
  protected void processSensitivityFlags() {
    writeLog("processSensitivityFlags(): no implementation in base class");
  }
  
  protected void doSpecialCompare(DealCompare dealCompare) {
    writeLog("doSpecialCompare(DealCompare dealCompare): no implementation in base class");
  }
  
  protected int getExistingPrimPropertyId() throws RemoteException, FinderException {
    writeLog("getExistingPrimPropertyId(): no implementation in base class");
    return 0;
  }

  protected void doSpecialProcessingOnReplace(int lExistPrimPropId) throws RemoteException, FinderException {
    writeLog("no implementation in base class");
  }

  //at 20/apr/2007, used for DJ
  protected void updatePartyProfileAssoc() throws RemoteException, FinderException {
    writeLog("updatePartyProfileAssoc(): no implementation in base class");
  }
  // ------------------------------------ Abstract methods end -----------------------
  //#DG598 end

  protected  boolean isSoftDenial( Deal pDeal ){
    int currStatus = pDeal.getStatusId();

    if( currStatus != Mc.DEAL_DENIED ){
      return false;
    }

    int denialReason = pDeal.getDenialReasonId();
    String reasonSoftness;
    int reasonSoftnessInt;
    boolean soft = true;

    reasonSoftness = PicklistData.getMatchingColumnValue(-1, PKL_DENIAL_REASON, denialReason,"catageory");

    if( reasonSoftness != null ){
      try{
        reasonSoftnessInt = Integer.parseInt(reasonSoftness);
        soft = reasonSoftnessInt == 1;
      }catch(Exception exc){
        soft = false;
      }
    }else{
      soft = false;
    }
    return soft;
  }

  protected ClientMessage findMessage(SessionResourceKit srk, int id, int language){
    ClientMessage rv = null;
    try{
      rv = ClientMessageHandler.getInstance().findMessage(srk,id,language);
    } catch(Exception exc){
      rv = new ClientMessage();
    }
    return rv;
  }

 public DealPK getStoredDealPK(){
  return storedDealPK;
 }

 public int getNotificationType(){
  return notificationType;
 }


 protected void writeLog( String text ){
    String logId = getClass().getName();
    if(logger == null){
		 //logger = ResourceManager.getSysLogger(logId);
		 logger = new SysLogger(this, logId);
    }
    if (!logger.isTraceOn())
		 return;
    String logMsga = "Ingestion: DupeCheck: "+logId +" : "+text;
    logger.trace( logMsga);
 }

 // added by denis's change - since 4.3GR
 protected void writeLog( String text , Object caller ){
	    if(logger == null){
	      logger = srk.getSysLogger();
	    }
	    msgBuf = new StringBuffer("Ingestion: DupeCheck: ");
	    msgBuf.append(caller.getClass().getName() ).append(" : ");
	    msgBuf.append(text);
	    logger.trace( msgBuf.toString() );
	 }

 public boolean isAnyDealLocked(){
  if(lockedDealId == -1){
    return false;
  }
  return true;
 }

 public int getLockedDealId(){
  return lockedDealId;
 }

  public boolean isDuplicate(){
    return duplicate;
  }

  protected void setDuplicate(boolean duplicateOrNot){
    duplicate = duplicateOrNot;
  }

  /**
   * Method returns the list of messages that are created for the broker.
   */
  public ArrayList getMessagesForBroker(){
    return this.messagesForBroker;
  }

  protected void setCapLinkRequired(boolean pRequired){
    capLinkRequired = pRequired;
  }

  public boolean isCapLinkRequired(){
    return capLinkRequired;
  }

  protected void setCapLinkSensitive(boolean pSensitive){
    capLinkSensitive = pSensitive;
  }

  public boolean isCapLinkSensitive(){
    return capLinkSensitive;
  }

  protected void setCervusUploadRequired(boolean pReq){
    cervusUploadRequired = pReq;
  }

  public boolean isCervusUploadRequired(){
    return cervusUploadRequired;
  }

  //#DG596 if users missing, send it back
	protected void checkUserProfiles(Deal deal) throws DealLockedException {
	  DealUserProfiles dealUserProfiles = new DealUserProfiles(deal, srk);
	  if (dealUserProfiles.getUnderwriterProfile() == null 
	  		|| dealUserProfiles.getAdministratorProfile() == null
	  		|| dealUserProfiles.getFunderProfile() == null) {   
	    String msg = " Underwriter or AdministratorId or Funder Profile Id missing - raising DealLockedException";
	    writeLog(msg);    
	    throw new DealLockedException(msg);
	  }
	}

  //#DG598 --------------------------- Dupe Check refactoring start --------------------------------------------
  /**
     * Messages for broker gets appended to original DOM.
     */
  protected void appendDupeCheckMessageToSource() throws Exception {
      List l = DomUtil.getNamedDescendants(dom, "Deal");
      if(l.isEmpty()) return;
  
      Node root = (Node)l.get(0);
  
      String msg = null;
  
      for (ClientMessage cm : messagesForBroker) {
        msg = cm.getMessageText();
        Element noteElement = dom.createElement("DealNotes");
        Element noteText = dom.createElement("dealNotesText");
        Element dealNotesCategory = dom.createElement("dealNotesCategory");
        Element dealNotesUser = dom.createElement("dealNotesUser");
  
        noteElement.appendChild(noteText);
        noteText.appendChild(dom.createTextNode(msg));
  
        noteElement.appendChild(dealNotesCategory);
        dealNotesCategory.appendChild(dom.createTextNode("Deal Ingestion"));
  
        noteElement.appendChild(dealNotesUser);
        dealNotesUser.appendChild(dom.createTextNode("SYSTEM"));
  
        root.appendChild(noteElement);
      }
   }
  
  /**
   * collect list of duplicate deals
   * @param pList
   * @param pDeal
   */
  protected void insertDeal(List pList, Deal pDeal) {
    writeLog("Method insertDeal entered.");
    if(pList.isEmpty()){
      pList.add(pDeal);
      return;
    }
    Iterator it = pList.iterator();
    int counter = 0;
    while(it.hasNext()){
      Deal lDeal = (Deal) it.next();
      if( lDeal.getDealId()  <  pDeal.getDealId()  ){
        writeLog("(1) Adding duplicate deal:" + pDeal.getDealId() + "(" + pDeal.getCopyId() +")");
        pList.add(counter, pDeal);
        writeLog("Method insertDeal finished (1).");
        return;
      }else if(lDeal.getDealId() == pDeal.getDealId()){
        writeLog("Method insertDeal finished (2).");
        return;
      }
      counter++;
    }
    writeLog("(2) Adding duplicate deal:" + pDeal.getDealId() + "(" + pDeal.getCopyId() +")");
    pList.add(pDeal);
    writeLog("Method insertDeal finished (3).");
  }
  
  /* #DG618 replaced
  protected String getNoNullString(String pStr) {
    if(pStr == null){
      return "";
    }
    return pStr;
  }*/
	/**
	 * Compares <code>str1</code> to <code>str2</code>, taking into account
	 * possible <code>null</code> values being passed in.
	 *
	 * @param str1 The first <code>String</code> to compare
	 * @param str2 The second <code>String</code> to compare
	 *
	 * @return whether the two strings were equal
	 */
	public static boolean isEqual(String str1, String str2)
	{
		return str1 == str2 ||
			str1 != null && str1.equalsIgnoreCase(str2);
	}
  
  
  /**
   * Message to be returned to broker is appended to the list.
   */
  protected void appendMessageForBroker(int id) {
    messagesForBroker.add(findMessage( srk, id, 0));
  }
  
  /**
   * Utility method called from several spots in this class.
   */
  protected String findBrokerName(Deal pDeal) throws Exception {
    SourceOfBusinessProfile sobProfile = pDeal.getSourceOfBusinessProfile();
    Contact sobContact = sobProfile.getContact();
    String retVal = sobContact.getContactFullName2();
    return retVal;
  }
  
  /**
   * Utility method called from several spots in this class.
   */
  protected String findSourceFirm(Deal pDeal) throws Exception {
    SourceFirmProfile sourceFirmProfile = pDeal.getSourceFirmProfile();
    String retVal = sourceFirmProfile.getSourceFirmName();
    return retVal;
  }
  
/* #DG600
    protected void preserveDealNotes(Collection pNotes, Deal pDeal) throws Exception {
    writeLog("Method preserveDealNotes entered.");
    Iterator it = pNotes.iterator();
  
    while( it.hasNext() ){
      DealNotes oneNote = (DealNotes)it.next();
      DealNotes newNote = new DealNotes(srk);
      newNote = newNote.create(new DealPK(pDeal.getDealId(), pDeal.getCopyId() ) )  ;
      newNote.setApplicationId( oneNote.getApplicationId() );
      newNote.setDealNotesCategoryId( oneNote.getDealNotesCategoryId() );
      newNote.setDealNotesUserId( oneNote.getDealNotesUserId() );
      newNote.setDealNotesText( oneNote.getDealNotesText() );
      newNote.setDealNotesDate(oneNote.getDealNotesDate());			// Tracker #2762 - Sasa
  
      newNote.ejbStore();
    }
    writeLog("Method preserveDealNotes finished.");
  }
*/  
  /**
   * Method returns true if two borrowers have the same social insurance number.
   */
  protected boolean isSINMatch(Borrower bOne, Borrower bTwo) {
    if(bOne.getSocialInsuranceNumber() == null){return false;}
    if(bTwo.getSocialInsuranceNumber() == null){return false;}
    if( bOne.getSocialInsuranceNumber().trim().equals( bTwo.getSocialInsuranceNumber().trim() )  ){
      return true;
    }
    return false;
  }
  
  /**
   * Method returns true when incoming deal is preApproval offered which has gone firm.
   * Going firm means that incoming deal has a Property address associated to it, and existing
   * deal is pre approval offerred.
   */
  protected boolean checkGoneFirm(Deal pDeal) throws Exception {
  
    // first we have to check whether the new deal has a special feature
    // id set to 1 (pre approval). If yes, do not check anything else, just
    // return false;
    if( pDeal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL ){
      return false;
    }
  
    //  then we check if Property exists for incoming deal
    Collection propColl = pDeal.getProperties();
    if(propColl.isEmpty() ){
      return false;
    }
    Iterator it = propColl.iterator();
    while( it.hasNext() ){
      Property currProp = (Property)it.next();
  
      String f1 = currProp.getPropertyStreetNumber();
      String f2 = currProp.getPropertyStreetName();
  
      // if Property address is not there - return "false"
      if( f1 == null || f2 == null || f1.trim().length()==0 || f2.trim().length()== 0)
      {
        return false;
      }
    }
    // otherwise, this is a preApproval gone firm
    return true;
  }
  
  private boolean isBorMatch(Borrower borOne, Borrower borTwo) {
    String oneBorrowerSIN = borOne.getSocialInsuranceNumber();
    String oneBorrowerFirstName = borOne.getBorrowerFirstName();
    String oneBorrowerMiddleInitial = borOne.getBorrowerMiddleInitial();
    String oneBorrowerLastName = borOne.getBorrowerLastName();
  
    String twoBorrowerSIN = borTwo.getSocialInsuranceNumber();
    String twoBorrowerFirstName = borTwo.getBorrowerFirstName();
    String twoBorrowerMiddleInitial = borTwo.getBorrowerMiddleInitial();
    String twoBorrowerLastName = borTwo.getBorrowerLastName();
  
    StringBuffer logBuf = new StringBuffer("Borrower ONE:(");
    logBuf.append("id = ").append(borOne.getBorrowerId()).append(":");
    logBuf.append("dealId = ").append(borOne.getDealId()).append(":");
    logBuf.append("copyId = ").append(borOne.getCopyId()).append(":");
    logBuf.append("SIN = ").append(oneBorrowerSIN).append(":");
    logBuf.append("FN = ").append(oneBorrowerFirstName).append(":");
    logBuf.append("MI = ").append(oneBorrowerMiddleInitial).append(":");
    logBuf.append("LN = ").append(oneBorrowerLastName).append(")");
    logBuf.append("Borrower TWO:(");
    logBuf.append("id = ").append(borTwo.getBorrowerId()).append(":");
    logBuf.append("dealId = ").append(borTwo.getDealId()).append(":");
    logBuf.append("copyId = ").append(borTwo.getCopyId()).append(":");
    logBuf.append("SIN = ").append(twoBorrowerSIN).append(":");
    logBuf.append("FN = ").append(twoBorrowerFirstName).append(":");
    logBuf.append("MI = ").append(twoBorrowerMiddleInitial).append(":");
    logBuf.append("LN = ").append(twoBorrowerLastName).append(")");
  
    writeLog( logBuf.toString() );
  
    if(oneBorrowerSIN != null && twoBorrowerSIN != null){
     	if(oneBorrowerSIN.equalsIgnoreCase(twoBorrowerSIN)){	//#DG618 
        return true;
      }
    }
  
    if( oneBorrowerFirstName == null && twoBorrowerFirstName == null &&
        oneBorrowerMiddleInitial == null && twoBorrowerMiddleInitial == null &&
        oneBorrowerLastName == null && twoBorrowerLastName == null)	{
      return false;
    }
  
    /*//#DG618 rewritten
    boolean firstNameMatch = false;;
    boolean middleNameMatch = false;
    boolean lastNameMatch = false;  
    if(getNoNullString(oneBorrowerFirstName).equals(getNoNullString(twoBorrowerFirstName))  ){	 
      firstNameMatch = true;
    }
    if(getNoNullString(oneBorrowerMiddleInitial).equals(getNoNullString(twoBorrowerMiddleInitial))  ){	 
      middleNameMatch = true;
    }
    if(getNoNullString(oneBorrowerLastName).equals(getNoNullString(twoBorrowerLastName))  ){	 
      lastNameMatch = true;
    }  
    if( firstNameMatch && middleNameMatch && lastNameMatch){
      return true;
    }*/
    return isEqual(oneBorrowerFirstName,twoBorrowerFirstName)
    	&& isEqual(oneBorrowerMiddleInitial,twoBorrowerMiddleInitial)
    	&& isEqual(oneBorrowerLastName,twoBorrowerLastName);	 
  }
  
  private boolean isPropMatch(Property propOne, Property propTwo) {
    String onePropertyStreetNumber = propOne.getPropertyStreetNumber();
    String onePropertyStreetName = propOne.getPropertyStreetName();
    String oneUnitNumber = propOne.getUnitNumber();
    String oneCity = propOne.getPropertyCity();
    int oneStreetTypeId = propOne.getStreetTypeId();
    int oneStreetDirectionId = propOne.getStreetDirectionId();
    
    String twoPropertyStreetNumber = propTwo.getPropertyStreetNumber();
    String twoPropertyStreetName = propTwo.getPropertyStreetName();
    String twoUnitNumber = propTwo.getUnitNumber();
    String twoCity = propTwo.getPropertyCity();
    int twoStreetTypeId = propTwo.getStreetTypeId();
    int twoStreetDirectionId = propTwo.getStreetDirectionId();
    
    if( (onePropertyStreetNumber == null || onePropertyStreetNumber.equals("")) &&
        ( onePropertyStreetName == null || onePropertyStreetName.equals("") ) &&
        ( oneUnitNumber == null || oneUnitNumber.equals("") ) &&
        ( oneCity == null || oneCity.equals("") ) &&
        ( oneStreetTypeId == 0) &&
        ( oneStreetDirectionId == 0 )
    ){
      return false;
    }
    if( ( twoPropertyStreetNumber == null || twoPropertyStreetNumber.equals("")) &&
        ( twoPropertyStreetName == null || twoPropertyStreetName.equals("") ) &&
        ( twoUnitNumber == null || twoUnitNumber.equals("") ) &&
        ( twoCity == null || twoCity.equals("") ) &&
        ( twoStreetTypeId == 0) &&
        ( twoStreetDirectionId == 0 )
    ){
      return false;
    }    
    
    /* 	#DG618 rewritten
    boolean streetNumberMatch ;
    boolean streetNameMatch ;
    boolean unitNumberMatch ;
    boolean cityMatch ;
    boolean streetTypeMatch ;
    boolean streetDirectionMatch ;  
    try{
      if(oneStreetDirectionId == twoStreetDirectionId){
        streetDirectionMatch = true;
      }else{
        streetDirectionMatch = false;
      }
    }catch(Exception exc){
      streetDirectionMatch = false;
    }  
    try{
      if(oneStreetTypeId == twoStreetTypeId){
        streetTypeMatch = true;
      }else{
        streetTypeMatch = false;
      }
    }catch(Exception exc){
      streetTypeMatch = false;
    }  
    if(getNoNullString(oneCity).equals(getNoNullString(twoCity))){
      cityMatch = true;
    }else{
      cityMatch = false;
    }
    if(getNoNullString(oneUnitNumber).equals(getNoNullString(twoUnitNumber))){
      unitNumberMatch = true;
    }else{
      unitNumberMatch = false;
    }
    if(getNoNullString(onePropertyStreetName).equals(getNoNullString(twoPropertyStreetName))){
      streetNameMatch = true;
    }else{
      streetNameMatch = false;
    }
    if(getNoNullString(onePropertyStreetNumber).equals(getNoNullString(twoPropertyStreetNumber))){
      streetNumberMatch = true;
    }else{
      streetNumberMatch = false;
    }  
    if( !streetNumberMatch || !streetNameMatch || !unitNumberMatch || !cityMatch || !streetTypeMatch || !streetDirectionMatch){
      return false;
    }
    return true;*/
    
    return oneStreetDirectionId == twoStreetDirectionId
    	&& oneStreetTypeId == twoStreetTypeId
    	&& isEqual(oneCity,twoCity) && isEqual(oneUnitNumber,twoUnitNumber)
    	&& isEqual(onePropertyStreetName,twoPropertyStreetName)
    	&& isEqual(onePropertyStreetNumber,twoPropertyStreetNumber);
  }

  protected boolean isBorrowerReallyDuplicate(Collection pBor, Deal pDeal) {
    writeLog("Method isBorrowerReallyDuplicate entered.");
    Collection dealBor;
    try{
      dealBor = pDeal.getBorrowers();
    }catch(Exception exc){
      writeLog("Method isBorrowerReallyDuplicate finished (1)- returns false.");
      return false;
    }
    if(dealBor == null){
      writeLog("Method isBorrowerReallyDuplicate finished (1 - a)- returns false.");
      return false;
    }
  
    Iterator dealBorIterator;
    Iterator it = pBor.iterator();
    while(it.hasNext()){
      Borrower oneBor = (Borrower)it.next();
      dealBorIterator = dealBor.iterator();
      while( dealBorIterator.hasNext() ){
        Borrower db = (Borrower)dealBorIterator.next();
        if(isBorMatch(oneBor,db)){
          writeLog("Method isBorrowerReallyDuplicate finished (2) - returns true.");
          return true;
        }
      }
    }
    writeLog("Method isBorrowerReallyDuplicate finished.");
    return false;
  }

  protected boolean isPropertyReallyDuplicate(Collection pProp, Deal pDeal) {
    writeLog("Method isPropertyReallyDuplicate entered.");
    Collection dealProp;
    try{
      dealProp = pDeal.getProperties();
    }catch(Exception exc){
      writeLog("Method isPropertyReallyDuplicate finished (1)- returns false.");
      return false;
    }
    if(dealProp == null){
      writeLog("Method isPropertyReallyDuplicate finished (1 - a)- returns false.");
      return false;
    }
    Iterator dealPropIterator;
    Iterator it = pProp.iterator();
    while(it.hasNext()){
      Property oneProp = (Property)it.next();
      dealPropIterator = dealProp.iterator();
      while( dealPropIterator.hasNext() ){
        Property dp = (Property)dealPropIterator.next();
        if(isPropMatch(oneProp,dp)){
          writeLog("Method isPropertyReallyDuplicate finished (2) - returns true.");
          return true;
        }
      }
    }
    writeLog("Method isPropertyReallyDuplicate finished (2) - returns false.");
    return false;
  }
  
  /**
   * Method returns true if two borrowers have the same name.
   */
  // Catherine commented this out, the following code have not been used
/*  
  private boolean isNameMatch(Borrower bOne, Borrower bTwo) {
    String fNameOne, lNameOne, fNameTwo, lNameTwo;
    String nameOne, nameTwo;
    fNameOne = bOne.getBorrowerFirstName();
    lNameOne = bOne.getBorrowerLastName();
  
    fNameTwo = bTwo.getBorrowerFirstName();
    lNameTwo = bTwo.getBorrowerLastName();
  
    nameOne = "";
    nameTwo = "";
    if(fNameOne != null){
      nameOne += fNameOne.trim();
    }
    if(lNameOne != null){
      nameOne += lNameOne.trim();
    }
    if(fNameTwo != null){
      nameTwo += fNameTwo.trim();
    }
    if(lNameTwo != null){
      nameTwo += lNameTwo;
    }
  
    if(nameOne.equals("") || nameTwo.equals("")){
      return false;
    }
  
    if(nameOne.equals(nameTwo)){
      return true;
    }
    return false;
  }
*/  
  /**
   * Utility method called findBorrowerId.
   */
  protected boolean isBorrowerMatch(Borrower oldBor, Borrower newBor) {
    if(isSINMatch(oldBor,newBor)){return true;}
  
    if( oldBor.getBorrowerFirstName() == null){return false;}
    if( oldBor.getBorrowerLastName() == null){return false;}
    if( newBor.getBorrowerFirstName() == null){return false;}
    if( newBor.getBorrowerLastName() == null){return false;}
  
    if( !( oldBor.getBorrowerFirstName().trim().equalsIgnoreCase(newBor.getBorrowerFirstName().trim()) )){
      return false;
    }
    if( !( oldBor.getBorrowerLastName().trim().equalsIgnoreCase(newBor.getBorrowerLastName().trim()) )){
      return false;
    }
    return true;
  }
  
  /**
   * Utility method called by findComponentId.
   */
  protected boolean isComponentMatch(Component oldCom, Component newComp) {
   

    if( !( oldCom.getComponentTypeId()== newComp.getComponentTypeId())){
      return false;
    }
    if( !( oldCom.getMtgProdId()== newComp.getMtgProdId())){
      return false;
    }
    return true;
  }
  
  /**
   * Utility method called by findPropertyId.
   */
  protected boolean isPropertyMatch(Property oldProp, Property newProp) {
    if( oldProp.getPropertyStreetNumber() == null){return false;}
    if( oldProp.getPropertyStreetName() == null){return false;}
    if( newProp.getPropertyStreetNumber() == null){return false;}
    if( newProp.getPropertyStreetName() == null){return false;}
  
    if( !( oldProp.getPropertyStreetNumber().trim().equalsIgnoreCase(newProp.getPropertyStreetNumber().trim()) )){
      return false;
    }
    if( !( oldProp.getPropertyStreetName().trim().equalsIgnoreCase(newProp.getPropertyStreetName().trim()) )){
      return false;
    }
    return true;
  }

  /**
  * Method looks for deals that have duplicate borrowers as incoming deal.
   * 
   * @throws Exception
   */
  protected void processBorrowers() throws Exception {
    writeLog("Process Borrowers Entered");
    Collection borrowers = deal.getBorrowers();
    Iterator it = borrowers.iterator();
    List found = new ArrayList();		
    List borrowerMatchList = new ArrayList();
    while(it.hasNext()){
      Borrower oneBor = (Borrower)it.next();
      found.addAll(this.matchBorrower(oneBor,null));
    }
    it = found.iterator();
    while(it.hasNext()){
      Deal d = new Deal(srk,null);
      d = d.findByRecommendedScenario(new DealPK(((Integer)it.next()).intValue() ,-1),true  );
      if(isBorrowerReallyDuplicate(borrowers,d) == false){
        continue;
      }
      this.insertDeal(borrowerMatchList,d);
    }
    createNoteForBorMatches(borrowerMatchList);
    writeLog("Process Borrowers Finished");
  }
  
  /**
  * Method looks for deals that have duplicate properties as incoming deal.
  */
  protected void processProperties() throws Exception {
    writeLog("Process Properties Entered");
    //Start of FXP18718
    //Process properties only when the current deal is NOT preapproval.
    if(deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL)
    	return;
    //End of FXP18718
    Collection properties = deal.getProperties();
    Iterator it = properties.iterator();
    List<Integer> found = new ArrayList<Integer>();
    List<Deal> propertyMatchesList = new ArrayList<Deal>();
    while( it.hasNext() ){
      Property oneProp = (Property)it.next();
      found.addAll( this.matchProperty(oneProp,null) );
    }
  
    it = found.iterator();
    while( it.hasNext() ){
      Deal d = new Deal(srk,null);
      d = d.findByRecommendedScenario(new DealPK( ((Integer)it.next()).intValue() , -1), true);
      if(isPropertyReallyDuplicate(properties,d) == false){
        continue;
      }
      if( d.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL ){
          this.insertDeal(propertyMatchesList,d);
      }
    }
    createNoteForPropMatches(propertyMatchesList);
    writeLog("Process Properties Finished");
  }
  
  /**
   * Utility method called byfindContextId.
   */
  protected int findBorrowerId(DocumentTracking pTrack, Collection oldBorrowers, Collection newBorrowers) {
    int retVal = -1;
    Iterator oldIterator = oldBorrowers.iterator();
    while( oldIterator.hasNext() ){
      Borrower bor = (Borrower)oldIterator.next();
      if( bor.getBorrowerId() != pTrack.getTagContextId() ){ continue; }
      Iterator newIterator = newBorrowers.iterator();
      while( newIterator.hasNext() ){
        Borrower nBor = (Borrower) newIterator.next();
        if(isBorrowerMatch(bor,nBor)){
          return nBor.getBorrowerId();
        }
      }
    }
    return retVal;
  }
  /*********
   * Component Conditions
   */
  /**
   * Utility method called by findContextId.
   */
  protected int findComponentId(DocumentTracking pTrack, Collection oldComponents, Collection newComponents) {
    int retVal = -1;
    Iterator oldIterator = oldComponents.iterator();
    while( oldIterator.hasNext() ){
      Component comp = (Component)oldIterator.next();
      if( comp.getComponentId() != pTrack.getTagContextId() ){ continue; }
      Iterator newIterator = newComponents.iterator();
      while( newIterator.hasNext() ){
          Component nCom = (Component) newIterator.next();
        if(isComponentMatch(comp,nCom)){
          return nCom.getComponentId();
        }
      }
    }
    return retVal;
  }
  
  /**
   * Utility method called by findContextId.
   */
  protected int findPropertyId(DocumentTracking pTrack, Collection oldProperties, Collection newProperties) throws Exception {
    int retVal = -1;
    Iterator oldIterator = oldProperties.iterator();
    while( oldIterator.hasNext() ){
      Property prop = (Property)oldIterator.next();
      if( prop.getPropertyId() != pTrack.getTagContextId() ){ continue; }
      Iterator newIterator = newProperties.iterator();
      while( newIterator.hasNext() ){
        Property nProp = (Property) newIterator.next();
        if(isPropertyMatch(prop, nProp)){
          return nProp.getPropertyId();
        }
      }
    }
    return retVal;
  }

  /**
   *  Method creates deal note record for each deal which has the same borrowers
   *  as incoming deal.
   *  @param pDealList contains all deals with duplicate borrowers.
   */
  protected void createNoteForBorMatches(Collection pDealList) throws Exception {
  	//#DG600 rewritten
    DealNotes note = new DealNotes(srk);

    /*noteText = new StringBuffer("Deal ").append(deal.getDealId()).append(" has duplicate borrower(s) ");
    noteText.append("submitted by ").append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));
    noteText.append(".");
    note.create( (DealPK)lDeal.getPk(), ((DealPK)lDeal.getPk()).getId(), Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, noteText.toString() );*/
    final Formattable notaOri = BXResources.newFormata(DUP_CHEK_DEAL_HAS_DUP, deal.getDealId(), 
  			findBrokerName(deal), findSourceFirm(deal));		//#DG636
    Formattable nota;

    for(Deal lDeal: (Collection<Deal>)pDealList)  {
      /*noteText = new StringBuffer("Deal ").append(lDeal.getDealId()).append(" has duplicate borrower(s) ");
      noteText.append("submitted by ").append(findBrokerName(lDeal)).append(" from ").append(findSourceFirm(lDeal));
      noteText.append(".");
      note.create( (DealPK)deal.getPk(), ((DealPK)deal.getPk()).getId(), Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, noteText.toString()  );*/
    	nota = BXResources.newFormata(DUP_CHEK_DEAL_HAS_DUP, lDeal.getDealId(), 
    			findBrokerName(lDeal), findSourceFirm(lDeal));
      note.create(deal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
  
      note.create(lDeal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, notaOri);		//#DG636
    }
  }

  /**
  *  Method creates deal note record for each deal which has the same properties
  *  as incoming deal.
  *  @param pDealList contains all deals with duplicate properties.
  */
  protected void createNoteForPropMatches(Collection pDealList) throws Exception {
    writeLog("Method createNoteForPropMatches entered");
  	//#DG600 rewritten
    DealNotes note = new DealNotes(srk);
    /*noteText = new StringBuffer("Deal ").append(deal.getDealId()).append(" has duplicate property ");
    noteText.append(" submitted by ").append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));
    noteText.append(".");
    note.create( (DealPK)lDeal.getPk(), ((DealPK)lDeal.getPk()).getId(), Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, noteText.toString() );*/
    final Formattable notaOri = BXResources.newFormata(DUP_CHEK_DEAL_HAS_DUP_PROP, deal.getDealId(), 
  			findBrokerName(deal), findSourceFirm(deal));		//#DG636
    Formattable nota;

    for(Deal lDeal: (Collection<Deal>)pDealList)  {
  
      writeLog("Writing note to deal : " + deal.getDealId() + "(" + deal.getCopyId() + ")");
      /*noteText = new StringBuffer("Deal ").append(lDeal.getDealId()).append(" has duplicate property ");
      noteText.append(" submitted by ").append(findBrokerName(lDeal)).append(" from ").append(findSourceFirm(lDeal));
      noteText.append(".");
      note.create( (DealPK)deal.getPk(), ((DealPK)deal.getPk()).getId(), Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, noteText.toString()  );*/
    	nota = BXResources.newFormata(DUP_CHEK_DEAL_HAS_DUP_PROP, lDeal.getDealId(), 
    			findBrokerName(lDeal), findSourceFirm(lDeal));
      note.create(deal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
  
      writeLog("Writing note to deal : " + lDeal.getDealId() + "(" + lDeal.getCopyId() + ")");
      note.create(lDeal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, notaOri);		//#DG636	
    }
    writeLog("Method createNoteForPropMatches finished");
  }

  protected Set<Integer> matchProperty(Property current, Date minDate) {
    writeLog("Method matchProperty Entered.");
    Set<Integer> matchingDeals = new HashSet<Integer>();
  
    if(current == null) return matchingDeals;
  
    String propertyStreetNumber = current.getPropertyStreetNumber();
    String propertyStreetName = current.getPropertyStreetName();
    String unitNumber = current.getUnitNumber();
    String city = current.getPropertyCity();
    int streetTypeId = current.getStreetTypeId();
    int streetDirectionId = current.getStreetDirectionId();
    int provinceId = current.getProvinceId();
  
    if( (propertyStreetNumber == null || propertyStreetNumber .equals("")) &&
        ( propertyStreetName == null || propertyStreetName .equals("") ) &&
        ( unitNumber == null || unitNumber .equals("") ) &&
        ( city == null || city .equals("") ) &&
        ( streetTypeId == 0 )  &&
        (streetDirectionId == 0)
    ){
      writeLog("Method matchProperty finisehd (1).");
      return matchingDeals;
    }
    writeLog("Control Point 1.");
    List result = null;
    writeLog("Control Point 2.");
    try
    {
      Property p = new Property(srk,null);
      result = (List)p.findByGenXDupeCheck(propertyStreetNumber,propertyStreetName,streetTypeId,
                             streetDirectionId,unitNumber,city,provinceId,minDate);
    }
    catch(Exception fie)
    {
      writeLog("Method matchProperty finisehd (2).");
      return null;
    }
    writeLog("Control Point 3.");
    //if(result.isEmpty()) return null;
    if(result.isEmpty()){
    	matchingDeals.clear();
      return matchingDeals;
    }
    writeLog("Control Point 3 a.");
    ListIterator li = result.listIterator();
    Property pr = null;
    writeLog("Control Point 4.");
    while(li.hasNext())
    {
      writeLog("Control Point 4-1.");
      pr = (Property)li.next();
      writeLog("Property tested : " + pr.getPropertyId() + "(" + pr.getCopyId() +")");
      int dealId = pr.getDealId();
  
      if(dealId != current.getDealId()){
        writeLog("Adding deal : " + current.getDealId() + "(" + current.getCopyId() +")");
        matchingDeals.add(new Integer(dealId));
      }
      writeLog("Control Point 4-2.");
    }
    writeLog("Control Point 5.");
    writeLog("Method matchProperty finisehd (3).");
    return matchingDeals;
  }

  //#DG600 added typed set
  /**
   * 
   * @param current
   * @param minDate
   * @return
   * @throws Exception
   */
  protected Set<Integer> matchBorrower(Borrower current, Date minDate) throws Exception {
      Set<Integer> matchingDeals = new HashSet<Integer>();
      List<Borrower> result = new ArrayList<Borrower>();
      String sin = current.getSocialInsuranceNumber();
  
      if(sin != null && !sin.trim().equals(""))
      {
         try
         {
            Borrower b = new Borrower(srk,null);
            result.addAll(b.findByGenXDupeCheckSin(sin,minDate));
         }
         catch(Exception e)
         {
           ;  // go on to demographic check
         }
      }
  
      try
      {
        String borrowerFirstName = current.getBorrowerFirstName();
        String borrowerMiddleInitial = current.getBorrowerMiddleInitial();
        String borrowerLastName = current.getBorrowerLastName();
        Date borrowerBirthDate = current.getBorrowerBirthDate();
  
  
        Borrower b = new Borrower(srk,null);
  //logger.debug("BILLY ==> before check CheckCriteria : borrowerName = " + borrowerFirstName + " " +
  //          borrowerFirstName + " " + borrowerLastName + " borrowerBirthDate = " + borrowerBirthDate);
        result.addAll( b.findByGenXDupeCheck(borrowerFirstName,borrowerMiddleInitial,
                                              borrowerLastName,borrowerBirthDate, minDate));
      }
      catch(FinderException fie)
      {
        return matchingDeals;
      }
      catch(RemoteException re)
      {
        return matchingDeals;
      }
  
  
      ListIterator li = result.listIterator();
      Borrower br = null;
  
      while(li.hasNext())
      {
        br = (Borrower)li.next();
        int dealId = br.getDealId();
  
        if(dealId != current.getDealId())
         matchingDeals.add(dealId);
      }
  
      return matchingDeals;
    }

  /**
   * Utility method called by performCreateXRefDeals.
   */
  protected Deal findDealToCompare(MasterDeal md, Deal ed) {
    Deal retDeal;
    List dealCopies = (List)md.getCopyIdsSorted();
  
    if(dealCopies.isEmpty() ){
      return null;
    }
    if(ed.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL ){
      if(ed.getCopyId() == ((Integer)dealCopies.get(0)).intValue() ){
        return null;
      }
      try{
        retDeal = new Deal( srk,null,ed.getDealId(), ((Integer)dealCopies.get(0)).intValue() );
        return retDeal;
      }catch(Exception exc){
        return null;
      }
    }else{
      Iterator it = dealCopies.iterator();
      while(it.hasNext()){
        int lCopyId = ((Integer)it.next()).intValue();
        if( lCopyId >= ed.getCopyId() ){
          return null;
        }
        try{
          Deal compDeal = new Deal(srk,null,ed.getDealId(),lCopyId);
          if(compDeal.getSpecialFeatureId() !=  Mc.SPECIAL_FEATURE_PRE_APPROVAL ){
            return compDeal;
          }
        }catch(Exception exc1){
          return null;
        }
      }
    }
    return null;
  }

  /**
   * This method determines which one deal ( from the list of active deals) should
   * be used as existing deal.
   * @param existingList contains the list of existing deal that matched incoming one.
   */
  protected Deal determineActiveDeal(List existingList) {
    writeLog("Method DetermineActiveDeal entered.");
    Iterator it = existingList.iterator();
    Deal lDeal = null;
    Deal selectedDeal = null;
    Deal maxDeal = null;
    int counter = 0;
    while( it.hasNext() ){
      lDeal = (Deal) it.next();
      // ALERT: ZIVKO: confirm it with dave is the spot correct.
      // we do not want to consider deal with denial reason 18
      // change required by David Krick 2002, Sep 11
      if(lDeal.getDenialReasonId() == Mc.DENIAL_REASON_SOURCE_RESUBMISSION){
        continue;
      }
      if( lDeal.getStatusCategoryId() < 3  ){
      //if( lDeal.getStatusId() < 20 ){
        if(selectedDeal == null){
          selectedDeal = lDeal;
        }else{
          //if(lDeal.getCopyId() > selectedDeal.getCopyId()){
          if(lDeal.getDealId() > selectedDeal.getDealId()){
            selectedDeal = lDeal;
          }
        }
        counter++;
      }else{
        if(maxDeal == null){
          maxDeal = lDeal;
        }else{
          if(lDeal.getDealId() > maxDeal.getDealId()){
            maxDeal = lDeal;
          }
        }
      }
    }
    if(counter == 1){
      return selectedDeal;
    }else if(counter == 0){
      return maxDeal;
    }else{
      msgBuf = new StringBuffer("Serious error occurred here. ");
      msgBuf.append(counter).append(" active deals found.");
      writeLog(msgBuf.toString());
      // ZIVKO:ALERT: send warning e-mail here
      return selectedDeal;
    }
  }

  /**
   * Utility method called from createWithExisting.
   */
  protected void preserveDocumentTracking(Collection tracks, Deal newDeal, DealPK pOldPK, boolean bPreApp) throws Exception {
    msgBuf = new StringBuffer("Method preserve documentTracking entered.");
    msgBuf.append(" New Deal Id is: ").append( newDeal.getDealId() ).append(" and copy id is: ").append( newDeal.getCopyId() );
    writeLog( msgBuf.toString() );
    Iterator it = tracks.iterator();
    while( it.hasNext() ){
      DocumentTracking oneTrack = (DocumentTracking)it.next();
  
      msgBuf = new StringBuffer("Document tracking id is: ");
      msgBuf.append(oneTrack.getDocumentTrackingId()).append(" Condition id is: ").append( oneTrack.getConditionId() );
      msgBuf.append(" Status is : ").append(oneTrack.getDocumentStatusId() );
      writeLog(msgBuf.toString());
      if(oneTrack.getConditionId() == 0){
        // in this case go to the straight copy of document tracking record
        copyDocTracking(oneTrack,newDeal);
      } else if(oneTrack.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED ) {
        // in this case go to the straight copy of document tracking record
        copyDocTracking(oneTrack,newDeal);
      } else if(oneTrack.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM ){
        // in this case go to the straight copy of document tracking record
        copyDocTracking(oneTrack,newDeal);
      } else {
        int lStat = oneTrack.getDocumentStatusId();
        switch(lStat){
          case Mc.DOC_STATUS_RECEIVED :{
            saveDocTracking(oneTrack,newDeal,pOldPK, bPreApp);
            break;
          }
          case Mc.DOC_STATUS_APPROVED :{
            saveDocTracking(oneTrack,newDeal,pOldPK, bPreApp);
            break;
          }
          case Mc.DOC_STATUS_WAIVED :{
            saveDocTracking(oneTrack,newDeal,pOldPK, bPreApp);
            break;
          }
          case Mc.DOC_STATUS_CAUTION :{
            saveDocTracking(oneTrack,newDeal,pOldPK, bPreApp);
            break;
          }
          //====================================================================
          // the following has been added based on the request from David Krick
          // on Aug 07 2002
          //====================================================================
          case Mc.DOC_STATUS_INSUFFICIENT:{
            saveDocTracking(oneTrack,newDeal,pOldPK, bPreApp);
            break;
          }
          //====================================================================
          // the following has been added based on the request from David Krick
          // on Aug 07 2002
          //====================================================================
          case Mc.DOC_STATUS_WAIVE_REQUESTED:{
            saveDocTracking(oneTrack,newDeal,pOldPK, bPreApp);
            break;
          }
          //insufficient
          //waive requested
        } // end switch
      } //end if
    }// end while
    writeLog(" Method preserveDocumentTracking exited.");
  }

  /**
   * Finding context Id is important for preserving document tracking records.
   */
  protected int findContextId(DocumentTracking pTrack, Deal pDeal, DealPK pOldPk, boolean bPreApp) throws Exception {
    //==========================================================================
    // if tagContextId is -1 do not proceed, just return -1 and leave
    // -1 in tagContextId field means that we deal with all document tracking
    // record so we can not enter comparison process.
    //==========================================================================
    if(pTrack.getTagContextId() == -1){return -1;}
    int retVal = -1;
    int lClassId = pTrack.getTagContextClassId();
    Deal lDeal = new Deal(srk,null,pOldPk.getId(),pOldPk.getCopyId());
  
    switch(lClassId){
      case ClassId.BORROWER:{
        Collection oldBorrowers = lDeal.getBorrowers();
        Collection newBorrowers = pDeal.getBorrowers();
        retVal =  findBorrowerId(pTrack,oldBorrowers,newBorrowers);
        break;
      }
      case ClassId.PROPERTY:{
        Collection oldProperties = lDeal.getProperties();
        Collection newProperties = pDeal.getProperties();
        retVal = findPropertyId(pTrack,oldProperties, newProperties);
        break;
      }
      case ClassId.COMPONENT:{
          Collection oldComponents = lDeal.getComponents();
          Collection newComponents = pDeal.getComponents();
          retVal = findComponentId(pTrack,oldComponents, newComponents);
          break;
        }
      case ClassId.DEAL:{
        retVal = (bPreApp) ? pTrack.getTagContextId() : pDeal.getDealId();
        break;
      }
    }
    return retVal;
  }

  /**
   * Method saves document tracking record with new data, so the conditions
   * associated to a deal will be preserved.
   */
  protected void saveDocTracking(DocumentTracking pTrack, Deal pDeal, DealPK pOldPK, boolean bPreApp) throws Exception {
    writeLog(" Method saveDocTracking entered.");
    DocumentTracking targetTracking = new DocumentTracking(srk);
    Condition lCondition = new Condition( srk,pTrack.getConditionId() );
  
    int lTagCtxId = findContextId(pTrack,pDeal, pOldPK, bPreApp);
    if(lTagCtxId == -1){
      return;   // if we can not find context, go out, do not preserve document tracking record.
    }
  
    //==========================================================================
    // this code for changing status of new document tracking record is based on
    // request by Dave Krick on August 9, 2002.
    //==========================================================================
    int lStatus;
    if( pTrack.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED ){
      if( pTrack.getDocumentStatusId() == Mc.DOC_STATUS_APPROVED ){
        lStatus = Mc.DOC_STATUS_RECEIVED;
      }else{
        lStatus = pTrack.getDocumentStatusId();
      }
    }else{
      lStatus = pTrack.getDocumentStatusId();
    }
    targetTracking = targetTracking.createWithNoVerbiage(lCondition, pDeal.getDealId(), pDeal.getCopyId(), lStatus, pTrack.getDocumentTrackingSourceId() );
    //targetTracking = targetTracking.createWithNoVerbiage(lCondition, pDeal.getDealId(), pDeal.getCopyId(), pTrack.getDocumentStatusId(), pTrack.getDocumentTrackingSourceId() );
    targetTracking.setDocumentRequestDate( pTrack.getDocumentRequestDate() );
    targetTracking.setDocumentDueDate( pTrack.getDocumentDueDate() );
    targetTracking.setDocumentResponsibilityId( pTrack.getDocumentResponsibilityId() );
    targetTracking.setDocumentResponsibilityRoleId( pTrack.getDocumentResponsibilityRoleId() );
    targetTracking.setApplicationId( pTrack.getApplicationId() );
    targetTracking.setDStatusChangeDate( pTrack.getDStatusChangeDate() );
  
    targetTracking.setTagContextClassId(pTrack.getTagContextClassId());
    targetTracking.setTagContextId(lTagCtxId);
  
    targetTracking.appendVerbiage(pTrack);
  
    targetTracking.ejbStore();
    writeLog(" Method saveDocTracking exited.");
  }

  /**
  * !!! VERY IMPORTANT !!!
  * Utility method for all NON preapproval situations that have gone firm.
  * Note: for 4.3GR: this part doesn't need to be changed since it's NON pre-approval
  */
  protected void preserveNonPreAppFirmUp() throws Exception {
    DealPK oldPK = new DealPK( existingDeal.getDealId(), existingDeal.getCopyId() );
    preserveDocumentTracking(existingDeal.getDocumentTracks(), deal, oldPK, false);
  }

  /**
  * Utility method used for collapsed or denied deals.
  */
  protected void compareDeals(Deal pExDeal, Deal pIncomingDeal) throws Exception {
    DealCompare dealCompare = new DealCompare(pExDeal,pIncomingDeal,srk);
    dealCompare.compareAll();
    
    doSpecialCompare(dealCompare);
    
    if(dealCompare.getTotalDifferences() > 0){
      dealCompare.writeNoteToDeal(null, pIncomingDeal);		//#DG600
    }
  }

  /**
   * Straight copy of document tracking record. Used for custom conditions.
   * @param pTrack document tracking record from existing deal.
   * @param pDeal new deal.
   */
  protected void copyDocTracking(DocumentTracking pTrack, Deal pDeal) throws Exception {
    writeLog(" Method saveDocTracking entered.");
    DocumentTracking targetTracking = new DocumentTracking(srk);
    Condition lCondition = new Condition( srk,pTrack.getConditionId() );
  
    targetTracking = targetTracking.createWithNoVerbiage(lCondition, pDeal.getDealId(), pDeal.getCopyId(), pTrack.getDocumentStatusId(), pTrack.getDocumentTrackingSourceId() );
    targetTracking.setDocumentRequestDate( pTrack.getDocumentRequestDate() );
    targetTracking.setDocumentDueDate( pTrack.getDocumentDueDate() );
    targetTracking.setDocumentResponsibilityId( pTrack.getDocumentResponsibilityId() );
    targetTracking.setDocumentResponsibilityRoleId( pTrack.getDocumentResponsibilityRoleId() );
    targetTracking.setApplicationId( pTrack.getApplicationId() );
    targetTracking.setDStatusChangeDate( pTrack.getDStatusChangeDate() );
  
    targetTracking.appendVerbiage(pTrack);
  
    targetTracking.ejbStore();
    writeLog(" Method copyDocTracking exited.");
  }

  /**
  * Utility method used for collapsed or denied deals.
  */
  protected void createXRef() throws Exception {
      writeLog("Cross referencing collapsed/denied deal. (entered)");
      if(deal == null){ return; }
      if(existingDeal == null){ return; }
  
      existingDeal.setReferenceDealNumber("" + deal.getDealId() );
      existingDeal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_RESUBMITTED_AS_DEAL);
  
      deal.setReferenceDealNumber("" + existingDeal.getDealId() );
      deal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_ORIGINAL_DEAL_RESUBMIT);
  
      // for 4.3GR START <- Venkata - CR53 - CIBC - FXP25031 - re-submission sent to the underwriter that originally declined the deal - Start 
      boolean isReassignOgirinalUser  = PropertiesCache.getInstance( ).getProperty(deal.getInstitutionProfileId(),
    		  "ingestion.route.declined.resubmission.to.original.uw" , "N" ).equalsIgnoreCase( "Y" );
      boolean isLOBMatchedOnly = PropertiesCache.getInstance( ).getProperty(deal.getInstitutionProfileId(),
    		  "ingestion.route.declined.resubmission.to.original.uw.lob" , "Y" ).equalsIgnoreCase( "Y" );
      if(isReassignOgirinalUser && 
      	existingDeal.getStatusId() == Mc.DEAL_DENIED && 
      	(!isLOBMatchedOnly || deal.getLineOfBusinessId() == existingDeal.getLineOfBusinessId())) {
      	writeLog("Existing Deal - " + existingDeal.getDealId() + " has a Denied statusId and Resubmitted Deal (" 
      			+deal.getDealId()+") and Existing Deal have same LOB " + deal.getLineOfBusinessId() , this);
      	
      	boolean isExistingDealICReject = existingDeal.getDenialReasonId() == Mc.DENIAL_REASON_IC_REJECT; //19 = IC-Reject *
      	writeLog("is ExistingDeal - " + existingDeal.getDealId() + " ICReject ?  " + isExistingDealICReject , this);
      	
      	DealUserProfiles dealUserProfiles = new DealUserProfiles(existingDeal, srk);
      	if(!isExistingDealICReject && (dealUserProfiles.getUnderwriterProfile().getProfileStatusId()) == Sc.PROFILE_STATUS_ACTIVE ) {
  			writeLog("Assigning the Underwriter (" + existingDeal.getUnderwriterUserId() + ") of the existing Deal - " + existingDeal.getDealId() , this);
      		deal.setUnderwriterUserId(existingDeal.getUnderwriterUserId());
        }
      }
     // for 4.3GR START <- Venkata - CR53 - CIBC - FXP25031 - End


			Formattable nota;	//#DG600 
      // note for existing deal
      DealNotes note = new DealNotes(srk);
      
      final String brokerName = findBrokerName(deal);
			final String sourceFirm = findSourceFirm(deal);
			/*StringBuffer noteText = new StringBuffer("Deal resubmitted as Deal ");
      noteText.append( deal.getDealId() ).append(" by ");
      noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal)).append(".");
      note.create((DealPK)existingDeal.getPk(),((DealPK)existingDeal.getPk()).getId(),Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,noteText.toString());*/
    	nota = BXResources.newFormata(DUP_CHEK_DEAL_RESUBMTD, deal.getDealId(), brokerName, sourceFirm);
      note.create(existingDeal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
  
      // note for incoming deal
      /*noteText = new StringBuffer("(previously denied/collapsed) as Deal #");
      noteText.append(existingDeal.getDealId()).append(" resubmitted by ");
      noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal)).append(".");
      note.create((DealPK)deal.getPk(),((DealPK)deal.getPk()).getId(),Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,noteText.toString());*/
    	nota = BXResources.newFormata(DUP_CHEK_DEAL_PREV_DENIED, existingDeal.getDealId(), brokerName, sourceFirm);		//#DG640   
      note.create(deal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636	
  
      deal.ejbStore();
      existingDeal.ejbStore();
      writeLog("Cross referencing collapsed/denied deal. (exitied)");
   }

  /**
   * Which action are we going to take depends on the supplied matrix. Matrix
   * has two lines(for now). One is when existing deal is preApp and the
   * second one is used when existing deal is not preApp.
   */
  protected void applyMatrix() throws Exception {
    writeLog("Apply matrix method entered.");
    boolean preApp = existingDeal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL;
    writeLog("the Existing deal is a pre-approval? "+preApp);
    if(preApp)
        applyMatrixForPreApp();
    else
        applyMatrixForNoPreApp();
    writeLog("Apply matrix method exited.");
  }

  /**
   * Method rejects incoming deal.
   */
  protected void performReject() throws DupCheckActionException, Exception {
    msgBuf = new StringBuffer("Method performReject entered.");
    msgBuf.append(" ExistingDeal is: ").append(existingDeal.getDealId()).append(" Copy Id:").append(existingDeal.getCopyId());
    writeLog(msgBuf.toString());
  
    deal.setStatusId(Mc.DEAL_DENIED);
    deal.setDenialReasonId( Mc.DENIAL_REASON_SOURCE_RESUBMISSION );
    // Made based on request by Dave Krick on Sep 27, 2002
    // ZIVKO:ALERT: find constants for these two zeroes
    //deal.setAdministratorId(0);
    deal.setUnderwriterUserId( existingDeal.getUnderwriterUserId() );
  
    try{
      deal.ejbStore();
    } catch(com.basis100.entity.RemoteException rExc){
      // ZIVKO:ALERT:handle this exception
    }
    //==========================================================================
    // create message for the broker
    // ZIVKO:ALERT: find the real id code for message
    //appendMessageForBroker(111,0);
  
    // note for existing deal
    DealNotes note = new DealNotes(srk);
    final String brokName = findBrokerName(deal);
		final String sourceName = findSourceFirm(deal);
		/*StringBuffer noteText = new StringBuffer("");
    noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));
    noteText.append(" attempted to update the deal but could not since the deal is ");
    noteText.append( PicklistData.getDescription("STATUS", existingDeal.getStatusId() ) );
    note.create((DealPK)existingDeal.getPk(),((DealPK)existingDeal.getPk()).getId(),Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,noteText.toString());*/
  	Formattable nota = BXResources.newFormata(DUP_CHEK_ATTEMPTEDRJCT, brokName, sourceName,
         BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_STATUS, existingDeal.getStatusId() ));
    note.create(existingDeal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
    
    // note for incoming deal
    /*noteText = new StringBuffer("");
    noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));
    noteText.append(" attempted to update the deal ").append( existingDeal.getDealId() );
    noteText.append(" but could not since the deal is ");
    noteText.append( PicklistData.getDescription("STATUS", existingDeal.getStatusId() ) );
    note.create((DealPK)deal.getPk(),((DealPK)deal.getPk()).getId(),Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,noteText.toString());*/
  	nota = BXResources.newFormata(DUP_CHEK_ATTEMPTEDRJCT_X, brokName, sourceName,
  			existingDeal.getDealId(),
            BXResources.newPkList(srk.getExpressState().getDealInstitutionId(), PKL_STATUS, existingDeal.getStatusId() ) );
    note.create(deal, Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636	
    
    //  Added by request of David Krick, Aug 12, 2002
    preserveNonPreAppFirmUp();
  
    storedDealPK = new DealPK(existingDeal.getDealId(),existingDeal.getCopyId() );
    notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2;
  
    writeLog("Method performReject entered.");
  }

  /**
     * <p>
     * Description : Creates new copy and appends it to existing deal. This
     * method is called only when a preApproval has gone firm (subject Property
     * must exist) and exisiting deal status is Pre-Approval-Offered
     * </p>
     * @version 1.0 Initial version
     * @version 1.1 XS_1.1 08-Aug-2008 Added method call to the
     *          processQualifyDetails(....)
     */
  protected void performCreateWithExisting() throws DupCheckActionException, Exception {
    msgBuf = new StringBuffer("Method performCreateWithExisting entered.");
    msgBuf.append(" ExistingDeal is: ").append(existingDeal.getDealId()).append(" Copy Id:").append(existingDeal.getCopyId());
    writeLog(msgBuf.toString());
  
    // preserve required fields.
    int lUnderwriterUserId = existingDeal.getUnderwriterUserId();
    int lAdministratorId = existingDeal.getAdministratorId();
    int lBranchProfileId = existingDeal.getBranchProfileId();
    int lFunderProfileId = existingDeal.getFunderProfileId();
  
    msgBuf = new StringBuffer(" Underwriter User Id :");
    msgBuf.append(lUnderwriterUserId);
    msgBuf.append(" AdministratorId User Id :").append(lAdministratorId);
    msgBuf.append(" Branch Profile Id :").append(lBranchProfileId);
    msgBuf.append(" Funder Profile Id :").append(lFunderProfileId);
    writeLog(msgBuf.toString());

    checkUserProfiles(existingDeal);  //#DG596 

    // --------------------- DJ, pvcs #957, 28-Oct-05, Catherine -- begin ---------------------
    updatePartyProfileAssoc();
    // --------------------- DJ, pvcs #957, 28-Oct-05, Catherine -- end --------------------- 

    //==========================================================================
    // keep the data that we will need once we enter the
    // preserve document tracking method
    //==========================================================================
    Collection<DocumentTracking> existingDocTracking = existingDeal.getDocumentTracks();
    //Collection existingDealNotes = existingDeal.getDealNotes();					// Tracker #2762
    //#DG600 Collection<DealNotes> incomingDealNotes = deal.getDealNotes();								// Tracker #2762 - Sasa, 04. April 2006
    DealPK keepPK = new DealPK( existingDeal.getDealId(), existingDeal.getCopyId() );
  
    //#DG596 moved up msgBuf = new StringBuffer(" Underwriter User Id :");
  
    MasterDeal existingMD = null;
    MasterDeal incomingMD = null;
  
    try
    {
      //DealPK pk = (DealPK)existingDeal.getPk();
      existingMD = new MasterDeal(srk,null,existingDeal.getDealId()); //::calcmon
      // Update the deal object locked status to "Yes".
      existingDeal.setScenarioLocked("Y");
      existingDeal.setScenarioRecommended("N");
      existingDeal.setCopyType("S");
      existingDeal.ejbStore();
      //  Create a new deal object with the existing deal number.
      incomingMD = new MasterDeal(srk,null,deal.getDealId());  //::calcmon
  
      // The new deal object should have a status of "Received",
      //  David Krick:  Potentially should change 'received' status to 'in underwriting'
      //  status because the potential for resubmission could blow away the deal.
      // Update deal Special Feature to 'blank'
  
      deal.setStatusId(Mc.DEAL_RECEIVED);
      deal.setSpecialFeatureId(0);
      deal.setScenarioRecommended("Y");
      deal.setCopyType("G");
      deal.setScenarioLocked("N");
      //========================================================================
      //deal.setPreApproval("Y");
      //deal.setDatePreAppFirm(new Date() );
      
      int gid = existingMD.copyFrom(incomingMD,incomingMD.getGoldCopyId());
  
      existingMD.setGoldCopyId(gid);
      existingMD.ejbStore();
  
      // ============================================================================
      // from this moment existing deal has changed. This is very important to notice.
      // From now on when we refer to existing deal we actually  work with new deal.
      // ===========================================================================
      DealPK pk = new DealPK(existingMD.getDealId(), gid);
      existingDeal = existingDeal.findByPrimaryKey(pk);
      //=============================================================================
      // Parameters:
      // existingDocTracking - document tracking records from existing deal
      // existingDeal - VEEEEERY important: from this moment existing deal has
      // changed to new one. This is the copy of incoming appended to existing deal.
      // keepPK is primary key of the old deal.
      //=============================================================================
      // for 4.3GR START <- Venkata - CR 58 - FXP25034 - Pre-Approval Firm Up Conditions Reset - CIBC - Start
  	  boolean isPreAppCondReset = PropertiesCache.getInstance()
  	      .getProperty(existingDeal.getInstitutionProfileId(), 
  	    		  "com.filogix.ingestion.preappfirm.conditions.reset" , "N" )
  	      .equalsIgnoreCase( "Y" );
  	  if(!isPreAppCondReset){
            // existing deal is actually new copy
            preserveDocumentTracking(existingDocTracking,existingDeal, keepPK, true); 
  	  }
      // for 4.3GR END <- Venkata - CR 58 - FXP25034 - Pre-Approval Firm Up Conditions Reset - CIBC - End

      //#DG600 rewritten
      DealNotes dealNota = new DealNotes(srk);
      //preserveDealNotes(existingDealNotes, existingDeal);							// Tracker #2762      
     	//preserveDealNotes(incomingDealNotes, existingDeal);							// Tracker #2762 - Sasa, 04. April 2006
      if(!(this instanceof CVDupeCheckProcessor))
      	dealNota.copyDealNotes(deal, existingDeal, -1);
      
      // -------------- Catherine, 01Feb05, BMO to CCAPS ---------------
      processServicingMortgageNumber();
      // -------------- Catherine end ----------------------------------

      final String brokerName = findBrokerName(deal);		//#DG640
			final String sourceFirm = findSourceFirm(deal);		//#DG640
			// note for existing deal
      /* #DG600
      StringBuffer noteText = new StringBuffer("Preapproval firmed up by  ");
      noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));
      note.create((DealPK)existingDeal.getPk(),((DealPK)existingDeal.getPk()).getId(),Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,noteText.toString());
      */
      Formattable nota= BXResources.newFormata(DUP_CHEK_PREAPPROVALFIRM, brokerName, sourceFirm);
      dealNota.create(existingDeal,Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
  
      // note for incoming deal
      /*noteText = new StringBuffer("( Preapproval Firm Up ) ");
      noteText.append("Deal ").append(existingDeal.getDealId()).append(" updated by ");
      noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));*/      
      nota= BXResources.newFormata(DUP_CHEK_PREAPPROVALFIRMUPD, 
      		//#DG636 deal.getDealId(), findBrokerName(deal), findSourceFirm(deal));
      		existingDeal.getDealId(), brokerName, sourceFirm);
      dealNota.create(deal,Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
  
      //  Requested by David Krick...Aug 12, 2002
      //  Don't want on a preapp gone firm
      existingDeal.setCheckNotesFlag("N");
      existingDeal.setUnderwriterUserId( lUnderwriterUserId );
      existingDeal.setAdministratorId( lAdministratorId );
      existingDeal.setBranchProfileId( lBranchProfileId );
      existingDeal.setFunderProfileId( lFunderProfileId );
      //=======================================================================
      existingDeal.setPreApproval("Y");
      existingDeal.setDatePreAppFirm(new Date() );
  
      existingDeal.forceChange("checkNotesFlag");
      existingDeal.forceChange("underwriterUserId");
      existingDeal.forceChange("administratorId");
      existingDeal.forceChange("branchProfileId");
      existingDeal.forceChange("funderProfileId");
      existingDeal.forceChange("preApproval");
      existingDeal.forceChange("datePreAppFirm");
      existingDeal.forceChange("checkNotesFlag");
  
      CCM ccm = new CCM(srk);
      ccm.setAsRecommendedScenario(existingDeal);
  
      deal.setStatusId(Mc.DEAL_COLLAPSED);
      deal.setDenialReasonId(Mc.DENIAL_REASON_SOURCE_RESUBMISSION);
      // Made based on request by Dave Krick on Sep 27, 2002
      // ZIVKO:ALERT: find constants for these two zeroes
      //deal.setAdministratorId(0);
      deal.setUnderwriterUserId(lUnderwriterUserId);
  
      deal.ejbStore();
      existingDeal.ejbStore();
      //**************MCM Impl team changes - 08-Aug-2008 - Starts - XS_1.1 //      
       processQualifyDetails(existingDeal.getDealId() , deal.getDealId());       
      //**************MCM Impl team changes - 08-Aug-2008 - Starts - XS_1.1 //
      storedDealPK = new DealPK(existingDeal.getDealId(),gid) ;
      notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2;
  
      writeLog("Method performCreateWithExisting exited.");
  
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: Method createWithExisting action failed";
  
      //srk.getSysLogger().error(msg);
      writeLog(msg);
      throw new DupCheckActionException(msg);
    }
  }

	/**
   * Cross references two deals.
   * 
   * @throws DupCheckActionException : If the cross reference cannot be normally completed<br>
   * @version 1.1  <br>
   * Date: 06/30/2006 <br>
   * Author: NBC/PP Implementation Team <br>
   * Change:  <br>
   *	Incorporared call to new method DealCompare.comparePartiesByType() to compare service branch parties. <br>
   */
  protected void performCreateXRefDeals() throws DupCheckActionException 
  {
    msgBuf = new StringBuffer("Method performCreateXRefDeals entered.");
    msgBuf.append(" ExistingDeal is: ").append(existingDeal.getDealId()).append(" Copy Id:").append(existingDeal.getCopyId());
    writeLog(msgBuf.toString());
  
    MasterDeal existingMD = null;
    
    try{
      existingMD = new MasterDeal(srk,null,existingDeal.getDealId());
      
      existingDeal.setReferenceDealNumber("" + deal.getDealId() );
      existingDeal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_RESUBMITTED_AS_DEAL);
  
      deal.setStatusId(Mc.DEAL_COLLAPSED);
      deal.setDenialReasonId(Mc.DENIAL_REASON_SOURCE_RESUBMISSION);
      // added based on request by Dave Krick on Sep 27, 2002
      //ZIVKO:ALERT: make constants for these two zeroes
      //deal.setAdministratorId(0);
      deal.setUnderwriterUserId( existingDeal.getUnderwriterUserId() );
  
      deal.setReferenceDealNumber("" + existingDeal.getDealId() );
      deal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_ORIGINAL_DEAL_RESUBMIT);

      // -------------- Catherine, 01Feb05, BMO to CCAPS ---------------
      processServicingMortgageNumber();
      // -------------- Catherine end ----------------------------------
      
      //========================================================================
      // Call dealCompare module
      //========================================================================
      Deal dealToCompare = this.findDealToCompare(existingMD,existingDeal);
      DealCompare dealCompare;
      if( dealToCompare == null ){
        dealCompare = new DealCompare(existingDeal,deal,srk);
      }else{
        dealCompare = new DealCompare(dealToCompare,deal,srk);
      }
      dealCompare.compareAll();
      
      doSpecialCompare(dealCompare);
  		//    ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
			// get flag and check if Service Branch check is activated, call compare parties for service branch if applicable
      // added institutionProfile from deal - fixed since 4.3GR
      boolean isServiceBranchCheckActive = PropertiesCache.getInstance( ).getProperty( deal.getInstitutionProfileId(), COM_BASIS100_PARTY_INCLUDE_SERVICE_BRANCH , "N" ).equalsIgnoreCase( "Y" );
		
      if ( isServiceBranchCheckActive ) {
    	  dealCompare.comparePartiesByType( Mc.PARTY_TYPE_SERVICE_BRANCH ); 
      }
      //    ***** Change by NBC Impl. Team - Version 1.1 - End*****//
  
      Date curDate = new Date();
      Formattable nota;
			if( dealCompare.getTotalDifferences() > 0){
				/*strDateVal = DocPrepLanguage.getInstance().getFormattedDate(new Date(), Mc.LANGUAGE_PREFERENCE_ENGLISH );
        notePrefix.append("Resubmission has been received for this deal and was saved as Deal ");
        notePrefix.append(deal.getDealId()).append(" on ").append(strDateVal).append(". ");
        dealCompare.writeNoteToDeal(notePrefix.toString(),existingDeal);*/
        nota = BXResources.newFormata(DUP_CHEK_RESUBMISIONRCVD, deal.getDealId(), curDate);
      	dealCompare.writeNoteToDeal(nota, existingDeal);
  
        /*notePrefix = new StringBuffer("This is a resubmission of Deal ");
        notePrefix.append(existingDeal.getDealId());
        dealCompare.writeNoteToDeal(notePrefix.toString(), deal); */
        nota = BXResources.newFormata(DUP_CHEK_RESUBMISION, existingDeal.getDealId(), curDate);	//#DG640 provide date just in case...
        dealCompare.writeNoteToDeal(nota, deal);
        // this has moved based on David's request on July 29, 2002
        existingDeal.setResubmissionFlag("Y");
  
        //  Added this to retain document tracking items
        //  David Krick - Aug 12, 2002
        preserveNonPreAppFirmUp();  
      } 
			else {
      	//#DG600 strDateVal = DocPrepLanguage.getInstance().getFormattedDate(curDate, Mc.LANGUAGE_PREFERENCE_ENGLISH );
        //notePrefix.append("Resubmission has been received for this deal and was saved as Deal ");
        //notePrefix.append(deal.getDealId()).append(" on ").append(strDateVal).append(". ");
        /*dealCompare.writeNoteToDeal("Resubmission has been received for this deal - see deal#" 
         * + deal.getDealId() + " (with no changes found).",existingDeal);*/
        nota = BXResources.newFormata(DUP_CHEK_RESUBMISIONRCVDA, deal.getDealId());
        //#DG628 if no differences then it's just a simple deal note to be written 
        //dealCompare.writeNoteToDeal(nota, existingDeal);
        DealNotes dealNota = new DealNotes(srk);
  			dealNota.create(existingDeal, DEAL_NOTES_CATEGORY_GENERAL, 0, nota);		//#DG636
      }
      //  John:  Changed this to avoid generation of an extra review deal notes
      //  when the source resubmission task is created.  Requested by David Krick
      //--> Ticket#558 : Modified the Deal Note message and set CheckNoteFlag to Y
      existingDeal.setCheckNotesFlag("Y");
      //==========================================================================
      //existingDeal.setResubmissionFlag("Y");
      //  John:  Changed this to avoid generation of an extra review deal notes
      //  when the source resubmission task is created.  Requested by David Krick
      deal.setCheckNotesFlag("N");
      deal.ejbStore();
      existingDeal.ejbStore();
  
      storedDealPK= new DealPK(existingDeal.getDealId(),existingDeal.getCopyId() );
      notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2;
  
      writeLog( "Method performCreateXRefDeals finished.");
    }catch( Exception exc ){
      String msg = "Duplicate Deal Check: CreateXRefDeals action failed.";
      writeLog(msg + "  Original exception message:" + exc );
      throw new DupCheckActionException(msg);
    }
  }

  /**
     * <p>
     * Description :Method replaces existing deal with incoming deal.
     * </p>
     * @version 1.0 Initial version
     * @version 1.1 XS_1.1 08-Aug-2008 Added method call to the
     *          processQualifyDetails(....)
     */
  protected void performReplace() throws Exception
  {
    // preserve required fields.
    msgBuf = new StringBuffer("Method performReplace() entered.");
    msgBuf.append(" ExistingDeal is: ").append(existingDeal.getDealId()).append(" Copy Id:").append(existingDeal.getCopyId());
    writeLog(msgBuf.toString());
    int lUnderwriterUserId = existingDeal.getUnderwriterUserId();
    int lAdministratorId = existingDeal.getAdministratorId();
    int lBranchProfileId = existingDeal.getBranchProfileId();
    int lFunderProfileId = existingDeal.getFunderProfileId();

    msgBuf = new StringBuffer(" Underwriter User Id :");
    msgBuf.append(lUnderwriterUserId);
    msgBuf.append(" AdministratorId User Id :").append(lAdministratorId);
    msgBuf.append(" Branch Profile Id :").append(lBranchProfileId);
    msgBuf.append(" Funder Profile Id :").append(lFunderProfileId);
    writeLog(msgBuf.toString());

    checkUserProfiles(existingDeal);  //#DG596 
    
    int lExistPrimPropId = getExistingPrimPropertyId();                         // Dj-specific

    //======
    Collection existingCBReportsColl = existingDeal.getCreditBureauReports();
    Collection incomingCBReportsColl = deal.getCreditBureauReports();
    //======
    //#DG600 Collection<DealNotes> incomingDealNotes = deal.getDealNotes();								// Tracker #2762 - Sasa, 04. April 2006

    //#DG596 moved up msgBuf = new StringBuffer(" Underwriter User Id :");....

    MasterDeal existingMD = null;
    MasterDeal incomingMD = null;

    try
    {

      existingMD = new MasterDeal(srk,null,existingDeal.getDealId());  //::calcmon
      incomingMD = new MasterDeal(srk,null,deal.getDealId());  //::calcmon

      Collection ids = null;

      ids = existingMD.getCopyIds();    

      int gid = existingMD.copyFrom(incomingMD,incomingMD.getGoldCopyId());

      if(ids != null)
      {
        Iterator it = ids.iterator();
        Integer n = null;

        while(it.hasNext())
        {
          n = (Integer)it.next();
          existingMD.deleteCopy(n.intValue());
        }
      }

      existingMD.setGoldCopyId(gid);
      existingMD.ejbStore();

      DealPK pk = new DealPK(existingMD.getDealId(), gid);
      existingDeal = existingDeal.findByPrimaryKey(pk);
      msgBuf.setLength(0);		//#DG628 reset
      msgBuf.append(" ExistingDeal after replacement is: ").append(existingDeal.getDealId()).append(" Copy Id:").append(existingDeal.getCopyId());
      writeLog(msgBuf.toString());

      //#DG600 rewritten
      DealNotes dealNota = new DealNotes(srk);
      //if (incomingDealNotes != null) 
      	//preserveDealNotes(incomingDealNotes, existingDeal);							// Tracker #2762 - Sasa, 04. April 2006
      dealNota.copyDealNotes(deal, existingDeal, -1);
      doSpecialProcessingOnReplace(lExistPrimPropId);

			// note for existing deal
      /* #DG600
      StringBuffer noteText = new StringBuffer("Deal resubmitted by ");
      noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));
      note.create((DealPK)existingDeal.getPk(),((DealPK)existingDeal.getPk()).getId(),Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,noteText.toString());
      */
      final String brokerName = findBrokerName(deal);
      final String sourceFirm = findSourceFirm(deal);
			Formattable nota= BXResources.newFormata(DUP_CHEK_RESUBMITED, brokerName, sourceFirm);
      dealNota.create(existingDeal,Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
     
      // note for incoming deal
      /*noteText = new StringBuffer("( Broker Resubmission ) ");
      noteText.append("Deal ").append(existingDeal.getDealId()).append(" replaced by ");
      noteText.append(findBrokerName(deal)).append(" from ").append(findSourceFirm(deal));
      note.create((DealPK)deal.getPk(),((DealPK)deal.getPk()).getId(),Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,noteText.toString() );*/
      nota= BXResources.newFormata(DUP_CHEK_RESUBMITEDBRK, 
      		//#DG636 deal.getDealId(), brokerName, sourceFirm);
      		existingDeal.getDealId(), brokerName, sourceFirm);
      dealNota.create(deal,Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, nota);		//#DG636
 
      //existingDeal.setCheckNotesFlag("Y");
      existingDeal.setCheckNotesFlag("N");
      existingDeal.setUnderwriterUserId( lUnderwriterUserId );
      existingDeal.setAdministratorId( lAdministratorId );
      existingDeal.setBranchProfileId( lBranchProfileId );
      existingDeal.setFunderProfileId( lFunderProfileId );

      existingDeal.forceChange("checkNotesFlag");
      existingDeal.forceChange("underwriterUserId");
      existingDeal.forceChange("administratorId");
      existingDeal.forceChange("branchProfileId");
      existingDeal.forceChange("funderProfileId");

      // -------------- Catherine, 01Feb05, BMO to CCAPS ---------------
      // added institutionProfileId - fixed since 4.3GR
      if (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DEAL_COPY_LENDER_NOTES, "N").equalsIgnoreCase("Y"))
      {
        existingDeal.forceChange("servicingMortgageNumber");

        String val = deal.getServicingMortgageNumber();
        logger.debug("DupeCheck: servicingMortgageNumber = " + val);

        //#DG600 copyDealNotesCategoryLender(existingDeal, deal);
        dealNota.copyDealNotes(existingDeal, deal, Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER);
        processServicingMortgageNumber();
      }
      // -------------- Catherine end ----------------------------------

      deal.setStatusId(Mc.DEAL_COLLAPSED);
      deal.setDenialReasonId(Mc.DENIAL_REASON_SOURCE_RESUBMISSION);
      // added based on request from Dave Krick on Sep 27, 2002
      // ZIVKO:ALERT: make constants for these two zeroes
      //deal.setAdministratorId(0);
      deal.setUnderwriterUserId(lUnderwriterUserId);
      // now cover the case when existing deal does not have credit bureau report
      // and incoming one has. In that case we have to bring CB report to existing deal.
      if( existingCBReportsColl.isEmpty() && incomingCBReportsColl.isEmpty() == false ){
        Iterator it = incomingCBReportsColl.iterator();
        CreditBureauReport report = null;
        while( it.hasNext() ){
          report = (CreditBureauReport) it.next();
          break;  // only one record
        }
        if( report != null ){
          CreditBureauReport oneReport = new CreditBureauReport(srk);
          oneReport = oneReport.create(  new DealPK(existingDeal.getDealId(), existingDeal.getCopyId() )  );
          oneReport.setCreditReport(report.getCreditReport());
          oneReport.ejbStore();
        }
      }

      existingDeal.ejbStore();
      deal.ejbStore();

      storedDealPK = new DealPK(existingDeal.getDealId(),gid);
      notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2;

      //**************MCM Impl team changes - 08-Aug-2008 - Starts - XS_1.1 //
       processQualifyDetails(existingDeal.getDealId() , deal.getDealId());       
       //**************MCM Impl team changes - 08-Aug-2008 - Ends - XS_1.1 //
      writeLog("Method performreplace exited.");
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: Replace action failed";
      writeLog(msg);
      throw new DupCheckActionException(msg);
    }
  }

  /**
   * Method inserts incoming deal into the database.
   */
  protected void performCreateWithNew() throws DupCheckActionException {
    msgBuf = new StringBuffer("Method performCreateWithNew entered.");
    writeLog(msgBuf.toString());
  
    //MasterDeal existingMD = null;
    //MasterDeal incomingMD = null;
  
    try
    {
      //int existingStatus = existingDeal.getStatusId();
  
      //if(existingStatus == Mc.DEAL_DENIED && !( isSoftDenial(existingDeal) ))
      //  deal.setPreviouslyDeclined("Y");
  
      deal.setStatusId(Mc.DEAL_RECEIVED);
      //deal.setCheckNotesFlag("Y");
      deal.setCheckNotesFlag("N");
  
      deal.ejbStore();
      storedDealPK = (DealPK)deal.getPk();
      notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL;
  
      writeLog( "Method performCreateWithNew finished." );
    }
    catch(Exception e)
    {
      String msg = "Duplicate Deal Check: performCreateWithNew action failed";
  
      //srk.getSysLogger().error(msg);
      writeLog(msg);
      throw new DupCheckActionException(msg);
    }
  }

  /**
   *  This is the entry point of processing duplicates.
   */
  public void process(Deal pDeal, SessionResourceKit pSrk, org.w3c.dom.Document pDom) throws Exception 
  {
    setDuplicate(false);    // by deafault deal is not duplicate.
    storedDealPK = null;
    deal = pDeal;
    dom = pDom;
    srk = pSrk;
    lockedDealId = -1;
    writeLog(" process() entered.");
    
    processSensitivityFlags();

    messagesForBroker = new java.util.ArrayList<ClientMessage>();
    DuplicateApplicationFinder souAppl = new DuplicateApplicationFinder(srk);
    souapplr = souAppl.search( deal, null );
    
    //==============================================================================
    // What is a match? Match is when two deals have the same source application ID.
    //==============================================================================
    if( souapplr.isMatch() ){
      writeLog("Deal " + deal.getDealId() + " (" + deal.getCopyId() + ") has a match in the database." );
      processDuplicate(  );
      // zivko:fxlink:phaseII
      //appendDupeCheckMessageToSource();
    }else{
      writeLog("Deal " + deal.getDealId() + " (" + deal.getCopyId() + ") does not have a match in the database." );
      performCreateWithNew();
      
      if ((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
              "com.filogix.dupecheck.dealcompare.turnOffNotesForNonMatch", "N")).equals("N")) {
          processBorrowers();
          processProperties();
      }
      return;
    }
  }

  /**
   * Incoming deal has a match in the database. So, process it.
   */
  protected void processDuplicate() throws Exception, DealLockedException {
    writeLog("processDuplicate() entered.");
    //#DG600 dealNotes = new ArrayList();
    matchedEntries = souapplr.getAllMatchedEntries();
    // step 1. determine active deal
    existingDeal = determineActiveDeal( matchedEntries );
    if(existingDeal == null){
      writeLog("Incoming deal" + deal.getDealId() + ": has a duplicate but no valid deal could be found.");
      throw new Exception("Exception - Incoming deal has a duplicate but no valid deal could be found.");
    }
  
    //==========================================================================
    // this addition has been made on Sep 05, 2002 based on discussion with
    // Dave Krick and Billy. Instead of dealing with gold copy of existing
    // deal we have to work with recommended scenario.
    //==========================================================================
    DealPK dpk = new DealPK( existingDeal.getDealId(), existingDeal.getCopyId() );
  
    //  Catherine, a small bug fixed 
    boolean isFndException = false;
    try {
      existingDeal = existingDeal.findByRecommendedScenario(dpk,true);
    }
    catch (FinderException fe){
       isFndException = (fe.getMessage().endsWith("findByRecommendedScenario() exception")? true : false);
    }
    
    if ((existingDeal == null) || isFndException){
      writeLog("Incoming deal " + deal.getDealId() + ": has a duplicate but no valid deal (recommended scenario) could be found.");
      throw new Exception("Exception - Incoming deal has a duplicate but no valid deal (recommended scenario) could be found.");
    }
  
    //==========================================================================
    // make sure that existing deal is not locked.
    //==========================================================================
    if( DLM.getInstance().isLocked(existingDeal.getDealId(),0,srk) ){
      msgBuf = new StringBuffer("Deal ");
      msgBuf.append(existingDeal.getDealId()).append(" has been locked by another user.");
      writeLog(msgBuf.toString());
      throw new DealLockedException(msgBuf.toString());
    }else{
      try{
        // =====================================================================
        // If existing deal is not locked, we have to lock it.
        //======================================================================
        msgBuf = new StringBuffer("Attempt to lock the deal: ");
        msgBuf.append(existingDeal.getDealId());
        writeLog( msgBuf.toString() );
        DLM.getInstance().lock(existingDeal.getDealId(),0,srk);
        lockedDealId = existingDeal.getDealId();
      }catch(Exception exc){
        msgBuf = new StringBuffer("Deal ");
        msgBuf.append(existingDeal.getDealId()).append(" : ").append("Unsuccessful attempt to lock the deal.");
        writeLog(msgBuf.toString());
        throw new DealLockedException(msgBuf.toString());
      }
    }
  
    // at this point we can say that the incoming deal is duplicate input
    setDuplicate(true);
  
    //=============================================================================
    // this is the point where we determine how are we going to process duplicate.
    //=============================================================================
    if(existingDeal.getStatusId()  == Mc.DEAL_PRE_APPROVAL_OFFERED ){
      msgBuf = new StringBuffer("Deal: ");
      msgBuf.append(existingDeal.getDealId()).append(" is PRE_APPROVAL_OFFERED ");
      writeLog(msgBuf.toString());
      if( checkGoneFirm(deal) ){
        msgBuf = new StringBuffer("Deal: ");
        msgBuf.append(deal.getDealId()).append(" has gone firm.");
        writeLog(msgBuf.toString());
        processUploadFlags();
        performCreateWithExisting();
      }else{
        msgBuf = new StringBuffer("Deal: ");
        msgBuf.append(deal.getDealId()).append(" has NOT gone firm.");
        writeLog(msgBuf.toString());
        applyMatrix();
      }
    }else{
      applyMatrix();
    }
  }

  /**
   * When existing deal is preApp apply this matrix.
   */
  protected void applyMatrixForPreApp() throws Exception {
    writeLog("Apply matrix for pre-app method entered.");
    int lStat = existingDeal.getStatusId();
    writeLog("Existing deal " + existingDeal.getDealId() + " has status: " + existingDeal.getStatusId());
    switch(lStat){
      case Mc.DEAL_INCOMPLETE :
      case Mc.DEAL_RECEIVED :
      {
        performReplace();
        break;
      }
      case Mc.DEAL_IN_UNDEREWRITING :
      case Mc.DEAL_HIGHER_APPROVAL_REQUESTED:
      case Mc.DEAL_JOINT_APPROVAL_REQUESTED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED:
      case Mc.DEAL_REVIEW_APPROVED_CHANGES: 
      case Mc.DEAL_APPROVE_WITH_CHANGES :
      case Mc.DEAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_FINAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REVIEW :
      case Mc.DEAL_APPROVED :
      // ----------  
      case Mc.DEAL_COMMIT_OFFERED :
      case Mc.DEAL_OFFER_ACCEPTED :
      case Mc.DEAL_PRE_APPROVAL_FIRM :
      case Mc.DEAL_CONDITIONS_OUT :
      case Mc.DEAL_CONDITIONS_SATISFIED :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED :
      {
        performCreateXRefDeals();
        break;
      }
      case Mc.DEAL_PRE_APPROVAL_OFFERED :
      {
        if(checkGoneFirm(deal)){
          performCreateWithExisting();
        }else{
          performCreateXRefDeals();
        }
        break;
      }
      case Mc.DEAL_POST_DATED_FUNDING :
      {
        performReject();
        // message 1 to broker
        appendMessageForBroker(DUPECHECK_MSG.PostFundPreApp);
        break;
      }
      case Mc.DEAL_FUNDED :
      {
        performReject();
        //message 3 to broker
        appendMessageForBroker(DUPECHECK_MSG.FundPreApp);
        break;
      }
      case Mc.DEAL_TO_SERVICING :
      {
        performReject();
        // message 5 to broker
        appendMessageForBroker(DUPECHECK_MSG.ToServicPreApp);
        break;
      }
      case Mc.DEAL_COLLAPSED :
      {
        checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.ColapsPreApp, false);
        break;
      }
      case Mc.DEAL_DENIED :
      {
        if(isSoftDenial( existingDeal) ){
          processSignificantChange(false);
        }else{
          checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.DeniedPreApp, false);
        }  
        break;
      }
      case Mc.DEAL_FUNDING_REVERSED :
      {
        performReject();
        // message 7 to broker
        appendMessageForBroker(DUPECHECK_MSG.FundRevPreApp);
        break;
      }
    }
  }

  /**
   * When existing deal is not preApp apply this matrix.
   */
  protected void applyMatrixForNoPreApp() throws Exception {
    writeLog("Apply matrix for non-pre-app method entered.");
    int lStat = existingDeal.getStatusId();
    writeLog("Existing deal " + existingDeal.getDealId() + " has status: " + existingDeal.getStatusId());
    switch(lStat){
      case Mc.DEAL_INCOMPLETE :
      {
        performReplace();
        break;
      }
      case Mc.DEAL_RECEIVED :
      {
        String lPreApproved = existingDeal.getPreApproval();
        Date preAppFirmUpDate = existingDeal.getDatePreAppFirm();
        if(lPreApproved.equalsIgnoreCase("y") && preAppFirmUpDate != null){
          performCreateXRefDeals();
        }else{
          performReplace();
        }
        break;
      }
      case Mc.DEAL_IN_UNDEREWRITING :
      case Mc.DEAL_HIGHER_APPROVAL_REQUESTED:
      case Mc.DEAL_JOINT_APPROVAL_REQUESTED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED:
      case Mc.DEAL_REVIEW_APPROVED_CHANGES: 
      case Mc.DEAL_APPROVE_WITH_CHANGES :
      case Mc.DEAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_FINAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REVIEW :
      case Mc.DEAL_APPROVED :
      case Mc.DEAL_PRE_APPROVAL_OFFERED :
      case Mc.DEAL_COMMIT_OFFERED :
      case Mc.DEAL_OFFER_ACCEPTED :
      case Mc.DEAL_PRE_APPROVAL_FIRM :
      case Mc.DEAL_CONDITIONS_OUT :
      case Mc.DEAL_CONDITIONS_SATISFIED :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED :
      {
        performCreateXRefDeals();
        break;
      }
      case Mc.DEAL_POST_DATED_FUNDING :
      {
        performReject();
        // message 2 to broker
        appendMessageForBroker(DUPECHECK_MSG.PostFund);
        break;
      }
      case Mc.DEAL_FUNDED :
      {
        performReject();
        //message 4 to broker
        appendMessageForBroker(DUPECHECK_MSG.Fund);
        break;
      }
      case Mc.DEAL_TO_SERVICING :
      {
        performReject();
        // message 6 to broker
        appendMessageForBroker(DUPECHECK_MSG.ToServic);
        break;
      }
      case Mc.DEAL_COLLAPSED :
      {
        checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.Colaps, false);
        break;
      }
      case Mc.DEAL_DENIED :
      {
        if(isSoftDenial( existingDeal) ){
          processSignificantChange(false);
        }else{
          checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.Denied, false);
        }
        break;
      }
      case Mc.DEAL_FUNDING_REVERSED :
      {
        performReject();
        // message 8 to broker
        appendMessageForBroker(DUPECHECK_MSG.FundRev);
        break;
      }
    }
  }

  // -------------- Catherine, 01Feb05, BMO to CCAPS ---------------
  /**
   * @param deal
   * @param newDeal
   * @throws Exception
   * Catherine, BMO to CCAPS  01Feb05
   * copies dealNotesCategoryId = DEAL_NOTES_CATEGORY_INGESTION_LENDER from deal to newDeal 
   */
/*
  protected void copyDealNotesCategoryLender(Deal deal, Deal newDeal) throws Exception
  {
    writeLog("copyDealNotesCategoryLender() entered: ");
      Collection notes  = deal.getDealNotes();
      Iterator nIt = notes.iterator();
      while (nIt.hasNext()){
        DealNotes oneNote = (DealNotes)nIt.next();
        if (oneNote.getDealNotesCategoryId() == Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER){
          int appId = TypeConverter.intTypeFrom(deal.getApplicationId());
          DealNotes newNotes  = new DealNotes(srk);
          newNotes.create((DealPK)newDeal.getPk(), appId,  
              Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER, oneNote.getDealNotesText());
          newNotes.ejbStore();
          logger.debug("DupeCheck: DEAL_NOTES_CATEGORY_INGESTION_LENDER added to a new deal");
        }
      }
      newDeal.setServicingMortgageNumber(deal.getServicingMortgageNumber());
  }
*/
  protected void processServicingMortgageNumber() {
    writeLog("processServicingMortgageNumber() entered: ");
    String servNr = existingDeal.getServicingMortgageNumber();
    logger.debug("DupeCheck.performCreateWithExisting(): servicingMortgageNumber for old deal = " + servNr);
    deal.setServicingMortgageNumber(servNr);
    logger.debug("DupeCheck.performCreateWithExisting(): SERVICINGMORTGAGENUMBER copied over");
  }
// -------------- Catherine, 01Feb05, BMO to CCAPS end ---------------

  protected boolean areDealsDifferent(Deal pExDeal, Deal pIncomingDeal) throws Exception {
    DealCompare dealCompare = new DealCompare(pExDeal,pIncomingDeal,srk);
    dealCompare.compareAll();
  
    if(dealCompare.getTotalDifferences() > 0){
      dealCompare.writeNoteToDeal(null, pIncomingDeal);
      return true;
    }
    return false;
  }

  protected void processSignificantChange(boolean isDoCompare) throws Exception, DupCheckActionException {
    writeLog("processSignificantChange() entered: ");
    createXRef();
    if (isDoCompare){
      compareDeals(existingDeal, deal);
    }
    performCreateWithNew();
    processBorrowers();
    processProperties();
  
    //  Added by request of David Krick, Aug 12, 2002
    preserveNonPreAppFirmUp();
  }

  protected void checkForSignificantChangeAndAppendMessage(int messageId, boolean isProcessUploadFlags) throws Exception, DupCheckActionException {
    writeLog("checkForSignificantChangeAndAppendMessage() entered: ");
    if (DealCompare.hasSignificantChanges(existingDeal, deal)){
      processSignificantChange(true);
      if (isProcessUploadFlags) {
        processUploadFlags();  
      }
    }else{
      performReject();
      appendMessageForBroker(messageId);
    }
  }
  //#DG598 end --------------------------- Dupe Check refactoring --------------------------------------------
  /***************************************************************************
   **************MCM Impl team changes - 08-Aug-2008 - Starts - XS_1.1 /
   **************************************************************************/
  /**
     * <p>
     * Description : This method process Qualifying Details. This method checks
     * if the Existing Deal is having any Entries in the QualifyDetails table if
     * exists , Then Updates the with the new values else Makes an New Entry
     * with the New Qualifying Details for a new Ingested Deal
     * </p>
     * @param existingDealId -
     *            Existing Deal Id
     * @param newDealId -
     *            new Ingested Deal Id
     * @version 1.0 XS_1.1 08-Aug-2008 Initial version
     */
    private void processQualifyDetails(int existingDealId, int newDealId){
        try{

            boolean qualifyflag = false;
            QualifyDetail existQualifydet = null;
            QualifyDetail newQualifydet = null;
            try{
                newQualifydet = new QualifyDetail(srk, newDealId);
            }catch (FinderException Ex){
                logger.info("DCP@::::Inside new QualifyDet catch Block::" + Ex);
            }
            try{
                existQualifydet = new QualifyDetail(srk, existingDealId);
                qualifyflag = true;
            }catch (FinderException e){
                qualifyflag = false;
                logger
                        .info("inside existing Qualifying Catch BLOck......."
                                + e);
            }
            // If new Qualify Details Exists then get the values and set it.
            if (newQualifydet != null){
                if (!qualifyflag){
                    // If any Exception for Existing Deal then Create entry to
                    // the QualifyDetail with existing deal
                    existQualifydet = new QualifyDetail(srk);

                    existQualifydet.create(existingDealId, newQualifydet
                            .getInterestCompoundingId(), newQualifydet
                            .getRepaymentTypeId());
                }
                existQualifydet.setAmortizationTerm(newQualifydet
                        .getAmortizationTerm());
                existQualifydet.setInterestCompoundingId(newQualifydet
                        .getInterestCompoundingId());
                existQualifydet.setPAndIPaymentAmountQualify(newQualifydet
                        .getPAndIPaymentAmountQualify());
                existQualifydet.setQualifyGds(newQualifydet.getQualifyGds());
                existQualifydet.setQualifyTds(newQualifydet.getQualifyTds());
                existQualifydet.setQualifyRate(newQualifydet.getQualifyRate());
                existQualifydet.setRepaymentTypeId(newQualifydet
                        .getRepaymentTypeId());
                existQualifydet.ejbStore();
            }
        }catch (Exception e){
            logger
                    .info("DCP@::::Caught inside processQualifyDetails Outer Catch:::"
                            + e);
        }
    } 
    /***************************************************************************
     **************MCM Impl team changes - 08-Aug-2008 - Ends - XS_1.1 /
     **************************************************************************/   
}

