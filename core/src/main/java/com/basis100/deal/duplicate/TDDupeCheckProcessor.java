package com.basis100.deal.duplicate;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 * 
 * 19/Apr/2007 DVG #DG598 dupeCheck engine refactoring
 */
import java.util.Date;

import MosSystem.Mc;

import com.filogix.util.Xc;

public final class TDDupeCheckProcessor extends DupeCheckProcessor implements Xc
{

  /**
   * When existing deal is preApp apply this matrix.
   */
  protected void applyMatrixForPreApp() throws Exception
  {
    writeLog("Apply matrix for pre-app method entered.");
    int lStat = existingDeal.getStatusId();
    writeLog("Existing deal " + existingDeal.getDealId() + " has status: " + existingDeal.getStatusId());
    switch(lStat){
      case Mc.DEAL_INCOMPLETE :
      case Mc.DEAL_RECEIVED :
      {
        if( areDealsDifferent(existingDeal,deal)){
          processUploadFlags();
        }
        performReplace();
        break;
      }
      case Mc.DEAL_IN_UNDEREWRITING :
      case Mc.DEAL_HIGHER_APPROVAL_REQUESTED:
      case Mc.DEAL_JOINT_APPROVAL_REQUESTED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED:
      case Mc.DEAL_REVIEW_APPROVED_CHANGES: 
      case Mc.DEAL_APPROVE_WITH_CHANGES :
      case Mc.DEAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_FINAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REVIEW :
      case Mc.DEAL_APPROVED :
      // ----------  
      case Mc.DEAL_COMMIT_OFFERED :
      case Mc.DEAL_OFFER_ACCEPTED :
      case Mc.DEAL_PRE_APPROVAL_FIRM :
      case Mc.DEAL_CONDITIONS_OUT :
      case Mc.DEAL_CONDITIONS_SATISFIED :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED :
      {
        performCreateXRefDeals();
        break;
      }
      case Mc.DEAL_PRE_APPROVAL_OFFERED :
      {
        if(checkGoneFirm(deal)){
          processUploadFlags();
          performCreateWithExisting();
        }else{
          performCreateXRefDeals();
        }
        break;
      }
      case Mc.DEAL_POST_DATED_FUNDING :
      {
        performReject();
        // message 1 to broker
        appendMessageForBroker(DUPECHECK_MSG.PostFundPreApp);
        break;
      }
      case Mc.DEAL_FUNDED :
      {
        performReject();
        //message 3 to broker
        appendMessageForBroker(DUPECHECK_MSG.FundPreApp);
        break;
      }
      case Mc.DEAL_TO_SERVICING :
      {
        performReject();
        // message 5 to broker
        appendMessageForBroker(DUPECHECK_MSG.ToServicPreApp);
        break;
      }
      case Mc.DEAL_COLLAPSED :
      {
        checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.ColapsPreApp, true);
        break;
      }
      case Mc.DEAL_DENIED :{
        if(isSoftDenial( existingDeal) ){
          processSignificantChange(false);
          processUploadFlags();
          }else{
          checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.DeniedPreApp, true);
        }
        break;
      }
      case Mc.DEAL_FUNDING_REVERSED :{
        performReject();
        // message 7 to broker
        appendMessageForBroker(DUPECHECK_MSG.FundRevPreApp);
        break;
      }
    }
  }

  /**
   * When existing deal is not preApp apply this matrix.
   */
  protected void applyMatrixForNoPreApp() throws Exception
  {
    writeLog("Apply matrix for non-pre-app method entered.");
    int lStat = existingDeal.getStatusId();
    writeLog("Existing deal " + existingDeal.getDealId() + " has status: " + existingDeal.getStatusId());
    switch(lStat){
      case Mc.DEAL_INCOMPLETE :{
        if( areDealsDifferent(existingDeal,deal)){
          processUploadFlags();
        }
        performReplace();
        break;
      }
      case Mc.DEAL_RECEIVED :{
        String lPreApproved = existingDeal.getPreApproval();
        Date preAppFirmUpDate = existingDeal.getDatePreAppFirm();
        if(lPreApproved.equalsIgnoreCase("y") && preAppFirmUpDate != null){
          performCreateXRefDeals();
        }else{
          if( areDealsDifferent(existingDeal,deal)){
            processUploadFlags();
          }
          performReplace();
        }
        break;
      }
      case Mc.DEAL_IN_UNDEREWRITING :
      case Mc.DEAL_HIGHER_APPROVAL_REQUESTED:
      case Mc.DEAL_JOINT_APPROVAL_REQUESTED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED:
      case Mc.DEAL_REVIEW_APPROVED_CHANGES: 
      case Mc.DEAL_APPROVE_WITH_CHANGES :
      case Mc.DEAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_FINAL_APPROVAL_RECOMMENDED :
      case Mc.DEAL_EXTERNAL_APPROVAL_REVIEW :
      case Mc.DEAL_APPROVED :
      case Mc.DEAL_PRE_APPROVAL_OFFERED :
      case Mc.DEAL_COMMIT_OFFERED :
      case Mc.DEAL_OFFER_ACCEPTED :
      case Mc.DEAL_PRE_APPROVAL_FIRM :
      case Mc.DEAL_CONDITIONS_OUT :
      case Mc.DEAL_CONDITIONS_SATISFIED :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT :
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED :
      {
        performCreateXRefDeals();
        break;
      }
      case Mc.DEAL_POST_DATED_FUNDING :{
        performReject();
        // message 2 to broker
        appendMessageForBroker(DUPECHECK_MSG.PostFund);
        break;
      }
      case Mc.DEAL_FUNDED :{
        performReject();
        //message 4 to broker
        appendMessageForBroker(DUPECHECK_MSG.Fund);
        break;
      }
      case Mc.DEAL_TO_SERVICING :{
        performReject();
        // message 6 to broker
        appendMessageForBroker(DUPECHECK_MSG.ToServic);
        break;
      }
      case Mc.DEAL_COLLAPSED :{
        checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.Colaps, true);
        break;
      }
      case Mc.DEAL_DENIED :{
        if(isSoftDenial( existingDeal) ){
          processSignificantChange(false);
          processUploadFlags();
          }else{
          checkForSignificantChangeAndAppendMessage(DUPECHECK_MSG.Denied, true);
        }
        break;
      }
      case Mc.DEAL_FUNDING_REVERSED :{
        performReject();
        // message 8 to broker
        appendMessageForBroker(DUPECHECK_MSG.FundRev);
        break;
      }
    }
  }

  protected void processUploadFlags(){
    writeLog("processUploadFlags() entered.");
    setCapLinkRequired(true);                                                
      }

  protected void processSensitivityFlags(){
    writeLog("processSensitivityFlags() entered.");
    setCapLinkSensitive(true);                                                  
  }

}
