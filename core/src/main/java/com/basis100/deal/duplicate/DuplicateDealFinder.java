package com.basis100.deal.duplicate;

import java.util.*;
import com.basis100.deal.util.*;
import com.basis100.deal.entity.*;
import com.basis100.picklist.PicklistData;
import com.basis100.log.*;
import com.basis100.resources.*;
import MosSystem.Mc;


public class DuplicateDealFinder extends DuplicateFinder
{

  protected SessionResourceKit srk;
  protected SysLogger logger;

  protected Deal duplicate;
  protected boolean sourceApplication;
  protected boolean source;
  protected boolean property;
  protected boolean applicant;


  private int sysDependantCutOffDate = 90;

  public DuplicateDealFinder(SessionResourceKit kit) throws SearchException
  {
     srk = kit;
     logger = srk.getSysLogger();
  }

  public SearchResult search(DealEntity de,  Date minDate)throws SearchException
  {
      sourceApplication = true;
      source = true;
      property = true;
      applicant = true;
      //=========================================================================
      // NOTE: this variable (propertySearchRequired) has been introduced because
      // property search IS NOT required when new incoming deal is preApproval
      // ( field specialfeatureid = 1 ). In this case we will assume that there
      // is no match on property.
      // Have a fun, eh...
      // NOTE posted by Zivko: 2002-01-29
      // Look for side effects because at this moment it is unclear, should
      // we look for propertydealdeltatype or not. We may introduce new bug.
      //=========================================================================
      boolean propertySearchRequired = true;

      Deal nd = (Deal)de;

      if( nd.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL ){
        propertySearchRequired = false;
      }
      //========================================================================
      // another funny thing with property match.
      // Look for more info in comment001 at the end of this class.
      // Message posted by Zivko 2002-02-07
      //========================================================================
      if( !isPropertySearchNeeded(nd) ){
        propertySearchRequired = false;
      }

      DuplicateApplicationFinder souAppl = new DuplicateApplicationFinder(srk);
      DuplicateSourceFinder src = new DuplicateSourceFinder(srk);
      DuplicateBorrowerFinder app = new DuplicateBorrowerFinder(srk);
      DuplicatePropertyFinder prp = new DuplicatePropertyFinder(srk);

      SearchResult souapplr = souAppl.search(nd, minDate);
      SearchResult srcsr = src.search(nd, minDate);
      SearchResult appsr = app.search(nd, minDate);
      SearchResult prpsr = null;

      if( propertySearchRequired ){
        prpsr = prp.search(nd, minDate);
      }else{
        prpsr = new PropertySearchResult(srk);
      }

      ArrayList matchingDeals = new ArrayList();

      if( souapplr.isMatch() )
      {
        Deal sourceApplicationDeal = souapplr.getDuplicateDeal();
        matchingDeals.add(sourceApplicationDeal);
        DupeCheck.debug("Match on Source Application ID");
      }
      else
      {
        sourceApplication = false;
      }
      if(srcsr.isMatch())
      {
        Deal srcDeal = srcsr.getDuplicateDeal();
        matchingDeals.add(srcDeal);
        DupeCheck.debug("Match on Source");
      }
      else
      {
        source = false;
      }
      if(appsr.isMatch())
      {
        Deal appDeal = appsr.getDuplicateDeal();
        matchingDeals.add(appDeal);
        DupeCheck.debug("Match on Borrower");
      }
      else
      {
        applicant = false;
      }

      if(prpsr.isMatch())
      {
        Deal prpDeal = prpsr.getDuplicateDeal();
        matchingDeals.add(prpDeal);
        DupeCheck.debug("Match on Property");
      }
      else
      {
        property = false;
      }

      if(matchingDeals.isEmpty())
      return new DealSearchResult(srk);

      Deal found = getMostRecentDeal(matchingDeals);
      int deltaType = -1;

      try
      {
        int prpdelta = propertyDeltaType(nd.getProperties());

        deltaType = detectDealFieldDelta(found,nd);

        deltaType += prpdelta;

      }
      catch(Exception e)
      {
        throw new SearchException("Unable to detect Property field values in DupeCheck");
      }

      int code = 0;
      SearchResult sresult;
      //========================================================================
      // plug here soft denial
      //========================================================================
      if( ( sourceApplication || source || applicant || property ) && processSoftDenial(found) ){
          //sresult = new DealSearchResult(srk);
          sresult = new DealSearchResult(srk,found,nd,code,deltaType);
          sresult.setSoftDenial(true);
          //sresult = new DealSearchResult(srk,found,nd,code,deltaType);
      }else{
          // determine levelonecode
          if( sourceApplication ){
            code = SearchResult.SOU_APPL ;
          }else {
             if( source ){code += SearchResult.S; }
             if( applicant ){code += SearchResult.A; }
             if( property ){code += SearchResult.P; }
          }
          sresult = new DealSearchResult(srk,found,nd,code,deltaType);
          sresult.setSoftDenial(false);
      }

      DupeCheck.debug(sresult.toString());

      return sresult;
  }

  private boolean isPropertySearchNeeded(Deal pDeal){
    try{
      Property prop = new Property(srk,null);
      prop = prop.findByPrimaryProperty( pDeal.getDealId(), pDeal
          .getCopyId(), pDeal.getInstitutionProfileId() );
      if( prop.getStreetTypeId() == 0 && prop.getPropertyStreetNumber() == null ){
        return false;
      }
    } catch(Exception exc){
      return false;
    }
    return true;
  }

  private boolean processSoftDenial( Deal pDeal ){
    int currStatus = pDeal.getStatusId();

    if( currStatus != Mc.DEAL_DENIED ){
      return false;
    }

    int denialReason = pDeal.getDenialReasonId();
    String reasonSoftness;
    int reasonSoftnessInt;
    boolean soft = true;

    // added inst id for ML: -1 is no institutionnized table inst Id
    reasonSoftness = PicklistData.getMatchingColumnValue(-1, "DenialReason", denialReason,"catageory");

    if( reasonSoftness != null ){
      try{
        reasonSoftnessInt = Integer.parseInt(reasonSoftness);
        if(reasonSoftnessInt == 1){
          soft = true;
        }else{
          soft = false;
        }
      }catch(Exception exc){
        soft = false;
      }
    }else{
      soft = false;
    }
    return soft;
  }

}

/*
comment001
Okay, here is an addition to the Property Match in Dupecheck similar
to the fix you did for us last week.

IF new deal Property.StreetTypeID = (0 or null) AND
Property.PropertyStreetNumber = null then treat it like a
preapporval and DO NOT MATCH (set match to FALSE)

This is since the brokers may send preapprovals in without setting
the preapproval flag.

 -----Original Message-----
From: 	David Coleman
Sent:	Thursday, February 07, 2002 9:25 AM
To:	Joe Brancati
Cc:	Paul Lewis; Debbie Jaipargas
Subject:	CTCC deal 614 - dupecheck preapproval issue

Deal 614 came in and was flagged in my ingestion parser
program as a potential preapproval dupecheck problem, so I checked it.

The deal IS a preapproval but the special feature was not set as preapproval
and a property record existed of TO BE DETERMINED, Kelowna BC.
Of course it matched on Property, luckily the deal it matched on had
been denied, and 'other' fields had changed.  This deal would NOT have been
ingested if the other deal had not been final disposition.

Even if Zivko fixes Dupecheck to NOT match property records based on the
special feature being set, then this deal would still pose a problem.
Previously this type of deal would have been stopped by IC rule since
the post code was empty, even still the broker could have put HOHOHO

David Coleman
Senior Business Analyst
Basis100 Inc.  http://www.basis100.com
33 Yonge Street, Suite 900
Toronto, Ontario, CANADA M5E1G4
Ph. 416 364 6085 x 138 Fax 416 364 2941
*/