package com.basis100.deal.duplicate;

public class DupCheckActionException extends SearchException
{

 /**
  * Constructs a <code>DupCheckActionException</code> with no specified detail message.
  */
  public DupCheckActionException()
  {
	  super();
  }

 /**
  * Constructs a <code>DupCheckActionException</code> with the specified message.
  * @param   message  the related message.
  */
  public DupCheckActionException(String message)
  {
    super(message);
  }
}

