package com.basis100.deal.duplicate;

public class DealLockedException extends Exception
{

    public DealLockedException()
    {
        super();
    }

    public DealLockedException(String message)
    {
        super(message);
    }

}