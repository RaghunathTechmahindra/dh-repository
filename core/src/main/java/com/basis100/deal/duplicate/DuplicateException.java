package com.basis100.deal.duplicate;

public class DuplicateException extends Exception
{

 /**
  * Constructs a <code>DuplicateException</code> with no specified detail message.
  */
  public DuplicateException()
  {
	  super();
  }

 /**
  * Constructs a <code>DuplicateException</code> with the specified message.
  * @param   message  the related message.
  */
  public DuplicateException(String message)
  {
    super(message);
  }
}

