package com.basis100.deal.xml;

import org.w3c.dom.*;
import java.io.*;
import java.util.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.xml.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
/**
 *   <pre>
 *   The <code>Association</code> maintains the association between Entities
 *   and DOM objects. Once the association is created it's internal Node can be
 *   attached to any parent node.
 *
 *   An Association's place within the DOM tree can be acertained
 *   by querying the object returned from Association getNode()
 *  </pre>
 */
public class EntityNodeAssociation
{
  private  Node node;
  private  DealEntity entity;
  private  int id = -1;
  private  int copyId = -1;
  private  String name;
  private  boolean attached = false;
  private  int ordinal;
  private  List setFieldList;
  private  Document owner;
  private  EntityNodeAssociation parent;
  private  List children;
  /**
   *  Constructs and Association with the given Element and DealEntity object
   *  The entity may be constructed but not populated with a primary key attribute.
   */
  public EntityNodeAssociation(Node e, DealEntity ent, Document owner, EntityNodeAssociation ass)
  {
     this.entity = ent;
     this.node = e;
     this.name = e.getNodeName();
     this.owner = owner;
     this.parent = parent;

     try
     {
         this.id = ent.getPk().getId();
         this.copyId = ent.getPk().getCopyId();
     }
     catch(Exception nptr)
     {
       ;//
       //node only ie - parent only not child;
     }
  }

  public void updateSetFields(String name)
  {
     if(setFieldList == null)
        setFieldList = new ArrayList();

     setFieldList.add(name);
  }


  public boolean isSetField(String name)
  {
    if(setFieldList == null) return false;

    if(setFieldList.isEmpty()) return false;

    return setFieldList.contains(name);
  }

  /**
   *  Appends this Associations node as a child to parentNode.
   *  @param parentNode the node to append to
   */
  public void attachTo(Node parentNode)
  {
    if(!attached)
    {
      parentNode.appendChild(this.node);
      attached = true;
    }

  }
   /**
   *  gets the deal entity for this Association. 
   */
  public DealEntity getEntity(){return this.entity;}

  /**
   *  gets the id of this Association. The id reflects the primaryKey id of the
   *  the internal entity.
   */
  public int getId(){return id;}


   /**
   *  gets the copyId of this Association. The id reflects the primaryKey id of the
   *  the internal entity.
   */
  public int getCopyId(){return copyId;}

  /**
   *  gets the internal Node for this Association. If attached the Node offers
   *  a means to determine the association's place in a DOM tree
   */
  public Node getNode(){ return this.node;}

  /**
   *  gets the name of this Association. The name will neccesarily be the name
   *  of both the internal node and the internal entity.
   */
  public String getName(){ return this.name;}

  /**
   *  gets whether this Association is attached (via it's internal node) to '
   *  a DOM tree
   */
  public boolean isAttached(){return attached;}

  /**
  *  @return true if this Associations DealEntity and Element are equal else return false;
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    EntityNodeAssociation other  = (EntityNodeAssociation)object;
    boolean retval = false;

    if(getId()== other.getId() && getName().equals(other.getName())) retval = true;

    return retval;
  }

  public EntityNodeAssociation getParent()
  {
    return this.parent;
  }

  public boolean hasParent()
  {
    return parent == null;
  }

  public boolean addChild(EntityNodeAssociation ass)
  {
    if(children == null)
    {
      children = new ArrayList();
    }

    return children.add(ass);
  }


  public Collection getChildren()
  {
    if(children == null)
    {
      children = new ArrayList();
    }
    return children;
  }
}
