package com.basis100.deal.xml;

import org.w3c.dom.*;
import java.util.*;
import java.lang.reflect.*;

import org.w3c.dom.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.xml.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;

/**
 * 
 * @author MCM Impl Team 
 * @version 1.1 31-July-2008 Bug Fix :artf751689 : Added check for the desc(which hols the picklist description) field in createSubTree() 
 * @version 1.2 29-Aug-2008 Removed the code added by the Bug Fix :artf751689 as its causing the regression.
 */
public class ElementBuilder
{

 /*
  public static Element createSubTree(Document owner, SessionResourceKit srk, DealEntity ctx )
  {
    return createSubTree(owner, null, srk, ctx, false, true);
  }
*/
  public static Element createSubTree(Document owner, String elementName, SessionResourceKit srk, DealEntity ctx, boolean picklist, int pLang)
  {
    return createSubTree(owner, elementName, srk, ctx, picklist, true, pLang);
  }

  public static Element createSubTree(Document owner, String elementName, SessionResourceKit srk,
                                int id, int copyid, String tableName, boolean picklistDesc, int pLang)
  {
     return createSubTree(owner, elementName, srk, id, copyid, tableName, picklistDesc, true, pLang);
  }

  public static Element createElement(Document owner, String name)
  {
    if(owner == null || name == null) return null;

    return owner.createElement(name);
  }

  public static Element createElement(Document owner, String name, Map attr)
  {
    Element ret = createElement(owner, name);

    if(attr == null) return ret;

    Set keys = attr.keySet();

    Iterator it = keys.iterator();

    String key = null;
    String value = null;

    while(it.hasNext())
    {
      key = (String)it.next();

      if(key == null) continue;

      value = (String)attr.get(key);

      ret.setAttribute(key,value);
    }

    return ret;
  }

  public static Element createElement(Document owner, String name, String data)
  {
    Element ret = createElement(owner, name);

    if(data == null || ret == null) return ret;

    DomUtil.appendString(ret, data);

    return ret;
  }

  public static Element createElement(Document owner, String name, String data, Map attrib)
  {
    Element ret = createElement(owner, name, attrib);

    if(data == null || ret == null) return ret;

    DomUtil.appendString(ret, data);

    return ret;
  }

  public static Element createEntityElement( Document owner, String elementName,
          SessionResourceKit srk, int id, int copyid, String tableName, boolean picklistDesc, boolean recurse, int pLang)
  {
    return createSubTree(owner, elementName, srk, id, copyid, tableName, picklistDesc, recurse, pLang);
  }

  public static Element createEntityElement( Document owner, String elementName,
          SessionResourceKit srk, int id, int copyid, String tableName, boolean picklistDesc, int pLang)
  {
    return createSubTree(owner, elementName, srk, id, copyid, tableName, picklistDesc, false, pLang);
  }

  private static Element createSubTree(Document owner, String elementName, SessionResourceKit srk, DealEntity ctx, boolean picklistDesc, boolean recurse, int pLang)
  {
    Element top = null;

    try
    {
      if(ctx == null || owner == null) return null;

      if(elementName == null || elementName.trim().length() == 0)
      {
        elementName = ctx.getEntityTableName();
      }

      top = owner.createElement(elementName);

      List children = null;
      List parents = null;

      if(recurse)
      {
        children = ctx.getChildren();
        parents  = ctx.getSecondaryParents();
      }

      Class ctxClass = ctx.getClass();

      Field[] fls = ctxClass.getDeclaredFields();

      int len = fls.length;

      String value = null;
      String fname = null;
      Element current = null;

      for(int i = 0; i < len; i++)
      {
        fname = fls[i].getName();

        // zivko:marker:
        //if(fname.toUpperCase().trim().startsWith("MTGPRODNAME")){
        //}

        value = ctx.getStringValue(fname);

        //eliminate empty elements  - could allow parameter for this.
        if(value == null || value.trim().length() == 0) continue;

        // zivko:marker:
        //true = substitute picklist values
        if(picklistDesc)
        {
          if(fname.toUpperCase().endsWith("ID"))
          {

            String table = fname.substring(0,fname.length() - 2);
            String desc = null;
            // FXLINK:PHASEII: modification request is to show id fields in the map
            if(PicklistData.containsTable(table)){
              current = createElement(owner, fname, value);
              top.appendChild(current);

              try{
				          desc = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), table,value, pLang);
                 } catch(Exception exc){
                desc = PicklistData.getDescription(table,value);
              }

            }else{
              desc = PicklistData.getDescription(table,value);
            }

            if(desc != null)
            {
              if(desc.trim().length() == 0)
               continue;

              value = desc;
              fname = table;
            }

/*
            // FXLINK:PHASEII: modification request is to show id fields in the map
            if(PicklistData.containsTable(table)){
              current = createElement(owner, fname, value);
              top.appendChild(current);
            }

            String desc = null;
            try{
              desc = BXResources.getPickListDescription(table,value, pLang);
            } catch(Exception exc){
              desc = null;
            }
            //String desc = PicklistData.getDescription(table,value);

            if(desc != null)
            {
              if(desc.trim().length() == 0)
               continue;

              value = desc;
              fname = table;
            }
*/
          }
        }

        current = createElement(owner, fname, value);

        if(current != null)
         top.appendChild(current);
      }

      DealEntity appEnt = null;

      if(children != null && !children.isEmpty())
      {
        Iterator chit  =  children.iterator();

        while(chit.hasNext())
        {
          appEnt = (DealEntity)chit.next();

          Element stree = createSubTree(owner, null, srk,appEnt,picklistDesc,recurse, pLang);

          if(stree != null)
           top.appendChild(stree);
        }

      }

      if(parents != null && !parents.isEmpty())
      {
        Iterator pit  =  parents.iterator();

        while(pit.hasNext())
        {
          appEnt = (DealEntity)pit.next();

          Element stree = createSubTree(owner, null, srk, appEnt, picklistDesc, recurse, pLang);

          if(stree != null)
           top.appendChild(stree);
        }

      }

    }
    catch(Exception e)
    {
      srk.getSysLogger().error("ElementBuilder : an error has occurred during element creation");
      srk.getSysLogger().error("ElementBuilder : error msg (null if unknown):" + e.getMessage());
    }

    return top;
  }

  private static Element createSubTree(Document owner, String elementName, SessionResourceKit srk,
                                int id, int copyid, String tableName, boolean picklistDesc, boolean recurse, int pLang)
  {

     DealEntity de = null;

     try
     {
        de = EntityBuilder.constructEntity(tableName,srk,null,id, copyid);
     }
     catch(Exception e)
     {
       return null;
     }

     if(de == null) return null;

     return createSubTree(owner, elementName, srk, de, picklistDesc, recurse, pLang);
  }

}
/*
  public static Element createSubTree(Document owner, String elementName, SessionResourceKit srk, DealEntity ctx, boolean picklistDesc, boolean recurse, int pLang)
  {
    Element top = null;

    try
    {
      if(ctx == null || owner == null) return null;

      if(elementName == null || elementName.trim().length() == 0)
      {
        elementName = ctx.getEntityTableName();
      }

      top = owner.createElement(elementName);

      List children = null;
      List parents = null;

      if(recurse)
      {
        children = ctx.getChildren();
        parents  = ctx.getSecondaryParents();
      }

      Class ctxClass = ctx.getClass();

      Field[] fls = ctxClass.getDeclaredFields();

      int len = fls.length;

      String value = null;
      String fname = null;
      Element current = null;

      for(int i = 0; i < len; i++)
      {
        fname = fls[i].getName();

        // zivko:marker:
        //if(fname.toUpperCase().trim().startsWith("MTGPRODNAME")){
        //}

        value = ctx.getStringValue(fname);

        //eliminate empty elements  - could allow parameter for this.
        if(value == null || value.trim().length() == 0) continue;

        boolean isPicklistType = false;

        // zivko:marker:
        //true = substitute picklist values
        if(picklistDesc)
        {
          if(fname.toUpperCase().endsWith("ID"))
          {

            String table = fname.substring(0,fname.length() - 2);
            String desc = null;
            // FXLINK:PHASEII: modification request is to show id fields in the map
            if(PicklistData.containsTable(table)){
              current = createElement(owner, fname, value);
              top.appendChild(current);

              try{
                desc = BXResources.getPickListDescription(table,value, pLang);
              } catch(Exception exc){
                desc = PicklistData.getDescription(table,value);
              }

            }else{
              desc = PicklistData.getDescription(table,value);
            }

            if(desc != null)
            {
              if(desc.trim().length() == 0)
               continue;

              value = desc;
              fname = table;
            }
          }
        }

        current = createElement(owner, fname, value);

        if(current != null)
         top.appendChild(current);
      }  // end for
    }
    catch(Exception e)
    {
      srk.getSysLogger().error("ElementBuilder : an error has occurred during element creation");
      srk.getSysLogger().error("ElementBuilder : error msg (null if unknown):" + e.getMessage());
    }

    return top;
  }
  //===
*/