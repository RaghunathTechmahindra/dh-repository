package com.basis100.deal.autodecision.helixinterface;

/**
 * 03/Nov/2004 DVG #DG105 helix webservice client to retrieve decision
 */

import java.net.*;
import java.rmi.*;
import java.util.*;
import javax.xml.rpc.*;

import com.basis100.deal.autodecision.helixinterface.helixdecision_wsdl.*;
import com.basis100.resources.*;

/**
 * <p>Title: FXpressJ2EE</p>
 * <p>Description: just provides interface to helix call retrieving a decision
 *  in regards to a specific deal </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: filogix</p>
 * @author Denis V. Gadelha 3/Nov/2004
 * @version 1.0
 */

public class AMEDecision {
  // server name url
  private static final String isServerNameURL = PropertiesCache.getInstance().getProperty(-1, "com.basis100.AutoDecision.HelixInterface.URL", "http://localhost");
  private static int iiMaxTries = -1;   // max tries
  private static HelixDecisionPort ioHelixSrv;

  /**
   * The helix remote interface
   * <p>Title: FXpressJ2EE</p>
   * <p>Description: FXpress this is the remote interface to helix itself.</p>
   * <p>Copyright: Copyright (c) 2002</p>
   * <p>Company: Filogix</p>
   * @author DG
   * @version 1.0
   */
  public interface HelixInterface extends java.rmi.Remote
  {
    /**
     * get the decision from helix regarding this deal
     * @param piDealId int deal id
     * @param piCopyId int copy id
     * @param piSourceApplicationId int source application id
     * @param piUploadSerialNumber int upload serial nr
     * @throws RemoteException
     * @return String the decision
     */
    public String getDecision(int piDealId, int piCopyId, int piSourceApplicationId, int piUploadSerialNumber) throws java.rmi.RemoteException;
  }

  class HelixDecisionSrv implements HelixInterface {

    public String getDecision(int piDealId, int piCopyId,
                              int piSourceApplicationId,
                              int piUploadSerialNumber)
        throws  java.rmi.RemoteException {
      return "Decision "+ (new Date().getTime());
    }
  }

  // initialize the interface
  static {
    String ls;
    if (iiMaxTries < 0) {
      //System.setSecurityManager(new RMISecurityManager());
      try {
        // the retry will be performed at the parent level but I'm leaving this code
        //    here for now and make it 1 try only
        ls = PropertiesCache.getInstance().getProperty(-1, "com.basis100.AutoDecision.HelixInterface.MaxTries", "1");
        iiMaxTries = Integer.parseInt(ls.trim());
      }
      catch (NumberFormatException loe) {
        iiMaxTries = 1;
      }
    }

//    try {
//      psServerNameURL = java.net.InetAddress.getLocalHost().getHostName();
//    }
//    catch (Exception e) {
//      e.printStackTrace();
//    }
//    if (psServerNameURL == "") {
//      System.out.println("usage: java SimpleRMIClient <IP address of host running RMI server>");
//      System.exit(0);
//    }

    ls = "Manual";
    //bind server object to object in client
    //HelixInterface loHelixSrv = (HelixInterface) Naming.lookup(isServerNameURL);
    try {
      ioHelixSrv = new HelixDecisionServiceLocator().getHelixDecisionPort(
       new URL("http://localhost:8082/myservice/services/HelixDecisionBindImpl"));
//       new URL(isServerNameURL));
    }
    catch (Exception loex) {
      loex.printStackTrace();
    }
  }

  /**
   * set initial params and call the remote interface max nr of times
   * @param piDealId int deal id
   * @param piCopyId int copy id
   * @param piSourceApplicationId int source application id
   * @param piUploadSerialNumber int upload serial nr
   * @throws RemoteException
   * @throws MalformedURLException
   * @throws NotBoundException
   * @throws ServiceException
   * @return String
   */
  public static String getAMEDecision(int piDealId, int piCopyId,
                                      int piSourceApplicationId,
                                      int piUploadSerialNumber) throws
      RemoteException, MalformedURLException, NotBoundException,
      ServiceException {

    String ls = null;

    //invoke method on server object
    for (; iiMaxTries > 0; iiMaxTries--) {
      ls = ioHelixSrv.getDecision(piDealId, piCopyId, piSourceApplicationId,
                                  piUploadSerialNumber);
      System.out.println("decision is " + ls);
      if (!ls.equalsIgnoreCase("undefined"))
        break;
    }
    System.out.println("RMI connection successful");
    return ls;
  }

  public static void main(String[] pargs) throws Exception {
    for (int li = 0; li < 11; li++) {
      System.out.println("=>"+AMEDecision.getAMEDecision(1,2,3,4));
    }
  }
}
