package com.basis100.deal.pk;

import java.io.Serializable;

public class RequestPK implements Serializable, IEntityBeanPK {
	
	private String name;
	private int id;
  private int copyId;

	public RequestPK(int id, int copyId) {
		this.id = id;
    this.copyId = copyId;
		this.name = "REQUESTID";
	}

	public int getId() {
		return id;
	}

	public int getCopyId() {
		return this.copyId;
	}

	public String getName() {
		return name;
	}
	
  /**
   * @return A hashcode for this primary key.
   */
	public int hashCode() {
      return name.hashCode() ^ id ;
  }
	
  /**
   * Tests whether this PK is equal to another (pk) object.
   * @param object Object to test for equality. 
   */
	public boolean equals(Object object) {
    if(this == object)
      return true;
    
    else if(object == null || getClass() != object.getClass() )
      return false;

    RequestPK other  = (RequestPK)object;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && 
    	 getName().equals(other.getName()) ) 
    	return true;

    return false;
  }

	public String getWhereClause() {
    String retWhere = " where " + getName() + " = "+ String.valueOf( getId()) +
    " AND copyId = " + String.valueOf(getCopyId());
    return retWhere;
	}
}
