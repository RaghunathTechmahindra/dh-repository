package com.basis100.deal.pk;

import java.io.Serializable;

public class ESBOutboundQueuePK implements IEntityBeanPK, Serializable {

    protected int id;
    private String name = "ESBOutboundQueueId";

    public ESBOutboundQueuePK(int id) {
    	this.id = id;
    }
	
    public int getCopyId() {
    	return 0;
    }

    public int getId() {
    	return id;
    }

    public String getName() {
    	return name;
    }

    public String getWhereClause() {
    	return " WHERE " + getName() + " = " + id;
    }
    
    /**
    *  @return true if
    */
    public boolean equals(Object object){
        if(this == object)
            return true;
        else if(object == null || getClass() != object.getClass() )
            return false;

        ESBOutboundQueuePK other  = (ESBOutboundQueuePK)object;
        boolean retval = false;

        if(getId()== other.getId() && getName().equals(other.getName()) ) retval = true;

        return retval;
    }

    public int hashCode() {
       return (name + id).hashCode();
    }
}
