package com.basis100.deal.pk;

import java.io.Serializable;

public class ScotiabankStepMortgagePK implements Serializable,IEntityBeanPK
{
  private String name;
  private int id;
  private int dealId;
  private int copyId;

  public ScotiabankStepMortgagePK( int id, int dealId, int copyId)
  {
	this.name = "stepMortgageId";
	this.id = id;
	this.dealId = dealId;
	this.copyId = copyId;
  }
  public int getId(){return this.id;}

  public void setCopyId(int copyId){ this.copyId = copyId;}

  public int getCopyId(){return this.copyId;}

  public void setDealId(int dealId){ this.dealId = dealId;}

  public int getDealId(){return this.dealId;}

  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = "+ String.valueOf( getId()) +
       " AND dealId = " + String.valueOf(getDealId()) +
       " AND copyId = " + String.valueOf(getCopyId());
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    ScotiabankStepMortgagePK other  = (ScotiabankStepMortgagePK)object;
    boolean retval = false;

    if(getId()== other.getId() && getDealId() == other.getDealId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
     return (name + id + dealId + copyId).hashCode();
  }
}
