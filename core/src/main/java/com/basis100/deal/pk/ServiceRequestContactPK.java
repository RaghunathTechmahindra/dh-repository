/*
 * ServiceRequestContactPK.java : PK for ServiceRequestcontactPK class
 * 
 * Created: 07-FEB-2006
 * Author:  Edward Steel
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author ESteel
 *
 */
public class ServiceRequestContactPK implements IEntityBeanPK, Serializable {
	
	private String name;
	private int id;
  private int copyId;
	private int requestId;

	/**
	 * Constructor.
	 */
	public ServiceRequestContactPK(int id, int requestId, int copyId) {
		this.name="SERVICEREQUESTCONTACTID";
		this.id = id;
    this.copyId = copyId;
		this.requestId = requestId;
	}

	/**
	 * @return the PK's ID.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return Copy ID (this PK doesn't have one).
	 */
	public int getCopyId() {
		return this.copyId;
	}
	
	/**
	 * @return PK's request ID.
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @return The PK's name.
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
	 */
	public String getWhereClause() {
    String retWhere = " where " + getName() + " = "+ String.valueOf( getId()) +
    " AND copyId = " + String.valueOf(getCopyId());

    return retWhere;
	}
	
  /**
   * Tests whether this ServiceRequestContactPK is equal to another.
   * @param object Object to test for equality.
   * @return Returns <code>true</code> if objects are equal.
   */
	public boolean equals(Object object) {
    if (this == object)
      return true;
    else if (object == null || getClass() != object.getClass())
      return false;

    ServiceRequestContactPK other = (ServiceRequestContactPK) object;
    boolean retval = false;

    if (getId() == other.getId() && getCopyId() == other.getCopyId() &&
    		getRequestId() == other.getRequestId() &&
        getName().equals(other.getName())) {
    	retval = true;
    }

    return retval;
  }
	
	/**
	 * Create a hash of this primary key.
	 * @returns A hash of this primary key.
	 */
	public int hashCode() {
		return name.hashCode() ^ id;
	}

}
