package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class DcFolderPK implements Serializable , IEntityBeanPK{
  private String name;
  private int id;

  public DcFolderPK(int id) {
    this.name = "dcFolderId"; 
    this.id = id;
  }

  public int getId(){return this.id;}

  public int getCopyId(){return -1;}

  public String getName(){return this.name;}

  public String getWhereClause(){
    String retWhere = " where " + getName() + " = " + String.valueOf( getId()) ;
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    DcFolderPK other  = (DcFolderPK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
      return name.hashCode() ^ id ;
  }
}
