/**
 * <p>Title: DealPurposePK.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jun 5, 2006)
 *
 */

package com.basis100.deal.pk;

import java.io.Serializable;

public class DealPurposePK implements IEntityBeanPK, Serializable
{
    private String name;
    private int id;
    
    public DealPurposePK( int id)
    {
        this.name = "dealPurposeId";
        this.id = id;

    }

    public int getId()
    {
        return id;
    }

    public int getCopyId()
    {
        return -1;
    }

    public String getName()
    {
        return name;
    }

    public String getWhereClause()
    {
        String retWhere = " where " + getName() + " = " + String.valueOf( getId()) ;
        return retWhere;
    }

      public boolean equals(Object object)
      {
        if(this == object)
          return true;
        else if(object == null || getClass() != object.getClass() )
          return false;

        DealPurposePK other  = (DealPurposePK)object;
        boolean retval = false;

        if(getId()== other.getId() && getName().equals(other.getName()) ) retval = true;

        return retval;
      }

      public int hashCode()
      {
         return (name.hashCode() ^ id);
      }
    }
