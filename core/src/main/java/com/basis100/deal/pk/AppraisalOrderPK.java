package com.basis100.deal.pk;

/**
 * Title: AppraisalOrderPK
 * Description: Primary Key of AppraisalOrder Entitye
 * Copyright:   Copyright (c) 2004
 * Company:     Filogix Inc
 * @author      Billy Lam
 * @version 1.0
 */

import java.io.Serializable;

public class AppraisalOrderPK implements Serializable,IEntityBeanPK
{
  private String name;
  private int id;
  private int copyId;

  public AppraisalOrderPK( int id, int copyId)
  {
    this.name = "appraisalOrderId";
    this.id = id;
    this.copyId = copyId;
  }
  public int getId(){return this.id;}
  public int getCopyId(){return this.copyId;}
  public String getName(){return this.name;}
  public String getWhereClause(){
    String retWhere = " where " + getName() + " = " +String.valueOf( getId()) +
       " AND copyId = " + String.valueOf(getCopyId());
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    AppraisalOrderPK other  = (AppraisalOrderPK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
     return name.hashCode() ^ (id + copyId);
  }
}