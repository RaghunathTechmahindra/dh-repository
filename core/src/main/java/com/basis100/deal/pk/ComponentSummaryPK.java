/*
 * @(#)ComponentSummaryPK.java May 15, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * 
 */
public class ComponentSummaryPK implements IEntityBeanPK, Serializable {
    private String name;
    private int    id;
    private int    copyId;

    public ComponentSummaryPK(int id, int copyId){
        this.name = "dealId";
        this.id = id;
        this.copyId = copyId;
    }

    /* (non-Javadoc)
     * @see com.basis100.deal.pk.IEntityBeanPK#getCopyId()
     */
    public int getCopyId() {
        return copyId;
    }

    /* (non-Javadoc)
     * @see com.basis100.deal.pk.IEntityBeanPK#getId()
     */
    public int getId() {
        return id;
    }

    /* (non-Javadoc)
     * @see com.basis100.deal.pk.IEntityBeanPK#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
     */
    public String getWhereClause() {
        String retWhere = " where " + getName() + " = "
        + String.valueOf(getId()) + " AND copyId = "
        + String.valueOf(getCopyId());
        return retWhere;
    }
    
    /**
     * 
     */
    public boolean equals(Object object)
    {
      if(this == object)
        return true;
      else if(object == null || getClass() != object.getClass() )
        return false;

      ComponentSummaryPK other  = (ComponentSummaryPK)object;
      boolean retval = false;

      if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

      return retval;
    }

    public int hashCode()
    {
       return (name + id + copyId).hashCode();
    }
    
    public String toString()
    {
      String out = this.getClass().getName();
      out += " name:" + getName() + " id:" + getId() + " copy:" + getCopyId();
      return out;
    }

}
