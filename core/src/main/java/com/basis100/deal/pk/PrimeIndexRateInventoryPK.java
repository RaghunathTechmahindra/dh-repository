package com.basis100.deal.pk;

import java.io.Serializable;

//--Ticket#781--XD_ARM/VRM--19Apr2005--start--//

public class PrimeIndexRateInventoryPK implements Serializable, IEntityBeanPK
{
  private String name;
  private int id;

   public PrimeIndexRateInventoryPK ( int id)
  {
    this.name = "primeIndexRateInventoryId";
    this.id = id;
  }

  public int getId(){return this.id;}
  public int getCopyId(){return -1;}
  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " + String.valueOf( getId());
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    PrimeIndexRateInventoryPK other  = (PrimeIndexRateInventoryPK)object;
    boolean retval = false;

    if(getId()== other.getId()  && getName().equals(other.getName()) ) retval = true;

    return retval;
  }
  public int hashCode()
  {
      return name.hashCode() ^ id ;
  }
}
