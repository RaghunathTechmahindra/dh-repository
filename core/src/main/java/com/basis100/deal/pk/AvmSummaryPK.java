/**
 * 
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author ESteel
 *
 */
public class AvmSummaryPK implements Serializable, IEntityBeanPK {
	
	private String name;
	private int id;

	/**
	 * Constructor.
	 */
	public AvmSummaryPK(int id) {
		this.name="RESPONSEID";
		this.id = id;
	}

	public boolean equals(Object object) {
    if (this == object)
      return true;
    else if (object == null || getClass() != object.getClass())
      return false;

    AvmSummaryPK other = (AvmSummaryPK) object;
    boolean retval = false;

    if (getId() == other.getId() && getCopyId() == other.getCopyId() &&
        getName().equals(other.getName())) 
    	retval = true;

    return retval;
	}
	
	
	/**
	 * @return id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return The copy id. (There isn't one in this case, so returns -1) 
	 */
	public int getCopyId() {
		return -1;
	}

	/**
	 * @return The name of the primary key column.
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
	 */
	public String getWhereClause() {
		return " WHERE " + getName() + " = " + String.valueOf(getId());
	}
	
	public int hashCode() {
		return name.hashCode() ^ id;
	}

}
