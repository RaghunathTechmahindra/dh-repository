/*
 * @(#)ComponentPK.java Apr 29, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * 
 */
public class ComponentPK implements Serializable, IEntityBeanPK {

    private String name;
    private int    id;
    private int    copyId;
    
    public ComponentPK(int id, int copyId){
        this.name = "componentId";
        this.id = id;
        this.copyId = copyId;
    }
    /**
     * 
     */
    public int getCopyId() {
        return this.copyId;
    }

    /**
     * 
     */
    public int getId() {
        return this.id;
    }

    /**
     * 
     */
    public String getName() {
        return this.name;
    }

    /**
     * 
     */
    public String getWhereClause() {
        String retWhere = " where " + getName() + " = "
                + String.valueOf(getId()) + " AND copyId = "
                + String.valueOf(getCopyId());
        return retWhere;
    }
    
    /**
     * 
     */
    public boolean equals(Object object)
    {
      if(this == object)
        return true;
      else if(object == null || getClass() != object.getClass() )
        return false;

      ComponentPK other  = (ComponentPK)object;
      boolean retval = false;

      if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

      return retval;
    }
    
    public int hashCode()
    {
       return (name + id + copyId).hashCode();
    }
    
    public String toString()
    {
      String out = this.getClass().getName();
      out += " name:" + getName() + " id:" + getId() + " copy:" + getCopyId();
      return out;
    }
}
