/*
 * @(#)ComponentOverdraftPK.java Apr 29, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>
 * Title: ComponentCreditCardPK
 * </p>
 * <p>
 * Description: This class represents a primary key for the ComponentCreditCard
 * entity.
 * </p>
 * @version 1.0 Initial Version
 * @version 1.1 15-Jul-2008 Modified equals(..) method
 */
public class ComponentCreditCardPK implements Serializable, IEntityBeanPK
{

    private String name;

    private int id;

    private int copyId;

    public ComponentCreditCardPK(int id, int copyId)
    {
        this.name = "componentId";
        this.id = id;
        this.copyId = copyId;
    }

    /**
     * 
     */
    public int getCopyId()
    {
        return this.copyId;
    }

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * 
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * 
     */
    public String getWhereClause()
    {
        String retWhere = " where " + getName() + " = "
                + String.valueOf(getId()) + " AND copyId = "
                + String.valueOf(getCopyId());
        return retWhere;
    }

    /**
     * <p>
     * Description: Checks the equality of the two objects
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 15-Jul-2008 Modified 'ComponentPK' to
     *          'ComponentCreditCardPK'
     * @return boolean - retuns the boolean value after comparing the two
     *         objects
     */
    public boolean equals(Object object)
    {
        if (this == object)
            return true;
        else
            if (object == null || getClass() != object.getClass())
                return false;

        ComponentCreditCardPK other = (ComponentCreditCardPK) object;
        boolean retval = false;

        if (getId() == other.getId() && getCopyId() == other.getCopyId()
                && getName().equals(other.getName()))
            retval = true;

        return retval;
    }

    public int hashCode()
    {
       return (name + id + copyId).hashCode();
    }
    
    public String toString()
    {
      String out = this.getClass().getName();
      out += " name:" + getName() + " id:" + getId() + " copy:" + getCopyId();
      return out;
    }
}
