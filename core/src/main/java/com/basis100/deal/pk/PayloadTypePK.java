/**
 * <p>Title: PayloadTypePK.java</p>
 *
 * <p>Description: Primary Key for PayloadType</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jul 5, 2006)
 *
 */

package com.basis100.deal.pk;

import java.io.Serializable;

public class PayloadTypePK implements IEntityBeanPK, Serializable
{
        private String name;
        private int id;
        
        public PayloadTypePK(int id) {
            this.name = "PayloadTypeID";
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }
        
        public int getCopyId() {
            return -1;
        }

        public String getWhereClause() {
            String retWhere = " WHERE " + getName() + " = " + String.valueOf(getId());
            
            return retWhere;
        }
        
      public boolean equals(Object object) {
        if (this == object)
          return true;
        else if (object == null || getClass() != object.getClass())
          return false;

        PayloadTypePK other = (PayloadTypePK) object;
        boolean retval = false;

        if (getId() == other.getId() && getName().equals(other.getName())) 
            retval = true;

        return retval;
      }

      public int hashCode() {
        return name.hashCode() ^ id;
      }
    }
