package com.basis100.deal.pk;

import java.io.Serializable;

public class SystemTypePK implements Serializable , IEntityBeanPK{

	  private String name;
	  private int id;


	  public SystemTypePK( int id)
	  {
		this.name = "systemTypeId";
		this.id = id;

	  }
	  public int getId(){return this.id;}

	  public int getCopyId(){return -1;}
	  
	  public String getName(){return this.name;}

	  public String getWhereClause(){
	    String retWhere = " where " + getName() + " = " + String.valueOf( getId()) ;
	    return retWhere;
	  }

	  public boolean equals(Object object)
	  {
	    if(this == object)
	      return true;
	    else if(object == null || getClass() != object.getClass() )
	      return false;

	    SystemTypePK other  = (SystemTypePK)object;
	    boolean retval = false;

	    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

	    return retval;
	  }

	   public int hashCode()
	  {
	      return name.hashCode() ^ id ;
	  }
	}

