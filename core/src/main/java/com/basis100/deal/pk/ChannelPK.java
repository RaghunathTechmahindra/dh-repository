/*
 * Primary Key for Channel
 * 
 * Created: 06-FEB-2006
 */
package com.basis100.deal.pk;

import java.io.Serializable;

public class ChannelPK implements IEntityBeanPK, Serializable {

	private String name;
	private int id;
	
	public ChannelPK(int id) {
		this.name = "CHANNELID";
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}
	
	public int getCopyId() {
		return -1;
	}

	public String getWhereClause() {
		String retWhere = " WHERE " + getName() + " = " + String.valueOf(getId());
		
		return retWhere;
	}
	
  public boolean equals(Object object) {
    if (this == object)
      return true;
    else if (object == null || getClass() != object.getClass())
      return false;

    ChannelPK other = (ChannelPK) object;
    boolean retval = false;

    if (getId() == other.getId() && getCopyId() == other.getCopyId() &&
        getName().equals(other.getName())) 
    	retval = true;

    return retval;
  }

  public int hashCode() {
    return name.hashCode() ^ id;
  }
}
