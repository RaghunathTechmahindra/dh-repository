package com.basis100.deal.pk;

import java.io.Serializable;

public class LenderToBranchAssocPK implements Serializable
{
  private String name;
  private int lenderId;
  private int branchId;
  private int contactId;

  public LenderToBranchAssocPK(int lenderProfileId, int branchProfileId, int contactId)
  {
	  this.name = "lenderToBranchAssoc";
	  this.lenderId = lenderProfileId;
    this.branchId = branchProfileId;
    this.contactId = contactId;
  }


  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where lenderProfileId = " + lenderId + " and branchProfileId = " +
    branchId + " and contactId = " + contactId;
    return retWhere;
  }

  public int hashCode()
  {
      String t = "" + lenderId + branchId + contactId;
      return name.hashCode() ^ t.hashCode();
  }
}

 

