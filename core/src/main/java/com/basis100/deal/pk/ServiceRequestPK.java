package com.basis100.deal.pk;

import java.io.Serializable;

public class ServiceRequestPK implements Serializable , IEntityBeanPK{
	
	private String name;
	private int id;
  private int copyId;
	
	/**
	 * Constructor. Create a PK with this id, and <code>name</code> set to the
	 * id's name.
	 * @param id ID to set.
	 */
	public ServiceRequestPK(int id, int copyId) {
		this.name = "REQUESTID"; 
		this.id = id;
    this.copyId = copyId;
	}
	
	/**
	 * @return This PK's ID.
	 */
	public int getId(){return this.id;}
	
	/**
	 * @return This PK's ID's name.
	 */
	public String getName(){return this.name;}
	
	public int getCopyId() {		return this.copyId;	}

  public void setCopyId(int copyId){ this.copyId = copyId;}

  
	/**
	 * @return SQL specifying the record unambiguously.
	 */
	public String getWhereClause(){
    String retWhere = " where " + getName() + " = "+ String.valueOf( getId()) +
    " AND copyId = " + String.valueOf(getCopyId());
    return retWhere;
	}
	
	/**
	 * Test for equality.
	 * @param object Object to test for equality with this PK.
	 * @return <code>true</code> if the object is a <code>ServiceRequestPK</code>
	 *  with equal ID, <code>false</code> otherwise.
	 */
	public boolean equals(Object object)
	{
		if(this == object)
			return true;
		else if(object == null || getClass() != object.getClass() )
			return false;
		
		ServiceRequestPK other  = (ServiceRequestPK)object;
		boolean retval = false;
		
		if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;
		
		return retval;
	}

 
	/**
	 * Hash this PK.
	 * @return a unique hash of this PK.
	 */
	public int hashCode()
	{
		return name.hashCode() ^ id ;
	}
}
