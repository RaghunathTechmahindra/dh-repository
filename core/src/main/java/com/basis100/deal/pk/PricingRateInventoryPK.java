package com.basis100.deal.pk;

import java.io.Serializable;
// Eden 06/29/2000;

public class PricingRateInventoryPK implements Serializable, IEntityBeanPK
{
  private String name;
  private int id;

   public PricingRateInventoryPK ( int id)
  {
	this.name = "PricingRateInventoryID";  // Double Check it..
	this.id = id;
  }

  public int getId(){return this.id;}
  public int getCopyId(){return -1;}
  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " + String.valueOf( getId());
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    PricingRateInventoryPK other  = (PricingRateInventoryPK)object;
    boolean retval = false;

    if(getId()== other.getId()  && getName().equals(other.getName()) ) retval = true;

    return retval;
  }
  public int hashCode()
  {
      return name.hashCode() ^ id ;
  }
}
