package com.basis100.deal.pk;

public class UserProfileBeanPK implements IEntityBeanPK
{
	int userProfileId;
    String name;
    int institutionProfileId;


	public UserProfileBeanPK(int userProfileId, int institutionProfileId)
	{
		this.userProfileId = userProfileId;
		this.name = "userProfileId";
        this.institutionProfileId = institutionProfileId;
	}
	public int getUserProfileId()
	{
		return userProfileId;
	}
	public void setUserProfileId(int userProfileId)
	{
		this.userProfileId = userProfileId;
	}
	public String toString()
	{
		return "UserProfileBeanPK(userProfileId=" + userProfileId 
        + ", institutionProfileId=" + institutionProfileId + ")";
	}

  public int getId(){return this.userProfileId;}
  public int getCopyId(){return -1;}
  public String getName(){return this.name;}
  public int getInstitutionId() {
      return this.institutionProfileId;
  }

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " 
               + String.valueOf( getId()) + " AND INSTITUTIONPROFILEID=" 
               + this.institutionProfileId;
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    UserProfileBeanPK other  = (UserProfileBeanPK)object;

    return (other != null)
        && (getId()== other.getId() )
        && (getCopyId() == other.getCopyId() )
        && (getInstitutionId() == other.getInstitutionId());

  }

  public int hashCode()
  {
    return name.hashCode() ^ userProfileId;
  }

  
}
