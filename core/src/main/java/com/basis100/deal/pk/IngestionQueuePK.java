package com.basis100.deal.pk;

import java.io.Serializable;

import com.basis100.deal.entity.IngestionQueue;
import com.basis100.deal.pk.IEntityBeanPK;

public class IngestionQueuePK implements Serializable, IEntityBeanPK {

	private static final long serialVersionUID = -3819260070280089392L;

	private int id;

	public IngestionQueuePK(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public int getCopyId() {
		throw new UnsupportedOperationException();
	}

	public String getName() {
		return IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID;
	}

	public String getWhereClause() {
		return " WHERE " + getName() + " = " + String.valueOf(getId());
	}

	public boolean equals(Object object) {
		if (this == object)
			return true;
		else if (object == null || getClass() != object.getClass())
			return false;
		IngestionQueuePK other = (IngestionQueuePK) object;
		return getId() == other.getId();
	}

	public int hashCode() {
		return getName().hashCode() ^ id;
	}

	public String toString() {
		return this.getClass().getName() + " name=" + getName() + " id=" + getId();
	}

}
