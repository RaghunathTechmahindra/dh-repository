/*
 * @(#)ComponentLOCPK.java Jun 09, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>
 * Title: ComponentLOCPK
 * </p>
 * <p>
 * Description: This class represents a primary key for the ComponentLOC entity.
 * </p>
 * @author MCM Impl Team
 * @Date 09-Jun-2008
 * @version 1.0 XS_11.11 Initial Version
 */
public class ComponentLOCPK implements Serializable, IEntityBeanPK
{
    // Instance Variables
    /** primary key name of the ComponentLOC entity. */
    private String name;

    /** the componentId is the id of the deal. */
    private int id;

    /** the copyId is the copyid of the deal. */
    private int copyId;

    /**
     * <p>
     * Description: Creates the ComponentLOC entity primary key by taking the
     * deal id and copyid.
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     */
    public ComponentLOCPK(int id, int copyId)
    {
        this.name = "componentId";
        this.id = id;
        this.copyId = copyId;
    }

    /**
     * <p>
     * Description: Returns the Component LOC copy id value.
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return int copyid of the deal.
     */
    public int getCopyId()
    {
        return this.copyId;
    }

    /**
     * <p>
     * Description: Returns the Component id value.
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return int deal id value.
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * <p>
     * Description: Returns the Primary key name for the componentLOC
     * </p>
     * @version 1.0 XS_11.11 Initial Version Returns the primary key field
     * @return String name of the primary key field
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * <p>
     * Description: Returns the where clause condition.
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return String - where condintion.
     */
    public String getWhereClause()
    {
        String retWhere = " where " + getName() + " = "
                + String.valueOf(getId()) + " AND copyId = "
                + String.valueOf(getCopyId());
        return retWhere;
    }

    /**
     * <p>
     * Description: Checks the equality of the two objects
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return boolean - retuns the boolean value after comparing the two
     *         objects
     */

    public boolean equals(Object object)
    {
        if (this == object)
            return true;
        else
            if (object == null || getClass() != object.getClass())
                return false;

        ComponentLOCPK other = (ComponentLOCPK) object;
        boolean retval = false;

        if (getId() == other.getId() && getCopyId() == other.getCopyId()
                && getName().equals(other.getName()))
            retval = true;

        return retval;
    }

    /**
     * <p>
     * Description: Calculates the hash code value
     * </p>
     * @version 1.0 XS_11.11 Initial Version
     * @return int - returns the calculated hashcode
     */
    public int hashCode()
    {
        return (name + id + copyId).hashCode();
    }

    
    public String toString()
    {
      String out = this.getClass().getName();
      out += " name:" + getName() + " id:" + getId() + " copy:" + getCopyId();
      return out;
    }
}
