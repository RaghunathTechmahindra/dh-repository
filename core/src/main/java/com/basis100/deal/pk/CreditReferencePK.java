package com.basis100.deal.pk;

import java.io.Serializable;

public class CreditReferencePK implements Serializable, IEntityBeanPK
{
  private String name;
  private int id;
  private int copyId;

  public CreditReferencePK( int id, int copyId)
  {
    this.name = "creditReferenceId";
    this.id = id;
    this.copyId = copyId;
  }

  public int getId(){return this.id;}

  public int getCopyId(){return this.copyId;}

  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " + String.valueOf( getId()) +
                      " AND copyId = " + String.valueOf(getCopyId());
    return retWhere;
  }

   /**
  *  @return true if
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    CreditReferencePK other  = (CreditReferencePK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
     return (name + id + copyId).hashCode();
  }
}
