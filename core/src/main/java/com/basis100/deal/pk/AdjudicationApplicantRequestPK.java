package com.basis100.deal.pk;

import java.io.Serializable;

public class AdjudicationApplicantRequestPK implements Serializable,IEntityBeanPK
{
  private String name;
  private int id;
  private int applicantNumber;
  private int copyId;
	public static final String REQUESTID = "requestId";
	public static final String APPLICANTNUMBER = "applicantNumber";
	public static final String COPYID = "copyId";


  public AdjudicationApplicantRequestPK( int id, int appnum,int copyId)
  {
    this.name = "requestId";
    this.id = id;
    this.applicantNumber = appnum;
    this.copyId = copyId;
  }

  public int getId(){return this.id;}
  public int getCopyId(){return this.copyId;}
  public String getName(){return this.name;}
  public int getAppNum() {
		return this.applicantNumber;
	}

  public String getWhereClause()
  {
 	String retWhere = " WHERE " + REQUESTID + " = " + String.valueOf( getId() ) + " AND " + APPLICANTNUMBER + " = " + String.valueOf( getAppNum() ) + " AND " + COPYID + " = " + String.valueOf( getCopyId() );
       
    return retWhere;
  }

   /**
  *  @return true if
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    AdjudicationApplicantPK other  = (AdjudicationApplicantPK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
     return (name + id + copyId).hashCode();
  }
}
