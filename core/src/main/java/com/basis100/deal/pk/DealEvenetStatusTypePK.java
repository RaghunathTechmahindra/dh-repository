/**
 * <p>Title: DealEvenetStatusTypePK.java</p>
 *
 * <p>Description: Primary Key class for DealEvenetStatusType</p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 28-Apr-09
 *
 */
package com.basis100.deal.pk;

import java.io.Serializable;

public class DealEvenetStatusTypePK implements IEntityBeanPK, Serializable {

    int id;
    String name = "DEALEVENTSTATUSTYPEID";
    
    public int getCopyId() {
        return 0;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getWhereClause() {
        return " WHERE " + getName() + " = " + id;
    }

    /**
     *  @return true if
     */
     public boolean equals(Object object){
         if(this == object)
             return true;
         else if(object == null || getClass() != object.getClass() )
             return false;

         DealEvenetStatusTypePK other  = (DealEvenetStatusTypePK)object;
         boolean retval = false;

         if(getId()== other.getId() && getName().equals(other.getName()) ) retval = true;

         return retval;
     }

     public int hashCode() {
        return (name + id).hashCode();
     }

}
