package com.basis100.deal.pk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Title: QuickLinksBeanPK.java
 * </p>
 * <p>
 * Description: Primary Key class for Quick Link Entity
 * </p>
 * 
 * @author
 * @version 1.0 (Initial Version � May 25, 2011)
 */
public class QuickLinksBeanPK implements IEntityBeanPK {

	private final static Logger  logger = LoggerFactory.getLogger(QuickLinksBeanPK.class);
	
	int quickLinkId;
	String name;

	/**
	 * <p>
	 * constructor
	 * </p>
	 * 
	 * @param quickLinkId : quick link id
	 * 
	 */
	public QuickLinksBeanPK(int quickLinkId) {
		this.quickLinkId = quickLinkId;
		this.name = "quickLinkId";
	}
	
	// get and set methods
	public int getQuickLinkId() {
		return quickLinkId;
	}

	public void setQuickLinkId(int quickLinkId) {
		this.quickLinkId = quickLinkId;
	}

	public String toString() {
		return "QuickLinksBeanPK (quickLinksId=" + quickLinkId + ")";
				
	}

	public int getId() {
		return this.quickLinkId;
	}

	public int getCopyId() {
		return -1;
	}

	public String getName() {
		return this.name;
	}


	public String getWhereClause() {
		String retWhere = " where " + getName() + " = "
				+ String.valueOf(getId());
		return retWhere;
	}

	public boolean equals(Object object) {
		if (this == object)
			return true;
		else if (object == null || getClass() != object.getClass())
			return false;

		QuickLinksBeanPK other = (QuickLinksBeanPK) object;

		return (other != null) && (getId() == other.getId())
				&& (getCopyId() == other.getCopyId());

	}

	public int hashCode() {
		return name.hashCode() ^ quickLinkId;
	}
}
