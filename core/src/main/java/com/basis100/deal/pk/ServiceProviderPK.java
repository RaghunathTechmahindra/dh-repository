/*
 * serviceProviderPK.java
 * 
 * Primary Key for ServiceProvider class
 * 
 * Created: 07-FEB-2006
 * Author: Edward Steel
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * Primary Key for ServiceProvider table
 * 
 * @author ESteel
 */
public class ServiceProviderPK implements Serializable, IEntityBeanPK {
	private String name;
	private int id;


	/**
	 * Constructor. Create a pk with specified ID and copy ID.
	 * <code>name</code> will be set to the default, the name of the ID. 
	 * @param id The ID to set.
	 * @param copyId The copy ID to set.
	 */
	public ServiceProviderPK(int id) {
		this.name = "SERVICEPROVIDERID";
		this.id   = id;
	}

	/**
	 * @see com.basis100.deal.pk.IEntityBeanPK#getId()
	 */
	public int getId() {
		return id;
	}

  public int getCopyId() {
    return -1;
  }
  
	/**
	 * @see com.basis100.deal.pk.IEntityBeanPK#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
	 */
	public String getWhereClause() {
		return " WHERE " + getName() + " = " + getId();
	}
	
  /**
   * Tests whether this serviceProviderPK is equal to another.
   * @param object Object to test for equality.
   * @return Returns <code>true</code> if objects are equal.
   */
	public boolean equals(Object object) {
    if (this == object)
      return true;
    else if (object == null || getClass() != object.getClass())
      return false;

    ServiceProviderPK other = (ServiceProviderPK) object;
    boolean retval = false;

    if (getId() == other.getId() &&
        getName().equals(other.getName())) retval = true;

    return retval;
  }
	
	/**
	 * @return Hashing of this primary key.
	 */
	public int hashCode() {
		return name.hashCode() ^ id;
	}
}
