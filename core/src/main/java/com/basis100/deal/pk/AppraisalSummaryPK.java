/**
 * 
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author ESteel
 *
 */
public class AppraisalSummaryPK implements IEntityBeanPK, Serializable {
	
	private String name;
	private int id;

	/**
	 * 
	 */
	public AppraisalSummaryPK(int id) {
		name = "RESPONSEID";
		this.id = id;
	}

	/**
	 * @return The ID.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return The Copy ID (-1 always, in this case).
	 */
	public int getCopyId() {
		return -1;
	}

	/**
	 * @return The Primary Key's column name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return The SQL <code>WHERE</code> clause that uniquely identifies this 
	 * record.
	 */
	public String getWhereClause() {
		return " WHERE " + getName() + " = " + String.valueOf(getId());
	}
	
  public boolean equals(Object object) {
    if (this == object)
      return true;
    else if (object == null || getClass() != object.getClass())
      return false;

    AppraisalSummaryPK other = (AppraisalSummaryPK) object;
    boolean retval = false;

    if (getId() == other.getId() && getCopyId() == other.getCopyId() &&
        getName().equals(other.getName())) 
    	retval = true;

    return retval;
  }

  public int hashCode() {
    return name.hashCode() ^ id;
  }
}
