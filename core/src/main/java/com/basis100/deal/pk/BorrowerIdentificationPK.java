package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>
 * Title: BorrowerIdentificationPK
 * </p>
 * 
 * <p>
 * Description: BorrowerIdentificationPK
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.3
 * 
 */
public class BorrowerIdentificationPK implements IEntityBeanPK, Serializable {

	private String name;

	private int id;

	private int copyId;

	/**
	 * 
	 */
	public BorrowerIdentificationPK() {
		super();
	}

	public BorrowerIdentificationPK(int id, int copyId) {
		this.name = "identificationId";
		this.id = id;
		this.copyId = copyId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.basis100.deal.pk.IEntityBeanPK#getId()
	 */
	public int getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.basis100.deal.pk.IEntityBeanPK#getCopyId()
	 */
	public int getCopyId() {
		return this.copyId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.basis100.deal.pk.IEntityBeanPK#getName()
	 */
	public String getName() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
	 */
	public String getWhereClause() {

		String retWhere = " where " + getName() + " = "
				+ String.valueOf(getId()) + " AND copyId = "
				+ String.valueOf(getCopyId());

		return retWhere;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object object) {
		if (this == object)
			return true;
		else if (object == null || getClass() != object.getClass())
			return false;

		BorrowerIdentificationPK other = (BorrowerIdentificationPK) object;
		boolean retval = false;

		if (getId() == other.getId() && getCopyId() == other.getCopyId()
				&& getName().equals(other.getName()))
			retval = true;

		return retval;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return (name + id + copyId).hashCode();
	}
}
