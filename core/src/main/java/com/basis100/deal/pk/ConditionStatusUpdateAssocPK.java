package com.basis100.deal.pk;

import java.io.Serializable;

public class ConditionStatusUpdateAssocPK implements IEntityBeanPK,
		Serializable {
	
	private int documentStatusId;
	private int systemId;
	
	public ConditionStatusUpdateAssocPK (int documentStatusId, int systemId)
	{
		this.documentStatusId = documentStatusId;
		this.systemId = systemId;
	}
	public int getCopyId() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getWhereClause() 
	{
		String retWhere = " where "
			+ "DOCUMENTSTATUSID = " + documentStatusId
			+ " and SYSTEMTYPEID = " + systemId;
		return retWhere;
	}

}
