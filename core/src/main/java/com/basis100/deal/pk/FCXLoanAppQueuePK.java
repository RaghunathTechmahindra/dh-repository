/**
 * 
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author Phil.Yuan
 *
 */
public class FCXLoanAppQueuePK implements Serializable, IEntityBeanPK {
	
	private int loanAppId;
	private String name;
	
	public FCXLoanAppQueuePK(int loanAppId){
		this.loanAppId = loanAppId;
		this.name = "FCXLoanAppQueueId";
	}

	/* (non-Javadoc)
	 * @see com.basis100.deal.pk.IEntityBeanPK#getCopyId()
	 */
	public int getCopyId() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.basis100.deal.pk.IEntityBeanPK#getId()
	 */
	public int getId() {
		return this.loanAppId;
	}

	/* (non-Javadoc)
	 * @see com.basis100.deal.pk.IEntityBeanPK#getName()
	 */
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
	 */
	public String getWhereClause() {
	    String retWhere = " where " + getName() + " = " + String.valueOf( getId());
	    return retWhere;
	}

}
