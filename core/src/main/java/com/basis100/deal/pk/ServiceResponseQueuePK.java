/*
 * ServiceResponseQueuePK.java:  Primary Key for the EventCallbackResponse table.
 * 
 * Created: 19-june-2006
 * Author:  Asif Atcha
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author AAtcha
 * 
 * *** WARNING ***
 * Althrough entity class, table and PK has copyid,
 * it's not used. Should be deleted
 * - Midori Sep 10, 2007
 */
public class ServiceResponseQueuePK implements IEntityBeanPK, Serializable {

	private int id;
	private String name;
	private int copyId;
	
	/**
	 * 
	 */
	public ServiceResponseQueuePK(int id) {
		this.id = id;
		this.name = "SERVICERESPONSEQUEUEID";
//		this.copyId = copyId;
	}

	 public int getId(){return this.id;}
	   public String getName(){return this.name;}
	   public int getCopyId(){return this.copyId;}
	   public String getWhereClause()
	   {
	    String retWhere = " where " + getName() + " = " + String.valueOf( getId()) ;
//	       " AND copyId = " + String.valueOf(getCopyId());
	    return retWhere;
	   }

  /**
   * @return A hashcode for this primary key.
   */
	 public int hashCode()
	  {
	     return (name + id + copyId).hashCode();
	  }
	
  /**
   * Tests whether this PK is equal to another (pk) object.
   * @param object Object to test for equality. 
   */
	public boolean equals(Object object) {
    if(this == object)
      return true;
    
    else if(object == null || getClass() != object.getClass() )
      return false;

    ServiceResponseQueuePK other  = (ServiceResponseQueuePK)object;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && 
    	 getName().equals(other.getName()) ) 
    	return true;

    return false;
  }
}
