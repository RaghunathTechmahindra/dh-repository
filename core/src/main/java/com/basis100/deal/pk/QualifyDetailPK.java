/*
 * @(#)QualifyDetailPK.java May 2, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * 
 */
public class QualifyDetailPK implements Serializable, IEntityBeanPK {
    private String name;
    private int    id;

    public QualifyDetailPK(int id){
        this.name = "dealId";
        this.id = id;
    }
    /**
     * 
     */
    public int getCopyId() {
        return -1;
    }

    /**
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * 
     */
    public String getWhereClause() {
        String retWhere = " where " + getName() + " = "
                + String.valueOf(getId()) ;
        return retWhere;
    }
    
    public boolean equals(Object object)
    {
      if(this == object)
        return true;
      else if(object == null || getClass() != object.getClass() )
        return false;

      QualifyDetailPK other  = (QualifyDetailPK)object;
      boolean retval = false;

      if(getId()== other.getId() && getName().equals(other.getName()) ) retval = true;

      return retval;
    }

}
