/*
 * ResponsePK.java:  Primary Key for the Response table.
 * 
 * Created: 07-FEB-2006
 * Author:  Edward Steel
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author ESteel
 */
public class ResponsePK implements IEntityBeanPK, Serializable {

    private int id;
    private String name;
    
    /**
     * Default Constructor. Creates a new PK and assigns the specified value to
     * <code>id</code> (<code>name</code> is assigned the default.).
     */
    public ResponsePK(int id) {
        this.id = id;
        this.name = "RESPONSEID";
    }

    /**
     * @see com.basis100.deal.pk.IEntityBeanPK#getId()
     */
    public int getId() { return id; }

    /**
     * @see com.basis100.deal.pk.IEntityBeanPK#getCopyId()
     */
    public int getCopyId() { return -1; }

    /**
     * @see com.basis100.deal.pk.IEntityBeanPK#getName()
     */
    public String getName() {
        return name;
    }

    /**
     * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
     */
    public String getWhereClause() {
        String sql = " WHERE " + getName() + " = " + String.valueOf(getId());
        
        return sql;
    }
    
  /**
   * @return A hashcode for this primary key.
   */
    public int hashCode() {
      return name.hashCode() ^ id ;
  }
    
  /**
   * Tests whether this PK is equal to another (pk) object.
   * @param object Object to test for equality. 
   */
    public boolean equals(Object object) {
    if(this == object)
      return true;
    
    else if(object == null || getClass() != object.getClass() )
      return false;

    ResponsePK other  = (ResponsePK)object;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && 
         getName().equals(other.getName()))  
        return true;

    return false;
  }

}
