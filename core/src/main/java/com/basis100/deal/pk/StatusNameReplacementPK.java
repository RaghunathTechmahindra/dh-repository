/**
 * <p>Title: StatusNameReplacementPK.java</p>
 *
 * <p>Description: Primary key class for StatusNameReplacement</p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 28-Apr-09
 *
 */
package com.basis100.deal.pk;

import java.io.Serializable;

public class StatusNameReplacementPK implements IEntityBeanPK, Serializable {

    int id;
    String name = "STATUSNAMEREPLACEMENTID";

    public int getCopyId() {
        return 0;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getWhereClause() {
        return " WHERE " + getName() + " = " + id;
    }
    /**
     *  @return true if
     */
     public boolean equals(Object object){
         if(this == object)
             return true;
         else if(object == null || getClass() != object.getClass() )
             return false;

         StatusNameReplacementPK other  = (StatusNameReplacementPK)object;
         boolean retval = false;

         if(getId()== other.getId() && getName().equals(other.getName()) ) retval = true;

         return retval;
     }

     public int hashCode() {
        return (name + id).hashCode();
     }

}
