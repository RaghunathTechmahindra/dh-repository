package com.basis100.deal.pk;

import java.io.Serializable;

public class MasterDealPK implements Serializable,IEntityBeanPK
{
  private static final String COLUMN_INSTITUTIONPROFILEID = "INSTITUTIONPROFILEID";
  private static final String COLUMN_DEALID = "DEALID";
  private String name;
  private int id;

  public MasterDealPK( int id)
  {
    this.name = COLUMN_DEALID;
    this.id = id;
  }  

  public int getId(){return this.id;}
  public int getCopyId(){return -1;}
  public String getName(){return this.name;}
  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " + String.valueOf( getId());
    return retWhere;
  }

  public String toString()
  {
    String out = this.getClass().getName();
    out += " name: " + getName() + " id: " + getId() ;
    return out;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    MasterDealPK other  = (MasterDealPK)object;
    boolean retval = false;

    if(getId()== other.getId() 
        && getName().equals(other.getName())) 
      retval = true;

    return retval;
  }

    public int hashCode()
  {
      return name.hashCode() ^ id ;
  }


}
