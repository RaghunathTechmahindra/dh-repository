package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>Title: BncAdjudicationApplRequestPK</p>
 * <p>Description: Primary key for the BncAdjudicationResponse entity class</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */

public class BncAdjudicationApplRequestPK
	implements Serializable, IEntityBeanPK
{
	private static final long serialVersionUID = 1L;
	private String name;
	private int id;
	private int applicantNumber;
	private int copyId;
	public static final String REQUESTID = "requestId";
	public static final String APPLICANTNUMBER = "applicantNumber";
	public static final String COPYID = "copyId";

	/**
	 * Constructor
	 * @param id Request ID.
	 * @param applicantNumber Value starting from 0.
	 */
	public BncAdjudicationApplRequestPK( int id , int applicantNumber, int copyId)
	{
		this.name = REQUESTID;
		this.id = id;
		this.applicantNumber = applicantNumber;
		this.copyId = copyId;
	}

	/**
	 * Return the request ID.
	 */
	public int getId()
	{
		return this.id;
	}

	/**
	 * Mandatory method, from the parent class, but 
	 * since there is no copy ID in the table return a dummy response.
	 */
	public int getCopyId()
	{
		return this.copyId;
	}

	/**
	 * Return the primary key column name.
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * 
	 * @return Value from 0 onward.
	 */
	public int getApplicantNumber() {
		return this.applicantNumber;
	}

	public String getWhereClause()
	{
		String retWhere = " WHERE " + REQUESTID + " = " + String.valueOf( getId() ) + " AND " + APPLICANTNUMBER + " = " + String.valueOf( getApplicantNumber() ) + " AND " + COPYID + " = " + String.valueOf( getCopyId() );
		return retWhere;
	}

	public boolean equals( Object object )
	{
		if ( this == object )
		{
			return true;
		}
		else if ( object == null || getClass() != object.getClass() )
		{
			return false;
		}
		BncAdjudicationApplRequestPK other = ( BncAdjudicationApplRequestPK ) object;
		boolean retval = false;
		if ( getId() == other.getId() && getName().equals( other.getName() ) )
		{
			retval = true;
		}
		return retval;
	}

	public int hashCode()
	{
		return ( name.hashCode() ^ id );
	}

}
