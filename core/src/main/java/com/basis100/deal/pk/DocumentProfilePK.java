package com.basis100.deal.pk;

import java.io.*;

public class DocumentProfilePK implements Serializable, IEntityBeanPK {

    private String name;

    private int id = -1;

    private String documentFullName;

    private String documentVersion;

    private int languagePreferenceId;

    private int lenderProfileId;

    private int documentTypeId;

    private String documentFormat;

    private int documentProfileStatus;

    public DocumentProfilePK(String version, int language, int lender,
            int type, String format)

    {
        this.name = "documentProfileId";
        this.documentFullName = "NULL";
        this.documentVersion = version;
        this.languagePreferenceId = language;
        this.lenderProfileId = lender;
        this.documentTypeId = type;
        this.documentFormat = format;
        this.documentProfileStatus = 0;
    }

    // Created by BILLY for compile -- Need to fix after the Build 3.2.0 --
    // 18Feb2001
    public DocumentProfilePK(int id) {
        this.name = "documentProfileId";
        this.id = id;
    }

    public int getId() {
        return -1;
    }

    public int getCopyId() {
        return -1;
    }

    public String getName() {
        return this.name;
    }

    public String getDocumentFullName() {
        return this.documentFullName;
    }

    public String getDocumentVersion() {
        return this.documentVersion;
    }

    public int getLanguagePreferenceId() {
        return this.languagePreferenceId;
    }

    public int getLenderProfileId() {
        return this.lenderProfileId;
    }

    public int getDocumentTypeId() {
        return this.documentTypeId;
    }

    public String getDocumentFormat() {
        return this.documentFormat;
    }

    public int getDocumentProfileStatus() {
        return this.documentProfileStatus;
    }

    public String getWhereClause() {
        String retWhere = " where documentVersion = '" + getDocumentVersion()
                + "'" + " AND languagePreferenceId = "
                + getLanguagePreferenceId() + " AND lenderProfileId = "
                + getLenderProfileId() + " AND documentTypeId = "
                + getDocumentTypeId() + " AND documentFormat = '"
                + getDocumentFormat() + "'" + "";

        return retWhere;
    }

    /**
     * @return true if
     */
    public boolean equals(Object object) {
        if (this == object)
            return true;
        else if (object == null || getClass() != object.getClass())
            return false;

        DocumentProfilePK other = (DocumentProfilePK) object;
        boolean retval = false;

        if (getId() == other.getId() && getName().equals(other.getName()))
            retval = true;

        return retval;
    }

    public int hashCode() {
        return name.hashCode() ^ id;
    }
}
