/*
 * ProcessStatusPK.java:  Primary Key for the ProcessStatus table.
 * 
 * Created: 19-june-2006
 * Author:  Asif Atcha
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author AAtcha
 */
public class ProcessStatusPK implements IEntityBeanPK, Serializable {

	private int id;
	private String name;
	
	/**
	 * 
	 */
	public ProcessStatusPK(int id) {
		this.id = id;
		this.name = "PROCESSSTATUSID";
	}

	/**
	 * @see com.basis100.deal.pk.IEntityBeanPK#getId()
	 */
	public int getId() { return id; }

	/**
	 * @see com.basis100.deal.pk.IEntityBeanPK#getCopyId()
	 */
	public int getCopyId() { return -1; }

	/**
	 * @see com.basis100.deal.pk.IEntityBeanPK#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @see com.basis100.deal.pk.IEntityBeanPK#getWhereClause()
	 */
	public String getWhereClause() {
		String sql = " WHERE " + getName() + " = " + String.valueOf(getId());
		
		return sql;
	}
	
  /**
   * @return A hashcode for this primary key.
   */
	public int hashCode() {
      return name.hashCode() ^ id ;
  }
	
  /**
   * Tests whether this PK is equal to another (pk) object.
   * @param object Object to test for equality. 
   */
	public boolean equals(Object object) {
    if(this == object)
      return true;
    
    else if(object == null || getClass() != object.getClass() )
      return false;

    ProcessStatusPK other  = (ProcessStatusPK)object;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && 
    	 getName().equals(other.getName()) ) 
    	return true;

    return false;
  }
}
