package com.basis100.deal.pk;

import java.io.Serializable;

public class LenderProfilePK implements Serializable, IEntityBeanPK
{
  private String name;
  private int id;

   public LenderProfilePK( int id)
  {
	  this.name = "lenderProfileId";
	  this.id = id;
  }  
  public int getId(){return this.id;}
  public int getCopyId(){return -1;}  
  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " + String.valueOf( getId());
    return retWhere;
  }

   /**
  *  @return true if
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    LenderProfilePK other  = (LenderProfilePK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
      return name.hashCode() ^ id ;
  }
}
