package com.basis100.deal.pk;

import java.io.Serializable;

public class ArchivedLoanDecisionPK implements Serializable,IEntityBeanPK
{
  private String name;
  private int id;

  public ArchivedLoanDecisionPK(int id)
  {
    this.name = "ArchivedLoanDecisionId";
    this.id = id;
  }

  public int getId(){return this.id;}

  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " + String.valueOf( getId());
    return retWhere;
  }
	/* (non-Javadoc)
	 * @see com.basis100.deal.pk.IEntityBeanPK#getCopyId()
	 * copyid is not part of the PK for this table
	 */
	public int getCopyId() {
		return 0;
	}
  /**
  *  @return true if
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    ArchivedLoanDecisionPK other  = (ArchivedLoanDecisionPK)object;
    boolean retval = false;

    if(getId()== other.getId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
     return (name + id).hashCode();
  }
}
