package com.basis100.deal.pk;

import java.io.Serializable;

public class PartyProfilePK implements Serializable,IEntityBeanPK
{
  private String name;
  private int id;

   public PartyProfilePK( int id)
  {
	this.name = "partyProfileId";
	this.id = id;
  }
  public int getId(){return this.id;}
  public int getCopyId(){return -1;}
  public String getName(){return this.name;}

  public String getWhereClause()
  {
    // Note copyId field is always 1 (by convention) for party profile records - the field is necessary
    // only to satisfy a DB constraint (FK relationship with CONTACT table)
    
    String retWhere = " where " + getName() + " = " + String.valueOf( getId()) + " AND copyid = 1";
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    PartyProfilePK other  = (PartyProfilePK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

   public int hashCode()
  {
      return name.hashCode() ^ id ;
  }
}
