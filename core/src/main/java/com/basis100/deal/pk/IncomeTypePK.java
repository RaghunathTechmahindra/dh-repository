package com.basis100.deal.pk;

import java.io.Serializable;

public class IncomeTypePK implements Serializable , IEntityBeanPK
{
  private String name;
  private int id;


  public IncomeTypePK( int id)
  {
	this.name = "incomeTypeId";
	this.id = id;

  }
  public int getId(){return this.id;}

  public int getCopyId(){return -1;}
  
  public String getName(){return this.name;}

  public String getWhereClause(){
    String retWhere = " where " + getName().toUpperCase() + " = " + String.valueOf( getId()) ;
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    IncomeTypePK other  = (IncomeTypePK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
      return name.hashCode() ^ id ;
  }
}
