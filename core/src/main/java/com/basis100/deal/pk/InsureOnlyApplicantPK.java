/*
 * Created on Nov 18, 2004
 *
 * TODO To change the template for this generated file go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * @author Neil Kong on Nov/18/2004
 */
public class InsureOnlyApplicantPK implements Serializable, IEntityBeanPK
{
  private String name;
  private int id;
  private int copyId;

  public InsureOnlyApplicantPK( int id, int copyId )
  {
    this.name = "insureOnlyApplicantId";
    this.id = id;
    this.copyId = copyId;
  }

  public int getId(){return this.id;}
  public String getName(){return this.name;}
  public int getCopyId(){return this.copyId;};
  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = " + String.valueOf( getId()) +
       " AND copyId = " + String.valueOf(getCopyId());
    return retWhere;
  }

   /**
  *  @return true if
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    InsureOnlyApplicantPK other  = (InsureOnlyApplicantPK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
     return (name + id + copyId).hashCode();
  }
}

