package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>Title: AdjudicationApplicantResponsePK</p>
 * <p>Description: Primary key for the AdjudicationApplicantResponse entity class</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */

public class AdjudicationApplicantResponsePK
	implements Serializable, IEntityBeanPK
{
	private String name;
	private int id;
	private int applicantNumber;

	public static final String RESPONSEID = "responseId";
	public static final String APPLICANTNUMBER = "applicantNumber";

	public AdjudicationApplicantResponsePK(int id, int appnum)
	{
		this.name = RESPONSEID;
		this.id = id;
		this.applicantNumber = appnum;
	}

	public int getId()
	{
		return this.id;
	}

	public int getCopyId()
	{
		return -1;
	}

	public int getApplicantNumber()
	{
		return applicantNumber;
	}

	public String getName()
	{
		return this.name;
	}

	public String getWhereClause()
	{
		String retWhere = " where " + getName() + " = " + String.valueOf(getId()) +
			" AND " + APPLICANTNUMBER + " = " +
			String.valueOf(getApplicantNumber());

		return retWhere;
	}

	/**
	 *  @return true if
	 */
	public boolean equals(Object object)
	{
		if (this == object)
		{
			return true;
		}
		else if (object == null || getClass() != object.getClass())
		{
			return false;
		}

		AdjudicationApplicantResponsePK other = (
			AdjudicationApplicantResponsePK)object;
		boolean retval = false;

		if (getId() == other.getId() &&
			getApplicantNumber() == other.getApplicantNumber() &&
			getName().equals(other.getName()))
		{
			retval = true;

		}
		return retval;
	}

	public int hashCode()
	{
		return (name + id + applicantNumber).hashCode();
	}

	public void setApplicantNumber(int applicantNumber)
	{
		this.applicantNumber = applicantNumber;
	}
}
