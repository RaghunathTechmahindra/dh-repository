/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 28-Apr-09
 *
 */
package com.basis100.deal.pk;

import java.io.Serializable;

public class DealEventStatusSnapshotPK implements IEntityBeanPK, Serializable {

    protected int dealId;
    private String name = "dealId";
    private int typeId;

    public DealEventStatusSnapshotPK(int dealId, int typeId) {
      this.name = "dealId";
      this.dealId = dealId;
      this.typeId = typeId;
    }

    public int getCopyId() {
        return 0;
    }

    public int getId() {
        return dealId;
    }

    public String getName() {
        return this.name;
    }

    public String getWhereClause() {
        return " WHERE " + getName() + " = " + dealId +
        " AND dealEventStatusTypeId = " + String.valueOf(getTypeId());
    }
    
    /**
     *  @return true if
     */
     public boolean equals(Object object){
         if(this == object)
             return true;
         else if(object == null || getClass() != object.getClass() )
             return false;

         DealEventStatusSnapshotPK other  = (DealEventStatusSnapshotPK)object;
         boolean retval = false;

         if(getId()== other.getId() && getName().equals(other.getName()) ) retval = true;

         return retval;
     }

     /*
     public int hashCode() {
        return (name + id).hashCode();
     }
     */
     
	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

}
