package com.basis100.deal.pk;

import java.io.Serializable;

public class ScotiabankAdditionalDetailsPK implements Serializable,IEntityBeanPK
{
  private String name;
  private int id;
  private int copyId;

  public ScotiabankAdditionalDetailsPK( int id, int copyId)
  {
	this.name = "dealId";
	this.id = id;
	this.copyId = copyId;
  }
  public int getId(){return this.id;}

  public void setCopyId(int copyId){ this.copyId = copyId;}

  public int getCopyId(){return this.copyId;}

  public String getName(){return this.name;}

  public String getWhereClause()
  {
    String retWhere = " where " + getName() + " = "+ String.valueOf( getId()) +
       " AND copyId = " + String.valueOf(getCopyId());
    return retWhere;
  }

  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    ScotiabankAdditionalDetailsPK other  = (ScotiabankAdditionalDetailsPK)object;
    boolean retval = false;

    if(getId()== other.getId() && getCopyId() == other.getCopyId() && getName().equals(other.getName()) ) retval = true;

    return retval;
  }

  public int hashCode()
  {
     return (name + id + copyId).hashCode();
  }
}
