package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>Title: BncAutoDeclineReasonPK</p>
 * <p>Description: Primary key for the BncAutoDeclineReason entity class</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */

public class BncAutoDeclineReasonPK
	implements Serializable, IEntityBeanPK
{
	private String name;
	private int id;
	private int autoDeclineReasonId;
	public static final String RESPONSEID = "responseId";
	public static final String AUTODECLINEREASONID = "autoDeclineReasonId";

	public BncAutoDeclineReasonPK(int id, int autoDeclineReasonId)
	{
		this.name = RESPONSEID;
		this.id = id;
		this.autoDeclineReasonId = autoDeclineReasonId;
	}

	public int getId()
	{
		return this.id;
	}

	public int getCopyId()
	{
		return -1;
	}

	public int getAutoDeclineReasonId()
	{
		return autoDeclineReasonId;
	}

	public String getName()
	{
		return this.name;
	}

	public String getWhereClause()
	{
		String retWhere = " WHERE " + getName() + " = " + String.valueOf(getId()) +
			" AND " + AUTODECLINEREASONID + " = " +
			String.valueOf(getAutoDeclineReasonId());
		return retWhere;
	}

	public boolean equals(Object object)
	{
		if (this == object)
		{
			return true;
		}
		else if (object == null || getClass() != object.getClass())
		{
			return false;
		}
		BncAutoDeclineReasonPK other = (BncAutoDeclineReasonPK)object;
		boolean retval = false;
		if (getId() == other.getId() &&
			getAutoDeclineReasonId() == other.getAutoDeclineReasonId() &&
			getName().equals(other.getName()))
		{
			retval = true;
		}
		return retval;
	}

	public int hashCode()
	{
		return (name.hashCode() ^ id);
	}

}
