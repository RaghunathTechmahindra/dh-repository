package com.basis100.deal.pk;

public interface IEntityBeanPK
{
  public int getId();
  public int getCopyId();
  public String getName();
  public String getWhereClause();

  public boolean equals(Object object);
  public int hashCode();

}
