/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 12-May-09
 *
 */
package com.basis100.deal.pk;

import java.io.Serializable;

public class DealEventStatusUpdateAssocPK implements IEntityBeanPK, Serializable {

    private int systemTypeId;
    private int dealEventStatusTypeId;
    private int statusId;

    public DealEventStatusUpdateAssocPK(int systemTypeId, int dealEventStatusTypeId, int statusId) {
        this.systemTypeId = systemTypeId;
        this.dealEventStatusTypeId = dealEventStatusTypeId;
        this.statusId = statusId;
    }
    
    public int getCopyId() {
        return 0;
    }

    public int getId() {
        return 0;
    }

    public String getName() {
        return null;
    }

    public String getWhereClause() {
        return "systemTypeId = " + systemTypeId 
        + " and dealEventStatusTypeId = " + dealEventStatusTypeId
        + " and statusId = " + statusId;
    }

    public int getSystemTypeId() {
        return systemTypeId;
    }

    public void setSystemTypeId(int systemTypeId) {
        this.systemTypeId = systemTypeId;
    }

    public int getDealEventStatusTypeId() {
        return dealEventStatusTypeId;
    }

    public void setDealEventStatusTypeId(int dealEventStatusTypeId) {
        this.dealEventStatusTypeId = dealEventStatusTypeId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

}
