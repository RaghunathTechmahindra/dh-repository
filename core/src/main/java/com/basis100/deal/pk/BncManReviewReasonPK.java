package com.basis100.deal.pk;

import java.io.Serializable;

/**
 * <p>Title: BncManReviewResasonPK</p>
 * <p>Description: Primary key for the BncManReviewResason entity class</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */

public class BncManReviewReasonPK
	implements Serializable, IEntityBeanPK
{
	private String name;
	private int id;
	private int manReviewReasonId;
	public static final String RESPONSEID = "responseId";
	public static final String MANREVIEWREASONID = "manReviewReasonId";

	public BncManReviewReasonPK(int id, int manReviewResasonID)
	{
		this.name = RESPONSEID;
		this.id = id;
		this.manReviewReasonId = manReviewResasonID;
	}

	public int getId()
	{
		return this.id;
	}

	public int getCopyId()
	{
		return -1;
	}

	public int getManReviewReasonId()
	{
		return manReviewReasonId;
	}

	public String getName()
	{
		return this.name;
	}

	public String getWhereClause()
	{
		String retWhere = " WHERE " + getName() + " = " + String.valueOf(getId()) +
			" AND " + MANREVIEWREASONID + " = " +
			String.valueOf(getManReviewReasonId());
		return retWhere;
	}

	public boolean equals(Object object)
	{
		if (this == object)
		{
			return true;
		}
		else if (object == null || getClass() != object.getClass())
		{
			return false;
		}
		BncManReviewReasonPK other = (BncManReviewReasonPK)object;
		boolean retval = false;
		if (getId() == other.getId() &&
			getManReviewReasonId() == other.getManReviewReasonId() &&
			getName().equals(other.getName()))
		{
			retval = true;
		}
		return retval;
	}

	public int hashCode()
	{
		return (name.hashCode() ^ id);
	}

}
