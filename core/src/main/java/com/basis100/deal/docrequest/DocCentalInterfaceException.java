package com.basis100.deal.docrequest;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

class DocCentalInterfaceException extends Exception {
  public DocCentalInterfaceException()
  {
    super();
  }

  public DocCentalInterfaceException(String msg)
  {
    super(msg);
  }
}
