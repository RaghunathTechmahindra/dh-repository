package com.basis100.deal.docrequest;

/**
 * 31/Aug/2005 DVG #DG306 link to Closing services (FCT)- request
 * @version 1.2  <br>
 * Date: 12/08/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	--Added the logic to generate the NBC solicitor Package PDF document in requestDoc()
 *  -- Added new method requestNBCSolicitorsPackage() to generate the NBC Solicitor package (PDF)document
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.DocBusinessRuleExecutor;
import com.basis100.deal.validation.DocBusinessRuleResponse;
import com.basis100.deal.conditions.ConditionParser;
import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.docprep.Dc;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.message.ClientMessage;
import com.basis100.util.message.ClientMessageHandler;

import MosSystem.Mc;
import com.filogix.util.Xc;

//  For determining which doc type is desired.
import com.basis100.deal.docprep.xsl.XSLMerger;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.DocumentQueue;
import com.basis100.deal.entity.MailDestinationType;
import com.basis100.deal.entity.UserProfile;

/**
 *  The DocumentRequest class provides methods for making Document Generation Requests.
 *  To use this class create an instance and equip it with the appropriate profile and
 *  email parameters. Once the document request has been created and 'loaded' it can be
 *  executed. Executing the reqest places a record on the DocumentRequestQueue
 *  <br><p>
 * <b> To use:<br> </b>
 *  Instantiate a <code>DocumentRequest</code> instance using one of the availabe constructors
 *  set any additional values required in the request - such as emailSubject<br>
 *  When you are ready to make the request - call execute(). The execute return value
 *  indicates whether a reply is to be expected from this document issue.
 *  <br></p>
 *
 * Convience methods for requests by name for specific document types will be
 * included in future. <br>
 * ie requestINGCommitment(int dealid) *
 * 
 * @version 1.1 <br>
 *          Date: 06/06/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Made changes to requestDealSummary() methods added parameters for
 *          email message and document type<br>
 *          This is for generating pec email<br>
 *          <br>
 *  in future. <br>ie requestINGCommitment(int dealid)
 */

public class DocumentRequest implements Xc
{
  private SessionResourceKit srk;
  private DocumentQueue request;

  private int dealId;
  private int dealCopyId;
  private int requestorId;

  private String emailFullName = "";
  private String emailSubject = "";
  private String emailText = "";                                               // for BR-generated emailText
  private String scenarioNumber;

  private List emailAddressList;
  private List faxAddressList;
  private int emailTotalChars = 0;
  private boolean expectReply;

  public boolean debug = true;

  /**
  *  Construct a Request for a document o
  *
  *  @param srk: an initialized SessionResourceKit
  *  @param deal: the primary key of the deal to which the document relates.
  *  @param requestorUserId: the user id of the requestor of this document.
  *  @param emailAddress: a valid semicolon delimited String of recipient addresses.
  *  @param emailSubj:  a subject for the document delivery SMTP message.
  *  @param emailFullName: a name for the message to be included in the Body of the message.
  */
  private DocumentRequest(SessionResourceKit srk, int dealId, int copyId, int requestorUserId,
                          String emailAddress, String emailSubj, String emailFullName )throws DocPrepException
  {
    this.srk = srk;

    try
    {
      this.dealId = dealId;
      this.dealCopyId = copyId;
      this.requestorId = requestorUserId;
      this.emailAddressList = new ArrayList();
      this.faxAddressList = new ArrayList();
      this.addEmailAddress(emailAddress);
      this.emailSubject = emailSubj;
      this.emailFullName = emailFullName;
    }
    catch(Exception e)
    {
       throw new DocPrepException("Could not construct the DocumentRequest " + e);
    }
  }

  // for BR-generated emailText ---------------------
  private DocumentRequest(SessionResourceKit srk, int deal, int copyId, int requestorUserId,
                          String emailAddress, String emailSubj, String emailFullName, String emailText )throws DocPrepException
  {
    this.srk = srk;
    try
    {
      this.dealId = deal;
      this.dealCopyId = copyId;
      this.requestorId = requestorUserId;
      this.emailAddressList = new ArrayList();
      this.faxAddressList = new ArrayList();
      this.addEmailAddress(emailAddress);
      this.emailSubject = emailSubj;
      this.emailFullName = emailFullName;
      this.emailText = emailText;
    }
    catch(Exception e)
    {
       throw new DocPrepException("Could not construct the DocumentRequest " + e);
    }
  }
  // for BR-generated emailText end ---------------------

  private DocumentRequest(SessionResourceKit srk,  int deal, int copyId, int requestorUserId )throws DocPrepException
  {
    this.srk = srk;

    try
    {
      this.dealId = deal;
      this.dealCopyId = copyId;
      this.requestorId = requestorUserId;
    }
    catch(Exception e)
    {
       throw new DocPrepException("Could not construct the DocumentRequest " + e);
    }
  }

  /**
  *  Execute this document request
  *
  */
  private void request(Deal deal, DocumentProfilePK pk) throws DocPrepException
  {
    request(deal, pk, null);
  }

  /**
  *  Execute this document request
  *
  */
  private void request(Deal deal, DocumentProfilePK pk, String scenarioNumber ) throws DocPrepException
  {
    try
    {
      this.request = new DocumentQueue(srk);

      Date now = new Date();

      request.create(now,requestorId,getEmailAddress(),emailFullName,emailSubject,dealId,dealCopyId, scenarioNumber, pk, getFaxNumbers());
    }
    catch(Exception e)
    {
      String msg = "Request for document failed. ";
      if(e.getMessage()!=null) msg += e.getMessage();
      throw new DocPrepException(msg);
    }
  }

  private void requestRTF(Deal deal, int type, int lang) throws DocPrepException
  {
    requestDoc(deal, type, lang, XSLMerger.TYPE_RTF);
  }

  public static DocumentRequest requestDocByBusRules(SessionResourceKit srk, Deal deal, int type, String scenarioNum) throws DocPrepException
  {
    return DocumentRequest.requestDocByBusRules(srk, deal, type, null, 0);
  }

  public static DocumentRequest requestDocByBusRules(SessionResourceKit srk, Deal deal, int type, String scenarioNum,
      int clientMessageId) throws DocPrepException
  {
    srk.getSysLogger().info("requestDocByBusRules : Required for deal id:" + deal.getDealId() + " (" + deal.getCopyId() + ")" + ":type required:" + type);
    //==========================================================================
    // If DocBusinessRuleExecutor returns true we can proceed and create the
    // document request otherwise, go out.
    //==========================================================================
    DocumentRequest req = null;
    try{
      DocBusinessRuleExecutor dbre = new DocBusinessRuleExecutor(srk);
      DocBusinessRuleResponse response = dbre.evaluateRequest(deal,type);
      if( response.isCreate() == false ){
    	  srk.getSysLogger().info("requestDocByBusRules : "+" response from business rules is false - no document will be produced.");
        // when business rule returns false, there is no need to create document.
        return req;
      }
      srk.getSysLogger().info("requestDocByBusRules : "+" response from business rules is true - document will be produced.");
      String eMailSubject = null;
      String eMailFullName = null;
      String eMailText = null;                                                 // for BR-generated emailText
      if( response.getDestinationType() == DocBusinessRuleResponse.C_DESTINATION_TYPE_E_MAIL ){
    	srk.getSysLogger().info("requestDocByBusRules : " + "generating emailText...");                      // " document destination is e-Mail."
        eMailSubject = response.getEmailSubject();
        eMailFullName = response.getEMailFullName();

        eMailText = response.getEmailText();
                                                                // read from the business rules
        if ((eMailText == null) && (clientMessageId != 0)){                                                  // if br do not provide emailtext, read from the message table if client message is provided
          // check to see if email text comes from ClientMessageHandler
          eMailText = generateClientMessage(srk, clientMessageId, deal);
        }
        if (eMailText != null){
          req = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                    srk.getExpressState().getUserProfileId(),"",
                                    eMailSubject,eMailFullName, eMailText);
        } else {
          req = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                    srk.getExpressState().getUserProfileId(),"",     // old route
                                    eMailSubject,eMailFullName);
        }
        // for BR-generated emailText ---------------------
        if (type == Dc.DOCUMENT_REQUEST_APPRAISAL){  //For READ,Appraisal CR NBC/PP implementation
        	req.requestDoc(deal, type, response.getLanguage(), response.getOutputType(), Dc.VERSION_1_NBCAPPRAISAL );}
        else{
        req.requestDoc(deal, type, response.getLanguage(), response.getOutputType(), response.getDocumentVersion() );
        }
        srk.getSysLogger().info("requestDocByBusRules : "+"emailText generation finished");                // document destination is e-Mail. :finished:
      }else{
    	  srk.getSysLogger().info("requestDocByBusRules : "+" document destination is file.");
        req = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                  srk.getExpressState().getUserProfileId());

        DocumentProfilePK pk = new DocumentProfilePK(response.getDocumentVersion(),
                                    response.getLanguage(),
                                    response.getLenderProfileId(),
                                    type,
                                    response.getOutputTypeStr() );
          req.request(deal, pk, scenarioNum);
          srk.getSysLogger().info("requestDocByBusRules : "+" document destination is file. :finished:");
      }
    } catch(Exception brexc){
      srk.getSysLogger().error("Error while processing business rule. Deal: " + deal.getDealId() + " Copy:" + deal.getCopyId() + " type:" + type);
      srk.getSysLogger().error(brexc);
      throw new DocPrepException( brexc.getMessage() );
    }
    srk.getSysLogger().info("requestDocByBusRules : end of method :");
    return req;
  }

  
  
  public static DocumentRequest requestDocByBusRuleslang(SessionResourceKit srk, Deal deal, int type, String scenarioNum,
	      int clientMessageId) throws DocPrepException
	  {
	    srk.getSysLogger().info("requestDocByBusRules : Required for deal id:" + deal.getDealId() + " (" + deal.getCopyId() + ")" + ":type required:" + type);
	    //==========================================================================
	    // If DocBusinessRuleExecutor returns true we can proceed and create the
	    // document request otherwise, go out.
	    //==========================================================================
	    DocumentRequest req = null;
	    try{
	    	int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);
		   if(lang == -1)lang = DocPrepLanguage.ENGLISH;
	      DocBusinessRuleExecutor dbre = new DocBusinessRuleExecutor(srk);
	      DocBusinessRuleResponse response = dbre.evaluateRequest(deal,type,lang);
	      
	      if( response.isCreate() == false ){
	        srk.getSysLogger().info("requestDocByBusRules : "+" response from business rules is false - no document will be produced.");
	        // when business rule returns false, there is no need to create document.
	        return req;
	      }
	      srk.getSysLogger().info("requestDocByBusRules : "+" response from business rules is true - document will be produced.");
	      String eMailSubject = null;
	      String eMailFullName = null;
	      String eMailText = null;                                                 // for BR-generated emailText
	    
	      
	      if( response.getDestinationType() == DocBusinessRuleResponse.C_DESTINATION_TYPE_E_MAIL ){
	        srk.getSysLogger().info("requestDocByBusRules : " + "generating emailText...");                      // " document destination is e-Mail."
	        eMailSubject = response.getEmailSubject();
	        eMailFullName = response.getEMailFullName();

	        eMailText = response.getEmailText();
	                                                                // read from the business rules
	        if ((eMailText == null) && (clientMessageId != 0)){                                                  // if br do not provide emailtext, read from the message table if client message is provided
	          // check to see if email text comes from ClientMessageHandler
	          eMailText = generateClientMessage(srk, clientMessageId, deal);
	        }

	        if (eMailText != null){
	          req = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
	                                    srk.getExpressState().getUserProfileId(),"",
	                                    eMailSubject,eMailFullName, eMailText);
	        } else {
	          req = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
	                                    srk.getExpressState().getUserProfileId(),"",     // old route
	                                    eMailSubject,eMailFullName);
	        }
	        srk.getSysLogger().debug("Language Pref ID"+ response.getLanguage());
	        // for BR-generated emailText ---------------------
	        req.requestDoc(deal, type, lang , response.getOutputType(), response.getDocumentVersion() );
	        srk.getSysLogger().info("requestDocByBusRules : "+"emailText generation finished");                // document destination is e-Mail. :finished:
	      }
	      else{
	        srk.getSysLogger().info("requestDocByBusRules : "+" document destination is file.");
	        req = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
	                                  srk.getExpressState().getUserProfileId());

	        DocumentProfilePK pk = new DocumentProfilePK(response.getDocumentVersion(),
	                                    lang,
	                                    response.getLenderProfileId(),
	                                    type,
	                                    response.getOutputTypeStr() );
	          srk.getSysLogger().debug("Language Pref ID"+ pk.getLanguagePreferenceId());
	          req.request(deal, pk, scenarioNum);
	        srk.getSysLogger().info("requestDocByBusRules : "+" document destination is file. :finished:");
	      }
	    } catch(Exception brexc){
	      srk.getSysLogger().error("Error while processing business rule. Deal: " + deal.getDealId() + " Copy:" + deal.getCopyId() + " type:" + type);
	      srk.getSysLogger().error(brexc);
	      throw new DocPrepException( brexc.getMessage() );
	    }
	    srk.getSysLogger().info("requestDocByBusRules : end of method :");
	    return req;
	  }

  
  

  // Catherine, #1053 start ---------------------------------------------------------------------------------
/**
   * <b>generateClientMessage()</b><p>
   *  reads client message from message table, parses it, replaces custom tags, if any, and returns message text
   * @param srk
   * @param clientMessageId - Message id
   * @param deal
   * @return returns message text
   */
  private static String generateClientMessage(SessionResourceKit srk, int clientMessageId, Deal deal)
  {
    String result = "";

    DocPrepLanguage dpl = DocPrepLanguage.getInstance();
    int lang = dpl.findPreferredLanguageId(deal, srk);

    try
    {
      ClientMessage clm = ClientMessageHandler.getInstance().findMessage(srk, clientMessageId, lang);
      clm.getMessageText();

      if (clm == null) {
        return result;
      }

      ConditionParser p = new ConditionParser();
      result = p.parseText(clm.getMessageText(), lang , deal, srk);

      clm = null;
      p = null;
      dpl = null;
    }
    catch (Exception e)
    {
      System.out.println("DocumentRequest.generateClientMessage(): cannot generate client message!");
      e.printStackTrace();
    }

    return result;
  }
  // Catherine, #1053 end ---------------------------------------------------------------------------------

 /**
  *  Execute this document request
  */
  private void requestDoc(Deal deal, int type, int lang, int docType, String pVersion) throws DocPrepException
  {
    try
    {
      this.request = new DocumentQueue(srk);

      DocumentProfilePK pk = setupPrimaryKey(deal,lang,type,XSLMerger.getFormat(docType),pVersion);

      DocumentProfile documentProfile = new DocumentProfile(srk);
      try{
        documentProfile = documentProfile.findByPrimaryKey(pk);
        if(documentProfile.getDocumentProfileStatus() < 0){
          return;
        }
      } catch(Exception exc){
        throw new DocPrepException("requestDoc(): Required document profile does not exist. Document type: " + type);
      }

      Date now = new Date();

      java.sql.Date currentTime = new java.sql.Date(now.getTime());

      // Get E-mail addresses based on MailDestinationType -- by BILLY 07Jan2002
      List theMailList = MailDestinationType.getEMailAddressList(srk, deal, pk);
      // Check if the returned list is null ==> use default
      if(theMailList != null && theMailList.size() > 0)
      {
        // Clear the Mail List
        this.emailAddressList.clear();
        ListIterator li = theMailList.listIterator();
        while(li.hasNext())
        {
          this.addEmailAddress((String)li.next());
        }
      }
      //============== fax numbers =============================================
      List theFaxList = MailDestinationType.getFaxNumberList(srk, deal, pk);
      // Check if the returned list is null ==> use default
      if(theFaxList != null && theFaxList.size() > 0)
      {
        // Clear the Mail List
        this.faxAddressList.clear();
        Iterator lit = theFaxList.listIterator();
        while(lit.hasNext())
        {
          this.faxAddressList.add((String)lit.next());
        }
      }
      //  --------------------------------- for BR-generated emailText ---------------------------------
      if (this.emailText != null) {
        request.create(currentTime,requestorId,getEmailAddress(),emailFullName,emailSubject,emailText,dealId,dealCopyId,null,pk, getFaxNumbers());
      } else {
        request.create(currentTime,requestorId,getEmailAddress(),emailFullName,emailSubject,dealId,dealCopyId, pk, getFaxNumbers());
      }
      //  --------------------------------- for BR-generated emailText end ---------------------------------
    }
    catch(Exception e)
    {
      String msg = "Request for document failed. ";
      if(e.getMessage()!=null) msg += e.getMessage();
      throw new DocPrepException(msg);
    }
  }


 /**
  *  Execute this document request
  */
  private void requestDoc(Deal deal, int type, int lang, int docType) throws DocPrepException
  {
    try
    {
      this.request = new DocumentQueue(srk);

      //***** Change by NBC/PP Implementation Team - Version 1.2 - Start *****//
      DocumentProfilePK pk;
      if(type == Dc.DOCUMENT_NBC_PACKAGE)
      {  
         pk = setupNBCPrimaryKey(deal,lang,type,XSLMerger.getFormat(docType));
      }
      else
      {    	  
         pk = setupPrimaryKey(deal,lang,type,XSLMerger.getFormat(docType));
      }
      //***** Change by NBC/PP Implementation Team - Version 1.2 - End *****//
      DocumentProfile documentProfile = new DocumentProfile(srk);
      try{
        documentProfile = documentProfile.findByPrimaryKey(pk);
        if(documentProfile.getDocumentProfileStatus() < 0){
          return;
        }
      } catch(Exception exc){
        throw new DocPrepException("requestDoc(): Required document profile does not exist. Document type: " + type);
      }

      Date now = new Date();

      java.sql.Date currentTime = new java.sql.Date(now.getTime());

      // Get E-mail addresses based on MailDestinationType -- by BILLY 07Jan2002
      List theMailList = MailDestinationType.getEMailAddressList(srk, deal, pk);
      // Check if the returned list is null ==> use default
      if(theMailList != null && theMailList.size() > 0)
      {
        // Clear the Mail List
        this.emailAddressList.clear();
        ListIterator li = theMailList.listIterator();
        while(li.hasNext())
        {
          this.addEmailAddress((String)li.next());
        }
      }
      //============== fax numbers =============================================
      List theFaxList = MailDestinationType.getFaxNumberList(srk, deal, pk);
      // Check if the returned list is null ==> use default
      if(theFaxList != null && theFaxList.size() > 0)
      {
        // Clear the Mail List
        this.faxAddressList.clear();
        Iterator lit = theFaxList.listIterator();
        while(lit.hasNext())
        {
          this.faxAddressList.add((String)lit.next());
        }
      }
      request.create(currentTime,requestorId,getEmailAddress(),emailFullName,emailSubject,dealId,dealCopyId, pk, getFaxNumbers());
    }
    catch(Exception e)
    {
      String msg = "Request for document failed. ";
      if(e.getMessage()!=null) msg += e.getMessage();
      throw new DocPrepException(msg);
    }
  }

   public int    getRequestorUserId(){ return this.requestorId ;}
   public String getEmailFullName(){ return this.emailFullName ;}
   public String getEmailSubject(){ return this.emailSubject ;}


   public String getEmailAddress()
   {
     if(emailAddressList == null || emailAddressList.isEmpty()) return "";

     ListIterator li = emailAddressList.listIterator();
     int count = emailAddressList.size();
     StringBuffer buf = new StringBuffer();

     while(li.hasNext())
     {
       count--;
       buf.append((String)li.next());

       if(count > 0) buf.append(",");
     }

     return buf.toString();

   }

   public String getFaxNumbers()
   {
     if(faxAddressList == null || faxAddressList.isEmpty()) return "";

     ListIterator li = faxAddressList.listIterator();
     int count = faxAddressList.size();
     StringBuffer buf = new StringBuffer();

     while(li.hasNext())
     {
       count--;
       buf.append((String)li.next());

       if(count > 0) buf.append(",");
     }
     return buf.toString();
   }


   private void setRequestorId(int val)
   {
      this.requestorId = val;
   }

   private void addEmailAddress(String val)
   {
     if(val == null) return;
     int len = val.length();
     emailTotalChars += len;
     int approval = val.indexOf('@');
     // one more check for addresses that do not have user name part (e.g. "@morty.com")
     if (approval != -1) {
       String userNm = val.substring(1, approval).trim();
       if(!(emailTotalChars >= 255) && len > 7 && (userNm.length()>0))
          this.emailAddressList.add(val);
     }
   }

   private void setEmailFullName(String val)
   {
      this.emailFullName = val;
   }

   private void setEmailSubject(String val)
   {
      this.emailSubject = val;
   }

   public static DocumentRequest requestDisbursmentLetter(SessionResourceKit srk, Deal deal)
       throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DISBURSMENT, null);
     }

     UserProfile up = null;
     String emailto = null;
     String subject = "Disbursment Letter for Deal Number: " + deal.getDealId();
     int userId = -1;
     DocumentRequest dr = null;

     try  //get the destination
     {
        up = new UserProfile(srk);
        userId = srk.getExpressState().getUserProfileId();
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

        if(lang == -1)lang = DocPrepLanguage.ENGLISH;

        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Disbursment Letter Attachment");

        dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_DISBURSMENT, lang);
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }

   // Catherine, #1053 start ---------------------------------------------
   /* This method provides a way of setting up email text
    * email text provided by the business rules (if any) will superced email text supplied to this method
    */
   public static DocumentRequest requestConditionsOutstanding(
                   SessionResourceKit srk, Deal deal, int type)throws DocPrepException
   {
     return requestConditionsOutstanding(srk, deal, type, 0);
   }
   // Catherine, #1053 end  ---------------------------------------------

   public static DocumentRequest requestConditionsOutstanding(
                   SessionResourceKit srk, Deal deal)throws DocPrepException
   {
    return requestConditionsOutstanding(srk, deal, XSLMerger.TYPE_RTF);
   }

   public static DocumentRequest requestConditionsOutstanding(
                   SessionResourceKit srk, 
                   Deal deal, int type, int messageId) throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
       // Catherine, #1053 start ---------------------------------------------
       switch (messageId)
      {
        case 0:
        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_CONDITIONS_OUTSTANDING, null);

        default:
          return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_CONDITIONS_OUTSTANDING, null, messageId);
      }
     }
     // Catherine, #1053 end   ---------------------------------------------

     UserProfile up = null;
     String emailto = null;
     String subject = "Conditions of Approval Outstanding for Deal: " + deal.getDealId();
     int userId = -1;
     DocumentRequest dr = null;
     Contact cont = null;

     try  //get the destination
     {
        up = new UserProfile(srk);
        userId = srk.getExpressState().getUserProfileId();
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

        if(lang == -1)lang = DocPrepLanguage.ENGLISH;

        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Conditions of Approval Outstanding Attachment");

        srk.getSysLogger().info("Requesting Broker Outstanding Conditions document on behalf of " +
            cont.getContactFirstName() + " " + cont.getContactLastName() + " for deal " +
            deal.getDealId() + ".");

        switch (type)
        {
            case XSLMerger.TYPE_RTF:
                 dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_CONDITIONS_OUTSTANDING, lang);
                 break;

            case XSLMerger.TYPE_PDF:
                 dr.requestDoc(deal, Dc.DOCUMENT_GENERAL_TYPE_CONDITIONS_OUTSTANDING_PDF, lang, type);
                 break;

            default:
                 throw new DocPrepException("requestConditionsOutstanding(): " +
                    "Unknown document type: " + type + ".  No document generated.");
        }
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }


    return dr;
   }

   public static DocumentRequest requestCommitment(SessionResourceKit srk, Deal deal)
       throws DocPrepException
   {
    return requestCommitment(srk, deal, XSLMerger.TYPE_RTF);
   }

   public static DocumentRequest requestDocByProfile(
                   SessionResourceKit srk, Deal deal, int pProfileId) 
       throws DocPrepException
   {
    srk.getSysLogger().info("RequestDocByProfile : Required for deal id:" + deal.getDealId() + " (" + deal.getCopyId() + ")" + ":type required:" + pProfileId);
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
        return DocumentRequest.requestDocByBusRules( srk, deal, pProfileId, null );
     }else{
        throw new DocPrepException("Method Request Doc By Profile requires com.basis100.document.create.by.business.rules property set to Y");
     }
   }

   /**
    *	The commitment letter can be generated two ways: <br>
    *	1.  After a final approval (Deal.StatusID(Description) = �Approved�)  <br>
    *	2.  When the Deal is in a Post Decision status category <br>
    *  (Deal.StatusID(Categoy) = Post Decision) and the   <br>
    *  Deal Modification page is successfully submitted when  <br>
    *  the Deal.CommitmentProduced = �N�  <br><br>
    *
    *	After the document has been created, it will be attached to an SMTP mail message.<br>
    * The mail message will have the following attributes: <br>
    *	<b>Sender:</b> LynxSystem <br>
    *	<b>Receiver:</b> Deal.UnderwriterUserID(UserProfile.ContactID(Contact/ContactEmailAddress)<br>
    *	<b>Subject:</b> "Commitment Letter for Deal Number: " +  Deal.DealNumber <br>
    *	<b>Body:</b>  Commitment Letter Attachment  <br>
    *
    **/
   public static DocumentRequest requestCommitment(SessionResourceKit srk, Deal deal, int type)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     DocumentRequest rv = null;
     if( useBusRules.equalsIgnoreCase("y") ){
        rv = DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_COMMITMENT, null);
        if(rv == null){
          rv = DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_CONDITIONAL_MTG_APPROVAL, null);
          return rv;
        }else{
          return rv;
        }
     }

     UserProfile up = null;
     String emailto = null;
     String subject = "Commitment Letter for Deal Number: " + deal.getDealId();
     int userId = -1;
     DocumentRequest dr = null;

     try  //get the destination
     {
        up = new UserProfile(srk);
        userId = srk.getExpressState().getUserProfileId();
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

        if(lang == -1)lang = DocPrepLanguage.ENGLISH;

        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Commitment Letter Attachment");

        switch (type)
        {
            case XSLMerger.TYPE_RTF:
                 dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_COMMITMENT, lang);
                 break;

            case XSLMerger.TYPE_PDF:
                 dr.requestDoc(deal, Dc.DOCUMENT_GENERAL_TYPE_COMMITMENT_PDF, lang, type);
                 break;

            default:
                 throw new DocPrepException("requestCommitment(): " +
                    "Unknown document type: " + type + ".  No document generated.");
        }
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }


    return dr;
   }

   /**
    * Original method that defaults to creating an RTF document.
    */
   public static DocumentRequest requestSolicitorsPackage(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
        return requestSolicitorsPackage(srk, deal, XSLMerger.TYPE_RTF);
   }

   /**
    *	The solicitor's package will be created when a request to <br>
    * create a solicitors package has been initiated:
    * After a successful submission of the Closing Activity page.
    *
    *	1.  After a final approval (Deal.StatusID(Description) = �Approved�)  <br>
    *	2.  When the Deal is in a Post Decision status category <br>
    *  (Deal.StatusID(Categoy) = Post Decision) and the   <br>
    *  Deal Modification page is successfully submitted when  <br>
    *  the Deal.CommitmentProduced = �N�  <br><br>
    *
    *	After the document has been created, it will be attached to an SMTP mail message.<br>
    * The mail message will have the following attributes: <br>
    *	<b>Sender:</b> LynxSystem <br>
    *	<b>Receiver:</b>Requested User (UserProfile.ContactID(ContactEmailAddress) <br>
    *	<b>Subject:</b> "Commitment Letter for Deal Number: " +  Deal.DealNumber <br>
    *	<b>Body:</b>  Commitment Letter Attachment  <br>
    *
    **/
   public static DocumentRequest requestSolicitorsPackage(SessionResourceKit srk, Deal deal, int type)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE, null);
     }

     UserProfile up = null;
     String emailto = null;
     int userId = -1;
     DocumentRequest dr = null;

     try  //get the destination
     {
        int status = deal.getStatusCategoryId();
        if(status == Mc.DEAL_STATUS_CATEGORY_POST_DECISION || status == Mc.DEAL_STATUS_CATEGORY_FINAL_DISPOSITION )
        {
          up = new UserProfile(srk);
          userId = deal.getAdministratorId();
          up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
          Contact cont = up.getContact();
          emailto = cont.getContactEmailAddress();

          if(emailto == null)
           throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());
        }
        else
        {
           String st = "Invalid Deal status for Solicitor's Package request. Deal status must be ";
           st += "Post-Decision. ";
           throw new DocPrepException(st);
        }
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

        if(lang == -1)lang = DocPrepLanguage.ENGLISH;

        String subject = "";

        if(lang == DocPrepLanguage.ENGLISH)
         subject =  "Solicitor's Package for Deal Number: " + deal.getDealId();
        else if(lang == DocPrepLanguage.FRENCH)
         subject =  "Solicitor's (Notary) Package for Deal Number: " + deal.getDealId();

        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
           emailto, subject, "Solicitor's Package Attachment");

        switch (type)
        {
            case XSLMerger.TYPE_RTF:
                 dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE, lang);
                 break;

            case XSLMerger.TYPE_PDF:
                 dr.requestDoc(deal, Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE_PDF, lang, type);
                 break;

            default:
                 throw new DocPrepException("requestSolicitorsPackage(): " +
                    "Unknown document type: " + type + ".  No document generated.");
        }
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }

// ***** Change by NBC/PP Implementation Team - Version 1.2 - Start *****//
   /**
    * requestNBCSolicitorsPackage(SessionResourceKit srk, Deal deal, int type)
    * This method is sued to generate the NBC specific solicitor package document
    *
    * @param SessionResourceKit srk <br>
    *   	 Deal �- deal <br>
    *  		 int -- type <br>
             * 
    * @return DocumentRequest : dr <br>
    */

   public static DocumentRequest requestNBCSolicitorsPackage(
	  SessionResourceKit srk, Deal deal, int type) throws DocPrepException
  {
	String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
		COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES, "N");

	 int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

     if(lang == -1)lang = DocPrepLanguage.ENGLISH;
	
	
	if (useBusRules.equalsIgnoreCase("y"))
	{
	  return DocumentRequest.requestDocByBusRules(srk, deal,
		  Dc.DOCUMENT_NBC_PACKAGE, null);
	}
	UserProfile up = null;
	String emailto = null;
	int userId = -1;
	DocumentRequest dr = null;

	try
	// get the destination
	{
	  int status = deal.getStatusCategoryId();
	  if (status == Mc.DEAL_STATUS_CATEGORY_POST_DECISION
		  || status == Mc.DEAL_STATUS_CATEGORY_FINAL_DISPOSITION)
	  {
		up = new UserProfile(srk);
		userId = deal.getAdministratorId();
		up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
		Contact cont = up.getContact();
		emailto = cont.getContactEmailAddress();

		if (emailto == null)
		  throw new DocPrepException(
			  "NBC _ Unable to determine destination email address for user: "
				  + up.getUserLogin());
	  }
	  else
	  {
		String st = "NBC _ Invalid Deal status for NBC Solicitor's Package request. Deal status must be ";
		st += "NBC _ Post-Decision. ";
		throw new DocPrepException(st);
	  }
	}
	catch (DocPrepException dpe)
	{
	  throw dpe;
	}
	catch (Exception ex)
	{
	  throw new DocPrepException(
		  "NBC _ Unable to determine Document Request user destination ");
	}

	try
	{
		lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,
		  srk);

	  if (lang == -1)
		lang = DocPrepLanguage.ENGLISH;

	  String subject = "";

	  if (lang == DocPrepLanguage.ENGLISH)
		subject = "NBC Solicitor's Package for Deal Number: "
			+ deal.getDealId();
	  else
		if (lang == DocPrepLanguage.FRENCH)
		  subject = "NBC Solicitor's (Notary) Package for Deal Number: "
			  + deal.getDealId();

	  dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
		  emailto, subject, "NBC Solicitor's Package Attachment");

	  switch (type)
	  {
	  case XSLMerger.TYPE_RTF:
		dr.requestRTF(deal, Dc.DOCUMENT_NBC_PACKAGE, lang);
		break;

	  case XSLMerger.TYPE_PDF:
		dr.requestDoc(deal, Dc.DOCUMENT_NBC_PACKAGE, lang, type);
		break;

	  default:
		throw new DocPrepException("NBC requestSolicitorsPackage(): "
			+ "NBC Unknown document type: " + type
			+ ".  No document generated for NBC pkg.");
	  }
	}
	catch (DocPrepException dpe)
	{
	  throw dpe;
	}
	catch (Exception ex)
	{
	  throw new DocPrepException(
		  "NBC Failed to make DocumentRequest for deal:  " + deal.getDealId());
	}

	return dr;
  }   
// ***** Change by NBC/PP Implementation Team - Version 1.2 - End *****//
   /**
     * Support for backwards-compatibility
             * @version 1.1 <br>
             *          Date: 06/06/006 <br>
             *          Author: NBC/PP Implementation Team <br>
             *          Change: <br>
             *          Made this change for Generate Pec email functionality. Added 2
             *          more parameters for document type -
             *          Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY and message id -
             *          clientMessageId in the overloaded method which is called within
             *          the method below <br>
    */
      public static DocumentRequest requestDealSummary(SessionResourceKit srk,
                                    Deal deal) throws DocPrepException {
                        // ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
                        int clientMessageId = 0;
                        return requestDealSummary(srk, deal,
                                                Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY, XSLMerger.TYPE_RTF,
                                                clientMessageId);
                        // ***** Change by NBC Impl. Team - Version 1.1 - End *****//

            }

    /**
    *	The Deal Summary Doc can be generated: <br>
    * 1.  By clicking the PRINT DEAL SUMMARY button on the Deal Summary Page
    *
    *  Lender	= MCAP  (Deal.LenderProfileID(ShortName)
    *  Branch	= Deal.BranchProfile.BranchTypeID(Description) = "Origination" <br>
    *
    *	After the document has been created, it will be attached to an SMTP mail message.<br>
    * The mail message will have the following attributes: <br>
    *	<b>Sender:</b> LynxSystem <br>
    *	<b>Receiver:</b> Requested User (UserProfile.ContactID(ContactEmailAddress) <br>
    *	<b>Subject:</b> "Commitment Letter for Deal Number: " +  Deal.DealNumber <br>
    *	<b>Body:</b>  Commitment Letter Attachment  <br>
       * 
             * @version 1.1 <br>
             *          Date: 06/06/006 <br>
             *          Author: NBC/PP Implementation Team <br>
             *          Change: <br>
             *          Made this change for Generate Pec email functionality. Added 2
             *          more parameters for document type -documentType and message id -
             *          clientMessageId. Made changes to requestDocByBusRules which is
             *          called within this method. Added documentType and
             *          clientMessageid params
             * 
             */
            
//          ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
            public static DocumentRequest requestDealSummary(SessionResourceKit srk,
                                    Deal deal, int documentType, int type, int clientMessageId)
                                    throws DocPrepException {
                        String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                                                COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES, "N");
                        if (useBusRules.equalsIgnoreCase("y")) {
                                    return DocumentRequest.requestDocByBusRules(srk, deal,
                                                            documentType, null, clientMessageId);
                        }
//                      ***** Change by NBC Impl. Team - Version 1.1 - end *****//



     UserProfile up = null;
     String emailto = null;
     String subject = "Deal Summary for Deal Number: " + deal.getDealId();
     DocumentRequest dr = null;
     int userId = srk.getExpressState().getUserProfileId();

     try  //get the destination
     {
        up = new UserProfile(srk);
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " +
           up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Deal Summary Attachment");

        switch (type)
        {
            case XSLMerger.TYPE_RTF:
                 dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY,
                    DocPrepLanguage.ENGLISH);
                 break;

            case XSLMerger.TYPE_PDF:
                 dr.requestDoc(deal, Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_PDF,
                    DocPrepLanguage.ENGLISH, type);
                 break;

            default:
                 throw new DocPrepException("requestDealSummary(): " +
                    "Unknown document type: " + type + ".  No document generated.");
        }
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }

   public static DocumentRequest requestDealSummaryLite(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_LITE, null);
     }

     UserProfile up = null;
     String emailto = null;
     String subject = "Deal Summary (S) for Deal Number: " + deal.getDealId();
     DocumentRequest dr = null;
     int userId = srk.getExpressState().getUserProfileId();

     try  //get the destination
     {
        up = new UserProfile(srk);
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Deal Summary Attachment");

        dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_LITE, DocPrepLanguage.ENGLISH);
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }

   public static DocumentRequest requestWireTransfer(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_WIRE_TRANSFER, null);
     }

     UserProfile up = null;
     String emailto = null;
     String subject = "Wire Transfer for Deal Number: " + deal.getDealId();
     DocumentRequest dr = null;
     int userId = srk.getExpressState().getUserProfileId();

     try  //get the destination
     {
        up = new UserProfile(srk);
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Wire Transfer Attachment");

        dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_WIRE_TRANSFER, DocPrepLanguage.ENGLISH);
        // Temp hard coded by BILLY in order to avoid a Full Build -- 22April2002
        //dr.requestRTF(deal, 20, DocPrepLanguage.ENGLISH);
        //====================================================================================
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

     return dr;
   }

   public static DocumentRequest requestDataEntry(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DATA_ENTRY, null);
     }

     UserProfile up = null;
     String emailto = null;
     String subject = "Data Entry for Deal Number: " + deal.getDealId();
     DocumentRequest dr = null;
     int userId = srk.getExpressState().getUserProfileId();

     try  //get the destination
     {
        up = new UserProfile(srk);
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Data Entry Attachment");

        dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_DATA_ENTRY, DocPrepLanguage.ENGLISH);
        // Temp hard coded to avoid a full build -- By BILLY 22April2002
        //dr.requestRTF(deal, 21, DocPrepLanguage.ENGLISH);
        //==================================================================================


     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

     return dr;
   }


   /**
    *	The Deal Summary Package Doc can be generated: <br>
    * 1.  When new deal ingested and WorkFlow created the initial Tasks
    *
    *  Lender	= Deal.LenderProfileID(ShortName)
    *  Branch	= Deal.BranchProfile.BranchTypeID(Description) = "Origination" <br>
    *
    *	After the document has been created, it will be attached to an SMTP mail message.<br>
    * The mail message will have the following attributes: <br>
    *	<b>Sender:</b> BasisXpress <br>
    *	<b>Receiver:</b> Email Address (com.basis100.docprep.maildealsummarypackageto)
    *                   defined in Properties File.  If the property not defined, it will
    *                   send to the assigned Underwriter by default. <br>
    *	<b>Subject:</b> "Deal Summary Package for Deal Number: " +  Deal.DealNumber <br>
    *	<b>Body:</b>  Deal Summary Package Attachment  <br>
    *
    **/
   public static DocumentRequest requestDealSummaryPackage(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
//srk.getSysLogger().debug("BILLY ==> In DocumentRequest.requestDealSummaryPackage !!");
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( useBusRules.equalsIgnoreCase("y") ){
        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_PACKAGE, null);
     }

     UserProfile up = null;
     String emailto = null;
     String subject = "Deal Summary Package for Deal Number: " + deal.getDealId();
     DocumentRequest dr = null;
     int userId = -1;

     try  //get the destination
     {
        // try if the destination email address defined in properties file
        emailto = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCPREP_MAILDEALSUMMARYPACKAGETO, "");

        // try get the email address from assigned Underwriter if Property not defined
        if(emailto == null || emailto.trim().equals(""))
        {
          up = new UserProfile(srk);
          userId = srk.getExpressState().getUserProfileId();
          up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
          Contact cont = up.getContact();
          emailto = cont.getContactEmailAddress();
        }

//srk.getSysLogger().debug("BILLY ==> the EMailAddress to send = " + emailto);

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Deal Summary Package Attachment");

        dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_PACKAGE, DocPrepLanguage.ENGLISH);
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }


   public static DocumentRequest requestTransferPackage(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     UserProfile up = null;
     String emailto = null;
     String body = null;

     int userId = deal.getAdministratorId();

     DocumentRequest dr = null;

     try  //get the destination
     {
        up = new UserProfile(srk);
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));
        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

        if(lang == -1)lang = DocPrepLanguage.ENGLISH;
        String subject = "";

        if(lang == DocPrepLanguage.ENGLISH)
        {
         subject =  "Transfer Package for Deal Number: " + deal.getDealId();
         body = "Transfer Package Attachment";

        }
        else if(lang == DocPrepLanguage.FRENCH)
        {
         subject =  "Transfer Letter for Deal Number: " + deal.getDealId();
         body = "Transfer Letter Attachment";
        }

        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, body);

        dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_TRANSFER_PACKAGE, lang);
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }


   public static DocumentRequest requestBridgePackage(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     UserProfile up = null;
     String emailto = null;
     String subject = "Bridge Package for Deal Number: " + deal.getDealId();
     int userId = -1;
     DocumentRequest dr = null;

     try  //get the destination
     {
        up = new UserProfile(srk);
        userId = deal.getAdministratorId();
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));

        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

        if(lang == -1)lang = DocPrepLanguage.ENGLISH;
        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "Bridge Package Attachment");


     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }

   public static DocumentRequest requestPreAppCertificate(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");

     DocumentRequest rv = null;
     if( useBusRules.equalsIgnoreCase("y") ){
        rv = DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_PRAPPCERT, null);
        if(rv == null){
          rv = DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_PRE_APPROVED_MTG_LETTER, null);
          return rv;
        }else{
          return rv;
        }
     }
//     if( useBusRules.equalsIgnoreCase("y") ){
//        return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_PRAPPCERT, null);
//     }
     UserProfile up = null;
     String emailto = null;
     String subject = "PreApproval Certificate for Deal Number: " + deal.getDealId();
     int userId = -1;
     DocumentRequest dr = null;

     try  //get the destination
     {
        up = new UserProfile(srk);
        userId = deal.getAdministratorId();
        up.findByPrimaryKey(new UserProfileBeanPK(userId, deal.getInstitutionProfileId()));

        Contact cont = up.getContact();
        emailto = cont.getContactEmailAddress();

        if(emailto == null)
         throw new DocPrepException("Unable to determine destination email address for user: " + up.getUserLogin());

     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Unable to determine Document Request user destination ");
     }

     try
     {
        int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

        if(lang == -1)lang = DocPrepLanguage.ENGLISH;

        dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userId,
                                emailto, subject, "PreApproval Certificate Attachment");

        dr.requestRTF(deal, Dc.DOCUMENT_GENERAL_TYPE_PRAPPCERT, lang);
     }
     catch(DocPrepException dpe)
     {
       throw dpe;
     }
     catch(Exception ex)
     {
       throw new DocPrepException("Failed to make DocumentRequest for deal:  " + deal.getDealId());
     }

    return dr;
   }

   public static DocumentRequest requestMortgageInsurance(SessionResourceKit srk, Deal deal, int userProfileId, int requiredAction) throws Exception
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( useBusRules.equalsIgnoreCase("y") ){
          if( requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED ){
            return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED, null);
          }else if( requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED ){
            return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED, null);
          }
       }

       DocumentRequest dr = null;
       //DocumentProfile profile = new DocumentProfile(srk);
       DocumentProfilePK pk = null;

       if(requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED)
       {
         pk = new DocumentProfilePK(Dc.VERSION_1_MICANCEL,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED,
                                    DOC_FMT_XML_UPLOAD);

         dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userProfileId);

       }
       else if(requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED)
       {
         pk = new DocumentProfilePK(Dc.VERSION_1_MIREQ,
                                  Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                  deal.getLenderProfileId(),
                                  Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED,
                                  DOC_FMT_XML_UPLOAD);

         dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userProfileId);

       }

       dr.request(deal, pk);
       return dr;
   }

   public static DocumentRequest requestMortgageInsurance(SessionResourceKit srk, Deal deal, String scenarioNumber, int userProfileId, int requiredAction) throws Exception
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( useBusRules.equalsIgnoreCase("y") ){
          if( requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED ){
            return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED, scenarioNumber);
          }else if( requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED ){
            return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED, scenarioNumber);
          }
       }

       DocumentRequest dr = null;
       //DocumentProfile profile = new DocumentProfile(srk);
       DocumentProfilePK pk = null;

       if(requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED)
       {
         pk = new DocumentProfilePK(Dc.VERSION_1_MICANCEL,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED,
                                    DOC_FMT_XML_UPLOAD);

         dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userProfileId);

       }
       else if(requiredAction == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED)
       {
         pk = new DocumentProfilePK(Dc.VERSION_1_MIREQ,
                                  Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                  deal.getLenderProfileId(),
                                  Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED,
                                  DOC_FMT_XML_UPLOAD);

         dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(), userProfileId);

       }

       dr.request(deal, pk, scenarioNumber);
       return dr;
   }

   public static DocumentRequest requestUpload(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( useBusRules.equalsIgnoreCase("y") ){
          return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_UPLOAD, null);
       }

       DocumentRequest dr = null;
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_UPLOAD,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_DEAL_UPLOAD,
                                    DOC_FMT_XML_UPLOAD);

       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                srk.getExpressState().getUserProfileId());
       dr.request(deal,pk);
       return dr;
   }

//==============================================================================================================
/* The following methods only for DEMO changes
   public static DocumentRequest requestEquifax(SessionResourceKit srk, Deal deal) throws DocPrepException
   {
       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),srk.getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_EQUIFAX_REQUEST,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_EQUIFAX_REQUEST,
                                    "xml-upload");
       dr.request(deal, pk);
       return dr;
   }


   public static DocumentRequest requestNASAppraisal(SessionResourceKit srk, Deal deal) throws DocPrepException
   {
       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),srk.getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_NAS_APPRAISAL,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_NAS_APPRAISAL,
                                    "xml-upload");
       dr.request(deal, pk);
       return dr;
   }

   public static DocumentRequest requestEquifax(SessionResourceKit srk, Deal deal) throws DocPrepException
   {
       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),srk.getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_EQUIFAX_REQUEST,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_EQUIFAX_REQUEST,
                                    "xml-upload");
       dr.request(deal, pk);
       return dr;
   }


   public static DocumentRequest requestFBCAppraisal(SessionResourceKit srk, Deal deal) throws DocPrepException
   {
       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),srk.getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_FBC_APPRAISAL,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_FBC_APPRAISAL,
                                    "xml-upload");
       dr.request(deal, pk);
       return dr;
   }
*/

   public static DocumentRequest requestEzenet(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( useBusRules.equalsIgnoreCase("y") ){
          return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_EZENET_BASE, null);
       }

       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                srk.getExpressState().getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_EZENET,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_EZENET_BASE,
                                    DOC_FMT_XML_UPLOAD);
       dr.request(deal, pk);
       return dr;
   }

   public static DocumentRequest requestPendingMessage(SessionResourceKit srk, Deal deal) throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( useBusRules.equalsIgnoreCase("y") ){
          return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE, null);
       }

       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                srk.getExpressState().getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_PENDING_MESSAGE,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE,
                                    DOC_FMT_XML_PENDING);
       dr.request(deal, pk);
       return dr;
   }

   public static DocumentRequest requestTDCTCapLink(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( useBusRules.equalsIgnoreCase("y") ){
          return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_TDCT_CAPLINK, null);
       }

       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                srk.getExpressState().getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_TDCT_CAPLINK,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_TDCT_CAPLINK,
                                    DOC_FMT_XML_UPLOAD);
       dr.request(deal, pk);
       return dr;
   }

   public static DocumentRequest requestApproval(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( useBusRules.equalsIgnoreCase("y") ){
          return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_APPROVAL, null);
       }

       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                srk.getExpressState().getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_APPROVAL,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_DEAL_APPROVAL,
                                    DOC_FMT_XML_APPROVAL);
       dr.request(deal, pk);
       return dr;
   }

   public static DocumentRequest requestDenial(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       DocumentRequest rv = null;
       if( useBusRules.equalsIgnoreCase("y") ){
          rv = DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_DENIAL, null);
          if(rv == null){
            rv = DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DECLINED_MTG_LETTER, null);
            return rv;
          }else{
            return rv;
          }
       }
//       if( useBusRules.equalsIgnoreCase("y") ){
//          return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_DENIAL, null);
//       }
       DocumentRequest dr = null;
       dr = new DocumentRequest(srk,deal.getDealId(),deal.getCopyId(),
                                srk.getExpressState().getUserProfileId());
       DocumentProfilePK pk = new DocumentProfilePK(Dc.VERSION_1_DENIAL,
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH,
                                    deal.getLenderProfileId(),
                                    Dc.DOCUMENT_GENERAL_TYPE_DEAL_DENIAL,
                                    DOC_FMT_XML_DENIAL);
       dr.request(deal, pk);
       return dr;
   }
/*
   public static DocumentProfilePK setupPrimaryKeyBusRule(Deal deal, int lang, int type, String format, DocBusinessRuleResponse pResp)
   {
      int lender = deal.getLenderProfileId();
      DocumentProfile dp = null;

      if(format == null) format = "rtf1";


      try
      {
          String version = pResp.getDocumentVersion();

          if(version != null && type == Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE)
          {
             int dpid = deal.getDealPurposeId();

             switch(dpid){
               case Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT : {
                 version += "_A";
                 break;
               }
               case Mc.DEAL_PURPOSE_REFI_EXTERNAL : {
                 version += "_B";
                 break;
               }
               default : {
                 version += "_std";
                 break;
               }
             }
          }

         return new DocumentProfilePK(version,lang,lender,type,format);

      }
      catch(Exception e)
      {
         return null;
      }
   }
*/

   public static DocumentProfilePK setupPrimaryKey(Deal deal, int lang, int type, String format, String pVersion)
   {
      int lender = deal.getLenderProfileId();

      if(format == null) format = "rtf1";

      try
      {
          String version = pVersion;

          if(version != null && type == Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE)
          {
             int dpid = deal.getDealPurposeId();

             switch(dpid){
               case Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT : {
                 version += "_A";
                 break;
               }
               case Mc.DEAL_PURPOSE_REFI_EXTERNAL : {
                 version += "_B";
                 break;
               }
               default : {
                 version += "_std";
                 break;
               }
             }
          }

         return new DocumentProfilePK(version,lang,lender,type,format);

      }
      catch(Exception e)
      {
         return null;
      }
   }


   public static DocumentProfilePK setupPrimaryKey(Deal deal, int lang, int type, String format)
   {
      int lender = deal.getLenderProfileId();

      if(format == null) format = "rtf1";

      try
      {
          String version = null;
          version = Dc.VERSION_1_GENX;
/*
 * switch(lender) { default: version = Dc.VERSION_1_GENX; }
*/

          if(version != null && type == Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE)
          {
             int dpid = deal.getDealPurposeId();

             switch(dpid){
               case Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT : {
                 version += "_A";
                 break;
               }
               case Mc.DEAL_PURPOSE_REFI_EXTERNAL : {
                 version += "_B";
                 break;
               }
               default : {
                 version += "_std";
                 break;
               }
             }
          }

         return new DocumentProfilePK(version,lang,lender,type,format);

      }
      catch(Exception e)
      {
         return null;
      }
   }

   //***** Change by NBC/PP Implementation Team - Version 1.3 - Start *****//
   /*
    * RS _ NBC IMPLEMENTATION _ START
    * @see java.lang.Object#toString()
    */
   public static DocumentProfilePK setupNBCPrimaryKey(Deal deal, int lang, int type, String format)
   {
	  
	  System.out.println("***********Inside DocumentProfilePK setupNBCPrimaryKey**************");
	  
      int lender = deal.getLenderProfileId();

      if(format == null) format = "pdf";

      try
      {
          String version = null;
          version = Dc.VERSION_1_GENX;
/*
          switch(lender)
          {
              default:
                version = Dc.VERSION_1_GENX;
          }
*/

          if(version != null)// && type == Dc.DOCUMENT_NBC_PACKAGE
          {
             int dpid = deal.getDealPurposeId();

             switch(dpid){
               case Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT : {
                 version += "_A";
                 break;
               }
               case Mc.DEAL_PURPOSE_REFI_EXTERNAL : {
                 version += "_B";
                 break;
               }
               default : {
                 version += "_std";
                 break;
               }
             }
          }

         return new DocumentProfilePK(version,lang,lender,type,format);

      }
      catch(Exception e)
      {
         return null;
      }
   }
   //***** Change by NBC/PP Implementation Team - Version 1.3 - End *****//
  public String toString()
  {
    String out = "DocumentRequest: " + hashCode() + " DealId: " + dealId;
    out += " For: " + this.emailFullName;

    return out;
  }

  //============================================================================
  // Desjardins
  //============================================================================
   public static DocumentRequest requestApplicationForInsurance(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_APPLICATION_FOR_INSURANCE, null);
   }
   public static DocumentRequest requestConditionalMtgApproval(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_CONDITIONAL_MTG_APPROVAL, null);
   }
   public static DocumentRequest requestUnConditionalMtgApproval(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MTG_APPROVAL_NO_COND, null);
   }
   public static DocumentRequest requestTransferredMtgFile(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_TRANSFERRED_MTG_FILE, null);
   }
   public static DocumentRequest requestMtgSubmittedForDecision(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MTG_SUBMITTED_FOR_DECISION, null);
   }
   public static DocumentRequest requestMtgFinancingApproval(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MTG_FINANCING_APPROVAL, null);
   }
   public static DocumentRequest requestPreAppMtgLetter(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_PRE_APPROVED_MTG_LETTER, null);
   }
   public static DocumentRequest requestDeclinedMtgLetter(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_DECLINED_MTG_LETTER, null);
   }
   public static DocumentRequest requestRateBonusMtgLetter(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_RATE_BONUS_MTG_LETTER, null);
   }   
   public static DocumentRequest requestCreditExperience(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_REQUESTED_CREDIT_EXPERIENCE, null);
   }
   public static DocumentRequest requestInstructionToAppraiser(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_INSTRUCTIONS_TO_APPRAISER, null);
   }
   public static DocumentRequest requestOfferToFinanceLetterCaisse(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_OFFER_TO_FINANCE_LETTER_CAISSE, null);
   }
   public static DocumentRequest requestLoanApplicationCancelled(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_LOAN_APPLICATION_CANCELLED, null);
   }
   public static DocumentRequest requestCreditApplication(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_CREDIT_APPLICATION, null);
   }
   public static DocumentRequest requestCreditAnalysisForm(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_CREDIT_ANALYSIS_FORM, null);
   }

   public static DocumentRequest requestMtgFinancingApprovalAddOn(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MTG_FINANCING_APPROVAL_ADD_ON, null);
   }

   //--DJ_CR005--02June2004--start--//
   // New approach for the request bundled with the necessary attachments (in pre-defined order).
   public static DocumentRequest requestTransferredMtgFileBundle(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_TRANSFERRED_MTG_FILE_BUNDLE, null);
   }

   public static DocumentRequest requestMtgSubmittedForDecisionBundle(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MTG_SUBMITTED_FOR_DECISION_BUNDLE, null);
   }

   public static DocumentRequest requestMtgFinancingApprovalAndOnBundle(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk,deal,Dc.DOCUMENT_GENERAL_TYPE_MTG_FINANCING_APPROVAL_BUNDLE, null);
   }
   //--DJ_CR005--end--//

   // MB 3.6 Doc Central by Catherine Rutgaizer -----------------------------

   private void requestAlert(String emailText) throws DocPrepException
   {
     try
     {
       this.request = new DocumentQueue(srk);
       Date now = new Date();

       request.create(now, requestorId,
                      getEmailAddress(), getEmailFullName(),
                      getEmailSubject(), emailText, dealId, dealCopyId);
     }
     catch(Exception e)
     {
       String msg = "Request for alert email failed. ";
       if(e.getMessage()!=null) msg += e.getMessage();
       throw new DocPrepException(msg);
     }
   }

   public static void requestAlertEmail(SessionResourceKit srk, 
                                        Deal deal, String emailTo, 
                                        String emailSubj, String emailText ) 
       throws DocPrepException {

     DocumentRequest dr = new DocumentRequest(srk, deal.getDealId(), deal.getCopyId(),
                                              srk.getExpressState().getUserProfileId(), 
                                              emailTo, emailSubj, "Alert" );
     dr.requestAlert(emailText);
   }
   // ---------------------

   public static void requestAlertEmail(SessionResourceKit srk, int dealId, int copyId, String emailTo, String emailSubj, String emailText)
      throws DocPrepException {

    DocumentRequest dr = new DocumentRequest(srk, dealId, copyId, srk.getExpressState().getUserProfileId(), emailTo, emailSubj,
        "Alert");
    dr.requestAlert(emailText);
  }

   // --------------------- DJ new Deal Summary #620
   public static DocumentRequest requestDealSummaryDJ(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk, deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_DJ, null);
   }
   // --------------------- DJ new Deal Summary #620 end

   // --------------------- DJ new Deal Summary #656
   public static DocumentRequest requestDealSummaryDJOnApprove(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk, deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_DJ_APPROVAL, null);
   }
   // --------------------- DJ new Deal Summary #656 end

   // ----------------- CV #662 --------------------------------------------------------------
   public static DocumentRequest requestSolFinalReportFlUp1(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_SOL_FOLLOW_UP_1, null);
   }

   public static DocumentRequest requestSolFinalReportFlUp2(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_SOL_FOLLOW_UP_2, null);
   }
   // ----------------- CV #662 end --------------------------------------------------------------
   public static DocumentRequest requestCCASUpload(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       return DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD, null);
   }

   // -----------------Lender to MCAP --------------------------------------------------------------
   public static DocumentRequest requestFundingUpload(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
       String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
       if( ! useBusRules.equalsIgnoreCase("y") ){
          throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
       }
       // Catherine, XD to MCAP, 20-Apr-05 start ---------
       DocumentRequest lResult = DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_FUNDING_UPLOAD, null);
       if (lResult != null){
         // update flag in DEAL table
         srk.getSysLogger().trace("requestFundingUpload@before Deal update");
         deal.updateFundingUploadDone(true, true);
         srk.getSysLogger().trace("requestFundingUpload@after Deal update");
         // Catherine, #1778, 20-Jul-05, begin ---------
         // update DealHistory logging
         try {
            DocPrepLanguage dpl = DocPrepLanguage.getInstance();
            int lang = dpl.findPreferredLanguageId(deal, srk);

            DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
			hLog.log(deal.getDealId(), deal.getCopyId(), 
			         BXResources.getDocPrepIngMsg("FUNDING_UPLOAD_DONE_MSG", lang), 
			         Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
		} catch (Exception e) {
			srk.getSysLogger().debug("Exception when using DealHistoryLogger: " + StringUtil.stack2string(e));
		}
         // Catherine, #1778, 20-Jul-05, end ---------
       }
       // Catherine, XD to MCAP, 20-Apr-05 end ---------
       return lResult;
   }
   // ----------------- Lender to MCAP end --------------------------------------------------------------

   //#DG622 request direct funding upload alternate 
		/* 
		 * request alternate funding upload for givend deal
		 *   
	   */   
   public static DocumentRequest requestFundingUploadAlt(SessionResourceKit srk,
			Deal deal) throws DocPrepException {
		DocumentProfilePK pk = new DocumentProfilePK(VERSION_FUNDING_ALT, 
				srk.getLanguageId(), deal.getLenderProfileId(),
				DOCUMENT_GENERAL_TYPE_FUNDING_UPLOAD, DOC_FMT_XML_FUNDING);
		DocumentRequest dr = new DocumentRequest(srk, deal.getDealId(), 
				deal.getCopyId(), srk.getExpressState().getUserProfileId());
		dr.request(deal, pk, null);
		return dr;
	}	

   // ----------------- #DG306 closing  --------------------------------------------------------------
   public static DocumentRequest requestClosingUpload(SessionResourceKit srk,
       Deal deal) throws DocPrepException {

     String useBusRules = PropertiesCache.getInstance().
         getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( ! useBusRules.equalsIgnoreCase("y") ){
       throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
     }
     DocumentRequest lResult = DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_CLOSING_UPLOAD, null);
     if (lResult != null){
       // update flag in DEAL table
       srk.getSysLogger().trace("requestClosingUpload@before Deal update");
       deal.updateClosingUploadDone(true, true);
       srk.getSysLogger().trace("requestClosingUpload@after Deal update");
       // update DealHistory logging
       try {
         DocPrepLanguage dpl = DocPrepLanguage.getInstance();
         int lang = dpl.findPreferredLanguageId(deal, srk);

         DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
         hLog.log(deal.getDealId(), deal.getCopyId(), 
                  BXResources.getDocPrepIngMsg("CLOSING_UPLOAD_DONE_MSG", lang), 
                  Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
       } catch (Exception e) {
         srk.getSysLogger().debug("Exception when using DealHistoryLogger: " + StringUtil.stack2string(e));
       }
     }
     return lResult;
   }
   // ----------------- #DG306 closing end --------------------------------------------------------------

   public static DocumentRequest requestGemoneyIaUpload(SessionResourceKit srk, Deal deal) throws DocPrepException
   {
        String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
        if( ! useBusRules.equalsIgnoreCase("y") ){
           throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
        }
        return DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_GEMONEY_IA_UPLOAD, null);
   }

   // -------------- Catherine, 26-Apr-05, GECF, new dialog screen start --------------
   public static DocumentRequest requestGEMoneyUpload(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( ! useBusRules.equalsIgnoreCase("y") ){
        throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
     }
     return DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD, null);
   }
   // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------

   // Rel 3.1 - Handles Onatario Disclosure Document generation - Sasa
   public static DocumentRequest requestDisclosureON(SessionResourceKit srk, Deal deal)throws DocPrepException
   {
     String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES,"N");
     if( ! useBusRules.equalsIgnoreCase("y") ){
        throw new DocPrepException("Request for this document must go through business rule. Modify mossys properties please.");
     }
     return DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_ON, null);
   }
   

   //	***** Change by NBC Impl. Team - Version 1.3 - Start *****//
   /**
    * requestAppraisalPackage
    * Method description 
    * The method validates the business rule for triggering the document by verifying its entry from business table
    * and places a request in the document Queue for the generation of Appraisal document
    * and throws an exception in case of a faliure
    * @param srk
    * @param deal
    * @return
    * @throws DocPrepException
    */
   public static DocumentRequest requestAppraisalPackage(SessionResourceKit srk,
 	  Deal deal) throws DocPrepException
   {
 	String useBusRules = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
 		COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES, "N");
 	if (useBusRules.equalsIgnoreCase("y"))
 	{
 	  return DocumentRequest.requestDocByBusRules(srk, deal,
 		  Dc.DOCUMENT_REQUEST_APPRAISAL, null,
 		  Dc.MESSAGE_NON_DISCLOSURE_REQUEST_APPRAISAL_EMAIL);
 	}
 	else
 	{

 	  throw new DocPrepException(
 		  "Request for this document must go through business rule. Modify mossys properties please.");
 	}
   }
   //	***** Change by NBC Impl. Team - Version 1.3 - End*****//

	/**
	 * added for test case, may be removed after revise test case.
	 */
   public DocumentQueue getRequest() {
       return request;
   }
}
