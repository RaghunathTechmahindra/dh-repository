package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class NoSecondaryFinancingCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());


    try
    {
       int dtid = deal.getDealTypeId();
       int dpid = deal.getDealPurposeId();
       String dSecondFinancing = deal.getSecondaryFinancing();

       if(
            dSecondFinancing.equalsIgnoreCase("N") &&
            (
                dpid == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT
                    || dpid == Mc.DEAL_PURPOSE_PORTABLE
                    || dpid == Mc.DEAL_PURPOSE_PORTABLE_DECREASE
                    || dpid == Mc.DEAL_PURPOSE_PORTABLE_INCREASE
                    || dpid == Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT
                    || dpid == Mc.DEAL_PURPOSE_REPLACEMENT
                    ||
                       dtid == Mc.DEAL_TYPE_CONVENTIONAL
            )
        )
        {
            List props =(List)deal.getProperties();
            Iterator pit = props.iterator();

            int type = 
                PicklistData.getPicklistIdValue("PropertyExpenseType","Second Mortgage");

            PropertyExpense pe = new PropertyExpense(srk,null);

            List pes = null;
            boolean found = false;
            Property prop = null;

            while(pit.hasNext())
            {
              prop = (Property)pit.next();

              try
              {
                pes = (List)pe.findByParentAndType((PropertyPK)prop.getPk(),type);
                
                if(!pes.isEmpty())
                {
                    found = true;
                }
              }
              catch(Exception e)
              {
                  continue;
              }
            }

            if(!found)
            {
              if(cond == null)
              {
                cond = new Condition(srk, Dc.NO_SECONDARY_FINANCING_ALLOWED);
                if(!cond.isActiveType()) return fml;

                parser = new ConditionParser();
              }

              prop.findByPrimaryProperty(deal.getDealId(), 
                         deal.getCopyId(), deal.getInstitutionProfileId());

              fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,prop,srk));
            }
        }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
     return fml;
  }
}
