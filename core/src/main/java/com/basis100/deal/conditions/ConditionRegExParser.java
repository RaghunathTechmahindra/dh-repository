package com.basis100.deal.conditions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import MosSystem.Mc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

public class ConditionRegExParser {
	
	private static final Pattern REGEX = Pattern.compile( "<\\w+>" );
	private SessionResourceKit srk;
	private SysLogger logger;
	
	private InnerTagCache itc;
	private Matcher matcher;
	private HashMap<String[], String> tags;
	
	public ConditionRegExParser (SessionResourceKit srk){
		this.srk = srk;
		logger = srk.getSysLogger();
		itc = InnerTagCache.getInstance();
		
		tags = new HashMap<String[], String>();
		matcher = REGEX.matcher( "" );
	}
	
	public void createSystemGeneratedDocTracking(ArrayList conditionList, Deal deal, DealEntity entity, 
			ArrayList resultList)throws Exception{
		
		String isGenerateOnTheFly = PropertiesCache.getInstance().
				getInstanceProperty("com.basis100.conditions.dtverbiage.generateonthefly", "N");
	
		try {
			Iterator itCond = conditionList.iterator();
			srk.setInterimAuditOn(false); //4.4 Transaction History

			if(null != isGenerateOnTheFly && "N".equals(isGenerateOnTheFly)) {
				while (itCond.hasNext()){
					

					Condition cond = (Condition)itCond.next();
	
					DocumentTracking dt = new DocumentTracking(srk);
				    DealPK pk = (DealPK)deal.getPk();
	
				    dt.create(cond,pk.getId(),pk.getCopyId(),
				        Mc.DOC_STATUS_OPEN, Dc.DT_SOURCE_SYSTEM_GENERATED );
	
				    IEntityBeanPK parsectxpk = entity.getPk();
				    
				    dt.setTagContextId( parsectxpk.getId() );
				    dt.setTagContextClassId(entity.getClassId() );
				    
				    parseAndSetText(cond, dt, entity);
				    
				    SourceOfBusinessProfile sob = deal.getSourceOfBusinessProfile();
	
				    int sobcat = -1;
	
				    if(sob != null)
				    sobcat = sob.getSourceOfBusinessCategoryId();
	
				    if(cond.getConditionResponsibilityRoleId() == Mc.CRR_SOURCE_OF_BUSINESS &&
				             sob != null && ( sobcat == Mc.SOB_CATEGORY_TYPE_DIRECT
				             || sobcat == Mc.SOB_CATEGORY_TYPE_INTERNET))
				    {
				      dt.setDocumentResponsibilityRoleId(Mc.CRR_USER_TYPE_APPLICANT);
				    }
				    
				    dt.ejbStore();
				    resultList.add(dt);
				}
			}
			else
				if(null != isGenerateOnTheFly && "Y".equals(isGenerateOnTheFly)) {
					
					while (itCond.hasNext()){
						Condition cond = (Condition)itCond.next();
					    DocumentTracking dt = new DocumentTracking(srk);
					    dt.createWithNoVerbiage(cond, deal.getDealId(), deal.getCopyId(), 
					    		                Mc.DOC_STATUS_OPEN, Dc.DT_SOURCE_SYSTEM_GENERATED);
					    
					    IEntityBeanPK parsectxpk = entity.getPk();
					    
					    dt.setTagContextId( parsectxpk.getId() );
					    dt.setTagContextClassId(entity.getClassId() );
					    
					    SourceOfBusinessProfile sob = deal.getSourceOfBusinessProfile();
						
					    int sobcat = -1;
		
					    if(sob != null)
					        sobcat = sob.getSourceOfBusinessCategoryId();
		
					    if(cond.getConditionResponsibilityRoleId() == Mc.CRR_SOURCE_OF_BUSINESS &&
					             sob != null && ( sobcat == Mc.SOB_CATEGORY_TYPE_DIRECT
					             || sobcat == Mc.SOB_CATEGORY_TYPE_INTERNET))
					    {
					      dt.setDocumentResponsibilityRoleId(Mc.CRR_USER_TYPE_APPLICANT);
					    }
					    
					    dt.ejbStore();
					    resultList.add(dt);
					}
					
				}
		}
		catch (Exception e){
			logger.error("ConditionRegExParser: ERROR: Fail to create System Generated DocTracking for deal: " + 
					deal.getDealId() + " copyId: " + deal.getCopyId());
			logger.error(e);
		}
		finally //4.4 Transaction History
		{
			srk.restoreAuditOn();
		}
		
	}
	
	public void parseAndSetText(Condition cond, DocumentTracking dt, DealEntity entity) throws Exception
	{
		try {
			String text = null;
		    int len = cond.verbiageSize();

		    for(int i = 0; i < len; i++)
		    {
		      text = replaceTages(cond.getConditionTextByLanguage(i), entity, i);
		      dt.setDocumentTextForLanguage(i,text);
		    }
		}
		catch (Exception e){
			logger.error("ConditionRegExParser: ERROR: Fail to parse Condition : " + 
					cond.getConditionId());
			logger.error(e);
		}
	    
	  }
	
	public String replaceTages (String conditionsText, DealEntity entity, int language) throws Exception{
		matcher.reset(conditionsText);
		String fullTag = null;
        //loop to grab all the tags per condition

        while( matcher.find() ) {
            fullTag = conditionsText.substring( matcher.start(), matcher.end() );
            //tag doesn't exist in hash
            String[] key = {fullTag, Integer.toString(language)};
            if( !tags.containsKey( key ) ) {
                //find the value of the tag
                tags.put(key, getTagReplaceValue(getTagLabel(fullTag), entity, language));
            }
        }
                
        Iterator iter = tags.keySet().iterator();
        String[] key;
        String tagValue;
        while( iter.hasNext() ) {
            key = ( String[] ) iter.next();
            if (key[1].equals(Integer.toString(language))){
	            tagValue = tags.get( key );
	            if( null != tagValue ) {
	            	conditionsText = conditionsText.replaceAll( key[0], tagValue.replaceAll("\\$", "\\\\\\$") );
	            }
	            else {
	            	conditionsText = conditionsText.replaceAll( key[0], "" );
	            }
            }

            tagValue = null;
            key = null;
        }
        return conditionsText;
	}
	
	public String getTagReplaceValue(String tag, DealEntity entity, int language) throws Exception {
		String value =  itc.getResult(srk,entity,tag,language);
		return value;
	}
	
	private String getTagLabel (String tag) throws Exception{
		try {
			return tag.substring(1, tag.length()-1);
		} 
		catch (Exception e){
			return "";
		}
	}
	
	public void createAllMissingDocumentVerbiage(int languagePref, Deal deal) throws Exception {
		
		String isGenerateOnTheFly = PropertiesCache.getInstance().
					getInstanceProperty("com.basis100.conditions.dtverbiage.generateonthefly", "N");
		
		if(null != isGenerateOnTheFly && "N".equals(isGenerateOnTheFly))
			return;
					
		    DocumentTracking dt = new DocumentTracking(srk);
		    DealPK  dealpk=  new DealPK(deal.getDealId(), deal.getCopyId());
		    deal.findByRecommendedScenario(dealpk, true);
//		    String sql = "select * from DOCUMENTTRACKING where dealid = " + deal.getDealId();
//		    Collection coll = dt.findBySqlColl(sql);
		    Collection coll = dt.findByDeal((DealPK)deal.getPk());
		    
    		Iterator it = coll.iterator();
		    while(it.hasNext()) {
		    	
		    	dt = (DocumentTracking)it.next();
		    	Condition condition = new Condition(srk, dt.getConditionId());
		    	
		    	if(condition.getDTVerbiageGenerationType() == 1) {
		    		continue;
		    	}
		    	else
		    		if(condition.getDTVerbiageGenerationType() == 0) {
		    	        int statusId = deal.getStatusId();
		    	        if(statusId != Mc.DEAL_APPROVED && statusId != Mc.DEAL_PRE_APPROVAL_OFFERED &&
		    	        		statusId != Mc.DEAL_COMMIT_OFFERED && statusId != Mc.DEAL_CONDITIONS_OUT &&
		    	        		statusId != Mc.DEAL_CONDITIONS_SATISFIED){
		    		        continue;
		    	        }
		    	        dt.createMissingDTVerbiage(condition);
		    		}
		    		else {
		    			throw new Exception("Unsupported DTVerbiageGenerationType");
		    		}
		    	
		    }
		
	}
}
