package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class WorkVisaCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {
      List bors = (List)deal.getBorrowers();

      Iterator it = bors.iterator();
      
      parser = new ConditionParser();

      Borrower bor = null;

      while(it.hasNext())
      {
        bor = (Borrower)it.next();

        int visaType = PicklistData.getPicklistIdValue("CitizenshipType", "Work Visa");

          if( bor.getCitizenshipTypeId() == visaType)
          {
             if(cond == null)
             {
               cond = new Condition(srk, Dc.WORK_VISA);
               if(!cond.isActiveType()) return fml;
             }

             DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,bor,srk);
             fml.addValue(dt);
          }


      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    //return new FMLQueryResult();
    return fml;
  }
} 
