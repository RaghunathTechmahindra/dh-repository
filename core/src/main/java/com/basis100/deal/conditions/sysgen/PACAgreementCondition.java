package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class PACAgreementCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    debug("Condition -> " + this.getClass().getName());


    try
    {
       int did = deal.getDealPurposeId();

       if(!(did == Mc.DEAL_PURPOSE_ASSUMPTION || did == Mc.DEAL_PURPOSE_INCREASED_ASSUMPTION))
        return fml;


       Condition cond = new Condition(srk, Dc.PAC_AGREEMENT_ASSUMPTIONS);
       if(!cond.isActiveType()) return fml;
       
       ConditionParser parser = new ConditionParser();
       DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,deal,srk);
       fml.addValue(dt);

    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
     return fml;
  }
}
