package com.basis100.deal.conditions;

/**
 * 13/May/2005 DVG #DG208 #1403  Error should be raised or logged when tag does not exist
 */

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.xml.DomUtil;
import com.basis100.deal.docprep.extract.*;
import java.io.*;
import java.util.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import org.w3c.dom.*;

import com.basis100.deal.docprep.ExpressDocprepHelper;

/**
*   <pre>
*   Singleton Cache for the Inner Tags withing Condition Verbiage.
*   Tag data extraction logic instances are constructed
*   as indicated in the innertagcondtions.fml file and are mapped to the xml node name.
*   The user of this cache need only ask for a query result suppling the tagname
*   and the expected DealEntity context. The cache will return an FMLQueryResult.
*   </pre>
*   <p>
*   @see  import com.basis100.deal.docprep.extract.FMLQueryResult
*/

public class InnerTagCache
{
  private static InnerTagCache instance;
  private static String DATA = "ConditionsInnerTag.mdfml";

  private List conditionTags;
  private Map tagMap;
  private Map queryNodeMap;
  private String codePackage;
  private SysLogger logger;

  private InnerTagCache()
  {
    try
    {
      logger = ResourceManager.getSysLogger("Conditions");
      String  home;
      String rootDir = ExpressDocprepHelper.getMdfmlFolder();
      
      if(rootDir.endsWith("/") || rootDir.endsWith("\\"))
		  home = rootDir + DATA;  
      else
    	  home = rootDir + "/"+ DATA;   

      ExtractDom fml = new ExtractDom(new File(home));
      codePackage = fml.getCodePackage();
      Document dom = fml.getDocument();
      conditionTags = DomUtil.getNamedDescendants(dom, "ConditionTag");
      tagMap = mapTags(conditionTags);
    }
    catch(Exception e)
    {
       logger.error("CONDITIONS: InnerTagCache failed during construction");
       logger.error(e);
    }
  }

  public static synchronized InnerTagCache getInstance()
  {
    if(instance == null)
     instance = new InnerTagCache();

    return instance;
  }

  public String getResult(SessionResourceKit srk, DealEntity de, String tagname, int lang)
  {
    try
    {
      DocumentFormat format = new DocumentFormat("ascii");
      FMLQueryHandler queryHandler = new FMLQueryHandler(srk);

      Element tag = (Element)tagMap.get(tagname);
      //#DG208   check for error if non existant
      if (tag == null) {
         String msg = "DOCPREP: QUERY ERROR: missing in ConditionsInnerTags: " + tagname;
         logger.debug(msg);
         System.err.println(msg);
         return msg;
      }

      List queryNodes = DomUtil.getNamedChildren(tag,"Query");

      Element function = (Element)DomUtil.getFirstNamedChild(tag,"Function");

      FMLQueryContext ctx = new FMLQueryContext(de, codePackage, lang, format);

      FMLQueryHandler handler = new FMLQueryHandler(srk);

      FMLQueryResult res = handler.handleQuery(tagname, tag, ctx);

      return res.getFirstStringValue(lang);
    }
    catch(Exception e)
    {
      return null;
    }
  }

  private Map mapTags(List nodes)throws Exception
  {
    Map map = new HashMap();

    Iterator it = nodes.iterator();
    Element current = null;

    while(it.hasNext())
    {
      current = (Element)it.next();
      String name = current.getAttribute("name");
      map.put(name,current);
    }

    return map;
  }

}
