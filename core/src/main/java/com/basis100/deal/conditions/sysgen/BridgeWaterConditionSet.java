package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import MosSystem.Mc;

public class BridgeWaterConditionSet extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition Set -> " + this.getClass().getName());
    int conditionId = 222;

    try
    {
       //223 - 235 (except for 229)
      parser = new ConditionParser();

      while(conditionId < 228)
      {
        conditionId++;

        try
        {
          cond = new Condition(srk, conditionId);

          if(cond.isActiveType())
          {
            DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,deal,srk);
            fml.addValue(dt);
            debug("Condition -> BridgeWaterConditionSet entry# " + conditionId );
          }
        }
        catch (FinderException fe)
        {
          String msg = "Non critical FinderException. Unable to get DocTracking for BridgeWater Set entry# " + conditionId ;
          srk.getSysLogger().info(msg + " " + fe.getMessage());
          continue;
        } // end try
      } // end while
    } // end try
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocTracking for BridgeWater Set entry# " + conditionId) ;
    }

    return fml;
  }

// =============================================================================
// NOTE: this code has been removed because the logic has changed, and now there
// no need to skip condition 229.
// =============================================================================
/*
        if(conditionId != 229)
        {
          try
          {
            cond = new Condition(srk, conditionId);

            if(cond.isActiveType())
            {
              DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,deal,srk);
              fml.addValue(dt);
              debug("Condition -> BridgeWaterConditionSet entry# " + conditionId );
            }
          }
          catch (FinderException fe)
          {
            String msg = "Non critical FinderException. Unable to get DocTracking for BridgeWater Set entry# " + conditionId ;
            srk.getSysLogger().info(msg + " " + fe.getMessage());
            continue;
          }
        }
*/
}
