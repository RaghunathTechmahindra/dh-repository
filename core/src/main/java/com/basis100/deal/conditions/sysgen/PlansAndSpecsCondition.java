package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class PlansAndSpecsCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {
      // Modified by BILLY to match system Spec. -- 19March2001
      Property p = new Property(srk,null);
      Collection properties = deal.getProperties();

      Iterator it = properties.iterator();

      int idToCheck = Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS;

      while(it.hasNext())
      {
        p = (Property)it.next();

        if(deal.getDealPurposeId() == idToCheck || p.isNewConstruction() )
        {
          if(cond == null)
          {
            cond = new Condition(srk, Dc.PLANS_AND_SPECS);
            if(!cond.isActiveType()) return fml;

            parser = new ConditionParser();
          }

          DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,p,srk);
          fml.addValue(dt);
        }
      }

    }
    catch (FinderException fe)
    {
      String msg = "Non critical FinderException. Unable to get DocTracking for: " + this.getClass().getName();
      srk.getSysLogger().error(msg + " " + fe.getMessage());
      srk.getSysLogger().error(fe);
      return fml;
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
    return fml;
  }
}
