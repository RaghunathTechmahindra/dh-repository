package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class NonRentalUnitCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {
      Property prop = new Property(srk,null);

      int id = PicklistData.getPicklistIdValue("OccupancyType","Owner-Occupied");

      Collection props = prop.findByDeal((DealPK)deal.getPk());

      Iterator it = props.iterator();

      while(it.hasNext())
      {
           prop = (Property)it.next();

           if(prop.getOccupancyTypeId() == id)
           {
              if(cond == null)
              {
                cond = new Condition(srk, Dc.NON_RENTAL_UNIT);

                if(!cond.isActiveType()) return fml;

                parser = new ConditionParser();
             }

             fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,prop,srk));
           }
      }
    }
    catch (FinderException fe)
    {
      String msg = "Non critical FinderException. Unable to get DocTracking for: " + this.getClass().getName();;
      srk.getSysLogger().error(msg + " " + fe.getMessage());
      srk.getSysLogger().error(fe);
      return fml;
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }
}
