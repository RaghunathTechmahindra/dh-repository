package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class IncomeVerificationSalariedCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition condition = null;
    ConditionParser parser = null;

    int selfEmpId = Mc.INCOME_SALARY;
    int statusId = Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;

    try
    {

      List ehlist = (List)deal.getEmploymentHistories();

      Iterator lt = ehlist.iterator();
      EmploymentHistory hist = null;


      while(lt.hasNext())
      {
        hist = (EmploymentHistory)lt.next();

        if(hist.getEmploymentHistoryStatusId() != statusId)
         continue;

        try
        {
          Income inc = hist.getIncome();

          if((inc.getIncomeTypeId() == selfEmpId) && inc.getMonthlyIncomeAmount() > 0)
          {
             if(condition == null)
             {
               condition = new Condition(srk, Dc.INCOME_VERIFICATION_SALARIED);

               if(!condition.isActiveType()) return fml;

               parser = new ConditionParser();
             }

           fml.addValue( parser.createSystemGeneratedDocTracking(condition,deal,hist,srk));

          }
        }
        catch(Exception e)
        {
          continue;
        }
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    //return new FMLQueryResult();
    return fml;
  }
} 
