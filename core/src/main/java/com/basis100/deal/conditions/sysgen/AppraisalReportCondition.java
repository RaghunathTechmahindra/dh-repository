package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import MosSystem.Mc;

public class AppraisalReportCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition condition = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {

      condition = new Condition(srk,Dc.APPRAISAL_REPORT);

      if(!condition.isActiveType())
          return fml;

      int dealPurpose = deal.getDealPurposeId();

      if(deal.getDealTypeId() == Mc.DEAL_TYPE_CONVENTIONAL && deal.getCombinedLTV() >= 50 && dealPurpose == Mc.DEAL_PURPOSE_PURCHASE)
      {
         //PartyProfile pp = new PartyProfile(srk);
         Property prop = new Property(srk,null);

         Collection props = prop.findByDeal((DealPK)deal.getPk());
         //Collection pps = null;

         Iterator it = props.iterator();

         while(it.hasNext())
         {
           prop = (Property)it.next();

           parser = new ConditionParser();

           fml.addValue( parser.createSystemGeneratedDocTracking(condition,deal,prop,srk));
         }
      }
    }
    catch (FinderException fe)
    {
      String msg = "Non critical FinderException. Unable to get DocTracking for: " + this.getClass().getName();;
      srk.getSysLogger().error(msg + " " + fe.getMessage());
      srk.getSysLogger().error(fe);
      return fml;
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }
}
