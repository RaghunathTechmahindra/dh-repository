package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class SolicitorPaymentOfDebtsCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;
    debug("Condition -> " + this.getClass().getName());

    try
    {
        Liability liability = new Liability(srk, null);

        List liabs = (List)liability.findByDealAndPayoffType((DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS);
        List liabsFromClose = (List)liability.findByDealAndPayoffType((DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE );
        liabs.addAll(liabsFromClose);

        //List liabs = (List)deal.getLiabilities();

        Iterator lt = liabs.iterator();
        //Liability liability = null;

        //int fromProceeds = PicklistData.getPicklistIdValue("LiabilityPayOffType","From Proceeds");

        while(lt.hasNext())
        {
          liability = (Liability)lt.next();
          if(liability.getPercentOutGDS() < 2d){
            continue;
          }
          //int id = liability.getLiabilityPayOffTypeId();

          //if(id == fromProceeds)
          //{
           if(cond == null)
           {
             cond = new Condition(srk, Dc.PAYMENT_OF_DEBTS_BY_SOLICITOR);
             if(!cond.isActiveType()) return fml;

             parser = new ConditionParser();
           }

           fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,liability,srk));
          //}
        }

    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }
}
