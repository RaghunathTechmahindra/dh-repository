package com.basis100.deal.conditions;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.resources.*;
import com.basis100.log.*;

import com.basis100.deal.util.TypeConverter;
import com.basis100.xml.DomUtil;

import java.io.*;
import java.util.*;

import org.w3c.dom.*;

/**
*   <pre>
*   Singleton Cache for the SystemGenerated condition queries. Query instances constructed
*   as indicated in the sysgencondtions.fml file are mapped to the xml node name.
*   The user of this cache need only ask for a query result suppling the tagname
*   and the expected DealEntity context. The cache will return an FMLQueryResult.
*   </pre>
*   <p>
*   @see  import com.basis100.deal.docprep.extract.FMLQueryResult
*/


public class SysGenCache
{
  private static SysGenCache instance;
  private List classes;
  private SysLogger log ;

  private SysGenCache()
  {
    try
    {
      log = ResourceManager.getSysLogger("SysGenCache");

      SysGenQuerySet set = new SysGenQuerySet();

      instantiateAndCache(set);
    }
    catch(Exception e)
    {
       log.error("QUERY_CACHE: Error occured while constructing SysGenCache" );

       if(e.getMessage() != null) log.error(e.getMessage());
       
       log.error(e);
    }

  }

  public static synchronized SysGenCache getInstance()
  {
    if(instance == null)
     instance = new SysGenCache();

    return instance;
  }

  /**
   * Create the internal cache structure: i.e. <br>
   * constructs the ConditionQuery classes and maps each to its describing
   * node.
   * @param - nodes - the list of nodes containing condition query class names
   */

  private void instantiateAndCache(SysGenQuerySet set)throws Exception
  {
    classes = new ArrayList();
    Iterator it = set.iterator();

    String current = null;

    while(it.hasNext())
    {
      current = (String)it.next();

      if(current == null) continue;

      try
      {
        Class queryclass = Class.forName(current);
        SysGenConditionExtractor tagquery = (SysGenConditionExtractor)queryclass.newInstance();
        classes.add(tagquery);
      }
      catch(ClassNotFoundException cnfe)
      {
        log.info("Unable to find System Generated Condition Logic for:" + cnfe.getMessage());
      }

    }

    return;
  }


  public Collection generateDocumentTracking(SessionResourceKit srk, Deal deal)
  {
    //ConditionParser p = new ConditionParser();
    
    ArrayList out = new ArrayList();

    Iterator it = classes.iterator();

    SysGenConditionExtractor blogic = null;

    FMLQueryResult result = null;

    while(it.hasNext())
    {
      blogic = (SysGenConditionExtractor)it.next();

      try
      {
        result = blogic.extract(deal,srk);
      }
      catch(Exception e)
      {
        log.error("Error in System Generated Condition for: " + blogic.getClass().getName());
        log.error(e);
        continue;
      }

      //  pass docTracking Entities back to caller
      Collection results = result.getEntities();

      out.addAll(results);
    }

    return out;
  }



}