package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class PortabilityCertificateCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    Deal deal = (Deal)de;
    Condition cond = null;
    FMLQueryResult fml = new FMLQueryResult();

    debug("Condition -> " + this.getClass().getName());

    try
    {
      cond = new Condition(srk, Dc.PORTABILITY_CERTIFICATE);
      if(!cond.isActiveType()) return fml;

      ConditionParser parser = new ConditionParser();

      Property p = new Property(srk,null);

      int id = PicklistData.getPicklistIdValue("WaterType","Well");

      List all = (List)p.findByDeal((DealPK)deal.getPk());
      Iterator it = all.iterator();

      while(it.hasNext())
      {
        p = (Property)it.next();

        if(p.getWaterTypeId() == id)
        {
          DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,p,srk);
          fml.addValue(dt);
        }
      }
     }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
    return fml;
  }

}
