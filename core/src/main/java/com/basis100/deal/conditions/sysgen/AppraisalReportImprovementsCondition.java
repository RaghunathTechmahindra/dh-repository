package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class AppraisalReportImprovementsCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition condition = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {

      if(deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS)
      {
         condition = new Condition(srk,Dc.APPAISAL_REPORT_IMPROVEMENTS);
         parser = new ConditionParser();

         if(!condition.isActiveType()) return fml;

         Collection all = deal.getProperties();

         Iterator it = all.iterator();
         Property current = null;

         while(it.hasNext())
         {
           current = (Property)it.next();

           if((current.getPurchasePrice() * 1.1) <= current.getEstimatedAppraisalValue())
           {
             fml.addValue( parser.createSystemGeneratedDocTracking(condition,deal,current,srk));
           }
         }
      }
    }
    catch (FinderException fe)
    {
      String msg = "Non critical FinderException. Unable to get DocTracking for: " + this.getClass().getName();;
      srk.getSysLogger().error(msg + " " + fe.getMessage());
      srk.getSysLogger().error(fe);
      return fml;
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }
} 
