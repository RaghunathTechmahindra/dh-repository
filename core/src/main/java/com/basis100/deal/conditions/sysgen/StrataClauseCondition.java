package com.basis100.deal.conditions.sysgen;
   
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class StrataClauseCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    try
    {
      Property p = new Property(srk,null);
      List all = (List)p.findByDeal((DealPK)deal.getPk());
      parser = new ConditionParser();
      Iterator it = all.iterator();

      while(it.hasNext())
      {
        p = (Property)it.next();
        if(p != null)
        {
            int id = p.getPropertyTypeId();
            int provinceId = p.getProvinceId();

            if( (id == Mc.PROPERTY_TYPE_FREEHOLD_CONDO || id == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO || id == Mc.PROPERTY_TYPE_CONDOMINIUM)
              && provinceId != Mc.PROVINCE_BRITISH_COLUMBIA && provinceId != Mc.PROVINCE_QUEBEC )

            {
              cond = new Condition(srk, Dc.STRATA_CLAUSE);
              if(!cond.isActiveType()) return fml;
              
              DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,p,srk);
              fml.addValue(dt);
          }
        }
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }
} 
