package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class DownpaymentSaleCondition extends SysGenConditionExtractor
{
  private static final double APPROX_ROUNDUP = 5.0;  /*in case one amount differs other*/

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Condition cond = null;
    ConditionParser parser = null;
    Deal deal = (Deal)de;

    try
    {
      List sources = (List)deal.getDownPaymentSources();

      Iterator it = sources.iterator();

      DownPaymentSource dps = null;

      while(it.hasNext())
      {
        dps = (DownPaymentSource)it.next();

        int type =
          PicklistData.getPicklistIdValue("DownPaymentSourceType","Sale of Existing Property");

        if(type == dps.getDownPaymentSourceTypeId() && dps.getAmount()>=(deal.getDownPaymentAmount()-APPROX_ROUNDUP))
        {
           if(cond == null)
           {
             cond = new Condition(srk, Dc.DOWNPAYMENT_SALE);

             if(!cond.isActiveType()) return fml;

             parser = new ConditionParser();
           }


           DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,dps,srk);
           fml.addValue(dt);
        }
      }

    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

   return fml;
  }
}
