package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class TransferClauseQuebecCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;
    
    debug("Condition -> " + this.getClass().getName());
    

    try
    {
      if(deal.getDealPurposeId() == Mc.DEAL_PURPOSE_TRANSFER)
      {
        // Check if any Property in Quebec
        Property p = new Property(srk,null);
        List all = (List)p.findByDeal((DealPK)deal.getPk());
        Iterator it = all.iterator();
        parser = new ConditionParser();
        
        while(it.hasNext())
        {
          p = (Property)it.next();
          if(p != null)
          {
            if(p.getProvinceId() == Mc.PROVINCE_QUEBEC)
            {
              cond = new Condition(srk,Dc.TRANSFER_CLAUSE_QUEBEC);
              if(!cond.isActiveType()) return fml;


              DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,deal,srk);
              
              fml.addValue(dt);
              break;
            }
          }
        }
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
     return fml;
  }
} 
