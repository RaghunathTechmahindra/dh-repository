package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class IncomeSourceCommissionCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition condition = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    int commish = Mc.INCOME_COMMISSION;
    int hourlyPlus = Mc.INCOME_HOURLY_PLUS_COMMISSION;
    int statusId = Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT;

    try
    {
      List eh = (List)deal.getEmploymentHistories();

      Iterator it = eh.iterator();

      EmploymentHistory hist = null;

      while(it.hasNext())
      {
        hist = (EmploymentHistory)it.next();

        if(hist.getEmploymentHistoryStatusId() != statusId)
         continue;

        try
        {
          Income inc = hist.getIncome();
          int incType = inc.getIncomeTypeId();


          if((incType == commish || incType == hourlyPlus)
               && inc.getMonthlyIncomeAmount() > 0)
          {
             if(condition == null)
             {
               condition = new Condition(srk, Dc.INCOME_SOURCE_COMMISSION);

               if(!condition.isActiveType()) return fml;

               parser = new ConditionParser();
             }

            DocumentTracking t =  parser.createSystemGeneratedDocTracking(condition,deal,hist,srk);

           fml.addValue(t);

          }

        }
        catch(Exception e)
        {
          continue;
        }
      }


    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    //return new FMLQueryResult();
    return fml;
  }
}
