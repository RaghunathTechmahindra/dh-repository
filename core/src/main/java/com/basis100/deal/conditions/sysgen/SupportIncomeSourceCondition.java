package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import MosSystem.Mc;

import java.util.*;

public class SupportIncomeSourceCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;
    debug("Condition -> " + this.getClass().getName());

    try
    {
        List incs = (List)deal.getIncomes();

        Iterator incit = incs.iterator();
        Income income = null;

        while(incit.hasNext())
        {
          income = (Income)incit.next();
          
          int id = income.getIncomeTypeId();
          double mp = income.getMonthlyIncomeAmount();


          //if( (id == Mc.LIABILITY_TYPE_CHILD_SUPPORT ||
          //     id == Mc.LIABILITY_TYPE_ALIMONY) && mp > 0)
          if( (id == Mc.INCOME_CHILD_SUPPORT ||
               id == Mc.INCOME_ALIMONY) && mp > 0)
          {
             if(cond == null)
             {
               cond = new Condition(srk, Dc.SUPPORT_AS_INCOME_SOURCE);

               if(!cond.isActiveType()) return fml;

               parser = new ConditionParser();
             }

             fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,income,srk));
          }
        }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    //return new FMLQueryResult();
    return fml;
  }
} 
