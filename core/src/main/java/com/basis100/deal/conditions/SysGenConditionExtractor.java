package com.basis100.deal.conditions;

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.SectionTagExtractor;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.log.*;

public abstract class SysGenConditionExtractor
{
   //public static boolean debug = true;
   public static boolean debug = false;
   public SysLogger logger = null;

   public abstract FMLQueryResult extract(DealEntity deal, SessionResourceKit srk)throws ExtractException;

   public String debug(String in)
   {
     if(debug)
       System.out.println(in);

     if(logger == null)
     {
       logger = ResourceManager.getSysLogger("SysGenConditionExtractor");
     }

     logger.debug(in);

     return in;
   }

}