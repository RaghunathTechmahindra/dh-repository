package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class QCondoCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {

      Property p = new Property(srk,null);

      List all = (List)p.findByDeal((DealPK)deal.getPk());

      Iterator it = all.iterator();
      parser = new ConditionParser();

      while(it.hasNext())
      {
        p = (Property)it.next();
        if(p != null)
        {
          int provinceId = p.getProvinceId();
          int pid = p.getPropertyTypeId();
          if( (pid == Mc.PROPERTY_TYPE_CONDOMINIUM || pid == Mc.PROPERTY_TYPE_FREEHOLD_CONDO ||
               pid == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO) &&  provinceId == Mc.PROVINCE_QUEBEC)
          {
            if(cond == null)
            {
              cond = new Condition(srk,Dc.QUEBEC_CONDO);
              if(!cond.isActiveType())
                 return fml;
            }


            DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,p,srk);

            fml.addValue(dt);
          }
        }
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
   return fml;
  }
}
