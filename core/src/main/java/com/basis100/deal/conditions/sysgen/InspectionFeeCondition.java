package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class InspectionFeeCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      Property p = new Property(srk,null);
      p = p.findByPrimaryProperty(deal.getDealId(), 
                   deal.getCopyId(), deal.getInstitutionProfileId());

      DealFee df = new DealFee(srk,null);
      int type = Mc.FEE_TYPE_INSPECTION_FEE;

      List fees = (List)df.findByDealAndType((DealPK)deal.getPk(),type);

      if(!fees.isEmpty())
      {
        df = (DealFee)fees.get(0);

        if(p.isNewConstruction() && df.getFeeAmount() > 0)
        {
          if(cond == null)
          {
            cond = new Condition(srk, Dc.INSPECTION_FEE);
            if(!cond.isActiveType()) return fml;
            parser = new ConditionParser();
          }

          DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,df,srk);
          fml.addValue(dt);

        }
      }
    }
    catch (FinderException fe)
    {
      String msg = "Non critical FinderException. Unable to get DocTracking for: " + this.getClass().getName();;
      srk.getSysLogger().error(msg + " " + fe.getMessage());
      srk.getSysLogger().error(fe);
      return fml;
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }
}
