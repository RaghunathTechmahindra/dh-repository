package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class SaleOfPropertyCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());
    
    try
    {
      Borrower b = new Borrower(srk,null);
      b = b.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
      int type = b.getBorrowerTypeId();


      if(type == Mc.BT_BUILDER)
      {
        DealFee fee = new DealFee(srk,null);
        int feetype = PicklistData.getPicklistIdValue("FeeType","Cancellation Fee");

        fee = fee.findFirstByDealAndType((DealPK)deal.getPk(), feetype);

        if(fee.getFeeAmount() > 0)
        {
          if(cond == null)
          {
            cond = new Condition(srk, Dc.SALE_OF_PROPERTY);
            if(!cond.isActiveType()) return fml;

            parser = new ConditionParser();
          }

          DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,fee,srk);

          fml.addValue(dt);
          
          return fml;
        }
      }

    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
    return new FMLQueryResult();
  }
} 
