package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class DebtsPaidFromProceedsCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {
      Liability liability = new Liability(srk, null);
      Collection c = liability.findByDealAndPayoffType((DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS);
      Collection cClose = liability.findByDealAndPayoffType((DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE );
      c.addAll(cClose);

      if(!c.isEmpty() && isPercentOutGDS(c) )
      {
        cond = new Condition(srk,Dc.DEBTS_PAID_FROM_PROCEEDS);
        if(!cond.isActiveType()) return fml;
        parser = new ConditionParser();
        fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,deal,srk));
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }

  private boolean isPercentOutGDS(Collection col){
    Iterator it = col.iterator();
    while(it.hasNext()){
      if( ((Liability)it.next()).getPercentOutGDS() < 2d  ){
        return true;
      }
    }
    return false;
  }

}
