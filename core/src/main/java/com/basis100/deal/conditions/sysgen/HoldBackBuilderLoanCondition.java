package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class HoldBackBuilderLoanCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {
      Borrower b = new Borrower(srk,null);
      b = b.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
      int type = b.getBorrowerTypeId();

      if(PicklistData.valueEqualsId("Builder","BorrowerType",type))
      {
        if(deal.getAdvanceHold() > 0)
        {
          cond = new Condition(srk, Dc.HOLD_BACK_BUILDER_LOAN);

          if(!cond.isActiveType()) return fml;

          parser = new ConditionParser();
          DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,deal,srk);

          fml.addValue(dt);
          return fml;
        }
      }

    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
    return new FMLQueryResult();
  }
} 
