package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class BridgeLoanCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {
        Bridge br = new Bridge(srk, null);
        br.setSilentMode(true);
        br = br.findByDeal((DealPK)deal.getPk());

        if(br != null && br.getNetLoanAmount() > 0)
        {
          cond = new Condition(srk,Dc.BRIDGE_LOAN);
          if(!cond.isActiveType()) return fml;

          parser = new ConditionParser();
          DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,br,srk);
          fml.addValue(dt);
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
     return fml;
  }



} 
