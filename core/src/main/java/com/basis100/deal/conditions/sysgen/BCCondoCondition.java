package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class BCCondoCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());

    try
    {
      // ALERT:ZR:  variable cond IS ALWAYS null at this point.
      // it must be moved . Ask Brad is the solution in the class
      // QCondoCondition o.k.
      //if(!cond.isActiveType()) return fml;

      Property p = new Property(srk,null);
      Collection properties = deal.getProperties();

      Iterator it = properties.iterator();

      parser = new ConditionParser();

      while(it.hasNext())
      {
        p = (Property)it.next();
        int pid = p.getPropertyTypeId();
        int provinceId = p.getProvinceId();

        if( (pid == Mc.PROPERTY_TYPE_CONDOMINIUM
            || pid == Mc.PROPERTY_TYPE_FREEHOLD_CONDO
            || pid == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO
            || pid == Mc.PROPERTY_TYPE_STRATA
            || pid == Mc.PROPERTY_TYPE_TOWNHOUSE_CONDO
            )
            && provinceId == Mc.PROVINCE_BRITISH_COLUMBIA)
        {

           cond = new Condition(srk,Dc.BC_CONDO);

           if(!cond.isActiveType()) return fml;
           
           DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,p,srk);

           fml.addValue(dt);

        }
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    return fml;
  }
}
