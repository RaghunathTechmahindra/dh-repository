package com.basis100.deal.conditions;

import org.w3c.dom.*;
import org.xml.sax.*;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.xml.*;
import MosSystem.Mc;

import java.io.*;
import java.util.*;
import java.text.*;

import com.basis100.log.*;
import com.basis100.resources.*;

/**
 * The class containing logic for parsing and replaceing tags within ConditionVerbiage.
 * MCM Impl Team : 23 Oct,2008 Bugfix :FXP22975 Setting document status as requested if for Component condition in createSystemGeneratedDocTracking() method
 */
public class ConditionParser implements DocumentHandler
{
  private SessionResourceKit srk;
  private InnerTagCache itc;
  private File description;
  private StringBuffer buffer;
  private String codePackage;
  private DealEntity entity;
  private int language;

/**
*   <pre>
 *   parse the supplied String in the indicated language using the given parse context.
 *   The parse context is the expected entity for tag replacement logic
 *   (com.basis100.deal.docprep.extract.doctag.* classes) for
 *   the supplied Conditions inner tags. The parser uses the Sun sax parser implementation.
 *   </pre>
 *   <p>
 *   @return verbiage parsed using the supplied parse context.
 */
  public String parseText(String in, int language, DealEntity parseCtx, SessionResourceKit srk)throws Exception
  {
    if(in == null) return null;

    this.language = language;
    this.buffer = new StringBuffer();
    this.itc = InnerTagCache.getInstance();
    this.srk = srk;
    this.entity = parseCtx;

    if(in == null)
     srk.getSysLogger().error("Null String obtained in parseText - ConditionParser - Current Entity: " + entity);


    StringBuffer inbuffer = new StringBuffer(in);

    if(!clean(inbuffer))  // does text have any tags? if not return
    {
      return in;
    }
    parseAndReplaceTags(inbuffer.toString());

    String val = this.buffer.toString();

    if(val == null) val = "";

    this.buffer = null;
    this.entity = null;

    return val;
  }

  /**
  *   puts Verbiage String in a form acceptable to the sax parser
  *   and this document handler
  *   return true if the parameter contains tags else return false;
  */
  private boolean clean(StringBuffer text)
  {
    if(text == null)
    return false;

    char[] in = text.toString().toCharArray();
    int len = in.length;

    StringBuffer buffer = new StringBuffer(len + 25);
    boolean intag = false;

    boolean tagfound = false;

    for ( int i = 0; i < len; i++ )
    {
      char ch = in[i];

      switch(ch)
      {
        case '<':
          buffer.append("<");
          intag = true;
          break;

        case '>':
          buffer.append("/>");

          if(intag) tagfound = true;

          intag = false;
          break;

        case '&':
          buffer.append("&amp;");
          break;

        case '"':
          buffer.append("&quot;");
          break;

        case ' ':
          if(intag)
           ;//descard space in tag
          else
           buffer.append(ch);
          break;

        default:  buffer.append(ch);
      }
    }


    if(tagfound)
    {
      buffer.insert(0,"<condition>");
      buffer.append("</condition>");
      text.replace(0,text.length(),buffer.toString());
      return true;
    }

    return false;
  }

  /**
   * parses the Condition text supplied using a SAX parser and replaces<br/>
   * tags found with data culled from logic within (or pointed to by)<br/>
   * the <i>conditionsInnerTag.mdfml</i> file
   * @param condition -  the ConditionVerbiage conditionText to be parsed
   */
  private void parseAndReplaceTags(String condition)throws Exception
  {
    try
    {
      Parser parser = XmlToolKit.getInstance().newSAXParser();
      parser.setDocumentHandler(this);
      parser.parse(new InputSource(new StringReader(condition)));
    }
    catch(Exception io)
    {
      SysLogger sys = srk.getSysLogger();
      sys.error("DOCPREP: ERROR: Parse Failure in condition: " + condition);
      sys.error(io);
      if(io.getMessage() != null) sys.error(io.getMessage());
    }

  }

  private String getTagReplaceValue(String tag)
  {
     if(tag.equals("condition"))return "";

     return itc.getResult(srk,entity,tag,language);
  }


  //   Receive notification of character data.
  public void characters(char[] ch, int start, int length)
  {
    buffer.append(new String(ch,start,length));
  }

  //  Receive notification of the end of a document.
  public void endDocument()
  {
  }


  //Receive notification of the end of an element.
  public void endElement(java.lang.String name)
  {
  }


  //  Receive notification of ignorable whitespace in element content.
  public void ignorableWhitespace(char[] ch, int start, int length)
  {

  }


  //Receive notification of a processing instruction.
  public void processingInstruction(java.lang.String target, java.lang.String data)
  {
    ;
  }


  // Receive an object for locating the origin of SAX document events.
  public void setDocumentLocator(Locator locator)
  {
    ;
  }

  //Receive notification of the beginning of a document.
  public void startDocument()
  {
  }


  public void startElement(java.lang.String name, AttributeList atts)
  {
     String val = getTagReplaceValue(name);
     if(val != null) buffer.append(val);
  }


   /**
  *   <pre>
  *   produce a DocumentTracking entity with parsed verbiage using
  *   the supplied Condition and parse context.
  *   The parse context is the expected entity for tag replacement logic
  *   (com.basis100.deal.docprep.extract.doctag.* classes) for
  *   the supplied Conditions inner tags.
  *   </pre>
  *   <p>
  *   @return verbiage parsed using the supplied parse context.
  */
  public DocumentTracking createSystemGeneratedDocTracking(Condition src, Deal deal, DealEntity parseCtx, SessionResourceKit srk)throws Exception
  {
    this.buffer = new StringBuffer();
    this.itc = InnerTagCache.getInstance();
    this.srk = srk;
    this.entity = parseCtx;

    DocumentTracking dt = new DocumentTracking(srk);
    DealPK pk = (DealPK)deal.getPk();
    /**
     * MCM Impl Team : 23 Oct,2008 Bugfix : FXP22975 Setting document status as requested if for Component condition 
     */
  if(parseCtx.getClassId()== ClassId.COMPONENT)
  {
      dt.create(src,pk.getId(),pk.getCopyId(),
              Mc.DOC_STATUS_REQUESTED, Dc.DT_SOURCE_SYSTEM_GENERATED );
  }
  /********************Bug Fix FXP22975 Ends***************************************************************************/
  else
  {
      dt.create(src,pk.getId(),pk.getCopyId(),
           Mc.DOC_STATUS_OPEN, Dc.DT_SOURCE_SYSTEM_GENERATED );
   }

    IEntityBeanPK parsectxpk = this.entity.getPk();

    //dt.setDocumentResponsibilityId( pk.getId() );
    //dt.setDocumentResponsibilityId( parsectxpk.getId() );
    dt.setTagContextId( parsectxpk.getId() );
    dt.setTagContextClassId( parseCtx.getClassId() );

    parseAndSetText(src, dt, parseCtx, srk);

    SourceOfBusinessProfile sob = deal.getSourceOfBusinessProfile();

    int sobcat = -1;

    if(sob != null)
    sobcat = sob.getSourceOfBusinessCategoryId();

    if(src.getConditionResponsibilityRoleId() == Mc.CRR_SOURCE_OF_BUSINESS &&
             sob != null && ( sobcat == Mc.SOB_CATEGORY_TYPE_DIRECT
             || sobcat == Mc.SOB_CATEGORY_TYPE_INTERNET))
    {
      dt.setDocumentResponsibilityRoleId(Mc.CRR_USER_TYPE_APPLICANT);
    }

	srk.setInterimAuditOn(false); //4.4 Transaction History
    dt.ejbStore();
	srk.restoreAuditOn();
	
    return dt;
  }

  public void parseAndSetText(Condition src, DocumentTracking dest, DealEntity parseCtx, SessionResourceKit srk) throws Exception
  {
    String text = null;
    int len = src.verbiageSize();

    for(int i = 0; i < len; i++)
    {
      text = parseText(src.getConditionTextByLanguage(i),i,parseCtx,srk);
      dest.setDocumentTextForLanguage(i,text);
    }
  }

  public static void main(String args[]) throws Exception
  {

     ResourceManager.init("\\mos\\admin\\","MOS_Q");
     PropertiesCache.addPropertiesFile( "\\mos\\admin\\mossys.properties");
     SessionResourceKit srk  = new SessionResourceKit("0");

     //Deal deal = new Deal(srk,null,16414,14);
     //DealPK dpk = new DealPK( 444,4);
     DealPK dpk = new DealPK( 17018,4);
     /*
     DealEntity de = (DealEntity) deal;
     String txt = new String("<LenderName> requires written confirmation of employment by <EmployerName> in teflecting: Start date, (ii) Position and (iii) Minimum annual salary <Salary>.");
     ConditionParser cp = new ConditionParser();
     for(int i=1; i<50; i++){
       cp.parseText(txt,0,de,srk);
     }

     Condition source = new Condition(srk,9);
     DocumentTracking dt = new DocumentTracking(srk);
     cp.parseAndSetText(source, dt, deal, srk);
     */

     ConditionHandler ch  = new ConditionHandler(srk);
     ch.buildSystemGeneratedDocumentTracking(dpk);

     //String txt = new String("requires written confirmation<LenderName> of employment by <EmployerName> in teflecting: Start date, (ii) Position and (iii) Minimum annual salary <Salary>.");
     //String txt1 = new String("<LenderName> requires written confirmation of employment by <EmployerName> in teflecting: Start date, (ii) Position and (iii) Minimum annual salary <Salary>.");

     //cp.parseText(txt,0,de,srk);
     //cp.parseText(txt1,0,de,srk);
  }

}