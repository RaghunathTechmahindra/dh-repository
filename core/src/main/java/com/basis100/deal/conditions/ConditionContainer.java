package com.basis100.deal.conditions;

public class ConditionContainer
{
   Integer condIdCode;
   String condText;

   private int statusId;

   public ConditionContainer(int parId, String parText){
     this.condIdCode = new Integer(parId);
     this.condText = new String(parText);

     statusId = -1;
   }

   public int getStatusId(){
    return statusId;
   }

   public void setStatusId(int pStat){
    statusId = pStat;
   }

   public Integer getCondIdCode(){
     return condIdCode;
   }

   public String getCondText(){
     return condText;
   }
}