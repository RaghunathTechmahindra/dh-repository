package com.basis100.deal.conditions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.ConditionBusinessRuleExecutor;
import com.basis100.entity.FinderException;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;


/**
 * @author MCM Impl Team
 * Change Log:
 * @version 1.1 11-July-2008 XS_16.17 Modified retrieveCommitmentLetterDocTracking() 
 *          method for getting the componentrelated condition for Commitment Letter.
 *
 */
public class ConditionHandler
{
  private SessionResourceKit srk;

  ////Release2.1--temp//
  SysLogger logger = SysLog.getSysLogger("CH");


  public ConditionHandler(SessionResourceKit srk)
  {
    this.srk = srk;
  }

/**
 *   Gets a Collection of System Generated DocumentTracking entities for the given
 *   Deal.<br>
 *   Checks logic for SystemGenerated conditions and determines which conditions
 *   are required. Conditions are parsed and tag replacements made.<br> The parsed Condition
 *   verbiage is stored in the DocumentTracking table. .
 *   <p>
 *   @return a Collection of DocumentTracking entities.
 */
  public Collection buildSystemGeneratedDocumentTracking(DealPK pk)throws Exception
  {
    try
    {
	
	    srk.setInterimAuditOn(false);  //4.4 Transaction History
		  
    Deal deal = new Deal(srk,null,pk.getId(),pk.getCopyId());

    int cat = deal.getStatusCategoryId();

     if(cat != Mc.DEAL_STATUS_CATEGORY_DECISION)
       return getSystemGeneratedDocumentTracking(deal);

    //delete all sysgen doctracking entries
    DocumentTracking dt = null;

    Collection oldDocTracking = getSystemGeneratedDocumentTracking(deal);  //doesn't include customized conditions

    Iterator it = oldDocTracking.iterator();

    while(it.hasNext())
    {
      dt = (DocumentTracking)it.next();
      if(dt != null) dt.ejbRemove();
      // remove those conditions where status is zero
      if( dt.getDocumentStatusId() == 0 ){
        it.remove();
      }
    }

    //==========================================================================
    // this is the spot where we can plug in new way of creating conditions
    // by using business rules.
    //==========================================================================
    String useBusRules = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.wayofcreating");
    // =========================================================================
    // there are three ways of creating conditions
    // 0 -> use only conditions created by software
    // 1 -> use only conditions created by business rules
    // 2 -> use conditions created by software + conditions created by business rule
    //==========================================================================
    int wayOfCreating;
    try{
      wayOfCreating = Integer.parseInt(useBusRules) ;
    }catch(Exception exc){
      wayOfCreating = 0;
    }
    
    //Custom conditions collection for Duplicate Default Conditions
    Collection oldCustomDT = getCustomDocumentTracking(deal);

    Collection newDTFromClass;
    Collection newDTFromRules;
    Collection newDTAll;
    switch(wayOfCreating){
      // from business rules only
      case(1) :{
        ConditionBusinessRuleExecutor cbre = new ConditionBusinessRuleExecutor(srk);
        newDTFromRules = cbre.extractConditions(deal);
        
        //Duplicate Conditions...
	    String DDCprop = 
	    	PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
	    			Xc.DUPLICATE_DEFAULT_CONDITIONS, "Y");
	    if ("Y".equalsIgnoreCase(DDCprop) && oldCustomDT != null)
	    	removeDTMatch(oldCustomDT, newDTFromRules);
          //copies documentstatus from old set to new 
          // also copy DocumentResponsibleRoleId if the old document came from 
          // duplicated Default condition for 4.3GR
        updateDocTrackingStatus( oldDocTracking, newDTFromRules ); 
        return newDTFromRules;
      }
      // from software + from business rules
      case(2) : {
        SysGenCache cache = SysGenCache.getInstance();
        newDTFromClass = cache.generateDocumentTracking(srk, deal);

        ConditionBusinessRuleExecutor cbre = new ConditionBusinessRuleExecutor(srk);
        newDTFromRules = cbre.extractConditions(deal);
        

        newDTAll = new java.util.ArrayList();
        newDTAll.addAll(newDTFromClass);
        newDTAll.addAll(newDTFromRules);

        //Duplicate Conditions...
	    String DDCprop = 
	    	PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
	    			"duplicate.default.conditions", "Y");
	    if ("Y".equalsIgnoreCase(DDCprop) && oldCustomDT != null)
	    	removeDTMatch(oldCustomDT, newDTAll);

       //copies documentstatus from old set to new 
       // also copy DocumentResponsibleRoleId if the old document came from 
       // duplicated Default condition for 4.3GR
        updateDocTrackingStatus( oldDocTracking, newDTAll );
        return newDTAll;
      }
      // from software only
      default : {
        SysGenCache cache = SysGenCache.getInstance();
        newDTFromClass = cache.generateDocumentTracking(srk, deal);

        //Duplicate Conditions...
	    String DDCprop = 
	    	PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
	    			"duplicate.default.conditions", "Y");
	    if ("Y".equalsIgnoreCase(DDCprop) && oldCustomDT != null)
            removeDTMatch(oldCustomDT, newDTFromClass);
        
       //copies documentstatus from old set to new 
       // also copy DocumentResponsibleRoleId if the old document came from 
       // duplicated Default condition for 4.3GR
        updateDocTrackingStatus( oldDocTracking, newDTFromClass );
        return newDTFromClass;
      }
    }
  }
    finally
    {
        srk.restoreAuditOn();  //4.4 Transaction History
    }
  }


  // ===========================================================================
  // utility method called by buildSystemGeneratedDocumentTracking
  // added to copy responsiblityRoleId if duplicate.default.conditions='Y' for //FXP27789
  //============================================================================
  private void updateDocTrackingStatus(Collection oldDocTracking , Collection newDocTracking) throws Exception
  {
     int institutionProfileId = -1;
     String DDCprop = null;
     boolean dupDefCond=false; 

    Iterator itNew = newDocTracking.iterator();
    Iterator itOld ;
    DocumentTracking dtOld;
    DocumentTracking dtNew;
    while( itNew.hasNext() ){
      dtNew = (DocumentTracking)itNew.next();
      if (DDCprop==null){
	  institutionProfileId=dtNew.getInstProfileId();
	  DDCprop = PropertiesCache.getInstance().getProperty(institutionProfileId,
	    			Xc.DUPLICATE_DEFAULT_CONDITIONS, "Y");
	  dupDefCond = "Y".equalsIgnoreCase(DDCprop);
      }
      itOld = oldDocTracking.iterator();
      while( itOld.hasNext() ){
        dtOld = (DocumentTracking)itOld.next();
        if( isDTMatch(dtOld,dtNew) ){
            dtNew.setDocumentStatusId( dtOld.getDocumentStatusId() );
            //FXP27789
            if (dupDefCond) {
        	dtNew.setDocumentResponsibilityRoleId(dtOld.getDocumentResponsibilityRoleId());
            }
            dtNew.ejbStore();
        }
      }
    }
  }

  // ===========================================================================
  // utility method called by updateDocTrackingStatus and
  // buildSystemGeneratedDocumentTracking
  //============================================================================
  private boolean isDTMatch(DocumentTracking dtOld, DocumentTracking dtNew){
/*
    String concatStrOld = "" + StringUtil.zeroLeftPad(dtOld.getDealId(),13)
                             + StringUtil.zeroLeftPad(dtOld.getConditionId(),13)
                             + StringUtil.zeroLeftPad(dtOld.getDocumentResponsibilityId(),38);
    String concatStrNew = "" + StringUtil.zeroLeftPad(dtNew.getDealId(),13)
                             + StringUtil.zeroLeftPad(dtNew.getConditionId(),13)
                             + StringUtil.zeroLeftPad(dtNew.getDocumentResponsibilityId(),38);
*/
    String concatStrOld = "" + StringUtil.zeroLeftPad(dtOld.getDealId(),13)
                             + StringUtil.zeroLeftPad(dtOld.getConditionId(),13)
                             + StringUtil.zeroLeftPad(dtOld.getTagContextId(),38);
    String concatStrNew = "" + StringUtil.zeroLeftPad(dtNew.getDealId(),13)
                             + StringUtil.zeroLeftPad(dtNew.getConditionId(),13)
                             + StringUtil.zeroLeftPad(dtNew.getTagContextId(),38);

    if(concatStrOld.equals(concatStrNew)){
      return true;
    }
    return false;
  }

  public Collection getSystemGeneratedDocumentTracking(Deal deal) throws Exception
  {
    DocumentTracking dt = new DocumentTracking(srk);

    return  dt.findByDealAndSource((DealPK)deal.getPk()
          ,Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED);
  }


/**
 *  <pre>gets the VariableVerbiage for the Condition with the given conditionId. The
 *  verbiage is parsed and substitutions are made based on the given Deal and Language
 *  preference.
 *  </pre>
 *  @return the parsed ConditionVerbiage for the indicated language.
 *  @param  d -  the Deal entity providing parse context.
 *  @param  conditionId -  the condition verbiage to parse.
 *  @param  languagePreferenceId - the verbiage language returned.
 *  @version 1.1 FXP26427 fix
 */
  public String getVariableVerbiage(Deal d, int conditionId, int languagePreferenceId)throws Exception
  {
    Condition cond = new Condition(srk);
    try
    {
     ConditionPK pk = new ConditionPK(conditionId);
     cond.setSilentMode(true);
     cond = cond.findByPrimaryKey(pk);

     if(cond.getConditionTypeId() != Mc.CONDITION_TYPE_VARIABLE_VERBIAGE)
     {
       String msg =  "CONDITION_HANDLER: Dissallowed attempt to get non - variable verbiage " +
                      "type with getVariableVerbiage - ConditionId = " + conditionId;

       srk.getSysLogger().warning(msg);
       return null;
     }
    }
    catch(FinderException fe)
    {
      srk.getSysLogger().error("CONDITION_HANDLER: Failed attempt to get variable verbiage - conditionId: " + conditionId );
      return null;
    }

    String out = cond.getConditionTextByLanguage(languagePreferenceId);

    if(out == null) return null;

    ConditionParser p = new ConditionParser();

    return p.parseText(out,languagePreferenceId,d,srk);
  }

 /**
 *  <pre>gets the VariableVerbiage for the Condition with the given Condition.condtionDesc. The
 *  verbiage is parsed and substitutions are made based on the given Deal and Language
 *  preference.
 *  </pre>
 *  @return the parsed ConditionVerbiage for the indicated language.
 *  @param  d -  the Deal entity providing parse context.
 *  @param  label -  the condition desc for the condition with verbiage to parse
 *  @param  languagePreferenceId - the verbiage language returned.
 */
  public String getVariableVerbiage(DealPK dpk, String label, int languagePreferenceId )throws Exception
  {
     Deal d = new Deal(srk,null,dpk.getId(),dpk.getCopyId());

     return getVariableVerbiage(d,label,languagePreferenceId);
  }


 /**
 *  <pre>gets the VariableVerbiage for the Condition with the given Condition.condtionDesc. The
 *  verbiage is parsed and substitutions are made based on the given Deal and Language
 *  preference.
 *  </pre>
 *  @return the parsed ConditionVerbiage for the indicated language.
 *  @param  d -  the Deal entity providing parse context.
 *  @param  label -  the condition desc for the condition with verbiage to parse
 *  @param  languagePreferenceId - the verbiage language returned.
 */

  public String getVariableVerbiage(Deal d, String label, int languagePreferenceId )throws Exception
  {

    if(label == null) return null;

    Condition cond = null;

    try
    {
     cond = new Condition(srk);

     cond = cond.findByTypeAndDescription(Mc.CONDITION_TYPE_VARIABLE_VERBIAGE,label);
    }
    catch(FinderException fe)
    {
      return null;
    }

    String out = cond.getConditionTextByLanguage(languagePreferenceId);

    if(out == null) return null;

    ConditionParser p = new ConditionParser();

    return p.parseText(out,languagePreferenceId,d,srk);
  }

   /**
 *  <pre>gets the VariableVerbiage for the Condition with the given Condition.condtionDesc. The
 *  verbiage is parsed and substitutions are made based on the given Deal and Language
 *  preference.
 *  </pre>
 *  @return the parsed ConditionVerbiage for the indicated language.
 *  @param  d -  the Deal entity providing parse context.
 *  @param  label -  the condition desc for the condition with verbiage to parse
 *  @param  languagePreferenceId - the verbiage language returned.
 */
  public String getElectronicVerbiage(Deal d, String label, int languagePreferenceId)throws Exception
  {
    if(label == null) return null;

    Condition cond = null;

    try
    {
     cond = new Condition(srk);

     cond = cond.findByTypeAndDescription(Mc.CONDITION_TYPE_ELECTRONIC_VERBIAGE,label);
    }
    catch(FinderException fe)
    {
      return null;
    }

    String out = cond.getConditionTextByLanguage(languagePreferenceId);

    if(out == null) return null;

    ConditionParser p = new ConditionParser();

    return p.parseText(out,languagePreferenceId,d,srk);
  }

  /**
 *  <pre>gets the ElectronicVerbiage for the Condition with the given conditionId. The
 *  verbiage is parsed and substitutions are made based on the given Deal and Language
 *  preference.
 *  </pre>
 *  @return the parsed ElectronicVerbiage for the indicated language.
 *  @param  d -  the Deal entity providing parse context.
 *  @param  conditionId -  the condition verbiage to parse.
 *  @param  languagePreferenceId - the verbiage language returned.
 */
  public String getElectronicVerbiage(Deal d, int condId, int languagePreferenceId)throws Exception
  {
    Condition cond = null;

    try
    {
     cond = new Condition(srk);
     cond = cond.findByPrimaryKey(new ConditionPK(condId));

     int condtype = cond.getConditionTypeId();

     if(condtype != Mc.CONDITION_TYPE_ELECTRONIC_VERBIAGE)
      return null;

    }
    catch(FinderException fe)
    {
      return null;
    }

    String out = cond.getConditionTextByLanguage(languagePreferenceId);

    ConditionParser p = new ConditionParser();

    return p.parseText(out,languagePreferenceId,d,srk);
  }

/**
 *  obtains and stores condition relating to the supplied <br/>
 *  conditionId as a DocumentTracking record for the indicated Deal.<br/>
 *
 *  @param conditionId - the primary key for the <code>Condition</code> to be used
 *  building this DocumentTracking record
 *  @param pk - Deal that 'owns' of this <code>DocumentTracking</code> entry
 *
 */
  //--Release2.1--//
  //// Product Team requests now to create a custom condition in a style of Standard condition
  //// (two records in the DocumentTrackingVerbiage table for both languages).
  //// Submit action on update of the documentText should propagate this change
  //// to the BOTH records (English and French) nevertheless what the session language is
  //// it at the moment.
  //// On the other hand for the new TD's ChangeRequest (joining Condition
  //// and DocTracking screens) will require the the update of the documentText
  //// based on the languageId.  This method will provide this and SHOULD STAY here.
  /**
  public DocumentTracking buildUserSelectedCondition(int conditionId, DealPK pk, int langId)throws Exception
  {
    Condition source = new Condition(srk,conditionId);

    DocumentTracking dt = new DocumentTracking(srk);

    Deal deal = new Deal(srk, null);
    deal = deal.findByPrimaryKey(pk);

    dt = dt.create(source, pk.getId(), pk.getCopyId(),Mc.DOC_STATUS_OPEN, Dc.DT_SOURCE_USER_SELECTED, langId);

    dt.setDocumentTrackingSourceId(Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED);

    ConditionParser parser = new ConditionParser();
    parser.parseAndSetText(source, dt, deal, srk);

    dt.ejbStore();

    return dt;
  }
  **/
/**
 *  builds a free form user composed <code>DocumentTracking</code>  <br/> and <code>DocumentTrackingVerbiage</code>
 *  records with the supplied text and label.
 *
 *  @param pk - Deal that 'owns' of this <code>DocumentTracking</code> entry
 *  @param text - the DocumentTrackingVerbiage.documentText
 *  @param label - the DocumentTrackingVerbiage.documentLabel
 *
 */
  public DocumentTracking buildFreeFormCondition(int conditionId, DealPK pk, String text, String label) throws Exception
  {
    Condition source = new Condition(srk,conditionId);

    DocumentTracking dt = new DocumentTracking(srk);

    logger.debug("CH@FrenchText:PD_Id: " + pk.getId());
    logger.debug("CH@FrenchText:Copy_Id: " + pk.getCopyId());

    dt = dt.create(source,pk.getId(),pk.getCopyId(),Mc.DOC_STATUS_OPEN, Dc.DT_SOURCE_CUSTOM_FREEFORM);

    dt.setDocumentTrackingSourceId(Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM);

    dt.setDocumentLabelForLanguage(0,label);

    dt.setDocumentLabelForLanguage(1,label);

    dt.setDocumentTextForLanguage(0,text);

    dt.setDocumentTextForLanguage(1,text);

    dt.ejbStore();

    return dt;

  }

  //--Release2.1--//
  //// Product Team requests now to create a custom condition in a style of Standard condition
  //// (two records in the DocumentTrackingVerbiage table for both languages).
  //// Submit action on update of the documentText should propagate this change
  //// to the BOTH records (English and French) nevertheless what the session language is
  //// it at the moment.
  //// On the other hand for the new TD's ChangeRequest (joining Condition
  //// and DocTracking screens) will require the the update of the documentText
  //// based on the languageId.  This method will provide this and SHOULD STAY here.
  /**
  public DocumentTracking buildFreeFormCondition(DealPK pk, String text, String label, int sessionLangId) throws Exception
  {
    Condition source = new Condition(srk,0);

    DocumentTracking dt = new DocumentTracking(srk);

    logger.debug("CH@FrenchText:PD_Id: " + pk.getId());
    logger.debug("CH@FrenchText:Copy_Id: " + pk.getCopyId());

    dt = dt.create(source,pk.getId(),pk.getCopyId(),Mc.DOC_STATUS_OPEN, Dc.DT_SOURCE_CUSTOM_FREEFORM, sessionLangId);

    dt.setDocumentTrackingSourceId(Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM);

    dt.setDocumentLabelForLanguage(0,label);

    dt.setDocumentLabelForLanguage(1,label);

    dt.setDocumentTextForLanguage(0,text);

    dt.setDocumentTextForLanguage(1,text);

    dt.ejbStore();

    return dt;

  }
  **/

/**
 *  retrieves a Collection of DocumentTracking entries valid for the commitment
 *  letter for the supplied  <code>Deal</code>. The DocumentTrackingVerbiage is
 *  parsed and in the indicated language.
 *  @author MCM Impl Team 
 *  @version 1.1 11-July-2008 XS_16.17 Added new function call to get the component related conditions
 */
  public Collection retrieveCommitmentLetterDocTracking(Deal deal,int languagePreference)throws Exception
  {
    DocumentTracking dt = new DocumentTracking(srk);

    Collection entries = dt.findForCommitmentLetter((DealPK)deal.getPk());
    /*****************MCM Impt Team changes XS_16.17 starts*******************************/
    Collection componentCondition=dt.getComponentCondition((DealPK)deal.getPk(), false);
    //combine both the collection 
    entries.addAll(componentCondition);
    /*****************MCM Impt Team changes XS_16.17 ends*******************************/  
    ArrayList textout = new ArrayList();
    

    if(!entries.isEmpty())
    {
      Iterator it = entries.iterator();

      ConditionParser p = new ConditionParser();

      while(it.hasNext())
      {
        dt = (DocumentTracking)it.next();

        String text = dt.getDocumentTextByLanguage(languagePreference);

        if(text == null)continue;

        text = p.parseText(text,languagePreference,deal,srk);

        dt.setDocumentTextForLanguage(languagePreference,text);

        textout.add(dt);
      }
    }

    return textout;
  }
 /**
 *  Performs updates to each <code>DocumentTracking</code> entry
 *  for the given <code>Deal</code> as required on Final Approval.  <br>
 *  This includes updates to DocumentRequestDate, DStatusChangeDate and
 *  DocumentStatus.     <br>
 *
 *  @param the deal to which documents are related
 */
  public void updateForApproval(Deal deal) throws Exception
  {
    DocumentTracking tr = new DocumentTracking(srk);
    List dts = (List)tr.findByDeal((DealPK)deal.getPk());

    Date commitDate = deal.getCommitmentIssueDate();

    Iterator it = dts.iterator();

    while(it.hasNext())
    {
      tr = (DocumentTracking)it.next();

      if(tr.getDocumentStatusId()== Mc.DOC_STATUS_OPEN)
      {
         tr.setDocumentStatusId(Mc.DOC_STATUS_REQUESTED);
      }

      tr.setDocumentRequestDate(commitDate);
      tr.setDStatusChangeDate(commitDate);
      tr.ejbStore();
    }

  }

  public void updateForPDecision(Deal deal) throws Exception
  {
    DocumentTracking tr = new DocumentTracking(srk);
    List dts = (List)tr.findByDeal((DealPK)deal.getPk());

    Date commitDate = deal.getCommitmentIssueDate();

    Iterator it = dts.iterator();

    while(it.hasNext())
    {
      tr = (DocumentTracking)it.next();

      if(tr.getDocumentStatusId()== Mc.DOC_STATUS_OPEN)
      {
        tr.setDocumentStatusId(Mc.DOC_STATUS_REQUESTED);
        tr.setDocumentRequestDate(commitDate);
        tr.setDStatusChangeDate(commitDate);
      }
      else
      {
        if(tr.getDocumentRequestDate() == null)
            tr.setDocumentRequestDate(commitDate);
        if(tr.getDStatusChangeDate() == null)
            tr.setDStatusChangeDate(commitDate);
      }

      tr.ejbStore();
    }

  }


  /**
   *  Deletes existing SystemGenerated DocumentTracking entries for this deal
   *  in preparation for repopulation.
   *
   */
  private void refreshSysGenDocTracking(Deal deal) throws Exception
  {
    DocumentTracking dt = null;

    Collection dts = getSystemGeneratedDocumentTracking(deal);

    Iterator it = dts.iterator();

    while(it.hasNext())
    {
      dt = (DocumentTracking)it.next();

      if(dt != null) dt.ejbRemove();
    }
  }





/**
 *   Update document tracking entities associated with the specified deal upon a commitment
 *   cancellation. Specifically this perfroms the following actions:
 *
 *   . removes all System Generated DocumentTracking entities for the given Deal.
 *   . sets request date and due date to null for all non-System Generated DocumentTracking entities
 *
 *   @return void
 */
  public void updateForCommitmentCancelled(Deal deal, boolean preservedStatus)throws Exception
  {
    DocumentTracking dt = new DocumentTracking(srk);

    Collection dts = dt.findByDeal(new DealPK(deal.getDealId(), deal.getCopyId()));

    Iterator it = dts.iterator();

    while(it.hasNext())
    {
        dt = (DocumentTracking)it.next();

        if (preservedStatus)
        {
            if (dt.getDocumentStatusId() == Mc.DOC_STATUS_APPROVED &&
                dt.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED)
            {
                dt.setDocumentStatusId(Mc.DOC_STATUS_RECEIVED);
                dt.ejbStore();
            }
        }
        else
        {
            if (dt.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED)
            {
                dt.ejbRemove();
            }
            else
            {
                dt.setDocumentRequestDate(null);
                dt.setDocumentDueDate(null);
                dt.ejbStore();
            }
        }
     }
  }

  public Collection getCustomDocumentTracking(Deal deal) throws Exception
  {
    DocumentTracking dt = new DocumentTracking(srk);

    return  dt.findByDealAndSource((DealPK)deal.getPk()
          ,Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM);
  }

  
  // ===========================================================================
  // utility method called by buildSystemGeneratedDocumentTracking
  // changes source for custom conditions
  //============================================================================
  private void removeDTMatch(Collection oldDocTracking , Collection newDocTracking) throws Exception
  {
    Iterator itNew = newDocTracking.iterator();
    Iterator itOld ;
    DocumentTracking dtOld;
    DocumentTracking dtNew;
    
    while( itNew.hasNext() ){
      dtNew = (DocumentTracking)itNew.next();
      itOld = oldDocTracking.iterator();
      while( itOld.hasNext() ){
        dtOld = (DocumentTracking)itOld.next();
        if( isDTMatch(dtOld,dtNew) ){
        	// check context class for 'looping conditions', contextid already checked in isDTMatch()
        	// just for completeness' sake, on the very unlikely chance that borrowerid = propertyid 
        	if (dtOld.getTagContextClassId() == dtNew.getTagContextClassId()) {
        		//remove from collection and database
            	itNew.remove();
        		dtNew.ejbRemove();
        	}
        }
      }
    }
  }

}