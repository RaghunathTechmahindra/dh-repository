package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class SupportAlimonyCondtion extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;
    debug("Condition -> " + this.getClass().getName());


    try
    {
        List liabs = (List)deal.getLiabilities();

        Iterator lt = liabs.iterator();
        Liability liability = null;

        while(lt.hasNext())
        {
          liability = (Liability)lt.next();
          if(liability.getPercentOutGDS() < 2d){
            continue;
          }
          int id = liability.getLiabilityTypeId();
          double mp = liability.getLiabilityMonthlyPayment();

          if( (id == Mc.LIABILITY_TYPE_CHILD_SUPPORT ||
               id == Mc.LIABILITY_TYPE_ALIMONY) && mp > 0)
          {
             if(cond == null)
             {
               cond = new Condition(srk, Dc.SUPPORT_ALIMONY_PAYMENTS);

               if(!cond.isActiveType()) return fml;

               parser = new ConditionParser();
             }

             fml.addValue( parser.createSystemGeneratedDocTracking(cond, deal,liability,srk));
          }
        }
    }
    catch(FinderException fe)
    {
      srk.getSysLogger().error("System Generated Condition Not Found - conditionDesc: Support\\Alimony Payments" );
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    //return new FMLQueryResult();
    return fml;
  }
}
