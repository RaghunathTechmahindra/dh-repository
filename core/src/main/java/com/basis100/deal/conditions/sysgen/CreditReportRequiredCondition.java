package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class CreditReportRequiredCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    Date appdate = deal.getApplicationDate();
    if(appdate == null)
     return fml;

    Calendar appcal = new GregorianCalendar();
    appcal.set(appdate.getYear(), appdate.getMonth(),appdate.getDate());
    appcal.add(appcal.DATE,-10);

    try
    {
      List bors = (List)deal.getBorrowers();

      Iterator it = bors.iterator();

      Borrower bor = null;
      Date cbdate = null;
      Calendar cbcal = new GregorianCalendar();

      cond = new Condition(srk,Dc.CREDIT_REPORT_REQUIRED);

      if(!cond.isActiveType()) return fml;

      parser = new ConditionParser();

      while(it.hasNext())
      {
        bor = (Borrower)it.next();
        cbdate = bor.getDateOfCreditBureauProfile();

        if(cbdate == null)
        {

          fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,bor,srk));
        }
        else
        {
          cbcal.set(cbdate.getYear(), cbdate.getMonth(),cbdate.getDate());

          if(!(appcal.before(cbcal)) )
          {
            fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,bor,srk));
          }
        }
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

    //return new FMLQueryResult();
    return fml;
  }
} 
