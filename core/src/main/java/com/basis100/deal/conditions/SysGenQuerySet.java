package com.basis100.deal.conditions;

import java.io.*;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Note: ALERT - this class should be generalized as a utility along with
 * SysGenQuerySet.... reads a file system directory or directory in archive and
 * constructs class names for the contents
 */

public class SysGenQuerySet {
    
    // logger
    private static final Log _log = LogFactory.getLog(SysGenQuerySet.class);

    private Set internal;

    private String pkg = "com/basis100/deal/conditions/sysgen";

    public SysGenQuerySet() throws Exception {
        init();
    }

    /*
     * init
     */
    public void init() throws Exception {
        
        // get simple name for this class
        String simpleClassName = this.getClass().getSimpleName();
        
        _log.info("SysGenQuerySet className: " + simpleClassName);
        _log.info("SysGenQuerySet className: " + this.getClass().getName());
        
        URL lsLocation = this.getClass()
                .getResource(simpleClassName + ".class");

        String location = lsLocation.toString();

        _log.info("SysGenQuerySet lsLocation: " + lsLocation);

        if (location != null
                && (location.startsWith("zip") || location.startsWith("jar")))
            internal = fromArchive(location);
        else
            internal = fromDirectory(location);
    }

    /*
     * get the list from Archive
     */
    private Set fromArchive(String archive) throws IOException,
            FileNotFoundException {
        
        _log.info("SysGenQuerySet archive: " + archive);

        Set out = new HashSet();

        if (archive != null) {
            
            // get a JarURLConnection
            URL url = new URL(archive);

            JarURLConnection jarConnection = (JarURLConnection) url
                    .openConnection();
            _log.info("entryName: jarurl: " + jarConnection.getJarFileURL());
            _log.info("URL: " + jarConnection.getURL());

            // enumeration the entries and put result in out
            JarFile jarFile = jarConnection.getJarFile();
            Enumeration entries = jarFile.entries();
            String name = "";
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                name = entry.getName();
                if (name.startsWith(pkg) && !entry.isDirectory()) {
                    name = name.substring(pkg.length() + 1);
                    int index = name.indexOf(".");

                    if (index != -1)
                        name = name.substring(0, index);

                    out.add("com.basis100.deal.conditions.sysgen." + name);
                }
            }
        }

        return out;
    }

    /*
     * get result from directory
     */
    private Set fromDirectory(String dir) throws MalformedURLException,
            URISyntaxException {
        // get the class name
        String simpleClassName = this.getClass().getSimpleName();
        _log.info("SysGenQuerySet className: " + simpleClassName);

        dir = dir.replace(simpleClassName + ".class", "sysgen");
        _log.info("SysGenQuerySet lsLocation: " + dir);

        URL directoryUrl = new URL(dir);

        Set out = new HashSet();
        File f = new File(directoryUrl.toURI());
        String[] filenames = f.list();

        for (int i = 0; i < filenames.length; i++) {
            String file = filenames[i];

            int index = file.indexOf('.');
            String calc = file.substring(0, index);

            out.add("com.basis100.deal.conditions.sysgen." + calc);
        }

        return out;
    }

    public int getSize() {
        return internal.size();
    }

    public Set getSet() {
        return internal;
    }

    public Iterator iterator() {
        return internal.iterator();
    }

    public String[] getAsStringArray() {
        String[] out = new String[internal.size()];
        Iterator it = internal.iterator();
        int index = 0;

        while (it.hasNext()) {
            out[index++] = (String) it.next();
        }

        return out;
    }

}
