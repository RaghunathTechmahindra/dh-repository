package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class InterestOnAdvancesCondition extends SysGenConditionExtractor
{
  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    try
    {
      Property p = new Property(srk,null);
      Collection props = p.findByDeal((DealPK)deal.getPk());
      Iterator it = props.iterator();
      parser = new ConditionParser();
      while(it.hasNext())
      {
        p = (Property)it.next();

        if(p.isNewConstruction())
        {
          cond = new Condition(srk, Dc.INTEREST_ON_ADVANCES);

          if(!cond.isActiveType()) return fml;

          DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,p,srk);

          fml.addValue(dt);
          return fml;    // we want to see only one instance of the condition
        }
      }

     }
     catch (Exception e)
     {
        srk.getSysLogger().error(e.getMessage());
        srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
     }

     return fml;
  }
}
