package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import MosSystem.Mc;

public class GECapitalInsuranceCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    try
    {
      int did = deal.getDealPurposeId();

      if( did == Mc.DEAL_PURPOSE_ASSUMPTION || did == Mc.DEAL_PURPOSE_INCREASED_ASSUMPTION
           || did == Mc.DEAL_PURPOSE_COVENANT_CHANGE || did == Mc.DEAL_PURPOSE_PARTIAL_DISCHARGE)
                return fml;

      int mid = deal.getMIIndicatorId();

      if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
            && deal.getMortgageInsurerId() == Mc.MI_INSURER_GE && deal.getMIPolicyNumber() == null)
      {
        cond = new Condition(srk, Dc.GE_CAPITAL_INSURANCE);

        if(!cond.isActiveType()) return fml;

        parser = new ConditionParser();

        DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,deal,srk);

        fml.addValue(dt);

        return fml;
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }

   return fml;
  }
} 
