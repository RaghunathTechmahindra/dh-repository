package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import MosSystem.Mc;

public class ReleaseOfCovenantCondition extends SysGenConditionExtractor
{

  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition -> " + this.getClass().getName());
    
    try
    {
      int did = deal.getDealPurposeId();

      if(!(did == Mc.DEAL_PURPOSE_ASSUMPTION
           || did == Mc.DEAL_PURPOSE_INCREASED_ASSUMPTION
           || did == Mc.DEAL_PURPOSE_COVENANT_CHANGE))
              return fml;

      List bors = (List)deal.getBorrowers();

      Iterator it = bors.iterator();

      Borrower bor = null;

      while(it.hasNext())
      {
        bor = (Borrower)it.next();

        if(bor.getBorrowerTypeId() == Mc.BT_RELEASED_CONVENANTOR)
        {
          if(cond == null)
          {
            cond = new Condition(srk, Dc.RELEASE_OF_COVENANT);
            
            if(!cond.isActiveType()) return fml;
            
            parser = new ConditionParser();
          }

          fml.addValue( parser.createSystemGeneratedDocTracking(cond,deal,bor,srk));
        }
      }
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
        throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
    }
     return fml;
  }
}
