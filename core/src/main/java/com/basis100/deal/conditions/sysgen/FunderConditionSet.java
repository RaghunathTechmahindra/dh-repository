package com.basis100.deal.conditions.sysgen;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.conditions.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import MosSystem.Mc;

public class FunderConditionSet extends SysGenConditionExtractor
{
  public FMLQueryResult extract(DealEntity de , SessionResourceKit srk)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    Condition cond = null;
    ConditionParser parser = null;

    debug("Condition Set -> " + this.getClass().getName());
    int condId;
    try
    {
      parser = new ConditionParser();
      cond = new Condition(srk);
      Collection condColl = cond.findByConditionType(Mc.CONDITION_TYPE_FUNDER);

      Iterator it = condColl.iterator();

      while(it.hasNext()){
        Condition oneCond = (Condition) it.next();
        condId = oneCond.getConditionId();
        DocumentTracking dt = parser.createSystemGeneratedDocTracking(oneCond,deal,deal,srk);
        fml.addValue(dt);
        debug("Condition -> FunderConditionSet entry# " + condId );
      }
    } // end try
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get DocTracking for FunderConditionSet ") ;
    }
    return fml;
  }
}