package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc ;

/**
 * <p>
 * Title: IADNumberOfDays
 * </p>
 * <p>
 * Description:Calculates IADNumberOfDays
 * </p>
 * @version 1.0 Initial Version <br>
 * @version 1.1 XS_11.18 Initial Version <br> - updated
 *          initParams(),getTarget(),doCalc()
 * @author MCM Impl Team <br>
 * @Date 09-Jul-2008 <br>
 *       Code Calc-34 <br>
 */
public class EffectiveAmortization extends DealCalc
{
  /**
   * <p>
   * Description: Default constructor that initalize the parameters and sets
   * the calculation number
   * </P>
   * @throws Exception -
   *             exception object
   * @version 1.0 Initial Version
   */
    public EffectiveAmortization()throws Exception
    {
      super();
      initParams();
      this.calcNumber = "Calc 34";
    }

    public Collection getTargets()
    {
      return this.targets;
    }
    
    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.18 | updated Exception
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
      try
      {
        addTarget(input); // Deal is the only target
      }
      catch(Exception e)
      {
          // XS_11.18 |Updated to StringBuffer from String
          StringBuffer msg = new StringBuffer(100);
          msg.append("Failed to get Targets for: ");
          msg.append(this.getClass().getName());
          msg.append(" :[Input]: ");
          msg.append(input.getClass().getName());
          msg.append("  Reason: " + e.getMessage() + " @input: "
                  + this.getDoCalcErrorDetail(input));
          logger.error(msg.toString());
          throw new TargetNotFoundException(msg.toString());
      }
    }
    
    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     * @version 1.1 XS_11.18 | Updated 
     *        -  Calc logic for Deal level product is a line of credit
     *        - Exception
     */
    public void doCalc(Object target) throws Exception
    {
      trace(this.getClass().getName());
      Deal deal = (Deal)target;
      MtgProd mtgProd=deal.getMtgProd();
      
      try
      {
        // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
        PricingRateInventory pricingRateInventory = new  PricingRateInventory(srk, deal.getPricingProfileId());
        int ppId = pricingRateInventory.getPricingProfileID();
        PricingProfile  pricingProfile = ( PricingProfile) this.entityCache.find(srk, new PricingProfilePK(ppId));
  
        String rateCode = pricingProfile.getRateCode () ;
        int repaymentTypeId = deal.getRepaymentTypeId();
        
        int UnderwriteAsTypeId = mtgProd.getUnderwriteAsTypeId(); 
        
        //If the deal level product is a line of credit then effectiveAmortization is set to 0.
        if(UnderwriteAsTypeId == Mc.COMPONENT_TYPE_LOC )
        {
            int effectiveAmortizationMonths = 0;
            deal.setEffectiveAmortizationMonths(effectiveAmortizationMonths);
            deal.ejbStore();
            return;
            
        }
        
        trace( "RateCode = " + rateCode + ", repaymentTypeId = " + repaymentTypeId + ", PricingProfileID = " + ppId);
        if ( rateCode != null )
        {
      	  //if ( rateCode.equals ( "UNCNF" ) )
          if ( rateCode.equals ( "UNCNF" ) || repaymentTypeId == 2 || repaymentTypeId == 3)
          {
              trace( "AmoritizationTerm = " + deal.getAmortizationTerm () );
              deal.setEffectiveAmortizationMonths ( validate(deal.getAmortizationTerm ()) );
              deal.ejbStore ();
              return;
          }
       }
  
        double ttlPymtAmt = deal.getTotalPaymentAmount();
        int pymtFreqId = deal.getPaymentFrequencyId ();
        int result = 0;
        trace( "TotalPaymentAmount = " +  ttlPymtAmt + ", PaymentFrequencyId = " + pymtFreqId);
        if (ttlPymtAmt > 0)
        {
             double paymentFrequency =
                  DealCalcUtil.annualize (deal.getPaymentFrequencyId () , "PaymentFrequency" ) ;
  
            // Get the InterestCompoundDesc from deal.MtgProg.interestcompoundId -- BILLY 07Dec2001
             // added inst id for ML
            String InterestCompoundDesc 
                = PicklistData.getMatchingColumnValue(-1,
                                                      "interestcompound",
                                                      deal.getMtgProd().getInterestCompoundingId(), 
                                                      "interestcompounddescription");
             double interestFactor =
                  DealCalcUtil.interestFactor ( deal.getNetInterestRate () , paymentFrequency, InterestCompoundDesc ) ;
  
             double paymentAmount = deal.getPandiPaymentAmount() + deal.getAdditionalPrincipal();
             double ttlLoanAmount = deal.getTotalLoanAmount();
  
             trace( "PaymentFrequency = " + paymentFrequency + ", paymentAmount = " + paymentAmount);
             trace( "InterestRate = " + deal.getNetInterestRate() + ", TotalLoanAmount = " + ttlLoanAmount);
             trace( "InterestCompoundDesc = " + InterestCompoundDesc + ", InterestFacotr = " +  interestFactor ) ;
  
             double effectiveAmoritizationMonths =
                  Math.log( ( 1 - ttlLoanAmount * ( interestFactor / paymentAmount ) ) )
                  	/ Math.log( Math.pow( 1 + interestFactor, -1 ) ) ;
             
            trace( "calculated effectiveAmoritizationMonths = " + effectiveAmoritizationMonths);
            switch ( deal.getPaymentFrequencyId() )
            {
                case Mc.PAY_FREQ_SEMIMONTHLY :
                  effectiveAmoritizationMonths /= 2;
                  break;
                case Mc.PAY_FREQ_BIWEEKLY : case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY :
                  effectiveAmoritizationMonths /= 2.1666667 ;
                  break;
                case Mc.PAY_FREQ_WEEKLY : case Mc.PAY_FREQ_ACCELERATED_WEEKLY :
                  effectiveAmoritizationMonths /= 4.3333333 ;
                  break;
             }
  
            result = (int) effectiveAmoritizationMonths ;
            trace( "Effecitve Amoritization Months = " + effectiveAmoritizationMonths);
            if ( (
              (int)(effectiveAmoritizationMonths * 1000.00)
                  -  ( (int) ( effectiveAmoritizationMonths ) ) * 1000.00
              )   > 9 )
                result ++ ;
            
            trace("interestFactor = " + interestFactor);
        }
        else  // if totalPaymentAmount > 0
          result = (int)getMaximumCalculatedValue();
  
        result = (result < 0 || result > 999) ? 0 : result;
  
        // Spec. changes -- By BILLY 11Jan2002
        if((result - deal.getAmortizationTerm()) == 1)
        {
          result = deal.getAmortizationTerm();
        }
        // ===================================
  
        trace("netLoanAmt = " + deal.getNetLoanAmount() + ", result = " + result);
        deal.setEffectiveAmortizationMonths( validate(result) );
        deal.ejbStore ();
        return;
  
      }
      catch ( Exception e )
      {
          // XS_11.18 |Updated to StringBuffer from String
          StringBuffer msg = new StringBuffer(100);
          msg.append("Begin Failure in Calculation");
          msg.append(this.getClass().getName());
          msg.append(":[Target]:");
          msg.append(target.getClass().getName());
          msg.append("  Reason: ");
          msg.append(e.getMessage());
          msg.append("@Target: ");
          msg.append(this.getDoCalcErrorDetail(target));
          logger.debug(msg.toString());
      }
  
       deal.setEffectiveAmortizationMonths(DEFAULT_FAILED_INT);
       deal.ejbStore ();
    }

    public boolean hasInput(CalcParam input)
    {
      return this.inputParam.contains(input);
    }
    
    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 Initial Version
     */
    private void initParams() throws ParamTypeException
    {
      targetParam = new ArrayList();
      inputParam  = new ArrayList();
      inputClassTypes = new HashSet();
  
      addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
      addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
      addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
      addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
      addInputParam(new CalcParam(ClassId.DEAL, "totalPaymentAmount"));
      addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
      addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
  
      	addTargetParam(new CalcParam(ClassId.DEAL, "effectiveAmortizationMonths", T3));
    }
}