package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name: Teaser Principal and Interest Payments
//Code: Calc 87
//07/13/2000

public class TeaserNetInterestRate extends DealCalc {

  public TeaserNetInterestRate() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 87";
  }

   public Collection getTargets()
  {    return this.targets;  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof Deal )
          addTarget ( input );
    }
    catch ( Exception e )
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal) target;

    try
    {
      trace( "Calc 87----deal.teaserTerm = " + deal.getTeaserTerm () ) ;
      if ( deal.getTeaserTerm () > 0 )
      {
          trace("Calc 87----deal.getTeaserTerm () > 0");
          MtgProd mtgProd = (MtgProd) entityCache.find ( srk,
                  new MtgProdPK( deal.getMtgProdId () )) ;

          double teaserNetInterestRate = 0.0 ;

          //--Ticket#1736--18July2005--start--//
          PricingProfile pp = mtgProd.getPricingProfile();
          PricingRateInventory pricingRateInventory = pp.findLatestInventoryByPricingProfileId(pp.getPricingProfileId());

          trace( "Calc 87----lastPricingProfileId = " + pp.getPricingProfileId() );
          trace( "Calc 87----lastPricingRateInventoryId = " + pricingRateInventory.getPricingRateInventoryID() );

          teaserNetInterestRate = pricingRateInventory.getTeaserDiscount();

          trace( "Calc 87----deal.netInterestRate = " + deal.getNetInterestRate () );
          trace( "Calc 87----pricingRateInventory.teaserDiscount = " + pricingRateInventory.getTeaserDiscount () );

          teaserNetInterestRate = deal.getNetInterestRate () -  pricingRateInventory.getTeaserDiscount () ;
          //--Ticket#1736--18July2005--end--//

          trace( "Calc 87----teaserNetInterestRate = " + teaserNetInterestRate);
          deal.setTeaserNetInterestRate (  teaserNetInterestRate );
          deal.ejbStore();
       }
       else
       {
          deal.setTeaserNetInterestRate ( 0 );
          deal.ejbStore();
       }
       return ;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

     deal.setTeaserNetInterestRate (DEFAULT_FAILED_DOUBLE );
     deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam ( new CalcParam( ClassId.DEAL , "netInterestRate") ) ;
    addInputParam ( new CalcParam ( ClassId.DEAL  , "teaserDiscount")) ;
    addInputParam ( new CalcParam ( ClassId.DEAL  , "MtgProdId")) ;
    addInputParam (new CalcParam (ClassId.DEAL, "teaserTerm"));

    addTargetParam ( new CalcParam ( ClassId.DEAL , "teaserNetInterestRate")) ;

  }

}
