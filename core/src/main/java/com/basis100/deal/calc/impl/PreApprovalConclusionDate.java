package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

//Name: PreApproval Conclusion Date
//Code:  Calc 71
//07/10/2000

public class PreApprovalConclusionDate extends DealCalc
{

  public PreApprovalConclusionDate()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 71";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget ( input );
    }
    catch ( Exception e )
    {
      String msg = "Failed to get Targets for: " + this.getClass().getName();
      msg += " :[Input]: " + input.getClass ().getName () ;
      logger.error(msg);
      msg = "  Reason: " + e.getMessage();
      msg += " @input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = ( Deal ) target ;
     try
     {
        Calendar  theDate = Calendar.getInstance ();

        if (  deal.getPreApprovalOriginalOfferDate () != null )
        {
          theDate.setTime (  deal.getPreApprovalOriginalOfferDate ());
          theDate.add ( Calendar.DAY_OF_MONTH ,  180 ); //180 calendar days from..

          deal.setPreApprovalConclusionDate ( theDate.getTime () );
          deal.ejbStore ();
        }
        return;
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    deal.setPreApprovalConclusionDate ( DEFAULT_FAILED_DATE );
    deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "preApprovalOriginalOfferDate"));

    addTargetParam(new CalcParam(ClassId.DEAL, "preApprovalConclusionDate"));
  }     

}
