package com.basis100.deal.calc;

/**
 * <p>Title: ClassId</p>
 *
 * <p>Description: Class ids list used for calcs and ingestion.</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * @author 
 * @version 1.1
 * Date: 08/10/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added BORROWERIDENTIFICATION constant and edited MAX from 30 to 31<br>
 * @version 1.2
 * Date:09-Jun-2008 <br>
 * Author: MCM Impl Team <br>
 * Story:XS_11.11
 * Change: <br>
 * Added COMPONENT, COMPONENTLOC, COMPONENTMORTGAGE  constant and edited MAX from 31 to 34 <br>
 ** @version 1.3
 * Date:25-Jun-2008 <br>
 * Author: MCM Impl Team <br>
 * Story:XS_11.5
 * Change: <br>
 * Added COMPONENTLoan  constant and edited MAX from 34 to 35 <br>
 * @version 1.4
 * Date:23-Jul-2008 <br>
 * Author: MCM Impl Team <br>
 * Story:XS_11.16
 * Change: <br>
 * Added COMPONENTSUMMARY, COMPONENTCREDITCARD, COMPONENTOVERDRAFT  constant and edited MAX from 34 to 38 <br>
 * @version 1.5
 * Date:08-Aug-2008 <br>
 * Author: MCM Impl Team <br>
 * Story:XS_1.1
 * Change: <br>
 * Added  QUALIFYDETAIL constant and edited MAX from 38 to 39 <br>
*
 */
public class ClassId
{
  public static final int DEFAULT              = -1;

  public static final int DEAL                 = 1;
  public static final int BORROWER             = 2;
  public static final int PROPERTY             = 3;
  public static final int ASSET                = 4;
  public static final int DOWNPAYMENTSOURCE    = 5;
  public static final int INCOME               = 6;
  public static final int LIABILITY            = 7;
  public static final int PROPERTYEXPENSE      = 8;
  public static final int ESCROW               = 9;
  public static final int CONDITION            = 10;
  public static final int PRICINGPROFILE       = 11;
  public static final int PRICINGRATEINVENTORY = 12;
  public static final int FEE                  = 13;
  public static final int DEALFEE              = 14;
  public static final int EMPLOYMENTHISTORY    = 15;
  public static final int DOCUMENTTRACKING     = 16;
  public static final int MTGPROD              = 17;
  public static final int BRIDGE               = 19;
  public static final int PARTYPROFILE         = 20;
  public static final int BORROWERADDRESS      = 21;
  public static final int CREDITREFERENCE      = 22;
  public static final int PARTYDEALPROPERTYASSOC   = 23;
  public static final int CONTACT              = 24;
  public static final int CREDITBUREAUREPORT   = 25;
  public static final int APPRAISALORDER       = 26;
  public static final int LIFEDISABILITYPREMIUMS = 27;
  //--DJ_CR136--23Nov2004--start--//
  public static final int INSUREONLYAPPLICANT = 28;
  public static final int LIFEDISPREMIUMSIONLYA = 29;
  //--DJ_CR136--23Nov2004--end--//
//***** Change by NBC Impl. Team - Version 1.1 - Start *****//
public static final int BORROWERIDENTIFICATION = 30;
//***** Change by NBC Impl. Team - Version 1.1 - End*****//

 // public static final int MAX      = 30;

//***** Change by MCM Impl. Team - Version 1.2 - Starts*****//

public static final int COMPONENT = 31;
public static final int COMPONENTLOC = 32;
public static final int COMPONENTMORTGAGE =33;
//***** Change by MCM Impl. Team - Version 1.2 - ends*****//

//***** Change by MCM Impl. Team - Version 1.3 - Starts*****//
public static final int COMPONENTLOAN =34;
//***** Change by MCM Impl. Team - Version 1.3 - ends*****//

//***** Change by MCM Impl. Team - Version 1.4 - XS_11.16 Starts*****//
public static final int COMPONENTSUMMARY =35;
public static final int COMPONENTCREDITCARD =36;
public static final int COMPONENTOVERDRAFT =37;
//***** Change by MCM Impl. Team - Version 1.4 - XS_11.16 ends*****//

//***** Change by MCM Impl. Team - Version 1.5 - XS_1.1 Starts*****//
public static final int QUALIFYDETAIL =38;
public static final int MAX    = 39;
//***** Change by MCM Impl. Team - Version 1.5 - XS_1.1 ends*****//

}



