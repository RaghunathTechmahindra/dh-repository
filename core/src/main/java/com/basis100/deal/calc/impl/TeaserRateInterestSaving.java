package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;

/**
 * 
 * <p>
 * Title: TeaserRateInterestSaving.java
 * </p>
 * 
 * <p>
 * Description: TeaserRateInterestSaving.java
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.1 <br>
 * Date: 11/8/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: <br>
 * 	Fixed calculation to follow specs, defect 1454
 */
public class TeaserRateInterestSaving extends DealCalc {
  	String errMsg;
  	
	/**
	 * 
	 * A constructor for this class.
	 * 
	 * @throws Exception
	 */
	public TeaserRateInterestSaving() throws Exception {
		super();
		initParams();
		this.calcNumber = "Calc 113";
		//String errMsg = "CALC Engine: EXCEPTION: TeaserRateInterestSaving.java failed during calculation. ";
	}

	/**
	 * method: getTargets description: returns the Collection object which
	 * contains all the defined target for the InterestOverTerm Class
	 * 
	 * @throws -
	 */
	public Collection getTargets() {
		return this.targets;
	}

	/**
	 * method: getTarget description: Method add the targets into the Calc Class
	 * 
	 * @param: Object, CaclMonitor
	 * @throws TargetNotFoundException
	 */
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
		try {
			addTarget(input);
		} catch (Exception e) {
			String msg = "Fail to get Targets" + this.getClass().getName();
			msg += ":[Input]:" + input.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@input: " + this.getDoCalcErrorDetail(input);
			logger.error(msg);
			throw new TargetNotFoundException(msg);
		}
	}

	/**
	 * method: hasInput description: Checks whether the required input exists or
	 * not.
	 * 
	 * @param CalcParam
	 * @return: Boolean
	 */
	public boolean hasInput(CalcParam input) {
		return this.inputParam.contains(input);
	}

	/**
	 * method: doCalc description: Performs the required business calculations.
	 * 
	 * @param: Object
	 * @throws: Exception
	 */
	public void doCalc(Object target) throws Exception {
		trace(this.getClass().getName());
		Deal deal = (Deal) target;
		double regularBalanceRemaining = 0.0;
		double regularInterestFactor = 0.0;
		double regularInterestOverTerm = 0.0;
		double teaserBalanceRemaining = 0.0;
		double teaserInterestFactor = 0.0;
		double teaserInterestOverTerm = 0.0;
		double teaserRateInterestSaving = 0.0;
		double teaserNetInterestRate = deal.getTeaserNetInterestRate();
		try {
			int term = 1;
			if ((teaserNetInterestRate <= 0d)) {
				deal.setTeaserRateInterestSaving(0.0);
				deal.ejbStore();
			} else {
			  	int paymentFrequencyId = deal.getPaymentFrequencyId();
			  	int repaymentTypeId = deal.getRepaymentTypeId();
			  	
				String decimalYear = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
					"com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
				double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
				double mPaymentsPerYear = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId,mDaysInYear);
				
			  	double totalPayments = 1;
				String DJNumberOfPayments = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.filogix.calc.DJNumberOfPayments", "N");
				if (DJNumberOfPayments.equalsIgnoreCase("Y")) {
				  // mPaymentsPerYear = 12, 24, 26 or 52
				  double mDJPaymentsPerYear = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId);
				  totalPayments = term * mDJPaymentsPerYear;
				} else {
				  totalPayments = term * mPaymentsPerYear;
				}
			
				int totalPaymentsTruncated = (int) totalPayments;
				double totalLoanAmount = deal.getTotalLoanAmount();
				double paymentAmount = deal.getPandiPaymentAmount();	
				double teaserPaymentAmount = deal.getTeaserPIAmount();
				int amortizationTerm = deal.getAmortizationTerm();				
			
				// get interest compounding descr 
				int mtgProdId = deal.getMtgProdId();
				int interestCompoundingId = -1;
				try {
					interestCompoundingId = (new MtgProd(srk, null, mtgProdId))
							.getInterestCompoundingId();
				} catch (Exception e) {
					srk.getSysLogger().warning( this.getClass(), errMsg
							+ (e.getMessage() != null ? "\n"
							+ e.getMessage() : ""));
				}
				if (interestCompoundingId == -1) {
					throw new Exception();
				}					
				String compound = BXResources.getPickListDescription(deal.getInstitutionProfileId(), "INTERESTCOMPOUND", Integer.toString(interestCompoundingId), 0);
				
				// calculate regular interest over term
				if ((repaymentTypeId == 2) || (repaymentTypeId == 3)) {
					regularBalanceRemaining = totalLoanAmount;
				} else if (deal.getAmortizationTerm() == term) {
					regularBalanceRemaining = 0.0;
				} else {
					double netInterestRate = deal.getNetInterestRate();
					
					//regularInterestFactor = Math.pow((1 + netInterestRate / (100 * compound)), compound / mPaymentsPerYear) - 1;
					regularInterestFactor = DealCalcUtil.interestFactor(netInterestRate, mPaymentsPerYear, compound);
					regularBalanceRemaining = deal.getTotalLoanAmount();	
					double interest = 0.0;
					double principalPaid = 0.0;
					
					for (int i = 1; i <= totalPaymentsTruncated; i++) {
						interest = DealCalcUtil.roundToNearestHundredth(regularBalanceRemaining * regularInterestFactor);
						principalPaid = DealCalcUtil.roundToNearestHundredth(paymentAmount - interest);
						regularBalanceRemaining = DealCalcUtil.roundToNearestHundredth(regularBalanceRemaining - principalPaid);
					}
					if (regularBalanceRemaining < 0d) {
						regularBalanceRemaining = 0.0;
					}
				}						
				regularInterestOverTerm = (paymentAmount * totalPaymentsTruncated) - (totalLoanAmount - regularBalanceRemaining);
				
				//calculate teaser interest over term
				if ((repaymentTypeId == 2)	|| (repaymentTypeId == 3)) {
					teaserBalanceRemaining = totalLoanAmount;
				} else if (amortizationTerm == term) {
					teaserBalanceRemaining = 0.0;
				} else {
//					Math.pow((1 + deal.getTeaserNetInterestRate() / (100 * compound)), (compound / mPaymentsPerYear)) - 1;
					teaserInterestFactor = DealCalcUtil.interestFactor(teaserNetInterestRate, mPaymentsPerYear, compound);					
					teaserBalanceRemaining = deal.getTotalLoanAmount();
					double interest = 0.0;
					double principalPaid = 0.0;
					
					for (int i = 1; i <= totalPaymentsTruncated; i++) {
						interest = DealCalcUtil.roundToNearestHundredth(teaserBalanceRemaining * teaserInterestFactor);
						principalPaid = DealCalcUtil.roundToNearestHundredth(teaserPaymentAmount - interest);
						regularBalanceRemaining = DealCalcUtil.roundToNearestHundredth(teaserBalanceRemaining - principalPaid);
					}
					if (teaserBalanceRemaining < 0d) {
						teaserBalanceRemaining = 0.0;
					}
				}				
				teaserInterestOverTerm = (teaserPaymentAmount * totalPaymentsTruncated)	- (totalLoanAmount - teaserBalanceRemaining);
				
				// calc and set teaser rate interest saving
				teaserRateInterestSaving = regularInterestOverTerm	- teaserInterestOverTerm;
				deal.setTeaserRateInterestSaving(teaserRateInterestSaving);
				deal.ejbStore();
			}
		} catch (Exception e) {
			String msg = "Benign Failure in Calculation"
					+ this.getClass().getName();
			msg += ":[Target]:" + target.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@Target: " + this.getDoCalcErrorDetail(target);
			logger.debug(msg);
		}
	}

	/**
	 * 
	 * initParams Method description: Initializes all the required input and
	 * output(targets) parameters
	 * 
	 * @throws ParamTypeException
	 */
	private void initParams() throws ParamTypeException {
		targetParam = new ArrayList();
		inputParam = new ArrayList();
		inputClassTypes = new HashSet();

		addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "teaserPIAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
		addInputParam(new CalcParam(ClassId.DEAL, "teaserNetInterestRate"));
		addInputParam(new CalcParam(ClassId.DEAL, "actualPaymentTerm"));

		addTargetParam(new CalcParam(ClassId.DEAL, "teaserRateInterestSaving"));
	}
}
