package com.basis100.deal.calc;

/**
 * 24/Jan/2008 DVG #DG688 LEN217161: Chantal Blanchard MI application fee from AIG does not display in SOLPAK 
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 * 26/Jan/2010 4.3GR, FXP23563, change P&I related calculation
 */

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;
/**
 * <p>
 * Title: DealCalcUtil.java
 * </p>
 * <p>
 * Description: DealCalcUtil.java
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.1 <br>
 *          Date: 08/23/2006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Modified to add field for interestOverTerm, teaserInterestSaved<br>
 *          Added methods: 1) pCalcNumOfPaymentsPerYear(int pPaymentFrequency,
 *          double daysInYear) 2) exMIFees( Deal deal, SessionResourceKit srk )
 *          3) getTotalAllFeeAmount(Deal deal) 4) exDefaultFeeAmount(int
 *          feeTypeId, SessionResourceKit srk) 5) exSelectedFeesTotalAmt(int[]
 *          pFeeArray, Deal deal)
 * @version 1.2 <br>
 *          Date : 17-Jun-2008 <br>
 *          Author: MCM Impl Team <br>
 *          Story: XS_11.4 <br>
 *          Change : Added <br>
 *          calculatePropertyTaxEscrowForDesjardins(..),calculatePropertyTaxEscrowForXceed(..),calculatePropertyTaxEscrowForTD(..)
 *          <br>
 *          methods to calculate Property Tax Escrow Amount.
 * @version 1.3 <br>
 *          Date : 23-Jun-2008 <br>
 *          Author: MCM Impl Team <br>
 *          Story: XS_11.6 <br>
 *          Change : Added calculatePerDiemInterestAmount(..) method.
 * @version 1.4<br>
 *          Date : 24-Jun-2008 <br>
 *          Author: MCM Impl Team <br>
 *          Story: XS_11.4 <br>
 *          Change : Review Comments Incorporated
 * @version 1.5<br>
 *          Date : 25-Jun-2008 <br>
 *          Author: MCM Impl Team <br>
 *          Story: XS_11.1 <br>
 *          Change : Added<br>
 *          calculatePandI(..),calculateNumOfPaymentsPerYear(..),calculateNoOfPaymentsToAmortize(..),numberOfPaymentsToAmortize(..)
 *          methods
 * @version 1.6 <br>
 *          Date:25-Jun-2008 <br>
 *          Author:MCM Impl Team <br>
 *          Story:XS_11.5<br>
 *          Change :Added getDaysBetween(..),getPeriodInDays(..) method. *
 * @version 1.7<br>
 *          Date:01-Jul-2008 <br>
 *          Author:MCM Impl Team <br>
 *          Story:XS_11.6<br>
 *          Change :Modified calculatePerDiemInterestAmount(..),calculateNoOfPaymentsToAmortize(.) method.
 * @version 1.8 <br>
 *          Date:22-Jul-2008 <br>
 *          Author:MCM Impl Team <br>
 *          Story:XS_11.9<br>
 *          Change :Added new method called interestFactor(..)
 * @version 1.9<br>
 *          Date:21-Jul-2008 <br>
 *          Author:MCM Impl Team <br>
 *          Story:XS_11.16<br>
 *          Change :Modified PandI3YearRate(...), Added condn for underWriterAsTypeId LOC        
 *                    
 */
public class DealCalcUtil
{

   private final static Logger logger = LoggerFactory.getLogger(DealCalcUtil.class);
    
   public static int MONTHS        = 12;

   public static String COMPOUND_FREQ_SEMI_ANNUAL_DESC  = "2";
   public static double COMPOUND_FREQ_SEMI_ANNUAL       = 2d;
   public static String COMPOUND_FREQ_SIMPLE_DESC  = "12";
   public static double COMPOUND_FREQ_SIMPLE       = 12d;
   //#DG688 let's establish this array as global list of MI premium fee types 
   public static final int [] MI_FEES = TypeConverter.intArrayTypeFromProp(
      Xc.COM_FILOGIX_MI_TYPE_FEES,
      new int[] { Mc.FEE_TYPE_CMHC_FEE, Mc.FEE_TYPE_GE_CAPITAL_FEE, Mc.FEE_TYPE_AIG_UG_FEE}
   );
   public static final int [] MI_PST_FEES = TypeConverter.intArrayTypeFromProp(
      Xc.COM_FILOGIX_MI_PST_TYPE_FEES, 
      new int[] { Xc.FEE_TYPE_CMHC_PST_ON_PREMIUM, Xc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE, 
         Xc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM, Xc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE,
         Xc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM, Xc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM_PAYABLE}
   );

  ///*****************************
   //mention chnage for calc
   
   //#DG538
//   static final boolean isMonthlyPaymtOnly = (PropertiesCache.getInstance().getProperty(
//       "com.basis100.calc.monthly.paymt.only", "N")).equals("Y");

   /**
    *  translates the value returned by the picklist period table description
    *  to an int value based on Calculation Specification.
    *
    *	PROPERTYEXPENSEPERIODID PEDESCRIPTION
    *	----------------------- -----------------------------------
    *	                      1 Semi-Annual
    *	                      0 Annual
    *	                      2 Monthly
    *	                      3 Quarterly
    *	                      4 Weekly
    *	PAYMENTFREQUENCYID PFDESCRIPTION
    *	------------------ -----------------------------------
    *	                 1 Semi Monthly
    *	                 2 Biweekly
    *	                 3 Accelerated Biweekly
    *	                 4 Weekly
    *	                 5 Accelerated Biweekly
    *	                 0 Monthly
    *
    *
    *	INCOMEPERIODID IPDESCRIPTION
    *	-------------- -----------------------------------
    *	             1 Semi-Annual
    *	             2 Monthly
    *	             3 Quarterly
    *	             4 Weekly
    *	             0 Annual
    *
    *
    */

   public static double annualize(int periodid, String table)
   {
      String desc = PicklistData.getDescription(table,periodid);

      boolean  special = false;

      if ( table.equalsIgnoreCase ("IncomePeriod" ) ||
            table.equalsIgnoreCase ("PropertyExpensePeriod" ) )
            special = true;

      if(desc == null) return 1;

      if(desc.equalsIgnoreCase("Semi Annual")
          || desc.equalsIgnoreCase ("Semi-Annual") )
      {
        return 2;
      }
      if(desc.equalsIgnoreCase("Quarterly"))
      {
        return 4;
      }
      if(desc.equalsIgnoreCase("Monthly"))
      {
        return 12;
      }
      if(desc.equalsIgnoreCase("Weekly"))
      {
        if ( special ) return 52 ;
        return 52.14 ; // =365.0/7.0
      }
      if(desc.equalsIgnoreCase("Semi Monthly")
          ||  desc.equalsIgnoreCase ("Semi-Monthly") )
      {
        return 24;
      }
      if(  desc.equalsIgnoreCase("BiWeekly")
            || desc.equalsIgnoreCase("Bi-Weekly") )
      {
        if ( special ) return 26 ;
        return 26.07 ; // =365/14.0
      }
      if(desc.equalsIgnoreCase("Accelerated Biweekly")
          || desc.equalsIgnoreCase ("Accelerated Bi-weekly"))
      {
        if ( special ) return 26 ;
        return 26.07 ;  // = 365/14.0;
      }
      if(desc.equalsIgnoreCase("Accelerated weekly"))
      {
        if ( special ) return 52 ;
        return 52.14 ;  // = 365/7.0;
      }
      else
      {
        return 1;
      }

   }

  /*
   * gets the lowest non zero value - if equal return a
   *
   */
  public static double lowerOf(double a, double b)
  {
    if(b <= 0) return a;
    if(a <= 0) return b;

    if( a <= b) return a;
    if( b < a) return b;

    return 0.0;
  }


   /**
   *  The interest factor is the payment frequency representation of the semi annual
   *  compounded Net Interest Rate.
   */
  public static double interestFactor(double interestRate, double annualizedPaymentFrequency, String compoundFreqDesc)
  {
     double ifact = 0;
     double compoundFreq = COMPOUND_FREQ_SEMI_ANNUAL;   // default
     // Added Compounding Interest Desc. i.e. to determintate by Caller -- BILLY 07Dec2001
     if(compoundFreqDesc != null && compoundFreqDesc.trim().equals(COMPOUND_FREQ_SIMPLE_DESC))
     {
        compoundFreq = COMPOUND_FREQ_SIMPLE;
     }


     ifact =  (interestRate / (100d * compoundFreq));

     ifact += 1 ;

     double exponent = compoundFreq/annualizedPaymentFrequency;

     ifact = Math.pow(ifact,exponent);
     ifact -= 1;

     return ifact;
  }
  /**
   *  This method calculates interestFactor based on 
   *  NetInterestRate and NoOfPaymentsPerYear
   */
  public static double interestFactor(double netInterestRate,double noOfPaymentsPerYear)
	{
	  double interestFactor=0;
	  interestFactor=(netInterestRate/noOfPaymentsPerYear)/100;
	  return interestFactor;
	}
	
  //#DG688 not used
  //private static int numberOfPaymentsToAmortize(int amortizationMonths, int numberOfAnnualPayments)
  //{
  //  return (amortizationMonths/12) * numberOfAnnualPayments;
  //}

  public static int sinCheckDigit(String sin)
  {
     sin = StringUtil.stripWhiteSpace(sin);
     long sinval = -1;

     if(sin.length()!= 9)
      return (int)sinval;
     else
      sinval = (int)TypeConverter.longTypeFrom(sin, -1);

      return (int)(sinval % 10);
  }

  //4.3GR, Jan 26, 2010, Deleted obsolete method - start
  //public static double GDSTDS3YearRate(SessionResourceKit srk, int pricingProfileId , CalcEntityCache entityCache ) throws   DealCalcException
  //4.3GR, Jan 26, 2010, Deleted obsolete method - end

  //------------------ NewPropertyNumberCheckDigit --------------------------------------
  public static String NewPropertyNumberCheckDigit( String CSMortgageNumber )
  {
    if ( CSMortgageNumber.length () == 8 )
      { // 8 digit is valid number
        int CSMnumber = (int ) TypeConverter.intTypeFrom ( "1" + CSMortgageNumber );

        int total = 0;
        total =  getNumber ( CSMnumber, 8 ) +
                  getNumber ( CSMnumber, 6 ) +
                  getNumber ( CSMnumber, 4 ) +
                  ( getNumber ( CSMnumber, 7 ) +
                    getNumber ( CSMnumber, 5 ) +
                    getNumber ( CSMnumber, 3 ) ) * 2       ;

        int checkDigit = ( 10 - total % 10 ) %  10;

        return ( CSMortgageNumber + checkDigit  );
      }
     else
        return null;
  }

  private static int getNumber ( int in, int location )
  {
    int i= (int) Math.pow ( 10, location-1 );
    int ii = in / i ;
    return ( ii - ( ii / 10 *10 ) );
  }
  //------------------ End of  NewPropertyNumberCheckDigit ------------------------------

  //4.3GR, Jan 20, 2010, Deleted obsolete method - start
  //public static double PandI(SessionResourceKit srk, CalcEntityCache entityCache, Deal deal) throws DealCalcException
  //4.3GR, Jan 20, 2010, Deleted obsolete method - end
  
  //4.3GR, Jan 26, 2010, Deleted obsolete method - start
  //public static double PandI3YearRate(SessionResourceKit srk, CalcEntityCache entityCache, Deal deal, int pymtFreqId) throws DealCalcException
  //4.3GR, Jan 26, 2010, Deleted obsolete method - end
  
  //4.3GR, Jan 26, 2010, Deleted obsolete method - start
  //private static double clacPaymentFreq(int paymentFreqId, boolean isDJDaysYear) 
  //4.3GR, Jan 26, 2010, Deleted obsolete method - end
  
	//	  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
  /**
   * Returns number of payments per year based on paymentFrequencyId and number of days in year used
   * (365.25 or 365)
   * 
   * @param pPaymentFrequency
   * @param daysInYear
   * @return
   */
  public static double pCalcNumOfPaymentsPerYear(int pPaymentFrequency, double daysInYear) {
	  double paymentsPerYear;
	  switch (pPaymentFrequency) {
	  	case Mc.PAY_FREQ_SEMIMONTHLY:				{ paymentsPerYear = 24d; break; }
	  	case Mc.PAY_FREQ_BIWEEKLY:					{ paymentsPerYear = daysInYear/14d; break; }
	  	case Mc.PAY_FREQ_WEEKLY:					{ paymentsPerYear = daysInYear/7d; break; }
	  	case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:	{ paymentsPerYear = daysInYear/14d; break; }
	  	case Mc.PAY_FREQ_ACCELERATED_WEEKLY:	{ paymentsPerYear = daysInYear/7d; break; }
	  	default: paymentsPerYear = 12d;
	  }
	  return paymentsPerYear;
  }
  
  /**
   * Returns number of payments per year based on paymentFrequencyId 
   *
   * @param pPaymentFrequency
   * @param daysInYear
   * @return
   */
  public static int pCalcNumOfPaymentsPerYear(int pPaymentFrequency) {
	  int paymentsPerYear;
	  switch (pPaymentFrequency) {
	  	case Mc.PAY_FREQ_SEMIMONTHLY:				{ paymentsPerYear = 24; break; }
	  	case Mc.PAY_FREQ_BIWEEKLY:					{ paymentsPerYear = 26; break; }
	  	case Mc.PAY_FREQ_WEEKLY:					{ paymentsPerYear = 52; break; }
	  	case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:	{ paymentsPerYear =26; break; }
	  	case Mc.PAY_FREQ_ACCELERATED_WEEKLY:	{ paymentsPerYear = 52; break; }
	  	default: paymentsPerYear = 12;
	  }
	  return paymentsPerYear;
  }

  /**
   * If MIStatusId is APPROVAL RECEIVED or APPROVED, sums up insurer fee from DealFee table, otherwise
   * uses default insurer's fee amount from Fee table.
   *  
   * WARNING: if deal has associated insurer's fee from different insurers (CMHC, GE, AIG UG), they will 
   * all be summed up.
   *  
   * @param feeTypeId
   * @return
   */
  public static double exMIFees( Deal deal, SessionResourceKit srk ) {
  	double fee = 0d;
	int miStatusId = deal.getMIStatusId();
	int mortgageInsurerId = deal.getMortgageInsurerId();
		if( miStatusId == Mc.MI_STATUS_APPROVAL_RECEIVED || miStatusId == Mc.MI_STATUS_APPROVED ) {
			fee = exSelectedFeesTotalAmt(MI_FEES, deal);
		}
		else {
			switch (mortgageInsurerId) {
				case Mc.MI_INSURER_CMHC: {
					fee = exDefaultFeeAmount(Mc.FEE_TYPE_CMHC_FEE, srk);
					break;
				}
				case Mc.MI_INSURER_GE: {
					fee = exDefaultFeeAmount(Mc.FEE_TYPE_GE_CAPITAL_FEE, srk);
					break;
				}
				case Mc.MI_INSURER_AIG_UG: {
					fee = exDefaultFeeAmount(Mc.FEE_TYPE_AIG_UG_FEE, srk);
					break;
				}
			}
		}
		return fee;
  }   

  /**
   * Finds default fee amount for a fee represented by feeTypeId
   * 
   * @param feeTypeId
   * @return
   */
  public static double exDefaultFeeAmount(int feeTypeId, SessionResourceKit srk) {
  	double val = 0d;
  	try{
  		val = (new Fee(srk, null)).findFirstByType(feeTypeId).getDefaultFeeAmount();
  	}
	   catch(Exception e){
		   //String msg = "DisclosureDataProvider failed while extracting default fee amount." + (e.getMessage() != null ? "\n" + e.getMessage() : ""); //#DG688   	
	   }
  	return val;
  }
  
  /**
   * Checks the current deal's fees against feeTypeIDs included in the input array and
   * sums those that have been used
   *
   * @param pFeeArray int[]
   * @return double
   */
  public static double exSelectedFeesTotalAmt(int[] pFeeArray, Deal deal) {
	   double val = 0d;
	   
	   try{		   
		   Collection dealFees = deal.getDealFees();
		   Iterator it = dealFees.iterator();
		   double feeTotal = 0;

		   while(it.hasNext()) {
			   DealFee oneFee = (DealFee) it.next();
			   int feeTypeID = oneFee.getFee().getFeeTypeId();
			   for(int i=0; i<pFeeArray.length; i++ ) {
				   if (feeTypeID == pFeeArray[i]) {
					   feeTotal += oneFee.getFeeAmount();
					   break;
				   }
			   }
		   }
		   val = feeTotal;
	   }
	   catch(Exception e){
		   //String msg = "DisclosureDataProvider failed while totaling fee amount." + (e.getMessage() != null ? "\n" + e.getMessage() : ""); //#DG688   	
	   }
	   return val;
  }
  
  /**
   * 
   * @param deal
   * @return
   * @throws Exception
   * 
   * Date: 11/8/2006 <br>
   * Author: NBC/PP Implementation Team <br>
   * Change: <br>
   * 	Fixed calculation to follow specs (Ontario and default) & feeStatusId, defect #1454
   */
  public static double getTotalAllFeeAmount(Deal deal,SessionResourceKit srk) throws Exception
  {
	  double totalAllFeeAmount = 0.0;
		
	  	//Removed the following code for the TAR fix -  SCR #4680 by NBC Impl. Team -Starts
		/*PropertiesCache.addPropertiesFile(Config
				.getProperty("SYS_PROPERTIES_LOCATION")
				+ "\\mossys.properties");*/
				
		/*ResourceManager.init(Config.getProperty("SYS_PROPERTIES_LOCATION"),
				PropertiesCache.getInstance().getProperty(
						"com.basis100.resource.connectionpoolname"));
		  SessionResourceKit srk = new SessionResourceKit("TotalAllFeeAmount");*/
		
		//Removed the following code for the TAR fix -  SCR #4680 by NBC Impl. Team -Ends

		try {
			Collection dealFees = deal.getDealFees();
			Iterator it = dealFees.iterator();
						
			Property property = new Property(srk, null);
			property = property
               .findByPrimaryProperty(deal.getDealId(), deal
                   .getCopyId(), deal.getInstitutionProfileId());
			
			switch(property.getProvinceId()) {
		   	 case Mc.PROVINCE_BRITISH_COLUMBIA: {
			   	while(it.hasNext()) {
			   		DealFee oneFee = (DealFee) it.next();
			   		int feeTypeId = oneFee.getFee().getFeeTypeId();
		  if (( feeTypeId == Mc.FEE_TYPE_BUYDOWN_FEE || feeTypeId == Mc.FEE_TYPE_BROKER_FEE ) 
			  && oneFee.getFeeStatusId() != Mc.FEE_STATUS_WAIVED) {
			   			totalAllFeeAmount += oneFee.getFeeAmount();
			   		}
			   	}
			   	break;
		   	 }
		   	 case Mc.PROVINCE_ALBERTA:
		   	 case Mc.PROVINCE_PRINCE_EDWARD_ISLAND: {
		   	   totalAllFeeAmount += deal.getMIPremiumAmount();		   	   
			   	while(it.hasNext()) {
			   		DealFee oneFee = (DealFee) it.next();
			   		int feeTypeId = oneFee.getFee().getFeeTypeId();
		  if(( feeTypeId == Mc.FEE_TYPE_BUYDOWN_FEE  || feeTypeId == Mc.FEE_TYPE_BROKER_FEE ||
			  feeTypeId == Mc.FEE_TYPE_CMHC_PREMIUM || feeTypeId == Mc.FEE_TYPE_GE_CAPITAL_PREMIUM )
			  && oneFee.getFeeStatusId() != Mc.FEE_STATUS_WAIVED) {
			   			totalAllFeeAmount += oneFee.getFeeAmount();
			   		}
			   	}
			   	break;
		   	 }
		   	 default: {
		   		totalAllFeeAmount += deal.getMIPremiumAmount();
		   		totalAllFeeAmount += deal.getMIPremiumPST();
		if (deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECEIVED || deal.getMIStatusId() == Mc.MI_STATUS_APPROVED) {
		  while(it.hasNext()) {
			DealFee oneFee = (DealFee) it.next();
			int feeTypeId = oneFee.getFee().getFeeTypeId();
			if(( feeTypeId == Mc.FEE_TYPE_APPRAISAL_FEE 			|| feeTypeId == Mc.FEE_TYPE_AIG_UG_FEE ||
				feeTypeId == Mc.FEE_TYPE_APPLICATION_FEE 			|| feeTypeId == Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED ||
				feeTypeId == Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL || feeTypeId == Mc.FEE_TYPE_BRIDGE_FEE ||
				feeTypeId == Mc.FEE_TYPE_BROKER_FEE 				|| feeTypeId == Mc.FEE_TYPE_BUYDOWN_FEE ||
				feeTypeId == Mc.FEE_TYPE_CMHC_FEE  				|| feeTypeId == Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE ||
				feeTypeId == Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE || feeTypeId == Mc.FEE_TYPE_GE_CAPITAL_FEE ||
				feeTypeId == Mc.FEE_TYPE_STANDBY_FEE 				|| feeTypeId == Mc.FEE_TYPE_TITLE_INSURANCE_FEE ||
				feeTypeId == Mc.FEE_TYPE_CANCELLATION_FEE			|| feeTypeId == Mc.FEE_TYPE_COUREER_FEE ||
				feeTypeId == Mc.FEE_TYPE_INSPECTION_FEE				|| feeTypeId == Mc.FEE_TYPE_ORIGINATION_FEE ||
				feeTypeId == Mc.FEE_TYPE_TERANET_FEE				|| feeTypeId == Mc.FEE_TYPE_WIRE_FEE ||
				feeTypeId == Mc.FEE_TYPE_BONUS_FEE)
				&& oneFee.getFeeStatusId() != Mc.FEE_STATUS_WAIVED){
			  totalAllFeeAmount += oneFee.getFeeAmount();
			}
		  }
		} else {
		   		 while(it.hasNext()) {
		   			 DealFee oneFee = (DealFee) it.next();
		   			 int feeTypeId = oneFee.getFee().getFeeTypeId();
		   			 
			if(( feeTypeId == Mc.FEE_TYPE_APPRAISAL_FEE       || feeTypeId == Mc.FEE_TYPE_APPLICATION_FEE     || 
		   				  feeTypeId == Mc.FEE_TYPE_BRIDGE_FEE          || feeTypeId == Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED ||
		   				  feeTypeId == Mc.FEE_TYPE_BROKER_FEE          || feeTypeId == Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL || 
		   				  feeTypeId == Mc.FEE_TYPE_BUYDOWN_FEE         || feeTypeId == Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE || 
		   				  feeTypeId == Mc.FEE_TYPE_STANDBY_FEE         || feeTypeId == Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE ||
		   				  feeTypeId == Mc.FEE_TYPE_TITLE_INSURANCE_FEE || feeTypeId == Mc.FEE_TYPE_BONUS_FEE ||				
		   				  feeTypeId == Mc.FEE_TYPE_CANCELLATION_FEE			|| feeTypeId == Mc.FEE_TYPE_COUREER_FEE ||
		  				  feeTypeId == Mc.FEE_TYPE_INSPECTION_FEE				|| feeTypeId == Mc.FEE_TYPE_ORIGINATION_FEE ||
						  feeTypeId == Mc.FEE_TYPE_TERANET_FEE				|| feeTypeId == Mc.FEE_TYPE_WIRE_FEE) 
						  && oneFee.getFeeStatusId() != Mc.FEE_STATUS_WAIVED) {
			  
		   				totalAllFeeAmount += oneFee.getFeeAmount();
		   			 }
			else if ((feeTypeId == Mc.FEE_TYPE_AIG_UG_FEE || feeTypeId == Mc.FEE_TYPE_CMHC_FEE 
				|| feeTypeId == Mc.FEE_TYPE_GE_CAPITAL_FEE ) && oneFee.getFeeStatusId() != Mc.FEE_STATUS_WAIVED) {
			  totalAllFeeAmount += oneFee.getFee().getDefaultFeeAmount();
		   		 }
		   	 }
		   }		   
		break;
	  }
	  } // end switch
			return totalAllFeeAmount;
			}
		 catch (Exception e) {
			 String msg = "Failure in DealCalcUtil.getTotalAllFeeAmount () \nReason: "+e ;//#DG688
		     srk.getSysLogger ().error ( msg );
		}
		 return 0.0;
  	}  
  
    /**
     * roundToNearestCent
     * 
     * @param double value to be rounded
     * @returns double - value rounded to nearest hundredth
     */
	public static double roundToNearestHundredth(double value){
	  java.math.BigDecimal bd = new java.math.BigDecimal(value);
	  bd = bd.setScale(2,java.math.BigDecimal.ROUND_HALF_UP);
	  return bd.doubleValue();		
	}
	
	//	  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
 
 //#DG538
	/**
	 * calculates P and I depending on the flags passed
	 * @param srk SessionResourceKit
	 * @param entityCache CalcEntityCache
	 * @param deal Deal
	 * @param is3YearRate boolean
	 * @return double
	 * @throws DealCalcException
	 */
	public static double PandIExpense(SessionResourceKit srk, CalcEntityCache entityCache, Deal deal, boolean is3YearRate) throws DealCalcException
	{ // Calculated regular and 3 year P&I expense

	    boolean isMonthlyPaymtOnly = (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
	        "com.basis100.calc.monthly.paymt.only", "N")).equals("Y");
	    double PandIExp, PAndI;
	    if(isMonthlyPaymtOnly) {
	        PAndI = is3YearRate ? //         DealCalcUtil.PandI3YearRate(srk, entityCache, deal, -100) :
	                deal.getPandIUsing3YearRate():
	                    deal.getPandIPaymentAmountMonthly();
	        PandIExp = PAndI * 12;
	    }
	    else {
	        PAndI = is3YearRate ? 
	                DealCalcUtil.calcPandIFully3Year(deal, entityCache) //4.3GR, Jan 26, 2010
	                : deal.getPandiPaymentAmount();
	                PandIExp = (PAndI + deal.getAdditionalPrincipal()) *
	        DealCalcUtil.annualize(deal.getPaymentFrequencyId(),"PaymentFrequency");
	    }
	    return PandIExp;
	}
    
  /**
   * Calculate payment frequency, i.e., number of payments per year.
   * 
   * @param paymentFreqId   Mc.Mc.PAY_FREQ_*
   * @param isDJDaysYear    if true 365.25 will be used. Otherwise 365.
   * @return
   */
  public static double calcPaymentFrequency(int paymentFreqId, boolean isDJDaysYear) {
      
      double daysInYear = isDJDaysYear ? 365.25d : 365d;
      
      double annualizedPmtFreq;
      switch (paymentFreqId) {
      case Mc.PAY_FREQ_WEEKLY:
      case Mc.PAY_FREQ_ACCELERATED_WEEKLY:      annualizedPmtFreq = daysInYear/7d; break;
      
      case Mc.PAY_FREQ_BIWEEKLY: 
      case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:    annualizedPmtFreq = daysInYear/14d; break;
      
      case Mc.PAY_FREQ_SEMIMONTHLY:             annualizedPmtFreq = 24d; break;
      default:                                  annualizedPmtFreq = 12d;
      }
      
      return annualizedPmtFreq;
  }
  /***********************************************************************
   **************MCM Impl team changes Starts - XS_11.4
  /**********************************************************************/
    /**
     * <p>
     * Decription : This method returns the calculated 'Property Tax Escrow
     * Amount' Conditon :com.basis100.calc.autocalctaxescrow ='TD'.
     * </p>
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @version 1.1 Review Comments Incorporated
     * @param interestAdjustmentDate -
     *            Interest Adjustment Date
     * @param totalAnnualTaxAmount -
     *            Total Annual Tax Amount
     * @return - Property Tax Escrow Amount of the component
     */
    public static double calculatePropertyTaxEscrowForTD(
            Date interestAdjustmentDate, double totalAnnualTaxAmount)
    {

        // Constants for Provine
        HashMap<Integer, Double> monthQuotient = new HashMap<Integer, Double>();
        monthQuotient.put(new Integer(Mc.JAN), new Double(24d / 17d));
        monthQuotient.put(new Integer(Mc.FEB), new Double(24d / 16d));
        monthQuotient.put(new Integer(Mc.MAR), new Double(22d / 15d));
        monthQuotient.put(new Integer(Mc.APR), new Double(20d / 14d));
        monthQuotient.put(new Integer(Mc.MAY), new Double(20d / 13d));
        monthQuotient.put(new Integer(Mc.JUN), new Double(18d / 12d));
        monthQuotient.put(new Integer(Mc.JUL), new Double(16d / 11d));
        monthQuotient.put(new Integer(Mc.AUG), new Double(18d / 10d));
        monthQuotient.put(new Integer(Mc.SEP), new Double(26d / 21d));
        monthQuotient.put(new Integer(Mc.OCT), new Double(26d / 20d));
        monthQuotient.put(new Integer(Mc.NOV), new Double(24d / 19d));
        monthQuotient.put(new Integer(Mc.DEC), new Double(24d / 18d));
      
        Integer IADMonth = 1; // default to 1
        if (interestAdjustmentDate != null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(interestAdjustmentDate);
            IADMonth = calendar.get(Calendar.MONTH) + 1; // 1=Jan...12=Dec
        }
        double monthQuotientForIAD = monthQuotient.get(IADMonth);
        double propertyTaxEscrowAmount = Math
                .round((totalAnnualTaxAmount / 12d) * monthQuotientForIAD
                        * 100d) / 100d;

        return propertyTaxEscrowAmount;

    }

    /**
     * Description : This method returns the calculated 'Property Tax Escrow
     * Amount' Condition :com.basis100.calc.autocalctaxescrow ='X'
     * @version 1.0 XS_11.4 16-Jun-2008 Initial version
     * @version 1.1 Review Comments Incorporated
     * @param firstPaymentDate -
     *            First Payment Date
     * @param totalAnnualTaxAmount -
     *            Total Annaual Tax Amount of the component
     * @param provinceId -
     *            Province Id of the property
     * @return - Property Tax Escrow Amount of the component
     */
    public static double calculatePropertyTaxEscrowForXceed(
            Date firstPaymentDate, double totalAnnualTaxAmount, int provinceId,
            int paymentFreqId)
    {
        /**
         * 'monthQuotientForNovaS' map contains month portion and the calulated
         * values.This map values applicable to only 'Nova Scotia' province (Key =
         * Month Portion and value = calculated values)
         */
        HashMap<Integer, Double> monthQuotientForNovaS = new HashMap<Integer, Double>();
        monthQuotientForNovaS.put(new Integer(Mc.JAN), new Double(1.5d / 20d));
        monthQuotientForNovaS.put(new Integer(Mc.FEB), new Double(1.5d / 20d));
        monthQuotientForNovaS.put(new Integer(Mc.MAR), new Double(1.5d / 19d));
        monthQuotientForNovaS.put(new Integer(Mc.APR), new Double(1.5d / 18d));
        monthQuotientForNovaS.put(new Integer(Mc.MAY), new Double(1.25d / 17d));
        monthQuotientForNovaS.put(new Integer(Mc.JUN), new Double(1.25d / 16d));
        monthQuotientForNovaS.put(new Integer(Mc.JUL), new Double(1.25d / 15d));
        monthQuotientForNovaS.put(new Integer(Mc.AUG), new Double(1.25d / 14d));
        monthQuotientForNovaS.put(new Integer(Mc.SEP), new Double(1.0d / 12d));
        monthQuotientForNovaS.put(new Integer(Mc.OCT), new Double(1.0d / 12d));
        monthQuotientForNovaS.put(new Integer(Mc.NOV), new Double(1.0d / 11d));
        monthQuotientForNovaS.put(new Integer(Mc.DEC), new Double(1.0d / 10d));

        /**
         * 'monthQuotientForNNM' map contains month portion and the calulated
         * values.This map values applicable to only for 'New Brunswick ,
         * Newfoundland' provinces (Key = Month Portion and value = calculated
         * values)
         */

        HashMap<Integer, Double> monthQuotientForNNM = new HashMap<Integer, Double>();
        monthQuotientForNNM.put(new Integer(Mc.JAN), new Double(1.25d / 14d));
        monthQuotientForNNM.put(new Integer(Mc.FEB), new Double(1.25d / 13d));
        monthQuotientForNNM.put(new Integer(Mc.MAR), new Double(1.25d / 12d));
        monthQuotientForNNM.put(new Integer(Mc.APR), new Double(1.0d / 12d));
        monthQuotientForNNM.put(new Integer(Mc.MAY), new Double(1.0d / 11d));
        monthQuotientForNNM.put(new Integer(Mc.JUN), new Double(1.0d / 10d));
        monthQuotientForNNM.put(new Integer(Mc.JUL), new Double(1.0d / 9d));
        monthQuotientForNNM.put(new Integer(Mc.AUG), new Double(1.5d / 20d));
        monthQuotientForNNM.put(new Integer(Mc.SEP), new Double(1.5d / 19d));
        monthQuotientForNNM.put(new Integer(Mc.OCT), new Double(1.5d / 18d));
        monthQuotientForNNM.put(new Integer(Mc.NOV), new Double(1.5d / 17d));
        monthQuotientForNNM.put(new Integer(Mc.DEC), new Double(1.25d / 15d));

        /**
         * 'monthQuotientForBAQSMOP' map contains month portion and the
         * calulated values.This map values applicable to only
         * 'BC,AB,QUE,SASK,MANITOBA,ONT,PEI' provinces (Key = Month Portion and
         * value = calculated values)
         */
        HashMap<Integer, Double> monthQuotientForBAQSMOP = new HashMap<Integer, Double>();
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.JAN), new Double(1.5d / 18d));
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.FEB), new Double(1.5d / 17d));
        monthQuotientForBAQSMOP.put(new Integer(Mc.MAR),
                new Double(1.25d / 16d));
        monthQuotientForBAQSMOP.put(new Integer(Mc.APR),
                new Double(1.25d / 15d));
        monthQuotientForBAQSMOP.put(new Integer(Mc.MAY),
                new Double(1.25d / 14d));
        monthQuotientForBAQSMOP.put(new Integer(Mc.JUN),
                new Double(1.25d / 13d));
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.JUL), new Double(1.0d / 12d));
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.AUG), new Double(1.0d / 11d));
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.SEP), new Double(1.5d / 20d));
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.OCT), new Double(1.5d / 19d));
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.NOV), new Double(1.5d / 18d));
        monthQuotientForBAQSMOP
                .put(new Integer(Mc.DEC), new Double(1.5d / 17d));

        // /Combined HasMAp
        /**
         * 'combinedList' map contains the combination of province and
         * respective month portion map objects (Key = ProvinceId and Value =
         * Contains all the monthly portion data)
         */
        HashMap<Object, Object> provinceMonthQuotientMatrix = new HashMap<Object, Object>();
        provinceMonthQuotientMatrix.put(Mc.NOVA_SCOTIA, monthQuotientForNovaS);// Nova
        // Scotia
        provinceMonthQuotientMatrix.put(Mc.NEW_BRUNSWICK, monthQuotientForNNM);// New
        // Brunswick
        provinceMonthQuotientMatrix.put(Mc.NEWFOUNDLAND_AND_LABRADOR,
                monthQuotientForNNM);// Newfoundland
        provinceMonthQuotientMatrix.put(Mc.BRITISH_COLUMBIA,
                monthQuotientForBAQSMOP);// BC
        provinceMonthQuotientMatrix.put(Mc.ALBERTA, monthQuotientForBAQSMOP);// AB
        provinceMonthQuotientMatrix.put(Mc.QUEBEC, monthQuotientForBAQSMOP);// QUE
        provinceMonthQuotientMatrix.put(Mc.SASKATCHEWAN,
                monthQuotientForBAQSMOP);// SASK
        provinceMonthQuotientMatrix.put(Mc.MANITOBA, monthQuotientForBAQSMOP);// MANITOBA
        provinceMonthQuotientMatrix.put(Mc.ONTARIO, monthQuotientForBAQSMOP);// ONT
        provinceMonthQuotientMatrix.put(Mc.PRINCE_EDWARD_ISLAND,
                monthQuotientForBAQSMOP);// PEI

        Integer firstPaymentMonth = 1;// default to 1
        if (firstPaymentDate != null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(firstPaymentDate);
            firstPaymentMonth = calendar.get(Calendar.MONTH) + 1;// 1=Jan
            // ...Dec
        }
        HashMap<String, Double> monthPortionObj = (HashMap<String, Double>) provinceMonthQuotientMatrix
                .get(provinceId);

        double monthPortion = monthPortionObj.get(firstPaymentMonth);
        double propertyTaxEscrowAmount = totalAnnualTaxAmount * monthPortion;

        // Applying paymentFrequencyId to calculated property Tax Escrow Amount
        switch (paymentFreqId)
        {
        case Mc.PAY_FREQ_MONTHLY: // Monthly
            propertyTaxEscrowAmount = propertyTaxEscrowAmount * 12d / 12d;
            break;
        case Mc.PAY_FREQ_SEMIMONTHLY: // semi-monthly
            propertyTaxEscrowAmount = propertyTaxEscrowAmount * 12d / 24d;
            break;
        case Mc.PAY_FREQ_BIWEEKLY: // bi-weekly
        case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY: // Accelerated bi-weekly
            propertyTaxEscrowAmount = propertyTaxEscrowAmount * 12d / 26d;
            break;
        case Mc.PAY_FREQ_WEEKLY: // weekly
        case Mc.PAY_FREQ_ACCELERATED_WEEKLY: // Accelerated weekly
            propertyTaxEscrowAmount = propertyTaxEscrowAmount * 12d / 52d;
            break;
        }
        propertyTaxEscrowAmount = Math.round(propertyTaxEscrowAmount * 100d) / 100d;
        return propertyTaxEscrowAmount;
    }

    /**
     * Description:This method returns the calculated 'Property Tax Escrow
     * Amount'. Condition : com.basis100.calc.autocalctaxescrow ='D'.
     * @version 1.0 XS_11.4 17-Jun-2008 Initial Version
     * @version 1.1 Review Comments Incorporated
     * @param paymentFrequencyId -
     *            Payment frequency of the component
     * @param totalAnnualTaxAmount -
     *            Total Annual Amount of the component
     * @return - Property Tax Escrow Amount of the component
     */
    public static double calculatePropertyTaxEscrowForDesjardins(
            int paymentFrequencyId, double totalAnnualTaxAmount)
    {
        double propertyTaxEscrowAmount = 0.00d;
        // (monthly)
        if (paymentFrequencyId == Mc.PAY_FREQ_MONTHLY)
        {
            propertyTaxEscrowAmount = totalAnnualTaxAmount / 12d;
        }
        // (semi-monthly)
        else
            if (paymentFrequencyId == Mc.PAY_FREQ_SEMIMONTHLY)
            {
                propertyTaxEscrowAmount = totalAnnualTaxAmount / 24d;
            }
            // (bi-weekly)OR 3 (Accelerated bi-weekly)
            else
                if (paymentFrequencyId == Mc.PAY_FREQ_BIWEEKLY
                        || paymentFrequencyId == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
                {
                    propertyTaxEscrowAmount = totalAnnualTaxAmount / 26d;
                }
                // (weekly) OR 5 (Accelerated weekly)
                else
                    if (paymentFrequencyId == Mc.PAY_FREQ_WEEKLY
                            || paymentFrequencyId == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
                    {
                        propertyTaxEscrowAmount = totalAnnualTaxAmount / 52d;
                    }
        propertyTaxEscrowAmount = Math.round(propertyTaxEscrowAmount * 100d) / 100d;
        return propertyTaxEscrowAmount;
    }
  /***********************************************************************
   **************MCM Impl team changes ends - XS_11.4
  /**********************************************************************/
    
    /***********************************************************************
     **************MCM Impl team changes Starts - XS_11.6
    /**********************************************************************/ 
    
    
    /**
     * Description:This method returns the calculated 'Per Diem Interest Amount'
     * Note : MCM Impl Team. Important
     * @Todo: Note to Developer, Currently this method is implemented for the
     *       Component Mortgage. This method can be resuable if the algorithm is
     *       same.
     * @version 1.0 XS_11.6 23-Jun-2008 Initial Version
     * @version 1.1 XS_11.6 01-Jul-2008 updated the logic with FSD changes
     * @param interestCompoundDesc -
     *            Interest Compound Description of the Component Payment
     *            frequency of the component
     * @param netInterestRate -
     *            Net Interest Rate of the component Mortgage
     * @param totalAmount -
     *            Total Amount
     * @return - Per Diem Interest Amount
     */
    public static double calculatePerDiemInterestAmount(
            String interestCompoundDesc, double netInterestRate,
            double totalAmount)
    {
        double perDiemInterestFactor = 0.0d;
        // Check if the interest Compound Description is Semi Annually
        if (interestCompoundDesc.equals(Mc.INTEREST_COMPOUND_SEMI_ANNUALLY))
        {
            double exponent = (2d / 365d);
            double base = 1d + (netInterestRate / 200d); //1+(netInterestRate /100*2)
            
            perDiemInterestFactor = Math.pow(base, exponent) -1;            
        }// Check if the interest Compound Description is monthly
        else
            if (interestCompoundDesc.equals(Mc.INTEREST_COMPOUND_MONTHLY))
            {
                perDiemInterestFactor = netInterestRate /(100d * 365d);
            }// Non of the conditions satisfied - default
            else
            {
                perDiemInterestFactor = netInterestRate /(100d * 365d);
            }
        double perDiemInterestAmount = perDiemInterestFactor
                * totalAmount;

        return perDiemInterestAmount;

    }    

    /***************************************************************************
     * *************MCM Impl team changes ends - XS_11.6 /
     **************************************************************************/
    /***************************************************************************
     **************MCM Impl team changes - 25-Jun-2008- Starts - XS_11.1/
     **************************************************************************/ 
    /**
     * MCM Impl Team. Important -- XS_11.1 -25-Jun-2008
     * @Todo: Note to Developer.Currently
     *        calculatePandI(..),calculateNumOfPaymentsPerYear(..),calculateNoOfPaymentsToAmortize(..),numberOfPaymentsToAmortize(..)
     *        these methods are implemented for Principle and Interest Payment
     *        (P&I Payment)for Component Mortgage, If the algorithm is same then
     *        we can resuse these methods
     */

    /**
     * <p>
     * Description : This method calculates the Principle and Interest Payment
     * by calculating the
     * numOfPaymentsPerYear,numOfPaymentsToAmortize,interestFactor
     * </p>
     * @version 1.0 25-Jun-2008 Initial Version
     * @param institutionProfileId -
     *            Institution Profile Id
     * @param amortizationMonths -
     *            Amortization Months
     * @param paymentFreqId -
     *            Payment Frequency Id
     * @param InterestCompoundDesc -
     *            Interest Compounding Description
     * @param totalAmount -
     *            Total Amount
     * @param netInterestRate -
     *            Net Interest Rate
     * @return - returns Principle And Interest Payment
     */
    public static double calculatePandI(int institutionProfileId,
            int amortizationMonths, int paymentFreqId,
            String InterestCompoundDesc, double totalAmount,
            double netInterestRate)
    {
        double numOfPaymentsPerYear = calculateNumOfPaymentsPerYear(
                institutionProfileId, paymentFreqId);
        double numOfPaymentsToAmortize = calculateNoOfPaymentsToAmortize(
                paymentFreqId, amortizationMonths, institutionProfileId,
                numOfPaymentsPerYear);
        double interestFactor = DealCalcUtil.interestFactor(netInterestRate,
                numOfPaymentsPerYear, InterestCompoundDesc);
        double pandI = calculatePandI(totalAmount, interestFactor,
                numOfPaymentsToAmortize);
        return pandI;
    }

    /**
     * Description :This method calculates the PandI based on the below formula
     * ComponentMortgage.pAndIPaymentAmount
     * =ComponentMortgage.totalMortgageAmount * (�Interest Factor�/ (1 - (1 /
     * ((�Interest Factor� + 1) ^ �Number of Payments to Amortize�))))
     * @version 1.0 25-Jun-2008 Initial Version
     * @param totalAmount -
     *            Total Amount of the given Component
     * @param interestFactor -
     *            Interest Factor of the given Component
     * @param numberOfPaymentsAmortize -
     *            Number Of Payments To Amortize
     * @return
     */
    public static double calculatePandI(double totalAmount,
            double interestFactor, double numberOfPaymentsAmortize)
    {
        double pandI = 0.0d;

        double denominator = 1 - 1 / Math.pow((interestFactor + 1),
                numberOfPaymentsAmortize);

        if (denominator > 0)
        {
            pandI = totalAmount * interestFactor / denominator;
        }
        return pandI;

    }

    /**
     * Description : This method calculates the Number of payments per year for
     * the given payment frequency
     * @version 1.0 25-Jun-2008 Initial Version
     * @param institutionProfileId -
     * @param paymentFreqId -
     *            Payment Frequency Id
     * @return - returns Number of payments per year
     */
    public static double calculateNumOfPaymentsPerYear(
            int institutionProfileId, int paymentFreqId)
    {
        double numOfPaymentsPerYear = 0.0d;

        boolean isDJDaysYear = (PropertiesCache.getInstance().getProperty(
                institutionProfileId,
                "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N"))
                .equals("Y");
        if (isDJDaysYear)
        {
            numOfPaymentsPerYear = pCalcNumOfPaymentsPerYear(paymentFreqId,
                    365.25d);
        }
        else
        {
            numOfPaymentsPerYear = pCalcNumOfPaymentsPerYear(paymentFreqId,
                    365d);
        }
        return numOfPaymentsPerYear;
    }

    /**
     * Desctription : This method returns the calculated No of Payments to
     * amortize for the given payment frequency
     * @version 1.0 25-Jun-2008 Initial Version
     * @version 1.1 01-Jul-2008 Removed the Else part and added as a default.
     * @param paymentFreqId -
     *            Payment Frequency Id
     * @param amortizationTerm -
     *            Amortization Term for the given component
     * @param institutionProfileId -
     *            Institution Profile Id
     * @param numOfPaymentsPerYear -
     *            Number of Payments Per year
     * @return - returns Number of payments to amortize
     */
    public static double calculateNoOfPaymentsToAmortize(int paymentFreqId,
            int amortizationTerm, int institutionProfileId,
            double numOfPaymentsPerYear)
    {

        boolean isUseDJNumberOfPayments = (PropertiesCache.getInstance()
                .getProperty(institutionProfileId,
                        "com.filogix.calc.DJNumberOfPayments", "N"))
                .equalsIgnoreCase("Y");
        double numOfPaymentsToAmortize = 0.0d;
        //If the isUseDJNumberOfPayments = 'False'
        numOfPaymentsToAmortize = numberOfPaymentsToAmortize(
                amortizationTerm, numOfPaymentsPerYear);
        //If the isUseDJNumberOfPayments = 'True'
        if (isUseDJNumberOfPayments)
        {
            if (paymentFreqId == Mc.PAY_FREQ_BIWEEKLY)
            {
                numOfPaymentsToAmortize = Math
                        .ceil((amortizationTerm / 12d) * 26d);
            }
            else
                if (paymentFreqId == Mc.PAY_FREQ_WEEKLY)
                {
                    numOfPaymentsToAmortize = Math
                            .ceil((amortizationTerm / 12d) * 52d);
                }
        }
        return numOfPaymentsToAmortize;
    }

    /**
     * Description : This method follows the below Algorithm Algorithm :
     * (amortizationTerm / 12) * PaymentsPerYear
     * @version 1.0 25-Jun-2008 Initial Version
     * @param amortizationTerm -
     *            Amortization Term
     * @param numberOfPaymentsPerYear -
     *            Number Of Payments Per Year
     * @return - returns the Number of payments to Amortize
     */
    public static double numberOfPaymentsToAmortize(int amortizationTerm,
            double numberOfPaymentsPerYear)
    {
        return (amortizationTerm / 12d) * numberOfPaymentsPerYear;
    }

    /***************************************************************************
     * *************MCM Impl team changes - 25-Jun-2008 -ends - XS_11.1 /
     **************************************************************************/
    /***************************************************************************
     * *************MCM Impl team changes - 25-Jun-2008 -starts - XS_11.5 /
     **************************************************************************/
    /**
     * <p>
     * Description :This method returns the number of days from Start Date to
     * End Date , but not including the End Date
     * </p>
     * @version 1.0 25-Jun-2008 XS_11.5 Initial Version.
     * @param startDate -
     *            Start Date
     * @param endDate -
     *            End Date
     * @return - returns the difference between start date to end date (eg:
     *         Start Date =Nov 30, 2007, End Date =Dec 5,2007 then the
     *         difference =5)
     */
    public static long getDaysBetween(Calendar startDate, Calendar endDate)
    {
        Calendar date = (Calendar) startDate.clone();
        long daysBetween = 0;
        while (date.before(endDate))
        {
            date.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }    
    /**
     * <p>
     * Description :This method returns the number of days for the given payment
     * frequency
     * </p>
     * @version 1.0 25-Jun-2008 XS_11.5 Initial Version.
     * @param paymentFrequency
     *            -Payment Frequency Id
     * @return - the number of period in days
     */
    public static int getPeriodInDays(int paymentFrequency)
    {
        int period = 0;
        if (paymentFrequency == Mc.PAY_FREQ_SEMIMONTHLY)
            period = 15;
        else
            if (paymentFrequency == Mc.PAY_FREQ_BIWEEKLY
                    || paymentFrequency == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
                period = 14;
            else
                if (paymentFrequency == Mc.PAY_FREQ_WEEKLY
                        || paymentFrequency == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
                    period = 7;
                else
                    if (paymentFrequency == Mc.PAY_FREQ_MONTHLY)
                        period = 30; // 30 days
        return period;
    }
    /***************************************************************************
     **************MCM Impl team changes - 25-Jun-2008 -ends - XS_11.5 /
     **************************************************************************/
  

    /**
   * check if mi primium is Standard or Extended
   * 
   * 1. check mortgageInsurer.supportForXtdPayload
   *   A = always, N = Never, 
   * if Y = depends 
   * 2. check MORTGAGEINSURANCETYPE.XTDPAYLOAD (Y/N)
   * 
   * @param deal
   * @return true = MI Premium is Extended <br> false = MI Premium is Standard 
   */
    public static boolean checkIfMIPremiumIsExtended(Deal deal) 
    {
        String supportXtd = PicklistData.getMatchingColumnValue(deal.getInstitutionProfileId(),
            "MORTGAGEINSURER", deal.getMortgageInsurerId(), "SUPPORTFORXTDPAYLOAD");

        String xtdPayload = PicklistData.getMatchingColumnValue(deal.getInstitutionProfileId(),
            "MORTGAGEINSURANCETYPE", deal.getMITypeId(), "XTDPAYLOAD");

        /* Serghei:: FXP20992
      if (Mc.MORTGAGEINSURER_XTD_PAYLOAD_ALWAYS.equalsIgnoreCase(supportXtd)) {
          _log.debug("extended: MORTGAGEINSURER.SUPPORTFORXTDPAYLOAD = " + supportXtd);
          return true; //extended

      } else if (Mc.MORTGAGEINSURER_XTD_PAYLOAD_YES.equalsIgnoreCase(supportXtd)) {


          String xtdPayload = PicklistData.getMatchingColumnValue(
                  "MORTGAGEINSURANCETYPE", deal.getMITypeId(), "XTDPAYLOAD");
          _log.debug("MORTGAGEINSURANCETYPE.XTDPAYLOAD = " + xtdPayload);

          return "Y".equalsIgnoreCase(xtdPayload); //depends on xtdPayload
      }
         */
        if(Mc.MORTGAGEINSURANCETYPE_XTDPAYLOAD_Y.equalsIgnoreCase(xtdPayload) && 
                (Mc.MORTGAGEINSURER_XTD_PAYLOAD_ALWAYS.equalsIgnoreCase(supportXtd) ||
                        Mc.MORTGAGEINSURER_XTD_PAYLOAD_YES.equalsIgnoreCase(supportXtd))) {

            return true; //extended
        }


        return false; // default : never extended
    }



    /**
     * calculates PandI (generic method, checks all necessary conditions)
     * 
     * this is a generic PandI method.
     * note that DealCalcUtil.calculatePandI (developed by Sapient) is generic method for the following formula.
     *  [Total Loan Amount] * ([Interest Factor] / (1 - (1 / (([Interest Factor]+ 1)^[Number of Payments to Amortize])))))
     * this method takes care of other conditions that are controled by some sys properties or values in some tables.
     * 
     * @param institutionProfileId
     * @param totalLoanAmount
     * @param paymentFrequencyId - only support between 0 and 5
     * @param netInterestRate
     * @param amortizationMonths
     * @param repaymentTypeId
     * @param interestCompoundingId
     * @param rateCode
     * @return PandIPayment amount
     */
    public static double calcPandIFully(int institutionProfileId, double totalLoanAmount, int paymentFrequencyId,
            double netInterestRate, int amortizationMonths, int repaymentTypeId, int interestCompoundingId, String rateCode) {

        double pandiAmount = 0.0d;

        if("UNCNF".equalsIgnoreCase(rateCode)) { 
            logger.warn("calcPandIFully: rateCode is UNCNF. returning 0");
            return pandiAmount; 
        } 

        if(paymentFrequencyId < Mc.PAY_FREQ_MONTHLY || Mc.PAY_FREQ_ACCELERATED_WEEKLY < paymentFrequencyId){
            IllegalArgumentException ex = new IllegalArgumentException(
                "paymentFrequencyId(" + paymentFrequencyId + ") is out of the target range");
            logger.error(ex.getMessage(), ex);
            // throw ex; //let's not throw exception for now. instead retuning 0.0d
            return pandiAmount;
        }

        String interestCompoundDesc = PicklistData.getMatchingColumnValue(
            -1, "interestcompound", interestCompoundingId, "interestcompounddescription");
        logger.debug("interestCompoundDesc=" + interestCompoundDesc);

        if (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
                || repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED) {

            //formula: [Total Loan Amount] * [Interest Factor]
            double numOfPaymentsPerYear = DealCalcUtil.calculateNumOfPaymentsPerYear(
                institutionProfileId, paymentFrequencyId);
            double interestFactor = DealCalcUtil.interestFactor(
                netInterestRate, numOfPaymentsPerYear, interestCompoundDesc);
            pandiAmount = totalLoanAmount * interestFactor;

        }else{

            if (paymentFrequencyId == Mc.PAY_FREQ_MONTHLY) {
                //formula: [Total Loan Amount] * ([Interest Factor] / 
                //            (1 - (1 / (([Interest Factor]+ 1)^[Number of Payments to Amortize])))))
                pandiAmount = DealCalcUtil.calculatePandI(institutionProfileId, amortizationMonths,
                    Mc.PAY_FREQ_MONTHLY, interestCompoundDesc, totalLoanAmount, netInterestRate);

            } else if (paymentFrequencyId == Mc.PAY_FREQ_SEMIMONTHLY) {
                //formula: [Total Loan Amount] * ([Interest Factor] / 
                //            (1 - (1 / (([Interest Factor]+ 1)^[Number of Payments to Amortize])))))
                pandiAmount = DealCalcUtil.calculatePandI(institutionProfileId, amortizationMonths,
                    paymentFrequencyId, interestCompoundDesc, totalLoanAmount, netInterestRate);

            } else if (paymentFrequencyId == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY) {
                //formula: Monthly P&I Payment / 2
                double monthlyPandI = DealCalcUtil.calculatePandI(institutionProfileId, amortizationMonths,
                    Mc.PAY_FREQ_MONTHLY, interestCompoundDesc, totalLoanAmount, netInterestRate);
                pandiAmount = monthlyPandI / 2d;

            } else if (paymentFrequencyId == Mc.PAY_FREQ_ACCELERATED_WEEKLY) {
                //formula: Monthly P&I Payment / 4
                double monthlyPandI = DealCalcUtil.calculatePandI(institutionProfileId, amortizationMonths,
                    Mc.PAY_FREQ_MONTHLY, interestCompoundDesc, totalLoanAmount, netInterestRate);
                pandiAmount = monthlyPandI / 4d;

            } else if (paymentFrequencyId == Mc.PAY_FREQ_BIWEEKLY
                    || paymentFrequencyId == Mc.PAY_FREQ_WEEKLY) {

                String useFormula = (PropertiesCache.getInstance().getProperty( institutionProfileId,
                    "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "N"));
                if ("Y".equalsIgnoreCase(useFormula)) {
                    //formula: [Total Loan Amount] * ([Interest Factor] / 
                    //            (1 - (1 / (([Interest Factor]+ 1)^[Number of Payments to Amortize])))))
                    pandiAmount = DealCalcUtil.calculatePandI(institutionProfileId, amortizationMonths,
                        paymentFrequencyId, interestCompoundDesc,totalLoanAmount, netInterestRate);
                } else {
                    double monthlyPandI = DealCalcUtil.calculatePandI(institutionProfileId, amortizationMonths,
                        Mc.PAY_FREQ_MONTHLY, interestCompoundDesc, totalLoanAmount, netInterestRate);
                    if (paymentFrequencyId == Mc.PAY_FREQ_BIWEEKLY) {
                        //formula: (Monthly P&I Payment * 12) /26
                        pandiAmount = (monthlyPandI * 12d) / 26d;
                    } else if (paymentFrequencyId == Mc.PAY_FREQ_WEEKLY) {
                        //formula: (Monthly P&I Payment * 12) /52
                        pandiAmount = (monthlyPandI * 12d) / 52d;
                    }
                }
            }
        }

        logger.debug("calcPandIFully output:pandiAmount=" + pandiAmount);
        //this value is not rounded.
        return pandiAmount;
    }

    /**
     * short cut method for PandI 3year calc 
     * 
     * @param deal
     * @return PandIPaymentAmount
     * @throws DealCalcException
     */
    public static double calcPandIFully3Year(Deal deal, CalcEntityCache entityCache) throws DealCalcException {

        int paymentFrequencyId = deal.getPaymentFrequencyId();
        return calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
    }

    /**
     * short cut method for PandI 3year calc 
     * calculates PandI based on the 3Year rate which is defined by
     * "com.basis100.calc.gdstds3yratecode" Basically the logic to calculate
     * PandI is exactly the same as regler PandI so this method eventually
     * executes calcPandIFully. this method changes some parameters for the
     * generic method to calculate 3 years rate
     * 
     * @param deal
     * @param paymentFrequencyId
     * @return PandIAmount based on 3Years Rate
     * @throws DealCalcException
     */
    public static double calcPandIFully3Year(Deal deal, int paymentFrequencyId, CalcEntityCache entityCache) throws DealCalcException {

        if (deal == null) throw new IllegalArgumentException("deal is null");
        if (entityCache == null) throw new IllegalArgumentException("entityCache is null");

        SessionResourceKit srk = deal.getSessionResourceKit();

        int institutionProfileId = srk.getExpressState().getDealInstitutionId();
        PropertiesCache pc = PropertiesCache.getInstance();

        double totalLoanAmount = deal.getTotalLoanAmount();
        int repaymentTypeId = deal.getRepaymentTypeId();
        int amortizationMonths = deal.getAmortizationTerm();

        MtgProd selectedMtgProd;
        try {
            selectedMtgProd = (MtgProd) entityCache.find(srk,
                new MtgProdPK(deal.getMtgProdId()));
        } catch (Exception e) {
            throw new DealCalcException("failed to find MtgProd", e);
        }

        String forcePandIFor3Year = (pc.getProperty(institutionProfileId, 
            "com.basis100.calc.forcepandifor3year", "N"));
        logger.debug("com.basis100.calc.forcepandifor3year=" + forcePandIFor3Year);
        
        if ("Y".equalsIgnoreCase(forcePandIFor3Year)
                && (deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC 
                        || selectedMtgProd.getUnderwriteAsTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC)) {
            repaymentTypeId = 0;
            amortizationMonths = 300;
        }
        
        //***** change for Express 5.0 *****//
        final double ltvThreshold = Double.parseDouble(pc.getProperty(institutionProfileId, Xc.ING_LTV_THRESHOLD, "80"));
        boolean allowOverrideQRP = pc.getProperty(institutionProfileId, "com.filogix.qualoverridechk", "Y").equalsIgnoreCase("Y");
        boolean allowOverrideRate = pc.getProperty(institutionProfileId, "com.filogix.qualrateoverride", "Y").equalsIgnoreCase("Y");
        final int miIndId = deal.getMIIndicatorId();
       
        String theRateCodeToUse = "";
        final double ltv = deal.getCombinedLTV();
        PricingRateInventory pri3y = null;
        String rateCode = "";
        double netInterestRate = deal.getGDSTDS3YearRate();
        if(!allowOverrideQRP)
        { //sysproperty derrived
            if(ltv > ltvThreshold 
                    && (miIndId == Xc.MII_REQUIRED_STD_GUIDELINES 
                            || miIndId == Xc.MII_UW_REQUIRED 
                            || miIndId == Xc.MII_MI_PRE_QUALIFICATION))
                theRateCodeToUse = "com.basis100.calc.gdstds3yratecode";
            else
                theRateCodeToUse = "com.basis100.calc.gdstds.cmhclowratioratecode";
            rateCode =  pc.getProperty(institutionProfileId, theRateCodeToUse, "00");
            try 
			{
                pri3y = new PricingRateInventory(srk).findMostRecentByRateCode(rateCode);
                netInterestRate = pri3y.getInternalRatePercentage();
            } 
			catch (Exception e) 
			{
                throw new DealCalcException("failed to find PricingRateInventory for 3years rate", e);
            }
            logger.debug("com.basis100.calc.gdstds3yratecode=" + rateCode);
        } 
        else 
        { 
        	//product given by the user
        	if ("Y".equalsIgnoreCase(deal.getQualifyingOverrideFlag()))
        	{
        		try
        		{
    				MtgProd mtgProd = new MtgProd(srk, null);
    				mtgProd = mtgProd.findByPrimaryKey(new MtgProdPK(deal.getOverrideQualProd()));
    				if (mtgProd!=null) {
    					PricingProfile  pp = mtgProd.getPricingProfile();
    					theRateCodeToUse = pp.getRateCode();
    				} else {
    					theRateCodeToUse =	pc.getProperty(institutionProfileId, theRateCodeToUse, "00");
    				}
    				pri3y = new PricingRateInventory(srk).findMostRecentByRateCode(theRateCodeToUse);
        		}
        		catch (Exception e) 
    			{
    				throw new DealCalcException("failed to find PricingRateInventory for 3years rate", e);
    			}
    			logger.debug("com.basis100.calc.gdstds3yratecode=" + theRateCodeToUse);
        	}
        	else
        	{
                if(ltv > ltvThreshold 
                        && (miIndId == Xc.MII_REQUIRED_STD_GUIDELINES 
                                || miIndId == Xc.MII_UW_REQUIRED 
                                || miIndId == Xc.MII_MI_PRE_QUALIFICATION))
                    theRateCodeToUse = "com.basis100.calc.gdstds3yratecode";
                else
                    theRateCodeToUse = "com.basis100.calc.gdstds.cmhclowratioratecode";
                rateCode =  pc.getProperty(institutionProfileId, theRateCodeToUse, "00");
                try 
    			{
                    pri3y = new PricingRateInventory(srk).findMostRecentByRateCode(rateCode);
    			}
                catch (Exception e) 
    			{
    				throw new DealCalcException("failed to find PricingRateInventory for 3years rate", e);
    			}
        	}

        	//rate given by the user
        	if ("Y".equalsIgnoreCase(deal.getQualifyingRateOverrideFlag()))
    			netInterestRate = deal.getGDSTDS3YearRate();
    		else 
    			netInterestRate = pri3y.getInternalRatePercentage();
        		
        }
/*			try 
			{
				MtgProd mtgProd = new MtgProd(srk, null);
				mtgProd = mtgProd.findByPrimaryKey(new MtgProdPK(deal.getOverrideQualProd()));
				if (mtgProd!=null) {
					PricingProfile  pp = mtgProd.getPricingProfile();
					theRateCodeToUse = pp.getRateCode();
				} else {
					theRateCodeToUse =	pc.getProperty(institutionProfileId, theRateCodeToUse, "00");
				}
				pri3y = new PricingRateInventory(srk).findMostRecentByRateCode(theRateCodeToUse);
			} 
			catch (Exception e) 
			{
				throw new DealCalcException("failed to find PricingRateInventory for 3years rate", e);
			}
			logger.debug("com.basis100.calc.gdstds3yratecode=" + theRateCodeToUse);
        }
		//use user-supplied rate when applicable.
		if (!allowOverrideQRP || "Y".equalsIgnoreCase(deal.getQualifyingRateOverrideFlag())) 
			netInterestRate = deal.getGDSTDS3YearRate();
		else 
			netInterestRate = pri3y.getInternalRatePercentage();*/
        //***** change for Express 5.0 *****//
        /***** remove in Express 5.0 start *****/
        //try {
        //    InstitutionProfile institution = (InstitutionProfile)entityCache.find(
        //        srk, new InstitutionProfilePK (deal.getInstitutionProfileId()) );
            //appliy discount rate
        //    if (institution.getThreeYearRateCompareDiscount())
        //        netInterestRate -= deal.getDiscount();
        //} catch (Exception e) {
        //    throw new DealCalcException("failed to find InstitutionProfile", e);
        //}
        /***** remove in Express 5.0 start *****/
        
        int interestCompoundingId;
        try {
            Collection<MtgProd> mtgProds3y = 
                (new MtgProd(srk, null)).findByPricingProfile(pri3y.getPricingProfileID());
            MtgProd mtgProd3y = (MtgProd)mtgProds3y.toArray()[0];
            interestCompoundingId = mtgProd3y.getInterestCompoundingId();
        } catch (Exception e) {
            throw new DealCalcException("failed to find MtgProd for 3years rate", e);
        }

        //InterestCompoundingId and rateCode has to be 3year's one
        return calcPandIFully(institutionProfileId, totalLoanAmount,
            paymentFrequencyId, netInterestRate, amortizationMonths,
            repaymentTypeId, interestCompoundingId, rateCode);
    }
}
