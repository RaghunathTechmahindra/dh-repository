package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;

/**
 * <p>
 * Title: TeaserPandIComponent
 * </p>
 * <p>
 * Description:Calculates the net interest rate for Components
 * </p>
 * Calculation Number- Calc-87.C
 * @version 1.0 XS_11.8 10-July-2008 Initial Version
 */
public class TeaserNetInterestRateComponent extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.8 10-July-2008 Initial Version
     */
    public TeaserNetInterestRateComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 87.C";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.8 10-July-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();
            if (classId == ClassId.COMPONENT)
            {
                Component component = (Component) input;
                // find component for the given componentId and
                addTarget(component);
            }else if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                // find component for the given componentId and
                // copyId
                Component component = (Component) entityCache.find(srk,
                        new ComponentPK(componentMtg.getComponentId(),
                                componentMtg.getCopyId()));
                addTarget(component);
            }
            else
                if (classId == ClassId.COMPONENTLOC)
                {
                    ComponentLOC componentLoc = (ComponentLOC) input;
                    // find component for the given componentId and
                    // copyId
                    Component component = (Component) entityCache.find(srk,
                            new ComponentPK(componentLoc.getComponentId(),
                                    componentLoc.getCopyId()));
                    addTarget(component);
                }
                else
                    if (classId == ClassId.COMPONENTLOAN)
                    {
                        ComponentLoan componentLoan = (ComponentLoan) input;
                        // find component for the given componentId and
                        // copyId
                        Component component = (Component) entityCache.find(srk,
                                new ComponentPK(componentLoan.getComponentId(),
                                        componentLoan.getCopyId()));
                        addTarget(component);
                    }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.8 10-July-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());

        try
        {
        	int componentTypeId;
        	double teaserNetInterestRate=0;
            Component targetComponent = (Component) target;
            // Retrive the Deal for the given component
            PricingRateInventory priInventory=new PricingRateInventory(srk,targetComponent.getPricingRateInventoryId());
            componentTypeId = targetComponent.getComponentTypeId();
            //targetComponent.getteaser
            int teaserTerm=priInventory.getTeaserTerm();
            
            if(teaserTerm > 0){
	            if (componentTypeId == Mc.COMPONENT_TYPE_MORTGAGE)
	            {
	            	ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
                    .find(srk, new ComponentMortgagePK(targetComponent
                            .getComponentId(), targetComponent.getCopyId()));
	            	teaserNetInterestRate=componentMtg.getNetInterestRate()-priInventory.getTeaserDiscount();
	            }
	            else
	            if (componentTypeId == Mc.COMPONENT_TYPE_LOC)
	            {
	            	ComponentLOC componentLoc = (ComponentLOC) this.entityCache
                    .find(srk, new ComponentLOCPK(targetComponent
                            .getComponentId(), targetComponent
                            .getCopyId()));
	            	teaserNetInterestRate=componentLoc.getNetInterestRate()-priInventory.getTeaserDiscount();
	            }
	            else
	            if (componentTypeId == Mc.COMPONENT_TYPE_LOAN)
	            {
	            	 ComponentLoan componentLoan = (ComponentLoan) this.entityCache
                     .find(srk, new ComponentLoanPK(targetComponent
                             .getComponentId(), targetComponent
                             .getCopyId()));
	            	 teaserNetInterestRate=componentLoan.getNetInterestRate()-priInventory.getTeaserDiscount();
	            }
            }else{
            	teaserNetInterestRate=0;
            }
            targetComponent.setTeaserNetInterestRate(validate(teaserNetInterestRate));
            targetComponent.ejbStore();
            return;

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.8 10-July-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "pricingRateInventoryId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "netInterestRate"));
        
        addTargetParam(new CalcParam(ClassId.COMPONENT,
                "teaserNetInterestRate", T13D2));
    }
}
