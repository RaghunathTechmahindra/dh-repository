/**
 * Title:        MIPremiumPST (Calc-101)
 * Description:  Calculation for PST on Mortgage Insurance Premium
 * Copyright:    Copyright (c) 2001
 * Company:      Basis100
 * Date :        23 Nov 2001
 * @author Billy Lam
 * @version
 *
 * 15.Aug.2011 DVG #DG1062 LEN510919-Street cap-MI Premium PST discrepancy-truncate
 */

package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.* ;
import com.basis100.entity.*;
import MosSystem.Mc;

public class MIPremiumPST extends DealCalc
{

  public MIPremiumPST()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 101";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        if ( input instanceof Deal )
        {
          addTarget( input );
        }
         else if ( input instanceof Property )
         {
             Property p = (Property)input;
             Deal deal = (Deal) entityCache.find (srk, 
                              new DealPK( p.getDealId(), p.getCopyId ())) ;
             addTarget( deal );
         }
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Deal deal = (Deal)target;

    try
    {
      // Get the primary Property
      Property thePriProp = new Property(srk, null);
      try{
        thePriProp = thePriProp.findByPrimaryProperty(deal.getDealId(), 
                           deal.getCopyId(), deal.getInstitutionProfileId());
      }
      catch(FinderException fe)
      {
        // Not Primary Property set
        // Set to default value and return
        deal.setMIPremiumPST(DEFAULT_FAILED_DOUBLE);
        deal.ejbStore();
        return;
      }

      // Get the province tax rate
      Province theProv = new Province(srk, thePriProp.getProvinceId());

      // Do this later ...
      //deal.setMIPremiumPST(deal.getMIPremiumAmount() *  theProv.getProvinceTaxRate() / 100 );

     // Don't round it if InsurerId = CMHC -- New requirement from Product 03Jan2002
     //   -- Modified by BILLY
     int mortgageInsurerId = deal.getMortgageInsurerId() ;
     //#DG1062 round a bit b4 trunc
     double miPst = deal.getMIPremiumAmount() * theProv.getProvinceTaxRate();
     if(mortgageInsurerId ==  Mc.MI_INSURER_CMHC)
     {
       // deal.setMIPremiumPST( validate(
               // Math.floor((deal.getMIPremiumAmount() * theProv.getProvinceTaxRate() / 100) * 100.00)  / 100.00  ) );
          miPst = Math.round(miPst*10000.) /10000.;	//	round to 4 decs b4 trunc
   	  miPst = Math.floor(miPst) / 100.;
     }
     else
     {
        //deal.setMIPremiumPST( validate(
         //       Math.round((deal.getMIPremiumAmount() * theProv.getProvinceTaxRate() / 100) * 100.00)  / 100.00  ) );
          miPst = Math.round(miPst) / 100.;
     }
     deal.setMIPremiumPST( validate(miPst) );

    trace( "Setting the MIpremiumPST amount from MIPremiumPST calculator :  Deal ID is : " + deal.getDealId() + " and copyId is :" + deal.getCopyId() + " and MIPremiumPST Amount is : " + deal.getMIPremiumPST());

      deal.ejbStore();
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

     deal.setMIPremiumPST(DEFAULT_FAILED_DOUBLE);
     deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTY, "provinceId"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "primaryPropertyFlag"));

    addInputParam(new CalcParam(ClassId.DEAL , "MIPremiumAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "mortgageInsurerId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "MIPremiumPST", T13D2));

  }
}