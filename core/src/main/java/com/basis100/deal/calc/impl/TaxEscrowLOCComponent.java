/*
 * @(#)TaxEscrowLOCComponent.java June 20, 2008 <story id : XS 11.12> Copyright
 * (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: TaxEscrowLOCComponent
 * </p>
 * <p>
 * Description: Calculates Escrow Payment Amount based on the payment frequency
 * of the LOC Component.
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.12 20-Jun-2008 Initial version
 */
public class TaxEscrowLOCComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     */
    public TaxEscrowLOCComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "CALC 107.CLOC";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            Deal deal = null;
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOC)
            {
                ComponentLOC componentLOC = (ComponentLOC) input;
                addTarget(componentLOC);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    deal = (Deal) input;

                }
                else
                    if (classId == ClassId.PROPERTY)
                    {
                        Property property = (Property) input;
                        deal = (Deal) entityCache.find(srk, new DealPK(property
                                .getDealId(), property.getCopyId()));

                    }
            if (deal != null)
            {
                Collection listOfcomponents = entityCache.getComponents(deal,
                        srk);
                Iterator componentsItr = listOfcomponents.iterator();
                while (componentsItr.hasNext())
                {
                    Component component = (Component) componentsItr.next();
                    // Checks the component type is LOC COMPONENT
                    if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOC)
                    { // Retrives the componentLOC Object for the given
                        // componentId and copyId
                        ComponentLOC componentLOC = (ComponentLOC) entityCache
                                .find(srk, new ComponentLOCPK(component
                                        .getComponentId(), component
                                        .getCopyId()));
                        addTarget(componentLOC);
                    }
                }
            }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        try
        {
            ComponentLOC targetComponentLOC = (ComponentLOC) target;

            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLOC.getComponentId(),
                            targetComponentLOC.getCopyId()));
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));
            // Retrive the Primary property object for the given dealId and
            // copyId
            Property primaryProp = new Property(srk, null)
                    .findByPrimaryProperty(deal.getDealId(), deal.getCopyId(),
                            deal.getInstitutionProfileId());
            // Retrive the autocalc TaxEscrow value from the Sysproperty
            String autoCalTaxEscrowSysProp = PropertiesCache.getInstance()
                    .getProperty(deal.getInstitutionProfileId(),
                            "com.basis100.calc.autocalctaxescrow");

            if (deal.getTaxPayorId() == Mc.TAX_PAYOR_BORROWER
                    || ("N").equalsIgnoreCase(targetComponentLOC
                            .getPropertyTaxAllocateFlag()))
            {
                targetComponentLOC.setPropertyTaxEscrowAmount(Mc.DEFAULT_VALUE);
                targetComponentLOC.ejbStore();
                return;
            }
            if (deal.getTaxPayorId() == Mc.TAX_PAYOR_LENDER
                    && ("Y".equalsIgnoreCase(targetComponentLOC
                            .getPropertyTaxAllocateFlag())))
            {
                // com.basis100.calc.autocalctaxescrow = T (TD method)
                if (("T").equalsIgnoreCase(autoCalTaxEscrowSysProp))
                {
                    double propertyTaxEscrowAmount = DealCalcUtil
                            .calculatePropertyTaxEscrowForTD(targetComponentLOC
                                    .getInterestAdjustmentDate(), primaryProp
                                    .getTotalAnnualTaxAmount());
                    targetComponentLOC
                            .setPropertyTaxEscrowAmount(validate(propertyTaxEscrowAmount));
                    targetComponentLOC.ejbStore();
                }
                // com.basis100.calc.autocalctaxescrow = �X� (Xceed method)
                else
                    if ("X".equalsIgnoreCase(autoCalTaxEscrowSysProp))
                    {
                        double propertyTaxEscrowAmount = DealCalcUtil
                                .calculatePropertyTaxEscrowForXceed(
                                        targetComponentLOC
                                                .getFirstPaymentDate(),
                                        primaryProp.getTotalAnnualTaxAmount(),
                                        primaryProp.getProvinceId(),
                                        targetComponentLOC
                                                .getPaymentFrequencyId());
                        targetComponentLOC
                                .setPropertyTaxEscrowAmount(validate(propertyTaxEscrowAmount));
                        targetComponentLOC.ejbStore();
                    }
                    // com.basis100.calc.autocalctaxescrow = �D� (Desjardins
                    // method)
                    else
                        if ("D".equalsIgnoreCase(autoCalTaxEscrowSysProp))
                        {
                            double propertyTaxEscrowAmount = DealCalcUtil
                                    .calculatePropertyTaxEscrowForDesjardins(
                                            targetComponentLOC
                                                    .getPaymentFrequencyId(),
                                            primaryProp
                                                    .getTotalAnnualTaxAmount());
                            targetComponentLOC
                                    .setPropertyTaxEscrowAmount(validate(propertyTaxEscrowAmount));
                            targetComponentLOC.ejbStore();
                        }
            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Initialize the target parameters
     * </p>
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @throws Exception -
     *             exception object
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();
        addInputParam(new CalcParam(ClassId.DEAL, "taxPayorId"));
        addInputParam(new CalcParam(ClassId.PROPERTY, "totalAnnualTaxAmount"));
        addInputParam(new CalcParam(ClassId.PROPERTY, "provinceId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC,
                "propertyTaxAllocateFlag"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "firstPaymentDate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC,
                "interestAdjustmentDate"));
        addTargetParam(new CalcParam(ClassId.COMPONENTLOC,
                "propertyTaxEscrowAmount", T13D2));

    }
}
