package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name: Total Purchase Price
//Code: CALC 89
//07/05/2000

public class TotalPropertyExpenses extends DealCalc {

  public TotalPropertyExpenses() throws Exception
  {
      super();
      initParams();
      this.calcNumber = "Calc 89";
  }

   public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
      try
      {
          if ( input instanceof  PropertyExpense )
          {
              PropertyExpense propertyExpense = ( PropertyExpense ) input;
              Property property = (Property) entityCache.find (srk,
                      new PropertyPK( propertyExpense.getPropertyId (), 
                           propertyExpense.getCopyId ()) ) ;
              addTarget ( property );
          }
      }
      catch(Exception e)
      {
          String msg = "Fail to get Targets" + this.getClass().getName();
          msg += ":[Input]:" + input.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@input: " + this.getDoCalcErrorDetail ( input );
          logger.error (msg );
          throw new TargetNotFoundException (msg);
      }

  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Property property = (Property) target;

     try
     {
       Collection propertyExpenses = this.entityCache.getPropertyExpenses (property);

       Iterator itPE = propertyExpenses.iterator ();

       double totalPropertyExpense = 0.0;
       PropertyExpense propertyExpense = null ;

       while ( itPE.hasNext () )
        {
          propertyExpense = ( PropertyExpense ) itPE.next ();
          totalPropertyExpense += propertyExpense.getPropertyExpenseAmount () *
              DealCalcUtil.annualize (propertyExpense.getPropertyExpensePeriodId () , "PropertyExpensePeriod");
        }
       totalPropertyExpense = Math.round ( totalPropertyExpense * 100 ) / 100.00 ; 
       property.setTotalPropertyExpenses (  validate(totalPropertyExpense) );
       property.ejbStore ();
       return;

     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    property.setTotalPropertyExpenses (DEFAULT_FAILED_DOUBLE);
    property.ejbStore ();

  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE ,"propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));

    addTargetParam(new CalcParam(ClassId.PROPERTY, "totalPropertyExpenses", T15D2));
  }

}
