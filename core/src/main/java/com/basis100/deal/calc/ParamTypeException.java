package com.basis100.deal.calc;

public class ParamTypeException extends DealCalcException
{
   /**
  * Constructs a <code>ParamTypeException</code> with no specified detail message.
  */

  public ParamTypeException()
  {
	  super();
  }

  /**
  * Constructs a <code>ParamTypeException</code> with the specified message.
  * @param   msg  the related message.
  */
  public ParamTypeException(String msg)
  {
    super(msg);
  }

} 