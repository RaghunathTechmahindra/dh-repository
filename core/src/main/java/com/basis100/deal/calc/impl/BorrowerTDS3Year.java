package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.resources.PropertiesCache;

import MosSystem.Mc;

//Name: Borrower TDS 3 Year
//Code: CALC 51
//07/12/2000

public class BorrowerTDS3Year extends DealCalc
{

  public BorrowerTDS3Year() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 51";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower borrower = (Borrower)entityCache.find(srk, 
                                    new BorrowerPK(i.getBorrowerId(), 
                                                   i.getCopyId()));
        addTarget(borrower);
        return;
      }

      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower borrower = (Borrower)entityCache.find(srk, 
                                   new BorrowerPK(l.getBorrowerId(), 
                                                  l.getCopyId()));
        addTarget(borrower);
        return;
      }

      Deal d = null;
      if(input instanceof PropertyExpense)
      {
        PropertyExpense propertyExpense = (PropertyExpense)input;
        Property property = (Property)entityCache.find(srk, 
                                  new PropertyPK(propertyExpense
                                		  .getPropertyId(), propertyExpense.getCopyId()));

        d = (Deal)entityCache.find(srk, 
                                  new DealPK(property
                                		  .getDealId(), property.getCopyId()));
      }
      else if(input instanceof Deal)
      {
        d = (Deal)input;
      }

      Collection bors = entityCache.getBorrowers(d);
      if(bors != null)
      {
        Iterator it = bors.iterator();
        while(it.hasNext())
        {
          addTarget(it.next());
        }
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Borrower borrower = (Borrower)target;

    try
    {
      Deal deal = (Deal)entityCache.find(srk, 
                           new DealPK(borrower.getDealId(), borrower.getCopyId()));

//      int originalRepaymentTypeId = deal.getRepaymentTypeId();
//      int originalAmortizationTerm = deal.getAmortizationTerm();
      
      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));

      double dbTDSRatio3Year = 0;
      String rateCode = pricingProfile.getRateCode();
      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          borrower.setTDS3Year(0);
          borrower.ejbStore();
          return;
        }
      }

      double totalPropertyExpense = 0.0;
      Collection propertyExpenses = this.entityCache.getPropertyExpenses(deal);
      Iterator itPE = propertyExpenses.iterator();
      // ----- Get the Property Expense -----------
      PropertyExpense propertyExpense = null;
      while(itPE.hasNext())
      { //Loop thourgh all the Property Expenses
        propertyExpense = (PropertyExpense)itPE.next();
        if(propertyExpense.getPeIncludeInTDS())
        {
          totalPropertyExpense += propertyExpense.getPropertyExpenseAmount() *
            DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(), "PropertyExpensePeriod")
            * propertyExpense.getPePercentInTDS() / 100.00;
        }
        trace("totalProeprtyExpense= " + totalPropertyExpense);
      } // End of Property Expense
      trace("totalProeprtyExpense= " + totalPropertyExpense);
      //--------------------End of all Property-->PropertyExpense --------------------------

      double dbTotalAnnualIncome = 0;
      double dbTotalTDSExpenses = totalPropertyExpense;

      // -----------------Work with Incomes----------------------
      Collection incomes = this.entityCache.getIncomes(borrower);
      Iterator itIn = incomes.iterator();
      Income income = null;
      while(itIn.hasNext()) // Loop through all the ** incomes
      {
        income = (Income)itIn.next();
        // if Included in TDS
        //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
        //--> By Billy 02Nov2004
        if(income.getIncIncludeInTDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
        {
          dbTotalAnnualIncome = dbTotalAnnualIncome + income.getIncomeAmount() *
            DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod")
            * income.getIncPercentInTDS() / 100.00;
          //===========================================================================

        }
        if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
        {
          dbTotalTDSExpenses = dbTotalTDSExpenses - income.getIncomeAmount() *
            DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod")
            * income.getIncPercentInTDS() / 100.00;
        }
        trace(" totalAnnualIncome = " + dbTotalAnnualIncome);
      }
      trace(" totalAnnualIncome = " + dbTotalAnnualIncome);
      trace("totalTDSExpense = " + dbTotalTDSExpenses);

      // ------------------Work with Liabilities---------------------
      Collection liabilities = this.entityCache.getLiabilities(borrower);
      Iterator itLi = liabilities.iterator();
      Liability liability = null;
      while(itLi.hasNext()) //Loop through all the ** liabilities
      {
        liability = (Liability)itLi.next();
        if(liability.getIncludeInTDS() && (liability.getLiabilityPayOffTypeId() == 0))
        { // Include in TDS and Libility Payoff Type is Blank, ( id=0 )
          /*2"Credit Card"*/
          /*10 "Unsercured Line of Credit"*/
          /*13"Secured Line of Credit"*/
          // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
          // -------------------------------------------------------------------
          // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
          //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
          if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
          {
            dbTotalTDSExpenses += liability.getLiabilityAmount() *
              liability.getPercentInTDS() / 100.00 * 12;
          }
          else

          /*"Closing Costs" */
          /*"high ratio"*/
          if((liability.getLiabilityTypeId() == 14) && (deal.getDealTypeId() == 1))
          {
            dbTotalTDSExpenses += liability.getLiabilityAmount();
          }
          else
          {
            dbTotalTDSExpenses += liability.getLiabilityMonthlyPayment() * 12
              * liability.getPercentInTDS() / 100.00;
          }
        }
      }
      trace("totalTDSExpenses = " + dbTotalTDSExpenses);
      
//    handling MOSSYS prop com.basis100.calc.forcepandifor3year
      /*boolean isForcePandIFor3Year = (PropertiesCache.getInstance().getProperty(
              "com.basis100.calc.forcepandifor3year", "N")).equals("Y");
      
      int iProdTypeId = deal.getProductTypeId();
      trace("isForcePandIFor3Year = " + isForcePandIFor3Year + ", ProdTypeId = " + iProdTypeId);
      trace("Original RepaymentTypeId = " + originalRepaymentTypeId + ", AmortizationTerm = " + originalAmortizationTerm);

      if(isForcePandIFor3Year && iProdTypeId == Mc.PRODUCT_TYPE_SECURED_LOC)
      {
    	  deal.setRepaymentTypeId(0);
    	  deal.setAmortizationTerm(300);
    	  trace("forcepandifor3year=Y and ProdTypeId=LOC, set RepaymentTypeId:0, AmortizationTerm:300");
      }*/
//    END of handling MOSSYS prop com.basis100.calc.forcepandifor3year

      
      //--------------------- Store the result ---------------------------------------
      /*#DG538 allow accelerated precision
      dbTotalTDSExpenses += (DealCalcUtil.PandI3YearRate(srk, entityCache, deal, -100) + deal.getAdditionalPrincipal())
        	* DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");*/
      dbTotalTDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, true);
      trace(" PandI = " + deal.getPandiPaymentAmount());
      trace(" AdditionalPrincipay = " + deal.getAdditionalPrincipal());
      trace(" PaymentFrequency = " +
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

      if(dbTotalAnnualIncome != 0)
      {
        dbTDSRatio3Year = dbTotalTDSExpenses / dbTotalAnnualIncome * 100;
        dbTDSRatio3Year = Math.round(dbTDSRatio3Year * 100.00) / 100.00;
      }
      else
      {
        dbTDSRatio3Year = getMaximumCalculatedValue();

      }
      
      trace(">>>Returning from Calc 51, result = " + dbTDSRatio3Year);
      borrower.setTDS3Year(validate(dbTDSRatio3Year));
      borrower.ejbStore();
      return;

    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(target);
      logger.debug(msg);
    }

    borrower.setTDS3Year(this.DEFAULT_FAILED_DOUBLE);
    borrower.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInTDS"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInTDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInTDS"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));

    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    //4.3GR Jan 26, 2010, deleting: don't need discount because it's looking at PAndIUsing3YearRate
    //addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIUsing3YearRate"));  //#DG538
    addInputParam(new CalcParam(ClassId.DEAL , "GDSTDS3YearRate"));	//#DG930

    addTargetParam(new CalcParam(ClassId.BORROWER, "TDS3Year", 999.99));
  }

}
