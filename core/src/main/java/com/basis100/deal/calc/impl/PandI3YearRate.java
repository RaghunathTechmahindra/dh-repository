/**
 * Title:        PandI3YearRate (Calc-109)
 * Description:  Calculate the monthly Principal and Interest payment using the rate found in the 3 year fixed rate product 
 * Date :        28 Mar, 2006
 * @author BruceZ
 * @version
 * 
 * 26/Jan/2010, 4.3GR, FXP23563, FXP27174 changed drastically
 */

package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;

public class PandI3YearRate extends DealCalc{

    public PandI3YearRate() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 109";
    }

    public void getTarget(Object input, CalcMonitor dcm)
    throws TargetNotFoundException {
        
        try {
            addTarget(input);
        } catch (Exception e) {
            String msg = "Fail to get Targets" + this.getClass().getName();
            msg += ":[Input]:" + input.getClass().getName();
            msg += "\nReason: " + e.getMessage();
            msg += "@input: " + this.getDoCalcErrorDetail(input);
            logger.error(msg);
            throw new TargetNotFoundException(msg);
        }
    }

    public void doCalc(Object target) throws Exception {
        trace(this.getClass().getName());
        Deal deal = (Deal) target;

        try {
            //calculates PandI based on the 3year rate which is defined by "com.basis100.calc.gdstds3yratecode"
            //basically, the logic to calculate PandI is exactly the same as regular PandI Calc
            double pandiUsing3YearRate = DealCalcUtil.calcPandIFully3Year(deal, Mc.PAY_FREQ_MONTHLY, entityCache);
            logger.debug("Calc-109 PandI3YearRate : pandiUsing3YearRate=" + pandiUsing3YearRate);
            //this calc won't round the result
            deal.setPandIUsing3YearRate(validate(pandiUsing3YearRate));
            deal.ejbStore();
            return;

        } catch (Exception e) {
            String msg = "Benign Failure in Calculation"
                + this.getClass().getName();
            msg += ":[Target]:" + target.getClass().getName();
            msg += "\nReason: " + e.getMessage();
            msg += "@Target: " + this.getDoCalcErrorDetail(target);
            logger.error(msg);
            logger.error(e);
        }

        deal.setPandIUsing3YearRate(DEFAULT_FAILED_DOUBLE);
        deal.ejbStore();
    }

    @SuppressWarnings("unchecked")
    public Collection getTargets()
    {
        return this.targets;
    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }

    @SuppressWarnings("unchecked")
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
        addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
        addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
        addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
        addInputParam(new CalcParam(ClassId.DEAL , "GDSTDS3YearRate"));//4.3GR

        addTargetParam(new CalcParam(ClassId.DEAL, "PANDIUSING3YEARRATE", T13D2));
    }
}
