package com.basis100.deal.calc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.AppraisalOrder;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Bridge;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.InsureOnlyApplicant;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.LifeDisabilityPremiums;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.QualifyDetail;
import com.basis100.deal.util.StringUtil;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.PropertiesCache;

/**
*<pre>
*	The CalcMonitor class provides the functional requirements for the deal calculations monitor 
*   as described in the overview. It provides an API to the application layer (conducting a transaction) to:
* 
*     -�Notify the monitor of entity objects modified � see method inputEntity().<br>
*     -�Perform required deal calculations � see method calc().<br>
*     -�Reuse an existing DealCalcMonitor object (e.g. for another transaction) � see method reset().<br>  
*     - Obtain a copy of a DealCalcMonitor object avoiding initialization processing.<br>
*
*	CalcMonitor has an internal class � MonitorMatrix that acts as a container for
* DealCalc objects.
* The MonitorMatrix provides facilities for cloning, sorting and accessing
* DealCalc objects.
*	A static block within the CalcMonitor class provides for the creation of the
* initial instance of a CalcMonitor. All subsequent CalcMonitors are deep copies
* of this original instance.
* Initialization and sorting within this initial calc monitor
* takes place during construction. <br>
*
* The following steps occur during construction:
* 
*	1.�The com.basis100.deal.calc.impl directory is read to obtain the calculation implementation classes.
*	2.�The classes are constructed and placed in an initial list.
*	3. The list is passed to the MonitorMatrix for sorting.
*
*	</pre>
* @version 1.1
* Date:11-Jun-2008 <br>
* Author: MCM Impl Team <br>
* Story:XS_11.11
* Change: <br>
* Modified doRemoveInput() method <br>
* @version 1.2
* Date:10-July-2008 <br>
* Author: MCM Impl Team <br>
* Story:XS_2.39
* Change: <br>
* Modified fixed error comment<br>
* @version 1.3 MCM FXP22573, Oct 2, 2008, ignore if dependency is in the same class.
*/

public class CalcMonitor
{
  private static final Log _log = LogFactory.getLog(CalcMonitor.class);
  private MonitorMatrix matrix;
  private List calcs;

  /**
   */
  private Map inputAttachSets;
  private CalcEntityCache entityCache;
  private SessionResourceKit srk;
  private SysLogger logger;

  private boolean debugMode = false;
  private Vector relatedCalcs = null;
  private Vector firedCalcs = null ;

  private boolean isInit = false;

  private static CalcMonitor initialInstance;

  static
  {
     initialInstance = new CalcMonitor();
  }
  /**
   * Returns a CalcMonitor is a deep copy of the intial instance.
   * @return a deep copy initialized with the provided SessionResourceKit.
   * @param srk - the session resource kit through which transaction demarcations
   * are conveyed from the application layer.
   *
   */
  public static CalcMonitor getMonitor(SessionResourceKit srk)
  {
    if(initialInstance == null)
    {
       initialInstance = new CalcMonitor();
    }

    return (CalcMonitor)initialInstance.getClone(srk);
  }


  private CalcMonitor()
  {
    logger = ResourceManager.getSysLogger("CalcMonitor");

    Set names = null;

    //read the impl directory and get all the calc names
    try
    {
      names = getCalcList();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

    calcs = new ArrayList();
    Iterator it = names.iterator();
    String classname = null;

    //got the calc names now instantiate and add to list
    while(it.hasNext())
    {
       try
       {
        classname = (String)it.next();
        Class c = Class.forName(classname);
        addCalc((DealCalc)c.newInstance());
       }
       catch(Exception e)
       {
         logger.error("Failed to instantiate a calculation named: " + classname);
       }
    }
   // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
   logger.calcTrace (CalcMonitor.class, "Initial Calcs success ..." );

   matrix = new MonitorMatrix(calcs);
   inputAttachSets = matrix.getAttachMap();
   
   printDebug();
   
  }


  private CalcMonitor(MonitorMatrix m)
  {
    this.matrix = m;
    this.inputAttachSets = m.getAttachMap();
    isInit = true;
    logger = ResourceManager.getSysLogger("CalcMonitor");
  }


 /**
  * Called to 'register' the specified calculations object with this monitor.
  * Must be called for each calculation known to the monitor.
  * Calls occur immediately after construction, and before calling init().
  */

  private void addCalc(DealCalc obj)
  {
    calcs.add(obj);
  }


  private Set getCalcList()throws DealCalcException
  {
    CalcSet calcs = null;

    try
    {
      calcs = new CalcSet();
    }
    catch(Exception e)
    {
      throw new DealCalcException("Couldn't get list of Calculations - check config files");
    }

    return calcs.getCalcSet();
  }



  public void setResourceKit(SessionResourceKit srk)
  {
    this.srk = srk;
    this.matrix.setSessionResourceKit(srk);
  }

  /**
   *  gets a deep copy clone. Provided to support lightweight creation of
   *  monitor objects avoiding the need to register
   *  calculations objects and the processing performed by init().
   *  @return  a deep copy of a CalcMonitor initialized with a SessionResourceKit
   *  @param  srk - the session resource kit to be associatied with the copy
   */
  private CalcMonitor getClone(SessionResourceKit srk)
  {
    this.entityCache = null;   // for safety in case developer does not call dcm.calc().

    MonitorMatrix m = (MonitorMatrix)this.matrix.clone();

    CalcMonitor cm = new CalcMonitor(m);

    cm.setResourceKit(srk);

    return cm;
  }

  /**
   *  reset this monitor for reuse with the same SessionResourceKit.
   */
  public void reset()
  {
    matrix.reset();
  }

   /**
   *  reset this monitor for reuse with a new SessionResourceKit.
   */
  public void reset(SessionResourceKit srk)
  {
    matrix.reset();
    setResourceKit(srk);
  }

  public void inputEntity(Object o)
  {
    inputEntityImpl(o, false);
  }

  public void inputRemovedEntity(Object o)
  {
    inputEntityImpl(o, true);
  }

  public void inputCreatedEntity(Object o)
  {
    inputEntityImpl(o, true);
  }



  /**
  *	Associate the given DealEntity object with this DCM.<br/>
  *
  * If the object parameter is not an instance of Deal Entity - this method does nothing, <br/>
  * Otherwise the class id of the object is determined. <br/>
  *
  * If the monitor contains an inputAttachSet associated with the provided parameter
  * class typeId this method iterates over the calculation objects in
  * the matching input attach list calling the attachInput()
  * methods for each  using the parameter object as an argument.
  * @version 1.2: MCM Team: fixed error message
  */
  private void inputEntityImpl(Object o, boolean remove)
  {
     if(!(o instanceof DealEntity)) return;
     DealEntity recieved = (DealEntity)o;

     try
      {
         int id = recieved.getClassId();
         Set attachedInputs = (Set)inputAttachSets.get(new Integer(id));

         if( ! attachedInputs.isEmpty() )
         {
             Iterator setit = attachedInputs.iterator();
             DealCalc clc = null;

             Collection vals = recieved.getChangedFields ();
             if ( vals != null )
             {
                 Iterator it = vals.iterator();

                 String currentKey = null;
                 CalcParam cp = null ;

                 while(setit.hasNext()) // loop through Calcs
                 {
                    clc = (DealCalc)setit.next();

                    if(remove)
                    {
                      clc.attachInput(o);
                    }
                    else
                    {
                      while(it.hasNext())   // Go through all the changed field of the recieved DealEntity
                      {
                         currentKey = (String)it.next();
                         cp = new CalcParam( id , currentKey);  // Generate a CalcParam for compare

                         if  ( clc.hasInput ( cp ) )
                          clc.attachInput ( o ) ;   // only specified field is set will trigger calc.
                      }

                      it = vals.iterator () ;
                    }
                 }
             }
          }
      }
      catch(Exception e)
      {
          String msg = "[CalcMonitor.inputEntity] error inputing Entity: " ;
          msg +=  "PKName= " + "[" + recieved.getPk().getName () + "=" + recieved.getPk().getId () + ";" ;
          msg +=  "CopyId=" + recieved.getPk().getCopyId () + "]" ;
          logger.error ( msg + " :: " + com.basis100.deal.util.StringUtil.stack2string(e) );
          //null pointer or classcast either way ....
          return;
      }
  }
  ///------------- Remove the object from the Matrix ----------------------
  public void removeInput(Object o)
  {
      try
      {
          DealEntity recieved = (DealEntity)o;
          int id = recieved.getClassId();
          Set attachedInputs =  (Set)inputAttachSets.get(new Integer(id));

          if( ! attachedInputs.isEmpty() )
          {
              //---------- go through the picked list and check ------------------
              Iterator setit = attachedInputs.iterator();
              while(setit.hasNext())
              {
                DealCalc clc = (DealCalc)setit.next();
                doRemoveInput( clc.getInputs () , recieved , id );
              } // ----------- End of Picked Calc List
          }
      }
      catch(Exception e)
      { //null pointer or classcast either way ....
          String msg = "[CalcMonitor.removeInput] removing Entity: ";
          msg += "[" + o.getClass ().getName () + "]" + e.getMessage () ;
          logger.error ( msg );
          return;
      }
  }

  /**
   * @version 1.0 Initial Version
   * @version 1.1 <br>
   * 		Date 11-Jun-2008 <br>
   * 		Author:MCM Impl Team <br>
   * 		Story:XS_11.11 & XS_11.6 <br>
   *      Change: Added cases for COMPONENTLOC ,COMPONENT, COMPONENTMORTGAGE <br>
   * @version 1.2 <br>
   *    Date 25-Jun-2008 <br>
   *    Author:MCM Impl Team <br>
   *    Story:XS_11.5 <br>
   *      Change: Added cases for COMPONENTLoan <br>
   * @version 1.3 <br> Nov 20, 2008: FXP23533: fixed infinite loop in doRemoveInput.
   *    
   * @param inputs -
   *            Collection of Calculation input
   * @param recieved -
   *            Recieved Entity
   * @param id -
   *            Class Id
   */
  private void doRemoveInput(Collection inputs, DealEntity recieved, int id )
  {
      if ( inputs == null ) return ;

      Iterator itInputs = inputs.iterator ();

      while ( itInputs.hasNext () )
      {
          switch ( id )
          {
          case ClassId.DEAL  :
              if ( ((Deal)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.BORROWER  :
              if ( (( Borrower)recieved).equals ( itInputs.next() ) )
                  itInputs.remove ();
              break;
          case ClassId.PROPERTY  :
              if ( ((Property) recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.ASSET  :
              if ( ((Asset)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.DOWNPAYMENTSOURCE  :
              if ( (( DownPaymentSource)recieved).equals ( itInputs.next() ) )
                  itInputs.remove ();
              break;
          case ClassId.INCOME  :
              if ( (( Income)recieved).equals ( itInputs.next () ))
                  itInputs.remove ();
              break;
          case ClassId.LIABILITY  :
              if ( (( Liability )recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break ;
          case ClassId.PROPERTYEXPENSE  :
              if ( (( PropertyExpense)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.ESCROW  :
              if ( (( EscrowPayment )recieved) .equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.CONDITION  :
              if ( (( Condition )recieved) .equals ( itInputs.next ()  ) )
                  itInputs.remove() ;
              break ;
          case ClassId.FEE  :
              if ( (( Fee ) recieved).equals ( itInputs.next () ) )
                  itInputs.remove () ;
              break;
          case ClassId.DEALFEE  :
              if ( (( DealFee)recieved).equals ( itInputs.next () ))
                  itInputs.remove ();
              break;
          case ClassId.EMPLOYMENTHISTORY  :
              if ( (( EmploymentHistory ) recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break ;
          case ClassId.DOCUMENTTRACKING  :
              if ( (( DocumentTracking )recieved).equals (itInputs.next() ) )
                  itInputs.remove ();
              break;
          case ClassId.MTGPROD  :
              if ( (( MtgProd )recieved).equals ( itInputs.next ()  ))
                  itInputs.remove();
              break ;
          case ClassId.BRIDGE :
              if ( ( (Bridge)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break ;
          case ClassId.APPRAISALORDER  :
              if ( (( AppraisalOrder)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.LIFEDISABILITYPREMIUMS  :
              if ( (( LifeDisabilityPremiums)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
              //--DJ_CR136--23Nov2004--start--//
          case ClassId.INSUREONLYAPPLICANT  :
              if ( (( InsureOnlyApplicant)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
              //--DJ_CR136--23Nov2004--end--//                


              //***** Change by MCM Impl. Team - Version 1.1  - Starts*****//
              //XS_11.11 & 11.6 Added Case for ComponentLOC, Component, ComponentMortgage

          case ClassId.COMPONENT  :
              if ( (( Component)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.COMPONENTLOC  :
              if ((( ComponentLOC)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;

          case ClassId.COMPONENTMORTGAGE  :
              if ( (( ComponentMortgage)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
              //***** Change by MCM Impl. Team - Version 1.1  - ends*****//
              //***** Change by MCM Impl. Team - Version 1.2  - starts*****//    
          case ClassId.COMPONENTLOAN :
              if ( (( ComponentLoan)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
              //***** Change by MCM Impl. Team - Version 1.2  - ends*****//          


              //FXP23533: MCM Nov 20 2008 -- start
          case ClassId.COMPONENTCREDITCARD  :
              if ((( ComponentCreditCard)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.COMPONENTSUMMARY  :
              if ((( ComponentSummary)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.COMPONENTOVERDRAFT  :
              if ((( ComponentOverdraft)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          case ClassId.QUALIFYDETAIL  :
              if ((( QualifyDetail)recieved).equals ( itInputs.next () ) )
                  itInputs.remove ();
              break;
          default:
              itInputs.next(); // it has to go to next anyway.
              _log.error(StringUtil.stack2string(new RuntimeException(
                  "CalcMonitor.doRemoveInput : unknown class id = " + id)));
          }              
          //FXP23533: MCM Nov 20 2008 -- end
      }
  }
  /////////////////////////////////////////////////////////////////////////////////
  // Remove the object from the target list ..
  // go through all the calcs ==> calc
  // check the calc.targParam , see if targParam.classid = o.classid
  // if yes
  // remove o from the calc.inputList
  public void removeTarget( Object o )
  {
      try
      {
          DealEntity recieved = (DealEntity)o;
          int id = recieved.getClassId();
          Set attachedInputs = null;
          Iterator itAllCalcs = null;
          DealCalc clc = null;
          Collection targetParams = null;
          Iterator itTP = null;

          for ( int i = 1;  i < ClassId.MAX ; i++  )
          {
              attachedInputs = (Set)inputAttachSets.get( new Integer(i) );
              if ( attachedInputs != null )
              {
                  itAllCalcs = attachedInputs.iterator () ;

                  while ( itAllCalcs.hasNext () ) // loop through all the Calculation
                  {
                      clc = (DealCalc)itAllCalcs.next();
                      targetParams = clc.getTargetParams () ;
                      itTP = targetParams.iterator () ;
                      while ( itTP.hasNext () )
                      {
                          CalcParam calcParam = ( CalcParam ) itTP.next () ;
                          if (  calcParam.getClassId () ==  id )
                          {
                              Collection inputs = clc.getInputs ();   // input list
                              Iterator itInputs = inputs.iterator () ;

                              while ( itInputs.hasNext () )
                              {
                                  DealEntity inputEntityItem = ( DealEntity ) itInputs.next () ;  //
                                  if ( inputEntityItem.getClassId () == id ) // this Entity is the same type of o
                                      itInputs.remove (); // remove it
                              } // Loop next input
                          } // ---- End of if Target is same as o
                      }// ------------- End of removing from target list
                  } // --- end of itAllCalcs.hasNext () ----------------
              } // end of attachedInputs != null
          } // --- End of for loop ----------------------
      } catch ( Exception e )
      {
        String msg = "[CalcMonitor.removeTarget] error removing Target: " ;
        msg += "[" + o.getClass ().getName () + "]" + e.getMessage () ;
        logger.error ( msg );
      }
  } // ======  End of Remove Target  ---------------------
/////////////////////////////////////////////////////////////////////////////////


  public void calc() throws DealCalcException
  {
     checkLogTracing();
     java.util.Date today = new java.util.Date();
	 //4.4 Transaction History
     boolean notAuditable = "N".equalsIgnoreCase(PropertiesCache.getInstance().getProperty(
    		 srk.getExpressState().getDealInstitutionId(), "com.filogix.transactionhistory.trackcalcs", "N"));
     if (notAuditable)
    	 srk.setInterimAuditOn(false);

     try
     {
       this.entityCache = new CalcEntityCache(this);
       // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
       logger.calcTrace (CalcMonitor.class,  "Calculation Start updated: " + today  + "-----------");
       int rows = matrix.rows();

       for(int i = rows -1 ; i >= 0; i--)
       {
         List currentRow = matrix.getRow(i);

         ListIterator li = currentRow.listIterator();
         while(li.hasNext())
         {
          DealCalc currentCalc = (DealCalc)li.next();
          if ( ! currentCalc.getInputs ().isEmpty () )
          {
            // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
            logger.calcTrace (CalcMonitor.class, "Calc:[" + currentCalc.getCalcNumber () + "] find target");
            currentCalc.checkTargets(this);   // Generate targets
            // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
            logger.calcTrace (CalcMonitor.class, "Calc:[" + currentCalc.getCalcNumber () + "] find target Done ");
            currentCalc.doCalcs();

            if ( debugMode  &&
                   (!firedCalcs.contains ( currentCalc.getClass ().getName() )  ) )
                firedCalcs.addElement ( currentCalc.getClass ().getName() );

            // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
            logger.calcTrace (CalcMonitor.class, "Calc:[" + currentCalc.getCalcNumber () + "] doCalc Done ");
          }
         }
       }

       // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
      logger.calcTrace (CalcMonitor.class,  "Calculation Finished at: " + today + "-----------");
      entityCache = null;

    }
    catch (DealCalcException e1)
    {
      restoreLogTracing();
      throw e1;
    }
    //4.4 Transaction History
    finally
    {
    	if (notAuditable)
    		srk.restoreAuditOn();
    }

    restoreLogTracing();
  }

  public CalcEntityCache getCalcEntityCache()
  {
    if(this.entityCache == null)
     throw new IllegalStateException("Cache is only available during CalcMonitor.calc operation");

    return entityCache;
  }

  public String toString()
  {
    String out = "CalcMonitor\n";
    out += matrix.toCalcNumberString();
    return out;
  }

  SysLogger jExecLogger = null;

  /**
  *
  * Checks to determine if calc monitor tracing has been turned off. If so ensures that
  * trace statement for the jdbc executor used during the calculation operation likewise
  * has tracing turned off.
  *
  **/

  private void checkLogTracing()
  {
     if (SysLog.isCalcTraceOn() == true)
      return;

     if (srk == null)
      return;

     JdbcExecutor jExec = srk.getJdbcExecutor();
     if (jExec == null)
      return;

     jExecLogger = jExec.getLogger();
     if (jExecLogger == null)
      return;

      jExecLogger.setJdbcTraceOn(false);
  }

  /**
  *
  * Restore jdbc executor logger tracing to original state (if changed in invocation of calc()).
  *
  **/


  private void restoreLogTracing()
  {
     if (jExecLogger == null)
      return;

      jExecLogger.setJdbcTraceOn(SysLog.isJdbcTraceOn());
  }


  class MonitorMatrix implements Cloneable
  {
     List x;
     Map attachMap;
     boolean attachInit;
     boolean matrixInit;

     MonitorMatrix(List calcs)
     {
       List temp = calcs;
       setMatrix(temp);
     }

     /**
     *  creates the List of lists that comprises the matrix and sorts the
     * supplied collection of DealCalc objects
     */
     private void setMatrix(Collection all)
     {
       x = new ArrayList();
       x.add(new ArrayList(all));

       while(doNextRow(x)){;}

       sweep(x);

       initAttachMap();

       return;
     }
     /**
     *  helper method to setMatrix()
     */
     private boolean doNextRow(List x)
     {
       List currentRow = (List)x.get(x.size()-1);

       if(currentRow.size() < 2) return false;

       boolean inputFound = false;

       List nextRow = new ArrayList();

       Iterator it = currentRow.iterator();

       while(it.hasNext())
       {
           DealCalc currentCalc = (DealCalc)it.next();

           if(isInput(currentCalc,currentRow))
           {
             nextRow.add(currentCalc);
             inputFound = true;
           }
       }

       if(nextRow.size() >= currentRow.size())
       {
             return false; // an exception should be thrown here an input/target loop has been discovered
       }

       if(inputFound)
       {
         x.add(nextRow);
         doNextRow(x);
       }

       return inputFound;
     }

     /**
     *  helper method to setMatrix() sweeps the rows for redundant calc objects
     *  after sort has occured
     */
     private void sweep(List x)
     {
       int end =  x.size() - 1;

       for(int i = 0; i < end  ; i++)
       {
         List row = (List)x.get(i);
         List below = (List)x.get(i + 1);
         Iterator it = below.iterator();

         while(it.hasNext())
         {
           row.remove(it.next());
         }
       }

     }

     private void reset()
     {
        ListIterator li = x.listIterator();

        while(li.hasNext())
        {
          List currentRow = (List)li.next();

          ListIterator ri = currentRow.listIterator();

          while(ri.hasNext())
          {
             DealCalc clc = (DealCalc)ri.next();
             clc.reset();
          }
        }
     }

     /**
     *  creates and returns a map of inputAttachSets based on the current matrix
     */
     private Map initAttachMap()
     {
       attachMap = new HashMap(131);

       for(int i = 1; i < ClassId.MAX; i++)
       {
         attachMap.put(new Integer(i), new HashSet());
       }

       ListIterator li = x.listIterator();

       while(li.hasNext())
       {
          List currentRow = (List)li.next();

          ListIterator ri = currentRow.listIterator();

          while(ri.hasNext())
          {
             DealCalc clc = (DealCalc)ri.next();
             Collection inputClassTypes = clc.getInputClassTypes();

             Iterator classTypes = inputClassTypes.iterator();

             while(classTypes.hasNext())
             {
               Integer type = (Integer)classTypes.next();

               Set attachList = (Set)attachMap.get(type);

               attachList.add(clc);
             }
          }
       }
       this.attachInit = true;
       return attachMap;
     }

     public Map getAttachMap()
     {
        if(attachInit)
          return this.attachMap;
        else
          return this.initAttachMap();
     }

     public String printInputAttachSets()
     {
       String ret = "";

       for(int i = 1; i < ClassId.MAX; i++)
       {
         Set s = (Set)attachMap.get(new Integer(i));
         ret += "\nAttach(" + i + ") : ";
         ret += s.toString();

       }
       return ret;
     }

     private int rows()
     {
        return x.size();
     }

     private List getRow(int index)
     {
       return (List)x.get(index);
     }


     private DealCalc get(int row, int index)
     {
       List rowlist = getRow(row);
       return (DealCalc)rowlist.get(index);
     }

     private void setSessionResourceKit(SessionResourceKit srk)
     {
       ListIterator xit = x.listIterator();
       List currentrow;

       while(xit.hasNext())
       {
          currentrow = (List)xit.next();

          ListIterator cont = currentrow.listIterator();
          DealCalc c = null;

          while(cont.hasNext())
          {
            c = (DealCalc)cont.next();
            c.setResourceKit(srk);
          }
      }

     }

     public Object clone()
     {
        try
        {
          MonitorMatrix other = (MonitorMatrix)super.clone();
          other.x = new ArrayList();

          ListIterator lit = this.x.listIterator();
          List current = null;

          while(lit.hasNext())
          {
            current = (List)lit.next();
            List newrow = new ArrayList();

            ListIterator cont = current.listIterator();
            DealCalc c = null;

            while(cont.hasNext())
            {
              c = (DealCalc)cont.next();
              newrow.add((DealCalc)c.clone());
            }

            other.x.add(newrow);
          }

          other.attachInit = false;
          other.matrixInit = true;

          return other;
        }
        catch(CloneNotSupportedException cnse)
        {
          return null;
        }
     }

     /**
     * @return true if the calc target is an input to any calc in row param
     */
     private boolean isInput(DealCalc calc, Collection row)
     {
         Iterator rowit = row.iterator();

         while(rowit.hasNext())
         {
             //starts MCM FXP22573, Oct 2, 2008, ignore if dependency is in the same class.
             DealCalc each = (DealCalc)rowit.next();
             if (calc == each) continue;
             //if(calc.isInputTo((DealCalc)rowit.next()))
             if(calc.isInputTo(each)) return true;
             //ends MCM FXP22573
         }
         return false;
     }

     public String toString()
     {
       ListIterator it = x.listIterator();
       String out = "";

       while(it.hasNext())
       {
         List row = (List)it.next();
         out += "\tRow: ";

          ListIterator it2 = row.listIterator();

          while(it2.hasNext())
          {
             DealCalc current = (DealCalc)it2.next();
             String n = current.getClass().getName();
             int index = n.lastIndexOf('.');
             n = n.substring(index+1);
             out+= "[" + n + "]";
          }

         out+= "\n";
       }
       return out;
     }

      public String toCalcNumberString()
     {
       ListIterator it = x.listIterator();
       String out = "";

       while(it.hasNext())
       {
         List row = (List)it.next();
         out += "\tRow: ";

          ListIterator it2 = row.listIterator();

          while(it2.hasNext())
          {
             DealCalc current = (DealCalc)it2.next();
             String n = current.getCalcNumber();
             out+= "[" + n + "]";
          }

         out+= "\n";
       }
       return out;
     }
  }

  /////////////////////////////////////////////////////////////////////////////////////////
  public void setDebug ( boolean b )
  {
    debugMode = b;
    if (debugMode)
    {
        firedCalcs = new Vector();
        relatedCalcs = new Vector();
    }
    else
    {
        firedCalcs = null;
        relatedCalcs = null;
    }
  }

  public void printRelatedCalcs( )
  {
      // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
    logger.calcTrace(CalcMonitor.class, "******************* Related Calculations *****************") ;
    for ( int i =0; i <= relatedCalcs.size () -1 ; i ++ )
    {
        // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
      logger.calcTrace(CalcMonitor.class,  "<" + i + "> [" + (String) relatedCalcs.get ( i ) + "]" );
    }
    // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
    logger.calcTrace(CalcMonitor.class, "******************* End of Related Calculations *****************") ;
  }

  public void printFiredCalcs( )
  {
      // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
    logger.calcTrace(CalcMonitor.class, "******************* Fired Calculations *****************") ;
    for ( int i =0; i <= firedCalcs.size () -1 ; i ++ )
    {
      // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
      logger.calcTrace(CalcMonitor.class,  "<" + i + "> [" + (String) firedCalcs.get ( i ) + "]" );
    }
    // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
    logger.calcTrace(CalcMonitor.class, "******************* End of Fired Calculations *****************") ;
  }

  public void findRelatedCalcs( CalcParam calcParam )
  {
    try
    {
         for ( int classId = 1;  classId <= ClassId.MAX ; classId ++  )
         {
             Set calcRow = (Set)inputAttachSets.get( new Integer( classId ));
             if ( calcRow != null )
             {
                 Iterator calcs = calcRow.iterator();
                 DealCalc calc = null;

                 while( calcs.hasNext()) // loop through Calcs
                 {
                      calc = (DealCalc)calcs.next();
                      if  ( calc.hasInput ( calcParam ) )
                      {
                          if ( !relatedCalcs.contains ( calc.getClass ().getName ()))
                              relatedCalcs.addElement ( calc.getClass().getName () );
                      }

                 }
             }
         } // loop by ClassId
    } catch ( Exception e )
    {
      logger.error ( "FindeReatedCalcs Error @ CalcParam(" + calcParam.getClassId () + ":" + calcParam.getName () + ")" );
    }
 }
  
  public void printDebug(){
	  if(_log.isDebugEnabled() == false) return;
	  try{
		  //## dump from matrix --------------------------------------------------##
		  _log.debug("start MonitorMatrix ---------- Calcs will be executed from bottom to top");
		  _log.debug("\n\n" + matrix.toString());
		  _log.debug("end MonitorMatrix-----------------------\n");

		  //## calc dependency 1 by 1 --------------------------------------------##
		  _log.debug("start printing Calc dependencies 1 by 1 ----------");
		  //dependency check
		  List<DealCalc> all = new ArrayList<DealCalc>();
		  for(Iterator<List> it = matrix.x.iterator(); it.hasNext();){
			  all.addAll(it.next());
		  }
		  ArrayList<String> msgList = new ArrayList<String>();
		  for(DealCalc cal : all){
			  for(DealCalc each : all){
				  if(cal.isInputTo(each)){
					  msgList.add("\t" + each + " " + each.getClass().getSimpleName()
							  + "\t -- (depends on) -->  \t" + cal + " " + cal.getClass().getSimpleName() );
				  }
			  }
		  }
		  Collections.sort(msgList);
		  for(String st: msgList) _log.debug(st);    
		  _log.debug("end printing Calc dependencies  1 by 1 ----------");

		  //## calc print dependency tree forward start---------------------------##
		  _log.debug("dependency tree forward start =====================");
	      List<DealCalc> independents = new ArrayList<DealCalc>();
	      independents.addAll(all);
	      
	      for(DealCalc cal : all){
	          for(DealCalc each : all){
	              if(cal.isInputTo(each)){
	            	  independents.remove(each);
	              }
	          }
	      }
	      
	      msgList = new ArrayList<String>();
	      for(DealCalc cal : independents){
	    	  msgList.addAll(quickAnalisys(0, cal, all, false));
	      }
	      for(String msg: msgList) _log.debug(msg);
	      _log.debug("dependency tree forward end =====================");
	      
	    //## calc print dependency tree backward start---------------------------##
	      _log.debug("dependency tree backward start  =====================");
	      List<DealCalc> dependencyTreeBottom = new ArrayList<DealCalc>();
	      dependencyTreeBottom.addAll(all);
	      
	      for(DealCalc cal : all){
	          for(DealCalc each : all){
	              if(cal.isInputTo(each)){
	            	  dependencyTreeBottom.remove(cal);
	              }
	          }
	      }
	      msgList = new ArrayList<String>();
	      
	      msgList = new ArrayList<String>();
	      for(DealCalc cal : dependencyTreeBottom){
	    	  msgList.addAll(quickAnalisys(0, cal, all, true));
	      }
	      for(String msg: msgList) _log.debug(msg);
	      _log.debug("dependency tree backward end  =====================");
	      
	  }catch(Exception ignore){
		  _log.warn("-- an exception happends in a debug method --", ignore);
	  }
  }
  
  private List<String> quickAnalisys(int level, DealCalc calc, List<DealCalc> all, boolean reverse){
	  

	  List<String> msgs = new ArrayList<String>();
	  List<String> childMsgs = new ArrayList<String>();
	  int numOfDependency = 0;
      for(DealCalc each : all){
    	  boolean dependency = (reverse) ? (each.isInputTo(calc)) : (calc.isInputTo(each));
          if(dependency){
        	  numOfDependency++;
        	  if(calc == each){
        		  String msg = "\tlv:\t" + level + "\t";
        		  for(int i=0; i<level; i++) msg+="\t";
        		  msg += "################ Circular Reference is detected in the Calc :" 
        				  + calc.getClass().getSimpleName() + " : " + calc + " ################## ";
        		  msgs.add(msg);
        		  continue;
        	  }
        	  childMsgs.addAll(quickAnalisys(level + 1, each, all, reverse));
          }
      }
      
	  if(level==0) msgs.add("----------------------------------------------");
	  StringBuffer sb = new StringBuffer();
	  sb.append("\tlv:\t").append(level).append("\t");
	  for(int i=0; i<level; i++)
		  sb.append("\t");
	  sb.append(calc.getClass().getSimpleName());
	  sb.append(" ").append(calc);
	  if(level==0 && numOfDependency == 0)
		  sb.append("  -- is a standalone calc ");
	  else if (!reverse && numOfDependency > 0)
		  sb.append("  -- <<< this calc's output will be the input for the following lv:" + (level + 1) + " of " + numOfDependency + " calc(s)... ");
	  else if (!reverse && numOfDependency == 0)
		  sb.append("  ");
	  else if (reverse && numOfDependency > 0)
		  sb.append("  -- >>> this calc uses output data from the following lv:" + (level + 1) + " of " + numOfDependency + " calc(s)... ");
	  else if (reverse && numOfDependency == 0)
		  sb.append("  ");

	  msgs.add(sb.toString());
	  msgs.addAll(childMsgs);
	  return msgs;
  }
  

  
}
