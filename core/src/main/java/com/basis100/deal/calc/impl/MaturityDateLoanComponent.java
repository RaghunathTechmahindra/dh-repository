/*
 * @(#)MaturityDateLoanComponent.java July 04, 2008
 *
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: MaturityDateLoanComponent
 * </p>
 * <p>
 * Description:Calculates MaturityDate for Loan components of
 * "component eligible" deals.
 * </p>
 *
 * @author MCM Impl Team <br>
 * @Date 04-July-2008 <br>
 * @version 1.0 XS_11.15 Initial Version <br>
 *          Code Calc-14.CLoan <br>
 */
public class MaturityDateLoanComponent extends DealCalc
{
    final long MillSecsInWeek = 604800000 ;  //7 * 24 * 60 * 60 * 10000
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     *
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.15 04-July-2008 Initial Version
     */
    public MaturityDateLoanComponent()throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 14.CLoan";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     *
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.15 04-July-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget ( input );
        }
        catch ( Exception e )
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     *
     * @version 1.0 XS_11.15 04-July-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());

        ComponentLoan componentLoan = (ComponentLoan) target ;
        Component componentObj= (Component) entityCache.find(srk, new ComponentPK(componentLoan.getComponentId(),
                componentLoan.getCopyId()));
        Deal dealObj= (Deal) entityCache.find(
                srk, new DealPK(componentObj.getDealId(),
                        componentObj.getCopyId()));

            try
            {

                Calendar maturityDate = Calendar.getInstance( Locale.getDefault () ) ;
                Date IAD = componentLoan.getInterestAdjustmentDate ( ) ;

                int paymentFrequencyId = componentLoan.getPaymentFrequencyId () ;
                trace( "PaymentFrequencyId = " + paymentFrequencyId );
                int term = componentLoan.getActualPaymentTerm () ;
                trace( "ActualPaymentTerm = " + term );
                if ( IAD != null )
                {
                    maturityDate.setTime (  IAD );
                    int dayOfWeek = maturityDate.get (  Calendar.DAY_OF_WEEK  ); //IAD Date WeekDay

                    Date IADdt = maturityDate.getTime(); //IAD date variable

                    // If Force Maturity Date is Monthly then skip the following calc and set MaturityDate to IAD + term months
                    if ((PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(), "com.basis100.calc.forcematuritydatetomonthlyequ", "N")).equals("Y"))
                    {
                        trace( "MaturityDate (Force to Monthly Equvalent = " + maturityDate.getTime () ) ;
                        maturityDate.add (Calendar.MONTH , term );
                        componentLoan.setMaturityDate ( maturityDate.getTime () );
                        componentLoan.ejbStore ();
                        return;
                    }

                    //Maturity date calculation for NBC  - BEGIN
                    double dNoOfPayments = 0.0d;
                    double dTotalPaymentTerms = 0.0d;
                    int intDays = 1;
                    if ((PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(), "com.basis100.calc.maturitydatemethod", "N")).equals("NBC"))
                    {
                        if ( oneOf( paymentFrequencyId,
                                Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                        {
                            //NBC202556 - round weekly and biweekly paymentperiods
                            dNoOfPayments = Math.round(365/14.0);
                            dTotalPaymentTerms = (term / 12.0d) * dNoOfPayments;
                            intDays = 2;
                        }
                        else if ( oneOf( paymentFrequencyId,
                                Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY ) )
                        {
                            dNoOfPayments = Math.round(365/7.0);
                            dTotalPaymentTerms = (term / 12.0d) * dNoOfPayments;
                        }
                        dTotalPaymentTerms = Math.round(dTotalPaymentTerms);
                        maturityDate.add(Calendar.DATE, ((int)dTotalPaymentTerms) * 7 * intDays);
                        if ( oneOf( paymentFrequencyId,
                                Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY,
                                Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY) )
                        {
                            componentLoan.setMaturityDate ( maturityDate.getTime () );
                            componentLoan.ejbStore ();
                            return;
                        }
                    }
                    // Maturity date calculation for NBC  - END

                    maturityDate.add (Calendar.MONTH , term );
                    componentLoan.setMaturityDate ( maturityDate.getTime () );
                    int dayOfWeek2 = maturityDate.get ( Calendar.DAY_OF_WEEK    ) ;//IAD + Term date day

                    // (Accelerated) Weekly, BiWeekly, Make sure same weekday.
                    if ( oneOf( paymentFrequencyId,
                            Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY ,
                            Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                    {
                        int dayDifferent = dayOfWeek - dayOfWeek2; // difference between the weekdays
                        if ( dayOfWeek != dayOfWeek2 ) // Making the day same for both IAD & MaturityDate
                        {
                            maturityDate.add(Calendar.DATE, dayDifferent);
                        }
                        if(dayDifferent>0)
                        {
                            maturityDate.add(Calendar.DATE , - 7 );
                        }

                    }
                    trace( "MaturityDate = " + maturityDate.getTime () ) ;
                    // If Bi-Weekly, make sure Weeks between IAD and CalculatedMaturidyDate is even
                    if ( oneOf( paymentFrequencyId,
                            Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                    {
                        double weekDiff = (((maturityDate.getTime().getTime() - IADdt.getTime())) * (1.0d))/ MillSecsInWeek;
                        if(Math.round(weekDiff) % 2 != 0)
                        {
                            maturityDate.add( Calendar.DATE , - 7 );
                        }

                    }
                    trace( "MaturityDate = " + maturityDate.getTime () ) ;
                    componentLoan.setMaturityDate ( maturityDate.getTime () );
                    componentLoan.ejbStore ();
                }
                return;
            }
            catch ( Exception e )
            {
                StringBuffer msg = new StringBuffer(100);
                msg.append("Begin Failure in Calculation");
                msg.append(this.getClass().getName());
                msg.append(":[Target]:");
                msg.append(target.getClass().getName());
                msg.append("  Reason: ");
                msg.append(e.getMessage());
                msg.append("@Target: ");
                msg.append(this.getDoCalcErrorDetail(target));
                logger.debug(msg.toString());
            }

            componentLoan.setMaturityDate(DEFAULT_FAILED_DATE);
            componentLoan.ejbStore();
        }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     *
     * @version 1.0 XS_11.15 04-July-2008 Initial Versionn
     *
     */
    private void initParams()throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        //Adding input parameters
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "InterestAdjustmentDate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN , "actualPaymentTerm"));

        addTargetParam(new CalcParam(ClassId.COMPONENTLOAN, "maturityDate"));
    }


}
