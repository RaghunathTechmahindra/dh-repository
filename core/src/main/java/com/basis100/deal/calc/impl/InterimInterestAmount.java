/**
 * Title:        InterimInterestAmount (Calc-12)
 * Description:  Calculation for Interim Interest Amount
 * Copyright:    Copyright (c) 2001
 * Company:      Basis100
 * Date :        23 Nov 2001
 * @author Billy Lam
 * @version
 */

package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.calc.*;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.* ;
import com.basis100.resources.PropertiesCache;

import MosSystem.Mc;


public class InterimInterestAmount extends DealCalc
{
  Deal deal;                                                                                       // Catherine, #1645 - Concentra, deal is a class member now
  
  public InterimInterestAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 12";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget( input ) ;        // Only deal comes here.
    }
    catch ( Exception e )
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     deal = (Deal) target ;                                                                        // Catherine, #1645 - Concentra, deal is a class member now 
     double perDiemInterest = 0d;
     double theResult = 0d;
     try
      {
        // Catherine, #1645 - Concentra - CR # 153 ------- begin  
        boolean isRoundPerDiem = (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.deal.calc.InterimInterest.norounding", "N")).equals("Y");
        trace("isRoundPerDiem = " + (isRoundPerDiem ? "Y" : "N"));
        
        if (isRoundPerDiem) {
          try {
            perDiemInterest = calcPerDiemInterestNoRound();
            perDiemInterest = Math.round ( perDiemInterest * 100000 ) / 100000.00 ;                // round to 5 decimal places
          }
          catch (Exception ex) {
            logger.debug("calc.InterimInterestAmount@doCalc(): Failed calculation perDiemInterestNoRound: " 
                + StringUtil.stack2string(ex));
          }
        } else {
          perDiemInterest = deal.getPerdiemInterestAmount();                                       // rounded to 2 decimal places
        }
        trace("calc.InterimInterestAmount@doCalc(): perDiemInterest rounded = " + perDiemInterest);
        theResult = perDiemInterest * deal.getIADNumberOfDays();
        theResult = Math.round( theResult * 100 ) / 100.00;                                        // round to 2 decimal places
        deal.setInterimInterestAmount(theResult);
        
        // Catherine, #1645 - Concentra - CR # 153 ------- end
        
        trace("Calc result : InterimInterestAmount = " + theResult);

        deal.ejbStore ();
        return;
      }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     deal.setInterimInterestAmount(DEFAULT_FAILED_DOUBLE);
     deal.ejbStore ();

  }

  // Catherine, #1645 - Concentra - CR # 153 ------- begin
  // calculation was borrowered from calc.PerDiemInterest 
  private double calcPerDiemInterestNoRound() throws Exception {
    
    // Get the InterestCompoundDesc from deal.lenderprofile.interestcompoundId
      // added inst id for ML
    String InterestCompoundDesc 
        = PicklistData.getMatchingColumnValue(-1,
                                              "interestcompound",
                                              deal.getLenderProfile().getInterestCompoundId(), 
                                              "interestcompounddescription");
    double interestFactor = 0;
    if(InterestCompoundDesc != null && InterestCompoundDesc.trim().equals("2"))
    {
      interestFactor = DealCalcUtil.interestFactor(deal.getNetInterestRate(),365, InterestCompoundDesc);
    }
    else if(InterestCompoundDesc != null && InterestCompoundDesc.trim().equals("12"))
    {
      interestFactor = (deal.getNetInterestRate()/100)/365;
    }
    else
    {
      interestFactor = (deal.getNetInterestRate()/100)/365;
    }

    trace( "calc@calcPerDiemInterestNoRound: InterestCompoundDesc = " + InterestCompoundDesc + "; InterestFacotr = " +  interestFactor ) ;

    double theResult = deal.getTotalLoanBridgeAmount() * interestFactor;
    trace( "calc@calcPerDiemInterestNoRound: PerdiemInterestAmount = " + theResult );
    
    return theResult;
  }
  // Catherine, #1645 - Concentra - CR # 153 ------- end  
  
  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "perdiemInterestAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "IADNumberOfDays"));

    addTargetParam(new CalcParam(ClassId.DEAL, "interimInterestAmount"));

  }

}