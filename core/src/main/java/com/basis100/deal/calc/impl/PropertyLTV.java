package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

// Name: PropertyLTV
// Code: CALC-3
// Property loan to value ratio
// 06/26/00

public class PropertyLTV extends DealCalc
{

  public PropertyLTV()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 3";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
       if(input instanceof Deal)
       {
         Deal d = (Deal)input;
         Collection c = this.entityCache.getProperties ( d ) ;

         Iterator props = c.iterator();

         while(props.hasNext())
         {
            addTarget(props.next());
         }

       }
       else if(input instanceof Property)
       {
         addTarget(input);
       }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Property pt = (Property)target;

    try
    {
        Deal deal = (Deal) entityCache.find ( srk,
                    new DealPK( pt.getDealId(), pt.getCopyId()) );

        double dbNetLoanAmount = deal.getNetLoanAmount ();  // Net Loan Amount
        double dbLowestValue = pt.getLendingValue () ;// Lending Value respenstive the Lower value
        trace( "netLoanAmount = " + dbNetLoanAmount );
        trace( "lendingValue = " + dbLowestValue );

        // See CALC-74 for detail on Lowest Value.
        double result = 0.0;
        if ( dbLowestValue == 0 )
        {
          result = 0.0;
        }
        else if(deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS)
        {
        	result = Math.round(dbNetLoanAmount / (dbLowestValue + deal.getRefiImprovementAmount()) * 10000.0) / 100.00;
        }
        else
        {
          result = Math.round(dbNetLoanAmount / dbLowestValue * 10000.0) / 100.00;
        }

        result =  Math.round ( result * 100 ) / 100.00;

        pt.setLoanToValue (validate(result));
        pt.ejbStore ();

        return;
    }
    catch(Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

    pt.setLoanToValue(DEFAULT_FAILED_DOUBLE);
    pt.ejbStore();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "netLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealPurposeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "refiImprovementAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTY,  "lendingValue") );

    addTargetParam(new CalcParam(ClassId.PROPERTY, "loanToValue", 999.99));
  }

}
