package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.entity.*;

// Name: Combined Total Property Expenses
// Code: Calc 29
// 06/30/2000


public class CombinedTotalPropertyExpenses extends DealCalc {

  public CombinedTotalPropertyExpenses() throws Exception {
    super();
    initParams();
    this.calcNumber = "Calc 29";
  }

   public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        PropertyExpense pe = (PropertyExpense) input;
        Property p = (Property) entityCache.find (srk, 
                         new PropertyPK(pe.getPropertyId(), pe.getCopyId())) ;
        Deal d = (Deal) entityCache.find (srk, 
                         new DealPK( p.getDealId(), p.getCopyId()));
        addTarget ( d ) ;
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
      trace(this.getClass().getName());
      Deal deal = (Deal) target;
      try
      {
          double totalPropertyExpense= 0.0;

          Collection expenses = null ;

          expenses = this.entityCache.getPropertyExpenses (deal);

          Iterator itPe = expenses.iterator ();

          PropertyExpense propertyExpense = null;

          while ( itPe.hasNext () )
          {
              propertyExpense = (PropertyExpense)itPe.next ();

              if (propertyExpense.getPeIncludeInGDS() || propertyExpense.getPeIncludeInTDS()){
                 trace("propertyExpense.getPeIncludeInGDS() = " + propertyExpense.getPeIncludeInGDS() +
                              " or propertyExpense.getIncInclueInTDS() = " +  propertyExpense.getPeIncludeInTDS() +
                              "\nCalculate totalpropertyExpense...");
                 totalPropertyExpense +=  propertyExpense.getPropertyExpenseAmount () *
                 DealCalcUtil.annualize ( propertyExpense.getPropertyExpensePeriodId (), "PropertyExpensePeriod");
                 trace( " totalPropertyExpense = " + totalPropertyExpense );
              }else{
                 trace("propertyExpense.getPeIncludeInGDS() = " + propertyExpense.getPeIncludeInGDS() +
                              " or propertyExpense.getPeIncludeInTDS() = " +  propertyExpense.getPeIncludeInTDS() +
                              "\nNo need to calculate totalPropertyExpense");
              }
          } // --- All Property Expenses -----------
          totalPropertyExpense = Math.round ( totalPropertyExpense * 100 ) / 100.00 ; 
          deal.setCombinedTotalPropertyExp (validate(totalPropertyExpense) );
          deal.ejbStore ();
          return;
      }
      catch ( Exception e )
      {
          String msg = "Benign Failure in Calculation" + this.getClass().getName();
          msg += ":[Target]:" + target.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@Target: " + this.getDoCalcErrorDetail ( target );
          logger.debug (msg );
      }

    deal.setCombinedTotalPropertyExp (DEFAULT_FAILED_DOUBLE);
    deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpensePeriodId"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "peIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "peIncludeInTDS"));
    
    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalPropertyExp", T13D2));
  }


}
