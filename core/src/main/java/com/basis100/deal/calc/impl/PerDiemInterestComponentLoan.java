/*
 * @(#)PerDiemInterestComponentLoan.java July 02, 2008 Copyright (C) 2008
 * Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.picklist.BXResources;

/**
 * <p>
 * Title: PerDiemInterestComponentLoan
 * </p>
 * <p>
 * Description:Calculates the Amount of Interest that is to be charged for "each
 * day" in the Interest Adjustment period on a Loan component of a "component
 * eligible" deal.
 * </p>
 * Calculation Number- Calc-102.CLoan
 * @version 1.0 XS_11.14 02-July-2008 Initial Version
 * @version 1.1 25-Jun-2008 Formatted doCalc(..) method
 */
public class PerDiemInterestComponentLoan extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.14 02-July-2008 Initial Version
     */
    public PerDiemInterestComponentLoan() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 102.CLoan";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.14 02-July-2008 Initial Version
     * @version 1.2 08-Jul-2008 Added ComponentTypeId check
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOAN)
            {
                ComponentLoan componentLoan = (ComponentLoan) input;
                addTarget(componentLoan);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Deal deal = (Deal) input;
                    // Get all the components for the deal
                    Collection listOfComponents = entityCache.getComponents(
                            deal, srk);
                    Iterator componentsItr = listOfComponents.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is LOAN COMPONENT
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOAN)
                        {
                            // Retrives the componentLoan Object for the
                            // given
                            // componentId and copyId
                            ComponentLoan componentLoan = (ComponentLoan) entityCache
                                    .find(srk, new ComponentLoanPK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            addTarget(componentLoan);
                        }
                    }
                }
                else
                    if (classId == ClassId.COMPONENT)
                    {
                        Component component = (Component) input;
                        try {
                            if(component.getComponentTypeId()== Mc.COMPONENT_TYPE_LOAN){
                                // find componentLoan for the given componentId and
                                // copyId
                                ComponentLoan componentLoan = (ComponentLoan) entityCache
                                        .find(srk, new ComponentLoanPK(component
                                                .getComponentId(), component
                                                .getCopyId()));
                                addTarget(componentLoan);
                            }
                        } catch (Exception e) {
                            trace("component is being deleted componentId = " + component.getComponentId());
                        }
                    }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.14 02-July-2008 Initial Version
     * @version 1.1 XS_11.6 23-Jun-2008 Moved the per Diem Interest Amount logic
     *          to DealCalcUtil class.Added calculatePerDiemInterestAmount(..)
     *          method call.
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());

        try
        {
            ComponentLoan targetComponentLoan = (ComponentLoan) target;
            // Retrieve the component for the given component Loan
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLoan.getComponentId(),
                            targetComponentLoan.getCopyId()));
            // Retrieve the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));
            // Retrieve the LenderProfile object for the given lender profile
            LenderProfile lenderProfile = (LenderProfile) this.entityCache
                    .find(srk, new LenderProfilePK(deal.getLenderProfileId()));
            // Retrieve the Interest Compound Description from the picklist using
            // BXResources for the given Interest Compound Id
            String interestCompoundDesc = BXResources.getPickListDescription(
                    lenderProfile.getInstitutionProfileId(),
                    "INTERESTCOMPOUND", lenderProfile.getInterestCompoundId(),
                    srk.getLanguageId());
            // Retrieve the per Diem Interest Amount from the DealCalcUtil
            double perDiemInterestAmount = DealCalcUtil
                    .calculatePerDiemInterestAmount(interestCompoundDesc,
                            targetComponentLoan.getNetInterestRate(),
                            targetComponentLoan.getLoanAmount());
            // Rounding off to two decimals
            double finalPerDiemInterestAmount = (Math
                    .round(perDiemInterestAmount * 100d) / 100d);
            targetComponentLoan
                    .setPerDiemInterestAmount(validate(finalPerDiemInterestAmount));

            targetComponentLoan.ejbStore();
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.14 02-July-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "lenderProfileId"));

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "loanAmount"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "netInterestRate"));

        // Max perDiemInterestAmount 99999999999.99
        addTargetParam(new CalcParam(ClassId.COMPONENTLOAN,
                "perDiemInterestAmount", T13D2));
    }

}
