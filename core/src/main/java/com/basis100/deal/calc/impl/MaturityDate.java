package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.resources.*;
import MosSystem.Mc;

/**
 * <p>
 * Title: MaturityDate
 * </p>
 * <p>
 * Description:Calculates Maturity Date
 * </p>
 * @version 1.0 Initial Version <br>
 * @version 1.1 XS_11.17 Initial Version <br> - updated
 *          initParams(),getTarget(),doCalc()
 * @author MCM Impl Team <br>
 * @Date 04-Jul-2008 <br>
 *       Code Calc-14 <br>
 */
public class MaturityDate extends DealCalc
{
    final long MillSecsInWeek = 604800000 ;  //7 * 24 * 60 * 60 * 10000

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 Initial Version
     */
    public MaturityDate()throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 14";
    }


    public Collection getTargets()
    {

        return this.targets;
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.17 | updated Exception
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget ( input );
        }
        catch ( Exception e )
        {
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     * @version 1.1 XS_11.17 | Updated 
     *        -  Calc logic for Deal level product is an MCM or a line of credit
     *        -  Exception
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        Deal deal = ( Deal ) target ;
        try
        {
            //Calendar previousDate = Calendar.getInstance( Locale.getDefault () ) ;
            Calendar maturityDate = Calendar.getInstance( Locale.getDefault () ) ;
            Date IAD = deal.getInterimInterestAdjustmentDate ( ) ;

            int paymentFrequencyId = deal.getPaymentFrequencyId () ;
            trace( "PaymentFrequencyId = " + paymentFrequencyId );
            int term = deal.getActualPaymentTerm () ;
            trace( "ActualPaymentTerm = " + term );

            MtgProd mtgProd=deal.getMtgProd();

            String componentEligibleFlag = mtgProd.getComponentEligibleFlag();
            int underwriteAsTypeId = mtgProd.getUnderwriteAsTypeId();    

            //If the Deal level product is an MCM or a line of credit then the deal MaturityDate will be stored as NULL
            if( "Y".equals(componentEligibleFlag) || underwriteAsTypeId == Mc.COMPONENT_TYPE_LOC)
            {
                deal.setMaturityDate(null);
                deal.ejbStore();
                return;
            }
            // If the Deal level product is NOT an MCM or a line of credit then the deal MaturityDate is calculated as follows
            if ( IAD != null )
            {
                maturityDate.setTime (  IAD );
                int dayOfWeek = maturityDate.get (  Calendar.DAY_OF_WEEK  ); //IAD Date WeekDay

                Date IADdt = maturityDate.getTime(); //IAD date variable 

                //# 5521  - Maturity date calculation for NBC  - BEGIN
                // Added the following statement @ appropriate places as it not
                // to be done for NBC Maturity Date calculations.
                //maturityDate.add (Calendar.MONTH , term );  // IAD=1/31/2000, term = 1
                // ===> maturityDate = 2/28/2000
                //#5521 -  Maturity date calculation for NBC  - END

                // Check if Force Maturity Date to Monthly Equivalent -- By BILLY 27Nov2001
                // If true skip the following calc and set MaturityDate to IAD + term months
            if ((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.calc.forcematuritydatetomonthlyequ", "N")).equals("Y"))
                {
                    trace( "MaturityDate (Force to Monthly Equvalent = " + maturityDate.getTime () ) ;
                    maturityDate.add (Calendar.MONTH , term );  
                    deal.setMaturityDate ( maturityDate.getTime () );
                    deal.ejbStore ();
                    return;
                }

                //# 5521 - Maturity date calculation for NBC  - BEGIN
                double dNoOfPayments = 0.0d;
                double dTotalPaymentTerms = 0.0d;
                int intDays = 1;
                if ((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.calc.maturitydatemethod", "N")).equals("NBC"))
                {
                    if ( oneOf( paymentFrequencyId,
                            Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                    {
            			//dNoOfPayments = 365/14.0;
            			//ticket # NBC202556: 
            			//if Payment Frequency is biweekly or biweekly accelerated, then  Number of payments per year = round(365/14,0)
            			//else if Payment Frequency is weekly or weekly accelerated, then Number of payments per year = round(365/7,0)

            			dNoOfPayments = Math.round(365/14.0);
                        dTotalPaymentTerms = (term / 12.0d) * dNoOfPayments;
                        intDays = 2;
                    }
                    else if ( oneOf( paymentFrequencyId,
                            Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY ) )
                    {
            		//dNoOfPayments = 365/7.0;
            		dNoOfPayments = Math.round(365/7.0);
                        dTotalPaymentTerms = (term / 12.0d) * dNoOfPayments;		
                    }
                    dTotalPaymentTerms = Math.round(dTotalPaymentTerms);
                    maturityDate.add(Calendar.DATE, ((int)dTotalPaymentTerms) * 7 * intDays);
                    if ( oneOf( paymentFrequencyId,
                            Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY,
                            Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY) )
                    {
                        deal.setMaturityDate ( maturityDate.getTime () );
                        deal.ejbStore ();
                        return;
                    }
                }
                //  # 5521 - Maturity date calculation for NBC  - END

                maturityDate.add (Calendar.MONTH , term );
                deal.setMaturityDate ( maturityDate.getTime () );
                int dayOfWeek2 = maturityDate.get ( Calendar.DAY_OF_WEEK    ) ;//IAD + Term date day

                // (Accelerated) Weekly, BiWeekly, Make sure same weekday.
                if ( oneOf( paymentFrequencyId,
                        Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY ,
                        Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                {
                    int dayDifferent = dayOfWeek - dayOfWeek2; // difference between the weekdays
                    if ( dayOfWeek != dayOfWeek2 ) // Making the day same for both IAD & MaturityDate
                    {
                        maturityDate.add(Calendar.DATE, dayDifferent);
                    }
                    if(dayDifferent>0)
                    {
                        maturityDate.add(Calendar.DATE , - 7 );
                    }

                    /*int dayDifferent = dayOfWeek - dayOfWeek2;
                  if ( dayDifferent < 0 )
                        dayDifferent += 7;

                  if ( dayOfWeek != dayOfWeek2 )
                     maturityDate.add( Calendar.DATE , - 7 + dayDifferent );
                     */
                }
                trace( "MaturityDate = " + maturityDate.getTime () ) ; 
                // If Bi-Weekly, make sure Weeks between IAD and CalculatedMaturidyDate is even
                if ( oneOf( paymentFrequencyId,
                        Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                {
                    double weekDiff = (((maturityDate.getTime().getTime() - IADdt.getTime())) * (1.0d))/ MillSecsInWeek;
                    if(Math.round(weekDiff) % 2 != 0)
                    {
                        maturityDate.add( Calendar.DATE , - 7 );
                    }
                    /* long millSecs = maturityDate.getTime ().getTime ()
                                  - previousDate.getTime().getTime () ;

                if  ( millSecs / MillSecsInWeek  % 2  != 0  ) // Weeks difference is even
                    maturityDate.add( Calendar.DATE , - 7 );
                     */
                }
                trace( "MaturityDate = " + maturityDate.getTime () ) ;
                deal.setMaturityDate ( maturityDate.getTime () );
                deal.ejbStore ();
            }
            return;
        }
        catch ( Exception e )
        {
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

        deal.setMaturityDate(DEFAULT_FAILED_DATE);
        deal.ejbStore();
    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 - Added mtgProdId as input parameter.
     */
    private void initParams()throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();


        addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.DEAL, "interimInterestAdjustmentDate"));
        addInputParam(new CalcParam(ClassId.DEAL , "actualPaymentTerm"));

        /*****************MCM Impl Team STARTS XS_11.17*******************/
        addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
        /*****************MCM Impl Team ENDS XS_11.17*******************/

        addTargetParam(new CalcParam(ClassId.DEAL, "maturityDate"));
    }


}
