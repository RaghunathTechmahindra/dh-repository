package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.TimeZone;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.resources.PropertiesCache;

//Name:  First Payment Date
//Code: Calc 13
//07/08/2000
/**
 * <p>
 * Title: FirstPaymentDate
 * </p>
 * <p>
 * Description:Calculates First Payment Date
 * </p>
 * @version 1.0 Initial Version <br>
 * @version 1.1 XS_11.17 Initial Version <br> - updated
 *          initParams(),getTarget(),doCalc()
 * @author MCM Impl Team <br>
 * @Date 04-Jul-2008 <br>
 *       Code Calc-13 <br>
 */
public class FirstPaymentDate extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 Initial Version
     */
    public FirstPaymentDate()throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 13";
    }


    public Collection getTargets()
    {
        return this.targets;
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.17 | updated Exception
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget(input);
        }
        catch ( Exception e)
        {
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     * @version 1.1 XS_11.17 | Updated 
     *        - Calc logic for Deal level product is a line of credit
     *        - Exception
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        Deal deal = ( Deal ) target ;
        MtgProd mtgProd=deal.getMtgProd();

        try
        {
            int period=0;
            int pf = deal.getPaymentFrequencyId();
            logger.debug("FirstpaymentDate(dealid = " + deal.getDealId() + ") calc: PaymentFrequencyId = " + pf);

            if ( pf == Mc.PAY_FREQ_SEMIMONTHLY )
                period = 15 ;
            else if ( pf == Mc.PAY_FREQ_BIWEEKLY || pf == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY )
                period = 14 ;
            else if ( pf == Mc.PAY_FREQ_WEEKLY || pf == Mc.PAY_FREQ_ACCELERATED_WEEKLY )
                period = 7 ;
            else if ( pf == Mc.PAY_FREQ_MONTHLY )
                period = 30 ; // 30 days
            logger.debug("FirstpaymentDate(dealid = " + deal.getDealId() + ") calc: Period = " + period);
            
            Calendar firstPaymentDate = Calendar.getInstance (TimeZone.getDefault (), Locale.getDefault ());

            Date ecDate = null;
            
            int UnderwriteAsTypeId = mtgProd.getUnderwriteAsTypeId();    
            
            // New requirement for Force IAD to 1st of Month if property set to Y  -- By Billy 27Nov2001
            // This property must be same among institutions in a instance and in DB - FXP21309
            if ((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
            		"com.basis100.calc.forceiadtofirstofmonth", "N")).equals("Y") && UnderwriteAsTypeId != Mc.COMPONENT_TYPE_LOC)
            {
                // Calc by IAD
                //ecDate = deal.getInterimInterestAdjustmentDate();
            	//when this calc is run IAD hasn't necessarily been recalced yet.  FXP33105.
            	ecDate = findIAD(deal.getEstimatedClosingDate(), deal.getInstitutionProfileId()
            				, deal.getPaymentFrequencyId());
                logger.debug("com.basis100.calc.forceiadtofirstofmonth is Y " );
            }
            else
            {
/*            	// FXP31827 - 10 Oct 2011
            	// Ensure First Payment Date is never before the Closing Date
                ecDate = deal.getInterimInterestAdjustmentDate();

                if (ecDate.before(deal.getEstimatedClosingDate())) 
                	ecDate = deal.getEstimatedClosingDate ();
*/
            	//FXP33105 - according to FSD
           		ecDate = deal.getEstimatedClosingDate();
            	
                logger.debug("com.basis100.calc.forceiadtofirstofmonth is N, "); 
            }
    		logger.debug(" dcDate will be deal EstimatedClosingDate: " + ecDate);

          if ( ecDate != null && ( period > 0 ) )
            {
        	  	logger.debug("ecDate!=null and period > 0 ");
                firstPaymentDate.setTime (  ecDate );
                if ( period == 30 )
                    firstPaymentDate.add ( Calendar.MONTH ,  1 );
                else
                    firstPaymentDate.add ( Calendar.DAY_OF_MONTH ,  period );  // = EstimatedClosingDate + period
                
                deal.setFirstPaymentDate ( firstPaymentDate.getTime ());
                deal.ejbStore ();
                return;
            }
            else
            {
                throw new Exception("Required Input = null -> deal.getEstimatedClosingDate()/getInterimInterestAdjustmentDate(). or PaymentFrequencey error ");
            }
        }
        catch ( Exception e )
        {
          
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
        //if we don't calc and return earlier, store default, whatever that is.
        deal.setFirstPaymentDate(DEFAULT_FAILED_DATE);
        deal.ejbStore();
    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }
    
    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 - Added mtgProdId as input parameter.
     */
    private void initParams()throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));
        
        /*****************MCM Impl Team STARTS XS_11.17*******************/
        addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
        /*****************MCM Impl Team ENDS XS_11.17*******************/
        
// FXP20744, deleted
// This property must be same among institutions in a instance and in DB - FXP21309
      if ((PropertiesCache.getInstance().getProperty(-1,
    		  "com.basis100.calc.forceiadtofirstofmonth", "N")).equals("Y"))
      {
        // Calc by IAD if force IAD to First of Month
        addInputParam(new CalcParam(ClassId.DEAL, "interimInterestAdjustmentDate"));
      }

        addTargetParam(new CalcParam(ClassId.DEAL, "firstPaymentDate"));
    }

    private Date findIAD(Date estCloseDate, int instId, int paymentFreq)
    {
    	// if Estimated Closing date not set ==> Exception
    	Date d = estCloseDate;//deal.getEstimatedClosingDate();

    	// Get the day to Back date from Properties
    	int theBackDay = 1;
    	theBackDay = Integer.parseInt(PropertiesCache.getInstance().getProperty(instId,
    		 "com.basis100.calc.forceiadtofirstofmonth.daytobackdate","1"));

    	Calendar firstDate = Calendar.getInstance (TimeZone.getDefault (), Locale.getDefault ());
    	firstDate.setTime(d);

    	int pf = paymentFreq;
    	if ((PropertiesCache.getInstance().getProperty(instId,"com.basis100.calc.forceiadtofirstofmonth.specialforxd","N")).equals("N") || 
    			 pf == Mc.PAY_FREQ_MONTHLY)
    	{
    		// Set the result Day to the first day of month
    		firstDate.set(Calendar.DAY_OF_MONTH, 1);
    		// Get day field from Est closing date
    		int estDate = d.getDate();
    		if (estDate > theBackDay)
    		{
    			// Set IAD to 1st day of next Month of Est. Closing Date
    			firstDate.add(Calendar.MONTH, 1);
    		}
    	}
    	else
    	{
    		// Special handle for Xceed to set
    		int daysFromFri = Calendar.FRIDAY - firstDate.get(Calendar.DAY_OF_WEEK);
    		trace("# of Days from Closing Friday = " + daysFromFri);
    		firstDate.add(Calendar.DAY_OF_MONTH, daysFromFri);
    	}
    	return firstDate.getTime();
    }
    
}
