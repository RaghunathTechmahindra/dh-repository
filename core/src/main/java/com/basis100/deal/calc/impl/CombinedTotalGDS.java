package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name: CombinedTotalGDS
//Code: Calc 5
//6/27/2000

public class CombinedTotalGDS extends DealCalc
{

  public CombinedTotalGDS() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 5";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                               new BorrowerPK(i.getBorrowerId(), 
                                              i.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                               new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Income ==> Deal
      if(input instanceof PropertyExpense)
      {
        PropertyExpense pe = (PropertyExpense)input;
        Property p = (Property)entityCache.find(srk, 
                             new PropertyPK(pe.getPropertyId(), pe.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                            new DealPK(p.getDealId(), p.getCopyId()));
        addTarget(d);
        return;
      } // PropertyExpense ==> Deal
      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                             new BorrowerPK(l.getBorrowerId(), 
                                            l.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                             new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Liability ==> Deal

      if(input instanceof Deal)
      {
        addTarget(input);
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;

    try
    {
      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));
      double dbGDSRatio = 0;

      String rateCode = pricingProfile.getRateCode();
      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          deal.setCombinedGDS(0);
          deal.ejbStore();
          return;
        }
      }

      double dbTotalAnnualIncome = 0;
      double dbTotalGDSExpenses = 0;

      Collection incomes = this.entityCache.getIncomes(deal);
      Iterator itIn = incomes.iterator();

      while(itIn.hasNext()) // Loop through all the ** incomes
      {
        Income income = (Income)itIn.next();
        // if Included in GDS
        //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
        //--> By Billy 02Nov2004
        if(income.getIncIncludeInGDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
        {
          dbTotalAnnualIncome = dbTotalAnnualIncome + income.getIncomeAmount() *
            DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") * income.getIncPercentInGDS() / 100.00;
        }
        //=============================================================================
        if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
        {
          dbTotalGDSExpenses = dbTotalGDSExpenses - income.getIncomeAmount() *
            DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") * income.getIncPercentInGDS() / 100.00;
        }
        trace("IncomePeriodId = " + income.getIncomePeriodId());
        trace(" IncomePeriod = " +
          DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod"));
      }
      trace("totalAnnualIncome = " + dbTotalAnnualIncome);
      trace("totalGDSExpenses = " + dbTotalGDSExpenses);
      // ------------------Work with Liabilities---------------------
      Collection liabilities = this.entityCache.getLiabilities(deal);
      Iterator itLi = liabilities.iterator();

      while(itLi.hasNext()) //Loop through all the ** liabilities
      {
        Liability liability = (Liability)itLi.next();
        // Include in GST and Libility Payoff Type is Blank, ( id=0 )
        if(liability.getIncludeInGDS() &&
          liability.getLiabilityPayOffTypeId() == 0)
        {
          // "Credit Card", "Unsecured Line of Credit" "Secured Line of Credit"
          // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
          // -------------------------------------------------------------------
          // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
          //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
          if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
          {
            dbTotalGDSExpenses += liability.getLiabilityAmount() *
              liability.getPercentInGDS() / 100.00 * 12;
          }
          else

          // Liab = "Closing Costs", type = "High ratio"
          if(liability.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
          {
            dbTotalGDSExpenses += liability.getLiabilityAmount();
          }
          else
          {
            dbTotalGDSExpenses += liability.getLiabilityMonthlyPayment() * 12 *
              liability.getPercentInGDS() / 100.00;
          }
        }
        trace(" totalGDSExpenses = " + dbTotalGDSExpenses);
      }

      Collection pe = this.entityCache.getPropertyExpenses(deal);
      Iterator itPe = pe.iterator();

      // ----- Get the Property Expenses -----------
      while(itPe.hasNext())
      {
        PropertyExpense propertyExpense = (PropertyExpense)itPe.next();

        if(propertyExpense.getPeIncludeInGDS())
        {
          dbTotalGDSExpenses = dbTotalGDSExpenses + propertyExpense.getPropertyExpenseAmount() *
            DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(),
            "PropertyExpensePeriod") * propertyExpense.getPePercentInGDS() / 100.00;

          trace("PropertyExpensePeriodId = " + propertyExpense.getPropertyExpensePeriodId());
          trace("PropertyExpensePeriod = " +
            DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(), "PropertyExpensePeriod"));
        }
      }

      /*#DG538 allow accelerated precision
      dbTotalGDSExpenses +=
        (deal.getPandiPaymentAmount() + deal.getAdditionalPrincipal())
        * DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");*/
      dbTotalGDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, false);

      trace("PandI = " + deal.getPandiPaymentAmount());
      trace("AdditionalPrincipal = " + deal.getAdditionalPrincipal());
      trace("deal.paymentFrequencyId = " + deal.getPaymentFrequencyId());
      trace("dbTotalGDSExpenses = " + dbTotalGDSExpenses);

      if(dbTotalAnnualIncome != 0)
      {
        dbGDSRatio = dbTotalGDSExpenses / dbTotalAnnualIncome * 100;
        dbGDSRatio = Math.round(dbGDSRatio * 100.00) / 100.00;
      }
      else
      {
        dbGDSRatio = this.getMaximumCalculatedValue();

      }
      deal.setCombinedGDS(validate(dbGDSRatio));
      deal.ejbStore();
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(target);
      logger.debug(msg);
    }

    deal.setCombinedGDS(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    //#DG538
    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIPaymentAmountMonthly"));
    //#DG538 end

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedGDS", 999.99));
  }

}
