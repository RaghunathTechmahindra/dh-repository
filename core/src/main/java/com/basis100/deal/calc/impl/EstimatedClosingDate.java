package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name:  Estimated Closing Date
//Code:  Calc 95

public class EstimatedClosingDate extends DealCalc {

  public EstimatedClosingDate () throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 95";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
      { addTarget ( input);
      }
    catch ( Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }      

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = ( Deal) target ;
     try
     {
        if ( deal.getEstimatedClosingDate () == null )
        {
            Calendar applicationDate = Calendar.getInstance () ;

            if ( deal.getApplicationDate () == null )
                throw ( new Exception("[Application Date == null Error]" ) ) ;

            applicationDate.setTime ( deal.getApplicationDate () );
            applicationDate.add ( Calendar.DAY_OF_MONTH  ,  90 );

            deal.setEstimatedClosingDate ( applicationDate.getTime () );
            deal.ejbStore ();
        }
        return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage() ;
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     deal.setEstimatedClosingDate ( DEFAULT_FAILED_DATE ) ;
     deal.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL , "applicationDate" ));

    addTargetParam(new CalcParam(ClassId.DEAL , "estimatedClosingDate" ));
  }


}
