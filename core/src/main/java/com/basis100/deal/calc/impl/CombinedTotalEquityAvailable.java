package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Combined Total Equity Available
//Code: CALC 33
//7/4/2000

public class CombinedTotalEquityAvailable extends DealCalc
{

  public CombinedTotalEquityAvailable() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 33";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      Property  p = (Property) input ;
      Deal d = (Deal) entityCache.find (srk, 
                        new DealPK( p.getDealId(), p.getCopyId()) );
      addTarget ( d ) ;
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
      trace(this.getClass().getName());

      Deal deal = (Deal) target;

      try
      {
          Collection properties = this.entityCache.getProperties (deal);
          Iterator itPr = properties.iterator ();

          double CombinedEquityAvailable = 0;
          while ( itPr.hasNext() )
          {
              Property property = ( Property ) itPr.next ();
              CombinedEquityAvailable += property.getEquityAvailable ();
              trace( "CombinedEquityAvailable = " + CombinedEquityAvailable );
          }
          CombinedEquityAvailable = Math.round (CombinedEquityAvailable * 100 ) / 100.00; 
          deal.setCombinedTotalEquityAvailable ( validate(CombinedEquityAvailable) );
          deal.ejbStore ();
          return;
      }
      catch ( Exception e )
      {
          String msg = "Benign Failure in Calculation" + this.getClass().getName();
          msg += ":[Target]:" + target.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@Target: " + this.getDoCalcErrorDetail ( target );          
          logger.debug (msg );
          
      }

      deal.setCombinedTotalEquityAvailable (DEFAULT_FAILED_DOUBLE);
      deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTY ,"equityAvailable"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalEquityAvailable", T13D2));
  }                                 
}
