package com.basis100.deal.calc;

import com.filogix.express.core.ExpressException;

public class DealCalcException extends ExpressException {

    /**
     * Constructs a <code>DealCalcException</code> with no specified detail
     * message.
     */

    public DealCalcException() {
        super();
    }

    public DealCalcException(String message) {
        super(message);
    }

    public DealCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public DealCalcException(Throwable cause) {
        super(cause);
    }

}