package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

//Name:  LotSize
//Code:  Calc 67
//07/04/2000

public class LotSize extends DealCalc
{


  public LotSize()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 67";
  }

  

  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    { addTarget ( input);
    }
    catch ( Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());
     Property property = (Property) target;

     try
     {
        int lotSize = property.getLotSizeFrontage () * property.getLotSizeDepth () ;

        property.setLotSize ( validate( lotSize ) );
        property.ejbStore ();
        return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     property.setLotSize(DEFAULT_FAILED_INT);
     property.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTY, "lotSizeFrontage"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "lotSizeDepth"));

    addTargetParam(new CalcParam(ClassId.PROPERTY, "lotSize", 99999999 ));
  }



} 
