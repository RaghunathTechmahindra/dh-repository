package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.BusinessCalendar;
import com.basis100.resources.PropertiesCache;

public class CommitmentExpiryDate extends DealCalc
{


  public CommitmentExpiryDate() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 30";
  }


  public Collection getTargets()
  {

    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
      try
      {
          addTarget ( input ) ;
      }
      catch(Exception e)
      {
          String msg = "Failed to get Targets for: " + this.getClass().getName();
          msg += " :[Input]: " + input.getClass ().getName () ;
          msg += "  Reason: " + e.getMessage();
          msg += " @input: " + this.getDoCalcErrorDetail ( input );
          logger.error (msg );
          throw new TargetNotFoundException (msg);
      }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;
    try
    {
      boolean notAutoCalcCommitExpiryDateFlag = "N".equalsIgnoreCase(deal.getAutoCalcCommitExpiryDate());
      if (notAutoCalcCommitExpiryDateFlag)
      {
        trace("AutoCalcCommitExpiryDate is Off - No changing CommitmentExpirationDate = " 
              + deal.getCommitmentExpirationDate());
        return;
      }
        
      Date calcCommitExpiryDate = getCalcCommitmentExpiryDate(deal);
      deal.setCommitmentExpirationDate(calcCommitExpiryDate);        
      trace("Result CommitmentExpirationDate = " + calcCommitExpiryDate);
      deal.ejbStore();
      return;
    }
    catch(Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

    deal.setCommitmentExpirationDate(DEFAULT_FAILED_DATE);
    deal.ejbStore();
  }

  public Date getCalcCommitmentExpiryDate(Deal deal)
    throws Exception
  {
    if(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.calc.commitmentexpirydate.usecommitmentterm", "N").equals("N"))
    {
       trace("Set ExpiryDate = EstClosingDate");
       return deal.getEstimatedClosingDate();
    }
    else
    {
      Calendar expiryDate = Calendar.getInstance();
      Date issueDate = deal.getCommitmentIssueDate();
      Date applicationDate = deal.getApplicationDate();
      if (issueDate == null)
      {
        trace("Set ExpiryDate = ApplicationDate + MtgProd.CommitmentTerm");
        expiryDate.setTime(applicationDate);
      }
      else
      {
        trace("Set ExpiryDate = CommitmentIssueDate + MtgProd.CommitmentTerm");
        expiryDate.setTime(issueDate);            
      }
      int hour = expiryDate.get(Calendar.HOUR_OF_DAY);

      int cTermInDay = 0; // default term = 0
      try{
        cTermInDay = Integer.parseInt(deal.getMtgProd().getCommitmentTerm());
      }
      catch(Exception e)
      {
        trace("Fail to get MtgProd.CommitmentTerm value -- Default Term to 0 !!");
      }
      expiryDate.add(Calendar.DAY_OF_MONTH, cTermInDay);
      
      //===================== ALERT !!! =============================================================
      //As we don't care the time of the date the quickest solution is to set the
      // Time to noon 12pm (Don't care of the time zone, it's dirty fix but may only work in TimeZone
      //  +/- 3 hrs.  It should be fixed in the future !!!!!
      //    -- By BILLY
      expiryDate.set(Calendar.HOUR_OF_DAY, 12);
      // ============================================================================================


      //Addition : if result date is non-BusinessDay ==> Back to the previous BusinessDay
      //    -- By Billy 12April2002
      // Add Business Days
      BusinessCalendar bCal = new BusinessCalendar(srk);
      // Get timezone of the branch of the Deal
      BranchProfile theBranch = new BranchProfile(srk, deal.getBranchProfileId());
      // It is tricky to back 1 min and forward 1 min to make the date become the last businessday
      Date resultDate = bCal.calcBusDate(expiryDate.getTime(), theBranch.getTimeZoneEntryId(), -1);
      resultDate = bCal.calcBusDate(resultDate, theBranch.getTimeZoneEntryId(), 1);

      //--> Bug fix to preserve the Hour same as Issued Date
      //--> By Billy 09June2003
      Calendar resultC = Calendar.getInstance();
      resultC.setTime(resultDate);
      resultC.set(Calendar.HOUR_OF_DAY, hour);
      resultDate = resultC.getTime();
      //====================================================
      return resultDate;
    }
  }
  
  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));
    //Spec. changes -- By BILLY 10April2002
    addInputParam(new CalcParam(ClassId.DEAL, "commitmentIssueDate"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    //CR03 - Express 3.2 Gen - Midori Dec 4, 2006
    addInputParam(new CalcParam(ClassId.DEAL, "applicationDate"));
    addInputParam(new CalcParam(ClassId.DEAL, "autoCalcCommitExpiryDate"));
    addInputParam(new CalcParam(ClassId.DEAL, "commitmentExpirationDate"));
    addTargetParam(new CalcParam(ClassId.DEAL, "commitmentExpirationDate"));
    //=================================================================
  }
}
