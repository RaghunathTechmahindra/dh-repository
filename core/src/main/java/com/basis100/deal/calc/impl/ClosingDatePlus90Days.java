package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

//Name: Closing Data Plus 90 Days
//Code: Calc 91
//07/17/2000

public class ClosingDatePlus90Days extends DealCalc {

  public ClosingDatePlus90Days  () throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 91";
  }


  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget ( input );
    }
    catch ( Exception e )
    {
      String msg = "Failed to get Targets for: " + this.getClass().getName();
      msg += " :[Input]: " + input.getClass ().getName () ;
      logger.error(msg);
      msg = "  Reason: " + e.getMessage();
      msg += " @input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Deal deal = ( Deal ) target ;

     try
     {
        Calendar closingDatePlus90  = null ;

        Date ACD = deal.getActualClosingDate () ;

        if ( ACD != null )
        {
          closingDatePlus90  = Calendar.getInstance ();
          closingDatePlus90.setTime ( ACD ) ;
          closingDatePlus90.add ( Calendar.DAY_OF_MONTH , 90 );
        }
        else
        {
          Date ESD = deal.getEstimatedClosingDate ()  ;
          if ( ESD != null )
          {
            closingDatePlus90  = Calendar.getInstance ();
            closingDatePlus90.setTime ( ESD );
            closingDatePlus90.add ( Calendar.DAY_OF_MONTH , 90 );
          }
        }

        if ( closingDatePlus90  != null )
        {
           deal.setClosingDatePlus90Days ( closingDatePlus90.getTime () );
           deal.ejbStore();
        }
        return;
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );        
        logger.debug (msg );
        
     }

     deal.setClosingDatePlus90Days ( this.DEFAULT_FAILED_DATE );
     deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "actualClosingDate"));
    addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));

    addTargetParam(new CalcParam(ClassId.DEAL, "closingDatePlus90Days"));
  }

}
