package com.basis100.deal.calc.impl;

/**
 * 15/Sep/2006 DVG #DG508 #4675  LTV for a deal is 0 on UWsheet
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import config.Config;

/**
 *
 * <p>
 * Title: BalanceRemainingAtEndOfTerm.java
 * </p>
 *
 * <p>
 * Description: BalanceRemainingAtEndOfTerm.java
 * </p>
 *
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 *
 * <p>
 * Company: Filogix Inc.
 * </p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.3 <br>
 * Date: 11/8/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: <br>
 * 	Fixed calculation to follow specs, defect 1454
 */
public class BalanceRemainingAtEndOfTerm extends DealCalc {
	String errMsg;

	/**
	 *
	 * A constructor for this class.
	 *
	 * @throws Exception
	 */
	public BalanceRemainingAtEndOfTerm() throws Exception {
		super();
		initParams();
		this.calcNumber = "Calc 111";
		//String errMsg = "CALC Engine: EXCEPTION: BalanceRemainingAtEndOfTerm.java failed during calcualtion of balance remaining at the end of term. ";
	}

	/**
	 * method: getTargets description: returns the Collection object which
	 * contains all the defined target for the CashBack Class
	 *
	 * @throws -
	 */
	public Collection getTargets() {
		return this.targets;
	}

	/**
	 * method: getTarget description: Method add the targets into the Calc Class
	 *
	 * @param: Object, CaclMonitor
	 * @throws TargetNotFoundException
	 */
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
		try {
			addTarget(input);
		} catch (Exception e) {
			String msg = "Fail to get Targets" + this.getClass().getName();
			msg += ":[Input]:" + input.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@input: " + this.getDoCalcErrorDetail(input);
			logger.error(msg);
			throw new TargetNotFoundException(msg);
		}
	}

	/**
	 * method: hasInput description: Checks whether the required input exists or
	 * not.
	 *
	 * @param CalcParam
	 * @return: Boolean
	 */
	public boolean hasInput(CalcParam input) {
		return this.inputParam.contains(input);
	}

	/**
	 * method: doCalc description: Performs the required business calculations.
	 *
	 * @param: Object
	 * @throws: Exception
	 */
	public void doCalc(Object target) throws Exception {
		trace(this.getClass().getName());
		Deal deal = (Deal) target;

		double balanceAmount = 0.0;
		int interestCompoundingId = -1;
    /*#DG508 no need
		PropertiesCache.addPropertiesFile(Config
				.getProperty("SYS_PROPERTIES_LOCATION")
				+ "mossys.properties");
		ResourceManager.init(Config.getProperty("SYS_PROPERTIES_LOCATION"),
				PropertiesCache.getInstance().getProperty(
						"com.basis100.resource.connectionpoolname"));
		srk = new SessionResourceKit("BalanceRemainingAtEndOfTerm");
    */
		String decimalYear = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
				"com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
		

		try {
			int repaymentTypeId = deal.getRepaymentTypeId();
			int amortizationTerm = deal.getAmortizationTerm();
			int mtgProdId = deal.getMtgProdId();
			int paymentFrequencyId = deal.getPaymentFrequencyId();
			double totalLoanAmount = deal.getTotalLoanAmount();
			double actualPaymentTerm = deal.getActualPaymentTerm();
			double netInterestRate = deal.getNetInterestRate();
			double pAndIpaymentAmount = deal.getPandiPaymentAmount();

			if (repaymentTypeId == 2 || repaymentTypeId == 3) {
				balanceAmount = totalLoanAmount;
				deal.setBalanceRemainingAtEndOfTerm(balanceAmount);
			} else if (actualPaymentTerm == amortizationTerm) {
				balanceAmount = 0.0;
				deal.setBalanceRemainingAtEndOfTerm(balanceAmount);
			} else {
				try {
					interestCompoundingId = (new MtgProd(srk, null, mtgProdId))
							.getInterestCompoundingId();
				} catch (Exception e) {
					srk.getSysLogger().warning(
							this.getClass(),
							errMsg
									+ (e.getMessage() != null ? "\n"
											+ e.getMessage() : ""));
				}
				if (interestCompoundingId == -1) {
					throw new Exception();
				}
						
				double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
				double paymentsPerYearForInterestFactor = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId, mDaysInYear);

				// using 0 as the default lang Id - Need to be refactored
				String compound =
					BXResources.
					getPickListDescription(deal.getInstitutionProfileId(),
										   "INTERESTCOMPOUND",
										   Integer.toString(interestCompoundingId), 0);
				double interestFactor = DealCalcUtil.interestFactor(netInterestRate, paymentsPerYearForInterestFactor, compound);

				// calculate total payments for amortization and truncate to an integer
				int paymentPerYearForAmortization = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId);
				double totalPayments = actualPaymentTerm / 12  * paymentPerYearForAmortization;
				
				int totalPaymentsTruncated = (int) totalPayments;
				  
				double interest, principal;
				balanceAmount = totalLoanAmount;

				for (int i = 1; i <=  totalPaymentsTruncated; i++) {
				  // Round to nearest hundredth
				  interest = DealCalcUtil.roundToNearestHundredth(balanceAmount * interestFactor);
				  principal = DealCalcUtil.roundToNearestHundredth(pAndIpaymentAmount - interest);
				  balanceAmount = DealCalcUtil.roundToNearestHundredth(balanceAmount - principal);
				  
				}
				
				if (balanceAmount < 0.0) {
					balanceAmount = 0.0;
				}				
				deal.setBalanceRemainingAtEndOfTerm(balanceAmount);
			}
			deal.ejbStore();
			return;
			
		} catch (NumberFormatException e) {
			String msg = "Number Format Exception in Calculation"
					+ this.getClass().getName();
			msg += ":[Target]:" + target.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@Target: " + this.getDoCalcErrorDetail(target);
			logger.debug(msg);
		} catch (Exception e) {
			String msg = "Benign Failure in Calculation"
					+ this.getClass().getName();
			msg += ":[Target]:" + target.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@Target: " + this.getDoCalcErrorDetail(target);
			logger.debug(msg);
		}
		deal.setBalanceRemainingAtEndOfTerm(this.DEFAULT_FAILED_DOUBLE);
		deal.ejbStore();
	}

	/**
	 *
	 * initParams Method description: Initializes all the required input and
	 * output(targets) parameters
	 *
	 * @throws ParamTypeException
	 */
	private void initParams() throws ParamTypeException {
		targetParam = new ArrayList();
		inputParam = new ArrayList();
		inputClassTypes = new HashSet();
		
		addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
		addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "actualPaymentTerm"));
		addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
		addInputParam(new CalcParam(ClassId.DEAL, "interestCompoundId"));
		addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.DEAL, "pAndIpaymentAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
		addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
		

		addTargetParam(new CalcParam(ClassId.DEAL,
				"balanceRemainingAtEndOfTerm"));
	}

}
