package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

public class TotalPaymentAmount extends DealCalc
{


  public TotalPaymentAmount()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 44";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        if ( input instanceof Deal )
             addTarget(input);
    }
    catch ( Exception e )
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal d = (Deal)target;
    try
    {
      double val = d.getPandiPaymentAmount() +
                    d.getEscrowPaymentAmount() +
                     d.getAdditionalPrincipal();
      val = Math.round ( val * 100 ) /100.00 ;                                              
      d.setTotalPaymentAmount(validate(val));
      d.ejbStore();
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

    d.setTotalPaymentAmount(DEFAULT_FAILED_DOUBLE);
    d.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "escrowPaymentAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));

    addTargetParam(new CalcParam(ClassId.DEAL, "totalPaymentAmount", T13D2));
  }



}
