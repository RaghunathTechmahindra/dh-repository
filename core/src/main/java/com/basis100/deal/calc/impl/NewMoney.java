package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.* ;
import MosSystem.Mc;

public class NewMoney extends DealCalc {

  public NewMoney() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 92";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {
        addTarget( input ) ;
     }
     catch ( Exception e )
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }


  public void doCalc(Object target) throws Exception
  {
      trace(this.getClass().getName());

      Deal d = (Deal) target;

      try
      {
          //New Money = deal.Total Loan Amount - deal.Existing Loan Amount
          double newMoney =
              d.getTotalLoanAmount () -  d.getExistingLoanAmount () ;
          newMoney = Math.round ( newMoney * 100) / 100.00 ;
          d.setNewMoney ( validate(newMoney) );
          d.ejbStore ();
          return ;
      }
      catch(Exception e)
      {
          String msg = "Benign Failure in Calculation" + this.getClass().getName();
          msg += ":[Target]:" + target.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@Target: " + this.getDoCalcErrorDetail ( target );
          logger.debug (msg );
      }

      d.setNewMoney ( this.DEFAULT_FAILED_DOUBLE );
      d.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "existingLoanAmount"));

    addTargetParam(new CalcParam(ClassId.DEAL, "newMoney", T13D2));
  }

}