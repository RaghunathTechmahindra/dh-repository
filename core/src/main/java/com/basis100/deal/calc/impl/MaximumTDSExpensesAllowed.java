package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name: maximumTDSExpensesAllowed
//Code: Calc 84
//07/18/2000

public class MaximumTDSExpensesAllowed extends DealCalc {

  public MaximumTDSExpensesAllowed() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 84";
  }


  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
       if(input instanceof Income)
       {
         Income i = (Income) input;
         Borrower b = (Borrower) entityCache.find(srk, 
                               new BorrowerPK(i.getBorrowerId (), 
                                              i.getCopyId()));
         Deal d = (Deal) entityCache.find (srk, 
                               new DealPK( b.getDealId(), b.getCopyId ()));
         addTarget (d);
         return;
       }

       if ( input instanceof Deal )
        addTarget(input);
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }

  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Deal deal = ( Deal ) target;

    try
    {

      //----- Total Combined Income ------------------------
      double totalCombinedIncome = 0.0;

      Collection incomes = this.entityCache.getIncomes (deal) ;
      Iterator itIN = incomes.iterator () ;
      Income income = null;

      while ( itIN.hasNext () )
      {
        income = ( Income ) itIN.next () ;

        if ( income.getIncIncludeInTDS () )
          //if  ( income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT )
          if  ( income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT )
                totalCombinedIncome += income.getIncomeAmount () *
                  DealCalcUtil.annualize (income.getIncomePeriodId (), "IncomePeriod") *
                  income.getIncPercentInTDS() / 100.00 ;
        trace( "totalCombinedIncome = " + totalCombinedIncome );

      } //--------- enf of all income --------------

      // ----------- Max TDS Allowed --------------
      double maxTDSAllowed = 0.0;

      if ( deal.getLineOfBusinessId () == Mc.LOB_A )
          if ( deal.getDealTypeId() == 1 /* High ratio */ )
              maxTDSAllowed = 0.40 ;
          else
              maxTDSAllowed = 0.42 ;
      else
        maxTDSAllowed = 0.50 ;
      trace( "maxTDSAllowed = " + maxTDSAllowed );
      //--------------- get Max TDS Expenses Allowed -----------------------
      double maxTDSExpenseAllowed = 0.0;
      maxTDSExpenseAllowed = totalCombinedIncome * maxTDSAllowed ;
      maxTDSExpenseAllowed = Math.round ( maxTDSExpenseAllowed * 100 ) /100.00;
      deal.setMaximumTDSExpensesAllowed ( validate(maxTDSExpenseAllowed) );
      deal.ejbStore ();
      return;

    }
     catch ( Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

    deal.setMaximumTDSExpensesAllowed(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncluedInTDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInTDS"));

    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "lineOfBusinessId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "maximumTDSExpensesAllowed", T13D2));
  }
}
