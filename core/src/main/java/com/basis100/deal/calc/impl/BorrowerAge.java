package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

public class BorrowerAge extends DealCalc
{

  public BorrowerAge()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 46";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget(input);
    }
    catch ( Exception e)
    {
      String msg = "Failed to get Targets for: " + this.getClass().getName();
      msg += " :[Input]: " + input.getClass ().getName () ;
      logger.error(msg);
      msg = "  Reason: " + e.getMessage();
      msg += " @input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Borrower b = (Borrower)target;

     try
     {

       Date d = b.getBorrowerBirthDate();

       if(d == null) return;

       Calendar then = Calendar.getInstance();
       then.setTime(d);

       Calendar now =  Calendar.getInstance(); //time zone

       int age = now.get(Calendar.YEAR) - then.get(Calendar.YEAR);

       // Bug fix to check if the bdate passed for this Year -- BY BILLY
       // Clear the Hour, Minute, Second fields for both (Just compare the date)
//trace("Before Convert:: Now = " + now + " Then = " + then);
       now.set(Calendar.HOUR, 0);
       now.set(Calendar.MINUTE, 0);
       now.set(Calendar.SECOND, 0);
       now.set(Calendar.MILLISECOND, 0);
       then.set(Calendar.HOUR, 0);
       then.set(Calendar.MINUTE, 0);
       then.set(Calendar.SECOND, 0);
       then.set(Calendar.MILLISECOND, 0);
       then.set(Calendar.YEAR, now.get(Calendar.YEAR));
//trace("After Convert:: Now = " + now + " Then = " + then);
       if(!then.before(now) == true)
       {
trace("Birthday not passed for this year ==> subtract by 1 !!");
        age--;
       }
trace("Borrower Age = " + age);

       b.setAge(validate(age));
       b.ejbStore();
       return;
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     b.setAge(DEFAULT_FAILED_INT);
     b.ejbStore();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.BORROWER, "borrowerBirthDate"));

    addTargetParam(new CalcParam(ClassId.BORROWER, "age", 99 ));
  }



}
