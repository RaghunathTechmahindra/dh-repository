package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;

/**
 * <p>
 * Title: PandIMonthlyMtgComponent
 * </p>
 * <p>
 * Description:Calculates Principal and Interest portion of payment , using the
 * monthly payment frequency values for mortgage component
 * </p>
 * Calculation Number- Calc-105.CM
 * @version 1.0 XS_11.1 20-Jun-2008 Initial Version
 * @version 1.1 25-Jun-2008 Formatted doCalc(..) method
 * @version 1.2 Modified the getTarget(..) method
 * @version 1.3 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
 */

public class PandIMonthlyMtgComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.1 20-Jun-2008 Initial Version
     */
    public PandIMonthlyMtgComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 105.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.1 20-Jun-2008 Initial Version
     * @version 1.1 08-Jul-2008 Added ComponentTypeId check
     * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    //MCM team added try-catch
                    Component component = (Component) input;
                    try {
                        if(component.getComponentTypeId()== Mc.COMPONENT_TYPE_MORTGAGE){
                        // find componentMortgage for the given componentId and
                        // copyId
                            ComponentMortgage componentMtg = (ComponentMortgage) entityCache
                                    .find(srk, new ComponentMortgagePK(component
                                            .getComponentId(), component.getCopyId()));
                            addTarget(componentMtg);
                        }
                    } catch (Exception e) {
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                 }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.1 20-Jun-2008 Initial Version
     * @version 1.1 XS_11.1 25-Jun-2008 Formatted the Code, added
     *          calculatePandI(..) from the DealCalcUtil
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        if (target == null) return;
        trace(this.getClass().getName());
        ComponentMortgage targetComponentMtg = (ComponentMortgage) target;
        try
        {
            // Retrive the component for the given component Mortgage
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentMtg.getComponentId(),
                            targetComponentMtg.getCopyId()));
            // Retrive the MtgProd for the given MtgProdId
            MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                    new MtgProdPK(component.getMtgProdId()));
            // Initializing variables
            int amortizationMonths = targetComponentMtg.getAmortizationTerm();

            double monthlyPandI = 0.0d;

            int repaymentTypeId = component.getRepaymentTypeId();

            // Get the InterestCompoundDesc from MtgProg.interestcompoundId
            String InterestCompoundDesc = PicklistData.getMatchingColumnValue(
                    -1, "interestcompound", mtgProd.getInterestCompoundingId(),
                    "interestcompounddescription");

            // Interest Only Variable or At Maturity
            if (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
                    || repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED)
            {
                // Calculate the Interest Factor using DealCalUtil
                double interestFactor = DealCalcUtil.interestFactor(
                        targetComponentMtg.getNetInterestRate(), 12d,
                        InterestCompoundDesc);
                monthlyPandI = targetComponentMtg.getTotalMortgageAmount()
                        * interestFactor;
            }
            else
            {
                monthlyPandI = DealCalcUtil.calculatePandI(component
                        .getInstitutionProfileId(), amortizationMonths,
                        Mc.PAY_FREQ_MONTHLY, InterestCompoundDesc,
                        targetComponentMtg.getTotalMortgageAmount(),
                        targetComponentMtg.getNetInterestRate());
            }
            monthlyPandI = Math.round(monthlyPandI * 100d) / 100.00d;
            //if the pAndIPaymentAmountMonthly is >99999999999.99 then Validate will round
            // of to the max given value in the TargetParam
            targetComponentMtg
                    .setPAndIPaymentAmountMonthly(validate(monthlyPandI));
            targetComponentMtg.ejbStore();
            return;
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

        targetComponentMtg
                .setPAndIPaymentAmountMonthly(this.DEFAULT_FAILED_DOUBLE);
        targetComponentMtg.ejbStore();
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.1 20-Jun-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "repaymentTypeId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "amortizationTerm"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));
        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "pAndIPaymentAmountMonthly", T13D2));

    }

}
