package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;

/**
 * <p>
 * Title: TeaserPandIComponent
 * </p>
 * <p>
 * Description:Calculates the The P and I amount for Components
 * </p>
 * Calculation Number- Calc-85.C
 * @version 1.0 XS_11.8 10-July-2008 Initial Version
 * @version 1.1 XS_11.8 24-Jul-2008 Refactored the Code
 */
public class TeaserPandIComponent extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.8 10-July-2008 Initial Version
     */
    public TeaserPandIComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 85.C";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.8 10-July-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENT)
            {
                Component component = (Component) input;
                addTarget(component);
            }
            else
                if (classId == ClassId.COMPONENTMORTGAGE)
                {
                    ComponentMortgage componentMtg = (ComponentMortgage) input;
                    // find component for the given componentId and
                    // copyId
                    Component component = (Component) entityCache.find(srk,
                            new ComponentPK(componentMtg.getComponentId(),
                                    componentMtg.getCopyId()));
                    addTarget(component);
                }
                else
                    if (classId == ClassId.COMPONENTLOC)
                    {
                        ComponentLOC componentLoc = (ComponentLOC) input;
                        // find component for the given componentId and
                        // copyId
                        Component component = (Component) entityCache.find(srk,
                                new ComponentPK(componentLoc.getComponentId(),
                                        componentLoc.getCopyId()));
                        addTarget(component);
                    }
                    else
                        if (classId == ClassId.COMPONENTLOAN)
                        {
                            ComponentLoan componentLoan = (ComponentLoan) input;
                            // find component for the given componentId and
                            // copyId
                            Component component = (Component) entityCache.find(
                                    srk, new ComponentPK(componentLoan
                                            .getComponentId(), componentLoan
                                            .getCopyId()));
                            addTarget(component);
                        }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.8 10-July-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());

        try
        {
            Component targetComponent = (Component) target;
            // Retrive the PricingRateInventory for the given
            // PricingRateInventoryId
            PricingRateInventory priInventory = new PricingRateInventory(srk,
                    targetComponent.getPricingRateInventoryId());
            int teaserTerm = priInventory.getTeaserTerm();

            if (teaserTerm > 0)
            {
                double monthlyPandIPaymentAmount = 0.0d;
                double teaserPandIPaymentAmount = 0.0d;
                double totalAmount = 0.0d;
                int paymentFrequency = -1;
                double numOfPaymentsPerYear = 12;// Default to Monthly
                int componentTypeId = targetComponent.getComponentTypeId();
                int repaymentTypeId = targetComponent.getRepaymentTypeId();

                // Retrive the MtgProd for the given MtgProdId

                MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                        new MtgProdPK(targetComponent.getMtgProdId()));

                String interestCompoundDesc = PicklistData
                        .getMatchingColumnValue(-1, "interestcompound", mtgProd
                                .getInterestCompoundingId(),
                                "interestcompounddescription");
                int amortizationMonths = 0;
                double interestFactor = 0.00d;
                if (componentTypeId == Mc.COMPONENT_TYPE_LOC)
                {
                    interestFactor = DealCalcUtil.interestFactor(
                            targetComponent.getTeaserNetInterestRate(),
                            numOfPaymentsPerYear);
                }
                else
                {
                    interestFactor = DealCalcUtil.interestFactor(
                            targetComponent.getTeaserNetInterestRate(),
                            numOfPaymentsPerYear, interestCompoundDesc);
                }

                if (interestFactor < 0)
                {
                    targetComponent.setTeaserPiAmount(0.0);
                    targetComponent.ejbStore();
                }
                else
                    if (componentTypeId == Mc.COMPONENT_TYPE_MORTGAGE)
                    {
                        ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
                                .find(srk, new ComponentMortgagePK(
                                        targetComponent.getComponentId(),
                                        targetComponent.getCopyId()));
                        totalAmount = componentMtg.getTotalMortgageAmount();
                        paymentFrequency = componentMtg.getPaymentFrequencyId();
                        amortizationMonths = componentMtg.getAmortizationTerm();
                    }
                    else
                        if (componentTypeId == Mc.COMPONENT_TYPE_LOC)
                        {
                            ComponentLOC componentLoc = (ComponentLOC) this.entityCache
                                    .find(srk, new ComponentLOCPK(
                                            targetComponent.getComponentId(),
                                            targetComponent.getCopyId()));
                            totalAmount = componentLoc.getTotalLocAmount();
                            paymentFrequency = componentLoc
                                    .getPaymentFrequencyId();

                        }
                        else
                            if (componentTypeId == Mc.COMPONENT_TYPE_LOAN)
                            {
                                ComponentLoan componentLoan = (ComponentLoan) this.entityCache
                                        .find(srk, new ComponentLoanPK(
                                                targetComponent
                                                        .getComponentId(),
                                                targetComponent.getCopyId()));
                                totalAmount = componentLoan.getLoanAmount();
                                paymentFrequency = componentLoan
                                        .getPaymentFrequencyId();
                                amortizationMonths = componentLoan
                                        .getActualPaymentTerm();
                            }
                if ((componentTypeId == Mc.COMPONENT_TYPE_MORTGAGE || componentTypeId == Mc.COMPONENT_TYPE_LOC)
                        && (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE || repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED))
                {
                    teaserPandIPaymentAmount = totalAmount * interestFactor;

                }
                else
                {
                    if (componentTypeId == Mc.COMPONENT_TYPE_LOC)
                    {
                        monthlyPandIPaymentAmount = T13D2;
                    }
                    else
                    {
                        // calculate Monthly PandI
                        monthlyPandIPaymentAmount = DealCalcUtil
                                .calculatePandI(targetComponent
                                        .getInstitutionProfileId(),
                                        amortizationMonths,
                                        Mc.PAY_FREQ_MONTHLY,
                                        interestCompoundDesc, totalAmount,
                                        targetComponent
                                                .getTeaserNetInterestRate());
                    }

                    if (paymentFrequency == Mc.PAY_FREQ_MONTHLY)
                    {
                        teaserPandIPaymentAmount = monthlyPandIPaymentAmount;
                    }
                    else
                        if (paymentFrequency == Mc.PAY_FREQ_SEMIMONTHLY)
                        {
                            teaserPandIPaymentAmount = monthlyPandIPaymentAmount / 2d;
                        }
                        else
                            if (paymentFrequency == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
                            {
                                teaserPandIPaymentAmount = monthlyPandIPaymentAmount / 4d;
                            }
                            else
                                if (paymentFrequency == Mc.PAY_FREQ_WEEKLY)
                                {
                                    teaserPandIPaymentAmount = (monthlyPandIPaymentAmount * 12d) / 52d;
                                }
                                else
                                    if (paymentFrequency == Mc.PAY_FREQ_BIWEEKLY)
                                    {
                                        teaserPandIPaymentAmount = (monthlyPandIPaymentAmount * 12d) / 26d;
                                    }

                }
                targetComponent.setTeaserPiAmount(Math
                        .round(teaserPandIPaymentAmount * 100d) / 100.00d);
                targetComponent.ejbStore();
            }
            else
            {
                targetComponent.setTeaserPiAmount(0.0d);
                targetComponent.ejbStore();
            }
            return;
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.8 10-July-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "pricingRateInventoryId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "teaserNetInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "repaymentTypeId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "totalLocAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "loanAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "paymentFrequencyId"));
        addTargetParam(new CalcParam(ClassId.COMPONENT, "teaserPiAmount", T12D6));
        // addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
        // "amortizationTerm"));
        // addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
        // "netInterestRate"));
        // addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
        // "actualPaymentTerm"));
    }
}
