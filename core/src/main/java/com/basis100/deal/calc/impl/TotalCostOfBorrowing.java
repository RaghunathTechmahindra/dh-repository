package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.FeePK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.resources.PropertiesCache;

/**
 * 
 * <p>Title: TotalCostOfBorrowing.java</p>
 *
 * <p>Description: TotalCostOfBorrowing.java</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.2 <br>
 * Date: 11/8/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: <br>
 * 	Fixed calculation to follow specs, defect #1454
 */
public class TotalCostOfBorrowing extends DealCalc {

	/**
	 * 
	 * A constructor for this class.
	 * 
	 * @throws Exception
	 */
	public TotalCostOfBorrowing() throws Exception {
		super();
		initParams();
		this.calcNumber = "Calc 114";
	}

	/**
	 * method: getTargets description: returns the Collection object which
	 * contains all the defined target for the CashBack Class
	 * 
	 * @throws -
	 */
	public Collection getTargets() {
		return this.targets;
	}

	/**
	 * method: getTarget description: Method add the targets into the Calc Class
	 * 
	 * @param: Object, CaclMonitor
	 * @throws TargetNotFoundException
	 */
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
		try {
//			addTarget(input);
	      if(input instanceof DealFee)
	      {
			Deal d = null;
	    	DealFee dealFee = (DealFee)input;
	        d = (Deal)entityCache.find(srk, 
                    new DealPK(dealFee.getDealId(), dealFee.getCopyId()));
	        addTarget(d);
	      }
	      
	      if(input instanceof Deal)
	      {
	    	addTarget(input);
	      }	      
	      
		} catch (Exception e) {
			String msg = "Fail to get Targets" + this.getClass().getName();
			msg += ":[Input]:" + input.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@input: " + this.getDoCalcErrorDetail(input);
			logger.error(msg);
			throw new TargetNotFoundException(msg);
		}
	}

	/**
	 * method: hasInput description: Checks whether the required input exists or
	 * not.
	 * 
	 * @param CalcParam
	 * @return: Boolean
	 */
	public boolean hasInput(CalcParam input) {
		return this.inputParam.contains(input);
	}

	/**
	 * method: doCalc description: Performs the required business calculations.
	 * 
	 * @param: Object
	 * @throws: Exception
	 */
	public void doCalc(Object target) throws Exception {
		trace(this.getClass().getName());
		Deal deal = (Deal) target;
		double totalCostOfBorrowing = 0d;
		try {
			double minRate = 0d;
			double maxRate = 1d;
			double tmpRate = 0.5d;
			double tmpAmount = 0d;

			double payment = deal.getPandIPaymentAmountMonthly();
			//ticket #14476
			//double netLoanAmount = deal.getNetLoanAmount();
			double netLoanAmount = deal.getTotalLoanAmount();
            //ticket #14476
			double totalFees = DealCalcUtil.getTotalAllFeeAmount(deal,srk);
			double mBalanceAtEndOfTerm = deal.getBalanceRemainingAtEndOfTerm();
			int paymentFrequencyId = deal.getPaymentFrequencyId();

			String decimalYear = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
					"com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
			double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;

			netLoanAmount -= totalFees; 
			  
			double balance;
			balance = mBalanceAtEndOfTerm;

			int actualPaymentTerm = deal.getActualPaymentTerm();
			if (actualPaymentTerm == 0 || deal.getNetInterestRate() == 0d || netLoanAmount < 0d || netLoanAmount <= payment ) {
			  totalCostOfBorrowing = 0d;
			} else {
				double mPaymentsPerYear = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId, mDaysInYear);
				double term = actualPaymentTerm * mPaymentsPerYear / 12d;

				while (tmpRate != 0d) {
					tmpAmount = ((1 - Math.pow(1 + tmpRate, (-1) * term)) / tmpRate) * payment;
					tmpAmount += (balance / Math.pow(1 + tmpRate, term));
						
					if (tmpAmount > 99999999.99	|| Math.abs(netLoanAmount - tmpAmount) < 0.0000001)
						break;

					if (netLoanAmount > tmpAmount)
						maxRate = tmpRate;
					else if (netLoanAmount < tmpAmount)
						minRate = tmpRate;

					tmpRate = (maxRate + minRate) / 2;
				}
				totalCostOfBorrowing = Math.pow(1d + tmpRate, mPaymentsPerYear) - 1d;
				totalCostOfBorrowing = totalCostOfBorrowing * 100d;
				if (totalCostOfBorrowing > 999.999d)
					totalCostOfBorrowing = 0d;
			}
			totalCostOfBorrowing = Math.round(totalCostOfBorrowing * 1000d) / 1000d;
			deal.setTotalCostOfBorrowing(totalCostOfBorrowing);
			deal.ejbStore();
			return;
		} catch (Exception e) {
			String msg = "Benign Failure in Calculation"
					+ this.getClass().getName();
			msg += ":[Target]:" + target.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@Target: " + this.getDoCalcErrorDetail(target);
			logger.debug(msg);
		}
		deal.setTotalCostOfBorrowing(this.DEFAULT_FAILED_DOUBLE);
		deal.ejbStore();
	}

	/**
	 * 
	 * initParams Method description: Initializes all the required input and
	 * output(targets) parameters
	 * 
	 * @throws ParamTypeException
	 */
	private void initParams() throws ParamTypeException {
		targetParam = new ArrayList();
		inputParam = new ArrayList();
		inputClassTypes = new HashSet();

		addInputParam(new CalcParam(ClassId.DEAL, "balanceRemainingAtEndOfTerm"));
		//addInputParam(new CalcParam(ClassId.DEAL, "netLoanAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "actualPaymentTerm"));
		addInputParam(new CalcParam(ClassId.DEAL, "MIStatusId"));
		addInputParam(new CalcParam(ClassId.DEAL, "MIPremiumPST"));
		addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.DEAL, "PandIPaymentAmountMonthly"));
		addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
		addInputParam(new CalcParam(ClassId.DEALFEE, "feeAmount"));
		addInputParam(new CalcParam(ClassId.DEALFEE, "feeStatusId"));
		
		addTargetParam(new CalcParam(ClassId.DEAL, "totalCostOfBorrowing"));
	}

}
