package com.basis100.deal.calc;

import java.util.*;

public class CalcParam
{
  protected int classId;
  protected String name;
  protected double maxCalculatedValue = -1.0;

  public CalcParam(int entity, String field)
  {
    this.classId = entity;
    this.name = field;
  }

  public CalcParam(int entity, String field, double maxCalculatedValue)
  {
    this.classId = entity;
    this.name = field;
    this.maxCalculatedValue = maxCalculatedValue;
  }


  public int getClassId(){ return this.classId;}

  public String getName(){ return this.name;}
  /**
  *  @return true if this CalcParam Classid and field are equal the params
  *  else return false;
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    CalcParam other  = (CalcParam)object;
      //PMI2 : Jan 29, 2008 - ignore entity name's case - Hiro
      if(getClassId() == other.getClassId()){
          if(getName().trim().equalsIgnoreCase(other.getName().trim()))
      return true;
      }
     return false;
  }

  public int hashCode()
  {
    return name.hashCode() + classId;
  }
}