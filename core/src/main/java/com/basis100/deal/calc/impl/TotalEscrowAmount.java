package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Total Escrow Amount
//Code: CALC 41
//07/04/2000

public class TotalEscrowAmount extends DealCalc
{


  public TotalEscrowAmount()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 41";
  }

  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof EscrowPayment )
      {
        EscrowPayment ep = ( EscrowPayment) input ;

        Deal deal = (Deal) entityCache.find (srk,
            new DealPK( ep.getDealId() , ep.getCopyId ()));

        addTarget( deal );
      }
      //--> Temp Bug fix : to make sure it will called after P&I calc (1)
      //--> Details pls refer to the comment on initParams() method.
      //--> By Billy 02Dec2002
      else if ( input instanceof Deal )
             addTarget(input);
      //=================================================================
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }

  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = ( Deal) target;

     try
     {
        Collection eps = deal.getEscrowPayments ();
         Iterator itEPS =  eps.iterator () ;
         double totalEAmount = 0.0 ;
         while ( itEPS.hasNext () )
          {
            EscrowPayment  ep = ( EscrowPayment ) itEPS.next () ;
            totalEAmount += ep.getEscrowPaymentAmount () ;
          } // end of all Escrow Paymensts
         totalEAmount = Math.round ( totalEAmount * 100 ) /100.00 ;
         deal.setEscrowPaymentAmount ( validate(totalEAmount));
         deal.ejbStore ();
         return;
    }
    catch(Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }
    deal.setEscrowPaymentAmount(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    //--> Temp Bug fix : There is a bug that when user made some changes that triggered the P&I
    //--> calc (#1). Somehow this calc executed before the P&I calc that caused the TotalPaymentAmount (calc 44)
    //--> missed the result of the escrowPaymentAmount.
    //--> This can be fixed by making sure it will called after P&I calc (1).  In order to achieve that I added
    //--> the PandiPaymentAmount as an input parameter.
    //--> TODO : for long term fixes we have to re-design the caching machanism to make sure we using the same Entity
    //--> Objects.
    //--> By Billy 02Dec2002
    addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
    //=================================================================

    addInputParam(new CalcParam(ClassId.ESCROW, "escrowPaymentAmount"));

    addTargetParam(new CalcParam(ClassId.DEAL, "escrowPaymentAmount", T13D2 ));
  }



}
