package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

public class CombinedHeatingExpense extends DealCalc
{


  public CombinedHeatingExpense()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 16";
  }



  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {
        PropertyExpense pe = (PropertyExpense)input;
        Property p = (Property) entityCache.find ( srk, 
                          new PropertyPK(pe.getPropertyId(), pe.getCopyId()));
        Deal deal = (Deal) entityCache.find(srk, 
                          new DealPK(p.getDealId(), p.getCopyId()));
        addTarget(deal);
     }
     catch(Exception e)
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }

  /**
   *  calculation based on monthly combined heating expense
   */
  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Deal deal = (Deal)target;

     try
     {
       double amt = 0.0;

       Collection pes = this.entityCache.getPropertyExpenses(deal);

       Iterator it2 = pes.iterator();

       PropertyExpense propertyExpense = null ;
       double annualPeriod = 1;

       while(it2.hasNext())
       {
          propertyExpense = (PropertyExpense)it2.next();
          if  ( propertyExpense.getPropertyExpenseTypeId () == 2 /* Heating Expense */ )
          {
              int id = propertyExpense.getPropertyExpensePeriodId();
              annualPeriod = DealCalcUtil.annualize(id,"PropertyExpensePeriod");
              amt += propertyExpense.getPropertyExpenseAmount() * (annualPeriod);
           }
          trace( "AnnualHeatingExpense = " + amt );
       }
       amt = Math.round ( amt * 100 ) / 100.00;
       deal.setCombinedTotalAnnualHeatingExp ( validate(amt) );
       deal.ejbStore ();
       return;
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     deal.setCombinedTotalAnnualHeatingExp ( DEFAULT_FAILED_DOUBLE );
     deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseTypeId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalAnnualHeatingExp" , T13D2));
  }
}
