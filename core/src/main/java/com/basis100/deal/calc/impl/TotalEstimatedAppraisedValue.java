package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Total Estimated Appraised Value
//Code: CALC 42
//07/04/2000

public class TotalEstimatedAppraisedValue extends DealCalc
{


  public TotalEstimatedAppraisedValue() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 42";
  }

 
  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {
       if ( input instanceof Property )
       {
         Property p = (Property)input;
         Deal d  = (Deal) entityCache.find (srk,
                    new DealPK( p.getDealId(), p.getCopyId())) ;
         addTarget(d);
       }
     }
     catch(Exception e)
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal dt = (Deal)target;
    try
    {
        Collection props = this.entityCache.getProperties(dt);

        Iterator pit = props.iterator();
        Property current = null;

        double val = 0;

        while(pit.hasNext())
        {
          current = (Property)pit.next();
          val += current.getEstimatedAppraisalValue();
        }
        val = Math.round ( val * 100) / 100.00 ; 
        dt.setTotalEstAppraisedValue(validate(val));
        dt.ejbStore();
        return;
    }
    catch(Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

    dt.setTotalEstAppraisedValue(DEFAULT_FAILED_DOUBLE);
    dt.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.PROPERTY, "estimatedAppraisalValue"));

    addTargetParam(new CalcParam(ClassId.DEAL, "totalEstAppraisedValue", T13D2));

  }



} 
