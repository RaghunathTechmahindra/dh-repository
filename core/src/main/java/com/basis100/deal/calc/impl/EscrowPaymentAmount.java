/**
 * Title:        EscrowPaymentAmount (Calc-107)
 * Description:  Escrow Payment Amount (It used to be calculated by the Mapping)
 * Copyright:    Copyright (c) 2001
 * Company:      Basis100
 * Date :        05 March 2002
 * @author Billy Lam
 * @version
 */

package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

public class EscrowPaymentAmount extends DealCalc
{
    public EscrowPaymentAmount()throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 107";
    }


    public Collection getTargets()
    {
        return this.targets;
    }


    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {

            if ( input instanceof Deal )
            {

                Deal deal = ( Deal ) input ;
                Collection escrowPayments = this.entityCache.getEscrowPayments( deal ) ;
                Iterator itES = escrowPayments.iterator ();
                // Add only the EscrowPayment with Type = 0 (Property Tax)
                while ( itES.hasNext () )
                {
                    EscrowPayment theEs = (EscrowPayment) itES.next();
                    if(theEs.getEscrowTypeId() == 0)
                    {
                        addTarget( theEs );
                    }
                }
            }
            else if ( input instanceof Property )
            {
                // Trigger only if Primary Property changed
                Property p = (Property)input;
                if(p.isPrimaryProperty() == true)
                {
                    Deal deal = (Deal) entityCache.find (srk, 
                            new DealPK( p.getDealId(), p.getCopyId ())) ;
                    Collection escrowPayments = this.entityCache.getEscrowPayments( deal ) ;
                    Iterator itES = escrowPayments.iterator ();
                    // Add only the EscrowPayment with Type = 0 (Property Tax)
                    while ( itES.hasNext () )
                    {
                        EscrowPayment theEs = (EscrowPayment) itES.next();
                        if(theEs.getEscrowTypeId() == 0)
                        {
                            addTarget( theEs );
                        }
                    }
                }
            }
            else if ( input instanceof EscrowPayment )
            {
                // Trigger only by the EscrowPayment Type = 0 (Property Tax)
                EscrowPayment es = (EscrowPayment)input;
                if(es.getEscrowTypeId() == 0)
                    addTarget( es );
            }

        }
        catch ( Exception e)
        {
            String msg = "Failed to get Targets for: " + this.getClass().getName();
            msg += " :[Input]: " + input.getClass ().getName () ;
            logger.error(msg);
            msg = "  Reason: " + e.getMessage();
            msg += " @input: " + this.getDoCalcErrorDetail ( input );
            logger.error (msg );
            throw new TargetNotFoundException (msg);
        }
    }



    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        EscrowPayment theEs = (EscrowPayment)target;
        
        Deal deal = (Deal)entityCache.find(srk, 
                new DealPK( theEs.getDealId(), theEs.getCopyId())) ;
        
        // Nothing to do if the EscrowPayment type <> 0 (Property Tax) OR
        //   Parameter(com.basis100.calc.autocalctaxescrow) == N
        if(theEs.getEscrowTypeId() != 0 ||
                (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.calc.autocalctaxescrow", "N")).equals("N"))
        {
            return;
        }

        try
        {

            Property primProp = new Property(srk, null).findByPrimaryProperty(
                    deal.getDealId(), deal.getCopyId(), deal
                            .getInstitutionProfileId());
            double totalAnnualTaxAmount = primProp.getTotalAnnualTaxAmount();
            double escrowPaymentAmount = 0;

            trace("PrimaryProperty.TotalAnnualTaxAmount = " + totalAnnualTaxAmount);

            Date iDate = deal.getInterimInterestAdjustmentDate();
            int iMonth = 1; // default to 1
            if (iDate != null)
            {
                trace("InterimInterestAdjustmentDate = " + iDate);
                iMonth = iDate.getMonth() + 1;   // 1=Jan ... 12=Dec
            }
            else
            {
                trace("InterimInterestAdjustmentDate not set -- default the iMonth to 1");
            }

            trace("iMonth = " + iMonth);

            //--> Modified for special Escrow Clac for Xceed
            //--> By BILLY 26Aug2003
            // get Province of Primary property
            int provinceCode = primProp.getProvinceId();
            // get the Month of FirstPaymentDate
            Date fDate = deal.getFirstPaymentDate();
            int fMonth = 1; // default to 1
            if (fDate != null)
            {
                trace("FirstPaymentDate = " + fDate);
                fMonth = fDate.getMonth() + 1;   // 1=Jan ... 12=Dec
            }
            else
            {
                trace("FirstPaymentDate not set -- default the fMonth to 1");
            }

            trace("fMonth = " + fMonth);
            //==============================================

            //==============================================
            //--> get PaymentFrequencyId for DJ mode
            //--> Modified by Billy 29Oct2003
            int paymentFreqId = deal.getPaymentFrequencyId();
            trace("paymentFreqId = " + paymentFreqId);
            //==============================================

            // get the property
            String theProp = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.calc.autocalctaxescrow", "N");
            trace("com.basis100.calc.autocalctaxescrow = " + theProp);
            //====================================================================================================
            //--> Change Request # DJ001
            //--> - Eliminated "com.basis100.calc.escrowpayment.usexceedspecialcalc"
            //--> - Redefined "com.basis100.calc.autocalctaxescrow" :
            //-->     N -- Disable
            //-->     T -- Normal mode (for TD, BW and COOP)
            //-->     X -- for Xceed
            //-->     D -- for Desjardins
            //-->
            //--> By Billy 29Oct2003
            //=====================================================================================================
            // Check which mode
            if(theProp.equals("X"))
            {
                trace("Running Calc for Xceed !!");
                //use special calc for Xceed -- for detail pls refer to product spec.
                switch(provinceCode)
                {
                case 7 : //Nova Scotia
                    switch(fMonth)
                    {
                    case 1 :   // Jan
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/20);
                        break;
                    case 2 :   // Feb
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/20);
                        break;
                    case 3 :   // Mar
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/19);
                        break;
                    case 4 :   // Apr
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/18);
                        break;
                    case 5 :   // May
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/17);
                        break;
                    case 6 :   // Jun
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/16);
                        break;
                    case 7 :   // Jul
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/15);
                        break;
                    case 8 :   // Aug
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/14);
                        break;
                    case 9 :   // Sep
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/12);
                        break;
                    case 10 :   // Oct
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/12);
                        break;
                    case 11 :   // Nov
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/11);
                        break;
                    case 12 :   // Dec
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/10);
                        break;
                    default:
                        break;
                    }
                    break;

                case 4 : //New Brunswick
                case 5 : //Newfoundland
                    switch(fMonth)
                    {
                    case 1 :   // Jan
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/14);
                        break;
                    case 2 :   // Feb
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/13);
                        break;
                    case 3 :   // Mar
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/12);
                        break;
                    case 4 :   // Apr
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/12);
                        break;
                    case 5 :   // May
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/11);
                        break;
                    case 6 :   // Jun
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/10);
                        break;
                    case 7 :   // Jul
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/9);
                        break;
                    case 8 :   // Aug
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/20);
                        break;
                    case 9 :   // Sep
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/19);
                        break;
                    case 10 :   // Oct
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/18);
                        break;
                    case 11 :   // Nov
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/17);
                        break;
                    case 12 :   // Dec
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/15);
                        break;
                    default:
                        break;
                    }
                    break;

                default : //Other provinces
                    switch(fMonth)
                    {
                    case 1 :   // Jan
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/18);
                        break;
                    case 2 :   // Feb
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/17);
                        break;
                    case 3 :   // Mar
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/16);
                        break;
                    case 4 :   // Apr
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/15);
                        break;
                    case 5 :   // May
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/14);
                        break;
                    case 6 :   // Jun
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.25)/13);
                        break;
                    case 7 :   // Jul
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/12);
                        break;
                    case 8 :   // Aug
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.00)/11);
                        break;
                    case 9 :   // Sep
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/20);
                        break;
                    case 10 :   // Oct
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/19);
                        break;
                    case 11 :   // Nov
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/18);
                        break;
                    case 12 :   // Dec
                        escrowPaymentAmount = ((totalAnnualTaxAmount*1.50)/17);
                        break;
                    default:
                        break;
                    }
                break;
                }// End Switch

                //--> New change required by Product to calc the end result based on PaymentFreq
                //--> By Billy 19Dec2003
                switch(paymentFreqId)
                {
                case 0 :   // Monthly
                    escrowPaymentAmount = escrowPaymentAmount*12/12;
                    break;
                case 1 :   // semi-monthly
                    escrowPaymentAmount = escrowPaymentAmount*12/24;
                    break;
                case 2 :   // bi-weekly
                case 3 :   // Accelerated bi-weekly
                    escrowPaymentAmount = escrowPaymentAmount*12/26;
                    break;
                case 4 :   // weekly
                case 5 :   // Accelerated weekly
                    escrowPaymentAmount = escrowPaymentAmount*12/52;
                    break;
                default:
                    break;
                }
                //===============================================================================
            } // End Xceed Calc
            else if(theProp.equals("D"))
            {
                trace("Running Calc for DJ !!");
                // Use Desjardin Calc
                switch(paymentFreqId)
                {
                case 0 :   // Monthly
                    escrowPaymentAmount = totalAnnualTaxAmount/12;
                    break;
                case 1 :   // semi-monthly
                    escrowPaymentAmount = totalAnnualTaxAmount/24;
                    break;
                case 2 :   // bi-weekly
                case 3 :   // Accelerated bi-weekly
                    escrowPaymentAmount = totalAnnualTaxAmount/26;
                    break;
                case 4 :   // weekly
                case 5 :   // Accelerated weekly
                    escrowPaymentAmount = totalAnnualTaxAmount/52;
                    break;
                default:
                    break;
                }
            } // End Desjardin Calc
            else
            {
                trace("Running Calc for TD and others !!");
                // Use normal clac
                switch(iMonth)
                {
                case 1 :   // Jan
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*24)/17;
                    break;
                case 2 :   // Feb
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*24)/16;
                    break;
                case 3 :   // Mar
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*22)/15;
                    break;
                case 4 :   // Apr
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*20)/14;
                    break;
                case 5 :   // May
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*20)/13;
                    break;
                case 6 :   // Jun
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*18)/12;
                    break;
                case 7 :   // Jul
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*16)/11;
                    break;
                case 8 :   // Aug
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*18)/10;
                    break;
                case 9 :   // Sep
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*26)/21;
                    break;
                case 10 :   // Oct
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*26)/20;
                    break;
                case 11 :   // Nov
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*24)/19;
                    break;
                case 12 :   // Dec
                    escrowPaymentAmount = ((totalAnnualTaxAmount/12)*24)/18;
                    break;
                default:
                    break;
                }
            }//End Normal calc
            //===============================================================================

            escrowPaymentAmount = Math.round ( escrowPaymentAmount * 100 ) / 100.00 ;
            trace("The escrowPaymentAmount = " + escrowPaymentAmount);

            theEs.setEscrowPaymentAmount(validate(escrowPaymentAmount));
        }
        catch ( Exception e )
        {
            String msg = "Benign Failure in Calculation" + this.getClass().getName();
            msg += ":[Target]:" + target.getClass ().getName () ;
            msg += "  Reason: " + e.getMessage();
            msg += "@Target: " + this.getDoCalcErrorDetail ( target );
            logger.debug (msg );

            theEs.setEscrowPaymentAmount(DEFAULT_FAILED_DOUBLE);
        }

        theEs.ejbStore();
    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }

    private void initParams()throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.PROPERTY, "totalAnnualTaxAmount"));
        addInputParam(new CalcParam(ClassId.ESCROW, "escrowTypeId"));
        addInputParam(new CalcParam(ClassId.DEAL, "interimInterestAdjustmentDate"));
        addInputParam(new CalcParam(ClassId.DEAL, "firstPaymentDate"));
        addInputParam(new CalcParam(ClassId.PROPERTY, "provinceId"));
        addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));

        addTargetParam(new CalcParam(ClassId.ESCROW, "escrowPaymentAmount", T13D2));
    }

}

