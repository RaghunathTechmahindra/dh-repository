package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * Title: IADNumberOfDaysLoanComponent
 * </p>
 * <p>
 * Description:Calculates the number of days between the Closing Date and the
 * Interest Adjustment Date (number of days in the interest adjustment period)
 * of a Loan Component
 * </p>
 * Calculation Number- Calc-103.CLoan
 * @version 1.0 XS_11.15 04-July-2008 Initial Version
 * @version 1.1 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
 */
public class IADNumberOfDaysLoanComponent extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.15 04-July-2008 Initial Version
     */
    public IADNumberOfDaysLoanComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 103.CLoan";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.15 04-July-2008 Initial Version
     * @version 1.1 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();
            if (classId == ClassId.COMPONENTLOAN)
            {
                ComponentLoan componentLoan = (ComponentLoan) input;

                addTarget(componentLoan);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Deal deal = (Deal) input;
                    Collection listOfcomponents = entityCache.getComponents(
                            deal, srk);
                    Iterator componentsItr = listOfcomponents.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is LOAN COMPONENT
                        // MCM team added try - catch
                        try {
                            if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOAN)
                            {
                                // Retrives the componentLoan Object for the
                                // given
                                // componentId and copyId
                                ComponentLoan componentLoan = (ComponentLoan) entityCache
                                        .find(srk, new ComponentLoanPK(
                                                component.getComponentId(),
                                                component.getCopyId()));
                                addTarget(componentLoan);
                            }
                        } catch (Exception e) {
                            trace("component is being deleted componentId = " + component.getComponentId());
                        }

                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.15 04-July-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());

        try
        {
            ComponentLoan targetComponentLoan = (ComponentLoan) target;
            // Retrive the Component for the given component Id and Copy ID
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLoan.getComponentId(),
                            targetComponentLoan.getCopyId()));
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));
            Calendar estClosingDate = Calendar.getInstance();
            estClosingDate.setTime(deal.getEstimatedClosingDate());
            Calendar iADate = Calendar.getInstance();
            iADate.setTime(targetComponentLoan.getInterestAdjustmentDate());
            //Retrive the Days Between Estimate closing date and Interest Adjustment date
            Long noOfdaysDiff = DealCalcUtil.getDaysBetween(estClosingDate,
                    iADate);
            //if the IadNumberOfDays is >99999 then Validate will round of to the max given value in the TargetParam
            targetComponentLoan.setIadNumberOfDays(validate(noOfdaysDiff.intValue()));
            targetComponentLoan.ejbStore();
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.15 04-July-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
                "interestAdjustmentDate"));
        // Max iadNumberOfDays 9999
        addTargetParam(new CalcParam(ClassId.COMPONENTLOAN,
                "iadNumberOfDays",T4));
    }
}
