package com.basis100.deal.calc;

import com.basis100.deal.calc.impl.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import java.io.*;
import com.basis100.resources.*;
import java.util.*;
import java.util.SimpleTimeZone.*;

import java.util.Calendar;
import com.basis100.jdbcservices.jdbcexecutor.*;

import com.basis100.deal.entity.*;

public class TestDCM
{

  static SessionResourceKit srk =   new SessionResourceKit("sysy");

  public void DCMtest()   throws Exception
  {
    JdbcExecutor jExec = srk.getJdbcExecutor();

    try
    {

      jExec.begin();

      CalcMonitor dcm = CalcMonitor.getMonitor(srk);

      Deal d = new Deal( srk, dcm, 6072, 3 );

      //Borrower b = new Borrower(srk, dcm );
      //b.create ( new DealPK ( 6072 , 3) ) ;
      //b.ejbStore ();

      d.setPaymentFrequencyId ( 1 );
      d.ejbStore ();

      dcm.setDebug ( true );
      //dcm.findRelatedCalcs( new CalcParam(ClassId.DEAL , "teaserTerm" ) ) ;

      dcm.calc ();

      //dcm.printRelatedCalcs ();
      //dcm.printFiredCalcs ();

      jExec.rollback ();

    }
    catch(Exception e)
    {
      jExec.rollback();
      e.printStackTrace();
    }
  }

  public static void main(String args[]) throws Exception
  {

    ResourceManager.init("g:\\mos\\admin\\", "MOS");
    PropertiesCache.addPropertiesFile("g:\\mos\\admin\\mossys.properties");

    TestDCM test = new TestDCM();

    test.DCMtest();
    System.exit ( 0 );
  }


 
}
