package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: IADAmountMtgComponent
 * </p>
 * <p>
 * Description:Calculates the Amount of interest to be collected which is
 * computed as the number of days from the Closing Date to Interest Adjustment
 * Date for which interest should be charged.
 * </p>
 * Calculation Number- Calc-12.CM
 * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
 * @version 1.1 Modified the getTarget(..) method
 * @version 1.2 artf751756 1-Aug-2008 added try catch block in getTarget
 */
public class IADAmountMtgComponent extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.5 24-Jun-2008 Initial Version
     */
    public IADAmountMtgComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 12.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.5 24-Jun-2008 Initial Version
     * @version 1.1 08-Jul-2008 Added ComponentTypeId check
     * @version 1.2 artf751756 1-Aug-2008 added try catch block
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                    if (classId == ClassId.COMPONENT)
                    {
                        Component component = (Component) input;
                        // MCM added try-catch
                        try {
                            if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                            {
                            // find componentMortgage for the given componentId and
                            // copyId
                                ComponentMortgage componentMtg = (ComponentMortgage) entityCache
                                        .find(srk, new ComponentMortgagePK(component
                                                .getComponentId(), component.getCopyId()));
                                addTarget(componentMtg);
                            }
                        } catch (Exception e) {
                            trace("component is being deleted componentId = " + component.getComponentId());
                        }
                    }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.5 24-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());

        try
        {
            ComponentMortgage targetComponentMtg = (ComponentMortgage) target;
            // Retrive the component for the given component Mortgage
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentMtg.getComponentId(),
                            targetComponentMtg.getCopyId()));
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));
            // Retrive the LenderProfile object for the given lender profile
            LenderProfile lenderProfile = (LenderProfile) this.entityCache
                    .find(srk, new LenderProfilePK(deal.getLenderProfileId()));
            // Retrive the Interest Compound Description from the picklist using
            // BXResources for the given Interest Compound Id
            String interestCompoundDesc = BXResources.getPickListDescription(
                    lenderProfile.getInstitutionProfileId(),
                    "INTERESTCOMPOUND", lenderProfile.getInterestCompoundId(),
                    srk.getLanguageId());
            // Retrive the Sysproperty for
            // 'com.basis100.deal.calc.InterimInterest.norounding' using
            // propertyCache
            String interimInterestNoRounding = PropertiesCache
                    .getInstance()
                    .getProperty(deal.getInstitutionProfileId(),
                            "com.basis100.deal.calc.InterimInterest.norounding");

            double iADAmount = 0.0d;
            double perDiemInterestAmount = 0.0d;
            if ("N".equalsIgnoreCase(interimInterestNoRounding))
            {
                perDiemInterestAmount = DealCalcUtil
                        .calculatePerDiemInterestAmount(interestCompoundDesc,
                                targetComponentMtg.getNetInterestRate(),
                                targetComponentMtg.getTotalMortgageAmount());
                // Rounding to 5 decimal places
                perDiemInterestAmount = Math
                        .round(perDiemInterestAmount * 100000) / 100000;
            }
            else
            {
                perDiemInterestAmount = targetComponentMtg
                        .getPerDiemInterestAmount();
            }
            // Rounding to 2 decimal places
            iADAmount = Math.round(perDiemInterestAmount
                    * targetComponentMtg.getIadNumberOfDays() * 100d) / 100d;
            // if the InterestAdjustmentAmount is >99999999999.99 then Validate
            // will round of to the max given value in the TargetParam
            targetComponentMtg.setInterestAdjustmentAmount(validate(iADAmount));
            targetComponentMtg.ejbStore();

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.5 24-Jun-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "perDiemInterestAmount"));

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "iadNumberOfDays"));

        // Max interestAdjustmentAmount 99999999999.99
        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "interestAdjustmentAmount", T13D2));
    }

}