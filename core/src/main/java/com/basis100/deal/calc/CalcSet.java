package com.basis100.deal.calc;

import java.io.*;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.jar.JarFile;
import java.util.zip.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CalcSet {

    private static final Log _log = LogFactory.getLog(CalcSet.class);

    private Set internal;

    private String pkg = "com/basis100/deal/calc/impl";

    public CalcSet() {
        init();
    }

    /*
     * init
     */
    public void init() {

        // get simple name for this class
        String simpleClassName = this.getClass().getSimpleName();
        _log.info("CalcSet className: " + simpleClassName);

        URL lsLocation = this.getClass()
                .getResource(simpleClassName + ".class");

        String dir = lsLocation.toString();

        _log.info("CalcSet lsLocation: " + lsLocation);

        if (lsLocation == null)
            throw new NullPointerException();
        try {
            if (dir.endsWith(".list"))
                internal = fromList(dir);
            else if (dir.startsWith("jar") || dir.startsWith("zip"))
                internal = fromArchive(dir);
            else
                internal = fromDirectory(dir);
        } catch (URISyntaxException e) {
            _log.error("CalcSet init error", e);
        } catch (FileNotFoundException e) {
            _log.error("CalcSet init error", e);
        } catch (IOException e) {
            _log.error("CalcSet init error", e);
        }
    }

    /**
     * Read the package list of classes from a file.
     * 
     * @param psLocation
     *            is the name of list file
     * @throws IOException
     * @author DVG 14/May/2004
     */
    private Set fromList(String psLocation) throws FileNotFoundException,
            URISyntaxException, IOException {

        File loList = new File(psLocation);

        if (!loList.isFile())
            return fromDirectory(psLocation);

        String ls;
        Set loout = new HashSet();
        BufferedReader loInRead;
        loInRead = new BufferedReader(new FileReader(loList));

        while ((ls = loInRead.readLine()) != null) {
            if (!ls.startsWith("#") && !ls.startsWith("//")) {
                int index = ls.indexOf('.');

                if (index >= 0)
                    ls = ls.substring(0, index);

                loout.add("com.basis100.deal.calc.impl." + ls);
            }
        }
        loInRead.close();
        return loout;
    }

    /**
     * Saves the package list of classes to a stream.
     * 
     * @param poOutStream
     *            is the output stream
     * @author DVG 13/May/2004
     */
    public void SaveToFile(PrintStream poOutStream) {

        Iterator loit = internal.iterator();
        while (loit.hasNext())
            poOutStream.println((String) loit.next());
    }

    /*
     * get the Calc Set from Archive
     */
    private Set fromArchive(String archive) throws FileNotFoundException,
            IOException {
        
        _log.info("CalcSet archive: " + archive);

        Set out = new HashSet();

        if (archive != null) {
            
            //get JarURLConnection
            URL url = new URL(archive);
            JarURLConnection jarConnection = (JarURLConnection) url
                    .openConnection();
            _log.info("entryName: jarurl: " + jarConnection.getJarFileURL());
            _log.info("URL: " + jarConnection.getURL());

            JarFile jarFile = jarConnection.getJarFile();

            // put the entrier in a set
            Enumeration entries = jarFile.entries();
            String name = "";
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                name = entry.getName();
                if (name.startsWith(pkg) && !entry.isDirectory()) {
                    name = name.substring(pkg.length() + 1);
                    int index = name.indexOf(".");

                    if (index != -1)
                        name = name.substring(0, index);

                    out.add("com.basis100.deal.calc.impl." + name);
                }
            }
        }

        return out;
    }

    /*
     * get the Calc Set from Directory
     */
    private Set fromDirectory(String dir) throws FileNotFoundException,
            URISyntaxException, IOException {

        // get the class name
        String simpleClassName = this.getClass().getSimpleName();
        _log.info("CalcSet className: " + simpleClassName);

        dir = dir.replace(simpleClassName + ".class", "impl");
        _log.info("CalcSet lsLocation: " + dir);

        URL directoryUrl = new URL(dir);

        Set out = new HashSet();
        File f = new File(directoryUrl.toURI());
        String[] filenames = f.list();

        for (int i = 0; i < filenames.length; i++) {
            String file = filenames[i];
            int index = file.indexOf('.');
            String calc = file.substring(0, index);
            out.add("com.basis100.deal.calc.impl." + calc);
        }

        return out;
    }

    public int getSize() {
        return internal.size();
    }

    public Set getCalcSet() {
        return internal;
    }

    public String[] getAsStringArray() {
        String[] out = new String[internal.size()];
        Iterator it = internal.iterator();
        int index = 0;

        while (it.hasNext()) {
            out[index++] = (String) it.next();
        }

        return out;
    }

}
