package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: TeaserPandIComponent
 * </p>
 * <p>
 * Description:Calculates the The P and I amount for Components
 * </p>
 * Calculation Number- Calc-85.C
 * @version 1.0 XS_11.8 10-July-2008 Initial Version
 * @version 1.1 XS_11.8 23-July-2008 Refactored doCalc
 */
public class TeaserMaturityDateComponent extends DealCalc {

	final long MillSecsInWeek = 604800000; //7 * 24 * 60 * 60 * 10000

	/**
	 * <p>
	 * Description: Default constructor that initalize the parameters and sets
	 * the calculation number
	 * </P>
	 * @throws Exception -
	 *             exception object
	 * @version 1.0 XS_11.8 10-July-2008 Initial Version
	 */
	public TeaserMaturityDateComponent() throws Exception {
		super();
		initParams();
		this.calcNumber = "Calc 86.C";
	}

	/**
	 * <p>
	 * Description:Given an input object this method determines the appropriate
	 * target object and adds the object to it's list of targets (targets).
	 * </p>
	 * @param input -
	 *            Input Object
	 * @param dcm -
	 *            deal calculation monitor object
	 * @throws TargetNotFoundException -
	 *             target not found exception object
	 * @version 1.0 XS_11.8 10-July-2008 Initial Version
	 */
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
		try {
			int classId = ((DealEntity) input).getClassId();

			if (classId == ClassId.COMPONENT) {
				Component component = (Component) input;
				// find component for the given componentId and
				addTarget(component);
			} else if (classId == ClassId.COMPONENTMORTGAGE) {
				ComponentMortgage componentMtg = (ComponentMortgage) input;
				// find component for the given componentId and
				// copyId
				Component component = (Component) entityCache.find(srk,
						new ComponentPK(componentMtg.getComponentId(),
								componentMtg.getCopyId()));
				addTarget(component);
			} else if (classId == ClassId.COMPONENTLOC) {
				ComponentLOC componentLoc = (ComponentLOC) input;
				// find component for the given componentId and
				// copyId
				Component component = (Component) entityCache.find(srk,
						new ComponentPK(componentLoc.getComponentId(),
								componentLoc.getCopyId()));
				addTarget(component);
			} else if (classId == ClassId.COMPONENTLOAN) {
				ComponentLoan componentLoan = (ComponentLoan) input;
				// find component for the given componentId and
				// copyId
				Component component = (Component) entityCache.find(srk,
						new ComponentPK(componentLoan.getComponentId(),
								componentLoan.getCopyId()));
				addTarget(component);
			}

		} catch (Exception e) {
			StringBuffer msg = new StringBuffer(100);
			msg.append("Failed to get Targets for: ");
			msg.append(this.getClass().getName());
			msg.append(" :[Input]: ");
			msg.append(input.getClass().getName());
			msg.append("  Reason: " + e.getMessage() + " @input: "
					+ this.getDoCalcErrorDetail(input));
			logger.error(msg.toString());
			throw new TargetNotFoundException(msg.toString());
		}
	}

	/**
	 * <p>
	 * Description: Runs the calculation on the target objects
	 * </p>
	 * @version 1.0 XS_11.8 10-July-2008 Initial Version
	 * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
	 * @param target
	 *            Target object
	 * @throws Exception -
	 *             exception object
	 */
	public void doCalc(Object target) throws Exception {

		trace(this.getClass().getName());

		Component targetComponent = (Component) target;

		PricingRateInventory priInventory = new PricingRateInventory(srk,
				targetComponent.getPricingRateInventoryId());
		int teaserTerm = priInventory.getTeaserTerm();

		try {
			Calendar maturityDate = Calendar.getInstance(Locale.getDefault());

			//Checking the Teaser Term,if it is greater than 0 only then calculation should execute.
			if (teaserTerm > 0) {

				Date IAD = null;
				int paymentFrequencyId = 0;

				int componentTypeId = targetComponent.getComponentTypeId();

				if (componentTypeId == Mc.COMPONENT_TYPE_MORTGAGE) {
					ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
							.find(srk, new ComponentMortgagePK(targetComponent
									.getComponentId(), targetComponent
									.getCopyId()));
					IAD = componentMtg.getInterestAdjustmentDate();
					paymentFrequencyId = componentMtg.getPaymentFrequencyId();

				} else if (componentTypeId == Mc.COMPONENT_TYPE_LOC) {
					ComponentLOC componentLoc = (ComponentLOC) this.entityCache
							.find(srk, new ComponentLOCPK(targetComponent
									.getComponentId(), targetComponent
									.getCopyId()));
					IAD = componentLoc.getInterestAdjustmentDate();
					paymentFrequencyId = componentLoc.getPaymentFrequencyId();

				} else if (componentTypeId == Mc.COMPONENT_TYPE_LOAN) {
					ComponentLoan componentLoan = (ComponentLoan) this.entityCache
							.find(srk, new ComponentLoanPK(targetComponent
									.getComponentId(), targetComponent
									.getCopyId()));

					IAD = componentLoan.getInterestAdjustmentDate();
					paymentFrequencyId = componentLoan.getPaymentFrequencyId();

				}
				trace("PaymentFrequencyId = " + paymentFrequencyId);
				trace("ActualPaymentTerm = " + teaserTerm);
				if (IAD != null) {
					maturityDate.setTime(IAD);
					int dayOfWeek = maturityDate.get(Calendar.DAY_OF_WEEK); //IAD Date WeekDay

					Date IADdt = maturityDate.getTime(); //IAD date variable 

					// If true skip the following calc and set MaturityDate to IAD + term months
					if ((PropertiesCache.getInstance().getProperty(
							targetComponent.getInstitutionProfileId(),
							"com.basis100.calc.forcematuritydatetomonthlyequ",
							"N")).equals("Y")) {
						trace("MaturityDate (Force to Monthly Equvalent = "
								+ maturityDate.getTime());
						maturityDate.add(Calendar.MONTH, teaserTerm);
						targetComponent.setTeaserMaturityDate(maturityDate
								.getTime());
						targetComponent.ejbStore();
						return;
					}

					maturityDate.add(Calendar.MONTH, teaserTerm);
					targetComponent.setTeaserMaturityDate(maturityDate
							.getTime());
					int dayOfWeek2 = maturityDate.get(Calendar.DAY_OF_WEEK);//IAD + Term date day

					// (Accelerated) Weekly, BiWeekly, Make sure same weekday.
					if (oneOf(paymentFrequencyId, Mc.PAY_FREQ_WEEKLY,
							Mc.PAY_FREQ_ACCELERATED_WEEKLY,
							Mc.PAY_FREQ_BIWEEKLY,
							Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)) {
						int dayDifferent = dayOfWeek - dayOfWeek2; // difference between the weekdays
						if (dayOfWeek != dayOfWeek2) // Making the day same for both IAD & MaturityDate
						{
							maturityDate.add(Calendar.DATE, dayDifferent);
						}
						if (dayDifferent > 0) {
							maturityDate.add(Calendar.DATE, -7);
						}

					}
					trace("MaturityDate = " + maturityDate.getTime());

					// If Bi-Weekly, make sure Weeks between IAD and CalculatedMaturidyDate is even
					if (oneOf(paymentFrequencyId, Mc.PAY_FREQ_BIWEEKLY,
							Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)) {
						double weekDiff = (((maturityDate.getTime().getTime() - IADdt
								.getTime())) * (1.0d))
								/ MillSecsInWeek;

						if (Math.round(weekDiff) % 2 != 0) {
							maturityDate.add(Calendar.DATE, -7);

						}
					}
					trace("MaturityDate = " + maturityDate.getTime());
					targetComponent.setTeaserMaturityDate(maturityDate
							.getTime());
					targetComponent.ejbStore();

					return;
				}

			} else {
				targetComponent.setTeaserMaturityDate(null);
				targetComponent.ejbStore();
			}
		} catch (Exception e) {
			StringBuffer msg = new StringBuffer(100);
			msg.append("Begin Failure in Calculation");
			msg.append(this.getClass().getName());
			msg.append(":[Target]:");
			msg.append(target.getClass().getName());
			msg.append("  Reason: ");
			msg.append(e.getMessage());
			msg.append("@Target: ");
			msg.append(this.getDoCalcErrorDetail(target));
			logger.debug(msg.toString());
		}

		targetComponent.setTeaserMaturityDate(DEFAULT_FAILED_DATE);
		targetComponent.ejbStore();

	}

	/**
	 * <p>
	 * Description: This method specifies input and target parameters.
	 * </p>
	 * @version 1.0 XS_11.8 10-July-2008 Initial Versionn
	 */
	private void initParams() throws ParamTypeException {
		targetParam = new ArrayList();
		inputParam = new ArrayList();
		inputClassTypes = new HashSet();

		addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
		addInputParam(new CalcParam(ClassId.COMPONENT, "pricingRateInventoryId"));
		addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
				"interestAdjustmentDate"));
		addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
				"paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOC,
				"interestAdjustmentDate"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOC, "paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
				"interestAdjustmentDate"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "paymentFrequencyId"));

		addTargetParam(new CalcParam(ClassId.COMPONENT, "teaserMaturityDate"));
	}
}
