/**
 * Title:        PandIMonthly (Calc-105)
 * Description:  Calculation for P&I Payment Amount Monthly
 * Copyright:    Copyright (c) 2001
 * Company:      Basis100
 * Date :        27 Nov 2001
 * @author Billy Lam
 * 
 * 26/Jan/2010, 4.3GR, FXP23563, FXP27174 changed drastically
 */

package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.pk.PricingProfilePK;

public class PandIMonthly extends DealCalc
{
public PandIMonthly() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 105";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
       addTarget( input );
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception {
      trace(this.getClass().getName());
      Deal deal = (Deal) target;

      try {

          int institutionProfileId = deal.getInstitutionProfileId();
          double totalLoanAmount = deal.getTotalLoanAmount();
          double netInterestRate = deal.getNetInterestRate();
          int amortizationMonths = deal.getAmortizationTerm();
          int repaymentTypeId = deal.getRepaymentTypeId();

          MtgProd mtgProd = (MtgProd) entityCache.find(srk, new MtgProdPK(
              deal.getMtgProdId()));
          int interestCompoundingId = mtgProd.getInterestCompoundingId();

          String rateCode = null;
          try{
              rateCode = mtgProd.getPricingProfile().getRateCode();
          }catch(Exception e){
              logger.error(e);
              rateCode = "";
          }

          double monthlyPandI = DealCalcUtil.calcPandIFully(
              institutionProfileId, totalLoanAmount, Mc.PAY_FREQ_MONTHLY,
              netInterestRate, amortizationMonths, repaymentTypeId,
              interestCompoundingId, rateCode);

          // rounding
          monthlyPandI = Math.round(monthlyPandI * 100d) / 100d;

          deal.setPandIPaymentAmountMonthly(validate(monthlyPandI));
          deal.ejbStore();
          return;
          
      } catch (Exception e) {
          String msg = "Benign Failure in Calculation"
              + this.getClass().getName();
          msg += ":[Target]:" + target.getClass().getName();
          msg += "\nReason: " + e.getMessage();
          msg += "@Target: " + this.getDoCalcErrorDetail(target);
          logger.error(msg);
          logger.error(e);
      }

      deal.setPandIPaymentAmountMonthly(this.DEFAULT_FAILED_DOUBLE);
      deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "interestCompoundId"));//4.3GR 

    addTargetParam(new CalcParam(ClassId.DEAL, "PandIPaymentAmountMonthly", T13D2));

  }

}
