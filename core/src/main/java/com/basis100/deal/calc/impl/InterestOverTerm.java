package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.PropertiesCache;

/**
 * 
 * <p>Title: InterestOverTerm</p>
 *
 * <p>Description: InterestOverTerm.java</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.1 <br>
 * Date: 11/8/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: <br>
 * 	Fixed calculation to follow specs, defect 1454
 */
public class InterestOverTerm extends DealCalc {
	/**
	 * 
	 * A constructor for this class.
	 * 
	 * @throws Exception
	 */
	public InterestOverTerm() throws Exception {
		super();
		initParams();
		this.calcNumber = "Calc 112";
	}

	/**
	 * method: getTargets description: returns the Collection object which
	 * contains all the defined target for the InterestOverTerm Class
	 * 
	 * @throws -
	 */
	public Collection getTargets() {
		return this.targets;
	}

	/**
	 * method: getTarget description: Method add the targets into the Calc Class
	 * 
	 * @param: Object, CaclMonitor
	 * @throws TargetNotFoundException
	 */
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
		try {
			addTarget(input);
		} catch (Exception e) {
			String msg = "Fail to get Targets" + this.getClass().getName();
			msg += ":[Input]:" + input.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@input: " + this.getDoCalcErrorDetail(input);
			logger.error(msg);
			throw new TargetNotFoundException(msg);
		}
	}

	/**
	 * method: hasInput description: Checks whether the required input exists or
	 * not.
	 * 
	 * @param CalcParam
	 * @return: Boolean
	 */
	public boolean hasInput(CalcParam input) {
		return this.inputParam.contains(input);
	}

	/**
	 * method: doCalc description: Performs the required business calculations.
	 * 
	 * @param: Object
	 * @throws: Exception
	 */
	public void doCalc(Object target) throws Exception {
		trace(this.getClass().getName());
		Deal deal = (Deal) target;
		try {
		  	double actualPaymentTerm = deal.getActualPaymentTerm();
		  	int paymentFrequencyId = deal.getPaymentFrequencyId();
		  	double paymentAmount = deal.getPandiPaymentAmount();
		  	double totalLoanAmount = deal.getTotalLoanAmount();
		  	double balanceRemaining = deal.getBalanceRemainingAtEndOfTerm();
		  	
			String decimalYear = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
					"com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
			double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
				
			String DJNumberOfPayments = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.filogix.calc.DJNumberOfPayments", "N");
			
//			calculate total payments and truncate to an integer
			double paymentPerYear;			
			if (DJNumberOfPayments.equalsIgnoreCase("Y")) {
			  paymentPerYear = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId);
			}
			else  {
			  paymentPerYear = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId, mDaysInYear);
			}

			double totalPayments =  actualPaymentTerm / 12  * paymentPerYear;
			int totalPaymentsTruncated = (int) totalPayments;
			
			double interestOverTerm = (paymentAmount * totalPaymentsTruncated) - (totalLoanAmount - balanceRemaining);			
			deal.setInterestOverTerm(interestOverTerm);
			deal.ejbStore();
			
			return;
		} catch (Exception e) {
			String msg = "Benign Failure in Calculation"
					+ this.getClass().getName();
			msg += ":[Target]:" + target.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@Target: " + this.getDoCalcErrorDetail(target);
			logger.debug(msg);
		}
	}

	/**
	 * 
	 * initParams Method description: Initializes all the required input and
	 * output(targets) parameters
	 * 
	 * @throws ParamTypeException
	 */
	private void initParams() throws ParamTypeException {
		targetParam = new ArrayList();
		inputParam = new ArrayList();
		inputClassTypes = new HashSet();

		addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "balanceRemainingAtEndOfTerm"));
		addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.DEAL, "actualPaymentTerm"));
		addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));

		addTargetParam(new CalcParam(ClassId.DEAL, "interestOverTerm"));
	}

}
