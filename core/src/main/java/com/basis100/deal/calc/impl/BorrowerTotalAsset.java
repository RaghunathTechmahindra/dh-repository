package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

public class BorrowerTotalAsset extends DealCalc
{


  public BorrowerTotalAsset() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 52";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {
       Asset a = (Asset)input;
       Borrower bt = (Borrower) entityCache.find ( srk, 
                              new BorrowerPK(a.getBorrowerId (), 
                                             a.getCopyId ()));
       addTarget(bt);
     }
     catch(Exception e)
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Borrower bt = (Borrower)target;

     try
     {
       double result = 0;
       Collection assets = this.entityCache.getAssets(bt);

       if ( assets != null )
       {
           Iterator ai = assets.iterator();
           Asset current = null;

           while(ai.hasNext())
           {
             current = (Asset)ai.next();
             //--Ticket#924--start--//
             // Even if an asset is not included in Net Worth, it is still an asset,
             // and should be reported as such.
             //if(current.getIncludeInNetWorth() == true)
             //{
               result += current.getAssetValue();
               trace("totalAsset = " + result );
             //}
             //--Ticket#924--end--//
             //else
             //{
             //   trace("Asset.getIncludeInNetWorth()() = " + current.getIncludeInNetWorth()
             //                +  "\nNo need to calculate total asset");
             //}
             //--Ticket#924--end--//
           }
        }

       result = Math.round ( result * 100 ) / 100.00 ;
       bt.setTotalAssetAmount(validate(result));
       bt.ejbStore();
       return;
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     bt.setTotalAssetAmount(this.DEFAULT_FAILED_DOUBLE);
     bt.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.ASSET, "assetValue"));
    addInputParam(new CalcParam(ClassId.ASSET, "includeInNetWorth"));

    addTargetParam(new CalcParam(ClassId.BORROWER, "totalAssetAmount",T13D2));
  }



}
