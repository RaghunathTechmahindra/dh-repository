package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.resources.PropertiesCache;

//Name: Interest Adjustment Date (IAD)
//Code: Calc-11
//06/30/2000

/**
 * <p>
 * Title: InterestAdjustmentDate
 * </p>
 * <p>
 * Description:Calculates Interest Adjustment Date
 * </p>
 * @version 1.0 Initial Version <br>
 * @version 1.1 XS_11.17 Initial Version <br> - updated
 *          initParams(),getTarget(),doCalc()
 * @author MCM Impl Team <br>
 * @Date 04-Jul-2008 <br>
 *       Code Calc-11 <br>
 */
public class InterestAdjustmentDate extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 Initial Version
     */
    public InterestAdjustmentDate() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 11";
    }

    public Collection getTargets()
    {
        return this.targets;
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.17 | updated Exception
     */
    public void getTarget(Object input, CalcMonitor dcm)
    throws TargetNotFoundException
    {
        try
        {
            addTarget(input); // Only deal comes here.
        }
        catch (Exception e)
        {
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     * @version 1.1 XS_11.17 | Updated 
     *        -  Calc logic for Deal level product is an MCM or a line of credit
     *        -  Exception
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        Deal deal = (Deal) target;

        MtgProd mtgProd = deal.getMtgProd();

        try
        {
            Calendar firstDate = Calendar.getInstance();

            String componentEligibleFlag = mtgProd.getComponentEligibleFlag();
            int underwriteAsTypeId = mtgProd.getUnderwriteAsTypeId();

            // If the Deal level product is an MCM or a line of credit then the deal IAD will default to the estimated closing
            if (("Y").equals(componentEligibleFlag) || underwriteAsTypeId == Mc.COMPONENT_TYPE_LOC)
            {
                Date estimatedClosingDate = deal.getEstimatedClosingDate();
                if (estimatedClosingDate == null)
                    throw new Exception(
                    "Required Input = null -> deal.getEstimatedClosingDate ().");

                deal.setInterimInterestAdjustmentDate(estimatedClosingDate);
                deal.ejbStore();
                return;
            }
            // If the Deal level product is NOT an MCM or a line of credit then the deal IAD is calculated as follows
            else
            {
                // New requirement for Force IAD to 1st of Month if property set
                // to Y -- By Billy 27Nov2001
                // This property must be same among institutions in a instance
                // and in DB - FXP21309
                boolean force = PropertiesCache.getInstance().getProperty(
                        deal.getInstitutionProfileId(),"com.basis100.calc.forceiadtofirstofmonth", "N").equals("Y");
                if ((PropertiesCache.getInstance().getProperty(deal
                        .getInstitutionProfileId(),"com.basis100.calc.forceiadtofirstofmonth", "N")).equals("Y"))
                {
                    // if Estimated Closing date not set ==> Exception
                    Date d = deal.getEstimatedClosingDate();

                    if (d == null)
                        throw new Exception("Required Input = null -> deal.getEstimatedClosingDate().");

                    // Get the day to Back day from Properties
                    int theBackDay = 1;
                    try                    {
                        theBackDay = Integer.parseInt(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                                 "com.basis100.calc.forceiadtofirstofmonth.daytobackdate","1"));
                    }
                    catch (Exception e)
                    {
                        trace("Invalid DaybackDate parameter ==> set to default = 1");
                    }

                    trace("The BackDay = " + theBackDay);

                    firstDate.setTime(d);

                    // --> Change request #126
                    // --> Check if Special Handling required for Xceed
                    // --> By Billy 12Nov2003
                    // Get Payment Frequency
                    int pf = deal.getPaymentFrequencyId();
                    if ((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.calc.forceiadtofirstofmonth.specialforxd","N")).equals("N") || 
                             pf == Mc.PAY_FREQ_MONTHLY)
                    {
                        // Set the result Day to the first day of month
                        firstDate.set(Calendar.DAY_OF_MONTH, 1);
                        // Get day field from Est closing date
                        int estDate = d.getDate();
                        if (estDate > theBackDay)
                        {
                            // Set IAD to 1st day of next Month of Est. Closing Date
                            firstDate.add(Calendar.MONTH, 1);
                        }
                    }
                    else
                    {
                        // Special handle for Xceed to set
                        trace("Perform special handling for Xceed.");
                        // Get current Day Of Week
                        int daysFromFri = Calendar.FRIDAY - firstDate.get(Calendar.DAY_OF_WEEK);
                        trace("# of Days from Closing Friday = " + daysFromFri);
                        firstDate.add(Calendar.DAY_OF_MONTH, daysFromFri);
                    }

                    trace("The result IAD = " + firstDate.toString());

                    deal.setInterimInterestAdjustmentDate(firstDate.getTime());
                }
                else
                {
                    if (deal.getFirstPaymentDate() == null)
                    { // no First Payment Date set
                        Date d = deal.getEstimatedClosingDate();

                        if (d == null)
                            throw new Exception("Required Input = null -> deal.getEstimatedClosingDate ().");

                        deal.setInterimInterestAdjustmentDate(d);
                    }
                    else
                    {
                        int period = 0;
                        int pf = deal.getPaymentFrequencyId();
                        firstDate.setTime(deal.getFirstPaymentDate());

                        if (pf == Mc.PAY_FREQ_MONTHLY)
                        {
                            firstDate.add(Calendar.MONTH, -1); // back one month
                        }
                        else
                        {
                            if (pf == Mc.PAY_FREQ_SEMIMONTHLY)
                                period = 15;
                            else if (pf == Mc.PAY_FREQ_BIWEEKLY || pf == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
                                    period = 14;
                                else if (pf == Mc.PAY_FREQ_WEEKLY || pf == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
                                        period = 7;
                            firstDate.add(Calendar.DAY_OF_MONTH, -period);
                        }
                        deal.setInterimInterestAdjustmentDate(firstDate.getTime());
                    }
                }

                deal.ejbStore();
                return;
            }
        }
        catch (Exception e)
        {
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

        deal.setInterimInterestAdjustmentDate(DEFAULT_FAILED_DATE);
        deal.ejbStore();

    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 - Added mtgProdId as input parameter.
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));

        /*****************MCM Impl Team STARTS XS_11.17*******************/
        addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
        /*****************MCM Impl Team ENDS XS_11.17*******************/

        //FXP20744, deleted 
        // This property must be same among institutions in a instance and in DB - FXP21309
        if ((PropertiesCache.getInstance().getProperty(-1,
                "com.basis100.calc.forceiadtofirstofmonth", "N")).equals("N"))
        {
            // Not include firstPaymentDate to avoid dead-lock if ForceIADtoFirstToMonth = Y
            addInputParam(new CalcParam(ClassId.DEAL, "firstPaymentDate"));
        }

        addTargetParam(new CalcParam(ClassId.DEAL,"interimInterestAdjustmentDate"));

    }

}
