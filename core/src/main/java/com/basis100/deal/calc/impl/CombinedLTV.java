package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

// Name: Combined LTV Ratio
// Code: Calc 20
// 06/30/2000

public class CombinedLTV extends DealCalc
{
  public CombinedLTV() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 20";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
       if(input instanceof Deal)
       {
         addTarget(input);
         return;
       }

       if(input instanceof Property)
       {
         Property p = (Property)input;
         Deal deal = (Deal) entityCache.find (srk, 
                           new DealPK( p.getDealId(), p.getCopyId ())) ;
         addTarget( deal );
       }

       //--> Debug by Billy 15April2003
       if(input instanceof Liability)
       {
          Liability l = (Liability) input;
          Borrower b = (Borrower) entityCache.find(srk, 
                               new BorrowerPK(l.getBorrowerId (), 
                                              l.getCopyId ()) );
          Deal d = (Deal) entityCache.find (srk, 
                              new DealPK( b.getDealId(), b.getCopyId ()));
          addTarget (d);
        }
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Deal deal = ( Deal ) target ;

     try
     {
        double combinedLTVRatio = 0.0;
        double totalEquityMortgageAmount = 0.0;

        Collection liabilities = this.entityCache.getLiabilities ( deal ) ;
        if ( liabilities != null )
        {
            Iterator itLiab = liabilities.iterator () ;
            Liability liability = null;
            while ( itLiab.hasNext() )
            {
                liability = (Liability) itLiab.next () ;
                if ( liability.getLiabilityTypeId ()== 1 )  // Equity Mortgage
                    totalEquityMortgageAmount += liability.getLiabilityAmount () ;
            }
         }  // .. end .. calculate totalEquityMortgageAmount ...

        trace( " Deal.specialFeatureId = " + deal.getSpecialFeatureId () );
        trace( " Deal.LienPositionId = " + deal.getLienPositionId() );

        if ( deal.getSpecialFeatureId () == 1  ) //"PreApproval"
        {
            if ( deal.getPAPurchasePrice () > 0 )
            {
                 combinedLTVRatio = ( deal.getNetLoanAmount () + totalEquityMortgageAmount )
                      / deal.getPAPurchasePrice ();
                 combinedLTVRatio = Math.round ( combinedLTVRatio * 10000 ) / 100.00;
            }
            else
                 combinedLTVRatio = 0.0 ;
        }   // end of deal.SpecialFeature == "Pre-Approval"
        else
        //--> New requirement to include Equity Mortgage if 2nd Mortgage
        //--> Requested by Product and modified by Billy 02April2003
        if (deal.getLienPositionId() == 1 ) //"Secound Mortgage"
        {
            if ( deal.getCombinedLendingValue() > 0 )
            {
                 combinedLTVRatio = ( deal.getNetLoanAmount () + totalEquityMortgageAmount )
                      / deal.getCombinedLendingValue();
                 combinedLTVRatio = Math.round ( combinedLTVRatio * 10000 ) / 100.00;
            }
            else
                 combinedLTVRatio = 0.0 ;
        }   // end of deal.LienPositionId == "Secound Mortgage"
        else
        {
            // !!!! temp taken out, due to caching problem.
            //Collection properties = this.entityCache.getProperties ( deal ) ;
            Collection properties = deal.getProperties () ;

            if ( properties.size () == 1 )
            {
                if ( properties != null )
                {
                    Iterator itProp = properties.iterator () ;
                    if ( itProp.hasNext() )
                    {
                        Property property = (Property) itProp.next ();
                        combinedLTVRatio = property.getLoanToValue ();
                    }
                }
            }
            else
            {
                if ( deal.getCombinedLendingValue () != 0 )
                {
                	if(deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS)
                	{
                		combinedLTVRatio = ( deal.getNetLoanAmount () + totalEquityMortgageAmount )
                			/ (deal.getCombinedLendingValue() + deal.getRefiImprovementAmount());
                	}
                	else
                	{
                    combinedLTVRatio = ( deal.getNetLoanAmount () + totalEquityMortgageAmount )
                        / deal.getCombinedLendingValue () ;
                	}
                    combinedLTVRatio = Math.round ( combinedLTVRatio * 10000 ) / 100.00 ;
                }
                else
                  combinedLTVRatio = this.getMaximumCalculatedValue();
            }
        } // end of deal.SpecialFeature != "Pre-Approval"
        combinedLTVRatio = Math.round ( combinedLTVRatio * 100 ) / 100.00 ;
        deal.setCombinedLTV (validate(combinedLTVRatio));
        deal.ejbStore ();
        return;
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    deal.setCombinedLTV (this.DEFAULT_FAILED_DOUBLE);
    deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "specialFeatureId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAPurchasePrice"));
    addInputParam(new CalcParam(ClassId.DEAL, "netLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL , "combinedLendingValue")) ;
    addInputParam(new CalcParam(ClassId.DEAL , "numberOfProperties")) ;
    addInputParam(new CalcParam(ClassId.DEAL, "refiImprovementAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealPurposeId"));
    addInputParam(new CalcParam(ClassId.DEAL , "lienPositionId"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "loanToValue"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "liabilityAmount"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedLTV", 999.99));
  }
}
