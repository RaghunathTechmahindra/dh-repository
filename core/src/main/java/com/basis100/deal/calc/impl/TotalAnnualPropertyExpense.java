package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

//Name: Total Annual Property Expenses
//Code: Calc 68
public class TotalAnnualPropertyExpense extends DealCalc
{


  public TotalAnnualPropertyExpense()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 68";
  }

  
  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof Deal )
          addTarget ( input );
    }
    catch ( Exception e )
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());
     Deal deal = ( Deal ) target;

     try
     {

       deal.setTotalPropertyExpenses ( validate(deal.getCombinedTotalPropertyExp () * 12));
       deal.ejbStore ();
       return;
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
        
     }

     deal.setTotalPropertyExpenses(DEFAULT_FAILED_DOUBLE);
     deal.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();
    /*
    addInputParam(new CalcParam(ClassId.DEAL, "combinedTotalPropertyExp"));

    addTargetParam(new CalcParam(ClassId.DEAL, "totalPropertyExpenses"));
    */
  }



} 
