/*
 * @(#)NetRateComponentLoan.java Jun 09, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentPK;

/**
 * <p>
 * Title: NetRateComponentLoan
 * </p>
 * <p>
 * Description: Calculates the Net Interest Rate for Loan Components of
 * "component eligible" deals
 * </p>
 * Code Calc-2.CLoan
 * @version 1.0 XS_11.14 02-July-2008 Initial Version
 * @version 1.1 Modified the getTarget(..) method
 * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
 */
public class NetRateComponentLoan extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.14 02-July-2008 Initial Version
     */
    public NetRateComponentLoan() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 2.CLoan";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.14 02-July-2008 Initial Version
     * @version 1.1 XS_11.14 08-Jul-2008 Added ComponentTypeId check
     * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOAN)
            {
                ComponentLoan componentLoan = (ComponentLoan) input;
                addTarget(componentLoan);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    Component component = (Component) input;
                    //MCM team added try-catch
                    try {
                        if(component.getComponentTypeId()== Mc.COMPONENT_TYPE_LOAN){
                            // find componentLoan for the given componentId and
                            // copyId
                            ComponentLoan componentLoan = (ComponentLoan) entityCache
                                    .find(srk, new ComponentLoanPK(component
                                            .getComponentId(), component.getCopyId()));
                            addTarget(componentLoan);
                        }
                    } catch (Exception e) {
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.14 02-July-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        if (target == null) return;
        trace(this.getClass().getName());
        try
        {
            ComponentLoan targetComponentLoan = (ComponentLoan) target;
            // Retrive component Object from the given target
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLoan.getComponentId(),
                            targetComponentLoan.getCopyId()));

            double componentLoanNetRate = component.getPostedRate()
                    - targetComponentLoan.getDiscount()
                    + targetComponentLoan.getPremium();
            // if the NetInterestRate is >999999.999999 then Validate will round
            // of to the max given value in the TargetParam
            targetComponentLoan
                    .setNetInterestRate(validate(componentLoanNetRate));

            targetComponentLoan.ejbStore();

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.14 02-July-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "postedRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "discount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "premium"));
        // Max NetInterestRate =999999.999999
        addTargetParam(new CalcParam(ClassId.COMPONENTLOAN, "netInterestRate",
                T12D6));
    }

}
