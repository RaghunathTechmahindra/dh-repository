package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name: BorrowerGDS
//Code: Calc 48
//07/04/2000

public class BorrowerGDS extends DealCalc
{

  public BorrowerGDS() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 48";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower borrower = (Borrower)entityCache.find(srk, 
                               new BorrowerPK(i.getBorrowerId(), 
                                              i.getCopyId()));
        addTarget(borrower);
        return;
      }

      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower borrower = (Borrower)entityCache.find(srk, 
                                 new BorrowerPK(l.getBorrowerId(), 
                                                l.getCopyId()));
        addTarget(borrower);
        return;
      }

      Deal d = null;
      if(input instanceof PropertyExpense)
      {
        PropertyExpense propertyExpense = (PropertyExpense)input;
        Property property = (Property)entityCache.find(srk, new PropertyPK(propertyExpense
        		.getPropertyId(), propertyExpense.getCopyId()));

        d = (Deal)entityCache.find(srk, new DealPK(property
        		.getDealId(), property.getCopyId()));
      }
      else if(input instanceof Deal)
      {
        d = (Deal)input;
      }

      Collection bors = entityCache.getBorrowers(d);
      if(bors != null)
      {
        Iterator it = bors.iterator();
        while(it.hasNext())
        {
          addTarget(it.next());
        }
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }

  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Borrower borrower = (Borrower)target;

    try
    {
      Deal deal = (Deal)entityCache.find(srk, new DealPK(borrower
    		  .getDealId(), borrower.getCopyId()));

      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));

      double dbGDSRatio = 0.0;
      String rateCode = pricingProfile.getRateCode();

      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        { // All the GDS = 0
          borrower.setGDS(0);
          borrower.ejbStore();
          return;
        }
      }

      //-------------Get the property Expensse , this is same for all Borrowers ----------
      Collection pe = this.entityCache.getPropertyExpenses(deal);
      double totalPropertyExpense = 0.0;
      if(pe != null)
      {
        Iterator itPe = pe.iterator();
        // ----- Get the Property Expense -----------
        PropertyExpense propertyExpense = null;
        while(itPe.hasNext())
        {
          propertyExpense = (PropertyExpense)itPe.next();
          if(propertyExpense.getPeIncludeInGDS())
          {
            totalPropertyExpense += propertyExpense.getPropertyExpenseAmount() *
              DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(), "PropertyExpensePeriod")
              * propertyExpense.getPePercentInGDS() / 100.00;
          }
          trace("totalPropertyExpense = " + totalPropertyExpense);
        } // End of Property Expense
      }
      trace("totalProperyExpense= " + totalPropertyExpense);
      //-----------  the borrower  for this deal ------------------------
      double dbTotalAnnualIncome = 0;
      double dbTotalGDSExpenses = totalPropertyExpense;
      // -----------------Work with Incomes----------------------
      Collection incomes = this.entityCache.getIncomes(borrower);
      if(incomes != null)
      {
        Iterator itIn = incomes.iterator();
        Income income = null;
        while(itIn.hasNext()) // Loop through all the  incomes
        {
          income = (Income)itIn.next();
          // if Included in GDS
          //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
          //--> By Billy 02Nov2004
          if(income.getIncIncludeInGDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
          {
            dbTotalAnnualIncome = dbTotalAnnualIncome + income.getIncomeAmount() *
              DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod")
              * income.getIncPercentInGDS() / 100.00;
          }
          //===============================================================================
          if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
          {
            dbTotalGDSExpenses = dbTotalGDSExpenses - income.getIncomeAmount() *
              DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod")
              * income.getIncPercentInGDS() / 100.00;
          }
          trace("totalAnnualIncome = " + dbTotalAnnualIncome);
          trace(" totalGDSExpenses = " + dbTotalGDSExpenses);
        }
      }
      // ------------------Work with Liabilities---------------------
      Collection liabilities = this.entityCache.getLiabilities(borrower);
      if(liabilities != null)
      {
        Iterator itLi = liabilities.iterator();
        Liability liability = null;
        while(itLi.hasNext()) //Loop through all the  liabilities
        {
          liability = (Liability)itLi.next();
          if(liability.getIncludeInGDS() && (liability.getLiabilityPayOffTypeId() == 0))
          { // Include in GST and Libility Payoff Type is Blank, ( id=0 )
            /*2"Credit Card"*/
            /*10 "Unsercured Line of Credit"*/
            /*13"Secured Line of Credit"*/
            // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
            // -------------------------------------------------------------------
            // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
            //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
            if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
            {
              dbTotalGDSExpenses += liability.getLiabilityAmount() *
                liability.getPercentInGDS() / 100.00 * 12;
            }
            else

            /*"Closing Costs" */
            /*"high ratio"*/
            if((liability.getLiabilityTypeId() == 14) && (deal.getDealTypeId() == 1))
            {
              dbTotalGDSExpenses += liability.getLiabilityAmount();
            }
            else
            {
              dbTotalGDSExpenses += liability.getLiabilityMonthlyPayment() * 12
                * liability.getPercentInGDS() / 100.00;
            }
          }
          trace("TotalGDSExpense ( + liability ) = " + dbTotalGDSExpenses);
        } // -------- end of all liabilities ---------
      }
      // ------------------------Get the GDS---------------------
      dbTotalGDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, false);    //#DG538 allow accelerated precision
      trace("totalGDSExpenses = " + dbTotalGDSExpenses);
      //#DG538 dbTotalGDSExpenses += (deal.getPandiPaymentAmount() + deal.getAdditionalPrincipal())
        // * DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");

      trace("PandI= " + deal.getPandiPaymentAmount());
      trace("AdditionalPrincipal = " + deal.getAdditionalPrincipal());
      trace("PaymentFrequency = " +
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

      if(dbTotalAnnualIncome != 0)
      {
        dbGDSRatio = dbTotalGDSExpenses / dbTotalAnnualIncome * 100;
        dbGDSRatio = Math.round(dbGDSRatio * 100.00) / 100.00;
      }
      else
      {
        dbGDSRatio = getMaximumCalculatedValue();

      }
      borrower.setGDS(validate(dbGDSRatio));
      borrower.ejbStore();
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(target);
      logger.debug(msg);
    }

    borrower.setGDS(DEFAULT_FAILED_DOUBLE);
    borrower.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    //#DG538
    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIPaymentAmountMonthly"));
    //#DG538 end

    addTargetParam(new CalcParam(ClassId.BORROWER, "GDS", 999.99));
  }

}
