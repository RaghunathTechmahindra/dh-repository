package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Number of Properties
//Code: Clac-98

public class NumberOfBorrowers extends DealCalc {

 public NumberOfBorrowers () throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 98";
  }


  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof Borrower )
      {
          Borrower b = (Borrower) input;
          Deal deal = (Deal) entityCache.find (srk , 
                            new DealPK(b.getDealId (), b.getCopyId ())) ;
          addTarget( deal );
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;

    try
    {
        int numberOfBorrowers = 0;
        //Collection borrowers = this.entityCache.getBorrowers ( deal );
        Collection borrowers = deal.getBorrowers ();
        if ( borrowers != null )
        {
            Iterator itBorrowers = borrowers.iterator ();
            trace( "totalBorrower Number = " + borrowers.size() );
            while ( itBorrowers.hasNext () )
            {
                Borrower borrower = (Borrower) itBorrowers.next ();
                trace( "BorrowerTypeId = " + borrower.getBorrowerTypeId () );
                // Bug fix -- not counting Rel Covntr (2) as borrower -- Linda 04Jan2001
                if ( oneOf( borrower.getBorrowerTypeId () , 0, 3 )  ) /* Borrower, Builder */
                      numberOfBorrowers ++ ;
            }
        }
        deal.setNumberOfBorrowers ( validate( numberOfBorrowers ) );
        deal.ejbStore();
        return;
    }
    catch (Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

    deal.setNumberOfBorrowers ( DEFAULT_FAILED_INT );
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam( new CalcParam( ClassId.BORROWER  ,  "dealId")) ;
    addInputParam( new CalcParam( ClassId.BORROWER ,  "copyId")) ;
    addInputParam( new CalcParam( ClassId.BORROWER ,  "borrowerTypeId")) ;

    addTargetParam( new CalcParam( ClassId.DEAL , "numberOfBorrowers", 99 ));

  }

} 