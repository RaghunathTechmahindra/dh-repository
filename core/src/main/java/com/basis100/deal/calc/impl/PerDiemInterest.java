/**
 * Title: PerDiemInterest (Calc-102) Description: Calculation for Per Diem
 * Interest Amount Copyright: Copyright (c) 2001 Company: Basis100 Date : 26 Nov
 * 2001
 * @author Billy Lam
 * @version
 */

package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.picklist.PicklistData;

/**
 * <p>
 * Title: IADNumberOfDays
 * </p>
 * <p>
 * Description:Calculates IADNumberOfDays
 * </p>
 * @version 1.0 Initial Version <br>
 * @version 1.1 XS_11.18 Initial Version <br> - updated
 *          initParams(),getTarget(),doCalc()
 * @author MCM Impl Team <br>
 * @Date 09-Jul-2008 <br>
 *       Code Calc-102 <br>
 */
public class PerDiemInterest extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 Initial Version
     */
    public PerDiemInterest() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 102";
    }

    public Collection getTargets()
    {
        return this.targets;
    }


    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.18 | updated Exception
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            addTarget(input);
        }
        catch (Exception e)
        {
            // XS_11.18 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     * @version 1.1 XS_11.18 | Updated 
     *        -  Calc logic for Deal level product is a line of credit
     *        - Exception
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        Deal deal = (Deal) target;
        MtgProd mtgProd=deal.getMtgProd();
        
        try
        {
            // Get the InterestCompoundDesc from
            // deal.lenderprofile.interestcompoundId
            // added inst id for ML
            String InterestCompoundDesc = PicklistData.getMatchingColumnValue(
                    -1, "interestcompound", deal.getLenderProfile()
                            .getInterestCompoundId(),
                    "interestcompounddescription");
            double interestFactor = 0;
            int UnderwriteAsTypeId = mtgProd.getUnderwriteAsTypeId();
            
            //If the deal level product is a line of credit then perdiemInterestAmount  is set to 0.
            if(UnderwriteAsTypeId == Mc.COMPONENT_TYPE_LOC )
            {
                deal.setPerdiemInterestAmount(0);
                deal.ejbStore();
                return;
            }
            
            if (InterestCompoundDesc != null
                    && InterestCompoundDesc.trim().equals("2"))
            {
                interestFactor = DealCalcUtil.interestFactor(deal
                        .getNetInterestRate(), 365, InterestCompoundDesc);
            }
            else
                if (InterestCompoundDesc != null
                        && InterestCompoundDesc.trim().equals("12"))
                {
                    interestFactor = (deal.getNetInterestRate() / 100) / 365;
                }
                else
                {
                    interestFactor = (deal.getNetInterestRate() / 100) / 365;
                }

            trace("InterestCompoundDesc = " + InterestCompoundDesc
                    + ":: InterestFacotr = " + interestFactor);

            double theResult = deal.getTotalLoanBridgeAmount() * interestFactor;
            theResult = Math.round(theResult * 100) / 100.00;
            trace("Result (PerdiemInterestAmount) = " + theResult);
            deal.setPerdiemInterestAmount(validate(theResult));

            deal.ejbStore();
            return;
        }
        catch (Exception e)
        {
            // XS_11.18 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());

        }

        deal.setPandiPaymentAmount(this.DEFAULT_FAILED_DOUBLE);
        deal.ejbStore();
    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }
    
    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 Initial Version
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "totalLoanBridgeAmount"));
        addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
        addInputParam(new CalcParam(ClassId.DEAL, "lenderProfileId"));
        addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));

        addTargetParam(new CalcParam(ClassId.DEAL, "perdiemInterestAmount",T13D2));

    }

}