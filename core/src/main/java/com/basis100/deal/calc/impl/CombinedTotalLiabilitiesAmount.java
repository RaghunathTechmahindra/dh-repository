package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

import com.basis100.entity.*;

// Name: Combined Total Liabilities Amount
// Code: Calc 28
// 06/30/2000


public class CombinedTotalLiabilitiesAmount extends DealCalc {

  public CombinedTotalLiabilitiesAmount() throws Exception {
     super();
    initParams();
    this.calcNumber = "Calc 28";
  }


   public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        Liability l = (Liability) input;
        Borrower b = (Borrower) entityCache.
                             find(srk, new BorrowerPK(l.getBorrowerId (), 
                                                      l.getCopyId ()) );
        Deal d = (Deal) entityCache.
                             find (srk, new DealPK(b.getDealId(), b.getCopyId()));
        addTarget (d);
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal) target;

    try
    {
      double totalLiabilities = 0.0;
      Collection liabilities  = null;

      liabilities = this.entityCache.getLiabilities (deal);
      Iterator itLi =  liabilities.iterator ();

      while ( itLi.hasNext () )
      {
         Liability liability = ( Liability) itLi.next ();
        
         if (liability.getIncludeInGDS() || liability.getIncludeInTDS()){
           trace("liability.getIncIncludeInGDS() = " + liability.getIncludeInGDS() +
                        " or liability.getIncInclueInTDS() = " +  liability.getIncludeInTDS() +
                        "\nCalculate totalLiabilities...");

           totalLiabilities += liability.getLiabilityAmount ();
           trace( "totalLiabilities = " + totalLiabilities );
         } else{
           trace("liability.getIncIncludeInGDS() = " + liability.getIncludeInGDS() +
                        " or liability.getIncInclueInTDS() = " +  liability.getIncludeInTDS() +
                        "\nNo need to calculate totalLiabilities");
         }
      } // -- ALl liabilities --
      totalLiabilities = Math.round ( totalLiabilities * 100 ) /100.00 ; 
      deal.setCombinedTotalLiabilities (validate(totalLiabilities) );
      deal.ejbStore ();
      return;
    }
    catch ( Exception e )
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
          
    }

   deal.setCombinedTotalLiabilities (this.DEFAULT_FAILED_DOUBLE );
   deal.ejbStore ();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.LIABILITY , "liabilityAmount"));

    addInputParam(new CalcParam(ClassId.LIABILITY , "includeInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "includeInTDS"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalLiabilities", T13D2));
  }


}
