package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.resources.PropertiesCache;
import com.filogix.util.Xc;

//Name:  Maxim Loan Limit
//Code:  Calc 77
//07/04/2000

public class MaximumLoanLimit extends DealCalc {

  public MaximumLoanLimit() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 77";
  }

   public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
      {
        if ( input instanceof Deal )
        {
          addTarget ( input);
          return;
        }  // Deal
        if ( input instanceof Property)
        {
          Property property = ( Property) input;

          Deal deal = (Deal) entityCache.find (srk,
                    new DealPK( property.getDealId (), property.getCopyId ()) ) ;
          addTarget ( deal );
        } // Property ===> Deal
      }
    catch ( Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());
     Deal deal = (Deal) target;
     try
     {
        double maxLoanLimit  = 0.0;
        trace( "DealTypeId = "   + deal.getDealTypeId () );
        if ( deal.getDealTypeId() == 0 /* Conventional */)
        {
          Collection properties = this.entityCache.getProperties (deal) ;
          Iterator itPR = properties.iterator () ;

          double combinedLendingValue = deal.getCombinedLendingValue () ;
          while ( itPR.hasNext () )
          {
            Property property = ( Property ) itPR.next () ;
            char primaryProperty = property.getPrimaryPropertyFlag () ;

            if ( property.getPropertyLocationId () == 0 /* Prime Location */
              && ( primaryProperty == 'Y' ) )
            {
                // "Condominium" "Strata" "Freehold Condo" "Leasenhold Condo" "Townhouse Condo"

                //3.2patch LTV project
            	double lLtv = Double.parseDouble(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), Xc.ING_LTV_THRESHOLD,"80"))/100;
                if(oneOf(property.getPropertyTypeId(), 2, 17, 14, 15, 16))
                {
                  if  (  combinedLendingValue > 300000  )
                      maxLoanLimit = 300000 * lLtv + ( combinedLendingValue - 300000) * 0.6 ;
                  else
                      maxLoanLimit = combinedLendingValue  * lLtv ;
                }
                else
                {
                  if ( combinedLendingValue > 500000 )
                     maxLoanLimit = ( 500000 * lLtv ) + ( combinedLendingValue - 500000) * 0.6;
                  else
                     maxLoanLimit = combinedLendingValue * lLtv ;
                }
                maxLoanLimit = Math.round ( maxLoanLimit * 100 ) /100.00 ;
                deal.setMaximumLoanAmount ( validate(maxLoanLimit) );
                deal.ejbStore ();
                return;
            }
           }//-- end of next property----
      } // ----- end of if DT Description = "COnventional"---------------
      return ;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

      deal.setMaximumLoanAmount(DEFAULT_FAILED_DOUBLE);
      deal.ejbStore();

  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL , "dealTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "combinedLendingValue") );

    addInputParam(new CalcParam(ClassId.PROPERTY , "primaryPropertyFlag"));
    addInputParam(new CalcParam(ClassId.PROPERTY , "propertyLocationId"));
    addInputParam(new CalcParam(ClassId.PROPERTY , "propertyTypeId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "maximumLoanAmount", T13D2));
  }

}
