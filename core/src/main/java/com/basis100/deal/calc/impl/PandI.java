package com.basis100.deal.calc.impl;

/**
 * 06/Apr/2006 DVG #DG400 #2835  Changes to Calc #1
 * 26/Jan/2010, 4.3GR, FXP23563, FXP27174 changed drastically
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcException;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.pk.PricingProfilePK;

public class PandI extends DealCalc
{

    public PandI() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 1";
    }

    boolean isDJNrPaymts = false;    //#DG400

    @SuppressWarnings("unchecked")
    public Collection getTargets()
    {
        return this.targets;
    }

    /**
     * Given an input object this method determines the appropriate target object
     * and adds the object to it's list of targets (targets).
     *
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget( input );
        }
        catch(Exception e)
        {
            String msg = "Fail to get Targets" + this.getClass().getName();
            msg += ":[Input]:" + input.getClass ().getName () ;
            msg += "\nReason: " + e.getMessage();
            msg += "@input: " + this.getDoCalcErrorDetail ( input );
            logger.error (msg );
            throw new TargetNotFoundException (msg);
        }
    }

    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        Deal deal = (Deal)target;
        if (deal == null) throw new IllegalArgumentException("deal is null");

        try{

            int institutionProfileId = deal.getInstitutionProfileId();
            double totalLoanAmount = deal.getTotalLoanAmount();
            int paymentFrequencyId = deal.getPaymentFrequencyId();
            double netInterestRate = deal.getNetInterestRate();
            int amortizationMonths = deal.getAmortizationTerm();
            int repaymentTypeId = deal.getRepaymentTypeId();

            MtgProd mtgProd = (MtgProd) entityCache.find(srk,
                new MtgProdPK(deal.getMtgProdId()));
            int interestCompoundingId = mtgProd.getInterestCompoundingId();

            String rateCode = null;
            try{
                rateCode = mtgProd.getPricingProfile().getRateCode();
            }catch(Exception e){
                logger.error(e);
                rateCode = "";
            }
            
            double pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount,
                paymentFrequencyId, netInterestRate, amortizationMonths,
                repaymentTypeId, interestCompoundingId, rateCode);

            //just rounding
            pandiAmount = Math.round ( pandiAmount * 100d ) /100d ;
            logger.debug("Calc-1 PandI: PandIPaymentAmount = " + pandiAmount);
            deal.setPandiPaymentAmount(validate(pandiAmount));
            deal.ejbStore();
            return;
        }
        catch(Exception e)
        {
            String msg = "Benign Failure in Calculation" + this.getClass().getName();
            msg += ":[Target]:" + target.getClass ().getName () ;
            msg += "\nReason: " + e.getMessage();
            msg += "@Target: " + this.getDoCalcErrorDetail ( target );
            logger.error(msg);
            logger.error(e);
        }

        deal.setPandiPaymentAmount(DEFAULT_FAILED_DOUBLE);
        deal.ejbStore();
    }

    public boolean hasInput(CalcParam input){
        return this.inputParam.contains(input);
    }


    @SuppressWarnings("unchecked")
    private void initParams() throws ParamTypeException{

        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
        addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
        addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
        addInputParam(new CalcParam(ClassId.DEAL, "interestCompoundId"));
        addInputParam(new CalcParam(ClassId.DEAL , "mtgProdId"));
        addInputParam(new CalcParam(ClassId.DEAL , "repaymentTypeId"));

        addTargetParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount", T13D2));

    }
}
