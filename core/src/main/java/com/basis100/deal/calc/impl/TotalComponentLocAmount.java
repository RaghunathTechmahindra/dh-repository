/*
 * @(#)TotalComponentLocAmount.java Jun 09, 2008 Copyright (C) 2008 Filogix
 * Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * Title: TotalComponentLocAmount
 * </p>
 * <p>
 * Description:Calculates Total Component LOC Amount for LOC components
 * </p>
 * @author MCM Impl Team <br>
 * @version 1.0 Initial Version <br>
 * @Date 09-Jun-2008 <br>
 *       Story - XS_11.11 <br>
 *       Code Calc-43.CLOC
 */
public class TotalComponentLocAmount extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Version
     */
    public TotalComponentLocAmount() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 43.CLOC";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * Caculation Logic :
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOC)
            {
                ComponentLOC componentLOC = (ComponentLOC) input;
                addTarget(componentLOC);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Collection listOfcomponents = null;
                    Deal deal = (Deal) input;
                    // Get all the components for the deal
                    listOfcomponents = entityCache.getComponents(deal, srk);
                    Iterator componentsItr = listOfcomponents.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is LOC COMPONENT
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOC)
                        {
                            // Retrives the componentLOC Object for the given
                            // componentId and copyId
                            ComponentLOC componentLOC = (ComponentLOC) entityCache
                                    .find(srk, new ComponentLOCPK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            addTarget(componentLOC);

                        }
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());
        try
        {
            ComponentLOC targetComponentLOC = (ComponentLOC) target;
            // Retive the component from the Target Object
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLOC.getComponentId(),
                            targetComponentLOC.getCopyId()));
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));

            if (deal.getMIUpfront().equals("N")
                    && targetComponentLOC.getMiAllocateFlag().equals("Y"))
            {
                double totalcompLocAmt = targetComponentLOC.getLocAmount()
                        + deal.getMIPremiumAmount();
                // if the TotalLocAmount is >99999999999.99 then Validate will
                // round of to the max given value in the TargetParam
                targetComponentLOC.setTotalLocAmount(validate(totalcompLocAmt));
                targetComponentLOC.ejbStore();
            }
            else
            {
                targetComponentLOC.setMiAllocateFlag("N");
                double totalcompLocAmt = targetComponentLOC.getLocAmount();
                // if the TotalLocAmount is >99999999999.99 then Validate will
                // round of to the max given value in the TargetParam
                targetComponentLOC.setTotalLocAmount(validate(totalcompLocAmt));
                targetComponentLOC.ejbStore();
            }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Initialize the target parameters
     * </p>
     * 
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Versionn
     * 
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "miUpFront"));
        addInputParam(new CalcParam(ClassId.DEAL, "miPremiumAmount"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "locAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "miAllocateFlag"));
        // Max value for TotalLocAmount is 99999999999.99
        addTargetParam(new CalcParam(ClassId.COMPONENTLOC, "totalLocAmount",T13D2));
    }

}
