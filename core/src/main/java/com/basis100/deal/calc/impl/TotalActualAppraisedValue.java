package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Total Actual Appraised Value
//Code: Calc 39
//07/04/2000

public class TotalActualAppraisedValue extends DealCalc
{


  public TotalActualAppraisedValue() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 39";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
      try
      {
        if ( input instanceof Property )
        {
           Property property = (Property) input;
           Deal deal = (Deal) entityCache.find (srk,
                  new DealPK( property.getDealId(), property.getCopyId()) );
           addTarget(deal);
        }
      }
      catch(Exception e)
      {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
      }
  }
       
  public void doCalc(Object target) throws Exception
  {

   trace(this.getClass().getName());
   Deal deal = ( Deal) target;

   try
   {
      Collection properties = this.entityCache.getProperties (deal);
      Iterator itPr = properties.iterator ();

      double TAAV = 0.0;
      Property property = null ;
      while ( itPr.hasNext())
      {
          property = (Property) itPr.next ();
          TAAV += property.getActualAppraisalValue () ;
      }
      TAAV = Math.round ( TAAV * 100 ) /100.00 ; 
      deal.setTotalActAppraisedValue ( validate(TAAV) );
      deal.ejbStore();
      return;
    }
    catch ( Exception e )
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
      
    }

    deal.setTotalActAppraisedValue(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }   

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.PROPERTY, "actualAppraisalValue"));

    addTargetParam(new CalcParam(ClassId.DEAL, "totalActAppraisedValue", T13D2));

  }



}
