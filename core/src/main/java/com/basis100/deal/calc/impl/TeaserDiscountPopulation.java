package com.basis100.deal.calc.impl;

import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import java.util.*;

//Name: TeaserTermPopulation
//Code: Calc 97

public class TeaserDiscountPopulation extends DealCalc{

  public TeaserDiscountPopulation() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 97" ;
  }

  public Collection getTargets()
  {    return this.targets;  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget( input ) ;
    }
    catch ( Exception e )
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = ( Deal ) target ;

    try
    {
       MtgProd prod =  deal.getMtgProd () ;
       if ( prod != null )
       {
          //--Ticket#1736--18July2005--start--//
          PricingProfile pp = prod.getPricingProfile();
          PricingRateInventory pricingRateInventory = pp.findLatestInventoryByPricingProfileId(pp.getPricingProfileId());

          trace( "Calc 97----lastPricingRateInventoryId = " + pricingRateInventory.getPricingRateInventoryID() );

          double discount = pricingRateInventory.getTeaserDiscount();
          //--Ticket#1736--18July2005--end--//

          trace( "pricingRateInventory.TeaserDiscount = " + discount );
          if ( discount > 0 )
          {
              deal.setTeaserDiscount ( discount );
          }
          else
          {
              deal.setTeaserDiscount ( 0 );
          }
          deal.ejbStore ();
       }
       return ;
    }
    catch(Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

    deal.setTeaserDiscount ( DEFAULT_FAILED_DOUBLE );
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam ( new CalcParam( ClassId.DEAL , "mtgProdId" ));
    //addInputParam ( new CalcParam( ClassId.DEAL , "netInterestRate" )) ; // Test Only

    addTargetParam( new CalcParam ( ClassId.DEAL , "teaserDiscount" ) ) ;
  }

}
