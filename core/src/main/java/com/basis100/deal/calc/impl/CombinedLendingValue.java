package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.util.*;


public class CombinedLendingValue extends DealCalc
{


  public CombinedLendingValue()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 75";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        if ( input instanceof Deal )
        {
          addTarget( input );
        }
         else if ( input instanceof Property )
         {
             Property p = (Property)input;
             Deal deal = (Deal) entityCache.find(srk, 
                           new DealPK( p.getDealId(), p.getCopyId())) ;
             addTarget( deal );
         }
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Deal deal = (Deal)target;

    try
    {
       double combinedLendingValue = 0.0;
       trace( " SpecialFeatureId = " + deal.getSpecialFeatureId () );
       double PAPurchasePrice = deal.getPAPurchasePrice();
       trace( " PAPurchasePrice = " + PAPurchasePrice );

       // Spec. Changes -- By BILLY 11Jan2002
       if ( deal.getSpecialFeatureId () == 1 && PAPurchasePrice>0 ) /* PreApproval */
       {
          combinedLendingValue = PAPurchasePrice;
          trace( "(SpecialFeature = PreApproval) CombinedLendingValue = PAPurchasePrice = " + combinedLendingValue );

       }
       else
       {
            // This may not work when getting the list from Cache, the properties on the cache may not uptodate
            //  -- By BILLY 22Jan2002
            //Collection properties = this.entityCache.getProperties (deal) ;
            Collection properties = deal.getProperties();

            if (properties != null && properties.size() > 0)
            {
                Iterator itPR = properties.iterator () ;
                Property property = null;

                while ( itPR.hasNext () )
                {
                  property = ( Property) itPR.next () ;

                  combinedLendingValue += property.getLendingValue() ;
                  trace( "CombinedLendingValue = " + combinedLendingValue );

                }
            }
            else
            {
              // No property on this deal
              combinedLendingValue = PAPurchasePrice;
              trace( "(No property) CombinedLendingValue = PAPurchasePrice = " + combinedLendingValue );
            }
       }

      combinedLendingValue = Math.round ( combinedLendingValue * 100 ) / 100.00 ;
      deal.setCombinedLendingValue( validate(combinedLendingValue) );
      deal.ejbStore();
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

     deal.setCombinedLendingValue( DEFAULT_FAILED_DOUBLE );
     deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTY, "lendingValue"));
    addInputParam(new CalcParam(ClassId.DEAL , "specialFeatureId"));
    addInputParam(new CalcParam(ClassId.DEAL , "PAPurchasePrice"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedLendingValue", T13D2));

  }


}
