package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

// This calculation should be taken out due to new Spec.
public class CommitmentPeriod extends DealCalc
{


  public CommitmentPeriod() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 32";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
      try
      {
          addTarget ( input ) ;
      }
      catch(Exception e)
      {
          String msg = "Fail to get Targets" + this.getClass().getName();
          msg += ":[Input]:" + input.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@input: " + this.getDoCalcErrorDetail ( input );
          logger.error (msg );
          throw new TargetNotFoundException (msg);
      }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;

    try
    {
       Date d1 = deal.getCommitmentIssueDate();
       Date d2 = deal.getCommitmentExpirationDate();
       int DAY = 86400000;

       if(d1 == null || d2 == null)
        throw new Exception("Required Inputs are null: deal.getCommitmentExpirationDate() || deal.getEstimatedClosingDate()");

        // Reset the Hour,Minute and Second parameters -- Compare only the date -- BY BILLY 16April2002
        Calendar d1InCal = Calendar.getInstance();
        d1InCal.clear();
        d1InCal.set(d1.getYear()+1900, d1.getMonth(), d1.getDate());

        Calendar d2InCal = Calendar.getInstance();
        d2InCal.clear();
        d2InCal.set(d2.getYear()+1900, d2.getMonth(), d2.getDate());

       long result = Math.abs( d1InCal.getTime().getTime () - d2InCal.getTime().getTime ()) / DAY ;

       if ( result > 0 ) deal.setCommitmentPeriod((int)validate(result));

       else  deal.setCommitmentPeriod(DEFAULT_FAILED_INT);   // 0

       deal.ejbStore();

       return;
    }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     deal.setCommitmentPeriod(this.DEFAULT_FAILED_INT);
     deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.DEAL, "commitmentIssueDate"));
    addInputParam(new CalcParam(ClassId.DEAL, "commitmentExpirationDate"));

    addTargetParam(new CalcParam(ClassId.DEAL, "commitmentPeriod", T3));

  }



}
