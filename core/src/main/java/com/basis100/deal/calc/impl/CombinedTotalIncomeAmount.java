package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

// Name: Combined Total Income 
// Code: Calc 27
// 06/30/2000

public class CombinedTotalIncomeAmount extends DealCalc {

  public CombinedTotalIncomeAmount() throws Exception {
    super();
    initParams();
    this.calcNumber = "Calc 27";
  }

   public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        Income i = (Income) input;
        Borrower b = (Borrower) entityCache.find(srk, 
                             new BorrowerPK(i.getBorrowerId (), 
                                            i.getCopyId ()) );
        Deal d = (Deal) entityCache.find (srk, 
                             new DealPK(b.getDealId(), b.getCopyId()));
        addTarget (d);
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Deal deal = (Deal) target;

    try
    {
      double totalIncomes =0.0;

      Collection incomes = this.entityCache.getIncomes (deal);
      Iterator itIn = incomes.iterator ();

      while ( itIn.hasNext () )
      { Income income = (Income)itIn.next ();

        if (income.getIncIncludeInGDS() || income.getIncIncludeInTDS()){
           trace("income.getIncIncludeInGDS() = " + income.getIncIncludeInGDS() +
                        " or income.getIncInclueInTDS() = " +  income.getIncIncludeInTDS() +
                        "\nCalculate totalIncomes...");

           totalIncomes +=  income.getIncomeAmount () *
              DealCalcUtil.annualize (income.getIncomePeriodId (), "IncomePeriod") ;
           trace( "totalIncomes = " + totalIncomes );
        }else{
           trace("income.getIncIncludeInGDS() = " + income.getIncIncludeInGDS() +
                        " or income.getIncInclueInTDS() = " +  income.getIncIncludeInTDS() +
                        "\nNo need to calculate totalIncomes.");
        }
      } // -- End of all Income ----
      totalIncomes = Math.round (totalIncomes * 100 ) / 100.00 ; 
      deal.setCombinedTotalIncome (validate(totalIncomes));
      deal.ejbStore ();
      return;
    }
    catch ( Exception e )
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

    deal.setCombinedTotalIncome (DEFAULT_FAILED_DOUBLE);
    deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME ,"incomePeriodId"));

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInTDS"));


    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalIncome", T13D2));
  }

}
