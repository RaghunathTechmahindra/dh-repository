package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

//Name:  New Property Number Check Digit
//Code:  Calc 78
//07/05/2000

public class NewPropertyNumberCheckDigit extends DealCalc {

  public NewPropertyNumberCheckDigit() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 78";
  }


  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
      {
        addTarget ( input);
      }
    catch ( Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = ( Deal ) target ;

     try
     {

     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    //addInputParam( new CalcParam(ClassId.DEAL , "propertyRefreenceNumber") );

    addTargetParam( new CalcParam(ClassId.DEAL , "propertyRefreenceNumber") );
   }
}
