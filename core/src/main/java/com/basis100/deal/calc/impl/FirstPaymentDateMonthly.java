/**
 * Title:        PandIMonthly (Calc-106)
 * Description:  First Payment Date(Monthly)
 * Copyright:    Copyright (c) 2001
 * Company:      Basis100
 * Date :        27 Nov 2001
 * @author Billy Lam
 * @version
 */

package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class FirstPaymentDateMonthly extends DealCalc
{
 public FirstPaymentDateMonthly()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 106";
  }


  public Collection getTargets()
  {
      return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
       addTarget(input);
    }
    catch ( Exception e)
    {
      String msg = "Failed to get Targets for: " + this.getClass().getName();
      msg += " :[Input]: " + input.getClass ().getName () ;
      logger.error(msg);
      msg = "  Reason: " + e.getMessage();
      msg += " @input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }



  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = ( Deal ) target ;

     try
     {
         Calendar firstPayDateMonthly = Calendar.getInstance();
         Date iDate = deal.getInterimInterestAdjustmentDate();
         if ( iDate != null)
         {
             logger.debug("FirstPaymentDateMonthly(dealid = " + deal.getDealId() + ") iDate: " + iDate);
           firstPayDateMonthly.setTime(iDate);
           firstPayDateMonthly.add(Calendar.MONTH , 1);

           deal.setFirstPaymentDateMonthly(firstPayDateMonthly.getTime());
           deal.ejbStore ();
           return;
         }
         else
         {
           throw new Exception("Required Input = null -> deal.getInterimInterestAdjustmentDate().");
         }
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );

     }

    deal.setFirstPaymentDateMonthly(DEFAULT_FAILED_DATE);
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "interimInterestAdjustmentDate"));

    addTargetParam(new CalcParam(ClassId.DEAL, "firstPaymentDateMonthly"));
  }

}