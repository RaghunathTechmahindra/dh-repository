package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;
//Name: Teaser Maturity Date
//Code: Calc 86
//07/13/2000

public class TeaserMaturityDate extends DealCalc {

  final long MillSecsInWeek = 604800000 ;  //7 * 24 * 60 * 60 * 10000

  public TeaserMaturityDate() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 86" ;
  }

   public Collection getTargets()
  {    return this.targets;  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof Deal )
          addTarget ( input );
    }
    catch ( Exception e )
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = ( Deal ) target ;

    try
    {
      if ( deal.getTeaserTerm () > 0 )
      {
          Date IAD = deal.getInterimInterestAdjustmentDate ( ) ;

          if ( IAD != null )
          {
            Calendar previousDate = Calendar.getInstance( Locale.getDefault () ) ;
            Calendar maturityDate = Calendar.getInstance( Locale.getDefault () ) ;
            int paymentFrequencyId = deal.getPaymentFrequencyId () ;
            int term = deal.getTeaserTerm () ;

            maturityDate.setTime (  IAD );
            int dayOfWeek = maturityDate.get (  Calendar.DAY_OF_WEEK  );

            maturityDate.add (Calendar.MONTH , term );  // IAD=1/31/2000, term = 1
                                                        // ===> maturityDate = 2/28/2000
            int dayOfWeek2 = maturityDate.get ( Calendar.DAY_OF_WEEK    ) ;

            // (Accelerated) Weekly, BiWeekly, Make sure same weekday.
            if ( oneOf( paymentFrequencyId,
                        Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY ,
                         Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
            {
                  if ( dayOfWeek != dayOfWeek2 )
                     maturityDate.add( Calendar.DATE , - 7 +  (dayOfWeek - dayOfWeek2) );
            }
            // If Bi-Weekly, make sure Weeks between IAD and CalculatedMaturidyDate is even
            if ( oneOf( paymentFrequencyId,
                        Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
            {
                long millSecs = maturityDate.getTime ().getTime ()
                                  - previousDate.getTime().getTime () ;
                if  ( millSecs / MillSecsInWeek  % 2  != 0  ) // Weeks difference is even
                    maturityDate.add( Calendar.DATE , - 7 );
            }

           deal.setTeaserMaturityDate ( maturityDate.getTime () );
           deal.ejbStore ();
           return;
          }
          else
          {
             throw new Exception("Required Input null ->deal.getInterimInterestAdjustmentDate ( ).");
          }
      }
      else
      {
         deal.setTeaserMaturityDate ( null );
         deal.ejbStore ();
         return;
      }
    }
    catch(Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

    deal.setTeaserMaturityDate(DEFAULT_FAILED_DATE);
    deal.ejbStore();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam ( new CalcParam( ClassId.DEAL , "interimInterestAdjustmentDate" ));
    addInputParam ( new CalcParam ( ClassId.DEAL , "teaserTerm" ) );
    addInputParam ( new CalcParam ( ClassId.DEAL , "paymentFrequencyId" ) ) ;

    addTargetParam( new CalcParam ( ClassId.DEAL , "TeaserMaturityDate" ) ) ;

  }

}
