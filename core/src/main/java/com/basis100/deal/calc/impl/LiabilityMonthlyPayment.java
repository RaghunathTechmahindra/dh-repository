package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Liability Monthly Payment
//Code: Calc 72
// 07/04/2000

public class LiabilityMonthlyPayment extends DealCalc {

  public LiabilityMonthlyPayment() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 72";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget ( input);
    }
    catch ( Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());

     Liability liability = ( Liability ) target;

     try
     {
        double liabilityMontylyPayment = 0.0;

        LiabilityType liabilityType = (LiabilityType) entityCache.find (srk,
              new LiabilityTypePK( liability.getLiabilityTypeId ()) );

        String qf = liabilityType.getLiabilityPaymentQualifier () ;
        trace( "Qualifier = " + qf );
        
        if (qf == null || (!qf.equalsIgnoreCase ("V")))
            return;

        // qualifier 'V'
        liabilityMontylyPayment  = liability.getLiabilityAmount () * liability.getPercentInTDS () / 100.00;
        liabilityMontylyPayment = Math.round ( liabilityMontylyPayment * 100 ) /100.00; 
        liability.setLiabilityMonthlyPayment ( validate(liabilityMontylyPayment) );
        liability.ejbStore ();

        return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     liability.setLiabilityMonthlyPayment(DEFAULT_FAILED_DOUBLE);
     liability.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.LIABILITY , "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "includeInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "liabilityTypeId" )) ;
    addInputParam(new CalcParam(ClassId.LIABILITY , "percentInTDS"));

    addTargetParam(new CalcParam(ClassId.LIABILITY , "liabilityMonthlyPayment",  T13D2));
  }


}
