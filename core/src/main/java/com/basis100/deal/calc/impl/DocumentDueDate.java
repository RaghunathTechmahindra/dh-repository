package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name: Document Due Date
//Code:  Calc 60
//07/11/2000

public class DocumentDueDate extends DealCalc
{


  public DocumentDueDate() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 60";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    // Condition, Deal
    try
    {
        Collection documentTracks = null;
        if ( input instanceof Deal )
        {
            Deal deal = ( Deal )  input;
            documentTracks = deal.getDocumentTracks () ;
        }
        if ( input instanceof Condition )
        {
            Condition condition = ( Condition ) input;
            DocumentTracking documentTracking = new DocumentTracking(srk );
            documentTracks =  documentTracking.findByConditionId ( condition.getConditionId () ) ;
        }

        if ( documentTracks != null )
        {
            Iterator itDTs = documentTracks.iterator () ;
            while ( itDTs.hasNext () )
              addTarget( itDTs.next () );
        }
    }
    catch ( Exception e )
    {
        String msg = "Failed to get Targets for: " + this.getClass().getName();
        msg += " :[Input]: " + input.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += " @input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     DocumentTracking documentTracking = (DocumentTracking) target ;

     try
     {

         Condition condition = (Condition) entityCache.find (srk,
                      new ConditionPK( documentTracking.getConditionId ()) );

         Deal deal = (Deal) entityCache.find ( srk, 
                        new DealPK(documentTracking.getDealId(), 
                             documentTracking.getCopyId()));

         Calendar documentDuedate = null ;

         if  ( condition.getConditionBasedOnDateId () == 1 /* Commitment Expected Return Date */)
          {
              documentDuedate = Calendar.getInstance () ;

              Date retDate = deal.getReturnDate ();

              if(retDate == null)
                throw new Exception("Required Input = null -> deal.getReturnDate (). ");

              documentDuedate.setTime(retDate);

              if (  condition.getConditionCalculationTypeId () == 1 /* After */ )
                  documentDuedate.add ( Calendar.DAY_OF_MONTH , condition.getDurationDays ());
              else if  ( condition.getConditionCalculationTypeId () == 0 /* Before */ )
                  documentDuedate.add (Calendar.DAY_OF_MONTH , - condition.getDurationDays ()) ;
          }
          else if ( condition.getConditionBasedOnDateId () == 0 /* Estimated Closing Date */)
          {
              documentDuedate = Calendar.getInstance () ;
              Date estClosing = deal.getEstimatedClosingDate ();

              if(estClosing == null)
                throw new Exception("Required Input = null -> deal.getEstimatedClosingDate ().");

              documentDuedate.setTime (estClosing);
              
              if ( condition.getConditionCalculationTypeId () == 1 /* After */ )
                  documentDuedate.add (  Calendar.DAY_OF_MONTH , condition.getDurationDays ());
              else if ( condition.getConditionCalculationTypeId () == 0 /* Before */ )
                  documentDuedate.add ( Calendar.DAY_OF_MONTH , - condition.getDurationDays () );
          }

          if ( documentDuedate != null )
          {
              Date time = documentDuedate.getTime ();
              if(time == null) return;

              documentTracking.setDocumentDueDate (time);
              documentTracking.ejbStore ();
          }
          return;
     }
     catch ( Exception e )
     {
          String msg = "Benign Failure in Calculation" + this.getClass().getName();
          msg += ":[Target]:" + target.getClass ().getName () ;
          msg += "  Reason: " + e.getMessage();
          logger.debug (msg );
     }

     documentTracking.setDocumentDueDate ( this.DEFAULT_FAILED_DATE );
     documentTracking.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam ( new CalcParam(ClassId.CONDITION , "durationDays") );
    addInputParam ( new CalcParam(ClassId.CONDITION , "conditionBasedOnDateId") );
    addInputParam ( new CalcParam(ClassId.CONDITION , "conditionCalculationTypeId") );

    addInputParam ( new CalcParam(ClassId.DEAL  , "returnDate") );
    addInputParam ( new CalcParam(ClassId.DEAL , "estimatedClosingDate")) ;

    addTargetParam ( new CalcParam(ClassId.DOCUMENTTRACKING , "documentDueDate") );

  }
}
