package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.pk.*;

public class BorrowerNetWorth extends DealCalc
{     

  public BorrowerNetWorth() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 47";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {

         int cid = ((DealEntity)input).getClassId();
         int borrowerId = 0;
         int copyId = 0;

         if ( cid == ClassId.BORROWER )
         {
            addTarget( input );
            return  ; 
         }
         else if(cid == ClassId.ASSET)
           {
             Asset a = (Asset)input;
             borrowerId = a.getBorrowerId();
             copyId = a.getCopyId();
           }
         else  if(cid == ClassId.LIABILITY)
           {
             Liability l = (Liability)input;
             borrowerId = l.getBorrowerId();
             copyId = l.getCopyId();
           }

         Borrower b = (Borrower) entityCache.find (srk, 
                              new BorrowerPK(borrowerId, 
                                             copyId)) ;
         addTarget(b);
     }
     catch(Exception e)
     {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException(msg);
     }
  }



  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Borrower bt = (Borrower)target;

     try
     {
       double netwrth = 0;
       Collection assets = this.entityCache.getAssets(bt);

       Iterator ai = assets.iterator();
       Asset current = null;

       while(ai.hasNext())
       {
         current = (Asset)ai.next();

         if(current.getIncludeInNetWorth())
         {
            netwrth +=   current.getAssetValue() * current.getPercentInNetWorth()/100.00 ;
            trace( "netWorth = " + netwrth );
         }
       }

       netwrth -= bt.getTotalLiabilityAmount () ;
       trace ( "netWorth = " + netwrth );
       
       netwrth = Math.round ( netwrth * 100 ) / 100.00 ;
       bt.setNetWorth(validate(netwrth));
       bt.ejbStore();
       return ;
     }
     catch(Exception e)
     {
        String msg = "Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.error (msg );
     }

     bt.setNetWorth(DEFAULT_FAILED_DOUBLE);
     bt.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }



  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.ASSET, "assetValue"));
    addInputParam(new CalcParam(ClassId.ASSET, "includeInNetWorth"));
    addInputParam(new CalcParam(ClassId.ASSET, "percentInNetWorth"));

    addInputParam(new CalcParam(ClassId.BORROWER , "totalLiabilityAmount"));

    addTargetParam(new CalcParam(ClassId.BORROWER, "netWorth", T13D2));
  }



} 
