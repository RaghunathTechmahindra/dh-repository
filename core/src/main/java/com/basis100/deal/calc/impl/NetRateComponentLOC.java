/*
 * @(#)NetRateComponentLOC.java Jun 09, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;

/**
 * <p>
 * Title: NetRateComponentLOC
 * </p>
 * <p>
 * Description:Calculates Net Interest Rate for Line of credit components of
 * "component eligible" deals.
 * </p>
 * @author MCM Impl Team <br>
 * @Date 09-Jun-2008 <br>
 * @version 1.0 XS_11.11 Initial Version <br>
 *          Code Calc-2.CLOC <br>
 * @version 1.1 Modified the getTarget(..) method         
 */
public class NetRateComponentLOC extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Version
     */
    public NetRateComponentLOC() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 2.CLOC";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Version
     * @version 1.1 XS_11.11 08-Jul-2008 Added ComponentTypeId check
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOC)
            {
                ComponentLOC componentLOC = (ComponentLOC) input;
                addTarget(componentLOC);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    Component component = (Component) input;
                    // MCM added try-catch
                    try {
                        // find componentLOC for the given componentId and copyId
                        if(component.getComponentTypeId()== Mc.COMPONENT_TYPE_LOC){
    	                    ComponentLOC componentLOC = (ComponentLOC) entityCache
    	                            .find(srk, new ComponentLOCPK(component
    	                                    .getComponentId(), component.getCopyId()));
    	                    addTarget(componentLOC);
                        }
                    } catch (Exception e) {
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        if (target==null) return;
        try
        {
            ComponentLOC targetComponentLOC = (ComponentLOC) target;
            // Retrive component Object from the given target
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLOC.getComponentId(),
                            targetComponentLOC.getCopyId()));

            double componentLocNetRate = component.getPostedRate()
                    - targetComponentLOC.getDiscount()
                    + targetComponentLOC.getPremium();
            // if the NetInterestRate is >999999.999999 then Validate will round
            // of to the max given value in the TargetParam
            targetComponentLOC
                    .setNetInterestRate(validate(componentLocNetRate));

            targetComponentLOC.ejbStore();

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * 
     * @version 1.0 XS_11.11 09-Jun-2008 Initial Versionn
     * 
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "postedRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "discount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "premium"));
        // Max NetInterestRate =999999.999999
        addTargetParam(new CalcParam(ClassId.COMPONENTLOC, "netInterestRate",
                T12D6));
    }

}
