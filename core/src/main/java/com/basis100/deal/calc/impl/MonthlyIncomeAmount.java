package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

//Name:  Annual Income Amount
//Code:  Calc 80
//07/04/2000

public class MonthlyIncomeAmount extends DealCalc {

  public MonthlyIncomeAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 80";
  }

  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        addTarget ( input);
    }
    catch ( Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Income income = (Income) target ;
     try
     {
        double monthlyIncomeAmount = 0.0;

        monthlyIncomeAmount = income.getAnnualIncomeAmount () /12.0 ;
        monthlyIncomeAmount = Math.round ( monthlyIncomeAmount * 100 ) / 100.00 ; 
        income.setMonthlyIncomeAmount ( validate(monthlyIncomeAmount) );
        income.ejbStore ();
        return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     income.setMonthlyIncomeAmount( DEFAULT_FAILED_DOUBLE );
     income.ejbStore();

  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME , "annualIncomeAmount") );

    addTargetParam(new CalcParam(ClassId.INCOME , "monthlyIncomeAmount", T13D2));
  }

}
