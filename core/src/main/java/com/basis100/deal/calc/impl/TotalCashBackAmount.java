/*
 * @(#)TotalCashBackAmount.java July 18, 2008 <story id : XS-11.16> Copyright
 * (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;

/**
 * <p>
 * Title: TotalCashBackAmount
 * </p>
 * <p>
 * Description: Calculates total cashback amount of all eligible components
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.16 Initial version
 * @version 1.1 artf751759 28-Jul-2008 Modified getTarget(..) method
 * @version 1.2 artf751756 30-Jul-2008 added Deal as input target
 * @version 1.3 artf751756 1-Aug-2008 changed input param to Component instead of ComponentSummary
 */
public class TotalCashBackAmount extends DealCalc
{

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @throws Exception
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 Initial version
     */
    public TotalCashBackAmount() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "CALC 115";
    }

    /**
     * <p>
     * Description: Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @version 1.0 XS_11.16 Initial version
     * @version 1.2 XS_11.16 28-Jul-2008 Modified the logic
     * @param input
     *            Object Input component.
     * @param dcm
     *            CalcMonitor runs the calculation of this Component entity.
     * @throws TargetNotFoundException
     *             exception that may occurs if the target is not found.
     * @see com.basis100.deal.calc.DealCalc#getTarget(java.lang.Object,
     *      com.basis100.deal.calc.CalcMonitor)
     * 
     * @version 1.2 artf751756 30-Jul-2008 added Deal as input target
     * @version 1.3 artf751756 1 Aug 2008 changed iniput param Component, instead of ComponentSummary
     */
    @Override
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException {
        try {
            int classId = ((DealEntity) input).getClassId();
            //***** MCM for artf751756 starts
//            if (classId == ClassId.COMPONENTSUMMARY) {
//                if (input != null ) {
//                    ComponentSummary componentSummary = (ComponentSummary) input;
//                    addTarget(componentSummary);
//                }
            if (classId == ClassId.COMPONENT) {
                Component component = (Component) input;
                ComponentSummary componentSummary = (ComponentSummary) entityCache
                    .find(srk, new ComponentSummaryPK(
                        component.getDealId(), component.getCopyId()));
                addTarget(componentSummary);
            //***** MCM for artf751756 ends
            } else  if (classId == ClassId.COMPONENTMORTGAGE) {
                ComponentMortgage componentMortgage = (ComponentMortgage) input;
                Component component = (Component) this.entityCache.find(srk,
                        new ComponentPK(componentMortgage.getComponentId(),
                                componentMortgage.getCopyId()));
                ComponentSummary componentSummary = (ComponentSummary) this.entityCache
                        .find(srk, new ComponentSummaryPK(
                                component.getDealId(), component.getCopyId()));
                addTarget(componentSummary);
            }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(":[Input]:");
            msg.append(input.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@input: ");
            msg.append(this.getDoCalcErrorDetail(input));
            logger.debug(msg.toString());
            throw new TargetNotFoundException(e.getMessage());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.16 Initial Version   
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        ComponentSummary targetComponentSummary = (ComponentSummary) target;

        Deal deal = (Deal) entityCache.find(srk, new DealPK(
                targetComponentSummary.getDealId(), targetComponentSummary
                        .getCopyId()));
        double totalCashbackAmount = 0.0d;
        try
        {
            // get the mtgprod for the deal
            MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                    new MtgProdPK(deal.getMtgProdId()));
            // Get all the Components
            Collection components = this.entityCache.getComponents(deal, srk);
            // Check Component eligibility
            if ("Y".equals(mtgProd.getComponentEligibleFlag())
                    && components != null)
            {
                Iterator itComponents = components.iterator();
                while (itComponents.hasNext())
                {
                    Component component = (Component) itComponents.next();
                    // ComponentMortgage
                    if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                    {
                        ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
                                .find(srk, new ComponentMortgagePK(component
                                        .getComponentId(), component
                                        .getCopyId()));
                        totalCashbackAmount += componentMtg.getCashBackAmount();
                    }
                }
            }
            // Set total cashback amount
            targetComponentSummary
                    .setTotalCashbackAmount(validate(totalCashbackAmount));
            targetComponentSummary.ejbStore();
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Initializes the input & target parameters which is called in
     * the constructor.
     * </p>
     * @version 1.0 XS_11.16 Initial Version
     * @throws ParamTypeException
     *             exception that may occur if param type is not valid.
     * @version 1.2 artf751756 30 Jul 2008 added iniput param ComponentSummary
     * @version 1.3 artf751756 1 Aug 2008 changed iniput param Component, instead of ComponentSummary
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();
        
        /***************MCM Impl team changes starts - artf751756  *******************/ 
//        addInputParam(new CalcParam(ClassId.COMPONENTSUMMARY, "NumberOfComponents"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "ComponentTypeId"));
        /***************MCM Impl team changes ends - artf751756  *******************/ 
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "cashBackAmount"));

        addTargetParam(new CalcParam(ClassId.COMPONENTSUMMARY,
                "totalCashbackAmount", T13D2));
    }

}
