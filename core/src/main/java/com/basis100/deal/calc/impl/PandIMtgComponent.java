package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: PandIMtgComponent
 * </p>
 * <p>
 * Description:Calculates The amount that is required to repay a mortgage over
 * the amortization period, based on the payment frequency selected.The P&I
 * payments will payback to the lender the original principal borrowed plus
 * interest.
 * </p>
 * Calculation Number- Calc-1.CM
 * @version 1.0 XS_11.1 20-Jun-2008 Initial Version
 * @version 1.1 25-Jun-2008 Formatted the doCalc(..) method
 * @version 1.1 Modified the getTarget(..) method
 */
public class PandIMtgComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.1 20-Jun-2008 Initial Version
     */
    public PandIMtgComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 1.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.1 19-Jun-2008 Initial Version
     * @version 1.1 25-Jun-2008 Moved the logic to DealCalcUtil and formatted
     *          the code.
     * @version 1.1 08-Jul-2008 Added ComponentTypeId check
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    try {
                        Component component = (Component) input;
                        if(component.getComponentTypeId()== Mc.COMPONENT_TYPE_MORTGAGE){
                        // find componentMortgage for the given componentId and
                        // copyId
                        ComponentMortgage componentMtg = (ComponentMortgage) entityCache
                                .find(srk, new ComponentMortgagePK(component
                                        .getComponentId(), component.getCopyId()));
                        addTarget(componentMtg);
                        } 
                    } catch (Exception e) {
                        // this case is Component is removed
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.1 20-Jun-2008 Initial Version
     * @version 1.1 25-Jun-2008 - Code Formatted
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        if (target == null) return;
        trace(this.getClass().getName());
        ComponentMortgage targetComponentMtg = (ComponentMortgage) target;

        try
        {
            // Retrive the component for the given component Mortgage
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentMtg.getComponentId(),
                            targetComponentMtg.getCopyId()));

            // Retrive the MtgProd for the given MtgProdId
            MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                    new MtgProdPK(component.getMtgProdId()));

            // Varibles for Calculation
            int amortizationMonths = targetComponentMtg.getAmortizationTerm();

            int paymentFreqId = targetComponentMtg.getPaymentFrequencyId();

            int repaymentTypeId = component.getRepaymentTypeId();

            String interestCompoundDesc = PicklistData.getMatchingColumnValue(
                    -1, "interestcompound", mtgProd.getInterestCompoundingId(),
                    "interestcompounddescription");
            boolean isUseFormula = (PropertiesCache.getInstance().getProperty(
                    component.getInstitutionProfileId(),
                    "com.basis100.calc.useformulaforweeklybiweeklynonaccpay",
                    "N")).equals("Y");

            if (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
                    || repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED)
            {
                double numOfPaymentsPerYear = DealCalcUtil
                        .calculateNumOfPaymentsPerYear(component
                                .getInstitutionProfileId(), paymentFreqId);
                double interestFactor = DealCalcUtil.interestFactor(
                        targetComponentMtg.getNetInterestRate(),
                        numOfPaymentsPerYear, interestCompoundDesc);
                double PandIPaymentAmount = Math.round(targetComponentMtg
                        .getTotalMortgageAmount()
                        * interestFactor * 100d) / 100d;
                targetComponentMtg.setPAndIPaymentAmount(PandIPaymentAmount);
                targetComponentMtg.ejbStore();
            }
            else
            {
                double pandIPayment = 0.0d;
                double monthlyPandI = DealCalcUtil.calculatePandI(component
                        .getInstitutionProfileId(), amortizationMonths,
                        Mc.PAY_FREQ_MONTHLY, interestCompoundDesc,
                        targetComponentMtg.getTotalMortgageAmount(),
                        targetComponentMtg.getNetInterestRate());

                if (paymentFreqId == Mc.PAY_FREQ_MONTHLY)
                {
                    pandIPayment = monthlyPandI;
                }
                else
                    if (paymentFreqId == Mc.PAY_FREQ_SEMIMONTHLY)
                    {

                        pandIPayment = DealCalcUtil.calculatePandI(component
                                .getInstitutionProfileId(), amortizationMonths,
                                paymentFreqId, interestCompoundDesc,
                                targetComponentMtg.getTotalMortgageAmount(),
                                targetComponentMtg.getNetInterestRate());
                    }
                    else
                        if (paymentFreqId == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
                        {
                            pandIPayment = monthlyPandI / 2d;
                        }
                        else
                            if (paymentFreqId == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
                            {
                                pandIPayment = monthlyPandI / 4d;
                            }
                            else
                                if (paymentFreqId == Mc.PAY_FREQ_BIWEEKLY
                                        || paymentFreqId == Mc.PAY_FREQ_WEEKLY)
                                {
                                    double pAndI = DealCalcUtil
                                            .calculatePandI(
                                                    component
                                                            .getInstitutionProfileId(),
                                                    amortizationMonths,
                                                    paymentFreqId,
                                                    interestCompoundDesc,
                                                    targetComponentMtg
                                                            .getTotalMortgageAmount(),
                                                    targetComponentMtg
                                                            .getNetInterestRate());
                                    if (isUseFormula == true)
                                    {
                                        pandIPayment = pAndI;
                                    }
                                    else
                                    {
                                        if (paymentFreqId == Mc.PAY_FREQ_BIWEEKLY)
                                        {
                                            pandIPayment = (monthlyPandI * 12d) / 26d;
                                        }
                                        else
                                            if (paymentFreqId == Mc.PAY_FREQ_WEEKLY)
                                            {
                                                pandIPayment = (monthlyPandI * 12d) / 52d;
                                            }
                                    }
                                }
                pandIPayment = Math.round(pandIPayment * 100d) / 100d;
                //if the pAndIPaymentAmount is >99999999999.99 then Validate will round
                // of to the max given value in the TargetParam
                targetComponentMtg.setPAndIPaymentAmount(validate(pandIPayment));
                targetComponentMtg.ejbStore();
            }
            return;
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());

        }
        targetComponentMtg.setPAndIPaymentAmount(Mc.DEFAULT_VALUE);
        targetComponentMtg.ejbStore();
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.1 20-Jun-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "repaymentTypeId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "amortizationTerm"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "netInterestRate"));

        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "pAndIPaymentAmount", T13D2));

    }
}
