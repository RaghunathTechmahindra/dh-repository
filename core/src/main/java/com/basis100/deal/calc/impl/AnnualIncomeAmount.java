package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name:  Annual Income Amount
//Code:  Calc 79
//07/04/2000

public class AnnualIncomeAmount extends DealCalc {

  public AnnualIncomeAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 79";
  }


   public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
      { addTarget ( input); // Only Income reachs here..
      }
    catch ( Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Income income = (Income) target ;

     try
     {
        double annualIncomeAmount = 0.0;

        annualIncomeAmount = income.getIncomeAmount () *
          DealCalcUtil.annualize (income.getIncomePeriodId () , "IncomePeriod") ;
        annualIncomeAmount = Math.round ( annualIncomeAmount * 100 ) / 100.00;  
        income.setAnnualIncomeAmount ( validate(annualIncomeAmount) );
        income.ejbStore ();
        return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage() ;
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     income.setAnnualIncomeAmount (DEFAULT_FAILED_DOUBLE);
     income.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME , "incomePeriodId") );

    addTargetParam(new CalcParam(ClassId.INCOME , "annualIncomeAmount", T13D2 ));
  }

  
}
