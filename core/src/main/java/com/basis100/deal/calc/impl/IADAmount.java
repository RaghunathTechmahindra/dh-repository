package com.basis100.deal.calc.impl;
//---- Not required in Release 1 ---------
import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.* ;

//Name: Interest Adjustment Amount (IAD)
//Code: Calc-12
//06/30/2000

public class IADAmount extends DealCalc {

  public IADAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 12";
  }

   public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {   addTarget( input ) ;        // Only deal comes here.
    }
    catch ( Exception e )
    {
      String msg = "Failed to getTarget : " + this.getClass().getName();
      logger.error(e);
      throw new TargetNotFoundException (msg);
     }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     return ;
     // not Required in Release.1
     /*
     try
      {
         Deal deal = (Deal) target ;
      }
     catch ( Exception e )
     {
      String msg = "Failure in calculation: " + this.getClass().getName();
      msg += "  Reason: " + e.getMessage();
      logger.error (e);
      
     }
     */
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    /*
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "firstPaymentDate"));
    addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));

    addTargetParam(new CalcParam(ClassId.DEAL, "interimInterestAdjustmentDate"));
    */
  }

}
