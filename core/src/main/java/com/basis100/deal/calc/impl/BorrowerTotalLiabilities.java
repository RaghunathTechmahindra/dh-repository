package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

public class BorrowerTotalLiabilities extends DealCalc
{

  public BorrowerTotalLiabilities()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 54";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {

     try
     {
        Liability lb = (Liability)input;
        Borrower bt = (Borrower) entityCache.find ( srk, 
                             new BorrowerPK( lb.getBorrowerId (), 
                                             lb.getCopyId ()));
        addTarget(bt);
     }
     catch(Exception e)
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }



  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());
     double result = 0;

     Borrower bt = (Borrower)target;

     try
     {
       Collection liabilities = this.entityCache.getLiabilities(bt);

       Iterator lib = liabilities.iterator();
       Liability current = null;

       while(lib.hasNext())
       {
         current = (Liability)lib.next();

         // Change request from Joe - add only if % out GDS < 2
         if(current.getPercentOutGDS() < 2)
         {
           result += current.getLiabilityAmount();
           trace( "toatlLiabilityAmount = " + result );
          }
          else
            trace( "PercentOutGDS() < 2 ==> toatlLiabilityAmount = " + result );
       }
       result = Math.round ( result * 100 ) / 100.00;
       bt.setTotalLiabilityAmount ( validate(result) );
       bt.ejbStore();
       return;

     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     bt.setTotalLiabilityAmount (DEFAULT_FAILED_DOUBLE);
     bt.ejbStore();

  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "percentOutGDS"));

    addTargetParam(new CalcParam(ClassId.BORROWER, "totalLiabilityAmount", T13D2));
  }



}
