package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;

import MosSystem.Mc;

// Name: Combined Total TDS 3 Years
// Code: Calc-9
// 06/30/2000

public class CombinedTotalTDS3Years extends DealCalc
{

  public CombinedTotalTDS3Years() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 9";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                                  new BorrowerPK(i.getBorrowerId(), 
                                                 i.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                                  new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Income ==> Deal
      if(input instanceof PropertyExpense)
      {
        PropertyExpense pe = (PropertyExpense)input;
        Property p = (Property)entityCache.find(srk, 
                                  new PropertyPK(pe.getPropertyId(), 
                                                 pe.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                                  new DealPK(p.getDealId(), p.getCopyId()));
        addTarget(d);
        return;
      } // PropertyExpense ==> Deal
      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                                  new BorrowerPK(l.getBorrowerId(), 
                                                 l.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                                  new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Liability ==> Deal

      if(input instanceof Deal)
      {
        addTarget(input);
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;
    trace("Entering Calc 9, dealId = " + deal.getDealId() + ", copyId = " + deal.getCopyId());
    //--DJ_Ticket841--start--//
    boolean isUseBorrowerOnlyOnThreeYearGDSTDS = (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                        "com.basis100.calc.useborroweronlyonthreeyearGDSTDS", "N")).equals("Y");
    trace( " isUseBorrowerOnlyOnThreeYearGDSTDS = " + isUseBorrowerOnlyOnThreeYearGDSTDS);

    if (isUseBorrowerOnlyOnThreeYearGDSTDS == false)
    {
       calcRegularCalc9(deal);
    }
    else if (isUseBorrowerOnlyOnThreeYearGDSTDS == true)
    {
       calcBorrowerOnlyCalc9(deal);
    }
    //--DJ_Ticket841--end--//
  }

  //--DJ_Ticket841--start--//
  private void calcRegularCalc9(Deal deal)
  {
//	  int originalRepaymentTypeId = deal.getRepaymentTypeId();
//      int originalAmortizationTerm = deal.getAmortizationTerm();
    try
    {
      double CombinedTotalTDS3Year = 0.00;

      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));
      String rateCode = pricingProfile.getRateCode();

      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          deal.setCombinedTDS3Year(0);
          deal.ejbStore();
          return;
        }
      }

      double totalIncomeAmount = 0.0;
      double totalTDSExpenses = 0.0;

      // ----- All Incomes Start ---------------
      Collection incomes = this.entityCache.getIncomes(deal);
      Iterator itIn = incomes.iterator();
      while(itIn.hasNext())
      {
        Income income = (Income)itIn.next();
        // -------- Calcuate the Total Income Amount -------------
        //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
        //--> By Billy 02Nov2004
        if(income.getIncIncludeInTDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
        {
          totalIncomeAmount += income.getIncomeAmount() *
            DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
            income.getIncPercentInTDS() / 100.00;
        }
        //=============================================================================
        // -------- Calcuate the Total Total TDS Expenses ------------------
        if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
        {
          totalTDSExpenses -= income.getIncomeAmount() *
            DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
            income.getIncPercentInTDS() / 100.00;
        }
        trace("totalIncomeAmount = " + totalIncomeAmount);
        trace("totalTDSExpenses ( + Income )= " + totalTDSExpenses);
      } // End of all Incomes

      //----- All liabilities start -------------
      Collection liabilities = this.entityCache.getLiabilities(deal);
      Iterator itLi = liabilities.iterator();
      while(itLi.hasNext())
      {
        Liability liability = (Liability)itLi.next();
        if(liability.getIncludeInTDS() && liability.getLiabilityPayOffTypeId() == 0) // non-paid off liability
        {
          // "Credit Card", "Unsecured Line of Credit" "Secured Line of Credit" "Line of Credit"
          // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
          // to do paymentfrequency.
          // -------------------------------------------------------------------
          // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
          //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
          if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
          {
            totalTDSExpenses += liability.getLiabilityAmount() * liability.getPercentInTDS()
              / 100.00 * 12; // double check !!!!
          }
          else

          // Liab = "Closing Costs", type = "High ratio"
          if(liability.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
          {
            totalTDSExpenses += liability.getLiabilityAmount();
          }
          else
          {
            totalTDSExpenses += liability.getLiabilityMonthlyPayment() * 12 *
              liability.getPercentInTDS() / 100.00;
          }
        }
        trace("totalTDSExpenses (+liability) = " + totalTDSExpenses);
      }

      //-------------Start with Properties:Expense --------------
      Collection pe = this.entityCache.getPropertyExpenses(deal);
      Iterator itPe = pe.iterator();

      // ----- Get the Property Expenses -----------
      while(itPe.hasNext())
      {
        PropertyExpense expense = (PropertyExpense)itPe.next();

        if(expense.getPeIncludeInTDS())
        {
          totalTDSExpenses += expense.getPropertyExpenseAmount() *
            DealCalcUtil.annualize(expense.getPropertyExpensePeriodId(), "PropertyExpensePeriod") *
            expense.getPePercentInTDS() / 100.00;
        }
        trace(" totalTDSExpenses ( +propertyExpense) = " + totalTDSExpenses);
      }
      trace(" totalTDSExpenses + propertyExpense = " + totalTDSExpenses);
      trace("PandI= " + deal.getPandiPaymentAmount());
      trace("AdditionalPrincipal = " + deal.getAdditionalPrincipal());
      trace("paymentFrequency= " +
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

//    start handling MOSSYS prop com.basis100.calc.forcepandifor3year
/*      boolean isForcePandIFor3Year = (PropertiesCache.getInstance().getProperty(
              "com.basis100.calc.forcepandifor3year", "N")).equals("Y");

      trace("isForcePandIFor3Year = " + isForcePandIFor3Year + ", ProdTypeId = " + iProdTypeId);
      trace("Original RepaymentTypeId = " + originalRepaymentTypeId + ", AmortizationTerm = " + originalAmortizationTerm);

      if(isForcePandIFor3Year && iProdTypeId == Mc.PRODUCT_TYPE_SECURED_LOC)
      {
    	  deal.setRepaymentTypeId(0);
    	  deal.setAmortizationTerm(300);
    	  trace("forcepandifor3year=Y and ProdTypeId=LOC, set RepaymentTypeId:0, AmortizationTerm:300");
      }*/
//    END of handling MOSSYS prop com.basis100.calc.forcepandifor3year
      
      /*#DG538 allow accelerated precision
      totalTDSExpenses +=
        (DealCalcUtil.PandI3YearRate(srk, entityCache, deal, -100) + deal.getAdditionalPrincipal()) *
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");*/
      totalTDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, true);

      trace("totalTDSExpense = " + totalTDSExpenses);

      if(totalIncomeAmount > 0)
      {
        CombinedTotalTDS3Year = totalTDSExpenses / totalIncomeAmount * 100;
        CombinedTotalTDS3Year = Math.round(CombinedTotalTDS3Year * 100.00) / 100.00;
      }
      else
      {
        CombinedTotalTDS3Year = this.getMaximumCalculatedValue();
      }

      logger.trace(">>>Returning from Calc 9, result  = " + CombinedTotalTDS3Year
    		  + ", netInterestRate = " + deal.getNetInterestRate());
      deal.setCombinedTDS3Year(validate(CombinedTotalTDS3Year));
//      deal.setRepaymentTypeId(originalRepaymentTypeId);
//      deal.setAmortizationTerm(originalAmortizationTerm);
      deal.ejbStore();
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + this.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(this.getClass().getName());
      logger.debug(msg);
    }

    // if failed
    try
    {
    deal.setCombinedTDS3Year(DEFAULT_FAILED_DOUBLE);
//    deal.setRepaymentTypeId(originalRepaymentTypeId);
//    deal.setAmortizationTerm(originalAmortizationTerm);
    deal.ejbStore();
    }
    catch(Exception ex)
    {
      //String msg = "Default Value Double Zero is set to: " + this.getClass().getName();
      logger.error(ex);
    }
  }

private void calcBorrowerOnlyCalc9(Deal deal)
{
//	int originalRepaymentTypeId = deal.getRepaymentTypeId();
//    int originalAmortizationTerm = deal.getAmortizationTerm();
    try
    {
      double CombinedTotalTDSBorrowerOnly = 0.00;

      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));
      String rateCode = pricingProfile.getRateCode();

      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          deal.setCombinedTDS3Year(0);
          deal.ejbStore();
          return;
        }
      }

    Collection borrowers = this.entityCache.getBorrowers(deal);
    if(borrowers != null)
    {
      double totalIncomeAmount = 0.0;
      double totalTDSExpenses = 0.0;

      Iterator itBO = borrowers.iterator();

      Borrower borrower = null;
      while(itBO.hasNext()) // Loop through all the borrowers
      {
        borrower = (Borrower)itBO.next();

        trace("BorrowerLastId to TDS calc (Borrower Only): " + borrower.getBorrowerId());
        trace("BorrowerLastName to TDS calc (Borrower Only): " + borrower.getBorrowerLastName());
        trace("BorrowerFirstName to TDS calc (Borrower Only): " + borrower.getBorrowerFirstName());

        if(borrower.getBorrowerTypeId() != Mc.BT_GUARANTOR /* Guarantor */)
        { // ----- Incomes Start ---------------
          Collection incomes = this.entityCache.getIncomes(borrower);
          if(incomes != null)
          {
            Iterator itIn = incomes.iterator();
            Income income = null;
            while(itIn.hasNext())
            {
              income = (Income)itIn.next();
              // -------- Calcuate the Total Income Amount -------------
              //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
              //--> By Billy 02Nov2004
              if(income.getIncIncludeInTDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
              {
                totalIncomeAmount += income.getIncomeAmount() *
                  DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
                  income.getIncPercentInTDS() / 100.00;
              }
              //=============================================================================
              // -------- Calcuate the Total Total TDS Expenses ------------------
              if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
              {
                totalTDSExpenses -= income.getIncomeAmount() *
                  DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
                  income.getIncPercentInTDS() / 100.00;
              }
              trace(" TotalIncomeAmount = " + totalIncomeAmount);
              trace("totalTDSExpenses = " + totalTDSExpenses);
            } // End of Incomes
          }
          //----- liabilities start -------------
          Collection liabilities = this.entityCache.getLiabilities(borrower);
          if(liabilities != null)
          {
            Iterator itLi = liabilities.iterator();
            Liability liability = null;
            while(itLi.hasNext())
            {
              liability = (Liability)itLi.next();
              if(liability.getIncludeInTDS() && liability.getLiabilityPayOffTypeId() == 0) // non-paid off liability
              {
                // "Credit Card" "Unsecured Line of Credit" "Secured Line of Credit"
                // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
                // -------------------------------------------------------------------
                // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
                //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
                if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
                {
                  totalTDSExpenses += liability.getLiabilityAmount() * liability.getPercentInTDS()
                    / 100.00 * 12; // double check !!
                }
                else

                // Liab = "Closing Costs", type = "High ratio"
                if(liability.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
                {
                  totalTDSExpenses += liability.getLiabilityAmount();
                }
                else
                {
                  totalTDSExpenses += liability.getLiabilityMonthlyPayment() * 12 *
                    liability.getPercentInTDS() / 100.00;
                }
              }
              trace("totalTDSExpenses (+liability) = " + totalTDSExpenses);
            } // End of liabilities
          }
        } // --- End of if BorrowerType is not Guarantor
      } // ---------- End of Borrower.hasNext()................

      //-------------Start with Properties:Expense --------------
      Collection pe = this.entityCache.getPropertyExpenses(deal);
      if(pe != null)
      {
        Iterator itPe = pe.iterator();
        // ----- Get the Property Expenses -----------
        PropertyExpense expense = null;
        while(itPe.hasNext())
        {
          expense = (PropertyExpense)itPe.next();
          if(expense.getPeIncludeInTDS())
          {
            totalTDSExpenses += expense.getPropertyExpenseAmount() *
              DealCalcUtil.annualize(expense.getPropertyExpensePeriodId(), "PropertyExpensePeriod") *
              expense.getPePercentInTDS() / 100.00;
          }
          trace("totalTDSExpense (+propertyExpense) = " + totalTDSExpenses);
        } // end of propertyExpenses
      }
      //---------------End of Property:PropertyExpense ---------------
      trace(" totalTDSExpenses + propertyExpense = " + totalTDSExpenses);
      trace("PandI= " + deal.getPandiPaymentAmount());
      trace("AdditionalPrincipal = " + deal.getAdditionalPrincipal());
      trace("paymentFrequency= " + DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

//    start handling MOSSYS prop com.basis100.calc.forcepandifor3year
      /*boolean isForcePandIFor3Year = (PropertiesCache.getInstance().getProperty(
              "com.basis100.calc.forcepandifor3year", "N")).equals("Y");
      
      trace("isForcePandIFor3Year = " + isForcePandIFor3Year + ", ProdTypeId = " + iProdTypeId);
      trace("Original RepaymentTypeId = " + originalRepaymentTypeId + ", AmortizationTerm = " + originalAmortizationTerm);

      if(isForcePandIFor3Year && iProdTypeId == Mc.PRODUCT_TYPE_SECURED_LOC)
      {
    	  deal.setRepaymentTypeId(0);
    	  deal.setAmortizationTerm(300);
    	  trace("forcepandifor3year=Y and ProdTypeId=LOC, set RepaymentTypeId:0, AmortizationTerm:300");
      }*/
//    END of handling MOSSYS prop com.basis100.calc.forcepandifor3year
      
      /*#DG538 allow accelerated precision
      totalTDSExpenses +=
        (DealCalcUtil.PandI3YearRate(srk, entityCache, deal, -100) + deal.getAdditionalPrincipal()) *
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");*/
      totalTDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, true);
      trace("totalTDSExpense = " + totalTDSExpenses);

      if(totalIncomeAmount > 0)
      {
        CombinedTotalTDSBorrowerOnly = totalTDSExpenses / totalIncomeAmount * 100;
        CombinedTotalTDSBorrowerOnly = Math.round(CombinedTotalTDSBorrowerOnly * 100.00) / 100.00;
      }
      else
      {
        CombinedTotalTDSBorrowerOnly = this.getMaximumCalculatedValue();
      }

      logger.trace(">>>Returning from Calc 9, result  = " + CombinedTotalTDSBorrowerOnly
    		  + "(CombinedTotalTDSBorrowerOnly), netInterestRate = " + deal.getNetInterestRate());
      
      deal.setCombinedTDS3Year(validate(CombinedTotalTDSBorrowerOnly));
//      deal.setRepaymentTypeId(originalRepaymentTypeId);
//      deal.setAmortizationTerm(originalAmortizationTerm);
      deal.ejbStore();
      return;
    } // end if borrowers != null
  }
  catch(Exception e)
  {
    String msg = "Benign Failure in Calculation" + this.getClass().getName();
    msg += ":[Target]:" + this.getClass().getName();
    msg += "\nReason: " + e.getMessage();
    msg += "@Target: " + this.getDoCalcErrorDetail(this.getClass().getName());
    logger.debug(msg);
  }

  // if failed set to default double zero
  try
  {
  deal.setCombinedTDS3Year(DEFAULT_FAILED_DOUBLE);
//  deal.setRepaymentTypeId(originalRepaymentTypeId);
//  deal.setAmortizationTerm(originalAmortizationTerm);
  deal.ejbStore();
  }
  catch(Exception ex)
  {
    //String msg = "Default Value Double Zero is set to: " + this.getClass().getName();
    logger.error(ex);
  }
}
//--DJ_Ticket841--end--//

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  } // ----------- End of hasInput()--------------------

  public void initParams() throws ParamTypeException
  {

    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInTDS"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInTDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInTDS"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    //4.3GR Jan 26, 2010, deleting: don't need discount because it's looking at PAndIUsing3YearRate
    //addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIUsing3YearRate"));  //#DG538
    addInputParam(new CalcParam(ClassId.DEAL , "GDSTDS3YearRate"));	//#DG930

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTDS3Year", 999.99));

  } // ---- End of initParams()---------

}
