package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name:  Annual Income Amount
//Code:  Calc 81
//07/05/2000

public class MonthlyExpenseAmount extends DealCalc {

  public MonthlyExpenseAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber  = "Calc 81";
  }


   public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget ( (PropertyExpense) input);
    }
    catch ( Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     PropertyExpense propertyExpense = (PropertyExpense) target;

     try
     {
        double monthlyExpenseAmount = 0.0;

        monthlyExpenseAmount  = propertyExpense.getPropertyExpenseAmount () *
              DealCalcUtil.annualize ( propertyExpense.getPropertyExpensePeriodId () , "PropertyExpensePeriod") / 12.0 ;
        monthlyExpenseAmount = Math.round ( monthlyExpenseAmount * 100 ) /100.00 ; 
        propertyExpense.setMonthlyExpenseAmount ( validate(monthlyExpenseAmount) );
        propertyExpense.ejbStore ();
        return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    propertyExpense.setMonthlyExpenseAmount( DEFAULT_FAILED_DOUBLE );
    propertyExpense.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpenseAmount") );

    addTargetParam(new CalcParam(ClassId.PROPERTYEXPENSE ,"monthlyExpenseAmount", T13D2));
  }

}
