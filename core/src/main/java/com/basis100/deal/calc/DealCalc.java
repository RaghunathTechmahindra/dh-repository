package com.basis100.deal.calc;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;

/**
 * <p>
 * Title : DealCalc
 * </p>
 * 
 * <p>
 * Description : Super class for all calculation implementation classes
 * </p>
 * 
 * @version 1.0 Initial version
 * @Version 1.1 XS_11.11& XS_11.6<br>
 * @Date 14-Jun-2008 <br>
 *       Author: MCM Impl Team Change: <br>
 *       Changes: - Added T12D6 static variable for 12 digits with 6 decimal
 *       places
 * @version 1.2 XS_11.3
 * @Date 18-Jun-2008 <br>
 *       Author: MCM Impl Team Change: <br>
 *       Changes: Added T3 for 3 digits
 * @version 1.3 XS_11.5
 * @Date 25-Jun-2008 <br>
 *       Author: MCM Impl Team Change: <br>
 *       Changes: Added T4 for 4 digits
 */




public abstract class DealCalc implements Cloneable
{
  protected List targetParam;   //the classId of the entity whose field(s) will be updated
  protected List inputParam;     //the CalcInput instances supplying this calc
  protected Set  targets;
  protected List inputs;
  protected Set  inputClassTypes;
  protected SysLogger logger;
  protected SessionResourceKit srk;

  protected String calcNumber = "";

  protected CalcEntityCache entityCache;

  public static int DEFAULT_FAILED_INT = 0;
  public static double DEFAULT_FAILED_DOUBLE = 0.0;
  public static Date DEFAULT_FAILED_DATE = null;
  public static String DEFAULT_FAILED_STRING = null;

  public static double T9D2 = 9999999.99;
  public static double T13D2 = 99999999999.99; // total 13 digits, 2 decimal
  public static double T15D2 = 9999999999999.99; // total 15 digits, 2 decimal
  //Added for XS_11.11 & XS_11.6 14-Jun-2008
  public static double T12D6     = 999999.999999; //Total 12 digits with 6 decimal places

  //Added for XS_11.3
  public static int T3 = 999;
  
//Added for XS_11.5 25-Jun-2008 
  public static int T4 = 9999; //Total 4 Digits
  
  public static double T4D2 = 99.99;
  
  
  public DealCalc()
  {
    logger = ResourceManager.getSysLogger("Calc");
    reset();
  }

  public double getMaximumCalculatedValue()
  {
      return ((CalcParam)targetParam.get(0)).maxCalculatedValue;
  }

	public double validate(double calculatedResult) {

		double max = getMaximumCalculatedValue();
		logger.calcTrace(DealCalc.class, "<<CalculatedResult== " + calculatedResult + ">>");

		//FXP29132, 5.0GR, July 16, 2010: added: returns 0-max when calculatedResult is smaller than that.
		if (max > 0 && Math.abs(calculatedResult) > max) {
			logger.warning("DealCalc Maximum encountered in ["
					+ this.getClass().getName() + "] value= " + calculatedResult);
			
			return (calculatedResult >= 0.0D) ? max : 0.0D - max;
		}

		return calculatedResult;
	}

  public long validate(long calculatedResult)
  {
      // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
      logger.calcTrace ( DealCalc.class, "<<CalculatedResult== " + calculatedResult + ">>" );
      return (long)validate((double)calculatedResult);
  }

  public int validate(int calculatedResult)
  {
      // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
      logger.calcTrace ( DealCalc.class, "<<CalculatedResult== " + calculatedResult + ">>" );
      return (int)validate((double)calculatedResult);
  }


 /**
 * If the object passed is not an instance of Deal Entity the method returns
 * immediately else the class id of the object is determined and
 * checked for a match in the inputs list (inputParam). If a match is found
 * the object parameter o is added to the input objects list (inputs).
 * @param o -  the DealEntity to attach to this calculation
 *
 */
  public final void attachInput(Object o)
  {
     if(!(o instanceof DealEntity))return;

     try
     {
         DealEntity de = (DealEntity)o;

         int id = de.getClassId();
         IEntityBeanPK pk = de.getPk();

         Iterator it = inputs.iterator();

         DealEntity current = null;

         while(it.hasNext())
         {
           current = (DealEntity)it.next();
           if(current.getPk().equals(pk))
           {
             it.remove();
           }
         }

         if(id != -1) inputs.add(o);
     } catch ( Exception e )
     {
         logger.error ( "[DealCalc.attachInput()] Attaching [" + o.getClass ().getName () + "]" + e.getMessage () );
     }
  }

 /**
 *  This method simply iterates over each object in the input object list (inputs).
 *  For each input object the method calls getTarget(o) providing the
 *  input object as the parameter.
 *  If the input object list is empty the method returns immediately.
 *  This method should not be replaced (overridden) in a subclass.
 */
  public final void checkTargets(CalcMonitor dcm)throws TargetNotFoundException
  {
    entityCache = dcm.getCalcEntityCache ();

    if(inputs.isEmpty()) return;

    ListIterator li = inputs.listIterator();

    while(li.hasNext())
    {
      getTarget(li.next(),dcm);
    }
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public Collection getInputs()
  {
    return this.inputs  ;  
  }
 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public abstract void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException ;

 /**
  *  adds the parameter to the targets list (targets)
  *  if not already present.
  */
  public final void addTarget(Object o)
  {
     if(!(o instanceof DealEntity))return;
     
     DealEntity de = (DealEntity)o;
     int id = de.getClassId();

     IEntityBeanPK pk = de.getPk();
     Iterator it = targets.iterator();
     if ( it != null )
     {
         DealEntity current = null;
         while(it.hasNext())
         {
           current = (DealEntity)it.next();
           if( current.getPk().equals(pk))
             it.remove();
         }
     }

     if(id != -1)
     {
       this.entityCache.addEntity(o);
       targets.add(o);
     }

  }


  public abstract void doCalc(Object target)throws Exception;

  public final void reset()
  {
     targets = new HashSet();
     inputs = new ArrayList();
  }

  public final void doCalcs()throws DealCalcException
  {
    try
    {
      Iterator li = targets.iterator();

      while(li.hasNext())
      {
         doCalc(li.next());
      }
    }
    catch(Exception e)
    {
      //String msg = "Calculations: Non - Benign Failure!!!!";
      logger.error(e.getMessage());
      logger.error(e);
    }

  }

  /**
   * Indicates whether this Calculation has an
   * input CalcParam matching the specified parameter.
   * @return true if a matching input is found using equals() semantics
   */
  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  /**
   * Indicates whether this Calculation has an
   * input ClassType id matching the specified parameter.
   * @return true if a matching input class type is found.
   */
  public boolean hasInputClassId(int id)
  {
     return inputClassTypes.contains(new Integer(id));
  }

  /**
  * @return true if this instance has a target CalcParam matching an Input CalcParam
  *  of the supplied DealCalc  else return false
  */
  public boolean isInputTo(DealCalc other)
  {
    Iterator tp = targetParam.iterator();

    while(tp.hasNext())
    {
      if(other.hasInput((CalcParam)tp.next()))
        return true;
    }

    return false;
  }

  /**
   *  Get the specification number associated with this calculation
   *  @return the specification number as a string
   */
  public String getCalcNumber()
  {
    return calcNumber;
  }

 /**
  * Adds a CalcParam object a target for the calculation. An exception
  * is thrown if a parameter object of more than a single class type is added.
  * @param tp a calculation parameter target for this calculation
  */
  public void addTargetParam(CalcParam tp) throws ParamTypeException
  {
    if(targetParam.isEmpty())
     targetParam.add(tp);
    else
    {
      CalcParam first = (CalcParam)targetParam.get(0);
      if(first.getClassId() != tp.getClassId())
       throw new ParamTypeException("Illegal Target Class Type - target must be of type: " + first.getClassId());

    }
  }
  /**
   * Adds a CalcParam object a input for the calculation.
   * @param tp a calculation parameter input for this calculation
   */
  public void addInputParam(CalcParam tp)
  {
    inputParam.add(tp);
    inputClassTypes.add(new Integer(tp.getClassId()));
  }

  public Collection getInputClassTypes()
  {
    return inputClassTypes;
  }

  /**
  *  Two calculation objects are deemed to be equal if they are the same calculation
  *  type.
  *
  * @return true if this DealCalc has a calcNumber equal to the parameter calcNumber
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    DealCalc other  = (DealCalc)object;

    if(getCalcNumber().equals(other.getCalcNumber()))
      return true;

     return false;
  }

  public int hashCode()
  {
    return calcNumber.hashCode() + this.getClass().getName().hashCode();
  }

  public String toString()
  {
    String out = "[" + calcNumber + "]";

    return out;
  }

  public void trace(String in)
  {
      // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
      this.logger.calcTrace(DealCalc.class, in);
  }

  public void setResourceKit(SessionResourceKit srkin)
  {
    srk = srkin;
  }

  public Object clone() throws CloneNotSupportedException
  {
     try
     {
        DealCalc other =  (DealCalc)super.clone();
        other.inputs = new ArrayList();
        other.targets = new HashSet();
        other.logger = ResourceManager.getSysLogger("Deal Calc Clone");
        return other;
     }
     catch(Exception e)
     {
        e.printStackTrace();
        return null;
     }

  }

  public List getTargetParams()
  {
    return this.targetParam ;
  }
  public List getInputParams()
  {
    return this.inputParam ; 
  }

  public boolean oneOf(int id, int l1)
  {
      return (id == l1);
  }

  public boolean oneOf(int id, int l1, int l2)
  {
      return (id == l1) || (id == l2);
  }

  public boolean oneOf(int id, int l1, int l2, int l3)
  {
      return (id == l1) || (id == l2) || (id == l3);
  }

  public boolean oneOf(int id, int l1, int l2, int l3, int l4)
  {
      return (id == l1) || (id == l2) || (id == l3) || (id == l4);
  }

  public boolean oneOf(int id, int l1, int l2, int l3, int l4, int l5)
  {
      return (id == l1) || (id == l2) || (id == l3) || (id == l4) || (id == l5);
  }

  public String  getDoCalcErrorDetail( Object target )
  {
      try
      {
         if ( target instanceof DealEntity )
         {
             DealEntity d = (DealEntity) target;
             return "[" + d.getPk().getName () + "=" + d.getPk().getId () + "]" +
                    "[CopyId=" + d.getPk().getCopyId () + "]" ;
         }
         else
         {
              return "[Target/Input Type Not DealEntity]" ;
         }
      } catch ( Exception e )
      {
         return e.getMessage ();
      }
  }
}
