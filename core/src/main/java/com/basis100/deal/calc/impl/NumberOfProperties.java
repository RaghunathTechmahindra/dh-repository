package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Number of Properties
//Code: Clac-35
//07/04/2000

public class NumberOfProperties extends DealCalc {

  public NumberOfProperties() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 35";
  }


  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof Property )
      {
          Property property = (Property) input;
          Deal deal = (Deal) entityCache.find (srk , 
                           new DealPK( property.getDealId (), property.getCopyId())) ;
          addTarget( deal );
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;

    try
    {
        int NumberOfProperties = 0;
        NumberOfProperties = this.entityCache.getProperties (deal).size ();
        
        deal.setNumberOfProperties (validate(NumberOfProperties) );
        deal.ejbStore();
        return;
    }
    catch (Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

    deal.setNumberOfProperties (this.DEFAULT_FAILED_INT );
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam( new CalcParam( ClassId.PROPERTY ,  "dealId")) ;
    addInputParam( new CalcParam( ClassId.PROPERTY ,  "copyId")) ;

    addTargetParam( new CalcParam( ClassId.DEAL , "numberOfProperties", 99 ));

  }

} 
