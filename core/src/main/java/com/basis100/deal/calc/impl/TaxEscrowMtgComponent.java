package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: TaxEscrowMtgComponent
 * </p>
 * <p>
 * Description:Calculates the Escrow Payment Amount based on payment frequency
 * for the Mortgage component.
 * </p>
 * @author MCM Impl Team <br>
 * @version 1.0 Initial Version <br>
 * @Date 16-Jun-2008 <br>
 *       Story - XS_11.4 <br>
 *       Code Calc-107.CM
 * @version 1.1 XS_11.4 24-Jun-2008 Review Comments Incorporated
 * @version 1.2 XS_11.4 26-Jun-2008 Modified initParams() method.
 */
public class TaxEscrowMtgComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     */
    public TaxEscrowMtgComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 107.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            Deal deal = null;
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;

                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    deal = (Deal) input;
                }
                else
                    if (classId == ClassId.PROPERTY)
                    {
                        Property property = (Property) input;
                        // Retrives the Deal Object for the given dealId and
                        // copyId
                        deal = (Deal) entityCache.find(srk, new DealPK(property
                                .getDealId(), property.getCopyId()));

                    }
            // Deal is common for Property, Deal.if Deal Object is not null
            // then, Retrive the components and add Component Mortagage as
            // target
            if (deal != null)
            {
                Collection listOfcomponents = entityCache.getComponents(deal,
                        srk);
                Iterator componentsItr = listOfcomponents.iterator();
                while (componentsItr.hasNext())
                {
                    Component component = (Component) componentsItr.next();
                    // Checks the component type is MORTGAGE COMPONENT
                    if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                    {
                        // Retrives the componentMtg Object for the
                        // given componentId and copyId
                        ComponentMortgage componentMtg = (ComponentMortgage) entityCache
                                .find(srk, new ComponentMortgagePK(component
                                        .getComponentId(), component
                                        .getCopyId()));
                        addTarget(componentMtg);

                    }
                }
            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @version 1.1 XS_11.4 Review Comments Incorporated
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        try
        {
            ComponentMortgage targetComponentMtg = (ComponentMortgage) target;
            // Retrive the component for the given component Mortgage
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentMtg.getComponentId(),
                            targetComponentMtg.getCopyId()));
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));
            // Retrive the Primary property object for the given dealId and
            // copyId
            Property primaryProp = new Property(srk, null)
                    .findByPrimaryProperty(deal.getDealId(), deal.getCopyId(),
                            deal.getInstitutionProfileId());
            // Retrive the autocalc TaxEscrow value from the Sysproperty
            String autoCalTaxEscrowSysProp = PropertiesCache.getInstance()
                    .getProperty(deal.getInstitutionProfileId(),
                            "com.basis100.calc.autocalctaxescrow");
            if (deal.getTaxPayorId() == Mc.TAX_PAYOR_BORROWER
                    || ("N").equalsIgnoreCase(targetComponentMtg
                            .getPropertyTaxAllocateFlag()))
            {
                targetComponentMtg.setPropertyTaxEscrowAmount(Mc.DEFAULT_VALUE);
                targetComponentMtg.ejbStore();
                return;

            }
            if (deal.getTaxPayorId() == Mc.TAX_PAYOR_LENDER
                    && ("Y".equalsIgnoreCase(targetComponentMtg
                            .getPropertyTaxAllocateFlag())))
            {
                // com.basis100.calc.autocalctaxescrow = T (TD method)
                if (("T").equalsIgnoreCase(autoCalTaxEscrowSysProp))
                {
                    double propertyTaxEscrowAmount = DealCalcUtil
                            .calculatePropertyTaxEscrowForTD(targetComponentMtg
                                    .getInterestAdjustmentDate(), primaryProp
                                    .getTotalAnnualTaxAmount());
                    targetComponentMtg
                            .setPropertyTaxEscrowAmount(validate(propertyTaxEscrowAmount));
                    targetComponentMtg.ejbStore();
                }
                // com.basis100.calc.autocalctaxescrow = �X� (Xceed method)
                else
                    if ("X".equalsIgnoreCase(autoCalTaxEscrowSysProp))
                    {
                        double propertyTaxEscrowAmount = DealCalcUtil
                                .calculatePropertyTaxEscrowForXceed(
                                        targetComponentMtg
                                                .getFirstPaymentDate(),
                                        primaryProp.getTotalAnnualTaxAmount(),
                                        primaryProp.getProvinceId(),
                                        targetComponentMtg
                                                .getPaymentFrequencyId());
                        targetComponentMtg
                                .setPropertyTaxEscrowAmount(validate(propertyTaxEscrowAmount));
                        targetComponentMtg.ejbStore();
                    }
                    // com.basis100.calc.autocalctaxescrow = �D� (Desjardins
                    // method)
                    else
                        if ("D".equalsIgnoreCase(autoCalTaxEscrowSysProp))
                        {
                            double propertyTaxEscrowAmount = DealCalcUtil
                                    .calculatePropertyTaxEscrowForDesjardins(
                                            targetComponentMtg
                                                    .getPaymentFrequencyId(),
                                            primaryProp
                                                    .getTotalAnnualTaxAmount());
                            targetComponentMtg
                                    .setPropertyTaxEscrowAmount(validate(propertyTaxEscrowAmount));
                            targetComponentMtg.ejbStore();
                        }
            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Initialize the target parameters
     * </p>
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @version 1.1 26-Jun-2008 Added 'primaryPropertyFlag' as an InputParameter
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();
        addInputParam(new CalcParam(ClassId.DEAL, "taxPayorId"));
        addInputParam(new CalcParam(ClassId.PROPERTY, "totalAnnualTaxAmount"));
        addInputParam(new CalcParam(ClassId.PROPERTY, "provinceId"));
        addInputParam(new CalcParam(ClassId.PROPERTY, "primaryPropertyFlag"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "propertyTaxAllocateFlag"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "firstPaymentDate"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "interestAdjustmentDate"));
        // Max propertyTaxEscrowAmount = 99999999999.99
        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "propertyTaxEscrowAmount", T13D2));

    }
}
