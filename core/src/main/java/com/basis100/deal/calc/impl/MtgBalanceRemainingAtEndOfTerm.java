/*
 * @(#)MtgBalanceRemainingAtEndOfTerm.java July 07, 2008 <story id : XS 11.7>
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

/**
 */

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: MtgBalanceRemainingAtEndOfTerm
 * </p>
 * <p>
 * Description: Calculates Balance remaining at end of term for Component
 * Mortgage
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.7 07-July-2008 Initial version
 * @version 1.1 XS_11.7 17-July-2008 doCalc() - Changed variable i to paymentNumber
 * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
 */
public class MtgBalanceRemainingAtEndOfTerm extends DealCalc
{

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @throws Exception
     * @author MCM Impl Team
     * @version 1.0 XS_11.7 07-July-2008 Initial version
     */
    public MtgBalanceRemainingAtEndOfTerm() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "CALC 111.CM";
    }

    /**
     * <p>
     * Description: Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @version 1.0 XS_11.7 Initial Version
     * @version 1.2 artf751756 1-Aug-2008 added try catch block
     * @param input
     *            Object Input component.
     * @param dcm
     *            CalcMonitor runs the calculation of this Component entity.
     * @throws TargetNotFoundException
     *             exception that may occurs if the target is not found.
     * @see com.basis100.deal.calc.DealCalc#getTarget(java.lang.Object,
     *      com.basis100.deal.calc.CalcMonitor)
     */
    @Override
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        int classId = ((DealEntity) input).getClassId();

        try
        {
            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    // MCM artf751756 add try-catch
                    Component component = (Component) input;
                    try {
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                        {
                            ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
                                    .find(srk, new ComponentMortgagePK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            addTarget(componentMtg);
                        }
                    } catch (Exception e) {
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Target for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Performs the calculation based on the calculation rules in
     * CALC#111.CM
     * </p>
     * @version 1.0 XS_11.7 Initial Version
     * @version 1.1 July-16-2008 Changed variable i to paymentNumber
     * @param target
     *            Object target component.
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     */
    @Override
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());

        ComponentMortgage targetComponentMtg = (ComponentMortgage) target;
        double balanceAmount = 0.0d;
        int interestCompoundingId = -1;

        try
        {

            // Get the Component entity
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentMtg.getComponentId(),
                            targetComponentMtg.getCopyId()));

            int repaymentTypeId = component.getRepaymentTypeId();
            int amortizationTerm = targetComponentMtg.getAmortizationTerm();
            int paymentFrequencyId = targetComponentMtg.getPaymentFrequencyId();
            double totalMortgageAmount = targetComponentMtg
                    .getTotalMortgageAmount();
            double actualPaymentTerm = targetComponentMtg
                    .getActualPaymentTerm();
            double netInterestRate = targetComponentMtg.getNetInterestRate();
            double pAndIpaymentAmount = targetComponentMtg
                    .getPAndIPaymentAmount();

            if (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
                    || repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED)
            {
                balanceAmount = totalMortgageAmount;
                targetComponentMtg
                        .setBalanceRemainingAtEndOfTerm(balanceAmount);
            }
            else
                if (actualPaymentTerm == amortizationTerm)
                {
                    balanceAmount = 0.0d;
                    targetComponentMtg
                            .setBalanceRemainingAtEndOfTerm(balanceAmount);
                }
                else
                {
                    MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                            new MtgProdPK(component.getMtgProdId()));
                    interestCompoundingId = mtgProd.getInterestCompoundingId();
                    boolean dJDaysYearFlag = Boolean
                            .getBoolean(PropertiesCache
                                    .getInstance()
                                    .getProperty(
                                            targetComponentMtg
                                                    .getInstitutionProfileId(),
                                            "com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear"));

                    // Payments per year
                    double paymentsPerYear = DealCalcUtil.calcPaymentFrequency(
                            targetComponentMtg.getPaymentFrequencyId(),
                            dJDaysYearFlag);

                    // Get interest compounding description
                    String interestCompoundDescription = BXResources
                            .getPickListDescription(targetComponentMtg
                                    .getInstitutionProfileId(),
                                    "INTERESTCOMPOUND", interestCompoundingId,
                                    srk.getLanguageId());

                    // Calculate Interest Factor
                    double interestFactor = DealCalcUtil.interestFactor(
                            netInterestRate, paymentsPerYear,
                            interestCompoundDescription);

                    // Calculate total payments and truncate to
                    // an integer
                    int payments = DealCalcUtil
                            .pCalcNumOfPaymentsPerYear(paymentFrequencyId);

                    double totalPayments = actualPaymentTerm / 12d * payments;

                    int totalPaymentsTruncated = (int) totalPayments;

                    double interest, principal;

                    balanceAmount = totalMortgageAmount;

                    for ( int paymentNumber = 1; paymentNumber <= totalPaymentsTruncated; paymentNumber++ )
                    {
                        // Round to nearest hundredth
                        interest = DealCalcUtil
                                .roundToNearestHundredth(balanceAmount
                                        * interestFactor);
                        principal = DealCalcUtil
                                .roundToNearestHundredth(pAndIpaymentAmount
                                        - interest);
                        balanceAmount = DealCalcUtil
                                .roundToNearestHundredth(balanceAmount
                                        - principal);
                    }

                    if (balanceAmount < 0.0d)
                    {
                        balanceAmount = 0.0d;
                    }
                    targetComponentMtg
                            .setBalanceRemainingAtEndOfTerm(validate(balanceAmount));
                }

            targetComponentMtg.ejbStore();
            return;
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
            throw e;
        }
    }

    /**
     * <p>
     * Description: Initializes the input & target parameters which is called in
     * the constructor.
     * </p>
     * @version 1.0 XS_11.7 Initial Version
     * @throws ParamTypeException
     *             exception that may occur if param type is not valid.
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "actualPaymentTerm"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "amortizationTerm"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "pAndIPaymentAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "repaymentTypeId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));

        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "balanceRemainingAtEndOfTerm", T12D6));
    }

}
