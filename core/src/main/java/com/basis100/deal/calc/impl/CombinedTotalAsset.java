package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Combined Total Asset
//Code: Calc 25
//07/08/2000

public class CombinedTotalAsset extends DealCalc
{

  public CombinedTotalAsset() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 25";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      Asset a = (Asset) input;
      Borrower b = (Borrower) entityCache.find (srk, 
                           new BorrowerPK(a.getBorrowerId (), 
                                          a.getCopyId ()));
      Deal d = (Deal) entityCache.find ( srk, 
                           new DealPK( b.getDealId(), b.getCopyId()));
      addTarget(  d ) ;
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Deal deal = ( Deal ) target;

     try
     {
        double combinedTotalAssetValue = 0.0;

        Collection assets = this.entityCache.getAssets (deal);
        Iterator itAssets = assets.iterator ();
        Asset asset = null;

        while ( itAssets.hasNext () )
        {
          asset = ( Asset ) itAssets.next ();
          combinedTotalAssetValue +=  asset.getAssetValue () ;
          trace( "combinedTotalAsset = " + combinedTotalAssetValue );
        }
        combinedTotalAssetValue = Math.round ( combinedTotalAssetValue * 100 ) / 100.00;
        deal.setCombinedTotalAssets ( validate(combinedTotalAssetValue) );
        deal.ejbStore ();
        return;
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     deal.setCombinedTotalAssets (this.DEFAULT_FAILED_DOUBLE);
     deal.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.ASSET, "assetValue"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalAssets", T13D2));
  }



} 
