package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * Title: FirstPaymentDateLimitComponent
 * </p>
 * <p>
 * Description:Calculates the The First Payment Date Limit will be calculated
 * for each component as 1 day beyond the maximum allowable First Payment Date
 * (Maximum First Payment Date is = Estimated Closing Date + two payment
 * frequencies � 1 Day)
 * </p>
 * Calculation Number- Calc-100.C
 * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
 */
public class FirstPaymentDateLimitComponent extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     */
    public FirstPaymentDateLimitComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 100.C";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();
            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                // find component for the given componentId and
                // copyId
                Component component = (Component) entityCache.find(srk,
                        new ComponentPK(componentMtg.getComponentId(),
                                componentMtg.getCopyId()));
                addTarget(component);
            }
            else
                if (classId == ClassId.COMPONENTLOC)
                {
                    ComponentLOC componentLoc = (ComponentLOC) input;
                    // find component for the given componentId and
                    // copyId
                    Component component = (Component) entityCache.find(srk,
                            new ComponentPK(componentLoc.getComponentId(),
                                    componentLoc.getCopyId()));
                    addTarget(component);
                }
                else
                    if (classId == ClassId.COMPONENTLOAN)
                    {
                        ComponentLoan componentLoan = (ComponentLoan) input;
                        // find component for the given componentId and
                        // copyId
                        Component component = (Component) entityCache.find(srk,
                                new ComponentPK(componentLoan.getComponentId(),
                                        componentLoan.getCopyId()));
                        addTarget(component);
                    }
                    else
                        if (classId == ClassId.DEAL)
                        {
                            Deal deal = (Deal) input;
                            Collection listOfcomponents = entityCache
                                    .getComponents(deal, srk);
                            Iterator componentsItr = listOfcomponents
                                    .iterator();
                            while (componentsItr.hasNext())
                            {
                                Component component = (Component) componentsItr
                                        .next();
                                int componentType = component
                                        .getComponentTypeId();
                                if (componentType == Mc.COMPONENT_TYPE_LOC
                                        || componentType == Mc.COMPONENT_TYPE_MORTGAGE
                                        || componentType == Mc.COMPONENT_TYPE_LOAN)
                                {
                                    addTarget(component);
                                }

                            }
                        }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());

        try
        {
            Component targetComponent = (Component) target;
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(
                    targetComponent.getDealId(), targetComponent.getCopyId()));

            int paymentFrequency = 0;
            int componentTypeId = targetComponent.getComponentTypeId();
            if (componentTypeId == Mc.COMPONENT_TYPE_MORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
                        .find(srk, new ComponentMortgagePK(targetComponent
                                .getComponentId(), targetComponent.getCopyId()));
                paymentFrequency = componentMtg.getPaymentFrequencyId();
            }
            else
                if (componentTypeId == Mc.COMPONENT_TYPE_LOC)
                {
                    ComponentLOC componentLoc = (ComponentLOC) this.entityCache
                            .find(srk, new ComponentLOCPK(targetComponent
                                    .getComponentId(), targetComponent
                                    .getCopyId()));
                    paymentFrequency = componentLoc.getPaymentFrequencyId();
                }
                else
                    if (componentTypeId == Mc.COMPONENT_TYPE_LOAN)
                    {
                        ComponentLoan componentLoan = (ComponentLoan) this.entityCache
                                .find(srk, new ComponentLoanPK(targetComponent
                                        .getComponentId(), targetComponent
                                        .getCopyId()));
                        paymentFrequency = componentLoan
                                .getPaymentFrequencyId();
                    }
            // Retrive the number of days for the given paymentFrequency
            int period = DealCalcUtil.getPeriodInDays(paymentFrequency);
            Calendar firstPaymentDate = Calendar.getInstance();
            Date estimatedClosingDate = deal.getEstimatedClosingDate();
            firstPaymentDate.setTime(estimatedClosingDate);
            if (period == 30)
                // If it is a monthly adding two months (2*Period)
                firstPaymentDate.add(Calendar.MONTH, 2); // monthly
            else
                if (period == 15)
                    // If it is Semi monthly adding 1 month (2*period =30)
                    firstPaymentDate.add(Calendar.MONTH, 1); // semi-monthly
                else
                    firstPaymentDate.add(Calendar.DAY_OF_MONTH, period * 2);// EstimatedClosingDate+period
                                                                            // *2

            targetComponent
                    .setFirstPaymentDateLimit(firstPaymentDate.getTime());
            targetComponent.ejbStore();
            return;

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "paymentFrequencyId"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "paymentFrequencyId"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "paymentFrequencyId"));
        addTargetParam(new CalcParam(ClassId.COMPONENT, "firstPaymentDateLimit"));
    }
}
