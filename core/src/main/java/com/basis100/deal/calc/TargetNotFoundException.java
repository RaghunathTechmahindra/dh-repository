package com.basis100.deal.calc;

public class TargetNotFoundException extends DealCalcException
{
   /**
  * Constructs a <code>TargetNotFoundException</code> with no specified detail message.
  */

  public TargetNotFoundException()
  {
	  super();
  }

  /**
  * Constructs a <code>TargetNotFoundException</code> with the specified message.
  * @param   msg  the related message.
  */
  public TargetNotFoundException(String msg)
  {
    super(msg);
  }

} 