package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Total Loan  and bridge Amount
//Code: Calc 76
// 7/4/2000

public class TotalLoanBridgeAmount extends DealCalc {

  public TotalLoanBridgeAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 76";
  }


  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        if ( input instanceof Deal )
        {
            addTarget(input);
        }
        else if ( input instanceof Bridge )
        {
            Bridge bridge= ( Bridge ) input;
            Deal deal = (Deal) entityCache.find (srk,
                   new DealPK( bridge.getDealId(), bridge.getCopyId ())   );
            addTarget( deal ) ;
         }
    }
    catch ( Exception e )
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal= ( Deal)  target;
     try
     {
       Bridge bridge= deal.getBridge ();

       double totalLoanBridgeAmount = deal.getTotalLoanAmount ();

       if(bridge != null)
       {
         totalLoanBridgeAmount += bridge.getNetLoanAmount();
       }

       totalLoanBridgeAmount = Math.round ( totalLoanBridgeAmount * 100 ) /100.00 ; 
       deal.setTotalLoanBridgeAmount ( validate(totalLoanBridgeAmount));
       deal.ejbStore();
       return;
      }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
        
     }
    deal.setTotalLoanBridgeAmount (DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();

  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();                              

    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.BRIDGE , "netLoanAmount" )) ;

    addTargetParam(new CalcParam(ClassId.DEAL, "totalLoanBridgeAmount", T13D2 ));

  }
    
}
