package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name: Combined Total GDS for Borrower Only
//Code: CALC-7
//06/29/2000

public class CombinedTotalGDSBorrowerOnly extends DealCalc
{

  public CombinedTotalGDSBorrowerOnly() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 7";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                               new BorrowerPK(i.getBorrowerId(), 
                                              i.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                               new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Income ==> Deal
      if(input instanceof PropertyExpense)
      {
        PropertyExpense pe = (PropertyExpense)input;
        Property p = (Property)entityCache.find(srk, new PropertyPK(pe
        		.getPropertyId(),pe.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, new DealPK(p
        		.getDealId(), p.getCopyId()));
        addTarget(d);
        return;
      } // PropertyExpense ==> Deal
      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                                new BorrowerPK(l.getBorrowerId(), 
                                               l.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                                new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Liability ==> Deal

      if(input instanceof Borrower)
      {
        Borrower b = (Borrower)input;
        Deal d = (Deal)entityCache.find(srk, 
                            new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      }

      if(input instanceof Deal)
      {
        addTarget(input);
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Deal deal = (Deal)target;

    try
    {
      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));

      double GDSRatioBorrowerOnly = 0;
      String rateCode = pricingProfile.getRateCode();
      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          deal.setCombinedGDSBorrower(validate(GDSRatioBorrowerOnly));
          deal.ejbStore();
          return;
        }
      }

      Collection borrowers = this.entityCache.getBorrowers(deal);
      if(borrowers != null)
      {
        Iterator itBO = borrowers.iterator();

        double totalAnnualIncome = 0.0;
        double totalGDSExpenses = 0.0;
        Borrower borrower = null;
        while(itBO.hasNext()) // Loop through all the borrowers
        {
          borrower = (Borrower)itBO.next();

          if(borrower.getBorrowerTypeId() != Mc.BT_GUARANTOR /* Guarantor */)
          {
            // ----- All the Incomes -------------
            Collection incomes = this.entityCache.getIncomes(borrower);
            Iterator itIn = incomes.iterator();
            while(itIn.hasNext())
            { //---------Loop through all the incomes, and get the Total Annual Income ---------
              Income income = (Income)itIn.next();
              //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
              //--> By Billy 02Nov2004
              if(income.getIncIncludeInGDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
              {
                totalAnnualIncome += income.getIncomeAmount() *
                  DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
                  income.getIncPercentInGDS() / 100.00;
                // -------- Get the Total Total GDS Expenses ------------------
              }
              //==============================================================================
              if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
              {
                totalGDSExpenses = totalGDSExpenses - income.getIncomeAmount() *
                  DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
                  income.getIncPercentInGDS() / 100.00;
              }
              trace(" IncomePeriod = " + DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod"));
              trace("totalGDSExpenses = " + totalGDSExpenses);
            }
            // ----- All the Liabilities  -------------
            Collection liabilities = this.entityCache.getLiabilities(borrower);
            Iterator itLi = liabilities.iterator();
            while(itLi.hasNext()) //Loop through all the ** liabilities
            {
              Liability liability = (Liability)itLi.next();
              // Include in GST and Libility Payoff Type is Blank, ( id=0 )
              if(liability.getIncludeInGDS() && (liability.getLiabilityPayOffTypeId() == 0))
              {
                // "Credit Card", "Unsecured Line of Credit" "Secured Line of Credit"
                // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
                // -------------------------------------------------------------------
                // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
                //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
                if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
                {
                  totalGDSExpenses += liability.getLiabilityAmount() * liability.getPercentInGDS() / 100.00 * 12;
                }
                else

                // Liab = "Closing Costs", type = "High ratio"
                if(liability.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
                {
                  totalGDSExpenses += liability.getLiabilityAmount();
                }
                else
                {
                  totalGDSExpenses += liability.getLiabilityMonthlyPayment() * 12 *
                    liability.getPercentInGDS() / 100.00;
                }
              }
              trace("liability.getLiabilityAmount () = " + liability.getLiabilityAmount());
              trace("totalGDSExpenses = " + totalGDSExpenses);
            }

          } // --- End of borrower Type != Guarantor ......
        } // ---------- End of itBo.hasNext()................

        Collection pe = this.entityCache.getPropertyExpenses(deal);
        Iterator itPe = pe.iterator();

        // ----- Get the Property Expenses -----------
        while(itPe.hasNext())
        { //Loop thourgh all the Property Expenses
          PropertyExpense propertyExpense = (PropertyExpense)itPe.next();
          if(propertyExpense.getPeIncludeInGDS())
          {
            totalGDSExpenses = totalGDSExpenses + propertyExpense.getPropertyExpenseAmount() *
              DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(), "PropertyExpensePeriod")
              * propertyExpense.getPePercentInGDS() / 100.00;
          }
          trace("propertyExpense = " + totalGDSExpenses);
        }

        /*#DG538 allow accelerated precision
        totalGDSExpenses +=
          (deal.getPandiPaymentAmount() + deal.getAdditionalPrincipal())
          * DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"); */
        totalGDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, false);

        trace("PandI = " + deal.getPandiPaymentAmount());
        trace(" AdditionalPrincipal = " + deal.getAdditionalPrincipal());
        trace("PaymentFrequency = " +
          DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

        if(totalAnnualIncome != 0)
        {
          GDSRatioBorrowerOnly = totalGDSExpenses / totalAnnualIncome * 100;
          GDSRatioBorrowerOnly = Math.round(GDSRatioBorrowerOnly * 100.00) / 100.00;
        }
        else
        {
          GDSRatioBorrowerOnly = this.getMaximumCalculatedValue();

        }
        deal.setCombinedGDSBorrower(validate(GDSRatioBorrowerOnly));
        deal.ejbStore();
        return;
      }
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(target);
      logger.debug(msg);
    }

    deal.setCombinedGDSBorrower(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  public void initParams() throws ParamTypeException
  {

    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    //#DG538
    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIPaymentAmountMonthly"));
    //#DG538 end

    addInputParam(new CalcParam(ClassId.BORROWER, "borrowerTypeId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedGDSBorrower", 999.99));

  }

}
