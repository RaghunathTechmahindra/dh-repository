package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.PropertiesCache;

//Name: Required Down Payment Amount
//Code: Calc 40
//07/4/2000

public class RequiredDownPaymentAmount extends DealCalc
{

  public RequiredDownPaymentAmount()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 40";
  }

  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
   try
    {
      addTarget(input);
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
      trace(this.getClass().getName());
      Deal deal = (Deal) target;

      try
      {
          double RequiredDownPaymentAmount = 0.0;
          if(deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS)
          {
        	  RequiredDownPaymentAmount = (deal.getTotalPurchasePrice () + deal.getRefiImprovementAmount()) 
        	  								- deal.getNetLoanAmount();
          }
          else
          {
          RequiredDownPaymentAmount = deal.getTotalPurchasePrice () - deal.getNetLoanAmount();
          }
          trace( "totalPurchasePrice = " + deal.getTotalPurchasePrice () );
          trace( "netLoanAmount = " + deal.getNetLoanAmount() );
          
          RequiredDownPaymentAmount = Math.round ( RequiredDownPaymentAmount * 100 ) / 100.00 ;  
          deal.setRequiredDownpayment ( validate(RequiredDownPaymentAmount) );
          deal.ejbStore ();
          return;
      }
      catch ( Exception e)
      {
          String msg = "Benign Failure in Calculation" + this.getClass().getName();
          msg += ":[Target]:" + target.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@Target: " + this.getDoCalcErrorDetail ( target );
          logger.debug (msg );
      }

     deal.setRequiredDownpayment ( DEFAULT_FAILED_DOUBLE );
     deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "totalPurchasePrice"));
    addInputParam(new CalcParam(ClassId.DEAL, "netLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "refiImprovementAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealPurposeId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "requiredDownpayment", T13D2));
  }

}
