package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.deal.pk.*;
import MosSystem.Mc;

//Name: Borrower TDS
//Code: Calc 50
//07/12/2000

public class BorrowerTDS extends DealCalc
{

  public BorrowerTDS() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 50";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower borrower = (Borrower)entityCache.find(srk, 
                                new BorrowerPK(i.getBorrowerId(), 
                                               i.getCopyId()));
        addTarget(borrower);
        return;
      }

      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower borrower = (Borrower)entityCache.find(srk, 
                                 new BorrowerPK(l.getBorrowerId(), 
                                                l.getCopyId()));
        addTarget(borrower);
        return;
      }

      Deal d = null;
      if(input instanceof PropertyExpense)
      {
        PropertyExpense propertyExpense = (PropertyExpense)input;
        Property property = (Property)entityCache.find(srk, 
                     new PropertyPK(propertyExpense.getPropertyId(),
                        propertyExpense.getCopyId()));

        d = (Deal)entityCache.find(srk, new DealPK(property
        		.getDealId(), property.getCopyId()));
      }
      else if(input instanceof Deal)
      {
        d = (Deal)input;
      }

      Collection bors = entityCache.getBorrowers(d);
      if(bors != null)
      {
        Iterator it = bors.iterator();
        while(it.hasNext())
        {
          DealEntity od = (DealEntity)it.next();
          addTarget(od);
          //addTarget(it.next());
        }
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }

  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Borrower bt = (Borrower)target;

    try
    {
      double totalAnnualIncome = 0;
      double period = 1;
      int percent = 0;
      double amount = 0;
      double currentIncome = 0;
      double tdsExpenseReduction = 0;
      double totalTDSExp = 0;

      Collection incomes = this.entityCache.getIncomes(bt);
      if(incomes != null)
      {
        Iterator it = incomes.iterator();
        Income current = null;
        while(it.hasNext())
        {
          current = (Income)it.next();
          if(current.getIncIncludeInTDS())
          {
            amount = current.getIncomeAmount();
            period = DealCalcUtil.annualize(current.getIncomePeriodId(), "IncomePeriod");
            percent = current.getIncPercentInTDS();
            currentIncome = amount * period * percent / 100.00; // double check !!

            if(current.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
            {
              tdsExpenseReduction += currentIncome;
            }
            //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
            //--> By Billy 02Nov2004
            else
            {
              totalAnnualIncome += currentIncome;
            }
            //=============================================================================
          }
          trace("[totalAnnualIncome= " + totalAnnualIncome + "]");
        } // end of .. whiile ( it.hasNext() ) ..
      } // end of .. if ( incomes != null ) ...

      trace("[totalAnnualIncome= " + totalAnnualIncome + "]");

      Deal deal = (Deal)entityCache.find(srk, 
                       new DealPK(bt.getDealId(), bt.getCopyId()));

      Collection pec = this.entityCache.getPropertyExpenses(deal);
      if(pec != null)
      {
        Iterator pit = pec.iterator();

        totalTDSExp = -1 * (tdsExpenseReduction);

        PropertyExpense currp = null;

        while(pit.hasNext())
        {
          currp = (PropertyExpense)pit.next();

          if(currp.getPeIncludeInTDS())
          {
            period = DealCalcUtil.annualize(currp.getPropertyExpensePeriodId(), "PropertyExpensePeriod");
            amount = currp.getPropertyExpenseAmount();
            percent = currp.getPePercentInTDS();

            totalTDSExp += amount * period * percent / 100.00;
          }
          trace("[totalPropertyExpense= " + totalTDSExp + "]");
        } // end of pit.hasNext()
      } // end of  .. if ( pec != null ) ..
      trace("[totalPropertyExpense= " + totalTDSExp + "]");

      // get all the liabilities
      Collection liabilties = this.entityCache.getLiabilities(bt);
      if(liabilties != null)
      {
        Iterator lit = liabilties.iterator();
        Liability curl = null;

        while(lit.hasNext())
        {
          curl = (Liability)lit.next();

          if(curl.getIncludeInTDS() && curl.getLiabilityPayOffTypeId() == 0)
          {
            amount = curl.getLiabilityAmount(); // multi paymentfrequency  to do

            // Modified to double by BILLY 17Jan2001
            double lPercent = curl.getPercentInTDS();

            double val = amount * lPercent / 100.00 * 12; // to do

            // "Credit Card" "Unsecured Line of Credit" "Secured Line of Credit"
            // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
            // -------------------------------------------------------------------
            // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
            //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
            if(curl.getLiabilityPaymentQualifier() != null && curl.getLiabilityPaymentQualifier().equals("V"))
            {
              totalTDSExp += val;
            }
            // Liab = "Closing Costs", Type = "High ratio"
            else if(curl.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
            {
              totalTDSExp += val;
            }
            else
            {
              totalTDSExp +=
                (curl.getLiabilityMonthlyPayment() * 12) * lPercent / 100.00;
            }
          } //end if curl.getIncludeInTDS () && ....
          trace("[propertyExpense + liabilityExpense= " + totalTDSExp + "]");
        } //end while  lit.hasNext ()
      } // end of .. if ( liabilities != null )
      trace("[propertyExpense + liabilityExpense= " + totalTDSExp + "]");

      double annualizedPaymentFrequency =
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");

      trace("[PaymentFrequency= " + annualizedPaymentFrequency + "]");
      trace("PandI = " + deal.getPandiPaymentAmount());
      trace(" AdditionalPrincipal = " + deal.getAdditionalPrincipal());

      //non - 3 year expression
      /*#DG538 allow accelerated precision
      double total = totalTDSExp +
        (deal.getPandiPaymentAmount() + deal.getAdditionalPrincipal())
        * annualizedPaymentFrequency; */
       double total = totalTDSExp +
           DealCalcUtil.PandIExpense(srk, entityCache, deal, false);
      trace("[propertyExpense + liabilityExpense + (PandI + additianal)*frequency= " + totalTDSExp + "]");

      double ratio = getMaximumCalculatedValue();
      if(totalAnnualIncome != 0.0)
      {
        ratio = Math.round(total * 10000.00 / totalAnnualIncome) / 100.00;

      }

      bt.setTDS(validate(ratio));
      bt.ejbStore();

      return;
    }
    catch(Exception e)
    {
      String msg = "Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(target);
      logger.error(msg);
    }

    bt.setTDS(DEFAULT_FAILED_DOUBLE);
    bt.ejbStore();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInTDS"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInTDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    //#DG538
    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    //#DG538 end

    addTargetParam(new CalcParam(ClassId.BORROWER, "TDS", 999.99));
  }

}
