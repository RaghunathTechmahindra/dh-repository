package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

public class NetRate extends DealCalc
{

  public NetRate()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 2";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
 public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        addTarget ( input);
    }
    catch ( Exception e)
    {
        String msg = "Failed to get Targets for: " + this.getClass().getName();
        msg += " :[Input]: " + input.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += " @input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal tg = (Deal)target;

    try
    {
        double br = tg.getBlendedRate();

        if(br > 0)
        {
         tg.setNetInterestRate(br);
        }
        else
        {
         double nr = tg.getPostedRate() - tg.getDiscount() + tg.getPremium() - tg.getBuydownRate () ;
         tg.setNetInterestRate(validate(nr));
        }
        tg.ejbStore ();
        return;
    }
    catch(Exception e)
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
    }

      tg.setNetInterestRate(DEFAULT_FAILED_DOUBLE);
      tg.ejbStore();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();      

    addInputParam(new CalcParam(ClassId.DEAL, "blendedRate"));
    addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "premium"));
    addInputParam(new CalcParam(ClassId.DEAL, "postedRate"));
    addInputParam(new CalcParam(ClassId.DEAL , "buydownRate")); 

    addTargetParam(new CalcParam(ClassId.DEAL, "netInterestRate", T12D6));
  }

 


}
