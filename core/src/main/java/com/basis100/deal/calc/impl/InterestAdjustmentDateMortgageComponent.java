/*
 * @(#)InterestAdjustmentDateMortgageComponent.java Jun 17, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: InterestAdjustmentDateMortgageComponent
 * </p>
 * <p>
 * Description:Calculates Interest Adjustment Date for Mortgage components of
 * "component eligible" deals.
 * </p>
 * 
 * @author MCM Impl Team <br>
 * @Date 17-Jun-2008 <br>
 * @version 1.0 XS_11.2 Initial Version <br>
 * @version 1.1 XS_11.2 Review Comments Incorporated
 * Code Calc-11.CM <br>
 */
public class InterestAdjustmentDateMortgageComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * 
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public InterestAdjustmentDateMortgageComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 11.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * 
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
    throws TargetNotFoundException {
        try {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Deal deal = (Deal) input;
                    // Get all the components for the deal
                    Collection listOfComponents = entityCache.getComponents(
                            deal, srk);
                    Iterator componentsItr = listOfComponents.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is MORTGAGE COMPONENT
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                        {
                            // Retrives the componentMortgage Object for the given  componentId and copyId
                            ComponentMortgage componentMtg = (ComponentMortgage) entityCache
                            .find(srk, new ComponentMortgagePK(
                                    component.getComponentId(),
                                    component.getCopyId()));
                            addTarget(componentMtg);
                        }
                    }
                }
        } catch (Exception e) {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }


    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * 
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());

        ComponentMortgage componentMortgage = (ComponentMortgage) target ;
        Component componentObj= (Component) entityCache.find(
                srk, new ComponentPK(componentMortgage.getComponentId(),
                        componentMortgage.getCopyId()));
        Deal dealObj= (Deal) entityCache.find(
                srk, new DealPK(componentObj.getDealId(),
                        componentObj.getCopyId()));
        try
        {

            Calendar firstDate = Calendar.getInstance() ;


            // This property must be same among institutions in a instance and in DB 
            if ((PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(),
                    "com.basis100.calc.forceiadtofirstofmonth", "N")).equals("Y"))
            {
                //if Estimated Closing date not set ==> Exception
                Date estimatedClosingDate = dealObj.getEstimatedClosingDate ();

                if(estimatedClosingDate == null)
                    throw new Exception("Required Input = null -> estimatedClosingDate.getEstimatedClosingDate().");

                //Force IAD to 1st of Month if property set to Y and  Get the day to Back day from Properties
                int theBackDay = 1;
                try{
                    theBackDay = Integer.parseInt(PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(),
                            "com.basis100.calc.forceiadtofirstofmonth.daytobackdate", "1"));
                }
                catch(Exception e)
                {
                    trace("Invalid DaybackDate parameter ==> set to default = 1");
                }

                trace("The BackDay = " + theBackDay);

                firstDate.setTime(estimatedClosingDate);

                // Get Payment Frequency
                int paymentFrequency = componentMortgage.getPaymentFrequencyId();

                // Check if Special Handling required for Xceed
                if((PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(), "com.basis100.calc.forceiadtofirstofmonth.specialforxd", "N")).equals("N") ||
                        paymentFrequency == Mc.PAY_FREQ_MONTHLY)
                {
                    // Set the result Day to the first day of month
                    firstDate.set(Calendar.DAY_OF_MONTH, 1);
                    // Get day field from Est closing date
                    int estDate = estimatedClosingDate.getDate();
                    if(estDate > theBackDay)
                    {
                        // Set IAD to 1st day of next Month of Est. Closing Date
                        firstDate.add(Calendar.MONTH, 1);
                    }
                }
                else
                {
                    // Special handle for Xceed to set
                    trace("Perform special handling for Xceed.");
                    // Get current Day Of Week
                    int daysFromFri = Calendar.FRIDAY - firstDate.get(Calendar.DAY_OF_WEEK);
                    trace("# of Days from Closing Friday = " + daysFromFri);
                    firstDate.add (Calendar.DAY_OF_MONTH, daysFromFri);
                }

                trace("The result IAD = " + firstDate.toString());

                componentMortgage.setInterestAdjustmentDate ( firstDate.getTime ());
            }
            else
            {
                //No First Payment Date set
                if ( componentMortgage.getFirstPaymentDate () == null )
                { 
                    Date estimatedClosingDate = dealObj.getEstimatedClosingDate ();

                    if(estimatedClosingDate == null)
                        throw new Exception("Required Input = null -> deal.getEstimatedClosingDate ().");

                    componentMortgage.setInterestAdjustmentDate ( estimatedClosingDate  );
                }
                // First Payment Date set
                else
                {
                    int period=0;
                    int paymentFrequency = componentMortgage.getPaymentFrequencyId ();
                    firstDate.setTime ( componentMortgage.getFirstPaymentDate () );

                    if ( paymentFrequency == Mc.PAY_FREQ_MONTHLY  )
                    {
                        firstDate.add ( Calendar.MONTH , -1 );  // back one month
                    }
                    else 
                    {
                        if ( paymentFrequency == Mc.PAY_FREQ_SEMIMONTHLY )
                            period = 15 ;
                        else if ( paymentFrequency == Mc.PAY_FREQ_BIWEEKLY || paymentFrequency == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY )
                            period = 14 ;
                        else if ( paymentFrequency == Mc.PAY_FREQ_WEEKLY || paymentFrequency == Mc.PAY_FREQ_ACCELERATED_WEEKLY )
                            period = 7 ;

                        firstDate.add (Calendar.DAY_OF_MONTH , - period );
                    }
                    componentMortgage.setInterestAdjustmentDate ( firstDate.getTime ());
                }
            }
            componentMortgage.ejbStore ();
            return;
        }
        catch ( Exception e )
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

        componentMortgage.setInterestAdjustmentDate(DEFAULT_FAILED_DATE);
        componentMortgage.ejbStore ();
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * 
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Versionn
     * 
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        //Adding input parameters
        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "paymentFrequencyId"));

        // This property must be same among institutions in a instance and in DB 
        if ((PropertiesCache.getInstance().getProperty(-1,
                "com.basis100.calc.forceiadtofirstofmonth", "N")).equals("N"))
        {
            // Not include firstPaymentDate to avoid dead-lock if ForceIADtoFirstToMonth = Y
            addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "firstPaymentDate"));
        }

        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "interestAdjustmentDate"));

    }

}
