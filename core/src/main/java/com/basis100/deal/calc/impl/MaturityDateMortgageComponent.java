/*
 * @(#)MaturityDateMortgageComponent.java Jun 17, 2008
 *
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.calc.*;
import com.basis100.resources.*;
import MosSystem.Mc;

/**
 * <p>
 * Title: MaturityDateMortgageComponent
 * </p>
 * <p>
 * Description:Calculates MaturityDate for Mortgage components of
 * "component eligible" deals.
 * </p>
 *
 * @author MCM Impl Team <br>
 * @Date 17-Jun-2008 <br>
 * @version 1.0 XS_11.2 Initial Version <br>
 *          Code Calc-14.CM <br>
 */
public class MaturityDateMortgageComponent extends DealCalc
{
    final long MillSecsInWeek = 604800000 ;  //7 * 24 * 60 * 60 * 10000
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     *
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public MaturityDateMortgageComponent()throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 14.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     *
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget ( input );
        }
        catch ( Exception e )
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     *
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());

        ComponentMortgage componentMortgage = (ComponentMortgage) target ;
        Component componentObj= (Component) entityCache.find(srk, new ComponentPK(componentMortgage.getComponentId(),
                componentMortgage.getCopyId()));
        Deal dealObj= (Deal) entityCache.find(
                srk, new DealPK(componentObj.getDealId(),
                        componentObj.getCopyId()));

            try
            {

                Calendar maturityDate = Calendar.getInstance( Locale.getDefault () ) ;
                Date IAD = componentMortgage.getInterestAdjustmentDate ( ) ;

                int paymentFrequencyId = componentMortgage.getPaymentFrequencyId () ;
                trace( "PaymentFrequencyId = " + paymentFrequencyId );
                int term = componentMortgage.getActualPaymentTerm () ;
                trace( "ActualPaymentTerm = " + term );
                if ( IAD != null )
                {
                    maturityDate.setTime (  IAD );
                    int dayOfWeek = maturityDate.get (  Calendar.DAY_OF_WEEK  ); //IAD Date WeekDay

                    Date IADdt = maturityDate.getTime(); //IAD date variable

                    // If Force Maturity Date is Monthly then skip the following calc and set MaturityDate to IAD + term months
                    if ((PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(), "com.basis100.calc.forcematuritydatetomonthlyequ", "N")).equals("Y"))
                    {
                        trace( "MaturityDate (Force to Monthly Equvalent = " + maturityDate.getTime () ) ;
                        maturityDate.add (Calendar.MONTH , term );
                        componentMortgage.setMaturityDate ( maturityDate.getTime () );
                        componentMortgage.ejbStore ();
                        return;
                    }

                    //Maturity date calculation for NBC  - BEGIN
                    double dNoOfPayments = 0.0d;
                    double dTotalPaymentTerms = 0.0d;
                    int intDays = 1;
                    if ((PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(), "com.basis100.calc.maturitydatemethod", "N")).equals("NBC"))
                    {
                        if ( oneOf( paymentFrequencyId,
                                Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                        {
                            //NBC202556 - round weekly and biweekly paymentperiods
                            dNoOfPayments = Math.round(365/14.0);
                            dTotalPaymentTerms = (term / 12.0d) * dNoOfPayments;
                            intDays = 2;
                        }
                        else if ( oneOf( paymentFrequencyId,
                                Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY ) )
                        {
                            dNoOfPayments = Math.round(365/7.0);
                            dTotalPaymentTerms = (term / 12.0d) * dNoOfPayments;
                        }
                        dTotalPaymentTerms = Math.round(dTotalPaymentTerms);
                        maturityDate.add(Calendar.DATE, ((int)dTotalPaymentTerms) * 7 * intDays);
                        if ( oneOf( paymentFrequencyId,
                                Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY,
                                Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY) )
                        {
                            componentMortgage.setMaturityDate ( maturityDate.getTime () );
                            componentMortgage.ejbStore ();
                            return;
                        }
                    }
                    // Maturity date calculation for NBC  - END

                    maturityDate.add (Calendar.MONTH , term );
                    componentMortgage.setMaturityDate ( maturityDate.getTime () );
                    int dayOfWeek2 = maturityDate.get ( Calendar.DAY_OF_WEEK    ) ;//IAD + Term date day

                    // (Accelerated) Weekly, BiWeekly, Make sure same weekday.
                    if ( oneOf( paymentFrequencyId,
                            Mc.PAY_FREQ_WEEKLY , Mc.PAY_FREQ_ACCELERATED_WEEKLY ,
                            Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                    {
                        int dayDifferent = dayOfWeek - dayOfWeek2; // difference between the weekdays
                        if ( dayOfWeek != dayOfWeek2 ) // Making the day same for both IAD & MaturityDate
                        {
                            maturityDate.add(Calendar.DATE, dayDifferent);
                        }
                        if(dayDifferent>0)
                        {
                            maturityDate.add(Calendar.DATE , - 7 );
                        }

                    }
                    trace( "MaturityDate = " + maturityDate.getTime () ) ;
                    // If Bi-Weekly, make sure Weeks between IAD and CalculatedMaturidyDate is even
                    if ( oneOf( paymentFrequencyId,
                            Mc.PAY_FREQ_BIWEEKLY , Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ) )
                    {
                        double weekDiff = (((maturityDate.getTime().getTime() - IADdt.getTime())) * (1.0d))/ MillSecsInWeek;
                        if(Math.round(weekDiff) % 2 != 0)
                        {
                            maturityDate.add( Calendar.DATE , - 7 );
                        }

                    }
                    trace( "MaturityDate = " + maturityDate.getTime () ) ;
                    componentMortgage.setMaturityDate ( maturityDate.getTime () );
                    componentMortgage.ejbStore ();
                }
                return;
            }
            catch ( Exception e )
            {
                StringBuffer msg = new StringBuffer(100);
                msg.append("Begin Failure in Calculation");
                msg.append(this.getClass().getName());
                msg.append(":[Target]:");
                msg.append(target.getClass().getName());
                msg.append("  Reason: ");
                msg.append(e.getMessage());
                msg.append("@Target: ");
                msg.append(this.getDoCalcErrorDetail(target));
                logger.debug(msg.toString());
            }

            componentMortgage.setMaturityDate(DEFAULT_FAILED_DATE);
            componentMortgage.ejbStore();
        }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     *
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Versionn
     *
     */
    private void initParams()throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        //Adding input parameters
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "InterestAdjustmentDate"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE , "actualPaymentTerm"));

        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "maturityDate"));
    }


}
