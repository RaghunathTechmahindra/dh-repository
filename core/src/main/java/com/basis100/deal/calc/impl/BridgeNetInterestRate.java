package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name:  Bridge Net Interest Rate
//Code:  Calc 94

public class BridgeNetInterestRate extends DealCalc {

  public BridgeNetInterestRate() throws Exception
   {
    super();
    initParams();
    this.calcNumber = "Calc 94";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget(input);
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());
     Bridge bridge = ( Bridge ) target ;

     try
     {
        double netRate = bridge.getPostedInterestRate () -
                bridge.getDiscount () + bridge.getPremium ();
        trace( "postedInterestRate = " + bridge.getPostedInterestRate () );
        trace( "discount = " + bridge.getDiscount () );
        trace( "Premium = " + bridge.getPremium () );
        
        bridge.setNetInterestRate ( validate( netRate) );
        bridge.ejbStore ();
        return; 
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     bridge.setNetInterestRate ( DEFAULT_FAILED_DOUBLE );
     bridge.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.BRIDGE , "postedInterestRate"));
    addInputParam(new CalcParam(ClassId.BRIDGE , "discount"));
    addInputParam(new CalcParam(ClassId.BRIDGE , "premium"));

    addTargetParam(new CalcParam(ClassId.BRIDGE , "netInterestRate", T12D6));
  }

}