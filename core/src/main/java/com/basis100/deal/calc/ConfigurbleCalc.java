package com.basis100.deal.calc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.AppraisalOrder;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.BorrowerIdentification;
import com.basis100.deal.entity.Bridge;
import com.basis100.deal.entity.CreditReference;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.InsureOnlyApplicant;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.LifeDisabilityPremiums;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.picklist.PicklistData;

public abstract class ConfigurbleCalc extends DealCalc {

    private final static Log _log = LogFactory
    .getLog(ConfigurbleCalc.class);

    public Collection getTargets() {
        return this.targets;
    }

    final public void getTarget(Object input, CalcMonitor dcm)
    throws TargetNotFoundException {

        Deal deal = getTargetDeal(input, dcm);
        if(deal == null){
            // target not found
            String msg = "target not found @" + getClass().getName();
            msg += ": input :" + input.getClass().getName();
            msg += "/" + getDoCalcErrorDetail(input);
            TargetNotFoundException e = new TargetNotFoundException(msg);
            _log.error(msg, e);
            throw e;
        }
        addTarget(deal); 
    }

    protected Deal getTargetDeal(Object input, CalcMonitor dcm)
    throws TargetNotFoundException {
        
        _log.debug("getTargetDeal : input = " + input.getClass().getName());
        try {
            if (input instanceof Deal) {
                return (Deal)input;
            }
            if (input instanceof Property) { 
                return findDealByProperty((Property) input);
            }
            if (input instanceof Borrower) {
                return findDealByBorrower((Borrower) input);
            }
            if (input instanceof Asset) {
                Asset a = (Asset) input;
                return findDealByBorrower(
                        findBorrower(a.getBorrowerId(), a.getCopyId()));
            }
            if (input instanceof DownPaymentSource) {
                DownPaymentSource dp = (DownPaymentSource) input;
                return findDeal(dp.getDealId(), dp.getCopyId());
            }
            if (input instanceof Income) {
                Income i = (Income) input;
                return findDealByBorrower(
                        findBorrower(i.getBorrowerId(), i.getCopyId()));
            }
            if (input instanceof Liability) {
                Liability l = (Liability) input;
                return findDealByBorrower(
                        findBorrower(l.getBorrowerId(), l.getCopyId()));
            }
            if (input instanceof PropertyExpense) {
                PropertyExpense pe = (PropertyExpense) input;
                return findDealByProperty(
                        findProperty(pe.getPropertyId(), pe.getCopyId()));
            }
            if (input instanceof EscrowPayment) {
                EscrowPayment e = (EscrowPayment) input;
                return findDeal(e.getDealId(), e.getCopyId());
            }
            if (input instanceof DealFee) {
                DealFee df = (DealFee) input;
                return findDeal(df.getDealId(), df.getCopyId());
            }
            if (input instanceof EmploymentHistory) {
                EmploymentHistory e = (EmploymentHistory) input;
                return findDealByBorrower(
                        findBorrower(e.getBorrowerId(), e.getCopyId()));
            }
            if (input instanceof DocumentTracking) {
                DocumentTracking dt = (DocumentTracking) input;
                return findDeal(dt.getDealId(), dt.getCopyId());
            }
            if (input instanceof Bridge) {
                Bridge b = (Bridge) input;
                return findDeal(b.getDealId(), b.getCopyId());
            }
            if (input instanceof BorrowerAddress) {
                BorrowerAddress ba = (BorrowerAddress) input;
                return findDealByBorrower(
                        findBorrower(ba.getBorrowerId(), ba.getCopyId()));
            }
            if (input instanceof CreditReference) {
                CreditReference cr = (CreditReference) input;
                return findDealByBorrower(
                        findBorrower(cr.getBorrowerId(), cr.getCopyId()));
            }
            if (input instanceof AppraisalOrder) {
                AppraisalOrder ao = (AppraisalOrder) input;
                return findDealByProperty(
                        findProperty(ao.getPropertyId(), ao.getCopyId()));
            }
            if (input instanceof LifeDisabilityPremiums) {
                LifeDisabilityPremiums l = (LifeDisabilityPremiums) input;
                return findDealByBorrower(
                        findBorrower(l.getBorrowerId(), l.getCopyId()));
            }
            if (input instanceof InsureOnlyApplicant) {
                InsureOnlyApplicant l = (InsureOnlyApplicant) input;
                return findDeal(l.getDealId(), l.getCopyId());
            }
            if (input instanceof BorrowerIdentification) {
                BorrowerIdentification bi = (BorrowerIdentification) input;
                return findDealByBorrower(
                        findBorrower(bi.getBorrowerId(), bi.getCopyId()));
            }
            // not found 
            return null;
        } catch (Exception e) {
            String msg = "fail to get Targets @" + getClass().getName();
            msg += ": input :" + input.getClass().getName();
            msg += "/" + getDoCalcErrorDetail(input);
            msg += ": error :" + e.getMessage();
            _log.error(msg, e);
            throw new TargetNotFoundException(msg);
        }
    }

    // note: entityCache's instance is always created when dcm.calc() 
    // is called. this entityCache caches only for 1 transaction.
    private Deal findDeal(int dealId, int copyId) throws Exception{
        return (Deal) entityCache.find(srk, new DealPK(dealId,copyId));
    }

    private Deal findDealByBorrower(Borrower b) throws Exception{
        return findDeal(b.getDealId(), b.getCopyId());
    }

    private Deal findDealByProperty(Property p) throws Exception{
        return findDeal(p.getDealId(), p.getCopyId());
    }

    private Borrower findBorrower(int borrowerId, int copyId) throws Exception{
        return (Borrower) entityCache.find(srk, new BorrowerPK(borrowerId, copyId));
    }

    private Property findProperty(int propertyId, int copyId) throws Exception{
        return (Property) entityCache.find(srk, new PropertyPK(propertyId, copyId));
    }


    public boolean hasInput(CalcParam input) {
        return this.inputParam.contains(input);
    }

    protected void initParams() throws ParamTypeException {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        _log.debug("initParams : calcTypeId = " + getCalcTypeid());
        
        List table = getCalcTargetConfig(getCalcTypeid());
        for (Iterator i = table.iterator(); i.hasNext();) {
            Map row = (Map) i.next();
            int classId = TypeConverter.intTypeFrom((String) row
                    .get(COL_CLASSID));
            String entityAttribute = (String) row
            .get(COL_ENTITYATTRIBUTE);

            _log.debug("initParams : target classId=" + classId 
                    + " entityAttribute=" + entityAttribute);

            //addInputParam(new CalcParam(classId, entityAttribute));
            addInputParam(new CalcParam(classId, entityAttribute));
        }
    }

    /**
     * calcTypeId has to be specified by each sub classes
     * @return calcTypeId
     */
    abstract protected int getCalcTypeid();

    
    protected static final String COL_CALCTYPEID = "CALCTYPEID";
    protected static final String COL_CLASSID = "CLASSID";
    protected static final String COL_ENTITYATTRIBUTE= "ENTITYATTRIBUTE";
    
    /**
     * get CalcTargetConfig table's data.
     * @param calcTypeId
     * @return List includes Map that has column.
     */
    protected static List getCalcTargetConfig (int calcTypeId){

        List calTypeIds = getPickListColumnList("CALCTARGETCONFIG", COL_CALCTYPEID);
        List clsIds = getPickListColumnList("CALCTARGETCONFIG", COL_CLASSID);
        List entAttrs = getPickListColumnList("CALCTARGETCONFIG", COL_ENTITYATTRIBUTE);
        
        List result = new Vector();
        for(int i =0; i<calTypeIds.size(); i++){
            int cTypeId = TypeConverter.intTypeFrom((String)calTypeIds.get(i));
            if(calcTypeId != cTypeId) continue;
            Map row = new HashMap();
            row.put(COL_CALCTYPEID, calTypeIds.get(i));
            row.put(COL_CLASSID, clsIds.get(i));
            row.put(COL_ENTITYATTRIBUTE, entAttrs.get(i));
            result.add(row);
        }
        _log.debug("getCalcTargetConfig list size = " + result.size());
        return result;
    }
    
    private static List getPickListColumnList(String table, String column){
        String cvs = PicklistData.getColumnValuesCDV(table, column);
        cvs = cvs.replace('\'', ' ');// change 'numberOfUnits', ->  numberOfUnits, 
        StringTokenizer st = new StringTokenizer(cvs, ",");
        List list = new ArrayList();
        while(st.hasMoreTokens()){
            list.add(st.nextToken().trim());
        }
        return list;
    }
}
