package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name: Teaser Principal and Interest Payments
//Code: Calc 85
//07/12/2000

public class TeaserPandI extends DealCalc {

  public TeaserPandI() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 85";
  }

  public Collection getTargets()
  {    return this.targets;  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof Deal )
          addTarget ( input );
    }
    catch ( Exception e )
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;

    try
    {
      if ( deal.getTeaserTerm () > 0 )
      {
		  // SEAN Jan 11, 2006: Migration to Log4j using the trace method in DealCalc
          trace("Calc 85----if---deal.getTeaserTerm () > 0 " );
          int amortizationMonths = deal.getAmortizationTerm();
          int pfid = deal.getPaymentFrequencyId();
          double interestFactor ;
          int numPayments ;
          trace( "Calc 85-------deal.AmortizationTerm = " + amortizationMonths );
          trace( "Calc 85-------PaymentFrequencyId = " + pfid );
          trace( "Calc 85-------TeaserNetInterestRate= " + deal.getTeaserNetInterestRate () );

          // Get the InterestCompoundDesc from deal.MtgProg.interestcompoundId -- BILLY 07Dec2001
          // added inst id for ML
          String InterestCompoundDesc 
              = PicklistData.getMatchingColumnValue(-1,
                                                    "interestcompound",
                                                    deal.getMtgProd().getInterestCompoundingId(),
                                                    "interestcompounddescription");

          if ( pfid == Mc.PAY_FREQ_SEMIMONTHLY )
          {
trace( "Calc 85------- pfid == Mc.PAY_FREQ_SEMIMONTHLY");
            numPayments = (amortizationMonths / 12 ) * 24 ;
            interestFactor =  DealCalcUtil.interestFactor(deal.getTeaserNetInterestRate () ,24, InterestCompoundDesc);
          }
          else
          { // Bi-weekly, Weekly, (Accelerated ) are based on montly payment.
trace( "Calc 85------- Bi-weekly, Weekly, (Accelerated ) are based on montly payment");
            numPayments = (amortizationMonths / 12 ) * 12 ;
            interestFactor =  DealCalcUtil.interestFactor(deal.getTeaserNetInterestRate () , 12, InterestCompoundDesc);
          }
          trace( "InterestCompoundDesc = " + InterestCompoundDesc + ":: InterestFacotr = " +  interestFactor ) ;

          double monthlyPandI =
                      1- 1 / Math.pow((interestFactor + 1),numPayments);

          if(monthlyPandI > 0)
          {
trace( "Calc 85------- monthlyPandI > 0");
            monthlyPandI = interestFactor / monthlyPandI;
            monthlyPandI = deal.getTotalLoanAmount() * monthlyPandI;
          }
          else
            monthlyPandI = 0;
          trace( "Calc 85-------monthlyPandI ( not finnal ) = " + monthlyPandI );

          if(pfid == Mc.PAY_FREQ_SEMIMONTHLY)
          {
            ; // done already!
          }
          else if(pfid == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
          {
            monthlyPandI /= 2;
          }
          else if(pfid == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
          {
            monthlyPandI /= 4;
          }
          else if(pfid == Mc.PAY_FREQ_BIWEEKLY)
          {
            monthlyPandI = (monthlyPandI * 12) / 26;
          }
          else if(pfid == Mc.PAY_FREQ_WEEKLY)
          {
            monthlyPandI = (monthlyPandI * 12) / 52;
          }
          else //monthly default....
          {
            ;  // nothing to do!
          }
          trace( "Calc 85-------teaserPandI = " + monthlyPandI );
          monthlyPandI = Math.round ( monthlyPandI * 100 ) /100.00;
          deal.setTeaserPIAmount ( validate(monthlyPandI));
          deal.ejbStore();
trace( "Calc 85------- deal.setTeaserPIAmount()= " + validate(monthlyPandI));
      }
      else
      {
// SEAN Jan 11, 2006: Migration to Log4j changed to use the trace method in DealCalc
trace("Calc 85---else----deal.setTeaserPIAmount ( 0 )");
          deal.setTeaserPIAmount ( 0 );
          deal.ejbStore();
      }
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

    deal.setTeaserPIAmount (DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "teaserTerm"));

    addInputParam(new CalcParam(ClassId.DEAL, "teaserNetInterestRate"));
    addInputParam(new CalcParam(ClassId.DEAL , "mtgProdId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "teaserPIAmount", T13D2));
  }

}
