/**
 * Title:        IADNumberOfDays (Calc-103)
 * Description:  Calculation for the number of days in Interest Adjustment Period
 * Copyright:    Copyright (c) 2001
 * Company:      Basis100
 * Date :        26 Nov 2001
 * @author Billy Lam
 * @version
 */

package com.basis100.deal.calc.impl;

import java.util.*;
import java.text.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

import MosSystem.Mc;

/**
 * <p>
 * Title: IADNumberOfDays
 * </p>
 * <p>
 * Description:Calculates IADNumberOfDays
 * </p>
 * @version 1.0 Initial Version <br>
 * @version 1.1 XS_11.17 Initial Version <br> - updated
 *          initParams(),getTarget(),doCalc()
 * @author MCM Impl Team <br>
 * @Date 04-Jul-2008 <br>
 *       Code Calc-103 <br>
 */
public class IADNumberOfDays extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 Initial Version
     */
    public IADNumberOfDays() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 103";
    }

    public Collection getTargets()
    {
        return this.targets;
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 Initial Version
     * @version 1.1 XS_11.17 | updated Exception
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget( input );
        }
        catch(Exception e)
        {
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }
    
    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     * @version 1.1 XS_11.17 | Updated 
     *        -  Calc logic for Deal level product is a line of credit
     *        - Exception
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        Deal deal = (Deal)target;
        MtgProd mtgProd=deal.getMtgProd();

        try
        {
            // Get Estimated Closing Date  and InterimInterestAdjustmentDate
            //  if null ==> set default Days to 0
            Date theEstClosingDt = deal.getEstimatedClosingDate();
            Date theIAD = deal.getInterimInterestAdjustmentDate();
            int IADNumOfDays = 0;
            
            int UnderwriteAsTypeId = mtgProd.getUnderwriteAsTypeId();   
            
            //If the deal level product is a line of credit then deal IADNumberOfDays is set 0
            if(theEstClosingDt == null || theIAD == null || UnderwriteAsTypeId == Mc.COMPONENT_TYPE_LOC )
            {
                trace( "The Est Closing Date or IAD = null ==> default the IADNumberOfDays =0");
            }
            //If the deal level product is NOT a line of credit then the deal IADNumberOfDays is calculated as follows
            else
            {
                // Calculate the # of days = from Estimated Closing Date - InterimInterestAdjustmentDate
                // (not including InterimInterestAdjustmentDate)
                SimpleDateFormat tsFmt = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss:SSS");
                // Reset the Hour,Minute and Second parameters -- Compare only the date
                Calendar theEstDateInCal = Calendar.getInstance();
                theEstDateInCal.clear();
                theEstDateInCal.set(theEstClosingDt.getYear()+1900, theEstClosingDt.getMonth(), theEstClosingDt.getDate());
                trace("The Est Closing Date (Orig) : " + tsFmt.format(theEstClosingDt) + "(" + theEstClosingDt.getTime() + ")" +
                        " ==> Converted To : " + tsFmt.format(theEstDateInCal.getTime()) + "(" + theEstDateInCal.getTime().getTime() + ")");
                Calendar theIADInCal = Calendar.getInstance();
                theIADInCal.clear();
                //--> Bug fixed to ensure it included the Est_closing date
                //--> By Billy 02April2003
                theIADInCal.set(theIAD.getYear()+1900, theIAD.getMonth(), theIAD.getDate(), 12, 0);
                trace("The IAD Date (Orig) : " + tsFmt.format(theIAD) + "(" + theIAD.getTime() + ")" +
                        " ==> Converted To : " + tsFmt.format(theIADInCal.getTime()) + "(" + theIADInCal.getTime().getTime() + ")");

                // Get the difference in millis sec.
                IADNumOfDays = (new Long((theIADInCal.getTime().getTime() - theEstDateInCal.getTime().getTime()) / (24*3600*1000))).intValue();

                trace("The result IADNumOfDays after compute = " + IADNumOfDays);

                if(IADNumOfDays < 0 )
                    IADNumOfDays = 0;
            }

            trace( "Result (IADNumOfDays) = " + IADNumOfDays );
            deal.setIADNumberOfDays(IADNumOfDays);

            deal.ejbStore();
            return;
        }
        catch(Exception e)
        {
            // XS_11.17 |Updated to StringBuffer from String
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());

        }

        deal.setIADNumberOfDays(0);
        deal.ejbStore();
    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 Initial Version
     * @version 1.1 - Added mtgProdId as input parameter.
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));
        addInputParam(new CalcParam(ClassId.DEAL, "interimInterestAdjustmentDate"));
        
        /*****************MCM Impl Team STARTS XS_11.17*******************/
        addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
        /*****************MCM Impl Team ENDS XS_11.17*******************/
        
        addTargetParam(new CalcParam(ClassId.DEAL, "IADNumberOfDays"));

    }

}