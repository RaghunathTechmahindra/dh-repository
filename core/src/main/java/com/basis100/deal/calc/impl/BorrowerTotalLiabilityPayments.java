package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

public class BorrowerTotalLiabilityPayments extends DealCalc
{


  public BorrowerTotalLiabilityPayments()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 55";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {
          Liability l = (Liability)input;
          Borrower bt = (Borrower) entityCache.find ( srk, 
                            new BorrowerPK(l.getBorrowerId (), 
                                           l.getCopyId ()));
          addTarget( bt );
     }
     catch(Exception e)
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());

    Borrower borrower = (Borrower)target;

    try
    {
        double totalLiabilityPayments = 0;

        // The copy from the Cache may not up-to-date : it may be modified by Calc 72
        //Collection libs = this.entityCache.getLiabilities(borrower);

        // Then we better get the Entities from table -- Temp fix by BILLY
        Collection libs = borrower.getLiabilities();

        // if ( libs != null )  libs should not be empty
        Iterator it = libs.iterator();
        Liability liability = null;

        while(it.hasNext())
        {
            liability = (Liability)it.next();
            // Change request from Joe - add only if % out GDS < 2
            if ( (liability.getLiabilityTypeId () != 14) /* Closing Cost */ && (liability.getPercentOutGDS() < 2))
              totalLiabilityPayments += liability.getLiabilityMonthlyPayment () ;
            trace( "totalLiabilityPaymentAmount = " + totalLiabilityPayments );
        } // end of it.hasnext () --------

        totalLiabilityPayments = Math.round ( totalLiabilityPayments * 100 ) / 100.00 ;
        borrower.setTotalLiabilityPayments ( validate(totalLiabilityPayments) );
        borrower.ejbStore ();
        return;
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    borrower.setTotalLiabilityPayments (DEFAULT_FAILED_DOUBLE);
    borrower.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.LIABILITY, "percentOutGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addTargetParam(new CalcParam(ClassId.BORROWER, "totalLiabilityPayments",T13D2));
  }



}
