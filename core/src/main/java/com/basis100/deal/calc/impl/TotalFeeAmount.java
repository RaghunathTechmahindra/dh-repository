package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name: Total Fee Amount
//Code: Calc-73
//07/04/2000

public class TotalFeeAmount extends DealCalc {

  public TotalFeeAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 73";
  }


  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {
        if ( input instanceof DealFee )
        {
            DealFee  dealFee = (DealFee) input;
            Deal deal = (Deal) entityCache.find ( srk ,
                    new DealPK( dealFee.getDealId (), dealFee.getCopyId ()) );
            addTarget( deal ) ;
        }
     }
     catch ( Exception e )
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = (Deal) target;
     try
     {

        double totalFeeAmount = 0.0;

        Collection dealFees = deal.getDealFees () ;
        Iterator itFee = dealFees.iterator ();

        while ( itFee.hasNext () )
        {
          DealFee dealFee = (DealFee) itFee.next ();
          Fee fee = (Fee) entityCache.find ( srk,
                  new FeePK( dealFee.getFeeId () ) ) ;
//
//          int feeTypeId = fee.getFeeTypeId();
//          String feeTypeDesc = PicklistData.getDescription("FeeType", feeTypeId);
//          if( ! feeTypeDesc.trim().equals("Appraisal Fee Payable") &&
//              ! feeTypeDesc.trim().equals("CMHC Fee Payable") &&
//              ! feeTypeDesc.trim().equals("Agent Referral Fee") &&
//              ! feeTypeDesc.trim().equals("Origination Fee") &&
//              ! feeTypeDesc.trim().equals("CMHC Premium") &&
//              ! feeTypeDesc.trim().equals("CMHC Premium Payable") &&
//              ! feeTypeDesc.trim().equals("CMHC PST On Premium") &&
//              ! feeTypeDesc.trim().equals("CMHC PST On Premium Payable") &&
//              ! feeTypeDesc.trim().equals("GE Capital Fee Payable") &&
//              ! feeTypeDesc.trim().equals("GE Capital Premium") &&
//              ! feeTypeDesc.trim().equals("GE Capital Premium Payable") &&
//              ! feeTypeDesc.trim().equals("GE Capital PST On Premium") &&
//              ! feeTypeDesc.trim().equals("GE Capital PST On Premium Payable") &&
//              ! feeTypeDesc.trim().equals("Property Tax Holdback") &&
//              ! feeTypeDesc.trim().equals("Cancellation Fee") )
          //Added by Clement for UG_AIG
          if(fee.getIncludeInTotalFeeAmount().equals("Y"))
          {
             totalFeeAmount += dealFee.getFeeAmount ();
          }

        }

        totalFeeAmount = Math.round ( totalFeeAmount * 100 ) /100.00;
        deal.setTotalFeeAmount (validate(totalFeeAmount));
        deal.ejbStore();
        return;
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    deal.setTotalFeeAmount(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.DEALFEE , "feeAmount"));
    addInputParam(new CalcParam(ClassId.DEALFEE , "feeId"));
    addInputParam(new CalcParam(ClassId.DEALFEE , "dealId")) ; 
    //addInputParam(new CalcParam(ClassId.FEE  , "feeGenerationTypeId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "totalFeeAmount", T15D2 ));
  }

}
