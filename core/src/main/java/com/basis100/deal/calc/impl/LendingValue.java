package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.util.*;
import com.basis100.deal.pk.DealPK ;
import MosSystem.Mc;

public class LendingValue extends DealCalc
{


  public LendingValue()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 74";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        if ( input instanceof Deal )
        {
          Deal deal = ( Deal ) input ;
          Collection properties = this.entityCache.getProperties ( deal ) ;
          Iterator itPR = properties.iterator () ;
          while ( itPR.hasNext () )
          {
              addTarget( itPR.next () );
          }
        }
        else if ( input instanceof Property )
            addTarget(input);
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Property pt = (Property)target;
    try
    {
      double pp = pt.getPurchasePrice();
      double aav = pt.getActualAppraisalValue();
      double eav = pt.getEstimatedAppraisalValue();
      double lendingValue = 0.0;

       Deal deal = (Deal)this.entityCache
                         .find(srk, new DealPK(pt.getDealId(),pt.getCopyId()));
       logger.calcTrace ( "specialFeatureId = " + deal.getSpecialFeatureId () );

       if  ( deal.getSpecialFeatureId () == Mc.SPECIAL_FEATURE_PRE_APPROVAL /* PreApproval */  )
       {
          pp = deal.getPAPurchasePrice () ;
       }
      trace( "PurchasePrice = " + pp );
      trace( "ActuralAppraisalValue = " + aav ) ;
      trace( "estimatedAppraisalValue = " + eav ) ;

      if  (  ( pp > 0) &&  ( aav > pp ) )
          lendingValue = pp;
      if  (  ( aav > 0 ) && ( pp > 0 ) && ( aav <= pp ) )
          lendingValue = aav;
      if  (  ( aav == 0 ) && ( pp > 0 ) &&  ( eav > 0) && ( pp <= eav ) )
          lendingValue = pp;
      if  (  ( aav == 0 ) && ( pp > 0 ) && ( eav > 0 ) && ( pp > eav ) )
          lendingValue = eav ;
      if (  ( aav > 0 ) && ( pp == 0 ) )
          lendingValue = aav ;
      if ( ( aav == 0) && ( pp == 0 ) )
          lendingValue = eav ;
      if ( ( aav == 0 ) && ( pp > 0) && ( eav == 0 ) )
          lendingValue = pp ;

      pt.setLendingValue(validate(lendingValue));
      pt.ejbStore();
      return;

    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail ( target );
      logger.debug (msg );
    }

     pt.setLendingValue(DEFAULT_FAILED_DOUBLE);
     pt.ejbStore();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTY, "purchasePrice"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "actualAppraisalValue"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "estimatedAppraisalValue"));

    addInputParam(new CalcParam(ClassId.DEAL , "specialFeatureId"));

    addTargetParam(new CalcParam(ClassId.PROPERTY, "lendingValue", T15D2));

  }
}
