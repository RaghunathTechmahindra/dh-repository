package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;

/**
 * <p>
 * Title: PandIMonthlyLoanComponent
 * </p>
 * <p>
 * Description:Calculates Principal and Interest portion of payment , using the
 * monthly payment frequency values for Loan component
 * </p>
 * Calculation Number- Calc-105.CM
 * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
 * @version 1.1 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
 */
public class PandIMonthlyLoanComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     */
    public PandIMonthlyLoanComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 105.CLoan";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @version 1.1 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOAN)
            {
                ComponentLoan componentLoan = (ComponentLoan) input;
                addTarget(componentLoan);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    Component component = (Component) input;
                    try {
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOAN)
                        {
                            // find componentLoan for the given componentId and
                            // copyId
                            ComponentLoan componentLoan = (ComponentLoan) entityCache
                                    .find(srk, new ComponentLoanPK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            addTarget(componentLoan);
                        }
                    } catch (Exception e) {
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        if (target == null) return;
        trace(this.getClass().getName());
        ComponentLoan targetComponentLoan = (ComponentLoan) target;
        try
        {
            // Retrive the component for the given component Loan
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLoan.getComponentId(),
                            targetComponentLoan.getCopyId()));
            // Retrive the MtgProd for the given MtgProdId
            MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                    new MtgProdPK(component.getMtgProdId()));
            // Initializing variables
            int actualPaymentTerm = targetComponentLoan.getActualPaymentTerm();

            double monthlyPandI = 0.0d;

            int repaymentTypeId = component.getRepaymentTypeId();

            // Get the InterestCompoundDesc from MtgProg.interestcompoundId
            String InterestCompoundDesc = PicklistData.getMatchingColumnValue(
                    -1, "interestcompound", mtgProd.getInterestCompoundingId(),
                    "interestcompounddescription");

            // Interest Only Variable or At Maturity
            if (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
                    || repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED)
            {
                // Calculate the Interest Factor using DealCalUtil
                double interestFactor = DealCalcUtil.interestFactor(
                        targetComponentLoan.getNetInterestRate(), 12d,
                        InterestCompoundDesc);
                monthlyPandI = targetComponentLoan.getLoanAmount()
                        * interestFactor;
            }
            else
            {
                monthlyPandI = DealCalcUtil.calculatePandI(component
                        .getInstitutionProfileId(), actualPaymentTerm,
                        Mc.PAY_FREQ_MONTHLY, InterestCompoundDesc,
                        targetComponentLoan.getLoanAmount(),
                        targetComponentLoan.getNetInterestRate());
            }
            monthlyPandI = Math.round(monthlyPandI * 100d) / 100.00d;
            // if the pAndIPaymentAmountMonthly is >99999999999.99 then Validate
            // will round
            // of to the max given value in the TargetParam
            targetComponentLoan
                    .setPAndIPaymentAmountMonthly(validate(monthlyPandI));
            targetComponentLoan.ejbStore();
            return;
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
        targetComponentLoan.ejbStore();
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "repaymentTypeId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "loanAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "actualPaymentTerm"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "netInterestRate"));
        addTargetParam(new CalcParam(ClassId.COMPONENTLOAN,
                "pAndIPaymentAmountMonthly", T13D2));

    }

}
