package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * Title: TotalPaymentAmountMtgComponent
 * </p>
 * <p>
 * Description: Calculates the Total of all payment elements for mortgage
 * component(P&I, Additional Principal, Tax Escrow if allocated to this mortgage
 * component).
 * </p>
 * Calculation Number- Calc-44.CM
 * @version 1.0 XS_11.1 19-Jun-2008 Initial Version
 */

public class TotalPaymentAmountMtgComponent extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.1 19-Jun-2008 Initial Version
     */
    public TotalPaymentAmountMtgComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 44.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.1 19-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Deal deal = (Deal) input;
                    // Get all the components for the deal
                    Collection listOfComponents = entityCache.getComponents(
                            deal, srk);
                    Iterator componentsItr = listOfComponents.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is Mortgage Component
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                        {
                            // Retrives the componentMortgage Object for the
                            // given
                            // componentId and copyId
                            ComponentMortgage componentMtg = (ComponentMortgage) entityCache
                                    .find(srk, new ComponentMortgagePK(
                                            component.getComponentId(),
                                            component.getCopyId()));
                            addTarget(componentMtg);
                        }
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.1 19-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());

        try
        {
            ComponentMortgage targetComponentMtg = (ComponentMortgage) target;
            // Retrive the component for the given component Mortgage
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentMtg.getComponentId(),
                            targetComponentMtg.getCopyId()));
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));
            if ((deal.getTaxPayorId() == Mc.TAX_PAYOR_LENDER)
                    && (("Y").equalsIgnoreCase(targetComponentMtg
                            .getPropertyTaxAllocateFlag())))
            {
                double totalPaymentAmount = targetComponentMtg
                        .getPAndIPaymentAmount()
                        + targetComponentMtg.getAdditionalPrincipal()
                        + targetComponentMtg.getPropertyTaxEscrowAmount();

                targetComponentMtg
                        .setTotalPaymentAmount(validate(totalPaymentAmount));
                targetComponentMtg.ejbStore();

            }
            else
            {
                double totalPaymentAmount = targetComponentMtg
                        .getPAndIPaymentAmount()
                        + targetComponentMtg.getAdditionalPrincipal();
                targetComponentMtg
                        .setTotalPaymentAmount(validate(totalPaymentAmount));
                targetComponentMtg.ejbStore();
            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.1 19-Jun-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "taxPayorId"));

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "propertyTaxEscrowAmount"));

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "propertyTaxAllocateFlag"));

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "pAndIPaymentAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "additionalPrincipal"));

        // Max totalPaymentAmount 99999999999.99
        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalPaymentAmount", T13D2));
    }

}
