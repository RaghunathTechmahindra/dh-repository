package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import com.filogix.util.Xc;

//Name:  GDS TDS 3 Year Rate
//Code:  Calc 88

public class GDSTDS3YearRate extends DealCalc {

  public GDSTDS3YearRate() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 88";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget(input);
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
      trace(this.getClass().getName());
      Deal deal = (Deal) target;
      try
      {
          double GDS_TDS3YRate = 0.0;
          Calendar curDate = Calendar.getInstance () ; // Get current date.

          // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
          PricingRateInventory pricingRateInventory = new  PricingRateInventory(srk, deal.getPricingProfileId());
          int ppId = pricingRateInventory.getPricingProfileID();
          PricingProfile  pricingProfile = ( PricingProfile) this.entityCache.find(srk, new PricingProfilePK(ppId));

          String rateCode = pricingProfile.getRateCode ();
          if ( ( rateCode != null ) && rateCode.equalsIgnoreCase ("UNCNF") )
            { GDS_TDS3YRate = 0.0; }
          else
          {
         	 //#DG930 use alternate qualifying rate  
              final PropertiesCache pc = PropertiesCache.getInstance();
              final int instId = deal.getInstitutionProfileId();
              final double ltvThreshold = Double.parseDouble(pc.getProperty(instId, Xc.ING_LTV_THRESHOLD, "80"));
              boolean allowOverrideQRP = pc.getProperty(instId, "com.filogix.qualoverridechk", "Y").equalsIgnoreCase("Y");
              final boolean allowOverrideRate = pc.getProperty(instId, "com.filogix.qualrateoverride", "Y").equalsIgnoreCase("Y");
              final int miIndId = deal.getMIIndicatorId();

              String theRateCodeToUse = "";
              final double ltv = deal.getCombinedLTV();
         	 if (allowOverrideQRP && "Y".equalsIgnoreCase(deal.getQualifyingRateOverrideFlag()))
         		 return; // qualify rate is already in deal
             if(allowOverrideQRP && "Y".equalsIgnoreCase(deal.getQualifyingOverrideFlag())) 
            	 return; //fxp33510
             else
 			 {
 				if(ltv > ltvThreshold 
 						 && (miIndId == Xc.MII_REQUIRED_STD_GUIDELINES 
 							|| miIndId == Xc.MII_UW_REQUIRED 
 							|| miIndId == Xc.MII_MI_PRE_QUALIFICATION))
 					 theRateCodeToUse = "com.basis100.calc.gdstds3yratecode";
 				 else
 					 theRateCodeToUse = "com.basis100.calc.gdstds.cmhclowratioratecode";
             }
             theRateCodeToUse = pc.getProperty(instId, theRateCodeToUse, "00");

             PricingRateInventory  pri1 = null;  // Get first for compare
             pri1 = new PricingRateInventory(srk).findMostRecentByRateCode(theRateCodeToUse);

             if ( pri1 == null )
             {
                 GDS_TDS3YRate = 0.0 ;
             }
             else
             {
                 GDS_TDS3YRate =  pri1.getInternalRatePercentage ();
             }
             InstitutionProfile inst = new InstitutionProfile(srk, deal.getInstitutionProfileId());
             if (inst.getThreeYearRateCompareDiscount() && GDS_TDS3YRate > 0.0)
            	 GDS_TDS3YRate -= deal.getDiscount();
         	 deal.setGDSTDS3YearRate ( validate( GDS_TDS3YRate ) );
             deal.ejbStore ();
           } // End of Else UNCNF
           return ;
      } // end of Try...
      catch ( Exception e )
      {
          String msg = "Benign Failure in Calculation" + this.getClass().getName();
          msg += ":[Target]:" + target.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@Target: " + this.getDoCalcErrorDetail ( target );
          logger.debug (msg );
      } // end of Catch.........

      deal.setGDSTDS3YearRate ( DEFAULT_FAILED_DOUBLE );
      deal.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

//    addInputParam(new CalcParam(ClassId.DEAL , "discount"));
    addInputParam(new CalcParam(ClassId.DEAL , "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL , "MIIndicatorId"));//#DG930
    addInputParam(new CalcParam(ClassId.DEAL , "combinedLTV"));
    addInputParam(new CalcParam(ClassId.DEAL , "qualifyingOverrideFlag"));
    addInputParam(new CalcParam(ClassId.DEAL , "overrideQualProd"));
    addInputParam(new CalcParam(ClassId.DEAL , "qualifyingRateOverrideFlag"));
    addInputParam(new CalcParam(ClassId.DEAL , "discount"));

    addTargetParam(new CalcParam(ClassId.DEAL , "GDSTDS3YearRate", T12D6)) ;
  }

}