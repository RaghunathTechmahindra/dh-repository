package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name: Total Purchase Price
//Code: CALC 45
//07/04/2000

public class TotalPurchasePrice extends DealCalc
{

  public TotalPurchasePrice()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 45";
  }

  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
   try
    {
      if ( input instanceof Deal )
          addTarget(input);
      else
         if ( input instanceof Property )
            {
                Property property = ( Property) input ;
                Deal deal = (Deal) entityCache.find ( srk,
                        new DealPK(property.getDealId (), 
                             property.getCopyId ()) );
                addTarget ( deal );
             }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }

  }

  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());
     Deal deal = (Deal)target;

     try
     {
        PicklistData pickListData = new PicklistData();

        double totalPurchasePrice = 0.0;
        trace( "SpecialFeatureId = " + deal.getSpecialFeatureId () );

        if ( deal.getSpecialFeatureId () != 1 ) // 1: Pre-Approval
        {
            Collection properties = this.entityCache.getProperties (deal);
            Iterator itPr = properties.iterator ();

            while ( itPr.hasNext () )
            {
                Property property = (Property) itPr.next ();
                totalPurchasePrice += property.getPurchasePrice () ;
                trace( "totalPurchasePrice = " + totalPurchasePrice );
            }
         }
        else
          totalPurchasePrice = deal.getPAPurchasePrice () ;

        trace( "totalPurchasePrice = " + totalPurchasePrice );
        totalPurchasePrice = Math.round ( totalPurchasePrice * 100) /100.00 ;
        deal.setTotalPurchasePrice ( validate(totalPurchasePrice) );
        deal.ejbStore();
        return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    deal.setTotalPurchasePrice(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "specialFeatureId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAPurchasePrice"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "purchasePrice"));                                                                   

    addTargetParam(new CalcParam(ClassId.DEAL, "totalPurchasePrice", T13D2 ));
  }



}
