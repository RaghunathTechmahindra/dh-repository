package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

// Name: Combined Total Condo Fee Expense
// Code: Calc 26
// 06/30/2000

public class CombinedTotalCondoFeeExpense extends DealCalc {

  public CombinedTotalCondoFeeExpense() throws Exception{
    super();
    initParams();
    this.calcNumber = "Calc 26";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        PropertyExpense pe = (PropertyExpense) input;
        Property p = (Property) entityCache.find(srk, 
                         new PropertyPK(pe.getPropertyId(), pe.getCopyId() ));
        Deal d = (Deal) entityCache.find(srk, 
                         new DealPK(p.getDealId(), p.getCopyId()));
        addTarget(d);
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Deal deal = (Deal) target;

     try
     {
        Collection pe = this.entityCache.getPropertyExpenses (deal);
        Iterator itPe = pe.iterator ();

        double totalCondoFee = 0.0;

        while ( itPe.hasNext() )
        {
          PropertyExpense propertyExpense = ( PropertyExpense ) itPe.next();

          if (propertyExpense.getPropertyExpenseTypeId () == 1 /* Condo Fees */)
            totalCondoFee += propertyExpense.getPropertyExpenseAmount () *
                DealCalcUtil.annualize (propertyExpense.getPropertyExpensePeriodId (), "PropertyExpensePeriod" ) ;
          trace( "totalCondoFee = " + totalCondoFee );
          trace( "PropertyExpensePeriod = " +
              DealCalcUtil.annualize (propertyExpense.getPropertyExpensePeriodId (), "PropertyExpensePeriod" ) );
        } // End of all Property expenses
        
        totalCondoFee = Math.round ( totalCondoFee * 100 ) / 100.00;
        deal.setCombinedTotalCondoFeeExp ( validate(totalCondoFee) ) ;
        deal.ejbStore ();
        return;
    }
    catch ( Exception e )
    {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );        
        logger.debug (msg );
     }

    deal.setCombinedTotalCondoFeeExp (DEFAULT_FAILED_DOUBLE ) ;
    deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpenseTypeId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalCondoFeeExp", T13D2));
  }

}
