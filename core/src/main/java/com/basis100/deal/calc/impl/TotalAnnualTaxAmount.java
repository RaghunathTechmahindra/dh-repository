package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Total Actual Appraised Value
//Code: Calc 90
//07/05/2000

public class TotalAnnualTaxAmount extends DealCalc {

  public TotalAnnualTaxAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 90";
  }


   public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
   try
    {
      if ( input instanceof  PropertyExpense )  // PropertyExpense ==> Property
      {
        PropertyExpense propertyExpense = ( PropertyExpense ) input;
        Property property = (Property) entityCache.find (srk,
            new PropertyPK(propertyExpense.getPropertyId (), 
                propertyExpense.getCopyId ()) ) ;
        addTarget ( property );
      }
    }
    catch(Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Property property = (Property) target;

     try
     {
       Collection propertyExpenses = this.entityCache.getPropertyExpenses (property);
       Iterator itPE = propertyExpenses.iterator ();

       double totalAnnualTaxAmount = 0.0;

       while ( itPE.hasNext () )
        {
          PropertyExpense propertyExpense = ( PropertyExpense ) itPE.next ();

          if (propertyExpense.getPropertyExpenseTypeId () == 0 /* Municipal Taxes */)
              totalAnnualTaxAmount  += propertyExpense.getPropertyExpenseAmount () *
                  DealCalcUtil.annualize (propertyExpense.getPropertyExpensePeriodId () , "PropertyExpensePeriod");
        }
       totalAnnualTaxAmount = Math.round ( totalAnnualTaxAmount * 100) /100.00 ; 
       property.setTotalAnnualTaxAmount ( validate(totalAnnualTaxAmount));
       property.ejbStore ();
       return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }
     
     property.setTotalAnnualTaxAmount (DEFAULT_FAILED_DOUBLE);
     property.ejbStore ();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE ,"propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpenseTypeId" )) ;

    addTargetParam(new CalcParam(ClassId.PROPERTY, "totalAnnualTaxAmount", T15D2 ));
  }


}
