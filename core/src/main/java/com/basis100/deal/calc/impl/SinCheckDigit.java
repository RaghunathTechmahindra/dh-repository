package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

public class SinCheckDigit extends DealCalc
{


  public SinCheckDigit()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 69";
  }

  

  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      addTarget(input);
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }  

  public void doCalc(Object target) throws Exception
  {

     trace(this.getClass().getName());
     Borrower bt = (Borrower)target;
     try
     {
        String sin = bt.getSocialInsuranceNumber();

        if ( sin == null || sin.length()<=0 )
        {
            bt.setSINCheckDigit( "0" );
            bt.ejbStore();
        }
        else
        {
            sin = sin.trim();
            int val = DealCalcUtil.sinCheckDigit(sin);

            if(val == -1)  // -1 indicates an invalid sin string (non-digit characters etc) dont do the check;
              throw new Exception("Required Input invalid ->borrower.getSocialInsuranceNumber().");

            bt.setSINCheckDigit( "" + val ) ;
            bt.ejbStore();
        }
        return;
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     bt.setSocialInsuranceNumber(DEFAULT_FAILED_STRING);
     bt.ejbStore();
  }



  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.BORROWER, "socialInsuranceNumber"));

    addTargetParam(new CalcParam(ClassId.BORROWER, "SINCheckDigit"));
  }



} 
