package com.basis100.deal.calc.impl;

/**
 * Title:        EquivalentInterestRate (Calc-108)
 * Description:  Calculation for Equivalent Interest Rate only used for DJ for the time being
 * Copyright:    Copyright (c) 2004
 * Company:      Filogix Inc.
 * Date :        21 July 2004
 * @author Billy Lam
 * @version 1.0 (initial version)
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.PropertiesCache;

public class EquivalentInterestRate
extends DealCalc
{

    public EquivalentInterestRate() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 108";
    }

    public Collection getTargets()
    {
        return this.targets;
    }

    /**
     * Given an input object this method determines the appropriate target object
     * and adds the object to it's list of targets (targets).
     *
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget(input);
        }
        catch(Exception e)
        {
            String msg = "Failed to get Targets for: " + this.getClass().getName();
            msg += " :[Input]: " + input.getClass().getName();
            msg += "  Reason: " + e.getMessage();
            msg += " @input: " + this.getDoCalcErrorDetail(input);
            logger.error(msg);
            throw new TargetNotFoundException(msg);
        }
    }

    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        Deal deal = (Deal)target;

        try
        {
            //--> Skip calc if the property set to No.  Should never hapened but just in case.
            if((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.calc.performequivalentInterestRate", "N")).equals("N"))
            {
                return;
            }

            //--> Reset to 0.000 if Interest Compound ID <> 1 (Monthly) or InterestType <> 2 (Capped Variable)
            if(deal.getMtgProd().getInterestCompoundingId() != Mc.INTEREST_COMPOUNDID_MONTHLY ||
                    deal.getMtgProd().getInterestTypeId() != 2)
            {
                trace( "Interest Compound ID <> 1 (Monthly) or InterestType <> 2 (Capped Variable) :: Reset EquivalentInterestRate = 0.0");
                deal.setEquivalentInterestRate(0.0d);
                deal.ejbStore();
                return;
            }

            //--> Perform the Formular :
            //--> 100*Compound*(POWER(1+((POWER(1+Interest/(100*PmtFreq),PmtFreq)-1)*100)/100,(1/Compound))-1)
            double netInterestRate = deal.getNetInterestRate();
            int paymentFreqId = deal.getPaymentFrequencyId();
            trace( "netInterestRate = " + netInterestRate);
            trace("paymentFreqId = " + paymentFreqId);
            //--> the purpose of this calc is to get the result of Semi-Annual compound if the rate is Monthly
            //--> That's why we hardcoded the compound to Semi-Annual = 2d
            double compound = DealCalcUtil.COMPOUND_FREQ_SEMI_ANNUAL;

            //--> Get the Annualized Payment
            double annualizedPayment = 0d;
            switch(paymentFreqId)
            {
            case Mc.PAY_FREQ_MONTHLY:
                annualizedPayment = 12d;
                break;

            case Mc.PAY_FREQ_SEMIMONTHLY:
                annualizedPayment = 24d;
                break;

            case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:
            case Mc.PAY_FREQ_BIWEEKLY:
                annualizedPayment = 26d;
                break;

            case Mc.PAY_FREQ_ACCELERATED_WEEKLY:
            case Mc.PAY_FREQ_WEEKLY:
                annualizedPayment = 52d;
                break;
            }
            trace("annualizedPayment = " + annualizedPayment);

            //--> Calc the formular
            double equIntRate = 0;
            //--> calculate : 1+Interest/(100*PmtFreq)
            equIntRate = (netInterestRate / (100d * annualizedPayment)) + 1;

            //--> calculate : POWER(1+Interest/(100*PmtFreq),PmtFreq)-1
            equIntRate = Math.pow(equIntRate, annualizedPayment) - 1;

            //--> calculate : 1+((POWER(1+Interest/(100*PmtFreq),PmtFreq)-1)*100)/100
            equIntRate = (equIntRate*100/100) + 1;

            //--> calculate : POWER(1+((POWER(1+Interest/(100*PmtFreq),PmtFreq)-1)*100)/100,(1/Compound))
            equIntRate = Math.pow(equIntRate, (1/compound));

            //--> calculate : 100*Compound*(POWER(1+((POWER(1+Interest/(100*PmtFreq),PmtFreq)-1)*100)/100,(1/Compound))-1)
            equIntRate = 100*compound*(equIntRate - 1);

            trace("equIntRate (before rounded) = " + equIntRate);
            //--> round to 3 decimal place
            equIntRate = Math.round ( equIntRate * 1000 ) /1000.00;
            trace("equIntRate (final) = " + equIntRate);
            deal.setEquivalentInterestRate(equIntRate);
            deal.ejbStore();
            return;
        }
        catch(Exception e)
        {
            String msg = "Benign Failure in Calculation" + this.getClass().getName();
            msg += ":[Target]:" + target.getClass().getName();
            msg += "  Reason: " + e.getMessage();
            msg += "@Target: " + this.getDoCalcErrorDetail(target);
            logger.debug(msg);
        }

        deal.setEquivalentInterestRate(DEFAULT_FAILED_DOUBLE);
        deal.ejbStore();
    }

    public boolean hasInput(CalcParam input)
    {
        return this.inputParam.contains(input);
    }

    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.DEAL, "netInterestRate"));
        addInputParam(new CalcParam(ClassId.DEAL, "interestCompoundId"));
        addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));

        addTargetParam(new CalcParam(ClassId.DEAL, "equivalentInterestRate", T12D6));

    }
}
