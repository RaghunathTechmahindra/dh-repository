package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

//Name: Combined Total Tax Expense
//Code: Calc 24
//07/08/2000

public class CombinedTotalTaxExpense extends DealCalc
{


  public CombinedTotalTaxExpense() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 24";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

 /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
        PropertyExpense pe = (PropertyExpense) input;
        Property p = (Property) entityCache.find (srk, 
                            new PropertyPK(pe.getPropertyId(), pe.getCopyId())) ;
        Deal d = (Deal) entityCache.find (srk, 
                            new DealPK( p.getDealId(), p.getCopyId ()));
        addTarget ( d ) ;
    }
    catch ( Exception e )
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = ( Deal ) target;

     try
     {
        double totalTaxExpense = 0.0;

        Collection propertyExpenses = this.entityCache.getPropertyExpenses (deal);
        Iterator itPE = propertyExpenses.iterator ();
        PropertyExpense propertyExpense = null;
        while ( itPE.hasNext()  )
          {
            propertyExpense = ( PropertyExpense) itPE.next() ;
            if (propertyExpense.getPropertyExpenseTypeId () == 0 /* Municipal Taxes */)
              {
                totalTaxExpense +=  propertyExpense.getPropertyExpenseAmount ()
                    * DealCalcUtil.annualize (propertyExpense.getPropertyExpensePeriodId (), "PropertyExpensePeriod");
              }
            trace("totalTaxExpense = " + totalTaxExpense );
          }  // ------- End of all Expense Property -------------
        totalTaxExpense = Math.round ( totalTaxExpense * 100 ) /100.00; 
        deal.setCombinedTotalAnnualTaxExp ( validate(totalTaxExpense) );
        deal.ejbStore ();
        return;
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );        
        logger.debug (msg );
     }
     
   deal.setCombinedTotalAnnualTaxExp ( this.DEFAULT_FAILED_DOUBLE );
   deal.ejbStore ();

  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseTypeId"));

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTotalAnnualTaxExp", T13D2));

  }



}
