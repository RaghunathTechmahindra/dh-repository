package com.basis100.deal.calc.impl;


import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name: Max Principal Amount
//Code: Calc 83
//07/17/2000
// to do:
// 611,25427       used to be good.

public class MaximumPrincipalAmount extends DealCalc {

  public MaximumPrincipalAmount() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 83";
  }

  public Collection getTargets()
  {
    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof Income )
        {
          Income i = (Income) input;
          Borrower b = (Borrower) entityCache.find(srk, 
                                 new BorrowerPK(i.getBorrowerId(), 
                                                i.getCopyId()));
          Deal d = (Deal) entityCache.find (srk, 
                                 new DealPK(b.getDealId(), b.getCopyId ()));
          addTarget (d);
          return;
        }   // Income ==> Deal

       if ( input instanceof Deal )
        {   addTarget ( input ) ;
        }
    }
    catch ( Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = (Deal) target;
     try
     {
         double maxPrincipalAmount = 0.0 ;

         // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
         PricingRateInventory pricingRateInventory = new  PricingRateInventory(srk, deal.getPricingProfileId());
         int ppId = pricingRateInventory.getPricingProfileID();
         PricingProfile  pricingProfile = ( PricingProfile) this.entityCache.find(srk, new PricingProfilePK(ppId));

         String rateCode = pricingProfile.getRateCode () ;
         if ( rateCode != null )
         {
            if ( rateCode.equalsIgnoreCase ( "UNCNF" ) )
            {
               deal.setMaximumPrincipalAmount ( 0 );
               deal.ejbStore ();
               return;
            }
         }

          double totalCombinedIncome = 0.0 ;

          double paymentFrequency = DealCalcUtil.annualize (deal.getPaymentFrequencyId () , "PaymentFrequency" );
          //Total Combined Income
          Collection incomes = this.entityCache.getIncomes (deal);
          Iterator itIN = incomes.iterator () ;
          Income income = null ;
          while ( itIN.hasNext () )
          {
              income = ( Income) itIN.next () ;
              if  ( income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT )
              //-------- totalCombinedIncome --------------
              {
                  if ( income.getIncIncludeInGDS () )
                     totalCombinedIncome += income.getIncomeAmount ()
                      * DealCalcUtil.annualize (income.getIncomePeriodId () , "IncomePeriod")
                        * income.getIncPercentInGDS () / 100.00 ;
              }
              trace( "[totalIncome= " + totalCombinedIncome + "]");
          }   // End of Incomes
          trace( "[totalIncome= " + totalCombinedIncome + "]");
          //----------------- Get the  Max GDS Allowed -----------------------------------------------
          double maxGDSAllowed = 0.00;
          int linesOfBusinessId = deal.getLineOfBusinessId ();
          int dealTypeId = deal.getDealTypeId ();

          if  ( linesOfBusinessId == Mc.LOB_A && dealTypeId == 1 /* High ratio */ )
             maxGDSAllowed = 0.32;
          if ( linesOfBusinessId == Mc.LOB_A && dealTypeId != 1 /* High ratio */ )
             maxGDSAllowed = 0.40;
          if ( linesOfBusinessId != Mc.LOB_A )
             maxGDSAllowed = 0.45;
         //----------------- End of Getting the  Max GDS Allowed ---------------------------------------
         //--------------- Calculate Max Principal Amount ------------
         trace( "[maxGDSAllowed= " + maxGDSAllowed + "]" );
         double interestFactor = 0.0;

        // Get the InterestCompoundDesc from deal.MtgProg.interestcompoundId -- BILLY 07Dec2001
         // added inst id for ML
        String InterestCompoundDesc 
            = PicklistData.getMatchingColumnValue(-1,
                                                  "interestcompound",
                                                  deal.getMtgProd().getInterestCompoundingId(), 
                                                  "interestcompounddescription");
         interestFactor = DealCalcUtil.interestFactor ( deal.getNetInterestRate () , paymentFrequency, InterestCompoundDesc) ;
         trace( "InterestCompoundDesc = " + InterestCompoundDesc + ":: InterestFacotr = " +  interestFactor + "]" ) ;

         if ( ( interestFactor !=0 ) && ( paymentFrequency !=0 ) )
           maxPrincipalAmount =
                ( 1 -
                   1/ ( Math.pow( 1+ interestFactor ,  deal.getAmortizationTerm () / 12 * paymentFrequency ))
                )
                /  interestFactor
                * ( totalCombinedIncome / paymentFrequency )
                * maxGDSAllowed ;
       maxPrincipalAmount = Math.round ( maxPrincipalAmount * 100 ) /100.00;
       deal.setMaximumPrincipalAmount ( validate(maxPrincipalAmount) );
       deal.ejbStore ();
       return;

     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

    deal.setMaximumPrincipalAmount(DEFAULT_FAILED_DOUBLE );
    deal.ejbStore ();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));

    addInputParam(new CalcParam(ClassId.DEAL , "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL , "lineOfBusinessId") );
    addInputParam(new CalcParam(ClassId.DEAL , "dealTypeId" ) );
    addInputParam(new CalcParam(ClassId.DEAL , "netInterestRate") );
    addInputParam(new CalcParam(ClassId.DEAL , "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL , "mtgProdId"));


    addTargetParam(new CalcParam(ClassId.DEAL, "maximumPrincipalAmount", T13D2));
  }
}
