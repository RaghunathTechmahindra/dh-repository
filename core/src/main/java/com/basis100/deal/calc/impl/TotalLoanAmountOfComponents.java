/*
 * @(#)TotalLoanAmountOfComponents.java July 17, 2008 <story id : XS-11.16>
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentCreditCardPK;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentOverdraftPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;

/**
 * <p>
 * Title: TotalLoanAmountOfComponents
 * </p>
 * <p>
 * Description: Calculates total loan amount for all the eligilble components.
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.16<> Jul 17, 2008 Initial version
 * @version 1.1 artf751756 Jul 30, 2008 Modified ComponentOverdraft and ComponentCreditCard setion of getTarget(..) 
 * @version 1.2 artf751756 Jul 30, 2008 added Deal as import parameter 
 * @version 1.3 artf751756 1 Aug 2008 changed iniput param Component, instead of ComponentSummary
 */

public class TotalLoanAmountOfComponents extends DealCalc
{

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @throws Exception
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial version
     */
    public TotalLoanAmountOfComponents() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "CALC 116";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     *             
     * @version 1.2 July 30 added Deal as input target       
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        
        try
        {
            int classId = ((DealEntity) input).getClassId();
            /*
             * int componentId = int copyid = if(mtg) else if(loc) else if
             * (loan) find the compoent add as target
             */
            //***** MCM for artf751756 starts
//            if (classId == ClassId.COMPONENTSUMMARY) {
//                if (input != null ) {
//                    ComponentSummary componentSummary = (ComponentSummary) input;
//                    addTarget(componentSummary);
//                }
            if (classId == ClassId.COMPONENT) {
                Component component = (Component) input;
                ComponentSummary componentSummary = (ComponentSummary) entityCache
                    .find(srk, new ComponentSummaryPK(
                        component.getDealId(), component.getCopyId()));
                addTarget(componentSummary);
            } else 
                //***** MCM for artf751756 starts
                if (classId == ClassId.COMPONENTMORTGAGE)
                {
                    ComponentMortgage componentMtg = (ComponentMortgage) input;
                    // find component for the given componentId and
                    // copyId
                    Component component = (Component) entityCache.find(srk,
                            new ComponentPK(componentMtg.getComponentId(),
                                    componentMtg.getCopyId()));    
                    ComponentSummary componentSummary = (ComponentSummary) entityCache
                            .find(srk, new ComponentSummaryPK(
                                    component.getDealId(), component.getCopyId()));
    
                    addTarget(componentSummary);
                    return;
                }
            else
                if (classId == ClassId.COMPONENTLOC)
                {
                    ComponentLOC componentLoc = (ComponentLOC) input;
                    // find component for the given componentId and
                    // copyId
                    Component component = (Component) entityCache.find(srk,
                            new ComponentPK(componentLoc.getComponentId(),
                                    componentLoc.getCopyId()));

                    try {
                        ComponentSummary componentSummary = (ComponentSummary) entityCache
                        .find(srk, new ComponentSummaryPK(component
                                .getDealId(), component.getCopyId()));
                        addTarget(componentSummary);
                    } catch (Exception e) {
                        // do nothing
                    }
                    return;
                }
                else
                    if (classId == ClassId.COMPONENTLOAN)
                    {
                        ComponentLoan componentLoan = (ComponentLoan) input;
                        // find component for the given componentId and
                        // copyId
                        Component component = (Component) entityCache.find(srk,
                                new ComponentPK(componentLoan.getComponentId(),
                                        componentLoan.getCopyId()));

                        ComponentSummary componentSummary = (ComponentSummary) entityCache
                                .find(srk, new ComponentSummaryPK(component
                                        .getDealId(), component.getCopyId()));

                        addTarget(componentSummary);
                        return;
                    }
                    else
                        if (classId == ClassId.COMPONENTOVERDRAFT)
                        {
                            ComponentOverdraft componentOverdraft = (ComponentOverdraft) input;
                            // find component for the given componentId and
                            // copyId
                            Component component = (Component) entityCache.find(
                                    srk,
                                    new ComponentPK(componentOverdraft
                                            .getComponentId(),
                                            componentOverdraft.getCopyId()));

                            ComponentSummary componentSummary = (ComponentSummary) entityCache
                                    .find(srk,
                                            new ComponentSummaryPK(component
                                                    .getDealId(), component
                                                    .getCopyId()));

                            addTarget(componentSummary);
                            return;
                        }
                        else
                            if (classId == ClassId.COMPONENTCREDITCARD)
                            {
                                ComponentCreditCard componentCreditCard = (ComponentCreditCard) input;
                                // find component for the given componentId and
                                // copyId
                                Component component = (Component) entityCache
                                        .find(
                                                srk,
                                                new ComponentPK(
                                                        componentCreditCard
                                                                .getComponentId(),
                                                        componentCreditCard
                                                                .getCopyId()));

                                ComponentSummary componentSummary = (ComponentSummary) entityCache
                                        .find(srk, new ComponentSummaryPK(
                                                component.getDealId(),
                                                component.getCopyId()));

                                addTarget(componentSummary);
                                return;
                            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(":[Input]:");
            msg.append(input.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@input: ");
            msg.append(this.getDoCalcErrorDetail(input));
            logger.debug(msg.toString());
            throw new TargetNotFoundException(e.getMessage());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.16 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        ComponentSummary targetComponentSummary = (ComponentSummary) target;

        Deal deal = (Deal) entityCache.find(srk, new DealPK(
                targetComponentSummary.getDealId(), targetComponentSummary
                        .getCopyId()));
        double totalAmount = 0.0d;
        try
        {
            // Get the mtgprod for the component
            MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                    new MtgProdPK(deal.getMtgProdId()));

            // Get all the components
            Collection components = deal.getComponents();
            // Check for the component eligibility
            if (mtgProd.getComponentEligibleFlag().equals("Y")
                    && components != null)
            {
                Iterator itComponents = components.iterator();
                Component component = null;
                while (itComponents.hasNext())
                {
                    component = (Component) itComponents.next();
                    // Component Mortgage
                    if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                    {
                        ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
                                .find(srk, new ComponentMortgagePK(component
                                        .getComponentId(), component
                                        .getCopyId()));
                        totalAmount += componentMtg.getTotalMortgageAmount();
                    }
                    else
                        // Component LOC
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOC)
                        {
                            ComponentLOC componentLOC = (ComponentLOC) this.entityCache
                                    .find(srk, new ComponentLOCPK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            totalAmount += componentLOC.getTotalLocAmount();
                        }
                        else
                            // Component Loan
                            if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOAN)
                            {
                                ComponentLoan componentLoan = (ComponentLoan) this.entityCache
                                        .find(srk, new ComponentLoanPK(
                                                component.getComponentId(),
                                                component.getCopyId()));
                                totalAmount += componentLoan.getLoanAmount();
                            }
                            else
                                // Component Credit Card
                                if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_CREDITCARD)
                                {
                                    ComponentCreditCard componentCreditCard = (ComponentCreditCard) this.entityCache
                                            .find(
                                                    srk,
                                                    new ComponentCreditCardPK(
                                                            component
                                                                    .getComponentId(),
                                                            component
                                                                    .getCopyId()));
                                    totalAmount += componentCreditCard
                                            .getCreditCardAmount();
                                }
                                else
                                    // Component Overdraft
                                    if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_OVERDRAFT)
                                    {
                                        ComponentOverdraft componentOverdraft = (ComponentOverdraft) this.entityCache
                                                .find(
                                                        srk,
                                                        new ComponentOverdraftPK(
                                                                component
                                                                        .getComponentId(),
                                                                component
                                                                        .getCopyId()));
                                        totalAmount += componentOverdraft
                                                .getOverDraftAmount();
                                    }

                }
            }
            // Set total amount
            targetComponentSummary.setTotalAmount(validate(totalAmount));
            // Save component
            targetComponentSummary.ejbStore();
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Initializes the input & target parameters which is called in
     * the constructor.
     * </p>
     * @version 1.0 XS_11.16 Initial Version
     * @throws ParamTypeException
     *             exception that may occur if param type is not valid.
     * @version 1.2 artf751756 30 Jul 2008 added iniput param ComponentSummary
     * @version 1.3 artf751756 1 Aug 2008 changed iniput param Component, instead of ComponentSummary
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        /***************MCM Impl team changes starts - artf751756  *******************/ 
//        addInputParam(new CalcParam(ClassId.COMPONENTSUMMARY, "NumberOfComponents"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "ComponentType"));
        /***************MCM Impl team changes ends - artf751756  *******************/ 

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "totalLocAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "loanAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTCREDITCARD,
                "creditCardAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTOVERDRAFT,
                "overDraftAmount"));

        addTargetParam(new CalcParam(ClassId.COMPONENTSUMMARY, "totalAmount",
                T13D2));
    }

}
