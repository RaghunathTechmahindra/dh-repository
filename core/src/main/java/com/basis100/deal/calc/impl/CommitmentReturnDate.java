package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.util.*;
import com.basis100.resources.*;

//Name: Commitment Return Date
//Code: Calc 38
//7/4/2000

public class CommitmentReturnDate extends DealCalc
{

  public CommitmentReturnDate() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 38";
  }

  public Collection getTargets()
  {

    return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
      try
      {
          addTarget ( input ) ;
      }
      catch(Exception e)
      {
          String msg = "Failed to get Targets for: " + this.getClass().getName();
          msg += " :[Input]: " + input.getClass ().getName () ;
          msg += "  Reason: " + e.getMessage();
          msg += " @input: " + this.getDoCalcErrorDetail ( input );
          logger.error (msg );
          throw new TargetNotFoundException (msg);
      }
  }

  public void doCalc(Object target) throws Exception
  {
      trace(this.getClass().getName());
      Deal deal = (Deal)target;

      try
      {
      logger.trace("Calc 38");
        Calendar returnDate = Calendar.getInstance () ;
        if (deal.getReturnDate() != null) return;
        if (deal.getCommitmentIssueDate() != null )     // modified Feb 26, 2001
        {
          trace( "CommitmentIssueDate = " + deal.getCommitmentIssueDate () );
          trace( "returnDate = " + deal.getReturnDate () );

          int daysToAdd = 7;
          int dayType = 0;  // 0 = Calendar day type || 1 = Business Day type
          try{
            daysToAdd = Integer.parseInt(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                "com.basis100.calc.commitmentreturndate.daystoadd", "7"));
            dayType = Integer.parseInt(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                "com.basis100.calc.commitmentreturndate.adddaytype", "0"));
          }
          catch(Exception e)
          {
            trace("Invalid parameter ==> use defaults");
          }

          trace("The daysToAdd = " + daysToAdd + " dayType = " + dayType);

          if(dayType == 0)
          {
            // Add Calendar days
            returnDate.setTime ( deal.getCommitmentIssueDate ());
            returnDate.add ( Calendar.DAY_OF_MONTH,  daysToAdd );
          }
          else
          {
            // Add Business Days
            BusinessCalendar bCal = new BusinessCalendar(srk);

            // Get timezone of the branch of the Deal
            BranchProfile theBranch = new BranchProfile(srk, deal.getBranchProfileId());

            // Get Number of Minutes for the Num of Business Day to add
            int daysInMinutes = daysToAdd * bCal.getBusinessDayDurationInMinutes();


            //===================== ALERT !!! =============================================================
            //As we don't care the time of the date the quickest solution is to set the
            // Time to noon 12pm (Don't care of the time zone, it's dirty fix but may only work in TimeZone
            //  +/- 3 hrs.  It should be fixed in the future !!!!!
            //    -- By BILLY
            Calendar tmpCDate = Calendar.getInstance();
            tmpCDate.setTime(deal.getCommitmentIssueDate());
            tmpCDate.set(Calendar.HOUR_OF_DAY, 12);
            // ============================================================================================
            // Calc the date
            //--> Bug fix to preserve the Hour same as Issued Date
            //--> By Billy 09June2003
            Date resultDate = bCal.calcBusDate(tmpCDate.getTime(), theBranch.getTimeZoneEntryId(), daysInMinutes);
            resultDate.setHours(deal.getCommitmentIssueDate().getHours());
            //======================================================
            returnDate.setTime(resultDate);
          }

          deal.setReturnDate ( returnDate.getTime () ) ;
          trace("Result CommitmentReturnDate = " + deal.getReturnDate());

          deal.ejbStore ();
          return;
        }
        else
        {
          trace("deal.getCommitmentIssueDate() = null.  Do nothing !!");
          return;
        }
      }
      catch (Exception e)
      {
          String msg = "Benign Failure in Calculation" + this.getClass().getName();
          msg += ":[Target]:" + target.getClass ().getName () ;
          msg += "  Reason: " + e.getMessage();
          msg += "@Target: " + this.getDoCalcErrorDetail ( target );
          logger.debug (msg );
      }

    deal.setReturnDate ( this.DEFAULT_FAILED_DATE) ;
    deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "commitmentIssueDate"));

    //Linda add --Feb 26, 2001
    addInputParam(new CalcParam(ClassId.DEAL, "returnDate"));
    //Added by BILLY -- 09April2002
    addInputParam(new CalcParam(ClassId.DEAL, "branchProfileId"));


    addTargetParam(new CalcParam(ClassId.DEAL, "returnDate"));
  }



}
