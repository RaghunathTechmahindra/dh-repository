package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: IADAmountLoanComponent
 * </p>
 * <p>
 * Description:Calculates the Amount of interest to be collected which is
 * computed as the number of days from the Closing Date to Interest Adjustment
 * Date for which interest should be charged.
 * </p>
 * Calculation Number- Calc-12.CLoan
 * @version 1.0 XS_11.15 25-Jun-2008 Initial Version
 * @version 1.0 Modified the getTarget(..) method
 * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
 */
public class IADAmountLoanComponent extends DealCalc
{

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.15 24-Jun-2008 Initial Version
     */
    public IADAmountLoanComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 12.CLoan";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.15 24-Jun-2008 Initial Version
     * @version 1.1 XS_11.15 08-Jul-2008 Added ComponentTypeId check
     * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block 
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOAN)
            {
                ComponentLoan componentLoan = (ComponentLoan) input;
                addTarget(componentLoan);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    Component component = (Component) input;
                    // find componentLoan for the given componentId and
                    // copyId
                    try {
                        if(component.getComponentTypeId()== Mc.COMPONENT_TYPE_LOAN){
                            ComponentLoan componentLoan = (ComponentLoan) entityCache
                                    .find(srk, new ComponentLoanPK(component
                                            .getComponentId(), component.getCopyId()));
                            addTarget(componentLoan);
                        }
                    } catch (Exception e) {
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.15 24-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());

        try
        {
            ComponentLoan targetComponentLoan = (ComponentLoan) target;
            // Retrive the component for the given component Loan
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLoan.getComponentId(),
                            targetComponentLoan.getCopyId()));
            // Retrive the Deal for the given component
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));
            // Retrive the LenderProfile object for the given lender profile
            LenderProfile lenderProfile = (LenderProfile) this.entityCache
                    .find(srk, new LenderProfilePK(deal.getLenderProfileId()));
            // Retrive the Interest Compound Description from the picklist using
            // BXResources for the given Interest Compound Id
            String interestCompoundDesc = BXResources.getPickListDescription(
                    lenderProfile.getInstitutionProfileId(),
                    "INTERESTCOMPOUND", lenderProfile.getInterestCompoundId(),
                    srk.getLanguageId());
            // Retrive the Sysproperty for
            // 'com.basis100.deal.calc.InterimInterest.norounding' using
            // propertyCache
            String interimInterestNoRounding = PropertiesCache
                    .getInstance()
                    .getProperty(deal.getInstitutionProfileId(),
                            "com.basis100.deal.calc.InterimInterest.norounding");

            double iADAmount = 0.0d;
            double perDiemInterestAmount = 0.0d;
            if ("N".equalsIgnoreCase(interimInterestNoRounding))
            {
                perDiemInterestAmount = DealCalcUtil
                        .calculatePerDiemInterestAmount(interestCompoundDesc,
                                targetComponentLoan.getNetInterestRate(),
                                targetComponentLoan.getLoanAmount());
                // Rounding to 5 decimal places
                perDiemInterestAmount = Math
                        .round(perDiemInterestAmount * 100000) / 100000;
            }
            else
            {
                perDiemInterestAmount = targetComponentLoan
                        .getPerDiemInterestAmount();
            }
            // Rounding to 2 decimal places
            iADAmount = Math.round(perDiemInterestAmount
                    * targetComponentLoan.getIadNumberOfDays() * 100d) / 100d;
            // if the InterestAdjustmentAmount is >99999999999.99 then Validate
            // will round of to the max given value in the TargetParam
            targetComponentLoan.setInterestAdjustmentAmount(validate(iADAmount));
            targetComponentLoan.ejbStore();

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.15 24-Jun-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
                "perDiemInterestAmount"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
                "loanAmount"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
                "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN,
                "iadNumberOfDays"));

        // Max interestAdjustmentAmount 99999999999.99
        addTargetParam(new CalcParam(ClassId.COMPONENTLOAN,
                "interestAdjustmentAmount", T13D2));
    }

}