package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name:  First Payment Date Limit
//Code: Calc 100

public class FirstPaymentDateLimit extends DealCalc {

   public FirstPaymentDateLimit()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 100";
  }


  public Collection getTargets()
  {
      return this.targets;
  }


  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
       addTarget(input);
    }
    catch ( Exception e)
    {
      String msg = "Failed to get Targets for: " + this.getClass().getName();
      msg += " :[Input]: " + input.getClass ().getName () ;
      logger.error(msg);
      msg = "  Reason: " + e.getMessage();
      msg += " @input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);
    }
  }



  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = ( Deal ) target ;

     try
     {
         int period=0;
         int pf = deal.getPaymentFrequencyId ();
         logger.debug("FirstpaymentDateLimit(dealid = " + deal.getDealId() + ") calc: PaymentFrequencyId = " + pf);

         if ( pf == Mc.PAY_FREQ_SEMIMONTHLY )
                period = 15 ;
         else if ( pf == Mc.PAY_FREQ_BIWEEKLY || pf == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY )
                period = 14 ;
         else if ( pf == Mc.PAY_FREQ_WEEKLY || pf == Mc.PAY_FREQ_ACCELERATED_WEEKLY )
                period = 7 ;
         else if ( pf == Mc.PAY_FREQ_MONTHLY )
            period = 30 ; // 30 days
         logger.debug("FirstpaymentDateLimit(dealid = " + deal.getDealId() + ") calc: Period = " + period);

         Calendar firstPaymentDate = Calendar.getInstance (TimeZone.getDefault (), Locale.getDefault ());
         Date ecDate = deal.getEstimatedClosingDate () ;
         if ( ecDate != null & ( period > 0 ) )
         {
           trace( "EstimatedClosingDate = " + ecDate );
           trace( "paymentFrequency = " + period );
           firstPaymentDate.setTime (  ecDate );
           if ( period == 30 )
               firstPaymentDate.add ( Calendar.MONTH ,  2 ); // monthly
           else if ( period == 15 )
               firstPaymentDate.add ( Calendar.MONTH ,  1); // semi-monthly
           else
               firstPaymentDate.add ( Calendar.DAY_OF_MONTH ,  period * 2 ) ;// = EstimatedClosingDate + period *2

           trace( "RenewwalDate = " + firstPaymentDate.getTime () );

           deal.setRenewalDate ( firstPaymentDate.getTime () );
           deal.ejbStore ();
           return;
         }
         else
         {
           throw new Exception("Required Input = null -> deal.getEstimatedClosingDate (). or PaymentFrequencey error ");
         }
     }
     catch ( Exception e )
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "  Reason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );

     }

    deal.setRenewalDate ( DEFAULT_FAILED_DATE );
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));

    addTargetParam(new CalcParam(ClassId.DEAL, "renewalDate"));
  }

}