package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;
import com.basis100.entity.*;

// Name: Combined Total TDS
// Code: CALC -8
// 06/30/2000

public class CombinedTotalTDS extends DealCalc
{

  public CombinedTotalTDS() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 8";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) 
                       throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                             new BorrowerPK(i.getBorrowerId(), 
                                            i.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                             new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Income ==> Deal
      if(input instanceof PropertyExpense)
      {
        PropertyExpense pe = (PropertyExpense)input;
        Property p = (Property)entityCache.find(srk, 
                           new PropertyPK(pe.getPropertyId(), pe.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                           new DealPK(p.getDealId(), p.getCopyId()));
        addTarget(d);
        return;
      } // PropertyExpense ==> Deal
      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                          new BorrowerPK(l.getBorrowerId(), 
                                         l.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                          new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Liability ==> Deal

      if(input instanceof Deal)
      {
        addTarget(input);
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;

    try
    {
      double CombinedTotalTDS = 0.00;

      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));
      String rateCode = pricingProfile.getRateCode();

      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          deal.setCombinedTDS(0);
          deal.ejbStore();
          return;
        }
      }

      double totalIncomeAmount = 0.0;
      double totalTDSExpenses = 0.0;
      // ----- All Incomes Start ---------------
      Collection incomes = this.entityCache.getIncomes(deal);
      if(incomes != null)
      {
        Iterator itIn = incomes.iterator();
        Income income = null;
        while(itIn.hasNext())
        {
          income = (Income)itIn.next();
          // -------- Calcuate the Total Income Amount -------------
          //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
          //--> By Billy 02Nov2004
          if(income.getIncIncludeInTDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
          {
            totalIncomeAmount += income.getIncomeAmount() *
              DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
              income.getIncPercentInTDS() / 100.00;
          }
          //======================================================================================
          // -------- Calcuate the Total Total TDS Expenses ------------------
          if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
          {
            totalTDSExpenses -= income.getIncomeAmount() *
              DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
              income.getIncPercentInTDS() / 100.00;
          }
          trace(" TotalIncomeAmount = " + totalIncomeAmount);
          trace(" TotalTDSExpenses ( income ) = " + totalTDSExpenses);
        } // End of all Incomes
      }
      //----- All liabilities start -------------
      Collection liabilities = this.entityCache.getLiabilities(deal);
      if(liabilities != null)
      {
        Iterator itLi = liabilities.iterator();
        Liability liability = null;
        while(itLi.hasNext())
        {
          liability = (Liability)itLi.next();
          if(liability.getIncludeInTDS() && liability.getLiabilityPayOffTypeId() == 0) // non-paid off liability
          {
            // "Credit Card", "Unsecured Line of Credit" "Secured Line of Credit"
            // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
            // -------------------------------------------------------------------
            // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
            //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
            if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
            {
              totalTDSExpenses += liability.getLiabilityAmount() * liability.getPercentInTDS()
                / 100.00 * 12; // double check !!
            }
            else

            // Liab = "Closing Costs", type = "High ratio"
            if(liability.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
            {
              totalTDSExpenses += liability.getLiabilityAmount();
            }
            else
            {
              totalTDSExpenses += liability.getLiabilityMonthlyPayment() * 12 *
                liability.getPercentInTDS() / 100.00;
            }
          }
          trace("TotalTDSExpenses (+liability) = " + totalTDSExpenses);
        }
      }
      //-------------Start with Properties:Expense --------------
      Collection pe = this.entityCache.getPropertyExpenses(deal);
      if(pe != null)
      {
        Iterator itPe = pe.iterator();
        PropertyExpense expense = null;

        // ----- Get the Property Expenses -----------
        while(itPe.hasNext())
        {
          expense = (PropertyExpense)itPe.next();
          if(expense.getPeIncludeInTDS())
          {
            totalTDSExpenses += expense.getPropertyExpenseAmount() *
              DealCalcUtil.annualize(expense.getPropertyExpensePeriodId(), "PropertyExpensePeriod") *
              expense.getPePercentInTDS() / 100.00;
          }
          trace("TotalTDSExpenses ( +propertyExpenses ) = " + totalTDSExpenses);
        }
      }

      /*#DG538 allow accelerated precision
      totalTDSExpenses += (deal.getPandiPaymentAmount() + deal.getAdditionalPrincipal()) *
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"); */
      totalTDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, false);

      trace("PandI = " + deal.getPandiPaymentAmount());
      trace("AdditionAlPrincipal = " + deal.getAdditionalPrincipal());
      trace(" PaymentFrequencyId = " + deal.getPaymentFrequencyId());
      trace(" PaymentFrequency  = " +
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

      if(totalIncomeAmount > 0)
      {
        CombinedTotalTDS = totalTDSExpenses / totalIncomeAmount * 100;
        CombinedTotalTDS = Math.round(CombinedTotalTDS * 100.00) / 100.00;
      }
      else
      {
        CombinedTotalTDS = this.getMaximumCalculatedValue();

      }
      deal.setCombinedTDS(validate(CombinedTotalTDS));
      deal.ejbStore();
      return;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + target.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(target);
      logger.debug(msg);
    }

    deal.setCombinedTDS(DEFAULT_FAILED_DOUBLE);
    deal.ejbStore();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInTDS"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInTDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInTDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addInputParam(new CalcParam(ClassId.DEAL, "PandiPaymentAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    //#DG538
    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIPaymentAmountMonthly"));
    addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    //#DG538 end

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedTDS", 999.99));

  }
}
