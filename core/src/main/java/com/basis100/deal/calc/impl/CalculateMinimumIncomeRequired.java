package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import MosSystem.Mc;

//Name:  Calculate Minimum Income Required
//Code:  CALC-82
//07/10/2000

public class CalculateMinimumIncomeRequired extends DealCalc {

  public CalculateMinimumIncomeRequired() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 82";
  }

   public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
      {
      if ( input instanceof PropertyExpense )
        {
           PropertyExpense pe = (PropertyExpense) input ;
           Property p = (Property) entityCache.find ( srk, 
                       new PropertyPK( pe.getPropertyId(), pe.getCopyId()));
           Deal deal = ( Deal ) entityCache.find (srk, 
                       new DealPK(p.getDealId(), p.getCopyId()));

           addTarget ( deal );
           return ;
        } // end of PropertyExpense ==> Deal ..

      if ( input instanceof Liability )
        {
           Liability l  = (Liability) input ;
           Borrower b = (Borrower) entityCache.find( srk, 
                                 new BorrowerPK( l.getBorrowerId (), 
                                                 l.getCopyId()) );
           Deal deal = (Deal) entityCache.find ( srk, 
                                new DealPK( b.getDealId (), b.getCopyId ()) );
           addTarget ( deal );
           return;
        } // end of liability ===> Deal ..

      if ( input instanceof Deal )
        {
          addTarget ( input ) ;
          return ;
        }

      }
    catch ( Exception e)
    {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        logger.error (msg );
        throw new TargetNotFoundException (msg);
    }
  }


  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());

     Deal deal = ( Deal ) target;

     try
     {
       double totalGDSExpense = 0.0;

        // -----------------Work with Incomes----------------------
        Collection incomes = this.entityCache.getIncomes (deal);
        Iterator itIn = incomes.iterator ();
        Income income = null;
        while ( itIn.hasNext() ) // Loop through all the ** incomes
        {
            income = (Income)itIn.next();
            if  ( income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT )
              totalGDSExpense -= income.getIncomeAmount ()
                  * DealCalcUtil.annualize(income.getIncomePeriodId (), "IncomePeriod")
                    * income.getIncPercentInGDS () /100.00 ;
            trace( " totalGDSExpense = (from Income) " + totalGDSExpense);
        }
         trace( " totalGDSExpense = (from Income) " + totalGDSExpense);

         Collection propertyExpenses = this.entityCache.getPropertyExpenses (deal);
         Iterator itPE = propertyExpenses.iterator ();
         PropertyExpense propertyExpense = null;
         while ( itPE.hasNext() )
         {
            // Property Expense
            propertyExpense = ( PropertyExpense ) itPE.next ();
            if  ( propertyExpense.getPeIncludeInGDS () ) // Included in GDS =Y
              {
                totalGDSExpense += propertyExpense.getPropertyExpenseAmount ()
                  * DealCalcUtil.annualize ( propertyExpense.getPropertyExpensePeriodId (), "PropertyExpensePeriod")
                  * propertyExpense.getPePercentInGDS () /100.00;
              }
            trace("totalGDSExpense = ( proeprtyExpense) = " + totalGDSExpense );
          } // --While itPE.hasNext().......
          trace("totalGDSExpense = ( proeprtyExpense) = " + totalGDSExpense );

          Collection liabilities = this.entityCache.getLiabilities (deal) ;
          Iterator itLi = liabilities.iterator () ;
          Liability liability = null;
          while ( itLi.hasNext () )
          { liability = ( Liability) itLi.next () ;
              if ( liability.getIncludeInGDS () && liability.getLiabilityPayOffTypeId () == 0 ) // non-paid off liability
                   totalGDSExpense += liability.getLiabilityMonthlyPayment () * 12 * liability.getPercentInGDS ()/100.00;
            trace("totalGDSExpense = ( libility ) = " + totalGDSExpense );
          } // .... All liabilities ----------
          trace("totalGDSExpense = ( libility ) = " + totalGDSExpense );

          //double pandi3YrRate = DealCalcUtil.PandI3YearRate(srk, entityCache, deal, -100);
          /*#DG538 allow accelerated precision
          totalGDSExpense += (pandi3YrRate + deal.getAdditionalPrincipal ()  ) *
                 DealCalcUtil.annualize (deal.getPaymentFrequencyId (), "PaymentFrequency") ;*/
          totalGDSExpense += DealCalcUtil.PandIExpense(srk, entityCache, deal, true);

          trace( " AdditionalPrincipal = " + deal.getAdditionalPrincipal () );
          //trace( " PandI = " + pandi3YrRate);

         //----------------- Get the  Max GDS Allowed -----------------------------------------------
          double maxGDSAllowed = 0.00;
          int linesOfBusinessId = deal.getLineOfBusinessId ();
          int dealTypeId = deal.getDealTypeId ();

          if  ( linesOfBusinessId == Mc.LOB_A && dealTypeId == 1 /* High ratio */ )
             maxGDSAllowed = 0.32;
          if ( linesOfBusinessId == Mc.LOB_A && dealTypeId != 1 /* High ratio */ )
             maxGDSAllowed = 0.40;
          if ( linesOfBusinessId != Mc.LOB_A )
             maxGDSAllowed = 0.45;
         trace( "maxGDSAllowed = " + maxGDSAllowed );
         //----------------- End of Getting the  Max GDS Allowed ---------------------------------------

          double minIncomeRequired= 0.0;
          minIncomeRequired = totalGDSExpense / maxGDSAllowed ;

          minIncomeRequired = Math.round ( minIncomeRequired * 100) / 100.00;
          deal.setMinimumIncomeRequired ( validate(minIncomeRequired) );
          deal.ejbStore();
          return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
//      logger.debug (msg );
        trace (msg );
     }

     deal.setMinimumIncomeRequired (DEFAULT_FAILED_DOUBLE);
     deal.ejbStore();

  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInGDS"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "pePercentInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE , "peIncludeInGDS"));

    addInputParam(new CalcParam(ClassId.LIABILITY , "includeInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "percentInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "liabilityMonthlyPayment"));
    addInputParam(new CalcParam(ClassId.LIABILITY , "liabilityPayOffTypeId"));

    addInputParam(new CalcParam(ClassId.DEAL , "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL , "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL , "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "lineOfBusinessId"));
    //#DG538
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIUsing3YearRate"));
    //#DG538 end
    addInputParam(new CalcParam(ClassId.DEAL , "GDSTDS3YearRate"));	//#DG930

    addTargetParam(new CalcParam(ClassId.DEAL , "minimumIncomeRequired", T13D2));
  }

}
