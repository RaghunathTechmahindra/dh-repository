/*
 * @(#)PandIPaymentAmountMonthlyComponentLOC.java Jun 19, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;

/**
 * <p>
 * Title: PandIPaymentAmountMonthlyComponentLOC
 * </p>
 * <p>
 * Description:Calculates P and I payment for Line of credit components of
 * "component eligible" deals.
 * </p>
 * @author MCM Impl Team <br>
 * @Date 19-July-2008 <br>
 * @version 1.0 XS_11.9 Initial Version <br>
 *          Code Calc-1.CLOC <br>
 * @version 1.1 artf751756 1-Aug-2008 added try catch block in getTarget
 */
public class PandIPaymentAmountMonthlyComponentLOC extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.9 19-July-2008 Initial Version
     */
    public PandIPaymentAmountMonthlyComponentLOC() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 105.CLOC";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.9 19-July-2008 Initial Version
     * @version 1.1 artf751756 1-Aug-2008 added try catch block
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOC)
            {
                ComponentLOC componentLOC = (ComponentLOC) input;
                addTarget(componentLOC);
            }
            else if (classId == ClassId.COMPONENT)
            {
                Component component = (Component) input;
                // find componentLOC for the given componentId and copyId
                // MCM team Added try-catch
                try {
                    if(component.getComponentTypeId()== Mc.COMPONENT_TYPE_LOC){
                        ComponentLOC componentLOC = (ComponentLOC) entityCache
                                .find(srk, new ComponentLOCPK(component
                                        .getComponentId(), component.getCopyId()));
                        addTarget(componentLOC);
                    }
                } catch (Exception e) {
                    trace("component is being deleted componentId = " + component.getComponentId());
                }
            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.9 19-July-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        try
        {
        	ComponentLOC targetComponentLOC = (ComponentLOC) target;
            // Retrive component Object from the given target
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLOC.getComponentId(),
                            targetComponentLOC.getCopyId()));
            double interestFactor=0;
            
        	//Calculating Interest Factor starts
        	
        	interestFactor=DealCalcUtil.interestFactor(targetComponentLOC.getNetInterestRate(), DealCalcUtil.calculateNumOfPaymentsPerYear
                	(component.getInstitutionProfileId(), Mc.PAY_FREQ_MONTHLY));
        	
        	//Calculating Interest Factor ends
        	
        	//Calculating PandI payment starts
        	if(component.getRepaymentTypeId()==Mc.REPAYMENT_INTEREST_ONLY_VARIABLE || 
        			component.getRepaymentTypeId()==Mc.REPAYMENT_INTEREST_ONLY_FIXED)
        	{
        		targetComponentLOC.setPAndIPaymentAmountMonthly(
        				Math.round((targetComponentLOC.getTotalLocAmount()*interestFactor)*100d)/100d);
        		targetComponentLOC.ejbStore(); 
        		 
        	}else{
        		targetComponentLOC.setPAndIPaymentAmountMonthly(T13D2);
        		targetComponentLOC.ejbStore();
        	}
        	//Calculating PandI payment ends

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * 
     * @version 1.0 XS_11.9 19-July-2008 Initial Versionn
     * 
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "repaymentTypeId"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "totalLocAmount"));
        
        // Max pAndIPaymentAmountMonthly =999999.999999
        addTargetParam(new CalcParam(ClassId.COMPONENTLOC, "pAndIPaymentAmountMonthly",T12D6));
    }

}
