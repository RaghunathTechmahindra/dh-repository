package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

public class TotalLoanAmount extends DealCalc
{


  public TotalLoanAmount()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 43";
  }



  public Collection getTargets()
  {
    return this.targets;
  }

  
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
     try
     {
        addTarget( input ) ;
     }
     catch ( Exception e )
     {
        String msg = "Fail to get Targets" + this.getClass().getName();
        msg += ":[Input]:" + input.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@input: " + this.getDoCalcErrorDetail ( input );
        logger.error (msg );
        throw new TargetNotFoundException (msg);
     }
  }



  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal d = (Deal)target;

     try
     {
       double val = d.getNetLoanAmount();

       String miupfront = d.getMIUpfront () ;
       if  ( miupfront != null )
           if(  miupfront.equalsIgnoreCase("N") )
              val += d.getMIPremiumAmount();
       val = Math.round ( val * 100 ) /100.00 ; 
       d.setTotalLoanAmount(validate(val));
       d.ejbStore();
       return;
     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

     d.setTotalLoanAmount(this.DEFAULT_FAILED_DOUBLE);
     d.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }


  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();


    addInputParam(new CalcParam(ClassId.DEAL, "netLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "MIUpfront"));
    addInputParam(new CalcParam(ClassId.DEAL, "MIPremiumAmount"));


    addTargetParam(new CalcParam(ClassId.DEAL, "totalLoanAmount", T13D2 ));
  }



}
