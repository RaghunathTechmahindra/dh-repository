package com.basis100.deal.calc.impl;

/**
 * 06/Nov/2006 DVG #DG538 IBC#1517  BNC UAT EXPERT/EXPRESS -  GDS/TDS
 */

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;

import MosSystem.Mc;

//Name: Combined Total GDS 3 Year
//Code: CALC-6
//06/29/2000

public class CombinedTotalGDS3Years extends DealCalc
{

  public CombinedTotalGDS3Years() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 6";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Income)
      {
        Income i = (Income)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                          new BorrowerPK(i.getBorrowerId(), 
                                         i.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                         new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Income ==> Deal
      if(input instanceof PropertyExpense)
      {
        PropertyExpense pe = (PropertyExpense)input;
        Property p = (Property)entityCache.find(srk, 
                           new PropertyPK(pe.getPropertyId(), pe.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                      new DealPK(p.getDealId(), p.getCopyId()));
        addTarget(d);
        return;
      } // PropertyExpense ==> Deal
      if(input instanceof Liability)
      {
        Liability l = (Liability)input;
        Borrower b = (Borrower)entityCache.find(srk, 
                          new BorrowerPK(l.getBorrowerId(), 
                                         l.getCopyId()));
        Deal d = (Deal)entityCache.find(srk, 
                          new DealPK(b.getDealId(), b.getCopyId()));
        addTarget(d);
        return;
      } // Liability ==> Deal

      if(input instanceof Deal)
      {
        addTarget(input);
      }
    }
    catch(Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }
  }

  public void doCalc(Object target) throws Exception
  {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;
    //--DJ_Ticket841--start--//
    boolean isUseBorrowerOnlyOnThreeYearGDSTDS = (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                        "com.basis100.calc.useborroweronlyonthreeyearGDSTDS", "N")).equals("Y");
    trace( " isUseBorrowerOnlyOnThreeYearGDSTDS = " + isUseBorrowerOnlyOnThreeYearGDSTDS);

    if (isUseBorrowerOnlyOnThreeYearGDSTDS == false)
    {
       calcRegularCalc6(deal);
    }
    else if (isUseBorrowerOnlyOnThreeYearGDSTDS == true)
    {
       calcBorrowerOnlyCalc6(deal);
    }
    //--DJ_Ticket841--end--//
  }

  //--DJ_Ticket841--start--//
  private void calcRegularCalc6(Deal deal)
  {
//	  int originalRepaymentTypeId = deal.getRepaymentTypeId();
//      int originalAmortizationTerm = deal.getAmortizationTerm();
      
    try
    {
      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));

      double dbGDSRatio3Year = 0;
      String rateCode = pricingProfile.getRateCode();
      
      trace("RateCode = " + rateCode + ", netRate = " + deal.getNetInterestRate());
      
      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          deal.setCombinedGDS3Year(0);
          deal.ejbStore();
          return;
        }
      }

      double dbTotalAnnualIncome = 0;
      double dbTotalGDSExpenses = 0;

      // -----------------Work with Incomes----------------------
      Collection incomes = this.entityCache.getIncomes(deal);
      Iterator itIn = incomes.iterator();
      Income income = null;
      while(itIn.hasNext()) // Loop through all the ** incomes
      {
        income = (Income)itIn.next();
        // if Included in GDS
        //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
        //--> By Billy 02Nov2004
        if(income.getIncIncludeInGDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
        {
          dbTotalAnnualIncome = dbTotalAnnualIncome + income.getIncomeAmount()
            * DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod")
            * income.getIncPercentInGDS() / 100.00;
        }
        //==============================================================================
        if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
        {
          dbTotalGDSExpenses = dbTotalGDSExpenses - income.getIncomeAmount()
            * DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod")
            * income.getIncPercentInGDS() / 100.00;
        }
        trace("IncomePeriod = " +
          DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod"));
        trace("totalGDSExpenses = " + dbTotalGDSExpenses);
      }
      // ------------------Work with Liabilities---------------------
      Collection liabilities = this.entityCache.getLiabilities(deal);
      Iterator itLi = liabilities.iterator();
      Liability liability = null;
      while(itLi.hasNext()) //Loop through all the ** liabilities
      {
        liability = (Liability)itLi.next();
        // Include in GST and Libility Payoff Type is Blank, ( id=0 )
        if(liability.getIncludeInGDS() && (liability.getLiabilityPayOffTypeId() == 0))
        {
          // "Credit Card", "Unsecured Line of Credit" "Secured Line of Credit"
          // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
          // -------------------------------------------------------------------
          // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
          //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
          if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
          {
            dbTotalGDSExpenses += liability.getLiabilityAmount() * liability.getPercentInGDS() / 100.00 * 12;
          }
          else // Liab = "Closing Costs", type = "High ratio"
          if(liability.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
          {
            dbTotalGDSExpenses += liability.getLiabilityAmount();
          }
          else
          {
            dbTotalGDSExpenses += liability.getLiabilityMonthlyPayment() * 12 *
              liability.getPercentInGDS() / 100.00;
          }
        }
        trace(" liability.liabilityAmount = " + liability.getLiabilityAmount());
        trace(" totalGDSExpenses = " + dbTotalGDSExpenses);
      }
      trace("TotalGDSExpenses = " + dbTotalGDSExpenses);

      Collection pe = this.entityCache.getPropertyExpenses(deal);
      Iterator itPe = pe.iterator();

      // ----- Get the Property Expenses -----------
      while(itPe.hasNext())
      { //Loop thourgh all the Property Expenses
        PropertyExpense propertyExpense = (PropertyExpense)itPe.next();
        if(propertyExpense.getPeIncludeInGDS())
        {
          dbTotalGDSExpenses = dbTotalGDSExpenses
            + propertyExpense.getPropertyExpenseAmount()
            * DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(), "PropertyExpensePeriod")
            * propertyExpense.getPePercentInGDS() / 100.00;
        }
        trace(" PropertyExpensePeriod = " +
          DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(), "PropertyExpensePeriod"));
        trace(" totalGDSExpenses = " + dbTotalGDSExpenses);
      } // End of Property Expense
      
      //handling MOSSYS prop com.basis100.calc.forcepandifor3year
      /*boolean isForcePandIFor3Year = (PropertiesCache.getInstance().getProperty(
              "com.basis100.calc.forcepandifor3year", "N")).equals("Y");
      
      trace("isForcePandIFor3Year = " + isForcePandIFor3Year + ", ProdTypeId = " + iProdTypeId);
      trace("Original RepaymentTypeId = " + originalRepaymentTypeId + ", AmortizationTerm = " + originalAmortizationTerm);

      if(isForcePandIFor3Year && iProdTypeId == Mc.PRODUCT_TYPE_SECURED_LOC)
      {
    	  deal.setRepaymentTypeId(0);
    	  deal.setAmortizationTerm(300);
    	  trace("forcepandifor3year=Y and ProdTypeId=LOC, set RepaymentTypeId:0, AmortizationTerm:300");
      }*/
//    END of handling MOSSYS prop com.basis100.calc.forcepandifor3year
      
      /*#DG538 allow accelerated precision
      dbTotalGDSExpenses +=
        (DealCalcUtil.PandI3YearRate(srk, entityCache, deal, -100) + deal.getAdditionalPrincipal())
        * DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");*/
      dbTotalGDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, true);

      trace("PandI = " + deal.getPandiPaymentAmount() + ", NetInterestRate = " + deal.getNetInterestRate());
      trace("PaymentFrequency = " + DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

      if(dbTotalAnnualIncome != 0)
      {
        dbGDSRatio3Year = dbTotalGDSExpenses / dbTotalAnnualIncome * 100;
        dbGDSRatio3Year = Math.round(dbGDSRatio3Year * 100.00) / 100.00;
      }
      else
      {
        dbGDSRatio3Year = this.getMaximumCalculatedValue();
      }

      logger.trace(">>>Returning from Calc 6-1., dbGDSRatio3Year = " + dbGDSRatio3Year);
      deal.setCombinedGDS3Year(validate(dbGDSRatio3Year));
//      deal.setRepaymentTypeId(originalRepaymentTypeId);
//	  deal.setAmortizationTerm(originalAmortizationTerm);
      deal.ejbStore();
      return;

    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + this.getClass().getName();
      msg += "\nReason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(this.getClass().getName());
      logger.debug(msg);
    }

    // if failed set to default double zero
    try
    {
    deal.setCombinedGDS3Year(DEFAULT_FAILED_DOUBLE);
//    deal.setRepaymentTypeId(originalRepaymentTypeId);
//    deal.setAmortizationTerm(originalAmortizationTerm);
    deal.ejbStore();
    }
    catch(Exception ex)
    {
      //String msg = "Default Value Double Zero is set to: " + this.getClass().getName();
      logger.error(ex);
    }
  }

  private void calcBorrowerOnlyCalc6(Deal deal)
  {
//	  int originalRepaymentTypeId = deal.getRepaymentTypeId();
//      int originalAmortizationTerm = deal.getAmortizationTerm();
    try
    {
      // the deal.pricingProfileId here is actually the pricingRateInventory.pricingRateInventoryId
      PricingRateInventory pricingRateInventory = new PricingRateInventory(srk, deal.getPricingProfileId());
      int ppId = pricingRateInventory.getPricingProfileID();
      PricingProfile pricingProfile = (PricingProfile)this.entityCache.find(srk, new PricingProfilePK(ppId));

      //double dbGDSRatio3Year = 0;
      double GDSRatioBorrowerOnly = 0.0;

      String rateCode = pricingProfile.getRateCode();
      if(rateCode != null)
      {
        if(rateCode.equalsIgnoreCase("UNCNF"))
        {
          deal.setCombinedGDS3Year(0);
          deal.ejbStore();
          return;
        }
    }

    Collection borrowers = this.entityCache.getBorrowers(deal);
    if(borrowers != null)
    {
      //double dbTotalAnnualIncome = 0;
      //double dbTotalGDSExpenses = 0;

      double totalAnnualIncome = 0.0;
      double totalGDSExpenses = 0.0;

      Iterator itBO = borrowers.iterator();

      Borrower borrower = null;
      while(itBO.hasNext()) // Loop through all the borrowers
      {
        borrower = (Borrower)itBO.next();

        trace(": " + borrower.getBorrowerId());
        trace("BorrowerLastName to GDS calc (Borrower Only): " + borrower.getBorrowerLastName());
        trace("BorrowerFirstName to GDS calc (Borrower Only): " + borrower.getBorrowerFirstName());

        if(borrower.getBorrowerTypeId() != Mc.BT_GUARANTOR )
        {
          // ----- All the Incomes -------------
          Collection incomes = this.entityCache.getIncomes(borrower);
          Iterator itIn = incomes.iterator();
          while(itIn.hasNext())
          { //---------Loop through all the incomes, and get the Total Annual Income ---------
            Income income = (Income)itIn.next();
            //--> Ticket#677 : INCOME_RENTAL_SUBTRACT income should not add to total income
            //--> By Billy 02Nov2004
            if(income.getIncIncludeInGDS() && income.getIncomeTypeId() != Mc.INCOME_RENTAL_SUBTRACT)
            {
              totalAnnualIncome += income.getIncomeAmount() *
                DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
                income.getIncPercentInGDS() / 100.00;
              // -------- Get the Total Total GDS Expenses ------------------
            }
            //==============================================================================
            if(income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT)
            {
              totalGDSExpenses = totalGDSExpenses - income.getIncomeAmount() *
                DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod") *
                income.getIncPercentInGDS() / 100.00;
            }
            trace(" IncomePeriod = " + DealCalcUtil.annualize(income.getIncomePeriodId(), "IncomePeriod"));
            trace("totalGDSExpenses = " + totalGDSExpenses);
          }
          // ----- All the Liabilities  -------------
          Collection liabilities = this.entityCache.getLiabilities(borrower);
          Iterator itLi = liabilities.iterator();
          while(itLi.hasNext()) //Loop through all the ** liabilities
          {
            Liability liability = (Liability)itLi.next();
            // Include in GST and Libility Payoff Type is Blank, ( id=0 )
            if(liability.getIncludeInGDS() && (liability.getLiabilityPayOffTypeId() == 0))
            {
              // "Credit Card", "Unsecured Line of Credit" "Secured Line of Credit"
              // Added "Student Loan" (7) as required by Product -- Billy 05Sept2001
              // -------------------------------------------------------------------
              // Modified to check if liabilityPaymentQualifier == 'V' -- Billy 13Nov2001
              //if (oneOf(liability.getLiabilityTypeId (), 2, 10, 13, 7))
              if(liability.getLiabilityPaymentQualifier() != null && liability.getLiabilityPaymentQualifier().equals("V"))
              {
                totalGDSExpenses += liability.getLiabilityAmount() * liability.getPercentInGDS() / 100.00 * 12;
              }
              else

              // Liab = "Closing Costs", type = "High ratio"
              if(liability.getLiabilityTypeId() == 14 && deal.getDealTypeId() == 1)
              {
                totalGDSExpenses += liability.getLiabilityAmount();
              }
              else
              {
                totalGDSExpenses += liability.getLiabilityMonthlyPayment() * 12 *
                  liability.getPercentInGDS() / 100.00;
              }
            }
            trace("liability.getLiabilityAmount () = " + liability.getLiabilityAmount());
            trace("totalGDSExpenses = " + totalGDSExpenses);
          }

        } // --- End of borrower Type != Guarantor ......
      } // ---------- End of Borrower.hasNext()................

      Collection pe = this.entityCache.getPropertyExpenses(deal);
      Iterator itPe = pe.iterator();

      while(itPe.hasNext())
      { //Loop thourgh all the Property Expenses
        PropertyExpense propertyExpense = (PropertyExpense)itPe.next();
        if(propertyExpense.getPeIncludeInGDS())
        {
          totalGDSExpenses = totalGDSExpenses + propertyExpense.getPropertyExpenseAmount() *
            DealCalcUtil.annualize(propertyExpense.getPropertyExpensePeriodId(), "PropertyExpensePeriod")
            * propertyExpense.getPePercentInGDS() / 100.00;
        }
        trace("propertyExpense = " + totalGDSExpenses);
      }
      
      //handling MOSSYS prop com.basis100.calc.forcepandifor3year
     /* boolean isForcePandIFor3Year = (PropertiesCache.getInstance().getProperty(
              "com.basis100.calc.forcepandifor3year", "N")).equals("Y");
      
      trace("isForcePandIFor3Year = " + isForcePandIFor3Year + ", ProdTypeId = " + iProdTypeId);
      trace("Original RepaymentTypeId = " + originalRepaymentTypeId + ", AmortizationTerm = " + originalAmortizationTerm);

      if(isForcePandIFor3Year && iProdTypeId == Mc.PRODUCT_TYPE_SECURED_LOC)
      {
    	  deal.setRepaymentTypeId(0);
    	  deal.setAmortizationTerm(300);
    	  trace("forcepandifor3year=Y and ProdTypeId=LOC, set RepaymentTypeId:0, AmortizationTerm:300");
      }*/
//    END of handling MOSSYS prop com.basis100.calc.forcepandifor3year
      
     /*#DG538 allow accelerated precision
     totalGDSExpenses +=
       (DealCalcUtil.PandI3YearRate(srk, entityCache, deal, -100) + deal.getAdditionalPrincipal())
       * DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency");*/
      totalGDSExpenses += DealCalcUtil.PandIExpense(srk, entityCache, deal, true);
      

      trace("PandI = " + deal.getPandiPaymentAmount());
      trace(" AdditionalPrincipal = " + deal.getAdditionalPrincipal());
      trace("PaymentFrequency = " +
        DealCalcUtil.annualize(deal.getPaymentFrequencyId(), "PaymentFrequency"));

     if(totalAnnualIncome != 0)
     {
       GDSRatioBorrowerOnly = totalGDSExpenses / totalAnnualIncome * 100;
       GDSRatioBorrowerOnly = Math.round(GDSRatioBorrowerOnly * 100.00) / 100.00;
     }
     else
     {
       GDSRatioBorrowerOnly = this.getMaximumCalculatedValue();
     }
     

     logger.trace(">>>Returning from Calc 6-2, calcBorrowerOnlyCalc6(), GDSRatioBorrowerOnly = " + GDSRatioBorrowerOnly);
     
     deal.setCombinedGDS3Year(validate(GDSRatioBorrowerOnly));
//     deal.setRepaymentTypeId(originalRepaymentTypeId);
//     deal.setAmortizationTerm(originalAmortizationTerm);
     deal.ejbStore();
     return;
    } // end if borrowers != null
  }
  catch(Exception e)
  {
    String msg = "Benign Failure in Calculation" + this.getClass().getName();
    msg += ":[Target]:" + this.getClass().getName();
    msg += "\nReason: " + e.getMessage();
    msg += "@Target: " + this.getDoCalcErrorDetail(this.getClass().getName());
    logger.debug(msg);
  }

  // if failed set to default double zero
  try
  {
  deal.setCombinedGDS3Year(DEFAULT_FAILED_DOUBLE);
//  deal.setRepaymentTypeId(originalRepaymentTypeId);
//  deal.setAmortizationTerm(originalAmortizationTerm);
  deal.ejbStore();
  }
  catch(Exception ex)
  {
    //String msg = "Default Value Double Zero is set to: " + this.getClass().getName();
    logger.error(ex);
  }
}
//--DJ_Ticket841--end--//

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incPercentInGDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomeTypeId"));

    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpenseAmount"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "propertyExpensePeriodId"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "pePercentInGDS"));
    addInputParam(new CalcParam(ClassId.PROPERTYEXPENSE, "peIncludeInGDS"));

    addInputParam(new CalcParam(ClassId.LIABILITY, "includeInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "percentInGDS"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityAmount"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityPayOffTypeId"));
    addInputParam(new CalcParam(ClassId.LIABILITY, "liabilityMonthlyPayment"));

    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "additionalPrincipal"));
    addInputParam(new CalcParam(ClassId.DEAL, "paymentFrequencyId"));
    //4.3GR Jan 26, 2010, deleting: don't need discount because it's looking at PAndIUsing3YearRate
    //addInputParam(new CalcParam(ClassId.DEAL, "discount"));
    addInputParam(new CalcParam(ClassId.DEAL, "pricingProfileId"));
    addInputParam(new CalcParam(ClassId.DEAL, "dealTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "PAndIUsing3YearRate"));  //#DG538
    addInputParam(new CalcParam(ClassId.DEAL , "GDSTDS3YearRate"));	//#DG930

    addTargetParam(new CalcParam(ClassId.DEAL, "combinedGDS3Year", 999.99));
  }

}
