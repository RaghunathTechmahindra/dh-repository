package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*; 
import com.basis100.deal.calc.*;

public class BorrowerTotalIncome extends DealCalc
{

  public BorrowerTotalIncome() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 53";
  }


  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {

     try
     {
         Income a = (Income)input;
         Borrower bt = (Borrower) entityCache.find ( srk, 
                                new BorrowerPK(a.getBorrowerId (), 
                                               a.getCopyId ()));
         addTarget(bt);
     }
     catch(Exception e)
     {
          String msg = "Fail to get Targets" + this.getClass().getName();
          msg += ":[Input]:" + input.getClass ().getName () ;
          msg += "\nReason: " + e.getMessage();
          msg += "@input: " + this.getDoCalcErrorDetail ( input );
          logger.error (msg );
          throw new TargetNotFoundException (msg);
     }
  }



  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     double result = 0;

     Borrower bt = (Borrower)target;

     try
     {
       Collection incomes = this.entityCache.getIncomes(bt);

       Iterator in = incomes.iterator();
       Income current = null;

       while(in.hasNext())
       {
         current = (Income)in.next();
         if(current.getIncIncludeInGDS() == true || current.getIncIncludeInTDS() == true){
           result += current.getIncomeAmount() * DealCalcUtil.annualize (current.getIncomePeriodId () , "IncomePeriod") ;
           trace( "totalIncomeAmount = " + result );
         }
         else{
         trace("Income.getIncIncludeInGDS() = " + current.getIncIncludeInGDS() + ", "
                       + "Income.getIncIncludeInTDS = " + current.getIncIncludeInTDS()
                       + "\nNo need to caculate borrower total income.");
         }


       }
       result = Math.round ( result * 100.00 ) / 100.00; 
       bt.setTotalIncomeAmount(validate(result));
       bt.ejbStore();
       return;

     }
     catch(Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }

      bt.setTotalIncomeAmount(DEFAULT_FAILED_DOUBLE);
      bt.ejbStore();
  }


  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams()throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.INCOME, "incomeAmount"));
    addInputParam(new CalcParam(ClassId.INCOME, "incomePeriodId"));
    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInTDS"));
    addInputParam(new CalcParam(ClassId.INCOME, "incIncludeInGDS"));

    addTargetParam(new CalcParam(ClassId.BORROWER, "totalIncomeAmount", T13D2));
  }



}
