package com.basis100.deal.calc;

import java.util.*;
import java.io.*;
import com.basis100.util.file.*;
import com.basis100.deal.calc.impl.*;
import com.basis100.deal.entity.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import java.lang.Object;
import com.basis100.deal.pk.*;
/**
 * Class for storing cache of entities used in calculations 
 * @version 1.0 Initial version
 * @Version 1.1 XS_11.11<br>
 * @Date 10-Jun-2008 <br>
 * Author: MCM Impl Team Change: <br>
 * Changes:   
 *   - Added getComponents() method for getting Component associated to the Deal
 *   - changed find() method for getting Components, ComponentLOC, ComponentMortgage, LenderProfile
 *   - Modified searchCache() - Added a check for ComponentPk
 *   - Modified CalcEntityCache() - Added a new HashMap for components
 * @Version 1.2 XS_11.5<br>
 * @Date 25-Jun-2008 <br>
 * Author: MCM Impl Team Change: <br>
 * Change: 
 *    - changed find() method for getting ComponentLoan
 * @Version 1.3 <br>
 * @Date 11-Jul-2008 <br>
 * Author: MCM Impl Team Change: <br>
 * Change: 
 *    - changed  getComponents(..)
 * @Version 1.4 XS_11.16<br>
 * 
 * @Date 24-Jul-2008 <br>
 * Author: MCM Impl Team Change: <br>
 * Change: 
 *    -  find(SessionResourceKit srk, IEntityBeanPK pk ) - Added if conditon for ComponentSummary
 *
 * @Version 1.5 artf751756<br>
 * @Date 30-Jul-2008 <br>
 * Author: MCM Impl Team Change: <br>
 * Change: 
 *    -  find(SessionResourceKit srk, IEntityBeanPK pk ) - Added if conditon for CreditCard
 *    -  find(SessionResourceKit srk, IEntityBeanPK pk ) - Added if conditon for OverDraft
 * 
 **/
public class CalcEntityCache
{

  private HashMap liabilities;
  private HashMap incomes;
  private HashMap assets;
  private HashMap borrowers;
  private HashMap properties;
  private HashMap propertyExpenses;
  private HashMap downPaymentSources;
  private HashMap pricingProfiles;
  private HashMap dealFees;
  private HashMap doctracks;
  private HashMap escrowPayments;

  private HashMap entities;
  private HashMap dealMap;

  //--DJ_CR136--23Nov2004--start--//
  private HashMap insureOnlyApplicants;
  //--DJ_CR136--23Nov2004--end--//
  
  //Added for XS_11.11 10-Jun-2008
  private HashMap components;


  private CalcMonitor dcm;

  private Deal deal;

  protected CalcEntityCache(CalcMonitor dcm)
  {
     liabilities  = new HashMap();
     incomes = new HashMap();
     assets = new HashMap();
     borrowers = new HashMap();
     properties = new HashMap();
     propertyExpenses = new HashMap();
     downPaymentSources = new HashMap();
     pricingProfiles = new HashMap();
     dealFees = new HashMap();
     doctracks = new HashMap();
     escrowPayments = new HashMap();

     //--DJ_CR136--23Nov2004--start--//
     insureOnlyApplicants = new HashMap();
     //--DJ_CR136--23Nov2004--end--//     
     entities = new HashMap();
     //Added for XS_11.11 10-Jun-2008
     components = new HashMap();
     this.dcm = dcm;
  }

  public Collection getBorrowers(DealEntity parent)throws Exception
  {

    Collection out = null;

    Deal d = (Deal)parent;

    String key = getKey(parent.getPk());

    out = (Collection)borrowers.get(key);

    if(out == null)
    {
      out = d.getBorrowers();
      borrowers.put(key, out);

      addToEntities(out);
    }

    return out;
  }

  public Collection getProperties(DealEntity parent)throws Exception
  {
    Collection out = null;

    Deal d = (Deal)parent;

    String key = getKey(parent.getPk());

    out = (Collection)properties.get(key);

    if(out == null)
    {
      out = d.getProperties();
      properties.put(key, out);
      addToEntities(out);
    }

    return out;
  }

  public Collection getLiabilities(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)liabilities.get(key);

      if(out == null)
      {
        out = d.getLiabilities();

        liabilities.put(key, out);
        addToEntities(out);
      }

      return out;
    }
    else if(pcid == ClassId.BORROWER)
    {
      Borrower b = (Borrower)parent;

      out = (Collection)liabilities.get(key);

      if(out == null)
      {
        out = b.getLiabilities();

        liabilities.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }


  public Collection getAssets(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)assets.get(key);

      if(out == null)
      {
        out = d.getAssets();

        assets.put(key, out);
        addToEntities(out);
      }

      return out;
    }
    else if(pcid == ClassId.BORROWER)
    {
      Borrower b = (Borrower)parent;

      out = (Collection)assets.get(key);

      if(out == null)
      {
        out = b.getAssets();

        assets.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }

  public Collection getIncomes(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)incomes.get(key);

      if(out == null)
      {
        out = d.getIncomes();

        incomes.put(key, out);
        addToEntities(out);
      }

      return out;
    }
    else if(pcid == ClassId.BORROWER)
    {
      Borrower b = (Borrower)parent;

      out = (Collection)incomes.get(key);

      if(out == null)
      {
        out = b.getIncomes();

        incomes.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }

  public Collection getPropertyExpenses(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)propertyExpenses.get(key);

      if(out == null)
      {
        out = d.getPropertyExpenses();

        propertyExpenses.put(key, out);
        addToEntities(out);
      }

      return out;
    }
    else if(pcid == ClassId.PROPERTY)
    {
      Property p = (Property)parent;

      out = (Collection)propertyExpenses.get(key);

      if(out == null)
      {
        out = p.getPropertyExpenses();

        propertyExpenses.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }


  public Collection getDownPaymentSources(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)downPaymentSources.get(key);

      if(out == null)
      {
        out = d.getDownPaymentSources();

        downPaymentSources.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }

  //--DJ_CR136--23Nov2004--start--//
  public Collection getInsureOnlyApplicants(DealEntity parent)throws Exception
{
  int pcid = parent.getClassId();
  Collection out = null;

  String key = getKey(parent.getPk());

  if(pcid == ClassId.DEAL)
  {
    Deal d = (Deal)parent;

    out = (Collection)insureOnlyApplicants.get(key);

    if(out == null)
    {
      out = d.getInsureOnlyApplicants();

      insureOnlyApplicants.put(key, out);
      addToEntities(out);
    }
    return out;
  }
  return new ArrayList();
}

  public Collection getDealFees(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)dealFees.get(key);

      if(out == null)
      {
        out = d.getDealFees();

        dealFees.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }

  public Collection getEscrowPayments(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)escrowPayments.get(key);

      if(out == null)
      {
        out = d.getEscrowPayments();

        escrowPayments.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }

  public Collection getCommitmentDocumentTracking(DealEntity parent)throws Exception
  {
    int pcid = parent.getClassId();
    Collection out = null;

    String key = getKey(parent.getPk());

    if(pcid == ClassId.DEAL)
    {
      Deal d = (Deal)parent;

      out = (Collection)doctracks.get(key);

      if(out == null)
      {
        out = d.getDocumentTracks();

        doctracks.put(key, out);
        addToEntities(out);
      }

      return out;
    }

    return new ArrayList();
  }

  public Collection getPricingProfiles ( SessionResourceKit srk , String rateCode ) throws Exception
  {
    Collection out = (Collection) pricingProfiles.get ( rateCode );

    if ( out == null )
    {
        PricingProfile pricingProfile = new PricingProfile(srk);
        out = pricingProfile.findByRateCode ( rateCode ) ;
        pricingProfiles.put ( rateCode , out ) ;
        addToEntities ( out );
    }
    return out ;
  }
  
  /**
	 * <p>
	 * Description: Retrives the Components associated to the Deal
	 * </p>
	 * @author MCM Impl Team
	 * @version 1.0 10-Jun-2008 XS_11.11 Initial Version
   * @version 1.1 11-Jul-2008 Moved listOfComponents to outside if block  
	 * @param parent -
	 *            Parent Entity
	 * @param srk -
	 *            stores the session level information.
	 * @return a collection of Components
	 * @throws Exception
	 */
	public Collection getComponents(DealEntity parent,
			SessionResourceKit srk) throws Exception {
		int parentClassId = parent.getClassId();
		Collection listOfComponents = null;
		String key = getKey(parent.getPk());
		if (parentClassId == ClassId.DEAL) {
			Deal deal = (Deal) parent;
			listOfComponents = (Collection) components.get(key);
			if (listOfComponents == null) {
				listOfComponents = deal.getComponents();
				components.put(key, listOfComponents);
				addToEntities(listOfComponents);			
			}
      return listOfComponents;
		}
		return new ArrayList();

	}  
  public void addToEntities(Collection toAdd)
  {
    if(toAdd == null || toAdd.isEmpty())
     return;

    Iterator it = toAdd.iterator();
    DealEntity de = null;
    IEntityBeanPK pk = null;

    while(it.hasNext())
    {
      de = (DealEntity)it.next();
      pk = de.getPk();

      if(pk != null && de != null)
      {
       entities.put(pk,de);
       de.dcm = this.dcm;
      }
    }
  }

  /**
   *  Finds the DealEntity identified by the provided primary key if resident in this cache.<br/>
   *  If one is not found a new Instance is created and synchonized with the <br/>
   *  persistent store and added to this cache.
   *  @version 1.0 Initial Version
   *  @version 1.1 XS_11.11 & XS_11.6 Added if conditon for ComponentPK,ComponentMortgagePK,ComponentLOCPK, LenderProfilePK
   *  @version 1.2 XS_11.5 25-Jun-2008 Added if conditon for ComponentLoanPK
   *  @version 1.3 XS_11.16 24-Jul-2008 Added if conditon for ComponentSummary   *  
   *  @return the found or created instance of DealEntity subclass
   *  
   */
  public DealEntity find(SessionResourceKit srk, IEntityBeanPK pk ) throws Exception
  {
    try
    {
        DealEntity dealEntity = (DealEntity) entities.get ( pk );

        if ( dealEntity == null )
        {

            if ( pk instanceof AssetPK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new Asset( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof BorrowerPK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new Borrower( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof BridgePK)
                dealEntity = new Bridge( srk, dcm, pk.getId (), pk.getCopyId() );
            else if ( pk instanceof ConditionPK)
                dealEntity = new Condition( srk, pk.getId ());
            else if ( pk instanceof DealPK)
               dealEntity = new Deal( srk, dcm, pk.getId (), pk.getCopyId ());
            else if ( pk instanceof DealFeePK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new DealFee( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof DocumentTrackingPK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new DocumentTracking( srk,  pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof DownPaymentSourcePK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new DownPaymentSource( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            //--DJ_CR136--23Nov2004--start--//
            else if ( pk instanceof InsureOnlyApplicantPK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new InsureOnlyApplicant( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            //--DJ_CR136--23Nov2004--end--//
            else if ( pk instanceof EscrowPaymentPK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new EscrowPayment( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof FeePK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new Fee( srk, dcm, pk.getId () );
                }
            }
            else if ( pk instanceof IncomePK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new Income( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof InstitutionProfilePK)
                dealEntity = new InstitutionProfile( srk,  pk.getId ());
            else if ( pk instanceof LiabilityPK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                   dealEntity = new Liability( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof LiabilityTypePK )
                dealEntity = new LiabilityType( srk, pk.getId () );
            else if ( pk instanceof MtgProdPK)
                dealEntity = new MtgProd ( srk, dcm, pk.getId ());
            else if ( pk instanceof PricingProfilePK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new PricingProfile( srk,  pk.getId () );
                }
            }
            else if ( pk instanceof PropertyPK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new Property( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            else if ( pk instanceof PropertyExpensePK)
            {
                dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new PropertyExpense( srk, dcm, pk.getId (), pk.getCopyId ());
                }
            }
            
            
            /***************MCM Impl team changes starts - XS_11.11 & XS_11.6*******************/
            /*
             * 10-Jun-2008 - Added condition for ComponentPK, ComponentLOCPK, ComponentMortgagePK,LenderProfilePK 
             */

            else if (pk instanceof ComponentPK){
            	dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                    //FXP23208, MCM 4.0, Oct 30, 2008 - use silentMode in case of deleting component.
                    dealEntity = new Component(srk, dcm);
                    dealEntity.setSilentMode(true);
                    dealEntity = ((Component)dealEntity).findByPrimaryKey((ComponentPK)pk);
                }
    		}
            else if (pk instanceof ComponentLOCPK){
            	dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                    //FXP23208, MCM 4.0, Oct 30, 2008 - use silentMode in case of deleting component.
                    //dealEntity = new ComponentLOC( srk, dcm,  pk.getId (), pk.getCopyId ());
                    dealEntity = new ComponentLOC(srk, dcm);
                    dealEntity.setSilentMode(true);
                    dealEntity = ((ComponentLOC)dealEntity).findByPrimaryKey((ComponentLOCPK)pk);
                }
    		}
            else if (pk instanceof ComponentMortgagePK){
            	dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                    //FXP23208, MCM 4.0, Oct 30, 2008 - use silentMode in case of deleting component.
                    //dealEntity = new ComponentMortgage( srk, dcm ,  pk.getId (), pk.getCopyId ());
                    dealEntity = new ComponentMortgage(srk, dcm);
                    dealEntity.setSilentMode(true);
                    dealEntity = ((ComponentMortgage)dealEntity).findByPrimaryKey((ComponentMortgagePK)pk);
                }
    		}
            else if(pk instanceof LenderProfilePK){
            	dealEntity = searchCache(pk);

                if(dealEntity == null)
                {
                  dealEntity = new LenderProfile( srk, pk.getId ());
                }
            }
            /***************MCM Impl team changes ends - XS_11.11 & XS_11.6*******************/
            /***************MCM Impl team changes starts - XS_11.5 ******************/
            else if (pk instanceof ComponentLoanPK){
                dealEntity = searchCache(pk);

                  if(dealEntity == null)
                  {
                    
                      //FXP23208, MCM 4.0, Oct 30, 2008 - use silentMode in case of deleting component.
                      //dealEntity = new ComponentLoan( srk, dcm ,  pk.getId (), pk.getCopyId ());
                      dealEntity = new ComponentLoan(srk, dcm);
                      dealEntity.setSilentMode(true);
                      dealEntity = ((ComponentLoan)dealEntity).findByPrimaryKey((ComponentLoanPK)pk);
                  }
            }        
         /***************MCM Impl team changes ends - XS_11.5******************/
         
            /***************MCM Impl team changes starts - artf751756 ******************/
            else if (pk instanceof ComponentCreditCardPK){
                dealEntity = searchCache(pk);

                  if(dealEntity == null)
                  {
                      //FXP23208, MCM 4.0, Oct 30, 2008 - use silentMode in case of deleting component.
                      //dealEntity = new ComponentCreditCard( srk, dcm ,  pk.getId (), pk.getCopyId ());
                      dealEntity = new ComponentCreditCard(srk, dcm);
                      dealEntity.setSilentMode(true);
                      dealEntity = ((ComponentCreditCard)dealEntity).findByPrimaryKey((ComponentCreditCardPK)pk);
                  }
            }
            else if (pk instanceof ComponentOverdraftPK){
                dealEntity = searchCache(pk);

                  if(dealEntity == null)
                  {
                      //FXP23208, MCM 4.0, Oct 30, 2008 - use silentMode in case of deleting component.
                      //dealEntity = new ComponentOverdraft( srk, dcm ,  pk.getId (), pk.getCopyId ());
                      dealEntity = new ComponentOverdraft(srk, dcm);
                      dealEntity.setSilentMode(true);
                      dealEntity = ((ComponentOverdraft)dealEntity).findByPrimaryKey((ComponentOverdraftPK)pk);
                  }
            }
            
         /***************MCM Impl team changes ends - artf751756******************/
            
            /***************MCM Impl team changes starts - XS_11.16 ******************/
            else if (pk instanceof ComponentSummaryPK){
                dealEntity = searchCache(pk);

                  if(dealEntity == null)
                  {
                    dealEntity = new ComponentSummary( srk,pk.getId (), pk.getCopyId ());
                  }
            }        
         /***************MCM Impl team changes ends - XS_11.16*****************/            
            else if ( dealEntity == null )
              throw (new Exception("[CalcEntityCache.find Error]PK not implemented: [" + pk.getName () + "]" ) );

            entities.put(pk, dealEntity);
        }
        
        else
        {
          // if(DealCalc.debug)
          //   ResourceManager.getSysLogger("0").debug("Found in Cache: " + pk);
        }

        return dealEntity ;

    }
    catch ( Exception e )
    {
      String msg = "[CalcEntityCache.find]\n[" +
          pk.getName () + ":" + pk.getId () + ":" + pk.getCopyId () + "]Error: " + e.getMessage () ;
      throw ( new Exception( msg) );
    }
  }

  /**
	 * Helper to find(..) method - Perform the actual search of this cache.
	 * 
	 * @version 1.0 Initial Version
	 * @version 1.1 XS_11.11<br>
	 *          Date: 10-Jun-2008 <br>
	 *          Author: MCM Impl Team Change: <br>
	 *          Added ComponentPk check <br>
	 */
  private DealEntity searchCache( IEntityBeanPK pk ) throws Exception
  {
      Collection entityCollection = null;

      if ( pk instanceof AssetPK)
        entityCollection = assets.values();
      else if ( pk instanceof BorrowerPK)
        entityCollection = borrowers.values();
      else if ( pk instanceof DealFeePK)
        entityCollection = dealFees.values();
      else if ( pk instanceof DocumentTrackingPK)
        entityCollection = doctracks.values();
      else if ( pk instanceof DownPaymentSourcePK)
        entityCollection = downPaymentSources.values();
      //--DJ_CR136--23Nov2004--start--//
      else if ( pk instanceof InsureOnlyApplicantPK)
        entityCollection = insureOnlyApplicants.values();
      //--DJ_CR136--23Nov2004--end--//
      else if ( pk instanceof EscrowPaymentPK)
        entityCollection = escrowPayments.values();
      else if ( pk instanceof PricingProfilePK)
        entityCollection = pricingProfiles.values();
      else if ( pk instanceof FeePK)
        entityCollection = dealFees.values();
      else if ( pk instanceof IncomePK)
        entityCollection = incomes.values();
      else if ( pk instanceof LiabilityPK)
        entityCollection = liabilities.values();
      else if ( pk instanceof PropertyPK)
        entityCollection = properties.values();
      else if ( pk instanceof PropertyExpensePK)
        entityCollection = propertyExpenses.values();   
      // XS_11.11 10-Jun-2008 - Added ComponentPK Check
      else if ( pk instanceof ComponentPK)
          entityCollection = components.values();

      if(entityCollection == null) return null;

      return searchCollections(new ArrayList(entityCollection), pk);
  }
 /**
   *  Helper to searchCache(..) method - Perform the actual search of this cache.
   *
   */
  private DealEntity searchCollections(List list, IEntityBeanPK pk)
  {
    if(list == null || list.isEmpty())
     return null;

    Iterator it = list.iterator();

    List current = null;

    DealEntity entity = null;

    while(it.hasNext())
    {
      current = (List)it.next();

      if(current == null || current.isEmpty())
       return null;

      Iterator cit = current.iterator();

      while(it.hasNext())
      {
        entity = (DealEntity)cit.next();

        if(entity.getPk().equals(pk))
        {
          return entity;
        }
      }
    }

    return null;
  }

  /**
   *  add an entity to the 'Entities' store of this cache.
   */
  public DealEntity addEntity(DealEntity in)
  {
     return (DealEntity)entities.put(in.getPk(),in);
  }



  public DealEntity addEntity(Object in)
  {
      DealEntity de = (DealEntity)in;
      return addEntity(de);
  }


  /**
   *  Gets (calculates) a 'Key' value for a given IEntityBeanPK PrimaryKey class.
   *  The value will be equal to the primary key id field * 10000 + the copyId.
   *  @ return the calculated key
   *  @ param the primarykey class
   */
  // Modified by Billy to change the Key to EntityName_ID_CopyId -- BILLY 29Oct2001
  //  Reason : Problem was found when e.g. DealId = 2, BorrowerId = 2 ==> same key for Incomes
  public String getKey(IEntityBeanPK pk)
  {
   return new String(pk.getName() + "_" + pk.getId() + "_" + pk.getCopyId());
  }
  
// deleted for ML
//  public static void main( String args[]) throws Exception
//  {
//    CalcEntityCache cc = new CalcEntityCache(null );
//    cc.find ( null, new UserProfileBeanPK (12) ) ;
//  }  
}
