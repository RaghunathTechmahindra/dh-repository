/*
 * @(#)TotalPaymentAmountComponentLOC.java July 19, 2008 Copyright (C) 2008 Filogix Limited
 * Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * Title: TotalPaymentAmountComponentLOC
 * </p>
 * <p>
 * Description:Calculates P and I Total payment for Line of credit components of
 * "component eligible" deals.
 * </p>
 * @author MCM Impl Team <br>
 * @Date 19-July-2008 <br>
 * @version 1.0 XS_11.9 Initial Version <br>
 *          Code Calc-1.CLOC <br>
 */
public class TotalPaymentAmountComponentLOC extends DealCalc
{
	private Collection componentCollec;
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.9 19-July-2008 Initial Version
     */
    public TotalPaymentAmountComponentLOC() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 44.CLOC";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.9 19-July-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOC)
            {
                ComponentLOC componentLOC = (ComponentLOC) input;
                addTarget(componentLOC);
            }
            else if (classId == ClassId.DEAL)
                    {
                        Deal deal = (Deal) input;
                        // retrives collection of components associated to the
                        // deal
                        componentCollec = this.entityCache.getComponents(deal, srk);
                        Iterator componentsItr = componentCollec.iterator();
                        while (componentsItr.hasNext())
                        {
                            Component component = (Component) componentsItr.next();
                            // Checks the component type is LOC COMPONENT
                            if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOC)
                            {
                                // Retrives the componentLOC Object for the
                                // given
                                // componentId and copyId
                                ComponentLOC componentLOC = (ComponentLOC) this.entityCache
                                        .find(srk, new ComponentLOCPK(component
                                                .getComponentId(), component
                                                .getCopyId()));
                                addTarget(componentLOC);
                            }
                        }
                    }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.9 19-July-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        try
        {
            ComponentLOC targetComponentLOC = (ComponentLOC) target;
            // Retrive component Object from the given target
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLOC.getComponentId(),
                            targetComponentLOC.getCopyId()));
            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(
            		component.getDealId(), component.getCopyId()));
            //Calculate total Payment starts
            if(deal.getTaxPayorId()==Mc.TAX_PAYOR_LENDER && targetComponentLOC.getPropertyTaxAllocateFlag().equalsIgnoreCase("Y"))
            {
            	targetComponentLOC.setTotalPaymentAmount(validate(
            			targetComponentLOC.getPAndIPaymentAmount()+targetComponentLOC.getPropertyTaxEscrowAmount()));
            	targetComponentLOC.ejbStore();
            }else{
            	targetComponentLOC.setTotalPaymentAmount(targetComponentLOC.getPAndIPaymentAmount());
            	targetComponentLOC.ejbStore();
            }
            //Calculate total Payment ends
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * 
     * @version 1.0 XS_11.9 19-July-2008 Initial Versionn
     * 
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "taxPayorId"));

        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "propertyTaxEscrowAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "propertyTaxAllocateFlag"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "pAndIPaymentAmount"));
        // Max totalPaymentAmount =999999.999999
        addTargetParam(new CalcParam(ClassId.COMPONENTLOC, "totalPaymentAmount",T12D6));
    }

}
