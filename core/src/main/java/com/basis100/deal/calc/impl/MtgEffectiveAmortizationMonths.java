/*
 * @(#)MtgEffectiveAmortizationMonths.java June 11, 2008 <story id : XS 11.3>
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.pk.PricingRateInventoryPK;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: MtgEffectiveAmortizationMonths
 * </p>
 * <p>
 * Description: Calculates effective amortization months for Component Mortgage
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.3 11-Jun-2008 Initial version
 * @version 1.1 XS_11.3 24-Jun-2008 Added 'd' for constants
 * @version 1.2 Modified the getTarget(..) method
 * @version 1.3 12-July-2008 BugFix:artf742237 modfied docalc() method for
 *          getting the mtgprodid from mortgagecomponent table.
 * @version 1.4 14-July-2008 BugFix:artf742257 modfied doCalc() method for
 *          getting the PricingRateInventory & PricingProfile
 * @version 1.5 artf751756 1-Aug-2008 added try catch block in getTarget
 */

public class MtgEffectiveAmortizationMonths extends DealCalc
{

    /**
     * Default Constructor
     * @throws Exception
     */
    public MtgEffectiveAmortizationMonths() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "CALC 34.CM";
    }

    /**
     * <p>
     * Description: Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @version 1.0 XS_11.3 Initial Version
     * @version 1.1 08-Jul-2008 Added ComponentTypeId check
     * @version 1.5 artf751756 1-Aug-2008 added try catch block
     * @param input
     *            Object Input component.
     * @param dcm
     *            CalcMonitor runs the calculation of this Component entity.
     * @throws TargetNotFoundException
     *             exception that may occurs if the target is not found.
     * @see com.basis100.deal.calc.DealCalc#getTarget(java.lang.Object,
     *      com.basis100.deal.calc.CalcMonitor)
     */
    @Override
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {

        int classId = ((DealEntity) input).getClassId();

        try
        {
            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    // MCM added try-catch
                    Component component = (Component) input;
                    try {
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                        {
                            // find componentMortgage for the given componentId and
                            // copyId
                            ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
                                    .find(srk, new ComponentMortgagePK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            if (componentMtg != null)
                            {
                                addTarget(componentMtg);
                            }
                        }
                    } catch (Exception e ){
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                }
        }
        catch (Exception e)
        {

            StringBuffer msg = new StringBuffer();
            msg.append("Failed to get Targets for:");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append(" @input: ");
            msg.append(this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Performs the calculation based on the calculation rules in
     * CALC#34.CM
     * </p>
     * @version 1.0 XS_11.3 Initial Version
     * @param target
     *            Object target component.
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @version 1.3 12-July-2008 BugFix:artf742237 modfied docalc() method for
     *          getting the mtgprodid from mortgagecomponent table.
     * @version 1.4 14-July-2008 BugFix:artf742257 modfied docalc() method for
     *          getting the PricingRateInventory & PricingProfile
     */
    @Override
    public void doCalc(Object target) throws Exception
    {

        trace(this.getClass().getName());

        double effectiveAmortizationMonths = 0.0d;

        ComponentMortgage targetComponentMtg = (ComponentMortgage) target;
        // Get the Component entity
        Component component = (Component) this.entityCache.find(srk,
                new ComponentPK(targetComponentMtg.getComponentId(),
                        targetComponentMtg.getCopyId()));
        // Get the Deal entity
        Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                .getDealId(), component.getCopyId()));
        // get the mtgprod for this component
        MtgProd mtgProd = (MtgProd) this.entityCache.find(srk, new MtgProdPK(
                component.getMtgProdId()));
        // Get PricingRateInventory
        PricingRateInventory pricingRateInventory = new PricingRateInventory(
                srk);
        PricingRateInventoryPK inventoryPK = new PricingRateInventoryPK(
                component.getPricingRateInventoryId());
        pricingRateInventory = pricingRateInventory
                .findByPrimaryKey(inventoryPK);

        // Get PricingProfile

        PricingProfile pricingProfile = pricingRateInventory
                .getPricingProfile();
        if (pricingProfile.getRateCode().equals(Mc.UNCNF)
                || component.getRepaymentTypeId() == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
                || component.getRepaymentTypeId() == Mc.REPAYMENT_INTEREST_ONLY_FIXED)
        {
            effectiveAmortizationMonths = targetComponentMtg
                    .getAmortizationTerm();
        }
        else
        {
            double paymentAmount = targetComponentMtg.getTotalPaymentAmount();
            // Get interestCompound Description
            String interestCompoundDesc = BXResources.getPickListDescription(
                    pricingProfile.getInstitutionProfileId(),
                    "INTERESTCOMPOUND", mtgProd.getInterestCompoundingId(), srk
                            .getLanguageId());

            boolean dJDaysYearFlag = Boolean
                    .getBoolean(PropertiesCache
                            .getInstance()
                            .getProperty(deal.getInstitutionProfileId(),
                                    "com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear"));
            // Get the payment frequency
            double paymentFrequency = DealCalcUtil.calcPaymentFrequency(
                    targetComponentMtg.getPaymentFrequencyId(), dJDaysYearFlag);

            // Formula calculation for interestFactor
            double interestFactor = DealCalcUtil.interestFactor(
                    targetComponentMtg.getNetInterestRate(), paymentFrequency,
                    interestCompoundDesc);
            // Formula Calculation for effective amortization months
            double numerator = Math.log((1d - targetComponentMtg
                    .getTotalMortgageAmount()
                    * (interestFactor / paymentAmount)));
            double denominator = Math.log(Math.pow((1d + interestFactor), -1d));

            effectiveAmortizationMonths = numerator / denominator;

            if (effectiveAmortizationMonths < 0d)
            {
                effectiveAmortizationMonths = 0d;
            }

            // additional calculation of effective amortization for payment
            // frequency
            if (targetComponentMtg.getPaymentFrequencyId() == Mc.PAY_FREQ_MONTHLY)
            { // if
                // payment
                // frequency
                // is
                // monthly
                ; // do nothing as the result will already be in months
            }
            else
                if (targetComponentMtg.getPaymentFrequencyId() == Mc.PAY_FREQ_SEMIMONTHLY)
                { // payment
                    // frequency
                    // is
                    // semi-monthly
                    effectiveAmortizationMonths = effectiveAmortizationMonths / 2d;
                }
                else
                    if (targetComponentMtg.getPaymentFrequencyId() == Mc.PAY_FREQ_BIWEEKLY
                            || targetComponentMtg.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
                    { // payment
                        // frequency
                        // is
                        // bi-weekly
                        // or
                        // bi-weekly
                        // accelerated)
                        effectiveAmortizationMonths = effectiveAmortizationMonths / 2.1666667d;
                    }
                    else
                        if (targetComponentMtg.getPaymentFrequencyId() == Mc.PAY_FREQ_WEEKLY
                                || targetComponentMtg.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
                        {// payment
                            // frequency
                            // is
                            // weekly
                            // or
                            // aceelerated
                            // weekly
                            effectiveAmortizationMonths = effectiveAmortizationMonths / 4.3333333d;
                        }
            effectiveAmortizationMonths = doRoundOff(effectiveAmortizationMonths); // do
            // the
            // round
            // off
            // of
            // effective
            // amortization
            // months

        }

        if (effectiveAmortizationMonths
                - targetComponentMtg.getAmortizationTerm() == 1d)
        {
            effectiveAmortizationMonths = validate(targetComponentMtg
                    .getAmortizationTerm());
        }

        targetComponentMtg
                .setEffectiveAmortizationMonths(validate((int) effectiveAmortizationMonths));
        targetComponentMtg.ejbStore();

    }

    /**
     * <p>
     * Description: Round off the effective amortization month if it is greater
     * than .009
     * </p>
     * @version 1.0 XS_11.3 Initial Version
     * @param effectiveAmortizationMonths
     *            double effective amortization months.
     * @return doRoundOff - double
     */
    private int doRoundOff(double effectiveAmortizationMonths)
    {
        double result = effectiveAmortizationMonths;
        String str1 = effectiveAmortizationMonths + "";
        int intDecimalPos = str1.indexOf('.'); // index position of decimal
        if (intDecimalPos != -1)
        {
            int intLength = str1.length();
            String temp = str1.substring(intDecimalPos, intLength); // extract
            // the
            // decimal
            // value
            double value = Double.parseDouble(temp.substring(0, 4)); // get
            // the
            // value
            if (value > .009)
                result = effectiveAmortizationMonths + 1d;
        }
        return (int) result;

    }

    /**
     * <p>
     * Description: Initializes the input & target parameters which is called in
     * the constructor.
     * </p>
     * @version 1.0 XS_11.3 Initial Version
     * @throws ParamTypeException
     *             exception that may occur if param type is not valid.
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "pricingRateInventoryId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "amortizationTerm"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "netInterestRate"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalPaymentAmount"));
        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "effectiveAmortizationMonths", T3));

    }

}
