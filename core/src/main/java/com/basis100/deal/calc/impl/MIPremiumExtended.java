package com.basis100.deal.calc.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.calc.ConfigurbleCalc;
import com.basis100.deal.calc.DealCalcException;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.StringUtil;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceWrapper;
import com.filogix.externallinks.services.ServiceDelegate;
import com.filogix.externallinks.services.ServiceInfo;

public class MIPremiumExtended
extends ConfigurbleCalc
{
    private final static Log _log = LogFactory.getLog(MIPremiumExtended.class);
    public MIPremiumExtended()
    throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 4A";
    }
    protected int getCalcTypeid()
    {
        return Mc.CALCTYPE_MIPREMIUM_EXTENDED;
    }

    /**
     * since this class extends ConfigurbleCalc,
     * target object is always Deal entity.
     */
    public void doCalc(Object target) throws Exception
    {
    	_log.debug("MIPremiumExtended:: doCalc methof starts.");
    	
        Deal deal = (Deal)target;

        if(checkIfDealIsDatXMIPremiumTarget(deal) == false) return;
        
        _log.debug("MIPremiumExtended:: checkIfDealIsDatXMIPremiumTarget executed.");
        
        if(DealCalcUtil.checkIfMIPremiumIsExtended(deal) == false) return;
        
        _log.debug("MIPremiumExtended:: DealCalcUtil.checkIfMIPremiumIsExtended executed.");

        ServiceInfo si = new ServiceInfo();
        si.setDealId(new Integer(deal.getDealId()));
        si.setCopyId(new Integer(deal.getCopyId()));
        si.setSrk(new SessionResourceWrapper(deal.getSessionResourceKit()));
        si.setLanguageId(deal.getSessionResourceKit().getLanguageId());
        si.setProductType(new Integer(deal.getMortgageInsurerId()));

        ServiceDelegate sd = new ServiceDelegate();
        try{
            _log.debug("sendMIPremiumExtendedRequest before");
            sd.sendMIPremiumExtendedRequest(si);
            _log.debug("sendMIPremiumExtendedRequest after");
        }catch(Exception e){
            _log.error(StringUtil.stack2string(e));
            throw e;
        }

    }

    /**
     * check if the deal is target for DatX MI premium.
     * this is the same check as MIPremium cals does.
     * @param deal
     * @return true - yes <br> false - no
     * @throws DealCalcException
     */
    private boolean checkIfDealIsDatXMIPremiumTarget(Deal deal) 
    throws DealCalcException {

        if(deal == null) throw new DealCalcException("deal is null");

        //--> Check to skip calc (just in case) if "miprocessinghandledoutsidexpress" = Y
        if(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), 
                "com.basis100.miinsurance.miprocessinghandledoutsidexpress", "N").equals("Y") &&
                deal.getMIStatusId() == Mc.MI_STATUS_APPROVED)
        {
            _log.debug("Not datx mi premium target : " +
                    "com.basis100.miinsurance.miprocessinghandledoutsidexpress = Y " +
                    "and deal.getMIStatusId() = " + deal.getMIStatusId());
            return false;
        }

        if(deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECEIVED   /* 15 Approval Received */
                || deal.getMIStatusId() == Mc.MI_STATUS_APPROVED          /* 16 Approved */
                || deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED )/* 23 Approved Premium Changed */
        {
            if(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.calc.mipremium.bypass", "N").equals("Y") &&
                    deal.getMIExistingPolicyNumber() != null && deal.getMIExistingPolicyNumber().trim().length() > 0)
            {
                _log.debug("Not mi premium datx target : " +
                        "com.basis100.calc.mipremium.bypass = Y " +
                        "and deal.getMIStatusId() = " + deal.getMIStatusId() +
                        "and deal.getMIExistingPolicyNumber() = " + deal.getMIExistingPolicyNumber());
                return false;
            }

        }

        // MIIndicator must be either requested std guidelines or uw required 
        if(deal.getMIIndicatorId() != Mc.MII_REQUIRED_STD_GUIDELINES  //1
                && deal.getMIIndicatorId() != Mc.MII_UW_REQUIRED)     //2
        {
            _log.debug("Not mi premium datx target : " +
                    "deal.getMIIndicatorId()=" + deal.getMIIndicatorId());
            return false;
        }


        // check if this system is capable of external link
        if (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), 
                "com.filogix.express.externallink", "N").equals("Y") == false) {
            _log.debug("Not extended: com.filogix.express.externallink = N");
            return false;
        }

        // check if selected MI support DatX
        int mortgageInsurerId = deal.getMortgageInsurerId();
        _log.debug("mortgageInsurerId = " + mortgageInsurerId);

        String datxSupported = PicklistData.getMatchingColumnValue(
                "MORTGAGEINSURER", mortgageInsurerId, "DATXMIPREMIUMSUPPORTED");
        if (datxSupported.equals("Y") == false) {
            _log.debug("Not extended: mortgageInsurer doesn't support datx");
            return false;
        }
        
        // this deal is target
        _log.debug("before check mi premium extended");
        return true; 
    }


}
