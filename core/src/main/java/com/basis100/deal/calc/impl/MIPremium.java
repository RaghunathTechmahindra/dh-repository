package com.basis100.deal.calc.impl;

/**
 * 04.Oct.2011 DVG #DG1064 LEN526303: Daniel Larue IG- Total loan amount is 1 cent or $0.01 too high
 * 31.May.2011 DVG #DG1056 NBC517902: Mi reset/Cmhc mipremium truncate
 */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceWrapper;
import com.filogix.externallinks.services.ServiceDelegate;
import com.filogix.externallinks.services.ServiceInfo;

// Name: MIPremium
// Code: CALC-4
// Mortgage insurence premium
// 06/27/200

/**
* <p>
* Title: MIPremium
* </p>
* <p>
* Description: Calculates Mortgage Insurance Premium
* </p>
* @author MCM Impl Team
* @version 1.0 Existing version
* @version 1.1 - Added Deal.mtgProdId as Input param
*            - Changed method parameter for mipRateForCMHCSelfEmployedSimplified in doCalcImpl 
*            - mipRateForCMHCSelfEmployedSimplified () - Added condn for productType LOC & UnderWriterType LOC          
*       
*/

public class MIPremium extends DealCalc
{
  public MIPremium() throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 4";
  }

  public Collection getTargets()
  {
    return this.targets;
  }

  /**
   * Given an input object this method determines the appropriate target object
   * and adds the object to it's list of targets (targets).
   *
   */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if(input instanceof Property)
      {
        Property p = (Property)input;
        Deal d = (Deal)entityCache.find(srk, 
                       new DealPK(p.getDealId(), p.getCopyId()));
        addTarget(d);
      }

      if(input instanceof Deal)
      {
        addTarget(input);
      }
    }
    catch(Exception e)
    {
      String msg = "Failed to get Targets for: " + this.getClass().getName();
      msg += " :[Input]: " + input.getClass().getName();
      msg += "  Reason: " + e.getMessage();
      msg += " @input: " + this.getDoCalcErrorDetail(input);
      logger.error(msg);
      throw new TargetNotFoundException(msg);
    }
  }

  /**
   * do the calculation.
   */
  public void doCalc(Object target) throws Exception {
    trace(this.getClass().getName());
    Deal deal = (Deal)target;
	if (doCalcImpl(deal))
		deal.ejbStore();
  }

  protected boolean doCalcImpl(Deal deal)
  {
    boolean isDoStore = false;

    try
    {
      trace("MIStatusId = " + deal.getMIStatusId());

      //--> Check to skip calc (just in case) if "miprocessinghandledoutsidexpress" = Y
      if(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.miinsurance.miprocessinghandledoutsidexpress", "N").equals("Y") &&
        deal.getMIStatusId() == Mc.MI_STATUS_APPROVED)
      {
        return isDoStore;
      }

	  trace("com.basis100.miinsurance.miprocessinghandledoutsidexpress = N");

      // New changes to Set the MIStatus = 'Error Reported By Insurer if Approved && MIPremium changed
      boolean isMIApproved = false;
      double oldMIPremium = deal.getMIPremiumAmount();

      if(deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECEIVED   /* 15 Approval Received */
        || deal.getMIStatusId() == Mc.MI_STATUS_APPROVED          /* 16 Approved */
        || deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED /* 23 Approved Premium Changed */)
      {
        if(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.calc.mipremium.bypass", "N").equals("Y") &&
          deal.getMIExistingPolicyNumber() != null && deal.getMIExistingPolicyNumber().trim().length() > 0)
        {
          return isDoStore;
        }

        isMIApproved = true;
      }

      double dbLoanToValue = deal.getCombinedLTV() / 100.00;
      int mortgageInsurerId = deal.getMortgageInsurerId();
      int mortgageInsuranceTypeId = deal.getMITypeId();
      int lienPositionId = deal.getLienPositionId();
      int miIndicatorId = deal.getMIIndicatorId();
      double dbMIPRate = 0.0;
      
      
      
      
      // added inst id for ML - no Institutionized insitutionId
      String DatxMIPremiumSupported 
          = PicklistData.getMatchingColumnValue( -1,
    		  	"MORTGAGEINSURER", mortgageInsurerId, "DATXMIPREMIUMSUPPORTED");
      logger.debug("DataX Supported? " + DatxMIPremiumSupported);
      logger.debug("MI Provider - " + deal.getMortgageInsurerId());
      
      trace("Props size: " + this.entityCache.getProperties(deal).size());
      trace("dbLoanToValue = " + dbLoanToValue);
      Collection props = this.entityCache.getProperties(deal);
      
      Vector propList = (Vector)deal.getProperties();
      Property aProperty = (Property) propList.get(0);

      //[Bypass Deals Section]
      if(!(oneOf(miIndicatorId, Mc.MII_REQUIRED_STD_GUIDELINES, Mc.MII_UW_REQUIRED)))
      {
        deal.setMIPremiumAmount(0);

        trace("miIndicatorId <> Mc.MII_REQUIRED_STD_GUIDELINES or Mc.MII_UW_REQUIRED, " +
        		"set MIPremium = 0 and exit calc. ");
        
		isDoStore = true;
        return isDoStore;
      }
      
//    Add for calculating premium externaly -- By Clement 24July2006
      else if(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.filogix.express.externallink","N").equals("Y") 
    		  && DatxMIPremiumSupported.equals("Y"))
      {
          // PMI2: when MIPremium is extended, this calc is not going to fire.
          if(DealCalcUtil.checkIfMIPremiumIsExtended(deal)) return false; 
          
    	  ServiceInfo SI = new ServiceInfo();
    	  
    	  // added for 
    	  SI.setDeal(deal);
    	  SI.setDealId(new Integer(deal.getDealId()));
    	  SI.setCopyId(new Integer(deal.getCopyId()));
    	  SI.setSrk(new SessionResourceWrapper(deal.getSessionResourceKit()));
    	  SI.setLanguageId(deal.getSessionResourceKit().getLanguageId());
    	  SI.setProductType(new Integer(deal.getMortgageInsurerId()));
    	  ServiceDelegate SD = new ServiceDelegate();
    	  SD.sendMIPremiumRequest(SI);
    	  isDoStore = true;
          return isDoStore;   	  
      }
      else if(mortgageInsuranceTypeId == Mc.MI_TYPE_TRANSFER_POLICY /* 3 Transfer Policy */
           || (mortgageInsurerId == Mc.MI_INSURER_CMHC 
               && aProperty.getPropertyTypeId() == Mc.PROPERTY_TYPE_INDIAN_RESERVATION
               && aProperty.getOnReserveTrustAgreementNumber() == null))
      {
        trace("Skip MI Premium calculator : MIType = Transfer. " + 
        		"set MIPremium = 0 and exit calc. ");
        deal.setMIPremiumAmount(0.0);
        return isDoStore;
      }
//    [Second Mortgage Section]
      else if(lienPositionId == Mc.LIEN_POSITION_SECOND)
      {
        dbMIPRate = 0.0500;
        trace("[Second Mortgage Section] : Set MI Premium Rate = 0.0500");
      }

      //[PRE APPROVAL Section]
      else if(deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL
        && (props == null || props.size() == 0))
      {
        trace("SpecialFeatureId = " + deal.getSpecialFeatureId());
        if(dbLoanToValue <= 0.75)
        {
          dbMIPRate = 0.0;
        }
        if((dbLoanToValue <= 0.80) && (dbLoanToValue > 0.75))
        {
          dbMIPRate = 0.0100;
        }
        if((dbLoanToValue <= 0.85) && (dbLoanToValue > 0.80))
        {
          dbMIPRate = 0.0175;
        }
        if((dbLoanToValue <= 0.90) && (dbLoanToValue > 0.85))
        {
          dbMIPRate = 0.0200;
        }
        if(dbLoanToValue > 0.90)
        {
          dbMIPRate = 0.0275;
        }
        trace("[PRE APPROVAL Section], dbMIPRate adjusted value = " + dbMIPRate);
      }

      else
      {
        if(props == null)
        {
          return isDoStore; // no property
        }
        Iterator iterator = props.iterator();
        Property property = null;

        while(iterator.hasNext())
        {
          property = (Property)iterator.next();
          if(property.isPrimaryProperty())
          {
            int intNumberOfUnits = property.getNumberOfUnits();
            int propertyUsageId = property.getPropertyUsageId();
            // added inst id for ML: -1 is no institutionnized table inst Id
            String refiFlagForPurchase = PicklistData
								.getMatchingColumnValue(-1, "DEALPURPOSE", deal
										.getDealPurposeId(), "REFINANCEFLAG");

            if(intNumberOfUnits < 5 && lienPositionId == Mc.LIEN_POSITION_FIRST /*First*/)
            {
              // Handle Primary Residence
              if(propertyUsageId == Mc.PROPERTY_USAGE_PRIMARY_RESIDENCE /*primary residence */
                || propertyUsageId == Mc.PROPERTY_USAGE_SECOND_HOME /* second home */)
              {
                // Handle Standard Insurance : not GETakeOut or GE 50/50
                if(oneOf(mortgageInsurerId, Mc.MI_INSURER_CMHC, Mc.MI_INSURER_GE)
					&& !oneOf(
							mortgageInsuranceTypeId,
							Mc.MI_TYPE_GE_5050_PARTIAL_COVERAGE,
							Mc.MI_TYPE_ALT_A_UPFRONT,
							Mc.MI_CREDIT_ASSIST,
							Mc.MI_CMHC_SELF_EMPLOYED_SIMPLIFIED)) 
                {
                	trace("Handle Standard Insurance : not GETakeOut or GE 50/50");
                	dbMIPRate = mipRateForStdInsNotGtoOrGE50(dbLoanToValue);
                }

                //[GE 50/50 Partial Coverage Section]
                // Handle GE 50/50 Partial Coverage Product
                if((mortgageInsurerId == Mc.MI_INSURER_GE) && 
                   (mortgageInsuranceTypeId == Mc.MI_TYPE_GE_5050_PARTIAL_COVERAGE))
                {
                  // =<0.75 not supported ==> Should be prevented by BusinessRule
                  if((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.80))
                  {
                    dbMIPRate = 0.0070;
                  }
                  if((dbLoanToValue > 0.80) && (dbLoanToValue <= 0.85))
                  {
                    dbMIPRate = 0.0105;
                  }
                  if((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.90))
                  {
                    dbMIPRate = 0.0120;
                    // >0.9 not supported ==> Should be prevented by BusinessRule
                  }
                  
                  trace("[GE 50/50 Partial Coverage Section], dbMIPRate adjusted value = " + dbMIPRate);
                }
                
                // Handle Genworth Business for self (Alt. A) Single Premium
                if (mortgageInsurerId == Mc.MI_INSURER_GE 
                    && mortgageInsuranceTypeId == Mc.MI_TYPE_ALT_A_UPFRONT
                    && propertyUsageId == Mc.PROPERTY_USAGE_PRIMARY_RESIDENCE) {
                	
                	//ticket #3462
                	int dealPurposeId = deal.getDealPurposeId();
                    // added inst id for ML: -1 is no institutionnized table inst Id
                	trace("table:DEALPURPOSE, id:" + dealPurposeId + ", description = " 
                	      + PicklistData.getMatchingColumnValue(-1, "DEALPURPOSE", dealPurposeId, "dpdescription") + ", REFINANCEFLAG = " + refiFlagForPurchase);

                	dbMIPRate = mipRateForPurchasePurpose(refiFlagForPurchase, dbLoanToValue);
                }
              }
              else
              {
            	//[Non-Standard Insurance Section]
                // Handle Non-Standard Insurance (Property Usage != Primary Residence) 
                
                /***** GR3.2.2 Mortgage Insurance Changes start *****/
                if (!oneOf(mortgageInsuranceTypeId,
                        Mc.MI_TYPE_GE_5050_PARTIAL_COVERAGE,
                        Mc.MI_TYPE_ALT_A_UPFRONT,
                        Mc.MI_CREDIT_ASSIST,
                        Mc.MI_CMHC_SELF_EMPLOYED_SIMPLIFIED)) {
                    if (dbLoanToValue <= 0.65) {
                        dbMIPRate = 0.0125;
                    }
                    if ((dbLoanToValue <= 0.75)
                            && (dbLoanToValue > 0.65)) {
                        dbMIPRate = 0.0175;
                    }
                    if ((dbLoanToValue <= 0.80)
                            && (dbLoanToValue > 0.75)) {
                        dbMIPRate = 0.0250;
                    }
                    if ((dbLoanToValue <= 0.85)
                            && (dbLoanToValue > 0.80)) {
                        dbMIPRate = 0.0350;
                    }
                    if ((dbLoanToValue <= 0.90)
                            && (dbLoanToValue > 0.85)) {
                        dbMIPRate = 0.0475;
                    }
                    if ((mortgageInsurerId == Mc.MI_INSURER_CMHC)
                            && (dbLoanToValue <= 0.95)
                            && (dbLoanToValue > 0.90)) {
                        if (mortgageInsuranceTypeId == Mc.MI_TYPE_CMHC_FLEXDOWN
                                && oneOf(propertyUsageId,
                                        Mc.PROPERTY_USAGE_RENTAL,
                                        Mc.PROPERTY_USAGE_INVESTMENT))
                            dbMIPRate = 0.0660;
                        else
                            dbMIPRate = 0.0650;
                    }
                    if ((mortgageInsurerId == Mc.MI_INSURER_CMHC)
                            && (dbLoanToValue <= 1)
                            && (dbLoanToValue > 0.95)) {
                        dbMIPRate = 0.0725;
                    }
                }
                /***** GR3.2.2 Mortgage Insurance Changes end *****/
              }
            } // end if Num of Unit < 5 && lien position = first
            
            //[Genworth CreditAssist]
            if(intNumberOfUnits < 3 
            		&& oneOf(propertyUsageId, Mc.PROPERTY_USAGE_PRIMARY_RESIDENCE, Mc.PROPERTY_USAGE_SECOND_HOME)
            		&& lienPositionId == Mc.LIEN_POSITION_FIRST
            		&& mortgageInsurerId == Mc.MI_INSURER_GE
            		&& mortgageInsuranceTypeId == Mc.MI_CREDIT_ASSIST)
            {
            	dbMIPRate = mipRateForGECreditAssist(refiFlagForPurchase, dbLoanToValue); 
            }
            
            //[CMHC- Self-employed Simplified]
            if(intNumberOfUnits < 3 
            		&& oneOf(propertyUsageId, Mc.PROPERTY_USAGE_PRIMARY_RESIDENCE, Mc.PROPERTY_USAGE_SECOND_HOME)
            		&& lienPositionId == Mc.LIEN_POSITION_FIRST
            		&& mortgageInsurerId == Mc.MI_INSURER_CMHC
            		&& mortgageInsuranceTypeId == Mc.MI_CMHC_SELF_EMPLOYED_SIMPLIFIED)
            {
              
//              ***************MCM Impl team changes Starts - XS_11.16 - Version 1.1 *******************/  
//              mtgProd.getUnderwriteAsTypeId() -  XS_11.16 for Calculation Mortgage Insurance Premium
                MtgProd mtgProd =  deal.getMtgProd();
                
            	  dbMIPRate = mipRateForCMHCSelfEmployedSimplified(dbLoanToValue, refiFlagForPurchase, deal.getProductTypeId(),mtgProd.getUnderwriteAsTypeId());
              //***************MCM Impl team changes ends - XS_11.16 - Version 1.1 *******************/
            }
          } // end of is primary property
        } // end of while
      } //end of else

      //boolean isProgressAdvance = false;
      if(dbMIPRate > 0)
      {
        //Additional Handling for Progress Draw Mortgages -- By BILLY 02April2002
        if(deal.getProgressAdvance() != null && deal.getProgressAdvance().equals("Y"))
        {
          //isProgressAdvance = true;
        }

        if(mortgageInsuranceTypeId == Mc.MI_TYPE_GE_CASHBACK || mortgageInsuranceTypeId == Mc.MI_TYPE_CMHC_FLEXDOWN)
        {
        	trace("Added 0.0015 for GE Cashback or CMHC Flex Down");
        	dbMIPRate += 0.0015;
        }
        
        if (deal.getLocRepaymentTypeId() == Mc.LOC_TYPE_5_20) {
        	trace("Added 0.0025 for LocRepaymentTypeId = LOC_TYPE_5_20");
        	dbMIPRate += 0.0025;
        }
        
        if (deal.getLocRepaymentTypeId() == Mc.LOC_TYPE_10_15) {
        	trace("Added 0.0050 for LocRepaymentTypeId = LOC_TYPE_10_15");
        	dbMIPRate += 0.0050;
        }
        
        if (mortgageInsurerId == Mc.MI_INSURER_GE
            && mortgageInsuranceTypeId == Mc.MI_TYPE_VACATION) {
        	trace("Added 0.0075 for mortgageInsurerId == Mc.MI_INSURER_GE && mortgageInsuranceTypeId == Mc.MI_TYPE_VACATION");
        	dbMIPRate += 0.0075;
        }
      }

      //[Amortization > 25 year Surcharge]
      /***** GR3.2.2 Mortgage Insurance Changes start *****/
      if(mortgageInsurerId == Mc.MI_INSURER_GE || 
              (mortgageInsurerId == Mc.MI_INSURER_CMHC && !propertyEngeryEffiency(deal)))
      {
    	  dbMIPRate = mipRateForAmortGT25YrSurcharge(dbMIPRate, deal.getAmortizationTerm());
      }
      /***** GR3.2.2 Mortgage Insurance Changes end *****/

      // Round the MIRate to 4 dec points
      dbMIPRate = (Math.round(dbMIPRate * 10000d)) / 10000d;
      trace("The result dbMIRate = " + dbMIPRate);
      double miPrem;
      double netAmt = deal.getNetLoanAmount();
      // Don't round it if InsurerId = CMHC
      if(mortgageInsurerId == Mc.MI_INSURER_CMHC)
      {
          //deal.setMIPremiumAmount(validate(
          //  Math.floor(dbMIPRate * deal.getNetLoanAmount() * 100.00) / 100.00));
          /***** fix for ticket FXP23138 *****/
         // deal.setMIPremiumAmount(validate(
           //       Math.floor(dbMIPRate * 1000.00 * deal.getNetLoanAmount() * 100.00) / (100.00 * 1000.00) ));
               miPrem = dbMIPRate * netAmt *100;	// multiply by 100 and later reduce to 2 cents
	      	miPrem = Math.round(miPrem*10000.) /10000.;	//#DG1056 //ticket FXP23138 *****
	         trace("result dbMIRate = " + dbMIPRate+"::CMHC prem(*100)= " + miPrem);
	      	miPrem = Math.floor(miPrem) /100.;
          /***** fix for ticket FXP23138 *****/
      }
      else
      {
         // deal.setMIPremiumAmount(validate(
          //  Math.round(dbMIPRate * deal.getNetLoanAmount() * 100.00) / 100.00));
           //#DG1064 for GE truncate net loan amt
	         miPrem = dbMIPRate * Math.floor(netAmt) *100;	// multiply by 100 and later reduce to 2 cents
	         trace("result dbMIRate = " + dbMIPRate+"::GE prem(*100)= " + miPrem);
	      	miPrem = Math.round(miPrem) /100.;
      }
 deal.setMIPremiumAmount(validate(miPrem));
 deal.setMIPremiumAmountPrevious(oldMIPremium); //5.0/MI beta phase
 
      trace("Setting the MI premium amount from MI Premium calculator :  Deal ID is : " + deal.getDealId() + " and copyId is :" +
        deal.getCopyId() + " and MI Premium Amount is : " + deal.getMIPremiumAmount());

      //// Do not update the premium amount if is MI process handled outside of BXP (such as BMO).
      if(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.miinsurance.miprocessinghandledoutsidexpress", "N").equals("N"))
      {
        trace("MIProcessing inside BXP");

        //Check if Approved && MIPremium changed ==> Change MIStatus to 'Error Reported By Insurer'
        // and append MI Response messages -- BY BILLY 08Feb2002
        if((isMIApproved == true) && (oldMIPremium != deal.getMIPremiumAmount()))
        {
        		deal.setMIStatusId(Mc.MI_STATUS_ERRORS_RECEIVED_BY_INSURER);

          UserProfile lUserProfile = new UserProfile(srk);
          
          // added instituionProfileId from ExpressState
          lUserProfile = lUserProfile
                     .findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(), srk.getExpressState().getDealInstitutionId()));

          int userLangId = lUserProfile.getUserLanguageId();
          String insurerName = BXResources.getPickListDescription(deal.getInstitutionProfileId(), "MORTGAGEINSURER", mortgageInsurerId, userLangId);
          
          // Generate MI Responses
          String retBuf = "";
          retBuf += insurerName;
          retBuf += " ";
          retBuf += TypeConverter.formattedDate(new Date(), "MMM dd, yyyy 'at' hh:mm:ss") + "\n\r";
          retBuf += "** \n\r";
          retBuf +=
            "** This Deal with MI Status = \'Approved\' has had changes that updated the Mortgage Insurance Premium. \n\r";
        	  retBuf += "** MI Status set to \' Errors Reported by Insurer \' \n\r";
          retBuf += "** \n\r";
          retBuf += "** \n\r";
/*
          // append it at the top of MI Response
          if(deal.getMortgageInsuranceResponse() != null)
          {
            deal.setMortgageInsuranceResponse(retBuf + "\n\r" + deal.getMortgageInsuranceResponse());
          }
          else
          {
            deal.setMortgageInsuranceResponse(retBuf);
          }
*/
          //fix for QC554, trying to make up for this awful design choice
          //send the updated response text to all copies of the deal
          DealPropagator dp = new DealPropagator(deal, null);
          if(deal.getMortgageInsuranceResponse() != null)
          {
            dp.setMortgageInsuranceResponse(retBuf + "\n\r" + deal.getMortgageInsuranceResponse());
          }
          else
          {
            dp.setMortgageInsuranceResponse(retBuf);
          }
          //commit this change, if we got this far the whole thing should be in a committable state anyway. 
          dp.ejbStore();
          //end QC554
          
        	  trace("MI premium amount changed with MI Status = Approved : set MI Staus to ERRORS_RECEIVED_BY_INSURER");
          trace("MIPremium, MIResponse: " + deal.getMortgageInsuranceResponse());
        }
      }
	  isDoStore = true;
      return isDoStore;
    }
    catch(Exception e)
    {
      String msg = "Benign Failure in Calculation" + this.getClass().getName();
      msg += ":[Target]:" + deal.getClass().getName();
      msg += "  Reason: " + e.getMessage();
      msg += "@Target: " + this.getDoCalcErrorDetail(deal);
      logger.debug(msg);
      logger.debug(StringUtil.stack2string(e));
      e.printStackTrace();
    }

    deal.setMIPremiumAmount(DEFAULT_FAILED_DOUBLE);
	isDoStore = true;
	return isDoStore;
  }

  private double mipRateForCMHCSelfEmployedSimplified(double dbLoanToValue, String refiFlagForPurchase, int productTypeId, int underWriterAsTypeId) {
	  trace("Entering mipRateForCMHCSelfEmployed.Simplified()," +
	      		"dbLoanToValue = " + String.valueOf(dbLoanToValue) + ", productTypeId = " + productTypeId 
	      		+ ", refiFlagForPurchase = " + refiFlagForPurchase+", underWriterAsTypeId = " + underWriterAsTypeId);
	  
	  double dbMIPRate = 0;
//	purchase
//  ***************MCM Impl team changes Starts - XS_11.16 - Version 1.1 *******************/  
//  Added condition underWriterAsTypeId != 2 -  XS_11.16 for Calculation Mortgage Insurance Premium
  	if(!refiFlagForPurchase.equalsIgnoreCase("Y") && (productTypeId != Mc.PRODUCT_TYPE_SECURED_LOC 
  	                                                && underWriterAsTypeId != Mc.PRODUCT_TYPE_SECURED_LOC)){
//  ***************MCM Impl team changes ends - XS_11.16 - Version 1.1 *******************/        
  		if(dbLoanToValue <= 0.65)
          {
        	  dbMIPRate = 0.0080;
          }
          if((dbLoanToValue > 0.65) && (dbLoanToValue <= 0.75))
          {
            dbMIPRate = 0.0100;
          }
          if((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.80))
          {
        	  dbMIPRate = 0.0164;
          }
          if((dbLoanToValue > 0.80) && (dbLoanToValue <= 0.85))
          {
        	  dbMIPRate = 0.0290;
          }
          if((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.90))
          {                   
        	  dbMIPRate = 0.0475;
          }  
          if((dbLoanToValue > 0.90) && (dbLoanToValue <= 0.95))
          {                   
          	dbMIPRate = 0.0600;
          }  
  	}
  	
  	//LOC OR (NOT Purchase)
  	//  ***************MCM Impl team changes Starts - XS_11.16 - Version 1.1 *******************/
  	//  Added condn underWriterAsTypeId==2 -  XS_11.16 for Calculation Mortgage Insurance Premium
  	if(refiFlagForPurchase.equalsIgnoreCase("Y") || (productTypeId == Mc.PRODUCT_TYPE_SECURED_LOC) 
                                                 || underWriterAsTypeId == Mc.PRODUCT_TYPE_SECURED_LOC){
//      ***************MCM Impl team changes ends - XS_11.16 - Version 1.1 *******************/        
//  		NOT Purchase
  		if(dbLoanToValue <= 0.65)
          {
        	  dbMIPRate = 0.0080;
          }
          if((dbLoanToValue > 0.65) && (dbLoanToValue <= 0.75))
          {
            dbMIPRate = 0.0100;
          }
          if((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.80))
          {
        	  dbMIPRate = 0.0164;
          }
          if((dbLoanToValue > 0.80) && (dbLoanToValue <= 0.85))
          {
        	  dbMIPRate = 0.0290;
          }
          if((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.90))
          {                   
        	  dbMIPRate = 0.0475;
          }  
  		}
	  	trace("Leaving mipRateForCMHCSelfEmployed.Simplified(), " +
	      		"adjusted dbLoanToValue = " + String.valueOf(dbLoanToValue));
	  return dbMIPRate;
	}

  private double mipRateForGECreditAssist(String refiFlagForPurchase, double dbLoanToValue) {
	  trace("Entering mipRateForGECreditAssist()," +
	      		"dbLoanToValue = " + String.valueOf(dbLoanToValue) + ", refiFlagForPurchase = " + refiFlagForPurchase);
	  
	  double dbMIPRate = 0;
//	purchase
  	if(!refiFlagForPurchase.equalsIgnoreCase("Y")){
  		if(dbLoanToValue <= 0.65)
          {
        	  dbMIPRate = 0.0190;
          }
          if((dbLoanToValue > 0.65) && (dbLoanToValue <= 0.75))
          {
            dbMIPRate = 0.0210;
          }
          if((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.80))
          {
        	  dbMIPRate = 0.0250;
          }
          if((dbLoanToValue > 0.80) && (dbLoanToValue <= 0.85))
          {
        	  dbMIPRate = 0.0350;
          }
          if((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.90))
          {                   
        	  dbMIPRate = 0.0475;
          }  
          if((dbLoanToValue > 0.90) && (dbLoanToValue <= 0.95))
          {                   
          	dbMIPRate = 0.0650;
          }  
  	}
  	else	
  	{
//  		NOT Purchase
  		if(dbLoanToValue <= 0.65)
          {
        	  dbMIPRate = 0.0190;
          }
          if((dbLoanToValue > 0.65) && (dbLoanToValue <= 0.75))
          {
            dbMIPRate = 0.0210;
          }
          if((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.80))
          {
        	  dbMIPRate = 0.0250;
          }
          if((dbLoanToValue > 0.80) && (dbLoanToValue <= 0.85))
          {
        	  dbMIPRate = 0.0350;
          }
          if((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.90))
          {                   
        	  dbMIPRate = 0.0475;
          }  
  	}
  	trace("Leaving mipRateForGECreditAssist(), " +
      		"adjusted dbLoanToValue = " + String.valueOf(dbLoanToValue));
  return dbMIPRate;
}

  private double mipRateForStdInsNotGtoOrGE50(double dbLoanToValue)
  {
	  trace("Entering mipRateForStdInsNotGtoOrGE50()," +
	      		"dbLoanToValue = " + String.valueOf(dbLoanToValue));
	  
	  double dbMIPRate = 0;
	  if(dbLoanToValue <= 0.65)
      {
        dbMIPRate = 0.0050;
      }
      if((dbLoanToValue > 0.65) && (dbLoanToValue <= 0.75))
      {
        dbMIPRate = 0.0065;
      }
      if((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.80))
      {
        dbMIPRate = 0.0100;
      }
      if((dbLoanToValue > 0.80) && (dbLoanToValue <= 0.85))
      {
        dbMIPRate = 0.0175;
      }
      if((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.90))
      {
        dbMIPRate = 0.0200;
      }
      if((dbLoanToValue > 0.90) && (dbLoanToValue <= 0.95))
      {
        dbMIPRate = 0.0275;
      }
      if(dbLoanToValue > 0.95)
      {
    	  dbMIPRate = 0.0310;
      }
      trace("Leaving mipRateForStdInsNotGtoOrGE50(), " +
	      		"adjusted dbLoanToValue = " + String.valueOf(dbLoanToValue));
      return dbMIPRate;
  }
  
  private double mipRateForAmortGT25YrSurcharge(double dbMIPRate, int amortTerm)
  {
      trace("Entering mipRateForAmortGT25Yr.Surcharge(), [Amortization > 25 year Surcharge], " +
      		"AmortizationTerm = " + amortTerm + ", dbMIPRate = " + dbMIPRate);
      
      double _addi = 0.0;
      
      if(amortTerm > 300 && amortTerm <= 360)
      {
    	  _addi += 0.0020;
      }
      else if(amortTerm > 360 && amortTerm <= 420)
      {
    	  _addi += 0.0040;
      }
      else if(amortTerm > 420 && amortTerm <= 480)
      {
    	  _addi += 0.0060;
      }
      
      dbMIPRate += _addi;
      trace("Leaving mipRateForAmortGT25Yr.Surcharge(..), new dbMIPRate = " + dbMIPRate);
	  return dbMIPRate;
  }
  
  private double mipRateForPurchasePurpose(String refinanceFlagForPurchasePurpose, double dbLoanToValue)
  {
	  trace("Entering mipRateForPurchasePurpose()," +
	      		"dbLoanToValue = " + String.valueOf(dbLoanToValue) +
	      		", refinanceFlagForPurchasePurpose = " + refinanceFlagForPurchasePurpose);
	 double dbMIPRate = 0;
	 //	ticket #4669
	 if(!refinanceFlagForPurchasePurpose.equalsIgnoreCase("Y"))
	 {
		 if ((dbLoanToValue <= 0.65)) {
			 dbMIPRate = 0.0080;
		 }
		 if ((dbLoanToValue > 0.65) && (dbLoanToValue <= 0.75)) {
			 dbMIPRate = 0.0100;
		 }
		 if ((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.8)) {
			 dbMIPRate = 0.0164;
		 }
		 if ((dbLoanToValue > 0.8) && (dbLoanToValue <= 0.85)) {
			 dbMIPRate = 0.0290;
		 }
		 if ((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.9)) {
			 dbMIPRate = 0.0475;
		 }
		 if ((dbLoanToValue > 0.9) && (dbLoanToValue <= 0.95)) {
			 dbMIPRate = 0.0600;
		 }
	 }
	 else
	 {
		 if ((dbLoanToValue <= 0.65)) {
			 dbMIPRate = 0.0080;
		 }
		 if ((dbLoanToValue > 0.65) && (dbLoanToValue <= 0.75)) {
			 dbMIPRate = 0.0100;
		 }
		 if ((dbLoanToValue > 0.75) && (dbLoanToValue <= 0.8)) {
			 dbMIPRate = 0.0164;
		 }
		 if ((dbLoanToValue > 0.8) && (dbLoanToValue <= 0.85)) {
			 dbMIPRate = 0.0290;
		 }
		 if ((dbLoanToValue > 0.85) && (dbLoanToValue <= 0.9)) {
			 dbMIPRate = 0.0475;
		 }
	 }
	 trace("Leaving mipRateForPurchasePurpose(), " +
	      		"adjusted dbLoanToValue = " + String.valueOf(dbLoanToValue));
	 return dbMIPRate;
  }
  
  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.PROPERTY, "numberOfUnits"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "onReserveTrustAgreementNumber"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "propertyUsageId"));
    addInputParam(new CalcParam(ClassId.PROPERTY, "primaryPropertyFlag"));

    addInputParam(new CalcParam(ClassId.DEAL, "amortizationTerm"));
    addInputParam(new CalcParam(ClassId.DEAL, "combinedLTV"));
    addInputParam(new CalcParam(ClassId.DEAL, "lienPositionId"));
    addInputParam(new CalcParam(ClassId.DEAL, "LOCRepaymentTypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "MIStatusId"));
    addInputParam(new CalcParam(ClassId.DEAL, "MITypeId"));
    addInputParam(new CalcParam(ClassId.DEAL, "mortgageInsurerId"));
    addInputParam(new CalcParam(ClassId.DEAL, "MIIndicatorId"));
    addInputParam(new CalcParam(ClassId.DEAL, "MIExistingPolicyNumber"));
    addInputParam(new CalcParam(ClassId.DEAL, "netLoanAmount"));
    addInputParam(new CalcParam(ClassId.DEAL, "specialFeatureId"));
//  ***************MCM Impl team changes Starts - XS_11.16 - Version 1.1 *******************/
    addInputParam(new CalcParam(ClassId.DEAL, "mtgProdId"));
//  ***************MCM Impl team changes Ends - XS_11.16 - Version 1.1 *******************/
    addTargetParam(new CalcParam(ClassId.DEAL, "MIPremiumAmount", T13D2));
  }
  

   /**
     * method to check property energy efficiency.
     * @param deal
     * @return
     * @throws Exception
     * GR3.2.2 Mortgage Insurance Changes  
     */
    private boolean propertyEngeryEffiency(Deal deal) throws Exception {
        Collection props = this.entityCache.getProperties(deal);
        boolean energyEfficiency = false;
        if (props != null) {
            Iterator iterator = props.iterator();
            Property property = null;
            while (iterator.hasNext()) {
                property = (Property) iterator.next();
                if (property.isPrimaryProperty()
                        && property.getMiEnergyEfficiency().equalsIgnoreCase(
                                "Y")) {
                    energyEfficiency = true;
                }
            }
        }
        return energyEfficiency;
    }
}
