/*
 * @(#)FirstPaymentDateMortgageComponent.java Jun 09, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: FirstPaymentDateMortgageComponent
 * </p>
 * <p>
 * Description:Calculates First Payment Date for Mortgage components of
 * "component eligible" deals.
 * </p>
 * 
 * @author MCM Impl Team <br>
 * @Date 17-Jun-2008 <br>
 * @version 1.0 XS_11.2 Initial Version <br>
 * @version 1.1 XS_11.2 Review Comments Incorporated
 *          Code Calc-13.CM <br>
 */

public class FirstPaymentDateMortgageComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * 
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public FirstPaymentDateMortgageComponent()throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 13.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * 
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
    throws TargetNotFoundException {
        try {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Deal deal = (Deal) input;
                    // Get all the components for the deal
                    Collection listOfComponents = entityCache.getComponents(
                            deal, srk);
                    Iterator componentsItr = listOfComponents.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is MORTGAGE COMPONENT
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                        {
                            // Retrives the componentMortgage Object for the given  componentId and copyId
                            ComponentMortgage componentMtg = (ComponentMortgage) entityCache
                                    .find(srk, new ComponentMortgagePK(
                                            component.getComponentId(),
                                            component.getCopyId()));
                            addTarget(componentMtg);
                        }
                    }
                }
        } catch (Exception e) {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }


    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * 
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        ComponentMortgage componentMortgage = (ComponentMortgage) target ;
        Component componentObj= (Component) entityCache.find(
                srk, new ComponentPK(componentMortgage.getComponentId(),
                        componentMortgage.getCopyId()));
        Deal dealObj= (Deal) entityCache.find(
                srk, new DealPK(componentObj.getDealId(),
                        componentObj.getCopyId()));

        try
        {

            int period=0;

            int paymentFrequency = componentMortgage.getPaymentFrequencyId ();
            // Period is set as per PaymentFrequencyId
            if ( paymentFrequency == Mc.PAY_FREQ_SEMIMONTHLY )
                period = 15 ; //15 days
            else if ( paymentFrequency == Mc.PAY_FREQ_BIWEEKLY || paymentFrequency == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY )
                period = 14 ; // 14 days
            else if ( paymentFrequency == Mc.PAY_FREQ_WEEKLY || paymentFrequency == Mc.PAY_FREQ_ACCELERATED_WEEKLY )
                period = 7 ;  // 7 days
            else if ( paymentFrequency == Mc.PAY_FREQ_MONTHLY )
                period = 30 ; // 30 days

            Calendar firstPaymentDate = Calendar.getInstance (TimeZone.getDefault (), Locale.getDefault ());

            Date estimatedClosingDate = null;

            // Force IAD to 1st of Month if property set to Y and this property must be same among institutions in a instance and in DB
            if ((PropertiesCache.getInstance().getProperty(dealObj.getInstitutionProfileId(), 
                    "com.basis100.calc.forceiadtofirstofmonth", "N")).equals("Y"))
            {
                // Calc by IAD
                estimatedClosingDate = componentMortgage.getInterestAdjustmentDate();
                logger.debug("com.basis100.calc.forceiadtofirstofmonth is Y " );
            }
            else
            {
                // By Closing date
                estimatedClosingDate = dealObj.getEstimatedClosingDate ();
                logger.debug("com.basis100.calc.forceiadtofirstofmonth is N, "); 
            }
            logger.debug(" dcDate will be deal EstimatedClosingDate: " + estimatedClosingDate);


            // if estimatedClosingDate is not null then only set value for  firstPaymentDate else thros exception
            if ( estimatedClosingDate != null && ( period > 0 ) )
            {

                firstPaymentDate.setTime (  estimatedClosingDate );
                if ( period == 30 )
                    firstPaymentDate.add ( Calendar.MONTH ,  1 );
                else
                    firstPaymentDate.add ( Calendar.DAY_OF_MONTH ,  period );  // = EstimatedClosingDate + period

                componentMortgage.setFirstPaymentDate ( firstPaymentDate.getTime ());
                componentMortgage.ejbStore ();
                return;
            }
            else
            {
                throw new Exception("Required Input = null -> deal.getEstimatedClosingDate()/getInterestAdjustmentDate(). or PaymentFrequencey error ");
            }
        }
        catch ( Exception e )
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());

        }

        componentMortgage.setFirstPaymentDate(DEFAULT_FAILED_DATE);
        componentMortgage.ejbStore();
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * 
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Versionn
     * 
     */
    private void initParams()throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();

        //Adding input parameters
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));

        //This property must be same among institutions in a instance and in DB
        if ((PropertiesCache.getInstance().getProperty(-1,
                "com.basis100.calc.forceiadtofirstofmonth", "N")).equals("Y"))
        {
            // Calc by IAD if force IAD to First of Month
            addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "interestAdjustmentDate"));
        }

        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "firstPaymentDate"));
    }

}
