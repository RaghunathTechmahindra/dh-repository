package com.basis100.deal.calc.impl;

import java.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;

//Name: Total Down Payment Amount
//Code: Calc 61
// 07/04/2000

public class TotalDownPaymentAmount extends DealCalc
{


  public TotalDownPaymentAmount()throws Exception
  {
    super();
    initParams();
    this.calcNumber = "Calc 61";
  }

  

  public Collection getTargets()
  {
    return this.targets;
  }

  /**
 * Given an input object this method determines the appropriate target object
 * and adds the object to it's list of targets (targets).
 *
 */
  public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
  {
    try
    {
      if ( input instanceof DownPaymentSource )
      {
        DownPaymentSource downpaymentSource   = ( DownPaymentSource) input ;

        Deal deal = (Deal) entityCache.find (srk,
            new DealPK( downpaymentSource.getDealId(), 
                 downpaymentSource.getCopyId()));

        addTarget( deal );
      }
    }
    catch ( Exception e)
    {
      String msg = "Fail to get Targets" + this.getClass().getName();
      msg += ":[Input]:" + input.getClass ().getName () ;
      msg += "\nReason: " + e.getMessage();
      msg += "@input: " + this.getDoCalcErrorDetail ( input );
      logger.error (msg );
      throw new TargetNotFoundException (msg);    }
  }



  public void doCalc(Object target) throws Exception
  {
     trace(this.getClass().getName());
     Deal deal = (Deal) target;

     try
     {
      Collection downPayments = deal.getDownPaymentSources () ;
      Iterator itDP = downPayments.iterator ();
      double totalDownPaymentAmount = 0.0;
      while  ( itDP.hasNext () )
      {
          DownPaymentSource downPaymentSource = ( DownPaymentSource) itDP.next ();
          totalDownPaymentAmount += downPaymentSource.getAmount ();
      } // End of .Loop through all down payments

      totalDownPaymentAmount = Math.round( totalDownPaymentAmount*100) /100.00 ; 
      deal.setDownPaymentAmount ( validate(totalDownPaymentAmount));
      deal.ejbStore ();
      return;
     }
     catch ( Exception e)
     {
        String msg = "Benign Failure in Calculation" + this.getClass().getName();
        msg += ":[Target]:" + target.getClass ().getName () ;
        msg += "\nReason: " + e.getMessage();
        msg += "@Target: " + this.getDoCalcErrorDetail ( target );
        logger.debug (msg );
     }
   deal.setDownPaymentAmount(DEFAULT_FAILED_DOUBLE);
   deal.ejbStore ();
  }

  public boolean hasInput(CalcParam input)
  {
    return this.inputParam.contains(input);
  }

  private void initParams() throws ParamTypeException
  {
    targetParam = new ArrayList();
    inputParam  = new ArrayList();
    inputClassTypes = new HashSet();

    addInputParam(new CalcParam(ClassId.DOWNPAYMENTSOURCE, "amount"));

    addTargetParam(new CalcParam(ClassId.DEAL, "downPaymentAmount", T13D2 ));
  }



}
