package com.basis100.deal.calc.impl;

import java.util.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.calc.*;

/**
 * 
 * <p>Title: CashBackAmount.java</p>
 *
 * <p>Description: CashBackAmount.java</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public class CashBack extends DealCalc {
	public boolean catchFlag = true;

	/**
	 * 
	 *  A constructor for this class.
	 * 
	 *  @throws Exception
	 */
	public CashBack() throws Exception {
		super();
		initParams();
		this.calcNumber = "Calc 110";
	}

	/**
	 * method: getTargets 
	 * description: returns the Collection object which contains all the defined target for the CashBack Class
	 * 
	 * @throws - 
	 */
	public Collection getTargets() {
		return this.targets;
	}

	/**
	 * method: getTarget
	 * description: Method add the targets into the Calc Class
	 * 
	 * @param: Object, CaclMonitor
	 * @throws TargetNotFoundException 
	 */
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
		try {
			addTarget(input);
		} catch (Exception e) {
			String msg = "Fail to get Targets" + this.getClass().getName();
			msg += ":[Input]:" + input.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@input: " + this.getDoCalcErrorDetail(input);
			logger.error(msg);
			throw new TargetNotFoundException(msg);
		}
	}

	/**
	 * method: hasInput
	 * description: Checks whether the required input exists or not.
	 * 
	 * @param CalcParam
	 * @return: Boolean
	 */
	public boolean hasInput(CalcParam input) {
		return this.inputParam.contains(input);
	}

	/**
	 * method: doCalc
	 * description: Performs the required business calculations.
	 * 
	 * @param: Object
	 * @throws: Exception
	 */
	public void doCalc(Object target) throws Exception {
		trace(this.getClass().getName());
		Deal deal = (Deal) target;

		try {
			double totalLoanAmount = deal.getTotalLoanAmount();
			String cashBackOverride = deal.getCashBackAmountOverride();
			double cashBackPercent = deal.getCashBackPercent();
			double cashBackAmount = deal.getCashBackAmount();
			
			if (cashBackOverride.equalsIgnoreCase("N")) {
				catchFlag = true;
				cashBackAmount = (cashBackPercent * totalLoanAmount) / 100;
				cashBackAmount = Math.round ( cashBackAmount * 100 ) /100.00;
				deal.setCashBackAmount(cashBackAmount);
				deal.ejbStore();
			} else if (cashBackOverride.equalsIgnoreCase("Y")) {
				catchFlag = false;
				if ((totalLoanAmount == 0)) {
					deal.setCashBackPercent(0.0);
				} else {
					cashBackPercent = 
							(cashBackAmount / totalLoanAmount) * 100;
					cashBackPercent = Math.round ( cashBackPercent * 100 ) /100.00;
					deal.setCashBackPercent(cashBackPercent);
				}
				deal.ejbStore();
			}
			return;
		} catch (Exception e) {
			String msg = "Benign Failure in Calculation"
					+ this.getClass().getName();
			msg += ":[Target]:" + target.getClass().getName();
			msg += "\nReason: " + e.getMessage();
			msg += "@Target: " + this.getDoCalcErrorDetail(target);
			logger.debug(msg);
		}
		if (catchFlag) {
			deal.setCashBackAmount(this.DEFAULT_FAILED_DOUBLE);
		} else {
			deal.setCashBackPercent(this.DEFAULT_FAILED_DOUBLE);
		}
		deal.ejbStore();
	}

	/**
	 * 
	 * initParams
	 * Method description: Initializes all the required input and output(targets) parameters 
	 *
	 * @throws ParamTypeException
	 */
	private void initParams() throws ParamTypeException {
		targetParam = new ArrayList();
		inputParam = new ArrayList();
		inputClassTypes = new HashSet();

		//addInputParam(new CalcParam(ClassId.DEAL, "cashBackPercent"));//Defect # 4675
		addInputParam(new CalcParam(ClassId.DEAL, "cashBackAmountOverride"));
		addInputParam(new CalcParam(ClassId.DEAL, "totalLoanAmount"));
		addInputParam(new CalcParam(ClassId.DEAL, "cashBackAmount"));//Defect # 4675

		//addTargetParam(new CalcParam(ClassId.DEAL, "cashBackAmount"));
		addTargetParam(new CalcParam(ClassId.DEAL, "cashBackPercent"));
	}

}
