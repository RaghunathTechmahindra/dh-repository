package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: TeaserPandIComponent
 * </p>
 * <p>
 * Description:Calculates the The P and I amount for Components
 * </p>
 * Calculation Number- Calc-113.C
 * 
 * @version 1.0 XS_11.8 10-July-2008 Initial Version
 * @version 1.1 XS_11.8 24-July-2008 Rafactored doCalc
 */
public class TeaserRateInterestSavingComponent extends DealCalc {

	/**
	 * <p>
	 * Description: Default constructor that initalize the parameters and sets
	 * the calculation number
	 * </P>
	 * 
	 * @throws Exception -
	 *             exception object
	 * @version 1.0 XS_11.8 10-July-2008 Initial Version
	 */
	public TeaserRateInterestSavingComponent() throws Exception {
		super();
		initParams();
		this.calcNumber = "Calc 113.C";
	}

	/**
	 * <p>
	 * Description:Given an input object this method determines the appropriate
	 * target object and adds the object to it's list of targets (targets).
	 * </p>
	 * 
	 * @param input -
	 *            Input Object
	 * @param dcm -
	 *            deal calculation monitor object
	 * @throws TargetNotFoundException -
	 *             target not found exception object
	 * @version 1.0 XS_11.8 10-July-2008 Initial Version
	 */
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
		try {
			int classId = ((DealEntity) input).getClassId();

			if (classId == ClassId.COMPONENT) {
				Component component = (Component) input;
				// find component for the given componentId and
				addTarget(component);
			} else if (classId == ClassId.COMPONENTMORTGAGE) {
				ComponentMortgage componentMtg = (ComponentMortgage) input;
				// find component for the given componentId and
				// copyId
				Component component = (Component) entityCache.find(srk,
						new ComponentPK(componentMtg.getComponentId(),
								componentMtg.getCopyId()));
				addTarget(component);
			} else if (classId == ClassId.COMPONENTLOAN) {
				ComponentLoan componentLoan = (ComponentLoan) input;
				// find component for the given componentId and
				// copyId
				Component component = (Component) entityCache.find(srk,
						new ComponentPK(componentLoan.getComponentId(),
								componentLoan.getCopyId()));
				addTarget(component);
			}
		} catch (Exception e) {
			StringBuffer msg = new StringBuffer(100);
			msg.append("Failed to get Targets for: ");
			msg.append(this.getClass().getName());
			msg.append(" :[Input]: ");
			msg.append(input.getClass().getName());
			msg.append("  Reason: " + e.getMessage() + " @input: "
					+ this.getDoCalcErrorDetail(input));
			logger.error(msg.toString());
			throw new TargetNotFoundException(msg.toString());
		}
	}

	/**
	 * <p>
	 * Description: Runs the calculation on the target objects
	 * </p>
	 * 
	 * @version 1.0 XS_11.8 10-July-2008 Initial Version
	 * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
	 * @param target
	 *            Target object
	 * @throws Exception -
	 *             exception object
	 */
	public void doCalc(Object target) throws Exception {
		trace(this.getClass().getName());

		try {

			Component targetComponent = (Component) target;

			int componentTypeId = targetComponent.getComponentTypeId();
			
			// Retrive the Deal for the given component
			// PricingRateInventory priInventory=new
			// PricingRateInventory(srk,targetComponent.getPricingRateInventoryId());

			int term = 1;

			//2009-01-25 FXP24160, if the component type is LOC than skip this calculation
			if (targetComponent.getTeaserNetInterestRate() <= 0 || componentTypeId == Mc.COMPONENT_TYPE_LOC) {

				targetComponent.setTeaserRateInterestSaving(0);
				targetComponent.ejbStore();
				return;

			} else {

				int paymentFrequencyId = 0;
				double pAndIPaymentAmount = 0;
				double mtgOrLoanAmount = 0;
				int amortizationTerm = 0;
				double regularNetInterestRate = 0;

				if (componentTypeId == Mc.COMPONENT_TYPE_MORTGAGE) {
					ComponentMortgage componentMtg = (ComponentMortgage) this.entityCache
							.find(srk, new ComponentMortgagePK(targetComponent
									.getComponentId(), targetComponent
									.getCopyId()));
					paymentFrequencyId = componentMtg.getPaymentFrequencyId();
					pAndIPaymentAmount = componentMtg.getPAndIPaymentAmount();
					mtgOrLoanAmount = componentMtg.getMortgageAmount();
					amortizationTerm = componentMtg.getAmortizationTerm();
					regularNetInterestRate = componentMtg.getNetInterestRate();

				} else if (componentTypeId == Mc.COMPONENT_TYPE_LOAN) {

					ComponentLoan componentLoan = (ComponentLoan) this.entityCache
							.find(srk, new ComponentLoanPK(targetComponent
									.getComponentId(), targetComponent
									.getCopyId()));

					paymentFrequencyId = componentLoan.getPaymentFrequencyId();
					pAndIPaymentAmount = componentLoan.getPAndIPaymentAmount();
					mtgOrLoanAmount = componentLoan.getLoanAmount();
					amortizationTerm = componentLoan.getActualPaymentTerm();
					regularNetInterestRate = componentLoan.getNetInterestRate();
				}

				double paymentsPerYear = 0;
				//double regularInterestFactor = 0.0;
				double regularBalanceRemaining = 0.0;
				double teaserInterestFactor = 0.0;
				double regularInterestOverTerm = 0.0;
				double teaserBalanceRemaining = 0.0;
				double teaserRateInterestSaving = 0.0;
				double teaserInterestOverTerm = 0.0;

				// Calculating Payments per year Starts
				String decimalYear = PropertiesCache.getInstance().getProperty(
						targetComponent.getInstitutionProfileId(),
						"com.basis100.calc.usepandipaymentsbasedondjdaysyear",
						"N");
				double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
				paymentsPerYear = DealCalcUtil.pCalcNumOfPaymentsPerYear(
						paymentFrequencyId, mDaysInYear);

				// Calculating Payments per year Ends

				// Calculating Total Payments Starts
				double totalPayments = 1;
				String DJNumberOfPayments = PropertiesCache.getInstance()
						.getProperty(targetComponent.getInstitutionProfileId(),
								"com.filogix.calc.DJNumberOfPayments", "N");
				if (DJNumberOfPayments.equalsIgnoreCase("Y")) {
					// mPaymentsPerYear = 12, 24, 26 or 52
					double mDJPaymentsPerYear = DealCalcUtil
							.pCalcNumOfPaymentsPerYear(paymentFrequencyId);
					totalPayments = term * mDJPaymentsPerYear;

				} else {
					totalPayments = term * paymentsPerYear;
				}

				int totalPaymentsTruncated = (int) totalPayments;
				// Calculating Total Payments Ends

				// Retrive the MtgProd for the given MtgProdId
				MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
						new MtgProdPK(targetComponent.getMtgProdId()));

				String interestCompoundDesc = PicklistData
						.getMatchingColumnValue(-1, "interestcompound", mtgProd
								.getInterestCompoundingId(),
								"interestcompounddescription");

				// Regular Balance Remaining Starts
				int repaymentTypeId = targetComponent.getRepaymentTypeId();

				// calculate regular interest over term
				if (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
						|| repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED) {

					regularBalanceRemaining = mtgOrLoanAmount;

				} else if (amortizationTerm == term) {

					regularBalanceRemaining = 0.0;

				} else {

					regularBalanceRemaining = mtgOrLoanAmount;
					double interest = 0.0;
					double principalPaid = 0.0;

					// Calculating Interest Factor Starts
					double regularInterestFactor = DealCalcUtil.interestFactor(
							regularNetInterestRate, paymentsPerYear,
							interestCompoundDesc);
					// Calculating Interest Factor Ends

					for (int paymentNumber = 1; paymentNumber <= totalPaymentsTruncated; paymentNumber++) {

						interest = DealCalcUtil
								.roundToNearestHundredth(regularBalanceRemaining
										* regularInterestFactor);
						principalPaid = DealCalcUtil
								.roundToNearestHundredth(pAndIPaymentAmount
										- interest);
						regularBalanceRemaining = DealCalcUtil
								.roundToNearestHundredth(regularBalanceRemaining
										- principalPaid);
					}
					if (regularBalanceRemaining < 0d) {
						regularBalanceRemaining = 0.0;
					}
				}
				// calculate Regular Balance Remaining Ends

				// calculate Regular InterestOverTerm Starts
				regularInterestOverTerm = (pAndIPaymentAmount * totalPaymentsTruncated)
						- (mtgOrLoanAmount - regularBalanceRemaining);
				// calculate Regular InterestOverTerm Ends

				// calculate Teaser Balance Remaining Starts

				double teaserPaymentAmount = targetComponent
						.getTeaserPiAmount();

				if (repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_VARIABLE
						|| repaymentTypeId == Mc.REPAYMENT_INTEREST_ONLY_FIXED) {

					teaserBalanceRemaining = mtgOrLoanAmount;

				} else if (amortizationTerm == term) {
					teaserBalanceRemaining = 0.0;
				} else {
					teaserInterestFactor = DealCalcUtil.interestFactor(
							targetComponent.getTeaserNetInterestRate(),
							paymentsPerYear, interestCompoundDesc);
					teaserBalanceRemaining = mtgOrLoanAmount;
					double interest = 0.0;
					double principalPaid = 0.0;

					for (int paymentNumber = 1; paymentNumber <= totalPaymentsTruncated; paymentNumber++) {
						interest = DealCalcUtil
								.roundToNearestHundredth(teaserBalanceRemaining
										* teaserInterestFactor);
						principalPaid = DealCalcUtil
								.roundToNearestHundredth(teaserPaymentAmount
										- interest);
						regularBalanceRemaining = DealCalcUtil
								.roundToNearestHundredth(teaserBalanceRemaining
										- principalPaid);
					}
					if (teaserBalanceRemaining < 0d) {
						teaserBalanceRemaining = 0.0;
					}
				}
				// calculate Teaser Balance Remaining Ends

				teaserInterestOverTerm = (teaserPaymentAmount * totalPaymentsTruncated)
						- (mtgOrLoanAmount - teaserBalanceRemaining);
				// calc and set teaser rate interest saving
				teaserRateInterestSaving = regularInterestOverTerm
						- teaserInterestOverTerm;
				
				// 2009-01-25 if the teaserRateInterestingSaving is less than 0, set it as 0
				if(teaserRateInterestSaving < 0)
				{
					targetComponent.setTeaserRateInterestSaving(0);
				}
				//this code is just to make sure the teaserRateInterestSaving amount does not exceed the number of digit the db can handle
				else if (teaserRateInterestSaving > DealCalc.T13D2)
				{
					targetComponent.setTeaserRateInterestSaving(DealCalc.T13D2);
				}
				else
				{
					targetComponent.setTeaserRateInterestSaving(teaserRateInterestSaving);
				}
				targetComponent.ejbStore();

				return;
			}

		} catch (Exception e) {
			StringBuffer msg = new StringBuffer(100);
			msg.append("Benign Failure in Calculation");
			msg.append(this.getClass().getName());
			msg.append(":[Target]:");
			msg.append(target.getClass().getName());
			msg.append("  Reason: ");
			msg.append(e.getMessage());
			msg.append("@Target: ");
			msg.append(this.getDoCalcErrorDetail(target));
			logger.debug(msg.toString());
		}
	}

	/**
	 * <p>
	 * Description: This method specifies input and target parameters.
	 * </p>
	 * 
	 * @version 1.0 XS_11.8 10-July-2008 Initial Versionn
	 */
	private void initParams() throws ParamTypeException {
		targetParam = new ArrayList();
		inputParam = new ArrayList();
		inputClassTypes = new HashSet();

		addInputParam(new CalcParam(ClassId.COMPONENT, "teaserNetInterestRate"));
		addInputParam(new CalcParam(ClassId.COMPONENT, "teaserPIAmount"));
		addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
				"netInterestRate"));
		addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
				"pAndIPaymentAmount"));
		addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
				"paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
				"totalMortgageAmount"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "netInterestRate"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "pAndIPaymentAmount"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "paymentFrequencyId"));
		addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "LoanAmount"));

		addTargetParam(new CalcParam(ClassId.COMPONENT,
				"teaserRateInterestSaving"));
	}
}
