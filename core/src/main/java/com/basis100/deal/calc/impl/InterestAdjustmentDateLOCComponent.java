/*
 * @(#)InterestAdjustmentDateLOCComponent.java Jun 24, 2008 Copyright (C) 2008
 * Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import MosSystem.Mc;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * Title: InterestAdjustmentDateLOCComponent
 * </p>
 * <p>
 * Description:Calculates Interest Adjustment Date for LOC components of
 * "component eligible" deals.
 * </p>
 * @author MCM Impl Team <br>
 * @Date 25-Jun-2008 <br>
 * @version 1.0 XS_11.10 Initial Version <br>
 * @Date 30-Jun-2008 <br>
 * @version 1.1 XS_11.10 - doCalc() - Check for Component type LOC added <br> -
 *          initParam() - Removed input params (mtgProdId & paymentFrequency Id)<br>
 *          Code Calc-11.CLOC <br>
 * @version 1.2 XS_11.10 09-Jul-2008 Removed check for Mossys property -
 *          forceiadtofirstofmonth
 */
public class InterestAdjustmentDateLOCComponent extends DealCalc
{

    private Collection componentCollec;

    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     */
    public InterestAdjustmentDateLOCComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 11.CLOC";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOC)
            {
                addTarget(input);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Deal deal = (Deal) input;
                    // retrives collection of components associated to the
                    // deal
                    componentCollec = this.entityCache.getComponents(deal, srk);
                    Iterator componentsItr = componentCollec.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is LOC COMPONENT
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOC)
                        {
                            // Retrives the componentLOC Object for the
                            // given
                            // componentId and copyId
                            ComponentLOC componentLOC = (ComponentLOC) this.entityCache
                                    .find(srk, new ComponentLOCPK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            addTarget(componentLOC);
                        }
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @version 1.1 XS_11.10 30-Jun-2008 Added check for ComponentLOC
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        // if the deal level product is line of credit then the deal
        // IAD will default to the estimated closing date
        ComponentLOC componentLOC = (ComponentLOC) target;
        Component componentObj = (Component) entityCache.find(srk,
                new ComponentPK(componentLOC.getComponentId(), componentLOC
                        .getCopyId()));

        if (componentObj.getComponentTypeId() == Mc.COMPONENT_TYPE_LOC)
        {
            Deal dealObj = (Deal) entityCache.find(srk, new DealPK(componentObj
                    .getDealId(), componentObj.getCopyId()));
            Calendar IAD = Calendar.getInstance(TimeZone.getDefault(), Locale
                    .getDefault());
            Date estimatedClosingDate = dealObj.getEstimatedClosingDate();
            IAD.setTime(estimatedClosingDate);
            componentLOC.setInterestAdjustmentDate(IAD.getTime());
            componentLOC.ejbStore();
        }
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @version 1.1 XS_11.10 30-Jun-2008 Removed input params(mtgProdId &
     *          paymentFrequencyId)
     * @version 1.2 XS_11.10 09-Jul-2008 Removed check for Mossys props
     *          forceiadtofirstofmonth
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        // Adding input parameters
        addInputParam(new CalcParam(ClassId.DEAL, "estimatedClosingDate"));
        
        /***** CON24326 ticket fix start 
         * add "componentId" to trigger this calc *****/
        addInputParam(new CalcParam(ClassId.COMPONENTLOC, "componentId"));
        /***** CON24326 ticket fix end  *****/
        
        // Target parameter
        addTargetParam(new CalcParam(ClassId.COMPONENTLOC,
                "interestAdjustmentDate"));
    }

}
