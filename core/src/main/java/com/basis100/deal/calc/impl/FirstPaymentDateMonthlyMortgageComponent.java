/*
 * @(#)FirstPaymentDateMonthlyMortgageComponent.java Jun 17, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.ComponentMortgage;

/**
 * <p>
 * Title: FirstPaymentDateMonthlyMortgageComponent
 * </p>
 * <p>
 * Description:Calculates First Payment Date Monthly for Mortgage components of
 * "component eligible" deals.
 * </p>
 * 
 * @author MCM Impl Team <br>
 * @Date 17-Jun-2008 <br>
 * @version 1.0 XS_11.2 Initial Version <br>
 *          Code Calc-106.CM <br>
 */
public class FirstPaymentDateMonthlyMortgageComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * 
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public FirstPaymentDateMonthlyMortgageComponent()throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 106.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * 
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     */
    public void getTarget(Object input, CalcMonitor dcm) throws TargetNotFoundException
    {
        try
        {
            addTarget(input);
        }
        catch ( Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }


    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * 
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        ComponentMortgage componentMortgage = (ComponentMortgage) target ;


        try
        {
            Calendar firstPayDateMonthly = Calendar.getInstance();
            Date IAD = componentMortgage.getInterestAdjustmentDate();
            if ( IAD != null)
            {
                // If IAD is not null then set it to firstPayDateMonthly
                firstPayDateMonthly.setTime(IAD);
                // Add a month to firstPayDateMonthly
                firstPayDateMonthly.add(Calendar.MONTH , 1);

                componentMortgage.setFirstPaymentDateMonthly(firstPayDateMonthly.getTime());
                componentMortgage.ejbStore ();
                return;
            }
            else
            {
                throw new Exception("Required Input = null -> componentMortgage.getInterestAdjustmentDate().");
            }
        }
        catch ( Exception e )
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Begin Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());

        }

        componentMortgage.setFirstPaymentDateMonthly(DEFAULT_FAILED_DATE);
        componentMortgage.ejbStore();
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * 
     * @version 1.0 XS_11.2 17-Jun-2008 Initial Versionn
     * 
     */
    private void initParams()throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam  = new ArrayList();
        inputClassTypes = new HashSet();
        //Adding input paramter 
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "interestAdjustmentDate"));

        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "firstPaymentDateMonthly"));
    }

}