package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.DealEntity;

/**
 * <p>
 * Title: CashbackMtgComponent
 * </p>
 * <p>
 * Description:Calculates the CashbackPercent or CashbackAmount for mortgage
 * component of a "component eligible" deal
 * </p>
 * @author MCM Impl Team <br>
 * @version 1.0 Initial Version <br>
 * @Date 16-Jun-2008 <br>
 *       Story - XS_11.4 <br>
 *       Code Calc-110.CM 
 * @version 1.1 XS_11.4 24-Jun-2008 Review Comments Incorporated  
 * @version 1.2 artf751759 28-Jul-2008 Modified the initParams() method.
 */
public class CashbackMtgComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     */
    public CashbackMtgComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 110.CM";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @version 1.1 XS_11.4 24-Jun-2008 Review Comments Incorporated
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        if (target==null) return;
        trace(this.getClass().getName());
        try
        {
            ComponentMortgage targetComponentMtg = (ComponentMortgage) target;

            if (("Y").equalsIgnoreCase(targetComponentMtg
                    .getCashBackAmountOverride()))
            {
                double cashbackPercentage =0.00d;
                if(targetComponentMtg
                        .getTotalMortgageAmount()!=0){
                cashbackPercentage =(targetComponentMtg
                        .getCashBackAmount() / targetComponentMtg
                        .getTotalMortgageAmount()) * 100d;
                }
                cashbackPercentage = Math.round(cashbackPercentage* 100d) / 100.00d;
                // if the cashBackPercentage is >999999.999999 then Validate
                // will round of to the max given value in the TargetParam
                targetComponentMtg
                        .setCashBackPercent(validate(cashbackPercentage));
                targetComponentMtg.ejbStore();
                
            }
            else
            {
                double cashBackAmount = (targetComponentMtg
                        .getTotalMortgageAmount() * targetComponentMtg
                        .getCashBackPercent()) / 100d;
                // if the cashBackAmount is >99999999999.99 then round of to max
                // value i.e T13D2
                if (cashBackAmount > T13D2)
                {
                    cashBackAmount = T13D2;
                }
                targetComponentMtg.setCashBackAmount(cashBackAmount);
                targetComponentMtg.ejbStore();
            }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Initialize the target parameters
     * </p>
     * @version 1.0 XS_11.4 09-Jun-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "cashBackAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "cashBackPercent"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "cashBackAmountOverride"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount"));
        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "cashBackAmount", T13D2));
    }

}
