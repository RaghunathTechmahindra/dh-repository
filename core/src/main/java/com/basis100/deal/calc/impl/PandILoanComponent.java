package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;

/**
 * <p>
 * Title: PandILoanComponent
 * </p>
 * <p>
 * Description:The amount that is required to repay a loan over the amortization
 * term, based on the payment frequency selected. The P&I payments will payback
 * to the lender the original principal borrowed plus interest.
 * </p>
 * Calculation Number- Calc-1.CLoan
 * @version 1.0 XS_11.13 02-Ju1-2008 Initial Version
 * @version 1.1 16-Jul-2008 Modified doCalc(..) method
 * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block in getTarget
 */
public class PandILoanComponent extends DealCalc
{
    /**
     * <p>
     * Description: Default constructor that initalize the parameters and sets
     * the calculation number
     * </P>
     * @throws Exception -
     *             exception object
     * @version 1.0 XS_11.13 02-Jul-2008 Initial Version
     */
    public PandILoanComponent() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "Calc 1.CLoan";
    }

    /**
     * <p>
     * Description:Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @param input -
     *            Input Object
     * @param dcm -
     *            deal calculation monitor object
     * @throws TargetNotFoundException -
     *             target not found exception object
     * @version 1.0 XS_11.13 02-Jun-2008 Initial Version
     * @version 1.2 Aug 1, 2008 fixed for artf751756 added try catch block
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        try
        {
            int classId = ((DealEntity) input).getClassId();

            if (classId == ClassId.COMPONENTLOAN)
            {
                ComponentLoan componentLoan = (ComponentLoan) input;
                addTarget(componentLoan);
            }
            else
                if (classId == ClassId.COMPONENT)
                {
                    Component component = (Component) input;
                    try {
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_LOAN)
                        {
                            // find componentLoan for the given componentId and
                            // copyId
                            ComponentLoan componentLoan = (ComponentLoan) entityCache
                                    .find(srk, new ComponentLoanPK(component
                                            .getComponentId(), component
                                            .getCopyId()));
                            addTarget(componentLoan);
                        }
                    } catch (Exception e) {
                        trace("component is being deleted componentId = " + component.getComponentId());
                    }
                }

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(" :[Input]: ");
            msg.append(input.getClass().getName());
            msg.append("  Reason: " + e.getMessage() + " @input: "
                    + this.getDoCalcErrorDetail(input));
            logger.error(msg.toString());
            throw new TargetNotFoundException(msg.toString());
        }

    }

    /**
     * <p>
     * Description: Runs the calculation on the target objects
     * </p>
     * @version 1.0 XS_11.13 02-Jul-2008 Initial Version
     * @version 1.1 16-Jul-2008 Removed the logic for RepaymentType (2(Interest
     *          only Variable and 3(Interest Only Fixed)) According to FSD
     *          changes
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     * @param target
     *            Target object
     * @throws Exception -
     *             exception object
     */
    public void doCalc(Object target) throws Exception
    {
        trace(this.getClass().getName());
        ComponentLoan targetComponentLoan = (ComponentLoan) target;

        try
        {
            // Retrive the component for the given component Loan
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentLoan.getComponentId(),
                            targetComponentLoan.getCopyId()));

            // Retrive the MtgProd for the given MtgProdId
            MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
                    new MtgProdPK(component.getMtgProdId()));

            // Varibles for Calculation
            int amortizationMonths = targetComponentLoan.getActualPaymentTerm();

            int paymentFreqId = targetComponentLoan.getPaymentFrequencyId();

            String interestCompoundDesc = PicklistData.getMatchingColumnValue(
                    -1, "interestcompound", mtgProd.getInterestCompoundingId(),
                    "interestcompounddescription");
            boolean isUseFormula = (PropertiesCache.getInstance().getProperty(
                    component.getInstitutionProfileId(),
                    "com.basis100.calc.useformulaforweeklybiweeklynonaccpay",
                    "N")).equals("Y");
            double pandIPayment = 0.0d;
            double monthlyPandI = DealCalcUtil.calculatePandI(component
                    .getInstitutionProfileId(), amortizationMonths,
                    Mc.PAY_FREQ_MONTHLY, interestCompoundDesc,
                    targetComponentLoan.getLoanAmount(), targetComponentLoan
                            .getNetInterestRate());

            if (paymentFreqId == Mc.PAY_FREQ_MONTHLY)
            {
                pandIPayment = monthlyPandI;
            }
            else
                if (paymentFreqId == Mc.PAY_FREQ_SEMIMONTHLY)
                {

                    pandIPayment = DealCalcUtil.calculatePandI(component
                            .getInstitutionProfileId(), amortizationMonths,
                            paymentFreqId, interestCompoundDesc,
                            targetComponentLoan.getLoanAmount(),
                            targetComponentLoan.getNetInterestRate());
                }
                else
                    if (paymentFreqId == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
                    {
                        pandIPayment = monthlyPandI / 2d;
                    }
                    else
                        if (paymentFreqId == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
                        {
                            pandIPayment = monthlyPandI / 4d;
                        }
                        else
                            if (paymentFreqId == Mc.PAY_FREQ_BIWEEKLY
                                    || paymentFreqId == Mc.PAY_FREQ_WEEKLY)
                            {
                                double pAndI = DealCalcUtil.calculatePandI(
                                        component.getInstitutionProfileId(),
                                        amortizationMonths, paymentFreqId,
                                        interestCompoundDesc,
                                        targetComponentLoan.getLoanAmount(),
                                        targetComponentLoan
                                                .getNetInterestRate());
                                if (isUseFormula == true)
                                {
                                    pandIPayment = pAndI;
                                }
                                else
                                {
                                    if (paymentFreqId == Mc.PAY_FREQ_BIWEEKLY)
                                    {
                                        pandIPayment = (monthlyPandI * 12d) / 26d;
                                    }
                                    else
                                        if (paymentFreqId == Mc.PAY_FREQ_WEEKLY)
                                        {
                                            pandIPayment = (monthlyPandI * 12d) / 52d;
                                        }
                                }
                            }
            pandIPayment = Math.round(pandIPayment * 100d) / 100d;
            targetComponentLoan.setPAndIPaymentAmount(pandIPayment);
            targetComponentLoan.ejbStore();
            return;
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());

        }
        targetComponentLoan.setPAndIPaymentAmount(Mc.DEFAULT_VALUE);
        targetComponentLoan.ejbStore();
    }

    /**
     * <p>
     * Description: This method specifies input and target parameters.
     * </p>
     * @version 1.0 XS_11.13 02-Jul-2008 Initial Versionn
     */
    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.COMPONENT, "mtgProdId"));
        addInputParam(new CalcParam(ClassId.COMPONENT, "repaymentTypeId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "paymentFrequencyId"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "loanAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "actualPaymentTerm"));
        addInputParam(new CalcParam(ClassId.COMPONENTLOAN, "netInterestRate"));

        addTargetParam(new CalcParam(ClassId.COMPONENTLOAN,
                "pAndIPaymentAmount", T13D2));

    }
}
