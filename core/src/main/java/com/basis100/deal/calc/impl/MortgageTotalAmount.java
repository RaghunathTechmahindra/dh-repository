/*
 * @(#)MortgageTotalAmount.java June 10, 2008 <story id : XS 11.3> Copyright (C)
 * 2008 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.calc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import MosSystem.Mc;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.CalcParam;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.calc.ParamTypeException;
import com.basis100.deal.calc.TargetNotFoundException;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * Title: MortgageTotalAmount
 * </p>
 * <p>
 * Description: Calculates total mortgage amount for Component Mortgage
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.3 10-Jun-2008 Initial version
 */

public class MortgageTotalAmount extends DealCalc
{

    /**
     * <p>
     * Description:Default constructor.
     * </p>
     * @version 1.0 Initial Version
     * @throws Exception
     *             exception that may occur while setting up parameters
     */
    public MortgageTotalAmount() throws Exception
    {
        super();
        initParams();
        this.calcNumber = "CALC 43.CM";
    }

    /**
     * <p>
     * Description: Given an input object this method determines the appropriate
     * target object and adds the object to it's list of targets (targets).
     * </p>
     * @version 1.0 XS_11.3 Initial Version
     * @param input
     *            Object Input component.
     * @param dcm
     *            CalcMonitor runs the calculation of this Component entity.
     * @throws TargetNotFoundException
     *             exception that may occurs if the target is not found.
     * @see com.basis100.deal.calc.DealCalc#getTarget(java.lang.Object,
     *      com.basis100.deal.calc.CalcMonitor)
     */
    public void getTarget(Object input, CalcMonitor dcm)
            throws TargetNotFoundException
    {
        Collection componentCollec = null;
        try
        {
            int classId = ((DealEntity) input).getClassId();
            
            if (classId == ClassId.COMPONENTMORTGAGE)
            {
                ComponentMortgage componentMtg = (ComponentMortgage) input;
                addTarget(componentMtg);
            }
            else
                if (classId == ClassId.DEAL)
                {
                    Deal deal = (Deal) input;
                    // retrives collection of components associated to the deal
                    componentCollec = this.entityCache.getComponents(deal, srk);
                    ComponentMortgage componentMtg = null;
                    Iterator componentsItr = componentCollec.iterator();
                    while (componentsItr.hasNext())
                    {
                        Component component = (Component) componentsItr.next();
                        // Checks the component type is MTG COMPONENT
                        if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
                        {
                            // Retrives the componentMtg Object for the given
                            // componentId and copyId
                            componentMtg = (ComponentMortgage) this.entityCache
                                    .find(srk, new ComponentMortgagePK(
                                            component.getComponentId(),
                                            component.getCopyId()));
                            addTarget(componentMtg);
                        }
                    }
                }
        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Failed to get Targets for: ");
            msg.append(this.getClass().getName());
            msg.append(":[Input]:");
            msg.append(input.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@input: ");
            msg.append(this.getDoCalcErrorDetail(input));
            logger.debug(msg.toString());
            throw new TargetNotFoundException(e.getMessage());
        }

    }

    /**
     * <p>
     * Description: Performs the calculation based on calculation rules in
     * CALC#43.CM
     * </p>
     * @version 1.0 XS_11.3 Initial Version
     * @param target
     *            Object target component.
     * @see com.basis100.deal.calc.DealCalc#doCalc(java.lang.Object)
     */
    @Override
    public void doCalc(Object target)
    {
        trace(this.getClass().getName());
        ComponentMortgage targetComponentMtg = (ComponentMortgage) target;

        double totalMortageAmount = 0.0d;
        try
        {
            Component component = (Component) this.entityCache.find(srk,
                    new ComponentPK(targetComponentMtg.getComponentId(),
                            targetComponentMtg.getCopyId()));

            Deal deal = (Deal) this.entityCache.find(srk, new DealPK(component
                    .getDealId(), component.getCopyId()));

            if (deal.getMIUpfront().equals("N")
                    && targetComponentMtg.getMiAllocateFlag().equals("Y"))
            {
                totalMortageAmount = targetComponentMtg.getMortgageAmount()
                        + deal.getMIPremiumAmount();
            }
            else
            {
                targetComponentMtg.setMiAllocateFlag("N");
                totalMortageAmount = targetComponentMtg.getMortgageAmount();
            }

            targetComponentMtg
                    .setTotalMortgageAmount(validate(totalMortageAmount));
            targetComponentMtg.ejbStore();

        }
        catch (Exception e)
        {
            StringBuffer msg = new StringBuffer(100);
            msg.append("Benign Failure in Calculation");
            msg.append(this.getClass().getName());
            msg.append(":[Target]:");
            msg.append(target.getClass().getName());
            msg.append("  Reason: ");
            msg.append(e.getMessage());
            msg.append("@Target: ");
            msg.append(this.getDoCalcErrorDetail(target));
            logger.debug(msg.toString());
        }
    }

    /**
     * <p>
     * Description: Initializes the input & target parameters which is called in
     * the constructor.
     * </p>
     * @version 1.0 XS_11.3 Initial Version
     * @throws ParamTypeException
     *             exception that may occur if param type is not valid.
     */

    private void initParams() throws ParamTypeException
    {
        targetParam = new ArrayList();
        inputParam = new ArrayList();
        inputClassTypes = new HashSet();

        addInputParam(new CalcParam(ClassId.DEAL, "MIUpfront"));
        addInputParam(new CalcParam(ClassId.DEAL, "MIPremiumAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "mortgageAmount"));
        addInputParam(new CalcParam(ClassId.COMPONENTMORTGAGE, "miAllocateFlag"));
        addTargetParam(new CalcParam(ClassId.COMPONENTMORTGAGE,
                "totalMortgageAmount", T13D2));
    }

}
