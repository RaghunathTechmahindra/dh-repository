/*
 * Created on May 11, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.deal.commitment;

import MosSystem.Mc;

import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris
 * 
 * This class is an extension of the CCM Class and has functionality specific to
 * Cervus.
 *  
 */
public class CVCCM extends CCM
{

    /**
     * @param srk
     */
    public CVCCM(SessionResourceKit srk)
    {

        super(srk);
    }

    public void sendServicingUpload(SessionResourceKit srk, Deal deal,
            String type, int languageId) throws Exception
    {
        String sysUpload = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                "com.basis100.system.sysupload",
                Mc.SERVICING_UPLOAD_TYPE_COMMITMENT);
        boolean sysMultiUpload = (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                "com.basis100.system.sysmultiupload", "Y")).equals("Y");
        boolean isUpdateServMortgageNum = (PropertiesCache.getInstance()
                .getProperty(deal.getInstitutionProfileId(),"com.basis100.system.updateservmortgagenum", "Y"))
                .equals("Y");
        // Addition parameter for the generation of ServicingMortgageNumber --
        // By BILLY 1Feb2002
        boolean isGenerateServMortgageNum = (PropertiesCache.getInstance()
                .getProperty(deal.getInstitutionProfileId(),"com.basis100.system.generateservicingmortnum",
                        "Y")).equals("Y");

        // Check Upload type matched
        //--> Ticket#371 :: Change for Cervus :: changes to support send @ all
        // stages
        //--> By Billy 27May2004
        if (sysUpload.equalsIgnoreCase(type)
                || sysUpload.equalsIgnoreCase("All"))
        //===========================================================================
        {
            // Check if OK to Upload to servicing system
            if (deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_TRANSFER
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_REFI_EXTERNAL
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_ETO_EXTERNAL
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_CONSTRUCTION
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PORTABLE
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PORTABLE_INCREASE
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PORTABLE_DECREASE
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_REPLACEMENT
                    || deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS)
            {
                try
                {
                    // check if already sent
                    boolean isSend = false;
                    String theServMortgNum = deal.getServicingMortgageNumber();
                    if ((isUpdateServMortgageNum == true)
                            && (theServMortgNum == null || theServMortgNum
                                    .trim().equals("")))
                    {
                        // This is the 1st time to send
                        //  create mortgage servicing number
                        if (isGenerateServMortgageNum == true
                                && isEmpty(deal.getServicingMortgageNumber()))
                            deal.setServicingMortgageNumber(getIPENumber(0,
                                    languageId, deal.getInstitutionProfileId()));

                        // Set to send it out
                        isSend = true;
                    } else
                    {
                        // Check if Multiple send supported
                        if (sysMultiUpload == true)
                            isSend = true;
                    }

                    if (isSend == true)
                    {
                        DocumentRequest dr = DocumentRequest.requestUpload(srk,
                                deal);
                        //update deal history
                        DealHistoryLogger hisLog = DealHistoryLogger
                                .getInstance(srk);
                        String logMsg = hisLog.exportToServicingSystem(deal
                                .getLenderProfileId());
                        hisLog
                                .log(deal.getDealId(), deal.getCopyId(),
                                        logMsg, Mc.DEAL_TX_TYPE_INTERFACE, 
                                        srk.getExpressState().getUserProfileId(), 
                                        deal.getStatusId());
                    }
                } catch (Exception e)
                {
                    SysLogger logger = srk.getSysLogger();
                    logger
                            .error("Error occured requesting upload to servicing for deal id="
                                    + deal.getDealId()
                                    + " (copyId="
                                    + deal.getCopyId() + ")");
                    logger.error(e);
                    throw e;
                }
            }
        }
    }

    protected boolean isEmpty(String str)
    {
        if (str == null)
            return true;

        if (str.trim().length() == 0)
            return true;

        return false;
    }

}