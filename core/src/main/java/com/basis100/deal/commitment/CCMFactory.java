/*
 * Created on May 11, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.deal.commitment;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris
 *
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CCMFactory
{

    public static final String EXPRESS_ENV_CV = "CV";
    public static final String EXPRESS_ENV_NONE = "N";
    
    private HashMap ccmEnvMap = new HashMap();
    private static CCMFactory instance = new CCMFactory();

    public CCMFactory()
    {

        ccmEnvMap.put(EXPRESS_ENV_CV, "com.basis100.deal.commitment.CVCCM");
        ccmEnvMap.put(EXPRESS_ENV_NONE, "com.basis100.deal.commitment.CCM");
    }


    public static CCMFactory getInstance(){

        return instance;
    }


    /**
     * This method is used to obtain a CCM object dependent upon the environment we are in.
     * The method was created because Cervus requires a Helix upload for pre-approvals.
     *
     * @param envString
     * @param srk
     * @return
     * @throws ClassNotFoundException
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public CCM getCCM(String envString, SessionResourceKit srk)
            throws ClassNotFoundException, SecurityException,
            IllegalArgumentException, NoSuchMethodException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException
    {

        Class clazz = null;
        String className = (String) ccmEnvMap.get(envString);
        if (className == null)
            return new CCM(srk);
        clazz = Class.forName(className);
        CCM ccm = (CCM) getObjectInstance(clazz, new Object[] { srk });
        return ccm;
    }



    /**
     * This class uses reflection to create a new instance of the class given in
     * the parameters
     *
     * @param clazz
     * @param params
     * @return
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws IllegalArgumentException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public Object getObjectInstance(Class clazz, Object[] params)
            throws SecurityException, NoSuchMethodException,
            IllegalArgumentException, InstantiationException,
            IllegalAccessException, InvocationTargetException

    {

        Class[] args = null;

        if (params != null)
        {

            args = new Class[params.length];
            for (int i = 0; i < params.length; i++)
            {

                args[i] = params[i].getClass();
            }
        }

        Constructor con = clazz.getConstructor(args);
        return con.newInstance(params);
    }

}
