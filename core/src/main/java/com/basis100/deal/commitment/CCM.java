package com.basis100.deal.commitment;
/**
 * 19/Apr/2005 DVG #DG194 Xceed to MCAP update, next steps
 * @version 1.1 <br>
 * Date: 08/08/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: <br>
 *   Made changes to requestFundingDocs() method <br>
 *     	- Added logic check for the NBC specific lender and call DocumentRequest.NBCrequestSolicitorsPackage
 *     		to generate the NBCSolicitorpackage PDF Document <br>
 * 
 */

import java.util.*;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.deal.validation.DocBusinessRuleExecutor;
import com.basis100.deal.util.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.security.*;
import com.basis100.mail.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.miprocess.*;
import com.basis100.picklist.*;
import com.basis100.deal.history.*;

import MosSystem.Sc;
import MosSystem.Mc;
//  	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docprep.xsl.XSLMerger;
//  	***** Change by NBC Impl. Team - Version 1.1 - End *****//
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;
import com.basis100.deal.calc.*;
import com.filogix.express.email.EmailSender;
import com.filogix.util.Xc;
import com.filogix.externallinks.framework.ServiceConst;

/////// whenever the recommended scenario is set  OR whenever a scenario is created (copy from gold)
///////
/*
      // transfer approval detail from the gold copy
      if (currentGold != null)
      {
          currentGold.transferApproveralHistory(deal);
      }
*/

/**
  * Commitment Change Module
  *
  * Summary:
  *
  * . issueCommitment(Deal deal)
  *
  * .
  *
  **/


public class CCM implements Xc
{
	private SessionResourceKit srk;
  private InstitutionProfile IPE = null;
  private SysLogger logger;

  public CCM(SessionResourceKit srk)
  {
    this.srk = srk;
    logger = srk.getSysLogger();
  }

  // New method to check if any MIRequest is required before Final Approval a deal
  //  i.e. User can determinate if to submit the MI Requet when approve a deal (new requirement)
  public boolean isMIRequestBeforeApproval(Deal deal) throws Exception
  {
     logger.trace("CCM@isMIRequestBeforeApproval-- enter!");
     try
     {
       if (deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL)
       {
         MIProcessHandler mi = MIProcessHandler.getInstance();
         int rcm = mi.determineRcmCommStatus(deal,srk);

         boolean notMIApprovalStatus = false == mi.isMIApprovalStatus(deal.getMIStatusId());

         if (notMIApprovalStatus && ( rcm == mi.C_I_INITIAL_REQUEST_PENDING ||
                                      rcm == mi.C_I_ERRORS_CORRECTED_AND_PENDING ||
                                      rcm == mi.C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M ||
                                      rcm == mi.C_I_CHANGES_PENDING)
             )
         {
            return(true);
         }
         else
         {
            return(false);
         }
      }
    }
    catch(Exception e)
    {
       String msg = e.getMessage();

       if(msg == null) msg = "Unknown Error";

       logger.error("CCM: isMIRequestBeforeApproval: error handling MI due to: " + msg);
       logger.error(e);
    }

    return(false);
  }

  //--Release2.1--//
  //--> Added LanguageId for Multilingual Support
  //--> By Billy 10Dec2002
  public void issueCommitment(Deal deal, int languageId) throws Exception
  {
    issueCommitment(deal, true, languageId);
  }
  /**
  *
  *  Args: deal - the recommended scenario (at final approval time)
  *        isMIRequest - Check and submit MI equest if true
  *
  *   New requirement -- Have user to confirm if the submittion of MIrequests
  *
  **/
  //--Release2.1--//
  //--> Added LanguageId for Multilingual Support
  //--> By Billy 10Dec2002
	public void issueCommitment(Deal deal, boolean isMIRequest, int languageId) throws Exception
  {
      DealPK dealPk = (DealPK)deal.getPk();
      Date now      = new Date();
      //DealHistoryLogger hisLog =  DealHistoryLogger.getInstance(srk);

      logger.trace("CCM@issueCommitment-- enter!");

      // handle MortgageInsurance related requirements if required
      if(isMIRequest == true)
      {
        try
        {
          if (deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL)
          {
            MIProcessHandler mi = MIProcessHandler.getInstance();
            int rcm = mi.determineRcmCommStatus(deal,srk);

            boolean notMIApprovalStatus = false == mi.isMIApprovalStatus(deal.getMIStatusId());


            if (notMIApprovalStatus &&
                (
                  rcm == mi.C_I_INITIAL_REQUEST_PENDING ||
                  rcm == mi.C_I_ERRORS_CORRECTED_AND_PENDING ||
                  rcm == mi.C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M ||
                  rcm == mi.C_I_CHANGES_PENDING
                )
               )
            {
              mi.placeMIRequest(deal, srk, rcm);
            }
          }
        }
        catch(Exception e)
        {
          String msg = e.getMessage();

          if(msg == null) msg = "Unknown Error";

          logger.error("CCM: IssueCommitment: error handling MI due to: " + msg);
          logger.error(e);
        }
      }

      // deal updates
      adoptRecommendedScenario(deal, 0);
      deal.setCommitmentIssueDate(now);

      // update document tracking conditions
      ConditionHandler cd = new ConditionHandler(srk);
      cd.updateForApproval(deal);

      if (deal.getRateLockExpirationDate() == null)
      {
        // that is if not set (it shouldn't be)

        deal.setRateLockExpirationDate(now);
	    }

      if (deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL)
      {
          // pre-approval ...
          deal.setPreApproval("Y");
          deal.setPreApprovalOriginalOfferDate(now);
      }
      else
      {
          // not pre-approval ...

          // Modified to call the new method to handle Servicing Upload -- By BILLY 08Nov2001
          // mortgage servicing number
          //if (isEmpty(deal.getServicingMortgageNumber()))
          //    deal.setServicingMortgageNumber(getIPENumber(0));

          // Added Parameter to control the creation of SourceFirm Reference Number -- BILLY 14March2002
          if((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.system.generatesourcefirmrefnum", "Y")).equals("Y"))
          {
            // allocate source firm profile client number if not already set
            SourceFirmProfile sfp = new SourceFirmProfile(srk);
            sfp = sfp.findByPrimaryKey(new SourceFirmProfilePK(deal.getSourceFirmProfileId()));

            if (isEmpty(sfp.getSfBusinessId()))
            {
              sfp.setSfBusinessId(getIPENumber(1, languageId, deal.getInstitutionProfileId()));
              sfp.ejbStore();
            }
          }

          // Added Parameter to control the creation of SourceOfBusiness Reference Number -- BILLY 14March2002
          if((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.system.generatesobrefnum", "Y")).equals("Y"))
          {
            // allocate source of business (e.g. broker) profile client number if not already set
            SourceOfBusinessProfile sobp = new SourceOfBusinessProfile(srk);
            sobp = sobp.findByPrimaryKey(new SourceOfBusinessProfilePK(deal.getSourceOfBusinessProfileId()));

            if (isEmpty(sobp.getSOBPBusinessId()))
            {
              sobp.setSOBPBusinessId(getIPENumber(1, languageId, deal.getInstitutionProfileId()));
              sobp.ejbStore();
            }
          }

          // Added Parameter to control the creation of Party Reference Number -- BILLY 14March2002
          if((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.system.generatepartyrefnum", "Y")).equals("Y"))
          {
            // allocate client numbers to parties assignred to deal where not already set
            PartyProfile party = new PartyProfile(srk);
            Collection parties = null;
            try
            {
              parties = party.findByDealAndType(dealPk, -1);
            }
            catch (Exception e)
            {
                // no parties (unusual but not fatal)
                parties = new Vector();
            }

            Iterator iParties  = parties.iterator();

            while (iParties.hasNext())
            {
                party = (PartyProfile)iParties.next();

                if (isEmpty(party.getPtPBusinessId()))
                {
                    party.setPtPBusinessId(getIPENumber(1, languageId, deal.getInstitutionProfileId()));
                    party.ejbStore();
                }

                if (party.getPartyTypeId() == 55)
                {
                  // message to Solicitor Network

                  ; /* ALERTR1 --> MOT R1 */
                }
            }
          }

          // Addition parameter for the generation of ClientReferenceNumber -- By BILLY 1Feb2002
          if((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.system.generateclientrefnum", "Y")).equals("Y"))
          {
            // allocate client numbers to borrowers on deal where not already set
            Borrower borrower    = new Borrower(srk, null);
            Collection borrowers = null;
            try
            {
              borrowers = borrower.findByAllOnDeal(dealPk);
            }
            catch (Exception e)
            {
                // no borrowers - should never happen ... but
                borrowers = new Vector();
            }

            Iterator iBorrowers  = borrowers.iterator();

            while (iBorrowers.hasNext())
            {
                borrower = (Borrower)iBorrowers.next();

                if (isEmpty(borrower.getClientReferenceNumber()))
                {
                    borrower.setClientReferenceNumber(getIPENumber(1, languageId, deal.getInstitutionProfileId()));
                    borrower.ejbStore();
                }
            }
          }

          // Addition parameter for the generation of PropertyReferenceNumber -- By BILLY 14March2002
          if((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.system.generatepropertyrefnum", "Y")).equals("Y"))
          {
            // allocate client numbers to borrowers on deal where not already set
            Property property    = new Property(srk, null);
            Collection properties = null;
            try
            {
              properties = property.findByDeal(dealPk);
            }
            catch (Exception e)
            {
                // no properties - should never happen ... but
                properties = new Vector();
            }

            Iterator iProperties  = properties.iterator();

            while (iProperties.hasNext())
            {
                property = (Property)iProperties.next();

                if (isEmpty(property.getPropertyReferenceNumber()))
                {
                    property.setPropertyReferenceNumber(getIPENumber(1, languageId, deal.getInstitutionProfileId()));
                    property.ejbStore();
                }
            }
          }
      }

      // request commitment doc.
      requestCommitmentDoc(srk, deal);

      if(deal.getRateLockExpirationDate() != null) deal.setRateLockExpirationDate(new Date());

      //ALERT: REPEATED CODE SECTION (DONE ABOVE?)
      if(deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL)
      {
        deal.setPreApproval("Y");
        deal.setPreApprovalOriginalOfferDate(deal.getCommitmentIssueDate());
      }
      
		//DA-DEC-UC-1/4/6 starts
		// Automated response to broker (electronic response)
		// requestElectronicResponseToBroker(srk, deal); 
		try {
			MasterDeal masterDeal = new MasterDeal(srk, null, deal.getDealId());
			if (masterDeal.getFXLinkSchemaId() == MasterDeal.FXLINKSCHEMA_FCX_V_1_0) {
				//ESBOutboundQueue queue = new ESBOutboundQueue(srk);
				//queue.create(queue.createPrimaryKey(), ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), deal.getSystemTypeId(), srk.getExpressState().getUserProfileId());
				//queue.setServiceSubTypeid(ServiceConst.SERVICE_SUBTYPE_LOANDECISION_COMMITMENT);
				//queue.ejbStore();
				
				// For Manual Deal originated from Deal Entry screen will have channel Media as 'M' 
			    // Block updating the Deal event status,Condition update status and deal decision for deals created from express.. 
			    // FXP32829 - Dec 06 2011 - S:\Project Repository\CMMI Active Projects\Express 4.2 Release\4 BA - PA Baselines\Data Alignment
				// FSD updated section v1.14
		      	if(deal.getChannelMedia() != null && deal.getChannelMedia().trim().equalsIgnoreCase(Xc.CHANNEL_MEDIA_M )) 
		      		return;
		
		      	//checks DOC Business rule before sending FCX approval and also sending email
				DocBusinessRuleExecutor dbre = new DocBusinessRuleExecutor(srk);
				if( dbre.evaluateRequest(
									deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_APPROVAL).isCreate()){
					
					ESBOutboundQueue queue = new ESBOutboundQueue(srk);
					queue.create(queue.createPrimaryKey(), ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), deal.getSystemTypeId(), srk.getExpressState().getUserProfileId());
					queue.setServiceSubTypeid(ServiceConst.SERVICE_SUBTYPE_LOANDECISION_COMMITMENT);
					queue.ejbStore();
				}				
			} else {
				requestElectronicResponseToBroker(srk, deal);
			}   
		} catch (Exception e) {
			EmailSender.sendAlertEmail("Deal Resolution", e);
			throw e;
		}  
		//DA-DEC-UC-1/4/6 ends
      
      // Send Servicing Upload
      sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_COMMITMENT, languageId);

  }

  /**
    * requestCommitmentDoc:
    *
    * Request commitment document. Typically called at approval but possible at other times, e.g. non-critical change with doc. prep.
    *
    * Args: deal
    **/

  public void requestCommitmentDoc(SessionResourceKit srk, Deal deal) throws Exception
  {
    requestCommitmentDoc(srk, deal, true);
  }

  public void requestCommitmentDoc(SessionResourceKit srk, Deal deal, boolean resetCommitmentIssueDate) throws Exception
  {
      // Product Commitment/Pre-Approval Certificate/Transfer document
      String doc = "{doc not set}";

      try
      {
          boolean approved = deal.getStatusId() == Mc.DEAL_APPROVED;

          DealStatusManager dsm = DealStatusManager.getInstance(srk);

         //post decision and commitment not produced
          boolean post_nc =
          (dsm.getStatusCategory(deal.getStatusId(), srk) == Mc.DEAL_STATUS_CATEGORY_POST_DECISION) &&
          !(TypeConverter.booleanFrom(deal.getCommitmentProduced()));

          if((approved || post_nc))
          {
            if ((deal.getSpecialFeatureId() == 1))    //Generic Pre-App spec
            {
                doc = "Pre-Approval Certificate";
                DocumentRequest.requestPreAppCertificate(srk, deal);
            }
            else   //Generic Commitment Spec
            {
                doc = "Commitment Letter";
                DocumentRequest dr = DocumentRequest.requestCommitment(srk, deal);
            }

            deal.setCommitmentProduced("Y");
            if(resetCommitmentIssueDate)
              deal.setCommitmentIssueDate(new Date());
         }
      }
      catch (Exception e)
      {
          SysLogger logger = srk.getSysLogger();
          logger.error("Error occured requesting " + doc + " for deal id=" + deal.getDealId() + " (copyId=" + deal.getCopyId() + ")");
          logger.error(e);
          throw e;
      }
  }

  /**
    * requestElectronicResponseToBroker:
    *
    * Request (electronic) response to broker - typically (but not necessarilly) at same time commitment letter issued.
    *
    * Args: deal
    **/

  public void requestElectronicResponseToBroker(SessionResourceKit srk, Deal deal) throws Exception
  {
      try
      {
          // Extract to electronic systems
          //-- FXLink Phase II --//
          //--> Send if deal.ChannelMedia = "E"
          if (!isEmpty(deal.getChannelMedia()) && deal.getChannelMedia().equals("E"))
          //==================================
          {
              // Request Automated Response to Broker System
              DocumentRequest.requestApproval(srk, deal);

              DealHistoryLogger hisLog =  DealHistoryLogger.getInstance(srk);

              String logMsg = hisLog.sourceSystemReponse(deal.getSourceOfBusinessProfileId());
              hisLog.log(deal.getDealId(), deal.getCopyId(), logMsg, Mc.DEAL_TX_TYPE_INTERFACE, 
                         srk.getExpressState().getUserProfileId());
          }
      }
      catch (Exception e)
      {
          SysLogger logger = srk.getSysLogger();
          logger.error("Error occured requesting broker response for deal id=" + deal.getDealId() + " (copyId=" + deal.getCopyId() + ")");
          logger.error(e);
          throw e;
      }
  }

  private String constructMIResponse(String parResp){
    String lStr = new String("");
    if(parResp == null){
      //return new String("");
    } else {
      lStr = parResp.trim();
    }
    StringBuffer retBuf = new StringBuffer("");
    if( lStr.equals("") ){return retBuf.toString();}

    int firstPartPos = parResp.indexOf(" ");
    if( firstPartPos == -1 ){
      retBuf.append(lStr);
    } else {
      retBuf.append( lStr.substring(0,firstPartPos) );
    }
    retBuf.append(" ").append(TypeConverter.formattedDate(new Date(),"MMM dd, yyyy 'at' hh:mm:ss") ).append("\n\r");
    retBuf.append("** \n\r");
    retBuf.append("** Deal being re-underwritten \n\r");
    retBuf.append("** MI status set to \' Errors Reported by Insurer \' \n\r");
    retBuf.append("** \n\r");
    retBuf.append("** \n\r");

    return retBuf.toString();
  }

  /**
    * cancelCommitment:
    *
    * Args: deal - gold copy of the deal.
    **/
  public void cancelCommitment (Deal deal) throws Exception
  {
    cancelCommitment(deal, true, 'K');
  }

  public void cancelCommitment (Deal deal, boolean isMIPNumClear) throws Exception
  {
    cancelCommitment(deal, isMIPNumClear, 'K');
  }

  public void cancelCommitment (Deal deal, char miStatusTreatment) throws Exception
  {
    cancelCommitment(deal, true, miStatusTreatment);
  }

  //===========================================================================================
  // Input :
  //  isMIPNumClear - true   : Clear the MI Policy # (default)
  //                  false  : Keep MI Policy #
  //
  //  miStatusTreatment - 'K' : Keep current MIStatus (default)
  //                      'C' : Clear MIStatus
  //                      'P' : Based on the Policy #
  //                            - Policy# <> "" ==> set MIStatus to "Errors Reported by Insurer"
  //                            - else Clear MIStatus
  //============================================================================================
  public void cancelCommitment (Deal deal, boolean isMIPNumClear, char miStatusTreatment) throws Exception
  {
      DealHistoryLogger hLog;
      String msg;
      //========================================================================================
      // the following code has been removed and changed as required by new changes in the spec
      //========================================================================================
      // cancel MI if outstanding
      // MIProcessHandler handler = MIProcessHandler.getInstance();
      // handler.placeMICancellationIfOutstanding(deal, srk);

      if(miStatusTreatment == 'P')
      {
        if(deal.getMIPolicyNumber() != null && !deal.getMIPolicyNumber().trim().equals(""))
        {
          deal.setMIStatusId(Mc.MI_STATUS_ERRORS_RECEIVED_BY_INSURER);
          deal.setMortgageInsuranceResponse(constructMIResponse(deal.getMortgageInsuranceResponse())
             + "\n\r" + deal.getMortgageInsuranceResponse() );
        }
        else
        {
          deal.setMIStatusId(Mc.MI_STATUS_BLANK);
        }
      }
      else if(miStatusTreatment == 'C')
      {
        deal.setMIStatusId(Mc.MI_STATUS_BLANK);
      }

      if(isMIPNumClear)
        deal.setMIPolicyNumber(null);

      //log to deal history
      hLog = DealHistoryLogger.getInstance(srk);
      msg = hLog.statusChange(deal.getStatusId(), "Decision Mod");
      deal.setStatusId(Mc.DEAL_RECEIVED);
      //Added to update the Status Change date -- By Billy 10July2001
      deal.setStatusDate(new Date());
      hLog.log(deal.getDealId(), deal.getCopyId(), msg, Mc.DEAL_TX_TYPE_EVENT, 
               srk.getExpressState().getUserProfileId(), deal.getStatusId());

      // update document tracking conditions
      ConditionHandler cd = new ConditionHandler(srk);
      //// SYNCADD.
      // Requested by Product to preserve the Status and change all Approval ==> Received
      // -- By Billy 15Aug2002
      cd.updateForCommitmentCancelled(deal, true);
      //=================================================================================

      // remove borrower client numbers
      Borrower borrower    = new Borrower(srk, null);
      Collection borrowers = borrower.findByAllOnDeal((DealPK)deal.getPk());
      Iterator iBorrowers  = borrowers.iterator();

      while (iBorrowers.hasNext())
      {
          borrower = (Borrower)iBorrowers.next();

          if (!isEmpty(borrower.getClientReferenceNumber()))
          {
              borrower.setClientReferenceNumber(null);
              borrower.ejbStore();
          }
      }

      // Catherine, BMO to CCAPS, 16Feb05 ---- begin ------------------
      if (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.filogix.deal.copy.lender.notes", "N").equalsIgnoreCase("N"))
      {
        deal.setServicingMortgageNumber(null);
      }
      // Catherine, BMO to CCAPS, 16Feb05 ---- end ------------------

      deal.setReturnDate(null);
      deal.setCommitmentAcceptDate(null);

      // Problem if the Application date doesn't change i.e. Cancellation on the same date as the
      //    Application date ==> No Change on Est Closing date ==> No change on CommitmentExpirationDate
      //    Therefore, if we make it null here it will keep null until we change EstClosiongDate.
      //deal.setCommitmentExpirationDate(null);
      deal.setCommitmentIssueDate(null);
      deal.setCommitmentProduced("N");
      deal.setCommitmentPeriod(0);
      deal.setRateLockExpirationDate(null);
      deal.setPreApprovalConclusionDate(null);
      deal.setPreApprovalOriginalOfferDate(null);

      deal.clearApproveralHistory();  //set approver chain to "0"

      deal.setApplicationDate(new Date());
      // log to deal history
  }

  /**
    * cleanupForDeniedOrCollasped:
    *
    * For a deal denied or collasped ensure that scenarions are locked, etc.
    *
    * Args: deal - recommended copy or gold copy the deal.
    *
    **/

  public void cleanupForDeniedOrCollasped(Deal deal) throws Exception
  {
      // never lock gold!
      if (!deal.getCopyType().equals("G"))
        deal.setScenarioLocked("Y");
      lockScenarios(deal);
  }


  /**
    * denyDeal:
    *
    * Called to deny a deal.
    *
    * Args: deal - recommended copy or gold copy the deal.  If recommended copy is adopted as the
    *              gold.
    *
    **/

 public void denyDeal(Deal deal, int denyReasonId) throws Exception
  {
		  DealHistoryLogger   hLog    = DealHistoryLogger.getInstance(srk);

      // cancel outstanding or approved MI policy
      MIProcessHandler miHandler = MIProcessHandler.getInstance();

      miHandler.placeMICancellationIfOutstanding(deal, srk);

      //QC-Ticket-418- Start - Removes Fee data from DealFee table based on DealId
      miHandler.removeFeeFromDeal(srk, deal);
      //QC-Ticket-418- End      
      
      // ok even if deal is "gold" copy - see method for details
      adoptRecommendedScenario(deal, 1);

      // update/remove doc tracking entries
      //// SYNCADD.
      // As requested by Product that we don't need to get rid of the default condition here
      // -- Modified by Billy 15Aug2002
      //ConditionHandler ch = new ConditionHandler(srk);
      //ch.updateForCommitmentCancelled(deal);
      //====================================================================================

      // update deal status (denied)
      int oldStatus = deal.getStatusId();
      DBA.recordDealStatus(srk, deal, Mc.DEAL_DENIED, -1, -1);
      // Create History Log if Status changed
      if(oldStatus != deal.getStatusId())
      {
        String msg = hLog.statusChange(oldStatus, "Deal Resolution");
        hLog.log(deal.getDealId(), deal.getCopyId(), msg, Mc.DEAL_TX_TYPE_EVENT, 
                 srk.getExpressState().getUserProfileId(), deal.getStatusId());
      }

      deal.setDenialReasonId(denyReasonId);
		  deal.setDecisionModificationTypeId(Mc.DM_NO_MOD_REQUESTED);
  }

  /**
    * collaspeDeal:
    *
    * Called to deny a deal.
    *
    * Args: deal - recommended or gold copy the deal. If recommended copy is adopted as the
    *              gold.
    *
    **/

 public void collaspeDeal(Deal deal) throws Exception
  {
		  DealHistoryLogger   hLog    = DealHistoryLogger.getInstance(srk);

      // cancel outstanding or approved MI policy
      MIProcessHandler miHandler = MIProcessHandler.getInstance();

      miHandler.placeMICancellationIfOutstanding(deal, srk);
      
      //QC-Ticket-418- Start - Removes Fee data from DealFee table based on DealId
      miHandler.removeFeeFromDeal(srk, deal);
      //QC-Ticket-418- End        

      // ok even if deal is "gold" copy - see method for details
      adoptRecommendedScenario(deal, 2);

      // update/remove doc tracking entries
      //// SYNCADD.
      // As requested by Product that we don't need to get rid of the default condition here
      // -- Modified by Billy 15Aug2002
      //ConditionHandler ch = new ConditionHandler(srk);
      //ch.updateForCommitmentCancelled(deal);
      //====================================================================================

      // update deal status (collasped)

      int oldStatus = deal.getStatusId();
      DBA.recordDealStatus(srk, deal, Mc.DEAL_COLLAPSED, Mc.DEAL_RES_CONDITION_DEAL_COLLAPSED, -1);
      // Create History Log if Status changed
      if(oldStatus != deal.getStatusId())
      {
        String msg = hLog.statusChange(oldStatus, "Deal Resolution");
        hLog.log(deal.getDealId(), deal.getCopyId(), msg, Mc.DEAL_TX_TYPE_EVENT, 
                 srk.getExpressState().getUserProfileId(), deal.getStatusId());
      }

		  deal.setDecisionModificationTypeId(Mc.DM_NO_MOD_REQUESTED);
  }



  /** adoptRecommendedScenario:
  *
  * Recommended scenario transformed to gold copy. Actions:
  *
  *    . current Gold copy changed to a scenario
  *    . all unlocked scenarios locked
  *    . recommended scenario marked as gold
  *    . master deal record updated
  *    . new (locked) scenario created as copy of (new) gold
  *
  * Note: all entities manipulated with the exception of the recommended scenario (gold after
  *       the call completes) are stored.
  *
  * Arg: deal - the recommended scenario for the deal
  *
  *      resolutionType - 0 - approval
  *                       1 - denial
  *                       2 - collaspe
  *
  *
  * PROGRAMMER NOTE
  *
  * The deal passed may actually be the gold copy. This occures when the method is called during a denial
  * post commitment. The actions performed in this case are:
  *
  *    . all unlocked scenarios locked (but not likely to be any!)
  *    . new (locked) scenario created as copy of gold
  *
  **/
  public void adoptRecommendedScenario(Deal deal, int resolutionType) throws Exception
  {
  Deal util = new Deal(deal.getSessionResourceKit(), null);

  Collection c = util.findByAllUnlocked(deal.getDealId());

  Iterator ic = c.iterator();

  // get master deal record
  MasterDeal md = new MasterDeal(deal.getSessionResourceKit(), null);
  md = md.findByPrimaryKey(new MasterDealPK(deal.getDealId()));

  //  time stamp string
  String tsString = TypeConverter.stringTypeFrom(new Date());

  while (ic.hasNext())
  {
      util = (Deal)ic.next();

      // skip the recommedned scenaio  (!!! note - could actually be gold when for denial post decision !!!)
      if (deal != null && deal.getCopyId() == util.getCopyId())
        continue;

      util.setScenarioLocked("Y");
      util.setScenarioRecommended("N");

      if (util.getCopyType().equals("G"))
      {
          util.setCopyType("S");
          util.setScenarioNumber(md.nextScenarioNumber());
          util.setScenarioDescription(formGoldScenarioDescription(deal, tsString, resolutionType));
      }

      util.ejbStore();
  }

  deal.setCopyType("G");
  deal.setScenarioLocked("N");
  deal.setScenarioRecommended("Y");
  // update MasterDeal Record
  md.setGoldCopyId(deal.getCopyId());
  md.ejbStore();

  // make new scenario as record of approval event
  int scenarioCid = md.copy(deal.getCopyId());

  Deal dealScenario = new Deal(srk, null);

  dealScenario = dealScenario.findByPrimaryKey(new DealPK(deal.getDealId(), scenarioCid));

  dealScenario.setCopyType("S");
  dealScenario.setScenarioLocked("Y");
  dealScenario.setScenarioRecommended("N");
  dealScenario.setScenarioDescription(formNewGoldDescription(dealScenario, tsString, resolutionType));
  dealScenario.setScenarioNumber(md.nextScenarioNumber());

  dealScenario.ejbStore();

  md.ejbStore();
}


public String formNewGoldDescription(Deal deal, String tsString, int resolutionType)
{
  if (tsString == null)
    tsString = TypeConverter.stringTypeFrom(new Date());

  if (deal.getSpecialFeatureId() == 1)
  {
      switch (resolutionType)
      {
          case 0:
            return "PreApproved at " + tsString;

          case 1:
            return "Denied at " + tsString;

          case 2:
            return "Collasped at " + tsString;
      }

    return "";
  }

  switch (resolutionType)
  {
      case 0:
        return "Approved at " + tsString;

      case 1:
        return "Denied at " + tsString;

      case 2:
        return "Collasped at " + tsString;
  }

  return "";
}

public String formGoldScenarioDescription(Deal deal, String tsString, int resolutionType)
{
  if (tsString == null)
      tsString = TypeConverter.stringTypeFrom(new Date());

  if (deal.getSpecialFeatureId() == 1)
  {
      switch (resolutionType)
      {
          case 0:
            return "Deal data (gold) at PreApproval";

          case 1:
            return "Deal data (gold) at Denial";

          case 2:
            return "Deal data (gold) at Collapse";
      }

      return "";
  }

  switch (resolutionType)
  {
      case 0:
        return "Deal data (gold) at Approval";

      case 1:
        return "Deal data (gold) at Denial";

      case 2:
        return "Deal data (gold) at Collapse";
  }

  return "";
}


  /** lockSenarios:
  *
  * Utility call to lock all open scenarios including the currently recommended scenario
  * (if one).
  *
  * One scenario for usage involves ensuring not deal changes occur during external lender
  * approval review/request prior to final approval of a deal.
  *
  * Args: deal - If not null expected to be the current recommended scenario. It can be provided
  *              to allow update of the lock property without storing the change to the database
  *              (that is, update the lock property but not call ejbStore() for the entity).
  *              In this case it is expected that the entity is undergoing manipulation
  *              in the callers transaction context and (all) updates will be stored (eventually)
  *              by the caller.
  *
  **/

  public void lockScenarios(Deal deal) throws Exception
  {
      Deal util = new Deal(deal.getSessionResourceKit(), null);

      Collection c = util.findByAllUnlocked(deal.getDealId());

      Iterator ic = c.iterator();

      while (ic.hasNext())
      {
          util = (Deal)ic.next();

          // gold never locked!
          if (!util.getCopyType().equals("G"))
            util.setScenarioLocked("Y");

          // skip if matches passed deal
          if (deal != null && deal.getCopyId() == util.getCopyId())
            continue;

          util.ejbStore();
      }
  }


  /** setAsRecommendedScenario:
  *
  * Utility call to mark the current scenario as the recommended scenario.
  *
  * Args: deal - the scenario to be marked as the recommended scenario
  *
  * Notes: All deal copies modified by the call, except the copy passed (if one) are stored.
  *
  *
  **/

  public void setAsRecommendedScenario(Deal deal) throws Exception
  {
      Deal util = new Deal(deal.getSessionResourceKit(), null);

      Collection c = util.findByAllUnlocked(deal.getDealId());

      Iterator ic = c.iterator();

      while (ic.hasNext())
      {
          util = (Deal)ic.next();

          // skip if matches passed deal
          if (deal != null && deal.getCopyId() == util.getCopyId())
            continue;

          util.setScenarioRecommended("N");

          util.ejbStore();
      }

      deal.setScenarioRecommended("Y");
  }







  private InstitutionProfile getInstitutionProfile() throws Exception
  {
      if (IPE != null)
        return IPE;

      IPE = new InstitutionProfile(srk);
      IPE = IPE.findByFirst();

      return IPE;
  }

  /**
  * getIPENumber:
  *
  * Get a number from the Institution Profile record based on type.
  *
  * Arg: Type - 0 = Mortgage Servicing Number
  *             1 = Client Number
  *
  * Result: number (string representation)
  *
  * Notes: Email alert message sent to Sys Adminisrators if running out of
  *        allocated block of numbers is detected.
  *
  *        Institution Profile record updated with new number
  *
  **/
  //--Release2.1--//
  //--> Added LanguageId for Multilingual Support
  //--> By Billy 10Dec2002
  //---------------- FXLink Phase II ------------------//
  //--> Make method public for Automatically generate the Client # when adding new SOB
  //--> By Billy 12Nov2003
  public String getIPENumber(int type, int languageId, int institutionId)  throws Exception
  {
      getInstitutionProfile();

      String num        = null;
      String maxNumber  = null;

      if (type == 0)
      {
          num         = IPE.getCSMortgageNumber();
          maxNumber   = IPE.getESMortgageNumber();
      }
      else // if (type == 1)
      {
          num         = IPE.getCSClientNumber();
          maxNumber   = IPE.getESClientNumber();
      }

      num = incrAndNoticeOnRangeCreep(type, num, maxNumber, 1000, languageId, institutionId);

      if (type == 0)
      {
          IPE.setCSMortgageNumber(num);
      }
      else // if (type == 1)
      {
          IPE.setCSClientNumber(num);
      }

      // update institution profile with the new number
      IPE.ejbStore();

      // for mtg servicing number a check didgit is added

      if (type == 0)
      {
          String was = num;
          num = DealCalcUtil.NewPropertyNumberCheckDigit(num);

          if (num == null)
          {
              String msg = "Failure generating Mortgage Servicing Number - Critical Error.";
              srk.getSysLogger().error(msg);
              num = was;
          }
      }


      return num;
  }

  //--Release2.1--//
  //--> Added LanguageId for Multilingual Support
  //--> By Billy10Dec2002
  private String incrAndNoticeOnRangeCreep(int type, String curr, String max, int range, int languageId, int institutionId) throws Exception
  {
      int len = curr.length();

      int next = 1 + (int)TypeConverter.intTypeFrom (curr);

      int iMax = (int)TypeConverter.intTypeFrom(max);

      // check range creep
      if ((iMax - next) <= range)
      {
          // notify system administrators of ragge creep
          sendNumberCreepAlert(type, languageId, institutionId);
      }

      // since there are currently no actions defined for a (next) number > max value we
      // return the next value even when this occurs -- assumed propblem will never escalate
      // to point!!

      // ... length 8, padding with 0's on left

      String strNum = "0000000000000000000000000000000000000000" + TypeConverter.stringTypeFrom(next);
      int sLen = strNum.length();
      strNum = strNum.substring(sLen-8, sLen);


      return strNum;
  }




  /* sendNumberCreepAlert:
  *
  * Generate alert e-mail message to active system administrator when IP number range creep
  * detected.
  *
  * Arg: Type - 0 = Mortgage Servicing Number
  *             1 = Client Number
  *
  **/

  private boolean clientNumberCreepAlertSent = false;

  //--Release2.1--//
  //--> Added LanguageId for Multilingual Support
  //--> By Billy 10Dec2002
  private void sendNumberCreepAlert(int type, int languageId, int institutionId) throws Exception
  {
      String msgBody = null;

      if (type == 0)
      {
          msgBody = BXResources.getSysMsg("EMAIL_ALERT_RUNNING_SHORT_ON_IP_MTG_SERVICING_NUMBERS", languageId);
      }
      else // if (type == 1)
      {
          // generate mail for this condition only once per usage of a given class instance
          if (clientNumberCreepAlertSent == true)
            return;

          msgBody = BXResources.getSysMsg("EMAIL_ALERT_RUNNING_SHORT_ON_IP_CLIENT_NUMBERS", languageId);
      }

      // get list of active system administrators

      UserProfile util = new UserProfile(srk);

      Vector sysAdminUsers = util.findByUserTypeInGroup(-1, Sc.USER_TYPE_SYS_ADMINISTRATOR, true, institutionId);

      if (sysAdminUsers.size() == 0)
      {
        // none active - locate an inactive one
        sysAdminUsers = util.findByUserTypeInGroup(-1, Sc.USER_TYPE_SYS_ADMINISTRATOR, false, institutionId);
      }

      if (sysAdminUsers.size() == 0)
      {
        srk.getSysLogger().error("Unable to locate profile for an active System Administrator - at least one must be defined.");
        srk.getSysLogger().error("Unable to generate automated E-Mail message to System Administrators");
        srk.getSysLogger().error("Intended Message: <" +  msgBody + ">");

        return;
      }

      // request E-mail generation

      try
      {

        SimpleMailMessage message = new SimpleMailMessage();
        message.makeDefault("warning");
        message.setText(msgBody);

        ProfileMailAddresser pma = new ProfileMailAddresser(srk);
        pma.addressMessage(message, new ArrayList(sysAdminUsers));

        message.send(null);
      }
      catch(MailException me)
      {
        srk.getSysLogger().error("Unable to notify System Administrator due to mailException.");
        srk.getSysLogger().error("Unable to generate automated E-Mail message to System Administrators");
        srk.getSysLogger().error("Intended Message: <" +  msgBody + ">");

        srk.getSysLogger().error(me);
      }

  }

  private boolean isEmpty(String str)
  {
      if (str == null)
        return true;

      if (str.trim().length() == 0)
        return true;

      return false;
  }

  // New rule to check if the Cancel, Deny and Collapse action should be continue
  public boolean checkIfActionAllowed(Deal deal)
  {
    if(deal.getMIPolicyNumber() == null || deal.getMIPolicyNumber().trim().equals(""))
    {
      switch(deal.getMIStatusId())
      {
      // Action not allowed if the MIStatus match the follow
        case Mc.MI_STATUS_INITIAL_REQUEST_PENDING         :
        case Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED       :
        case Mc.MI_STATUS_ERRORS_CORRECTED_AND_PENDING    :
        case Mc.MI_STATUS_CORRECTIONS_SUBMITTED           :
        {
          return false;
        }

      // Action allowed but have to reset the MIStatus to BLANK
        case Mc.MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER:
        case Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER:
        {
          // Reset MIStatusid
          try{
            srk.beginTransaction();
            // Reset all copies ]
            int dealId = deal.getDealId();
            MasterDeal md = new MasterDeal(srk, null, dealId);
						Collection copyIds = md.getCopyIds();
						Iterator iCopy = copyIds.iterator();
            int copyId = 0;
     	      Deal nextDeal = null;
						for(;iCopy.hasNext();)
						{
              copyId = ((Integer)iCopy.next()).intValue();
              nextDeal = new Deal(srk, null, dealId, copyId);
              nextDeal.setMIStatusId(Mc.MI_STATUS_BLANK);
              nextDeal.ejbStore();
						}
            srk.commitTransaction();
            return true;
          }
          catch(Exception e)
          {
            srk.getSysLogger().error("CCM: @checkIfActionAllowed problem when resetting the MIStatusId - "
                + e.getMessage());
            return true;
          }
        }
        default:
        {
          return true;
        }
      } // switch
    } // if
    return true;
  }


 /** #DG724 rewritten
	 *
	 * New method to handle Servicing Upload -- By Billy 08Nov2001
	 * 
	 * @param srk
	 * @param deal
	 * @param type
	 * @param langId
	 * @throws Exception
	 */
	public void sendServicingUpload(SessionResourceKit srk, Deal deal,
		String type, int langId) throws Exception {
		
		final PropertiesCache pc = PropertiesCache.getInstance();
		String sysUpload = pc.getProperty(deal.getInstitutionProfileId(), COM_BASIS100_SYSTEM_SYSUPLOAD, Mc.SERVICING_UPLOAD_TYPE_COMMITMENT);

		//#DG724 rewritten
		//Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());		//#DG724
		// Check Upload type matched
		//--> Ticket#371 :: Change for Cervus :: changes to support send @ all stages
		//--> By Billy 27May2004
		if (!(sysUpload.equalsIgnoreCase(type) || sysUpload.equalsIgnoreCase("All")))
			return;

		logger.trace("sysUpload="+sysUpload+";deal pk="+deal.getPk()+";purp.id="+deal.getDealPurposeId());
		// Check if OK to Upload to servicing system
		if(!checkStatus(deal))
			return;
		
		boolean sysMultiUpload = pc.getProperty(deal.getInstitutionProfileId(), COM_BASIS100_SYSTEM_SYSMULTIUPLOAD, "Y").equals("Y");
		boolean isUpdateServMortgageNum = pc.getProperty(deal.getInstitutionProfileId(), COM_BASIS100_SYSTEM_UPDATESERVMORTGAGENUM, "Y").equals("Y");
		// Addition parameter for the generation of ServicingMortgageNumber --By BILLY 1Feb2002
		boolean isGenerateServMortgageNum = pc.getProperty(deal.getInstitutionProfileId(), COM_BASIS100_SYSTEM_GENERATESERVICINGMORTNUM, "Y").equals("Y");
		String theServMortgNum = deal.getServicingMortgageNumber();
		
		try {
			if ((isUpdateServMortgageNum) && isEmpty(theServMortgNum)) {
				// This is the 1st time to send
				//  create mortgage servicing number
				if (isGenerateServMortgageNum)
					deal.setServicingMortgageNumber(getIPENumber(0, langId, deal.getInstitutionProfileId()));
			}
			else {
				// Check if Multiple send supported
				if (!sysMultiUpload)
					return;
			}
			
			//DocumentRequest dr = 
			DocumentRequest.requestUpload(srk, deal);
			//update deal history
			DealHistoryLogger hisLog = DealHistoryLogger.getInstance(srk);
			String logMsg = hisLog.exportToServicingSystem(deal.getLenderProfileId());
			hisLog.log(deal.getDealId(), deal.getCopyId(), logMsg,
					Mc.DEAL_TX_TYPE_INTERFACE, srk.getExpressState().getUserProfileId(), 
					deal.getStatusId());
		}
		catch (Exception e) {
			logger.error("Error occured requesting upload to servicing for deal id="
							+ deal.getDealId() + " (copyId=" + deal.getCopyId() + ")");
			logger.error(e);
			throw e;
		}
	}

	/** 	//#DG724 
	 * check if this deal is supposed to trigger upload
	 * 
	 * @param deal 
	 * @return true is upload is to be triggered
	 */
	protected boolean checkStatus(Deal deal) {
		
		final String lenderEnvrId = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_ENVIRONMENT, "N");

		final int purpId = deal.getDealPurposeId();
		final int specialFeatureId = deal.getSpecialFeatureId();
		if(lenderEnvrId.equals(LENDER_ENVR_ID.CERVUS))	{
			switch (purpId) {
				case Mc.DEAL_PURPOSE_PURCHASE:
				case Mc.DEAL_PURPOSE_TRANSFER:
				case Mc.DEAL_PURPOSE_REFI_EXTERNAL:
				case Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT:
				case Mc.DEAL_PURPOSE_ETO_EXTERNAL:
				case Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT:
				case Mc.DEAL_PURPOSE_CONSTRUCTION:
				case DEAL_PURPOSE_ASSUMPTION:			//added
				case DEAL_PURPOSE_INCREASED_ASSUMPTION:			//added 
				case Mc.DEAL_PURPOSE_PORTABLE:
				case Mc.DEAL_PURPOSE_PORTABLE_INCREASE:
				case Mc.DEAL_PURPOSE_PORTABLE_DECREASE:
				case DEAL_PURPOSE_COVENANT_CHANGE:							//added				
				case Mc.DEAL_PURPOSE_REPLACEMENT:
				case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS:					
					return true;
				default:
					// old code
					//if (deal.getSpecialFeatureId() != SPECIAL_FEATURE_PRE_APPROVAL)
					//	return;		
					return specialFeatureId == SPECIAL_FEATURE_PRE_APPROVAL;						
			}				
		}
		else	{
			// old code
			//if ((deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL) && (
			//	deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE ||

			if(specialFeatureId == SPECIAL_FEATURE_PRE_APPROVAL)
				return false;

			switch (purpId) {
				case Mc.DEAL_PURPOSE_PURCHASE:
				case Mc.DEAL_PURPOSE_TRANSFER:
				case Mc.DEAL_PURPOSE_REFI_EXTERNAL:
				case Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT:
				case Mc.DEAL_PURPOSE_ETO_EXTERNAL:
				case Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT:
				case Mc.DEAL_PURPOSE_CONSTRUCTION:
				case Mc.DEAL_PURPOSE_PORTABLE:
				case Mc.DEAL_PURPOSE_PORTABLE_INCREASE:
				case Mc.DEAL_PURPOSE_PORTABLE_DECREASE:
				case Mc.DEAL_PURPOSE_REPLACEMENT:
				case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS:					
					return true;
				default:					
			}
		}
		
		return false;
	}

  /**
    * requestDisbursmentLetter:
    *
    * Args: deal
    **/
  public void requestDisbursmentLetter(SessionResourceKit srk, Deal deal) throws Exception
  {
      try
      {
          // Request Disbursment Letter
          DocumentRequest.requestDisbursmentLetter(srk, deal);

          DealHistoryLogger hisLog =  DealHistoryLogger.getInstance(srk);

          //String logMsg = hisLog.sourceSystemReponse(deal.getSourceOfBusinessProfileId());
          String logMsg = "Disbursment Letter Submitted";
          hisLog.log(deal.getDealId(), deal.getCopyId(), logMsg, Mc.DEAL_TX_TYPE_INTERFACE, 
                     srk.getExpressState().getUserProfileId());

      }
      catch (Exception e)
      {
          SysLogger logger = srk.getSysLogger();
          logger.error("Error occured requesting Disbursment Letter for deal id=" + deal.getDealId() + " (copyId=" + deal.getCopyId() + ")");
          logger.error(e);
          throw e;
      }
  }

  /**
    * requestFundingDocs:
    *
    * -- Wire Transfer Doc + Data Entry Doc
    *
    * Args: deal
    **/
  public void requestFundingDocs(SessionResourceKit srk, Deal deal) throws Exception
  {
      try
      {
          // Request Wire Transfer
          DocumentRequest.requestWireTransfer(srk, deal);
          // Request Data Entry
          DocumentRequest.requestDataEntry(srk, deal);

          //Request IA Data Sheet - For GE Multi Investor
           if (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
		   		"com.filogix.environment", "N").equalsIgnoreCase("GE"))
	     DocumentRequest.requestGemoneyIaUpload(srk, deal);


          /* #DG194
          // ----------------- Catheirine, XD to MCAP --------------------------------------------------------------
          if (PropertiesCache.getInstance().getProperty("com.filogix.docprep.generate.funding.upload", "N").equalsIgnoreCase("Y")){
            srk.getSysLogger().debug("Requesting MCAP funding upload....");
            DocumentRequest.requestFundingUpload(srk, deal);
            deal.setFundingUploadDone("Y");
            srk.getSysLogger().debug("Requesting MCAP funding upload done.");
          }
          // ----------------- Catherine, XD to MCAP end -----------------------------------------------------------*/

          DealHistoryLogger hisLog =  DealHistoryLogger.getInstance(srk);

          //String logMsg = hisLog.sourceSystemReponse(deal.getSourceOfBusinessProfileId());
          String logMsg = "WireTransfer & DataEntry docs. Submitted";
          hisLog.log(deal.getDealId(), deal.getCopyId(), logMsg, Mc.DEAL_TX_TYPE_INTERFACE, 
                     srk.getExpressState().getUserProfileId());

      }
      catch (Exception e)
      {
          SysLogger logger = srk.getSysLogger();
          logger.error("Error occured requesting Funding docs for deal id=" + deal.getDealId() + " (copyId=" + deal.getCopyId() + ")");
          logger.error(e);
          throw e;
      }
  }
}


