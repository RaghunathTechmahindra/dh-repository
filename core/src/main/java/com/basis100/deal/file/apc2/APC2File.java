package com.basis100.deal.file.apc2;

import java.io.*;
import java.util.*;


public class APC2File
{
  private BufferedReader file;

  /**
   *  a list of Sub string APC2Objects in this APC2File
   */
  private List objects;

  /**
   *  constructs an empty APC2File object
   */
  public APC2File()
  {
    objects = new ArrayList();
  }

  /**
   *  reads an APC2 format file and creates a list of APC2Objects each with a list of
   * components.
   */
  public void load(String filename) throws APC2Exception
  {
    try
    {
      file = new BufferedReader(new FileReader(filename));
    }
    catch(IOException io)
    {
       throw new APC2Exception("Error while accessing file: \n" + io.getMessage());
    }
    try
    {
     // APC2Util.stripFile(filename,"\n\r");

      while(true)
      {
         APC2Object object = readObject();

         if(object == null) break;

         objects.add(object);
      }
    }
    catch(Exception e)
    {
      throw new APC2Exception("Error Occurred in the APC2 File load/parse process.");
    }

  }


  private APC2Object readObject() throws Exception
  {     
     int eof = 0;

     //object type length..............................
     char[] len = new char[5];   //always 5 bytes
     eof = file.read(len,0,5);   //read the object type length
     if(eof == -1) return null;
     String objectTypeLengthStr = new String(len);

     int objectTypeLength = Integer.parseInt(objectTypeLengthStr);

     char[] type = new char[objectTypeLength];    // read the object type
     eof = file.read(type,0,objectTypeLength);
     if(eof == -1) return null;
     String objectType = new String(type);


     char prefix = (char)file.read();     //read the object len prefix
     if(prefix == -1) return null;
     int objectLengthPrefix = Prefix.valueOf(prefix);

     len = new char[objectLengthPrefix];
     eof = file.read(len,0,objectLengthPrefix);    //read the object len
     if(eof == -1) return null;
     String oblen = new String(len);
     int objectLength = Integer.parseInt(oblen);

     prefix = (char)file.read();  //read the object name prefix
     if(eof == -1) return null;
     int namePrefix = Prefix.valueOf(prefix);

     len = new char[namePrefix];
     eof = file.read(len,0,namePrefix);
     if(eof == -1) return null;
     String objectName = new String(len);

     //read the object - already read  ===> the object includes namePrefix and name and components
     len = new char[objectLength - (namePrefix + 1)];
     eof = file.read(len,0,objectLength - (namePrefix + 1));
     if(eof == -1) return null;
     String object = new String(len);
     APC2Object current =  new APC2Object(objectType,objectName,object);
     
     return current;
  }

  public void add(APC2Object object)
  {
    if(objects == null)objects = new ArrayList();

    objects.add(object);
  }
   /**
   *  @return a list APC2Objects contained by this APC2File
   */
  public List getObjects()
  {
    return this.objects;
  }


  public String toString()
  {
    StringBuffer buffer = new StringBuffer();

    ListIterator li = this.objects.listIterator();

    while(li.hasNext())
    {
      APC2Object current = (APC2Object)li.next();
      buffer.append("[" + current.getName() + "]\n");
      buffer.append(current.toString());
    }

    return new String(buffer);
  }
  
  public String toAPC2String()
  {
     StringBuffer buffer = new StringBuffer();
     ListIterator li = objects.listIterator();

     while(li.hasNext())
     {
       APC2Object current = (APC2Object)li.next();

       buffer.append(current.toAPC2String());

     }
     
     return new String(buffer);
  }

}


