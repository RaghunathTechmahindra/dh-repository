package com.basis100.deal.file.apc2;

import java.io.*;
import java.util.*;
import com.basis100.deal.util.*;

public class APC2Object
{
  /**
   *  a list of Sub string APC2Components in this APC2Object
   */
  private List components;

  private String type;
  private String name;
  private char[] internal;
  private CharArrayReader reader;

  /**
   *  constructs an APC2Object of type and name provided. Parses APC2Component list
   *  from the provided character array.
   */
  public APC2Object(String type, String name, char[] obj) throws Exception
  {
    this.components = new ArrayList();
    this.type = type;
    this.name = name;
    this.internal = obj;

    parseInternal();

  }

  /**
   *  constructs an APC2Object of type and name provided. Parses APC2Component list
   *  from the provided String.
   */
  public APC2Object(String type, String name, String obj) throws Exception
  {
    this(type,name,obj.toCharArray());
  }

  public APC2Object(){}
   /**
   *  constructs an APC2Object of type and name and component list provided.
   */
  public APC2Object(String type, String name, List componentList)
  {
     this.type = type;
     this.name = name;
     this.components = new ArrayList(componentList);
  }
   /**
   *  constructs an APC2Object (of default type "T2") with the name and component list provided.
   */
  public APC2Object(String name, List componentList)
  {
     this.type = "T2";
     this.name = name;
     this.components = new ArrayList(componentList);
  }
   /**
   *  constructs an APC2Object of default type "T2",
   *  default name "ADD_CLIENT" and component list provided.
   */
  public APC2Object(List componentList)
  {
     this.type = "T2";
     this.name = "ADD_CLIENT";
     this.components = new ArrayList(componentList);
  }

  /**
   *  constructs an APC2Object with an empty component list. The Object has
   *  a default type "T2"  and the name provided.
   */
  public APC2Object(String name)
  {
     this.type = "T2";
     this.name = name;
     this.components = new ArrayList(89);
  }
   /**
   *  adds a child component to the component list
   */
  public void add(APC2Component component)
  {
    if(components == null)components = new ArrayList();

    components.add(component);
  }

  private void parseInternal() throws Exception
  {
    reader = new CharArrayReader(internal);

    while(true)
    {
       APC2Component component = readComponent();

       if(component == null) break;

       components.add(component);
    }
  }
  /**
   * reads a child component and adds it to the component list
   */
  private APC2Component readComponent() throws Exception
  {
      int eof = 0;
      int prefix;
      char[] buffer;

      if(internal.length == 0) return null;

      prefix = reader.read();
      if(prefix == -1) return null;
      int componentLengthPrefix = Prefix.valueOf(prefix);

      buffer = new char[componentLengthPrefix];
      eof = reader.read(buffer,0,componentLengthPrefix);
      if(eof == -1) return null;
      String componentLengthStr = new String(buffer);
      int componentLength = Integer.parseInt(componentLengthStr);

      buffer = new char[4];
      eof = reader.read(buffer,0,4);
      if(eof == -1) return null;
      String componentType = new String(buffer);

      prefix = reader.read();
      if(prefix == -1) return null;
      int componentNamePrefix = Prefix.valueOf(prefix);

      buffer = new char[componentNamePrefix];
      eof = reader.read(buffer,0,componentNamePrefix);
      if(eof == -1) return null;
      String componentName = new String(buffer);

      //"entire component len" seems to refer to name,nameprefix,type

      buffer = new char[componentLength - (componentNamePrefix + 5)];
      eof = reader.read(buffer,0,componentLength - (componentNamePrefix + 5));
      if(eof == -1) return null;
      
      APC2Component current = new APC2Component(this, componentType,componentName,buffer);

      return current;
  }

  /**
   *  gets the name of this APC2Object
   */
  public String getName(){return this.name;}

   /**
   * @return a printable *.ini format representation of this APC2Object
   * <pre>
   * the format used is as follows:
   * [componentName]
   * field.toString
   * field.toString
   *
   * [componentName]
   * field.toString
   * field.toString
   * </pre>
   * @see com.basis100.deal.file.apc2.APC2Component
   */
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();

    ListIterator li = this.components.listIterator();

    while(li.hasNext())
    {
      APC2Component current = (APC2Component)li.next();
      buffer.append(APC2Util.getNewline() + "[" + this.getName() + ":" + current.getName() + "]" + APC2Util.getNewline());
      buffer.append(current.toString());

    }

    return new String(buffer);
  }

  public String getType(){return this.type;}

  public void setType(String type){this.type = type;}

  private StringBuffer assembleComponents()
  {
    StringBuffer buffer = new StringBuffer();

    ListIterator li = components.listIterator();

     while(li.hasNext())
     {
       APC2Component current = (APC2Component)li.next();
       buffer.append(current.toAPC2String());
     }

    return buffer;

  }

  private String makeObjectTypeLen()
  {
     String len = TypeConverter.stringTypeFrom(this.type.length());

     char[] lench = len.toCharArray();
     char zero = '0';
     char[] out = new char[5];

     int count = len.length() - 1;

     for(int i = 4; i >= 0; i--)
     {
       if(count >= 0) out[i] = lench[count--];
       else out[i] = zero;
     }

     return new String(out);
  }

  /**
   * @return a String representation of this component in APC2 format
   */
   
  public String toAPC2String()
  {
     StringBuffer buffer = new StringBuffer();
     buffer.append(makeObjectTypeLen());
     buffer.append(getType());
     
     StringBuffer temp = assembleComponents();

     //object len include name + nameprefix + components
     int len = temp.length() + name.length() + 1;

     //append object length prefix:
     buffer.append(Prefix.getPrefixFor(len));
     //append object length:
     buffer.append(TypeConverter.stringTypeFrom(len)) ;
     //append name prefix
     buffer.append(Prefix.getPrefixFor(this.name));
     //append name
     buffer.append(this.name);
     //append the components
     buffer.append(temp);
     temp = null;

     return new String(buffer);
  }

}


