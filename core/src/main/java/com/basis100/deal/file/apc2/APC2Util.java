package com.basis100.deal.file.apc2;

import java.io.*;

public class APC2Util
{
  private static String newline;


  public static String parseObjectName(String in) throws APC2Exception
  {
    int index = -2;

    if(in == null || (index = in.indexOf(':')) == -1)
    {
       throw new APC2Exception("Invalid Object:Component header in file");
    }

    return in.substring(0,index);

  }

  public static String parseComponentName(String in) throws APC2Exception
  {
    int index = -2;

    if(in == null || (index = in.indexOf(':')) == -1)
    {
       throw new APC2Exception("Invalid Object:Component header in file");
    }

    return in.substring(index+1,in.length());
  }

  /**
   *  Provides the platform independent new line
   *  @return the newline character as a String
   */
  public static String getNewline()
  {
     if(newline == null)  newline = System.getProperty("line.separator");
     return newline;
  }

  /**
   *  counts the number of decimal places in a long value.
   *  used for providing prefix information in apc2 printing
   *  @return the number of decimal places in the input parameter
   *  @param a long value
   */
  public static int countDecimalPlacesIn(long value)
  {
     int count = 0;
     value = Math.abs(value);

     if(value == 0) return 1;

     while(value > 0)
     {
       value = Math.abs(value/10);
       count++;
     }

     return count;
   }

}


