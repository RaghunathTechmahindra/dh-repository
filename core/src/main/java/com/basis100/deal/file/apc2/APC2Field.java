package com.basis100.deal.file.apc2;

import java.sql.*;
import java.util.Vector;
import java.util.Enumeration;
import com.basis100.deal.util.*;


public class APC2Field
{
  private String name;
  private String data;
   /**
   *  the object form of the sub string container of this object
   */
  private APC2Component parent;

  private String newline;
  /**
   *  constructs an APC2field with the parent, name and data provided. The data
   *  is passed as a character array
   */
  public APC2Field(APC2Component parent, String name, char[] dataArray)
  {
     this.name = name;
     this.parent = parent;
     this.data = new String(dataArray);
     this.data = data.trim();
     this.newline = System.getProperty("line.separator");
  }
  /**
   *  constructs an APC2field with the name and string data provided.
   */
  public APC2Field(String name, String data)
  {
     this.name = name;
     this.data = data.trim();
     this.newline = System.getProperty("line.separator");
  }

  public APC2Field(){}

  /**
   * gets the total length of theis field
   * @return the length of the name=value String as an int
   */
  public int getTotalLength()
  {
     int ret = 0;
     if(name != null)ret = name.length();
     if(data != null)ret += data.length();

     return ++ret;
  }
   /**
   * gets the name of this field
   * @return a String value for the name of this object
   */
  public String getName(){return this.name;}

    /**
   * @return a String representation of this field suitable for use
   * in an *.ini type file
   * <pre>
   * the format used is as follows:
   * name=value
   * </pre>
   */
  public String toString()
  {
    StringBuffer buf = new StringBuffer(this.name + "=" + data + newline );
    return new String(buf);
  }

  /**
  *  @return true if equals APC2Object with the same name else return false;
  */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    else if(object == null || getClass() != object.getClass() )
      return false;

    APC2Field other  = (APC2Field)object;
    boolean retval = false;

    if(this.name.equals(other.getName())) retval = true;

    return retval;
  }

   /**
   * @return a String representation of this field in APC2 format
   * @see com.basis100.deal.file.apc2.APC2Component
   */
  public String toAPC2String()
  {
     StringBuffer buffer = new StringBuffer();

     int totalLength = getTotalLength();
     buffer.append(Prefix.getPrefixFor((long)totalLength));
     buffer.append(TypeConverter.stringTypeFrom(totalLength));
     buffer.append(Prefix.getPrefixFor(name));
     buffer.append(name);
     buffer.append(data);
     return new String(buffer);
  }
} 


