package com.basis100.deal.file.apc2;

import java.io.*;
import java.util.*;
import com.basis100.util.file.*;
import com.basis100.log.*;
//import com.basis100.deal.*;
/**
 *   Convert a file in ini format to one in apc2 format. The ini file should represent a single APC2Object
 *   which correlates to one deal.
 *   The ini file must be in the format:
 *   <pre><b>
 *   [Apc2ComponentName]
 *   fieldname=data
 *   fieldname=data .....etc
 *   </b></pre><br>
 *
 *   <h3>File Naming Rules:<h3>
 *   the outbound file will be named with the following string:<br>
 *   prefix stipulated + input_file_name + extension stipulated.
 *
 */

public class IniPacker
{
   private IniDataView reader;
   private APC2File file;
   private boolean isPacked = false;
   private String target;
   private String prefix = "";
   private String extension = "";



/**
 *   Construct an APC2Packer.
 *   @param apc2Name the file to unpack
 *   @param unpackDir destinationDirectory for ini format file(s)
 *   @param prefix the prefix for the file name
 *   @param fileExt the outbound filename extension
 *   @param clarica a boolean indicating whether this file is a Clarica file to be
 *   unpacked in order
 */
  public IniPacker(String infile)
  {
     this.reader = new IniDataView();
     this.target = infile;
  }

  public void pack(String outfile)throws APC2Exception
  {

    try
    {
      if(!reader.loadIniFile(target)) throw new Exception("Failed to load file: " + target);

      List objects = new ArrayList(createObjects());
      this.file = new APC2File();

      ListIterator li = objects.listIterator();

      while(li.hasNext())
      {
        file.add((APC2Object)li.next());
      }

      this.isPacked = true;
      this.printTo(outfile);
    }
    catch(IOException io)
    {
      String msg = "Error printing packed file: \n" + io.getMessage();
      throw new APC2Exception(msg);
    }
    catch(Exception e)
    {
      String msg = "APC2 Pack Failure: " + e.getMessage();
      throw new APC2Exception(msg);
    }

  }


/**
 *  Prints the contents of this APC2Packer to the named
 *  file in APC2 format.
 *  @param  the full path and name of the file
 *  @throws IOException if an error occurs
 */
  private void printTo(String file) throws IOException
  {
     printTo(new PrintWriter(new FileWriter(file)));
  }

/**
 *  Prints the contents of this APC2Packer in APC2 format
 *  @param  the file to print to
 *  @throws IOException if an error occurs
 */
  private void printTo(File file) throws IOException
  {
     printTo(new PrintWriter(new FileWriter(file)));
  }

/**
 *  Prints the contents of this APC2Packer in APC2 format
 *  @throws IOException if an error occurs
 */
  private void printTo(PrintWriter pw) throws IOException
  {
    BufferedWriter writer = new BufferedWriter(pw);

    if(isPacked)
    {
      writer.write(file.toAPC2String());
      writer.flush();
    }
  }
/**
 *  Prints the contents of this APC2Packer in APC2 format
 *  @throws IOException if an error occurs
 */
  private void printTo(PrintStream ps) throws IOException
  {
    printTo(new PrintWriter(ps));

  }

  private Collection createObjects()throws APC2Exception
  {
    Vector names = reader.getSectionNames();
    int len = names.size();
    Set checkSet = new HashSet(17);
    Map objMap = new HashMap(17);

    for(int i = 0; i < len; i++)
    {
      SectionData current = reader.getSectionAt(i);

      String oName = APC2Util.parseObjectName(current.getSectionName());
      String cName = APC2Util.parseComponentName(current.getSectionName());

      if(checkSet.add(oName))
      {
        objMap.put(oName,new APC2Object(oName));
      }

      List fields = createFields(current);
      APC2Object curObj = (APC2Object)objMap.get(oName);
      curObj.add(new APC2Component(cName,fields));
    }

     return objMap.values();
  }

  private List createFields(SectionData data)
  {
    List fields = new ArrayList(data.size());

    Vector fieldNames = data.getKeys();

    Enumeration e = fieldNames.elements();

    while(e.hasMoreElements())
    {
      String fieldname = (String)e.nextElement();
      String value     = data.getValueFor(fieldname);

      fields.add(new APC2Field(fieldname,value));

    }
    return fields;
  }

  private IniDataView getReader()
  {
    return this.reader;
  }

}


