package com.basis100.deal.file.apc2;

import com.basis100.deal.util.*;

public class Prefix
{
   private static String prefixString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   private static int RANGE_MAX = 36;



   public static int valueOf(String prefix)
   {
       return prefixString.indexOf(prefix);
   }

   public static int valueOf(char prefix)
   {
       return prefixString.indexOf(prefix);
   }

   public static int valueOf(int prefix)
   {
       return valueOf((char)prefix);
   }

   /**
    *  gets the appropriate prefix for a given String length
    *  @return the prefix as a String or null if the parameter exceeds prefix range
    */
   public static String getPrefixFor(String value)
   {
      int len = value.length();

      if(len <= RANGE_MAX) return String.valueOf(prefixString.charAt(len));
      else return null;
   }

   /**
    *  gets the appropriate prefix for a given String length
    *  @return the prefix as a String or null if the parameter exceeds prefix range
    */
   public static String getPrefixFor(long value)
   {
      int len = APC2Util.countDecimalPlacesIn(value);

      if(len <= RANGE_MAX) return String.valueOf(prefixString.charAt(len));
      else return null;
   }


 

   public static void main(String args[])
   {
      
   }
}


