package com.basis100.deal.file.apc2;
import java.io.*;
import com.basis100.log.*;
import com.basis100.util.file.*;
/**
 * Provides a simplified interface to an instantiated SysLog 
 */
public class APC2ErrorLog
{
  private static APC2ErrorLog instance;
  private static SysLogger logger;

  private APC2ErrorLog()
  {
    SysLog.init(".\\properties\\");
    logger = SysLog.getSysLogger("APC2 System");
  }
   /**
   *  gets the sole instance of this ErrorLog wrapper
   */
  public static APC2ErrorLog getInstance()
  {
    if(instance == null) instance = new APC2ErrorLog();

    return instance;
  }

  public static void log(String value)
  {
    logger.error(value);
  }
}