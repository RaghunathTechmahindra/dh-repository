package com.basis100.deal.file.apc2;

import java.io.*;
import java.util.*;
import com.basis100.deal.util.*;
import com.basis100.util.file.*;

/**
*  This APC2Component class represents the APC2 component sub string as an object.
*  The component consists of a name, type and string representations of fields
*/

public class APC2Component
{
  /**
   *  a list of Sub string APC2fields in this APC2Component
   */
  private List fields;
  /**
   *  the object form of the sub string container of this object
   */
  private APC2Object parent;

  private String type;
  private String name;
  private char[] internal;
  private CharArrayReader reader;

  /**
   *  constructs an APC2Object of type and name provided. Parses APC2Field list
   *  from the provided character array.
   */
  public APC2Component(APC2Object parent, String type, String name, char[] data)
  {
     this.type = type;
     this.name = name;
     this.parent = parent;
     this.internal = data;
     this.fields = new ArrayList();

     try
     {
        parseInternal();
     }
     catch(Exception e)
     {
       e.printStackTrace();
     }
  }
  /**
   *  constructs an APC2Object of type and name provided. Parses APC2Field list
   *  from the provided String data.
   */
  public APC2Component(APC2Object parent, String type, String name, String data)
  {
    this(parent,type,name,data.toCharArray());
  }
  /**
   *  constructs an APC2Object of type: "DATA" with the name and list of fields provided.
   */
  public APC2Component(String name,List fields)
  {
     this(name,"DATA",fields);
  }
 /**
   *  constructs an APC2Object of type, name and list of fields provided.
   */
  public APC2Component(String name, String type,List fieldList)
  {
    this.name = name;
    this.type = type;
    this.fields = new ArrayList(fieldList);
  }

  /**
   * appends a field to this component's list of fields
   */
  public void add(APC2Field field)
  {
    if(fields == null)fields = new ArrayList();

    fields.add(field);
  }

  private void parseInternal() throws Exception
  {
    reader = new CharArrayReader(internal);

    while(true)
    {
       APC2Field field = readField();

       if(field == null) break;

       fields.add(field);
    }

    orderFields();

  }

  private APC2Field readField() throws Exception
  {
    APC2Field current = null;
    String fieldName = null;
    try
    {
      int eof = 0;
      int prefix;
      char[] buffer;

      if(internal.length == 0) return null;

      prefix = reader.read();
      if(prefix == -1) return null;
      int fieldLengthPrefix = Prefix.valueOf(prefix);

      buffer = new char[fieldLengthPrefix];
      eof = reader.read(buffer,0,fieldLengthPrefix);
      if(eof == -1) return null;
      String fieldLengthStr = new String(buffer);
      int fieldLength = Integer.parseInt(fieldLengthStr);

      prefix = reader.read();
      if(prefix == -1) return null;
      int fieldNamePrefix = Prefix.valueOf(prefix);

      buffer = new char[fieldNamePrefix];
      eof = reader.read(buffer,0,fieldNamePrefix);
      if(eof == -1) return null;
      fieldName = new String(buffer);

      int fieldDataLength = fieldLength - (fieldNamePrefix + 1);
      buffer = new char[fieldDataLength];
      eof = reader.read(buffer,0,fieldDataLength);
      if(eof == -1) return null;

      current = new APC2Field(this,fieldName,buffer);
      return current;
    }
    catch(Exception e)
    {
      e.printStackTrace();
      throw new Exception("Error reading field: " + fieldName + "\nFor Component: " + this.name);
    }
  }

  /**
   * @return the name of this component as a String
   */

  public String getName()
  {
    return this.name;
  }

   /**
   * @return a String representation of this component suitable for use
   * in an *.ini type file
   * <pre>
   * the format used is as follows:
   * [componentName]
   * field.toString
   * field.toString
   * </pre>
   * @see com.basis100.deal.file.apc2.APC2Field
   */
  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    ListIterator li = this.fields.listIterator();

    while(li.hasNext())
    {
      APC2Field field = (APC2Field)li.next();
      buf.append(field.toString());
    }

    return new String(buf);
  }
  /**
   * Sort the field list into the order stipulated by the clarica.ini order file.
   * any missing fields (found in order file but not in this object's list) are inserted.
   */
  private void orderFields()
  {

     if(this.fields == null) return;

     if(!APC2ToIni.hasOrder) return;

     IniDataView orderFile = APC2ToIni.order;

     if(!orderFile.containsSectionIgnoreCase(this.name)) return;

     SectionData sd = orderFile.getSection(this.name);

     Vector names = sd.getKeys();

     int len = names.size();

     List newFieldList = new ArrayList(len);

     // create a new list
     //for each name in order file section create a field object
     //check if 'real field' exists
     //  if yes put the real one in list else put the order file field in list
     for(int i = 0; i < len; i++)
     {
        APC2Field orderListField = new APC2Field((String)names.elementAt(i),"");
        APC2Field current = null;
        ListIterator li = fields.listIterator();
        boolean added = false;

        while(li.hasNext())
        {
          current = (APC2Field)li.next();

          if(current.equals(orderListField))
          {
            newFieldList.add(current);
            added = true;
            break;
          }
        }

        if(!added)
        {
           newFieldList.add(orderListField);
           added = false;
        }
     }

     this.fields = newFieldList;
  }


  /**
   *   Assemble the fields for printing. Entails appending the field string to
   *   a String buffer
   *   @return a StringBuffer containing a String representation of the name-data field.
   */
  private StringBuffer assembleFields()
  {
     StringBuffer buffer = new StringBuffer();

     ListIterator li = fields.listIterator();

      while(li.hasNext())
      {
        APC2Field current = (APC2Field)li.next();
        buffer.append(current.toAPC2String());
      }
      
    return buffer;
  }

   /**
   * @return a String representation of this component in APC2 format
   * <pre>
   * for example:
   * 3225DENT6ClIENT<APC2Field.toAPC2String()>
   * - Where (in order):
   * - the 3 indicates the component length prefix
   * - 225 is the component length
   * - four byte component type
   * - 6 is the component name length prefix
   * - followed by the component name
   * - followed by 255 bytes of APC2field strings
   * </pre>
   * @see com.basis100.deal.file.apc2.APC2Field
   */
  public String toAPC2String()
  {
     StringBuffer buffer = new StringBuffer();


     StringBuffer fieldBuf = this.assembleFields();
     int componentLen = fieldBuf.length();
     componentLen +=5; // for name prefix  + type
     componentLen += this.name.length();

     buffer.append(Prefix.getPrefixFor((long)componentLen));
     buffer.append(TypeConverter.stringTypeFrom(componentLen));
     buffer.append(this.type);

     buffer.append(Prefix.getPrefixFor(this.name));
     buffer.append(this.name);
     buffer.append(new String(fieldBuf));

     return new String(buffer);
  }














}


