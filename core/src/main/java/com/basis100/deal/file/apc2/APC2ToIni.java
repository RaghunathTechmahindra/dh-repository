package com.basis100.deal.file.apc2;

import java.io.*;
import java.util.*;
import com.basis100.util.file.*;
import com.basis100.log.*;
import com.basis100.resources.*;
/**
 *   Convert a file in apc2  format to one or more files in *.ini format. The ini
 *   files each represent a single APC2Object (correlating to one deal).
 *   The resulting ini file will be in the format:
 *   <pre><b>
 *   [Apc2ComponentName]
 *   fieldname=data
 *   fieldname=data .....etc
 *   </b></pre><br>
 *
 *   <h3>File Naming Rules:<h3>
 *   The outbound file will be named with the following string:<br>
 *   Stipulated prefix + APC2ObjectName + a zero based index for each object in the apc2 file + extension stipulated.
 *
 */
public class APC2ToIni
{

  private SessionResourceKit srk;
  private SysLogger logger;
  private File directory;
  private APC2File apc2File;
  public static IniDataView order;
  public static boolean hasOrder;
  public static String CLARICA = "\\mos\\admin\\apc2\\clarica.ini";

 /**
 *   Construct an APC2Unpacker.
 *   @param apc2Name the file to unpack
 *   @param unpackDir destinationDirectory for ini format file(s)
 *   @param prefix the prefix for the file name
 *   @param fileExt the outbound filename extension
 *   @param clarica a boolean indicating whether this file is a Clarica file to be
 *   unpacked in order
 */
  public APC2ToIni(SessionResourceKit srk, String apc2Name) throws APC2Exception
  {
    this.srk = srk;
    this.logger = srk.getSysLogger();
    order = new IniDataView();
    hasOrder = order.loadIniFile(CLARICA);

    if(!hasOrder)
    {
      String msg =  "Order file: " + CLARICA + " for Clarica not found.\n";
      msg += "The Order file enforces the order and presence of upacked fields.\n";
      throw new APC2Exception(msg);
    }

    try
    {
      apc2File = new APC2File();
      apc2File.load(apc2Name);
    }
    catch(Exception e)
    {
      String msg  ="Error Unpacking APC2File: " + apc2Name;
      msg += "\n" + "Info: ";
      String info = e.getMessage();
      if(info == null) info = "None available.";

      msg += info;

      throw new APC2Exception(msg);
    }
  }

 /**
 *   Unpack an APC2 file for Clarica ingestion
 *  
 */

  public void unpack(String outfile) throws APC2Exception
  {
    List objects = this.apc2File.getObjects();
    ListIterator li = objects.listIterator();
    StringBuffer allObjects = new StringBuffer();

    while(li.hasNext())
    {
      APC2Object obj = (APC2Object)li.next();
      allObjects.append(obj.toString());
    }

    FileWriter writer = null;
    
    try
    {
      writer = new FileWriter(outfile);
      writer.write(new String(allObjects));
      writer.flush();
      writer.close();
      writer = null;
    }
    catch(Exception e)
    {
      logger.error(e);
      logger.error("APC2Unpacker: Error encountered while writing unpack file: "  + outfile);
      throw new APC2Exception("APC2Unpacker: IO Error encountered");
    }
    finally
    {
     try
     { writer.flush();
       writer.close();
       writer = null;
     }catch(Exception fe){;}
    }

  }

}