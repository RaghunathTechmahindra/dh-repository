package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;



public class MIText extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;
    
    try
    {
      int mindid = deal.getMIIndicatorId();
      
      if(mindid == Mc.MII_REQUIRED_STD_GUIDELINES || mindid == Mc.MII_UW_REQUIRED)
      {
          ConditionHandler ch = new ConditionHandler(srk);
          val = ch.getVariableVerbiage(deal,Dc.MORTGAGE_INSURANCE_TEXT,lang);
      }

      fml.addValue(val, fml.STRING);
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
