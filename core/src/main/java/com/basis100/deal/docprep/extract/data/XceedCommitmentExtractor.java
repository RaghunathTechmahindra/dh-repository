package com.basis100.deal.docprep.extract.data;

/**
 * 28/Aug/2008 DVG #DG746 FXP21967: LS-MLMQA - "Odd" characters in Express Commitment Letter 
 * 07/Nov/2005 DVG #DG356 #2272  Wells Fargo E2E - Express Commitment letter
 * 09/Sep/2005 DVG #DG312 #1965  Xeed Solicitor Package Middle Name initial is not shown
 */

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.xml.*;
import com.basis100.deal.conditions.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.picklist.*;
import java.util.*;
import MosSystem.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.*;
import com.basis100.deal.conditions.*;

/**
 * <PRE>
 * Creates an XML document for use as a Commitment Letter
 * Copyright (c) 2002 Basis100, Inc
 * </PRE>
 * @author       John Schisler
 * @version 1.0
 *
 */

public class XceedCommitmentExtractor extends GENXCommitmentExtractor
{
    public XceedCommitmentExtractor()
    {
        super();
    }

    public XceedCommitmentExtractor(XSLExtractor x)
    {
        super(x);
    }

    /**
     * Builds the XML doc from various records in the database.
     *
     */
    protected void buildXMLDataDoc() throws Exception
    {
        doc = XmlToolKit.getInstance().newDocument();

        Element root = doc.createElement("Document");

        //#DG312 make a full blown deal tree available so that this xml can be used for all templates
        createDealTree(root);

        root.setAttribute("type","1");
        root.setAttribute("lang","1");

        root.appendChild(getCommitmentTags(doc));
        doc.appendChild(root);
    }

    /**
     * Allows outside classes to build typical commitment letter tags.
     * For example, the solicitor's package has the commitment letter within it,
     * so to avoid redoing the same logic, we are going to use the same logic
     * the actual C/L data build process uses.
     *
     * We need to pass the Document because if you try to build part of a document
     * node structure and then try to add to it using another document, it
     * throws an exception.  To avoid this, we have the user pass the document
     * that they are using to construct Nodes.
     *
     * @param pDoc <code>Document</code> to use for building nodes.
     * @return The completed node tree.
     */
    public Element getCommitmentTags(Document pDoc)
    {
        //  Add all the original C/L tags
        Element root = super.getCommitmentTags(pDoc);

        //  Now add Xceed-specific tags

        //==========================================================================
        // Void Chq
        //==========================================================================
        addTo(root, extractVoidChq());

        //==========================================================================
        // Title Insurance
        //==========================================================================
        addTo(root, extractTitleInsurance());

        //==========================================================================
        // Fire Insurance
        //==========================================================================
        addTo(root, extractFireInsurance());

        //==========================================================================
        // Taxes Paid
        //==========================================================================
        addTo(root, extractTaxesPaid());

        //==========================================================================
        // Rate Guarantee
        //==========================================================================
        addTo(root, extractRateGuarantee());

        //==========================================================================
        // Privilege Payment
        //==========================================================================
        addTo(root, extractPrivilegePayment());

        //==========================================================================
        // Interest Calc
        //==========================================================================
        addTo(root, extractInterestCalc());

        //==========================================================================
        // Closing
        //==========================================================================
        addTo(root, extractClosing());

        //==========================================================================
        // Acceptance
        //==========================================================================
        addTo(root, extractAcceptance());

        //==========================================================================
        // Signature
        //==========================================================================
        addTo(root, extractSignature());

        //==========================================================================
        // Care of label
        //==========================================================================
        addTo(root, extractCareOf());

        //==========================================================================
        // Broker Email
        //==========================================================================
        addTo(root, extractBrokerEmail());

        //==========================================================================
        // BrokerContact Phone number and Broker Contact Fax number
        //==========================================================================
        addTo(root,extractBrokerAgentContactData());

        return root;
    }

    //==========================================================================
    // see comment 001 at the end of this class
    //==========================================================================
    protected Node extractBrokerAgentContactData()
    {
        Node groupNode = doc.createElement("BrokerAgentContactData");

        try
        {
            SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();

            Contact contact = sobp.getContact();

            String phoneVal = "";
            String faxVal = "";

            String lPhone  = contact.getContactPhoneNumber();
            String lExt = contact.getContactPhoneNumberExtension();
            String lFax = contact.getContactFaxNumber();

            if( lPhone != null ){
              if(lExt != null && !isEmpty(lExt) ){
                phoneVal = StringUtil.getAsFormattedPhoneNumber( lPhone, lExt) ;
              }else {
                phoneVal = StringUtil.getAsFormattedPhoneNumber( lPhone, null) ;
              }
            }

            if(lFax != null ){
              faxVal = StringUtil.getAsFormattedPhoneNumber( lFax, null) ;
            }
            Node lPhoneNode = getTag("BrokerAgentPhoneNumber", phoneVal);
            Node lFaxNode = getTag("BrokerAgentFaxNumber",faxVal);
            groupNode.appendChild(lPhoneNode);
            groupNode.appendChild(lFaxNode);
        }
        catch (Exception e){}

        return groupNode;
    }



    protected Node extractTotalAmount()
    {
        String val = null;

        try
        {
            double amt = deal.getNetLoanAmount() + deal.getMIPremiumAmount() + totalFees;

            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("TotalAmount", val);
    }

    protected Node extractInterestRate()
    {
        String val = null;

        try
        {
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory( deal.getPricingProfileId());
            val = null;

            if(pp != null && pp.getRateCode().equalsIgnoreCase("UNCNF"))
            {
                ConditionHandler ch = new ConditionHandler(srk);
                val = ch.getVariableVerbiage(deal, Dc.UNCONFIRMED_RATE, lang);
            }
            else
                val = formatInterestRate(deal.getNetInterestRate());
        }
        catch (Exception e){}

        return getTag("InterestRate", val);
    }

    protected Node extractVoidChq()
    {
      String val = null;

      try
      {
        //--> In order to fix the bug that DocPrep creating the un-necessary PACAgreementCondition when
        //--> Generating the Instruct Solicitor document.
        //--> By Billy 17Aug2004
        /*
               ConditionHandler ch = new ConditionHandler(srk);
               ConditionParser parser = new ConditionParser();
               Condition c = new Condition(srk, Dc.PAC_AGREEMENT_ASSUMPTIONS);
               DocumentTracking dt = parser.createSystemGeneratedDocTracking(c, deal, deal, srk);
         */
        //--> Check if Any PACAgreementCondition documentTracking record
        DocumentTracking dt = new DocumentTracking(srk);
        Collection dts = dt.findByDealAndConditionId((DealPK)(deal.getPk()), Dc.PAC_AGREEMENT_ASSUMPTIONS);

        Iterator it = dts.iterator();

        if(it.hasNext())
        {
          dt = (DocumentTracking)it.next();
          val = dt.getDocumentTextByLanguage(lang);
        }
        else
        {
          val = "nil";
        }
      }
      catch(Exception e)
      {
        logger.error("Exception @XceedCommitmentExtractor.extractVoidChq : " + e);
      }

      return getTag("VoidChq", val);
    }


    protected Node extractTitleInsurance()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.TRANSFER_SURVEY, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("TitleInsurance"), val));
    }

    protected Node extractFireInsurance()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            //  TODO:  Need a constant here instead of 406.
            val = ch.getVariableVerbiage(deal, 406, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("FireInsurance"), val));
    }

    protected Node extractTaxesPaid()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.TAXES_PAID, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("TaxesPaid"), val));
    }

    protected Node extractRateGuarantee()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);

            val = ch.getVariableVerbiage(deal, Dc.RATE_GUARANTEE, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("RateGuarantee"), val));
    }

    protected Node extractInterestCalc()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            //  TODO:  Need a constant here, instead of 377.
            val = ch.getVariableVerbiage(deal, 377, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("InterestCalc"), val));
    }

    protected Node extractClosing()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);

            val = ch.getVariableVerbiage(deal, Dc.CLOSING_TEXT_ENDING, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("Closing"), val));
    }

    protected Node extractAcceptance()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getElectronicVerbiage(deal, Dc.ACCEPTANCE_RETURN_STATEMENT, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("Acceptance"), val));
    }

    protected Node extractSignature()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.SIGNATURE, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("Signature"), val));
    }

    protected Node extractPrivilege()
    {
        return null;
    }

    protected Node extractPrivilegePayment()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);

            val = ch.getVariableVerbiage(deal, Dc.PRIVILEGE, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("PrivilegePayment"), val));
    }

    protected Node extractPrepayment()
    {
        return null;
    }

    protected Node extractLoanAmount()
    {
        String val = null;

        try
        {
            double amt = deal.getNetLoanAmount();

            List dealFees = (List)deal.getDealFees();

            Iterator i = dealFees.iterator();
            DealFee df;
            Fee fee;

            while (i.hasNext())
            {
                df = (DealFee)i.next();
                fee = df.getFee();

                if (fee.getFeeTypeId() == Dc.FEE_TYPE_APPLICATION_FEE)
                   amt -= df.getFeeAmount();
            }
            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("LoanAmount", val);
    }

    protected Node extractConditions()
    {
        Node elemNode = null;

        try
        {
            //ConditionHandler ch = new ConditionHandler(srk);
            List dts = (List) deal.getDocumentTracks();
            Collections.sort(dts, new DocumentStatusComparator());

            if (dts.isEmpty())
               return null;

            boolean bShowRole =
                PropertiesCache.getInstance().
                    getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.show.responsibilityrole.tag", "Y").
                        equalsIgnoreCase("Y");

            Iterator i = dts.iterator();

            String role;
            //ConditionParser parser = new ConditionParser();
            Condition cond;
            DocumentTracking dt;
            StringBuffer buf;
            int sourceId, condTypeId, statusId, roleid;
            Node condNode;
            ConditionParser p = new ConditionParser();

            while (i.hasNext())
            {
                dt = (DocumentTracking)i.next();
                cond = new Condition(srk, dt.getConditionId());

                condTypeId = cond.getConditionTypeId();
                sourceId = dt.getDocumentTrackingSourceId();
                statusId = dt.getDocumentStatusId();

                roleid = dt.getDocumentResponsibilityRoleId();
                //role = PicklistData.getDescription("ConditionResponsibilityRole", roleid);
                //ALERT:BX:ZR Release2.1.docprep
                role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", roleid, lang);

                if ((sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED ||
                     sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED ||
                     sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM) &&
                    (condTypeId == Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING ||
                     condTypeId == Mc.CONDITION_TYPE_COMMITMENT_ONLY) &&
                    (roleid == Mc.CRR_PARTY_TYPE_APPRAISER ||
                     roleid == Mc.CRR_PARTY_TYPE_SOLICITOR ||
                     roleid == Mc.CRR_SOURCE_OF_BUSINESS ||
                     roleid == Mc.CRR_USER_TYPE_APPLICANT) &&
                    (statusId == Mc.DOC_STATUS_REQUESTED ||
                     statusId == Mc.DOC_STATUS_RECEIVED ||
                     statusId == Mc.DOC_STATUS_INSUFFICIENT ||
                     statusId == Mc.DOC_STATUS_CAUTION))
                {
                   buf = new StringBuffer();

                   //  This is handled in the xsl template now...
                   //buf.append(++index).append(".").append(format.space());

                   buf.append(dt.getDocumentTextByLanguage(lang)).append(format.space());

                   if(bShowRole)
                   {
                     //role = PicklistData.getDescription("ConditionResponsibilityRole", dt.getDocumentResponsibilityRoleId());
                     //ALERT:BX:ZR Release2.1.docprep
                     role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", dt.getDocumentResponsibilityRoleId(), lang);
                     //role = DocPrepLanguage.getInstance().getTerm(role, lang);
                     buf.append('(').append(role).append(')');
                   }

                    if (elemNode == null)
                       elemNode = doc.createElement("Conditions");

                   //  JS - changed to make the XML data not RTF-specific.  This
                   //  new way adheres to other XML generation techniques,
                   //  where the lines previously divided by '\par'
                   //  are now divided into child nodes.  This allows the
                   //  XSL to format to the respective document type's syntax.
//                addTo(elemNode, "Condition", convertCRtoRTF(buf.toString(), format));
                   condNode = doc.createElement("Condition");
                   /*#DG356 add condition id
                   elemNode.appendChild(convertCRToLines(condNode,
                      p.parseText(buf.toString(),lang,deal,srk)));
                    */
                  condNode.appendChild( getTag("conditionId", String.valueOf(cond.getConditionId())));
                  convertCRToLines(condNode, p.parseText(buf.toString(),lang,deal,srk));
                  elemNode.appendChild(condNode);

                   //#DG312 System.out.println(buf.toString());
                   //#DG746 debug(buf.toString());
                }
            }
        }
        catch (Exception e){System.out.println("Logger exception: " + e);}

        return elemNode;
    }

    protected Node extractBrokerFirmName()
    {
        //if (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS)
        if( isCCAPSOrSpecialRegion() )
        {
            String val = null;

            try
            {
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(),
                                                          deal.getInstitutionProfileId()));

                Contact c = up.getContact();

                val = "";

                if (c.getContactFirstName() != null)
                   val += c.getContactFirstName();

                if (c.getContactLastName() != null)
                   val += " " + c.getContactLastName();
            }
            catch (Exception e){}

            return getTag("BrokerFirmName", val);
        }
        else
            return super.extractBrokerFirmName();
    }

    protected boolean isCCAPSOrSpecialRegion(){
      if( deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS ){ return true; }

      try{
        int lRegion = deal.getSourceOfBusinessProfile().getSOBRegionId();
        switch(lRegion){
          case 91 : { return true; }
          case 92 : { return true; }
          case 93 : { return true; }
          case 94 : { return true; }
          case 95 : { return true; }
          case 96 : { return true; }
          case 97 : { return true; }
          case 98 : { return true; }
          default : { return false; }
        }
      }catch(Exception exc){
        return false;
      }
    }

    protected Node extractBrokerAgentName()
    {
        if( isCCAPSOrSpecialRegion() )
        //if (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS)
        {
            String val = null;

            try
            {
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                          deal.getInstitutionProfileId()));

                Contact c = up.getContact();

                val = c.getContactJobTitle();
            }
            catch (Exception e){}

            return getTag("BrokerAgentName", val);
        }
        else
            return super.extractBrokerAgentName();
    }

    protected Node extractBrokerAddressSection()
    {
        if( isCCAPSOrSpecialRegion() )
        //if (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS)
        {
            Node elemNode = null;

            try
            {
                UserProfile up = new UserProfile(srk);
                up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                               deal.getInstitutionProfileId()));
                Contact c = up.getContact();
                String str;

                //  Phone
                str = StringUtil.getAsFormattedPhoneNumber(c.getContactPhoneNumber(), null);

                if (!isEmpty(str))
                {
                    if (!isEmpty(c.getContactPhoneNumberExtension()))
                    {
                       str += " Ext " + c.getContactPhoneNumberExtension();
                    }

                    if (elemNode == null)
                       elemNode = doc.createElement("BrokerAddress");

                    addTo(elemNode, "Phone", str);
                }

                //  Fax
                str = StringUtil.getAsFormattedPhoneNumber(c.getContactFaxNumber(), null);

                if (!isEmpty(str))
                {
                    if (elemNode == null)
                       elemNode = doc.createElement("BrokerAddress");

                    addTo(elemNode, "Fax", str);
                }
            }
            catch (Exception e) {}

            return elemNode;
        }
        else
            return super.extractBrokerAddressSection();
    }

    protected Node extractAdminFee()
    {
        String val = null;

        try
        {
            List fees = (List)deal.getDealFees();
            double amt = 0;

            Iterator i = fees.iterator();

            while (i.hasNext())
            {
                DealFee df = (DealFee)i.next();

                if (df.getFee().getFeeTypeId() == Mc.FEE_TYPE_APPLICATION_FEE ||
                    df.getFee().getFeeTypeId() == Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED)
                   amt += df.getFeeAmount();
            }

            totalFees += amt;

            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("AdminFee", val);
    }

    /**
     * This redundant code is required since this class doesn't inherit from
     * XceedExtractor, where a duplicate version of this method already exists.
     */
    protected Node extractCareOf()
    {
        String val = null;
// ZIVKO:ALERT:XCEED
        if( isCCAPSOrSpecialRegion() ){
          val = BXResources.getDocPrepIngMsg("FROM",lang);
        }else{
          val = BXResources.getDocPrepIngMsg("CARE_OF",lang);
        }
       /*
        try
        {
            int lSystemTypeId = deal.getSystemTypeId();
            switch(lSystemTypeId){
              case Mc.SYSTEM_TYPE_CCAPS :{
                val = BXResources.getDocPrepIngMsg("FROM",lang);
                break;
              }
              default : {
                int lRegion = deal.getSourceOfBusinessProfile().getSOBRegionId();
                switch(lRegion){
                  case 91 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  case 92 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  case 93 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  case 94 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  case 95 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  case 96 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  case 97 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  case 98 : { val = BXResources.getDocPrepIngMsg("FROM",lang);break; }
                  default : { val = BXResources.getDocPrepIngMsg("CARE_OF",lang);break; }
                }
                break;
              }
            }

            //val = (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS) ?
            //      (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? "FROM:" : "DE") :
            //      (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? "C/O:" : "AUX BONS SOINS DE");
        }
        catch (Exception e){}
        */

        return getTag("CareOf", val);
    }
}


/*
=============================
Comment 001
=============================
You can get this information from the following values...

deal.sourceofbusinessprofileID
from that, the contactID
then Contact.ContactPhoneNumber and Contact.ContactFaxNumber


-----Original Message-----
From: John Walker [mailto:jwalker@XceedMortgage.com]
Sent: Monday, April 28, 2003 2:46 PM
To: Derek Matthewson
Subject: RE: Commitment Letter for Deal: 11934


How can we get that to show the agent�s contact info? Brokers are not receiving if we send it to the firm.



Thanks,

John



-----Original Message-----
From: Derek Matthewson [mailto:dmatthewson@basis100.com]
Sent: Monday, April 28, 2003 2:10 PM
To: John Walker
Subject: RE: Commitment Letter for Deal: 11934

*/
