package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class SolicitorSection extends ReplaceableSectionTagExtractor
{
  public FMLQueryResult extract(DealEntity dl, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Deal deal = (Deal)dl;
    FMLQueryResult fml = new FMLQueryResult();


    try
    {
       fml.addValue(deal);
       PartyProfile pp = new PartyProfile(srk);
       pp.setSilentMode(true);
       pp = pp.findByDealSolicitor(deal.getDealId());

       //found party profile so tag info can be added to the doc
       fml.setAction(this.DONT_REPLACE);

       //else a finder exception was thrown so replace and dont parse

    }
    catch(NullPointerException npe)
    {
       fml.setAction(this.REPLACE_ONLY);
       return fml;
    }
    catch(FinderException fe)
    {
       fml.setAction(this.REPLACE_ONLY);
       return fml;
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }


    return fml;
  }
}
