package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class EmployerEmailAddress extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    try
    {

      if(de.getClassId() == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;
        EmploymentHistory hist = new EmploymentHistory(srk);
        hist = hist.findByCurrentEmployment((BorrowerPK)bor.getPk());
        return extract(hist, lang, format, srk);
      }
      else if(de.getClassId() == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        Borrower bor = new Borrower(srk,null);
        bor = bor.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
        EmploymentHistory hist = new EmploymentHistory(srk);
        hist = hist.findByCurrentEmployment((BorrowerPK)bor.getPk());
        return extract(hist, lang, format, srk);
      }

   


    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
