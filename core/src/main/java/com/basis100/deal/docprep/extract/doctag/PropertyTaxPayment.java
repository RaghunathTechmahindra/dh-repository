package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

public class PropertyTaxPayment extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;

    try
    {
      ConditionHandler ch = new ConditionHandler(srk);

      if(deal.getTaxPayorId() == Mc.TAX_PAYOR_BORROWER)
      {
        val = ch.getVariableVerbiage(deal,Dc.PROPERTY_TAX_CLIENT,lang);
      }
      else
      {
        val = ch.getVariableVerbiage(deal,Dc.PROPERTY_TAX_LENDER,lang);
      }


      fml.addValue(val, fml.STRING);

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
