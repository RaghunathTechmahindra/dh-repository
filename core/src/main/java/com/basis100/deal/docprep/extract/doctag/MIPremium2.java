package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import MosSystem.Mc;
import com.basis100.deal.conditions.*;



public class MIPremium2 extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    
    try
    {
       double amt = 0.0;
       int mid = deal.getMIIndicatorId();


       if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
            && deal.getMIUpfront().equals("N"))
       {
          amt = deal.getMIPremiumAmount();
          fml.addCurrency(amt);
       }

    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
