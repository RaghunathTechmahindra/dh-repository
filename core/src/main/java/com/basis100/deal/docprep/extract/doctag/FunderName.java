package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

public class FunderName extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String outVal = "";
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      int upId = deal.getFunderProfileId();
      UserProfileBeanPK upbPK = new UserProfileBeanPK(upId, deal.getInstitutionProfileId());
      UserProfile up = new UserProfile(srk);

      up = up.findByPrimaryKey(upbPK);

      Contact contact = up.getContact();
      if(contact != null){
        outVal += contact.getContactFirstName() + " " + contact.getContactLastName();
      }
      fml.addValue(outVal,fml.STRING);
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}