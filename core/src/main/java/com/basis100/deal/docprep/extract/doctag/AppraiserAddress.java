package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;


public class AppraiserAddress extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.PROPERTY)
      {
        Property p = (Property)de;

        FMLQueryResult result = (new Appraiser()).extract(p, lang, format, srk);

        List values = (List)result.getStringValues(lang);

        if(values.size() > 0)
        {
          PartyProfile pp = (PartyProfile)values.get(0);
          int contid = pp.getContactId();

          Contact c = new Contact(srk);
          c = c.findByPrimaryKey(new ContactPK(contid, 1));
          return (new AddressList().extract(c, lang, format, srk));
        }


      }
      
     

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
