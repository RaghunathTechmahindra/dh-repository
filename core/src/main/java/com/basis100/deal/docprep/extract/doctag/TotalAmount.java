package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class TotalAmount extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    double amt = 0.0;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {

       int id = deal.getDealPurposeId();
       int mindid = deal.getMIIndicatorId();

       if( id == Mc.DEAL_PURPOSE_PORTABLE_INCREASE || id == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
       {
          amt = deal.getTotalLoanAmount();
          fml.addCurrency(amt);
       }
       else if((mindid == Mc.MII_REQUIRED_STD_GUIDELINES || mindid == Mc.MII_UW_REQUIRED)
                 && deal.getMIUpfront().equals("N"))
       {
          amt = deal.getTotalLoanAmount();
          fml.addCurrency(amt);
       }
       else
       {
         amt = deal.getNetLoanAmount();
         fml.addCurrency(amt);
       }

      return fml;
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
   /* catch(FinderException fe)
    {
       return fml;
    }*/
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }
}    /*If the Deal.DealPurpose is not one of ("Refinance Existing Client", "Port Increase", "Portable" or "Portable Decrease")
        If Deal.MIIndicator  = "Rqd. Standard Guidelines" or "UW Required", and Deal.MIUpfront = "N"
Insert the Condition.ConditionText for Condition.ShortName of "Total Loan Amount" , a language of preferred language, and a Condition.ConditionType of "Variable Verbiage".

If the Deal.DealPurpose = "Refinance Existing Client" or "Port Increase"
Insert the Condition.ConditionText for Condition.ShortName of "Total Loan Amount" , a language of preferred language, and a Condition.ConditionType of "Variable Verbiage".

If the Deal.DealPurpose = "Portable" or "Portable Decrease"
        If Deal.MIIndicator  = "Rqd. Standard Guidelines" or "UW Required", and Deal.MIUpfront = "N"
Insert the Condition.ConditionText for Condition.ShortName of "CMHC Premium Verb" , a language of preferred language, and a Condition.ConditionType of "Variable Verbiage".    */
