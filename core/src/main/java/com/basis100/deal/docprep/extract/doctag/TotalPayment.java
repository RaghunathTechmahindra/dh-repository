package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;

public class TotalPayment extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      //======================================================================================
      // PROGRAMMER'S NOTE: Deal table - column pricingprofileid does not contain
      // pricing profile id, it contains pricing rate inventory id. So the path goes:
      // take pricingprofileid from deal, based on that key find the pricingrateinventoryid
      // in the pricingrateinventory table. From that record pick pricingprofileid value and
      // that is the key to search in the pricingprofile table.
      // Method findByPricingRateInventory in the PricingProfile class does exactly that.
      //=====================================================================================
      //PricingProfile pp = new PricingProfile(srk,deal.getPricingProfileId());
      PricingProfile pp = new PricingProfile(srk);
      pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

      String rc = pp.getRateCode();

      if(rc.equalsIgnoreCase("UNCNF")){
        fml.addValue("Unconfirmed", fml.STRING);
      } else {
        double totalAmount = 0d;
        totalAmount += deal.getPandiPaymentAmount();
        totalAmount += deal.getAdditionalPrincipal();
        fml.addCurrency(totalAmount);
      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }


}