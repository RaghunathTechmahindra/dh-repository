package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.picklist.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
/**
 *   if the recieved class is:<br>
 *   - Deal - get address info from primary borrower.<br>
 *   - Borrower - get address info from borrower current address.<br>
 *   - BorrowerAddress - get address info from BorrowerAddress.<br>
 *   - Property - get address info from Property.<br>
 *
 */

public class AddressPostal extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());


    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        Borrower primary = new Borrower(srk,null);
        primary = primary.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
        return extract(primary, lang, format, srk);
      }
      else if(clsid == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;
        BorrowerAddress baddr = new BorrowerAddress(srk);
        baddr = baddr.findByCurrentAddress(bor.getBorrowerId(),bor.getCopyId());

        return new AddressPostal().extract(baddr, lang, format, srk);
      }
      else if(clsid == ClassId.BORROWERADDRESS)
      {
        BorrowerAddress baddr = (BorrowerAddress)de;
        Addr addr = baddr.getAddr();

        if(addr != null)
        {

          String fsa = addr.getPostalFSA();
          String ldu = addr.getPostalLDU();

          if((ldu != null && fsa != null))
          {
            //String val = ldu + format.space() + fsa;
            String val = fsa + format.space() + ldu;
            fml.addValue(val,fml.STRING);
          }

        }
      }
      else if(de.getClassId() == ClassId.EMPLOYMENTHISTORY)
      {
        EmploymentHistory hist =(EmploymentHistory)de;
        Contact c = hist.getContact();
        Addr addr = c.getAddr();

        if(addr != null)
        {

          String fsa = addr.getPostalFSA();
          String ldu = addr.getPostalLDU();

          if((ldu != null && fsa != null))
          {
            //String val = ldu + format.space() + fsa;
            String val = fsa + format.space() + ldu;
            fml.addValue(val,fml.STRING);
          }

        }

      }


    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
