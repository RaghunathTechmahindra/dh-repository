package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class SecondClause extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";

    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;

    try
    {
      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal.getDealId(), deal
          .getCopyId(), deal.getInstitutionProfileId());
      //int ptid = prim.getPropertyTypeId();
      int prid = prim.getProvinceId();
      
      ConditionHandler ch = new ConditionHandler(srk);

      if(deal.isSecondaryFinancing())
      {

          if(prid == Mc.PROVINCE_BRITISH_COLUMBIA)
          {
            val += ch.getVariableVerbiage(deal,"Second Offer Clause",lang);

            val += format.carriageReturn();
            val += ch.getVariableVerbiage(deal,"Second Default Clause",lang);

            val += format.carriageReturn();
            val += ch.getVariableVerbiage(deal,"Second Default Cost Clause",lang);
          }
      }

      fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
