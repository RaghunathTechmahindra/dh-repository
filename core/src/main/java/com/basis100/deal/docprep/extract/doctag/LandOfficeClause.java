package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;
/**
 * If the Primary Property.ProvinceID(Description) = "Alberta" or "Nova Scotia",
 * Insert the Condition.ConditionText for Condition.ShortName of
 * "Statement of Disclosure Verb", a language of preferred language,
 * and a Condition.ConditionType of "Variable Verbiage".  + <carriage return" +
 * Insert the Condition.ConditionText for Condition.ShortName of "Statement of Disclosure'"
 * , a language of preferred language, and a Condition.ConditionType of "Variable Verbiage".
 * (Replace <RegistrationName> with Deal.LenderID(LenderProfile.RegestrationName) and <ShortName>
 * with Deal.LenderID(LenderProfile.LPShortName)Else,Remove Tag from template and delete
 */

public class LandOfficeClause extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal
          .getDealId(), deal.getCopyId(), deal.getInstitutionProfileId());
      int id = prim.getProvinceId();

      if(id == Mc.PROVINCE_ALBERTA ||
         id == Mc.PROVINCE_BRITISH_COLUMBIA ||
         id == Mc.PROVINCE_MANITOBA  ||
         id == Mc.PROVINCE_SASKATCHEWAN)
      {
        val = "Land Titles Office";
      }
      else
      {
        val = "Land Registry Office";
      }

      fml.addValue(val, fml.TERM);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
