/**
 * changes done by Catherine Rutgaizer, November 15, 2004
 * to accomodate requirements for AGF pvcs #685
 * <!-- Catherine, new tag for AGF, #685 -->
 */

package com.basis100.deal.docprep.extract.doctag;

import MosSystem.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import java.util.List;
import com.basis100.deal.pk.DealPK;
import java.util.Iterator;


public class TotalAmountPlusFees extends DocumentTagExtractor
{
  private int id;
  private int mid;

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk) throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    double amt = 0.0;
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      Deal deal = (Deal)de;

      id = deal.getDealPurposeId();
      mid = deal.getMIIndicatorId();

      double totalDedAmount = this.calcDeductions(deal, srk);

      /*
      if (id == Mc.DEAL_PURPOSE_PORTABLE_INCREASE ||
          id == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT) {
        amt = deal.getTotalLoanAmount();
        amt += totalDedAmount;
        fml.addCurrency(amt);
      }
      else if ( (mid == Mc.MII_REQUIRED_STD_GUIDELINES ||
                 mid == Mc.MII_UW_REQUIRED)
               && deal.getMIUpfront().equals("N")) {
        amt = deal.getTotalLoanAmount();
        amt += totalDedAmount;
        fml.addCurrency(amt);
      }
      else {
      */
        amt = deal.getNetLoanAmount();
        amt += totalDedAmount;
        fml.addCurrency(amt);
      //}
      return fml;
    }
    catch(NullPointerException npe)
    {
       srk.getSysLogger().error(npe.getMessage());
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
  }

  /**
   * This logic is copied from com.basis100.deal.docprep.extract.doctag.TotalDeductionsAmount
   * @param de DealEntity
   * @param srk SessionResourceKit
   * @return double
   */
  public double calcDeductions(DealEntity de, SessionResourceKit srk)
  {
    double amt = 0.0;
    Deal deal = (Deal) de;

    try {
      if ( (mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
          && deal.getMIUpfront().equals("N")) {
        amt = deal.getMIPremiumAmount();
      }
      DealFee pstFee = new DealFee(srk, null);
      int pstFeeTypes[] = { Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM,  Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM };
      List pstFees = (List) pstFee.findByDealAndTypes( (DealPK) deal.getPk(), pstFeeTypes);

      double miPSTAmount;
      if (pstFees.size() > 0) {
        pstFee = (DealFee) pstFees.get(0);
        miPSTAmount = pstFee.getFeeAmount();
      }
      else {
        miPSTAmount = deal.getMIPremiumPST();
      }

      amt += miPSTAmount;
      DealFee fee = new DealFee(srk, null);

      int[] types = {
          0, 1, 2, 3, 4, 9, 10, 13, 15, 19, 21, 22, 25, 26, 27, 28};

      List fees = (List) fee.findByDealAndTypes( (DealPK) de.getPk(), types);

      Iterator it = fees.iterator();
      DealFee current = null;

      while (it.hasNext()) {
        current = (DealFee) it.next();
        if (! (current.getFeeStatusId() == Mc.FEE_STATUS_WAIVED)) {
          amt += current.getFeeAmount();
        }
      }
    }
    catch (NullPointerException npe) {
      String msg = npe.getMessage();
      msg =
          "DOCPREP ERROR in TotalAmountPlusFees: Unable to calculate TotalDeductions: " +
          this.getClass().getName() + " msg: " + msg;
      srk.getSysLogger().error(msg);
      return amt;
    }
    catch (Exception e) {
      String msg = e.getMessage();
      if (msg == null) msg = "Unknown Error";
      msg =
          "DOCPREP ERROR in TotalAmountPlusFees: Unable to calculate TotalDeductions: " +
          this.getClass().getName() + " msg: " + msg;
      srk.getSysLogger().error(msg);
    }
    return amt;
  }
}



/*If the Deal.DealPurpose is not one of ("Refinance Existing Client", "Port Increase", "Portable" or "Portable Decrease")
        If Deal.MIIndicator  = "Rqd. Standard Guidelines" or "UW Required", and Deal.MIUpfront = "N"
Insert the Condition.ConditionText for Condition.ShortName of "Total Loan Amount" , a language of preferred language, and a Condition.ConditionType of "Variable Verbiage".

If the Deal.DealPurpose = "Refinance Existing Client" or "Port Increase"
Insert the Condition.ConditionText for Condition.ShortName of "Total Loan Amount" , a language of preferred language, and a Condition.ConditionType of "Variable Verbiage".

If the Deal.DealPurpose = "Portable" or "Portable Decrease"
        If Deal.MIIndicator  = "Rqd. Standard Guidelines" or "UW Required", and Deal.MIUpfront = "N"
Insert the Condition.ConditionText for Condition.ShortName of "CMHC Premium Verb" , a language of preferred language, and a Condition.ConditionType of "Variable Verbiage".    */
