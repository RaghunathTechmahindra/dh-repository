package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class PartyProfileProvince extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;


    try
    {
      PartyProfile pp = new PartyProfile(srk);
      pp.setSilentMode(true);
      pp.findByDealSolicitor(deal.getDealId());

      Contact cont = new Contact(srk,pp.getContactId(),1);

      Addr add = cont.getAddr();

      //val = PicklistData.getDescription("Province",add.getProvinceId());
      //ALERT:BX:ZR Release2.1.docprep
      val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province",  add.getProvinceId(), lang);

      fml.addValue(val, fml.TERM);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
