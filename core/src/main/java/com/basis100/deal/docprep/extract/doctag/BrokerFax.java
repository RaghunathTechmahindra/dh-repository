package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.entity.*;

public class BrokerFax extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    try
    {

      Deal deal = (Deal)de;

      SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();
      Contact contact = sobp.getContact();
      val = StringUtil.getAsFormattedPhoneNumber( contact.getContactFaxNumber(), null);

      fml.addValue(val, fml.STRING);

    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(e);
      throw new ExtractException("ERROR: Unable to extract Broker fax number");
    }

    return fml;
  }

}