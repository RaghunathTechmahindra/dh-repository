package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class SalePropertyAddress2 extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Deal deal = (Deal)de;

    try
    {
      Borrower b = new Borrower(srk,null);
      b.findByPrimaryBorrower(deal.getDealId(),deal.getCopyId());

      BorrowerAddress ba = new BorrowerAddress(srk,null);

      try
      {
        ba = ba.findBySaleAddress(b.getBorrowerId(),b.getCopyId());
      }
      catch(FinderException fex)
      {
        ba = ba.findByCurrentAddress(b.getBorrowerId(),b.getCopyId());
      }

      return new AddressLine2().extract(ba, lang, format, srk);

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }
}
