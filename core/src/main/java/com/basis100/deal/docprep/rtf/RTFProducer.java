package com.basis100.deal.docprep.rtf;

import java.io.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;

public class RTFProducer {

  DocumentGenerationHandler handler;
  private int dealNumber;

  public RTFProducer(DocumentGenerationHandler dgh)
  {
    this.handler = dgh;
    dealNumber = -1;
  }

  public void setDealNumber(int num){
    dealNumber = num;
  }

  public StringBuffer createDocument(String data)
  {
    //====================================================================
    // Note: data file comes as a string (in xml format).
    // template file comes as a file (path is in document profile).
    // result comes as string buffer (it has to be stored back in
    // event);
    //====================================================================

    try{
      PropertiesCache resources = PropertiesCache.getInstance();

      SessionResourceKit srk = handler.getSessionResourceKit();
      SysLogger log = srk.getSysLogger();

      log.trace("DOCPREP: RTFPRODUCER: begin rtf production.");

      DocumentProfile profile = handler.getDocumentProfile();
      log.trace("DOCPREP: RTFPRODUCER: got document profile.");

      String lFileLocation;
      String rootDir = ExpressDocprepHelper.getTemplatesFolder();

	  if(rootDir.endsWith("/") || rootDir.endsWith("\\"))
		  lFileLocation = rootDir + profile.getDocumentTemplatePath();
	  else
		  lFileLocation = rootDir + "/"+ profile.getDocumentTemplatePath();
      
      File lTemplateFile = new File(lFileLocation);
      log.trace("DOCPREP: RTFPRODUCER: got file location: " + lTemplateFile.getAbsolutePath());

      ReportCreator lReportCreator = new ReportCreator();
      log.trace("DOCPREP: RTFPRODUCER: make document.");
      StringBuffer retBuff = lReportCreator.makeRTFReport(data,lTemplateFile);
      log.trace("DOCPREP: RTFPRODUCER: document created.");

      // Catherine: 16-Dec-04: the following was commented out because this produced a duplicate RTF file in the archive
/*
      //========================================================================
      // ALERT!!!!!!
      // for test purposes only
      // saveDataAndReport(data, retBuff);
      // this method must be commented out before build.
      //========================================================================

      try
      {
          DocumentGenerationManager.getInstance().archiveData(new String(retBuff), dealNumber ,profile.getDocumentFullName(),".rtf");
      }
      catch(Exception e)
      {
        System.out.println("Failed to write debug rtf file");
      }

      log.trace("DOCPREP: RTFPRODUCER: end of work.");
*/
      return retBuff;

    } catch (Exception exc){
        exc.printStackTrace();
    }
    return null;
  }

  // for test purposes only
  private void saveDataAndReport(String pData, StringBuffer pFile){
    String path = "d:\\temprtf\\";
    String dataFileName = path + "ZZZCommitment.xml";
    String reportFileName = path + "ZZZCommitment.rtf";
    try{
      FileWriter dataWr = new FileWriter(dataFileName);
      FileWriter rptWr = new FileWriter(reportFileName);

      dataWr.write(pData);
      dataWr.flush();
      dataWr.close();

      rptWr.write(new String(pFile));
      rptWr.flush();
      rptWr.close();

    } catch(Exception exc){
      System.out.println("Exception of saveDataAndReport - RTFProducer.");
    }
  }
}
//==============================================================================
// END OF CLASS
//==============================================================================

