package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class SolicitorsFeeSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    try
    {
      DealFee fee = new DealFee(srk,null);

      //int[] types = { 0,1,2,3,4,9,10,15,22};
      // new types have been added to the list
      //int[] types = { 0,1,2,3,4,9,10,15,22,25,26,27};
      int[] types = { 0,1,2,3,4,9,10,15,22,25,26,27,28}; // change required by ticket #594

      List fees = (List)fee.findByDealAndTypes((DealPK)de.getPk(), types );

      Iterator fit = fees.iterator();
      DealFee current = null;

      while(fit.hasNext())
      {
        current = (DealFee)fit.next();

        if( !(current.getFeeStatusId() == Mc.FEE_STATUS_WAIVED) ){
          fml.addValue(current);
        }
      }

      //fml.addAll(fees);

      return fml;

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }
}
