package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.picklist.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.calc.*;

public class Occupation extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    int id = de.getClassId();

    try
    {
      if(id == ClassId.DEAL)
      {

      }
      else if(id == ClassId.EMPLOYMENTHISTORY)
      {
        EmploymentHistory hist = (EmploymentHistory)de;
        int hid = hist.getOccupationId();

        //val = PicklistData.getDescription("Occupation",hid);
         //ALERT:BX:ZR Release2.1.docprep
         val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Occupation",  hid, lang);

        fml.addValue(val, fml.TERM);
      }
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
