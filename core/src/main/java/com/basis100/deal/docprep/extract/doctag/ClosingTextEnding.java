package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class ClosingTextEnding extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
       int lid = deal.getLenderProfileId();
       LenderProfile lp = new LenderProfile(srk,lid);
       String lender = lp.getLPShortName();

       if(lender.equalsIgnoreCase("ING"))
       {
         return new FMLQueryResult();
       }
       else
       {


         ConditionHandler ch = new ConditionHandler(srk);
         val = ch.getVariableVerbiage(deal,Dc.CLOSING_TEXT_ENDING,lang);

       }
        String outVal = "";

        if(format.getType().startsWith("rtf")){
         outVal = this.convertCRtoRTF(val, format);
        }
        fml.addValue(outVal, fml.STRING);

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }

  private String convertCRtoRTF(String pStr, DocumentFormat format){
    if(! format.getType().startsWith("rtf") ){ return pStr; }
    int ind;
    StringBuffer retBuf = new StringBuffer("");
    while( (ind = pStr.indexOf("�")) != -1 ){
      retBuf.append(pStr.substring(0,ind));
      retBuf.append( format.carriageReturn() );
      pStr = pStr.substring(ind + 1);
    }
    retBuf.append(pStr);
    return retBuf.toString() ;
  }


}