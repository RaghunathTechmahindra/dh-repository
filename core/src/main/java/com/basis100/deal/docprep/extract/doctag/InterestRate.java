package com.basis100.deal.docprep.extract.doctag;

import MosSystem.Mc;

import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.DocumentTagExtractor;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.entity.FinderException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.picklist.BXResources;
/**
 * @version 1.0 - Existing version
 * @version 1.1 | MCM Impl Team. | XS_16.4 | 11-Aug-2008 | 
 *                                 modified extract(..) method to return �Interest Rate� value 
 *                                 as �N/A*� when DealProductType is component eligible 
 *                                 and UnderWriteTypeAs is Mortgage  
 *  
 */
public class InterestRate extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Deal deal = (Deal)de;
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      //======================================================================================
      // PROGRAMMER'S NOTE: Deal table - column pricingprofileid does not contain
      // pricing profile id, it contains pricing rate inventory id. So the path goes:
      // take pricingprofileid from deal, based on that key find the pricingrateinventoryid
      // in the pricingrateinventory table. From that record pick pricingprofileid value and
      // that is the key to search in the pricingprofile table.
      // Method findByPricingRateInventory in the PricingProfile class does exactly that.
      //=====================================================================================
      //PricingProfile pp = new PricingProfile(srk,deal.getPricingProfileId());
      PricingProfile pp = new PricingProfile(srk);
      pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );
      String rc = pp.getRateCode();
      
      /**** MCM Impl Team | XS_16.4 | 11-Aug-2008 | 
       *                    Logic added to display 'N/A*' for 'Interest Rate' 
       *                    field when the deal product is component eligible 
       *                    and UnderWriteTypeAs is 'Mortgage'. - Starts
       * ****/
      MtgProd mtgProd  = deal.getMtgProd();
      String compEligibleFlag = mtgProd.getComponentEligibleFlag();      
      
      if("Y".equalsIgnoreCase(compEligibleFlag) && mtgProd.getUnderwriteAsTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
      {	      
    	  fml.addValue(BXResources.getGenericMsg(BXResources.NOT_AVAILABLE_LABEL, lang), fml.STRING);  	       	     
      }
      else
      {
	      if(rc.equalsIgnoreCase("UNCNF"))
	      {
	        ConditionHandler ch = new ConditionHandler(srk);
	         fml.addValue(ch.getVariableVerbiage(deal, Dc.UNCONFIRMED_RATE,lang),fml.STRING);
	      }
	      else if(deal.getDiscount() != 0 && deal.getPremium() != 0)
	      {
            fml.addRate(deal.getNetInterestRate());         //fml.addValue("N/A" + format.space(), fml.STRING);
	      }
	      else
	      {
	        fml.addRate(deal.getNetInterestRate());
	      }
      } 
      /**** MCM Impl Team | XS_16.4 | 11-Aug-2008 | - Ends ***/
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
