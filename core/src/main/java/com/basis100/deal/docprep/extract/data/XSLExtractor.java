package com.basis100.deal.docprep.extract.data;

/**
 * 28/Aug/2008 DVG #DG746 FXP21967: LS-MLMQA - "Odd" characters in Express Commitment Letter 
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 */

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.docprep.extract.*;

import org.w3c.dom.*;
import MosSystem.Mc;
import java.util.*;

import com.basis100.deal.docprep.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.picklist.*;

//#DG238 make it inheritor of DealExtractor
public abstract class XSLExtractor extends DealExtractor
{
	public static boolean debug = false;
  //#DG746 public SysLogger logger = null;

	// use parent: Outstanding Conditiondocument was not generated Oct 6, 2008 MIDORI - STARTS 
//  protected Deal deal;
//  protected int lang;
//  protected DocumentFormat format;
//  protected SessionResourceKit srk;
/*
     Serghei:: September 25, 2008
     Paradigm FXP22495 Declaration of the Document should be removed from this class,
     cause it declared in the supper DealExtractor class. Otherwise 
     the doc instance in the supper DealExtractor will be null;
*/
//  protected Document doc;
	// use parent: Outstanding Conditiondocument was not generated Oct 6, 2008 MIDORI - ENDS 

  public abstract String extractXMLData(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk) throws Exception;

  /* #DG746 
  public String debug(String in) {
		if (debug)
			System.out.println(in);
		if (logger == null) {
			logger = ResourceManager.getSysLogger("TestXSLExtractor");
		}
		logger.debug(in);
		return in;
	} */

   /**
     * Builds a language-specific tag used by the XSL when formatting.
     *
     * @return The completed language tag, or null if the class-level
     *         variable <code>lang</code> is unrecognized.
     */
    protected Node getLanguageTag()    {
        Element elem = null;
        switch (lang)        {
            case Mc.LANGUAGE_PREFERENCE_ENGLISH:
                 elem = doc.createElement("LanguageEnglish");
                 break;
            case Mc.LANGUAGE_PREFERENCE_FRENCH:
                 elem = doc.createElement("LanguageFrench");
                 break;
            case Mc.LANGUAGE_PREFERENCE_SPANISH:
                 elem = doc.createElement("LanguageSpanish");
                 break;
            default:
                 break;
        }
        if (elem != null)
           elem.appendChild(doc.createTextNode("Y"));
        return elem;
    }

    /**
     * Formats the current date and formats an XML tag with it.
     *
     * @return The formatted current date XML tag.
     *
     */
    protected Node extractCurrentDate()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getInstance().getFormattedDate(new Date(), lang);
        }
        catch (Exception e){}

        return getTag("CurrentDate", val);
    } 

    /* #DG746 use parent's 
    public Node convertCRToLines(Node root, String pStr)    {
      return convertCRToLines(doc, root, pStr, "�\n");
    }

    public Node convertCRToLines(Node root, String pStr, String returnChar)    {
        return convertCRToLines(doc, root, pStr, returnChar);
    }

    /**
     * Converts all text in <code>pStr</code> to Line elements, delimited by
     * <code>returnChar</code> and attaches them to <code>root</code>.
     *
     * @param pDoc the document to use to create the line nodes
     * @param root the node to attach the create line items to
     * @param pStr the string to divide up into line nodes
     * @param returnChar the character sequence to use as a line delimiter.
     *
     * @return the node that was passed in via <code>root</code>, complete with
     * line nodes.
     * /
    public static Node convertCRToLines(Document pDoc, Node root, String pStr, String returnChar)    {
        if (root == null || pDoc == null || pStr == null)
           return root;
        StringTokenizer st = new StringTokenizer(pStr, returnChar);
        //String token;
        Node child;
        while (st.hasMoreTokens())
        {
            child = pDoc.createElement("Line");
            child.appendChild(pDoc.createTextNode(st.nextToken().trim()));
            root.appendChild(child);
        }
        return root;
    } 

    /** #DG746 not used
     * 
     * Replaces new line characters "�" and "\n" with
     * the rtf-specific character sequence for carriage returns.
     *
     * @param pStr the <code>String</code> to convert
     * @param format the format to get the appropriate carriage return character from
     *
     * @return the reformatted string.
     *
     * @deprecated replaced by <code>convertCRToLines()</code>.  This method formats
     * the XML to be RTF-specific.  Since we want to keep the data implementation
     * independent, this is no longer a viable solution for paragraphs.
     * <code>convertCRToLines()</code> keeps the data implementation-independent,
     * making it the XSL's responsibility to format paragraphs according to the
     * respective document's syntax.
     * /
    public static String convertCRtoRTF(String pStr, DocumentFormat format)    {
        pStr = convertCRtoRTF(pStr, format, "�");
        pStr = convertCRtoRTF(pStr, format, "\n");
        return pStr;
    }

    /**
     * Replaces any new line characters (defined by <code>returnChar</code>) with
     * the rtf-specific character sequence for carriage returns.
     *
     * @param pStr the <code>String</code> to convert
     * @param format the format to get the appropriate carriage return character from
     * @param returnChar the characters to replace with the carriage return character.
     *
     * @return the reformatted string.
     *
     * @deprecated replaced by <code>convertCRToLines()</code>.  This method formats
     * the XML to be RTF-specific.  Since we want to keep the data implementation
     * independent, this is no longer a viable solution for paragraphs.
     * <code>convertCRToLines()</code> keeps the data implementation-independent,
     * making it the XSL's responsibility to format paragraphs according to the
     * respective document's syntax.
     * /
    public static String convertCRtoRTF(String pStr, DocumentFormat format, String returnChar)
    {
        if(pStr == null || !format.getType().startsWith("rtf"))
             return pStr;

        int ind;
        StringBuffer retBuf = new StringBuffer("");

        while( (ind = pStr.indexOf(returnChar)) != -1 )
        {
            retBuf.append(pStr.substring(0,ind));
            retBuf.append( format.carriageReturn() );
            pStr = pStr.substring(ind + 1);
        }

        retBuf.append(pStr);
        return retBuf.toString();
    } 

    /* #DG746 use parent's 
     *
     * Replaces various characters that would break an XML string with their
     * escaped counterparts.  These characters include &, < and >.
     *
     * @param pStr the string to replace any illegal characters in.
     *
     * @return the XML-safe string.
     * /
    public static String makeSafeXMLString(String pStr)
    {
        if (pStr == null)
           return pStr;
        int ind;        
        StringBuffer retBuf = new StringBuffer("");
        ind = pStr.indexOf("&");
        if (ind == -1)
           ind = pStr.indexOf("<");
        if (ind == -1)
           ind = pStr.indexOf(">");        
        while( ind != -1 )        {
            retBuf.append(pStr.substring(0,ind));
            if (pStr.charAt(ind) == '&')
               retBuf.append( "&amp;" );
            else if (pStr.charAt(ind) == '<')
               retBuf.append( "&lt;" );
            else if (pStr.charAt(ind) == '>')
               retBuf.append( "&gt;" );
            pStr = pStr.substring(ind + 1);
            ind = pStr.indexOf("&");
            if (ind == -1)
               ind = pStr.indexOf("<");
            if (ind == -1)
               ind = pStr.indexOf(">");
        }
        retBuf.append(pStr);        
        return retBuf.toString();
    } 

    /**
     * 
     * Utility method that adds <code>ins</code> to <code>inl</code>
     * only if both are not <code>null</code>.
     *
     * @param inl the list to add the string to.
     * @param ins the string to add to the list.
     *
     * @return whether the addition was successful.
     * /
    protected boolean addTo(List inl, String ins)    {
        if (!isEmpty(ins) && inl != null)        {
           inl.add(ins);
           return true;
        }
        return false;
    } 

    /**
     * Utility method that adds <code>childNode</code> to <code>parentNode</code>
     * only if both are not <code>null</code>.
     *
     * @param parentNode the parent node to add the child to.
     * @param childNode the child node to be added.
     *
     * @return whether the addition was successful.
     * /
    protected boolean addTo(Node parentNode, Node childNode)    {
        try        {
            parentNode.appendChild(childNode);
            return true;
        }
        catch (Exception e) {}
        return false;
    }

    /**
     * Utility method that adds a new <code>Node</code> with a
     * name of <code>tagName</code> and data of <code>data</code>.
     *
     * @param parentNode the parent node to add the new Node to.
     * @param tagName the name of the child node to be added.
     * @param data the child node text to be added.
     *
     * @return whether the addition was successful.
     * /
    protected boolean addTo(Node parentNode, String tagName, String data)    {
        try        {
            if (parentNode != null && !isEmpty(tagName) && !isEmpty(data))            {
                data = data.trim();
                parentNode.appendChild(getTag(tagName, data));
                return true;
            }
        }
        catch (Exception e) {}
        return false;
    }

    /**
     * Utility method that returns a new <code>Node</code> with data of <code>data</code>
     * and a name of <code>tagName</code>.
     *
     * @param tagName the name of the child node to be added.
     * @param data the child node text to be added.
     *
     * @return the new <code>Node</code> or <code>null</code> if an error is encountered.
     * /
    protected Node getTag(String tagName, String data)    {
        //  DEBUG
        //  This ensures that SOMETHING gets put in the field.  This is useful
        //  for debugging XML data source.
        //if (data == null || data.trim().length() == 0)
           //data = "ABC123";
        try        {
            if (!isEmpty(tagName) && !isEmpty(data))            {
                data = data.trim();
                Node elemNode = doc.createElement(tagName);
                elemNode.appendChild(doc.createTextNode(data));
                return elemNode;
            }
        }
        catch (Exception e) {}
        return null;
    } */

    /**
     * 
     */
    protected boolean isFeeOfRequiredType(Fee pFee, int[] reqTypes)
    {
        for (int i=0; i < reqTypes.length; i++)
          if (pFee.getFeeTypeId() == reqTypes[i]) return true;

        return false;
    }

    protected Node extractIADAmount()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getInstance().
                    getFormattedCurrency(deal.getInterimInterestAmount(), lang);
        }
        catch (Exception e){}

        return getTag("IADAmount", val);
    }

    protected Node extractPerDiemInterestAmount()
    {
        String val = null;
        try
        {
            val = DocPrepLanguage.getInstance().
                    getFormattedCurrency(deal.getPerdiemInterestAmount(), lang);
        }
        catch (Exception e){}

        return getTag("PerDiemInterestAmount", val);
    }

    protected Node extractPAndIPaymentMonthly()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getInstance().
                    getFormattedCurrency(deal.getPandIPaymentAmountMonthly(), lang);
        }
        catch (Exception e){}

        return getTag("PandIPaymentMonthly", val);
    }

    protected Node extractPAndIPayment()
    {
        return extractPAndIPayment("PandIPayment");
    }

    protected Node extractPAndIPayment(String tagName)
    {
        String val = null;

        try
        {
            //======================================================================================
            // PROGRAMMER'S NOTE: Deal table - column pricingprofileid does not contain
            // pricing profile id, it contains pricing rate inventory id. So the path goes:
            // take pricingprofileid from deal, based on that key find the pricingrateinventoryid
            // in the pricingrateinventory table. From that record pick pricingprofileid value and
            // that is the key to search in the pricingprofile table.
            // Method findByPricingRateInventory in the PricingProfile class does exactly that.
            //=====================================================================================
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

            String rc = (pp == null ? "" : pp.getRateCode());

            val = (rc != null && rc.equalsIgnoreCase(RATE_CODE_UNCONFIRMED)) ?
                  "Unconfirmed" :
                  DocPrepLanguage.getInstance().getFormattedCurrency(deal.getPandiPaymentAmount(),lang);
        }
        catch(Exception exc)    {}

        return getTag(tagName, val);
    }

    protected Node extractTerm(String tagName)
    {
        Node retNode = doc.createElement(tagName);
        Node textNode;

        String retValStr = null;
        //String ptDesc = null;
        String ptDescTranslated = null;
        int apt = deal.getActualPaymentTerm();

        retValStr = DocPrepLanguage.getYearsAndMonths(apt,lang)+ format.space();		//#DG746

        try{
          int ptid = deal.getPaymentTermId();
          int paymentTermTypeId = findPaymentTermTypeId(srk,ptid);
          if(paymentTermTypeId == -1){
            ptDescTranslated = "";
          }else{
            //ptDesc = PicklistData.getDescription("PaymentTermType",paymentTermTypeId);
            //ALERT:BX:ZR Release2.1.docprep
            ptDescTranslated = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_PAYMENT_TERM_TYPE, paymentTermTypeId, lang);
            //ptDescTranslated = DocPrepLanguage.getInstance().getTerm(ptDesc,lang);
            if(ptDescTranslated == null){
              ptDescTranslated = "";
            }
          }
        }catch(Exception exc){
          return null;
        }

        retValStr += ptDescTranslated;
        textNode = doc.createTextNode(retValStr);
        retNode.appendChild(textNode);
        return retNode;
    }

  protected int findPaymentTermTypeId( SessionResourceKit srk, int parId )
  {
    JdbcExecutor jExec = srk.getJdbcExecutor();
    String sql01 = "Select * from paymentterm where paymenttermid = " + parId;

    int retVal = -1;
    boolean gotRecord = false;
    try{
      int key = jExec.execute(sql01);

      for (; jExec.next(key); )
      {
        retVal = jExec.getInt(key,"pttypeid");
        gotRecord = true;
        break; // only one record, please.
      }
      jExec.closeData(key);
    } catch (Exception exc){
      String msg = exc.toString()
       	+ "\n Method: findPaymentTermTypeId Class: XSLExtractor paymenttermid: " + parId + "\n"
      	+ "SQL statement used: " + sql01;
      srk.getSysLogger().error("DOCPREP: XSLExtractor: findPaymentTermTypeId:" + parId + " ."+msg);		//#DG746
      return -1;
    }

    return (gotRecord) ? retVal : -1;
  }

  /* #DG746 use parent's 
    protected boolean isEmpty(String str)    {
        return (str == null || str.trim().length() == 0);
    }*/
}
