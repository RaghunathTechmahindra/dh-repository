package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class GuarantorNames extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;

    try
    {
      Collection borrowers = deal.getBorrowers();

      if(borrowers.isEmpty())
       return new FMLQueryResult();

      Iterator it = borrowers.iterator();
      Borrower bor = null;
      String firstname = null;
      String lastname = null;

      while(it.hasNext())
      {
        bor = (Borrower)it.next();

        if(bor.getBorrowerTypeId() == Mc.BT_GUARANTOR)
        {
          firstname = bor.getBorrowerFirstName();
          lastname = bor.getBorrowerLastName();

          if(firstname != null && lastname != null &&
              firstname.length() != 0 && lastname.length()!= 0)
          {
             val = firstname + format.space() + lastname;
             fml.addValue(val,fml.STRING);
          }
        }
      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
