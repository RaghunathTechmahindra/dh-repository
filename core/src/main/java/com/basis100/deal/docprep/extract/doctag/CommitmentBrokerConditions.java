package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.*;

/**
 * Title:        Your Product Name
 * Description:  Your description
 * Copyright:    Copyright (c) 1999
 * Company:
 * @author
 * @version
 */

public class CommitmentBrokerConditions extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    StringBuffer outBuf = new StringBuffer("");
    int index = 0;
    Deal deal = (Deal)de;
    FMLQueryResult fml = new FMLQueryResult();

    List outList = new java.util.ArrayList();
    try
    {
        //
        // broker conditions
        //
        BrokerApprovalConditionHandler bach = new BrokerApprovalConditionHandler(srk);
        Map condMap = bach.extractBrokerApprovalConditionNodes(deal,format);
        Set condMapKeys = condMap.keySet();
        Iterator condMapIterator = condMapKeys.iterator();
        //
        // now do the cleaning. From the condMap which contains all broker conditions
        // remove those conditions contained in the clConditions (commitment letter conditions).
        //
        Integer currentBCKey;
        ConditionContainer conditionContainer;

        condMapKeys = condMap.keySet();
        condMapIterator = condMapKeys.iterator();
        while( condMapIterator.hasNext() ){
          currentBCKey = (Integer)condMapIterator.next();
          conditionContainer = (ConditionContainer)condMap.get(currentBCKey) ;
          Integer lConditionKey = conditionContainer.getCondIdCode() ;
          Condition cond = new Condition( srk,lConditionKey.intValue() );
          if(cond.getConditionId() == Dc.ACCEPTANCE_RETURN_STATEMENT ){
            continue;
          }
          if(cond.getConditionId() == Dc.PRIVACY_CLAUSE ){
            continue;
          }
          if(cond.getConditionId() == Dc.PRIVACY_CLAUSE_ENDING ){
            continue;
          }
          if(cond.getConditionId() == Dc.CLOSING_TEXT ){
            continue;
          }
          if(cond.getConditionId() == Dc.CLOSING_TEXT_ENDING ){
            continue;
          }

          if(cond.getConditionTypeId() == Mc.CONDITION_TYPE_VARIABLE_VERBIAGE ){
            outList.add(conditionContainer);
          }else if(cond.getConditionTypeId() == Mc.CONDITION_TYPE_ELECTRONIC_VERBIAGE  ){
            outList.add(conditionContainer);
          }
        }
        //======================================================================
        // at this point we have clean set of broker conditions
        //======================================================================
        String text;
        Iterator lIterator = outList.iterator();
        ConditionParser p;
        String outText;

        while ( lIterator.hasNext() ){
          ConditionContainer cCont = (ConditionContainer)lIterator.next();
          text = cCont.getCondText() ;

          p = new ConditionParser();
          outText = p.parseText(text,lang,deal,srk);

          outBuf.append(++index).append(".").append(format.space()) ;
          outBuf.append(outText) ;
          if(format.getType().startsWith("rtf")){
           outBuf.append(format.carriageReturn()).append(format.carriageReturn()) ;
          }
        }

        String wrkStr = outBuf.toString();
        //val = outBuf.toString();
        if(format.getType().startsWith("rtf")){
         val = this.convertCRtoRTF(wrkStr, format);
        }
        //System.out.println(val);
        fml.addValue(val, fml.STRING);

    }catch(Exception exc){
      System.out.println("Exception occurred here.");
    }
    return fml;
  }

  private String convertCRtoRTF(String pStr, DocumentFormat format){
    if(! format.getType().startsWith("rtf") ){ return pStr; }
    int ind;
    StringBuffer retBuf = new StringBuffer("");
    while( (ind = pStr.indexOf("�")) != -1 ){
      retBuf.append(pStr.substring(0,ind));
      retBuf.append( format.carriageReturn() );
      pStr = pStr.substring(ind + 1);
    }
    retBuf.append(pStr);
    return retBuf.toString() ;
  }

}