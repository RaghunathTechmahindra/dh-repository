package com.basis100.deal.docprep.extract;

import org.w3c.dom.*;
import org.xml.sax.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import MosSystem.Mc;

import java.io.*;
import java.util.*;
import java.text.*;

import com.basis100.log.*;
import com.basis100.resources.*;

/**
 *  The ComponentResult class provides a mechanism for breaking an FMLQueryResult<br/>
 *  into different FML types for the purpose of Language. <br/>
 *  The result is built by cumulatively adding the required types and each is converted <br/>
 *  using the appropriate localization rules for the type. <br/>
 *
 *  The component result is structurally a pair of list which match entries with FML type.
 *
 *  To use the ComponentResult class construct an instance and add the components of
 *  the result in such a way as to expose their language based characteristics.
 *
 *  The phrase "Loan Value: 1254.1452" would be handled as follows:<br/><br/>
 *<pre>
 *  FMLQueryResult fml = new FMLQueryResult();
 *  ComponentResult cr = new ComponentResult();
 *  cr.addResult("Loan Value", fml.TERM);
 *  cr.addResult(": ", fml.STRING);
 *  cr.addResult(1254.1452, fml.CURRENCY);
 *  fml.addComponentResult(cr);
 *</pre>
 *
 */
public class ComponentResult
{

  private List type;
  private List result;
  int index = -1;


  public void addResult(Date result, int type)
  {
    if(result == null) return;

    if(this.type == null || this.result == null)
    {
      this.type = new ArrayList();
      this.result = new ArrayList();
    }

    this.result.add(result);
    this.type.add(new Integer(type));
  }

  public void addResult(String result, int type)
  {
    if(result == null) return;
    
    if(this.type == null || this.result == null)
    {
      this.type = new ArrayList();
      this.result = new ArrayList();
    }

    if(type == FMLQueryResult.TERM &&
       (result.startsWith(" ") || result.endsWith(" ")))
    {
      StringBuffer buf = new StringBuffer(result);
      StringBuffer pre = new StringBuffer();
      StringBuffer post = new StringBuffer();
      int count = 0;

      while(Character.isWhitespace(buf.charAt(count)))
      {
        pre.append(buf.charAt(count++));
      }

      count = result.length()-1;

      while(Character.isWhitespace(buf.charAt(count)))
      {
        post.append(buf.charAt(count--));
      }

      this.result.add(pre.toString());
      this.type.add(new Integer(FMLQueryResult.STRING));
      this.result.add(result.trim());
      this.type.add(new Integer(FMLQueryResult.TERM));
      this.result.add(post.toString());
      this.type.add(new Integer(FMLQueryResult.STRING));


    }
    else
    {
      this.result.add(result);
      this.type.add(new Integer(type));
    }
  }

  public void addResult(double result, int type)
  {
    if(this.type == null || this.result == null)
    {
      this.type = new ArrayList();
      this.result = new ArrayList();
    }

    this.result.add(new Double(result));
    this.type.add(new Integer(type));
  }

  public boolean hasNext()
  {
    if (result == null)
    {
	return false;
    }
    if(index < result.size()-1)
    {
     return true;
    }
    return false;
  }

  public Object next()
  {
    index++;
    return result.get(index);
  }

  public Object previous()
  {
    index--;
    return result.get(index);
  }

  public int type()
  {
    return ((Integer)type.get(index)).intValue();
  }

}
