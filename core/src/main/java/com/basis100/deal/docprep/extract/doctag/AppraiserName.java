
package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class AppraiserName extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.PARTYPROFILE)
      {
        PartyProfile pp = (PartyProfile)de;
        fml.addValue(pp.getPartyName(),fml.STRING);
      }
      else if(clsid == ClassId.PROPERTY)
      {
        Property p = (Property)de;
        FMLQueryResult result = (new Appraiser()).extract(p, lang, format, srk);

        List list = result.getEntities();

        PartyProfile pp = null;

        if(!list.isEmpty())
         pp = (PartyProfile)list.get(0);
        else
        {
          fml.addValue("an approved appraiser", fml.STRING);
          return fml;
        }

        if(pp == null)
        {
          fml.addValue("an approved appraiser", fml.STRING);
          return fml;
        }
        else
        {
          fml.addValue(pp.getPartyName(), fml.STRING);
          return fml;
        }
      }

    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
