package com.basis100.deal.docprep.gui;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

import com.basis100.deal.docprep.deliver.*;
import com.basis100.mail.*;
import com.basis100.resources.*;

public class DPAlertManager
{
  private String onExceptionContact;
  private final String defaultSubj;
  private final String defaultFrom;
  private final String env;
  private final String alertFrom;
  private final String alertTo;

  public DPAlertManager()
  {
    onExceptionContact = null;
    env = PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.running.environment");
    alertFrom = PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.alert.email.sender");
    alertTo = PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.alert.email.recipient");

    String val = PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.alert.email.default.subject");
    if (val == null){
      val = "DocPrep Alert";
    }
    defaultSubj = val;
    defaultFrom = "qa@bx.filogix.com";
  }

  public void setOnExceptionContact(String pVal){
    onExceptionContact = pVal;
  }

  public String getOnExceptionContact(){
    return onExceptionContact;
  }

  public void sendEmailAlert(String pText, String pSubject){
    // do not send the message is mossys.property is not set to y
    if( !PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.send.email.alert", "Y").equalsIgnoreCase("y")  ){
      return;
    }
    SimpleMailMessage message;
    // to improve error logging on the console window
    System.out.println("DOCPREP: Sending Alert email to support...");
    
    try{

      message = new SimpleMailMessage();
      message.clearTo();
      message.setFrom( alertFrom );
      message.appendTo( alertTo );
      message.setText( pText );
      message.setSubject(pSubject + ": " + env);
      MailDeliver.send(message);
    }catch(Exception exc){
      // try it one more time ( this time we send the message to emergency recipient )
      if(onExceptionContact != null ){
        try{
          message = new SimpleMailMessage();
          message.clearTo();
          message.setFrom(defaultFrom);
          message.appendTo(onExceptionContact);
          message.setText( pText );
          message.setSubject(PropertiesCache.getInstance().getProperty(-1, pSubject + ": " + env));
          MailDeliver.send(message);
        }catch(Exception anotherExce){
          anotherExce.printStackTrace();
        }
      }
      exc.printStackTrace();
    }
  }

  // default subject
  public void sendEmailAlert(String pText){
    sendEmailAlert(pText, defaultSubj);
  }

}
