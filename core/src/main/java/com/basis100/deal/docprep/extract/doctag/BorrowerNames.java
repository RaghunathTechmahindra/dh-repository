package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class BorrowerNames extends DocumentTagExtractor 
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      Collection borrowers = deal.getBorrowers();

      if(borrowers.isEmpty())
       return new FMLQueryResult();

      Iterator it = borrowers.iterator();

      Borrower bor = null;
      String firstname = null;
      String lastname = null;

      while(it.hasNext())
      {
        bor = (Borrower)it.next();

        if(bor.getBorrowerTypeId() == Mc.BT_BORROWER)
        {
          firstname = bor.getBorrowerFirstName();
          lastname = bor.getBorrowerLastName();

          if(firstname == null || lastname == null ||
              firstname.length() == 0 || lastname.length()== 0)
                return new FMLQueryResult();

          val = firstname + format.space() + lastname;
          fml.addValue(val, fml.STRING);
        }
        //=========================================================================
        // PROGRAMMER'S NOTE: Zivko removed the following line and moved it
        // within if brackets. The problem was that the code as it was, repeated
        // the name of the last "good" borrower if, the current one is guarantor.
        // The visible consequence of this was duplicating the name of the previous
        // "good" borrower.
        //=========================================================================
        //fml.addValue(val, fml.STRING);

      }
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
