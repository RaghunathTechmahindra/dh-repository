package com.basis100.deal.docprep.extract.doctag;

import java.util.*;

import MosSystem.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;

public class NetMortgageAmount extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        double amt = deal.getTotalLoanAmount();
        double totalDedAmount = 0d;
        //======================================================================
        // Calculate totalDeductionAmount
        //======================================================================
        // 1. calculate Premium
        //======================================================================
        int mid = deal.getMIIndicatorId();;
        if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
              && deal.getMIUpfront().equals("N"))
        {
          totalDedAmount = deal.getMIPremiumAmount();
        }

        // --------------------------------- Catherine  ---------------------------------
        //========================================================================
        // Catherine, 23-Nov-04
        // MIPST was missing,
        // this piece of code has been copied from TotalDeductionsAmount class
        //========================================================================
        DealFee pstFee = new DealFee(srk,null);
        int pstFeeTypes[] = { Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM}; //{13,19}
        List pstFees = (List)pstFee.findByDealAndTypes((DealPK)deal.getPk(), pstFeeTypes);
        double miPSTAmount ;
        if(pstFees.size() > 0)
        {
          pstFee = (DealFee)pstFees.get(0);
          miPSTAmount = pstFee.getFeeAmount();
        }else{
          miPSTAmount = deal.getMIPremiumPST();
        }

        totalDedAmount += miPSTAmount;
        // --------------------------------- end Catherine  ---------------------------------
        //======================================================================
        // 2. Calculate fee total
        //======================================================================
        DealFee fee = new DealFee(srk,null);
        // --- Catherine, 23-Nov-04, removed {13,19} because they are processed earilie
        // added {25,26,27,28} because they are used in TotalDeductionsAmount and were not used here
        int[] types = { 0,1,2,3,4,9,10,15,21,22,25,26,27,28};
        // --- end Catherine
        List fees = (List)fee.findByDealAndTypes((DealPK)de.getPk(), types );
        Iterator it = fees.iterator();
        DealFee current = null;
        while(it.hasNext())
        {
          current = (DealFee)it.next();
          totalDedAmount += current.getFeeAmount();
        }
        //======================================================================
        // netMortgageAmount = totalLoanAmount - totalDeductionAmount
        // so, go and calculate it
        //======================================================================
        amt -= totalDedAmount;
/*
        TotalDeductionsAmount tda = new TotalDeductionsAmount();
        FMLQueryResult temp = tda.extract(de,lang,format,srk);

        if(temp.hasNonEmptyValue())
        {
          double tempd = Double.parseDouble(fml.getFirstStringValue(lang));
          amt -= tempd;
        }
*/
        fml.addCurrency(amt);
      }
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch (Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
