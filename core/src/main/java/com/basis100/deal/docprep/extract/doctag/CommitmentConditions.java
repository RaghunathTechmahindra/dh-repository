package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.util.*;
/**
 * 
 * @author MCM Impl Team Changes 
 * @version 1.1 12-Aug-2008 XS_16.5 updated extract() method to check the section code
 * @version 1.2 06-Oct-2008 Bug Fix :FXP22542 current.getSectionCode change to current.loadSectionCode().
 */
public class CommitmentConditions extends DocumentTagExtractor
{
 
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    Deal deal = (Deal)de;

    FMLQueryResult fml = new FMLQueryResult();
    String showCondRespRole = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.show.responsibilityrole.tag","Y");
    try
    {
       StringBuffer buf = new StringBuffer();

       int index = 0;

       ConditionHandler handler = new ConditionHandler(srk);

       // gets the Document tracking entries with tags already parsed
       List text = (List)handler.retrieveCommitmentLetterDocTracking(deal,lang);

       ListIterator li = text.listIterator();

       DocumentTracking current = null;
       String role = "Unknown Responsibility Role";
        String hasCondToExclude = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.filogix.docprep.doctags.hide.conditions");
        while(li.hasNext())
        {
           current = (DocumentTracking)li.next();
           if(hasCondToExclude != null  ){
             if( StringUtil.isInTheList(hasCondToExclude, ""+current.getConditionId() ) ){
                continue;
             }
           }
           /*************MCM Impl Team Changes XS_16.5 Added if check for select only non component related conditions*********************/
           /**
            * current.getSectionCode change to current.loadSectionCode(). 
            */
           if(!("MCM").equals(current.loadSectionCode())){
           buf.append(++index).append(".").append(format.space());
           buf.append(current.getDocumentTextByLanguage(lang)).append(format.space());
           if(showCondRespRole.equalsIgnoreCase("Y")){
             //role = PicklistData.getDescription("ConditionResponsibilityRole",current.getDocumentResponsibilityRoleId());
             //ALERT:BX:ZR Release2.1.docprep
             role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", current.getDocumentResponsibilityRoleId(), lang);
             //role = DocPrepLanguage.getInstance().getTerm(role, lang);
             buf.append('(').append(role).append(')').append(format.carriageReturn());
             buf.append( format.carriageReturn() );
           }else{
              buf.append( format.carriageReturn() );
              buf.append( format.carriageReturn() );
           }
           }
        }

        val += buf.toString();

        if(format.getType().startsWith("rtf")){
         val = this.convertCRtoRTF(val, format);
        }

      fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
     catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get CommitmentConditions ");
    }


    return fml;
  }

  private String convertCRtoRTF(String pStr, DocumentFormat format){
    if(! format.getType().startsWith("rtf") ){ return pStr; }
    int ind;
    StringBuffer retBuf = new StringBuffer("");
    while( (ind = pStr.indexOf("�")) != -1 ){
      retBuf.append(pStr.substring(0,ind));
      retBuf.append( format.carriageReturn() );
      pStr = pStr.substring(ind + 1);
    }
    retBuf.append(pStr);
    return retBuf.toString() ;
  }



}
