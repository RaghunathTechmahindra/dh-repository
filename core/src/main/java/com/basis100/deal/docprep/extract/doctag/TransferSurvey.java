package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;


public class TransferSurvey extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    int bid = deal.getBranchProfileId();

    try
    {
      BranchProfile branch = new BranchProfile(srk,bid);

      Contact cont = new Contact(srk,branch.getContactId(),1);

      Addr addr = cont.getAddr();

      int id = addr.getProvinceId();

      if(id != Mc.PROVINCE_BRITISH_COLUMBIA)
      {

        ConditionHandler ch = new ConditionHandler(srk);
        val = ch.getVariableVerbiage(deal, Dc.TRANSFER_SURVEY,lang);
        fml.addValue(val, fml.STRING);
      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
