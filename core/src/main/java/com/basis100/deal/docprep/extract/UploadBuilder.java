package com.basis100.deal.docprep.extract;

/**
* 19/Apr/2005 DVG #DG190 GE Money AU DATX Equifax upload
 */

import org.w3c.dom.*;
import org.w3c.dom.traversal.*;
import MosSystem.*;
import java.io.*;
import java.util.*;
import java.sql.*;

import com.basis100.log.*;
import com.basis100.resources.*;

import com.basis100.jdbcservices.jdbcexecutor.*;
import java.lang.reflect.*;

import com.basis100.deal.entity.*;
//import com.basis100.deal.entity.EntityBuilder;
import com.basis100.deal.pk.*;
import com.basis100.deal.xml.*;
import com.basis100.xml.*;
import com.basis100.picklist.*;
import com.basis100.deal.util.*;
import com.basis100.deal.docprep.*;
import javax.xml.parsers.DocumentBuilder;



/**
 *   This class is responsible for generating a DOM reflecting<br>
 *   The Upload File defines transformation rules for achieving the upload xml file.<br>
 *   The upload is created starting with the creation of Elements and fields reflecting the deal tree.<br>
 *   That is the generation component calls getChildren and getSecondaryParent on each encountered <br>
 *   entity starting with Deal.<br><br>
 *   Once the Deal tree has been constructed an number of rules can be applied to the nodes of the tree.
 *
 *   <ol>
 *   <li>1. Entity Level rules - Extensions to the tree:
 *     <ul>
 *     <li>a. subtree rule
 *
 *         <ol>
 *       <li>- Creates an Element with name = @name</li>
 *       <li>- Constructs an entity with class name com.basis100.deal.entity = @entity or if @entity is not present @name with primaryKey id given by  the parent node value given by @searchKey and the copyid indicated.  (See note on the copyid attribute behaviour)</li>
 *       <li>- Calls getSecondaryParents and getChildren on the entity and each delivered child constructing all possible entity and field elements.</li>
 *        </ol>
 *
 *     <li>b. </i>element rule  -performs as in the subtree rule but does not call for secondaryParents or children</li>
 *     </ul>
 *
 *   <li>2. Method Level rules - method calls the method rule calls the method <br>
 *        indicated by @name on the given entity constructed with the parentField <br>
 *        - id given by @searchKey with the indicated parameters and appends an element <br>
 *        with the name given by @elementName with the  method return 	value.
 *
 *   <li>3. Field Level rules -
 *     <dl>
 *       <dt>a. Exclude rule - exclude the field given by @name</dt>
 *       <dd></dd>
 *       <dt>b. Map rules - @type = 'picklist' map the value of the field indicated by name to the value of the picklist description for the fieldName given by @with</dt>
 *       <dd></dd>
 *   </dl>
 *
 *   </ul>
 */

public class UploadBuilder
{
  //private Document document;
  private Node schemaRoot;
  private ExtractDom parsed;

  private ElementBuilder ebuilder;

  private SessionResourceKit srk;
  private SysLogger logger;

  private Map parentChildMap;
  private Map childParentMap;

  private Element dealElement;
  private Element documentElement;

  private static int ENTITY_ACTION = 0;
  private static int SUBTREE_ACTION = 1;


  /**
   *  Constructs and initializes an UploadBuilder.
   *  @param handler - the UploadHandler that holds the .upload DOM with upload rules
   */
  public UploadBuilder(ExtractDom parsed, SessionResourceKit srk)throws DocPrepException
  {
    try
    {
      this.srk = srk;
      this.logger = srk.getSysLogger();
      parentChildMap = new HashMap(89);
      childParentMap = new HashMap(89);
     // associationList = handler.getNodeList() ;
      this.parsed = parsed;
     }
     catch(Exception e)
     {
       String msg = "DOCPREP: Failed to construct a MOSDomGenerator";
       throw new DocPrepException(msg);
     }
  }

  public Element getDealElement()
  {
    return this.dealElement;
  }

  /**
   *  Generates an XML document upload of database data as described in the upload rules file.<br/>
   *  The upload begins with the deal copy tree and rules are applied in a cumulative and <br/>
   *  serial fashion. <br/>
   *  Upload rules are  applied to the nodes of the Deal tree starting with
   *  extension rules.
   *
   *  ie. method rules would be applied to the deal tree and all extensions <br/>
   *  because method rules follow extension rules.
   *
   *  @param dealId - of deal to create upload from
   *  @param copyId - of deal to create upload from
   *
   */
  public Document generateDocument(int dealId, int copyId, int pLanguage)throws DocPrepException
  {
    ebuilder = new ElementBuilder();
    Document aDocument = XmlToolKit.getInstance().newDocument();
    return buildDoc(aDocument,dealId,copyId, pLanguage);
  }

  /**
   *  Performs the work for the generate document methods.<br/>
   *  Creates the document node and constructs the Deal entity. The Deal entity is<br/>
   *  supplied to the <code>ElementBuilder</code> and the Deal tree created. Picklist <br/>
   *  id values are converted to description form within the ElementBuilder.<br/>
   *
   *  Upload rules are then applied to the nodes of the Deal tree starting with
   *  extension rules. Rules are cumulative an are applied to all produced nodes. <br/>
   *  ie. method rules would be applied to the deal tree and all extensions because method rules
   *  follow extension rules.
   *
   */
  private Document buildDoc(Document document, int dealId, int copyId, int pLang)throws DocPrepException
  {
    documentElement = document.createElement("Document");
    document.appendChild(documentElement);

    setSchemaRoot(parsed.getDocument());

    try
    {
      DealEntity dealEntity  =  EntityBuilder.constructEntity("Deal",srk,null,dealId,copyId);
      dealElement = ElementBuilder.createSubTree(document, "Deal", srk, dealEntity, true, pLang);
      documentElement.appendChild(dealElement);
      applySchema(document, pLang);
    }
    catch(Exception fe)
    {
      String msg = "DOCPREP Error: Failed while building Upload: ";

      if(fe.getMessage() != null)
       msg += fe.getMessage();
      else
       msg += " Unknown Error. Thrown: " + fe.getClass().getName();

      logger.error(msg);
      logger.error(fe);
      throw new DocPrepException(msg);
    }

     return document;
  }

  private void applySchema(Document dom, int pLang) throws DocPrepException
  {
    TreeWalker walk = XmlToolKit.getInstance().getElementTreeWalker(dom);

    Node node = null;

    while((node = walk.nextNode()) != null)
    {
      applyExtensionsTo((Element)node, dom, pLang);
    }

    node = null;
    walk = XmlToolKit.getInstance().getElementTreeWalker(dom);

    while((node = walk.nextNode()) != null)
    {
      String name = node.getNodeName();
      applyMethodsTo((Element)node, dom);
    }

    node = null;
    walk = XmlToolKit.getInstance().getElementTreeWalker(dom);

    while((node = walk.nextNode()) != null)
    {
      applyFieldsTo((Element)node, dom, pLang);
    }

  }

  class FieldNodeFilter implements NodeFilter
  {

     public short acceptNode(Node node)
     {
       if(node.getNodeType() == Node.ELEMENT_NODE)
       {
         String name = node.getNodeName();
         if(name == null) name = "Id";

         if((node.getNodeType()== Node.ELEMENT_NODE && DomUtil.hasTextNodeChild((Element)node))
            || name.endsWith("Id")
            || Character.isLowerCase(name.charAt(0))
           )
         {
           return NodeFilter.FILTER_REJECT;
         }
         else
         {
           return NodeFilter.FILTER_ACCEPT;
         }
       }

       return NodeFilter.FILTER_REJECT;
     }
  }

  /**
   *  Obtains and applies <i>extension rules</i> outlined in the upload file<br/>
   *  for nodes matching the supplied node.<br/>
   *  Rules may result in the creation of new elements or the removal of elements. <br/>
   *
   *  @param elem - the current deal tree element
   */
  private void applyExtensionsTo(Element elem, Document dom, int pLang)
  {
    Node infonode = DomUtil.getFirstNamedChild(schemaRoot,elem.getNodeName());

    if(infonode == null)
     return;

    String nodename = infonode.getNodeName();

    List extensions = getExtensionRulesFor(nodename);

    if(!extensions.isEmpty())
    {
       Iterator eit = extensions.iterator();

       Element current = null;
       String action = null;

       while(eit.hasNext())
       {
         current = (Element)eit.next();

         action = current.getAttribute("action");

         if(action == null) continue;

         if(action.equalsIgnoreCase("subtree"))
         {
           handleExtensionRule(dom, current,elem, true, pLang);
         }
         else if(action.equalsIgnoreCase("element"))
         {
           handleExtensionRule(dom, current,elem,false, pLang);
         }
       }
    }
  }

  /**
   *  Obtains <i>field level</i> rules in the upload file for nodes matching the supplied node.<br/>
   *  Rules may result in the creation of new elements or the removal of elements.
   *
   *  @param elem - the current deal tree element
   *  @param owner -  the document owner of elem
   */
  private void applyFieldsTo(Element elem, Document owner, int pLang)throws DocPrepException
  {
    String elemname = null;
    String nodename = null;

    try
    {

      Node infonode = DomUtil.getFirstNamedChild(schemaRoot,elem.getNodeName());

      if(infonode == null)
       return;

      nodename = infonode.getNodeName();

      List fields = getFieldRulesFor(nodename);

      if(!fields.isEmpty())
      {
         Iterator fit = fields.iterator();

         Element current = null;
         String action = null;

         while(fit.hasNext())
         {
           current = (Element)fit.next();

           action = current.getAttribute("action");

           if(action == null) continue;

           if(action.equalsIgnoreCase("exclude"))
           {
             handleExcludeAction(current,elem);
           }else if(action.equalsIgnoreCase("excludeall") )
           {
             handleExcludeAllAction(current,elem);
           }
           else if(action.equalsIgnoreCase("sqlstmt"))
           {
             handleSqlStatement(current,elem,owner);
           }
           else if(action.equalsIgnoreCase("map"))
           {
             String type = current.getAttribute("type");
             if(type == null) type = "";

             if(type.equalsIgnoreCase("picklist"))
             {
               handlePicklistMapAction(current,elem);
             }else if(type.equalsIgnoreCase("renameentityfield")){
               handleRenameEntityField(current, elem, owner);
             }
           }
         }
      }
    }
    catch(Exception e)
    {
       String msg = "DOCPREP: Error applying schema fields NodeName: " + nodename + " on Element: " + elemname ;

       if(e.getMessage() != null)
           msg += " Exception: " + e.getMessage();
       else
           msg += " Unknown Reason - Exception type: " + e.getClass().getName();

       throw new DocPrepException(msg);


    }
  }

   /**
   *  Obtains method rules in the upload file for nodes matching the supplied node.<br/>
   *  Rules may result in the creation of new elements or the removal of elements.<br/>
   *
   *  @param elem - the current deal tree element
   *  @param owner -  the document owner of elem
   */
  private void applyMethodsTo(Element elem, Document owner)
  {
    Node infonode = DomUtil.getFirstNamedChild(schemaRoot,elem.getNodeName());

    if(infonode == null)
     return;

    String nodename = infonode.getNodeName();

    List methods = getPEMethodRulesFor(nodename);

    if(!methods.isEmpty())
    {
       Iterator mit = methods.iterator();

       Element current = null;

       while(mit.hasNext())
       {
         current = (Element)mit.next();

         handlePEMethodAction(current,elem,owner);
       }
    }
  }

 /**
  *  Obtains Method rule nodes and applies them to the given parent. The parent must be <br/>
  *  a DealEntity class which can be constructed using the searchkey <br/>
  *  node present as child under the parent. The method called must be within <br/>
  *  the parent entity.<br/>
  *
  *  @param schema -  the upload file node with the relevant instructions
  *  @param parent -  the parent recieving any produced elements
  *  @param owner -  the document owner of parent
  */
  private void handlePEMethodAction(Element schema, Element parent, Document owner)
  {
    if( parent == null || schema == null) return;

    //name of Element to build
    String elementName = schema.getAttribute("elementName");

    if(elementName == null) return;

    try
    {
      String methodVal = handleInvokeMethod(parent , schema);

      if(methodVal != null)
      {
        Element methodElement = owner.createElement(elementName);
        methodElement.appendChild(owner.createTextNode(methodVal));
        parent.appendChild(methodElement);
      }


    }
    catch(Exception e)
    {
      srk.getSysLogger().debug("UploadBuilder : tried to invoke method for " +
                                elementName + " and failed.");
      return;
    }

  }

  private void handleExcludeAllAction(Element schema, Element parent)
  {
    if( parent == null || schema == null) return;
    String name = schema.getAttribute("name");
    for(;;){
      Element elem = (Element)DomUtil.getFirstNamedChild(parent, name) ;
      if(elem == null){break;}
      parent.removeChild(elem);
    }
    if(!parent.hasChildNodes() && parent.getNodeType() != parent.DOCUMENT_NODE)
    {
      Node grandparent = parent.getParentNode();

      if(grandparent != null)
       grandparent.removeChild(parent);
    }
  }

 /**
  *  gets exclude rule nodes and applies them to the given parent by removing the <br/>
  *  indicated child node if present. If removal empties the parent of element children<br/>
  *  the parent is also removed unless it is the document node itself.
  *
  *  @param schema -  the upload file node with the relevant instructions
  *  @param parent -  the parent recieving any produced elements
  */
  private void handleExcludeAction(Element schema, Element parent)
  {
    if( parent == null || schema == null) return;

    String name = schema.getAttribute("name");

    Element elem = (Element)DomUtil.getFirstNamedChild(parent, name) ;

    if(elem != null)
    {
      parent.removeChild(elem);

      if(!parent.hasChildNodes() && parent.getNodeType() != parent.DOCUMENT_NODE)
      {
        Node grandparent = parent.getParentNode();

        if(grandparent != null)
         grandparent.removeChild(parent);
      }
    }
  }
  /**
   * @param - schema - the Element node with rule instructions
   * @param - parent -  the Element node to append create nodes to.
   * @param - owner -  the Document being built.
   */
  private void handleSqlStatement(Element schema, Element parent, Document owner)
  {
    if( parent == null || schema == null) return;

    String elementName = schema.getAttribute("elementName");
    String query = schema.getAttribute("query");

    List params = DomUtil.getNamedChildren( schema, "Param") ;

    try
    {
      String value = executeStatement(schema, parent, query, params);

      if(value == null || value.trim().length() == 0) return;

      Element newElement = owner.createElement(elementName);

      DomUtil.setElementText(newElement,value);

      parent.appendChild(newElement);
    }
    catch(Exception e)
    {
       logger.debug("DOCPREP: Failed to execute stmt: " + query);
    }

  }


  private String executeStatement(Element schema, Element parent, String query, List params)throws Exception
  {
    if( query == null || params == null ) return null;

    int paramCount = 0;
    char[] queryArr = query.toCharArray();
    int len = queryArr.length;

    char curr = ' ';

    for(int i = 0; i < len; i++)
    {
      curr = queryArr[i];
      if(curr == '?')  paramCount++;
    }


    if(paramCount != params.size())
      throw new Exception("Invalid sql statment : incorrect number of parameters");

    Iterator it = params.iterator();
    Element param = null;
    String type = null;
    String searchKey = null;

    while(it.hasNext())
    {
      param = (Element)it.next();

      if(param == null) continue;

      type = param.getAttribute("type");
      searchKey = param.getAttribute("searchKey");

      query = insertParam(parent, query, searchKey);
    }

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String value = null;

    try
    {
        int key = jExec.execute(query);

        for (; jExec.next(key); )
        {
          value = jExec.getString(key,1);
          break;
        }

        jExec.closeData(key);

    }
    catch(Exception e1)
    {
      logger.error("DOCPREP: UploadBuilder sqlstmt failed. Sql: " + query);
      logger.error(e1);
      return null;
    }

    return value;

  }

  private String insertParam(Element parent, String query, String key)
  {
    String toInsert = DomUtil.getChildValueIgnoreCase(parent,key);

    int index = query.indexOf('?');
    StringBuffer qbuf = null;

    if(index > 0)
    {
      qbuf = new StringBuffer(query);
      qbuf.replace(index,index + 1,toInsert);
      query = qbuf.toString();
    }

    return query;
  }

  private void handleRenameEntityField( Element schema, Element parent, Document owner){
    if( parent == null || schema == null) return;

    String name = schema.getAttribute("name");

    String tablename = null;

    String with = schema.getAttribute("with");

    if(with == null) return;

    Element elem = (Element)DomUtil.getFirstNamedChild(parent, name) ;

    if(elem != null)
    {

      String text = null;

      try
      {
        text = DomUtil.getNodeText(elem);
        Element renamedNode = owner.createElement(with);
        DomUtil.appendString(renamedNode, text );
        parent.appendChild(renamedNode);
        //owner.getFirstChild().appendChild(renamedNode);
      }
      catch(Exception e)
      {
      }
    }
  }

  private void handlePicklistMapAction(Element schema, Element parent)
  {
    if( parent == null || schema == null) return;

    String name = schema.getAttribute("name");

    String tablename = null;

    String with = schema.getAttribute("with");

    if(with == null) return;

    with = with.trim();

    if(with.length() < 3) return;

    if(with.endsWith("Id"))
    {
      with = with.substring(0,with.length() - 2);
    }

    Element elem = (Element)DomUtil.getFirstNamedChild(parent, name) ;

    if(elem != null)
    {

      String text = null;

      try
      {
        text = DomUtil.getNodeText(elem);
      }
      catch(Exception e)
      {
      }

      // zivko:marker  visit this spot for french support for fx link
      text = PicklistData.getDescription(with,text,true);
      DomUtil.setElementText(elem, text);
    }
  }

  private void handleExtensionRule(Document dom, Element schema, Element parent, boolean subtree, int pLang)
  {
     if( parent == null || schema == null) return;

     String specialProcessing = schema.getAttribute("callProcessingMethod");
     if( specialProcessing != null && specialProcessing.equalsIgnoreCase("true")){
        String lName = schema.getAttribute("nodeName");
        List lParams = DomUtil.getNamedChildren( schema, "Param") ;
        doSpecialProcessing(lName,lParams,dom,schema,parent, pLang);
        return;
     }
     int fkid = -1;
     boolean method = false;

     String elementName = schema.getAttribute("name");

     if(elementName == null) return;

     //the name of the entity to create:
     String entityName = schema.getAttribute("entity");


     //determine if the entity is a dbchild
     // defaults to false
     String dbChildAttr = schema.getAttribute("dbchild");
     if(dbChildAttr == null) dbChildAttr = "false";
     boolean isDBChild = dbChildAttr.equalsIgnoreCase("true");


     //the key field of the above entity:
     String searchField = schema.getAttribute("searchKey");
     Element sub = null;

     if(searchField == null || searchField.length() == 0)
     {
       sub = (Element)DomUtil.getFirstNamedChild(schema, "SearchKey");
       String subtype = "";

       if(sub != null)
        subtype = sub.getAttribute("type");

       if(subtype.equalsIgnoreCase("pemethod"))
       {
         method = true;

         try
         {
           String fkval =  handleInvokeMethod(parent, sub);
           fkid = Integer.parseInt(fkval);
         }
         catch(Exception e)
         {
           ;
         }
       }
     }

     //determine if the entity is a copytype (ie in the deal copy tree);
     // defaults to true
     String copyAttr = schema.getAttribute("copyid");
     if(copyAttr == null) copyAttr = "true";
     boolean isCopyType = !copyAttr.equalsIgnoreCase("false");

     //get the name of the parent node -  (the database parent or child)
     String parentName = parent.getNodeName();

     //if the entity name is not indicated it is the same as the element to create
     if(entityName == null || entityName.trim().length() == 0)
        entityName = elementName;

     if(!method)
     {
       if(isDBChild)
       {
          searchField = parentName + "Id";
          fkid = findFKID(parent,searchField);
       }
       else
       {
          fkid = findFKID(parent, searchField);
       }
     }

      int copyid = findCopyId(schema, parent);

     Element stree = null;

     //if it is a database parent relation
     if(!isDBChild)
     {
        stree = ElementBuilder.createSubTree(dom, elementName, srk, fkid, copyid, entityName, true, pLang);

        if(stree != null)
        {
          parent.appendChild(stree);
          return;
        }
     }
     else
     {
        List ids = findChildEntityIds(parentName, searchField, entityName, fkid, copyid, isCopyType);

        Iterator it = ids.iterator();
        Integer curr = null;

        //  build an entity and construct an element for each id
        // append to the parent
        while(it.hasNext())
        {
           curr = (Integer)it.next();

           Element append = ElementBuilder.
              createEntityElement(dom, entityName, srk, curr.intValue(),
                                  copyid, entityName, true, pLang);

           parent.appendChild(append);
        }
     }


  }

  public int findFKID(Element parent, String searchField)
  {
     String value = DomUtil.getChildValueIgnoreCase(parent,searchField);

     int fkid = -1;
     try
     {
       return Integer.parseInt(value);
     }
     catch(Exception e){ return fkid;}
  }


  public int findCopyId(Element schema, Element parent)
  {
    String schemaVal = schema.getAttribute("copyid");

    int numericVal = -1;

    try
    {
      numericVal = Integer.parseInt(schemaVal);
    }
    catch(NumberFormatException nfe)
    {
      ;//fall through
    }

    String val = DomUtil.getChildValueIgnoreCase(parent, "copyId");

    try
    {
      numericVal = Integer.parseInt(val);
    }
    catch(Exception e)
    {
      return -1;
    }

    return numericVal;
  }

  private List findChildEntityIds(String pname, String pfield, String entity, int fkid, int copyId, boolean copy)
  {
      List out = new ArrayList();

      JdbcExecutor jExec = srk.getJdbcExecutor();

      String query = "Select " + entity + "Id from "  + entity + " where " + pfield +
                    " = " + fkid;

      if(copy) query += " and copyId = " + copyId;


      try
      {
          int key = jExec.execute(query);
          int val = -1;

          for (; jExec.next(key); )
          {
            val = jExec.getInt(key,1);

            if(val != -1)
            {
              out.add(new Integer(val));
            }

          }

          jExec.closeData(key);

      }
      catch(Exception e1)
      {
        logger.error("DOCPREP: UploadBuilder sqlstmt failed. Sql: " + query);
        logger.error(e1);
        return out;
      }

    return out;

  }



  private List getExtensionRulesFor(String name)
  {
    return getSchemaNodes(name,"Extension",null,null);
  }

  private List getFieldRulesFor(String name)
  {
    return getSchemaNodes(name,"Field",null,null);
  }

  private List getPEMethodRulesFor(String name)
  {
    return getSchemaNodes(name,"PEMethod",null,null);
  }

  private Map getMappedFieldsFor(String name)
  {
    List nodes = getSchemaNodes(name,"Field","action","map");

    Map out = new HashMap();

    Iterator ni = nodes.iterator();

    Element current = null;
    String key = null;
    String val = null;

    while(ni.hasNext())
    {
      current = (Element)ni.next();

      key = current.getAttribute("name");
      val = current.getAttribute("with");

      if(key != null && val != null)
      {
        out.put(key,val);
      }
    }

    return out;
  }


  /**
   *  gets a list of the values associated with a given node name with a given type
   *  attribute and value. For instance get values for child nodes of name = "Borrower"
   *  of type "Field" where the given attribute has the given value
   */
  private List getSchemaNodes(String name, String type, String attr, String value)
  {
    Node n = DomUtil.getFirstNamedChild(schemaRoot, name);

    List l = DomUtil.getNamedDescendants(n, type);

    if(l.isEmpty()) return l;

    List out = new ArrayList();

    Iterator li = l.iterator();
    Node current = null;

    while(li.hasNext())
    {
       current = (Node)li.next();

       if(attr != null &&  value != null)
       {
          String attval = DomUtil.getAttributeValue(current,attr);

          if(attval.equals(value))
           out.add(current);
       }
       else
       {
         out.add(current);
       }
    }

    return out;
  }

  private String getFieldValue(DealEntity de, String fieldName)
  {
    return de.getStringValue(fieldName);
  }


  private void setSchemaRoot(Document schema)
  {
    this.schemaRoot = DomUtil.getFirstNamedChild(schema, "Document");
  }

  /**
   *  Returns this Builder's handler handler.
   */
  public ExtractDom getExtractDom()
  {
    return this.parsed;
  }


  public String toXMLString(Document dom, String enc, boolean format)
  {
    String out = null;

    //ALERT: ADDRESS ENCODING ISSUES.

    try
    {
      StringWriter wr = new StringWriter();
      XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();
      if(enc != null){
        writer.setEncoding(enc);
      }

      out = writer.printString(dom);
    }
    catch(Exception e)
    {
      return null;
    }

    return out;
  }

  public String toXMLString(Document dom, int pLanguage)
  {
    if(pLanguage == Mc.LANGUAGE_PREFERENCE_FRENCH){
      return toXMLString(dom, "iso-8859-1", false);
    }else{
      return toXMLString(dom, "UTF-8", false);
    }

  }



  public String handleInvokeMethod(Element pe,  Element meth)throws Exception
  {
    if(pe == null || meth == null) return null;

    String searchKey = meth.getAttribute("searchKey");

    String methodName = meth.getAttribute("name");
    //name of Element to build

    String entityName = pe.getNodeName();


    if(entityName == null || methodName == null || searchKey == null)
     return null;

    int fkid = this.findFKID(pe, searchKey);
    if(fkid == -1) return null;

    int copyid = this.findCopyId(meth, pe);
    DealEntity de = null;

    try
    {
       de = EntityBuilder.constructEntity(entityName, srk, null, fkid, copyid);
    }
    catch(Exception e)
    {
      srk.getSysLogger().debug("UploadBuilder : tried to build entity " + entityName + " and failed.");
      return null;
    }

    return invokeMethod(de, meth);
  }


  public String invokeMethod(Object on, Node methNode)throws Exception
  {
    if(on == null || methNode == null) return null;

    List paramNodes = DomUtil.getNamedChildren(methNode, "Param");

    List parameters = getParameters(paramNodes);
    String name = DomUtil.getAttributeValue(methNode,"name");

    Class[] ptypes = getParameterTypes(parameters);
    Object[] pvals = getParameterValues(parameters);

    String type = null;
    try
    {
      Method method = on.getClass().getDeclaredMethod(name, ptypes);
      type = method.getReturnType().getName();

      if(type.equals("java.lang.String"))
      {
        String result = (String)method.invoke(on,pvals);
        if( result == null || result.equals("null") || result.equals("") )
          return null;
        else
          return result;
      }
      else if(type.equals("int"))
      {
        Integer ret = ((Integer)method.invoke(on,pvals));
        return TypeConverter.stringTypeFrom(ret.intValue());
      }
      else if(type.equals("short"))
      {
        Short ret = ((Short)method.invoke(on,pvals));
        return TypeConverter.stringTypeFrom(ret.shortValue());
      }
      else if(type.equals("long"))
      {
        Long ret = ((Long)method.invoke(on,pvals));
        return TypeConverter.stringTypeFrom(ret.longValue());
      }
      else if(type.equals("double"))
      {
        Double ret = ((Double)method.invoke(on,pvals));
        return TypeConverter.stringTypeFrom(ret.doubleValue());
      }
      else if(type.equals("char"))
      {
        Character ret = (Character)method.invoke(on,pvals);
        return ret.toString();
      }
      else if(type.equals("float"))
      {
        Float ret = ((Float)method.invoke(on,pvals));
        return TypeConverter.stringTypeFrom(ret.floatValue());
      }
      else if(type.equals("boolean"))
      {
        Boolean ret = ((Boolean)method.invoke(on,pvals));
        return TypeConverter.stringFromBoolean(ret.booleanValue(),"N","Y");
      }
      else if(type.equals("java.sql.Date") || type.equals("java.util.Date") )
      {
        java.util.Date dt = (java.util.Date)method.invoke(on,pvals);
        if( dt == null)
         return null;
        else
         return TypeConverter.stringTypeFrom(dt);
      }
      else throw new Exception();
    }
    catch(Exception ex )
    {
      //========================================================================
      // zivko made this change on Oct 24, 2002 because logger does not tell
      // what is the name of method or what is the required type.
      //========================================================================
      String msg = "Error getting value for type: " + type;
      msg += " for field named: " + name;
      logger.debug(msg);
      logger.error(ex);

    }
  return null;
 }


 private List getParameters(List nodes)throws Exception
 {
    Iterator li = nodes.iterator();
    Node current = null;
    List out = new ArrayList();

    while(li.hasNext())
    {
      current = (Node)li.next();

      String type = DomUtil.getAttributeValue(current,"type");
      String value = DomUtil.getAttributeValue(current,"value");

      if(type.equals("java.lang.String"))
      {
        out.add(new Param(String.class,value));
      }
      if(type.equals("String"))
      {
        out.add(new Param(String.class,value));
      }
      else if(type.equals("int"))
      {
        out.add(new Param(int.class, new Integer(value)));
      }
      else if(type.equals("short"))
      {
        out.add(new Param(short.class, new Integer(value)));
      }
      else if(type.equals("long"))
      {
        out.add(new Param(long.class, new Long(value)));
      }
      else if(type.equals("double"))
      {
        out.add(new Param(double.class,new Double(value)));
      }
      else if(type.equals("char"))
      {
        out.add(new Param(char.class,new Character(value.charAt(0))));
      }
      else if(type.equals("float"))
      {
        out.add(new Param(float.class,new Float(value)));
      }
      else if(type.equals("boolean"))
      {
        out.add(new Param(boolean.class,new Boolean(value)));
      }

    }
    return out;
  }


  public Class[] getParameterTypes(List pars)
  {
    Iterator li = pars.iterator();
    Param current = null;
    Class[] cl = new Class[pars.size()];
    int index = 0;

    while(li.hasNext())
    {
      current = (Param)li.next();
      cl[index] = current.type;
      index++;
    }
    return cl;
  }

  public Object[] getParameterValues(List pars)
  {
    Iterator li = pars.iterator();
    Param current = null;
    Object[] cl = new Object[pars.size()];
    int index = 0;

    while(li.hasNext())
    {
      current = (Param)li.next();
      cl[index] = current.value;
      index++;
    }

    return cl;
  }

  class Param
  {
      Class type;
      Object value;

      public Param(Class clss, Object val)
      {
        type = clss;
        value = val;
      }

  }

  private void doSpecialProcessing(String pName, List pParams, Document dom, Element schema, Element parent, int pLang){
    String lSearchKey;
    String value;
    int fkid;
    if(pName.equals("PartyProfile")){
      try{
        lSearchKey =  DomUtil.getElementText( (Element) pParams.get(0) );
        value = DomUtil.getChildValueIgnoreCase(parent,lSearchKey);

        fkid = Integer.parseInt(value);

        PartyProfile partyProfile = new PartyProfile(srk);
        Collection ppCollection = partyProfile.findByDealAndType(fkid, -1);

        Iterator it = ppCollection.iterator();
        while( it.hasNext() ){
          PartyProfile pp = (PartyProfile) it.next();
          Element ppElem = ElementBuilder.createEntityElement(dom,"PartyProfile",srk,pp.getPartyProfileId(),-1,"PartyProfile",true, true, pLang);
          parent.appendChild(ppElem);
        }
      }catch(Exception exc){
          // do nothing here
      }
    }else if(pName.equals("DealNotes")){
      // zivko:marker coding should take place here
      try{
        lSearchKey =  DomUtil.getElementText( (Element) pParams.get(0) );
        value = DomUtil.getChildValueIgnoreCase(parent,lSearchKey);

        fkid = Integer.parseInt(value);

        DealNotes dn = new DealNotes(srk);

        Collection dnCollection = dn.findByDeal(new DealPK(fkid,-1));
        Iterator it = dnCollection.iterator();
        while(it.hasNext()){
          DealNotes oneNote = (DealNotes)it.next();
          // ticket #258
          if(oneNote.getDealNotesCategoryId() == Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION){
            continue;
          }
          Element ppElem = ElementBuilder.createEntityElement(dom,"DealNotes",srk,oneNote.getDealNotesId(),-1,"DealNotes",true, true, pLang);
          parent.appendChild(ppElem);
        }

      }catch(Exception dnException){
          // do nothing here
      }
    }  // end if it is PARTY PROFILE

    //#DG190 insert cb in xml format
    else if(pName.equals("equifaxCreditReportXml")){
      // zivko:marker coding should take place here
      try{
        lSearchKey =  DomUtil.getElementText( (Element) pParams.get(0) );
        value = DomUtil.getChildValueIgnoreCase(parent,lSearchKey);
        fkid = Integer.parseInt(value);
        lSearchKey =  DomUtil.getElementText( (Element) pParams.get(1) );
        value = DomUtil.getChildValueIgnoreCase(parent,lSearchKey);
//        int acopyId = findCopyId(schema, parent);
        int acopyId = Integer.parseInt(value);
        
        Borrower abor = new Borrower(srk, null);
        abor = abor.findByPrimaryKey(new BorrowerPK(fkid, acopyId));
        String cb = abor.getEquifaxCreditReportXml();

        // now parse it and insert as xml node
        DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();
        Document cbdom =  parser.parse(new ByteArrayInputStream(cb.getBytes()));
        Element cbele = dom.createElement(pName);
        Element cbNod = cbdom.getDocumentElement();
        cbele.appendChild(dom.importNode(cbNod, true));
        parent.appendChild(cbele);

      }catch(Exception dnException){
          logger.debug(pName+StringUtil.stack2string(dnException));
      }
    }      //#DG190 end
  }

}
