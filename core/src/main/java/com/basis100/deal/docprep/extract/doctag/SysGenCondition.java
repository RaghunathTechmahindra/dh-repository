package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class SysGenCondition extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());
    FMLQueryResult fml = new FMLQueryResult();

    DocumentTracking dt = (DocumentTracking)de;
    String showCondRespRole = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.show.responsibilityrole.tag","Y");
    try
    {


      int id = dt.getDocumentResponsibilityRoleId();
      String role;

      String val = dt.getDocumentTextByLanguage(lang);
      if(showCondRespRole.equalsIgnoreCase("Y")){
        //role = PicklistData.getDescription("ConditionResponsibilityRole",id);
        //ALERT:BX:ZR Release2.1.docprep
        role  = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole",  id, lang);
        //role = DocPrepLanguage.getInstance().getTerm(role, lang);
        val += format.space() + "(" + role + ")";
      }
      if(debug) val = "[ ConditionLabel: " + dt.getDocumentLabelByLanguage(lang) + " ]"  + val;

      fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
