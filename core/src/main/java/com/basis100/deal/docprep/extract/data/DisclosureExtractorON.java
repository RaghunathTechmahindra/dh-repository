package com.basis100.deal.docprep.extract.data;

import org.w3c.dom.*;

import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.entity.*;
import com.basis100.xml.*;

import com.basis100.entity.FinderException;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2002</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class DisclosureExtractorON extends GENXExtractor {

  DisclosureDataProvider ddp;
  double totalCheckedFees = 0;

  /**
   * Class constructor
   */
  public DisclosureExtractorON(){}
 
  /**
   * Called by DocumentGeneratorHandler for all document types
   *
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }

  /**
   * Builds entire xml structure for Disclosure Document (called Statement of
   * Mortgage as well) for Ontario
   *
   * @throws Exception
   */
  protected void buildXMLDataDoc() throws Exception  {
    doc = XmlToolKit.getInstance().newDocument();

    Element docRoot = doc.createElement("Document");
    docRoot = buildBaseTags(docRoot);
    doc.appendChild(docRoot);
  }

   /**
    * Extracts all the necessery data from deal to make Disclosure document. Creates root node and
    * all the children nodes of a DOM document. Each node of the document (except root) has 
    * a structure: <nodeName type="string">. Type can be "string", "date", "int" and "double" and 
    * is used by the class RPTMerger to recreate proper data types from String provided by xml and 
    * create hashMap. If type of node is "date" it must be in format defined by static 
    * DisclosureDataProvider.DDP_DATE_FORMAT. That hushMap will then be passed to ReportMill together 
    * with template name in order to produce a PDF file. 
    *
    * @param root Element
    * @return updated root Element
    * @throws ExtractException
    */
   private Element buildBaseTags(Element root) throws ExtractException {

	 ddp = new DisclosureDataProvider(srk, deal, lang);
	 ddp.setDocumentFormat(format);
	 ddp.setDocumentTypeId(findDocumentTypeId());
	   
	 try{	   
		Property pp = getPrimaryProperty();	

		totalFees = 0d;
		   
		pCreateNode(root, "lenderOffice", "string", ddp.exLenderShortName());											// A
		pCreateNode(root, "transactionNumber", "string", Integer.toString(ddp.exDealId()));							// B
		pCreateNode(root, "propAddrLine1", "string", ddp.exLegalLines(pp));												// C + D
		pCreateNode(root, "propAddrLine2", "string", ddp.exPrimaryPropertyAddressLine1(pp));						// D1
		pCreateNode(root, "isActingAsLender", "string", ddp.exQuestion("020E"));										// E
		pCreateNode(root, "actingAsLenderDescription", "string", ddp.exQuestion("020F"));							// F
		pCreateNode(root, "commitDate", "date", ddp.formatDate(ddp.exCommitmentIssueDate()));						// G
		pCreateNode(root, "regularOrCollateral", "string", "Regular");														// H
		pCreateNode(root, "firstSecondOrThird", "string", ddp.exLienPosition());										// I		
		pCreateNode(root, "principalAmt", "double", Double.toString(ddp.exTotalLoanAmount()));						// J and FF
		pCreateNode(root, "intRate", "string", formatInterestRate(ddp.exNetInterestRateFixed()));					// K
		pCreateNode(root, "variableIntRate", "string", formatInterestRate(ddp.exNetInterestRateVariable()));	// L
		pCreateNode(root, "repayFreq", "string", ddp.exPaymentFrequency());												// M
		pCreateNode(root, "freqPmtAmt", "double", Double.toString(ddp.exTotalPaymentAmount()));					// N
		pCreateNode(root, "plusOrIncluding", "string", ddp.exRepaymentType());											// O
		pCreateNode(root, "years", "string", ddp.exAmortizationTermInYears());											// P
		pCreateNode(root, "compounding", "string", ddp.exInterestCompoundingDescription());							// Q
		   
		addFeesCommon("MtgBroker", DisclosureDataProvider.pMTG_BROKER_FEES, root);										// R
		addFeesCommon("Bonus", DisclosureDataProvider.pBONUS_FEES, root);													// S
		addFeesCommon("OtherLendersFees", DisclosureDataProvider.pOTHER_LENDER_FEES, root);							// T
//	not used      addTempFunct("LenderLegal");																					// U		   
		addFeesCommon("Appraisal", DisclosureDataProvider.pAPPRAISAL_FEES, root);										// V
		addOther1Fees(root, "MI Premium");																							// W
		addOther2Fees(root, "Provincial Tax on MI Premium");																	// X
		addOther3Fees(root, "Insurance Fee");																						// Y
//	not used      addTempFunct("OtherFee4");																						// Z
//	not used      addTempFunct("OtherAmt1");																						// AA
//	not used      addTempFunct("OtherAmt2");																						// BB
//	not used      addTempFunct("OtherAmt3");																						// CC
//	not used      addTempFunct("OtherAmt4");																						// DD
		   
		pCreateNode(root, "feeTotalAmt", "string", Double.toString(totalFees));										// EE1
		pCreateNode(root, "feeTotalDeductedAmt", "double", Double.toString(totalCheckedFees));					    // EE2
		pCreateNode(root, "netAdvanceAmt", "double", Double.toString(ddp.exTotalLoanAmount() - totalCheckedFees));	// HH		
		pCreateNode(root, "totalCostOfBorrowingPct", "string", formatInterestRate(ddp.exTotalCostOfBorrowing()));	// II
		
		pCreateNode(root, "termLength", "string", Integer.toString(ddp.exActualPaymentTerm()));					    // KK
		pCreateNode(root, "termLengthUnits", "string", "Months");													// LL
		pCreateNode(root, "remainingAmt", "double", Double.toString(ddp.exBalanceRemainingAtEndOfTerm()));		    // MM
		pCreateNode(root, "privilegesOrPenalties", "string", ddp.exQuestion("08NN"));								// NN
		pCreateNode(root, "otherTermsAndConditions", "string", ddp.exQuestion("09OO"));								// OO
		pCreateNode(root, "commitExpiresDate", "date", ddp.formatDate(ddp.exEstimatedClosingDate()));			    // PP, QQ and RR
		pCreateNode(root, "bkrName", "string", ddp.exSourceFirmName());												// SS, YY
		pCreateNode(root, "bkrRegNumber", "string", ddp.exQuestion("11TT"));										// TT
		pCreateNode(root, "bkrSignatureDate", "date", ddp.formatDate(ddp.exBkrSignatureDate()));					// UU
		pCreateNode(root, "bkrFullName", "string", ddp.exSourceOfBusinessContactName());							// VV
		pCreateNode(root, "borrowerName", "string", ddp.exPrimaryBorrowerName());									// WW
		pCreateNode(root, "bkrOneLineNameAndAddress", "string", lenderRegistrationNameAndAddress(ddp));				// YY + XX
		pCreateNode(root, "borrowerAddr", "string", ddp.exPrimaryBorrowerAddress());								// ZZ
	 }
	 catch( Exception e ) {
		e.printStackTrace();
		String msg = "DisclosureExtractorON failed to extract data from DB." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		srk.getSysLogger().warning(this.getClass(), msg);
		throw new ExtractException(msg);
	 }
	 return root;
  }


   /**
    * Common method for fee processing. Creates feeDescription by concatenating 
    * descriptions of all the fees IDs listed in pFeesGroup array and totals their amounts.
    * 
    * @param tagRoot String
    * @param pFeesGroup int[]
    */
   private void addFeesCommon(String tagRoot, int [] pFeesGroup, Element root ) {
   	double fee = 0d;
   	fee = ddp.exSelectedFeesTotalAmt(pFeesGroup);
   	if( fee != 0d ) {
   		totalFees += fee;
   		pCreateNode(root, "fee" + tagRoot + "Desc", "string", ddp.exSelectedFeesDescription(pFeesGroup));
   		pCreateNode(root, "fee" + tagRoot + "Amt", "string", Double.toString(fee));
   		if( fee > 0d ) {
   			pCreateNode(root, "isFee" + tagRoot + "AmtDeducted", "string", "X");
   			totalCheckedFees += fee;
   		}
   	}
   }

   /**
    * feeOtherFee1 (MI Premium) processor
    * 
    * @param root
    */
   private void addOther1Fees(Element root, String name) {
   	addOther1_or_Other2Fees("OtherFee1", deal.getMIPremiumAmount(), name, root);
   }

   /**
    * feeOtherFee2 (Provincial Tax on MI) processor
    * 
    * @param root
    */
   private void addOther2Fees(Element root, String name) {   		
   	addOther1_or_Other2Fees("OtherFee2", deal.getMIPremiumPST(), name, root);
   }

   /**
    * feeOtherFee1 and feeOtherFee2 processor. Difference is in amt passed 
    * and ID in the pFeesGroup.
    * 
    * @param tagRoot
    * @param pFeesGroup
    * @param amt
    * @param root
    */
   private void addOther1_or_Other2Fees(String tagRoot, double amt, String name, Element root) {
   	try{
   		totalFees += amt;
   		pCreateNode( root, "fee" + tagRoot + "Desc", "string", name);
   		pCreateNode(root, "fee" + tagRoot + "Amt", "string", Double.toString(amt));
   		if( amt > 0d ) {
   			pCreateNode(root, "isFee" + tagRoot + "AmtDeducted", "string", "X");
   			totalCheckedFees += amt;
   		}
   	}
   	catch (Exception e){
   		srk.getSysLogger().warning(this.getClass(), "DisclosureExtractorON failed during extraction of " + name + ".\n" + e.getMessage());
   	};
   }
	
	/**
	 * Extracts feeOther3Fees (Insurance Fee)
	 * 
	 * @param root
	 * @param name
	 */
	private void addOther3Fees( Element root, String name) {
		try{
			double fee = ddp.exMIFees(deal.getMortgageInsurerId());
			if( fee != 0d ) {
				totalFees += fee;
				pCreateNode(root, "feeOtherFee3Desc", "string", name);
				pCreateNode(root, "feeOtherFee3Amt", "string", Double.toString(fee));
				if( fee > 0d ) {
					pCreateNode(root, "isFeeOtherFee3AmtDeducted", "string", "X");
					totalCheckedFees += fee;
				}
			}
		}
   	catch (Exception e){
   		srk.getSysLogger().warning(this.getClass(), "DisclosureExtractorON failed during extraction of " + name + ".\n" + e.getMessage());
   	};		
	}

   /**
    * Retrurns lenderShortName and lenderAddress as one string
    * 
    * @param ddp
    * @return
    */
   private String lenderRegistrationNameAndAddress(DisclosureDataProvider ddp) {
	   return ddp.exLenderRegistrationName() + format.space() + ddp.exLenderAddress();
   }
  
   /**
    * Finds documentType id necessery for DisclosureQuestion table search.
    * 
    * @return
    */
   private int findDocumentTypeId() {
   	int documentTypeId = -1;
   	String errMsg = "DisclosureExtractorON trying to find its documentTypeId.\n";
   	try {
   		DocumentProfile dp = (new DocumentProfile(srk)).findByClassName(this.getClass().getName(), lang);
   		documentTypeId = dp.getDocumentTypeId();		
   	}
   	catch(FinderException e ){
   		srk.getSysLogger().warning(this.getClass(), errMsg + e.getMessage());		
   	}
   	catch( Exception e ) {
   		srk.getSysLogger().warning(this.getClass(), errMsg + e.getMessage());		
   	}
   	return documentTypeId;
   }
   	
   /**
    * 
    * @param root
    * @param nodeName
    * @param nodeType
    * @param nodeText
    */
   private void pCreateNode(Node root, String nodeName, String nodeType, String nodeText ) {
   	if( nodeText != null && nodeText.trim().length() != 0 ) {
   		Node xNode = getTag(nodeName, nodeText);
   		((Element)xNode).setAttribute("type", nodeType);
   		addTo(root, xNode);
   	}
   }
}
