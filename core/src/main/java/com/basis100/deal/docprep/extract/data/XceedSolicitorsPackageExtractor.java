package com.basis100.deal.docprep.extract.data;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import MosSystem.Mc;

import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Province;
import com.basis100.deal.pk.DealPK;
import com.basis100.picklist.BXResources;

public class XceedSolicitorsPackageExtractor extends GENXSolicitorsPackageExtractor
{
    protected void addCLTags(Node root)
    {
        root.appendChild(new XceedCommitmentExtractor((XSLExtractor)this).getCommitmentTags(doc));
    }

    /**
     * Builds the XML doc from various records in the database.
     *
     */
    protected void buildXMLDataDoc() throws Exception
    {
        super.buildXMLDataDoc();

        NodeList nl = doc.getElementsByTagName("SolicitorsPackage");

        Node root = nl.item(0);

        //==========================================================================
        // Lender Name Fund
        //==========================================================================
        addTo(root, extractLenderFund());

        //==========================================================================
        // Lender Profile Id -> Derek Memorial Code (2003, July, 09 )
        //==========================================================================
        addTo(root, extractLenderProfileId() );
        //==========================================================================
        // Sole Joint Tenants
        //==========================================================================
        addTo(root, extractSoleJointTenants());

        //==========================================================================
        // Loan Amount Pre Fee
        //==========================================================================
        addTo(root, extractLoanAmountPreFee());

        // =========================================================================
        // Tax portion extracted will be adjusted by payment frequency
        // what we need is monthly portion. This method provides tax portion (monthly).
        // =========================================================================
        addTo( root,extractTaxPortionMonthly() );

        //==========================================================================
        // Tax Portion
        //==========================================================================
        addTo(root, extractTaxPortion());

        //==========================================================================
        // Current Year
        //==========================================================================
        addTo(root, extractCurrentYear());

        //==========================================================================
        // Per Diem
        //==========================================================================
        addTo( root, extractPerDiem() );

        //==========================================================================
        // Primary Property Province
        //==========================================================================
        addTo( root, extractPrimaryPropertyProvince() );

        //==========================================================================
        // Lien Position and lienPositionTerm
        //==========================================================================
        addTo( root, extractLienPosition() );
        //addTo( root, extractLienPositionTerm() );

        //==========================================================================
        // Refi flag
        //==========================================================================
        addTo( root, extractRefiFlag() );

        //==========================================================================
        // Undertaking flag
        //==========================================================================
        addTo( root, extractUndertakingFlag() );

        //==========================================================================
        // Mortgage #
        //==========================================================================
        addTo( root, extractApplicationId("MortgageNum") );

        //==========================================================================
        // IADDateNextMonthFirst #
        //==========================================================================
        addTo( root, extractIADDateNextMonthFirst() );
        //==========================================================================
        // FirstPaymentDateDateNextMonth #
        //==========================================================================
        addTo( root, extractFirstPaymentDateDateNextMonth() );
        //==========================================================================
        // MaturityDateAdjusted #
        //==========================================================================
        addTo( root, extractMaturityDateAdjusted() );

        //==========================================================================
        // Liability sections
        //==========================================================================
        addTo(root, extractLiabilityCloseSection());
        addTo(root, extractLiabilityOpenSection());
        addTo(root, extractEquityMtgLiability());
    }

    protected Node extractMaturityDateAdjusted()
    {
        String val = null;

        try
        {
            //Date lDate = deal.getMaturityDate();
            Date lDate = calculateIADDateNextMonthFirst();
            if(lDate == null){
              return getTag("maturityDateAdjusted", "");
            }
            Calendar wCal = Calendar.getInstance();
            wCal.setTime(lDate);
            wCal.add(Calendar.MONTH, deal.getActualPaymentTerm());
            val = formatDate( wCal.getTime() );
        }
        catch (Exception e){}

        return getTag("maturityDateAdjusted", val);
    }


    protected Date calculateIADDateNextMonthFirst(){
      try{
        //Date lDate = deal.getInterimInterestAdjustmentDate();
        Date lDate = deal.getEstimatedClosingDate();
        if(lDate == null){
          return null;
        }
        Calendar wCal = Calendar.getInstance();
        wCal.setTime(lDate);
        //=========================================================================
        // ticket 169 says that if the day falls on first of the month do nothing
        //=========================================================================
        if(wCal.get(Calendar.DAY_OF_MONTH ) == 1){
          return lDate;
        }else{
          wCal.add(Calendar.MONTH,1);
          wCal.set(Calendar.DAY_OF_MONTH,1);
          return wCal.getTime();
        }
      }catch(Exception exc){
        return null;
      }
    }

    protected Node extractIADDateNextMonthFirst()
    {
        String val = null;

        try
        {
            Date lDate = calculateIADDateNextMonthFirst();
            if(lDate == null){
              return getTag("IADDateNextMonthFirst", "");
            }
            val = formatDate( lDate );
        }
        catch (Exception e){}

        return getTag("IADDateNextMonthFirst", val);
    }

    protected Node extractFirstPaymentDateDateNextMonth()
    {
        String val = null;

        try
        {
            Date lDate = calculateIADDateNextMonthFirst();
            if(lDate == null){
              return getTag("FirstPaymentDateNextMonth", "");
            }

            Calendar wCal = Calendar.getInstance();
            wCal.setTime(lDate);
            //=========================================================================
            // ticket 169 says that if the day falls on first of the month do nothing
            //=========================================================================

            wCal.add(Calendar.MONTH,1);
            val = formatDate( wCal.getTime() );
        }
        catch (Exception e){}

        return getTag("FirstPaymentDateNextMonth", val);
    }


    protected Node extractSoleJointTenants()
    {
        String val = null;

        try
        {
            int count = deal.getBorrowers().size();

            val = (count == 1) ? "Sole" : "Joint Tenants";
        }
        catch (Exception e){}

        return getTag("SoleJointTenants", val);
    }

    protected Node extractLoanAmountPreFee()
    {
        String val = null;

        try
        {
            double amt = deal.getNetLoanAmount();

            List list = (List)deal.getDealFees();

            Iterator i = list.iterator();
            DealFee df;
            Fee fee;

            while (i.hasNext())
            {
                df = (DealFee)i.next();
                fee = df.getFee();

                if (fee.getFeeTypeId() == Mc.FEE_TYPE_APPLICATION_FEE)
                   amt -= df.getFeeAmount();
            }

            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("LoanAmountPreFee", val);
    }

    protected Node extractInterestRate()
    {
        String val = null;

        try
        {
            val = formatInterestRate(deal.getNetInterestRate());
        }
        catch (Exception e){}

        return getTag("InterestRate", val);
    }

    protected Node extractPAndIPayment()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getPandiPaymentAmount());
        }
        catch (Exception e){}

        return getTag("PandIPayment", val);
    }

    protected Node extractFees()
    {
        Node node = null;

        try
        {
            List list = (List)deal.getDealFees();

            Iterator i = list.iterator();
            DealFee df;
            Fee fee;
            String type, fpm, feeVerbStr;
            Node feeNode;
            Node refundableFeeNode;
            while (i.hasNext())
            {
                df = (DealFee)i.next();
                fee = df.getFee();
                // request that refundable fees should not show up in fees section
                if( fee.getRefundable() != null && fee.getRefundable().equalsIgnoreCase("y") ){
                  if (node == null)
                     node = doc.createElement("Fees");
                  refundableFeeNode = doc.createElement("refundableFee");
                  type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", fee.getFeeTypeId(), lang);
                  fpm = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeePaymentMethod", df.getFeePaymentMethodId(), lang);
                  //feeVerbStr = type + format.space() + "(" + fpm + ")";
                  feeVerbStr = type ;

                  addTo(refundableFeeNode, "FeeVerb", feeVerbStr);
                  addTo(refundableFeeNode, "FeeAmount", formatCurrency(df.getFeeAmount()));
                  // we do not want this type of fee being added to total amount
                  //totalFees += df.getFeeAmount();
                  addTo(node, refundableFeeNode);
                  continue;
                }

                if (!fee.getPayableIndicator() &&
                    df.getFeeAmount() != 0 &&
                    (fee.getFeeTypeId() != Mc.FEE_TYPE_APPLICATION_FEE &&
                     fee.getFeeTypeId() != Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED))
                {
                    if (node == null)
                       node = doc.createElement("Fees");

                    feeNode = doc.createElement("Fee");

                    //type = PicklistData.getDescription("FeeType", fee.getFeeTypeId());
                    //ALERT:BX:ZR Release2.1.docprep
                    type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", fee.getFeeTypeId(), lang);

                    //fpm = PicklistData.getDescription("FeePaymentMethod", df.getFeePaymentMethodId());
                    //ALERT:BX:ZR Release2.1.docprep
                    fpm = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeePaymentMethod", df.getFeePaymentMethodId(), lang);

                    feeVerbStr = type + format.space() + "(" + fpm + ")";
                    addTo(feeNode, "FeeVerb", feeVerbStr);

                    addTo(feeNode, "FeeAmount", formatCurrency(df.getFeeAmount()));

                    totalFees += df.getFeeAmount();

                    addTo(node, feeNode);
                }
            }
        }
        catch (Exception e){}

        return node;
    }

    /**  This method is used by the base class, but not the Xceed-specific
     *   extractor.  This returns null to prevent the base method from being
     *   called, which would mess up the total fees.
     */
    protected Node extractPremium()
    {
        return null;
    }

    /**  This method is used by the base class, but not the Xceed-specific
     *   extractor.  This returns null to prevent the base method from being
     *   called, which would mess up the total fees.
     */
    protected Node extractPST()
    {
        return null;
    }

    /**  The super class logic is fine, however, we need this value
     *   added to the totalFees variable.  Therefore, we just add the
     *   <code>deal.getInterimInterestAmount()</code> to
     *   <code>totalFees</code> and return the node output from the
     *   super class' method.
     */
    protected Node extractIADAmount()
    {
        totalFees += deal.getInterimInterestAmount();

        return super.extractIADAmount();
    }

    protected Node extractCurrentYear()
    {
        String val = null;

        try
        {
            Calendar cal = Calendar.getInstance();
            val = Integer.toString(cal.get(Calendar.YEAR));
        }
        catch (Exception e) {}

        return getTag("CurrentYear", val);
    }

    protected Node extractPerDiem()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getPerdiemInterestAmount());
        }
        catch (Exception e) {}

        return getTag("PerDiem", val);
    }

    protected Node extractPrimaryPropertyProvince()
    {
        String val = null;

        try
        {
            Property p = getPrimaryProperty();
            Province prov = new Province(srk, p.getProvinceId());

            val = prov.getProvinceAbbreviation();
        }
        catch (Exception e) {}

        return getTag("PrimaryPropertyProvince", val);
    }

    protected Node extractProvincialClause()
    {
        String carriageReturn = "�";
        StringBuffer val = null;

        try
        {
            Property prim = getPrimaryProperty();

            ConditionHandler ch = new ConditionHandler(srk);
            val = new StringBuffer();

            switch (prim.getProvinceId())
            {
                case Mc.PROVINCE_ALBERTA:
                    val.append(ch.getVariableVerbiage(deal, Dc.ALBERTA_CONVENTIONAL_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_BRITISH_COLUMBIA:
                    val.append(ch.getVariableVerbiage(deal, Dc.BC_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.BC_LAND_TITLE_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.BC_FORM_DATE_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_MANITOBA:
                    val.append(ch.getVariableVerbiage(deal, Dc.MANITOBA_HEADER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.MANITOBA_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.MANITOBA_FORM_DATE_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_NEW_BRUNSWICK:
                    val.append(ch.getVariableVerbiage(deal, Dc.NB_CONVENTIONAL_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NB_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NB_DELETIONS_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NB_OFFER_LETTER_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_NEWFOUNDLAND:
                    val.append(ch.getVariableVerbiage(deal, Dc.NF_CONVENTIONAL_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NF_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NF_DELETIONS_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NF_OFFER_LETTER_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_NOVA_SCOTIA:
                    val.append(ch.getVariableVerbiage(deal, Dc.NOVA_SCOTIA_CONVENTIONAL_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NOVA_SCOTIA_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NOVA_SCOTIA_DELETIONS_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.NOVA_SCOTIA_OFFER_LETTER_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_ONTARIO:
                    val.append(ch.getVariableVerbiage(deal, Dc.ONTARIO_HEADER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.ONTARIO_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.ONTARIO_EREGISTRATION_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.ONTARIO_CONVENTIONAL_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.ONTARIO_LRRA_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.ONTARIO_SPOUSAL_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.ONTARIO_FORM_DATE_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_PRINCE_EDWARD_ISLAND:
                    val.append(ch.getVariableVerbiage(deal, Dc.PEI_CONVENTIONAL_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.PEI_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.PEI_DELETIONS_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.PEI_OFFER_LETTER_CLAUSE, lang));
                    break;

                case Mc.PROVINCE_SASKATCHEWAN:
                    val.append(ch.getVariableVerbiage(deal, Dc.SK_LENDER_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.SK_DELETIONS_CLAUSE, lang))
                       .append(carriageReturn)
                       .append(ch.getVariableVerbiage(deal, Dc.SK_OFFER_LETTER_CLAUSE, lang));
                    break;
            }
        }
        catch(Exception exc){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("ProvincialClause"), val.toString()));
    }

    protected Node extractLenderProfileId()
    {
      return getTag("LenderProfileId", "" + deal.getLenderProfileId() );
    }


    /**
     * Since this class doesn't inherit from XceedExtractor, this duplicate code is needed.
     */
    protected Node extractLenderFund()
    {
      if(deal.getLenderProfileId() == 0){
        return null;
      }
      String lTmpFund = BXResources.getDocPrepIngMsg("XCEED_LENDER_FUNDER", lang);
      if(lTmpFund == null){
        return null;
      }else if(lTmpFund.trim().equals("")){
        return null;
      }else if(lTmpFund.trim().equals("XCEED_LENDER_FUNDER")){
        return null;
      }
      return getTag("LenderNameFund",lTmpFund);
    }

    /**
     * This tag is needed because LienPosition tag has been used for comparisons.
     * If we put in that tag french terms all comparisons will blow up.
     */
    /*
    protected Node extractLienPositionTerm()
    {
        String val = null;

        try
        {
            //val = (deal.getLienPositionId() == Mc.LIEN_POSITION_SECOND) ? "SECOND" : "FIRST";
            val = (deal.getLienPositionId() == Mc.LIEN_POSITION_SECOND) ?
                   BXResources.getPickListDescription() .getDocPrepIngMsg("LIEN_POSITION_FIRST",lang):
                   BXResources.getDocPrepIngMsg("LIEN_POSITION_SECOND",lang);
        }
        catch (Exception e) {}

        return getTag("LienPositionTerm", val);
    }
    */
    protected Node extractLienPosition()
    {
        String val = null;

        try
        {
            val = (deal.getLienPositionId() == Mc.LIEN_POSITION_SECOND) ? "SECOND" : "FIRST";
            //val = (deal.getLienPositionId() == Mc.LIEN_POSITION_SECOND) ?
            //       BXResources.getDocPrepIngMsg("LIEN_POSITION_FIRST",lang):
            //       BXResources.getDocPrepIngMsg("LIEN_POSITION_SECOND",lang);
        }
        catch (Exception e) {}

        return getTag("LienPosition", val);
    }

    /**
     * We just want a flag in the XSL that indicates whether this deal is
     * a refi or not.
     */
    protected Node extractRefiFlag()
    {
        String val = null;

        try
        {
            if (deal.getDealPurposeId() != Mc.DEAL_PURPOSE_PURCHASE &&
                deal.getDealPurposeId() != Mc.DEAL_PURPOSE_CONSTRUCTION &&
                deal.getDealPurposeId() != Mc.DEAL_PURPOSE_PORTABLE &&
                deal.getDealPurposeId() != Mc.DEAL_PURPOSE_COVENANT_CHANGE &&
                deal.getDealPurposeId() != Mc.DEAL_PURPOSE_PARTIAL_DISCHARGE &&
                deal.getDealPurposeId() != Mc.DEAL_PURPOSE_REPLACEMENT)
                val = "Y";
        }
        catch (Exception e) {}

        return getTag("Refi", val);
    }

    protected Node extractUndertakingFlag()
    {
        String val = null;

        try
        {
            Liability l = new Liability(srk,null);

            List l1 = (List)l.findByDealAndPayoffType( (DealPK)deal.getPk(),
               Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS ) ;

            List l2 = (List)l.findByDealAndPayoffType( (DealPK)deal.getPk(),
               Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE ) ;

            if (l1.size() > 0 || l2.size() > 0)
               val = "Y";
        }
        catch (Exception e) {}

        return getTag("UndertakingFlag", val);
    }

    protected Node extractAdminFee()
    {
        String val = null;

        try
        {
            List fees = (List)deal.getDealFees();
            double amt = 0;

            Iterator i = fees.iterator();

            while (i.hasNext())
            {
                DealFee df = (DealFee)i.next();

                if (df.getFee().getFeeTypeId() == Mc.FEE_TYPE_APPLICATION_FEE ||
                    df.getFee().getFeeTypeId() == Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED)
                   amt += df.getFeeAmount();
            }

            totalFees += amt;

            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("AdminFee", val);
    }

    protected Node extractLiabilityCloseSection()
    {
        return extractLiabilitySection(Mc.LIABILITY_PAYOFFTYPE_INTERNAL_FUNDS_CLOSE, // 6
               "LiabilitiesClose");
    }

    protected Node extractLiabilityOpenSection()
    {
        return extractLiabilitySection(Mc.LIABILITY_PAYOFFTYPE_INTERNAL_FUNDS, // 3
               "LiabilitiesOpen");
    }

    protected Node extractEquityMtgLiability(){
      Node liabilityNode = null;
      try{
        Collection lColl = deal.getBorrowers();
        Iterator it = lColl.iterator();
        while(it.hasNext()){
          Borrower b = (Borrower) it.next();
          if( !b.isPrimaryBorrower() ){
            continue;
          }
          Collection liabColl = b.getLiabilities();
          Iterator liabIt = liabColl.iterator();
          while(liabIt.hasNext()){
            Liability l = (Liability)liabIt.next();
            if(l.getLiabilityTypeId() == Mc.LIABILITY_TYPE_EQUITY_MORTGAGE){
              // we got an assurance that there will be one and only one
              // liability of this type for second mortgage.
              // It should be taken care of by business rule layer.
              // It is why we return after finding the first one of this
              // type of liability.
              liabilityNode = doc.createElement("equityMtgLiability");
              addTo(liabilityNode, getTag("liabilityAmount", formatCurrency( l.getLiabilityAmount())));
              addTo(liabilityNode, getTag("liabilityMonthlyPayment", formatCurrency( l.getLiabilityMonthlyPayment())));
              return liabilityNode;
            }
          }
        }
      }catch(Exception exc){
          return null;
      }
      return null;
    }

    protected Node extractLiabilitySection(int payoffType, String tagName)
    {
        Node elemNode = null;

        try
        {
            Liability l = new Liability(srk,null);
            Node lineNode = null;

            List out = (List)l.findByDealAndPayoffType( (DealPK)deal.getPk(),
               payoffType) ;

            Iterator i = out.iterator();
            String liabDesc, liabAmt;

            while (i.hasNext())
            {
                l = (Liability)i.next();

                if (l.getLiabilityPayOffTypeId() != Mc.LIABILITY_TYPE_MORTGAGE)
                {
                    if (elemNode == null)
                       elemNode = doc.createElement(tagName);

                    lineNode = doc.createElement("Liability");

                    liabDesc = (!isEmpty(l.getLiabilityDescription())) ?
                                l.getLiabilityDescription() : "";

                    //ALERT:BX:ZR Release2.1.docprep
                    //liabDesc += "(" + PicklistData.getDescription("LiabilityPayoffType", l.getLiabilityPayOffTypeId()) + ")";
                    liabDesc += "(" + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeePaymentMethod", l.getLiabilityPayOffTypeId(), lang) + ")";

                    liabAmt = formatCurrency(l.getLiabilityAmount());

                    addTo(lineNode, getTag("Description", liabDesc));
                    addTo(lineNode, getTag("Amount", liabAmt));
                    addTo(elemNode, lineNode);
                }
            }

        }
        catch (Exception e) {}

        return elemNode;
    }
}