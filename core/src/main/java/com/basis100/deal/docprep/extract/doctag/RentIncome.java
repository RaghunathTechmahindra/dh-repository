package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class RentIncome extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();

        try
        {
            if (de.getClassId() == ClassId.BORROWER)
            {
                Borrower borrower = (Borrower)de;

                List incs = (List)borrower.getIncomes();

                Iterator incit = incs.iterator();
                Income income = null;
                double amt = 0.0;

                while(incit.hasNext())
                {
                    income = (Income)incit.next();

                    if ((income.getIncomeTypeId() == Mc.INCOME_RENTAL_ADD ||
                         income.getIncomeTypeId() == Mc.INCOME_RENTAL_SUBTRACT) &&
                         income.getMonthlyIncomeAmount() > 0)
                    {
                        amt += income.getMonthlyIncomeAmount();
                    }
                }
                fml.addCurrency(amt);
            }
        }
        catch (Exception e)
        {
            srk.getSysLogger().error(e.getMessage());
            srk.getSysLogger().error(e);
            throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
        }

        return fml;
    }
}
