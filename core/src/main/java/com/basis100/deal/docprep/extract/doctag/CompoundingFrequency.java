package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

public class CompoundingFrequency extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String outVal = "";
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      Property prop = new Property(srk,null);
      try{
        prop = prop.findByPrimaryProperty(deal
            .getDealId(),deal.getCopyId(), deal.getInstitutionProfileId() );
      }catch(RemoteException rexc){
        return fml;
      }catch(FinderException fexc){
        return fml;
      }catch(Exception exc1){
        return fml;
      }
      int dwellingType = prop.getDwellingTypeId();
      if(dwellingType == Mc.DWELLING_TYPE_MOBILE){
        outVal = "monthly";
      }else{
        outVal = "semi-annually";
      }
      fml.addValue(outVal,fml.STRING);
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}