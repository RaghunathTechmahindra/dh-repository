package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
/**
 *  Deal.PricingProfileID to  MTGPROD.privilegePaymentID  to  privilegePayment.PPDescription
 */
public class MtgProdInterestType extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      String desc = null;

      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        //======================================================================================
        // PROGRAMMER'S NOTE: Deal table - column pricingprofileid does not contain
        // pricing profile id, it contains pricing rate inventory id. So the path goes:
        // take pricingprofileid from deal, based on that key find the pricingrateinventoryid
        // in the pricingrateinventory table. From that record pick pricingprofileid value and
        // that is the key to search in the pricingprofile table.
        // Method findByPricingRateInventory in the PricingProfile class does exactly that.
        //=====================================================================================
        //PricingProfile pp = new PricingProfile(srk,deal.getPricingProfileId());
        PricingProfile pp = new PricingProfile(srk);
        pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

        MtgProd pr = new MtgProd(srk,null);

        List all = (List)pr.findByPricingProfile(pp.getPricingProfileId());

        if(all.size() > 0)
        {
          pr = (MtgProd)all.get(0);
          int itypeid = pr.getInterestTypeId();
          //desc = PicklistData.getDescription("InterestType", itypeid);
          //ALERT:BX:ZR Release2.1.docprep
          desc = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "InterestType", itypeid, lang);
        }

        if(desc != null)
         fml.addValue(desc,fml.STRING);
      }

    }
    catch (Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
