package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class EarlyRenewal extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());
    FMLQueryResult fml = new FMLQueryResult();
    String val = null;
    
    Deal deal = (Deal)de;

    try
    {
      
      ConditionHandler ch = new ConditionHandler(srk);

      String prodName = "";
      int id = deal.getMtgProdId();
      MtgProd mp = new MtgProd(srk,null);
      mp = mp.findByPrimaryKey(new MtgProdPK(id));

      if(mp != null) prodName = mp.getMPShortName();

      if(prodName.equalsIgnoreCase("VIP"))
      {
        val = ch.getVariableVerbiage(deal,Dc.NO_EARLY_RENEWAL,lang);
        fml.addValue(val,fml.STRING);
        return fml;
      }

      try
      {
        LenderProfile lp = deal.getLenderProfile();
        String lpsn = lp.getLPShortName();

        if(lpsn != null && lpsn.equals("ING"));
        {
          val = ch.getVariableVerbiage(deal,Dc.EARLY_RENEWAL_ING,lang);
          fml.addValue(val,fml.STRING);
          return fml;
        }
      }
      catch(FinderException fe)
      {
        ; //fall through
      }

      val = ch.getVariableVerbiage(deal,Dc.EARLY_RENEWAL,lang);
      fml.addValue(val,fml.STRING);
      return fml;

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }
}
