package com.basis100.deal.docprep.extract.data;
/**
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.log.*;
import com.basis100.xml.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.util.collections.*;
import com.basis100.picklist.*;
import java.util.*;
import java.text.*;
import MosSystem.*;

public class TestExtractor extends XSLExtractor
{
  public String extractXMLData(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk) throws Exception
  {
    String retVal ;
    Deal deal = (Deal)de;


    Document doc = buildXMLDataDoc(deal, lang, format, srk);
    XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();
    retVal = xmlwr.printString(doc);

    return retVal;
  }

  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    //buildXMLDataDoc();
  }
  private Document buildXMLDataDoc(Deal pDeal, int lang, DocumentFormat format, SessionResourceKit srk) throws Exception
  {
    Document retDoc = XmlToolKit.getInstance().newDocument();

    Element root = retDoc.createElement("Document");
    root.setAttribute("type","1");
    root.setAttribute("lang","1");
    retDoc.appendChild(root);

    return retDoc;
  }

}
