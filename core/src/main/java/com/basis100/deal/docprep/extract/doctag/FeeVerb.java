package com.basis100.deal.docprep.extract.doctag;

import MosSystem.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.entity.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;

//Alert to do after spec clarification

public class FeeVerb extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();
    try
    {

      if(clsid == ClassId.FEE){
        Fee oneFee = (Fee) de;
        fml.addString(oneFee.getFeeDescription());
        return fml;
      }
      if(clsid == ClassId.DEALFEE)
      {
        DealFee dealFee = (DealFee)de;
        Fee fee = dealFee.getFee();
        int feeType = fee.getFeeTypeId();
        int feestat = dealFee.getFeeStatusId();
        int fpmid = dealFee.getFeePaymentMethodId();

        Deal deal = new Deal(srk,null,dealFee.getDealId(),dealFee.getCopyId());
        int payor = deal.getMIPayorId();
        String uf = deal.getMIUpfront();

        if(feestat == Mc.FEE_STATUS_RECIEVED || feestat == Mc.FEE_STATUS_REQUIRED)
        {
          ComponentResult res = new ComponentResult();
          //String type = PicklistData.getDescription("FeeType",feeType);
          //ALERT:BX:ZR Release2.1.docprep
          String type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", feeType, lang);

          //String fpm   = PicklistData.getDescription("FeePaymentMethod",fpmid);
          //ALERT:BX:ZR Release2.1.docprep
          String fpm = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeePaymentMethod", fpmid, lang);

          res.addResult(type, FMLQueryResult.TERM);
          res.addResult("(", FMLQueryResult.STRING);
          res.addResult(fpm, FMLQueryResult.TERM);
          res.addResult(") ",FMLQueryResult.STRING);

          fml.addComponentResult(res);
          return fml;
        }

        if(payor == Mc.MI_PAYOR_BORROWER && deal.getMIPremiumAmount() > 0)
        {
            //// TODO  this must go throug BX resources for freedom version
           String payStr = "Take from First Advance";

           if(uf != null && uf.equalsIgnoreCase("Y") )
           {
              payStr = "Cheque";
           }


          ConditionHandler ch = new ConditionHandler(srk);
          String vv = ch.getVariableVerbiage(deal,Dc.PST_VERB,lang);

          if(vv != null)
          {
            ComponentResult res = new ComponentResult();

            res.addResult(vv, FMLQueryResult.STRING);
            res.addResult(" ( ", FMLQueryResult.STRING);
            res.addResult(payStr, FMLQueryResult.TERM);
            res.addResult(" ) ",FMLQueryResult.STRING);

            fml.addComponentResult(res);
            return fml;
          }
       }
      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

   return fml;
  }
}
