package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class DownPaymentAvailable extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk) throws com.basis100.deal.docprep.extract.ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();

        double amt = 0.0;

        try
        {
            Deal deal = (Deal) de;

            List prps = (List)deal.getDownPaymentSources();
            Iterator it = prps.iterator();

            while(it.hasNext())
            {
                DownPaymentSource dps = (DownPaymentSource)it.next();

                amt += dps.getAmount();
            }
        }
        catch (Exception e)
        {
            if (e instanceof FinderException || e instanceof NullPointerException)
               return fml;

            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);
            throw new ExtractException(msg);
        }

        fml.addCurrency(amt);

        return fml;
    }
}