package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import com.basis100.picklist.*;
import MosSystem.Mc;
import com.basis100.entity.*;
import com.basis100.deal.calc.*;

/**
 * Title:        Property Address All
 * Description:  Extracts and formats a complete property address XML tag
 * Copyright:    Copyright (c) 2002
 * Company:      Basis100, Inc
 * @author       John Schisler
 * @version 1.0
 */
public class PropertyAddressAll extends DocumentTagExtractor
{
    /*      Specs require:

            [", " or ", situ�e au " depending on the preferred language] +
            Ifexist(AddressStreetNumber) + <space> +
            Ifexist(AddressStreetName) + <space> +
            Ifexist(AddressStreetType) + <space> +
            Ifexist(AddressStreetDirection) + format.carriageReturn() +
            Ifexist(AddressLine2) + format.carriageReturn() +
            Address.City + format.carriageReturn() +
            Address.ProvinceID(ProvinceName) + "," + <space> +
            Address.PostalFSA + <space> + Address.PostalLDU

            *** We do null checks on ALL fields before adding to string.
     */
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk) throws com.basis100.deal.docprep.extract.ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();
        Property prop;
        String val;

        try
        {
            if(de.getClassId() == ClassId.DEAL){
              Deal deal = (Deal)de;
              prop = new Property(srk,null);
              prop = prop.findByPrimaryProperty(deal.getDealId(),
                             deal.getCopyId(), deal.getInstitutionProfileId() );
              val = this.doTagExtraction(prop,format,lang, srk);
              fml.addValue(val, fml.STRING);
            }
            else if (de.getClassId() == ClassId.PROPERTY)
            {
                prop = (Property)de;
                val = this.doTagExtraction(prop,format,lang, srk);
                fml.addValue(val, fml.STRING);

                /////////////////////////////
                //  DEBUG
                //System.out.println(val);
            }
        }
        catch(FinderException fe){
          return fml;
        }
        catch(NullPointerException npe)
        {
            return fml;
        }
        catch (Exception e)
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);
            throw new ExtractException(msg);
        }
        return fml;
    }

    /** Converts all new line characters to the carriage return
     *  character specified in <code>format</code>.
     *
     * @param pStr <code>String</code> to convert.
     * @param format contains the document-specific carriage return character to use.
     *
     * @return the formatted <code>String</code>.
     */
    private String convertCRtoRTF(String pStr, DocumentFormat format)
    {
        if(! format.getType().startsWith("rtf") )
             return pStr;

        int ind;
        StringBuffer retBuf = new StringBuffer();

        while( (ind = pStr.indexOf("�")) != -1 )
        {
            retBuf.append(pStr.substring(0,ind));
            retBuf.append( format.carriageReturn() );
            pStr = pStr.substring(ind + 1);
        }

        retBuf.append(pStr);
        return retBuf.toString();
    }

    private String doTagExtraction(Property p, DocumentFormat format, int pLang, SessionResourceKit srk){
      StringBuffer propAdd = new StringBuffer("");
      if (p.getPropertyStreetNumber() != null)
         propAdd.append(format.space()).append(p.getPropertyStreetNumber());

      if (p.getPropertyStreetName() != null)
         propAdd.append(format.space()).append(p.getPropertyStreetName());


      //ALERT:BX:ZR Release2.1.docprep
      //propAdd.append(format.space()).append(PicklistData.getDescription("StreetType", p.getStreetTypeId()));
      if (p.getStreetTypeId() != 0)
        propAdd.append(format.space()).append(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",  p.getStreetTypeId(), pLang));


       //ALERT:BX:ZR Release2.1.docprep
       //propAdd.append(format.space()).append(PicklistData.getDescription("StreetDirection", p.getStreetDirectionId()));
      if (p.getStreetDirectionId() != 0)
         propAdd.append(format.space()).append(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection",  p.getStreetDirectionId(), pLang));

      if (p.getPropertyAddressLine2() != null)
         propAdd.append(format.carriageReturn()).append(p.getPropertyAddressLine2());

      if (p.getPropertyCity() != null)
         propAdd.append(format.carriageReturn()).append(p.getPropertyCity());

      //ALERT:BX:ZR Release2.1.docprep
      //propAdd.append(format.carriageReturn()).append(PicklistData.getDescription("Province",p.getProvinceId()));
      propAdd.append(format.carriageReturn()).append(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province", p.getProvinceId(), pLang));

      if (p.getPropertyPostalFSA() != null)
         propAdd.append(",").append(format.space()).append(p.getPropertyPostalFSA());

      if (p.getPropertyPostalLDU() != null)
         propAdd.append(format.space()).append(p.getPropertyPostalLDU());

      String val = (format.getType().startsWith("rtf")) ?
                      convertCRtoRTF(propAdd.toString(), format) :
                      propAdd.toString();

      return val;

    }
}