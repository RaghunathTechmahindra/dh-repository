package com.basis100.deal.docprep.extract;

/**
 * 30/Jun/2006 DVG #DG452 #3632  All Clients - Format correction for tag "PropertyAddressAll"
 */

import com.basis100.deal.xml.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;


public class DocumentFormat
{
  private DocumentProfile profile;
  private String type;
  private String version;

  private String[] supported = {"xml", "ascii", "rtf", "pdf", "csv"}; // later add "html"

  private String space = " ";
  private String carriageReturn = "\n\r";
  private String notApplicable="N/A";
  //add any format control items


  public DocumentFormat(DocumentProfile profile) throws UnsupportedFormatException
  {
    this(profile.getDocumentFormat());
    this.profile = profile;
  }

  public DocumentFormat(String formatString) throws UnsupportedFormatException
  {
    setType(formatString);
    setVersion(formatString);

    if(notSupported(type))
     throw new UnsupportedFormatException(version);

    setFormatStrings(type);
  }

  public DocumentProfile getDocumentProfile()
  {
    return profile;
  }

  public void setVersion(String in)
  {
    this.version = in;
  }

  public void setType(String in)
  {
    if(in != null && in.length() > 2)
     this.type = in.substring(0,3);
  }

  public String getVersion()
  {
    return version;
  }

  public String getType()
  {
     return type;
  }

  public boolean notSupported(String type)
  {
    for(int i = 0; i < supported.length; i++)
    {
      if(supported[i].startsWith(type))
       return false;
    }

    return true;
  }

  private void setFormatStrings(String type)
  {
    if(type.startsWith("rtf"))
    {
      space = " ";
      carriageReturn = "\\par ";
    }
    //#DG452 asc format, which is used for innertags on the screen, uses default settings
    // from InnerTagCache ->      DocumentFormat format = new DocumentFormat("ascii");
    //else if(type.startsWith("asc") || type.startsWith("xml"))
    else if(type.startsWith("xml"))
    {
      space = " ";
      carriageReturn = String.valueOf((char)Character.LINE_SEPARATOR);
    }
    else if(type.startsWith("html"))
    {
      space = "&nbsp;";
      carriageReturn = "<br>";
    }
  }

  public String carriageReturn()
  {
    return carriageReturn;
  }

  public String space()
  {
    return space;
  }
  public void setNotApplicable(String in)
  {
    this.notApplicable = in;
  }

  public String getNotApplicable()
  {
    return notApplicable;
  }
}
