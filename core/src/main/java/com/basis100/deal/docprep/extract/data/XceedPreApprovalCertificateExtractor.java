package com.basis100.deal.docprep.extract.data;

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.xml.*;
import com.basis100.deal.conditions.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.picklist.*;
import java.util.*;
import MosSystem.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.*;

/**
 * <PRE>
 * Creates an XML document for use as a Pre-Approval Certificate
 * Copyright (c) 2002 Basis100, Inc
 * </PRE>
 * @author       John Schisler
 * @version 1.0
 *
 */

public class XceedPreApprovalCertificateExtractor extends GENXPreApprovalCertificateExtractor
{
    /**
     * Builds the XML doc from various records in the database.
     *
     */
    protected void buildXMLDataDoc() throws Exception
    {
        doc = XmlToolKit.getInstance().newDocument();
        Node elemNode;
        Element root = doc.createElement("Document");
        root.setAttribute("type","1");
        root.setAttribute("lang","1");
        doc.appendChild(root);

        //==========================================================================
        // Language stuff
        //==========================================================================
        addTo(root, getLanguageTag());

        //==========================================================================
        // Lender Name
        //==========================================================================
        addTo(root, extractLenderName());

        //==========================================================================
        // Branch Address
        //==========================================================================
        elemNode = extractBranchAddressSection();

        //  For both the phone and fax numbers, we want to include the toll free
        //  version, if available, otherwise we use the non-toll free version.
        if (elemNode != null)
        {
            //==========================================================================
            // Branch Phone Number
            //==========================================================================
            Node tempNode = extractBranchPhone(null, true, "Phone");

            if (tempNode == null)
               tempNode = extractBranchPhone(null, false, "Phone");

            addTo(elemNode, tempNode);

            //==========================================================================
            // Branch Fax Number
            //==========================================================================
            tempNode = extractBranchFax(null, true, "Fax");

            if (tempNode == null)
               tempNode = extractBranchFax(null, false, "Fax");

            addTo(elemNode, tempNode);

            root.appendChild(elemNode);
        }

        //==========================================================================
        // Borrower Names
        //==========================================================================
        addTo(root, extractBorrowerNames());

        //==========================================================================
        // Client address section
        //==========================================================================
        addTo(root, extractClientAddressSection());

        //==========================================================================
        // Underwriter
        //==========================================================================
        addTo(root, extractUnderwriter());

        //==========================================================================
        // Current Date
        //==========================================================================
        addTo(root, extractCurrentDate());

        //==========================================================================
        // Branch Current Time
        //==========================================================================
        addTo(root, extractBranchCurrentTime());

        //==========================================================================
        // Broker Firm Name
        //==========================================================================
        addTo(root, extractBrokerFirmName());

        //==========================================================================
        // Broker Agent Name
        //==========================================================================
        addTo(root, extractBrokerAgentName());

        //==========================================================================
        // Broker Address Section
        //==========================================================================
        addTo(root, extractBrokerAddressSection());

        //==========================================================================
        // Broker Source Number
        //==========================================================================
        addTo(root, extractBrokerSourceNum());

        //==========================================================================
        // Deal Num
        //==========================================================================
        addTo(root, extractDealNum());

        //==========================================================================
        // App Sections
        //==========================================================================
        addTo(root, extractPreAppOpen());
        addTo(root, extractPreAppBold());
        addTo(root, extractPreAppProduct());
        addTo(root, extractPreAppRate());
        addTo(root, extractPreAppAttach());
        addTo(root, extractClosingText());

        //==========================================================================
        // Care of label
        //==========================================================================
        addTo(root, extractCareOf());

        //==========================================================================
        // Broker Email
        //==========================================================================
        addTo(root, extractBrokerEmail());
    }

    protected Node extractPreAppOpen()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.PREAPP_OPEN, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("PreAppOpen"), val));
    }

    protected Node extractPreAppBold()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.PREAPP_BOLD, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("PreAppBold"), val));
    }

    protected Node extractPreAppProduct()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.PREAPP_PRODUCT, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("PreAppProduct"), val));
    }

    protected Node extractPreAppRate()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.PREAPP_RATE, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("PreAppRate"), val));
    }

    protected Node extractPreAppAttach()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.PREAPP_ATTACH, lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("PreAppAttach"), val));
    }

    protected Node extractBrokerAddressSection()
    {
        if (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS)
        {
            Node elemNode = null;

            try
            {
                UserProfile up = new UserProfile(srk);
                up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                               deal.getInstitutionProfileId()));
                Contact c = up.getContact();
                String str;

                //  Phone
                str = StringUtil.getAsFormattedPhoneNumber(c.getContactPhoneNumber(), null);

                if (!isEmpty(str))
                {
                    if (!isEmpty(c.getContactPhoneNumberExtension()))
                    {
                       str += " Ext " + c.getContactPhoneNumberExtension();
                    }

                    if (elemNode == null)
                       elemNode = doc.createElement("BrokerAddress");

                    addTo(elemNode, "Phone", str);
                }

                //  Fax
                str = StringUtil.getAsFormattedPhoneNumber(c.getContactFaxNumber(), null);

                if (!isEmpty(str))
                {
                    if (elemNode == null)
                       elemNode = doc.createElement("BrokerAddress");

                    addTo(elemNode, "Fax", str);
                }
            }
            catch (Exception e) {}

            return elemNode;
        }
        else
            return super.extractBrokerAddressSection();
    }

    /**
     * This redundant code is required since this class doesn't inherit from
     * XceedExtractor, where a duplicate version of this method already exists.
     */
    protected Node extractCareOf()
    {
        String val = null;

        try
        {
            val = (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS) ?
                  "FROM:" : "C/O:";
        }
        catch (Exception e){}

        return getTag("CareOf", val);
    }

    protected Node extractBrokerFirmName()
    {
        if (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS)
        {
            String val = null;

            try
            {
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                          deal.getInstitutionProfileId()));

                Contact c = up.getContact();

                val = "";

                if (c.getContactFirstName() != null)
                   val += c.getContactFirstName();

                if (c.getContactLastName() != null)
                   val += " " + c.getContactLastName();
            }
            catch (Exception e){}

            return getTag("BrokerFirmName", val);
        }
        else
            return super.extractBrokerFirmName();
    }

    protected Node extractBrokerAgentName()
    {
        if (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS)
        {
            String val = null;

            try
            {
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                          deal.getInstitutionProfileId()));

                Contact c = up.getContact();

                val = c.getContactJobTitle();
            }
            catch (Exception e){}

            return getTag("BrokerAgentName", val);
        }
        else
            return super.extractBrokerAgentName();
    }
}