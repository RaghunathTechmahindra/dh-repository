package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;


public class AddressListShort extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    try
    {

      if(clsid == ClassId.DEAL)
      {
         Deal deal = (Deal)de;
         Collection props = deal.getProperties();
         int count = 0;
         Iterator it = props.iterator();

         ComponentResult res = new ComponentResult();

         Property pr = null;

          while(it.hasNext())
          {
            if(count > 0)
            {
              res.addResult(",and ", FMLQueryResult.TERM);
            }
            count++;
            pr = (Property)it.next();
            FMLQueryResult fml2 = extract(pr, lang, format, srk);

            String str = fml2.getFirstStringValue(lang);

            res.addResult(str,fml2.STRING);
          }

        fml.addComponentResult(res);
        return fml;

      }
      else if(clsid == ClassId.PROPERTY)
      {
        Property pr = (Property)de;

        String streetNum = pr.getPropertyStreetNumber();
        String streetName = pr.getPropertyStreetName();
        //String streetType = PicklistData.getDescription("StreetType",pr.getStreetTypeId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetType = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType", pr.getStreetTypeId(), lang);

        //String streetDir  = PicklistData.getDescription("StreetDirection",pr.getStreetDirectionId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetDir = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection", pr.getStreetDirectionId(), lang);

        String line2  = pr.getPropertyAddressLine2();

        String val = "";

        if(streetNum != null && streetNum.length() > 0)
          val += streetNum + format.space();
        if(streetName != null && streetName.length() > 0)
          val += streetName + format.space();
        if(streetType != null && streetType.length() > 0)
          val += streetType + format.space();
        if(streetDir != null && streetDir.length() > 0)
          val += streetDir + format.space();
        if(line2 != null && line2.length() > 0)
          val += line2 + format.space();

        fml.addValue(val, fml.STRING);
        return fml;
      }


    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

   return fml;

 }
}
