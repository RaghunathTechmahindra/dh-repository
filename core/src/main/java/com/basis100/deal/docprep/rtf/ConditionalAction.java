package com.basis100.deal.docprep.rtf;
/**
 * 
 * @author MCM Impl Team Changes
 * @version 1.1 Added new action type for hiding the sections
 *
 */

public class ConditionalAction {
  // event types
  public static final int ON_OCCURENCE = 1;
  public static final int ON_SWITCH = 2;
  public static final int ON_UNKNOWN = 0;
  // action types
  public static final int AT_REPLACE_WITH_VAR = 1;
  public static final int AT_REPLACE_WITH_CONST = 2;
  public static final int AT_TURN_SWITCH_ON = 3;
  public static final int AT_TURN_SWITCH_OFF = 4;
  /**********MCM Impl Team XS_16.5 Changes Starts************/
  public static final int AT_HIDESECTION = 5;
  /**********MCM Impl Team XS_16.5 Changes Ends************/
  public static final int AT_UNKNOWN = 0;
  // comparison constants
  public static final String CE_EQUALS = ".eq.";
  public static final String CE_GREATER_OR_EQUAL = ".ge.";
  public static final String CE_GREATER_THAN = ".gt.";
  public static final String CE_LOWER_OR_EQUAL = ".le.";
  public static final String CE_LOWER_THAN = ".lt.";
  public static final String CE_NOT_EQUAL = ".ne.";
  public static final String CE_UNKNOWN = ".unknown.";

  private String varName;
  private String monitoredVarName;
  private String actionParamOne;
  private String comparisonExpression;
  private String switchName;
  private boolean switchStatus;
  private int eventType;
  private int actionType;
  private int occurenceNumber;
  private String actionName;

  public ConditionalAction() {
    varName = null;
    monitoredVarName = null;
    actionParamOne = null;
    switchName = null;
    switchStatus = true;
    actionName = null;
    eventType = ON_UNKNOWN;
    actionType = AT_UNKNOWN;
    comparisonExpression = CE_UNKNOWN;
  }

  public void setMonitoredVarName(String nameToSet){
    monitoredVarName = new String(nameToSet);
  }

  public String getMonitoredVarName(){
    return monitoredVarName;
  }

  public void setSwitchStatus(boolean status){
    switchStatus = status;
  }

  public boolean getSwitchStatus(){
    return switchStatus;
  }

  public String getActionParamOne(){
    return actionParamOne;
  }

  public void setActionParamOne(String parToSet){
    actionParamOne = new String(parToSet);
  }

  public void setActionName(String nameToSet){
    nameToSet = nameToSet.toLowerCase().trim();
    if(nameToSet.equals("replacewithvar")){
      actionType = AT_REPLACE_WITH_VAR;
    }else if(nameToSet.equals("replacewithconst")){
      actionType = AT_REPLACE_WITH_CONST;
    }else if(nameToSet.equals("hidesection")){
      actionType = AT_HIDESECTION;
    }else if(nameToSet.equals("turnswitchon")){
      actionType = AT_TURN_SWITCH_ON;
    }else if(nameToSet.equals("turnswitchoff")){
      actionType = AT_TURN_SWITCH_OFF;
    } else {
      actionType = AT_UNKNOWN;
    }
  }

  public void setOccurenceNumber(int numToSet){
    occurenceNumber = numToSet;
  }

  public int getOccurenceNumber(){
    return occurenceNumber;
  }

  public void setComparisonExpression(String exprToSet){
    comparisonExpression = new String(exprToSet.trim());
  }

  public String getComparisonExpression(){
    return comparisonExpression;
  }

  public void setSwitchName(String nameToSet){
    switchName = new String(nameToSet);
  }

  public String getSwitchName(){
    return switchName;
  }

  protected void setActionType(int typeToSet){
    actionType = typeToSet;
  }

  public int getActionType(){
    return actionType;
  }

  public int getEventType(){
    return eventType;
  }

  public void setEventType(int typeToSet){
    eventType = typeToSet;
  }

  public void setVarName(String nameToSet){
    varName = new String(nameToSet.trim().toLowerCase());
  }

  public String getVarName(){
    return varName;
  }

  public boolean isOccComparisonSatisfied(int leftPart){
    if(comparisonExpression.equalsIgnoreCase(CE_EQUALS)){
      if( leftPart == occurenceNumber ){return true;}
    } else if(comparisonExpression.equalsIgnoreCase(CE_GREATER_OR_EQUAL)){
      if( leftPart >= occurenceNumber ){return true;}
    } else if(comparisonExpression.equalsIgnoreCase(CE_GREATER_THAN)){
      if( leftPart > occurenceNumber ){return true;}
    } else if(comparisonExpression.equalsIgnoreCase(CE_LOWER_OR_EQUAL)){
      if( leftPart <= occurenceNumber ){return true;}
    } else if(comparisonExpression.equalsIgnoreCase(CE_LOWER_THAN)){
      if( leftPart < occurenceNumber ){return true;}
    } else if(comparisonExpression.equalsIgnoreCase(CE_NOT_EQUAL)){
      if( !(leftPart == occurenceNumber) ){return true;}
    }
    return false;
  }

}



