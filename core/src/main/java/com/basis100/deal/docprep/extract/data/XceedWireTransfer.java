package com.basis100.deal.docprep.extract.data;
/**
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import MosSystem.*;

import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.*;

import com.basis100.entity.FinderException;

import com.basis100.picklist.*;

import com.basis100.resources.*;

import com.basis100.xml.*;

import org.w3c.dom.*;

import java.util.*;


/**
 * Generates XML for the Xceed Wire Transfer document type
 *
 */
public class XceedWireTransfer extends XceedExtractor
{

  protected double totalRefundableFees = 0d;
  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }
  /**
   * Builds the XML doc from various records in the database.
   *
   */
  protected void buildXMLDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();

    Element root = doc.createElement("Document");
    root.setAttribute("type", "1");
    root.setAttribute("lang", "1");
    doc.appendChild(root);

    Property p = getPrimaryProperty();

    //==========================================================================
    // Language stuff
    //==========================================================================
    addTo(root, getLanguageTag());

    //==========================================================================
    // Solicitor info.  We don't need all the fields, but it saves making
    // new methods for each piece of data that we DO need.
    //==========================================================================
    addTo(root, extractSolicitorSection());

    //==========================================================================
    // Lender Name
    //==========================================================================
    addTo(root, extractLenderName());

    //==========================================================================
    // Current Date
    //==========================================================================
    addTo(root, extractCurrentDate());

    //==========================================================================
    // Broker Firm Name
    //==========================================================================
    addTo(root, extractBrokerFirmName());

    //==========================================================================
    // Broker Agent Name
    //==========================================================================
    addTo(root, extractBrokerAgentName());

    //==========================================================================
    // Borrower Last Name
    //==========================================================================
    addTo(root, extractBorrowerLastName());

    //==========================================================================
    // Approval Date
    //==========================================================================
    addTo(root, extractApprovalDate());

    //==========================================================================
    // Estimated Closing Date
    //==========================================================================
    addTo(root, extractEstClosingDate());

    //==========================================================================
    // Adjusted estimated Closing Date
    // If estimated closing date falls on MON, SUN or SAT it will be moved to
    // FRIDAY.
    //==========================================================================
    addTo(root, extractAdjustedEstClosingDate());

    //==========================================================================
    // Product type
    //==========================================================================
    addTo(root, extractProduct());

    //==========================================================================
    // Charge Priority
    //==========================================================================
    addTo(root, extractChargePriority());

    //==========================================================================
    // Maturity Date
    //==========================================================================
    addTo(root, extractMaturityDate());

    //==========================================================================
    // Loan amount
    //==========================================================================
    addTo(root, extractTotalLoanAmount("LoanAmount"));

    //==========================================================================
    //  Fees
    //  This method MUST be called before calculating the net wire (seen below)
    //  or the total fees won't be calculated and the document will be incorrect.
    //==========================================================================
    addTo(root, extractFees());

    //==========================================================================
    // IAD amount
    //==========================================================================
    addTo(root, extractIADAmount());

    //==========================================================================
    // Net Wire
    //==========================================================================
    addTo(root, extractNetWire());

    //==========================================================================
    // Net interest rate
    //==========================================================================
    addTo(root, extractNetRate("NetInterestRate"));

    //==========================================================================
    // Term
    //==========================================================================
    addTo(root, extractTerm());

    //==========================================================================
    // Amortization
    //==========================================================================
    addTo(root, extractAmortization());

    //==========================================================================
    // Actual appraisal
    //==========================================================================
    addTo(root, extractActualAppraisalValue(p));

    //==========================================================================
    // LTV
    //==========================================================================
    addTo(root, extractLTV());

    //==========================================================================
    // Funder name
    //==========================================================================
    addTo(root, extractFunderName());

    //==========================================================================
    // Servicing Mortgage Number
    //==========================================================================
    addTo(root, extractServicingMortgageNumber());

    //==========================================================================
    // Lender Name Fund
    //==========================================================================
    addTo(root, extractLenderFund());

    //==========================================================================
    // P & I Payment
    //==========================================================================
    addTo(root, extractPAndIPayment());

    //==========================================================================
    // LOB Description
    //==========================================================================
    addTo(root, extractLOBDescription());

    //==========================================================================
    // Payment Frequency
    //==========================================================================
    addTo(root, extractPaymentFrequency());

    //==========================================================================
    // Administrator Data
    //==========================================================================
    addTo(root, extractAdministratorData());

  }

    protected Node extractAdministratorData(){
      Node elemNode = doc.createElement("Administrator");
      try
      {
          UserProfile up = new UserProfile(srk);
          up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                         deal.getInstitutionProfileId()));

          Contact c = up.getContact();

          String val = new String();

          if (!isEmpty(c.getContactFirstName())){
             val = c.getContactFirstName();
             addTo(elemNode,"contactFirstName",val);
          }

          if (!isEmpty(c.getContactLastName())){
              val =  c.getContactLastName();
              addTo(elemNode,"contactLastName",val);
          }

          if (!isEmpty(c.getContactPhoneNumberExtension())){
              val =  c.getContactPhoneNumberExtension();
              addTo(elemNode,"contactPhoneNumberExtension",val);
          }

          if (!isEmpty(c.getContactEmailAddress())){
              val =  c.getContactEmailAddress();
              addTo(elemNode,"contactEmailAddress",val);
          }

      }
      catch(Exception exc){}

      return elemNode;
    }


  /**
   * This method will move date to friday if estimated closing date falls on
   * MONDAY, SUNDAY or SATURDAY.
   */
  protected Node extractAdjustedEstClosingDate()
  {
      String val = null;

      try
      {
        Date lDate = deal.getEstimatedClosingDate();
        if(lDate == null){
          val = "";
        } else {
          Calendar cal = Calendar.getInstance();
          cal.setTime(lDate);
          if(cal.DAY_OF_WEEK == Calendar.SATURDAY){
            cal.add(Calendar.DATE, -1);
          }else if (cal.DAY_OF_WEEK == Calendar.SUNDAY){
            cal.add(Calendar.DATE, -2);
          }else if(cal.DAY_OF_WEEK == Calendar.MONDAY){
            cal.add(Calendar.DATE, -3);
          }
          Date valDate = cal.getTime();
          try{
             val = DocPrepLanguage.getInstance().getFormattedDate(valDate, lang);
          }catch(Exception exc){
             val = "NULL";
          }
        }
      }
      catch (Exception e){
        return getTag("AdjustedEstClosingDate", "ERROR");
      }

      return getTag("AdjustedEstClosingDate", val);
  }

  /**
   * Retrieves the Charge Priority from the deal and formats an XML tag with it.
   *
   * @return The completed Charge Priority XML tag.
   *
   */
  protected Node extractChargePriority()
  {
    String val = null;

    try
    {
      val = (deal.getLienPositionId() == 1) ? "Second" : "First";
    }
    catch (Exception e)
    {
    }

    return getTag("ChargePriority", val);
  }

  /**
   * Retrieves and calculates deal fees and formats an XML tag with it.
   *
   * @return The completed fees XML tag.
   *
   */
  protected Node extractFees()
  {
    Node elemNode = null;

    try
    {
      Collection fees = deal.getDealFees();

      if (fees.isEmpty())
      {
        return null;
      }

      elemNode = doc.createElement("Fees");
      Node itemNode;
      Iterator it = fees.iterator();
      DealFee df;
      double amt;
      int status;
      int feeid;
      String type;

      while (it.hasNext())
      {
        df     = (DealFee) it.next();
        amt    = df.getFeeAmount();
        status = df.getFeeStatusId();
        feeid  = df.getFeeId();

        // change for refundable fee
        if(df.getFee() != null && df.getFee().getRefundable() != null && df.getFee().getRefundable().equalsIgnoreCase("y")){
          itemNode = doc.createElement("refundableFee");

          type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType",
                                                    df.getFee().getFeeTypeId(),
                                                    lang);

          addTo(itemNode, "Verb", type);
          addTo(itemNode, "Amount", formatCurrency(amt));

          addTo(elemNode, itemNode);
          totalRefundableFees += amt;
          continue;
        }

        if (!df.getFee().getPayableIndicator() && (amt != 0d) &&
              (status != Mc.FEE_STATUS_RECIEVED) &&
              (status != Mc.FEE_STATUS_WAIVED) &&
              (feeid != Mc.FEE_TYPE_CMHC_FEE) &&
              (feeid != Mc.FEE_TYPE_APPRAISAL_FEE_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_CMHC_FEE_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_CMHC_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_FEE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE))
        {
          itemNode = doc.createElement("Fee");

          type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType",
                                                    df.getFee().getFeeTypeId(),
                                                    lang);

          addTo(itemNode, "Verb", type);
          addTo(itemNode, "Amount", formatCurrency(amt));

          addTo(elemNode, itemNode);

          totalFees += amt;
        }
      }
    }
    catch (Exception exc)
    {
    }

    return elemNode;
  }

  /**
   * Calculates the net wire from the deal and formats an XML tag with it.
   *
   * @return The completed net wire XML tag.
   *
   */
  protected Node extractNetWire()
  {
    String val = null;

    try
    {
      double amt = deal.getTotalLoanAmount() - totalFees -
                   deal.getInterimInterestAmount() + totalRefundableFees ;

      val = formatCurrency(amt);
    }
    catch (Exception e)
    {
    }

    return getTag("NetWire", val);
  }

  protected Node extractLOBDescription()
  {
    String val = null;

    try
    {
      //val = PicklistData.getDescription("LineOfBusiness", deal.getLineOfBusinessId());
      //ALERT:BX:ZR Release2.1.docprep
      val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LineOfBusiness",
                                               deal.getLineOfBusinessId(), lang);
    }
    catch (Exception e)
    {
    }

    return getTag("LOBDescription", val);
  }

  protected Node extractActualAppraisalValue(Property p)
  {
    String val = null;

    try
    {
      switch (deal.getDealPurposeId())
      {
        case Mc.DEAL_PURPOSE_PURCHASE: // 0
        case Mc.DEAL_PURPOSE_CONSTRUCTION: // 8
        case Mc.DEAL_PURPOSE_ASSUMPTION: // 9
        case Mc.DEAL_PURPOSE_INCREASED_ASSUMPTION: // 10
        case Mc.DEAL_PURPOSE_PORTABLE: // 11
        case Mc.DEAL_PURPOSE_PORTABLE_INCREASE: // 12
        case Mc.DEAL_PURPOSE_PORTABLE_DECREASE: // 13
        case Mc.DEAL_PURPOSE_REPLACEMENT: // 16
        case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS: // 17
        {
          if (p.getPurchasePrice() < p.getActualAppraisalValue())
          {
            val = formatCurrency(p.getPurchasePrice());
          }
          else
          {
            val = formatCurrency(p.getActualAppraisalValue());
          }

          break;
        }

        default:
          val = formatCurrency(p.getActualAppraisalValue());

          break;
      }
    }
    catch (Exception e)
    {
    }

    return getTag("ActualAppraisalValue", val);
  }

  protected Node extractLTV()
  {
    String val = null;
    Property p = getPrimaryProperty();

    try
    {
      switch (deal.getDealPurposeId())
      {
        case Mc.DEAL_PURPOSE_PURCHASE: // 0
        case Mc.DEAL_PURPOSE_CONSTRUCTION: // 8
        case Mc.DEAL_PURPOSE_ASSUMPTION: // 9
        case Mc.DEAL_PURPOSE_INCREASED_ASSUMPTION: // 10
        case Mc.DEAL_PURPOSE_PORTABLE: // 11
        case Mc.DEAL_PURPOSE_PORTABLE_INCREASE: // 12
        case Mc.DEAL_PURPOSE_PORTABLE_DECREASE: // 13
        case Mc.DEAL_PURPOSE_REPLACEMENT: // 16
        case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS: // 17
        {
          if (p.getPurchasePrice() < p.getActualAppraisalValue())
          {
            val = formatRatio(deal.getNetLoanAmount() / p.getPurchasePrice() * 100);
          }
          else
          {
            val = formatRatio(deal.getNetLoanAmount() / p.getActualAppraisalValue() * 100);
          }
          break;
        }

        default:
          val = formatRatio(deal.getNetLoanAmount() / p.getActualAppraisalValue() * 100);
          break;
      }
    }
    catch (Exception e)
    {
    }

    return getTag("LTV", val);
  }
}
