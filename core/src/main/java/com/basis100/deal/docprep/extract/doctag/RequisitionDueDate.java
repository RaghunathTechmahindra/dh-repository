package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class RequisitionDueDate extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());


    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    
    try
    {
      DocumentTracking tr = new DocumentTracking(srk);
      Collection dts = tr.findByLabel((DealPK)deal.getPk(), "Solicitor's Requisition For Funds");

      if(dts.isEmpty())
       return new FMLQueryResult();

      ArrayList l = new ArrayList(dts);

      if(!l.isEmpty())
      {
        DocumentTracking current = (DocumentTracking)l.get(0);

        fml.addValue(current.getDocumentDueDate());
      }


    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;

  }
}
