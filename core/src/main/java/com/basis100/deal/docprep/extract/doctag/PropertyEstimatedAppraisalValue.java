package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class PropertyEstimatedAppraisalValue extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    Property p = null;
    Deal d = null;
    try
    {
        if(clsid == ClassId.DEAL){
          d = (Deal) de;
          p = new Property(srk,null);
          p = p.findByPrimaryProperty(d.getDealId(), d
              .getCopyId(), d.getInstitutionProfileId());
          if( p != null ){
            fml.addCurrency(p.getEstimatedAppraisalValue());
          }
        }else if(clsid == ClassId.PROPERTY)
        {
          p = (Property)de;

          fml.addCurrency(p.getEstimatedAppraisalValue());
        }
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
   catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
