package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class PropertyAddressSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Collection out = new ArrayList();
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      List prps = (List)deal.getProperties();

      Iterator it = prps.iterator();

      Property prim = null;

      while(it.hasNext())
      {
        prim = (Property)it.next();

        if(prim.isPrimaryProperty())
        {
          it.remove();
          out.add(prim);
        }
      }

      out.addAll(prps);
      fml.setValues(out,fml.ENTITY);

    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    return fml;
  }
}
