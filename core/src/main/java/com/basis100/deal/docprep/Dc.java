package com.basis100.deal.docprep;

/**
 * 24/Aug/2007 DVG #DG622 FXP18288: Firstline - Express to Target Download  
 * 15/Jan/2007 DVG #DG568 #5437  Merix - Update Verbiage  for Cond ID's 106, 121, 132, 151 and 324  
 			- as agreed with Brant, a new range of unique cond. ids is started
 * 22/Jun/2006 DVG #DG442 #3360  Concentra - Commitment Letter Updates
 * 04/Apr/2006 DVG #DG398 #2773  CR#177 VRM - Quotation
  - Provincial Clause extraction change
 * 05/Oct/2005 DVG #DG330 #2199  Solicitor Package (Lender profile)
 * 06/Sep/2005 DVG #DG308 #2027  Change to the solicitor package
 * 15/Jul/2005 DVG #DG262 #1804  Solicitor's Package
 * 04/Jul/2005 DVG #DG242 link 2 to Funding (Profile) and
 *   new link to Closing services (FCT)
 * 19/Apr/2005 DVG #DG190 GE Money AU DATX Equifax upload
* 03/Mar/2005 DVG #DG164 #1054  Prospera - CR001
    new web service created from their wsdl file and called from:
     DocumentGenerationHandler.java
 @version 1.1  <br>
 * Date: 06/06/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added the following 2 constants for Pec Email functionality <br>
 *	DOCUMENT_PEC_EMAIL_DEAL_SUMMARY
 MESSAGE_PEC_EMAIL 
 * @version 1.2  <br>
 * Date: 08/04/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added a new constant for Failure Mail functionality. <br>
 *	DOCUMENT_GENERAL_TYPE_FAILURE_EMAIL <br>
 * @version 1.3  <br>
 * Date: 08/08/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added a new constant for eMail Message and NBC Solicitor package functionality. <br>
 *	MESSAGE_NON_DISCLOSURE_COMMIT_EMAIL <br>
 *  DOCUMENT_NBC_PACKAGE <br>
 */

import MosSystem.Mc;


public interface Dc extends Mc
{

  //conditions
  public static final int CUSTOM_CONDITIONS = 0;
  public static final int TRANSFER_CLAUSE = 1;
  public static final int SUPPORT_ALIMONY_PAYMENTS = 2;
  public static final int SUPPORT_AS_INCOME_SOURCE = 3;
  public static final int SEPERATION_AGREEMENT = 4;
  public static final int DOWNPAYMENT_EQUALIZATION = 5;
  public static final int DOWNPAYMENT_SALE = 6;
  public static final int PURCHASE_AND_SALE_AGREEMENT = 8;
  public static final int INCOME_VERIFICATION_SALARIED = 9;
  public static final int INCOME_VERIFICATION_SELF_EMPLOYED = 10;
  public static final int CREDIT_REPORT_REQUIRED = 12;
  public static final int CMHC_REQUIRED = 13;
  public static final int EXISTING_FIRST_IN_GOOD_STANDING = 14;
  public static final int SECOND_MORTGAGE_CONFIRMATION = 15;
  public static final int NO_SECONDARY_FINANCING_ALLOWED = 16;
  public static final int OWNER_OCCUPIED = 17;
  public static final int PAYMENT_OF_DEBTS_BY_SOLICITOR = 18;
  public static final int DEBTS_PAID_PRIOR_TO_ADVANCE = 19;
  public static final int APPRAISAL_REPORT = 20;
  public static final int NON_RENTAL_UNIT = 21;
  public static final int STATEMENT_OF_MORTGAGE = 22;
  public static final int RENTAL_INCOME = 23;
  public static final int ASSIGNMENT_OF_RENTS = 24;
  public static final int STATEMENT_OF_DISCLOSURE = 25;
  public static final int BRIDGE_LOAN = 26;
  public static final int PORTABILITY_CERTIFICATE = 27;
  public static final int SEPTIC_CERTIFICATE = 28;
  public static final int PLANS_AND_SPECS = 29;
  public static final int OCCUPANCY_CERTIFICATE = 30;
  public static final int INSPECTION_FEE = 31;
  public static final int INTEREST_ON_ADVANCES = 32;
  public static final int HOLD_BACK_CONSTRUCTION_IMPROVEMENT = 33;
  public static final int HOLD_BACK_BUILDER_LOAN = 34;
  public static final int SALE_OF_PROPERTY = 35;
  public static final int MLS_LISTING = 36;
  public static final int APPRAISAL_SUBSTITUTE = 37;
  public static final int INTERALIA_MORTGAGE = 38;
  public static final int COMPLETION_ADVANCES = 39;
  public static final int BC_CONDO = 40;
  //public static final int PROPERTY_DISCLOSURE = 41;
  public static final int GE_CAPITAL_INSURANCE = 42;
  public static final int COVENANT_CHANGES_ASSUMPTIONS = 43;
  public static final int PAC_AGREEMENT_ASSUMPTIONS = 44;
  public static final int ASSUMPTION_AGREEMENT = 45;
  public static final int RELEASE_OF_COVENANT = 46;
  public static final int PARTIAL_DISCHARGE = 47;
  public static final int PURCHASE_AND_SALE_EXISTING_CLIENT = 48;
  public static final int INCOME_SOURCE_COMMISSION = 50;
  public static final int STRATA_CLAUSE = 51;
  public static final int AGRICULTUAL_DECLARATION = 52;
  public static final int APPAISAL_REPORT_IMPROVEMENTS = 53;
  public static final int WORK_VISA = 54;
  public static final int SIGNED_COMMITMENT = 55;
  public static final int SOLICITOR_NAME_AND_ADDRESS = 56;
  public static final int VOID_CHEQUE = 57;
  public static final int PAC_PFC_FORM = 58;
  public static final int SOLICITOR_REQUISITION_OF_FUNDS = 59;
  public static final int TAXES = 60;
  public static final int NO_TAXES = 61;
  public static final int PRIVILEGE = 62;
  public static final int PRIVILEGE_OPEN = 63;
  public static final int PRIVILEGE_NOT_ALLOWED = 64;
  public static final int PREPAYMENT_NHA = 65;
  public static final int PREPAYMENT_NHA_LONGTERM = 66;
  public static final int PREPAYMENT_3_MONTH_PENALTY = 67;
  public static final int PREPAYMENT_OPEN_WITH_PENALTY = 68;
  public static final int PREPAYMENT_OPEN_PENALTY_LONG_TERM = 69;
  public static final int PREPAYMENT_NOT_ALLOWED = 70;
  public static final int PREPAYMENT_OPEN = 71;
  public static final int UNCONFIRMED_RATE = 72;
  public static final int UNCONFIRMED = 73;
  public static final int BUYDOWN_PROGRESS = 74;
  public static final int BUYDOWN_ADVANCE = 75;
  public static final int BUYDOWN_DETAILS = 76;
  public static final int BUYDOWN = 77;
  public static final int AUTO_ADJUST = 78;
  public static final int PROJECTED_BALANCE_VERB = 79;
  public static final int PREMIUM_VERB = 80;
  public static final int TOTAL_LOAN_AMOUNT = 81;
  public static final int NEW_FUNDS_VERB = 82;
  public static final int BASE_LOAN_AMOUNT = 83;
  public static final int LOAN_AMOUNT = 84;
  public static final int PandI = 85;
  public static final int PST_VERB = 86;
  public static final int EMILIWEB = 87;
  public static final int MLI_WAIVER = 88;
  public static final int EXISTING = 89;
  public static final int WEEKLY_PAYMENT_FREQUENCY = 90;
  public static final int BIWEEKLY_PAYMENT_FREQUENCY = 91;
  public static final int MONTHLY_PAYMENT_FREQUENCY = 92;
  public static final int VARIABLE_PAYMENTS = 93;
  public static final int EARLY_RENEWAL = 94;
  public static final int NO_EARLY_RENEWAL = 95;
  public static final int PREPAYMENT_2_MONTH_PENALTY = 96;
  public static final int REFINANCE = 97;
  public static final int ALBERTA_EXECUTION = 98;
  public static final int EXECUTION = 99;
  public static final int SURVEYVERB = 100;
  public static final int ALBERTA_SURVEY_CLAUSE = 101;
  public static final int BC_SURVEY_CLAUSE = 102;
  public static final int MAN_SASK_SURVEY_CLAUSE = 103;
  public static final int DEFAULT_SURVEY_CLAUSE = 104;
  public static final int ONTARIO_SURVEY_CLAUSE = 105;
  public static final int ALBERTA_INSURED_CONDO_CLAUSE = 106;
  public static final int ALBERTA_INSURED_CLAUSE = 107;
  public static final int ALBERTA_CONVENTIONAL_CLAUSE = 108;
  public static final int ALBERTA_CLAUSE = 109;
  public static final int BC_HEADER_CLAUSE = 110;
  public static final int BC_LENDER_CLAUSE = 111;
  public static final int BC_INSURED_CLAUSE = 112;
  public static final int BC_CONVENTIONAL_CLAUSE = 113;
  public static final int BC_LAND_TITLE_CLAUSE = 114;
  public static final int BC_FORM_DATE_CLAUSE = 115;
  public static final int MANITOBA_HEADER_CLAUSE = 116;
  public static final int MANITOBA_LENDER_CLAUSE = 117;
  public static final int MANITOBA_INSURED_CLAUSE = 118;
  public static final int MANITOBA_CONVENTIONAL_CLAUSE = 119;
  public static final int MANITOBA_FORM_DATE_CLAUSE = 120;
  public static final int NB_INSURED_CONDO_CLAUSE = 121;
  public static final int NB_INSURED_CLAUSE = 122;
  public static final int NB_CONVENTIONAL_CLAUSE = 123;
  public static final int NB_LENDER_CLAUSE = 124;
  public static final int NB_DELETIONS_CLAUSE = 125;
  public static final int NB_OFFER_LETTER_CLAUSE = 126;
  public static final int NF_INSURED_CLAUSE = 127;
  public static final int NF_CONVENTIONAL_CLAUSE = 128;
  public static final int NF_LENDER_CLAUSE = 129;
  public static final int NF_DELETIONS_CLAUSE = 130;
  public static final int NF_OFFER_LETTER_CLAUSE = 131;
  public static final int NOVA_SCOTIA_INSURED_CONDO_CLAUSE = 132;
  public static final int NOVA_SCOTIA_INSURED_CLAUSE = 133;
  public static final int NOVA_SCOTIA_CONVENTIONAL_CLAUSE = 134;
  public static final int NOVA_SCOTIA_LENDER_CLAUSE = 135;
  public static final int NOVA_SCOTIA_DELETIONS_CLAUSE = 136;
  public static final int NOVA_SCOTIA_OFFER_LETTER_CLAUSE = 137;
  public static final int ONTARIO_HEADER_CLAUSE = 138;
  public static final int ONTARIO_LENDER_CLAUSE = 139;
  public static final int ONTARIO_INSURED_CLAUSE = 140;
  public static final int ONTARIO_CONVENTIONAL_CLAUSE = 141;
  public static final int ONTARIO_LRRA_CLAUSE = 142;
  public static final int ONTARIO_SPOUSAL_CLAUSE = 143;
  public static final int ONTARIO_FORM_DATE_CLAUSE = 144;
  public static final int ONTARIO_EREGISTRATION_CLAUSE = 145;
  public static final int PEI_INSURED_CLAUSE = 146;
  public static final int PEI_CONVENTIONAL_CLAUSE = 147;
  public static final int PEI_LENDER_CLAUSE = 148;
  public static final int PEI_DELETIONS_CLAUSE = 149;
  public static final int PEI_OFFER_LETTER_CLAUSE = 150;
  public static final int SK_INSURED_CONDO_CLAUSE = 151;
  public static final int SK_INSURED_CLAUSE = 152;
  public static final int SK_CONVENTIONAL_CLAUSE = 153;
  public static final int SK_GRADUATED_CLAUSE = 154;
  public static final int SK_LENDER_CLAUSE = 155;
  public static final int SK_DELETIONS_CLAUSE = 156;
  public static final int SK_OFFER_LETTER_CLAUSE = 157;
  public static final int SECOND_OFFER_CLAUSE = 158;
  public static final int SECOND_DEFAULT_CLAUSE = 159;
  public static final int SECOND_DEFAULT_COST_CLAUSE = 160;
  public static final int BUILDER_HEADER_CLAUSE = 161;
  public static final int BUILDER_CLAUSE = 162;
  public static final int DISCLOSURE = 163;
  public static final int TITLE_CERTIFICATE = 164;
  public static final int _CERTIFICATE = 165;
  public static final int DUPLICATE_MORTGAGE_FORM_2 = 166;
  public static final int DUPLICATE_MORTGAGE = 167;
  public static final int DISCLOSURE_AND_WAIVER = 168;
  public static final int DIRECTION = 169;
  public static final int DUPLICATE_MORTGAGE_ORIGINAL = 170;
  public static final int BUSINESS_VERB = 171;
  public static final int RESIDENCE_VERB = 172;
  public static final int STATEMENT_OF_DISCLOSURE_VERB = 173;
  public static final int STATEMENT_OF_DISCLOSURE_RPT = 174;
  public static final int BC_EXTRA_REGISTRATION_NUMBER = 175;
  public static final int DND = 176;
  public static final int VARIABLE_INSTRUCTION = 177;
  public static final int MORTGAGE_INSURANCE_TEXT = 178;
  public static final int CLOSING_TEXT = 179;
  public static final int BRIDGE_SECOND_MORTGAGE = 180;
  public static final int VARIABLE_PAYMENT_PREAPP = 181;
  public static final int BROKER_INSTRUCTION = 182;
  public static final int BRIDGE_REGISTER_SECOND = 183;
  public static final int WORK_LABEL = 184;
  public static final int PROPERTY_TAX_CLIENT = 185;
  public static final int PROPERTY_TAX_LENDER = 186;
  public static final int GUARANTOR_VERB = 187;
  public static final int TRANSFER_SURVEY = 188;
  public static final int GUARANTOR_LABEL = 189;
  public static final int COVENANTOR_LABEL = 190;
  public static final int MULTIPLE_GUARANTOR_LABEL = 191;
  public static final int MULTIPLE_COVENANTOR_LABEL = 192;
  public static final int	DOWNPAYMENT_MULTIPLE = 193;
  public static final int	CLOSING_COSTS = 194;
  public static final int	TRANSFER_ACCEPTANCE_CONFIRMATION = 195;
  public static final int	TRANSFER_LETTER_OF_DIRECTION = 196;
  public static final int	TRANSFER_AMENDING_AGREEMENT = 197;
  public static final int	TRANSFER_PAYOUT_STATEMENT = 198;
  public static final int	COVENANTOR_VERB = 199;
  public static final int	BLENDED_RATE = 200;
  public static final int	EARLY_RENEWAL_ING = 201;
  public static final int	ADDITIONAL_SURVEY_CLAUSE = 202;
  public static final int	MORTGAGE_APPROVAL_TERM = 203;
  public static final int	SURVEY_APPROVAL_TERM = 204;
  public static final int	PROCESSING_APPROVAL_TERM = 205;
  public static final int	NSF_APPROVAL_TERM = 206;
  public static final int	REPRESENTATION_APPROVAL_TERM = 207;
  public static final int	RENEWAL_APPROVAL_TERM = 208;
  public static final int	RATE_GUARANTEE_APPROVAL_TERM = 209;
  public static final int       TAX_CONDITION = 209;
  public static final int	SOLICITOR_INSERT_PREAMBLE = 210;
  public static final int	SOLICITOR_INSERT_END_TEXT = 211;
  public static final int	ACCEPTANCE_RETURN_STATEMENT = 212;
  public static final int	ACCEPTANCE_CHANGE_STATEMENT = 213;
  public static final int	ACCEPTANCE_PAC_STATEMENT = 214;
  public static final int	ACCEPTANCE_INSTITUTION_STATEMENT = 215;
  public static final int	ASSUMPTION_APPROVAL_TERM = 216;
  public static final int	PORTABLILITY_APPROVAL_TERM = 217;
  public static final int	INSTRUCTIONS_APPROVAL_TERM = 218;
  public static final int	TRANSFER_CLAUSE_QUEBEC = 219;
  public static final int	QUEBEC_CONDO = 220;
  public static final int	BROKER_PFC_CLAUSE = 221;
  public static final int	BROKER_SOLICITOR_CLAUSE = 222;
  public static final int	FIRE_INSURANCE = 225;
  public static final int       PREMIUM_SALES_TAX = 229;
  public static final int       CMHC_GE_APPLICATION_FEE = 230;
  public static final int       DEBTS_PAID_FROM_PROCEEDS = 292;
  public static final int       PROPERTY_TAX_HOLDBACK_INTERIM = 295;
  public static final int       PROPERTY_TAX_HOLDBACK_FINAL = 296;

  public static final int       ALBERTA_DISCLOSURE_CLAUSE = 323;    // Catherine, for Rel. 3.1 - Disclosure
  public static final int       BC_INSURED_CONDO = 324;
  public static final int       BC_ADDRESS = 325;
  public static final int       NB_DISCLOSURE_CLAUSE = 326;         // Catherine, for Rel. 3.1 - Disclosure
  public static final int       NF_DISCLOSURE_CLAUSE = 327;         // Catherine, for Rel. 3.1 - Disclosure
  public static final int       NS_DISCLOSURE_CLAUSE = 328;         // Catherine, for Rel. 3.1 - Disclosure
  public static final int       ONTARIO_ADDRESS = 329;
  public static final int       ONTARIO_TERANET = 330;
  public static final int       PEI_DISCLOSURE_AND_TAX = 331;
  public static final int       SOLICITOR_INSTRUCTION_PROGRESS_ADV = 333;

  public static final int       CLOSING_TEXT_ENDING = 373;
  public static final int       SCHEDULE_TEXT_A = 374;
  public static final int       PRIVACY_CLAUSE = 375;
  public static final int       RATE_GUARANTEE = 399;
  public static final int       PRIVACY_CLAUSE_ENDING = 376;
  public static final int       PREAPP_OPEN = 379;
  public static final int       PREAPP_BOLD = 380;
  public static final int       PREAPP_PRODUCT = 381;
  public static final int       PREAPP_RATE = 382;
  public static final int       PREAPP_ATTACH = 383;
  public static final int       AUTO_ADJUST_X = 399;

  public static final int       TAXES_PAID = 404;
  public static final int       CLOSING_TEXT_ENDING_X = 408;
  public static final int       BC_DISCLOSURE_CLAUSE = 412;

  public static final int       YUKON_CONVENTIONAL_CLAUSE = 417;
  public static final int       YUKON_INSURED_CLAUSE = 418;
  public static final int       YUKON_DELETIONS_CLAUSE = 419;
  public static final int       YUKON_LENDER_CLAUSE = 420;
  public static final int       YUKON_OFFER_LETTER_CLAUSE = 421;
  public static final int       NWT_CONVENTIONAL_CLAUSE = 422;
  public static final int       NWT_INSURED_CLAUSE = 423;
  public static final int       NWT_DELETIONS_CLAUSE = 424;
  public static final int       NWT_LENDER_CLAUSE = 425;
  public static final int       NWT_OFFER_LETTER_CLAUSE = 426;

  //#DG398 VRMs
  public static final int       ALBERTA_INSURED_CONDO_CLAUSE_VRM = 475;
  public static final int       ALBERTA_INSURED_CLAUSE_VRM = 476;
  public static final int       ALBERTA_CONVENTIONAL_CLAUSE_VRM = 477;
  public static final int       BC_INSURED_CLAUSE_VRM = 478;
  public static final int       BC_CONVENTIONAL_CLAUSE_VRM = 479;
  public static final int       MANITOBA_INSURED_CLAUSE_VRM = 480;
  public static final int       MANITOBA_CONVENTIONAL_CLAUSE_VRM = 481;
  public static final int       NB_INSURED_CLAUSE_VRM = 482;
  public static final int       NB_CONVENTIONAL_CLAUSE_VRM = 483;
  public static final int       NF_INSURED_CLAUSE_VRM = 484;
  public static final int       NF_CONVENTIONAL_CLAUSE_VRM = 485;
  public static final int       NOVA_SCOTIA_INSURED_CLAUSE_VRM = 486;
  public static final int       NOVA_SCOTIA_CONVENTIONAL_CLAUSE_VRM = 487;
  public static final int       ONTARIO_INSURED_CLAUSE_VRM = 488;
  public static final int       ONTARIO_CONVENTIONAL_CLAUSE_VRM = 489;
  public static final int       PEI_INSURED_CLAUSE_VRM = 490;
  public static final int       PEI_CONVENTIONAL_CLAUSE_VRM = 491;
  public static final int       SK_INSURED_CLAUSE_VRM = 492;
  public static final int       SK_CONVENTIONAL_CLAUSE_VRM = 493;
  public static final int       YUKON_CONVENTIONAL_CLAUSE_VRM = 494;
  public static final int       YUKON_INSURED_CLAUSE_VRM = 495;
  public static final int       NWT_CONVENTIONAL_CLAUSE_VRM = 496;
  public static final int       NWT_INSURED_CLAUSE_VRM = 497;
  //#DG398 VRMs end
  //#DG442 condo VRMs
  public static final int       SK_INSURED_CONDO_CLAUSE_VRM = 499;
  public static final int       NB_INSURED_CONDO_CLAUSE_VRM = 501;
  public static final int       NS_INSURED_CONDO_CLAUSE_VRM = 503;
  //#DG442 end
  public static final int       BC_INSURED_CONDO_VRM = 5000;		//#DG568

  public static final int       INTEREST_CALCULATION = 580;
  public static final int       SIGNATURE = 581;

//  public static final int       PAC_AGREEMENT_ASSUMPTIONS = 396;
//  public static final int       AUTO_ADJUST = 399;

  //#DG308
  public static final int       OpenStatem = 1001;
  public static final int       BorIdTxt = 1002;
  public static final int       FundingTxt = 1003;
  public static final int       ConfirmClosing = 1004;
  public static final int       TitleInsReq = 1005;
  public static final int       Searches = 1006;
  public static final int       SurveyReq = 1007;
  public static final int       CondoStrataTitle = 1008;
  public static final int       FireInsur = 1009;
  public static final int       NewConstruction = 1010;
  public static final int       ConstructionProgrAdv = 1011;
  public static final int       AssignmentRents = 1012;
  public static final int       Disclosure = 1013;
  public static final int       PlanningAct = 1014;
  public static final int       MatrimonialPropAct = 1015;
  public static final int       Renew4Guarantors = 1016;
  public static final int       CorporationAsBor = 1017;
  public static final int       SolicitorRpt = 1018;
  //#DG308 end
  public static final int       RegisteredName = 1019;  //#DG330
  public static final int       RegisteredAddress = 1020;  //#DG330
  public static final int       RegisteredPhoneFax = 1021;  //#DG330

  public static final int DT_SOURCE_SYSTEM_GENERATED = 0;
  public static final int DT_SOURCE_USER_SELECTED = 1;
  public static final int DT_SOURCE_CUSTOM_FREEFORM = 2;


  public static final int LENDER_PROFILE_GENERIC   = 0;
  public static final int LENDER_PROFILE_ING     =   1;
  public static final int LENDER_PROFILE_CLARICA  =  2;
  public static final int LENDER_PROFILE_GC        = 3;

 // public static final int LENDER_PROFILE_MCAP   =  99  ; //not used but present in code for now

  public static final int DOCUMENT_GENERAL_TYPE_COMMITMENT = 1;
  public static final int DOCUMENT_GENERAL_TYPE_TRANSFER_PACKAGE = 2;
  public static final int DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE = 3;
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY = 4;
  public static final int DOCUMENT_GENERAL_TYPE_PRAPPCERT = 5;
  public static final int DOCUMENT_GENERAL_TYPE_BRIDGE_PACKAGE = 6;
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_UPLOAD = 7;
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_APPROVAL = 8;
  public static final int DOCUMENT_GENERAL_TYPE_MI_REQUIRED  = 9;
  public static final int DOCUMENT_GENERAL_TYPE_MI_CANCELLED = 10;
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_DENIAL = 11;
  public static final int DOCUMENT_GENERAL_TYPE_EZENET_BASE = 12;
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_PACKAGE = 13;
  public static final int DOCUMENT_GENERAL_TYPE_DISBURSMENT = 17;
  public static final int DOCUMENT_GENERAL_TYPE_CONDITIONS_OUTSTANDING = 18;
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_LITE = 19;
  public static final int DOCUMENT_GENERAL_TYPE_WIRE_TRANSFER = 20;
  public static final int DOCUMENT_GENERAL_TYPE_DATA_ENTRY = 21;
  public static final int DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE = 22;
  public static final int DOCUMENT_GENERAL_TYPE_WIRE_TRANSFER_2 = 23;
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_PDF = 50;
  public static final int DOCUMENT_GENERAL_TYPE_CONDITIONS_OUTSTANDING_PDF = 51;
  public static final int DOCUMENT_GENERAL_TYPE_COMMITMENT_PDF = 52;
  public static final int DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE_PDF = 53;
  public static final int DOCUMENT_GENERAL_TYPE_TDCT_CAPLINK = 54;

  public static final int DOCUMENT_GENERAL_TYPE_MEMO = 99;

  public static final int DOCUMENT_GENERAL_TYPE_APPLICATION_FOR_INSURANCE = 60;
  public static final int DOCUMENT_GENERAL_TYPE_CONDITIONAL_MTG_APPROVAL = 61;
  public static final int DOCUMENT_GENERAL_TYPE_MTG_APPROVAL_NO_COND = 62;
  public static final int DOCUMENT_GENERAL_TYPE_TRANSFERRED_MTG_FILE = 63;
  public static final int DOCUMENT_GENERAL_TYPE_MTG_SUBMITTED_FOR_DECISION = 64;
  public static final int DOCUMENT_GENERAL_TYPE_MTG_FINANCING_APPROVAL = 65;
  public static final int DOCUMENT_GENERAL_TYPE_PRE_APPROVED_MTG_LETTER = 66;
  public static final int DOCUMENT_GENERAL_TYPE_DECLINED_MTG_LETTER = 67;
  public static final int DOCUMENT_GENERAL_TYPE_RATE_BONUS_MTG_LETTER = 110;
  public static final int DOCUMENT_GENERAL_TYPE_REQUESTED_CREDIT_EXPERIENCE = 68;
  public static final int DOCUMENT_GENERAL_TYPE_INSTRUCTIONS_TO_APPRAISER = 69;
  public static final int DOCUMENT_GENERAL_TYPE_OFFER_TO_FINANCE_LETTER_CAISSE = 70;
  public static final int DOCUMENT_GENERAL_TYPE_LOAN_APPLICATION_CANCELLED = 71;
  public static final int DOCUMENT_GENERAL_TYPE_CREDIT_APPLICATION = 72;
  public static final int DOCUMENT_GENERAL_TYPE_CREDIT_ANALYSIS_FORM = 73;
  public static final int DOCUMENT_GENERAL_TYPE_MTG_FINANCING_APPROVAL_ADD_ON = 74;

  //--DJ_CR005--02June2004--start--//
  // New approach for the request bundled with the necessary attachments (in pre-defined order).
  public static final int DOCUMENT_GENERAL_TYPE_TRANSFERRED_MTG_FILE_BUNDLE = 75;
  public static final int DOCUMENT_GENERAL_TYPE_MTG_SUBMITTED_FOR_DECISION_BUNDLE = 76;
  public static final int DOCUMENT_GENERAL_TYPE_MTG_FINANCING_APPROVAL_BUNDLE = 77;
  //--DJ_CR005--end--//

  public static final int DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_DJ = 78;          // DJ new Deal Summary #620
  public static final int DOCUMENT_GENERAL_TYPE_DEAL_SUMMARY_DJ_APPROVAL = 79; // DJ new Deal Summary #656

  public static final int DOCUMENT_GENERAL_TYPE_SOL_FOLLOW_UP_1 = 80;          // CV #662
  public static final int DOCUMENT_GENERAL_TYPE_SOL_FOLLOW_UP_2 = 81;          // CV #662
  public static final int DOCUMENT_GENERAL_TYPE_FUNDING_UPLOAD  = 55;          // XD to MCAP
  public static final int DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD    = 82;          // BMO to CCAPS 1.2
  public static final int DOCUMENT_GENERAL_TYPE_PROSPERA_UPLOAD    = 83;          //#DG164
  public static final int DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD    = 84;          //#DG190
  //public static final int DOCUMENT_GENERAL_TYPE_FUNDING_UPLOAD2  = 85;          //#DG242
  public static final int DOCUMENT_GENERAL_TYPE_CLOSING_UPLOAD    = 86;          //#DG242
  public static final int DOCUMENT_GENERAL_TYPE_GEMONEY_IA_UPLOAD	= 88;     //IA Data Sheet

  // Rel 3.1 Disclosure document types - Sasa
  public static final int DOCUMENT_GENERAL_TYPE_DISCLOSURE_AB = 101;
  public static final int DOCUMENT_GENERAL_TYPE_DISCLOSURE_BC = 102;
  public static final int DOCUMENT_GENERAL_TYPE_DISCLOSURE_NL = 103;
  public static final int DOCUMENT_GENERAL_TYPE_DISCLOSURE_NS = 104;
  public static final int DOCUMENT_GENERAL_TYPE_DISCLOSURE_ON = 105;
  public static final int DOCUMENT_GENERAL_TYPE_DISCLOSURE_PE = 106;
  //Addition for DJ Credit scoring
  public static final int DOCUMENT_DJ_AUTO_TYPE_CONDITIONAL_MTG_APPROVAL = 109;

//	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final int DOCUMENT_PEC_EMAIL_DEAL_SUMMARY = 200;
//	***** Change by NBC Impl. Team - Version 1.1 - End*****//

	//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	public static final int DOCUMENT_GENERAL_TYPE_FAILURE_EMAIL = 210;
	//	***** Change by NBC Impl. Team - Version 1.2 - End*****//
	
  public static final String VERSION_1_GENX = "GenX1.0";
  public static final String VERSION_1_MIREQ = "MI Request";
  public static final String VERSION_1_MICANCEL = "Mi Cancelled";
  public static final String VERSION_1_UPLOAD = "Upload";
  public static final String VERSION_1_APPROVAL = "Approval";
  public static final String VERSION_1_DENIAL = "Denial";
  public static final String VERSION_1_EZENET = "Ezenet";
  public static final String VERSION_1_TDCT_CAPLINK = "Caplink";
  public static final String VERSION_1_PENDING_MESSAGE = "Pending Message";
  public static final String VERSION_1_CCAPS           = "ccaps";              // BMO to CCAPS 1.2
  public static final String VERSION_1_NBCAPPRAISAL    = "NBCAppraisal"; //Appraisal CR NBC/PP implementation
  public static final String VERSION_FUNDING     = "Funding";            // link to MCAP
  public static final String VERSION_FUNDING2 	 = "Funding2";	//#DG622
  public static final String VERSION_FUNDING_ALT = "FundingAlt";	//#DG622

  //#DG622 these are the tag names to retrieve the outbound paths from 'application.xml'  
  public static final String OUT_TAG_FILOGIXLINK = "filogixlink";
  public static final String OUT_TAG_APPROVAL = VERSION_1_APPROVAL;
  public static final String OUT_TAG_DENIAL = VERSION_1_DENIAL;
  public static final String OUT_TAG_UPLOAD = VERSION_1_UPLOAD;
  public static final String OUT_TAG_EZENET = VERSION_1_EZENET;
  public static final String OUT_TAG_CAPLINK = VERSION_1_TDCT_CAPLINK;
  public static final String OUT_TAG_PENDINGMESSAGE = "pendingmessage";
  public static final String OUT_TAG_GE = "ge";
  public static final String OUT_TAG_CMHC = "cmhc";
  //#DG622 end   

  public static final String UPLOAD_FILE_NAMING_MOS = "MOS";
  public static final String UPLOAD_FILE_NAMING_CV  = "CV";
/*
  public static final int	SYSTEM_TYPE_MANUAL_ENTRY =	0;
  public static final int	SYSTEM_TYPE_MORTY =	1;
  public static final int	SYSTEM_TYPE_CENTRIC =	2;
  public static final int	SYSTEM_TYPE_OME =	3;
  public static final int	SYSTEM_TYPE_CLARICA =	4;
  public static final int	SYSTEM_TYPE_EXTRANET =	5;
  public static final int	SYSTEM_TYPE_ECNI =	6;
*/
  public static final int	DOCUMENT_REQUEST_STATUS_DISABLED = -100;
  public static final int DOCUMENT_REQUEST_STATUS_NEW = 0;
  public static final int DOCUMENT_REQUEST_STATUS_RETRIED = 1;
  public static final int DOCUMENT_REQUEST_STATUS_FAILED = 2;
  public static final int DOCUMENT_REQUEST_STATUS_IN_PROCESS = 9;

  // Catherine, #1053 start ---------------------------------------------------------------------------------
  // references to Message table (based on message id)
  public static final int MESSAGE_AUTOBROKER_15_PTC   = 200;                   // CVAutoBroker15PTC
  public static final int MESSAGE_AUTOBROKER_10_PTC = 201;                   // CVAutoBroker7/10PTC
  public static final int MESSAGE_AUTOBROKER_7_PTC = 204;                   // CVAutoBroker7/10PTC
  public static final int MESSAGE_AUTOBROKER_3_COMMIT = 202;                   // CVAutoBroker3DayCommit
  public static final int MESSAGE_AUTOBROKER_7_COMMIT = 203;                   // CVAutoBroker7DayCommit

	// Catherine, #1053 end
	// ------------------------------------------------------------------------------------

//	***** Change by NBC Impl. Team - Version 1.1 - Start *****//

	public static final int MESSAGE_PEC_EMAIL = 300;
//	***** Change by NBC Impl. Team - Version 1.1 - End*****//

  //FeePaymentMethod #DG262
  public static final int FEE_PayMethodTakeFromFirstAdvance = 0;
  public static final int FEE_PayMethodCheque = 1;
  public static final int FEE_PayMethodTakeFromEachAdvance = 3;

  // Catherine: Rel 3.1 - Disclosure -------------------------------------------- begin -----

  // Catherine: Rel 3.1 - Disclosure -------------------------------------------- end -----
//	***** Change by NBC Impl. Team - Version 1.3 - Start *****//
	public static final int MESSAGE_NON_DISCLOSURE_COMMIT_EMAIL = 301;
	public static final int DOCUMENT_NBC_PACKAGE = 220;
	public static final int DOCUMENT_REQUEST_APPRAISAL = 230;  
	public static final int MESSAGE_NON_DISCLOSURE_REQUEST_APPRAISAL_EMAIL = 302;
//	***** Change by NBC Impl. Team - Version 1.3 - End*****//
	
}
