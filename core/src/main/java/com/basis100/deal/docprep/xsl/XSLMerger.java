package com.basis100.deal.docprep.xsl;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Driver;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docprep.DocumentGenerationManager;
import com.basis100.deal.docprep.ExpressDocprepHelper;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

public class XSLMerger
{
  //DocumentGenerationHandler handler;
    private int dealNumber;
    private int institutionNumber;
  private SysLogger logger;
  private DocumentProfile documentProfile;
  private String mXslDirLocation;
  private SessionResourceKit srk = null;		//#DG650 set as a parameter to the template
  private Deal deal = null;		//#DG650 set as a parameter to the template

  protected static final int TYPE_UNKNOWN = -1;
  public static final int TYPE_RTF = 0;
  public static final int TYPE_PDF = 1;
  public static final int TYPE_HTML = 2;
  public static final int TYPE_CSV = 3;

  public XSLMerger() {
    dealNumber = -1;
    institutionNumber = -1;
    logger = null;
    documentProfile = null;
    
    mXslDirLocation = ExpressDocprepHelper.getTemplatesFolder();
    
  }

  //#DG650 rewritten  
  //public XSLMerger( int pDealNo, int instituionNumber, SysLogger pLogger, DocumentProfile pDocumentProfile ) {
  public XSLMerger(Deal deal, SessionResourceKit srk, DocumentProfile pDocumentProfile ) {
  	  this.srk = srk;		//#DG650
  	  this.deal = deal;		//#DG650
	  dealNumber = deal.getDealId();
  	  this.institutionNumber =  srk.getExpressState().getDealInstitutionId(); 
	  logger = srk.getSysLogger();
	  documentProfile = pDocumentProfile;

	  mXslDirLocation = ExpressDocprepHelper.getTemplatesFolder();
	  	  
  }

  public void setLogger( SysLogger pLog ){
    logger = pLog;
  }

  public void setDocumentProfile(DocumentProfile pProfile){
    documentProfile = pProfile;
  }

  /*#DG650 not used 
  public void setDealNumber(int num){
    dealNumber = num;
  }*/

  /**
   * Get the file extension for <code>type</code>.
   */
  public static String getDocTypeExt(int type)
  {
	  switch (type) {
	  	case TYPE_RTF: return "rtf";
	  	case TYPE_PDF: return "pdf";
	  	case TYPE_CSV: return "csv";
	  	default:       return "Unknown";
	  }
  }

  /**
   * Get the document format for <code>type</code>.
   */
  public static String getFormat(int type)
  {
	  switch (type) {
	  	case TYPE_RTF: return "rtf1";
	  	case TYPE_PDF: return "pdf";
	  	case TYPE_CSV: return "csv";
	  	default:       return "Unknown";
	  }
  }

  public static int getTypeFromFormat(String format)
  {
	  if (format.equalsIgnoreCase("rtf1"))
		  return TYPE_RTF;
	  else if (format.equalsIgnoreCase("pdf"))
		  return TYPE_PDF;
	  else if (format.equalsIgnoreCase("csv"))
		  return TYPE_CSV;
	  else
		  return TYPE_UNKNOWN;
  }

  private String replaceString(String pInStr, String pReplaceStr, String pReplaceTo){
	  if(pReplaceStr == null || pInStr == null || pReplaceTo == null){
		  return pInStr;
	  }
	  if(pInStr.trim().equals("") || pReplaceStr.trim().equals("") || pReplaceTo.trim().equals("")){
		  return pInStr;
	  }
	  int lIndex;
	  StringBuffer retBuf = new StringBuffer();
	  for(;;){
		  lIndex = pInStr.indexOf(pReplaceStr );
		  if(lIndex == -1){
			  retBuf.append(pInStr);
			  break;
		  }
		  retBuf.append( pInStr.substring(0,lIndex) ).append(pReplaceTo);
		  pInStr = pInStr.substring(lIndex + pReplaceStr.length());
	  }
	  return retBuf.toString();
  }


  public Vector createRTFDocuments(String data) throws Exception
  {
	  Vector rv = new Vector();
	  Vector v = documentProfile.extractTemplates();
	  Iterator it = v.iterator();
	  int lCounter = 0;
	  while( it.hasNext() ){
		  String lXSLTemplateName = (String)it.next();
		  if(lXSLTemplateName == null) {continue;}
		  if(lXSLTemplateName.trim().equals("")) {continue;}
		  
		  lCounter++;
		  String rtfExt = PropertiesCache.getInstance().getProperty(documentProfile.getInstProfileId(), 
		                                                            "com.basis100.alternate.rtf.extension", "rtf");
		  File f = createDocument(data,lXSLTemplateName,new Integer(lCounter), rtfExt);
		  if( f != null ){
			  rv.addElement(f);
		  }
	  }
	  return rv;
  }

  public Vector createCSVDocuments(String data) throws Exception{
	  Vector rv = new Vector();
	  Vector v = documentProfile.extractTemplates();
	  Iterator it = v.iterator();
	  int lCounter = 0;
	  while (it.hasNext()) {
		  String lXSLTemplateName = (String) it.next();
		  if (lXSLTemplateName == null) {continue;}
		  if (lXSLTemplateName.trim().equals("")) {continue;}
		  
		  lCounter++;
		  String csvExt = PropertiesCache.getInstance().getProperty(documentProfile.getInstProfileId(),
		                                                            "com.basis100.alternate.csv.extension", "csv");
		  File f = createDocument(data, lXSLTemplateName, new Integer(lCounter),csvExt);
		  if (f != null) {
			  rv.addElement(f);
		  }
	  }
	  return rv;
  }

  /**
   * Rel 3.1 - Sasha
   * Returns Vector, which elements are byte arrays of generated documents, one document per template
   * from a DocumentProfile.documentTemplatePath field
   *
   * @param data
   * @return
   * @throws DocPrepException
   */
  public Vector getPDFDocuments(String data) throws DocPrepException {
	  return pGetPDFDocuments( data);
  }


  /**
   * Rel 3.1 - Sasha
   * Passes xmlString to the local method, which then generates PDF files from vector of byte arrays
   * and writes them to a disk
   *
   * @param data
   * @return
   * @throws DocPrepException, FileNotFoundException, IOException
   */
  public Vector createPDFDocumentsFromString(String data) throws DocPrepException, FileNotFoundException, IOException {
	  Vector vPdfFile = null;

	  //Generate PDF documents as byte arrays
	  Vector vPdfByte = pGetPDFDocuments(data);
	  if( vPdfByte != null )
		  vPdfFile = createPDFDocumentsFromBytes(vPdfByte);

	  return vPdfFile;
  }

  /**
   * Rel 3.1 - Sasha
   * Generates PDF files from a vector of byte arrays and writes them to a disk
   *
   * @param vPdfByte
   * @return
   * @throws DocPrepException, FileNotFoundException, IOException
   */
  public Vector createPDFDocumentsFromBytes(Vector vPdfByte) throws DocPrepException, FileNotFoundException, IOException {
	  Vector vPdfFile = null;

	  if( vPdfByte != null ) {
		  int iCounter = 0;
		  String sPdfBaseFileName = DocumentGenerationManager.getInstance().getArchiveDir() + "/";

		  vPdfFile = new Vector();
		  Iterator it = vPdfByte.iterator();
		  while( it.hasNext()) {

			  // Generate file name for the current document
			  iCounter++;
			  File pdfFile = new File( sPdfBaseFileName + generateFileName(new Integer(iCounter)) + ".pdf" );
			  if( pdfFile.exists()) {
				  throw new DocPrepException("Unable to generate output PDF file. File already exists : " + pdfFile.getAbsolutePath());
			  }

			  logger.trace("DOCPREP: XSLMerger: Attempting to write PDF document: " + pdfFile.getAbsolutePath());

			  // Write the document to the disk
			  OutputStream pdfStream = new FileOutputStream(pdfFile);
			  pdfStream = new BufferedOutputStream(pdfStream);
			  try {
				  pdfStream.write((byte[])it.next());
			  }
			  catch(Exception ex) {
				  ex.printStackTrace();
				  String msg = "Could not write a pdf file to a disk." +ex;
				  throw new DocPrepException( msg );
			  }
			  finally {
				  pdfStream.close();
			  }
			  vPdfFile.add(pdfFile);
		  }
		  logger.trace("DOCPREP: XSLMerger: Finished writing PDF documents.");
	  }
	  return vPdfFile;
  }

  /**
   * Rel 3.1 Sasha
   * Returns Vector, which elements are byte arrays of generated documents, one document per template
   * from a DocumentProfile.documentTemplatePath field.
   *
   * @param data
   * @return
   * @throws DocPrepException
   */
  private Vector pGetPDFDocuments(String data) throws DocPrepException {
	  Vector vPDFs = new Vector();
	  Vector vTemplates = documentProfile.extractTemplates();

	  Iterator it = vTemplates.iterator();
	  while( it.hasNext()) {
		  String sTemplateName = (String)it.next();
		  if( sTemplateName == null || sTemplateName.trim().equals("") )
			  continue;

		  sTemplateName = mXslDirLocation + sTemplateName;
		  try {
			  logger.trace("DOCPREP: XSLMerger: Started creating PDF byte stream based on : " + sTemplateName);

			  //#DG650 byte [] pdfByteArray = (new XSL2PDFTransformer()).generateDocument(new File(sTemplateName), data);
			  byte [] pdfByteArray = generateDocument(new File(sTemplateName), data);
			  if( pdfByteArray != null) {
				  vPDFs.add(pdfByteArray);
			  }
		  }
		  catch( Exception ex) {
			  ex.printStackTrace();
			  throw new DocPrepException("Could not create pdf byte stream: " + ex);     //#DG650
		  }
	  }
	  return vPDFs;
  }

	/** #DG650 moved from 'XSL2PDFTransformer' class
	 * Generates a PDF document as a byte array
	 * 
	 * @param templateFile
	 * @param xmlString
	 * @return
	 * @throws DocPrepException
	 */
	protected byte[] generateDocument(File templateFile, String xmlString) throws DocPrepException {
		ByteArrayOutputStream outBytes = new ByteArrayOutputStream();
		
		try {
   	 	//* changes for fop 0.93 - DG 12/jan/2007
			Driver driver = new Driver();
			driver.setRenderer(Driver.RENDER_PDF);
			driver.setOutputStream(outBytes);
			
      //#DG650 rewritten 
			//TransformerFactory factory = TransformerFactory.newInstance();
			//Transformer transformer = factory.newTransformer(new StreamSource( templateFile ));
			Transformer transformer = initTranformer(templateFile);
			transformer.setParameter("versionParam", "2.0");
			
			//Setup input for XSLT transformation
			Source src = new StreamSource(new StringReader( xmlString ) );
			
			// Resulting SAX events (the generated FO) must be piped through to FOP
			Result res = new SAXResult( driver.getContentHandler());
			
			// Start XSLT transformation and FOP processing
			transformer.transform(src, res);
			transformer.clearParameters();
			//*/
			/* changes for fop 0.93 - DG 12/jan/2007
			FopFactory fopFactory = FopFactory.newInstance();	    
			Transformer transformer = initTranformer(templateFile);
			FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
			// configure foUserAgent as desired			
			// Construct fop with desired output format and output stream
			Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, outBytes);			
			Source xmlSource = new StreamSource(new StringReader( xmlString ) );
			// Resulting SAX events (the generated FO) must be piped through to FOP
			Result res = new SAXResult(fop.getDefaultHandler());
			
			// Start XSLT transformation and FOP processing
			transformer.transform(xmlSource, res);
			*/
			return outBytes.toByteArray();	        
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			String msg = "Exception in PDFproducer.GenerateDocument: "+ex;
			throw new DocPrepException( msg);
		}
	}

	//#DG650 not used - delete after Jan/2009
  /**
   * Creates a document through a 2 step process, using an FO file as
   * the first creation step.  This method first creates an FO file and then
   * uses external libraries to generate the end result file, which it returns.
   *
   * @param data the xml data to use for transformation
   * @param type the document type to create with the xml data
   *
   * @return the created document.
   * /
  public File createFODocument(String data, int type) throws Exception
  {
    File outputFile = null;

    try
    {
      PropertiesCache resources = PropertiesCache.getInstance();

      logger.trace("DOCPREP: XSLMerger: begin " + XSLMerger.getDocTypeExt(type)
         + " production.");

      String lFileLocation;

      String rootDir = ExpressDocprepHelper.getTemplatesFolder();

          if(rootDir.endsWith("/") || rootDir.endsWith("\\"))  
        	  lFileLocation = rootDir + documentProfile.getDocumentTemplatePath();  
           else  
        	   lFileLocation = rootDir + "/"+ documentProfile.getDocumentTemplatePath();
            
      File lTemplateFile = new File(lFileLocation);
      logger.trace("DOCPREP: XSLMerger: got file location: " +
         lTemplateFile.getAbsolutePath());

      try
      {
        //Source xmlSource = new StreamSource(data);
        Source xmlSource = new StreamSource(new StringReader(data));

        logger.trace("TRANSFORMATION STARTED");
        Transformer transformer = initTranformer(lTemplateFile);
        StringWriter swr = new StringWriter();
        logger.trace("Ready to call transform method.");
        transformer.transform(xmlSource, new StreamResult(swr)  );
        logger.trace("TRANSFORMATION ENDED");

        //===========================================================================
        // There comes one very dirty fix. We bumped into this issue in production
        // because it has not been tested at all in first place. If this was
        // tested it would have been discovered.
        //
        // What is the problem? Our sorce xml file contains '&' character and every
        // instance has been properly written as '&amp;'. The problem is that we
        // move data from this file into FO file through XSL transformation and
        // during this process software writes '&amp;' as '&' so when we enter
        // process of PDF rendering through FOP libraries, their parser complains
        // about this character.
        // What is fix? We will scan this string for every instance of '&' character
        // and replace it with '&amp;'. It will cure only one part of the problem
        // because we will not cover the problem with '<' and '>' and what is the worse
        // we can not do it at all because these characters are part of tags.
        //
        // Message posted by Zivko on March 31, 2003
        //===========================================================================
        // String foStr = swr.toString().trim();
        String foStr = replaceString(swr.toString().trim(),"&","&amp;");

        //  Archive the FO file
        try
        {
            logger.trace("DOCPREP: XSLMerger: archiving FO file.");
            DocumentGenerationManager.getInstance().archiveData(foStr, dealNumber, institutionNumber, documentProfile.getDocumentFullName(), ".fo");
            logger.trace("DOCPREP: XSLMerger: archiving FO file - finished.");
        }
        catch(Exception e)
        {
            logger.trace("DOCPREP: XSLMerger: Failed to write FO file - archive data: " +
               e);
        }


        //  Here is the place that you would add logic for other types of
        //  FO converters.  Right now, we only support XML/XSL->FO->PDF here.
        if (type == TYPE_PDF)
        {
            //  Extra creation step here required, because at this point, we simply
            //  have the created FO file.  We need to do another step to
            //  create the pdf file and return its contents.
            File dir = DocumentGenerationManager.getInstance().getArchiveDir();

            InputSource input = new InputSource(new StringReader(foStr));
            outputFile = File.createTempFile(documentProfile.getDocumentFullName(), ".pdf", dir);
            FileOutputStream output = new FileOutputStream(outputFile);

            //  Generate the pdf to the temp file
              Driver driver = new Driver(input, output);
              driver.setRenderer(Driver.RENDER_PDF);
              driver.run();
        }
        else if (type == TYPE_HTML)
        {
            //  HTML render code will go here
            //  (remove exception code of course)
            String msg = "HTML not supported yet!";
            logger.trace(msg);

            throw new Exception(msg);
        }
        else
        {
            String msg = "Unknown file type: " + type;
            logger.trace(msg);

            throw new Exception(msg);
        }
      }
      catch(Exception e)
      {
        logger.trace("An error occured while trying to render " +
           XSLMerger.getDocTypeExt(type) + ": [" + e+ "]");

        return null;
      }

      logger.trace("DOCPREP: XSLMerger: document created.");

      //  Archive the generated file
      try
      {
        logger.trace("DOCPREP: XSLMerger: archiving data file.");
        DocumentGenerationManager.getInstance().archiveData(outputFile, dealNumber, institutionNumber, 
          documentProfile.getDocumentFullName(), "." + getDocTypeExt(type));
        logger.trace("DOCPREP: XSLMerger: archiving data file - finished.");
      }
      catch(Exception e)
      {
        logger.trace("DOCPREP: XSLMerger: Failed to write " + getDocTypeExt(type) +
           " file - archive data" +e);
      }
    }
    catch (Exception exc)
    {
        exc.printStackTrace();
        logger.trace("DOCPREP: XSLMerger: "+exc);			//#DG650
    }
    return outputFile;
  }*/

  //2006-6-14 Dsun: Added new parameter String fileExt
  //private File createDocument(String pData, String pXSLTemplateName,Integer pOrdNumber) throws Exception
  private File createDocument(String pData, String pXSLTemplateName,Integer pOrdNumber, String fileExt) throws Exception {
	  
	  File outputFile = null;
	  try{
		  logger.trace("DOCPREP: XSLMerger: begin RTF production.");
		 	  
		  File lTemplateFile = ExpressDocprepHelper.getTemplateAsFile(pXSLTemplateName);
		  logger.trace("DOCPREP: XSLMerger: got file location: " + lTemplateFile.getAbsolutePath());
		  
		  if(DocumentGenerationManager.getInstance().getArchiveDir() == null){
			  logger.trace("Archive dir is null");
		  }
		  // #910 Catherine, 30Jan05, fix for BMO (document extension should be ".Doc")
//		   String rtfExt = PropertiesCache.getInstance().getProperty("com.basis100.alternate.rtf.extension", "rtf");
		  outputFile = new File(""+ DocumentGenerationManager.getInstance().getArchiveDir().getAbsolutePath() + "/" + generateFileName(pOrdNumber) + "." + fileExt);
		  // #910 end
		  logger.trace("DOCPREP: XSLMerger: output file name generated : " + outputFile.getAbsolutePath());
		  // make sure that we do not have this file already created on disk
		  if( outputFile.exists() ){
			  throw new DocPrepException("Unable to generate output file. File already exists.");
		  }
		  
		  try{
			  Source xmlSource = new StreamSource(new StringReader(pData));
			  
			  logger.trace("TRANSFORMATION STARTED");
        //#DG650 rewritten 
//        Source xslSource = new StreamSource(lTemplateFile);
//        TransformerFactory tFactory = TransformerFactory.newInstance();
//        Transformer transformer = tFactory.newTransformer(xslSource);
//        
//        StringWriter swr = new StringWriter();
//        logger.trace("Ready to call transform method.");
//        transformer.transform(xmlSource, new StreamResult(swr)  );
//        logger.trace("TRANSFORMATION ENDED");
//        retStr = swr.toString().trim();
//        FileWriter fwr = new FileWriter(outputFile);
//        fwr.write(retStr);
//        fwr.flush();
//        fwr.close();
			  
			  Transformer transformer = initTranformer(lTemplateFile);
			  
			  FileWriter fwr = new FileWriter(outputFile);
			  logger.trace("Ready to call transform method.");
			  transformer.transform(xmlSource, new StreamResult(fwr)  );
			  logger.trace("TRANSFORMATION ENDED");
			  fwr.close();
		  }
		  catch(Exception mergeException){
			  logger.trace("DOCPREP: XSLMerger@mergeException: "+mergeException);			//#DG650
		  }
		  
		  logger.trace("DOCPREP: XSLMerger: document created.");
	  } 
	  catch (Exception exc) {
		  exc.printStackTrace();
		  logger.trace("DOCPREP: XSLMerger: "+exc);			//#DG650
	  }
	  return outputFile;
  }

 	/**#DG650 
 	 * creates a transformer and sets its initial parameters 
 	 *  ( to be used inside the xsl template if needed)
 	 *  	
	 * @param transformer
	 * @throws TransformerConfigurationException 
	 */
	protected Transformer initTranformer(File templateFile) throws TransformerConfigurationException {
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(new StreamSource( templateFile ));
		transformer.setErrorListener(errListen);
		transformer.setParameter("srk", srk);
		transformer.setParameter("deal", deal);
		transformer.setParameter("documentProfile", documentProfile);
		/*transformer.setParameter("DocumentFullName", documentProfile.getDocumentFullName());
		transformer.setParameter("DocumentDescription", documentProfile.getDocumentDescription());
		transformer.setParameter("DocumentVersion", documentProfile.getDocumentVersion());
		transformer.setParameter("DocumentTemplatePath", documentProfile.getDocumentTemplatePath());
		transformer.setParameter("DocumentSchemaPath", documentProfile.getDocumentSchemaPath());
		transformer.setParameter("LanguagePreferenceId", new Integer(documentProfile.getLanguagePreferenceId()));
		transformer.setParameter("LenderProfileId", new Integer(documentProfile.getLenderProfileId()));
		transformer.setParameter("DocumentTypeId", new Integer(documentProfile.getDocumentTypeId()));
		transformer.setParameter("DocumentFormat", documentProfile.getDocumentFormat());
		transformer.setParameter("DocumentProfileStatus", new Integer(documentProfile.getDocumentProfileStatus()));
		transformer.setParameter("ReplyFlag", documentProfile.getReplyFlag());
		transformer.setParameter("DigestKey", documentProfile.getDigestKey());
		transformer.setParameter("MailDestinationTypeId", new Integer(documentProfile.getMailDestinationTypeId()));*/
		return transformer;
	}

  /**
   * Helper method to generate file name.
   */
  private String generateFileName(Integer pOrderNumber){
    logger.trace("Trying to generate file name.");
    //#DG650 rewritten with stringbuffer
    StringBuffer lName = new StringBuffer(documentProfile.getDocumentFullName());		
    if( pOrderNumber != null ){
      lName.append("_[").append(pOrderNumber.intValue()).append("]_");		
    }
    Date cd = new Date();
    String datestr = TypeConverter.isoDateFrom(cd);
    lName.append(datestr).append("_").append(dealNumber);

    logger.trace("Name generated is " + lName);
    return lName.toString();
  }

    public int getInstitutionNumber() {
        return institutionNumber;
    }
    
    public void setInstitutionNumber(int institutionNumber) {
        this.institutionNumber = institutionNumber;
    }

  //#DG650 used to log tranformation errors
  private ErrorListener errListen = new ErrorListener() {
    public void warning(TransformerException exc)
        throws TransformerException{
      String msg = exc.toString();
      System.err.println(msg);
      logger.warning(msg);
    }
    public void error(TransformerException exc)
        throws TransformerException{
      String msg = exc.toString();
      System.err.println(msg);
      logger.error(msg);
    }
    public void fatalError(TransformerException exc)
        throws TransformerException{
      String msg = "Fatal:"+exc.toString();
      System.err.println(msg);
      logger.error(msg);
    }
  };
}
