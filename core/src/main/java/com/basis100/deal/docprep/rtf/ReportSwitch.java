package com.basis100.deal.docprep.rtf;

public class ReportSwitch {
  private boolean status;
  private String name;

  public ReportSwitch() {
    status = false;
    name = null;
  }

  public void setName(String nameToSet){
    name = new String(nameToSet.trim().toLowerCase());
  }

  public String getName(){
    return name;
  }

  public void setStatus(boolean statusToSet){
    status = statusToSet;
  }

  public boolean isStatus(){
    return status;
  }

  public boolean isSwitchON(){
    if(status){return true;}
    return false;
  }

  public boolean isSwitchOFF(){
    if(status){return false;}
    return true;
  }

}
