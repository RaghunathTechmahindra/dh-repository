package com.basis100.deal.docprep.extract.doctag;

import MosSystem.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.entity.*;
import com.basis100.resources.*;


/**
 * Commitment
*/




public class CommitmentFeeAmount extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    double amt = 0.0;
    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.FEE){
        Fee oneFee = (Fee) de;
        fml.addCurrency(oneFee.getDefaultFeeAmount());
        return fml;
      }
      if(clsid == ClassId.DEALFEE)
      {
        DealFee dealFee = (DealFee)de;
        Fee fee = dealFee.getFee();
        int feeType = fee.getFeeTypeId();
        int feestat = dealFee.getFeeStatusId();

        Deal deal = new Deal(srk,null,dealFee.getDealId(),dealFee.getCopyId());
        int payor = deal.getMIPayorId();
        String uf = deal.getMIUpfront();

          if(feestat == Mc.FEE_STATUS_RECIEVED || feestat == Mc.FEE_STATUS_REQUIRED)
          {
            if( feeType == Mc.FEE_TYPE_CMHC_PREMIUM || feeType == Mc.FEE_TYPE_CMHC_FEE)
            {
              if(uf != null && payor == Mc.MI_PAYOR_BORROWER && uf.equalsIgnoreCase("N"));
              {
                amt = dealFee.getFeeAmount();
                fml.addCurrency(amt);
                return fml;
              }
            }
            else
            {
              amt = dealFee.getFeeAmount();
              fml.addCurrency(amt);
              return fml;
            }
          }

          if(payor == Mc.MI_PAYOR_BORROWER && deal.getMIPremiumAmount() > 0)
          {

            Property prop = new Property(srk,null);
            prop = prop.findByPrimaryProperty(deal.getDealId(),
                            deal.getCopyId(), deal.getInstitutionProfileId());
            int provid = -1;

            if(prop != null)
              provid = prop.getProvinceId();

            Province prov = new Province(srk,provid);

            double txrt = 0.0;

            if(prov != null)
             txrt = prov.getProvinceTaxRate();

            amt = deal.getMIPremiumAmount() * ( txrt / 100 );
            fml.addCurrency(amt);
            return fml;

          }
      }

     fml.addCurrency(amt);

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
