package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
/**
 *   if the recieved class is:<br>
 *   - Deal - get address info from primary borrower.<br>
 *   - Borrower - get address info from borrower current address.<br>
 *   - BorrowerAddress - get address info from BorrowerAddress.<br>
 *   - Property - get address info from Property.<br>
 *
 */

public class AORPropertyList extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());


    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.PROPERTY)
      {

        Property p = (Property)de;

        Deal parent = new Deal(srk,null,p.getDealId(),p.getCopyId());

        Collection all = parent.getProperties();

        if(all.size() <= 1)
        {
          fml.addValue(" ", fml.STRING);
          return fml;
        }

        String streetNum = p.getPropertyStreetNumber();
        String streetName = p.getPropertyStreetName();
        //String streetType = PicklistData.getDescription("StreetType",p.getStreetTypeId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetType = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType", p.getStreetTypeId(), lang);

        //String streetDir  = PicklistData.getDescription("StreetDirection",p.getStreetDirectionId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetDir = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection", p.getStreetDirectionId(), lang);

        // Added by BILLY to macth System Spec. -- 19March2001
        String addLine2 = p.getPropertyAddressLine2();
        String city = p.getPropertyCity();
        String province = p.getProvinceAbbreviation();
        String postalFSA = p.getPropertyPostalFSA();
        String postalLDU = p.getPropertyPostalLDU();

        String val = "";

        if(streetNum != null && streetNum.length() > 0)
          val += streetNum + format.space();
        if(streetName != null && streetName.length() > 0)
          val += streetName + format.space();
        if(streetType != null && streetType.length() > 0)
          val += streetType + format.space();
        if(streetDir != null && streetDir.length() > 0)
          val += streetDir + format.space();
        if(addLine2 != null && addLine2.length() > 0)
          val += addLine2 + format.space();
        if(city != null && city.length() > 0)
          val += city + format.space();
        if(province != null && province.length() > 0)
          val += province + "," + format.space();
        if(postalFSA != null && postalFSA.length() > 0)
          val += postalFSA + format.space();
        if(postalLDU != null && postalLDU.length() > 0)
          val += postalLDU + format.space();

        if(val.length() > 0) fml.addValue(val,fml.STRING);


      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
