package com.basis100.deal.docprep;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Date;
import MosSystem.Mc;

import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.DocumentTagExtractor;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.data.DealExtractor;
import com.basis100.deal.docprep.extract.data.XSLExtractor;
import com.basis100.deal.util.TypeConverter;
import com.basis100.deal.validation.BCBusinessRuleExecutor;
import com.basis100.deal.validation.ConditionBusinessRuleExecutor;
import com.basis100.deal.validation.PagelessBusinessRuleExecutor;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.conditions.BrokerApprovalConditionHandler;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.conditions.SysGenConditionExtractor;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.message.ClientMessage;
import com.basis100.util.message.ClientMessageHandler;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.xml.XmlToolKit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import com.basis100.deal.docprep.rtf.ReportCreator;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.Income;

import org.apache.fop.apps.Driver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;


public class TestDocGen
{

  public Deal getDeal(SessionResourceKit srk) throws Exception
  {


// approval
//return new Deal(srk,null, 1562,1);

return new Deal(srk,null, 1180,1 );
//XCEED
//return new Deal(srk,null, 83407,28 );

// DESJARDINS

// TD
//return new Deal(srk,null, 1180,27 );

//return new Deal(srk,null, 81,1);
  }

  public static void main(String[] args)throws Exception
  {
      TestDocGen tdg = new TestDocGen();
      System.out.println("Start:");
      tdg.testMerge();
      System.out.println("End:");


/*
    SessionResourceKit srk = null;
    try
    {
      TestDocGen tdg = new TestDocGen();
      System.out.println("Start:");

      srk = tdg.setup();

      System.out.println("Initialized - testing...");

      //tdg.testConditions(srk,tdg.getDeal(srk));
      tdg.testCCMCancel( srk,tdg.getDeal(srk) );

      System.out.println("End.");
    }
    catch(Exception ex)
    {
     srk.rollbackTransaction();
     ex.printStackTrace();
    }
*/
  }

  public void testXSLOutstandingCondMerge()throws Exception
  {
      System.out.println("started");
      FileWriter fwr ;

      File xmlInFile = new File("d:\\test\\zzz.xml");
      File xslInFile = new File("d:\\test\\outstandingcondCT.xsl");
      Source xmlSource = new StreamSource(xmlInFile);
      Source xslSource = new StreamSource(xslInFile);

      System.out.println("TRANSOFRMATION STARTED");
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformer = tFactory.newTransformer(xslSource);
      StringWriter swr = new StringWriter();
      transformer.transform(xmlSource, new StreamResult(swr)  );
      System.out.println("TRANSOFRMATION ENDED");

      fwr = new FileWriter("d:\\test\\OC.rtf");
      fwr.write(swr.toString().trim());
      fwr.flush();
      fwr.close();

      System.out.println("finished");

  }

  public void testMailFromSysLogger(SessionResourceKit srk) throws Exception
  {
    System.out.println("Started");
    srk.getSysLogger().mail("This is test message from the logger");
    System.out.println("Finished");
  }

  public void testMerge(){
  try{
    File xslInFile = new File("d:\\mos\\admin\\docgen\\templates\\XceedOutstandingCond.xsl");
    File xmlInFile = new File("f:\\tempderek\\conditions\\ Conditions Outstanding_20030915011114_20463.xml");
    //StringWriter swr = new StringWriter();

    Source xslSource = new StreamSource(xslInFile);
    Source xmlSource = new StreamSource(xmlInFile);

    System.out.println("TRANSOFRMATION STARTED");
    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer = tFactory.newTransformer(xslSource);
    StringWriter swr = new StringWriter();
    transformer.transform(xmlSource, new StreamResult(swr)  );
    System.out.println("TRANSOFRMATION ENDED");

    FileWriter fwr = new FileWriter("f:\\tempderek\\XD_COND_OUTSTENDING.rtf");
    fwr.write(swr.toString().trim());
    fwr.flush();
    fwr.close();

    System.out.println("finished");
  } catch(Exception exc){
    exc.printStackTrace();
    System.out.println("Here is the exception");
  }
  }

  public void testMergeTwoFiles() throws Exception{
      try{
        File xslFile = new File("d:\\mos\\admin\\docgen\\templates\\XceedSolicitorsPackage.xsl");
        File xmlFile = new File("f:\\temporary\\xceed\\ Solicitors Package Generic _20030929045321_80198.xml");
        //Source xmlSource = new StreamSource(data);
        Source xmlSource = new StreamSource(xmlFile);
        Source xslSource = new StreamSource(xslFile);

        System.out.println("TRANSFORMATION STARTED");
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer(xslSource);
        StringWriter swr = new StringWriter();
        System.out.println("Ready to call transform method.");
        transformer.transform(xmlSource, new StreamResult(swr)  );
        System.out.println("TRANSFORMATION ENDED");

        String retStr = swr.toString().trim();
        System.out.println(retStr);
      }
      catch(Exception mergeException) {
        mergeException.printStackTrace();
      }

  }

  public void testXSLMerger(String eName, SessionResourceKit srk, DealEntity de) throws Exception
  {
    if(de == null){
      Deal deal = this.getDeal(srk);
      de = (DealEntity) deal;
    }

    String extrStr;
    Class queryclass = Class.forName(eName);
    XSLExtractor tagquery = (XSLExtractor)queryclass.newInstance();
    DocumentFormat df = new DocumentFormat("rtf1");
    FileWriter fwr;

    System.out.println("Extraction started");
    //extrStr = tagquery.extractXMLData(de,0,df,srk);
    extrStr = tagquery.extractXMLData(de,0,df,srk);
    System.out.println("Extraction ended");

    fwr = new FileWriter("f:\\temporary\\zr_cond_outstanding.xml");
    fwr.write(extrStr);
    fwr.flush();
    fwr.close();

    Source xmlSource = new StreamSource(new StringReader(extrStr));
    //Source xmlSource = new StreamSource(new FileReader("f:\\temporary\\zr_wire.xml"));

    File xslInFile = new File("d:\\mos\\admin\\docgen\\templates\\outstandingcondBMO.xsl");
    Source xslSource = new StreamSource(xslInFile);

    System.out.println("TRANSOFRMATION STARTED");
    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer = tFactory.newTransformer(xslSource);
    StringWriter swr = new StringWriter();
    transformer.transform(xmlSource, new StreamResult(swr)  );
    System.out.println("TRANSOFRMATION ENDED");

    fwr = new FileWriter("f:\\temporary\\zr_cond_outstanding.rtf");
    fwr.write(swr.toString().trim());
    fwr.flush();
    fwr.close();

    System.out.println("finished");

  }

  public void testXSLDJMergerFromFile() throws Exception
  {

    String extrStr;

    FileWriter fwr;


    Source xmlSource = new StreamSource(new FileReader("f:\\temporary\\zr_out.xml"));

    File xslInFile = new File("d:\\mos\\admin\\docgen\\templates\\DJ_003.xsl");
    Source xslSource = new StreamSource(xslInFile);

    System.out.println("TRANSOFRMATION STARTED");
    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer = tFactory.newTransformer(xslSource);
    StringWriter swr = new StringWriter();
    transformer.transform(xmlSource, new StreamResult(swr)  );
    System.out.println("TRANSOFRMATION ENDED");

    fwr = new FileWriter("f:\\temporary\\zr_out.fo");
    fwr.write(swr.toString().trim());
    fwr.flush();
    fwr.close();

    FileReader fileReader = new FileReader("f:\\temporary\\zr_out.fo");
    InputSource input = new InputSource(fileReader);
    File outputFile = new File("f:\\temporary\\zr_out.pdf");
    FileOutputStream output = new FileOutputStream(outputFile);

    //  Generate the pdf to the temp file
    Driver driver = new Driver(input, output);
    driver.setRenderer(Driver.RENDER_PDF);
    driver.run();
    System.out.println("finished");

  }

  public void testXSLDJMerger(String eName, SessionResourceKit srk, DealEntity de, String pTemplateName, String pOutputName) throws Exception
  {
    if(de == null){
      Deal deal = this.getDeal(srk);
      de = (DealEntity) deal;
    }

    String extrStr;
    Class queryclass = Class.forName(eName);
    DealExtractor tagquery = (DealExtractor)queryclass.newInstance();
    DocumentFormat df = new DocumentFormat("rtf1");
    FileWriter fwr;

    System.out.println("Extraction started");
    //extrStr = tagquery.extractXMLData(de,0,df,srk);
    extrStr = tagquery.extractXMLData(de,1,df,srk);
    System.out.println("Extraction ended");

    Source xmlSource = new StreamSource(new StringReader(extrStr));

    FileWriter tmpWriter = new FileWriter("f:\\temporary\\zr_out.xml");
    tmpWriter.write(extrStr);
    tmpWriter.flush();
    tmpWriter.close();



   File xslInFile = new File(pTemplateName);
    //File xslInFile = new File("d:\\mos\\admin\\docgen\\templates\\DJ_refuse_f.xsl");
    Source xslSource = new StreamSource(xslInFile);

    System.out.println("TRANSOFRMATION STARTED");
    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer = tFactory.newTransformer(xslSource);
    StringWriter swr = new StringWriter();
    transformer.transform(xmlSource, new StreamResult(swr)  );
    System.out.println("TRANSOFRMATION ENDED");

    fwr = new FileWriter("f:\\temporary\\zr_out.fo");
    fwr.write(swr.toString().trim());
    fwr.flush();
    fwr.close();

    FileReader fileReader = new FileReader("f:\\temporary\\zr_out.fo");
    InputSource input = new InputSource(fileReader);
    File outputFile = new File(pOutputName);
    FileOutputStream output = new FileOutputStream(outputFile);

    //  Generate the pdf to the temp file
    Driver driver = new Driver(input, output);
    driver.setRenderer(Driver.RENDER_PDF);
    driver.run();
    System.out.println("finished");
 }
  public void testXSLDealExtractor(String pName, SessionResourceKit srk, DealEntity de) throws Exception
  {
    if(de == null){
      Deal deal = this.getDeal(srk);
      de = (DealEntity) deal;
    }
    try
    {
      FileWriter fwr ;

      String extrStr;
      Class queryclass = Class.forName(pName);
      DealExtractor tagquery = (DealExtractor)queryclass.newInstance();
      DocumentFormat df = new DocumentFormat("rtf1");

      System.out.println("Extraction started");
      extrStr = tagquery.extractXMLData(de,1,df,srk);
      //extrStr = tagquery.extractXMLData(de,1,df,srk);
      System.out.println(extrStr);
      System.out.println("Extraction ended");

      fwr = new FileWriter("f:\\downloads\\zzz.xml");
      fwr.write(extrStr);
      fwr.flush();
      fwr.close();

      System.out.println("finished");
    }
    catch(Exception cnf)
    {
       srk.getSysLogger().debug("DOCPREP: QUERY ERROR: Type: code Location: " + pName);
       System.out.println("DOCPREP: QUERY ERROR: Type: code Location: " + pName);
    }
  }

  public void testXSLExtractor(String eName, SessionResourceKit srk, DealEntity de) throws Exception
  {

    if(de == null){
      Deal deal = this.getDeal(srk);
      de = (DealEntity) deal;
    }
    try
    {
      FileWriter fwr ;

      String extrStr;
      Class queryclass = Class.forName(eName);
      XSLExtractor tagquery = (XSLExtractor)queryclass.newInstance();
      DocumentFormat df = new DocumentFormat("rtf1");

      System.out.println("Extraction started");
      extrStr = tagquery.extractXMLData(de,0,df,srk);
      //extrStr = tagquery.extractXMLData(de,1,df,srk);
      System.out.println("Extraction ended");



      fwr = new FileWriter("f:\\temporary\\DATA.xml");
      fwr.write(extrStr);
      fwr.flush();
      fwr.close();


      File xmlInFile = new File("f:\\temporary\\DATA.xml");
      File xslInFile = new File("d:\\mos\\admin\\docgen\\templates\\XceedMCAPDataInputSheet.xsl");
      Source xmlSource = new StreamSource(xmlInFile);
      Source xslSource = new StreamSource(xslInFile);

      System.out.println("TRANSOFRMATION STARTED");
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformer = tFactory.newTransformer(xslSource);
      StringWriter swr = new StringWriter();
      transformer.transform(xmlSource, new StreamResult(swr)  );
      System.out.println("TRANSOFRMATION ENDED");

      fwr = new FileWriter("f:\\temporary\\DATA.rtf");
      fwr.write(swr.toString().trim());
      fwr.flush();
      fwr.close();

//===============================================================================================================
      System.out.println("finished");
    }
    catch(Exception cnf)
    {
      cnf.printStackTrace();
       srk.getSysLogger().debug("DOCPREP: QUERY ERROR: Type: code Location: " + eName);
       System.out.println("DOCPREP: QUERY ERROR: Type: code Location: " + eName);
    }
  }

  public void testConditionHandler(SessionResourceKit srk){

    try{
      Deal deal = this.getDeal(srk);
      DealPK pk = new DealPK(deal.getDealId(), deal.getCopyId());
      ConditionHandler handler = new ConditionHandler(srk);
      handler.buildSystemGeneratedDocumentTracking(pk);
    }catch(Exception exc){
      System.out.println("Shit exception");
    }
  }

  public void testBrokerApprovalConditionHandler(SessionResourceKit srk) throws Exception
  {
    Deal deal = this.getDeal(srk);
    DocumentFormat df = new DocumentFormat("rtf1");
    Document doc = XmlToolKit.getInstance().newDocument();
    Node elemNode = doc.createElement("something");
    BrokerApprovalConditionHandler bach = new BrokerApprovalConditionHandler(srk);
    bach.buildBrokerApprovalConditionNodes(deal,doc,elemNode, df);
  }

  public void testOneConditionGen(String genName, SessionResourceKit srk, DealEntity de) throws Exception
  {
    FMLQueryResult fml = new FMLQueryResult();
    if(de == null){
      Deal deal = this.getDeal(srk);
      de = (DealEntity) deal;
    }
    try
    {
      Class queryclass = Class.forName(genName);
      SysGenConditionExtractor tagquery = (SysGenConditionExtractor)queryclass.newInstance();

      fml = tagquery.extract(de, srk);
    }
    catch(Exception cnf)
    {
       srk.getSysLogger().debug("DOCPREP: QUERY ERROR: Type: code Location: " + genName);
       System.out.println("DOCPREP: QUERY ERROR: Type: code Location: " + genName);
       fml.addValue("QUERY ERROR!", fml.STRING);
    }

    //return fml;

  }


  public void testOneExtractor(String extractorName, SessionResourceKit srk, DealEntity de) throws Exception
  {
    FMLQueryResult fml = new FMLQueryResult();
    if(de == null){
      Deal deal = this.getDeal(srk);
      de = (DealEntity) deal;
    }
    try
    {
      Class queryclass = Class.forName(extractorName);
      DocumentTagExtractor tagquery = (DocumentTagExtractor)queryclass.newInstance();
      DocumentFormat df = new DocumentFormat("rtf1");

      //fml = tagquery.extract(de, 0, null, srk);
      fml = tagquery.extract(de, 0, df, srk);
    }
    catch(Exception cnf)
    {
       srk.getSysLogger().debug("DOCPREP: QUERY ERROR: Type: code Location: " + extractorName);
       System.out.println("DOCPREP: QUERY ERROR: Type: code Location: " + extractorName);
       fml.addValue("QUERY ERROR!", fml.STRING);
    }

    //return fml;

  }

  public void testXMLParser(){
    try{
      File file = new File("d:\\tempdavid\\s_INCM037-00309-F_447018.failed");
      DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();
      Document dom =  parser.parse(file);
    } catch(Exception exc){
      System.out.println("I am now in exception");
      System.out.println(exc.getMessage());
    }

  }

  public SessionResourceKit setup()throws Exception
  {
    System.out.println("Init start...");
    PropertiesCache.addPropertiesFile("d:\\mos\\admin\\mossys.properties");
    ResourceManager.init("d:\\mos\\admin\\");
//    ResourceManager.init("d:\\mos\\admin\\",PropertiesCache.getInstance().getProperty("com.basis100.resource.connectionpoolname"));
    SessionResourceKit srk = new SessionResourceKit("3790");
    System.out.println("Init end.");
    return srk;
  }

  public void testConditionHandler(SessionResourceKit srk, Deal d)throws Exception
  {
    System.out.println("test ConditionHandler");
   // srk.beginTransaction();
    if(d == null)
     d = this.getDeal(srk);

  //   d.setAdvanceHold(1.1);

      //Property p = new Property(srk,null,7211,d.getCopyId());
      //p.setOccupancyTypeId(2);
     // p.ejbStore();

   // Borrower b = new Borrower(srk, null, 11069,1);

  //  d.ejbStore();
   // this.createIncAndEH(srk, b);
  //  srk.commitTransaction();
    srk.beginTransaction();

    ConditionHandler handler = new ConditionHandler(srk);
    handler.updateForApproval(d);

    System.out.println("Build sys gen conds:");
    handler.buildSystemGeneratedDocumentTracking((DealPK)d.getPk()) ;

/*

    System.out.println("Retrieve commitment conds:" );

    Collection all = handler.retrieveCommitmentLetterDocTracking(d,0);

    Iterator it = all.iterator();

    while(it.hasNext())
    {
      DocumentTracking dt = (DocumentTracking)it.next();

      System.out.println(dt.getDocumentLabel(0));
      System.out.println("--------------------------------");
      System.out.println(dt.getDocumentTextByLanguage(0));

    }

    srk.commitTransaction();
    System.out.println("test ConditionHandler end");
*/
  }

  public void testConditionsOutstanding(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Deal Summary Lite");

        if(d == null)
         d = this.getDeal(srk);
      DocumentRequest.requestConditionsOutstanding(srk,d);
      //DocumentRequest.requestDealSummaryLite(srk,d);
      System.out.println("Request DealSummaryLite Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testDealSummaryLite(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Deal Summary Lite");

        if(d == null)
         d = this.getDeal(srk);

      DocumentRequest.requestDealSummaryLite(srk,d);
      System.out.println("Request DealSummaryLite Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }


  public void testSummary(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Summary");

        if(d == null)
         d = this.getDeal(srk);

      DocumentRequest.requestDealSummary(srk,d);
      System.out.println("Request Summary Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

   public void testDisbursment(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      if(d== null){
        d= this.getDeal(srk);
      }
      srk.beginTransaction();
      System.out.println("Request Disbursment");
      DocumentRequest.requestDisbursmentLetter(srk,d);
      System.out.println("Request Disbursment Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      e.printStackTrace();
      srk.rollbackTransaction();
    }

  }


   public void testCommit(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Commit");
      //DocumentRequest.requestCommitment(srk,new Deal(srk,null,444,88));
      //DocumentRequest.requestCommitment(srk,new Deal(srk,null,301,53));
      DocumentRequest.requestCommitment(srk,new Deal(srk,null,1180,1));
      System.out.println("Request Commit Complete");
      srk.commitTransaction();

/*
      srk.beginTransaction();
     System.out.println("Request Commit");
      if(d == null) d = getDeal(srk);
      /*


      ch.updateForApproval(d);
      //createDealfee(srk, d);

      d.ejbStore();

      DocumentRequest.requestCommitment(srk,d);
      System.out.println("Request Commit Complete");   */

/*
      ConditionHandler ch = new ConditionHandler(srk);
      ch.buildSystemGeneratedDocumentTracking((DealPK)d.getPk());

      CCM ccm = new CCM(srk);
      ccm.issueCommitment(d);
      System.out.println("Request Commit");

      srk.commitTransaction();
*/
    }
    catch(Exception e)
    {
      e.printStackTrace();
      srk.rollbackTransaction();
    }

  }

  public void testFOProduction() throws Exception
  {
/*
    XSLMerger merger = new XSLMerger(null);
    FileReader fr = new FileReader("f:\\tempmove\\ Commitment Letter Generic _20030331035314_9957.fo");
    LineNumberReader lnr = new LineNumberReader(fr);
    StringWriter swr = new StringWriter();

    String line;
    while( (line = lnr.readLine()) != null ){
      swr.write(line);
    }
    swr.flush();
    String fileAsStr = swr.toString();
    swr.close();
    lnr.close();
    fr.close();
    String outStr = merger.replaceString(fileAsStr,"&","&amp;");
    FileWriter fwr = new FileWriter("f:\\downloads\\zzz.xml");
    fwr.write(outStr);
    fwr.flush();
    fwr.close();
*/
  }

   public void testMultiCommit(SessionResourceKit srk)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Commit");
      DocumentRequest.requestCommitment(srk,new Deal(srk,null,6932,1));
      DocumentRequest.requestCommitment(srk,new Deal(srk,null,7211,1));
      DocumentRequest.requestCommitment(srk,new Deal(srk,null,7275,1));
      DocumentRequest.requestCommitment(srk,new Deal(srk,null,7276,1));
      System.out.println("Request Commit Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testSolicitors(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Solicitors");
      if(d == null) {
          d = getDeal(srk);
      }

      DocumentRequest.requestSolicitorsPackage(srk,d);
      System.out.println("Request Solicitors Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testBridge(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Bridge");
      if(d == null) d = getDeal(srk);

      DocumentRequest.requestBridgePackage(srk,d);
      System.out.println("Request Bridge Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testPreApp(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request PreAppCert");

      if(d == null) d = getDeal(srk);

      ConditionHandler ch = new ConditionHandler(srk);
      ch.updateForApproval(d);


      DocumentRequest.requestPreAppCertificate(srk,d);
      System.out.println("Request PreAppCert Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testTransferPackage(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Transfer Package");

      if(d == null) d = getDeal(srk);

      DocumentRequest.requestTransferPackage(srk,d);
      System.out.println("Request Transfer Package Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }
  }


  public void testUpload(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Upload");

      if(d == null) d = getDeal(srk);

      DocumentRequest.requestUpload(srk,d);
      System.out.println("Request Upload Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testUploadBuilder(SessionResourceKit srk) throws Exception
  {

  }

  /*
  public void testFBCAppraisal(SessionResourceKit srk, Deal d) throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request FBCAppraisal");

      if(d == null) d = getDeal(srk);

     DocumentRequest.requestFBCAppraisal(srk,d);

      System.out.println("Request FBCAppraisal Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }
  }

*/
/*
  public void testNASAppraisal(SessionResourceKit srk, Deal d) throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request NASAppraisal");

      if(d == null) d = getDeal(srk);

     DocumentRequest.requestNASAppraisal(srk,d);

      System.out.println("Request NASAppraisal Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }
  }
*/
/*
  public void testEquiFax(SessionResourceKit srk, Deal d) throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request Equifax");

      if(d == null) d = getDeal(srk);

      DocumentRequest.requestEquifax(srk,d);

      System.out.println("Request Equifax Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }
  }
*/
  public void testEzenet(SessionResourceKit srk, Deal d) throws Exception
  {
       System.out.println("Ezenet requested...entered");
       if(d == null){
         d = getDeal(srk);
       }
       DocumentRequest.requestEzenet(srk,d);
       System.out.println("Ezenet requested...exited");
       srk.commitTransaction();
  }

  public void testPendingMessage(SessionResourceKit srk, Deal d) throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request approval");

      if(d == null) d = getDeal(srk);

      //ConditionHandler h = new ConditionHandler(srk);
      //h.buildSystemGeneratedDocumentTracking((DealPK)d.getPk());
      //System.out.println("Built sysgen doctracking before approval ");
      DocumentRequest.requestPendingMessage(srk,d);

      System.out.println("Request approval Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }


  public void testTDCTCapLink(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request approval");

      if(d == null) d = getDeal(srk);

      //ConditionHandler h = new ConditionHandler(srk);
      //h.buildSystemGeneratedDocumentTracking((DealPK)d.getPk());
      //System.out.println("Built sysgen doctracking before approval ");
      DocumentRequest.requestTDCTCapLink(srk,d);

      System.out.println("requestTDCTCapLink Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testApproval(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request approval");

      if(d == null) d = getDeal(srk);

      //ConditionHandler h = new ConditionHandler(srk);
      //h.buildSystemGeneratedDocumentTracking((DealPK)d.getPk());
      //System.out.println("Built sysgen doctracking before approval ");
      DocumentRequest.requestApproval(srk,d);

      System.out.println("Request approval Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

/*  public void testApprovalAndCommit(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request approval");

      if(d == null) d = getDeal(srk);

      ConditionHandler h = new ConditionHandler(srk);

      h.buildSystemGeneratedDocumentTracking((DealPK)d.getPk());
      System.out.println("Built sysgen doctracking before approval ");
      DocumentRequest.requestApproval(srk,d);


      System.out.println("Request approval Complete");

      System.out.println("Request Commit");
      if(d == null) d = getDeal(srk);


      ConditionHandler ch = new ConditionHandler(srk);
      ch.buildSystemGeneratedDocumentTracking((DealPK)d.getPk());
      ch.updateForApproval(d);
      //createDealfee(srk, d);

      d.ejbStore();

      DocumentRequest.requestCommitment(srk,d);
      System.out.println("Request Commit Complete");



      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  } */

  public void testDenial(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("Request denial");

      if(d == null) d = getDeal(srk);
      DocumentRequest.requestDenial(srk,d);

      System.out.println("Request denial Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }



  public void testRTFCreator() throws Exception
  {

    File templateFile = new File("c:\\testrtf\\templates\\solicitorsPackageCT.xml");
    File dataFile = new File("c:\\testrtf\\data\\Solpak.xml");
    File outputFile = new File("c:\\testrtf\\output\\solicitorZZZ.rtf");

// commitment letter
   // File templateFile = new File("c:\\testrtf\\templates\\commitmentCT.xml");
   // File dataFile = new File("c:\\testrtf\\data\\CLG.xml");
   // File outputFile = new File("c:\\testrtf\\output\\CT.rtf");

    FileReader fr = new FileReader(dataFile);
    LineNumberReader lnr = new LineNumberReader(fr);
    String line;
    StringBuffer dataBuf = new StringBuffer("");
    String dataStr;
    while( (line = lnr.readLine()) != null  ){
      dataBuf.append(line);
    }
    dataStr = dataBuf.toString();

    ReportCreator lReportCreator = new ReportCreator();
    StringBuffer retBuff = lReportCreator.makeRTFReport(dataStr,templateFile);

    FileWriter fwr = new FileWriter(outputFile);
    fwr.write(retBuff.toString());
    fwr.flush();
    fwr.close();
    System.out.println("finished");
  }

  public void testCCMCancel(SessionResourceKit srk, Deal d) throws Exception
  {
    srk.beginTransaction();
    CCM ccm = new CCM(srk);
    ccm.cancelCommitment(d,true);
    srk.commitTransaction();
  }

  public void testMIRequest(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("request MI");

      if(d == null) d = getDeal(srk);
      try
      {

        d.setMITypeId(Mc.MI_TYPE_FULL_SERVICE);
        d.setMIPayorId(Mc.MI_PAYOR_BORROWER);

        MIProcessHandler mi = MIProcessHandler.getInstance();
        mi.updateMIAttributes(d,srk,false);
      srk.commitTransaction();
      srk.beginTransaction();
        int rcm = mi.determineRcmCommStatus(d,srk);
      srk.commitTransaction();

        boolean notMIApprovalStatus = false == mi.isMIApprovalStatus(d.getMIStatusId());

        /*if (notMIApprovalStatus &&
            (
              rcm == mi.C_I_INITIAL_REQUEST_PENDING ||
              rcm == mi.C_I_ERRORS_CORRECTED_AND_PENDING ||
              rcm == mi.C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M ||
              rcm == mi.C_I_CHANGES_PENDING
            )
           )
        { */
          srk.beginTransaction();
           mi.placeMIRequest(d, srk, rcm);
          srk.commitTransaction();
        //}
      }
      catch(Exception e)
      {
         e.printStackTrace();
         throw new Exception();
      }

      srk.beginTransaction();
      d.ejbStore();

      System.out.println("Request MI Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void testConditionParser(SessionResourceKit srk, Deal d) throws Exception
  {
    if(d == null) d = getDeal(srk);
    ConditionHandler h = new ConditionHandler(srk);
    String something = h.getVariableVerbiage(d,53,0);
    System.out.println("Something is :" + something);
  }

  public void testConditions(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("start");

      if(d == null) d = getDeal(srk);

      //Property p = new Property(srk,null,7211,d.getCopyId());
      //p.setOccupancyTypeId(2);
     // p.ejbStore();

      d.ejbStore();

      ConditionHandler h = new ConditionHandler(srk);

      h.buildSystemGeneratedDocumentTracking((DealPK)d.getPk());

      System.out.println("Built sysgen doctracking before approval ");
      System.out.println("Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }


   public void testDT(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("make dt");

      if(d == null) d = getDeal(srk);


      DocumentTracking dt = new DocumentTracking(srk,943,1);


      System.out.println("make dt Complete");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.rollbackTransaction();
    }

  }

  public void createDealfee(SessionResourceKit srk, Deal d)throws Exception
  {
    try
    {
      srk.beginTransaction();
      System.out.println("do stuff");

      if(d == null) d = getDeal(srk);

      DealFee fee = new DealFee(srk,null);
      fee.create((DealPK)d.getPk());
      fee.setFeeAmount(9000.99);
      fee.setFeePaymentDate(new Date());
      fee.setFeePaymentMethodId(3);
      fee.setFeeId(13);
      fee.ejbStore();
      srk.commitTransaction();
      System.out.println("end stuff");
    }
    catch(Exception e)
    {
      e.printStackTrace();
      srk.rollbackTransaction();
    }

  }

  public void testPagelessBRExecutor(SessionResourceKit srk){

    String rulePrefix = null;
    PassiveMessage pm = new PassiveMessage();


    try
    {
      Deal d = this.getDeal(srk);
      DealPK dpk = (DealPK) d.getPk();

      PagelessBusinessRuleExecutor bre = new PagelessBusinessRuleExecutor();
      
      // chagend for ML from pm = bre.validate(dpk,srk,pm,"DCP_254");
      pm = bre.validate(d,srk,pm,"DCP_254");

    }
    catch (Exception e)
    {
      System.out.println("Evo me u exception");
    }
    if( pm.getExceptionEncountered() ){
      System.out.println("Exception encountered");
    }
  }

  public void testBCBusinessRuleExecutor(SessionResourceKit srk){
    try{
      Deal d = this.getDeal(srk);
      BCBusinessRuleExecutor cbre = new BCBusinessRuleExecutor(srk);

      Collection condVect = cbre.extractConditions(d);
      System.out.println("Finished");
    }catch(Exception exc){
      System.out.println("I am in exception now");
    }
  }

  public void testConditionBusinessRuleExecutor(SessionResourceKit srk){
    try{
      Deal d = this.getDeal(srk);
      ConditionBusinessRuleExecutor cbre = new ConditionBusinessRuleExecutor(srk);

      cbre.extractConditions(d);
    }catch(Exception exc){
      System.out.println("I am in exception now");
    }
  }
/*
  public void testDocumentGeneration(SessionResourceKit srk) throws Exception
  {
    try{
      Deal d = this.getDeal(srk);
      DocumentQueue queue = new DocumentQueue(srk);
      queue.setDealId(d.getDealId());
      queue.setDealCopyId(d.getCopyId());
      queue.setDocumentFormat("xml-upload");
      queue.setDocumentVersion("MI Request");
      queue.setDocumentTypeId(9);
      queue.setLenderProfileId(0);
      queue.setLanguagePreferenceId(0);
      queue.setTimeRequested(new Date());

      DocumentGenerationHandler dgh = new DocumentGenerationHandler(queue);
      dgh.start();
    }catch(Exception exc){
      System.out.println("I am in exception");
    }
  }
*/
  public void createIncAndEH(SessionResourceKit srk, Borrower b)throws Exception
  {
    try
    {

      System.out.println("do create inc and eh");


      EmploymentHistory eh = new EmploymentHistory(srk);
      eh.create((BorrowerPK)b.getPk());
      Income i = new Income(srk,null);
      i.create((EmploymentHistoryPK)eh.getPk());

      i.setIncomeAmount(50000);
      i.setIncomeTypeId(Mc.INCOME_COMMISSION);

      eh.setEmploymentHistoryStatusId(Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT);

      i.ejbStore();
      eh.ejbStore();

      System.out.println("end");
    }
    catch(Exception e)
    {
      e.printStackTrace();
      srk.rollbackTransaction();
    }

  }

  public void extractLogData()
  {
    try{
      File input = new File("d:\\temp\\main.log");

      FileReader fr;
      LineNumberReader lnr;

      fr = new FileReader(input);
      lnr = new LineNumberReader(fr);
      String line;
      String process;
      FileWriter fwr = new FileWriter("d:\\temp\\main.txt");
      while( (line = lnr.readLine()) != null){
        process = line.toLowerCase();
        if(process.indexOf("dealfee") != -1){
          fwr.write(process + "\n");
        }
      }
      fwr.flush();
      fwr.close();

      lnr.close();
      fr.close();
      System.out.println("finished");
    } catch(Exception exc){
      System.out.println("I am in exception now");
    }
  }

  public void testMessageModule(SessionResourceKit srk){
    for(int i=100; i<109; i++){
    try{
      ClientMessage msg = ClientMessageHandler.getInstance().findMessage(srk,i,0);
      System.out.println("MESSAGE:" + i +"**"+ msg.getMessageId()+"::" + msg.getMessageText());
    }catch(Exception exc){
      System.out.println("Exception");
    }
    }

    try{
      ClientMessage msg = ClientMessageHandler.getInstance().findMessage(srk,107,0);
      System.out.println("MESSAGE:" + 104 +"**"+ msg.getMessageId()+"::" + msg.getMessageText());
    }catch(Exception exc){
      System.out.println("Exception");
    }

  }


  public void testReportCreator()
  {
    try{
      FileReader fr = new FileReader("f:\\tempbilly\\s_INCM055-00170-F_674004.deal");
      //FileReader fr = new FileReader("f:\\tempbilly\\s_INCM055-00170-F_674000.xml");
      LineNumberReader lnr = new LineNumberReader(fr);
      String line;
      StringBuffer sb = new StringBuffer();
      while( (line = lnr.readLine() ) != null){
        //sb.append(line).append("\n");
        sb.append(line);
      }
      ReportCreator rc = new ReportCreator();
      String ps = sb.toString();
      if(ps.indexOf("#xc") == -1){
        System.out.println("not found");
      }else {
        System.out.println("found:"+ ps.indexOf("#xc"));
      }
      rc.makeRTFReport(ps, null);
    } catch(Exception exc){
      exc.printStackTrace();
    }
  }

  public void testInteger(){
    //String pStr = "929.022668153103";
    String pStr = "929";
    try{
      int lVal = Integer.parseInt(pStr);
    }catch(Exception exc){
      System.out.println("In Exception");
      exc.printStackTrace();
    }
  }

  public void findMatchingBracket(){
    try {
      FileInputStream fis = new FileInputStream("f:\\temporary\\scheldon\\Deal Summary Generic.rtf");
      BufferedInputStream bis = new BufferedInputStream(fis);
      int cnt = 0;
      int b;
      int countOpen = 0;
      int countClosed = 0;
      java.util.Vector vect = new java.util.Vector();
      while ((b = bis.read()) != -1) {
        //if (b == '\n')
        //  cnt++;
        cnt++;
        if(b == '{'){
          vect.addElement(new Integer(cnt));
          countOpen++;
        }else if(b == '}'){
          vect.remove(0);
          countClosed++;
        }
        System.out.println("->"+ cnt);
      }
      bis.close();
      fis.close();
      System.out.println("Not closed bracket is:"+((Integer)vect.elementAt(0)).intValue());
      System.out.println("OPEN   :"+ countOpen);
      System.out.println("CLOSED :"+ countClosed);
    }
    catch (IOException e) {
      System.err.println(e);
    }

  }

  public void testDocumentRequest(SessionResourceKit srk){
    try{
      Deal d = getDeal(srk);

     // DocumentRequest.requestCommitment(srk,d);
     //DocumentRequest.requestInstructionToAppraiser(srk,d);
     DocumentRequest.requestInstructionToAppraiser(srk,d);

    }catch(Exception exc){
      System.out.println("I am in exception now....");
    }
  }

  public void testDropChar(){
    String myStr = "1970-06-12T00:00:00";
    Date d = TypeConverter.dateFrom(myStr);
    System.out.println("" + d.getTime());
  }

}

