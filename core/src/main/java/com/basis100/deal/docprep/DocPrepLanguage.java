package com.basis100.deal.docprep;

/**
 * 21/Jun/2005 DVG #DG232 #1659  Express Standard solicitorís package (fixes)
 */

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;


import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.picklist.*;

import java.util.*;
import java.text.*;

/**
 *  A singleton that provides language conversion services for the DocGen module.
 */

public class DocPrepLanguage
{

  private static final String DATEFORMAT = "d MMMM yyyy";
  private static final String DJDATETIMEFORMAT = "yyyy-MM-dd HH:mm:ss"; // Windows regional settings for French(Canada)

  public static final int ENGLISH = 0;
  public static final int FRENCH  = 1;
  public static final int SPANISH = 2;

  private static int NUMBER    = 0;
  private static int CURRENCY  = 1;
  private static int PERCENT   = 2;
  private static int RATIO     = 3;
  private static int CURRENCY_WITHOUT_DOLLAR     =4;
  // Added Constant for formatting Interest Rate without percentage symbol - TAR # 1612
  private static int INTEREST_WITHOUT_PERCENTAGE	 =5;

  private Locale french_ca;
  private Locale french_fr;
  private Locale english;
  private Locale spanish;

  public PropertiesCache pCache;

  private static DocPrepLanguage instance;

  private Map bundleMap;

  private DocPrepLanguage()
  {
    french_ca = new Locale("fr", "");
    french_fr = new Locale("fr", "");
    english = new Locale("en", "CA");
    spanish = new Locale("es", "US");
    bundleMap = new HashMap();

  }

  //this class caches resource bundle data so - singleton.
  public static synchronized DocPrepLanguage getInstance()
  {
    if(instance == null)
     instance = new DocPrepLanguage();

     return instance;
  }

  /**
  * Finds the preferred language based the preference of the the primary

  *  <code>Borrower</code>.

  * @param   Deal -  the Borrower parent.

  * @param   srk

  * @return an int representing the languagePreferenceId

  */
  private int findPreferredLang(Deal deal, SessionResourceKit srk)
  {
    try
    {
        if(deal == null) return -1;

        Borrower primary = new Borrower(srk,null);
        primary = primary.findByPrimaryBorrower(deal.getDealId(),deal.getCopyId());

        if(primary != null)
        {
          return primary.getLanguagePreferenceId();
        }
    }
    catch(Exception e)
    {
      String msg = "DOCPREP: INFO: Unable to determine preferred language - no primary borrower found";
      srk.getSysLogger().info(msg + ":where clause: " + deal.getPk().getWhereClause());
    }

    return -1;
  }

  public int findPreferredLanguageId(DealEntity de, SessionResourceKit srk)
  {
    try
    {

      int clsid = de.getClassId();

      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        return findPreferredLang(deal,srk);
      }
      else if(clsid == ClassId.DEALFEE)
      {
        DealFee df = (DealFee)de;
        Deal deal = new Deal(srk,null,df.getDealId(),df.getCopyId());
        return findPreferredLang(deal,srk);
      }
    }
    catch(Exception e)
    {
      String msg = "DOCPREP: INFO: Unable to determine preferred language - no primary borrower found";
      srk.getSysLogger().info(msg + ":: " + de.getPk());
    }

    return -1;
  }

  /**
  * gets 'Equivalent' French String for the supplied English Term as contained

  * in the translation properties file.

  *

  * @param   English -  the English

  * @return  the appropriate French language String or if none is found return null

  */
  public String getTerm(String key, int lang)
  {
	  
	  return BXResources.getGenericMsg(key, lang);

  }


  /**
  *

  * @return a String representing the int parameter formatted as per the languagePreferenceId lang.

  */
  public String getFormattedNumber(int n ,int lang)
  {
    return getFormattedNumber((double)n ,lang);
  }

  /**
  *

  * @return a String representing the long parameter formatted as per the languagePreferenceId lang.

  */
  public String getFormattedNumber(long n ,int lang)
  {
    return getFormattedNumber((double)n ,lang);
  }

  /**
  *

  * @return a String representing the double parameter formatted as per the languagePreferenceId lang.

  */
  public String getFormattedNumber(double d ,int lang)
  {
    Double amount = new Double(d);
    NumberFormat numberFormatter;
    numberFormatter = getNumberFormatter(lang,NUMBER);
    return numberFormatter.format(amount);
  }
   /**
  *

  * @return a NumberFormat object initialize per specification and the languagePreferenceId lang.

  */
  private NumberFormat getNumberFormatter(int lang, int type)
  {
    NumberFormat frForm = null;

    switch(lang)
    {
     case ENGLISH :
        if(type == NUMBER)
          frForm = NumberFormat.getNumberInstance(english);
        else if(type == PERCENT)
        {
          frForm = NumberFormat.getNumberInstance(english);
          DecimalFormat df = (DecimalFormat)frForm;
          df.applyPattern("###,##0.000'%'");
        }
        else if(type == RATIO)
        {
          frForm = NumberFormat.getNumberInstance(english);
          DecimalFormat df = (DecimalFormat)frForm;
          df.applyPattern("###,##0.00'%'");
        }
        else if(type == CURRENCY)
          //frForm = NumberFormat.getCurrencyInstance(english);
          //#DG232 frForm = new DecimalFormat("$#,##0.00 ;($#,##0.00)", new DecimalFormatSymbols(english));
          //#LEN217095 frForm = new DecimalFormat("#,##0.00;($#,##0.00)", new DecimalFormatSymbols(english)); by Anna Ai    
          frForm = new DecimalFormat("$#,##0.00;($#,##0.00)", new DecimalFormatSymbols(english));  
		else if(type == CURRENCY_WITHOUT_DOLLAR)
		  frForm = new DecimalFormat("#,##0.00;($#,##0.00)", new DecimalFormatSymbols(english));
		else if (type == INTEREST_WITHOUT_PERCENTAGE) {
			// TAR : 1612 - Formatting Interest Rate without '%' symbol
	          frForm = NumberFormat.getNumberInstance(english);
	          DecimalFormat df = (DecimalFormat)frForm;
	          df.applyPattern("###,##0.000");
			}
        break;
     case FRENCH :
        if(type == NUMBER)
          frForm = NumberFormat.getNumberInstance(french_ca);
        else if(type == PERCENT)
        {
          frForm = NumberFormat.getNumberInstance(french_ca);
          DecimalFormat df = (DecimalFormat)frForm;
          df.applyPattern("###,##0.000'%'");
        }
         else if(type == RATIO)
        {
          frForm = NumberFormat.getNumberInstance(french_ca);
          DecimalFormat df = (DecimalFormat)frForm;
          df.applyPattern("###,##0.00'%'");
        }
        else if(type == CURRENCY)
          //frForm = NumberFormat.getCurrencyInstance(french_ca);
          //#DG232 frForm = new DecimalFormat("#,##0.00$ ;(#,##0.00$)", new DecimalFormatSymbols(french_ca));
          frForm = new DecimalFormat("#,##0.00$;(#,##0.00$)", new DecimalFormatSymbols(french_ca));
		else if(type == CURRENCY_WITHOUT_DOLLAR)
		  frForm = new DecimalFormat("#,##0.00;($#,##0.00)", new DecimalFormatSymbols(french_ca));
        break;
     case SPANISH :
        if(type == NUMBER)
          frForm = NumberFormat.getNumberInstance(spanish);
        else if(type == PERCENT)
          frForm = NumberFormat.getPercentInstance(spanish);
        else if(type == CURRENCY)
          frForm = NumberFormat.getCurrencyInstance(spanish);
        break;
     default:
        if(type == NUMBER)
          frForm = NumberFormat.getNumberInstance(english);
        else if(type == PERCENT)
          frForm = NumberFormat.getPercentInstance(english);
        else if(type == CURRENCY)
          frForm = NumberFormat.getCurrencyInstance(english);
        break;
    }
    return frForm;

  }


  /**
  *

  * @return a String representing the date parameter formatted as per the languagePreferenceId lang.

  * in the date format specified.

  */
  public String getFormattedDate(Date d,int lang, int format)
  {
    //DateFormat dateFormatter = null;
    SimpleDateFormat dateFormatter = null;
    // first try to get formatting string for date format from BX Resources
    // if it does not exist take default.
    String lDateFormatStr = BXResources.getGenericMsg("DOCPREP_DEFAULT_DATE_FORMAT",lang);
    if(lDateFormatStr == null || lDateFormatStr.trim().equals("DOCPREP_DEFAULT_DATE_FORMAT")){
      lDateFormatStr = DATEFORMAT;
    }
    switch(lang)
    {
     case ENGLISH : {
        //dateFormatter = DateFormat.getDateInstance(format,english);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , english);
        break;
      }
     case FRENCH :{
        //dateFormatter = DateFormat.getDateInstance(format,french_fr);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , french_fr);
        break;
     }
     case SPANISH : {
        //dateFormatter = DateFormat.getDateInstance(format,spanish);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , spanish);
        break;
     }
     default: {
        //dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT,english);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , english);
        break;
      }
    }

   return dateFormatter.format(d);
  }

  /**
  *

  * @return a String representing the date parameter formatted as per the languagePreferenceId lang.

  * in the date format = DateFormat.LONG.

  */
  public String getFormattedDate(Date d,int lang)
  {
     return getFormattedDate(d,lang,DateFormat.LONG);
  }

  /**
  *

  * @return a String representing the double parameter formatted as per the languagePreferenceId lang.

  * and the specification for currency - 2 decimal places

  */
  public String getFormattedCurrency(double c,int lang)
  {
    Double amount = new Double(c);
    NumberFormat numberFormatter;
    numberFormatter = getNumberFormatter(lang,CURRENCY);
    return numberFormatter.format(amount);
  }
  
  /**
  *

  * @return a String representing the double parameter formatted as per the languagePreferenceId lang.

  * and the specification for currency - 2 decimal places, without the dollar sign

  */
  public String getFormattedCurrencyWithoutDollar(double c,int lang)
  {
    Double amount = new Double(c);
    NumberFormat numberFormatter;
    numberFormatter = getNumberFormatter(lang,CURRENCY_WITHOUT_DOLLAR);
    return numberFormatter.format(amount);
  }
  
  // TAR : 1612 - Formatting Interest Rate without '%' symbol
  /**
  *
  * @return a String representing the Interest Rate formatted as per the languagePreferenceId lang.
  * and the specification for Interest Rate - 3 decimal places, without the '%' sign
  * 
  * @param double - Interest Rate value
  * @param lang	  - Language preference id.
  * 
  * @return	String - Formatted Interest Rate.
  */
  public String getFormattedInterestRateWithoutPercentage(double c,int lang)
  {
    Double amount = new Double(c);
    NumberFormat numberFormatter;
    numberFormatter = getNumberFormatter(lang,INTEREST_WITHOUT_PERCENTAGE);
    return numberFormatter.format(amount);
  }  
  
  /**
  *

  * @return a String representing the double parameter formatted as per the languagePreferenceId lang.

  * and the specification for interest rates - 3 decimal places

  */
  public String getFormattedInterestRate(double r,int lang)
  {
    Double amount = new Double(r);
    NumberFormat numberFormatter;
    numberFormatter = getNumberFormatter(lang,PERCENT);
    return numberFormatter.format(amount);
  }


   /**
  *

  * @return a String representing the double parameter formatted as per the languagePreferenceId lang.

  * and the specification for ratios - 2 decimal places

  */
  public String getFormattedRatio(double r,int lang)
  {
    Double amount = new Double(r);
    NumberFormat numberFormatter;
    numberFormatter = getNumberFormatter(lang,RATIO);
    return numberFormatter.format(amount);
  }

  /**
  *

  * @return a String representing term number of months formatted as per the languagePreferenceId lang.

  * and the specification -( emulating the user interface)

  * <br>ie: yrs: mths:

  */
  public static String getYearsAndMonths(int term, int lang)
  {
    int years = term/12;
    int months = term - (years * 12);
    Object[] args = {new Integer(years), new Integer(months)};
    //MessageFormat format = new MessageFormat("yrs: {0,number,Integer} mths: {1,number,Integer}");
    MessageFormat format = new MessageFormat("{0,number,Integer} years  {1,number,Integer} months");

    switch(lang)
    {
     case ENGLISH :
        break;
     case FRENCH :
        //format.applyPattern("ans: {0,number,Integer} mois: {1,number,Integer}");
        format.applyPattern("{0,number,Integer} ans {1,number,Integer} mois");
        break;
     case SPANISH :
        break;
     default:
        break;
    }

    return format.format(args);
  }


  /**
  *
  * @return a String representing the time formatted as per the languagePreferenceId lang.
  * in the date format = DateFormat.LONG.
  */
  public String getFormattedDateTime(Date d,int lang)
  {
     return getFormattedDateTime(d, lang, DateFormat.LONG);
  }

  /**
  *
  * @return a String representing the time formatted as per the languagePreferenceId lang.
  * in the date format specified.
  */
  public String getFormattedDateTime(Date d, int lang, int format)
  {
    SimpleDateFormat dateFormatter = null;
    // first try to get formatting string for date format from BX Resources
    // if it does not exist take default.
    String lDateFormatStr = DJDATETIMEFORMAT;

    switch(lang)
    {
     case ENGLISH : {
        //dateFormatter = DateFormat.getDateInstance(format,english);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , english);
        break;
      }
     case FRENCH :{
        //dateFormatter = DateFormat.getDateInstance(format,french_fr);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , french_fr);
        break;
     }
     case SPANISH : {
        //dateFormatter = DateFormat.getDateInstance(format,spanish);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , spanish);
        break;
     }
     default: {
        //dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT,english);
        dateFormatter = new SimpleDateFormat(lDateFormatStr , english);
        break;
      }
    }

   return dateFormatter.format(d);
  }




}





