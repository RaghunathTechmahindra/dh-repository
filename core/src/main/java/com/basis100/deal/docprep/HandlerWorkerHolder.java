package com.basis100.deal.docprep;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
import com.basis100.deal.util.*;
import java.io.InputStream;
public class HandlerWorkerHolder
{

  private static final String EOL = "\n";
  private int workerNumberId;
  private String errorMessage;
  private int documentQueueId;
  private boolean inException;
  private String queueDetails;
  private int requestorUserId;
  // ---------- CR, 19-Jul-04, PGP for DJ  ---------
  private InputStream[] pgpKeys;
  // ---------- CR, 19-Jul-04, PGP for DJ end ---------
  private int institutionProfileId;


  public HandlerWorkerHolder(int pNumber)
  {
    workerNumberId = pNumber;
    errorMessage = null;
    inException = false;
    queueDetails = null;
    requestorUserId = -1;
  }

  public void setRequestorUserId(int pId){
    requestorUserId = pId;
  }
  public int getRequestorUserId(){
    return requestorUserId;
  }

  public void setQueueDetails(String pDetails){
    queueDetails = pDetails;
  }

  public String generateInfo(){
    StringBuffer sb = new StringBuffer();
    sb.append("<HandlerWorkerHolder> \n");
    sb.append("<id>").append(workerNumberId).append("</id> \n");
    sb.append("<exception>").append(inException).append("</exception> \n");
    sb.append("<errorMessage> \n").append( StringUtil.makeSafeXMLString(errorMessage) ).append("</errorMessage> \n");
    if( queueDetails != null ){
      sb.append( queueDetails );
    }

    sb.append("</HandlerWorkerHolder>");
    return sb.toString();
  }

  public boolean isInException(){
    return inException;
  }

  public void setInException(boolean pBool){
    inException = pBool;
  }

  public int getDocumentQueueId(){
    return documentQueueId;
  }

  public void setDocumentQueueId(int pId){
    documentQueueId = pId;
  }

  public void appendErrorMessage(String pMess){
    if (errorMessage == null){
      errorMessage = new String(pMess);
    }else{
      errorMessage += EOL;
      errorMessage += pMess;
    }
  }

  public void clearErrorMessage(){
    errorMessage = new String("");
  }

  public String getErrorMessage(){
    return errorMessage;
  }

  public int getWorkerNumberId(){
    return workerNumberId;
  }

  public void setWorkerNumberId(int pId){
    workerNumberId = pId;
  }

  public void setPGPkeys(InputStream[] pgpKeys){
      this.pgpKeys = pgpKeys ;
  }

  public InputStream[] getPGPKeys(){
    return this.pgpKeys;
  }

public int getInstitutionProfileId() {
    return institutionProfileId;
}

public void setInstitutionProfileId(int institutionProfileId) {
    this.institutionProfileId = institutionProfileId;
}

}
