package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class PrePayment extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {

      ConditionHandler ch = new ConditionHandler(srk);

      int pt = deal.getActualPaymentTerm();
      int id = deal.getPrePaymentOptionsId();
      //String option = PicklistData.getDescription("PrePaymentOptions", id);
      //ALERT:BX:ZR Release2.1.docprep
      String option  = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PrePaymentOptions",  id, lang);

      if(option.equalsIgnoreCase("NHA"))
      {
        //val = ch.getVariableVerbiage(deal,Dc.PREPAYMENT_NHA,lang);

        //==================================================================
        // ALERT:
        // Zivko made this change on 17 Sep 2002, based on request from
        // Derek. Condition 66 (PREPAYMENT_NHA_LONGTERM) should be triggered
        // for actual payment term of 5 years (60 months).
        //===================================================================
        //if(pt > 36)
        if(pt > 60)
        {
           val = ch.getVariableVerbiage(deal,Dc.PREPAYMENT_NHA_LONGTERM,lang);
        }else{
           val = ch.getVariableVerbiage(deal,Dc.PREPAYMENT_NHA,lang);
        }
      }
      else if(option.equalsIgnoreCase("Open with Penalty"))
      {
        val = ch.getVariableVerbiage(deal,Dc.PREPAYMENT_OPEN_WITH_PENALTY,lang);

        if(pt > 60)
        {
           val += ch.getVariableVerbiage(deal,Dc.PREPAYMENT_OPEN_PENALTY_LONG_TERM,lang);

        }
      }
      else if(option.equalsIgnoreCase("Open"))
      {
        val = ch.getVariableVerbiage(deal,Dc.PREPAYMENT_OPEN,lang);
      }
      else if(option.equalsIgnoreCase("Not Allowed"))
      {
        val = ch.getVariableVerbiage(deal,"PrePayment Not Allowed",lang);
      }
      else if(option.equalsIgnoreCase("3 Month Penalty"))
      {
        val = ch.getVariableVerbiage(deal,Dc.PREPAYMENT_3_MONTH_PENALTY,lang);
      }
      else if(option.equalsIgnoreCase("2 Month Penalty"))
      {
        val = ch.getVariableVerbiage(deal,Dc.PREPAYMENT_2_MONTH_PENALTY,lang);
      }


      fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
