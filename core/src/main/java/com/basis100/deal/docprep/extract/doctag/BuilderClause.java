package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class BuilderClause extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      int id = deal.getDealPurposeId();

      ConditionHandler ch = new ConditionHandler(srk);

      if(id == Mc.DEAL_PURPOSE_CONSTRUCTION)
      {
        val += ch.getVariableVerbiage(deal, Dc.BUILDER_HEADER_CLAUSE,lang);
        val += format.carriageReturn();
        val += ch.getVariableVerbiage(deal, Dc.BUILDER_CLAUSE,lang);
      }

      fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
