package com.basis100.deal.docprep.extract.data;
/**
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.xml.*;
import com.basis100.deal.conditions.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.picklist.*;
import java.util.*;
import MosSystem.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.*;
import com.basis100.deal.conditions.*;

/**
 * <PRE>
 * Creates an XML document for use as a Pre-Approval Certificate
 * Copyright (c) 2002 Basis100, Inc
 * </PRE>
 * @author       John Schisler
 * @version 1.0
 * @author MCM Impl Team 
 * change log:
 * @version 1.1 11-July-2008 XS_16.17 Added new '<ConditionSection>' 
 *                                    element inside every '<Condition>' tag in extractCondition() method
 */

public class GENXPreApprovalCertificateExtractor extends GENXExtractor
{
  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   * @author MCM Impl Team 
   * change log:
   * @version 1.1 11-July-2008 XS_16.17 Added prodcutType tag for the document.
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }
    /**
     * Builds the XML doc from various records in the database.
     *
     */
    protected void buildXMLDataDoc() throws Exception
    {
        doc = XmlToolKit.getInstance().newDocument();
        Node elemNode;
        Element root = doc.createElement("Document");
        root.setAttribute("type","1");
        root.setAttribute("lang","1");
        doc.appendChild(root);

        //==========================================================================
        // Language stuff
        //==========================================================================
        addTo(root, getLanguageTag());

        //==========================================================================
        // Lender Name
        //==========================================================================
        addTo(root, extractLenderName());

        //==========================================================================
        // Branch Address
        //==========================================================================
        elemNode = extractBranchAddressSection();

        //  For both the phone and fax numbers, we want to include the toll free
        //  version, if available, otherwise we use the non-toll free version.
        if (elemNode != null)
        {
            //==========================================================================
            // Branch Phone Number
            //==========================================================================
            String prefix = (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) ? "Tel: " : "T�l: " ;

            Node tempNode = extractBranchPhone(prefix, true, "Phone");

            if (tempNode == null)
               tempNode = extractBranchPhone(prefix, false, "Phone");

            addTo(elemNode, tempNode);

            //==========================================================================
            // Branch Fax Number
            //==========================================================================
            prefix = (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) ? "Fax: " : "T�l�c: " ;

            tempNode = extractBranchFax(prefix, true, "Fax");

            if (tempNode == null)
               tempNode = extractBranchFax(prefix, false, "Fax");

            addTo(elemNode, tempNode);

            root.appendChild(elemNode);
        }

        //==========================================================================
        // Current Date
        //==========================================================================
        addTo(root, extractCurrentDate());

        //==========================================================================
        // Branch Current Time
        //==========================================================================
        addTo(root, extractBranchCurrentTime());

        //==========================================================================
        // Broker Firm Name
        //==========================================================================
        addTo(root, extractBrokerFirmName());

        //==========================================================================
        // Broker Agent Name
        //==========================================================================
        addTo(root, extractBrokerAgentName());

        //==========================================================================
        // Broker Address Section
        //==========================================================================
        addTo(root, extractBrokerAddressSection());

        //==========================================================================
        // Broker Source Number
        //==========================================================================
        addTo(root, extractBrokerSourceNum());

        //==========================================================================
        // Deal Num
        //==========================================================================
        addTo(root, extractDealNum());

        //==========================================================================
        // Client Names
        //==========================================================================
        addTo(root, extractClientNames());

        //==========================================================================
        // Client address section
        //==========================================================================
        addTo(root, extractClientAddressSection());

        //==========================================================================
        // Total Loan Amount
        //==========================================================================
        addTo(root, extractTotalLoanAmount());

        //==========================================================================
        // Loan Amount
        //==========================================================================
        addTo(root, extractLoanAmount());

        //==========================================================================
        // Premium
        //==========================================================================
        addTo(root, extractPremium());

        //==========================================================================
        // PST
        //==========================================================================
        addTo(root, extractPST());

        //==========================================================================
        // Deal purpose
        //==========================================================================
        addTo(root, extractDealPurpose());

        //==========================================================================
        // Deal Type
        //==========================================================================
        addTo(root, extractDealType());
        //==========================================================================
        /***************MCM Impl Team XS_16.17 Changes Starts**********************/
        // Product Type
        //==========================================================================
        addTo(root, extractProductType());
        /***************MCM Impl Team XS_16.17 Changes Starts**********************/
        //==========================================================================
        // Fixed Rate Variable Rate Description
        //==========================================================================
        addTo(root, extractFRVRDescription());

        //==========================================================================
        // MI Premium
        //==========================================================================
        addTo(root, extractMIPremium());

        //==========================================================================
        // Purchase Price
        //==========================================================================
        addTo(root, extractPAPurchasePrice("PurchasePrice"));

        //==========================================================================
        // Term
        //==========================================================================
        addTo(root, extractTerm());

        //==========================================================================
        // Amortization
        //==========================================================================
        addTo(root, extractAmortization());

        //==========================================================================
        // Interest Rate
        //==========================================================================
        addTo(root, extractInterestRate());

        //==========================================================================
        // P And I Payment
        //==========================================================================
        addTo(root, extractPAndIPayment());

        //==========================================================================
        // Payment Frequency
        //==========================================================================
        addTo(root, extractPaymentFrequency());

        //==========================================================================
        // Conditions
        //==========================================================================
        addTo(root, extractConditions());

        //==========================================================================
        // Commitment Expiry Date
        //==========================================================================
        addTo(root, extractCommitmentExpiryDate());

        //==========================================================================
        // Underwriter
        //==========================================================================
        addTo(root, extractUnderwriter());

        //==========================================================================
        //  Fees
        //==========================================================================
        addTo(root, extractFees());

        //==========================================================================
        //  Document Due Date
        //==========================================================================
        addTo(root, getTag("DocumentDueDate", "five (5)"));
    }

    protected Node extractGuarantorNamesLine()
    {
        String line = null;

        try
        {
            List borrowers = getBorrowers();

            if (borrowers.isEmpty())
               return null;

            Iterator it = borrowers.iterator();
            String firstname, lastname;
            Borrower bor;
            String val = null;

            while (it.hasNext())
            {
                if (line == null)
                   line = new String();

                bor = (Borrower)it.next();

                if (bor.getBorrowerTypeId() == Mc.BT_GUARANTOR)
                {
                    firstname = bor.getBorrowerFirstName();
                    lastname = bor.getBorrowerLastName();

                    if (!isEmpty(firstname) && !isEmpty(lastname))
                    {
                        val = firstname + format.space() + lastname;
                        line += val + (it.hasNext()?",":"") + format.space();
                    }
                }
            }

            if (line.length() > 0)
            {
                line = line.trim();

                if (line.endsWith(","))
                    line = line.substring(0,line.length()-1);
            }
        }
        catch (Exception e){}

        return getTag("GuarantorNamesLine", line);
    }

    /**
     * Retrieves the client names on the deal and formats an XML tag with them.
     *
     * @return The completed client names XML tag.
     *
     */
    protected Node extractClientNames()
    {
        Node elemNode = null;

        try
        {
            List c = getBorrowers();

            if (c.isEmpty())
               return null;

            Collections.sort(c, new PrimaryBorrowerComparator());

            Iterator i = c.iterator();

            elemNode = doc.createElement("ClientNames");

            String val;
            Borrower b;

            while (i.hasNext())
            {
                b =(Borrower)i.next();

                //val = PicklistData.getDescription("Salutation", b.getSalutationId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Salutation", b.getSalutationId(), lang);

                val = (val == null) ? "" : val + format.space();

                if (!isEmpty(b.getBorrowerFirstName()))
                    val += b.getBorrowerFirstName() + format.space();

                if (!isEmpty(b.getBorrowerMiddleInitial()))
                    val += b.getBorrowerMiddleInitial() + format.space();

                if (!isEmpty(b.getBorrowerLastName()))
                    val += b.getBorrowerLastName() + format.space();

                addTo(elemNode, getTag("Name", val));
            }
        }
        catch (Exception e){}

        return elemNode;
    }
/**
 * 
 *@author MCM Impl Team 
 * change log:
 * @version 1.1 11-July-2008 XS_16.17 Added new '<ConditionSection>' 
 *                                    element inside every '<Condition>' tag in extractCondition() method
 */
    protected Node extractConditions()
    {
        Node elemNode = null;

        try
        {
            List dts = (List) deal.getDocumentTracks();

            if (dts.isEmpty())
               return null;

            boolean bShowRole =
                PropertiesCache.getInstance().
                    getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.show.responsibilityrole.tag", "Y").
                        equalsIgnoreCase("Y");

            //  Sort by Requested first, then alphabetical after that...
            Collections.sort(dts, new DocumentStatusComparator());

            Iterator i = dts.iterator();
            int sourceId, condTypeId, statusId, roleid;
            String role, condition;
            Node lineNode;
            int index = 2;
            Node condNode;
            ConditionParser p = new ConditionParser();

            while (i.hasNext())
            {
                lineNode = doc.createElement("Line");

                DocumentTracking dt = (DocumentTracking)i.next();
                Condition cond = new Condition(srk, dt.getConditionId());

                condTypeId = cond.getConditionTypeId();
                sourceId = dt.getDocumentTrackingSourceId();
                statusId = dt.getDocumentStatusId();
                roleid = dt.getDocumentResponsibilityRoleId();

                //role = PicklistData.getDescription("ConditionResponsibilityRole", dt.getDocumentResponsibilityRoleId());
                //ALERT:BX:ZR Release2.1.docprep
                role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", dt.getDocumentResponsibilityRoleId(), lang);

                if ((sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED ||
                     sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED ||
                     sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM) &&
                    (condTypeId == Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING ||
                     condTypeId == Mc.CONDITION_TYPE_COMMITMENT_ONLY) &&
                    (roleid == Mc.CRR_PARTY_TYPE_APPRAISER ||
                     roleid == Mc.CRR_PARTY_TYPE_SOLICITOR ||
                     roleid == Mc.CRR_SOURCE_OF_BUSINESS ||
                     roleid == Mc.CRR_USER_TYPE_APPLICANT) &&
                    (statusId == Mc.DOC_STATUS_REQUESTED ||
                     statusId == Mc.DOC_STATUS_RECEIVED ||
                     statusId == Mc.DOC_STATUS_INSUFFICIENT ||
                     statusId == Mc.DOC_STATUS_CAUTION||
                     statusId == Mc.DOC_STATUS_WAIVED))
                {
                    if (elemNode == null)
                       elemNode = doc.createElement("Conditions");

                    condition = dt.getDocumentTextByLanguage(lang);

                    condition = (condition == null) ?
                                index++ + "." + format.space() : 
                                index++ + "." + format.space() + condition;

                    if (bShowRole)
                       condition += format.space() + "(" + role + ")";

                   //  JS - changed to make the XML data not RTF-specific.  This
                   //  new way adheres to other XML generation techniques,
                   //  where the lines previously divided by '\par'
                   //  are now divided into child nodes.  This allows the
                   //  XSL to format to the respective document type's syntax.
                   condNode = doc.createElement("Condition");
                   /*********************MCM Impl Team XS_16.17 changes statrs*****************************/
                   addTo(condNode,"ConditionSection", dt.loadSectionCode());
                   /*********************MCM Impl Team XS_16.17 changes ends*****************************/
                   elemNode.appendChild(convertCRToLines(condNode,
                      p.parseText(condition,lang,deal,srk)));
                }
            }
        }
        catch (Exception e)  {}

        return elemNode;
    }

    protected Node extractMIPremium()
    {
        String val = null;

        try
        {
            int miIndID = deal.getMIIndicatorId();

            if (miIndID == Mc.MII_REQUIRED_STD_GUIDELINES || miIndID == Mc.MII_UW_REQUIRED)
               val = formatCurrency(deal.getMIPremiumAmount());
        }
        catch (Exception e){}

        return getTag("MIPremium", val);
    }
}
