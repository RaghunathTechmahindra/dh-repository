package com.basis100.deal.docprep;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.basis100.deal.docprep.extract.data.RatioFieldList;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;
// PROGRAMMERS NOTE: this class has changed for multiple unresolved exception fix
/**
 *  A singleton that holds parameters and resources required <br/>
 *  for the Document generation process.
 *
 */
public class DocumentGenerationManager
{
    private static DocumentGenerationManager instance;
    public static boolean debug = true;

    private  SysLogger logger;

    /**mdfml, upload file cache*/
    private  Map resourceCache;

    /**admin folder*/
    private  String resourceRoot;

    /** outbound directories cached here*/
    private  HashMap outbound;
    private  File archiveDir;
    private  boolean archiveOn;

    private  int monitorInterval = 8;
    private  int staleTimeOut = 20; //minutes

    private static final int UPLOAD = 10;
    private static final int APPROVAL = 20;
    private static final int DOCUMENT = 30;

    private java.util.ArrayList unwantedDocuments;
    private RatioFieldList ratioFieldList;
    private DocumentGenerationManager()
    {
      unwantedDocuments = new java.util.ArrayList();

      logger = ResourceManager.getSysLogger("DocumentEventManager");

      resourceCache = new HashMap();

      outbound = new HashMap();

      ratioFieldList = new RatioFieldList();

      try
      {    	  
    	DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();  
    	  
    	Document config = parser.parse(ClassLoader.getSystemResourceAsStream("application.xml"));  

        Element docgen = DomUtil.getFirstNamedDescendant(config, "docgen");

        if(docgen == null)
        {
          String msg = "DOCPREP: No configuration module found in application.xml";
          logger.error(msg);
          System.out.println(msg);
          logger.mail(msg);
        }

        List nodes = DomUtil.getNamedDescendants(docgen, "outbound");

        Iterator outs = nodes.iterator();
        Element outelem = null;
        String name = null;
        String path = null;
        File dir = null;

        while(outs.hasNext())
        {
          outelem = (Element)outs.next();

          name = outelem.getAttribute("name");
          path = outelem.getAttribute("path");

          if(name == null || name.trim().length() == 0)
          {
            String msg = "DOCPREP: No Value given for name or path attributes "+
            " for outbound directory.";
            logger.error(msg);
            System.out.println(msg);
            continue;
          }

          dir = new File(path);

            if(!dir.exists())
            {
              if(!dir.mkdirs())
              {
                String msg = "DOCPREP: Given directory for: " + name + " does not exist " +
                "and could not be made. ";

                logger.error(msg);
                System.out.println(msg);
              }
            }

          outbound.put(name.toLowerCase(),dir);

        }//end while

        //=======================================================================================
        // load list of unwanted documents from application.xml file
        logger.trace("List of unwanted documents - start");
        try{
          Element unwanted = DomUtil.getFirstNamedDescendant(config,"unwanteddocuments");
          if( unwanted != null ){
              List uwNodes = DomUtil.getNamedDescendants(unwanted, "document");
              Iterator uwIterator = uwNodes.iterator();
              while( uwIterator.hasNext() ){
                Element docElem = (Element)uwIterator.next();
                String elVal = DomUtil.getElementText(docElem);
                if( elVal != null ){
                  this.unwantedDocuments.add( elVal.trim() );
                }
              }
          }
        } catch(Exception uwExcecption){
          logger.error("List of unwanted documents could not be loaded. It will be created as empty list.");
        }
        logger.trace("List of unwanted documents - end");

        //=======================================================================================
        try
        {
          Element debugArchive = DomUtil.getFirstNamedDescendant(config,"Archive");
          String archiveLocation = debugArchive.getAttribute("location");
          File archive = new File(archiveLocation);

          if(archive.exists() && archive.isDirectory() && archive.canWrite())
          {
            String onval = debugArchive.getAttribute("active");

            if(onval != null)
             this.archiveOn = TypeConverter.booleanFrom(onval.trim(), "false");

            this.archiveDir = archive;
          }
          else {
            this.archiveDir = null;
            this.archiveOn = false;
            throw new DocPrepException("invalid value for <Archive location>. Documents will not be archived!");
          }
        }
        catch(Exception e) //catch null values for any of the above
        {
           String msg = "DOCPREP MANAGER ERROR: couldn't create archive directory: " + e;
           logger.error(msg);
           System.out.println(msg);
           msg = null;
        }

        Element interval = DomUtil.getFirstNamedDescendant(config, "QueueMonitor");

        if(interval == null)
         this.monitorInterval = 8;
        else
        {
          String monvalue  = interval.getAttribute("intervalsec");
          String staletime = interval.getAttribute("staletime");

          try
          {
            this.monitorInterval = Integer.parseInt(monvalue);
          }
          catch(Exception e)
          {
            this.monitorInterval = 8; //8 second monitor default
          }

          try
          {
            this.staleTimeOut = Integer.parseInt(staletime);
          }
          catch(Exception e)
          {
            this.staleTimeOut = 20; //8 second monitor default
          }
        }

       }
       catch(Exception e)
       {
          String msg = "DOCPREP: No Value given for name or path attributes "+
          " for outbound directory.";
          logger.error(msg);
          System.out.println(msg);
       }
    }

    public boolean isDocumentWanted( String docName ){
      if(unwantedDocuments.contains( docName.trim() )){
        return false;
      }
      return true;
    }

  /**
   *   Return the sole instance of this Manager. If none exists create an instance
   *   and store it in a static variable.
   */
    public static synchronized DocumentGenerationManager getInstance()
    {
      if(instance == null)
      {
       instance = new DocumentGenerationManager();
      }

      return instance;
    }

   /**
    *   generate a Document using the DocumentQueue instance supplied by creating <br/>
    *   and starting a handler thread.
    *   @param request - the DocumentQueue entity
    */
    //public void generateDocument(DocumentQueue request)
    public synchronized void generateDocument(  HandlerWorkerHolder pHolder, ActionListener pListener) throws Exception
    {

      try
      {

        DocumentGenerationManager.debug("\n\nDOCPREP: Document Requested. " + new Date());
        //DocumentGenerationHandler handler = new DocumentGenerationHandler(request) ;
        DocumentGenerationHandler handler = new DocumentGenerationHandler( pHolder, pListener) ;
        //DocumentGenerationManager.debug("DOCPREP: " + handler);
        //DocumentGenerationManager.debug("DOCPREP: " + request);
        //DocumentGenerationManager.debug("DOCPREP: Requestor ID: " + request.getRequestorUserId());
        //DocumentGenerationManager.debug("DOCPREP: Deal: " + request.getDealId());
        //DocumentGenerationManager.debug("DOCPREP: Document Type: " + handler.getDocumentFullName() + "\n");
        handler.start();

        handler = null;
      }
      catch(Exception ex)
      {
        String msg = "DOCPREP: Failed to start DocumentGenerationHandler due to error: [ " + ex + " ]";
        System.out.println(msg);
        logger.error(msg);
        throw ex;
      }

    }


    /**
     *  gets the parameter interval (in seconds) at which the request queue is monitored
     */
    public int getMonitorInterval()
    {
      return this.monitorInterval;
    }



    /**
     *  gets the parameter interval (in seconds) at which the request queue is monitored
     */
    public int getStaleTimeOut()
    {
      return this.staleTimeOut;
    }

    /**
     *  gets the String value representing the path to 'xml resources'
     */
    public String getResourcesRoot()
    {
      return resourceRoot;
    }

    public Object getResource(String key)
    {
      return resourceCache.get(key);
    }

    public void putResource(String key, Object in)
    {
      resourceCache.put(key,in);
    }

    public File getOutboundDirectoryFor(String name)
    {
      return (File)outbound.get(name.toLowerCase());
    }

    /**
     * Archives <code>file</code> to the archive directory and returns it with
     * its new path.  If an error occurs, returns <code>null</code>.
     *
     * @param file the file to be archived
     * @param dealId the deal number of the file's data
     * @param the prefix for the archive file
     * @param ext the extension of the archive file
     *
     * @return the archived file, complete with path or <code>null</code> if an
     * error occurred.
     */
    public File archiveData(File file, int dealId, int institutionProfileId, String prefix, String ext)
    {
      if (!archiveOn) return null;

      try
      {
          if(file == null)
          {
            logger.error("DOCPREP: MANAGER: Invalid file recieved for archive.");
            return null;
          }

          Date cd = new Date();

          String datestr = TypeConverter.isoDateFrom(cd);

          if(ext == null)
           ext = "txt";

          if(ext.startsWith("."))
          {
            ext = ext.substring(1);
          }

          String name = prefix + "_" + datestr;

          if(dealId > 0)
              name = name + "_" + dealId;
          
          // add for ML
          if(institutionProfileId > 0)
              name = name + "_" + institutionProfileId;

          name = archiveDir.getAbsolutePath() + "/" + name + "." + ext;;

          logger.trace("DOCPREP: DocumentGenerationManager: archiving file name : " + name);

          file.renameTo(new File(name));

          return file;
      }
      catch(Exception e)
      {
         logger.error("DOCPREP: MANAGER: Couldn't write archive file.");
         logger.error(e);

         return null;
      }
    }

    public boolean archiveData(String data, int dealId, int insititutionProfileId,  String prefix, String ext)
    {
      OutputStreamWriter osw = null;

      if(!archiveOn) return false;

      try
      {
          if(data == null || data.trim().length() == 0)
          {
            data = "NO DATA RECEIVED";
            logger.error("DOCPREP: MANAGER: Empty Data recieved for archive.");
          }

          Date cd = new Date();

          String datestr = TypeConverter.isoDateFrom(cd);

          // 1) find ext
          if (ext == null){  ext = "txt"; }
          if (ext.startsWith("."))
          {
            ext = ext.substring(1);
          }

          // 2) name
          String name = prefix + "_" + datestr;
          if (dealId > 0) { name = name + "_" + dealId; }
          if (insititutionProfileId > 0) { name = name + "_" + insititutionProfileId; }

          name = archiveDir.getAbsolutePath() + "/" + name + "." + ext;;
          logger.trace("DOCPREP: DocumentGenerationManager: archiving data file name : " + name);

          // 3) write
          osw = new OutputStreamWriter(new FileOutputStream(name));
          osw.write(data);
          osw.flush();
          osw.close();
          osw = null;
          return true;
      }
      catch(Exception e)
      {
         logger.error("DOCPREP: MANAGER: Couldn't write archive file.");
         logger.error(e);
         return false;
      }
      finally
      {
       try{osw.close();}catch(Exception le){;}
      }
    }

    public static void debug(String msg)
    {
      if(debug)
      {
        System.out.println(msg);
      }
    }

    public File getArchiveDir()
    {
        return archiveDir;
    }

    public RatioFieldList getRatioFieldList(){
      return ratioFieldList;
    }
}

