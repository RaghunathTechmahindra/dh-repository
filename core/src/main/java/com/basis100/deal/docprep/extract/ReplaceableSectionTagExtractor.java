package com.basis100.deal.docprep.extract;

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import java.util.*;


public abstract class ReplaceableSectionTagExtractor extends DocumentTagExtractor
{
  public static int REMOVE = -1;
   public static int REPLACE_ONLY = 0;
   public static int REPLACE_AND_PARSE = 1;
   public static int DONT_REPLACE = 2;

}