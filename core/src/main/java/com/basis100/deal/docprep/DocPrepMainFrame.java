package com.basis100.deal.docprep;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.UIManager;

import com.basis100.deal.docprep.gui.MainAreaPanel;
import com.basis100.deal.entity.DocumentQueue;
import com.basis100.deal.util.StringUtil;
import com.basis100.log.SysLogger;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.FinderException; 
import com.basis100.entity.RemoteException; 

public class DocPrepMainFrame extends JFrame implements ActionListener
{
  public static final int DEFAULT_MONITOR_DELAY = 8;
  private static String BUILD_NUMBER = "9.6.0.6";

  private JToolBar toolBarPanel;
  private MainAreaPanel mainAreaPanel;
//  private JButton startButton;
  private JButton stopButton;
  private JButton resumeMonitoringButton;
  private JButton statusButton;
  private JButton increaseButton;
  private JButton decreaseButton;
  private JButton failedinfoButton;
  private JButton exitButton;
  private DocPrepMainController controller;
  private DocRequestQueueMonitor monitor;
  private static String workPath;
  private javax.swing.Timer monitorTimer;
//  private javax.swing.Timer startTimer;
  private int ordNumber;
//  private SessionResourceKit srk;
  private String dpFrameTitle;

  private SysLogger logger;

  public DocPrepMainFrame()
  {
    super();
    logger = null;
    ordNumber = 0;
    dpFrameTitle = BUILD_NUMBER;
    setTitle(dpFrameTitle);
    monitor = null;
    workPath = null;  // must be supplied as application parameter
    monitorTimer = new javax.swing.Timer(2000,this);
    monitorTimer.setCoalesce(true);
    //monitorTimer.setRepeats(false);

    setLocation(10,10);
    setSize(400,400);

    toolBarPanel = new JToolBar();
    toolBarPanel.setPreferredSize(new Dimension(400,25));
    toolBarPanel.setFloatable(false);

    //startButton = new JButton("Start");
    //startButton.setSize(23,23);
    //startButton.setPreferredSize(new Dimension(23,23));
    //startButton.setMinimumSize(new Dimension(23,23));
    //startButton.addActionListener(this);

    stopButton = new JButton("Stop Monitoring");
    stopButton.setSize(23,23);
    stopButton.setPreferredSize(new Dimension(23,23));
    stopButton.setMinimumSize(new Dimension(23,23));
    stopButton.setEnabled(false);
    stopButton.addActionListener(this);

    resumeMonitoringButton = new JButton("Resume Monitoring");
    resumeMonitoringButton.setSize(23,23);
    resumeMonitoringButton.setPreferredSize(new Dimension(23,23));
    resumeMonitoringButton.setMinimumSize(new Dimension(23,23));
    resumeMonitoringButton.setEnabled(false);
    resumeMonitoringButton.addActionListener(this);

    increaseButton = new JButton("Increase");
    increaseButton.setSize(23,23);
    increaseButton.setPreferredSize(new Dimension(23,23));
    increaseButton.setMinimumSize(new Dimension(23,23));
    increaseButton.setEnabled(false);
    increaseButton.addActionListener(this);

    decreaseButton = new JButton("Decrease");
    decreaseButton.setSize(23,23);
    decreaseButton.setPreferredSize(new Dimension(23,23));
    decreaseButton.setMinimumSize(new Dimension(23,23));
    decreaseButton.setEnabled(false);
    decreaseButton.addActionListener(this);

    failedinfoButton = new JButton("Failed Info");
    failedinfoButton.setSize(23,23);
    failedinfoButton.setPreferredSize(new Dimension(23,23));
    failedinfoButton.setMinimumSize(new Dimension(23,23));
    failedinfoButton.setEnabled(false);
    failedinfoButton.addActionListener(this);

    statusButton = new JButton("Status");
    statusButton.setSize(23,23);
    statusButton.setPreferredSize(new Dimension(23,23));
    statusButton.setMinimumSize(new Dimension(23,23));
    statusButton.setEnabled(false);
    statusButton.addActionListener(this);

    exitButton = new JButton("Exit");
    exitButton.setSize(23,23);
    exitButton.setPreferredSize(new Dimension(23,23));
    exitButton.setMinimumSize(new Dimension(23,23));
    exitButton.setEnabled(false);
    exitButton.addActionListener(this);

    //toolBarPanel.add(startButton);
    toolBarPanel.add(stopButton);
    toolBarPanel.add(resumeMonitoringButton);
    toolBarPanel.add(increaseButton);
    toolBarPanel.add(decreaseButton);
    toolBarPanel.add(failedinfoButton);
    toolBarPanel.add(statusButton);
    toolBarPanel.add(exitButton);

    this.getContentPane().add(toolBarPanel, java.awt.BorderLayout.NORTH);

    mainAreaPanel = new MainAreaPanel();
    mainAreaPanel.setPreferredSize(new Dimension(300,300));
    mainAreaPanel.setBackground(Color.gray);
    mainAreaPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder() );
    this.getContentPane().add(mainAreaPanel, java.awt.BorderLayout.CENTER);
    pack();

    //startTimer = new javax.swing.Timer(5000,this);
    //startTimer.setRepeats(false);
    //startTimer.start();
  }

  public void actionPerformed(java.awt.event.ActionEvent ev)
  {
    Object obj = ev.getSource();
		if(obj instanceof HandlerWorkerHolder)
		{ 
			  System.out.println("Frame received the event : " + ev.getActionCommand() ); 
      }
		else if( obj == resumeMonitoringButton)
		{ 
      resumeMonitoringButton.setEnabled(false);
      monitorTimer.start();
      stopButton.setEnabled(true);
      controller.setMonitoringStopped(false);
		}
		else if(obj == increaseButton)
		{ 
      controller.increaseWrkThreadSize();
		}
		else if(obj == decreaseButton)
		{ 
      controller.decreaseWrkThreadSize();
		}
		else if(obj == stopButton)
		{ 
      monitorTimer.stop();
      resumeMonitoringButton.setEnabled(true);
      stopButton.setEnabled(false);
      controller.setMonitoringStopped(true);
		}
		else if(obj == exitButton)
		{ 
      // zivko:marker this must be taken care of and user should
      // be prompted for exit. We also must allow all threads in process to
      // finish their work.
      System.exit(0);
		}
		else if(obj == statusButton)
		{ 
      System.out.println(controller.extractStatistics());
		}
		else if(obj == failedinfoButton)
		{ 
			  if( monitor != null )
			  { 
        System.out.println(controller.extractFailureMessages());
      }
		}
		else if(obj == monitorTimer)
		{ 
			SessionResourceKit srk = null; 
			try
			{ 
				  srk = new SessionResourceKit("DocPrepMainFrame"); 
				  processTimerEvent(srk); 
			}
			catch(Exception e)
			{ 
					  e.printStackTrace(); 
					  logger.error(StringUtil.stack2string(e)); 
			}
			finally
			{ 
				if(srk != null)
				{ 
					  srk.freeResources(); 
				} 
      }
    }
  }

  private void processTimerEvent(SessionResourceKit srk)throws Exception
  {
	  // reset VPD 
	  srk.getExpressState().cleanDealIds();

	  DocumentQueue queue = new DocumentQueue(srk);
	  queue = queue.findNextEntryTest();
	  if(queue != null)
	  {
		  if( controller.canLaunchNewRequest() == false )
		  {
			  System.out.println("System is busy, we will wait a little bit....");
			  logger.warning("System is busy, we will wait a little bit....");
			  return;
		  }
		  // set VPD for ML
		  srk.getExpressState().setDealInstitutionId(queue.getInstProfileId());
		  ordNumber++;
		  int oldQueue = queue.getDocumentQueueId();
		  oldQueue = oldQueue * (-1);
		  System.out.println("Starting process ----> " + ordNumber);

		  HandlerWorkerHolder holder = new HandlerWorkerHolder(ordNumber);
		  holder.setDocumentQueueId(oldQueue);
		  holder.setQueueDetails( collectQueueData(queue) );
		  holder.setRequestorUserId( queue.getRequestorUserId() );
		  holder.setInstitutionProfileId(queue.getInstProfileId());
		  holder.setPGPkeys(controller.getPGPkeyInputStream(queue.getInstProfileId()));

		  srk.beginTransaction();
		  queue.setRequestStatusId(Dc.DOCUMENT_REQUEST_STATUS_IN_PROCESS);
		  queue.setDocumentQueueId(oldQueue);
		  queue.ejbStore();
		  srk.commitTransaction();

		  controller.launchNewProcess(holder);

	  }
  }

  private int init(String path)
  {
    try
    {
      PropertiesCache.addPropertiesFile(path);
      PropertiesCache resources = PropertiesCache.getInstance();

      setTitle(resources.getInstanceProperty("com.filogix.docprep.running.environment") + " - DocPrep v."  + BUILD_NUMBER);
      
      // ---- read timer setting from the file -----------------
      int delay = Integer.parseInt(resources.getInstanceProperty("com.filogix.docprep.monitor.number.of.miliseconds", "5000"));
      monitorTimer.setDelay(delay);
      //  -----------------

      System.out.println("DOCPREP: INFO: RESOURCE MANAGER INIT COMPLETE. ");
      logger = ResourceManager.getSysLogger("DocPrep");
      PicklistData.init();

      logger.info("DOCPREP: INFO: resource cache init complete.");
      System.out.println("DOCPREP: INFO: PICKLIST INIT COMPLETE. ");

      
      System.out.println("Instantiating controller... started");
      
      controller = new DocPrepMainController();
      controller.addListener(this);
      System.out.println("Instantiating controller... finished");

      mainAreaPanel.assignDPGuiListener(controller);
      return DocumentGenerationManager.getInstance().getMonitorInterval();
    }
    catch(Exception e)
    {
      String msg = "Failed to initialize the DocPrep System!\n";
      if(e.getMessage()!= null) msg+= e.getMessage();
      System.out.println(msg);
      e.printStackTrace();
    }

    return DEFAULT_MONITOR_DELAY;
  }

  private void startProcess() throws Exception
  {
   try
   {
      if(workPath == null){
        throw new Exception("Docprep could not start. Mossys.properties file has not been defined.");
      }

      int interval = init(workPath);

      java.util.Date today = new java.util.Date();

      monitor = new DocRequestQueueMonitor(interval);
      monitor.addActionListener(controller);

      System.out.println("\n _______________________________________________________________________\n");
      System.out.println("\tDocument Generation Running. ");
      System.out.println("\tBuild #" + BUILD_NUMBER + ": " + today);
      System.out.println("\tDocument Queue Monitor Thread Active.");
      System.out.println(  "_______________________________________________________________________\n");

      stopButton.setEnabled(true);
      resumeMonitoringButton.setEnabled(false);
      increaseButton.setEnabled(true);
      decreaseButton.setEnabled(true);
      exitButton.setEnabled(true);
      failedinfoButton.setEnabled(true);
      statusButton.setEnabled(true);

      StringBuffer msgBuf = new StringBuffer("\n ========================================================= \n");
      msgBuf.append("*** DOCPREP INITIALIZATION PROCESS *** \n");
      msgBuf.append("=========================================================");
      logger.debug( msgBuf.toString() );

      System.out.println("Starting monitor timer: ");
      try{
        String lMonitorDelay = PropertiesCache.getInstance().getInstanceProperty("com.filogix.docprep.monitor.delay.milliseconds", "2000");
        monitorTimer.setDelay( Integer.parseInt(lMonitorDelay) );
      }catch(Exception exc){
        System.out.println("Exception in setting monitor timer. The default 2000 will be used.");
        // do nothing here. We already set monitor interval to be 2000
      }
      System.out.println("Monitor delay set to: " + monitorTimer.getDelay() + " milliseconds");
      monitorTimer.start();
      System.out.println("_______________________________________________________________________\n");
   }
   catch(Throwable t)
   {
     System.out.println("DOCPREP: ERROR: Failure in DocPrep Module.");
     t.printStackTrace();
   }
  }


  public static void main(String[] args)
  {
   try{

      UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
      //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
    } catch(Exception exc){
      // do nothing
    }

    DocPrepMainFrame mainFrame = new DocPrepMainFrame();

     workPath =  (new File("mossys.properties")).getAbsolutePath();


    mainFrame.setVisible(true);
    try{
      mainFrame.startProcess();
    } catch(Exception exc){
      System.out.println("*****************************************************");
      System.out.println("***   Could not start process due to exception    ***");
      System.out.println("*****************************************************");
      exc.printStackTrace();

    }
  }

  public String collectQueueData(DocumentQueue pQueue){
    StringBuffer sb = new StringBuffer("");
    sb.append("<documentQueue> \n");

    sb.append("<documentQueueId>").append( pQueue.getDocumentQueueId() ).append("</documentQueueId> \n");
    sb.append("<timeRequested>").append( DocPrepLanguage.getInstance().getFormattedDate(pQueue.getTimeRequested(), 0) ).append("</timeRequested> \n");
    sb.append("<requestorUserId>").append( pQueue.getRequestorUserId() ).append("</requestorUserId> \n");
    sb.append("<emailAddress>").append( StringUtil.makeSafeXMLString( pQueue.getEmailAddress()) ).append("</emailAddress> \n");
    sb.append("<emailFullName>").append( StringUtil.makeSafeXMLString( pQueue.getEmailFullName()) ).append("</emailFullName> \n");
    sb.append("<emailSubject>").append( StringUtil.makeSafeXMLString( pQueue.getEmailSubject()) ).append("</emailSubject> \n");
    sb.append("<languagePreferenceId>").append( pQueue.getLanguagePreferenceId() ).append("</languagePreferenceId> \n");
    sb.append("<lenderProfileId>").append( pQueue.getLenderProfileId() ).append("</lenderProfileId> \n");
    sb.append("<documentTypeId>").append( pQueue.getDocumentTypeId() ).append("</documentTypeId> \n");
    sb.append("<documentFormat>").append( StringUtil.makeSafeXMLString( pQueue.getDocumentFormat()) ).append("</documentFormat> \n");
    sb.append("<documentVersion>").append( StringUtil.makeSafeXMLString( pQueue.getDocumentVersion()) ).append("</documentVersion>");
    sb.append("<dealId>").append( pQueue.getDealId() ).append("</dealId> \n");
    sb.append("<dealCopyId>").append( pQueue.getDealCopyId() ).append("</dealCopyId> \n");
    sb.append("<scenarioNumber>").append( StringUtil.makeSafeXMLString(  pQueue.getScenarioNumber()) ).append("</scenarioNumber> \n");
    sb.append("<emailText>").append( StringUtil.makeSafeXMLString(  pQueue.getEmailText()) ).append("</emailText> \n");
    sb.append("<faxNumbers>").append( StringUtil.makeSafeXMLString( pQueue.getFaxNumbers()) ).append("</faxNumbers> \n");
    sb.append("<requestStatusId>").append( pQueue.getRequestStatusId() ).append("</requestStatusId> \n");

    sb.append("</documentQueue> \n");

    return sb.toString();
  }


}
