package com.basis100.deal.docprep.extract;

/**
 * 04/Jul/2005 DVG #DG242 link 2 to Funding (Profile) and
 *   new link to Closing services (FCT)
 * 30/May/2005 DVG #DG216 #1462  GE-Production - AU request - retrieve specific copy id
 * 19/Apr/2005 DVG #DG190 GE Money AU DATX Equifax upload
* 03/Mar/2005 DVG #DG164 #1054  Prospera - CR001
    new web service created from their wsdl file and called from:
     DocumentGenerationHandler.java
 */

import java.util.*;

import org.w3c.dom.*;
import MosSystem.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.util.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.xml.*;

/**
 *  Extracts upload style documents including Approvals, Denials and MIRequests.
 *  An upload rules file is used to provide meta information .
 */
public class UploadExtractor implements Extractor
{
   private ExtractDom extractTemplate;
   private int type;
   private SessionResourceKit srk;
   private SysLogger logger;
 //  private DocumentGenerationHandler dgHandler;
 //  private DocumentProfile profile;


   public static boolean debug = true;

   public UploadExtractor(SessionResourceKit srk, ExtractDom dom)
   {
     this.srk = srk;
     logger = srk.getSysLogger();
     extractTemplate = dom;
   }

   public String extractXmlString(DocumentGenerationHandler dgHandler) throws ExtractException
   {
      try
      {
        int dealId = dgHandler.getDealId();
        DocumentProfile profile = dgHandler.getDocumentProfile();

        int copyId = -1;
        int dgTypeId = profile.getDocumentTypeId();

        Deal deal = null; //#DG216
        if(dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_DEAL_UPLOAD ||
            dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_DEAL_APPROVAL ||
            dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_DEAL_DENIAL ||
            dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_TDCT_CAPLINK ||
            dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_PROSPERA_UPLOAD ||  //#DG164
            dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD)                   // BMO to CCAPS 1.2
        {

          MasterDeal md = new MasterDeal(srk, null, dealId);
          copyId = md.getGoldCopyId();
          deal = new Deal(srk, null, dealId, copyId); //#DG216 moved
        }
        else if (
                 dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED ||
                 dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED ||
                 dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_EZENET_BASE ||
                 dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE ||
                 dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD ||  //#DG190 //#DG216 moved
                 dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_CLOSING_UPLOAD ||  //#DG242
                 dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_FUNDING_UPLOAD              // XD to MCAP
                )
        {
          deal = dgHandler.getDeal(); //#DG216
          copyId = dgHandler.getDealCopyId();
        }

        DocumentContext ctx = new DocumentContext(dgHandler.getDocumentProfile(), deal);
        UploadBuilder generator = new UploadBuilder(extractTemplate, srk);

        // why the hell we go with dealid na copy id when we already have deal object on hand  ?!?!?!?!?!
        // message posted by zivko
        Document dom = generator.generateDocument(dealId, copyId, dgHandler.getLanguagePreferenceId() );
        if(dgTypeId == Dc.DOCUMENT_GENERAL_TYPE_DEAL_APPROVAL)
        {
          logger.trace("DOCPREP: BrokerApproval Request Entry ->" + deal.getDealId());
          // zivko:marker this is the spot where we want to test the deal status
          // and see if the deal is collapsed or denied. in case that deal has one of these
          // two statuses we should not go to retrieve conditions.
          switch(deal.getStatusId()){
            case Mc.DEAL_COLLAPSED : {
              // do nothing
              break;
            }
            case Mc.DEAL_DENIED : {
              // do nothing
              break;
            }
            default : {
              BrokerApprovalConditionHandler bach = new BrokerApprovalConditionHandler(srk);
              bach.buildBrokerApprovalConditionNodes(deal,dom,generator.getDealElement(), ctx.getFormat());
              break;
            }
          }
        }

        if(  isFXLinkType(deal.getInstitutionProfileId(), deal.getSystemTypeId() )  ){
          Element documentTypeNode = dom.createElement("documentType");
          DomUtil.appendString(documentTypeNode, determineDocumentTypeNodeValue(deal) );
          dom.getFirstChild().appendChild(documentTypeNode);
        }

        return generator.toXMLString(dom, dgHandler.getLanguagePreferenceId());
      }
      catch(Exception e)
      {
        StringUtil.stack2string(e);
        String msg = e.getMessage();
        throw new ExtractException("DOCPREP: UploadExtractor failed: " + msg);
      }
   }

  private boolean isFXLinkType(int institutionProfileID, int pType) throws Exception
  {
    String fxlinkTypes = PropertiesCache.getInstance().getProperty(institutionProfileID, "com.basis100.fxlink.types");
    // if the property does not exist return false
    if(fxlinkTypes == null){
      return false;
    }
    try
    {
      StringTokenizer st = new StringTokenizer(fxlinkTypes, ",");
      String token;

      while (st.hasMoreTokens())
      {
        token = st.nextToken();

        if( Integer.parseInt(token.trim() ) == pType){
          return true;
        }
      }
      return false;
    }
    catch (Exception e)
    {
      throw new Exception("ERROR - wrong property value(s) for property: com.basis100.fxlink.types ");
    }
  }

   private String determineDocumentTypeNodeValue(Deal pDeal){
      int lStat = pDeal.getStatusId();
      switch(lStat)
      {
        case Mc.DEAL_APPROVED:
        case Mc.DEAL_COMMIT_OFFERED:
        case Mc.DEAL_OFFER_ACCEPTED:
        case Mc.DEAL_CONDITIONS_OUT:
        case Mc.DEAL_CONDITIONS_SATISFIED:
        case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT:
        case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED:
        case Mc.DEAL_POST_DATED_FUNDING:
        case Mc.DEAL_FUNDED:
        case Mc.DEAL_TO_SERVICING:
         return "APPROVAL";

        case Mc.DEAL_PRE_APPROVAL_OFFERED:
         return "PREAPPROVAL";

        case Mc.DEAL_DENIED:
        case Mc.DEAL_COLLAPSED:
         return "DENIED";

        default:
         return "MESSAGE";
      }

   }

   private int determineRequiredLanguage(Deal pDeal){

      return Mc.LANGUAGE_PREFERENCE_ENGLISH;
   }
// it appears that this method has not been used at all
// for now, comment it out.
/*
   public static String createUploadString(DealPK pk, SessionResourceKit srk, File file) throws Exception
   {
      ExtractDom parsed = new ExtractDom(file);

      UploadBuilder generator = new UploadBuilder(parsed,srk);

      org.w3c.dom.Document d = generator.generateDocument(pk.getId(),pk.getCopyId());

      XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();

      return writer.printString(d);
   }
*/

}


