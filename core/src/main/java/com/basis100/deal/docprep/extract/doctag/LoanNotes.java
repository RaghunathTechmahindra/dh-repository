package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class LoanNotes extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());
    
    Deal deal = (Deal)de;
    FMLQueryResult fml = new FMLQueryResult();
    
    try
    {
      List out = new ArrayList();
      ConditionHandler ch = new ConditionHandler(srk);
      //======================================================================================
      // PROGRAMMER'S NOTE: Deal table - column pricingprofileid does not contain
      // pricing profile id, it contains pricing rate inventory id. So the path goes:
      // take pricingprofileid from deal, based on that key find the pricingrateinventoryid
      // in the pricingrateinventory table. From that record pick pricingprofileid value and
      // that is the key to search in the pricingprofile table.
      // Method findByPricingRateInventory in the PricingProfile class does exactly that.
      //=====================================================================================
      //PricingProfile pp = new PricingProfile(srk,deal.getPricingProfileId());
      PricingProfile pp = new PricingProfile(srk);
      pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

      String ratecode = pp.getRateCode();

      if(ratecode != null && !ratecode.equals("UNCNF")
         && deal.getLineOfBusinessId() == Mc.LOB_A && deal.getBlendedRate() == 0)
      {
        addTo(out,ch.getVariableVerbiage(deal,"Auto Adjust",lang));

        DealFee df = new DealFee(srk,null);
        Collection c = df.findByDealAndType((DealPK)deal.getPk(), Mc.FEE_TYPE_BUYDOWN_FEE);

        Iterator it = c.iterator();

        while(it.hasNext())
        {
          df = (DealFee)it.next();

          if(df.getFeeAmount() <= 0)
           it.remove();
        }

        if(!c.isEmpty() )
        {
          int spi = PicklistData.getPicklistIdValue("SpecialFeature","Progress");

          if(deal.getSpecialFeatureId() == spi)
          {
             addTo(out,ch.getVariableVerbiage(deal,"Buydown Progress",lang));
          }
          else
          {
             addTo(out,ch.getVariableVerbiage(deal,"Buydown Advance",lang));
          }

          addTo(out,ch.getVariableVerbiage(deal,"Buydown Details",lang));
          addTo(out,ch.getVariableVerbiage(deal,"Buydown",lang));
        }

      }
      else if(deal.getBlendedRate() > 0)
      {
       addTo(out,ch.getVariableVerbiage(deal,"Blended Rate",lang));
      }

      int dpid = deal.getDealPurposeId();
   
      if(dpid == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT
          || dpid == Mc.DEAL_PURPOSE_PORTABLE
          || dpid == Mc.DEAL_PURPOSE_PORTABLE_DECREASE
          || dpid == Mc.DEAL_PURPOSE_PORTABLE_INCREASE
          || dpid == Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT
          || dpid == Mc.DEAL_PURPOSE_REPLACEMENT)
      {

          addTo(out,ch.getVariableVerbiage(deal,"Existing",lang));
      }


      if(deal.getTaxPayorId() == Mc.TAX_PAYOR_LENDER)
      {
       addTo(out,ch.getVariableVerbiage(deal,"Taxes",lang));
      }
      else
      {
       addTo(out,ch.getVariableVerbiage(deal,"No Taxes",lang));
      }

      MtgProd prod = new MtgProd(srk,null,deal.getMtgProdId());


      String sn = prod.getMPShortName();

      if(sn != null && sn.equals("VIP"))
      addTo(out,ch.getVariableVerbiage(deal,"Variable Payment",lang));


      Iterator oit = out.iterator();
      String curr = "";

      while(oit.hasNext())
      {
        curr += (String)oit.next() + format.carriageReturn();
      }

      fml.addValue(curr, fml.STRING);

      return fml;

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
     String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }


  private void addTo(List inl, String ins)
  {
   if(ins != null)
    if(inl != null)
      inl.add(ins);

  }


}
