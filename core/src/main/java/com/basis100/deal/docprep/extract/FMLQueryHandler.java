package com.basis100.deal.docprep.extract;

import org.w3c.dom.*;

import MosSystem.Mc;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.*;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.picklist.*;
import com.basis100.xml.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

import java.util.*;

/** MCM Impl Team
 * @version 1.3 20-Oct-2008 Bugfix:FXP22986 : Added langauge id parameter and added code to get picklist description from bxresources
 **/
public class FMLQueryHandler
{

  public static boolean debug = false;

  private SysLogger logger;
  private SessionResourceKit srk;

  private FMLQueryProcessor functionProcessor;

  //RESERVED WORDS:

  private static String CHILDREN = "children";
  private static String PARENTS  = "parents";
  private static String P_BOR    = "primaryborrower";
  private static String P_PROP   = "primaryproperty";
  private static String C_EHIST  = "currentEmployer";
  private static String BOR      = "borrowers";
  private static String PRP      = "properties";


  private static String OWNER    = "owner";


  public FMLQueryHandler(SessionResourceKit srk)
  {
    this.srk = srk;
    logger = srk.getSysLogger();
    functionProcessor =  new FMLQueryProcessor(srk);
  }


  public FMLQueryResult handleQuery(String name, Node tag, FMLQueryContext ctx)
  {
    List queryNodes = DomUtil.getNamedChildren(tag,"Query");
    FMLQueryResult res = null;

    if(ctx.isSection())
    {
      try
      {
        res = runSectionQuery(name, (Element)tag, ctx);
      }
      catch(Exception e)
      {

      }
    }
    else
    {
      Element function = (Element)DomUtil.getFirstNamedChild(tag,"Function");

      res = runQuery(name, queryNodes, ctx, ctx.getDocumentFormat());

      res = functionProcessor.processFunction(res, function, ctx);

    }

    return res;
  }

  //same as above for now - to change
  public FMLQueryResult handleSubQuery(String name, Element fnode, FMLQueryContext ctx)
  {
    List queryNodes = DomUtil.getNamedChildren(fnode,"Query");

    Element function = (Element)DomUtil.getFirstNamedChild(fnode,"Function");

    FMLQueryResult res = runQuery(name,queryNodes, ctx, ctx.getDocumentFormat());

    res = functionProcessor.processFunction(res, function, ctx);

    return res;
  }

  private FMLQueryResult runQuery(String name, List qnodes, FMLQueryContext ctx, DocumentFormat format)
  {

    ListIterator it = qnodes.listIterator();

    FMLQueryResult fml = new FMLQueryResult();
    FMLQueryResult temp = null;
    Element queryNode = null;

    while(it.hasNext())
    {
      try
      {
        queryNode = (Element)it.next();

        String queryType = queryNode.getAttribute("type");
        String queryId = queryNode.getAttribute("id");
        String path = queryNode.getAttribute("path");

        int fmlType = getFmlType(queryNode.getAttribute("format"));

        if(queryType.equals("code"))
        {
          String pkg = ctx.getCodePackage();

          if(pkg.length() > 0)
           path = pkg + "." + path;

          temp = runCodeQuery(queryNode, path, format, ctx, queryId);
        }
        else if(queryType.equals("picklist"))
        {
          temp = runPicklistQuery(queryNode, path, ctx, queryId, fml);
        }
        else if(queryType.equals("db"))
        {
          temp = runDBQuery(queryNode, path, fmlType, ctx, queryId, fml);
        }
        else if(queryType.equals("db_direct"))
        {
          temp = runDBDirect(queryNode, fmlType, ctx, queryId, fml);
        }
        else if(queryType.equals("condition"))
        {
          temp = runConditionQuery(queryNode, ctx, queryId);
        }

       if(temp == null) continue;

       if(!temp.hasNonEmptyValue())
          addDefault(temp, queryNode, fmlType, ctx, queryId);

       fml.add(temp);

      }
      catch(Exception e)
      {
       e.printStackTrace();//ALERT DEBUG ONLY
      }
    }

    fml.setName(name);

    return fml;
  }

  public FMLQueryResult runSectionQuery(String name, Element stag, FMLQueryContext ctx)throws ExtractException
  {
     Element queryNode = (Element)DomUtil.getFirstNamedChild(stag, "Query");

     try
     {

     String queryType = queryNode.getAttribute("type");
     String queryId = queryNode.getAttribute("id");
     String path = queryNode.getAttribute("path");

     if(queryType.equals("code"))
     {
       String pkg = ctx.getCodePackage();

       if(pkg.length() > 0)
         path = pkg + "." + path;

       return runCodeQuery(queryNode, path, ctx.getDocumentFormat(), ctx, queryId);
     }
     else if(queryType.equals("db"))
     {
       return runSectionDBQuery( queryNode, ctx.getDocumentFormat(), ctx, queryId);
     }

     }
     catch(NullPointerException npe)
     {
       throw new ExtractException("DOCPREP: No 'Query' or Query 'Type' found as child of Section Tag");
     }

     return new FMLQueryResult();
  }

  public FMLQueryResult runDBQuery(Element node, String path, int fmlType, FMLQueryContext ctx, String queryId, FMLQueryResult res)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();

    if(fmlType == FMLQueryResult.ENTITY)
    {
       if(path.equalsIgnoreCase(CHILDREN))
       {
         return retrieveChildEntities(ctx);
       }
    }
    else
    {
      String sql = buildSelect(path, node, ctx);

      String fieldVal = executeSingleFieldQuery(sql, ctx, res);

      //if no value is found but one must be associated with an id  ??ALERT
      if( fieldVal == null )
      {
        srk.getSysLogger().debug("DOCPREP: QUERY EMPTY: Type: db Node: " + node.getAttribute("path"));
        fieldVal = "";
        fmlType = fml.STRING;
      }

      fml.addValue(fieldVal,fmlType,queryId);

    }


    return fml;
  }


  public FMLQueryResult runDBDirect(Element node,  int fmlType, FMLQueryContext ctx, String queryId, FMLQueryResult res)throws ExtractException
  									//add new filed <ctx> and <res>
  {
    FMLQueryResult fml = new FMLQueryResult();

    String sql = node.getAttribute("path");

    String fieldVal = executeSingleFieldQuery(sql, ctx, res);

    //if no value is found but one must be associated with an id  ??ALERT
    if( fieldVal == null )
    {
      fieldVal = "";
      fmlType = fml.STRING;
    }

    fml.addValue(fieldVal,fmlType,queryId);

    return fml;
  }

  public FMLQueryResult runPicklistQuery(Element qnode, String path, FMLQueryContext ctx, String queryId, FMLQueryResult res)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();

    String sql = buildSelect(path, qnode, ctx);  //Alert node goes at null

    String idval = executeSingleFieldQuery(sql, ctx, res);
    /**BugFix:FXP22986 : Added language preference id*/
    String desc = getPicklistDescription(qnode, path, idval,ctx.getLanguage());

    fml.addValue(desc, fml.TERM, queryId);

    return fml;
  }

  //ToDo query logic here: for now just a few special cases

  public FMLQueryResult runSectionDBQuery(Element qnode, DocumentFormat format,
                                      FMLQueryContext ctx, String id)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      String path = qnode.getAttribute("path");
      String contextName = ctx.getName();
      IEntityBeanPK pk = ctx.getEntityCtx().getPk();

      if(ctx.getName().equals("Deal"))
      {
        if(path.equalsIgnoreCase(P_BOR))
        {
          Borrower pb = new Borrower(srk,null);
          pb.findByPrimaryBorrower(pk.getId(), pk.getCopyId()) ;
          fml.addValue(pb);
          return fml;
        }
        else if(path.equalsIgnoreCase(P_PROP))
        {
          Property pp = new Property(srk,null);
          pp.findByPrimaryProperty(pk.getId(), 
								   pk.getCopyId(),
								   srk.getExpressState().getDealInstitutionId()) ;
          fml.addValue(pp);
          return fml;
        }
        else if(path.equalsIgnoreCase(BOR))
        {
          Borrower pb = new Borrower(srk,null);
          fml.addAll((List)pb.findByDeal((DealPK)pk));
          return fml;
        }
        else if(path.equalsIgnoreCase(PRP))
        {
          Property pp = new Property(srk,null);
          fml.addAll((List)pp.findByDeal((DealPK)pk));
          return fml;
        }
      }
      else if(ctx.getName().equals("Borrower"))
      {
        if(path.equalsIgnoreCase(C_EHIST))
        {
          EmploymentHistory eh = new EmploymentHistory(srk);
          eh.findByCurrentEmployment((BorrowerPK)pk); ;
          fml.addValue(eh);
          return fml;
        }
      }

      throw new Exception("DB Section Query not handled.");

    }
    catch(Exception cnf)
    {
       String msg = cnf.getMessage();

       if(msg == null) msg = "DB Section Query: Unknown Reason.";

       srk.getSysLogger().debug("DOCPREP: QUERY ERROR:  " + msg);
       System.out.println("DOCPREP: QUERY ERROR: " + msg);
       fml.addValue("QUERY ERROR!", fml.STRING);
    }

    return fml;
  }


  //Note no Id is associated with the code query yet - todo

  public FMLQueryResult runCodeQuery(Element qnode, String clazz, DocumentFormat format,
                                      FMLQueryContext ctx, String id)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      Class queryclass = Class.forName(clazz);
      DocumentTagExtractor tagquery = (DocumentTagExtractor)queryclass.newInstance();

      fml = tagquery.extract(ctx.getEntityCtx(), ctx.getLanguage(), format, srk);
    }
    catch(Exception cnf)
    {
       srk.getSysLogger().debug("DOCPREP: QUERY ERROR: Type: code Location: " + clazz);
       System.out.println("DOCPREP: QUERY ERROR: Type: code Location: " + clazz);
       fml.addValue("QUERY ERROR!", fml.STRING);
    }

    return fml;
  }

  public FMLQueryResult runConditionQuery(Element query, FMLQueryContext ctx, String queryId)throws ExtractException
  {
    FMLQueryResult fml = new FMLQueryResult();

    fml.addString("", queryId);

    Deal deal = null;
    ConditionHandler ch = new ConditionHandler(srk);

    String conditionValue = null;

    if(ctx.getEntityCtx().getClassId() != ClassId.DEAL)
    {
      logger.error("DOCPREP: QUERY: Invalid context for condition query");
      return fml;
    }
    else
    {
      deal = (Deal)ctx.getEntityCtx();
    }

    String path = query.getAttribute("path");

    if(path != null)
     path = path.trim();

    if(path.length() == 0)
    {
      logger.error("DOCPREP: QUERY: Invalid path for condition query");
      return fml;
    }

    int condId = -1;

    try{ condId = Integer.parseInt(path); } catch(NumberFormatException e){;}

    if(condId < 0)
    {
      try
      {
        conditionValue = ch.getVariableVerbiage(deal, path, ctx.getLanguage());

        if(conditionValue != null)
        {
          fml.addValue(conditionValue, fml.STRING, queryId);
          return fml;
        }
      }
      catch(Exception e)
      {
        logger.error("DOCPREP: QUERY: Failed condition query.");
        logger.error(e);
        return fml;
      }
    }
    else
    {
      try
      {
        conditionValue = ch.getVariableVerbiage(deal, condId, ctx.getLanguage());

        if(conditionValue != null)
        {
          fml.addValue(conditionValue, fml.STRING, queryId);
          return fml;
        }
      }
      catch(Exception e)
      {
         logger.error("DOCPREP: QUERY: Invalid conditionId for condition query");
         return fml;
      }
    }

    return fml;

  }


  public String buildSelect(String path, Element queryNode, FMLQueryContext ctx)
  {
     try
     {
        List conds = conditionalStmts(queryNode);
        List meta  = stmtMetaInfo(queryNode);

        List table = new ArrayList();
        List field = new ArrayList();

        StringTokenizer tk = new StringTokenizer(path,"/");

        while(tk.hasMoreTokens())
        {
          String tval = tk.nextToken();

          int ind = tval.indexOf('.');

          if(ind != -1)
          {
             table.add( tval.substring(0,ind));
             field.add(tval.substring(ind + 1));
          }
        }

        String tableStr = (String)table.get(0);

        StringBuffer sql = new StringBuffer();

       // System.out.println(path + " : " + tableStr + " : " +ctx.getName());

        //special cases
        if(tableStr.equalsIgnoreCase(P_BOR) && ctx.getName().equals("Deal"))
        {
          sql.append("Select ").append((String)field.get(0)).append(" from Borrower ");
          sql.append(ctx.getWhere());
          sql.append(" AND primaryBorrowerFlag = 'Y'");

          return sql.toString();
        }
        else if(tableStr.equalsIgnoreCase(P_PROP) && ctx.getName().equals("Deal"))
        {
          sql.append("Select ").append((String)field.get(0)).append(" from Property ");
          sql.append(ctx.getWhere());
          sql.append(" AND primaryPropertyFlag = 'Y'");

          return sql.toString();
        }
        else if(tableStr.equalsIgnoreCase(C_EHIST) && ctx.getName().equals("Borrower"))
        {
          sql.append("Select ").append((String)field.get(0)).append(" from EmploymentHistory ");
          sql.append(ctx.getWhere());
          sql.append(" AND employmentHistoryStatusId = ").append(Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT);

          return sql.toString();
        }


        int end = table.size() - 1;


        List condStmts = new ArrayList();

        String selectField = null;
        String whereField = null;
        String tableField = null;

        String ctxWhere = ctx.getWhere();
        //String ctxCopy = parseCtxCopy(ctxWhere);     //future use
       // if(ctxCopy == null) ctxCopy = "";

        for(int i = end; i >= 0 ; i--)
        {
          condStmts.add(condStmt((String)field.get(i),conds));
          selectField = (String)field.get(i);
          tableField = (String)table.get(i);

          sql.append("Select ");
          sql.append( selectField );
          sql.append(" from ");
          sql.append( tableField );

          if(i != 0)
          {
            sql.append( " where ");

            whereField = (String)field.get(i - 1);

            whereField = statementMetaInfo(whereField,tableField, meta);
            sql.append(whereField);
            sql.append(" = (");
          }
        }

        sql.append(ctxWhere).append(" ");

        if(condStmts.size() == 1)
         sql.append((String)condStmts.get(0));

        for(int j = 0; j < end; j++)
        {
          sql.append(") ");
          sql.append((String)condStmts.get(j));
        }
        //sql.append("Order By Copyid desc");
   
        return sql.toString();
     }
     catch(Exception e)
     {

       String msg = "QUERYERROR!! - " + queryNode.getNodeName() + "sql:" + queryNode.getAttribute("path");

       if(e.getMessage() != null)
        msg += " " + e.getMessage();
       else
        msg += " " + e.getClass().getName();

       logger.error(msg);
       System.out.println(msg);
     }

     return "";

  }
     //doesnt account for sandwiched vars
  public String parseCtxCopy(String where)
  {
    String whereUpper = where.toUpperCase();
    int and = whereUpper.indexOf("AND");
    int copy = whereUpper.indexOf("COPYID");
    int endand = whereUpper.lastIndexOf("AND");
    String copyStr = null;

    if(and != -1 && copy != -1)
    {
      if(and == endand)
       copyStr = where.substring(and);
      else if(copy > endand)
      {
        copyStr = where.substring(endand);
      }
      else if(copy < endand)
      {
        copyStr = where.substring(and, endand);
      }
    }


    return copyStr;
  }


  public FMLQueryResult retrieveChildEntities(FMLQueryContext ctx)
  {
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      List vals =(List)ctx.getEntityCtx().getChildren();

      Iterator it = vals.iterator();

      while(it.hasNext())
      {
        fml.addValue((DealEntity)it.next());
      }

    }
    catch(Exception e)
    {
      return fml;
    }

    return fml;
  }


  private String executeSingleFieldQuery(String sql, FMLQueryContext ctx, FMLQueryResult res)
  {
    //------add by clement---------    
    FMLQueryProcessor fmlf = new FMLQueryProcessor(srk);  
    sql = fmlf.replacePattern(res, sql, ctx);
    //------------------------------
    String val = null;

    JdbcExecutor jExec = srk.getJdbcExecutor();

    try
    {
        int key = jExec.execute(sql);

        for (; jExec.next(key); )
        {
            val = jExec.getString(key,1);
            break;
        }

        jExec.closeData(key);
      }
      catch (Exception e)
      {
         srk.getSysLogger().error("DOCPREP: QUERY FAILED: Sql: " + sql);
         srk.getSysLogger().error(e);
         return null;
      }

     return val;
  }



  public String condStmt(String targ, List conds)
  {
    if(conds == null || conds.isEmpty() || targ == null)
    {
      return "";
    }

    Iterator it = conds.iterator();
    ConditionalStatement cs = null;

    while(it.hasNext())
    {
      cs = (ConditionalStatement)it.next();

      if(cs.target.equalsIgnoreCase(targ))
       return cs.stmt;
    }

    return "";
  }



  public List conditionalStmts(Element node)
  {
    List all = DomUtil.getNamedChildren(node, "ConditionalStatement");

    List out = new ArrayList();

    Iterator it = all.iterator();
    Element current = null;

    while(it.hasNext())
    {
      current = (Element)it.next();
      String type  = current.getAttribute("type") ;
      String value = current.getAttribute("value");
      String target  = current.getAttribute("target");

      out.add(new ConditionalStatement(type,value,target));
    }

    return out;
  }

  public List stmtMetaInfo(Element node)
  {
    List all = DomUtil.getNamedChildren(node, "StatementMetaInfo");

    List out = new ArrayList();

    Iterator it = all.iterator();
    Element current = null;

    while(it.hasNext())
    {
      current = (Element)it.next();
      String type  = current.getAttribute("type") ;
      String value = current.getAttribute("value");
      String target  = current.getAttribute("target");

      out.add(new StatementMetaInfo(type,value,target));
    }

    return out;
  }

  public String statementMetaInfo(String in, String targ, List meta)
  {
    if(meta == null || meta.isEmpty() || targ == null)
    {
      return in;
    }

    Iterator it = meta.iterator();
    StatementMetaInfo cs = null;

    while(it.hasNext())
    {
      cs = (StatementMetaInfo)it.next();

      if(cs.target.equalsIgnoreCase(targ))
      {
        if(cs.type.equals("mapto"))
        {
          return cs.info;
        }
      }
    }

    return in;
  }

/** @version 1.3 20-Oct-2008 Bugfix:FXP22986 : Added langauge id parameter and added code to get picklist description from bxresources */
 
  
  public String getPicklistDescription(Element qnode, String path, String id,int lang )
  {

    String tablename = qnode.getAttribute("table");

    if(tablename == null || tablename.trim().length() == 0)
    {
      path = path.toUpperCase() ;

      if(path.endsWith("ID/"))
        path = path.substring(0, path.length()-1);

      int dx = path.lastIndexOf(".");

      if(dx == -1)dx = 0;

      path = path.substring(dx + 1,path.length()-2);
    }
    else
    {
      path = tablename;
    }
    String desc = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), path,id,lang); 
    
    return desc;
  }

/********************************************************************************************************/ 
// /* public String getPicklistDescription(Element qnode, String path, String id )
//  {
//
//    String tablename = qnode.getAttribute("table");
//
//    if(tablename == null || tablename.trim().length() == 0)
//    {
//      path = path.toUpperCase() ;
//
//      if(path.endsWith("ID/"))
//        path = path.substring(0, path.length()-1);
//
//      int dx = path.lastIndexOf(".");
//
//      if(dx == -1)dx = 0;
//
//      path = path.substring(dx + 1,path.length()-2);
//    }
//    else
//    {
//      path = tablename;
//    }
///***********MCM Impl Team Bug Fix : added institution profileid **************************/ 
//    String desc = PicklistData.getDescription(srk.getExpressState().getDealInstitutionId(),path,id);
///***********MCM Impl Team Bug Fix : added institution profileid **************************/    
//    if(desc==null || desc.equals(" "))
//    {
//        desc=PicklistData.getDescription(path,id);
//    }
//    return desc;
//  }
//*/


  private boolean addDefault(FMLQueryResult current, Element qnode, int fmlType, FMLQueryContext ctx, String queryId)
  {
     if(current == null || qnode == null || !qnode.hasChildNodes())
      return false;

     Element defnode = (Element)DomUtil.getFirstNamedChild(qnode, "Default");

     if(defnode == null) return false;

     String type = defnode.getAttribute("type");
     String path = defnode.getAttribute("path");

     if(type != null && type.trim().length() != 0)
     {
       if(type.equals("static"))
       {
          current.addValue(path,fmlType,queryId);
          return true;
       }
       //else if - other types todo - queries ...

     }


   return false;

  }

  public static int getFmlType(String in)
  {
    if(in.equalsIgnoreCase("String"))
    {
      return FMLQueryResult.STRING;
    }
    else if(in.equalsIgnoreCase("Term"))
    {
     return FMLQueryResult.TERM;
    }
    else if(in.equalsIgnoreCase("Currency"))
    {
     return FMLQueryResult.CURRENCY;
    }
    else if(in.equalsIgnoreCase("Rate"))
    {
     return FMLQueryResult.RATE;
    }
    else if(in.equalsIgnoreCase("Ratio"))
    {
     return FMLQueryResult.RATIO;
    }
    else if(in.equalsIgnoreCase("Date"))
    {
     return FMLQueryResult.DATE;
    }
    else if(in.equalsIgnoreCase("YearsAndMonths"))
    {
     return FMLQueryResult.YEARS_AND_MONTHS;
    }
    else if(in.equalsIgnoreCase("Entity"))
    {
     return FMLQueryResult.ENTITY;
    }
    else if(in.equalsIgnoreCase("phonenumber"))
    {
     return FMLQueryResult.PHONE_NUMBER;
    }

    return FMLQueryResult.STRING;

  }


  class ConditionalStatement
  {
    String type;
    String stmt;
    String target;

    public ConditionalStatement(String type, String stmt, String target)
    {
      this.type = type;
      this.stmt = stmt;
      this.target = target;
    }
  }

  class StatementMetaInfo
  {
    String type;
    String info;
    String target;

    public StatementMetaInfo(String type, String info, String target)
    {
      this.type = type;
      this.info = info;
      this.target = target;
    }
  }

  public static void main(String[] args)  throws Exception
  {
  System.out.println("Init ...");
  /*PropertiesCache.addPropertiesFile("c:\\mos\\admin\\mossys.properties");
  ResourceManager.init("c:\\mos\\admin\\","MOS");
  SessionResourceKit srk = new SessionResourceKit("0");
  System.out.println("Init end.");
  FMLQueryResult fml = new FMLQueryResult();
  Deal deal = new Deal(srk,null,9645,1);
  Borrower b = new Borrower(srk,null);
  b.findByFirstBorrower(deal.getDealId(), deal.getCopyId()); */


  }





}
