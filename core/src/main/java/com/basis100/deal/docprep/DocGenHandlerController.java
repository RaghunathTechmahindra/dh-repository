package com.basis100.deal.docprep;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

import java.util.*;
import org.w3c.dom.*;
import com.basis100.xml.*;
import com.basis100.resources.PropertiesCache;
import org.apache.commons.logging.Log; 
import org.apache.commons.logging.LogFactory; 

public class DocGenHandlerController
{
  private static final int INCREASE_SIZE = 10;
  private int minimumNumberOfThreads;
  private int maximumNumberOfThreads;
  private int maxNumAllowedWorkers;
  private Vector workerIdVect;
  private Vector failedWorkerHolder;
  private int numberOfFailedThreads;
  private int numberOfSuccessfulThreads;
  private int totalNumberOfThreads;

  private static Log logger = LogFactory.getLog(DocGenHandlerController.class); 
  
  public DocGenHandlerController()
  {

    numberOfFailedThreads = 0;
    numberOfSuccessfulThreads = 0;
    totalNumberOfThreads = 0;

    // ------------------------------- read values from property file
    minimumNumberOfThreads = Integer.parseInt(PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.minimum.number.of.working.threads", "10"));
    maximumNumberOfThreads = Integer.parseInt(PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.maximum.number.of.working.threads", "100"));
    maxNumAllowedWorkers = Integer.parseInt(PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.number.of.worker.threads","20"));
    // -------------------------------
    workerIdVect = new Vector();
    failedWorkerHolder = new Vector();
  }

  public void addToFailedList(HandlerWorkerHolder pHold){
    synchronized(failedWorkerHolder){
      failedWorkerHolder.addElement(pHold);
      numberOfFailedThreads++;
    }
  }

  public String extractStatistics(){
    StringBuffer rv = new StringBuffer("");
    rv.append("<statistics>");
    rv.append("<activeThreads>").append(workerIdVect.size()).append("</activeThreads>");
    rv.append("<failedThreads>").append(numberOfFailedThreads).append("</failedThreads>");
    rv.append("<totalNumberOfThreads>").append(totalNumberOfThreads).append("</totalNumberOfThreads>");
    rv.append("<numberOfSuccessfulThreads>").append(numberOfSuccessfulThreads).append("</numberOfSuccessfulThreads>");
    rv.append("<wrkThreads>").append(maxNumAllowedWorkers).append("</wrkThreads>");
    rv.append("<maxThreads>").append(maximumNumberOfThreads).append("</maxThreads>");
    rv.append("<minThreads>").append(minimumNumberOfThreads).append("</minThreads>");
    rv.append("</statistics>");
    return rv.toString();
  }

  public String extractFailedInfo(){
    synchronized(failedWorkerHolder){
      StringBuffer sb = new StringBuffer("<FailedInfo>");
      Iterator it = failedWorkerHolder.iterator();
      while(it.hasNext()){
        sb.append(((HandlerWorkerHolder) it.next()).generateInfo());
      }
      sb.append("</FailedInfo>");
      return sb.toString();
    }
  }

  public boolean isThereFreeSpot(){
    if(maxNumAllowedWorkers > this.getSize() )
	{
      return true;
    }
	else
	{
		logger.info("the number of worker thread reached its limit :  " + 
			"(" + this.getSize() + "/" + maxNumAllowedWorkers + ")");
		return false;
	}
  }

  public int getNumberOfFreeSpots(){
    return maxNumAllowedWorkers - getSize() ;
  }

  public int getSize(){
    return workerIdVect.size();
  }

  public void removeWorker(HandlerWorkerHolder pHold){
    synchronized(workerIdVect){
      workerIdVect.remove(pHold);
      numberOfSuccessfulThreads++;
    }
  }

  public void addWorker(HandlerWorkerHolder pWorker){
    synchronized(workerIdVect){
      workerIdVect.addElement(pWorker);
      totalNumberOfThreads++;
    }
  }

  public void setMaxNumAllowedWorkers(int pNum){
    maxNumAllowedWorkers = pNum;
  }

  public int getMaxNumAllowedWorkers(){
    return maxNumAllowedWorkers;
  }

  public void increaseSize(){
    maxNumAllowedWorkers += this.INCREASE_SIZE;
    if(maxNumAllowedWorkers > maximumNumberOfThreads){
      maxNumAllowedWorkers = maximumNumberOfThreads;
    }
  }

  public void decreaseSize(){
    maxNumAllowedWorkers -= this.INCREASE_SIZE;
    if(maxNumAllowedWorkers < minimumNumberOfThreads){
      maxNumAllowedWorkers = minimumNumberOfThreads;
    }
  }

}
