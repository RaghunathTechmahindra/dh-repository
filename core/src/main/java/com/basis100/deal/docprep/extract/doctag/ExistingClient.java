package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
 import com.basis100.picklist.*;
import com.basis100.deal.calc.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class ExistingClient extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    
    Borrower primary = null;

    try
    {

      if(de.getClassId() == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        primary = new Borrower(srk,null);
        primary = primary.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
      }

      if(primary != null)
      {
        val = primary.getExistingClient();
        fml.addValue(val, fml.TERM);
      }


    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
