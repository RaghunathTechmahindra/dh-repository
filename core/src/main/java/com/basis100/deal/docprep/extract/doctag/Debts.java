package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

public class Debts extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    //Deal deal = (Deal)de;
    String carriageReturn = System.getProperty("line.separator", "\n\r");

    int clsid = de.getClassId();
    ComponentResult result;
    boolean atStart;
    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        Liability liability = new Liability(srk, null);

        List liabs = (List)liability.findByDealAndPayoffType((DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_PRIOR_TO_ADVANCE);
        List liabsFromClose = (List)liability.findByDealAndPayoffType((DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_PRIOR_TO_ADVANCE_CLOSE);

        liabs.addAll(liabsFromClose);

        Iterator it = liabs.iterator();

        Liability liab = null;

        result = new ComponentResult();
        atStart = true;
        while(it.hasNext())
        {
          liab = (Liability)it.next();
          if(liab.getPercentOutGDS() < 2d){
            if(atStart){
              atStart = false;
            }else{
              result.addResult(", ",fml.STRING);
            }
            result.addResult(liab.getLiabilityDescription(),fml.STRING);
            result.addResult(" debt of ",fml.TERM);
            result.addResult(liab.getLiabilityAmount(),fml.CURRENCY);
            result.addResult(carriageReturn,fml.STRING);
          }
        }
        fml.addComponentResult(result);
      }else if(clsid == ClassId.LIABILITY){
        Liability lLiab = (Liability)de;

        if(lLiab.getPercentOutGDS() < 2d){
          result = new ComponentResult();
          result.addResult(lLiab.getLiabilityDescription(),fml.STRING);
          result.addResult(" debt of ",fml.TERM);
          result.addResult(lLiab.getLiabilityAmount(),fml.CURRENCY);
          result.addResult("  ",fml.STRING);
          fml.addComponentResult(result);
        }
      }else if(clsid == ClassId.BORROWER){
        Borrower borrower = (Borrower)de;
        Liability liability = new Liability(srk, null);
        Collection c = liability.findByBorrower((BorrowerPK)borrower.getPk());

        if (!c.isEmpty())
        {
            double amt;
            Liability oneLiab = null;
            StringBuffer buff = new StringBuffer();

            Iterator i = c.iterator();

            atStart = true;
            while (i.hasNext())
            {
                oneLiab = (Liability)i.next();

                if (oneLiab.getLiabilityPayOffTypeId() == Mc.LIABILITY_PAYOFFTYPE_PRIOR_TO_ADVANCE ||
                   oneLiab.getLiabilityPayOffTypeId() == Mc.LIABILITY_PAYOFFTYPE_PRIOR_TO_ADVANCE_CLOSE)
                {
                    amt = oneLiab.getLiabilityAmount();
                    if(oneLiab.getPercentOutGDS() < 2d){
                      if(atStart){
                        atStart = false;
                      }else{
                        buff.append(", ");
                      }

                      buff.append(oneLiab.getLiabilityDescription());
                      buff.append(lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? " debt of " : " au montant de ");
                      buff.append(DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang));
                      buff.append(format.carriageReturn());
                    }
                }
            }
            fml.addString( buff.toString() );
        }
      }
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
/*
  private String cleanLastComma(String inputStr){
    if(inputStr != null){
      String processStr = inputStr.trim();
      if (inputStr.endsWith(",")){
        processStr = processStr.substring(0,processStr.length()-1);
        return processStr;
      }
    }
    return inputStr;
  }
*/
}
