package com.basis100.deal.docprep.extract.data;
/**
 * 19/Jul/2006 DVG #DG466 #3021  CR 309 - combination of DJ deal summary (DJ_DealSummaryLite.xsl) and CR 120.
 * 24/May/2006 SL         #3021  CR 309  funcional spec for the CR 309 (changes to document CR 120)
 * 03/May/2006 DVG #DG412 #3022  CR 311  funcional spec for the CR 311 (changes to document CR 053)
 * 25/Apr/2005 DVG #DG200 #1231  DJ - CR120 - change reference of deal.firstpaymentdatemonthly to deal.firstpaymentdate
*      add new field firstpaymentdate
 */

import java.util.*;

import org.w3c.dom.*;
import MosSystem.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.xml.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;

public final class DJSpecialExtractor extends DealExtractor implements java.awt.event.ActionListener
{
  private static final int LIABILITY_GROUP_OTHER = 0;
  private static final int LIABILITY_GROUP_CREDIT_CARD = 1;
  private static final int LIABILITY_GROUP_LOANS = 2;
  private static final int LIABILITY_GROUP_TBD = 10;
/*
  private static final int LIABILITY_GROUP_MUNICIPALITY = 3;              // not used
  private static final int LIABILITY_GROUP_HEATING = 4;                   // not used
  private static final int LIABILITY_GROUP_CONDO = 5;                     // not used
*/
  // cumulatives for assets
  protected double totAssetCash ;                                          // this inclueds assets of type 0. 9, or 13 and included in net worth
  protected double totAssetProperty ;                                      // assets type 3, 5, 7
  protected double totAssetVehicle ;                                       // asset type 4
  protected double totAssetStocks ;                                        // asset type 6
  protected double totAssetRrsp ;                                          // asset type 1
  protected double totAssetGift ;                                          // asset type 2
  protected double totAssetOther ;                                         // asset type 8
  protected double totAssetHolding ;                                       // asset type 10
  protected double totAssetHouseHoldGoods ;                                // asset type 11
  protected double totAssetLifeInsur ;                                     // asset type 12
  private double totalAssetSide ;                                          // this one holds cumulative data for assets

  // cumulatives for liabilities
  protected double totLiabCreditCardMonthly;
  protected double totLiabCreditCard;
  protected double totLiabMtgMonthly;
  protected double totLiabMtg;
  protected double totLiabLeaseMonthly;
  protected double totLiabLease;
  protected double totLiabChildMonthly;
  protected double totLiabChild;
  protected double totOtherLiabMonthly;
  protected double totOtherLiab;

  protected double totalMonthlyLiabilities;
  protected double totalLiabilitySide;

  // ====
  protected double totalGDS;
  protected double totalTDS;
  protected double totalSumIncome;
  protected double totalSumLiability;
  protected double totalSumAsset;
  protected double totalNetWorth;
  protected double totMonthlySumIncludeInTDS;

  protected Vector assetListedVector;
  //============================================================================
  // implemenatation of abstract methods
  //============================================================================
  public String extractXMLData(DealEntity de, int pLang, DocumentFormat pFormat, SessionResourceKit pSrk) throws Exception
  {
        ratioFieldList = DocumentGenerationManager.getInstance().getRatioFieldList();
        initializeSumRegisters();   // this must be the first statement since we have to initialize these variables
        addActionListener(this);
        deal = (Deal) de;
        lang = pLang;
        format = pFormat;
        srk = pSrk;

        String goTest = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.filogix.zivko.test","N");
        if( goTest.equalsIgnoreCase("y") ){
          generateDataDoc();
        }else{
          buildDataDoc();
        }

        XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

        if (pLang == Mc.LANGUAGE_PREFERENCE_FRENCH){
          xmlwr.setEncoding("iso-8859-1");
        }
        else    {
          xmlwr.setEncoding("UTF-8")    ;
        }

        doFinalTags();
        return xmlwr.printString(doc);

  }

  protected void generateDataDoc() throws Exception
  {

    doc = XmlToolKit.getInstance().newDocument();
    Element root = doc.createElement("Document");
    root.setAttribute("type","1");
    root.setAttribute("lang","" + lang);

    specElem = doc.createElement("specialRequirementTags");

    Element dealNode = createSubTree(doc,"Deal",srk,deal,true,true,lang, true);
    //Element dealNode = createSubTree(doc,"Deal",srk,deal,true,true,lang, false);
    root.appendChild( dealNode ) ;
    doc.appendChild(root);
  }


  // ------------- the following assets and liabilities are used in DJ_119 -------------
  protected void doFinalTags(){
    // assets
    totalAssetSide += totAssetCash;
    totalAssetSide += totAssetProperty;
    totalAssetSide += totAssetVehicle;
    totalAssetSide += totAssetStocks;
    totalAssetSide += totAssetRrsp;
    totalAssetSide += totAssetGift;
    totalAssetSide += totAssetOther;
    totalAssetSide += totAssetHolding;
    totalAssetSide += totAssetHouseHoldGoods;
    totalAssetSide += totAssetLifeInsur;

    addTo(specElem,"TotalAssetCash", formatCurrency(totAssetCash));
    addTo(specElem,"TotAssetProperty", formatCurrency(totAssetProperty));
    addTo(specElem,"TotAssetVehicle", formatCurrency(totAssetVehicle));
    addTo(specElem,"TotAssetStocks", formatCurrency(totAssetStocks));
    addTo(specElem,"TotAssetRrsp" ,formatCurrency(totAssetRrsp));
    addTo(specElem,"TotAssetGift" ,formatCurrency(totAssetGift));
    addTo(specElem,"TotAssetOther" ,formatCurrency(totAssetOther));
    addTo(specElem,"TotAssetHolding", formatCurrency(totAssetHolding));
    addTo(specElem,"TotAssetHouseHoldGoods", formatCurrency(totAssetHouseHoldGoods));
    addTo(specElem,"TotAssetLifeInsur", formatCurrency(totAssetLifeInsur));
    addTo(specElem,"TotalAssetSide" ,formatCurrency(totalAssetSide));

    // liabilities
    totalMonthlyLiabilities += totLiabCreditCardMonthly;
    totalMonthlyLiabilities += totLiabMtgMonthly;
    totalMonthlyLiabilities += totLiabLeaseMonthly;
    totalMonthlyLiabilities += totLiabChildMonthly;
    totalMonthlyLiabilities += totOtherLiabMonthly;

    totalLiabilitySide += totLiabCreditCard;
    totalLiabilitySide += totLiabMtg;
    totalLiabilitySide += totLiabLease;
    totalLiabilitySide += totLiabChild;
    totalLiabilitySide += totOtherLiab;

    addTo(specElem,"TotLiabCreditCardMonthly", formatCurrency(totLiabCreditCardMonthly));
    addTo(specElem,"TotLiabMtgMonthly", formatCurrency(totLiabMtgMonthly));
    addTo(specElem,"TotLiabLeaseMonthly", formatCurrency(totLiabLeaseMonthly));
    addTo(specElem,"TotLiabChildMonthly", formatCurrency(totLiabChildMonthly));
    addTo(specElem,"TotOtherLiabMonthly", formatCurrency(totOtherLiabMonthly));
    addTo(specElem,"TotalMonthlyLiabilities", formatCurrency(totalMonthlyLiabilities));

    addTo(specElem,"totLiabCreditCard", formatCurrency(totLiabCreditCard));
    addTo(specElem,"totLiabMtg", formatCurrency(totLiabMtg));
    addTo(specElem,"totLiabLease", formatCurrency(totLiabLease));
    addTo(specElem,"totLiabChild", formatCurrency(totLiabChild));
    addTo(specElem,"totOtherLiab", formatCurrency(totOtherLiab));
    addTo(specElem,"totalLiabilitySide", formatCurrency(totalLiabilitySide));

    addTo(specElem,"totalGDS", formatCurrency(totalGDS));
    addTo(specElem,"totalTDS", formatCurrency(totalTDS));
    addTo(specElem,"totalSumIncome", formatCurrency(totalSumIncome));
    addTo(specElem,"totalSumLiability", formatCurrency(totalSumLiability));
    addTo(specElem,"totalSumAsset", formatCurrency(totalSumAsset));
    addTo(specElem,"totalNetWorth", formatCurrency(totalNetWorth));

  }

  protected void initializeSumRegisters(){
    // assets
    totAssetCash = 0d;
    totAssetProperty = 0d;
    totAssetVehicle = 0d;
    totAssetStocks = 0d;
    totAssetRrsp = 0d;
    totAssetGift = 0d;
    totAssetOther = 0d;
    totAssetHolding = 0d;
    totAssetHouseHoldGoods = 0d;
    totAssetLifeInsur = 0d;
    totalAssetSide = 0d;

    // liabilities
    totLiabCreditCardMonthly = 0d;
    totLiabCreditCard = 0d;
    totLiabMtgMonthly = 0d;
    totLiabMtg = 0d;
    totLiabLeaseMonthly = 0d;
    totLiabLease = 0d;
    totLiabChildMonthly = 0d;
    totLiabChild = 0d;
    totOtherLiabMonthly = 0d;
    totOtherLiab = 0d;

    totalMonthlyLiabilities = 0d;
    totalLiabilitySide = 0d;

    // ====
    totalGDS = 0d;
    totalTDS = 0d;
    totalSumIncome = 0d;
    totalSumLiability = 0d;
    totalSumAsset = 0d;
    totalNetWorth = 0d;
  }

  protected void handleLiabilitySpecialProcessing(Liability pLiab){
    if (pLiab == null) { return; }
    Element listedLiabNode;

    switch(pLiab.getLiabilityTypeId()){
      // 1 - Equity Mortgage
      case Mc.LIABILITY_TYPE_EQUITY_MORTGAGE : {
        totLiabMtgMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabMtg += pLiab.getLiabilityAmount();
        break;
      }
      // 2 - credit card
      case Mc.LIABILITY_TYPE_CREDIT_CARD : {
        totLiabCreditCardMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabCreditCard += pLiab.getLiabilityAmount();
        break;
      }
      // 3
      case Mc.LIABILITY_TYPE_PERSONAL_LOAN : {
        totLiabMtgMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabMtg += pLiab.getLiabilityAmount();
        break;
      }
      // 4
      case Mc.LIABILITY_TYPE_AUTO_LOAN : {
        totLiabMtgMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabMtg += pLiab.getLiabilityAmount();
        break;
      }
      // 5
      case Mc.LIABILITY_TYPE_ALIMONY : {
        totLiabChildMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabChild += pLiab.getLiabilityAmount();
        break;
      }
      // 6
      case Mc.LIABILITY_TYPE_CHILD_SUPPORT : {
        totLiabChildMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabChild += pLiab.getLiabilityAmount();
        break;
      }
      // 7
      case Mc.LIABILITY_TYPE_STUDENT_LOAN : {
        totLiabMtgMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabMtg += pLiab.getLiabilityAmount();
        break;
      }
      // 9
      case Mc.LIABILITY_TYPE_OTHER : {
        listedLiabNode =   doc.createElement("ListedLiability");
        addTo(listedLiabNode,"ListedLiabilityTitle",pLiab.getLiabilityDescription());
        addTo(listedLiabNode,"ListedLiabilityMonthlyPayment",formatCurrency( pLiab.getLiabilityMonthlyPayment() ));
        addTo(listedLiabNode,"ListedLiabilityPayment",formatCurrency( pLiab.getLiabilityAmount() ));
        addTo(specElem,listedLiabNode);

        totOtherLiabMonthly += pLiab.getLiabilityMonthlyPayment();
        totOtherLiab += pLiab.getLiabilityAmount();
        break;
      }
      // 10
      case Mc.LIABILITY_TYPE_UNSECURED_LINE_OF_CREDIT : {
        totLiabCreditCardMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabCreditCard += pLiab.getLiabilityAmount();
        break;
      }
      // 11
      case Mc.LIABILITY_TYPE_INCOME_TAX : {
        listedLiabNode =   doc.createElement("ListedLiability");
        addTo(listedLiabNode,"ListedLiabilityTitle",pLiab.getLiabilityDescription());
        addTo(listedLiabNode,"ListedLiabilityMonthlyPayment",formatCurrency( pLiab.getLiabilityMonthlyPayment() ));
        addTo(listedLiabNode,"ListedLiabilityPayment",formatCurrency( pLiab.getLiabilityAmount() ));
        addTo(specElem,listedLiabNode);

        totOtherLiabMonthly += pLiab.getLiabilityMonthlyPayment();
        totOtherLiab += pLiab.getLiabilityAmount();
        break;
      }
      // 12
      case Mc.LIABILITY_TYPE_MORTGAGE_ON_A_RENTAL_PROPERTY : {
        totLiabMtgMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabMtg += pLiab.getLiabilityAmount();
        break;
      }
      // 13
      case Mc.LIABILITY_TYPE_SECURED_LINE_OF_CREDIT : {
        totLiabCreditCardMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabCreditCard += pLiab.getLiabilityAmount();
        break;
      }
      // 14
      case Mc.LIABILITY_TYPE_CLOSING_COSTS : {
        listedLiabNode =   doc.createElement("ListedLiability");
        addTo(listedLiabNode,"ListedLiabilityTitle",pLiab.getLiabilityDescription());
        addTo(listedLiabNode,"ListedLiabilityMonthlyPayment",formatCurrency( pLiab.getLiabilityMonthlyPayment() ));
        addTo(listedLiabNode,"ListedLiabilityPayment",formatCurrency( pLiab.getLiabilityAmount() ));
        addTo(specElem,listedLiabNode);

        totOtherLiabMonthly += pLiab.getLiabilityMonthlyPayment();
        totOtherLiab += pLiab.getLiabilityAmount();
        break;
      }
      // 15
      case Mc.LIABILITY_TYPE_LEASE : {
        totLiabLeaseMonthly += pLiab.getLiabilityMonthlyPayment();
        totLiabLease += pLiab.getLiabilityAmount();
        break;
      }
      case Mc.LIABILITY_TYPE_RENT : {
        totOtherLiabMonthly += pLiab.getLiabilityMonthlyPayment();
        totOtherLiab += pLiab.getLiabilityAmount();
        break;
      }
    }// end switch
  }


  protected void handleAssetSpecialProcessing(Asset pAsset){
    if(pAsset == null){ return; }
    Element listedAssetNode;
    switch( pAsset.getAssetTypeId() ){
      // 0
      case Mc.ASSET_TYPE_SAVINGS : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetCash += pAsset.getAssetValue();
        }
        break;
      }
      // 1
      case Mc.ASSET_TYPE_RRSP : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetRrsp += pAsset.getAssetValue();
        }
        break;
      }
      // 2
      case Mc.ASSET_TYPE_GIFT : {
        if(pAsset.getIncludeInNetWorth()){
          listedAssetNode =   doc.createElement("ListedAsset");
          addTo(listedAssetNode,"ListedAssetTitle",pAsset.getAssetDescription());
          addTo(listedAssetNode,"ListedAssetValue",formatCurrency( pAsset.getAssetValue() ));
          addTo(specElem,listedAssetNode);
          totAssetGift += pAsset.getAssetValue();
        }
        break;
      }
      // 3
      case Mc.ASSET_TYPE_RESIDENCE : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetProperty += pAsset.getAssetValue();
        }
        break;
      }
      // 4
      case Mc.ASSET_TYPE_VEHICLE : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetVehicle += pAsset.getAssetValue();
        }
        break;
      }
      // 5
      case Mc.ASSET_TYPE_PROPERTY : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetProperty += pAsset.getAssetValue();
        }
        break;
      }
      // 6
      case Mc.ASSET_TYPE_STOCKS_BONDS_MUTUAL : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetStocks += pAsset.getAssetValue();
        }
        break;
      }
      // 7
      case Mc.ASSET_TYPE_RENTAL_PROPERRTY : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetProperty += pAsset.getAssetValue();
        }
        break;
      }
      // 8
      case Mc.ASSET_TYPE_OTHER : {
        if(pAsset.getIncludeInNetWorth()){
          listedAssetNode =   doc.createElement("ListedAsset");
          addTo(listedAssetNode,"ListedAssetTitle",pAsset.getAssetDescription());
          addTo(listedAssetNode,"ListedAssetValue",formatCurrency( pAsset.getAssetValue() ));
          addTo(specElem,listedAssetNode);
          totAssetOther += pAsset.getAssetValue();
        }
        break;
      }
      // 9
      case Mc.ASSET_TYPE_DOWN_PAYMENT : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetCash += pAsset.getAssetValue();
        }
        break;
      }
      // 10
      case Mc.ASSET_TYPE_HOLDING : {
        if(pAsset.getIncludeInNetWorth()){
          listedAssetNode =   doc.createElement("ListedAsset");
          addTo(listedAssetNode,"ListedAssetTitle",pAsset.getAssetDescription());
          addTo(listedAssetNode,"ListedAssetValue",formatCurrency( pAsset.getAssetValue() ));
          addTo(specElem,listedAssetNode);

          totAssetHolding += pAsset.getAssetValue();
        }
        break;
      }
      // 11
      case Mc.ASSET_TYPE_HOUSEHOLD_GOODS : {
        if(pAsset.getIncludeInNetWorth()){
          listedAssetNode =   doc.createElement("ListedAsset");
          addTo(listedAssetNode,"ListedAssetTitle",pAsset.getAssetDescription());
          addTo(listedAssetNode,"ListedAssetValue",formatCurrency( pAsset.getAssetValue() ));
          addTo(specElem,listedAssetNode);

          totAssetHouseHoldGoods += pAsset.getAssetValue();
        }
        break;
      }
      // 12
      case Mc.ASSET_TYPE_LIFE_INSURANCE : {
        if(pAsset.getIncludeInNetWorth()){
          listedAssetNode =   doc.createElement("ListedAsset");
          addTo(listedAssetNode,"ListedAssetTitle",pAsset.getAssetDescription());
          addTo(listedAssetNode,"ListedAssetValue",formatCurrency( pAsset.getAssetValue() ));
          addTo(specElem,listedAssetNode);

          totAssetLifeInsur += pAsset.getAssetValue();
        }
        break;
      }
      // 13
      case Mc.ASSET_TYPE_DEPOSIT : {
        if(pAsset.getIncludeInNetWorth()){
          totAssetCash += pAsset.getAssetValue();
        }
        break;
      }
    } // end of switch

  }

  protected void buildDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();
    Element root = doc.createElement("Document");
    root.setAttribute("type","1");
    root.setAttribute("lang","" + lang);

    specElem = doc.createElement("specialRequirementTags");
    buildDealTags();


    root.appendChild( dealElement ) ;
    root.appendChild( dealAdditionalElement ) ;

    root.appendChild( buildGeneralTags()  );
    root.appendChild( buildSpecificTags() );
    doc.appendChild(root);
  }

  protected Node buildSpecificTags() throws Exception
  {
    String val = null;
    addTo(specElem, extractBorrowerNames());
    addTo(specElem, extractRealEstateData());
    addTo(specElem, discountOrPremium());
    //addTo(specElem,processActualPaymentTerm());
    addTo(specElem, processAmortizationTerm());
    addTo(specElem, createLiabilitiyNodes());
    extractDealFees();
    //Ticket 3021
    addTo(specElem, extractDownpaymentSource());
    //#DG412
    int dealPurpose = deal.getDealPurposeId();
    boolean isDownPay = deal.getDownPaymentAmount() > 0
        && dealPurpose != Mc.DEAL_PURPOSE_REFI_EXTERNAL
        && dealPurpose != Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT
        && dealPurpose != Mc.DEAL_PURPOSE_WORKOUT
        && deal.getLineOfBusinessId() != Mc.LOB_B;
    addTo(specElem, "isDownpayment", (isDownPay ? "Y" : "N"));
    //#DG412 end
    addTo(specElem, "isRefiImprAmnt", (deal.getRefiImprovementAmount() > 0 ? "Y" : "N"));
    if( deal.getFirstPaymentDate() != null ) //#DG200
      addTo(specElem, "firstPaymentDate" , formatDate(  deal.getFirstPaymentDate()  ) );

    //Ticket 3021
    addTo(specElem,"combinedGDS",  formatRatio(deal.getCombinedGDS() ) );
    addTo(specElem,"combinedTDS",  formatRatio(deal.getCombinedTDS() ) );
    addTo(specElem, "CombinedTDS3Year" , formatRatio(  deal.getCombinedTDS3Year()  ) );
    addTo(specElem, "CombinedGDS3Year" , formatRatio(  deal.getCombinedGDS3Year()  ) );

    try{
        val = formatCurrency(deal.getCombinedTotalIncome());
    } catch (Exception e){}
    addTo(specElem,"CombinedTotalIncome", val);

    try{
          val = formatRatio(deal.getCombinedLTV());
    } catch (Exception e){}
    addTo(specElem,"CombinedLTV", val);

    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MIStatus",
                                             deal.getMIStatusId(),
                                             lang);
    addTo(specElem, "MIStatus", val);

    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "SpecialFeature",
                                             deal.getSpecialFeatureId(),
                                             lang);
    addTo(specElem, "SpecialFeature", val);

    return specElem;

  }

  protected void extractDealFees() throws FinderException, RemoteException,
      Exception {

    DealFee dealFee = new DealFee(srk, null);
    Collection coll = dealFee.findByDeal(new DealPK(deal.getDealId(),deal.getCopyId()));

    Iterator it = coll.iterator();
    double sum = 0.0;
    double val = 0.0;
    while( it.hasNext() ){
      Element oneFeeNode = doc.createElement("DealFee");
      DealFee fee = (DealFee)it.next();
      addTo(oneFeeNode, "feeId", "" + fee.getFeeId());
      val = fee.getFeeAmount();
      if ((fee.getFeeId() == 0 || fee.getFeeId() == 22) && val > 0.0){
        sum += val;
      }
      addTo(oneFeeNode, "feeAmount", formatCurrency(val));
      addTo(oneFeeNode, "feePaymentMethodId", "" + fee.getFeePaymentMethodId());
      addTo(specElem, oneFeeNode);
      }
      if (sum > 0.0){
        addTo(specElem, "FraisDeval", formatCurrency(sum));
      }
    }

  protected int findLiabilityGroup(Liability pLiab){
          switch(pLiab.getLiabilityTypeId()){
            // loans
            case 0:
            case 1:
            case 3:
            case 4 :
            case 7 :
            case 10 :
            case 12 :
            case 13 :
            case 21 : {
              return LIABILITY_GROUP_LOANS;
            }

            // credit cards
            case 2 :
            // Dept. Store Credit Card
            case 20 :{
              return LIABILITY_GROUP_CREDIT_CARD;
            }

            // OTHER
            case 5 :
            case 6 :
            case 8 :
            case 9 :
            case 11 :
            case 14 :
            case 15 :
            case 16 :{
              return LIABILITY_GROUP_OTHER;
            }
/*
            // municipal                                             // not used
            case 17 : {
              return LIABILITY_GROUP_MUNICIPALITY;
            }
            // condo                                                 // not used
            case 18 : {
              return LIABILITY_GROUP_CONDO;
            }
            // heating                                               // not used
            case 19 : {
              return LIABILITY_GROUP_HEATING;
            }

 */           default :{                                               // not used
              return LIABILITY_GROUP_TBD;
            }

          }
  }

// ---------- the following liabilities are used in DJ_120 ---------------
  protected Node createLiabilitiyNodes(){
    Node rvNode = doc.createElement("variousLiabilities");
    double variosLiabilitiesTotal = 0d;

    Node creditCardNode = doc.createElement("creditCardLiabilities");
    double creditCardTotal = 0d;

    Node loansNode = doc.createElement("loanLiabilities");
    double loansTotal = 0d;

    Node othersNode = doc.createElement("otherLiabilities");
    double otherTotal = 0d;

    double heatingMonthly = 0d;
    double taxMonthly = 0d;
    double condoMonthly = 0d;

    Node singleNode;
//    double heatingTotal = 0d;
//    double municipalTotal = 0d;
//    double condoTotal = 0d;
    Collection coll;
    try{
      coll = deal.getBorrowers();
    } catch (Exception exc){
      return rvNode;
    }
    if(coll == null){
      return rvNode;
    }
    Iterator it = coll.iterator();
    while( it.hasNext() ){
      Borrower borr = (Borrower)it.next();
      if(borr.isGuarantor() ) { continue; }
      try{
        Collection lBorLiabCol = borr.getLiabilities();
        Iterator lBorLiabIt = lBorLiabCol.iterator();
        while( lBorLiabIt.hasNext() ){
          Liability lLiab = (Liability)lBorLiabIt.next();

          // #680:  In order to ensure that liabilities are not included when they are to be payed off, it is not sufficient to simply check that the includeInTDS value = Y, but also to ensure that the liabilityPayoffTypeID = 0 (i.e. not payed prior to advance or close, from internal funds or from proceeds)
          if ((lLiab.getIncludeInTDS() == true) && (lLiab.getLiabilityPayOffTypeId()==0)) {
            switch (findLiabilityGroup(lLiab)) {
              case LIABILITY_GROUP_CREDIT_CARD: {
                creditCardTotal += lLiab.getLiabilityMonthlyPayment();
                variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();

                singleNode = doc.createElement("creditCardLiability");
                addTo(singleNode, "amount",
                      formatCurrency(lLiab.getLiabilityMonthlyPayment()));
                addTo(creditCardNode, singleNode);

                //singleNode = doc.createElement("liabilityId");
                addTo(singleNode, "liabilityId", "" + lLiab.getLiabilityId());
                addTo(creditCardNode, singleNode);
                break;
              }
              case LIABILITY_GROUP_LOANS: {
                loansTotal += lLiab.getLiabilityMonthlyPayment();
                variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();

                singleNode = doc.createElement("loanLiability");
                addTo(singleNode, "amount",
                      formatCurrency(lLiab.getLiabilityMonthlyPayment()));
                addTo(loansNode, singleNode);

                //singleNode = doc.createElement("liabilityId");
                addTo(singleNode, "liabilityId", "" + lLiab.getLiabilityId());
                addTo(loansNode, singleNode);

                break;
              }
/*
              case LIABILITY_GROUP_CONDO: {
                condoTotal += lLiab.getLiabilityMonthlyPayment();
                variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();
                break;
              }
              case LIABILITY_GROUP_HEATING: {
                heatingTotal += lLiab.getLiabilityMonthlyPayment();
                variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();
                break;
              }
              case LIABILITY_GROUP_MUNICIPALITY: {
                municipalTotal += lLiab.getLiabilityMonthlyPayment();
                variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();
                break;
              }
 */
              case LIABILITY_GROUP_OTHER: {
                otherTotal += lLiab.getLiabilityMonthlyPayment();
                variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();

                singleNode = doc.createElement("otherLiability");
                addTo(singleNode, "amount",
                      formatCurrency(lLiab.getLiabilityMonthlyPayment()));
                addTo(othersNode, singleNode);

                addTo(singleNode, "liabilityId", "" + lLiab.getLiabilityId());
                addTo(othersNode, singleNode);

                break;
              }
            } // end switch
          }
        }
      }catch(Exception exc){
        // do nothing here, skip to the next borrower
      }
    }
    // now bring out all the totals
    addTo(rvNode,creditCardNode);
    addTo(rvNode,"creditCardLiabilityTotal",formatCurrency(creditCardTotal));

    addTo(rvNode,loansNode);
    addTo(rvNode,"loansLiabilityTotal",formatCurrency(loansTotal));

    addTo(rvNode,othersNode);
    addTo(rvNode,"otherLiabilityTotal",formatCurrency(otherTotal));

    //singleNode = doc.createElement("municipalLiabilityTotal");
//    addTo(rvNode,"municipalLiabilityTotal",formatCurrency(municipalTotal));
    //addTo(rvNode,singleNode);

    //singleNode = doc.createElement("condoLiabilityTotal");
//    addTo(rvNode,"condoLiabilityTotal",formatCurrency(condoTotal));
    //addTo(rvNode,singleNode);

    //singleNode = doc.createElement("heatingLiabilityTotal");
//    addTo(rvNode,"heatingLiabilityTotal",formatCurrency(heatingTotal));
    //addTo(rvNode,singleNode);

/*
        Node monthlyHeatNode = doc.createElement("monthlyHeatingExp");
        double heatingMonthly = 0d;

        Node monthlyTaxNode = doc.createElement("monthlyTaxExp");
        double taxMonthly = 0d;

        Node monthlyCondoNode = doc.createElement("monthlyCondoExp");
        double condoMonthly = 0d;
*/

    heatingMonthly = deal.getCombinedTotalAnnualHeatingExp() / 12;
    taxMonthly = deal.getCombinedTotalAnnualTaxExp() / 12;
    condoMonthly =  deal.getCombinedTotalCondoFeeExp() / 12;

    addTo(rvNode, "monthlyHeatingExp", "" + formatCurrency(heatingMonthly));
    addTo(rvNode, "monthlyTaxExp", "" + formatCurrency(taxMonthly));
    addTo(rvNode, "monthlyCondoExp", "" + formatCurrency(condoMonthly));

    variosLiabilitiesTotal += heatingMonthly;
    variosLiabilitiesTotal += taxMonthly;
    variosLiabilitiesTotal += condoMonthly;

    addTo(rvNode,"variousLiabilityTotal",formatCurrency(variosLiabilitiesTotal));

    return rvNode;
  }

  protected Node processAmortizationTerm(){
    Node rvNode = doc.createElement("amortizationTermProcessed");

    if(  isRemainder(deal.getAmortizationTerm(), 12)  ){
      addTo(rvNode,getTag("termNumber",""+ deal.getAmortizationTerm()/12));
      addTo(rvNode,getTag("termMeasure", "YEARS"));
    }else{
      addTo(rvNode,getTag("termNumber",""+ deal.getAmortizationTerm()));
      addTo(rvNode,getTag("termMeasure", "MONTHS"));
    }
    return rvNode;
  }


  protected boolean isRemainder(int pNum1, int pNumDiv){
    int lNum;
    lNum = pNum1 % pNumDiv;
    if( lNum == 0 ){
      return true;
    }
    return false;
  }

  protected Node processActualPaymentTerm(){
    Node rvNode = doc.createElement("actualPaymentTermProcessed");

    if(  isRemainder(deal.getActualPaymentTerm(),12)  ){
      addTo(rvNode,getTag("termNumber",""+ deal.getActualPaymentTerm()/12));
      addTo(rvNode,getTag("termMeasure", "YEARS"));
    }else{
      addTo(rvNode,getTag("termNumber",""+ deal.getActualPaymentTerm()));
      addTo(rvNode,getTag("termMeasure", "MONTHS"));
    }
    return rvNode;
  }

  protected Node discountOrPremium() throws Exception
  {
    Node dealOrPremiumNode = doc.createElement("discountOrPremium");
    if(deal.getDiscount() == 0d && deal.getPremium() == 0d){
      return getTag("discountOrPremium","NONE");
    }else if(deal.getDiscount() > 0d){
      return getTag("discountOrPremium","DISCOUNT");
    }else if(deal.getPremium()>0d){
      return getTag("discountOrPremium","PREMIUM");
    }
    return getTag("discountOrPremium","ERROR");
  }

  protected Node extractRealEstateData() throws Exception
  {
    Node realEstateNode = doc.createElement("realEstateData");
    Node partyProfileNode = doc.createElement("partyProfile");
    PartyProfile pp = new PartyProfile(srk);
    // ALERT party type solicitor must change to party type real estate
    Collection col = pp.findByDealAndType( new DealPK(deal.getDealId(), 
                                           deal.getCopyId()), Mc.PARTY_TYPE_SOLICITOR );
    if(col.isEmpty()){return null;}
    Iterator it = col.iterator();
    while( it.hasNext() ){
      PartyProfile profile = (PartyProfile) it.next();
      addTo(partyProfileNode,"partyCompanyName",profile.getPartyCompanyName());
      addTo(partyProfileNode,"partyName",profile.getPartyName());
      addTo(partyProfileNode, extractContactData(profile.getContact()));
      addTo(realEstateNode,partyProfileNode);
      break;   // one record only
    }
    return realEstateNode;
  }

  protected Node extractBorrowerNames() throws Exception
  {
    Element bNodeList = doc.createElement("OrderedBorrowers");

    Collection pCol = deal.getBorrowers();
    Vector bVect = sortBorrowers(pCol);
    int lCount = 0;
    String val;
    for(int i=0; i<bVect.size(); i++){
      // if(lCount == 4){break; }
      Iterator pColIt = pCol.iterator();
      Node bNode ;
      while( pColIt.hasNext() ){
        Borrower b = (Borrower) pColIt.next();
        if( b.getBorrowerId() == ((Integer) bVect.elementAt(i)).intValue() ){
          bNode = extractBorrowerData(b);
          addTo(  bNode, "totalIncomeAmountMonthly", formatCurrency(b.getTotalIncomeAmount()/12) );
          addTo(  bNode, "monthsAtCurrentAddress",  convertMonthsToYearsAndMonths( extractMaxMonthsOnCurrentAddr(b)) );
          addTo(  bNode, "monthsAtCurrentEmployment", convertMonthsToYearsAndMonths( extractMaxMonthsOnCurrentEmpl(b)) );
          addTo(  bNode, "bankruptcyStatusID", ""+ b.getBankruptcyStatusId());
          addTo(  bNode, "insuranceProportionsId", ""+ b.getInsuranceProportionsId());
          addTo(  bNode, "netWorth", ""+ formatCurrency(b.getNetWorth()) );   //#DG466

          //Ticket 3021
          addTo(  bNode, "age", ""+ b.getAge());

          val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "bankruptcyStatus", b.getBankruptcyStatusId() , lang);
          addTo(  bNode, "bankruptcyStatus", val);

          val = b.getGuarantorOtherLoans();
          if ( val.equalsIgnoreCase("y")) {
            addTo(bNode, "guarantorOtherLoans", "Oui");
          } else {
            addTo(bNode, "guarantorOtherLoans", "Non");
          }
          extractBorrowerCreditReference( b, bNode);

          Date dd = b.getCreditBureauOnFileDate();
          if ( dd != null){
            addTo(bNode, "creditBureauOnFileDate","" + formatDate(b.getCreditBureauOnFileDate()));
          }
          addTo(bNodeList,bNode);

          int LDInsTypeID = extractLDInsuranceTypeId(b);
          if (LDInsTypeID != 0){
            addTo( bNode, "LDInsuranceTypeId", ""+ LDInsTypeID);
          }
          lCount++;
          break;
        }
      }
    }
    return bNodeList;
  }


  /**
   * Sorts borrowers like this:  First place takes primary borrower
   * followed by other regular borrowers ordered by borrower id,
   * then guarantors and coventors
   */
  protected Vector sortBorrowers(Collection pColl){
    Vector retVect = new Vector();
    Iterator it = pColl.iterator();
    while(it.hasNext()){
      Borrower b = (Borrower) it.next();
      if(b.isGuarantor()){continue;}
      if(b.isPrimaryBorrower()){
        retVect.insertElementAt(new Integer(b.getBorrowerId()), 0);
      }
    }
    if(retVect.size() == 0){
      return retVect;
    }

    // borrowers
    it = pColl.iterator();
    while ( it.hasNext() ){
      Borrower b = (Borrower) it.next();
      if( b.isPrimaryBorrower() ){ continue; }
      if( b.isGuarantor() ){ continue; }
      if (b.getBorrowerTypeId() == 2) {continue;} // skip coventors
      boolean inserted = false;
      for( int i=1;i<retVect.size(); i++ ){
        if(b.getBorrowerId() < ((Integer)retVect.elementAt(i)).intValue() ){
          retVect.insertElementAt(new Integer(b.getBorrowerId()),i);
          inserted = true;
          break;
        }
      }
      if( !inserted ){
        retVect.addElement(new Integer(b.getBorrowerId()));
      }
    }

    // guarantors
    it = pColl.iterator();
    while ( it.hasNext() ){
      Borrower b = (Borrower) it.next();
      if( b.isPrimaryBorrower() ){ continue; }
      if( b.isGuarantor() == false ){ continue; }
      if (b.getBorrowerTypeId() == 2) {continue;} // skip coventors
      boolean inserted = false;
      // fixed sorting algorithm
      int curSize = retVect.size();
      for( int i=curSize;i<retVect.size(); i++ ){
        if(b.getBorrowerId() < ((Integer)retVect.elementAt(i)).intValue() ){
          retVect.insertElementAt(new Integer(b.getBorrowerId()),i);
          inserted = true;
          break;
        }
      }
      if( !inserted ){
        retVect.addElement(new Integer(b.getBorrowerId()));
      }
    }

    // coventors
    it = pColl.iterator();
    while ( it.hasNext() ){
      Borrower b = (Borrower) it.next();
      if (b.getBorrowerTypeId() == 2) {
        boolean inserted = false;
        int curSize = retVect.size();
        for (int i = curSize; i < retVect.size(); i++) {
          if (b.getBorrowerId() < ( (Integer) retVect.elementAt(i)).intValue()) {
            retVect.insertElementAt(new Integer(b.getBorrowerId()), i);
            inserted = true;
            break;
          }
        }
        if (!inserted) {
          retVect.addElement(new Integer(b.getBorrowerId()));
        }
      }
    }
    return retVect;
  }

  protected void handleAssetProcessing(Object obj)
  {
    try{
      Collection col = ((Borrower)obj).getAssets();
      Iterator it = col.iterator();
      while( it.hasNext() ){
        handleAssetSpecialProcessing((Asset)it.next());
      }
    } catch(Exception exc){
      // log this exception
    }
  }

  protected void handleLiabilityProcessing(Object obj)
  {
    try{
      Collection col = ((Borrower)obj).getLiabilities();
      Iterator it = col.iterator();
      while( it.hasNext() ){
        handleLiabilitySpecialProcessing((Liability)it.next());
      }
    } catch(Exception exc){
      // log this exception
    }
  }

  public void processNetWorth( Borrower pBor){
    totalGDS += pBor.getGDS();
    totalTDS += pBor.getTDS();
    totalSumIncome += pBor.getTotalIncomeAmount();
    totalSumLiability += pBor.getTotalLiabilityAmount();
    totalSumAsset += pBor.getTotalAssetAmount();
    totalNetWorth += pBor.getNetWorth();
  }

  public void actionPerformed(java.awt.event.ActionEvent ev){
    Object obj = ev.getSource();
    if(obj instanceof Borrower){
      if(ev.getActionCommand() != null && ev.getActionCommand().equals("process_assets")){
        handleAssetProcessing(obj);
      }else if(ev.getActionCommand() != null && ev.getActionCommand().equals("process_liabilities")){
        handleLiabilityProcessing(obj);
      }else if(ev.getActionCommand() != null && ev.getActionCommand().equals("process_net_worth")){
        processNetWorth((Borrower)obj);
      }
    }
  }

  protected int extractLDInsuranceTypeId( Borrower pBor) throws Exception{
    int ldpType = 0;
    LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk);
    BorrowerPK pk = new BorrowerPK(pBor.getBorrowerId(), 
                                   deal.getCopyId());

    Collection coll = ldp.findByBorrower(pk);
    if (!coll.isEmpty()){
      Vector vect = new Vector(coll);
      ldpType = ((LifeDisabilityPremiums)vect.elementAt(0)).getLDInsuranceTypeId();

    }
    return ldpType;
  }

  // Ticket 3021
  protected  void extractBorrowerLiabilities(Borrower pBor, Node pNode){
    Element oneLiabNode ;
    String  val;
    try{
      Collection lLiabCol = pBor.getLiabilities();
      Iterator lLiabColIt = lLiabCol.iterator();
      while(lLiabColIt.hasNext()){
        oneLiabNode = doc.createElement("Liability");
        Liability lLiab = (Liability) lLiabColIt.next();

        addTo(oneLiabNode, "liabilityId", "" + lLiab.getLiabilityId() );
        addTo(oneLiabNode, "liabilityTypeId", "" + lLiab.getLiabilityTypeId() );

        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LiabilityType", lLiab.getLiabilityTypeId() , lang);
        addTo(oneLiabNode, "liabilityType", val);
        addTo(oneLiabNode, "borrowerId", "" + lLiab.getBorrowerId() );
        addTo(oneLiabNode, "liabilityDescription", "" + lLiab.getLiabilityDescription() );
        addTo(oneLiabNode, "liabilityAmount", formatCurrency(lLiab.getLiabilityAmount()) );
        addTo(oneLiabNode, "liabilityMonthlyPayment", formatCurrency(lLiab.getLiabilityMonthlyPayment() ) );
        addTo(oneLiabNode, "percentIncludedInGDS", formatRatio(lLiab.getPercentInGDS()));
        addTo(oneLiabNode, "percentIncludedInTDS", formatRatio(lLiab.getPercentInTDS()));
        addTo(oneLiabNode, "liabilityPayoffTypeId", "" + lLiab.getLiabilityPayOffTypeId());

        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LiabilityPayoffType", lLiab.getLiabilityPayOffTypeId() , lang);
        addTo(oneLiabNode, "liabilityPayoffType", val);

        addTo(pNode,oneLiabNode);
      }
    }catch(Exception lLiabExc){
      // log this exceptio
    }
  }

  protected  void extractBorrowerAssets(Borrower pBor, Node pNode){
    Element oneAssetNode ;
    String val;

    try{
      Collection lAssetCol = pBor.getAssets();
      Iterator lAssetColIt = lAssetCol.iterator();
      while(lAssetColIt.hasNext()){
        oneAssetNode = doc.createElement("Asset");
        Asset lAsset = (Asset) lAssetColIt.next();

        addTo(oneAssetNode, "assetId", "" + lAsset.getAssetId() );
        addTo(oneAssetNode, "assetDescription", lAsset.getAssetDescription() );
        addTo(oneAssetNode, "borrowerId", "" + lAsset.getBorrowerId() );
        addTo(oneAssetNode, "assetValue", "" + formatCurrency(lAsset.getAssetValue() ) );
        addTo(oneAssetNode, "assetTypeId", "" + lAsset.getAssetTypeId() );

        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "AssetType", lAsset.getAssetTypeId() , lang);
        addTo(oneAssetNode, "assetType", val);
        addTo(oneAssetNode, "copyId", "" + lAsset.getCopyId() );
        addTo(pNode,oneAssetNode);
      }
    }catch(Exception lAssetExc){
      // log this exception
    }
  }

  protected  void extractEmploymentHistories(Borrower pBor, Node pNode){
    Element oneHistNode ;
    String  val;
    try{
      Collection lHistCol = pBor.getEmploymentHistories();
      Iterator lHistColIt = lHistCol.iterator();
      while(lHistColIt.hasNext()){
        oneHistNode = doc.createElement("EmploymentHistory");
        EmploymentHistory lHist = (EmploymentHistory) lHistColIt.next();
        addTo(oneHistNode, "employmentHistoryId", "" + lHist.getEmploymentHistoryId() );
        addTo(oneHistNode, "employmentHistoryTypeId", "" + lHist.getEmploymentHistoryTypeId() );
        addTo(oneHistNode, "employmentHistoryStatusId", "" + lHist.getEmploymentHistoryStatusId() );

        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "employmentHistoryStatus", lHist.getEmploymentHistoryStatusId() , lang);
        addTo(oneHistNode, "employmentHistoryStatus", val);
        addTo(oneHistNode, "borrowerId", "" + lHist.getBorrowerId() );
        addTo(oneHistNode, "monthsOfService", "" + lHist.getMonthsOfService() );

        val = DocPrepLanguage.getInstance().getYearsAndMonths(lHist.getMonthsOfService(), lang);
        addTo(oneHistNode, "timeAtJob", val);

        int id = lHist.getJobTitleId();
        addTo(oneHistNode, "jobTitleId", "" + id );

        val = " ";
        if (id==0) {
          val = lHist.getJobTitle();
        }
        else {
          val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "JobTitle", id,lang);
        }
        addTo(oneHistNode, "jobTitle", val );
        addTo(oneHistNode, "employerName", lHist.getEmployerName() );
        addTo(oneHistNode, extractContactData( lHist.getContact()));
        addTo(oneHistNode, extractIncome(lHist.getIncome()));
        addTo(pNode,oneHistNode);
      }
    }catch(Exception lHistExc){
      // log this exception
    }
  }

  protected Node extractIncome(Income pIncome){
    String  val;
    Node rv = doc.createElement("Income");
    if(pIncome == null){
      return rv;
    }
    addTo(rv,"incomeId", ""+pIncome.getIncomeId());
    addTo(rv,"incomeAmount", formatCurrency(pIncome.getAnnualIncomeAmount()));
    addTo(rv,"monthlyIncomeAmount", formatCurrency(pIncome.getMonthlyIncomeAmount()));
    addTo(rv,"incomeTypeId", "" + pIncome.getIncomeTypeId());

    //#DG466 val = BXResources.getPickListDescription("AssetType", pIncome.getIncomeTypeId() , lang);
    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "incomeType", pIncome.getIncomeTypeId() , lang);
    addTo(rv, "incomeType", val);
    addTo(rv,"incomeDescription", pIncome.getIncomeDescription());

    if (pIncome.getIncIncludeInTDS()) {
      totMonthlySumIncludeInTDS += pIncome.getMonthlyIncomeAmount()*(pIncome.getIncPercentInTDS()) / 100;
    }

    return rv;
  }

  protected  void extractIncomes(Borrower pBor, Node pNode){

    try{
      totMonthlySumIncludeInTDS = 0d;
      Collection lIncomeCol = pBor.getIncomes();
      Iterator lIncomeColIt = lIncomeCol.iterator();
      while(lIncomeColIt.hasNext()){
        Income lIncome = (Income) lIncomeColIt.next();
        addTo(pNode,extractIncome(lIncome));
      }
      addTo(pNode, "totalMonthlySumIncludeInTDS", formatCurrency(totMonthlySumIncludeInTDS));
    }catch(Exception lHistExc){
      //do nothing here
    }
  }

  protected  void extractBorrowerCreditReference(Borrower pBor, Node pNode){
    Element oneCreditRefNode ;
    String  val;

    try{
      Collection lCreditRefCol = pBor.getCreditReferences();
      Iterator lCreditRefColIt = lCreditRefCol.iterator();
      while(lCreditRefColIt.hasNext()){
        oneCreditRefNode = doc.createElement("CreditReference");
        CreditReference lCreditRef = (CreditReference) lCreditRefColIt.next();

        if ( lCreditRef.getCurrentBalance()== 0 ) {
          addTo(oneCreditRefNode, "creditRefTypeID", "" + lCreditRef.getCreditRefTypeId());

          val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "creditRefType",
                                                   lCreditRef.getCreditRefTypeId(),
                                                   lang);
          addTo(oneCreditRefNode, "creditRefType", val);
          addTo(oneCreditRefNode, "creditReferenceDescription",
                lCreditRef.getCreditReferenceDescription());
          addTo(oneCreditRefNode, "institutionName", "" + lCreditRef.getInstitutionName());
          addTo(oneCreditRefNode, "accountNumber", "" + lCreditRef.getAccountNumber());
          addTo(oneCreditRefNode, "currentBalance",
                "" + formatCurrency(lCreditRef.getCurrentBalance()));

          addTo(pNode, oneCreditRefNode);
        }
      }
    }catch(Exception lCreditRefExc){
      // log this exception
    }
  }

  protected Node extractPropertyData(Property pProp) throws Exception
  {
    Element onePropNode = doc.createElement("Property");
    String val;
    if(pProp.isPrimaryProperty()){
      addTo(onePropNode , "primaryPropertyFlag", "Y" );
    }else{
      addTo(onePropNode , "primaryPropertyFlag", "N" );
    }
    addTo(onePropNode , "propertyId", ""+pProp.getPropertyId() );
    addTo(onePropNode , "propertyStreetNumber", pProp.getPropertyStreetNumber() );
    addTo(onePropNode , "propertyStreetName", pProp.getPropertyStreetName() );

    addTo(onePropNode , "propertyAddressLine2", pProp.getPropertyAddressLine2() );
    addTo(onePropNode , "propertyPostalFSA", pProp.getPropertyPostalFSA() );
    addTo(onePropNode , "propertyPostalLDU", pProp.getPropertyPostalLDU() );
    addTo(onePropNode , "propertyCity", pProp.getPropertyCity() );
    addTo(onePropNode , "provinceId", ""+pProp.getProvinceId() );
    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province",pProp.getProvinceId(),lang);
    addTo(onePropNode , "province", val );
    addTo(onePropNode,"unitNumber" ,pProp.getUnitNumber());

    addTo(onePropNode , "newConstructionId",  ""+pProp.getNewConstructionId() );
    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "NewConstruction",pProp.getNewConstructionId(),lang);
    addTo(onePropNode , "newConstruction", val );

    addTo(onePropNode,"dwellingTypeId","" + pProp.getDwellingTypeId());
    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DwellingType",pProp.getDwellingTypeId(),lang);
    addTo(onePropNode , "dwellingType", val );

    addTo(onePropNode , "propertyTypeId",  ""+pProp.getPropertyTypeId() );
    addTo(onePropNode,"streetTypeId" ,""+pProp.getStreetTypeId());
    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",pProp.getStreetTypeId(),lang);
    addTo(onePropNode , "streetType", val );

    addTo(onePropNode,"streetDirectionId" ,""+pProp.getStreetDirectionId());
    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection",pProp.getStreetDirectionId(),lang);
    addTo(onePropNode , "streetDirection", val );
    addTo(onePropNode , "waterTypeId", "" + pProp.getWaterTypeId());

    addTo(onePropNode , "legalLine1", pProp.getLegalLine1());
    addTo(onePropNode , "legalLine2", pProp.getLegalLine2());
    addTo(onePropNode , "legalLine3", pProp.getLegalLine3());

    addTo(onePropNode , "structureAge", ""+ pProp.getStructureAge());
    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "OccupancyType",pProp.getOccupancyTypeId(),lang);
    addTo(onePropNode , "occupancyType", val );

    AppraisalOrder appraisalOrder = new AppraisalOrder(srk,null);
    PropertyPK propPK = new PropertyPK( pProp.getPropertyId(), pProp.getCopyId());
    appraisalOrder = appraisalOrder.findByProperty(propPK);
    if(appraisalOrder != null){
      addTo( onePropNode, extractApraisalOrder(appraisalOrder) );
    }
    return onePropNode;
  }

  protected Node extractDownpaymentSource()
  {
      Node dsNode = null;
      Node lineNode;
      String val;

      try
      {
          List sources = (List)deal.getDownPaymentSources();

          if (sources.isEmpty())
             return null;

          dsNode = doc.createElement("DownPayments");
          Iterator i = sources.iterator();

          while (i.hasNext())
          {
              DownPaymentSource dps = (DownPaymentSource)i.next();

              lineNode = doc.createElement("DownPayment");

              val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DownPaymentSourceType", dps.getDownPaymentSourceTypeId() , lang);

              addTo(lineNode, "downPaymentSourceType", val);

              //  Description
              addTo(lineNode, "dPSDescription", dps.getDPSDescription());

              //  Amount
              addTo(lineNode, "amount", formatCurrency(dps.getAmount()));

              dsNode .appendChild(lineNode);

          }
      }
      catch (Exception e){}

      return dsNode;
  }

}

// =============================================================================
// list of created nodes
// =============================================================================


