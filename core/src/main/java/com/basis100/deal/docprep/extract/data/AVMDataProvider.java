package com.basis100.deal.docprep.extract.data;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.basis100.deal.docprep.extract.data.DealExtractor;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.ServiceProvider;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;

import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.resources.SessionResourceKit;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

public class AVMDataProvider extends DealExtractor {
	
	private Request mRequest;
	private ServiceRequest mServiceRequest;	
	private ServiceProduct mServiceProduct;
	private ServiceRequestContact mSRContact;
	
	public AVMDataProvider(){}
	public AVMDataProvider(SessionResourceKit pSrk, Request pReq, int pLang) throws Exception {
		srk = pSrk;
		lang = pLang;
		mRequest = pReq;
		mServiceRequest = new ServiceRequest(srk, mRequest.getRequestId(), mRequest.getCopyId());
		mServiceProduct = new ServiceProduct(srk, mRequest.getServiceProductId());
		mSRContact = new ServiceRequestContact(srk, mRequest.findContactId(), mRequest.getRequestId(), mRequest.getCopyId());
	}

	public void setDomDocument(Document pDoc) {
		doc = pDoc;
	}
	
	protected String exServiceProviderName() {
		return exServiceProviderName(mServiceProduct.getServiceProviderId());
	}

	protected String exServiceProviderName(int pProviderId) {
		String ret = null;
		try{
			ret = (new ServiceProvider(srk, pProviderId)).getServiceProviderName();
		}
		catch( Exception e) {}
		return ret;
	}
	
	protected int exServiceProviderId() {return mServiceProduct.getServiceProviderId();}	
	protected int exServiceTypeId() {return mServiceProduct.getServiceTypeId();}
	protected String exServiceTypeName() {return exServiceTypeName( mServiceProduct.getServiceTypeId());}
	protected String exServiceTypeName(int pServiceTyeId) {return exFromBXResources("SERVICETYPE", Integer.toString(pServiceTyeId));}
	protected int exServiceSubTypeId() {return mServiceProduct.getServiceSubTypeId();}
	protected String exServiceSubTypeName() {return exServiceSubTypeName(mServiceProduct.getServiceSubTypeId());}
	protected String exServiceSubTypeName(int pServiceSubTypeId) {return exFromBXResources("SERVICESUBTYPE", Integer.toString(pServiceSubTypeId));}	
	protected int exServiceSubSubTypeId() {return mServiceProduct.getServiceSubSubTypeId();}
	protected String exServiceSubSubTypeName() {return exServiceSubSubTypeName(mServiceProduct.getServiceSubSubTypeId());}
	protected String exServiceSubSubTypeName(int pServiceSubSubTypeId) {return exFromBXResources("SERVICESUBSUBTYPE", Integer.toString(pServiceSubSubTypeId));}	
	protected String exProductName() {return mServiceProduct.getServiceProductName();}
	protected int exTransactionTypeId() {return mRequest.getServiceTransactionTypeId();}
	protected String exTransactionTypeName() {return exTransactionTypeName(mRequest.getServiceTransactionTypeId());}
	protected String exTransactionTypeName(int pTransactionId) {return exFromBXResources("SERVICETRANSACTIONTYPE", Integer.toString(pTransactionId));}
	protected int exDatxTimeout() throws Exception { return mRequest.findDatxTimeout(); }
	protected String exServiceProviderRefNo() throws FinderException, RemoteException {
		return (new ServiceRequest(srk, mRequest.getRequestId(), mRequest.getCopyId())).getServiceProviderRefNo();
	}
	protected String exSRContactFirstName() {return mSRContact.getFirstName();}
	protected String exSRContactLastName() {return mSRContact.getLastName();}
	protected String exSRContactEmail() {return mSRContact.getEmailAddress();}
	protected String exSRContactHomePhone() {return mSRContact.getHomeAreaCode() + "-" + mSRContact.getHomePhoneNumber();}
	protected String exSRContactWorkPhone() {return mSRContact.getWorkAreaCode() + "-" + mSRContact.getWorkPhoneNumber();}
	protected String exSRContactWorkExtension() {return mSRContact.getWorkExtension();}
	protected String exSRContactCellPhone() {return mSRContact.getCellAreaCode() + "-" + mSRContact.getCellPhoneNumber();}
	protected String exSRContactComments() {return mSRContact.getComments();}
	protected String exSpecialInstructions() {return mServiceRequest.getSpecialInstructions();}
	protected int exPropertyId() {return mServiceRequest.getPropertyId();}
	
	/**
	 * 
	 * @param pDoc
	 * @return
	 * @throws Exception
	 */
	protected Node exCurrentUser(Document pDoc) throws Exception {
		UserProfile userProfile = new UserProfile(srk);
        // added instituionProfileId for ML
		userProfile = userProfile
               .findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(), 
                                                       srk.getExpressState().getDealInstitutionId()));

		if(userProfile != null){
			return createSubTree(pDoc, "currentUser", srk,  userProfile, true,true, lang, true);
		}
		return null;
	}
	
	/**
	 * 
	 * @param table
	 * @param value
	 * @return
	 */
	private String exFromBXResources(String table, String value) {
		String desc = null;
        try {
            desc = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), table, value, lang);
        } 
        catch(Exception exc) {
            srk.getSysLogger().error("Missing resource bundle: table = " + table + "value = " + value + "lang = " + lang);
            desc = PicklistData.getDescription(table, value);
        }
        return desc;		
	}
}
