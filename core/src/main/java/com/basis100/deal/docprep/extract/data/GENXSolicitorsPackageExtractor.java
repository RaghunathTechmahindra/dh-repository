package com.basis100.deal.docprep.extract.data;

/**
 * 13.Feb.2009 DVG #DG812 FXP23493: Concentra CR207 - Amend interest type code transmission to MI insurers.   
 * 30/Jun/2008 DVG #DG732 FXP21552: Concentra CR206 Condition amendments (2) 
 * 24/Jan/2008 DVG #DG688 LEN217161: Chantal Blanchard MI application fee from AIG does not display in SOLPAK 
 * 15/Jan/2007 DVG #DG568 #5437  Merix - Update Verbiage  for Cond ID's 106, 121, 132, 151 and 324  
 	 		- since there are already many clients, BC condo feature is mosys
 * 21/Dec/2006 DVG #DG562 #2773  CR#177 VRM - Quotation 
			- minor change
 * 30/Oct/2006 DVG #DG534 #5139  CTFS CR 19 MI Premium on Conventional Apps
 * 26/Jul/2006 DVG #DG474 #4061  Xceed Production Issue - Total Deductions Calculation
 * 25/Jul/2006 DVG #DG470 #3930  Concentra - Build Fixes
 * 22/Jun/2006 DVG #DG442 #3360  Concentra - Commitment Letter Updates
 * 31/May/2006 DVG #DG430 #3356  Concentra - Solicitor's Package Cosmetic Changes
 *           could NOT get response back from Enna, so rollingback, hard coded in CT template
 * 04/Apr/2006 DVG #DG398 #2773  CR#177 VRM - Quotation
  - Provincial Clause extraction change
 * 25/Jan/2006 DVG #DG382 #2682  Solicitor Package - for express Standard lenders
 * 24/Oct/2005 DVG #DG344 #2286  CR#132 Solicitors Package changes
  #1670  Concentra - CR #158 (Solicitors package)
  - total conversion from the xml to the xsl format
 * 05/Oct/2005 DVG #DG330 #2199  Solicitor Package (Lender profile)
 * 29/Sep/2005 DVG #DG324 #2059  Merix - E2E - Changes to Solicitor Package
 * 06/Sep/2005 DVG #DG308 #2027  Change to the solicitor package
 * 25/Aug/2005 DVG #DG298 #1632  Merix - E2E - Condition 121 132 and 151 are not firing
 *    - repeated 'sk lender clause'
 * 03/Aug/2005 DVG #DG278	#1617  Merix - E2E - Condition 106 is not firing  #1625 #1632
 * 15/Jul/2005 DVG #DG262 #1804  Solicitor's Package
 * 13/Jul/2005 DVG #DG258 #1793  Add Fee Tag to next build  #1791
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.xml.XmlToolKit;
import com.filogix.util.Xc;

public class GENXSolicitorsPackageExtractor extends GENXExtractor implements Xc
{
	
	//static boolean useBCCondoClauses = PropertiesCache.getInstance().getProperty("com.filogix.docprep.use.BC.condo.clauses", "N").equalsIgnoreCase("Y"); //#DG568 
	
  double MIPremium = 0;   //#DG324

    /**
     * Made this into a separate method so that super classes have the chance to
     * specify which commitment letter data they want.
     * @param root Node
     */
    protected void addCLTags(Node root)
    {
        root.appendChild(new GENXCommitmentExtractor((XSLExtractor)this).getCommitmentTags(doc));
    }

    //#DG238
    /**
     * For compatibility after making this class inherit from DealExtractor/XSLExtractor
     * @throws Exception
     */
    protected void buildDataDoc() throws Exception {
      buildXMLDataDoc();
    }
    /**
     * Builds the XML doc from various records in the database.
     *
     * @throws Exception
     */
    protected void buildXMLDataDoc() throws Exception
    {
		logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());		//#DG812
        doc = XmlToolKit.getInstance().newDocument();
        Node elemNode;
        Element root;
        Element parent;

        parent = doc.createElement("Document");

        //#DG238/#DG312 make a full blown deal tree available so that this xml can be used for all templates
        createDealTree(parent);

        root = doc.createElement("SolicitorsPackage");
        parent.setAttribute("type","1");
        //#DG688 not used parent.setAttribute("lang", "1");

        try
        {
            addCLTags(parent);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error(e);
        }

        doc.appendChild(parent);
        parent.appendChild(root);

        //==========================================================================
        // Language stuff
        //==========================================================================
        addTo(root, getLanguageTag());

        //  Let's keep a running total of the fees...
        bKeepFeeTotal = true;
        double amt = 0;   //#DG474 temp variable

        //==========================================================================
        // Lender Name
        //==========================================================================
        addTo(root, extractLenderName());

        //==========================================================================
        // Mortgage #
        //==========================================================================
        //addTo(root, extractApplicationId("MortgageNum"));
        addTo(root, extractReferenceNum());

        //==========================================================================
        // eRegistation
        //==========================================================================
        addTo(root, extractERegistration());

        //==========================================================================
        // Execution
        //==========================================================================
        addTo(root, extractExecution());

        //==========================================================================
        // Schedule Text
        //==========================================================================
        addTo(root, extractScheduleText());

        //==========================================================================
        // Solicitor Special Instructions
        //==========================================================================
        addTo(root, extractSolicitorSpecialInstructions());

        //==========================================================================
        // Survey Clause
        //==========================================================================
        addTo(root, extractSurveyClause());

        //==========================================================================
        // Land Office Clause
        //==========================================================================
        addTo(root, extractLandOfficeClause());

        //==========================================================================
        // Current Date
        //==========================================================================
        addTo(root, extractCurrentDate());

        //==========================================================================
        // PST
        //==========================================================================
        addTo(root, extractPST());

        //==========================================================================
        // Premium
        //==========================================================================
        //#DG324 bKeepFeeTotal = false;   //#DG382 exclude from total deductions
        // let it add to total fees but then save MI premium amt and
        // subtract it for the sake of #DG382
        MIPremium = totalFees;
        addTo(root, extractPremium());
        MIPremium = totalFees - MIPremium;
        if (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.filogix.docprep.solpackage.include.MIPremium", "N").equalsIgnoreCase("N"))
          totalFees -= MIPremium;
        //#DG324 bKeepFeeTotal = true;   //#DG382

        //==========================================================================
        // Admin Fee
        //==========================================================================
        //#DG474 //#DG258 moved to after 'extractTotalDeductionsAmount' so as not to affect 'TotalDeductionsAmount'
        //#DG474 moved back here and controlled by instance since it may be used by all descendants
        amt = totalFees;
        addTo(root, extractAdminFee());
        if (getClass().equals(GENXSolicitorsPackageExtractor.class))
          totalFees = amt;

        //==========================================================================
        // Fees
        //==========================================================================
        addTo(root, extractFees());

        //==========================================================================
        // IAD Amount
        //==========================================================================
        addTo(root, extractIADAmount());
        addTo(root, extractIADAmountMinus5());

        //==========================================================================
        // Per Diem Interest Amount
        //==========================================================================
        addTo(root, extractPerDiemInterestAmount());

        //==========================================================================
        // Total Deductions Amount - the position of this does affect its output
        //==========================================================================
        addTo(root, extractTotalDeductionsAmount());
        addTo(root, extractTotalDeductionsAmountMinus5());

        //==========================================================================
        // net loan Amount    //#DG258 keep this location since it uses totalfees
        //==========================================================================
        addTo(root, extractNetAdvanceAmount());

        //==========================================================================
        // Admin Fee
        //==========================================================================
        //#DG474 //#DG258 moved here after 'extractTotalDeductionsAmount' so as not to affect 'TotalDeductionsAmount'
        //#DG474 addTo(root, extractAdminFee());

        //==========================================================================
        // advance Amount    //#DG324 KEEP this location since it uses totalfees
        //==========================================================================
        addTo(root, extractAdvanceAmount());

        //==========================================================================
        // Liabilities
        //==========================================================================
        addTo(root, extractLiabilities());

        //==========================================================================
        // LP Description
        //==========================================================================
        addTo(root, extractLPDescription());

        //==========================================================================
        // Borrower Names
        //==========================================================================
        addTo(root, extractBorrowerNames());

        //==========================================================================
        // Guarantor Names
        //==========================================================================
        addTo(root, extractGuarantorNames());

        //==========================================================================
        // Property Address
        //==========================================================================
        addTo(root, extractPropertyAddress());

        //==========================================================================
        // P & I Payment
        //==========================================================================
        //addTo(root, extractPAndIPayment());

        //==========================================================================
        // P & I Payment (Monthly)
        //==========================================================================
        addTo(root, extractPAndIPaymentMonthly());

        //==========================================================================
        // Funder Name
        //==========================================================================
        addTo(root, extractFunderName());

        //==========================================================================
        // Funder Extension
        //==========================================================================
        addTo(root, extractFunderExtension());

        //==========================================================================
        // Administrator Name
        //==========================================================================
        addTo(root, extractAdministratorName());

        //==========================================================================
        // Administrator Data (this one should replace the above method call)
        //==========================================================================
        addTo(root, extractAdministratorData());

        //==========================================================================
        // Interest Rate
        //==========================================================================
        addTo(root, extractInterestRate());

        //==========================================================================
        // Advance Date
        //==========================================================================
        addTo(root, extractAdvanceDate());

        //==========================================================================
        // Loan Amount
        //==========================================================================
        addTo(root, extractLoanAmount());

        //==========================================================================
        // Solicitor section
        //==========================================================================
        addTo(root, extractSolicitorSection());

        //==========================================================================
        // Branch Address
        //==========================================================================
        elemNode = extractBranchAddressSection();

        if (elemNode != null)
        {
            //==========================================================================
            // Branch Phone Number
            //==========================================================================
            //String prefix = (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) ? "Tel: " : "T�l: " ;

            addTo(elemNode, extractBranchPhone(null, true, "Phone"));

            //==========================================================================
            // Branch Fax Number
            //==========================================================================
            //prefix = (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) ? "Fax: " : "T�l�c: " ;

            addTo(elemNode, extractBranchFax(null, true, "Fax"));

            addTo(root, elemNode);
        }

        //==========================================================================
        // Existing Loan Amount
        //==========================================================================
        addTo(root, extractExistingLoanAmount());

        //==========================================================================
        // Guarantor Clause
        //==========================================================================
        addTo(root, extractGuarantorClause());

        //==========================================================================
        // Provincial Clause
        //==========================================================================
        addTo(root, extractProvincialClause());

        //==========================================================================
        // IADDate
        //==========================================================================
        addTo(root, extractIADDate());

        //==========================================================================
        // External Refinance Section
        //==========================================================================
        if (deal.getDealPurposeId() == Mc.DEAL_PURPOSE_REFI_EXTERNAL)
           addTo(root, extractExternalRefiSection());

        //==========================================================================
        // Internal Refinance Section
        //==========================================================================
        if (deal.getDealPurposeId() == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
           addTo(root, extractInternalRefiSection());

        //==========================================================================
        // First Payment Date
        //==========================================================================
        //addTo(root, extractFirstPaymentDate());

        //==========================================================================
        // First Payment Date (Monthly)
        //==========================================================================
        addTo(root, extractFirstPaymentDateMonthly());

        //==========================================================================
        // Maturity Date
        //==========================================================================
        addTo(root, extractMaturityDate());

        //==========================================================================
        // Holdback - on purpose, no output effect on 'TotalDeductionsAmount' for this class
        //==========================================================================
        addTo(root, extractAdvanceHold());

        //==========================================================================
        //#DG308  variable verbiage conditions added for standard

        addTo(root, condVarVerb(Dc.OpenStatem, "OpenStatem"));
        addTo(root, condVarVerb(Dc.BorIdTxt, "BorIdTxt"));
        addTo(root, condVarVerb(Dc.FundingTxt, "FundingTxt"));
        addTo(root, condVarVerb(Dc.ConfirmClosing, "ConfirmClosing"));
        addTo(root, condVarVerb(Dc.TitleInsReq, "TitleInsReq"));
        addTo(root, condVarVerb(Dc.Searches, "Searches"));
        addTo(root, condVarVerb(Dc.SurveyReq, "SurveyReq"));
        addTo(root, condVarVerb(Dc.CondoStrataTitle, "CondoStrataTitle"));
        addTo(root, condVarVerb(Dc.FireInsur, "FireInsur"));
        addTo(root, condVarVerb(Dc.NewConstruction, "NewConstructionTxt"));
        addTo(root, condVarVerb(Dc.ConstructionProgrAdv, "ConstructionProgrAdv"));
        addTo(root, condVarVerb(Dc.AssignmentRents, "AssignmentRents"));
        addTo(root, condVarVerb(Dc.Disclosure, "Disclosure"));
        addTo(root, condVarVerb(Dc.PlanningAct, "PlanningAct"));
        addTo(root, condVarVerb(Dc.MatrimonialPropAct, "MatrimonialPropAct"));
        addTo(root, condVarVerb(Dc.Renew4Guarantors, "Renew4Guarantors"));
        addTo(root, condVarVerb(Dc.CorporationAsBor, "CorporationAsBor"));
        addTo(root, condVarVerb(Dc.SolicitorRpt, "SolicitorRpt"));
        //#DG308  end

        addTo(root, condVarVerb(Dc.RegisteredName, "RegisteredName"));  //#DG330
        addTo(root, condVarVerb(Dc.RegisteredAddress, "RegisteredAddress"));  //#DG330
        addTo(root, condVarVerb(Dc.RegisteredPhoneFax, "RegisteredPhoneFax"));  //#DG330

        //==========================================================================

    }

  protected Node extractERegistration()
    {
        String val = null;

        try
        {
            Property p = getPrimaryProperty();
            //#DG688 if((PicklistData.getDescription("Province",p.getProvinceId())).equals("Ontario"))
            if (p.getProvinceId() == PROVINCE_ONTARIO)
            {
                ConditionHandler ch = new ConditionHandler(srk);

                val = ch.getVariableVerbiage(deal, "Ontario eRegistration Clause",lang);
            }
        }
        catch (Exception e) {}

        return getTag("eRegistration", val);
    }

    protected Node extractExecution()
    {
        String val = null;

        try
        {
            Property prim = getPrimaryProperty();
            int id = prim.getProvinceId();

            ConditionHandler ch = new ConditionHandler(srk);

            val = (id == Mc.PROVINCE_ALBERTA) ?
                ch.getVariableVerbiage(deal, Dc.ALBERTA_EXECUTION, lang) :
                ch.getVariableVerbiage(deal, Dc.EXECUTION, lang);
        }
        catch (Exception e) {}

        return getTag("Execution", val);
    }

    protected Node extractScheduleText()
    {
        String val = null;

        try
        {
            String prodName = "";
            int id = deal.getMtgProdId();
            MtgProd mp = new MtgProd(srk,null);
            mp = mp.findByPrimaryKey(new MtgProdPK(id));

            if (mp != null) prodName = mp.getMPShortName();

            if (prodName.equalsIgnoreCase("VIP"))
            {
                val = "B";
            }
            else
            {
                val = "A";
                //======================================================================
                // removed as per request that came from product team. New condition has
                // to be inserted instead of letter A. The condition id code is 374 and
                // condition name is "scheduletext A"
                //
                //ConditionHandler ch = new ConditionHandler(srk);
                //val = ch.getVariableVerbiage(deal, Dc.SCHEDULE_TEXT_A, lang);
            }
        }
        catch (Exception e) {}

        return getTag("ScheduleText", val);
    }

    protected Node extractSolicitorSpecialInstructions()
    {
        String val = null;

        try
        {
            val = deal.getSolicitorSpecialInstructions();
        }
        catch (Exception e) {}

        return getTag("SolicitorSpecialInstructions", val);
    }

    protected Node extractSurveyClause()
    {
        String val = null;

        try
        {
            int id = deal.getDealPurposeId();
            Property prim = new Property(srk,null);
            prim.findByPrimaryProperty(deal.getDealId(), 
                      deal.getCopyId(), deal.getInstitutionProfileId());
            int ptid = prim.getPropertyTypeId();

            if (id != Mc.DEAL_PURPOSE_PORTABLE_INCREASE &&
                id != Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT &&
                ptid != Mc.PROPERTY_TYPE_CONDOMINIUM &&
                ptid != Mc.PROPERTY_TYPE_FREEHOLD_CONDO &&
                ptid != Mc.PROPERTY_TYPE_LEASEHOLD_CONDO &&
                ptid != Mc.PROPERTY_TYPE_TOWNHOUSE_CONDO)
            {
                ConditionHandler ch = new ConditionHandler(srk);

                // the following has been removed because it has been removed from spec.
                //val += ch.getVariableVerbiage(deal,Dc.SURVEYVERB,lang);
                //val += format.carriageReturn();

                switch (prim.getProvinceId())
                {
                    case Mc.PROVINCE_ALBERTA:
                         val = ch.getVariableVerbiage(deal,"Alberta Survey Clause",lang);
                         break;

                    case Mc.PROVINCE_BRITISH_COLUMBIA:
                         val = ch.getVariableVerbiage(deal,"BC Survey Clause",lang);
                         break;

                    case Mc.PROVINCE_MANITOBA:
                    case Mc.PROVINCE_SASKATCHEWAN:
                         val = ch.getVariableVerbiage(deal,"Man/Sask Survey Clause",lang);
                         break;

                    case Mc.PROVINCE_ONTARIO:
                         val = ch.getVariableVerbiage(deal,"Ontario Survey Clause",lang);
                         break;

                    case Mc.PROVINCE_NEW_BRUNSWICK:
                    case Mc.PROVINCE_NEWFOUNDLAND:
                    case Mc.PROVINCE_NOVA_SCOTIA:
                    case Mc.PROVINCE_PRINCE_EDWARD_ISLAND:
                    case Mc.PROVINCE_QUEBEC:
                    case Mc.PROVINCE_YUKON:
                    case Mc.PROVINCE_NUNAVUT:
                    case Mc.PROVINCE_NORTHWEST_TERRITORIES:
                         val = ch.getVariableVerbiage(deal,"Default", lang);
                         break;
                }
            }
        }
        catch (Exception e) {}

        return getTag("SurveyClause", val);
    }

    protected Node extractLandOfficeClause()
    {
        String val = null;

        try
        {
            Property prim = getPrimaryProperty();
            int id = prim.getProvinceId();

            if (id == Mc.PROVINCE_ALBERTA ||
                id == Mc.PROVINCE_BRITISH_COLUMBIA ||
                id == Mc.PROVINCE_MANITOBA  ||
                id == Mc.PROVINCE_SASKATCHEWAN)
            {
                val = "Land Titles Office";
            }
            else
            {
                val = "Land Registry Office";
            }
        }
        catch (Exception e) {}

        return getTag("LandOfficeClause", val);
    }

    protected Node extractIADAmountMinus5()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getInstance().
                    getFormattedCurrency(deal.getInterimInterestAmount()- 5, lang);
        }
        catch (Exception e){}

        return getTag("IADAmountMinus5", val);
    }

    protected Node extractTotalDeductionsAmount()
    {
        String val = null;

        try
        {
            val = formatCurrency(totalFees);
        }
        catch (Exception e) {}

        return getTag("TotalDeductionsAmount", val);
    }

    protected Node extractTotalDeductionsAmountMinus5()
    {
        String val = null;

        try
        {
            val = formatCurrency(totalFees - 5);
        }
        catch (Exception e) {}

        return getTag("TotalDeductionsAmountMinus5", val);
    }

    protected Node extractLiabilities()
    {
        Node elemNode = null;

        try
        {
            Liability l = new Liability(srk,null);
            Node lineNode = null;

            List out = (List)l.findByDealAndPayoffType( (DealPK)deal.getPk(),
               Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS ) ;
            List outClosed = (List)l.findByDealAndPayoffType( (DealPK)deal.getPk(),
               Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE  ) ;

            out.addAll(outClosed);

            List outFiltered = filterLiabilities(out);

            //  Now that we have the liabilities, add them as nodes
            elemNode = doc.createElement("Liabilities");

            Iterator i = outFiltered.iterator();
            String liabDesc, liabAmt;

            while (i.hasNext())
            {
                l = (Liability)i.next();
                lineNode = doc.createElement("Liability");

                liabDesc = (!isEmpty(l.getLiabilityDescription())) ?
                            l.getLiabilityDescription() :
                            //PicklistData.getDescription("LiabilityType", l.getLiabilityTypeId());
                            //ALERT:BX:ZR Release2.1.docprep
                            BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LiabilityType", l.getLiabilityTypeId(), lang);

                liabAmt = formatCurrency(l.getLiabilityAmount());

                addTo(lineNode, getTag("Description", liabDesc));
                addTo(lineNode, getTag("Amount", liabAmt));
                addTo(elemNode, lineNode);
            }
        }
        catch (Exception e) {}

        return elemNode;
    }

    protected Node extractLPDescription()
    {
        String val = null;

        try
        {
            switch (deal.getLienPositionId())
            {
                case 0: val = "First";
                        break;

                case 1: val = "Second";
                        break;
            }
        }
        catch (Exception e)  {}

        return getTag("LPDescription", val);
    }

    protected Node extractFunderExtension()
    {
        String val = null;

        try
        {
            int upId = deal.getFunderProfileId();
            UserProfileBeanPK upbPK = new UserProfileBeanPK(upId, 
                                                            deal.getInstitutionProfileId());
            UserProfile up = new UserProfile(srk);

            up = up.findByPrimaryKey(upbPK);

            Contact contact = up.getContact();

            if (contact != null)
                val =  contact.getContactPhoneNumberExtension();
        }
        catch (Exception e)  {}

        return getTag("FunderExtension", val);
    }

    protected Node extractAdministratorData(){
      Node elemNode = doc.createElement("Administrator");
      try
      {
        String val;
        UserProfile up = new UserProfile(srk);
        up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                       deal.getInstitutionProfileId()));

        //#DG238  //#DG534 rewritten to include language
        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "UserType", up.getUserTypeId(), lang);
        if (isEmpty(val))
          val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "UserType", Mc.USER_TYPE_ADMIN, lang);
        addTo(elemNode, "userDescription", val);
        //#DG238 end

          Contact c = up.getContact();
          if (!isEmpty(c.getContactFirstName())){
             val = c.getContactFirstName();
             addTo(elemNode,"contactFirstName",val);
          }

          if (!isEmpty(c.getContactLastName())){
              val =  c.getContactLastName();
              addTo(elemNode,"contactLastName",val);
          }

          if (!isEmpty(c.getContactPhoneNumberExtension())){
              val =  c.getContactPhoneNumberExtension();
              addTo(elemNode,"contactPhoneNumberExtension",val);
          }

          if (!isEmpty(c.getContactEmailAddress())){
              val =  c.getContactEmailAddress();
              addTo(elemNode,"contactEmailAddress",val);
          }

      }
      catch(Exception exc){}

      return elemNode;
    }

    protected Node extractAdministratorName()
    {
        String val = null;

        try
        {
            UserProfile up = new UserProfile(srk);
	    up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(),
                                                       deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            val = new String();

            if (!isEmpty(c.getContactFirstName()))
               val += c.getContactFirstName();

            if (!isEmpty(c.getContactLastName()))
                val += format.space() + c.getContactLastName();
        }
        catch(Exception exc){}

        return getTag("AdministratorName", val);
    }

    protected Node extractExistingLoanAmount()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getExistingLoanAmount());
        }
        catch(Exception exc){}

        return getTag("ExistingLoanAmount", val);
    }


    //#DG398 #2773  Provincial Clause extraction VRM change - rewritten
    protected Node extractProvincialClause()
    {
        StringBuffer val = null;

        try {
          val = extractProvincialClauseVal();
        }
        catch(Exception exc){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("ProvincialClause"), val.toString()));
    }

    /**
     * extract a StringBuffer value for provincial clause
     * @return StringBuffer
     * @throws Exception
     */
    private StringBuffer extractProvincialClauseVal() throws Exception {
      final String carriageReturn = "�";
      StringBuffer val;

      //int id = deal.getDealPurposeId();
      int mid = deal.getMIIndicatorId();
      // is this a variable mortgage product?
      //#DG562 int condClaus = deal.getMtgProdId() == 16? 1: 0;   // this indexes VRM conditions clauses
      //#DG812 for concentra Adjustable is to use VRM clauses 
      //int condClaus = deal.getInterestTypeId() == Mc.INTEREST_TYPE_VRM ? 1 : 0; // this indexes VRM conditions clauses
      final int intType = deal.getInterestTypeId();
      // this indexes VRM conditions clauses
      int condClaus = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_ENVIRONMENT, "N").equals("CON")?		
              intType == INTEREST_TYPE_VRM || intType == INTEREST_TYPE_ADJUSTABLE ? 1 : 0: 
              intType == INTEREST_TYPE_VRM ? 1 : 0; 
      int claus[];   //holds VRM and non VRM conditions clauses
      int cond;

      Property prim = getPrimaryProperty();
      int ptid = prim.getPropertyTypeId();
      int prid = prim.getProvinceId();

      ConditionHandler ch = new ConditionHandler(srk);
      val = new StringBuffer();

      boolean useBCCondoClauses = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.filogix.docprep.use.BC.condo.clauses", "N").equalsIgnoreCase("Y");
      //#DG442 is GE insurer to be considered as conventional? (CT specific)
      //#DG732 extending above ^ : use insured clauses ONLY if CMHC insurer (CT specific) 
      boolean useInsurOnlyIfCmhc = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DOCPREP_SOLPACKAGE_IS_GECONVENTION, "N").equalsIgnoreCase("Y");
      // set the two sub booleans (below province)
      int insurer = deal.getMortgageInsurerId();
      boolean useInsured =
          (mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED) &&
           //#DG278 deal.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
           insurer != Mc.MI_INSURER_BLANK
           //#DG732 && !(isGEConvention && insurer == Mc.MI_INSURER_GE );
           && (!useInsurOnlyIfCmhc || insurer == Mc.MI_INSURER_CMHC);

      boolean propType = (ptid == Mc.PROPERTY_TYPE_CONDOMINIUM ||
          ptid == Mc.PROPERTY_TYPE_FREEHOLD_CONDO ||
          ptid == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO ||
          ptid == Mc.PROPERTY_TYPE_STRATA ||  //#DG442
          ptid == Mc.PROPERTY_TYPE_TOWNHOUSE_CONDO);

      if (prid == Mc.PROVINCE_NUNAVUT)
      {

      }
      //#DG398 NEW - YK and NWT added
      else if( prid == Mc.PROVINCE_NORTHWEST_TERRITORIES )
      {
        val.append(ch.getVariableVerbiage(deal, Dc.NWT_DELETIONS_CLAUSE ,lang));
        val.append(ch.getVariableVerbiage(deal, Dc.NWT_LENDER_CLAUSE ,lang));
        val.append(ch.getVariableVerbiage(deal, Dc.NWT_OFFER_LETTER_CLAUSE ,lang));

        claus = useInsured?
            new int[] {Dc.NWT_INSURED_CLAUSE, Dc.NWT_INSURED_CLAUSE_VRM}:
            new int[] {Dc.NWT_CONVENTIONAL_CLAUSE, Dc.NWT_CONVENTIONAL_CLAUSE_VRM};
        cond = claus[condClaus];
        val.append( ch.getVariableVerbiage(deal, cond, lang));
      }
      else if( prid ==  Mc.PROVINCE_YUKON ){
        val.append(ch.getVariableVerbiage(deal, Dc.YUKON_DELETIONS_CLAUSE ,lang));
        val.append(ch.getVariableVerbiage(deal, Dc.YUKON_LENDER_CLAUSE ,lang));
        val.append(ch.getVariableVerbiage(deal, Dc.YUKON_OFFER_LETTER_CLAUSE ,lang));

        claus = useInsured?
            new int[] {Dc.YUKON_INSURED_CLAUSE, Dc.YUKON_INSURED_CLAUSE_VRM}:
            new int[] {Dc.YUKON_CONVENTIONAL_CLAUSE, Dc.YUKON_CONVENTIONAL_CLAUSE_VRM};
        cond = claus[condClaus];
        val.append( ch.getVariableVerbiage(deal, cond, lang));
      }
      else if(prid == Mc.PROVINCE_ALBERTA)
      {
        if(useInsured) {
          claus = propType ?
              new int[] {Dc.ALBERTA_INSURED_CONDO_CLAUSE, Dc.ALBERTA_INSURED_CONDO_CLAUSE_VRM}:
              new int[] {Dc.ALBERTA_INSURED_CLAUSE, Dc.ALBERTA_INSURED_CLAUSE_VRM};
        }
        else {
          claus = new int[] {Dc.ALBERTA_CONVENTIONAL_CLAUSE, Dc.ALBERTA_CONVENTIONAL_CLAUSE_VRM};
        }
        cond = claus[condClaus];
        val.append( ch.getVariableVerbiage(deal, cond, lang));

        val.append(carriageReturn);
        val.append(ch.getVariableVerbiage(deal, Dc.ALBERTA_CLAUSE, lang));
      }
      else if(prid == Mc.PROVINCE_BRITISH_COLUMBIA)
      {
          val.append(ch.getVariableVerbiage(deal, Dc.BC_HEADER_CLAUSE, lang));
          val.append( carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, "BC Lender Clause", lang));
          val.append(  carriageReturn);

          //#DG568 rewritten to include condo clauses
          if(useInsured) {
            claus = propType && useBCCondoClauses?
                  new int[] {Dc.BC_INSURED_CONDO, Dc.BC_INSURED_CONDO_VRM}:
                  new int[] {Dc.BC_INSURED_CLAUSE, Dc.BC_INSURED_CLAUSE_VRM};
          }
          else {
            claus = new int[] {Dc.BC_CONVENTIONAL_CLAUSE, Dc.BC_CONVENTIONAL_CLAUSE_VRM};
          }
          cond = claus[condClaus];
          val.append( ch.getVariableVerbiage(deal, cond, lang));

          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.BC_LAND_TITLE_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.BC_FORM_DATE_CLAUSE, lang));
          //val.append(  carriageReturn);
          //val.append(  ch.getVariableVerbiage(deal, Dc.BC_ADDRESS ,lang));  // #DG430
      }
      else if(prid == Mc.PROVINCE_MANITOBA)
      { //#DG278 rewritten
          val.append(  ch.getVariableVerbiage(deal, Dc.MANITOBA_HEADER_CLAUSE, lang))
              .append( carriageReturn);

          claus = useInsured?
              new int[] {Dc.MANITOBA_INSURED_CLAUSE, Dc.MANITOBA_INSURED_CLAUSE_VRM}:
              new int[] {Dc.MANITOBA_CONVENTIONAL_CLAUSE, Dc.MANITOBA_CONVENTIONAL_CLAUSE_VRM};
          cond = claus[condClaus];
          val.append( ch.getVariableVerbiage(deal, cond, lang));

          val.append( carriageReturn)
              .append( ch.getVariableVerbiage(deal, Dc.MANITOBA_FORM_DATE_CLAUSE, lang))
              .append( carriageReturn)
              .append( ch.getVariableVerbiage(deal, Dc.MANITOBA_LENDER_CLAUSE, lang));
      }
      else if(prid == Mc.PROVINCE_NEW_BRUNSWICK)
      {
        if(useInsured) {
          claus = propType ?  //#DG442
              new int[] {Dc.NB_INSURED_CONDO_CLAUSE, Dc.NB_INSURED_CONDO_CLAUSE_VRM}:
              new int[] {Dc.NB_INSURED_CLAUSE, Dc.NB_INSURED_CLAUSE_VRM};
        }
        else {
          val.append(  carriageReturn);
          claus = new int[] {Dc.NB_CONVENTIONAL_CLAUSE, Dc.NB_CONVENTIONAL_CLAUSE_VRM};
        }
        cond = claus[condClaus];
        val.append( ch.getVariableVerbiage(deal, cond, lang));

        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal, Dc.NB_LENDER_CLAUSE, lang));
        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal, Dc.NB_DELETIONS_CLAUSE, lang));
        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal, Dc.NB_OFFER_LETTER_CLAUSE, lang));
      }
      else if(prid == Mc.PROVINCE_NEWFOUNDLAND )
      {
          val.append(  carriageReturn);

          claus = useInsured?
              new int[] {Dc.NF_INSURED_CLAUSE, Dc.NF_INSURED_CLAUSE_VRM}:
              new int[] {Dc.NF_CONVENTIONAL_CLAUSE, Dc.NF_CONVENTIONAL_CLAUSE_VRM};
          cond = claus[condClaus];
          val.append( ch.getVariableVerbiage(deal, cond, lang));

          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.NF_LENDER_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.NF_DELETIONS_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.NF_OFFER_LETTER_CLAUSE, lang));
      }
      else if(prid == Mc.PROVINCE_NOVA_SCOTIA)
      {
        if(useInsured) {
          claus = propType ?  //#DG442
              new int[] {Dc.NOVA_SCOTIA_INSURED_CONDO_CLAUSE, Dc.NS_INSURED_CONDO_CLAUSE_VRM}:
              new int[] {Dc.NOVA_SCOTIA_INSURED_CLAUSE, Dc.NOVA_SCOTIA_INSURED_CLAUSE_VRM};
        }
        else {
          val.append(  carriageReturn);
          claus = new int[] {Dc.NOVA_SCOTIA_CONVENTIONAL_CLAUSE, Dc.NOVA_SCOTIA_CONVENTIONAL_CLAUSE_VRM};
        }
        cond = claus[condClaus];
        val.append( ch.getVariableVerbiage(deal, cond, lang));

        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal, Dc.NOVA_SCOTIA_LENDER_CLAUSE, lang));
        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal,Dc.NOVA_SCOTIA_DELETIONS_CLAUSE,lang));
        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal,Dc.NOVA_SCOTIA_OFFER_LETTER_CLAUSE,lang));
      }
      else if(prid == Mc.PROVINCE_ONTARIO)
      {
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_HEADER_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_LENDER_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_EREGISTRATION_CLAUSE, lang));
          val.append(  carriageReturn);

          claus = useInsured?
              new int[] {Dc.ONTARIO_INSURED_CLAUSE, Dc.ONTARIO_INSURED_CLAUSE_VRM}:
              new int[] {Dc.ONTARIO_CONVENTIONAL_CLAUSE, Dc.ONTARIO_CONVENTIONAL_CLAUSE_VRM};
          cond = claus[condClaus];
          val.append( ch.getVariableVerbiage(deal, cond, lang));

          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_LRRA_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_SPOUSAL_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_FORM_DATE_CLAUSE, lang));

          /* #DG430
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_ADDRESS,lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.ONTARIO_TERANET,lang));
          // #DG430 end */
      }
      else if(prid == Mc.PROVINCE_PRINCE_EDWARD_ISLAND)
      {
          val.append(  carriageReturn);

          claus = useInsured?
              new int[] {Dc.PEI_INSURED_CLAUSE, Dc.PEI_INSURED_CLAUSE_VRM}:
              new int[] {Dc.PEI_CONVENTIONAL_CLAUSE, Dc.PEI_CONVENTIONAL_CLAUSE_VRM};
          cond = claus[condClaus];
          val.append( ch.getVariableVerbiage(deal, cond, lang));

          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.PEI_LENDER_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.PEI_DELETIONS_CLAUSE, lang));
          val.append(  carriageReturn);
          val.append(  ch.getVariableVerbiage(deal, Dc.PEI_OFFER_LETTER_CLAUSE, lang));
      }
      else if(prid == Mc.PROVINCE_SASKATCHEWAN)
      {
        if(useInsured) {
          claus = propType ?  //#DG442
              new int[] {Dc.SK_INSURED_CONDO_CLAUSE, Dc.SK_INSURED_CONDO_CLAUSE_VRM}:
              new int[] {Dc.SK_INSURED_CLAUSE, Dc.SK_INSURED_CLAUSE_VRM};
        }
        else {
          val.append(  carriageReturn);
          claus = new int[] {Dc.SK_CONVENTIONAL_CLAUSE, Dc.SK_CONVENTIONAL_CLAUSE_VRM};
        }
        cond = claus[condClaus];
        val.append( ch.getVariableVerbiage(deal, cond, lang));

        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal, Dc.SK_LENDER_CLAUSE, lang));
        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal, Dc.SK_DELETIONS_CLAUSE, lang));
        val.append(  carriageReturn);
        val.append(  ch.getVariableVerbiage(deal, Dc.SK_OFFER_LETTER_CLAUSE, lang));
      }
      return val;
    }

    protected Node extractFees()    {
      try
      {
        Node retNode = doc.createElement("Fees");
        //#DG534 skip some fees
        int payor = deal.getMIPayorId();
        boolean MIUpfront = deal.getMIUpfront() != null && deal.getMIUpfront().equalsIgnoreCase("Y");
        Node elemNode;

        DealFee dealFee = new DealFee(srk,null);
        // #DG688 added AIG. These fees will the default for the mossys property
        int[] feeTypesDef = { Mc.FEE_TYPE_APPRAISAL_FEE,
            Mc.FEE_TYPE_BRIDGE_FEE,
            Mc.FEE_TYPE_BUYDOWN_FEE,
            Mc.FEE_TYPE_CMHC_FEE,
            Mc.FEE_TYPE_STANDBY_FEE,
            Mc.FEE_TYPE_ORIGINATION_FEE,
            Mc.FEE_TYPE_APPLICATION_FEE,
            Mc.FEE_TYPE_INSPECTION_FEE,
            Mc.FEE_TYPE_GE_CAPITAL_FEE,
            Mc.FEE_TYPE_PROPERTY_TAX_HOLDBACK,
            Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL,
            Mc.FEE_TYPE_PROPERTY_TAX_HOLDBACK_INTERIM,
            Mc.FEE_TYPE_PROPERTY_TAX_HOLDBACK_FINAL,
            Mc.FEE_TYPE_COUREER_FEE,
            Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED,
            Mc.FEE_TYPE_TITLE_INSURANCE_FEE,
            Mc.FEE_TYPE_AIG_UG_FEE};
        final int[] feeTypes = TypeConverter.intArrayTypeFromProp(COM_FILOGIX_DOCPREP_SOLPACKAGE_FEES, feeTypesDef);
        
        List fees = (List)dealFee.findByDealAndTypes((DealPK)deal.getPk(), feeTypes );

        Iterator i = fees.iterator();
        String type;

        while (i.hasNext())
        {
          dealFee = (DealFee)i.next();

          if( dealFee.getFeePaymentMethodId() == Dc.FEE_PayMethodCheque)  //#DG262
            continue;

          //#DG534 maybe criteria here should be harmonized with overriden GENXExtractor.extractFees
          //#DG688 double feeAmt = dealFee.getFeeAmount();
          int feeStatusId = dealFee.getFeeStatusId();
          Fee fee = dealFee.getFee();

          // if FEE_STATUS_REQUIRED || FEE_STATUS_RECIEVED
          //boolean isPayable = fee.getPayableIndicator();
          int feeType = fee.getFeeTypeId();

          //#DG688
          //if (//#DG534 mayb should the same as:GENXExtractor.extractFees...  isPayable || feeAmt == 0d ||
          //    ! (feeStatusId == Mc.FEE_STATUS_REQUIRED || feeStatusId == Mc.FEE_STATUS_RECIEVED)   //#DG470
          //    || ( MIUpfront && payor == Mc.MI_PAYOR_LENDER &&
          //        (feeType == Mc.FEE_TYPE_CMHC_FEE || feeType == Mc.FEE_TYPE_GE_CAPITAL_FEE )
          //    ))
          //  continue;
          if (!(feeStatusId == Mc.FEE_STATUS_REQUIRED || feeStatusId == Mc.FEE_STATUS_RECIEVED))
             continue;
          if (MIUpfront && payor == Mc.MI_PAYOR_LENDER) 
             for (int j = 0; j <= DealCalcUtil.MI_FEES.length; j++) 
                if (feeType == DealCalcUtil.MI_FEES[j])
                   continue;

          elemNode = doc.createElement("Fee");
          type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", feeType, lang); //#DG688

          addTo(elemNode, "FeeVerb", type);
          double oneAmt = dealFee.getFeeAmount();
          addTo(elemNode, "FeeAmount", formatCurrency(oneAmt));
          addTo(retNode, elemNode);

          totalFees += oneAmt;    //#DG258
        }

        return retNode;
      }
      catch (Exception e)
      {
        return null;
      }
    }

    protected Node extractInternalRefiSection()
    {
        Node elemNode = null;

        try
        {
            elemNode = doc.createElement("InternalRefi");

            //  TODO:  Add more nodes relevant only to refinance situations
        }
        catch(Exception exc){}

        return elemNode;
    }

    //#DG258
    /**
     * Calculates net advance = loan amount - total fees
     * @return Node
     */
    protected Node extractNetAdvanceAmount() {
      String val = null;

      try {
        val = formatCurrency(calcLoanAmount() - totalFees);
      }
      catch (Exception exc) {}

      return getTag("NetAdvanceAmount", val);
    }

    //#DG324
    /**
     * Calculates advance = total loan amount - total fees - ins.pst
     * @return Node
     */
    protected Node extractAdvanceAmount() {
      String val = null;

      try {
        double tmp = deal.getTotalLoanAmount() - totalFees;
        val = formatCurrency(tmp);
      }
      catch (Exception exc) {}

      return getTag("AdvanceAmount", val);
    }

    protected Node extractExternalRefiSection()
    {
        Node elemNode = null;

        try
        {
            elemNode = doc.createElement("ExternalRefi");

            //  TODO:  Add more nodes relevant only to refinance situations
        }
        catch(Exception exc){}

        return elemNode;
    }

    protected Node extractAdvanceHold()
    {
        Node node = super.extractAdvanceHold();

        if (node != null)
           totalFees += deal.getAdvanceHold();

        return node;
    }

    private List filterLiabilities(List parList)
    {
        ListIterator li = parList.listIterator();
        List retList = new java.util.ArrayList();

        while( li.hasNext() )
        {
            Liability lLiability = (Liability)li.next();
            //System.out.println("ZR: liability :" + lLiability.getPercentOutGDS() );
            if( lLiability.getPercentOutGDS() < 2 )
            {
                retList.add(lLiability);
            }
        }
        return retList;
    }

    //#DG308 many var. verbiage items
    /**
     * create a node <code>tagName</code> with the text taken from the condition
     * verbiage of given <code>condId</code>
     * @param condId int condition id to retrieve
     * @param tagName String name of node to create
     * @return Node the node
     */
    protected Node condVarVerb(int condId, String tagName) {
      String val = null;
      try {
        ConditionHandler ch = new ConditionHandler(srk);
        val = ch.getVariableVerbiage(deal, condId, lang);
      }
      catch (Exception e) {}

      return val == null? null : convertCRToLines(doc.createElement(tagName), val);
    }
}
