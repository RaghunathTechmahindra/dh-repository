package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.entity.*;
import MosSystem.Mc;


public class TransferringLenderSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;

    try
    {
      PartyProfile pp = new PartyProfile(srk);

      List col = (List)pp.findByDealAndType((DealPK)deal.getPk(), Mc.PARTY_TYPE_TRANSFERRING_LENDER);

      if(!col.isEmpty())
      {
        fml.addValue((PartyProfile)col.get(0));
      }
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
