package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;

public class PropertyPurchasePrice extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    double amt = 0.0;

    try
    {
      if(clsid == ClassId.PROPERTY)
      {
         Property p = (Property)de;
         amt = p.getPurchasePrice();
         fml.addCurrency(amt);
      }
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;

        Property pp = new Property(srk,null);
        pp = pp.findByPrimaryProperty(deal.getDealId(),deal
            .getCopyId(), deal.getInstitutionProfileId());
        amt = pp.getPurchasePrice();
        fml.addCurrency(amt);

      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
