package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class SolicitorReportList extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;

    try
    {
      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal.getDealId(), deal
          .getCopyId(), deal.getInstitutionProfileId());
      //int ptid = prim.getPropertyTypeId();
      int prid = prim.getProvinceId();

      ConditionHandler ch = new ConditionHandler(srk);

      if(prid == Mc.PROVINCE_ALBERTA)
      {
       val += ch.getVariableVerbiage(deal,"Title Certificate",lang);
       val += format.carriageReturn();

       val += ch.getVariableVerbiage(deal,"Solicitor Certificate",lang);
       val += format.carriageReturn();

       val += ch.getVariableVerbiage(deal,"Duplicate Mortgage Form 2",lang);
      }
      else if(prid == Mc.PROVINCE_BRITISH_COLUMBIA)
      {
       val += ch.getVariableVerbiage(deal,"Solicitor Certificate",lang);
       val += format.carriageReturn();

       val += ch.getVariableVerbiage(deal,"Duplicate Mortgage Form 2",lang);
       val += format.carriageReturn();

       val += ch.getVariableVerbiage(deal,"Direction",lang);
      }
      else if(prid == Mc.PROVINCE_MANITOBA  || prid == Mc.PROVINCE_SASKATCHEWAN
              || prid == Mc.PROVINCE_NEW_BRUNSWICK || prid == Mc.PROVINCE_PRINCE_EDWARD_ISLAND
              || prid == Mc.PROVINCE_ONTARIO)
      {
       val += ch.getVariableVerbiage(deal,"Solicitor Certificate",lang);
       val += format.carriageReturn();

       val += ch.getVariableVerbiage(deal,"Duplicate Mortgage Form 2",lang);
       val += format.carriageReturn();

      }
      else if( prid == Mc.PROVINCE_NEWFOUNDLAND || prid == Mc.PROVINCE_NOVA_SCOTIA)
      {
       val += ch.getVariableVerbiage(deal,"Solicitor Certificate",lang);
       val += format.carriageReturn();

       val += ch.getVariableVerbiage(deal,"Duplicate Mortgage Form 2",lang);
       val += format.carriageReturn();


       val += ch.getVariableVerbiage(deal,"Disclosure and Waiver",lang);
      }

      fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
