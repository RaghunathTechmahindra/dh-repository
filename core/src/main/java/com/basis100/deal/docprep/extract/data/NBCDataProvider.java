/**
 *
 */
package com.basis100.deal.docprep.extract.data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: NBCDataProvider.java
 * </p>
 * <p>
 * Description: NBCDataProvider.java
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 *
 * @author NBC/PP Implementation Team
 * @version xxx
 */
public class NBCDataProvider {
	private Deal deal;

	// private DealFee dealFee;

	private SessionResourceKit srk;

	private int lang;

	private DocumentFormat format;

	private static final String CHECKED_CHKBOX = "1";

	private static final String UNCHECKED_CHKBOX = "0";

	private static final int INT_ZERO_CHECK = 0;

	private static final int INT_ONE_CHECK = 1;

	//private static final int PROPERTY_TYPE_LEASED_LAND = 20;

        // fixes for SCR#1494 - start
private static final int PAYMENT_TERM_OPEN = 1;

private static final int PAYMENT_TERM_CLOSED = 0;
                // fixes for SCR#1494 - end

	private static final int PROD_TYPE_ID_ZERO = 0;

	private static final String PLUS = "+";

	private static final String MINUS = "-";

	private static final String DATE_SEPARATOR = "/";

	private static final String CHECK_YES = "Y";

	private static final String CHECK_NO = "N";

	private static final int PROD_TYPE_MTG_EXTENSION = 25;

	private static final int MTGPRODID_SELOC = 25;

	private static final int MTGPRODID_FREE = 26;

	private static final int MTGPRODID_BUDGET = 28;

	private static final int MTGPRODID_15 = 15;

	private static final int MTGPRODID_17 = 17;

	private static final int MTGPRODID_30 = 30;

	private static final int MTGPRODID_31 = 31;

	private static final int MTGPRODID_HOME=-1;

	//private static final int ESCROW_ADMIN_FEE = 7;

	//private static final int DEAL_PURPOSE_SWITCH = 1;

	private static final int DOC_RESPON_ROLE_ID_0 = 0;

	private static final int DOC_RESPON_ROLE_ID_50 = 50;

	private static final int DOC_RESPON_ROLE_ID_51 = 51;

	private static final int DOC_RESPON_ROLE_ID_90 = 90;

	private static final String OTHER_SPEC_ENG = "administration fees";

	private static final String OTHER_SPEC_FRNCH = "frais d�administration";

	MtgProd mtgProd;

	ServiceRequestContact serviceReqCntct;

	ServiceRequest serviceReq;

	PricingProfile pp = null;

	MtgProdPK mtgPK = null;

	Collection borrowersList;

	Property pProperty;

	Request req;

	RequestPK reqPK = null;

	/**
	 * A constructor for this class.
	 *
	 * @param pSrk
	 * @param pDeal
	 * @param pLang
	 */
	public NBCDataProvider(SessionResourceKit pSrk, Deal pDeal, int pLang) {
		deal = pDeal;
		srk = pSrk;
		lang = pLang;

		try {
			mtgProd = new MtgProd(srk, null);
			mtgPK = new MtgProdPK(deal.getMtgProdId());
			mtgProd = mtgProd.findByPrimaryKey(mtgPK);

			pp = deal.getActualPricingProfile();

			borrowersList = deal.getBorrowers();

			/*
			 * b = new Borrower(srk, null); b =
			 * b.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
			 */

			pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());

			req = new Request(srk);
			req = req.findByDealIdAndCopyId(deal.getDealId(), deal.getCopyId());

			reqPK = new RequestPK(req.getRequestId(), req.getCopyId());

			serviceReqCntct = new ServiceRequestContact(srk);

			serviceReq = new ServiceRequest(srk, req.getRequestId(), req
					.getCopyId());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed in the Constructor"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
	}

	/**
	 * setDocumentFormat Method description
	 *
	 * @param pFormat
	 */
	protected void setDocumentFormat(DocumentFormat pFormat) {
		format = pFormat;
	}

	/**
     * exModSubrogn Returns true if the triggering condition for Doc. 15021 is true (Product Type is Mortgage &
     * Deal purpose is switch)
     *
     * @return boolean
     */
    protected boolean exModSubrogn() {
        boolean val = false;
        try {
            if ((deal.getDealPurposeId() == Mc.DEAL_PURPOSE_TRANSFER)
                && (deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE))
                val = true;
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting ModSubrogn"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

    /**
     * exStDisclosure Returns true if the triggering condition for Doc. 12641 is true (Product Type is
     * Mortgage)
     *
     * @return boolean
     */
    protected boolean exStDisclosure() {
        boolean val = false;
        try {
            if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE)
                val = true;
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting StDisclosure"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

    /**
     * exLOCredit Returns true if the triggering condition for Doc. 15721 is true (Product Type is Secured LOC &
     * Deal purpose is not switch)
     *
     * @return boolean
     */
    protected boolean exLOCredit() {
        boolean val = false;
        try {
            if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC)
                && (deal.getDealPurposeId() != Mc.DEAL_PURPOSE_TRANSFER))
                val = true;
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting LOCredit"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

    /**
     * exAgMtgLoan Returns true if the triggering condition for Doc. 17918 is true (Product Type is Mortgage,
     * Province is not Quebec & Deal purpose is not switch)
     *
     * @return Boolean
     */
    protected boolean exAgMtgLoan() {
        boolean val = false;
        try {
            /*
             * if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE) && (pProperty.getProvinceId() ==
             * Mc.PROVINCE_QUEBEC)) {
             */

            if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE)
                && (pProperty.getProvinceId() != Mc.PROVINCE_QUEBEC)
                && (deal.getDealPurposeId() != Mc.DEAL_PURPOSE_TRANSFER)) {
                val = true;
            }
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting AgTermLoan"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

    /**
     * exAgTermLoanImmHyp Returns true if the triggering condition for Doc. 14597 is true (Product Type is
     * Mortgage, Province is Quebec & Deal purpose is not switch)
     *
     * @return Boolean
     */
    protected boolean exAgTermLoanImmHyp() {
        boolean val = false;
        try {
            /*
             * if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE) && (pProperty.getProvinceId() ==
             * Mc.PROVINCE_QUEBEC)) {
             */

            if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE)
                && (pProperty.getProvinceId() == Mc.PROVINCE_QUEBEC)
                && (deal.getDealPurposeId() != Mc.DEAL_PURPOSE_TRANSFER)) {
                val = true;
            }
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting AgTermLoan"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

	/**
	 * exTotalLoanAmount Returns toal payment amount as a string
	 *
	 * @return
	 */
	protected String exTotalLoanAmount() {
		String val = "";
		try {
			val = formatCurrency(deal.getTotalLoanAmount());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting TotalLoanAmount"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exTeaserPIAmount Returns the formatted TeaserPI amount as a rate
	 *
	 * @return String <Formatted TeaserPIAmount as Rate>
	 */
	protected String exTeaserPIAmount() {
		String val = "";
		try {
		  //Added condtion to put nothing if the value is 0.0
		  	 if(deal.getTeaserPIAmount()!=0)
			val = formatCurrency(deal.getTeaserPIAmount());
		}
		catch (Exception e) {
			String msg =
				"NBCDataProvider failed while extracting TeaserPIAmount"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;

	}

	/**
	 * exRegCharge If Deal.DPDescription = Purchase (Deal.DealPurposeID = 0)
	 * then returns the purchase price, except if the appraisal value is not
	 * null and is less than the purchase price, then returns the appraisal
	 * value.
	 *
	 * @return
	 */
	protected String exRegCharge() {
		String val = "";
		try {
			// 24/08/07 Venkata, Added new condition as per NBC CR017 Document Logic Update. Ticket # FXP17792 - Start
			//  estimated value = Primary Property's EstimatedAppraisalValue
			double estimatedApprValue = 0d;
			try {
				Property pProperty = new Property(srk, null);
				pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
						.getCopyId(), deal.getInstitutionProfileId());
				estimatedApprValue = pProperty.getEstimatedAppraisalValue();

			} catch (Exception e) { }
			
			if(estimatedApprValue != 0d)
			{
				val = formatCurrency(estimatedApprValue);
			}
			else
			{
			// 24/08/07 Venkata, Added new condition as per NBC CR017 Document Logic Update. Ticket # FXP17792 - end
			if (deal.getDealPurposeId() == Mc.DEAL_PURPOSE_PURCHASE) {
				if ((deal.getTotalActAppraisedValue() != 0d)
						&& (deal.getTotalActAppraisedValue() < deal
								.getTotalPurchasePrice())) {
					val = formatCurrency(deal.getTotalActAppraisedValue());
				} else {
					val = formatCurrency(deal.getTotalPurchasePrice());
				}
			} else {
				if (deal.getTotalActAppraisedValue() != 0d) {
					val = formatCurrency(deal.getTotalActAppraisedValue());
					}
					// 03/08/07 Venkata, Commented as per NBC CR017 Document Logic Update. Ticket # FXP17792
					/*
					else 
					{
					val = formatCurrency(deal.getTotalEstAppraisedValue());
				}
					*/
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting RegCharge"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;

	}

	/**
	 * exTeaserRate Returns the formatted TeaserNetInterestRate as a rate
	 *
	 * @return String <Formatted TeaserNetInterestRate as Rate>
	 */
	protected String exTeaserRate() {
		double rate = 0d;
		String val = "";
		try {
			rate = deal.getTeaserNetInterestRate();
			//Added condtion to put nothing if the value is 0.0
			if(rate!=0)
			val = String.valueOf(rate);
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting TeaserRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exSelectedFeesTotalAmt Checks the current deal's fees against feeIDs
	 * included in pFeeArray and sums those that have been used
	 *
	 * @param pFeeArray
	 *            int[]
	 * @return double
	 */
	protected double exSelectedFeesTotalAmt(int[] pFeeArray) {
		double val = 0;
		try {
			Collection dealFees = new ArrayList();
			dealFees = deal.getDealFees();
			Iterator it = dealFees.iterator();
			double feeTotal = 0;
			DealFee oneFee = null;
			while (it.hasNext()) {
				oneFee = new DealFee(srk, CalcMonitor.getMonitor(srk));
				oneFee = (DealFee) it.next();
				int feeID = oneFee.getFeeId();
				for (int i = 0; i < pFeeArray.length; i++) {
					if (feeID == pFeeArray[i]) {
						feeTotal += oneFee.getFeeAmount();
						break;
					}
				}
			}
			val = feeTotal;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting SelectedFeesTotalAmt"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exSolFees <Confirmation awaited>
	 *
	 * @return
	 */
	protected String exSolFees() {
		String val = "";
		double feeTotal = 0;
		try {
			Collection dealFees = deal.getDealFees();
			Iterator it = dealFees.iterator();
			while (it.hasNext()) {
				DealFee oneFee = (DealFee) it.next();
				if (oneFee.getFeeId() == Mc.FEE_TYPE_CTFS_SOLICITOR_FEE) {
					feeTotal += oneFee.getFeeAmount();
				}
			}
			val = formatCurrency(feeTotal);
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting SolFees"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exVarRateAdjFreq Returns InterestRateChangeFrequency if
	 * Deal.InterestTypeID <>0 & InterestRateChangeFrequency >0 else Blank
	 *
	 * @return String <InterestRateChangeFrequency>
	 */
	protected String exVarRateAdjFreq() {
		String val = "";
		try {
			if ((deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)
					&& (pp.getInterestRateChangeFrequency() > 0)) {
//fixes for SCR#1708 - start
				val = String.valueOf(formatDouble(pp.getInterestRateChangeFrequency()));
//fixes for SCR#1708 - end
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting VarRateAdjFreq"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exTerm1 Returns InterestRateChangeFrequency if Deal.InterestTypeID <>0 &
	 * ProductType is PRODUCT_TYPE_SECURED_LOC else Blank
	 *
	 * @return String <InterestRateChangeFrequency>
	 */
	protected String exTerm1() {
		String val = "";
		try {
			if ((deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)
					&& ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE) || (deal
							.getProductTypeId() == PROD_TYPE_ID_ZERO))) {
//fixes for SCR#1708 - start
				val = String.valueOf(formatDouble(pp.getInterestRateChangeFrequency()));
//fixes for SCR#1708 - end
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting Term1"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exFixedRateLoanTerm Returns InterestRateChangeFrequency if deal.MTGPRODID =
	 * 15 or 17
	 *
	 * @return String <InterestRateChangeFrequency>
	 */
	protected String exFixedRateLoanTerm() {
		String val = "";
		try {
			if (deal.getMtgProdId() == MTGPRODID_15
					|| deal.getMtgProdId() == MTGPRODID_17) {
//fixes for SCR#1708 - start
				val = String.valueOf(formatDouble(pp.getInterestRateChangeFrequency()));
//fixes for SCR#1708 - end
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting FixedRateLoanTerm"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exIntRevFreq Returns InterestRateChangeFrequency if Deal.InterestTypeID
	 * <>0 & ProductType is PRODUCT_TYPE_MORTGAGE else Blank
	 *
	 * @return String <InterestRateChangeFrequency>
	 */
	protected String exIntRevFreq() {
		String val = "";
		Calendar c = Calendar.getInstance();
		long intFreqFactor = 0l;
		try {
			if ((pp.getInterestRateChangeFrequency() > 0)
					&& ((deal.getProductTypeId() == PROD_TYPE_ID_ZERO) || (deal
							.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE))) {
				if (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_MONTHLY) {
					c.setTime(deal.getInterimInterestAdjustmentDate());
					c.add(Calendar.MONTH, (int) pp
							.getInterestRateChangeFrequency());
				} else if ((deal.getPaymentFrequencyId() == Mc.PAY_FREQ_BIWEEKLY)
						|| (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)) {
					intFreqFactor = Math.round(pp
							.getInterestRateChangeFrequency() * 26 / 12 * 14);
					c.setTime(deal.getInterimInterestAdjustmentDate());
					c.add(Calendar.WEEK_OF_YEAR, (int) intFreqFactor); // Fixing Solicitor Issue # 94 - Changed from Month to Week of year
				} else if ((deal.getPaymentFrequencyId() == Mc.PAY_FREQ_WEEKLY)
						|| (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_WEEKLY)) {
					intFreqFactor = Math.round(pp
							.getInterestRateChangeFrequency() * 52 / 12 * 7);
					c.setTime(deal.getInterimInterestAdjustmentDate());
					c.add(Calendar.WEEK_OF_YEAR, (int) intFreqFactor); // Fixing Solicitor Issue # 94 - Changed from Month to Week of year
				}
			} else {
				c.setTime(deal.getInterimInterestAdjustmentDate());
			}
			val = formatDate(c.getTime());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting IntRevFreq"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exIntRateRevFreq Returns InterestRateChangeFrequency if
	 * Deal.InterestTypeID <>0 & InterestRateChangeFrequency >0 & ProductTypeId =
	 * PRODUCT_TYPE_MORTGAGE else Blank
	 *
	 * @return String <InterestRateChangeFrequency>
	 */
	protected String exIntRateRevFreq() {
		String val = "";
		try {
			if ((deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)
					&& ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE) || (deal
							.getProductTypeId() == INT_ZERO_CHECK))
					&& (pp.getInterestRateChangeFrequency() > 0)) {
//fixes for SCR#1708 - start
				val = String.valueOf(formatDouble(pp.getInterestRateChangeFrequency()));
//fixes for SCR#1708 - end
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting IntRateRevFreq"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exIntRateRevFreq2 method is used to display the InterestRateChangeFrequency in Weeks.
	 * Depending upon the PaymentFrequncy, the stored value is calculated to
	 * express the result in terms of week.
	 *
	 * @return String - Formatted number of weeks with two places after decimal.
	 */
	// Added Logic for Bi-Weekly and Monthly
	protected String exIntRateRevFreq2() {
		String val = "";
		DecimalFormat df = new DecimalFormat();
		df.applyPattern("###,##0.00"); // Applying the format for the number.
		if ((deal.getPaymentFrequencyId() == Mc.PAY_FREQ_WEEKLY)
				|| (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_WEEKLY)) {
//fixes for SCR#1708 - start
			val = String.valueOf(formatDouble(pp.getInterestRateChangeFrequency() * 4));
		}
		if ((deal.getPaymentFrequencyId() == Mc.PAY_FREQ_BIWEEKLY)
				|| (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)) {
//			val = String.valueOf(df
//					.format(pp.getInterestRateChangeFrequency() * 2));
				val = String.valueOf(formatDouble(pp.getInterestRateChangeFrequency() * 2));
		}
		if (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_SEMIMONTHLY) {
//			val = String.valueOf(df
//					.format((pp.getInterestRateChangeFrequency() * 365)
//							/ (24 * 7))); // Converted the Semi-monthly data to days and then convert into weeks.
			val = String.valueOf(formatDouble((pp.getInterestRateChangeFrequency() * 365)
							/ (24 * 7))); // Converted the Semi-monthly data to days and then convert into weeks.
//fixes for SCR#1708 - end
		}
		if (val != null && (val.trim().length() == 0 || "0".equals(val.trim()))) {
			srk.getSysLogger().debug(this.getClass(),
			 "NBCDataProvider.exIntRateRevFreq2: val length or value is 0, set val to null.");
			val = null;
		}
		return val;
	}

	/**
	 * exIntRateRevFreq2 Method description
	 *
	 * @return
	 */

	protected String exIntRateRevFreq1() {
		String val = "";
		try {
			if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE)
					&& (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)
					&& (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_MONTHLY)) {
//fixes for SCR#1708 - start
				val = String.valueOf(formatDouble(pp.getInterestRateChangeFrequency()));
//fixes for SCR#1708 - end
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting IntRateRevFreq1"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		if (val != null && (val.trim().length() == 0 || "0".equals(val.trim()))) {
			srk.getSysLogger().debug(this.getClass(),
			 "NBCDataProvider.exIntRateRevFreq1: val length or value is 0, set val to null.");
			val = null;
		}
		return val;
	}

	/**
	 * exTerm2 If PricingRegistrationTerm is not null, returns the same
	 * otherwise returns the ActualPaymentTerm
	 *
	 * @return String <PricingRegistrationTerm or ActualPaymentTerm>
	 */
	protected String exTerm2() {
		String term = "";
		try {
			if (pp.getPricingRegistrationTerm() != 0d) {
// fixes for SCR#1708 - start
				term = String.valueOf(formatDouble(pp.getPricingRegistrationTerm()));
// fixes for SCR#1708 - end
			} else {
				term = String.valueOf(deal.getActualPaymentTerm());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting Term2"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return term;
	}

	/**
	 * exTotAllPmts Returns the total of all payments according to frequency. If
	 * PricingRegistrationTerm is not null , use PricingRegistrationTerm else
	 * use ActualPaymentTerm
	 *
	 * @return String <Tot. of all Payments (Pricipal & Interest)>
	 */
	protected String exTotAllPmts() {
		double term = 0d;
		String val = "";
		try {
			if (pp.getPricingRegistrationTerm() != 0d)
				term = pp.getPricingRegistrationTerm();
			else
				term = deal.getActualPaymentTerm();

			if (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_MONTHLY) {
				val = String.valueOf(deal.getPandiPaymentAmount() * term);
			} else if ((deal.getPaymentFrequencyId() == Mc.PAY_FREQ_BIWEEKLY)
					|| (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)) {
				val = String.valueOf(deal.getPandiPaymentAmount() * term * 26
						/ 12);
			} else {
				val = String.valueOf(deal.getPandiPaymentAmount() * term * 52
						/ 12);
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting TotAllPmts"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exAppFees Returns the sum of the Fee Amounts for the FeeIDs present in
	 * the array passed in as a parameter.
	 *
	 * @param pFeeArray
	 *            An array consisting of similar kinds of fees (Viz. Appraisal
	 *            fees etc.)
	 * @return double <Sum of the FeeAmounts of the Fees present in the array>
	 */
	protected double exAppFees(int[] pFeeArray) {
		double val = 0;
		try {
			Collection dealFees = deal.getDealFees();
			Iterator it = dealFees.iterator();
			double feeTotal = 0;

			while (it.hasNext()) {
				DealFee oneFee = (DealFee) it.next();
				int feeID = oneFee.getFeeId();
				for (int i = 0; i < pFeeArray.length; i++) {
					if (feeID == pFeeArray[i]) {
						feeTotal += oneFee.getFeeAmount();
						break;
					}
				}
			}
			val = feeTotal;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting AppFees"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exTeaserTerm Returns the TeaserTerm
	 *
	 * @return String
	 */
	protected String exTeaserTerm() {
		String val = "";
		int finalTerm = 0;
		try {
		  //Added condtion to put nothing if the value is 0.0
		  if(deal.getTeaserTerm()!=0){
			  finalTerm = (deal.getTeaserTerm() * 12) / 365;  // Modified the logic to calculate the number of days to months.
			  val = Integer.toString(finalTerm);
		  }
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting TeaserTerm"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exFixEffInterestRate Returns a checked checkbox if the Interest Type is
	 * fixed & unchecked checkbox otherwise
	 *
	 * @return String <"0" or "1" for unchecked/unchecked checkbox>
	 */
	protected String exFixEffInterestRate() {
		String val = null;
		try {
			if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting FixEffInterestRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exVarEffInterestRate Returns a checked checkbox if the Interest Type is
	 * Capped & unchecked checkbox otherwise
	 *
	 * @return String <"0" or "1" for unchecked/unchecked checkbox>
	 */
	protected String exVarEffInterestRate() {
		String val = null;
		try {
			if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_CAPPED) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting VarEffInterestRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exFixPromRate Method description
	 *
	 * @return
	 */

	protected double exFixPromRate() {
		double val = 0d;

		try {
			if ((deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)
					&& (deal.getTeaserTerm() > INT_ZERO_CHECK)
					&& (deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE))
				val = deal.getTeaserNetInterestRate();

		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting FixPromRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * Returns Total payment amount as a string
	 *
	 * @return String
	 */
	protected String exTotalPurchasePrice() {
		String val = "";
		try {
			val = formatCurrency(deal.getTotalPurchasePrice());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting TotalPurchasePrice"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPrimaryBorrowerName1 Returns primary borrower's name in the desired
	 * format
	 *
	 * @return String <Borrower First Name> <Middle Initial> <Last Name>
	 */
	protected String exPrimaryBorrowerName() {
		String val = "";
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();

					if (borrower.isPrimaryBorrower()
							&& borrower.getBorrowerTypeId() == Mc.BT_BORROWER) {
						val = formBorrowerName(borrower.getBorrowerFirstName(),
								borrower.getBorrowerMiddleInitial(), borrower
										.getBorrowerLastName());
						break;
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PrimaryBorrowerName1"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPrimaryBorrowerName2 Returns Co-borrower's name in the desired format
	 *
	 * @return String <Borrower First Name> <Middle Initial> <Last Name>
	 */
	protected String exCoBorrowersName() {
		String val = null;
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();

					if (!borrower.isPrimaryBorrower()
							&& borrower.getBorrowerTypeId() == Mc.BT_BORROWER) {
						if (val != null && val.length() != 0) {
							val = val
									+ ", "
									+ formBorrowerName(borrower
											.getBorrowerFirstName(), borrower
											.getBorrowerMiddleInitial(),
											borrower.getBorrowerLastName());
						} else {
							val = formBorrowerName(borrower
									.getBorrowerFirstName(), borrower
									.getBorrowerMiddleInitial(), borrower
									.getBorrowerLastName());
						}
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PrimaryBorrowerName1"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPrimaryBorrowerName2 Returns Co-borrower's name in the desired format
	 *
	 * @return String <Borrower First Name> <Middle Initial> <Last Name>
	 */
	protected String exGuarantorNames() {
		String val = "";
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();

					if (!borrower.isPrimaryBorrower()
							&& borrower.getBorrowerTypeId() == Mc.BT_GUARANTOR) {
						if (val != null && val.length() != 0) {
							val = val
									+ ", "
									+ formBorrowerName(borrower
											.getBorrowerFirstName(), borrower
											.getBorrowerMiddleInitial(),
											borrower.getBorrowerLastName());
						} else {
							val = formBorrowerName(borrower
									.getBorrowerFirstName(), borrower
									.getBorrowerMiddleInitial(), borrower
									.getBorrowerLastName());
						}
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting GuarantorName/s"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}


	protected String[] GuaranterNamesforLoop()
	{
	  	ArrayList val = new ArrayList();
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();
					if (borrower.getBorrowerTypeId() == Mc.BT_GUARANTOR) {
						val.add(formBorrowerName(borrower
								.getBorrowerFirstName(), borrower
								.getBorrowerMiddleInitial(), borrower
								.getBorrowerLastName()));
						}
					}
				}
		} catch (Exception e) {
			String msg = "NBCExtractor failed while extracting Guaranter names name6"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
	  String[] guaranterNames_list=new String[val.size()];
	  for(int loopcntr =0;loopcntr<val.size();loopcntr++)
	  {
		guaranterNames_list[loopcntr]=(String)val.get(loopcntr);
	  }
	  return guaranterNames_list;
	}

	protected String[] BorrowerNamesforLoop()
	{

	 ArrayList val = new ArrayList();

		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();
					if (borrower.getBorrowerTypeId() == Mc.BT_BORROWER) {

						val.add( formBorrowerName(borrower
								.getBorrowerFirstName(), borrower
								.getBorrowerMiddleInitial(), borrower
								.getBorrowerLastName()));
						}
					}
				}

		} catch (Exception e) {
			String msg = "NBCExtractor failed while extracting Borrower name6"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		String[] borrowernames_list = new String[val.size()];
	 for (int loopcntr=0;loopcntr<val.size();loopcntr++)
	 {
	   borrowernames_list[loopcntr]= (String)val.get(loopcntr);

	 }
	  return borrowernames_list;
	}



	/**
	 * Retrieves and formats the Guarantor Names in the desired format
	 *
	 * @return String <Borrower First Name> <Middle Initial> <Last Name>
	 */
	protected String exQuebecGuarantorNames(Property pp) {

		String val = "";
		try {
			if (borrowersList != null) {

				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();

					if ( borrower.getBorrowerTypeId() == Mc.BT_GUARANTOR
							/*&& pp.getPrimaryPropertyFlag() == 'Y'
							&& pp.getProvinceId() == Mc.PROVINCE_QUEBEC*/) {
						if (val != null && val.length() != 0) {
							val = val
									+ ", "
									+ formBorrowerName(borrower
											.getBorrowerFirstName(), borrower
											.getBorrowerMiddleInitial(),
											borrower.getBorrowerLastName());
						} else {
							val = formBorrowerName(borrower
									.getBorrowerFirstName(), borrower
									.getBorrowerMiddleInitial(), borrower
									.getBorrowerLastName());
						}
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting GuarantorName/s"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exGuarantorName3 Returns a node containing the GuarantorName & address in
	 * one line for all the guarantors in the deal
	 *
	 * @param tagName
	 * @return
	 */
	protected String exGuarantorNameWithAddress() {

		String val = "";
		try {
			if (borrowersList != null) {

				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();

					if (!borrower.isPrimaryBorrower()
							&& borrower.getBorrowerTypeId() == Mc.BT_GUARANTOR) {
						if (val != null && val.length() != 0) {
							val = val
									+ ", "
									+ formBorrowerName(borrower
											.getBorrowerFirstName(), borrower
											.getBorrowerMiddleInitial(),
											borrower.getBorrowerLastName());
						} else {
							val = formBorrowerName(borrower
									.getBorrowerFirstName(), borrower
									.getBorrowerMiddleInitial(), borrower
									.getBorrowerLastName());
						}
						Collection borrowerAddresses = borrower
						.getBorrowerAddresses();
						if (borrowerAddresses != null) {
							Iterator borrowerAddressesIt = borrowerAddresses
									.iterator();
							while (borrowerAddressesIt.hasNext()) {
								BorrowerAddress badd = (BorrowerAddress) borrowerAddressesIt
										.next();
								// Checks If Borrower Address Type is 'Current'
								if (badd.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT) {
									Addr add = badd.getAddr();
									//***** Change by NBC Impl. Team - Bug Fix #1470 - Start *****//
									String formAdd = formatBorAddress(add);
									if(formAdd != null && formAdd.trim().length() != 0){
									  val = val + ", " + formAdd;
									}
									//***** Change by NBC Impl. Team - Bug Fix #1470 - End *****//
									break;
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting GuarantorName/s"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * @return
	 */
	protected String[] exCoBorNameAddress() {
		String[] val = new String[borrowersList.size() - 1];
		int coBorrowerCount = 0;
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();
					if (!borrower.isPrimaryBorrower()
							&& borrower.getBorrowerTypeId() == Mc.BT_BORROWER) {
						val[coBorrowerCount] = formBorrowerName(borrower
								.getBorrowerFirstName(), borrower
								.getBorrowerMiddleInitial(), borrower
								.getBorrowerLastName());

						Collection borrowerAddresses = borrower
								.getBorrowerAddresses();

						if (borrowerAddresses != null) {
							val[coBorrowerCount] += exCurrentAddress(borrowerAddresses);
						}

						/*
						 * if (borrowerAddresses != null) { Iterator
						 * borrowerAddressesIt = borrowerAddresses .iterator();
						 * while (borrowerAddressesIt.hasNext()) {
						 * BorrowerAddress badd = (BorrowerAddress)
						 * borrowerAddressesIt .next(); // Checks If Borrower
						 * Address Type is 'Current' if
						 * (badd.getBorrowerAddressTypeId() ==
						 * Mc.BT_ADDR_TYPE_CURRENT) { Addr add = badd.getAddr();
						 * val[coBorrowerCount] += formatBorAddress(add); break; } } }
						 */
						coBorrowerCount++;
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCExtractor failed while extracting Borrower name6"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	protected String[] exCoBorName() {
		String[] val = new String[borrowersList.size() - 1];
		int coBorrowerCount = 0;
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();
					if (!borrower.isPrimaryBorrower()
							&& borrower.getBorrowerTypeId() == Mc.BT_BORROWER) {
						val[coBorrowerCount] = formBorrowerName(borrower
								.getBorrowerFirstName(), borrower
								.getBorrowerMiddleInitial(), borrower
								.getBorrowerLastName());

						coBorrowerCount++;
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCExtractor failed while extracting Borrower name6"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * @param borObj
	 * @return
	 */
	protected String exPrimaryBorNameAddress() {
		String val = "";
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();

					if (borrower.isPrimaryBorrower()
							&& borrower.getBorrowerTypeId() == Mc.BT_BORROWER) {
						val = formBorrowerName(borrower.getBorrowerFirstName(),
								borrower.getBorrowerMiddleInitial(), borrower
										.getBorrowerLastName());

						Collection primBorrowerAddresses = borrower
								.getBorrowerAddresses();
						if (primBorrowerAddresses != null) {
							val += ", ";
							val += exCurrentAddress(primBorrowerAddresses);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PrimaryBorrowerName1"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	private String exCurrentAddress(Collection borrowerAddresses) {
		String formattedAddress = "";
		try {
			if (borrowerAddresses != null) {
				Iterator borrowerAddressesIt = borrowerAddresses.iterator();
				while (borrowerAddressesIt.hasNext()) {
					BorrowerAddress badd = (BorrowerAddress) borrowerAddressesIt
							.next();
					// Checks If Borrower Address Type is 'Current'
					if (badd.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT) {
						Addr add = badd.getAddr();
						formattedAddress = formatBorAddress(add);
						break;
					}
				}
			}
		} catch (FinderException fe) {
			String msg = "NBCDataProvider failed while formatting current address"
					+ (fe.getMessage() != null ? "\n" + fe.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		} catch (RemoteException re) {
			String msg = "NBCDataProvider failed while formatting current address"
					+ (re.getMessage() != null ? "\n" + re.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return formattedAddress;
	}

	/**
	 * exInterestRate Return a checked Checkbox if the Interest Type is fixed
	 * else leave blank
	 *
	 * @param interestType
	 *            interestType = fixed interestType = variable
	 * @return
	 */
	protected String exInterestRate(int interestType) {
		String val = "";
		try {
			if (interestType == Mc.INTEREST_TYPE_FIXED) {
				if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
					val = CHECKED_CHKBOX;
				} else {
					val = UNCHECKED_CHKBOX;
				}
			} else {
				if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
					val = CHECKED_CHKBOX;
				} else {
					val = UNCHECKED_CHKBOX;
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting InterestRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		if (val != null && val.equals("0")) {
			srk.getSysLogger().debug(this.getClass(), "NBCDataProvider.exInterestRate: val is 0, set val to null.");
			val = null;
		}
		return val;
	}

	/**
	 * exVariableInterestRate Method description
	 *
	 * @return
	 */
	protected String exVariableInterestRate() {
		String val = "";
		try {
			if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting VariableInterestRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPOSContactName Method description
	 *
	 * @return
	 */
	protected String exPOSContactName() {
		String val = null;

		try {
			SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();

			if (sobp.getContact() != null) {
				Contact c = sobp.getContact();

				val = new String();

				if (!isEmpty(c.getContactFirstName()))
					val += c.getContactFirstName() + format.space();

				if (!isEmpty(c.getContactMiddleInitial()))
					val += c.getContactMiddleInitial() + "." + format.space();

				if (!isEmpty(c.getContactLastName()))
					val += c.getContactLastName();
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting POSContactName"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exMtgCheck Return a checked checkbox if the ProductType is Mortgage
	 *
	 * @return
	 */

	protected String exMtgCheck() {
		String val = "";
		try {
			if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE)
					|| (deal.getProductTypeId() == PROD_TYPE_ID_ZERO)) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MtgCheck"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exLocCheck Return a checked checkbox if the ProductType is SELOC
	 *
	 * @return
	 */
	protected String exLocCheck() {
		String val = "";
		try {
			if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting LocCheck"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exBorHomPhoneNo Returns the Client's Home Telephone number
	 *
	 * @return
	 * Change: Reformatted returned phone number
	 */
	protected String exBorHomPhoneNo() {
		String val = "";
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();
					if (borrower.isPrimaryBorrower() && borrower.getBorrowerHomePhoneNumber() != null) {
					  	val = StringUtil.getAsFormattedPhoneNumber(borrower.getBorrowerHomePhoneNumber(), "");
						// Break the loop after a primary borrower is found
						break;
					}
				}

			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting BorHomPhoneNo"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exBorWorkPhoneNo Returns the Client's Work Telephone number & extension
	 *
	 * @return
	 * Change: Reformatted returned phone number
	 */
	protected String exBorWorkPhoneNo() {
		String val = "";
		try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();
					if (borrower.isPrimaryBorrower()) {
					  if (borrower.getBorrowerWorkPhoneNumber() != null) {
						val = StringUtil.getAsFormattedPhoneNumber(borrower.getBorrowerWorkPhoneNumber(), borrower.getBorrowerWorkPhoneExtension());

					  }
						// Break the loop after a primary borrower is found
						break;
					}
				}

			}

		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting BorWorkPhoneNo"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPropApt Method description
	 *
	 * @return
	 */
	protected String exPropApt() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			val = pProperty.getUnitNumber();

		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PropApt"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exPropCity Returns the Property's City Name
	 *
	 * @return
	 */
	protected String exPropCity() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			val = pProperty.getPropertyCity();

		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PropCity"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exPropLotNo Returns the Property's Lot no.
	 *
	 * @return
	 */
	protected String exPropLotNo() {
		String val = "";
		try {
			val = pProperty.getLegalLine1();
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PropLotNo"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exPropPostalCode Returns the Property's Postal Code
	 *
	 * @return
	 */
	protected String exPropPostalCode() {
		String val = "";
		try {
			val += pProperty.getPropertyPostalFSA() + format.space();
			val += pProperty.getPropertyPostalLDU();
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PropPostalCode"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exPropProvince Returns the Property's Province
	 *
	 * @return
	 */
	protected String exPropProvince() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());

				val += BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PROVINCE", pProperty.getProvinceId(), lang);

		}
		catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PropProvince"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exResSingleUnit Method description
	 *
	 * @return
	 */
	protected String exResSingleUnit() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());

			boolean chkDwellTypeID = (pProperty.getDwellingTypeId() == Mc.DWELLING_TYPE_DETACHED)
					|| (pProperty.getDwellingTypeId() == Mc.DWELLING_TYPE_SEMI_DETACHED);
			boolean chkPropertyTypeID = (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_COMMERCIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_ACREAGE)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_APARTMENT_WITH_STORES)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_RETAIL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_OFFICE_BUILDING)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_INDUSTRIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LAND_FARM)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_AGRICULTURAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LEASED_LAND);
			if (chkDwellTypeID && chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting ResSingleUnit"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exResDuplex Method description
	 *
	 * @return
	 */
	protected String exResDuplex() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			boolean chkDwellTypeID = pProperty.getDwellingTypeId() == Mc.DWELLING_TYPE_DUPLEX;
			boolean chkPropertyTypeID = (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_COMMERCIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_ACREAGE)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_APARTMENT_WITH_STORES)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_RETAIL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_OFFICE_BUILDING)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_INDUSTRIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LAND_FARM)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_AGRICULTURAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LEASED_LAND);
			if (chkDwellTypeID && chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting ResDuplex"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exResTriplex Method description
	 *
	 * @return
	 */
	protected String exResTriplex() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			boolean chkDwellTypeID = pProperty.getDwellingTypeId() == Mc.DWELLING_TYPE_TRI_PLEX;
			boolean chkPropertyTypeID = (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_COMMERCIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_ACREAGE)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_APARTMENT_WITH_STORES)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_RETAIL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_OFFICE_BUILDING)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_INDUSTRIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LAND_FARM)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_AGRICULTURAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LEASED_LAND);
			if (chkDwellTypeID && chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting ResTriplex"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exResQplex Method description
	 *
	 * @return
	 */
	protected String exResQplex() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			boolean chkDwellTypeID = pProperty.getDwellingTypeId() == Mc.DWELLING_TYPE_FOUR_PLEX;
			boolean chkPropertyTypeID = (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_COMMERCIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_ACREAGE)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_APARTMENT_WITH_STORES)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_RETAIL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_OFFICE_BUILDING)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_INDUSTRIAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LAND_FARM)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_AGRICULTURAL)
					&& (pProperty.getPropertyId() != Mc.PROPERTY_TYPE_LEASED_LAND);
			if (chkDwellTypeID && chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting Qplex"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exCommUnit Return checked box if Property type is a Commercial Unit
	 *
	 * @return String <Checked/Unchecked checkbox>
	 */
	protected String exCommUnit() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			boolean chkPropertyTypeID = (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_COMMERCIAL)
					|| (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_APARTMENT_WITH_STORES)
					|| (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_RETAIL)
					|| (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_OFFICE_BUILDING);

			if (chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting CommUnit"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exIndUnit Return checked box if Property type is Industrial
	 *
	 * @return String <Checked/Unchecked checkbox>
	 */
	protected String exIndUnit() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			boolean chkPropertyTypeID = (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_INDUSTRIAL);

			if (chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting IndUnit"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exVacLot Return checked box if Property type is Acreage or Fixed Land
	 *
	 * @return String <Checked/Unchecked checkbox>
	 */
	protected String exVacLot() {
		String val = "";
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			boolean chkPropertyTypeID = (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_ACREAGE)
					|| (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_LEASED_LAND);

			if (chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting VacLot"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exFarmUnit Method description
	 *
	 * @return String <Checked/Unchecked checkbox>
	 */
	protected String exFarmUnit() {
		String val = null;
		try {
			Property pProperty = new Property(srk, null);
			pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			boolean chkPropertyTypeID = (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_LAND_FARM)
					|| (pProperty.getPropertyId() == Mc.PROPERTY_TYPE_AGRICULTURAL);

			if (chkPropertyTypeID) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting FarmUnit"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exFirstMtg Returns the TotalLoanAmount if the LienPositionID is 0
	 *
	 * @return
	 * Change: use formatCurrency, instead of String.getValue
	 */
	protected String exFirstMtg() {
		String val = null;
		try {
			if (deal.getLienPositionId() == INT_ZERO_CHECK) {
				val = formatCurrency(deal.getTotalLoanAmount());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting FirstMtg"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exSecondMtg Method description
	 *
	 * @return
	 * Change: use formatCurrency, instead of String.getValue
	 */
	protected String exSecondMtg() {
		String val = null;
		try {
			if (deal.getLienPositionId() != INT_ZERO_CHECK) {
				val = formatCurrency(deal.getTotalLoanAmount());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting SecondMtg"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * exCashBkAmt Returns the Cashback Amount from the deal table
	 *
	 * @return
	 */
	protected String exCashBkAmt() {
		String val = "";
		try {
		  	// Addded condition for Null check
		  if(deal.getCashBackAmount()!=0d)
			val = formatCurrency(deal.getCashBackAmount());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting CashBkAmt"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * formBorrowerName Method description
	 *
	 * @param brFirstName
	 * @param brMiddleInitial
	 * @param brLastName
	 * @return
	 */
	public String formBorrowerName(String brFirstName, String brMiddleInitial,
			String brLastName) {
		String val = "";
		try {
			if (!isEmpty(brFirstName) && !isEmpty(brLastName))
				val = brFirstName + format.space();

			if (!isEmpty(brMiddleInitial))
				val += brMiddleInitial + "." + format.space();

			val += brLastName;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while forming Borrower Names"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exAmortizationTermInYears Returns amortization term of the mortgage in
	 * years
	 *
	 * @return String
	 */
	protected String exAmortizationTerm() {
		String val = "";
		try {
			val = Integer.toString(deal.getAmortizationTerm());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting AmortizationTerm"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exCommitmentExpirationDate Returns commitment issue date as string
	 *
	 * @return String
	 */
	protected String exCommitmentExpirationDate() {
		String val = "";
		try {
			val = formatDate(deal.getCommitmentExpirationDate());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting CommitmentExpirationDate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPerFlexLine Returns CHECKED if deal.MTGProdID = MTGPRODID_SELOC (id = 30 )else
	 * returns <Checkbox> UNCHECKED or CHECKED
	 *
	 * @return String
	 */
	protected String exPerFlexLine1() {
		String val = "";
		try {
			if (deal.getMtgProdId() == MTGPRODID_30)
				val = CHECKED_CHKBOX;
			else
				val = UNCHECKED_CHKBOX;
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exPerFlexLine2 Returns CHECKED if deal.MTGProdID =
	 * MTGPRODID_30 else
	 *
	 * @return <Checkbox> UNCHECKED or CHECKED
	 */
	protected String exPerFlexLine2() {
		String val = "";
		try {
			if (deal.getMtgProdId() == MTGPRODID_HOME)
				val = CHECKED_CHKBOX;
			else
				val = UNCHECKED_CHKBOX;
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exAllInOne Returns CHECKED if deal.MTGProdID =
	 * MTGPRODID_31 else
	 *
	 * @return <Checkbox> UNCHECKED or CHECKED
	 */
	protected String exAllInOne() {
		String val = "";
		try {
			if (deal.getMtgProdId() == MTGPRODID_31)
				val = CHECKED_CHKBOX;
			else
				val = UNCHECKED_CHKBOX;
		} catch (Exception e) {
		}
		return val;
	}

    /**
     * exFixedPayAmount Returns PandIPaymentAmount if deal.MTGProdID = 15 0r 17
     * else returns blank
     *
     * @return String
     * @version <br>
     * Date: 11/04/2006 <br>
     * Change: <br>
     * 	Changed return type to string and added check for fixed interest rate type
     */
    protected String exFixedPayAmount() {
      	String val = "";
        try {
            if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED &&
            	(deal.getMtgProdId() == MTGPRODID_15
                    || deal.getMtgProdId() == MTGPRODID_17)) {

                // only set if interest type is variable
                val = formatCurrency(deal.getPandiPaymentAmount() + deal.getAdditionalPrincipal());
            }
        } catch (Exception e) {
        }

        return val;
    }
	/**
	 * exPerFlexLine Returns CriticalRate if deal.MTGProdID = 15 0r 17 else
	 * returns blank
	 *
	 * @return String
	 */
	protected String exCriticalRate() {
		String val = "";

		try {
            if (deal.getMtgProdId() == MTGPRODID_15 || deal.getMtgProdId() == MTGPRODID_17){
                // Ticket 1615--start//
                int paymentFrequency = 1;
                switch (deal.getPaymentFrequencyId()) {
                case Mc.PAY_FREQ_MONTHLY:
                    paymentFrequency = 12;
                    break;
                case Mc.PAY_FREQ_BIWEEKLY:
                // cascade down
                case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:
                    paymentFrequency = 26;
                    break;
                case Mc.PAY_FREQ_WEEKLY:
                // cascade down
                case Mc.PAY_FREQ_ACCELERATED_WEEKLY:
                    paymentFrequency = 52;
                    break;
                default:
                    break;
                }

                if (deal.getTotalLoanAmount() != 0) {
                    DecimalFormat decimalFormat = new DecimalFormat();
                    decimalFormat.setMaximumFractionDigits(2);
                    val = String.valueOf(decimalFormat.format((deal.getPandiPaymentAmount() / deal.getTotalLoanAmount()) * paymentFrequency * 100));
                } else {
                    srk.getSysLogger().warning(this.getClass(),
                        "NBCDataProvider.exCriticalRate(): deal.getTotalLoanAmount == 0, return empty string");
                }
			}
            // Ticket 1615--end//
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * @return String
	 */
	/**
	 * exPaymentFreq exPaymentFreq Returns CHECKED or UNCHECKED depending on
	 * parameter passed.
	 *
	 * @param id
	 *            id = Mc.PAY_FREQ_MONTHLY id = Mc.PAY_FREQ_BIWEEKLY id =
	 *            Mc.PAY_FREQ_WEEKLY
	 * @return
	 */
	protected String exPaymentFreq(int id, int accId) {
		String val = "";
		try {
			if ((deal.getPaymentFrequencyId() == id)
					|| (deal.getPaymentFrequencyId() == accId))
				val = CHECKED_CHKBOX;
			else
				val = UNCHECKED_CHKBOX;
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exNewLoanRefinancing Returns 1 as a String if DealPurposeID =
	 * 0,4,5,6,7,12,13, or 17. Else returns 0 as a String.
	 *
	 * @return String
	 */
	protected String exNewLoanRefinancing() {
		int val = -1;
		try {
			val = deal.getDealPurposeId();
			switch (val) {
			case 0:
			case 4:
			case 5:
			case 6:
			case 7:
			case 12:
			case 13:
			case 17:
				return CHECKED_CHKBOX;
			}
		} catch (Exception e) {
		}
		return UNCHECKED_CHKBOX;
	}

	/**
	 * exTrasfer Returns 1 as a String if DealPurposeID = 1. Else returns 0 as a
	 * String.
	 *
	 * @return String
	 */
	protected String exTrasfer() {
		int val = -1;
		try {
			val = deal.getDealPurposeId();
			switch (val) {
			case 1:
				return CHECKED_CHKBOX;
			}
		} catch (Exception e) {
		}
		return UNCHECKED_CHKBOX;
	}

	/**
	 * exFixedDiscount Returns discount for Fixed interest rate type deal.
	 *
	 * @return String
	 */
	protected String exFixedDiscount() {
		String val = "";
		try {
			if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED)
				val = String.valueOf(deal.getDiscount());
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exDownPayment Returns DownPaymentAmount
	 *
	 * @return String
	 */
	protected String exDownPayment() {
		String val = "";
		try {
			val = formatCurrency(deal.getDownPaymentAmount());
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exLifeDisabilityPremium Returns discount for Fixed interest rate type
	 * deal.
	 *
	 * @return String
	 */
	/*
	 * protected String exLifeDisabilityPremium() { String val = null; try { val =
	 * String.valueOf(deal.getLifePremium() + deal.getDisabilityPremium()); }
	 * catch (Exception e) { } return val; }
	 */

	/**
	 * exMIFeesCP Returns 1 if LOC Repayment Type is
	 * INTEREST_PLUS_FIXED_PRINCIPAL. Else returns 0
	 *
	 * @return String
	 */
	protected String exMIFeesCP() {
		String val = "";
		try {
			if (deal.getLocRepaymentTypeId() == Mc.LOC_TYPE_5_20)
				return CHECKED_CHKBOX;
			else
				return UNCHECKED_CHKBOX;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MIFeesCP"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exMIFeesCP1 Returns 1 if LOC Repayment Type is INTEREST_ONLY_VARIABLE
	 * Else returns 0
	 *
	 * @return String
	 */
	protected String exMIFeesCP1() {
		String val = "";
		try {
			if (deal.getLocRepaymentTypeId() == Mc.LOC_TYPE_10_15)
				return CHECKED_CHKBOX;
			else
				return UNCHECKED_CHKBOX;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MIFeesCP1"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exMIPremiumAmount Returns MIPremiumAmount for deals which are not a
	 * swtich.
	 *
	 * @return String
	 */
	protected String exMIPremiumAmount() {
		String val = "";
		try {
			if (deal.getDealPurposeId() != Mc.DEAL_PURPOSE_TRANSFER) {
				val = formatCurrency(deal.getMIPremiumAmount());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MIPremiumAmount"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exMIFeesTax Returns MIPremiumPST
	 *
	 * @return String
	 */
	protected String exMIFeesTax() {
		String val = "";
		try {
			val = formatCurrency(deal.getMIPremiumPST());
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exMIFeesTax Returns MIPremiumPST
	 *
	 * @return String
	 */
	/**
	 * exLoanInsType returns CHECKED or UNCHECKED depending on the mortgage
	 * insurer id passed as parameter.
	 *
	 * @param mortInsId
	 *            int
	 * @return
	 * @version <br>
	 * Date: 12/06/2006<br>
	 * Change: Added check for MI Indicator 
	 */
	protected String exLoanInsType(int mortInsId) {
		String val = UNCHECKED_CHKBOX;
		try {
		  /* Change by NBC/PP Implementation Team - Ticket #1683 - Start */	  	  
	  	  if ( deal.getMIIndicatorId() == Mc.MI_INDICATOR_NOT_REQUIRED   
	  		|| deal.getMIIndicatorId() == Mc.MI_INDICATOR_UW_WAIVED
	  		|| deal.getMIIndicatorId() == Mc.MI_INDICATOR_APPLICATION_FOR_LOAN_ASSESSMENT
	  		|| deal.getMIIndicatorId() == Mc.MI_INDICATOR_PRE_QUALIFICATION) {
	  		//	check uninsured if Deal.MIIndicatorID = 0 or 3 or 4 or 5, regardless of MortgageInsurerId
	  		if (mortInsId == Mc.MI_INSURER_BLANK) {
				val = CHECKED_CHKBOX;
	  		}
	  	  }		  		
	  	  else {
	  		// check insurer ID for CMHC or GE only
	  		if (mortInsId == Mc.MI_INSURER_CMHC || mortInsId == Mc.MI_INSURER_GE) {
    			if (deal.getMortgageInsurerId() == mortInsId) {
    				val = CHECKED_CHKBOX;    		
    			}
	  		}
	  	  }
	  	  /* Change by NBC/PP Implementation Team - Ticket #1683 - End */
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exInterestRateFixed Returns PostedRate for deals which have fixed
	 * interest rate type
	 *
	 * @return String
	 */
	protected String exInterestRateFixed() {
		String val = "";
		try {
			if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
				val = String.valueOf(deal.getPostedRate());
			}
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exNetInterestRate Returns deal.netInterestRate for interestTypeId =
	 * INTEREST_TYPE_FIXED or for interestTypeId != INTEREST_TYPE_FIXED
	 * depending on parameter.
	 *
	 * @param interestRateType
	 *            String interestRateType = "fixed" or interestRateType =
	 *            "variable"
	 * @return double
	 */
	protected Double exNetInterestRate(int interestRateType) {
		Double val = null;
		try {
			if (interestRateType == Mc.INTEREST_TYPE_FIXED) {
				if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
					val = new Double(deal.getNetInterestRate());
				}
			} else if (interestRateType != Mc.INTEREST_TYPE_FIXED) {
				if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED && deal.getTeaserTerm() != 0 && deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE) {
					val = new Double(deal.getNetInterestRate());
				}
			}
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exPromPeriod Returns deal.actualPaymentTerm for interestTypeId =
	 * INTEREST_TYPE_FIXED
	 *
	 * @return String
	 */
	protected String exPromPeriod() {
		String val = "";
		try {
			if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)
				val = Integer.toString(deal.getActualPaymentTerm());
		} catch (Exception e) {
		}
		return val;
	}

	/**
     * exRateLoan Returns 1 or 0 as a String depending on Payment term ID
     *
     * @param which String which = "open" or which = "closed"
     * @param paymentTermTypeId Type of the payment term which indicates if it is open or closed term
     * @return String
     */
    protected String exRateLoan(String which, int paymentTermTypeId) {
        String val = UNCHECKED_CHKBOX;
        if (which == "open") {
            if (paymentTermTypeId == INT_ONE_CHECK) {
                val = CHECKED_CHKBOX;
            }
        } else if (which == "closed") {
            if (paymentTermTypeId == INT_ZERO_CHECK) {
                val = CHECKED_CHKBOX;
            }
        }
        return val;
    }

	/**
	 * exPrepayment Returns 1 or 0 depending on which payment term type is
	 * selected
	 *
	 * @param which
	 *            String which = "open" or which = "closed"
	 * @return String
	 */
        protected String exPrepayment(String which) {
                String val = "";
                try {
// fixes for SCR#1494 - start
                        int ptid = deal.getPaymentTermId();
                        int paymentTermType = getPaymentTermType(ptid);
                        if (paymentTermType != PAYMENT_TERM_OPEN && paymentTermType != PAYMENT_TERM_CLOSED) {
                                return UNCHECKED_CHKBOX;
                        }

			if (which == null) return UNCHECKED_CHKBOX;

			if (which.equals("open")) {
                                return paymentTermType == PAYMENT_TERM_OPEN? CHECKED_CHKBOX: UNCHECKED_CHKBOX;
			}else if (which.equals("closed")) {
                                return paymentTermType == PAYMENT_TERM_CLOSED? CHECKED_CHKBOX: UNCHECKED_CHKBOX;
                        }
                        // fixes for SCR#1494 - end
                } catch (Exception e) {
                }
                return val;
        }


	/**
	 * exNetLoanAmount Returns netLoanAmount as a string
	 *
	 * @return String
	 */
	protected String exNetLoanAmount() {
		return formatCurrency(deal.getNetLoanAmount());
	}

	/**
	 * exDateDisburse Returns a Disbursement Date if EstimatedClosingDate is not
	 * null
	 *
	 * @return String
	 */
	// fixes for SCR#1494,1463 - start
	protected String exDateDisburse() {
//		String val = "";
		String format_date = "";
		try {
			if (deal.getEstimatedClosingDate() != null
					&& deal.getProgressAdvance().equalsIgnoreCase(CHECK_NO)) {
				format_date = formatDateForDisclosure(deal.
					getEstimatedClosingDate());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting DateDisburse"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return format_date;
	}
	// fixes for SCR#1494,1463 - end


	/**
	 * exFirstPaymentDate Returns a First Payment Date if FirstPaymentDate is
	 * not null
	 *
	 * @return String
	 */

	protected String exFirstPaymentDate() {
		String val = "";
		try {
			if (deal.getFirstPaymentDate() != null)
				val = formatDate(deal.getFirstPaymentDate());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting FirstPaymentDate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exFirstPaymentDate Returns a First Payment Date in the Year/Month/Date
	 * format if FirstPaymentDate is not null
	 *
	 * @return String
	 */
	// fixes for SCR#1494,1463 - start
	protected String exFirstPmtDateFrmttd() {
//		String val = "";
		String format_date = "";
		try {
			if (deal.getFirstPaymentDate() != null) {
				format_date = formatDateForDisclosure(deal.getFirstPaymentDate());
//				val = getYear(format_date) + DATE_SEPARATOR
//						+ getMonth(format_date) + DATE_SEPARATOR
//						+ getDay(format_date);
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting FirstPmtDateFrmttd"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return format_date;
	}
	// fixes for SCR#1494,1463 - end


	/**
	 * exPaymentSchedule Returns payment schedule as a string If Payment
	 * Frequency is monthly, it returns calender date else if returns the week
	 * day name.
	 *
	 * @return String
	 */
	protected String exPaymentSchedule() {
		HashMap days = new HashMap(7);
		days.put("Monday", "Lundi");
		days.put("Tuesday", "Mardi");
		days.put("Wednesday", "Mercredi");
		days.put("Thursday", "Jeudi");
		days.put("Friday", "Vendredi");
		days.put("Saturday", "Samedi");
		days.put("Sunday", "Dimanche");

		String val = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		SimpleDateFormat sdf1 = new SimpleDateFormat("EEEEEEEEE");
		try {
			if (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_MONTHLY) {
				val = sdf.format(deal.getFirstPaymentDate());
			} else {
				if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH)
					val = sdf1.format(deal.getFirstPaymentDate());
				else
					val = (String) days.get(sdf1.format(deal
							.getFirstPaymentDate()));
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PaymentSchedule"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exOtherSpec Returns a string depending on language
	 *
	 * @return String
	 */
	protected String exOtherSpec() {
		String val = "";
		try {
			if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH)
				val = OTHER_SPEC_ENG;
			else if (lang == Mc.LANGUAGE_PREFERENCE_FRENCH)
				val = OTHER_SPEC_FRNCH;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting OtherSpec"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exTotPayAmt Returns PAndIPaymentAmount + EscrowPaymentAmount (where
	 * EscrowTypeID=0)
	 *
	 * @return
	 * Date: 11/08/2006 <br>
	 * Change: <br>
	 * 	Added check for Escrow type = admin fee
	 */
	protected String exTotPayAmt() {
		String val = "";
		double escrowAmt = 0d;
		EscrowPayment epObj;
		try {
            // Start of 1615 bug fix
			Collection ep = deal.getEscrowPayments();
            if (ep != null) {
			Iterator it = ep.iterator();
			while (it.hasNext()) {
				epObj = (EscrowPayment) it.next();
                    /*
                     * if (deal.getDealId() == epObj.getDealId()) { epId = epObj.getEscrowPaymentId();
                     *  } epObj = new EscrowPayment(srk, CalcMonitor.getMonitor(srk), epId, deal.getCopyId());
                     */

                    if (epObj.getEscrowTypeId() == Mc.ESCROWTYPE_PROPERTY_TAX
                        || epObj.getEscrowTypeId() == Mc.ESCROWTYPE_ADMIN_FEE) {
                        escrowAmt += epObj.getEscrowPaymentAmount();
                        //break;
				}
			}
			val = formatCurrency(deal.getPandiPaymentAmount() + escrowAmt);
            }
            //Start of 1615 bug fix end
		}
		catch (Exception e) {
		}
		return val;
	}

	/**
	 * exFrmttdCurrDate It returns the current date in the "Year/Month/Day"
	 * format
	 *
	 * @return
	 */
        // fixes for SCR#1494,1463 - start
        protected String exFrmttdCurrDate() {
                String val = "";
                // SCR 1472
                try {
                        val = formatDateForDisclosure(new Date());
                }
                catch (Exception e) {
                        String msg =
                                "NBCDataProvider failed while extracting FrmttdCurrDate"
                                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
                        srk.getSysLogger().warning(this.getClass(), msg);
                }
                return val;
        }

        // fixes for SCR#1494,1463 - end


	/**
	 * exCTPHAddress Returns a CTPH Address string depending on language
	 *
	 * @return String
	 */

	protected String exCTPHAddress() {
		String val = "";
		try {
			if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH)
				val = "Mortgage Processing Centre - M.P.C., 500 place d'Armes, Montreal, QC";
			else if (lang == Mc.LANGUAGE_PREFERENCE_FRENCH)
				val = "Centre de Traitement des Pr�ts Hypoth�caires - C.T.P.H., 500 place d'Armes, Montr�al, QC";
		} catch (Exception e) {
		}
		return val;
	}

	/**
	 * exAlwChkd Returns a checkbox that's always checked
	 *
	 * @return String CHECKED_CHKBOX
	 */
	protected String exAlwChkd() {
		return CHECKED_CHKBOX;
	}

	/**
	 * exAlwUnChkd Returns a checkbox that's always unchecked
	 *
	 * @return String UNCHECKED_CHKBOX
	 */
	protected String exAlwUnChkd() {
		return UNCHECKED_CHKBOX;
	}

	/**
	 * exPropTaxInstall Returns the EscrowPaymentAmount for Municipal taxes
	 *
	 * @return
	 */
	protected String exPropTaxInstall() {
		String val = "";
		double escrowAmt = 0d;
		EscrowPayment epObj = null;
		try {
			Collection ep = new ArrayList();
			ep = deal.getEscrowPayments();
			Iterator it = ep.iterator();
			while (it.hasNext()) {
				epObj = (EscrowPayment) it.next();
				if (epObj.getEscrowTypeId() == INT_ZERO_CHECK) {
					escrowAmt = epObj.getEscrowPaymentAmount();
					val = formatCurrency(escrowAmt);
					break;
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PropTaxInstall"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exInterimInterestAdjustmentDate Returns InterimInterestAdjustmentDate +
	 * InterestRateChangeFrequency
	 *
	 * @return String
	 */
	protected String exInterimInterestAdjustmentDate() {
		return formatDate(deal.getInterimInterestAdjustmentDate());
	}

	/**
	 * exInterimIntAdjDateFrmttd Method description
	 *
	 * @return
	 */
	// fixes for SCR#1494,1463 - start
	protected String exInterimIntAdjDateFrmttd() {
//		String val = "";
		String format_date = "";
		try {
			format_date = formatDateForDisclosure(deal.
												  getInterimInterestAdjustmentDate());
		}
		catch (Exception e) {
			String msg =
				"NBCDataProvider failed while extracting InterimIntAdjDate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return format_date;
	}
	// fixes for SCR#1494,1463 - end
	
	/**
	 * exMaturityDate <br>
	 * The method checks if the maturity date is not null. If yes, returns
	 * formatted maturity date of the deal else returns formatted date of
	 * (Interim Interest Adjustment Date + Pricing Registration Term)
	 * 
	 * @return String : containing formatted date which contains either maturity
	 *         date or (Interim Interest Adjustment date + Pricing registeration
	 *         term)
	 */
	protected String exMaturityDate() {
		String date = "";
		Calendar c = Calendar.getInstance();
		try {
			if (deal.getMaturityDate() == null) {
				c.setTime(deal.getInterimInterestAdjustmentDate());
				c.add(Calendar.MONTH, (int) pp.getPricingRegistrationTerm());
				date = formatDate(c.getTime());
			} else {
				date = formatDate(deal.getMaturityDate());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MaturityDate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return date;
	}
	
	/**
	 * exMaturityDate Returns the InterimInterestAdjustmentDate +
	 * PricingRegistrationTerm if PricingRegistrationTerm is not null
	 *
	 * @return
	 */
	// fixes for SCR#1494,1463 - start
	protected String exMaturityDateFrmttd() {
		String date = "";
//		String val = "";
		Calendar c = Calendar.getInstance();
		try {
			if (pp.getPricingRegistrationTerm() != 0d) {
				c.setTime(deal.getInterimInterestAdjustmentDate());
				c.add(Calendar.MONTH, (int) pp.getPricingRegistrationTerm());
				date = formatDateForDisclosure(c.getTime());
//				val = getYear(date) + DATE_SEPARATOR + getMonth(date)
//						+ DATE_SEPARATOR + getDay(date);
			} else {
				date = formatDateForDisclosure(deal.getMaturityDate());
//				val = getYear(date) + DATE_SEPARATOR + getMonth(date)
//						+ DATE_SEPARATOR + getDay(date);
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MaturityDate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}		
		return date;
	}
	// fixes for SCR#1494,1463 - end

	/**
	 * exDiffMortRateSpread Returns NetInterestRate if Interest Type is Variable &
	 * ProductType is Mortgage
	 *
	 * @return String
	 */
	protected String exDiffMortRateSpread() {
		String val = "";

		try {
			if (((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE) || (deal
					.getProductTypeId() == INT_ZERO_CHECK))
					&& (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)) {
				val = String.valueOf(deal.getNetInterestRate());

			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting DiffMortRateSpread"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exDiffMortRateSpread Returns NetInterestRate if Interest Type is Variable &
	 * ProductType is Mortgage
	 *
	 * @return String
	 */
	protected String exCalcField() {
		String val = null;

		try {
			if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC)
					&& (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)) {
				val = String.valueOf(deal.getNetInterestRate());
				if ( (val == null) || (val.trim().length()==0)) {
				  return "0.00";
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting CalcField"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exIntRateSpread Returns PLUS if product type is MORTGAGE and Interest
	 * Type is not fixed, else returns MINUS
	 *
	 * @return String
	 */
	protected String exIntRateSpread() {
		String val = "";
		double calcVal = 0d;

		try {
			if (((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE || deal
					.getProductTypeId() == INT_ZERO_CHECK))
					&& deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
				calcVal = deal.getPrimeBaseAdj() + deal.getPremium()
						- deal.getBuydownRate() - deal.getDiscount();
				if (calcVal > 0d) {
					val = PLUS;
				} else {
					val = MINUS;
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting IntRateSpread"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exSpreadGT0 Returns PLUS if getPrimeBaseAdj > 0 else returns MINUS
	 *
	 * @return String
	 */
	protected String exSpreadGT0() {
		String val = "";
		//Added codefix for QA Testing
		double valDouble = 0d;
		valDouble = deal.getPrimeBaseAdj() + deal.getPremium() - deal.getBuydownRate() - deal.getDiscount();
		try {
			if (valDouble > 0d) {
				val = PLUS;
			}
			else {
				val = MINUS;
			}
		}
		catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting SpreadGT0"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
     * exOwnerName Returns the owner Name for the ServiceRequestContact
     *
     * @return String PropertyOwnerName
     */
    protected String exOwnerName() {
        String val = "";
        try {
            Collection serviceReqColl = serviceReqCntct.findByRequest(reqPK);
            if (serviceReqColl != null) {
                Iterator it = serviceReqColl.iterator();
                while (it.hasNext()) {
                    ServiceRequestContact src = (ServiceRequestContact) it.next();
                    val = src.getPropertyOwnerName();
                }
            }
		}
		catch (Exception e) {
		  String msg ="NBCDataProvider failed while extracting PropertyOwnerName" +
		  (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

    /**
	 * exCntactName Returns the Contact Name for the Appraisal order
	 * 
	 * @return String ContactName concatinating the First Name and LastName from
	 *         the ServiceRequestContact for the Borrower
	 */
	protected String exCntactName() {
		String val = "";
		String fname = "";
		String lname = "";
		try {
			Collection serviceReqColl = serviceReqCntct.findByRequest(reqPK);
			if (serviceReqColl != null && serviceReqColl.size() != 0) {
				Iterator it = serviceReqColl.iterator();
				while (it.hasNext()) {
					// Added check for the Name not being null - Bug# 1478
					// -start
					ServiceRequestContact src = (ServiceRequestContact) it
							.next();
					if (src.getBorrowerId() == 0) {
						if (src.getFirstName() != null) {
							fname = src.getFirstName() + format.space();
						}
						if (src.getLastName() != null) {
							lname = src.getLastName();
						}
						val = fname + lname;
						// Added check for the Name not being null - Bug# 1478
						// -end
					}
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting Contact Name"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		if (val == null && val.trim().length() == 0) {
			val = "";
		}
		return val;
	}

    /**
	 * exCntHomePhNo Returns the Contact Person's Home Phone No. for the
	 * ServiceRequestContact
	 * 
	 * @return String ContactHomePhoneNum
	 */
    protected String exCntHomePhNo() {
        String val = "";
        try {

            Collection serviceReqColl = serviceReqCntct.findByRequest(reqPK);

            if (serviceReqColl != null && serviceReqColl.size() != 0) {
                Iterator it = serviceReqColl.iterator();
                while (it.hasNext()) {
                    ServiceRequestContact src = (ServiceRequestContact) it.next();
                    if (src.getBorrowerId() == 0) {
                        if (src.getHomeAreaCode() != null && src.getHomePhoneNumber() != null) {
                            val = StringUtil.getAsFormattedPhoneNumber(src.getHomeAreaCode().concat(
                                src.getHomePhoneNumber()), "");
                        }
                    }
                }
            }
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting Contact Home Phone No."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

    /**
     * exCntWorkPhNo Returns the Contact Person's Work Phone No. for the ServiceRequestContact
     *
     * @return String ContactWorkPhoneNum
     */

    protected String exCntWorkPhNo() {
        String val = "";
        try {
            Collection serviceReqColl = serviceReqCntct.findByRequest(reqPK);
            if (serviceReqColl != null) {
                Iterator it = serviceReqColl.iterator();
                while (it.hasNext()) {
                    ServiceRequestContact src = (ServiceRequestContact) it.next();
                    if (src.getBorrowerId() == 0) {
                        if (src.getWorkAreaCode() != null && src.getWorkPhoneNumber() != null) {
                            val = StringUtil.getAsFormattedPhoneNumber(src.getWorkAreaCode().concat(
                                src.getWorkPhoneNumber()), src.getWorkExtension());
                        }

                    }
                }
            }
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting Contact Work Phone No."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
            e.printStackTrace();

        }
        return val;
    }

    /**
     * exCntCellPhNo Returns the Contact Person's Cell Phone No. for the ServiceRequestContact
     *
     * @return String ContactWorkPhoneNum
     */

    protected String exCntCellPhNo() {
        String val = "";
        try {
            Collection serviceReqColl = serviceReqCntct.findByRequest(reqPK);
            if (serviceReqColl != null) {
                Iterator it = serviceReqColl.iterator();
                while (it.hasNext()) {
                    ServiceRequestContact src = (ServiceRequestContact) it.next();
                    if (src.getBorrowerId() == 0) {
                        if (src.getCellAreaCode() != null && src.getCellPhoneNumber() != null) {
                            val = StringUtil.getAsFormattedPhoneNumber(src.getCellAreaCode().concat(
                                src.getCellPhoneNumber()), "");
                        }
                    }
                }
            }
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting Contact Cell Phone No."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

	 /**
		 * exSpInstrn Returns the Comment for appraiser (Spl. Instructions) for
		 * the ServiceRequestContact
		 *
		 * @return String exSpInstrn
		 */

	  protected String exSpInstrn() {
		String val = "";
		try {
		  if (serviceReq !=  null) {
			val = serviceReq.getSpecialInstructions();
		  }
		} catch (Exception e) {
		  	String msg = "NBCDataProvider failed while extracting Special Instructions" + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		  	srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	  }

	/**
	 * exInterestRateActDate Returns Current Date as a String if Interest Type
	 * is not fixed. and Product Type is MORTGAGE.
	 *
	 * @return String
	 */
	protected String exInterestRateActDate() {
		String val = "";

		try {
			if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE || deal
					.getProductTypeId() == INT_ZERO_CHECK)
					&& deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
				val = formatDate(new Date());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting InterestRateActDate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exConstAdv Returns CHECKED_CHKBOX if Deal.ProgressAdvance = Y else
	 * returns UNCHECKED_CHKBOX.
	 *
	 * @return String
	 */
	protected String exConstAdv() {
		String val = "";

		try {
			if (deal.getProgressAdvance() == CHECK_YES)
				val = CHECKED_CHKBOX;
			else
				val = UNCHECKED_CHKBOX;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting ConstAdv"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exMaxDisclosableAmount Returns Property.ActualAppraiseValue if it is
	 * greater than 0 else returns Property.EstimatedAppraisalValue
	 *
	 * @param pProperty
	 *            Property
	 * @return String
	 * 
	 * @version <br>
	 * Date: 12/06/2006 <br>
	 * Change: Return total loan amount for the document 15021, currently only doc that uses this method
	 */
	protected String exMaxDisclosableAmount() {
		String val = "";

		try {
		  /* Change by NBC/PP Implementation Team - Ticket #1794 - Start */
			/*if (pProperty.getActualAppraisalValue() > 0d) {
				val = formatCurrency(pProperty.getActualAppraisalValue());
			} else {
				val = formatCurrency(pProperty.getEstimatedAppraisalValue());
			}
			*/
		  
		  	// Should return total loan amount, and not appraised value
		  	val = formatCurrency(deal.getTotalLoanAmount());		  

		  /* Change by NBC/PP Implementation Team - Ticket #1794 - End */
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MaxDisclosableAmount"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * This method will return false if the productTypeID == 0 and the interestTypeID != 0
	 * It is used in the NBCSolicitorPackage XSLs to determine whether fields for section 18
	 * of document 15021 should be displayed.
	 *
	 * Fix for ticket 1472
	 */
	protected String exShowVIR() {
		String returnString = "false";
		if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE && deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
			returnString = "true";
		}
		return returnString;
	}

	/**
	 * exInterestRate Returns Variable Interest Rate if Interest Type is not
	 * fixed and Product type is mortgage.
	 *
	 * @param which
	 *            int which = 0 (No condition) which = 1 (Interest type is
	 *            fixed.) which = 2 (Interest type is fixed and Product Type is
	 *            mortgage)
	 * @return String
	 */
	protected String exProdInterestRate(int which) {
		String val = "";

		try {
			switch (which) {
			case 0:

				val = String.valueOf(deal.getPrimeIndexRate());
				break;
			case 1:
				  // Changed conditions for checking Interest type = SLOC
					if ( (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)
						&&
						(deal.getProductTypeId() ==
						 Mc.PRODUCT_TYPE_SECURED_LOC)) {
					val = String.valueOf(deal.getPrimeIndexRate());

				}
				break;
			case 2:
			  	// Show Variable Interest Rate, if product type = mortgage NOT 0
				if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE
					//|| deal.getProductTypeId() == PROD_TYPE_ID_ZERO
						&& deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
					  	//Code fix for QA Testing Bugs - start
					val = String.valueOf(deal.getPrimeIndexRate());
					    //Code fix for QA Testing Bugs - end
				}
				break;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting ProdInterestRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		//Code fix for QA Testing Bugs - start
		if (val != null && val.trim().length()==0) {
		  //Code fix for QA Testing Bugs - end
			srk.getSysLogger().debug(this.getClass(),
									 "NBCDataProvider.exProdInterestRate: val is 0, set val to null.");
			val = null;
		}
		return val;
	}

	/**
	 * exPrimeBaseAdj Returns PrimeBaseAdj
	 *
	 * @param which
	 *            String which = "mortgage" or which = "other"
	 * @return String
	 */
	protected String exPrimeBaseAdj() {
		String val = "";
		double calcVal = 0d;
		try {
			if (((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE) || (deal
					.getProductTypeId() == PROD_TYPE_ID_ZERO))
					&& (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)) {
				calcVal = deal.getPrimeBaseAdj() + deal.getPremium()
						- deal.getBuydownRate() - deal.getDiscount();

				if (calcVal < 0) {
					calcVal = Math.abs(calcVal);
				}
				val = String.valueOf(calcVal);
			}

		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PrimeBaseAdj"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		if (val != null && val.equals("0")) {
			srk
					.getSysLogger()
					.debug(this.getClass(),
							"NBCDataProvider.exPrimeBaseAdj: val is 0, set val to null.");
			val = null;
		}
		return val;
	}

	/**
	 * exVarRateSpread Returns PrimeBaseAdj + Premium - Discount only if
	 * InterestType <> 0
	 *
	 * @return String
	 */
	//modified as part of QA Testing
	protected String exVarRateSpread() {
        String val = "";
        try {
            if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
                val = String.valueOf(Math.abs(deal.getPrimeBaseAdj() + deal.getPremium() - deal.getBuydownRate() - deal.getDiscount()));
            }
        } catch (Exception e) {
            String msg = "NBCDataProvider failed while extracting VarRateSpread"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

	/**
	 * exVarRateToPrime Returns variable rate to Prime at current date if teh
	 * product Type is SELOC
	 *
	 * @return String
	 */
	protected String exVarRateToPrime() {
		String val = null;
		try {

			if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC) {
				val = String.valueOf(deal.getPrimeBaseAdj() + deal.getPremium()
						- deal.getBuydownRate() - deal.getDiscount());

			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting VarRateToPrime"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		if( (val == null) || (val.equals("")))
			return "0.00";
		else
		return val;
	}

	/**
	 * exSolInstrn Returns SolicitorSpecialInstructions
	 *
	 * @return String
	 */
	protected String exSolInstrn() {
		return deal.getSolicitorSpecialInstructions();
	}

	/**
	 * exSourceApplicationId Returns SourceApplicationId
	 *
	 * @return String
	 */
	protected String exSourceApplicationId() {
		return deal.getSourceApplicationId();
	}

	/**
	 * exRefPayment Returns PAndIAmount if DealPurposeID is
	 * DEAL_PURPOSE_REFI_EXISTING_CLIENT.
	 *
	 * @return String
	 */
	protected String exRefPayment() {
		String val = "";

		try {
			if ((deal.getDealPurposeId() == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
					|| (deal.getDealPurposeId() == Mc.DEAL_PURPOSE_ETO_EXTERNAL)
					|| (deal.getDealPurposeId() == Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT)) {
				val = String.valueOf(deal.getPandiPaymentAmount());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting RefPayment"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exMonthlyPayment Returns PAndIAmount if Interest Type is fixed.
	 *
	 * @return String
	 */
	protected String exMonthlyPayment() {
		String val = "";

		try {
			if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
				val = formatCurrency(deal.getPandiPaymentAmount());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MonthlyPayment"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	// NOT IN THE REVISED DATA-MAPPING LIST - START
	/**
	 * exOpenRateLoan Returns 1 if PaymentTermType.PTType is OPEN else returns
	 * 0.
	 *
	 * @return String
	 */
	/*
	 * protected String exOpenRateLoan() { String val = ""; try { if (true) val =
	 * CHECKED_CHKBOX; else val = UNCHECKED_CHKBOX; } catch (Exception e) { }
	 * return val; } /** exClosedRateLoan Returns 1 if PaymentTermType.PTType is
	 * CLOSED else returns 0. @return String
	 */
	/*
	 * protected String exClosedRateLoan() { String val = ""; try { if (true)
	 * val = "1"; else val = "0"; } catch (Exception e) { } return val; }
	 */
	// NOT IN THE REVISED DATA-MAPPING LIST - END
	/**
	 * exPropertyUnits Returns 1 or 0 depending on parameter which and value of
	 * NumberOfUnits.
	 *
	 * @param which
	 *            String which = 0 (numberofunits <=4)or which = 1
	 *            (numberofunits >4)
	 * @return String
	 */
	protected String exPropertyUnits(int which, Property pProperty) {
		String val = "";

		try {
			if (which == 0)
				val = pProperty.getNumberOfUnits() <= 4 ? CHECKED_CHKBOX
						: UNCHECKED_CHKBOX;
			else if (which == 1)
				val = pProperty.getNumberOfUnits() > 4 ? CHECKED_CHKBOX
						: UNCHECKED_CHKBOX;
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PropertyUnits"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exIntRateAutoRenew Returns a checked checkbox if
	 * InterestRateChangeFrequency > 0 else returns an unchecked checkbox
	 *
	 * @return String <Checked or Unchecked checkbox>
	 */
	protected String exIntRateAutoRenew() {
		String val = "";
		try {
			if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE || deal
					.getProductTypeId() == INT_ZERO_CHECK)
					&& pp.getInterestRateChangeFrequency() > 0) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting IntRateAutoRenew"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exIntRateNotAutoRenew Returns a checked checkbox if
	 * InterestRateChangeFrequency = 0 or null else returns an unchecked
	 * checkbox
	 *
	 * @return String <Checked or Unchecked checkbox>
	 */
	protected String exIntRateNotAutoRenew() {
		String val = "";
		try {
			if ((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE || deal
					.getProductTypeId() == INT_ZERO_CHECK)
					&& (pp.getInterestRateChangeFrequency() == 0d)) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting IntRateNotAutoRenew"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	protected String exCappedRate() {
		String val = "";
		PricingRateInventory prInven;
		try {
			prInven = deal.getPricingRateInventory();
			if (deal.getInterestTypeId() != 0) {
				val = String.valueOf(prInven.getInternalRatePercentage());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting CappedRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exBudgetOpt Returns a checked checkbox if the MtgProduct ID is 28
	 *
	 * @return String <Checked or Unchecked checkbox>
	 */
	protected String exBudgetOpt() {
		String val = "";
		try {
			if (deal.getMtgProdId() == MTGPRODID_BUDGET) {
				val = CHECKED_CHKBOX;
			} else {
				val = UNCHECKED_CHKBOX;
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting BudgetOpt"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exEmployeeProgPayment Returns PAndIAmount if Interest Type is not fixed.
	 *
	 * @return String
	 */
	protected String exEmployeeProgPayment() {
		String val = "";
		//Added condtions - Producttype = Mortgage or Zero
		try {

		  if((deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE || deal
			  .getProductTypeId() == INT_ZERO_CHECK)
			  	&& (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED)) {
				val = formatCurrency(deal.getPandiPaymentAmount());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting EmployeeProgPayment"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPandIAmt Returns PAndIAmount
	 *
	 * @return String
	 * @version <br>
	 * Date: 12/06/2006 <br>
	 * Change: Use formatCurrency on PandIPaymentAmount, ticket #1689
	 */
	protected String exPandIAmt() {
		String val = "";

		try {
		  /* Ticket #1689 - Start */
		  	// format as Currency, which will handle the display of the dollar sign
			val = formatCurrency(deal.getPandiPaymentAmount());
		  /* Ticket #1689 - End */	
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting PandIAmt"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exEscrowPaymentAmount Returns EscrowPaymentAmount
	 *
	 * @return double
	 */
	protected double exEscrowPaymentAmount(int id) {
		double escrowAmt = 0d;
		EscrowPayment epObj;
		try {

			Collection ep = deal.getEscrowPayments();

			Iterator it = ep.iterator();
			while (it.hasNext()) {
				epObj = (EscrowPayment) it.next();

				if (epObj.getEscrowTypeId() == id) {
					escrowAmt = epObj.getEscrowPaymentAmount();
					break;
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting EscrowPaymentAmount"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return escrowAmt;
	}

	/**
	 * exAdministrationFees Returns administration fees
	 *
	 * @return double
	 */
	protected double exAdministrationFees(int id) {
		double escrowAmt = exEscrowPaymentAmount(id);
		double paymentFrequencyMultiplier = pCalcPayFreqFactor(deal.getPaymentFrequencyId());
		double numberOfPayments = deal.getActualPaymentTerm() * paymentFrequencyMultiplier;

		return escrowAmt * numberOfPayments;
	}


	/**
	 * exDocTrackText Method description
	 *
	 * @return
	 */
	protected String[] exDocTrackText() {
		ArrayList val=new ArrayList();
		DocumentTracking dtObj = null;
		DocumentTracking.DocTrackingVerbiage dtvObj = null;
		Condition cnDtn = null;
		try {
			Collection dtCol = deal.getDocumentTracks();
			Iterator it = dtCol.iterator();
			while (it.hasNext()) {
				dtObj = (DocumentTracking) it.next();
				dtvObj = dtObj.new DocTrackingVerbiage(srk, dtObj
						.getDocumentTrackingId(), dtObj.getCopyId(), lang,
						dtObj.getDocumentLabelByLanguage(lang), dtObj
								.getDocumentTextByLanguage(lang));
				cnDtn = new Condition(srk, dtObj.getConditionId());

				if (((cnDtn.getConditionTypeId() == Mc.CONDITION_TYPE_COMMITMENT_ONLY) || (cnDtn
						.getConditionTypeId() == Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING))
						&& ((dtObj.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED)
								|| (dtObj.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM) || (dtObj
								.getDocumentTrackingSourceId() == Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED))
						&& ((dtObj.getDocumentResponsibilityRoleId() == DOC_RESPON_ROLE_ID_50)
								|| (dtObj.getDocumentResponsibilityRoleId() == DOC_RESPON_ROLE_ID_51)
								|| (dtObj.getDocumentResponsibilityRoleId() == DOC_RESPON_ROLE_ID_0) || (dtObj
								.getDocumentResponsibilityRoleId() == DOC_RESPON_ROLE_ID_90))
						&& ((dtObj.getDocumentStatusId() == Mc.DOC_STATUS_REQUESTED)
								|| (dtObj.getDocumentStatusId() == Mc.DOC_STATUS_RECEIVED)
								|| (dtObj.getDocumentStatusId() == Mc.DOC_STATUS_INSUFFICIENT)
								|| (dtObj.getDocumentStatusId() == Mc.DOC_STATUS_CAUTION) || (dtObj
								.getDocumentStatusId() == Mc.DOC_STATUS_WAIVE_REQUESTED))) {
					val.add(dtvObj.getDocumentText());
				}
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting DocTrackText"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		String[] Text=new String[21];
		for (int i=0;i<val.size();i++){

		 Text[i]=val.get(i).toString();
		}
		for(int cntr = val.size();cntr<21;cntr++)
		  Text[cntr]="";
		return Text;
	}

	/**
	 * exLoanAmount Returns the sum of MIPremiumAmount (If deal purpose is
	 * transfer) & NetLoanAmount
	 *
	 * @return
	 */
	protected String exLoanAmount() {
		String val = "";
		try {
			if (deal.getDealPurposeId() != Mc.DEAL_PURPOSE_TRANSFER) {
				val = formatCurrency(deal.getMIPremiumAmount()
						+ deal.getNetLoanAmount());
			} else {
				val = formatCurrency(deal.getNetLoanAmount());
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting LoanAmount"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exIntOverTerm Extracts the Interest Over Term as calculated by the Calc
	 * Engine
	 *
	 * @return
	 * Date: 10/11/2006 <br>
	 * Change: <br>
	 * 	Added Interim interest amount to interest cost
	 */
	protected String exIntOverTerm() {
		String val = "";
		try {
		  	double interestCost = 0d;
			if (deal.getInterestOverTerm() != 0d) {
			  interestCost += deal.getInterestOverTerm();
			}
			if (deal.getInterimInterestAmount() != 0d) {
			  interestCost += deal.getInterimInterestAmount();
			}
			val = formatCurrency(interestCost);
		}
		catch (Exception e) {
			String msg =
				"NBCDataProvider failed while extracting exIntOverTerm"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exBalRemEOT Extracts the Balance Remaining at End of term as calculated
	 * by the Calc Engine
	 *
	 * @return
	 */
	protected String exBalRemEOT() {
		String val = "";
		try {
			if (deal.getBalanceRemainingAtEndOfTerm() != 0d)
				val = String.valueOf(deal.getBalanceRemainingAtEndOfTerm());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting exBalRemEOT"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exTeaserCalc Extracts the Teaser Interest Rate as calculated by the Calc
	 * Engine
	 *
	 * @return
	 */
	protected String exTeaserCalc() {
		String val = "";
		try {
			if (deal.getTeaserRateInterestSaving() != 0d)
				val = formatCurrency(deal.getTeaserRateInterestSaving());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting exTeaserCalc"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * exPerFlexLine Returns CriticalRate if deal.MTGProdID = 15 0r 17 else
	 * returns blank
	 *
	 * @return String
	 */
	protected String exTotalCostBorrowing() {
		String val = "";
		try {

		  	// fix for #1615 - Start
		  	double totalCostOfBorrowing = pCalcNBCTotalCostOfBorrowingNBC();
			val = formatCurrency(totalCostOfBorrowing);
//			 fix for #1615 - End


	} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting TotalCostBorrowing"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * pCalcNBCTotalCostOfBorrowingNBC
	 *
	 * @return double - total cost of borrowing, as specified by NBC
	 *
	 */
	private double pCalcNBCTotalCostOfBorrowingNBC() throws FinderException, RemoteException
	{
	  double totalCostOfBorrowing = 0d;
	  //#DG600 not used 
	  /*double totalFees = 0d;
	  double actualPaymentTerm = deal.getActualPaymentTerm();
	  int paymentFrequencyId = deal.getPaymentFrequencyId();
	  EscrowPayment dealEscrowPayment = new EscrowPayment(srk);
	  double paymentFrequencyMultiplier = pCalcPayFreqFactor(paymentFrequencyId);
	  double numberOfPayments = actualPaymentTerm * paymentFrequencyMultiplier;
	  // double escrowFees = dealEscrowPayment.findByDealIdAndType(deal.getDealId(), deal.getCopyId(), Mc.ESCROWTYPE_ADMIN_FEE);
	  //totalFees = escrowFees * numberOfPayments;
		Collection<EscrowPayment> eps = dealEscrowPayment.findByDealAndType((DealPK) deal.getPk(), Mc.ESCROWTYPE_ADMIN_FEE);
		for (EscrowPayment paym: eps) 
	  	totalFees += paym.getEscrowPaymentAmount() * numberOfPayments;*/

	  int[] pAPPRAISAL_FEES =
	  { Mc.FEE_TYPE_APPRAISAL_FEE, Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL };

	  totalCostOfBorrowing = deal.getInterestOverTerm() + + deal.getInterimInterestAmount() +
	  exSelectedFeesTotalAmt(pAPPRAISAL_FEES) + exAdministrationFees(Mc.ESCROWTYPE_ADMIN_FEE);

	  return totalCostOfBorrowing;
	}

	/**
	 * pCalcAveragePrincipleOutstanding
	 *
	 * @return double - total cost of borrowing, as specified by NBC
	 *
	 */
	private double pCalcAveragePrincipleOutstanding() throws FinderException, RemoteException
	{
	  double averagePrincipleOutstanding = 1d;

	  // total interest cost
	  int paymentFrequencyId = deal.getPaymentFrequencyId();
	  double paymentFrequencyMultiplier = pCalcPayFreqFactor(paymentFrequencyId);
	  double actualPaymentTerm = deal.getActualPaymentTerm();
	  double totalPaymentsInTerm = actualPaymentTerm * paymentFrequencyMultiplier;
	  double totalPayments = deal.getPandiPaymentAmount() * totalPaymentsInTerm;
	  double totalInterestCost = totalPayments - (deal.getTotalLoanAmount() - deal.getBalanceRemainingAtEndOfTerm());

	  // interest factor
	  int interestCompoundingId = (new MtgProd(srk, null, deal.getMtgProdId())).getInterestCompoundingId();
	  int compoundingFrequency = 2;	  // default if InterestCompoundId == 0
	  if (interestCompoundingId == 1) {
		compoundingFrequency = 12;
	  }
	  double paymentFrequencyFactor = pCalcPayFreqFactorWithDays(paymentFrequencyId, 365);
	  double interestFactor = (Math.pow((1 + deal.getNetInterestRate() / (100 * compoundingFrequency)),
		  (compoundingFrequency / paymentFrequencyFactor))) - 1;

	  averagePrincipleOutstanding = totalInterestCost / interestFactor / totalPaymentsInTerm;

	  return averagePrincipleOutstanding;
	}

	/**
	 * exPerFlexLine Returns CriticalRate if deal.MTGProdID = 15 0r 17 else
	 * returns blank
	 *
	 * @return String
	 */
	protected String exAnnualCostBorrowing() {
		String val = "";
		double annualCost = 0d;

		try {

		    // use NBC-specific total cost of borrowing, defect #1615
		    double totalCostOfBorrowing = pCalcNBCTotalCostOfBorrowingNBC();
			if (totalCostOfBorrowing != 0d){
			  	double actualPaymentTerm = deal.getActualPaymentTerm();
			  	double term = actualPaymentTerm / 12;
			  	double averagePrincipleOutstanding = pCalcAveragePrincipleOutstanding();

				annualCost = (totalCostOfBorrowing / (term * averagePrincipleOutstanding)) * 100;

				// Round the value to 2 decimals
				java.math.BigDecimal bd = new java.math.BigDecimal(annualCost);
				bd = bd.setScale(2,java.math.BigDecimal.ROUND_UP);
				annualCost = bd.doubleValue();

				// if COB < NetInterestRate, use NetInterestRate
				if (annualCost < deal.getNetInterestRate())
				  annualCost = deal.getNetInterestRate();

				val = String.valueOf(annualCost);
			}
			//ticket 1463, doc 12641--start//
			else val =  String.valueOf(0d);
			//ticket 1463, doc 12641--end//
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting TotalCostBorrowing"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * Returns number of payments per year based on paymentFrequencyId
	 *
	 * @param pPaymentFrequency
	 * @return
	 */
	private double pCalcPayFreqFactor(int pPaymentFrequency) {
		double paymentsFreqFactor;
		switch (pPaymentFrequency) {
		case Mc.PAY_FREQ_MONTHLY: {
			paymentsFreqFactor = 1d;
			break;
		}
		case Mc.PAY_FREQ_BIWEEKLY: {
			paymentsFreqFactor = 26 / 12d;
			break;
		}
		case Mc.PAY_FREQ_WEEKLY: {
			paymentsFreqFactor = 52 / 12d;
			break;
		}
		case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY: {
			paymentsFreqFactor = 26 / 12d;
			break;
		}
		case Mc.PAY_FREQ_ACCELERATED_WEEKLY: {
			paymentsFreqFactor = 52 / 12d;
			break;
		}
		default:
			paymentsFreqFactor = 1d;
		}
		return paymentsFreqFactor;
	}

	/**
	 * Returns number of payments per year based on paymentFrequencyId and
	 * number of days in year used (365.25 or 365)
	 *
	 * @param pPaymentFrequency
	 * @param daysInYear
	 * @return
	 */
	private double pCalcPayFreqFactorWithDays(int pPaymentFrequency, double daysInYear) {
	  double paymentsPerYear;
	  switch (pPaymentFrequency) {
	  case Mc.PAY_FREQ_SEMIMONTHLY: {
		paymentsPerYear = 24d;
		break;
	  }
	  case Mc.PAY_FREQ_BIWEEKLY: {
		paymentsPerYear = daysInYear / 14d;
		break;
	  }
	  case Mc.PAY_FREQ_WEEKLY: {
		paymentsPerYear = daysInYear / 7d;
		break;
	  }
	  case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY: {
		paymentsPerYear = daysInYear / 14d;
		break;
	  }
	  case Mc.PAY_FREQ_ACCELERATED_WEEKLY: {
		paymentsPerYear = daysInYear / 7d;
		break;
	  }
	  default:
		paymentsPerYear = 12d;
	  }
	  return  paymentsPerYear;
	}


	/**
	 * Returns date as string
	 *
	 * @return String
	 */
	protected String getDay(String date) {
		String val = "";
		try {
			if (date != null) {
				val = (date.toString()).substring(4, 6);
			}
		} catch (Exception e) {
			String msg = "Date  ."
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/**
	 * Returns date as string
	 *
	 * @return String
	 */
	protected String getMonth(String date) {
		String val = "";

		try {
			if (date != null) {
				val = (date.toString()).substring(0, 3);
			}
		} catch (Exception e) {
			String msg = "Month  ."
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * Returns date as string
	 *
	 * @return String
	 */
	protected String getYear(String date) {
		String val = "";
		try {
			if (date != null) {
				val = (date.toString()).substring(7, date.length());
			}
		} catch (Exception e) {
			String msg = "Year  ."
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}

	/**
	 * Returns amt formatted for Express as a string
	 *
	 * @param amt
	 *            double
	 * @return String
	 */
	protected String formatCurrency(double amt) {
	  if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) {
	      // English, don't use dollar sign - hard coded into template
	      return DocPrepLanguage.getInstance().getFormattedCurrencyWithoutDollar(amt, lang);
	    }
	    else {
	      // French
	      return DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang);
	    }
	}

	/**
	 * Utility method that formats <code>rate</code> using the current doc
	 * language. settings.
	 *
	 * @param amt
	 *            number to format into the relevant rate style
	 * @return The formatted number.
	 */
	protected String formatInterestRate(double rate) {
		return DocPrepLanguage.getInstance().getFormattedInterestRate(rate,
				lang);
	}

	/**
	 * Returns date in the Express format for a lnaguage
	 *
	 * @param date
	 *            Date
	 * @return String
	 */
	private String formatDate(Date date) {
		try {
			return DocPrepLanguage.getInstance().getFormattedDate(date, lang);
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * Returns true if string is null or ""
	 *
	 * @param str
	 *            String
	 * @return boolean
	 */
	private boolean isEmpty(String str) {
		return (str == null || str.trim().length() == INT_ZERO_CHECK);
	}

	/**
	 * @param p
	 * @return
	 */
	protected String formatBorAddress(Addr addrObj) {
		String val = null;
		try {
			val = "";

			if (!isEmpty(addrObj.getStreetNumber())) {
				val += addrObj.getStreetNumber();
			}

			//Ticket 1470--start//
			if (!isEmpty(addrObj.getStreetName())) {
				val += format.space() + addrObj.getStreetName();
			}
			if (addrObj.getStreetTypeId() != INT_ZERO_CHECK) {
				val += format.space()
						+ BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",
								addrObj.getStreetTypeId(), lang);
			}

				val += ", ";
			//Ticket 1470--end//

			if (!isEmpty(addrObj.getUnitNumber())) {
				val += addrObj.getUnitNumber();
				val += ", ";
			}

			if (!isEmpty(addrObj.getAddressLine2())) {
				val += addrObj.getAddressLine2();
				val += ", ";
			}

			if (!isEmpty(addrObj.getCity())) {
				val += addrObj.getCity();
				val += ", ";
			}
			if (addrObj.getProvinceId() != INT_ZERO_CHECK) {
				val += format.space()
						+ BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province",
								addrObj.getProvinceId(), lang);
				val += " ";
			}

			if (!isEmpty(addrObj.getPostalFSA())) {
				val += addrObj.getPostalFSA();
				val += " ";
			}
			if (!isEmpty(addrObj.getPostalLDU())) {
				val += addrObj.getPostalLDU();
			}
		} catch (Exception e) {
			e.printStackTrace();
			String msg = "NBCExtractor failed while extracting Address Line"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}

		return val;
	}

	/*Added methods for looping og Guarenters and Borrowers*/
	protected int GuaranterList(){
	  int cntr=0;
	  try {
	  if (borrowersList != null) {

		Iterator borrowerIt = borrowersList.iterator();
		while (borrowerIt.hasNext()) {
			Borrower borrower = (Borrower) borrowerIt.next();

			if (!borrower.isPrimaryBorrower()
					&& borrower.getBorrowerTypeId() == Mc.BT_GUARANTOR) {
				cntr ++;
			}
		}
	}
	  } catch (Exception e) {
		String msg = "NBCDataProvider failed while extracting GuarantorName/s"
				+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
		srk.getSysLogger().warning(this.getClass(), msg);
	}
	  return cntr;

	}
	protected int BorrowerList(){
	  String[] coborName=exCoBorName();

	   return coborName.length+1;

	}

	/**
	 * Return a "true" string if the property is in Quebec, "false" otherwise.
	 */
	protected String exProvinceIsQuebec() {
		if (pProperty != null && pProperty.getProvinceId() == Mc.PROVINCE_QUEBEC) return "true";
		return "false";
	}

	/**
	 * Return a "true" string if Guarantor exists for the deal, "false" otherwise.
	 */
	protected String exGuarantorIsAvailable() {
	  try {
		if (borrowersList != null) {
		  Iterator borrowerIt = borrowersList.iterator();
		  while (borrowerIt.hasNext()) {
			Borrower borrower = (Borrower) borrowerIt.next();
			if (!borrower.isPrimaryBorrower()
					&& borrower.getBorrowerTypeId() == Mc.BT_GUARANTOR) {
			  return "true";
			}
		  }
		}
	  } catch (Exception e) {
		String msg = "NBCDataProvider failed while extracting GuarantorName/s"
				+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
		srk.getSysLogger().warning(this.getClass(), msg);
	  }
	  return "false";
	}

	// fixes for SCR#1494,1463 - start
	/**
	 *
	 * @return a String representing the date parameter formatted specifically for
	 * Disclosure doc (#12641)
	 */
	public String formatDateForDisclosure(Date d)
	{
		SimpleDateFormat dateFormatter = null;
		String lDateFormatStr = BXResources.getGenericMsg("DOCPREP_12641_DATE_FORMAT",lang);
		if(lDateFormatStr == null || lDateFormatStr.trim().equals("DOCPREP_12641_DATE_FORMAT")){
			lDateFormatStr = "yyyy/MMM/d";
		}
			switch(lang)
			{
				case DocPrepLanguage.ENGLISH : {
				dateFormatter = new SimpleDateFormat(lDateFormatStr , new Locale("en", "CA"));
				break;
			}
			case DocPrepLanguage.FRENCH :{
				dateFormatter = new SimpleDateFormat(lDateFormatStr , new Locale("fr", ""));
			break;
		}
		default: {
			dateFormatter = new SimpleDateFormat(lDateFormatStr , new Locale("en", "CA"));
			break;
		}
	}

	return dateFormatter.format(d);
}
// fixes for SCR#1494,1463 - end


// fixes for SCR#1494 - start
/**
* Returns the payment term type id for the given payment term id
* @param paymentTermId int Payment Term ID
* @return int Payment term type id
*/
private int getPaymentTermType(int paymentTermId) {
       JdbcExecutor jExec = srk.getJdbcExecutor();
      StringBuffer sqlb = new StringBuffer();
      sqlb.append("SELECT PTTYPEID FROM PAYMENTTERM WHERE PAYMENTTERMID = ");
      sqlb.append(""+paymentTermId);
      int paymentTermType = -1;

      String sql = new String(sqlb);
      int key = 0;
      try {
                      key = jExec.execute(sql);
                      for(; jExec.next(key); ) {
                              paymentTermType = jExec.getInt(key, "PTTYPEID");
                                      break;
                      }
      } catch (Exception e) {
              StackTraceElement[] trace = e.getStackTrace();
              StringBuffer stackTraceBuffer = new StringBuffer();
              for (int i = 0; i < trace.length; i++) {
                      stackTraceBuffer.append("\t" + trace[i].toString() + "\n");
              }
              srk.getSysLogger().error(e.toString());
              srk.getSysLogger().error(stackTraceBuffer.toString());
      }
      finally {
                      try {
                                      if (key != 0) {
                                                      jExec.closeData(key);
                                      }
                      }
                      catch (Exception e) {
                              StackTraceElement[] trace = e.getStackTrace();
                              StringBuffer stackTraceBuffer = new StringBuffer();
                              for (int i = 0; i < trace.length; i++) {
                                      stackTraceBuffer.append("\t" + trace[i].toString() + "\n");
                              }
                              srk.getSysLogger().error(e.toString());
                              srk.getSysLogger().error(stackTraceBuffer.toString());
                      }
      }
      return paymentTermType;
}
// fixes for SCR#1494 - end

// fixes for SCR#1708 - start
	/**
	 * Converts a double into int (whole number)
	 * @param number double number to be formatted
	 * @return int whole number
	 */
	private int formatDouble(double number) {
		return (int) number;
	}
	// fixes for SCR#1708 - end

}
