package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;

public class CombinedGDSBorrowerOnly extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();
    
    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        double amt = deal.getCombinedGDSBorrower();
        fml.addRatio(amt);
      }
      else if(clsid == ClassId.BORROWER)
      {
        Borrower b = (Borrower)de;
        Deal deal = new Deal(srk,null,b.getDealId(), b.getCopyId());
        return extract(deal, lang, format, srk);
      }
    }
    catch (Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
