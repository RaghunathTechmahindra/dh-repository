package com.basis100.deal.docprep.extract;

import com.basis100.deal.docprep.*;

/**
 *   The Extractor interface enforces a DocPrep database extraction protocol. The
 *   implementor of start should create a new thread, extract data, create an XML
 *   DOM, load a DocGenEvent with a string representation of the xml dom and fire
 *   an event with id = DocGenEvent.DATA_EXTRACT_COMPLETE_EVENT
 */
public interface Extractor
{
  public String extractXmlString(DocumentGenerationHandler handler)throws ExtractException;
  
}