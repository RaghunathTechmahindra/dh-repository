package com.basis100.deal.docprep.rtf;

import java.util.*;

public class SwitchManager {
  private Hashtable switchTable;

  public SwitchManager() {
    switchTable = new java.util.Hashtable();
  }

  public void addSwitch(ReportSwitch swToAdd) throws Exception
  {
    if(swToAdd.getName() == null){
      throw new Exception("Could not add switch without the name.");
    }
    if(switchTable.containsKey(swToAdd.getName())){
      throw new Exception("Switch " + swToAdd.getName() + " already exists in this table." );
    }
    switchTable.put(swToAdd.getName(),swToAdd);
  }

  public boolean isSwitchExist(String nameToSearch){
    return switchTable.containsKey(nameToSearch.trim().toLowerCase());
  }

  public ReportSwitch getSwitchByName(String nameToSearch){
    nameToSearch = nameToSearch.trim().toLowerCase();
    if( isSwitchExist(nameToSearch) ){
      return (ReportSwitch)switchTable.get(nameToSearch);
    }
    return null;
  }

  public void turnSwitchON(String pName)  throws Exception
  {
    if( isSwitchExist(pName) ){
      getSwitchByName(pName).setStatus(true);
    } else {
      ReportSwitch switchToAdd = new ReportSwitch();
      switchToAdd.setName(pName);
      switchToAdd.setStatus(true);
      addSwitch(switchToAdd);
    }
  }

  public void turnSwitchOFF(String pName) throws Exception
  {
    if( isSwitchExist(pName) ){
      getSwitchByName(pName).setStatus(false);
    } else {
      ReportSwitch switchToAdd = new ReportSwitch();
      switchToAdd.setName(pName);
      switchToAdd.setStatus(false);
      addSwitch(switchToAdd);
    }
  }

  public boolean isSwitchOK(String pName, boolean pStatus)
  {
    if( !  (isSwitchExist(pName))  ){ return true;}
    if(pStatus && getSwitchByName(pName).isSwitchON()){
      return true;
    } else if((pStatus == false) && getSwitchByName(pName).isSwitchOFF()){
      return true;
    }
    return false;
  }

}
