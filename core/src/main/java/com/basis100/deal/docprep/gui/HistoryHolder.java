package com.basis100.deal.docprep.gui;

import java.util.List;
import java.util.Vector;

public class HistoryHolder {
  protected List list;
  private int capacity;
  private int index;
  private boolean DEBUG = true;

  public HistoryHolder(int capacity) {
    this.capacity = capacity;
    list = new Vector(capacity);
    index = 0;
  }

  public int getCapacity(){
    return this.capacity;
  }

  public List getHistoryList(){
    return this.list;
  }

  public void addPage(Object obj){
    list.add(obj);
    index = list.size() - 1;
  }

/*
  public void setCapacity(int capacity){
    this.capacity = capacity;
    if (list == null) {
      list = new Vector(this.capacity);
    } else {
      Vector temp = new Vector(capacity);
      temp.addAll(list);
      list = new Vector(this.capacity);
      list.addAll(temp);
      temp = null;
    }
  }
*/
  public Object getNext(){
     if (index < (list.size() - 1)){
       index++;
     }else{
       index = list.size() - 1;
     }
     return list.get(index);
     // JOptionPane.showMessageDialog(this, "Forward");
  }

  public Object getPrev(){
    if (index >=1){
      index--;
    } else {
      index = 0;
    }
    return list.get(index);
    //JOptionPane.showMessageDialog(this, "Backward");
  }


  public Object getFirst(){
    index = 0;
    return list.get(index);
    //JOptionPane.showMessageDialog(this, "Start");
  }

  public Object getLast(){
    index = list.size() - 1;
    return list.get(index);
    // JOptionPane.showMessageDialog(this, "End");
  }
}