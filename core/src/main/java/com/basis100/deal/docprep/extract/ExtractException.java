package com.basis100.deal.docprep.extract;

import com.basis100.deal.docprep.*;

public class ExtractException extends Exception
{

 /**
  * Constructs a <code>ExtractException</code> with no specified detail message.
  */
  public ExtractException()
  {
	  super();
  }

 /**
  * Constructs a <code>ExtractException</code> with the specified message.
  * @param   message  the related message.
  */
  public ExtractException(String message)
  {
    super(message);
  }
}

