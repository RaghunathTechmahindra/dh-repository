package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

/**
 * Commitment
*/
public class TotalFeeAmount extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    double amt = 0.0;
    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {

      if(clsid == ClassId.DEAL)
      {
         Deal deal = (Deal)de;

         List fees = (List)deal.getDealFees();

         Iterator it = fees.iterator();
         DealFee current = null;

         while(it.hasNext())
         {
            current = (DealFee)it.next();
            amt += current.getFeeAmount();
         }
      }

     fml.addCurrency(amt);

    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
