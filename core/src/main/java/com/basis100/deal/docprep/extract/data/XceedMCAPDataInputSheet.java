package com.basis100.deal.docprep.extract.data;

/**
* 13/Aug/2007 Venkata - Added DealTree FXP17877 - GE Money CR26D_ii - HELOC product changes - New Data sheet (MCAP info)
* 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
 - Many changes to become the Xprs standard
*    by making this class inherit from DealExtractor/XSLExtractor
*    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
*    is made available so that all templates: GENX type and the newer ones
*    alike can share the same xml
* 13/Apr/2005 DVG #DG182 #1207 Xceed - VRM (data sheet)
* 21/Mar/2005 DVG #DG172 #1103  Xceed - Data Sheet  - new field
 * 04/Jan/2005 DVG #DG118 	scr#813 Xceed - Data Sheet Change
 */

import java.util.*;

import org.w3c.dom.*;
import MosSystem.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;
import com.basis100.picklist.*;
import com.basis100.xml.*;

/**
 * Generates XML for the Xceed MCAP Data Input Sheet document type
 */
public class XceedMCAPDataInputSheet extends XceedExtractor
{
  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }
  /**
   * Builds the XML doc from various records in the database.
   *
   * @throws Exception
   */
  protected void buildXMLDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();

    Node elemNode;
    Element root = doc.createElement("Document");
    root.setAttribute("type", "1");
    root.setAttribute("lang", "1");
    doc.appendChild(root);
    
    //Venkata - 13 Aug 2007 - begin
    //Added DealTree FXP17877 - GE Money CR26D_ii - HELOC product changes - New Data sheet (MCAP info) 
    createDealTree(root);
    //Venkata - 13 Aug 2007 - end
    
    Property p = getPrimaryProperty();

    //==========================================================================
    // Language stuff
    //==========================================================================
    addTo(root, getLanguageTag());

    //==========================================================================
    // Lender Name
    //==========================================================================
    addTo(root, extractLenderName());

    //==========================================================================
    // Branch Address
    //==========================================================================
    elemNode = extractBranchAddressSection();

    if (elemNode != null)
    {
      //==========================================================================
      // Branch Phone Number (local)
      //==========================================================================
      addTo(elemNode, extractBranchPhone(null, false, "PhoneLocal"));

      //==========================================================================
      // Branch Phone Number (toll-free)
      //==========================================================================
      addTo(elemNode, extractBranchPhone(null, true, "PhoneTollFree"));

      //==========================================================================
      // Branch Fax Number (local)
      //==========================================================================
      addTo(elemNode, extractBranchFax(null, false, "FaxLocal"));

      //==========================================================================
      // Branch Fax Number (toll-free)
      //==========================================================================
      addTo(elemNode, extractBranchFax(null, true, "FaxTollFree"));

      root.appendChild(elemNode);
    }

    //==========================================================================
    // Deal #
    //==========================================================================
    addTo(root, extractDealNum());

    //==========================================================================
    // Current Date
    //==========================================================================
    addTo(root, extractCurrentDate());

    //==========================================================================
    // Servicing Mortgage Num
    //==========================================================================
    addTo(root, extractServicingMortgageNum());

    //==========================================================================
    // Financing Program
    //==========================================================================
    addTo(root, extractFinancingProgram());

    //==========================================================================
    // Previous Bankrupt Status
    //==========================================================================
    addTo(root, extractPreviousBankruptStatus());

    //==========================================================================
    // Xceed product type (alpha)
    //==========================================================================
    addTo(root, extractXceedProductTypeAlpha());

    //==========================================================================
    // Borrower Names
    //==========================================================================
    addTo(root, extractBorrowerNames());

    //==========================================================================
    // Guarantor Clause
    //==========================================================================
    addTo(root, extractGuarantorClause());

    //==========================================================================
    // Guarantor Names
    //==========================================================================
    addTo(root, extractGuarantorNames());

    //==========================================================================
    // Address
    //==========================================================================
    addTo(root, extractPrimaryBorrowerAddress());

    //==========================================================================
    // Borrower last name
    //==========================================================================
    addTo(root, extractBorrowerLastName());

    //==========================================================================
    // deal with primary borrower data
    //==========================================================================
    // Borrower phone number
    //==========================================================================
    Borrower lBorr = getPrimaryBorrower();
    addTo(root, extractBorrowerPhoneNumber(lBorr));
    addTo(root, extractBorrowerWorkPhoneNumber(lBorr));
    addTo(root, extractBorrowerWorkPhoneNumberExt(lBorr));

    //==========================================================================
    // Primary Borrower name
    //==========================================================================
    addTo(root, extractPrimaryBorrowerName());

    //==========================================================================
    // Bank Branch Number
    //==========================================================================
    addTo(root, extractBankBranchNumber());

    //==========================================================================
    // Bank Number
    //==========================================================================
    addTo(root, extractBankNumber());

    //==========================================================================
    // Bank Account Number
    //==========================================================================
    addTo(root, extractBankAccountNumber());

    //==========================================================================
    // Bank Name
    //==========================================================================
    addTo(root, extractBankName());

    //==========================================================================
    // Charge Priority
    //==========================================================================
    addTo(root, extractChargePriority());

    //==========================================================================
    // Property Address
    //==========================================================================
    addTo(root, extractPropertyAddress());

    //==========================================================================
    // Property Street Name
    //==========================================================================
    addTo(root, extractPropertyStreetName());

    //==========================================================================
    // Property Legal Lines
    //==========================================================================
    addLegalLines(root, getPrimaryProperty());

    //==========================================================================
    // Occupancy Type
    //==========================================================================
    addTo(root, extractOccupancyType(p));

    //==========================================================================
    // Property Type
    //==========================================================================
    addTo(root, extractPropertyType(p));

    //==========================================================================
    // Improvement Type
    //==========================================================================
    addTo(root, extractImprovementType());

    //==========================================================================
    // Condo
    //==========================================================================
    addTo(root, extractCondo());

    //==========================================================================
    // New Construction
    //==========================================================================
    addTo(root, extractNewConstruction(p));

    //==========================================================================
    // Condo Free Hold
    //==========================================================================
    addTo(root, extractCondoFreeHold());

    //==========================================================================
    // Appraisal Date
    //==========================================================================
    addTo(root, extractAppraisalDate(p));

    //==========================================================================
    // Land Value
    //==========================================================================
    addTo(root, extractLandValue(p));

    //==========================================================================
    // Improvement Value
    //==========================================================================
    addTo(root, extractImprovementValue());

    //==========================================================================
    // Actual Appraisal
    //==========================================================================
    addTo(root, extractActualAppraisal());

    //==========================================================================
    // Number of Units
    //==========================================================================
    addTo(root, extractNumUnits());

    //==========================================================================
    // High Ratio
    //==========================================================================
    addTo(root, extractHighRatio());

    //==========================================================================
    // Mortgage Insurance
    //==========================================================================
    addTo(root, extractMortgageInsurance());

    //==========================================================================
    // Solicitor info
    //==========================================================================
    addTo(root, extractSolicitorSection());

    //==========================================================================
    // Application date
    //==========================================================================
    addTo(root, extractApplicationDate());

    //==========================================================================
    // Approval date
    //==========================================================================
    addTo(root, extractApprovalDate());

    //==========================================================================
    // Accepted date
    //==========================================================================
    addTo(root, extractAcceptedDate());

    //==========================================================================
    // LOB
    //==========================================================================
    addTo(root, extractLOB());

    //==========================================================================
    // Xceed product type (numeric)
    //==========================================================================
    addTo(root, extractXceedProductTypeNumeric());

    //==========================================================================
    // SOB
    //==========================================================================
    addTo(root, extractSOB());

    //==========================================================================
    // Loan Amount
    //==========================================================================
    addTo(root, extractTotalLoanAmount("LoanAmount"));

    //==========================================================================
    // Advance date
    //==========================================================================
    addTo(root, extractAdvanceDate());

    //==========================================================================
    // Net Interest Rate
    //==========================================================================
    addTo(root, extractNetRate("NetInterestRate"));

    //==========================================================================
    // IAD date
    //==========================================================================
    addTo(root, extractIADDate());

    //#DG118
    //==========================================================================
    // Deal.PrePaymentOptionsID
    //==========================================================================
    addTo(root, extractDealPrePaymentOptionsID());
    //#DG118 end

    //==========================================================================
    // Payment frequency
    //==========================================================================
    addTo(root, extractPaymentFrequency());

    //==========================================================================
    // Accelerated
    //==========================================================================
    addTo(root, extractPaymentFrequencyAccelerated());


    //==========================================================================
    // First payment date
    //==========================================================================
    addTo(root, extractFirstPaymentDate());

    //==========================================================================
    // Payment Term
    //==========================================================================
    addTo(root, extractTerm());

    //==========================================================================
    // Maturity date
    //==========================================================================
    addTo(root, extractMaturityDate());

    //==========================================================================
    // Amortization
    //==========================================================================
    addTo(root, extractAmortization());

    //==========================================================================
    // Total payment
    //==========================================================================
    addTo(root, extractTotalPayment());

    //==========================================================================
    // Tax department name
    //==========================================================================
    addTo(root, extractTaxDepartmentName());

    //==========================================================================
    // Annual taxes
    //==========================================================================
    addTo(root, extractAnnualTaxes());

    //==========================================================================
    // Tax escrow amount adjusted by payment frequency
    //==========================================================================
    addTo(root, extractTaxEscrowAmountByFrequency());

    //==========================================================================
    // Tax escrow amount
    //==========================================================================
    addTo(root, extractTaxEscrowAmount());

    //==========================================================================
    // Total payment amount
    //==========================================================================
    addTo(root, extractTotalPayment("TotalPaymentAmount"));

    //==========================================================================
    // Language
    //==========================================================================
    addTo(root, extractLanguage());

    addTo(root, extractCrossSell());  //  #DG172 Retrieves Cross sell value
    addTo(root, getTag("InterestTypeId", String.valueOf(deal.getInterestTypeId())));  //  #DG182 new, InterestTypeId
    addTo(root, getTag("MtgProdId", String.valueOf( deal.getMtgProdId())));    //#1207 Xceed
  }

  /**
   * Extracts the accepted date
   *
   * @return populated tag
   */
  protected Node extractAcceptedDate()
  {
    String val = null;

    try
    {
      val = formatDate(deal.getCommitmentAcceptDate());
    }
    catch (Exception e)
    {
    }

    return getTag("AcceptedDate", val);
  }

  /**
   * Extracts the actual appraisal amount.
   *
   * @return populated tag
   */
  protected Node extractActualAppraisal()
  {
    String val = null;

    try
    {
      val = formatCurrency(getPrimaryProperty().getActualAppraisalValue());
    }
    catch (Exception e)
    {
    }

    return getTag("ActualAppraisal", val);
  }

  /**
   * Extracts the application date.
   *
   * @return populated tag
   */
  protected Node extractApplicationDate()
  {
    String val = null;

    try
    {
      val = formatDate(deal.getApplicationDate());
    }
    catch (Exception e)
    {
    }

    return getTag("ApplicationDate", val);
  }

  /**
   * Retrieves the Bank Account # from the deal and formats an XML tag with it.
   *
   * @return The completed Bank Account # XML tag.
   */
  protected Node extractBankAccountNumber()
  {
    String val = null;

    try
    {
      val = deal.getBankAccountNumber();
    }
    catch (Exception e)
    {
    }

    return getTag("BankAccountNumber", val);
  }

  /**
   * Retrieves the Bank Branch # from the deal and formats an XML tag with it.
   *
   * @return The completed Bank Branch # XML tag.
   */
  protected Node extractBankBranchNumber()
  {
    String val = null;

    try
    {
      val = deal.getBankABANumber().substring(4);
    }
    catch (Exception e)
    {
    }

    return getTag("BankBranchNumber", val);
  }

  /**
   * Retrieves the Bank Name from the deal and formats an XML tag with it.
   *
   * @return The completed Bank Name XML tag.
   */
  protected Node extractBankName()
  {
    String val = null;

    try
    {
      val = deal.getBankName();
    }
    catch (Exception e)
    {
    }

    return getTag("BankName", val);
  }

  /**
   * Retrieves the Bank # from the deal and formats an XML tag with it.
   *
   * @return The completed Bank # XML tag.
   */
  protected Node extractBankNumber()
  {
    String val = null;

    try
    {
      val = deal.getBankABANumber().substring(0, 3);
    }
    catch (Exception e)
    {
    }

    return getTag("BankNumber", val);
  }

  /**
   * Retrieves the phone phone number of the primary borrower from the deal and
   * formats an XML tag with it.
   *
   * @return The completed phone number XML tag.
   * @param pBor Borrower
   * @return Node
   */
  protected Node extractBorrowerWorkPhoneNumber(Borrower pBor)
  {
    String val = null;

    try
    {
      val = StringUtil.getAsFormattedPhoneNumber(pBor.getBorrowerWorkPhoneNumber(), null);
    }
    catch (Exception e)
    {
    }

    return getTag("BorrowerWorkPhoneNumber", val);
  }

  /**
   * Retrieves the phone phone number of the primary borrower from the deal and
   * formats an XML tag with it.
   *
   * @return The completed phone number XML tag.
   * @param pBor Borrower
   * @return Node
   */
  protected Node extractBorrowerWorkPhoneNumberExt(Borrower pBor)
  {
    String val = null;

    try
    {
      val = StringUtil.getAsFormattedPhoneNumber(pBor.getBorrowerWorkPhoneExtension(), null);
    }
    catch (Exception e)
    {
    }

    return getTag("BorrowerWorkPhoneNumberExt", val);
  }


  /**
   * Retrieves the phone number of the primary borrower from the deal and
   * formats an XML tag with it.
   *
   * @return The completed phone number XML tag.
   * @param pBor Borrower
   * @return Node
   */
  protected Node extractBorrowerPhoneNumber(Borrower pBor)
  {
    String val = null;

    try
    {
      val = StringUtil.getAsFormattedPhoneNumber(pBor.getBorrowerHomePhoneNumber(),  null);
    }
    catch (Exception e)
    {
    }

    return getTag("BorrowerPhoneNumber", val);
  }

  /**
   * Retrieves the Charge Priority from the deal and formats an XML tag with
   * it.
   *
   * @return The completed Charge Priority XML tag.
   */
  protected Node extractChargePriority()
  {
    String val = null;

    try
    {
      val = (deal.getLienPositionId() == 1) ? "2" : "1";
    }
    catch (Exception e)
    {
    }

    return getTag("ChargePriority", val);
  }

  /**
   * Extracts condo
   *
   * @return populated tag
   */
  protected Node extractCondo()
  {
    String val = null;

    try
    {
      int ptID = getPrimaryProperty().getPropertyTypeId();

      if ((ptID == Mc.PROPERTY_TYPE_CONDOMINIUM) ||
            (ptID == Mc.PROPERTY_TYPE_FREEHOLD_CONDO) ||
            (ptID == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO) ||
            (ptID == Mc.PROPERTY_TYPE_TOWNHOUSE_CONDO) ||
            (ptID == Mc.PROPERTY_TYPE_STRATA))
      {
        val = "Y";
      }
      else
      {
        val = "N";
      }
    }
    catch (Exception e)
    {
    }

    return getTag("Condo", val);
  }

  /**
   * Extracts condo freehold
   *
   * @return populated tag
   */
  protected Node extractCondoFreeHold()
  {
    String val = null;

    try
    {
      int ptID = getPrimaryProperty().getPropertyTypeId();

      val = (ptID == Mc.PROPERTY_TYPE_FREEHOLD_CONDO) ? "Y" : "N";
    }
    catch (Exception e)
    {
    }

    return getTag("CondoFreeHold", val);
  }

  /**
   * Retrieves the Deal Number from the deal and formats an XML tag with it.
   *
   * @return The completed Deal Number XML tag.
   */
  protected Node extractDealNum()
  {
    String val = null;

    try
    {
      val = Integer.toString(deal.getDealId());
    }
    catch (Exception e)
    {
    }

    return getTag("DealNum", val);
  }

  /**
   * Retrieves the financing program from the deal and formats an XML tag with
   * it.
   *
   * @return The completed financing program XML tag.
   */
  protected Node extractFinancingProgram()
  {
    String val = null;

    try
    {
      int id = deal.getFinanceProgramId();

      val = ((id == 1) || (id == 2)) ? "Y" : "N";
    }
    catch (Exception e)
    {
    }

    return getTag("FinancingProgram", val);
  }

  /**
   * Extracts High ratio
   *
   * @return populated tag
   */
  protected Node extractHighRatio()
  {
    String val = null;

    try
    {
      int lId  = deal.getMtgProdId() ;
      if ( lId == 111 || lId == 112 || lId == 113 || lId == 114){
        val = "Y";
      }else{
        int id = deal.getDealTypeId();

        val = ((id == Mc.DEAL_TYPE_CONVENTIONAL) ||
              (id == Mc.DEAL_TYPE_CONV_INSURED)) ? "N" : "Y";
      }
    }
    catch (Exception e)
    {
    }

    return getTag("HighRatio", val);
  }

  /**
   * Retrieves the improvement type on the primary property and formats an XML
   * tag with it.
   *
   * @return The completed improvement type XML tag.
   */
  protected Node extractImprovementType()
  {
    String val = null;

    try
    {
      Property p = getPrimaryProperty();

      int dtID = p.getDwellingTypeId();
      int ptID = p.getPropertyTypeId();

      if ((ptID == Mc.PROPERTY_TYPE_CONDOMINIUM) ||
            (ptID == Mc.PROPERTY_TYPE_FREEHOLD_CONDO) ||
            (ptID == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO) ||
            (ptID == Mc.PROPERTY_TYPE_TOWNHOUSE_CONDO) ||
            (ptID == Mc.PROPERTY_TYPE_STRATA))
      {
        val = "07";
      }
      else
      {
        switch (dtID)
        {
          case Mc.DWELLING_TYPE_DETACHED:
          case Mc.DWELLING_TYPE_MOBILE:
            val = "01";

            break;

          case Mc.DWELLING_TYPE_SEMI_DETACHED:
            val = "02";

            break;

          case Mc.DWELLING_TYPE_DUPLEX:
            val = "03";

            break;

          case Mc.DWELLING_TYPE_TRI_PLEX:
            val = "04";

            break;

          default:

            //val = PicklistData.getDescription("DwellingType", dtID);
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DwellingType", dtID, lang);
        }
      }
    }
    catch (Exception e)
    {
    }

    return getTag("ImprovementType", val);
  }

  /**
   * Extracts improvement value.
   *
   * @return populated tag
   */
  protected Node extractImprovementValue()
  {
    String val = null;

    try
    {
      Property p = getPrimaryProperty();

      val = formatCurrency(p.getActualAppraisalValue() - p.getLandValue());
    }
    catch (Exception e)
    {
    }

    return getTag("ImprovementValue", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractLOB()
  {
    String val = null;

    try
    {
      int id = deal.getLineOfBusinessId();

      switch (id)
      {
        case 0:
          val = "IE";

          break;

        case 1:
          val = "IF";

          break;

        case 2:
          val = "IG";

          break;
      }
    }
    catch (Exception e)
    {
    }

    return getTag("LOB", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractLanguage()
  {
    String val = null;

    try
    {
      Borrower b = getPrimaryBorrower();

      val = (b.getLanguagePreferenceId() == Mc.LANGUAGE_PREFERENCE_FRENCH)
            ? "F" : "E";
    }
    catch (Exception e)
    {
    }

    return getTag("Language", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractMortgageInsurance()
  {
    String val = null;

    try
    {
      switch (deal.getMortgageInsurerId())
      {
        case Mc.MI_INSURER_BLANK:
          val = "1";

          break;

        case Mc.MI_INSURER_CMHC:
          val = "2";

          break;

        case Mc.MI_INSURER_GE:
          val = "4";

          break;
      }
    }
    catch (Exception e)
    {
    }

    return getTag("MortgageInsurance", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractNumUnits()
  {
    String val = null;

    try
    {
      val = Integer.toString(getPrimaryProperty().getNumberOfUnits());
    }
    catch (Exception e)
    {
    }

    return getTag("NumUnits", val);
  }


  /**
   * If paymentFrequencyId from deal entity is 3 or 5  return Y
   * otherwise return N.
   *
   * @return Node
   */
  protected Node extractPaymentFrequencyAccelerated(){
    if(deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY ||
       deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_WEEKLY){
      return getTag("PaymentFrequencyAccelerated", "Y");
    }
    return getTag("PaymentFrequencyAccelerated", "N");
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractPaymentFrequency()
  {
    String val = null;

    try
    {
      switch (deal.getPaymentFrequencyId())
      {
        case 0:
          val = "12";

          break;

        case 1:
          val = "24";

          break;

        case 2:
        case 3:
          val = "26";

          break;

        case 4:
        case 5:
          val = "52";

          break;
      }
    }
    catch (Exception e)
    {
    }

    return getTag("PaymentFrequency", val);
  }

  /**
   * Indicates whether the client has been bankrupt before and formats an XML
   * tag with it.
   *
   * @return The completed previous bankrupt status XML tag.
   */
  protected Node extractPreviousBankruptStatus()
  {
    String val = null;

    try
    {
      int id = getPrimaryBorrower().getBankruptcyStatusId();

      val = (id == 0) ? "N" : "Y";
    }
    catch (Exception e)
    {
    }

    return getTag("PreviousBankruptStatus", val);
  }

  /**
   * Retrieves the primary borrower's address from the deal and formats an XML
   * tag with it.
   *
   * @return The completed address XML tag.
   */
  protected Node extractPrimaryBorrowerAddress()
  {
    Node addrNode = null;

    try
    {
      BorrowerAddress ba = new BorrowerAddress(srk);
      Borrower b         = getPrimaryBorrower();

      ba.findByCurrentAddress(b.getBorrowerId(), b.getCopyId());

      Addr addr = ba.getAddr();

      if (addr == null)
      {
        return null;
      }

      addrNode = doc.createElement("BorrowerAddress");

      addTo(addrNode, "Line1", addr.getAddressLine1());
      addTo(addrNode, "Line2", addr.getAddressLine2());
      addTo(addrNode, "City", addr.getCity());

      Province prov = new Province(srk, addr.getProvinceId());

      addTo(addrNode, "Province", prov.getProvinceName());

      //  PostalCode
      addTo(addrNode, extractPostalCode(addr));
    }
    catch (Exception e)
    {
    }

    return addrNode;
  }

  /**
   * Retrieves the name of the primary borrower from the deal and formats an
   * XML tag with it.
   *
   * @return The completed name XML tag.
   */
  protected Node extractPrimaryBorrowerName()
  {
    String val = null;

    try
    {
      Borrower b = getPrimaryBorrower();

      val = "";

      if (b.getBorrowerFirstName() != null)
      {
        val += b.getBorrowerFirstName();
      }

      if (b.getBorrowerLastName() != null)
      {
        val += (format.space() + b.getBorrowerLastName());
      }
    }
    catch (Exception e)
    {
    }

    return getTag("PrimaryBorrowerName", val);
  }

  /**
   * Retrieves the Property street name from the deal and formats an XML tag
   * with it.
   *
   * @return The completed property street name XML tag.
   */
  protected Node extractPropertyStreetName()
  {
    String val = null;

    try
    {
      val = getPrimaryProperty().getPropertyStreetName();
    }
    catch (Exception e)
    {
    }

    return getTag("PropertyStreetName", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractSOB()
  {
    String val = null;

    try
    {
      int id = deal.getSourceOfBusinessProfileId();

      if ((id == Mc.SOB_CATEGORY_TYPE_INTERNET) ||
            (id == Mc.SOB_CATEGORY_TYPE_DIRECT) ||
            (id == Mc.SOB_CATEGORY_TYPE_STAFF_REFERRAL) ||
            (id == Mc.SOB_CATEGORY_TYPE_STAFF_OF_LENDER))
      {
        val = "02";
      }
      else
      {
        val = "01";
      }
    }
    catch (Exception e)
    {
    }

    return getTag("SOB", val);
  }

  /**
   * Retrieves the Servicing Mortgage Number from the deal and formats an XML
   * tag with it.
   *
   * @return The completed Servicing Mortgage Number XML tag.
   */
  protected Node extractServicingMortgageNum()
  {
    String val = null;

    try
    {
      val = deal.getServicingMortgageNumber();
    }
    catch (Exception e)
    {
    }

    return getTag("ServicingMortgageNum", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractTaxDepartmentName()
  {
    String val = null;

    try
    {
      PartyProfile partyProfile = new PartyProfile(srk);
      DealPK dealpk             = new DealPK(deal.getDealId(), deal.getCopyId());
      List parties              = (List) partyProfile.findByDealAndType(dealpk,
                                                                        Mc.PARTY_TYPE_MUNICIPALITY);

      val = ((PartyProfile) parties.get(0)).getPartyCompanyName();
    }
    catch (Exception e)
    {
    }

    return getTag("TaxDepartmentName", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractTaxEscrowAmount()
  {
    String val = null;

    try
    {
      EscrowPayment ep = new EscrowPayment(srk, null);

      List list = (List) ep.findByDeal((DealPK) deal.getPk());

      if (!list.isEmpty())
      {
        Iterator i = list.iterator();

        while (i.hasNext())
        {
          ep = (EscrowPayment) i.next();

          if (ep.getEscrowTypeId() == 0)
          {
            val = formatCurrency(ep.getEscrowPaymentAmount());

            break;
          }
        }
      }
    }
    catch (Exception e)
    {
    }

    return getTag("TaxEscrowAmount", val);
  }

  protected Node extractTaxEscrowAmountByFrequency()
  {
    String val = null;

    try
    {
      EscrowPayment ep = new EscrowPayment(srk, null);

      List list = (List) ep.findByDeal((DealPK) deal.getPk());

      if (!list.isEmpty())
      {
        Iterator i = list.iterator();

        while (i.hasNext())
        {
          ep = (EscrowPayment) i.next();

          if (ep.getEscrowTypeId() == 0)
          {

            double lAmt = ep.getEscrowPaymentAmount();
            if(deal.getPaymentFrequencyId() == Mc.PAY_FREQ_BIWEEKLY || deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY){
              lAmt = lAmt * 12 / 26;
            }else if(deal.getPaymentFrequencyId() == Mc.PAY_FREQ_WEEKLY || deal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_WEEKLY){
              lAmt = lAmt * 12 / 52;
            }
            val = formatCurrency( lAmt );

            //val = formatCurrency(ep.getEscrowPaymentAmount());

            break;
          }
        }
      }
    }
    catch (Exception e)
    {
    }

    return getTag("TaxEscrowAmountByFrequency", val);
  }


  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractTotalPayment()
  {
    String val = null;

    try
    {
      val = formatCurrency(deal.getPandiPaymentAmount() +
                           deal.getAdditionalPrincipal());
    }
    catch (Exception e)
    {
    }

    return getTag("TotalPayment", val);
  }

  /**
   * Retrieves the Xceed product type alpha from the deal and formats an XML
   * tag with it.
   *
   * @return The completed Xceed product type alpha XML tag.
   */
  protected Node extractXceedProductTypeAlpha()
  {
    String val = null;

    try
    {
      PricingProfile pp = new PricingProfile(srk,
                                             deal.getMtgProd()
                                                 .getPricingProfileId());

      val = pp.getPPDescription();
    }
    catch (Exception e)
    {
    }

    return getTag("XceedProductTypeAlpha", val);
  }

  /**
   * DOCUMENT ME!
   *
   * @return Node
   */
  protected Node extractXceedProductTypeNumeric()
  {
    String val = null;

    try
    {
      int lId  = deal.getMtgProdId() ;
      if ( lId == 111 || lId == 112 || lId == 113 || lId == 114){
        val = "45";
      }else{
        PricingProfile pp = new PricingProfile(srk,
                                               deal.getMtgProd()
                                                   .getPricingProfileId());

        val = pp.getPPBusinessId();
      }
    }
    catch (Exception e)
    {  // empty exception block ?!?!?!?!?
    }

    return getTag("XceedProductTypeNumeric", val);
  }

  /**  #DG118
   * Retrieves the PrePaymentOptionsID from the deal and formats an XML tag with it.
   *
   * @return Node The completed XML tag.
   */
  protected Node extractDealPrePaymentOptionsID()
  {
    String lsval = null;

    try
    {
      lsval = Integer.toString(deal.getPrePaymentOptionsId());
    }
    catch (Exception e)
    {
    }

    return getTag("PrePaymentOptionsID", lsval);
  }

  /**  #DG172
   * Retrieves Cross sell value from bundles or crossellprofile.csdescription and formats an XML tag with it.
   *
   * @return Node The completed XML tag.
   */
  protected Node extractCrossSell()
  {
    String sval = null;
    int icrossId = deal.getCrossSellProfileId();
    try
    {
      CrossSellProfile ocrs = new CrossSellProfile(srk, icrossId);
      sval = ocrs.getCSDescription();
    }
    catch (Exception e)
    {
      sval = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "CROSSSELLPROFILE",icrossId,lang);
    }
    return getTag("CrossSellProfile", sval);
  }
}
