package com.basis100.deal.docprep.extract.data;
/**
 * 27/Aug/2007 Venkata - FXP18053 - NBC CR022 - Deal summary changes
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.conditions.ConditionParser;
import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Bridge;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.CreditBureauReport;
import com.basis100.deal.entity.CrossSellProfile;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.IncomeType;
import com.basis100.deal.entity.InvestorProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.Province;
import com.basis100.deal.entity.QualifyDetail;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.CurrentBorrowerAddressComparator;
import com.basis100.deal.util.collections.CurrentEmploymentComparator;
import com.basis100.deal.util.collections.DocumentStatusComparator;
import com.basis100.deal.util.collections.LiabilityPercentOutGDSComparator;
import com.basis100.deal.util.collections.PrimaryBorrowerComparator;
import com.basis100.deal.util.collections.PrimaryPropertyComparator;
import com.basis100.deal.validation.PagelessBusinessRuleExecutor;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.mtgrates.PricingLogic2;
import com.basis100.mtgrates.RateInfo;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.xml.XmlToolKit;
import com.filogix.util.Xc;


/**
 * <p>
 * Title: GENXDealSummary.java
 * </p>
 * <p>
 * Description : Generates XML for Deal Summary document type
 * </p>
 * @version 1.0 Initial Version
 * @version 1.1<br>
 *          Date:01-Aug-2008 <br>
 *          Author:MCM Impl Team <br>
 *          Story:XS_16.23<br>
 *          Change :Modified buildXMLDataDoc(), Added
 *          extractQualifyingDetailsSection(), extractComponentDetailsSection()
 *          methods.
 * @version 1.2<br>
 *          Date:05-Aug-2008 <br>
 *          Author:MCM Impl Team <br>
 *          Story:XS_16.23<br>
 *          Modified extractQualifyingDetailsSection() method
 *
 * @version 1.3<br>
 *          Date:03-Oct-2008 <br>
 *          Author:MCM Impl Team <br>
 *          Artifact:FXP22747<br>
 *          Modified extractQualifyingDetailsSection() method
 *          
 * @version 1.4<br>
 *          Date:12/03/2008 <br>
 *          Artifact:FXP23783 <br>
 *          fix various "Yes" "No" label issue
 */
public class GENXDealSummary extends GENXExtractor
{

	//Ticket 616--start//
	MtgProd mtgProd;
	PricingProfile pp = null;
	//Ticket 616--end//

  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }
    /**
     * Builds the XML doc from various records in the database.
     * @version 1.0 Initial Version
     * @version 1.1 01-Aug-2008 XS_16.23 Added method call
     *          extractComponentDetailsSection(),
     *          extractQualifyingDetailsSection() to Generate XML related to
     *          Component Details, Summary,Qualifying Details Section.
     */
    protected void buildXMLDataDoc() throws Exception
    {
        doc = XmlToolKit.getInstance().newDocument();
        Element root = doc.createElement("Document");
        root.setAttribute("type","1");
        root.setAttribute("lang","1");
        doc.appendChild(root);

        //==========================================================================
        // Language stuff
        //==========================================================================
        addTo(root, getLanguageTag());

        //==========================================================================
        // Current Date
        //==========================================================================
        addTo(root, extractCurrentDate());

        //==========================================================================
        // Deal Num
        //==========================================================================
        addTo(root, extractDealNum());

        //==========================================================================
        // Borrower Name
        //==========================================================================
        addTo(root, extractBorrowerName());

        //==========================================================================
        // Deal Status
        //==========================================================================
        addTo(root, extractDealStatus());

        //==========================================================================
        // Deal Status Date
        //==========================================================================
        addTo(root, extractDealStatusDate());

        //==========================================================================
        // Source Firm (short)
        //==========================================================================
        addTo(root, extractSourceFirm());

        //==========================================================================
        // Source of Business (short)
        //==========================================================================
        addTo(root, extractSource());

        //==========================================================================
        // Line Of Business
        //==========================================================================
        addTo(root, extractLOB());

        //==========================================================================
        // Deal Type
        //==========================================================================
        addTo(root, extractDealType());

        //==========================================================================
        // Deal Purpose
        //==========================================================================
        addTo(root, extractDealPurpose());

        //==========================================================================
        // Total Loan Amount
        //==========================================================================
        addTo(root, extractTotalLoanAmount());

        //==========================================================================
        // Purchase Price
        //==========================================================================
        addTo(root, extractPurchasePrice());

        //==========================================================================
        // Payment Term
        //==========================================================================
        addTo(root, extractPaymentTerm());

        //==========================================================================
        // Actual Payment Term
        //==========================================================================
        addTo(root, extractActualPaymentTerm());

        //==========================================================================
        // Estimated Closing Date
        //==========================================================================
        addTo(root, extractEstClosingDate());

        //==========================================================================
        // Special Feature
        //==========================================================================
        addTo(root, extractSpecialFeature());

        //==========================================================================
        // Applicant details section (employment, assets, liabilities, etc)
        //==========================================================================
        addTo(root, extractApplicantDetailsSection());

        //==========================================================================
        // Properties
        //==========================================================================
        addTo(root, extractPropertiesSection());

        //==========================================================================
        // Effective Amortization
        //==========================================================================
        addTo(root, extractEffectiveAmortization());

        //==========================================================================
        // Amortization
        //==========================================================================
        addTo(root, extractAmortization());

        //==========================================================================
        // Payment Frequency
        //==========================================================================
        addTo(root, extractPaymentFrequency());

        //==========================================================================
        // LTV Ratio
        //==========================================================================
        addTo(root, extractLTV());

        //==========================================================================
        // Rate Code
        //==========================================================================
        addTo(root, extractRateCode());

        //==========================================================================
        // Rate Date
        //==========================================================================
        addTo(root, extractRateDate());

        //==========================================================================
        // Posted Interest Rate
        //==========================================================================
        addTo(root, extractPostedInterestRate());

        //==========================================================================
        // Net Interest Rate
        //==========================================================================
        addTo(root, extractNetRate());

        //==========================================================================
        // Rate Locked In
        //==========================================================================
        addTo(root, extractRateLockedIn());

        //==========================================================================
        // Discount
        //==========================================================================
        addTo(root, extractDiscount());

        //==========================================================================
        // Premium
        //==========================================================================
        addTo(root, extractPremium());

        //==========================================================================
        // Buydown Rate
        //==========================================================================
        addTo(root, extractBuydownRate());

        //==========================================================================
        // PreApproval Estimate Purchase Price
        //==========================================================================
        addTo(root, extractPAPurchasePrice("PreAppEstPurchasePrice"));

        //==========================================================================
        // Special Feature
        //==========================================================================
        //addTo(root, extractSpecialFeature());

        //==========================================================================
        // P&I Payment
        //==========================================================================
        addTo(root, extractPandIPayment());

        //==========================================================================
        // Additional Principal Payment
        //==========================================================================
        addTo(root, extractAdditionalPrincipalPayment());

        //==========================================================================
        // Total Escrow Payment
        //==========================================================================
        addTo(root, extractTotalEscrowPayment());

        //==========================================================================
        // Total Payment
        //==========================================================================
        addTo(root, extractTotalPayment());

        //==========================================================================
        // First Payment Date
        //==========================================================================
        addTo(root, extractFirstPaymentDate());

        //==========================================================================
        // Maturity Date
        //==========================================================================
        addTo(root, extractMaturityDate());

        //==========================================================================
        // Second Mortgage
        //==========================================================================
        addTo(root, extractSecondMortgage());

        //==========================================================================
        // Equity Available
        //==========================================================================
        addTo(root, extractEquityAvailable());

        //==========================================================================
        // Product
        //==========================================================================
        addTo(root, extractProduct());

        //==========================================================================
        // PrePayment Option
        //==========================================================================
        addTo(root, extractPrepaymentOption());

        //==========================================================================
        // PrePayment Penalty
        //==========================================================================
        addTo(root, extractPrepayment());

        //==========================================================================
        // Reference Type
        //==========================================================================
        addTo(root, extractReferenceType());

        //==========================================================================
        // Reference #
        //==========================================================================
        addTo(root, extractReferenceNum());

        //==========================================================================
        // Estimated Closing Date
        //==========================================================================
        addTo(root, extractEstClosingDate());

        //==========================================================================
        // Actual Closing Date
        //==========================================================================
        addTo(root, extractActualClosingDate());

        //==========================================================================
        // Privilege Payment Option
        //==========================================================================
        addTo(root, extractPrivilegePaymentOption());

        //==========================================================================
        // Source Name
        //==========================================================================
        addTo(root, extractSourceName());

        //==========================================================================
        // Reference Source App #
        //==========================================================================
        addTo(root, extractReferenceSourceAppNum());

        //==========================================================================
        // Cross Sell
        //==========================================================================
        addTo(root, extractCrossSell());

        //==========================================================================
        // Financing Program
        //==========================================================================
        addTo(root, extractFinancingProgram());

        //==========================================================================
        // Source Address
        //==========================================================================
        addTo(root, extractSourceAddress());

        //==========================================================================
        // Source Phone
        //==========================================================================
        addTo(root, extractSourcePhone());

        //==========================================================================
        // Source Fax
        //==========================================================================
        addTo(root, extractSourceFax());

        //==========================================================================
        // Total Purchase Price
        //==========================================================================
        addTo(root, extractTotalPurchasePrice());

        //==========================================================================
        // Total Estimated Value
        //==========================================================================
        addTo(root, extractTotalEstimatedValue());

        //==========================================================================
        // Total Actual Appraised Value
        //==========================================================================
        addTo(root, extractTotalActualAppraisedValue());

        //==========================================================================
        // Lender
        //==========================================================================
        addTo(root, extractLender());

        //==========================================================================
        // Source Fax
        //==========================================================================
        addTo(root, extractInvestor());

        //==========================================================================
        // MI Section
        //==========================================================================
        addTo(root, extractMortageInsuranceInformation());

        //==========================================================================
        // Bridge Details
        //==========================================================================
        addTo(root, extractBridgeDetailsSection());

        //==========================================================================
        // Equity Take-Out section
        //==========================================================================
        String val = null;
        try
        {
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DealRefinanceFlag", deal.getDealPurposeId() , 0);
        }
        catch (Exception e){}

        if ( val.equals("Y"))
        {
            addTo(root, extractEquityTakeOutSection());
        }

        //==========================================================================
        //  Progress Advance
        //==========================================================================
        addTo(root, extractProgressAdvance());

        //==========================================================================
        //  Advance To Date Amount
        //==========================================================================
        addTo(root, extractAdvanceToDateAmount());

        //==========================================================================
        //  Next Advance Amount
        //==========================================================================
        addTo(root, extractNextAdvanceAmount());

        //==========================================================================
        //  Advance Number
        //==========================================================================
        addTo(root, extractAdvanceNumber());

        //==========================================================================
        // Down Payments Section
        //==========================================================================
        addTo(root, extractDownPaymentsSection());

        //==========================================================================
        // Credit Score
        //==========================================================================
        addTo(root, extractCreditScore());

        //==========================================================================
        // Combined GDS
        //==========================================================================
        addTo(root, extractCombinedGDS());

        //==========================================================================
        // Combined 3 Year GDS
        //==========================================================================
        addTo(root, extractCombined3YrGDS());

        //==========================================================================
        // Combined TDS
        //==========================================================================
        addTo(root, extractCombinedTDS());

        //==========================================================================
        // Combined 3 Year TDS
        //==========================================================================
        addTo(root, extractCombined3YrTDS());
        
        //==========================================================================
        // Qualify Rate
        //==========================================================================
        addTo(root, extractQualifyRate());
        
        //==========================================================================
        // Qualify Product
        //==========================================================================
        addTo(root, extractQualifyProduct());

        //==========================================================================
        // Combined Total Income
        //==========================================================================
        addTo(root, extractCombinedTotalIncome());

        //==========================================================================
        // Combined Total Assets
        //==========================================================================
        addTo(root, extractCombinedTotalAssets());

        //==========================================================================
        // Combined Total Liabilities
        //==========================================================================
        addTo(root, extractCombinedTotalLiabilities());

        //==========================================================================
        // Underwriter
        //==========================================================================
        addTo(root, extractUnderwriter());

        //==========================================================================
        // Final Approver
        //==========================================================================
        addTo(root, extractFinalApprover());

        //==========================================================================
        // Joint Approver
        //==========================================================================
        addTo(root, extractJointApprover());

        //==========================================================================
        // Administrator
        //==========================================================================
        addTo(root, extractAdministrator());

        //==========================================================================
        // Funder
        //==========================================================================
        addTo(root, extractFunder());

        //==========================================================================
        // Approvers (1st through 5th)
        //==========================================================================
        //for (int i = 1; i < 6; i++)
        //    addTo(root, extractApprover(i));

        //==========================================================================
        // Fees Section
        //==========================================================================
        addTo(root, extractFeesSection());

        //==========================================================================
        // Conditions Section
        //==========================================================================
        addTo(root, extractConditionsSection());

        //==========================================================================
        // Notes Section
        //==========================================================================
        addTo(root, extractNotesSection());

        //==========================================================================
        // Credit Bureau Report
        //==========================================================================
        addTo(root, extractCreditBureauReport());

        //==========================================================================
        // Business Rules
        //==========================================================================
        addTo(root, extractBusinessRulesSection());

        //==========================================================================
        // Net Loan Amount
        //==========================================================================
        addTo(root, extractNetLoanAmount());

        //==========================================================================
        // Holdback
        //==========================================================================
        addTo(root, extractAdvanceHold());

        //==========================================================================
        // Commitment Return Date
        //==========================================================================
        addTo(root, extractCommitmentExpiryDate());

        //==========================================================================
        // Tax Payor
        //==========================================================================
        addTo(root, extractTaxPayor());

        //==========================================================================
        // Charge
        //==========================================================================
        addTo(root, extractCharge());

        //==========================================================================
        // Branch Name
        //==========================================================================
        addTo(root, extractBranchName());

        //==========================================================================
        // Reference Existing Mortgage Number
        //==========================================================================
        addTo(root, "RefExistingMtgNum", deal.getRefExistingMtgNumber());

        //==========================================================================
        // Repayment Type
        //==========================================================================
        addTo(root, extractRepaymentType());

        //==========================================================================
        // Down payment
        //==========================================================================
       /*Other details for Bank Name and others*/
        addTo(root, extractFeesSectionEnhancement());
        addTo(root, extractTotalEscrowPaymentEnhancement());
        addTo(root,extractOtherDetailsbank());
        addTo(root, extractInterestAdjustmentRate());
        addTo(root, extractRequiredDownPaymentAmount());
         addTo(root, extractnulltag());
        addTo(root, extractCashbackAmount());
        addTo(root, extractBankName());
        addTo(root, extractBankTransitNumber());
        addTo(root, extractBankAccountNumber());
        //------> Rel 3.1 - Sasa
        addTo(root, extractProductType());
        //------> End of Rel 3.1 - Sasa
        addTo(root, extractIndexRate());
        addTo(root, extractIndexAdjustment());

        //Ticket 616--start//
        mtgProd = new MtgProd(srk, null);
        MtgProdPK mtgPK = new MtgProdPK(deal.getMtgProdId());
		mtgProd = mtgProd.findByPrimaryKey(mtgPK);

		pp = deal.getActualPricingProfile();
        //Ticket 616--end//

	addTo(root, exAlternativeID());
	addTo(root, exSOBBusinessID());
	addTo(root, exBestRate());
	addTo(root, exProductCode());
	addTo(root, exInterestRateChangeFreq());
	addTo(root, exBestRateLabel());
	addTo(root, extractAffiliationProgram());

   /**************MCM Impl team changes - 31-Jul-2008 - Starts - XS_16.23 ****/
  addTo(root, extractComponentDetailsSection());

  addTo(root, extractQualifyingDetailsSection());
  //To Retrive the Under Writer Type ID
  addTo(root, "UnderWriteAsId",String.valueOf(deal.getMtgProd().getUnderwriteAsTypeId()));

  addTo(root, getTag("ComponentEligibleFlag",deal.getMtgProd().getComponentEligibleFlag()));
  //To Retrive the Component Type ID
  addTo(root, "ComponentTypeId",String.valueOf(deal.getMtgProd().getComponentTypeId()));
  //artf753276 based on this Keep or remove the code starts*******************//
  //To Retrive the Component Type Description
  String componentType = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ComponentType", deal.getMtgProd().getComponentTypeId() , lang);
  addTo(root, "ComponentType",componentType);
  //To Retrive the Under Writer Type Description
  addTo(root, extracUnderWriteAs());
//artf753276**************//
  /**************MCM Impl team changes - 1-Aug-2008 - Ends - XS_16.23 ****/

  		//4.4 Submission agent...
		addTo(root, extractSource2Name());
		addTo(root, extractSource2Address());
		addTo(root, extractSource2Phone());
		addTo(root, extractSource2Fax());
        
        addTo(root, extractLicenseNumber());
        addTo(root, extractLicense2Number());
        addTo(root, extractWaiverDate());

    }

    //Changes for Enhancement --ravi

    protected Node extractIndexAdjustment()
    {
      String val = null;

      try
      {
    	  //Ticket 616--start//
          //val = formatCurrency(deal.getInterestOverTerm());
          val = formatInterestRate(deal.getPrimeBaseAdj());
    	  //Ticket 616--end//

          if ((Integer.parseInt(val)) < 0)
        	val="-"+val;
      }
      catch (Exception e){}

      return getTag("IndexAdjustment", val);
    }


    protected Node extractFeesSectionEnhancement()
    {
        Node elemNode = null;
        Node lineNode;
        String val;

        try
        {
            List fees = (List)deal.getDealFees();
           if (fees.isEmpty())
            {
               return null;
            }
            Iterator i = fees.iterator();
            while (i.hasNext())
            {
              DealFee df = (DealFee)i.next();
               if (elemNode == null)
                {
                   elemNode = doc.createElement("Fees");
                 }
                lineNode = doc.createElement("Fee");

                //  Fee Type
                //val = PicklistData.getDescription("FeeType", df.getFee().getFeeTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", df.getFee().getFeeTypeId() , lang);

                addTo(lineNode, "Type", val);
                //  Amount
                addTo(lineNode, "Amount", formatCurrency(df.getFeeAmount()));

                //  Status
                //val = PicklistData.getDescription("FeeStatus", df.getFeeStatusId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeStatus", df.getFeeStatusId() , lang);
                addTo(lineNode, "Status", val);
                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){      }

        return elemNode;
    }




    protected Node extractTotalEscrowPaymentEnhancement()
    {

      Node elemNode = null;
      Node lineNode;
      String val;
      double Totamt = 0;
      try
      {
        Collection col = deal.getEscrowPayments();
          if (col.isEmpty())
          {
             return null;
          }
          Iterator it = col.iterator();

          while (it.hasNext())
          {
        	EscrowPayment ep = (EscrowPayment)it.next();
              if (elemNode == null)
              {
                 elemNode = doc.createElement("EscrowDetails");
              }
              lineNode = doc.createElement("Escrow");
              //  Fee Type
              //ALERT:BX:ZR Release2.1.docprep
              val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "EscrowType", ep.getEscrowTypeId() , lang);
              addTo(lineNode, "Type", val);

              //  Amount

              Totamt = Totamt+ep.getEscrowPaymentAmount();
              addTo(lineNode, "Amount", formatCurrency(ep.getEscrowPaymentAmount()));
              addTo(lineNode, "Description",ep.getEscrowPaymentDescription());
              //  Status
              //val = PicklistData.getDescription("FeeStatus", df.getFeeStatusId());
              //ALERT:BX:ZR Release2.1.docprep
              //val = BXResources.getPickListDescription("FeeStatus", df.getFeeStatusId() , lang);

              //addTo(lineNode, "Status", val);

              elemNode.appendChild(lineNode);
          }
          addTo(elemNode,"TotalEscrowPayment",String.valueOf(formatCurrency(Totamt)));
      }
      catch (Exception e){
        e.printStackTrace();
      }

      return elemNode;
    }


    protected Node extractBankName()
    {
        String val = "";

        try
        {
            val = deal.getBankName();

        }
        catch (Exception e){}

        return getTag("BankName", val);
    }

    protected Node extractBankTransitNumber()
    {
        String val = "";

        try
        {

           BranchProfile bp = new BranchProfile(srk, deal.getBranchProfileId());
           //if((!(deal.getBankABANumber()).equals(null))&&(!(deal.getCMHCBranchTransitNumber()).equals(null) ))

	   //Ticket 616--#9 start//
           //val = deal.getBankABANumber()+"-"+ bp.getCMHCBranchTransitNumber();
	   if (deal.getBankABANumber() != null) {
		val = deal.getBankABANumber();
	   }
	   //Ticket 616--#9 end//

           //else
        	 //val=

        }
        catch (Exception e){}

        return getTag("BankTransitNumber", val);
    }

    protected Node extractBankAccountNumber()
    {
        String val = "";

        try
        {
            val = deal.getBankAccountNumber();

        }
        catch (Exception e){}

        return getTag("BankAccountNumber", val);
    }




    protected Node extractnulltag()
    {

        return getTag("NullTag", "");
    }


    protected Node extractIndexRate()
    {
        String val = null;

        try
        {

        	//Ticket 616--start//
            //val = formatCurrency(deal.getInterestOverTerm());
            val = formatInterestRate(deal.getPrimeIndexRate());
        	//Ticket 616--end//
        }
        catch (Exception e){}

        return getTag("IndexRate", val);
    }


    protected Node extractCashbackAmount()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getCashBackAmount());

        }
        catch (Exception e){}

        return getTag("CashbackAmount", val);
    }



    //Changes for Bank Details Ravi
    protected Node extractOtherDetailsbank(){

      Node elemNode = null;
      int ppid=0;
      PartyProfile ppObj;
      String bankAddr=" ";
      elemNode = doc.createElement("PartyProfile");
      try
      {
    	Collection party = deal.getPartyProfiles();
    	Iterator it = party.iterator();
    	while(it.hasNext())
    	{
    	  ppObj = (PartyProfile)it.next();
    	  if(deal.getDealId()==ppObj.getDealId())
    	  ppid=ppObj.getPartyProfileId();
    	  ppObj = new PartyProfile(srk,ppid);
          //List party =
    	  if(ppObj==null)
    		return null;
    	  Node lineNode = doc.createElement("Parties");
    	  addTo(lineNode, "partyType", ppObj.getPartyName());

    	  String partyDesc = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PartyType", ppObj.getPartyTypeId() , lang);
    	  addTo(lineNode, "partyName", partyDesc);

    	  addTo(lineNode, "BusinessID", ppObj.getPtPBusinessId());

    	  addTo(lineNode, "CompanyName", ppObj.getPartyCompanyName());
        Contact appContact = ppObj.getContact();
        Addr addr = appContact.getAddr();
        String address = "";

        if (!isEmpty(addr.getAddressLine1()))
           address += addr.getAddressLine1();
        if (!isEmpty(addr.getAddressLine2()))
           address += format.space() + addr.getAddressLine2();
       if (!isEmpty(addr.getCity()))
           address += format.space() + addr.getCity();
        if (!isEmpty(addr.getPostalFSA()))
           address += format.space() + addr.getPostalFSA();
       if (!isEmpty(addr.getPostalLDU()))
           address += format.space() + addr.getPostalLDU();

        Province prov = new Province(srk, addr.getProvinceId());

        address += format.space() + prov.getProvinceName();

        addTo(lineNode, "PartyAddress", address);
       String var2="";
     var2 = ppObj.getBankName();
    if(ppObj.getBankAccNum()==null)
    {
      if(var2==null)
      {
    	bankAddr="";
      }
      else
      {
    	bankAddr=var2;
      }
    }
    else
    {
      if(var2==null)
      {
    	bankAddr=ppObj.getBankAccNum();
      }
      else
      {
    	bankAddr=ppObj.getBankAccNum()+" "+var2;
      }
    }
    addTo(lineNode, "BankAddress", bankAddr);
       elemNode.appendChild(lineNode);
    	}

    }catch(Exception e){  }
    return elemNode;
    }


    protected Node extractBorrowerName()
    {
        String val = null;

        try
        {
            Borrower b = getPrimaryBorrower();

            String firstname = b.getBorrowerFirstName();
            String mi = b.getBorrowerMiddleInitial();
            String lastname = b.getBorrowerLastName();
            String suffix = BXResources.getPickListDescription(srk
                    .getExpressState().getDealInstitutionId(),
                    "SUFFIX", b.getSuffixId(), lang);

            if (!isEmpty(firstname) && !isEmpty(lastname))
            {
                val = firstname;

                if (!isEmpty(mi))
                   val += format.space() + mi;

                val += format.space() + lastname;
                
                if (!isEmpty(suffix))
                    val += format.space() + suffix;
            }
        }
        catch (Exception e){}

        return getTag("BorrowerName", val);
    }

    protected Node extractDealStatus()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("Status", deal.getStatusId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Status", deal.getStatusId() , lang);
        }
        catch (Exception e){}

        return getTag("DealStatus", val);
    }

    protected Node extractDealStatusDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getStatusDate());
        }
        catch (Exception e){}

        return getTag("DealStatusDate", val);
    }

    protected Node extractSourceFirm()
    {
        String val = null;

        try
        {
            SourceFirmProfile sf = new SourceFirmProfile(srk, deal.getSourceFirmProfileId());
            val = sf.getSourceFirmName();
        }
        catch (Exception e){}

        return getTag("SourceFirm", val);
    }

    protected Node extractSource()
    {
        String val = null;

        try
        {
            SourceOfBusinessProfile sob =
                new SourceOfBusinessProfile(srk, deal.getSourceOfBusinessProfileId());

            val = sob.getSOBPShortName();
        }
        catch (Exception e){}

        return getTag("Source", val);
    }

    protected Node extractLOB()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("LineOfBusiness", deal.getLineOfBusinessId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LineOfBusiness", deal.getLineOfBusinessId() , lang);
        }
        catch (Exception e){}

        return getTag("LOB", val);
    }

    protected Node extractPaymentTerm()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("PaymentTerm", deal.getPaymentTermId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentTerm", deal.getPaymentTermId() , lang);
        }
        catch (Exception e){}

        return getTag("PaymentTerm", val);
    }


    protected Node extractActualPaymentTerm()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getInstance().getYearsAndMonths(deal.getActualPaymentTerm(), lang);
        }
        catch (Exception e){}

        return getTag("ActualPaymentTerm", val);
    }

    protected Node extractSpecialFeature()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("SpecialFeature", deal.getSpecialFeatureId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "SpecialFeature", deal.getSpecialFeatureId() , lang);
        }
        catch (Exception e){}

        return getTag("SpecialFeature", val);
    }

    protected Node extractApplicantDetailsSection() {
        Node elemNode = null;

        try
        {
            List applicants = getBorrowers();

            if (applicants.isEmpty())
               return null;

            Collections.sort(applicants, new PrimaryBorrowerComparator());
            Iterator i = applicants.iterator();
            String name, sal, val;

            elemNode = doc.createElement("ApplicantDetails");

            while (i.hasNext())
            {
                Borrower b = (Borrower)i.next();

                Node lineNode = doc.createElement("Applicant");

                //  Applicant Name
                name = new String();

                //sal = PicklistData.getDescription("Salutation", b.getSalutationId());
                //ALERT:BX:ZR Release2.1.docprep
                sal = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Salutation", b.getSalutationId() , lang);

                if (sal != null)
                   name += sal;

                if (!isEmpty(b.getBorrowerFirstName()))
                   name += format.space() + b.getBorrowerFirstName();

                if (!isEmpty(b.getBorrowerMiddleInitial()))
                   name += format.space() + b.getBorrowerMiddleInitial();

                if (!isEmpty(b.getBorrowerLastName()))
                   name += format.space() + b.getBorrowerLastName();
                
                String suffix = BXResources.getPickListDescription(srk
                        .getExpressState().getDealInstitutionId(),
                        "SUFFIX", b.getSuffixId(), lang);
                
                if (!isEmpty(suffix))
                    name += format.space() + suffix;

                addTo(lineNode, "Name", name);

                //  Applicant Type
                //val = PicklistData.getDescription("BorrowerType", b.getBorrowerTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "BorrowerType", b.getBorrowerTypeId() , lang);

                addTo(lineNode, "Type", val);

                //  PrimaryBorrower
                addTo(lineNode, "PrimaryBorrower", b.isPrimaryBorrower() ? "Y" : "N");

                //  Net Worth
                addTo(lineNode, "NetWorth", formatCurrency(b.getNetWorth()));

                //  Date of birth
                val = formatDate(b.getBorrowerBirthDate());

                addTo(lineNode, "DateOfBirth", val);

                //  Marital Status
                //val = PicklistData.getDescription("MaritalStatus", b.getMaritalStatusId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MaritalStatus", b.getMaritalStatusId() , lang);

                addTo(lineNode, "MaritalStatus", val);

                //  SIN
                addTo(lineNode, getTag("SIN", StringUtil.formatSIN(b.getSocialInsuranceNumber())));

                //  Citizenship status
                //val = PicklistData.getDescription("CitizenshipType", b.getCitizenshipTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "CitizenshipType", b.getCitizenshipTypeId() , lang);

                addTo(lineNode, "CitizenshipStatus", val);

                //  # of dependants
                addTo(lineNode, "NumDependants", Integer.toString(b.getNumberOfDependents()));

                //  Existing client?
                val = b.isExistingClient() ? Xc.YES_LABEL: Xc.NO_LABEL;

                addTo(lineNode, "ExistingClient", BXResources.getGenericMsg(val, lang));

                //  Client Reference Num
                addTo(lineNode, getTag("ClientReferenceNum", b.getClientReferenceNumber()));

                //  # of times bankrupt
                addTo(lineNode, "TimesBankrupt", Integer.toString(b.getNumberOfTimesBankrupt()));

                //  Bankruptcy status
                //val = PicklistData.getDescription("BankruptcyStatus", b.getBankruptcyStatusId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "BankruptcyStatus", b.getBankruptcyStatusId() , lang);

                addTo(lineNode, "BankruptcyStatus", val);

                //  Staff of Lender
                addTo(lineNode, "StaffOfLender", b.getStaffOfLender() ? "Y" : "N");

                //  Home Phone
                if (b.getBorrowerHomePhoneNumber() != null)
                {
                    val = StringUtil.getAsFormattedPhoneNumber(b.getBorrowerHomePhoneNumber(), null);

                    addTo(lineNode, getTag("HomePhone", val));
                }

                //  Work Phone
                if (b.getBorrowerWorkPhoneNumber() != null)
                {
                    val = StringUtil.getAsFormattedPhoneNumber(b.getBorrowerWorkPhoneNumber(),
                                                           b.getBorrowerWorkPhoneExtension());

                    addTo(lineNode, getTag("WorkPhone", val));
                }
                
                //  Cell Phone
                if (b.getBorrowerCellPhoneNumber() != null)
                {
                    val = StringUtil.getAsFormattedPhoneNumber(b.getBorrowerCellPhoneNumber(), null);

                    addTo(lineNode, getTag("CellPhone", val));
                }
                
                //  Fax
                if (b.getBorrowerFaxNumber() != null)
                {
                    val = StringUtil.getAsFormattedPhoneNumber(b.getBorrowerFaxNumber(), null);

                    addTo(lineNode, getTag("Fax", val));
                }

                //  Email
                addTo(lineNode, getTag("EmailAddress", b.getBorrowerEmailAddress()));

                //  Prefer contact method
                val = BXResources.getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "PREFMETHODOFCONTACT", b
                        .getPrefContactMethodId(), lang);
                addTo(lineNode, "PreferredContactMethod", val);
                
                //  Language preference
                //val = PicklistData.getDescription("LanguagePreference", b.getLanguagePreferenceId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LanguagePreference", b.getLanguagePreferenceId() , lang);

                addTo(lineNode, "LanguagePreference", val);

                //  Addresses
                addTo(lineNode, extractApplicantAddressSection(b));

                //  Employment Info
                addTo(lineNode, extractApplicantEmploymentSection(b));

                //  Assets section
                addTo(lineNode, extractAssetsSection(b));

                //  Liabilities section
                addTo(lineNode, extractLiabilitiesSection(b));

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){   e.printStackTrace();     }

        return elemNode;
    }

    protected Node extractApplicantAddressSection(Borrower b)
    {
        Node elemNode = null;

        try
        {
            List addresses = (List)b.getBorrowerAddresses();

            if (addresses.isEmpty())
               return null;

            Collections.sort(addresses, new CurrentBorrowerAddressComparator());
            Iterator i = addresses.iterator();
            String val;

            elemNode = doc.createElement("Addresses");

            while (i.hasNext())
            {
                BorrowerAddress ba = (BorrowerAddress)i.next();

                Addr addr = ba.getAddr();

                Node lineNode = doc.createElement("Address");

                addTo(lineNode, "Line1", addr.getAddressLine1());
                addTo(lineNode, "Line2", addr.getAddressLine2());
                addTo(lineNode, "City", addr.getCity());

                Province prov = new Province(srk, addr.getProvinceId());

                addTo(lineNode, "Province", prov.getProvinceName());

                //  PostalCode
                addTo(lineNode, extractPostalCode(addr));

                //  Time at residence
                val = DocPrepLanguage.getInstance().getYearsAndMonths(ba.getMonthsAtAddress(), lang);

                addTo(lineNode, "TimeAtResidence", val);

                //  Borrower Address Status
                //val = PicklistData.getDescription("BorrowerAddressType",
                //    ba.getBorrowerAddressTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "BorrowerAddressType", ba.getBorrowerAddressTypeId() , lang);

                addTo(lineNode, "Status", val);

                //  Residential Status
                //val = PicklistData.getDescription("ResidentialStatus",
                //    ba.getResidentialStatusId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ResidentialStatus", ba.getResidentialStatusId() , lang);

                addTo(lineNode, getTag("ResidentialStatusID", val));

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractApplicantEmploymentSection(Borrower b)
    {
        Node elemNode = null;

        try
        {
            List ehs = (List)b.getEmploymentHistories();

            if (ehs.isEmpty())
               return null;

            Collections.sort(ehs, new CurrentEmploymentComparator());
            Iterator i = ehs.iterator();
            String val;
            Contact c;
            Addr addr;

            elemNode = doc.createElement("EmploymentHistory");

            while (i.hasNext())
            {
                EmploymentHistory eh = (EmploymentHistory)i.next();

                Node lineNode = doc.createElement("Employment");

                addTo(lineNode, "EmployerName", eh.getEmployerName());

                //val = PicklistData.getDescription("EmploymentHistoryStatus",
                //    eh.getEmploymentHistoryStatusId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "EmploymentHistoryStatus", eh.getEmploymentHistoryStatusId() , lang);

                addTo(lineNode, "EmploymentStatus", val);

                //val = PicklistData.getDescription("EmploymentHistoryType",
                //    eh.getEmploymentHistoryTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "EmploymentHistoryType", eh.getEmploymentHistoryTypeId() , lang);

                addTo(lineNode, "EmploymentType", val);

                c = eh.getContact();

                if (c != null)
                {
                    if (c.getContactPhoneNumber() != null)
                    {
                        val = StringUtil.getAsFormattedPhoneNumber(c.getContactPhoneNumber(),
                                                           c.getContactPhoneNumberExtension());

                        addTo(lineNode, "EmployerWorkPhone", val);
                    }

                    if (c.getContactFaxNumber() != null)
                    {
                        val = StringUtil.getAsFormattedPhoneNumber(c.getContactFaxNumber(), null);

                        addTo(lineNode, "EmployerFax", val);
                    }

                    addTo(lineNode, "EmployerEmailAddress", c.getContactEmailAddress());

                    addr = c.getAddr();

                    if (addr != null)
                    {
                        addTo(lineNode, "EmployerMailingAddressLine1", addr.getAddressLine1());
                        addTo(lineNode, "EmployerMailingAddressLine2", addr.getAddressLine2());
                        addTo(lineNode, "EmployerCity", addr.getCity());

                        //  Province
                        Province prov = new Province(srk, addr.getProvinceId());

                        addTo(lineNode, "EmployerProvince", prov.getProvinceName());

                        //  Postal Code
                        addTo(lineNode, extractPostalCode(addr, "EmployerPostalCode"));
                    }

                    //  Occupation
                    //val = PicklistData.getDescription("Occupation", eh.getOccupationId());
                    //ALERT:BX:ZR Release2.1.docprep
                    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Occupation", eh.getOccupationId() , lang);

                    addTo(lineNode, "Occupation", val);

                    //  Industry Sector
                    //val = PicklistData.getDescription("IndustrySector", eh.getIndustrySectorId());
                    //ALERT:BX:ZR Release2.1.docprep
                    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "IndustrySector", eh.getIndustrySectorId() , lang);

                    addTo(lineNode, "IndustrySector", val);

                    //  Time at job
                    val = DocPrepLanguage.getInstance().getYearsAndMonths(eh.getMonthsOfService(), lang);

                    addTo(lineNode, "TimeAtJob", val);

                    // 6-Oct-04, Catherine for DJ deal summary --------
                    //  Job title
                    if (eh.getJobTitleId() != 0) {
                      val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "JobTitle",  eh.getJobTitleId(), lang);
                    } else {
                      val = eh.getJobTitle();
                    }
                    addTo(lineNode, "JobTitle", val);
                    // 6-Oct-04, Catherine for DJ deal summary end --------

                    //  Income Section
                    addIncomeTo(lineNode, eh.getIncome());
                }

                elemNode.appendChild(lineNode);
            }

            //  Other income section
            addTo(elemNode, extractOtherIncomeSection(b));
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractOtherIncomeSection(Borrower b)
    {
        Node elemNode = null;

        try
        {
            List incomes = (List)new Income(srk,null).findByOtherIncome((BorrowerPK)b.getPk());

            if (incomes.isEmpty())
               return null;

            elemNode = doc.createElement("OtherIncome");
            Iterator i = incomes.iterator();
            Node lineNode;

            while (i.hasNext())
            {
                Income inc = (Income)i.next();
                lineNode = doc.createElement("Line");

                addIncomeTo(lineNode, inc);

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractAssetsSection(Borrower b)
    {
        Node elemNode = null;

        try
        {
            List assets = (List)b.getAssets();

            if (assets.isEmpty())
               return null;

            elemNode = doc.createElement("Assets");
            Iterator i = assets.iterator();
            Node lineNode;
            String val;
            double total = 0d;

            while (i.hasNext())
            {
                Asset asset = (Asset)i.next();
                lineNode = doc.createElement("Asset");

                //  Asset type
                //val = PicklistData.getDescription("AssetType", asset.getAssetTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "AssetType", asset.getAssetTypeId() , lang);

                addTo(lineNode, "AssetType", val);

                //  Asset description
                addTo(lineNode, "AssetDescription", makeSafeXMLString(asset.getAssetDescription()));

                //  Asset value
                addTo(lineNode, "AssetValue", formatCurrency(asset.getAssetValue()));

                //  % included in net worth
                val = formatRatio((double)asset.getPercentInNetWorth());

                addTo(lineNode, "PercentIncludedInNetWorth", val);

                total += asset.getAssetValue();

                elemNode.appendChild(lineNode);
            }

            addTo(elemNode, "Total", formatCurrency(total));
        }
        catch (Exception e){}

        return elemNode;
    }

    //  -------------- Catherine for DJ #620 --------------
    // extracts only non-employment related income
    protected Node extractOtherIncome(Borrower b){
      Node elemNode = null;

      try
      {
          List incomes = (List)b.getIncomes();

          if (incomes.isEmpty())
            return null;

          elemNode = doc.createElement("OtherIncomes");
          Iterator i = incomes.iterator();
          Node lineNode;
          String val;

          IncomeType lIncomeType;

          int nodeCnt = 0;
          while (i.hasNext())
          {
              Income inc = (Income)i.next();
              lIncomeType = new IncomeType( srk, inc.getIncomeTypeId() );

              if( lIncomeType.getEmploymentRelated().equalsIgnoreCase("n") )
              {
                  nodeCnt++;
                  lineNode = doc.createElement("Income");

                  val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "IncomeType", inc.getIncomeTypeId() , lang);

                  addTo(lineNode, "IncomeType", val);

                  //  Income description
                  addTo(lineNode, "IncomeDescription", makeSafeXMLString(inc.getIncomeDescription()));

                  //  Income amount
                  addTo(lineNode, "AnnualIncomeAmount", formatCurrency(inc.getAnnualIncomeAmount()));

                  elemNode.appendChild(lineNode);
              }
          }
          if (nodeCnt == 0){
            return null;
          }
      }
      catch (Exception e){}

      return elemNode;
    }

    protected Node extractLiabilitiesSection(Borrower b)
    {
        Node elemNode = null;

        try
        {
            List liabilities = (List)b.getLiabilities();

            if (liabilities.isEmpty())
               return null;

            elemNode = doc.createElement("Liabilities");
            Iterator i = liabilities.iterator();
            Node lineNode;
            String val;
            double totalAmount = 0d;
            double monthlyAmount = 0d;

            //==================================================================
            //  Changed Oct 16, 2002 (John S) to resolve the following issue:
            //
            //  Issue Description:
            //
            //    Change the Liability Section of the Deal Summary to display
            //    1.If a mossys property set to 'Y'
            //
            //    only select the live liablilities (PercentOutGDS <2. )
            //
            //    2.If a mossys property set to 'N'
            //
            //    select all liability records but sort the records by PercentOutGDS)
            //    and only add to the section totals if PercentOutGDS <2.
            //==================================================================
            boolean bShowLiveOnly =
                PropertiesCache.getInstance().
                    getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.show.live.liabilities.only", "N").
                        equalsIgnoreCase("Y");

            if (!bShowLiveOnly)
                Collections.sort(liabilities, new LiabilityPercentOutGDSComparator());

            while (i.hasNext())
            {
                Liability lib = (Liability)i.next();

                if (!bShowLiveOnly ||
                    (bShowLiveOnly && lib.getPercentOutGDS() < 2))
                {
                    lineNode = doc.createElement("Liability");

                    //  Liability type
                    //val = PicklistData.getDescription("LiabilityType", lib.getLiabilityTypeId());
                    //ALERT:BX:ZR Release2.1.docprep
                    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LiabilityType", lib.getLiabilityTypeId() , lang);

                    addTo(lineNode, "LiabilityType", val);

                    
                    if(lib.getCreditBureauRecordIndicator() != null)
                    {
                        String cbFlag = BXResources
                                .getGenericMsg(
                                        ((("Y").equalsIgnoreCase(lib
                                                .getCreditBureauRecordIndicator()) ? Xc.YES_LABEL
                                                : Xc.NO_LABEL)), lang);
    
                        addTo(lineNode, "CB", cbFlag);
                    }
                    
                    //  Liability description
                    addTo(lineNode, "LiabilityDescription", makeSafeXMLString(lib.getLiabilityDescription()));
                    
                    //  Liability limit
                    addTo(lineNode, "LiabilityLimit", formatCurrency(lib.getCreditLimit()));

                    //  Liability amount
                    addTo(lineNode, "LiabilityAmount", formatCurrency(lib.getLiabilityAmount()));

                    //  Liability monthly payment
                    addTo(lineNode, "LiabilityMonthlyPayment", formatCurrency(lib.getLiabilityMonthlyPayment()));

                    //  Liability marutity date
                    addTo(lineNode, "LiabilityMarutityDate", formatDate(lib.getMaturityDate()));
                    
                    //  % included in GDS
                    addTo(lineNode, "PercentIncludedInGDS", formatRatio(lib.getPercentInGDS()));

                    //  % included in TDS
                    addTo(lineNode, "PercentIncludedInTDS", formatRatio(lib.getPercentInTDS()));

                    //  Liability payoff type id
                    addTo(lineNode, "LiabilityPayoffTypeId", "" + lib.getLiabilityPayOffTypeId());                     // pvcs #958, Catherine

                    //  Liability payoff type
                    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LiabilityPayoffType", lib.getLiabilityPayOffTypeId() , lang);
                    addTo(lineNode, "LiabilityPayoffType", val);

                    addTo(lineNode, "LiabilityIncludeInGDS", lib.getIncludeInGDS() ? "Y" : "N" );                      // pvcs #958, Catherine
                    addTo(lineNode, "LiabilityIncludeInTDS", lib.getIncludeInTDS() ? "Y" : "N");                       // pvcs #958, Catherine

                    if (bShowLiveOnly ||
                        (!bShowLiveOnly && lib.getPercentOutGDS() < 2))
                    {
                        totalAmount += lib.getLiabilityAmount();
                        monthlyAmount += lib.getLiabilityMonthlyPayment();
                    }

                    elemNode.appendChild(lineNode);
                }
            }

            addTo(elemNode, "TotalAmount", formatCurrency(totalAmount));
            addTo(elemNode, "TotalMonthly", formatCurrency(monthlyAmount));
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractPropertiesSection()
    {
        Node elemNode = null;

        try
        {
            List c = (List)deal.getProperties();

            if (c.isEmpty())
               return null;

            Collections.sort(c, new PrimaryPropertyComparator());

            Iterator i = c.iterator();
            String val;
            Node lineNode;

            elemNode = doc.createElement("Properties");

            while (i.hasNext())
            {
                Property p =(Property)i.next();

                lineNode = doc.createElement("Property");

                //  Primary property
                addTo(lineNode, "PrimaryProperty", BXResources.getGenericMsg((p
                        .isPrimaryProperty() ? Xc.YES_LABEL : Xc.NO_LABEL),
                        lang));

                //  Address Line 1
                String add1 = "";

                if( ! isEmpty(p.getUnitNumber() ) ) {
                  add1 += p.getUnitNumber();
                  add1 += "-";
                }
                if (!isEmpty(p.getPropertyStreetNumber()))
                   add1 += p.getPropertyStreetNumber();

                // Tracker #2800 - Sasa: Format of French and English addresses is different
                if( lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) {
                	if (!isEmpty(p.getPropertyStreetName()))    add1 += format.space() + p.getPropertyStreetName();
                	if (!isEmpty(p.getStreetTypeDescription())) add1 += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",  p.getStreetTypeId() , lang);
                }
                else {
                	if (!isEmpty(p.getStreetTypeDescription())) add1 += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",  p.getStreetTypeId() , lang);
                	if (!isEmpty(p.getPropertyStreetName()))    add1 += format.space() + p.getPropertyStreetName();
                }
                // End of Tracker #2800 - Sasa

                if (p.getStreetDirectionId() != 0)
                   //ALERT:BX:ZR Release2.1.docprep
                   add1 += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection", p.getStreetDirectionId() , lang);

                addTo(lineNode, "AddressLine1", add1);

                //  Address line 2
                addTo(lineNode, "AddressLine2", p.getPropertyAddressLine2());

                //  Address city
                addTo(lineNode, "City", p.getPropertyCity());

                //  Address province
                Province prov = new Province(srk, p.getProvinceId());
                addTo(lineNode, "Province", prov.getProvinceName());

                //  Address postal code
                addTo(lineNode, extractPostalCode(p));

                //  Legal lines
                addLegalLines(lineNode, p);

                //  Dwelling type
                //val = PicklistData.getDescription("DwellingType", p.getDwellingTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DwellingType", p.getDwellingTypeId() , lang);

                addTo(lineNode, "DwellingType", val);

                //  Dwelling style
                //val = PicklistData.getDescription("DwellingStyle", p.getDwellingStyleId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DwellingStyle", p.getDwellingStyleId() , lang);

                addTo(lineNode, "DwellingStyle", val);

                //  Builder Name
                addTo(lineNode, "BuilderName", p.getBuilderName());

                //  Garage Size
                //val = PicklistData.getDescription("GarageSize", p.getGarageSizeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "GarageSize", p.getGarageSizeId() , lang);

                addTo(lineNode, "GarageSize", val);

                //  Garage Type
                //val = PicklistData.getDescription("GarageType", p.getGarageTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "GarageType", p.getGarageTypeId() , lang);

                addTo(lineNode, "GarageType", val);

                //  New construction
                //val = PicklistData.getDescription("NewConstruction", p.getNewConstructionId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "NewConstruction", p.getNewConstructionId() , lang);

                addTo(lineNode, "NewConstruction", val);

                //  Structure age
                addTo(lineNode, "StructureAge", Integer.toString(p.getStructureAge()));

                //  # of units
                addTo(lineNode, "NumUnits", Integer.toString(p.getNumberOfUnits()));

                //  # of bedrooms
                addTo(lineNode, "NumBedrooms", Integer.toString(p.getNumberOfBedrooms()));

                //  Living space
                //val = PicklistData.getDescription("LivingSpaceUnitOfMeasure", p.getLivingSpaceUnitOfMeasureId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LivingSpaceUnitOfMeasure", p.getLivingSpaceUnitOfMeasureId() , lang);

                if (!isEmpty(val))
                {
                    val = Integer.toString(p.getLivingSpace()) + format.space() + val;

                    addTo(lineNode, "LivingSpace", val);
                }

                //  Lot size
                //val = PicklistData.getDescription("LotSizeUnitOfMeasure", p.getLotSizeUnitOfMeasureId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LotSizeUnitOfMeasure", p.getLotSizeUnitOfMeasureId() , lang);

                if (!isEmpty(val))
                {
                    val = Integer.toString(p.getLotSize()) + format.space() + val;

                    addTo(lineNode, "LotSize", val);
                }

                //  Heat type
                //val = PicklistData.getDescription("HeatType", p.getHeatTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "HeatType", p.getHeatTypeId() , lang);

                addTo(lineNode, "HeatingType", val);

                //  UFFI insulation?
                addTo(lineNode, "UFFIInsulation", BXResources.getGenericMsg((p.getInsulatedWithUFFI() ? Xc.YES_LABEL: Xc.NO_LABEL), lang));

                //  Water type
                //val = PicklistData.getDescription("WaterType", p.getWaterTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "WaterType", p.getWaterTypeId() , lang);

                addTo(lineNode, "Water", val);

                //  Sewage type
                //val = PicklistData.getDescription("SewageType", p.getSewageTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "SewageType", p.getSewageTypeId() , lang);

                addTo(lineNode, "Sewage", val);

                //  MLS
                addTo(lineNode, "MLS", p.getMLSListingFlag());

                //  Property usage
                //val = PicklistData.getDescription("PropertyUsage", p.getPropertyUsageId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PropertyUsage", p.getPropertyUsageId() , lang);

                addTo(lineNode, "PropertyUsage", val);

                //  Occupancy type
                addTo(lineNode, extractOccupancyType(p));

                //  Property type
                addTo(lineNode, extractPropertyType(p));

                //  Property location
                //val = PicklistData.getDescription("PropertyLocation", p.getPropertyLocationId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PropertyLocation", p.getPropertyLocationId() , lang);

                addTo(lineNode, "PropertyLocation", val);

                //  Property zoning
                //val = PicklistData.getDescription("PropertyZoning", p.getZoning());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PropertyZoning", p.getZoning() , lang);

                addTo(lineNode, "Zoning", val);

                //  Purchase Price
                addTo(lineNode, extractPurchasePrice(p));

                //  Land value
                addTo(lineNode, "LandValue", formatCurrency(p.getLandValue()));

                //  Equity available
                addTo(lineNode, "EquityAvailable", formatCurrency(p.getEquityAvailable()));

                //  Estimated value
                addTo(lineNode, "EstimatedValue", formatCurrency(p.getEstimatedAppraisalValue()));

                //  Actual appraised value
                addTo(lineNode, extractActualAppraisalValue(p));

                //  Appraisal date
                addTo(lineNode, extractAppraisalDate(p));

                //  Appraisal source
                //val = PicklistData.getDescription("AppraisalSource", p.getAppraisalSourceId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "AppraisalSource", p.getAppraisalSourceId() , lang);

                addTo(lineNode, "AppraisalSource", val);

                //  Appraiser Information
                try
                {
                    PartyProfile pp = new PartyProfile(srk);
                    List lp = (List)pp.findByPropertyDealAndType(p, Mc.PARTY_TYPE_APPRAISER);

                    pp = (PartyProfile)lp.get(0);

                    if (!isEmpty(pp.getPartyName()))
                       addTo(lineNode, "AppraiserName", pp.getPartyName());

                    Contact appContact = pp.getContact();
                    Addr addr = appContact.getAddr();
                    String address = "";

                    if (!isEmpty(addr.getAddressLine1()))
                       address += addr.getAddressLine1();

                    if (!isEmpty(addr.getAddressLine2()))
                       address += format.space() + addr.getAddressLine2();

                    if (!isEmpty(addr.getCity()))
                       address += format.space() + addr.getCity();

                    if (!isEmpty(addr.getPostalFSA()))
                       address += format.space() + addr.getPostalFSA();

                    if (!isEmpty(addr.getPostalLDU()))
                       address += format.space() + addr.getPostalLDU();

                    prov = new Province(srk, addr.getProvinceId());

                    address += format.space() + prov.getProvinceName();

                    addTo(lineNode, "AppraiserAddress", address);

                    if (appContact.getContactPhoneNumber() != null)
                    {
                        val = StringUtil.getAsFormattedPhoneNumber(
                                appContact.getContactPhoneNumber(),
                                appContact.getContactPhoneNumberExtension());

                        addTo(lineNode, "AppraiserPhone", val);
                    }

                    if (appContact.getContactFaxNumber() != null)
                    {
                        val = StringUtil.getAsFormattedPhoneNumber(
                                appContact.getContactFaxNumber(), null);

                        addTo(lineNode, "AppraiserFax", val);
                    }
                }
                catch (Exception e) {}

                //  Property Expenses
                addTo(lineNode, extractExpensesForProperty(p));

                //------> Rel 3.1 - Sasa
                addTo(lineNode, "tenureType", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "TenureType", p.getTenureTypeId(), lang));
                addTo(lineNode, "MIEnergyEfficiency", p.getMiEnergyEfficiency());
                addTo(lineNode, "subdivisionDiscount", p.getSubdivisionDiscount());
                addTo(lineNode, "onReserveTrustAgreementNumber", p.getOnReserveTrustAgreementNumber());
                //------> End of Rel 3.1 - Sasa

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractExpensesForProperty(Property p)
    {
        Node elemNode = null;

        try
        {
            List expenses = (List)p.getPropertyExpenses();

            if (expenses.isEmpty())
               return null;

            elemNode = doc.createElement("PropertyExpenses");
            Iterator i = expenses.iterator();
            Node lineNode;
            String val;

            while (i.hasNext())
            {
                PropertyExpense exp = (PropertyExpense)i.next();
                lineNode = doc.createElement("Expense");

                //  Expense type
                //val = PicklistData.getDescription("PropertyExpenseType", exp.getPropertyExpenseTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PropertyExpenseType", exp.getPropertyExpenseTypeId() , lang);

                addTo(lineNode, "ExpenseType", val);

                //  Expense description
                val = exp.getPropertyExpenseDescription();

                addTo(lineNode, "ExpenseDescription", val);

                //  Expense period
                //val = PicklistData.getDescription("PropertyExpensePeriod", exp.getPropertyExpensePeriodId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PropertyExpensePeriod", exp.getPropertyExpensePeriodId() , lang);

                addTo(lineNode, "ExpensePeriod", val);

                //  Amount
                addTo(lineNode, "ExpenseAmount", formatCurrency(exp.getPropertyExpenseAmount()));

                //  % included in GDS
                val = formatRatio(exp.getPePercentInGDS());

                addTo(lineNode, "PercentIncludedInGDS", val);

                //  % included in TDS
                val = formatRatio(exp.getPePercentInTDS());

                addTo(lineNode, "PercentIncludedInTDS", val);

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected boolean addIncomeTo(Node parentNode, Income inc)
    {
        if (parentNode == null || inc == null)
           return false;

        try
        {
            //  Type
            //String val = PicklistData.getDescription("IncomeType", inc.getIncomeTypeId());
            //ALERT:BX:ZR Release2.1.docprep
            String val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "IncomeType", inc.getIncomeTypeId() , lang);

            addTo(parentNode, "IncomeType", val);

            //  Description
            addTo(parentNode, "IncomeDescription", inc.getIncomeDescription());

            //  Period
            //val = PicklistData.getDescription("IncomePeriod", inc.getIncomePeriodId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "IncomePeriod", inc.getIncomePeriodId() , lang);

            addTo(parentNode, "IncomePeriod", val);

            //  Amount
            val = formatCurrency(inc.getIncomeAmount());
            addTo(parentNode, "IncomeAmount", val);

            // --- 6-Oct-04
            // Catherine for DJ: the income extracted in the previous line might be monthly or annual
            // (period is specified as incomePeriod),
            // that was mistakenly used in Deal Summary Lite in place of Annual Income
            // now we are going to use annualIncomeAmount there
            val = formatCurrency(inc.getAnnualIncomeAmount());
            addTo(parentNode, "annualIncomeAmount", val);
            // --- 6-Oct-04 end

            //  % included in GDS
            val = formatRatio(inc.getIncPercentInGDS());

            addTo(parentNode, "PercentIncludedInGDS", val);

            //  % included in TDS
            val = formatRatio(inc.getIncPercentInTDS());

            addTo(parentNode, "PercentIncludedInTDS", val);

            return true;
        }
        catch (Exception e)  {}

        return false;
    }

    protected Node extractRateCode()
    {
        String val = null;

        try
        {
            int prInvId = deal.getPricingProfileId();
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory(prInvId);

            val = pp.getRateCode();
        }
        catch (Exception e){}

        return getTag("RateCode", val);
    }

    protected Node extractRateDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getRateDate());
        }
        catch (Exception e){}

        return getTag("RateDate", val);
    }

    protected Node extractPostedInterestRate()
    {
        String val = null;

        try
        {
            val = formatInterestRate(deal.getPostedRate());
        }
        catch (Exception e){}

        return getTag("PostedInterestRate", val);
    }

    protected Node extractRateLockedIn()
    {
        String val = null;

        try
        {
            val = deal.getRateLock();
        }
        catch (Exception e){}

        return getTag("RateLockedIn", val);
    }

    protected Node extractDiscount()
    {
        String val = null;

        try
        {
            val = formatInterestRate(deal.getDiscount());
        }
        catch (Exception e){}

        return getTag("Discount", val);
    }

    protected Node extractPremium()
    {
        String val = null;

        try
        {
            val = formatInterestRate(deal.getPremium());
        }
        catch (Exception e){}

        return getTag("Premium", val);
    }

    protected Node extractBuydownRate()
    {
        String val = null;

        try
        {
            val = formatInterestRate(deal.getBuydownRate());
        }
        catch (Exception e){}

        return getTag("BuydownRate", val);
    }

    protected Node extractPandIPayment()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getPandiPaymentAmount());
        }
        catch (Exception e){}

        return getTag("PandIPayment", val);
    }

    protected Node extractAdditionalPrincipalPayment()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getAdditionalPrincipal());
        }
        catch (Exception e){}

        return getTag("AdditionalPrincipalPayment", val);
    }

    protected Node extractTotalEscrowPayment()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getEscrowPaymentAmount());
        }
        catch (Exception e){}

        return getTag("TotalEscrowPayment", val);
    }

    protected Node extractSecondMortgage()
    {
        String val = null;

        try
        {
            val = deal.isSecondaryFinancing() ? Xc.YES_LABEL: Xc.NO_LABEL;
        }
        catch (Exception e){}

        return getTag("SecondMortgage", BXResources.getGenericMsg(val, lang));
    }


    protected Node extractInterestAdjustmentRate()
    {
        String val = null;

        try
        {
            val =(formatDate(deal.getInterimInterestAdjustmentDate()));
        }
        catch (Exception e){}

        return getTag("InterestAdjustmentRate", val);
    }


    protected Node extractEquityAvailable()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getCombinedTotalEquityAvailable());
        }
        catch (Exception e){}

        return getTag("EquityAvailable", val);
    }

    protected Node extractPrepaymentOption()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("PrePaymentOptions", deal.getPrePaymentOptionsId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PrePaymentOptions", deal.getPrePaymentOptionsId() , lang);
        }
        catch (Exception e){}

        return getTag("PrePaymentOption", val);
    }

    protected Node extractReferenceType()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("ReferenceDealType", deal.getReferenceDealTypeId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ReferenceDealType", deal.getReferenceDealTypeId() , lang);
        }
        catch (Exception e){}

        return getTag("ReferenceType", val);
    }

    protected Node extractPrivilegePaymentOption()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("PrivilegePayment", deal.getPrivilegePaymentId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PrivilegePayment", deal.getPrivilegePaymentId() , lang);
        }
        catch (Exception e){}

        return getTag("PrivilegePaymentOption", val);
    }

    protected Node extractCrossSell()
    {
        String val = null;

        try
        {
            CrossSellProfile csp = new CrossSellProfile(srk, deal.getCrossSellProfileId());

            //FXP24068
            //val = csp.getCSDescription();
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "CROSSSELLPROFILE", csp.getCrossSellProfileId(), lang);
        }
        catch (Exception e){}

        return getTag("CrossSell", val);
    }

    protected Node extractSourceAddress()
    {
        String val = null;

        try
        {
            SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();

            Contact contact = sobp.getContact();
            Addr addr = contact.getAddr();

            val = new String();

            if (!isEmpty(addr.getAddressLine1()))
               val += addr.getAddressLine1();

            if (!isEmpty(addr.getCity()))
               val += format.space() + addr.getCity();

            Province prov = new Province(srk, addr.getProvinceId());

            val += format.space() + prov.getProvinceAbbreviation();
        }
        catch (Exception e){}

        return getTag("SourceAddress", val);
    }

    protected Node extractTotalPurchasePrice()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getTotalPurchasePrice());
        }
        catch (Exception e){}

        return getTag("TotalPurchasePrice", val);
    }

    protected Node extractTotalEstimatedValue()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getTotalEstAppraisedValue());
        }
        catch (Exception e){}

        return getTag("TotalEstimatedValue", val);
    }

    protected Node extractTotalActualAppraisedValue()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getTotalActAppraisedValue());
        }
        catch (Exception e){}

        return getTag("TotalActualAppraisedValue", val);
    }

    protected Node extractLender()
    {
        String val = null;

        try
        {
            val = deal.getLenderProfile().getLPShortName();
        }
        catch (Exception e){}

        return getTag("Lender", val);
    }

    protected Node extractInvestor()
    {
        String val = null;

        try
        {
            InvestorProfile ip = new InvestorProfile(srk, deal.getInvestorProfileId());

            val = ip.getIPShortName();
        }
        catch (Exception e){}

        return getTag("Investor", val);
    }

    protected Node extractBridgeDetailsSection()
    {
        Node elemNode = null;

        try
        {
            Bridge br = new Bridge(srk,null);
            br = br.findByDeal((DealPK)deal.getPk());

            if (br == null || br.getBridgeId() == 0)
               return null;

            elemNode = doc.createElement("BridgeDetails");

            //  Purpose
            //String val = PicklistData.getDescription("BridgePurpose", br.getBridgePurposeId());
            //ALERT:BX:ZR Release2.1.docprep
            String val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "BridgePurpose", br.getBridgePurposeId() , lang);

            addTo(elemNode, "Purpose", val);

            //  Lender
            LenderProfile lp = new LenderProfile(srk, br.getLenderProfileId());

            addTo(elemNode, "Lender", lp.getLenderName());

            //  Product
            addTo(elemNode, extractProduct());

            //  Payment term
            //val = PicklistData.getDescription("PaymentTerm", br.getPaymentTermId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources .getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentTerm", br.getPaymentTermId() , lang);

            addTo(elemNode, "PaymentTerm", val);

            //  Rate code
            int prInvId = br.getPricingProfileId();
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory(prInvId);

            val = pp.getRateCode();

            addTo(elemNode, "RateCode", val);

            //  Posted interest rate
            val = formatInterestRate(br.getPostedRate());

            addTo(elemNode, "PostedInterestRate", val);

            //  Rate date
            val = formatDate(br.getRateDate());

            addTo(elemNode, "RateDate", val);

            //  Discount
            addTo(elemNode, "Discount", formatInterestRate(br.getDiscount()));

            //  Premium
            addTo(elemNode, "Premium", formatInterestRate(br.getPremium()));

            //  Net rate
            val = formatInterestRate(br.getNetInterestRate());

            addTo(elemNode, "NetRate", val);

            //  Loan amount
            val = formatCurrency(br.getNetLoanAmount());

            addTo(elemNode, "LoanAmount", val);

            //  Maturity date
            val = formatDate(br.getMaturityDate());

            addTo(elemNode, "MaturityDate", val);
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractDownPaymentsSection()
    {
        Node elemNode = null;
        Node lineNode;
        String val;

        try
        {
            List sources = (List)deal.getDownPaymentSources();

            if (sources.isEmpty())
               return null;

            elemNode = doc.createElement("DownPayments");
            Iterator i = sources.iterator();
            double amt = 0;

            while (i.hasNext())
            {
                DownPaymentSource dps = (DownPaymentSource)i.next();

                lineNode = doc.createElement("DownPayment");

                //  Source
                val = PicklistData.getDescription("DownPaymentSourceType", dps.getDownPaymentSourceTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DownPaymentSourceType", dps.getDownPaymentSourceTypeId() , lang);

                addTo(lineNode, "Source", val);

                //  Description
                addTo(lineNode, "Description", dps.getDPSDescription());

                //  Amount
                addTo(lineNode, "Amount", formatCurrency(dps.getAmount()));

                elemNode.appendChild(lineNode);

                amt += dps.getAmount();
            }
            addTo(elemNode, "Total", formatCurrency(amt));
            addTo(elemNode, "RequiredDownPayment",
               formatCurrency(deal.getRequiredDownpayment()));
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractCreditScore()
    {
        String val = null;

        try
        {
            Borrower b = getPrimaryBorrower();
            val = Integer.toString(b.getCreditScore());
        }
        catch (Exception e){}

        return getTag("CreditScore", val);
    }

    protected Node extractCombinedGDS()
    {
        String val = null;

        try
        {
            val = formatRatio(deal.getCombinedGDS());
        }
        catch (Exception e){}

        return getTag("CombinedGDS", val);
    }

    // ---------------------- pvcs #1065 (1), change for DJ_DealSummaryLite by Catherine, 08-Mar-05 ----------

    protected Node extractCombinedGDSBorrower()
    {
        String val = null;

        try
        {
            val = formatRatio(deal.getCombinedGDSBorrower());
        }
        catch (Exception e){}

        return getTag("CombinedGDSBorrower", val);
    }

    protected Node extractCombinedTDSBorrower()
    {
        String val = null;

        try
        {
            val = formatRatio(deal.getCombinedTDSBorrower());
        }
        catch (Exception e){}

        return getTag("CombinedTDSBorrower", val);
    }

    // ---------------------- pvcs #1065 (1), change for DJ_DealSummaryLite by Catherine, 08-Mar-05 end ----------

    protected Node extractCombined3YrGDS()
    {
        String val = null;

        try
        {
            val = formatRatio(deal.getCombinedGDS3Year());
        }
        catch (Exception e){}

        return getTag("Combined3YrGDS", val);
    }

    protected Node extractCombinedTDS()
    {
        String val = null;

        try
        {
            val = formatRatio(deal.getCombinedTDS());
        }
        catch (Exception e){}

        return getTag("CombinedTDS", val);
    }

    protected Node extractCombined3YrTDS()
    {
        String val = null;

        try
        {
            val = formatRatio(deal.getCombinedTDS3Year());
        }
        catch (Exception e){}

        return getTag("Combined3YrTDS", val);
    }

    protected Node extractCombinedTotalIncome()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getCombinedTotalIncome());
        }
        catch (Exception e){}

        return getTag("CombinedTotalIncome", val);
    }

    protected Node extractCombinedTotalAssets()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getCombinedTotalAssets());
        }
        catch (Exception e){}

        return getTag("CombinedTotalAssets", val);
    }

    protected Node extractCombinedTotalLiabilities()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getCombinedTotalLiabilities());
        }
        catch (Exception e){}

        return getTag("CombinedTotalLiabilities", val);
    }

    protected String formatName(Contact c)
    {
        String name = "";

        if (c == null)
           return null;

        if (!isEmpty(c.getContactLastName()))
           name += c.getContactLastName();

        if (!isEmpty(c.getContactFirstName()))
        {
            if (name.trim().length() > 0)
               name += "," + format.space();

            name += c.getContactFirstName().charAt(0) + ".";
        }
        return name;
    }

    // Venkata - FXP18053 - NBC CR022 - Deal summary changes - begin
    protected String getFullName(Contact c)
    {
        String name = "";

        if (c == null)
           return null;

        if (!isEmpty(c.getContactLastName()))
           name += c.getContactLastName();

        if (!isEmpty(c.getContactFirstName()))
        {
            if (name.trim().length() > 0)
               name += "," + format.space();

            name += c.getContactFirstName();
        }
        return name;
    }
    // Venkata - FXP18053 - NBC CR022 - Deal summary changes - end

    protected Node extractUnderwriter()
    {
        try
        {
            UserProfile up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(),
                                                           deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            return getTag("Underwriter", formatName(c));
        }
        catch(Exception exc){}

        return null;
    }

    protected Node extractFinalApprover()
    {
        try
        {
            UserProfile up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getFinalApproverId(),
                                                           deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            return getTag("FinalApprover", formatName(c));
        }
        catch(Exception exc){}

        return null;
    }

    protected Node extractJointApprover()
    {
        Node elemNode = null;

        try
        {
            UserProfile up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getJointApproverId(),
                                                           deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            return getTag("JointApprover", formatName(c));
        }
        catch(Exception exc){}

        return elemNode;
    }

    protected Node extractAdministrator()
    {
        Node elemNode = null;

        try
        {
            UserProfile up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(),
                                                           deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            return getTag("Administrator", formatName(c));
        }
        catch(Exception exc){}

        return elemNode;
    }

    protected Node extractFunder()
    {
        Node elemNode = null;

        try
        {
            UserProfile up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getFunderProfileId(),
                                                           deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            return getTag("Funder", formatName(c));
        }
        catch(Exception exc){}

        return elemNode;
    }

    protected Node extractApprover(int approverNumber)
    {
        if (approverNumber < 1 || approverNumber > 5)
           return null;

        String tagName;

        try
        {
            UserProfile up = new UserProfile(srk);
      int iProfileId;

            switch (approverNumber)
            {
                case 2: iProfileId = deal.getSecondApproverId();
                        tagName = "SecondApprover";
                        break;

                case 3: iProfileId = deal.getThirdApproverId();
                        tagName = "ThirdApprover";
                        break;

                case 4: iProfileId = deal.getFourthApproverId();
                        tagName = "FourthApprover";
                        break;

                case 5: iProfileId = deal.getFifthApproverId();
                        tagName = "FifthApprover";
                        break;

                //  default to first approver
                default:
                        iProfileId = deal.getUnderwriterUserId();
                        tagName = "FirstApprover";
                        break;

            }

            up = up.findByPrimaryKey(new UserProfileBeanPK(iProfileId, deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            return getTag(tagName, formatName(c));
        }
        catch(Exception exc){}

        return null;
    }

    protected Node extractFeesSection()
    {
        Node elemNode = null;
        Node lineNode;
        String val;

        try
        {
            List fees = (List)deal.getDealFees();

            if (fees.isEmpty())
               return null;

            Iterator i = fees.iterator();

            while (i.hasNext())
            {
                DealFee df = (DealFee)i.next();

                if (df.getFee().getFeeTypeId() != Mc.FEE_TYPE_ORIGINATION_FEE &&
                    df.getFee().getFeeTypeId() != Mc.FEE_TYPE_AGENT_REFERRAL_FEE &&
                    df.getFee().getFeeTypeId() != Mc.FEE_TYPE_APPLICATION_FEE)
                    continue;

                if (elemNode == null)
                   elemNode = doc.createElement("Fees");

                lineNode = doc.createElement("Fee");

                //  Fee Type
                //val = PicklistData.getDescription("FeeType", df.getFee().getFeeTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", df.getFee().getFeeTypeId() , lang);

                addTo(lineNode, "Type", val);

                //  Amount
                addTo(lineNode, "Amount", formatCurrency(df.getFeeAmount()));

                //  Status
                //val = PicklistData.getDescription("FeeStatus", df.getFeeStatusId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeStatus", df.getFeeStatusId() , lang);

                addTo(lineNode, "Status", val);

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractConditionsSection()
    {
        Node elemNode = null;

        try
        {
            List dts = (List) deal.getDocumentTracks();

            if (dts.isEmpty())
               return null;

            boolean bShowRole =
                PropertiesCache.getInstance().
                    getProperty(deal.getInstitutionProfileId(),"com.basis100.conditions.show.responsibilityrole.tag", "Y").
                        equalsIgnoreCase("Y");

            //  Sort by Requested first, then alphabetical after that...
            Collections.sort(dts, new DocumentStatusComparator());

            Iterator i = dts.iterator();
            int sourceId, condTypeId, statusId, roleid;
            String role;
            Node condition;
            ConditionParser p = new ConditionParser();

            while (i.hasNext())
            {
                DocumentTracking dt = (DocumentTracking)i.next();
                Condition cond = new Condition(srk, dt.getConditionId());

                condTypeId = cond.getConditionTypeId();
                sourceId = dt.getDocumentTrackingSourceId();
                statusId = dt.getDocumentStatusId();
                roleid = dt.getDocumentResponsibilityRoleId();

                //role = PicklistData.getDescription("ConditionResponsibilityRole", roleid);
                //ALERT:BX:ZR Release2.1.docprep
                role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", roleid , lang);

                if ((sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED ||
                     sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED ||
                     sourceId   == Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM) &&
                    (condTypeId == Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING ||
                     condTypeId == Mc.CONDITION_TYPE_COMMITMENT_ONLY) &&
                    (roleid == Mc.CRR_PARTY_TYPE_APPRAISER ||
                     roleid == Mc.CRR_PARTY_TYPE_SOLICITOR ||
                     roleid == Mc.CRR_SOURCE_OF_BUSINESS ||
                     roleid == Mc.CRR_USER_TYPE_APPLICANT))
                {
                    if (statusId == Mc.DOC_STATUS_REQUESTED ||
                        statusId == Mc.DOC_STATUS_RECEIVED ||
                        statusId == Mc.DOC_STATUS_INSUFFICIENT ||
                        statusId == Mc.DOC_STATUS_CAUTION||
                        statusId == Mc.DOC_STATUS_WAIVED)
                    {
                        if (elemNode == null)
                           elemNode = doc.createElement("Conditions");

                        condition = doc.createElement("Condition");

                        String conditionsOut = dt.getDocumentTextByLanguage(lang);
                        conditionsOut = p.parseText(conditionsOut,lang,deal,srk);

                        //  Index handled in template now...
                        //val = index++ + "." + format.space() + conditionsOut;

                        if (bShowRole)
                           conditionsOut += format.space() + "(" + role + ")";

                        //  The following line makes FO-derived documents work when there
                        //  are special characters in there (&).  However, if you enable it
                        //  now, the RTF doc will show the XML character sequence instead of
                        //  the intended character.  (Example, & shows up as &amp;).
                        condition = convertCRToLines(condition, makeSafeXMLString(conditionsOut));
                        //condition = convertCRToLines(condition, conditionsOut);

                        elemNode.appendChild(condition);
                    }
                }
            }
        }
        catch (Exception e) {}

        return elemNode;
    }

    protected Node extractNotesSection()
    {
        Node elemNode = null;
        Node lineNode;
        String val;

        try
        {
            List notes = (List)deal.getDealNotes();

            if (notes.isEmpty())
               return null;

            //Collections.sort(notes, new NotesComparator());
            Iterator i = notes.iterator();

            UserProfile up = new UserProfile(srk);
            String user;

            elemNode = doc.createElement("Notes");
            Node textNode;

            while (i.hasNext())
            {
                DealNotes dn = (DealNotes)i.next();

                lineNode = doc.createElement("Note");

                //  Date
                val = formatDate(dn.getDealNotesDate());

                addTo(lineNode, "Date", val);

                //  User
                try
                {
                	// 27/Aug/2007 Venkata - commented and corrected as follows to get the UserProfile
                    //up = up.findByContactId(dn.getDealNotesUserId());
                    up = up.findByPrimaryKey(new UserProfileBeanPK(dn.getDealNotesUserId(),
                                                                   deal.getInstitutionProfileId()));
                    if (up != null)
                    {
                        user = formatName(up.getContact());

                        addTo(lineNode, "User", user);
                        addTo(lineNode, "UserFullName", getFullName(up.getContact()));
                    }
                }
                catch (FinderException e) {}
                // Venkata - FXP18053 - NBC CR022 - Deal summary changes - begin
                // Deal Notes Category
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DEALNOTESCATEGORY", dn.getDealNotesCategoryId() , lang);
                addTo(lineNode, "DealNotesCategory", val);
                // Venkata - FXP18053 - NBC CR022 - Deal summary changes - end
                //  Text

                textNode = doc.createElement("Text");

                lineNode.appendChild(convertCRToLines(textNode, dn.getDealNotesText()));

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractMortageInsuranceInformation()
    {
        Node elemNode = null;
        String val = null;

        try
        {
            elemNode = doc.createElement("MI");

            //  MI Indicator
            //val = PicklistData.getDescription("MIIndicator", deal.getMIIndicatorId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MIIndicator", deal.getMIIndicatorId() , lang);

            addTo(elemNode, "Indicator", val);

            //  MI Status
            //val = PicklistData.getDescription("MIStatus", deal.getMIStatusId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MIStatus", deal.getMIStatusId() , lang);

            addTo(elemNode, "Status", val);

            //  MI Existing Policy Number
            addTo(elemNode, "ExistingPolicyNumber", deal.getMIExistingPolicyNumber());

            //  MI Certificate #
            addTo(elemNode, "CertificateNum", deal.getMIPolicyNumber());

            //  Prequalification MI Num
            addTo(elemNode, "PrequalificationMINum", deal.getPreQualificationMICertNum());

            //  MI Type
            //val = PicklistData.getDescription("MortgageInsuranceType", deal.getMITypeId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MortgageInsuranceType", deal.getMITypeId() , lang);

            addTo(elemNode, "Type", val);

            //  MI Insurer
            //val = PicklistData.getDescription("MortgageInsurer", deal.getMortgageInsurerId());
            //ALERT:BX:ZR Release2.1.docprep
            //val = BXResources.getPickListDescription("MortgageInsurer", deal.getMortgageInsurerId() , lang);
            // Mortgageinsurer table exists in the database, it is a picklist but we do not have it's
            // counterpart in the BXResource. Mortgage insurance carrier should be used.
            // message posted by Zivko (July 25, 2003)
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MortgageInsuranceCarrier", deal.getMortgageInsurerId() , lang);

            addTo(elemNode, "Insurer", val);

            //  MI Payor
            //val = PicklistData.getDescription("MortgageInsurancePayor", deal.getMIPayorId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MortgageInsurancePayor", deal.getMIPayorId() , lang);

            addTo(elemNode, "Payor", val);

            //  MI Upfront
            addTo(elemNode, "Upfront", deal.getMIUpfront());

            //  MI Premium
            addTo(elemNode, "Premium", formatCurrency(deal.getMIPremiumAmount()));

            //  MI RU Intervention
            addTo(elemNode, "RUIntervention", deal.getMIRUIntervention());

            //------> Rel 3.1 - Sasa
            addTo(elemNode, "progressAdvanceType", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ProgressAdvanceType", deal.getProgressAdvanceTypeId(), lang));
            addTo(elemNode, "progressAdvanceInspectionBy", deal.getProgressAdvanceInspectionBy());
            addTo(elemNode, "selfDirectedRRSP", deal.getSelfDirectedRRSP());
            addTo(elemNode, "cmhcProductTrackerIdentifier", deal.getCmhcProductTrackerIdentifier());
            addTo(elemNode, "locRepaymentType", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LocRepaymentType", deal.getLocRepaymentTypeId(), lang));
            addTo(elemNode, "locAmortizationMonths", Integer.toString(deal.getLocAmortizationMonths()));
            addTo(elemNode, "locInterestOnlyMaturityDate", formatDate(deal.getLocInterestOnlyMaturityDate()));
            addTo(elemNode, "requestStandardService", deal.getRequestStandardService());
            //------> End of Rel 3.1 - Sasa
        }
        catch(Exception exc){}

        return elemNode;
    }
    /**
     * Description : This Method Returns Equity Take out Related elements Node
     * for a deal
     * @version 1.0 Initial Vesrion
     * @version 1.1 XS_16.23 01-Aug-2008 Added ExistingMtgNumber,Product Type,
     *          Additional Information Elements
     * @return element Node of Equity Take Out
     */
    protected Node extractEquityTakeOutSection()
    {
        Node elemNode = null;

        try
        {
            elemNode = doc.createElement("Refi");

            //  Refi Original Purchase Date
            addTo(elemNode, "OriginalPurchaseDate", formatDate(deal.getRefiOrigPurchaseDate()));

            //  Refi Mortgage Holder
            addTo(elemNode, "MortgageHolder", deal.getRefiCurMortgageHolder());

            //  Refi Original Purchase Price
            addTo(elemNode, "PurchasePrice", formatCurrency(deal.getRefiOrigPurchasePrice()));

            //  Refi Improvement Value
            addTo(elemNode, "ImprovementValue", formatCurrency(deal.getRefiImprovementAmount()));

            //  Refi Blended Amortization
            addTo(elemNode, "BlendedAmortization", deal.getRefiBlendedAmortization());

            //  Refi Purpose
            addTo(elemNode, "Purpose", deal.getRefiPurpose());

            //  Refi Original Mortgage Amount
            addTo(elemNode, "OriginalMtgAmount", formatCurrency(deal.getRefiOrigMtgAmount()));

            //  Existing Loan Amount
            addTo(elemNode, "ExistingLoanAmount", formatCurrency(deal.getExistingLoanAmount()));

            //  Refi Improvement Description
            addTo(elemNode, "ImproveDesc", deal.getRefiImprovementsDesc());

            /******MCM IMpl Team V1.1  XS_16.23 01-08-2008 Starts *****/
            //Refi Existing Mortgage Number
            addTo(elemNode, "ExistingMtgNumber", deal.getRefExistingMtgNumber());

            //Refi Product Type
            addTo(elemNode, "ProductType", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ProductType", deal.getRefiProductTypeId(), lang));

            //Refi Additional Information

            addTo(elemNode, "AdditionalInformation", deal.getRefiAdditionalInformation());

            /******MCM IMpl Team V1.1 XS_16.23 01-08-2008 Ends *****/


        }
        catch(Exception exc){}

        return elemNode;
    }

    protected Node extractProgressAdvance()
    {
        String val = null;

        try
        {
            val = deal.getProgressAdvance();
        }
        catch(Exception exc){}

        return getTag("ProgressAdvance", val);
    }

    protected Node extractAdvanceToDateAmount()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getAdvanceToDateAmount());
        }
        catch(Exception exc){}

        return getTag("AdvanceToDateAmount", val);
    }

    protected Node extractNextAdvanceAmount()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getNextAdvanceAmount());
        }
        catch(Exception exc){}

        return getTag("NextAdvanceAmount", val);
    }

    protected Node extractAdvanceNumber()
    {
        String val = null;

        try
        {
            val = Integer.toString(deal.getAdvanceNumber());
        }
        catch(Exception exc){}

        return getTag("AdvanceNumber", val);
    }

    protected Node extractFinancingProgram()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("FinanceProgram", deal.getFinanceProgramId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FinanceProgram", deal.getFinanceProgramId() , lang);
        }
        catch(Exception exc){}

        return getTag("FinancingProgram", val);
    }

    protected Node extractCreditBureauReport()
    {
        Node node = null;

        try
        {
            List reports = (List)deal.getCreditBureauReports();

            Iterator i = reports.iterator();
            CreditBureauReport cbr;

            while( i.hasNext() )
            {
                if (node == null)
                   node = doc.createElement("CreditBureauReport");

                cbr = (CreditBureauReport)i.next();

                if (!isEmpty(cbr.getCreditReport()))
                {
                    node = convertCRToLines(node, makeSafeXMLString(cbr.getCreditReport()));

                    //  Will essentially add a new line when parsed by XSL
                    node.appendChild(doc.createElement("Line"));
                }
            }
        }
        catch (Exception e){}

        return node;
    }

    protected Node extractBusinessRulesSection()
    {
        Node elemNode = null;

        try
        {
            Vector prefVect = new java.util.Vector();
            HashMap globMap = new java.util.HashMap();

            PassiveMessage pm = new PassiveMessage();
            PassiveMessage borrPm = new PassiveMessage();
            PassiveMessage propPm = new PassiveMessage();
            pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
            //BusinessRuleExecutor brExec = new BusinessRuleExecutor();
            PagelessBusinessRuleExecutor plBrExec = new PagelessBusinessRuleExecutor();

            DealPK dealpk = (DealPK)deal.getPk();
            prefVect.addElement("DE-%");
            prefVect.addElement("DI-%");
            prefVect.addElement("IC-%");
            prefVect.addElement("UN-%");
            prefVect.addElement("MI-%");
            globMap.put("CurrentBorrowerId", new Integer(-1));
            globMap.put("CurrentPropertyId", new Integer(-1));

            UserProfileBeanPK upbPK = new UserProfileBeanPK( deal.getUnderwriterUserId(),
                                                             deal.getInstitutionProfileId());
            UserProfile up = new UserProfile(srk);

            try
            {
                up = up.findByPrimaryKey(upbPK);
            }
            catch(RemoteException rExc)
            {
                throw new Exception(this.getClass().getName() +
                      "Remote exception while trying to find user profile.\n + " +
                      rExc.getMessage() );
            }
            catch(FinderException fExc)
            {
                throw new Exception(this.getClass().getName() +
                      "Finder Exception exception while trying to find user profile.\n" +
                      fExc.getMessage());
            }

            try
            {
                globMap.put( "UserTypeId", new Integer(up.getUserTypeId()) );
            }
            catch(Exception exc)
            {
                throw new Exception(this.getClass().getName() +
                   ":(1) ERROR- Could not create userTypeId.\n" + exc.getMessage() );
            }
            // validate(DealPK pk, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs, HashMap globPairs)
            //plBrExec.validate(dealpk,srk,pm,prefVect,globMap);
            plBrExec.validate(deal, srk, pm, prefVect, globMap, lang );

            // System.out.println("Dealid " + deal.getDealId() + " Copy :" + deal.getCopyId());
            // brExec.BREValidator(dealpk, srk, pm,"DI-%") ;
            // brExec.BREValidator(dealpk, srk, pm,"DE-%") ;
            // brExec.BREValidator(dealpk, srk, pm,"IC-%") ;
            // brExec.BREValidator(dealpk, srk, pm,"UN-%") ;
            // brExec.BREValidator(dealpk, srk, pm,"MI-%") ;
            // borrower related business rules
            Collection borrCol = deal.getBorrowers();
            Iterator itBor = borrCol.iterator();
            Borrower lBorrower;
            Property lProperty;
            globMap = new java.util.HashMap();

            while( itBor.hasNext() )
            {
                borrPm = new PassiveMessage();
                lBorrower = (Borrower)itBor.next();
                pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
                //pm.addMsg(Sc.PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM + lBorrower.formFullName(),pm.INFO);
                pm.addMsg( BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM",lang) + lBorrower.formFullName(),pm.INFO);
                pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
                globMap.put("CurrentBorrowerId", new Integer( lBorrower.getBorrowerId() ));
                globMap.put("CurrentPropertyId", new Integer(-1));

                try
                {
                    globMap.put( "UserTypeId", new Integer(up.getUserTypeId()) );
                }
                catch(Exception exc)
                {
                    throw new Exception(this.getClass().getName() +
                       ":(2) ERROR- Could not create userTypeId.\n" + exc.getMessage() );
                }

                prefVect.removeAllElements();
                prefVect.add("DEA-%");
                //--DJ_LDI_CR--start--//
                prefVect.add("LDI-%");
                //--DJ_LDI_CR--end--//

                //plBrExec.validate(dealpk,srk,borrPm,prefVect,globMap);
                plBrExec.validate(deal,srk,borrPm,prefVect,globMap,lang);
                //brExec.setCurrentProperty(-1);
                //brExec.setCurrentBorrower( lBorrower.getBorrowerId() );
                //PassiveMessage borrPm = brExec.BREValidator(dealpk, srk, null,"DEA-%");
                pm.addAllMessages(borrPm);
            }

            // Property related business rules
            Collection propCol = deal.getProperties();
            Iterator itProp = propCol.iterator();
            globMap = new java.util.HashMap();

            while( itProp.hasNext() )
            {
                propPm = new PassiveMessage();
                lProperty = (Property)itProp.next();
                pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
                //pm.addMsg(Sc.PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM + lProperty.formPropertyAddress(),pm.INFO);
                pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM",lang) + lProperty.formPropertyAddress(),pm.INFO);
                pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
                globMap.put("CurrentBorrowerId", new Integer(-1));
                globMap.put("CurrentPropertyId", new Integer(lProperty.getPropertyId() ));

                try
                {
                    globMap.put( "UserTypeId", new Integer(up.getUserTypeId()) );
                }
                catch(Exception exc)
                {
                    throw new Exception(this.getClass().getName() +
                       ":(3) ERROR- Could not create userTypeId.\n" + exc.getMessage() );
                }

                prefVect.removeAllElements();
                prefVect.add("DEP-%");
                //plBrExec.validate(dealpk,srk,propPm,prefVect,globMap);
                plBrExec.validate(deal,srk,propPm,prefVect,globMap,lang);
                //brExec.setCurrentBorrower(-1);
                //brExec.setCurrentProperty(lProperty.getPropertyId());
                //PassiveMessage propPm = brExec.BREValidator(dealpk, srk, null,"DEP-%");
                pm.addAllMessages(propPm);
            }

            String outMsg;
            Element lineNode;

            for(int i=0; i<pm.getNumMessages(); i++)
            {
                outMsg = (String)pm.getMsgs().elementAt(i);

                //--Release2.1--//
                //// Translation lookup.
                lineNode = doc.createElement("Rule");
                ////lineNode = doc.createElement(BXResources.getGenericMsg("RULE_LABEL", theSession.getLanguageId()));

                if (((Integer)pm.getMsgTypes().elementAt(i)).intValue() == PassiveMessage.CRITICAL)
                {
                    lineNode.setAttribute("Critical", "Y");
                //    outMsg = "C " + outMsg;
                }
                else if (((Integer)pm.getMsgTypes().elementAt(i)).intValue() == PassiveMessage.NONCRITICAL)
                {
                    lineNode.setAttribute("Critical", "N");
                  //  outMsg = "- " + outMsg;
                }

                if (elemNode == null)
                   elemNode = doc.createElement("BusinessRules");

                lineNode.appendChild(doc.createTextNode(outMsg));
                addTo(elemNode, lineNode);
                //val += outMsg;
            }
        }
        catch (Exception e){}

        return elemNode;
     }

    protected Node extractBranchName()
    {
        String val = null;

        try
        {
            BranchProfile b = new BranchProfile(srk, deal.getBranchProfileId());

            //FXP24068
            //val = b.getBranchName();
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "BRANCHPROFILE", deal.getBranchProfileId(), lang);
        }
        catch(Exception exc){}

        return getTag("BranchName", val);
    }

    protected Node extractRequiredDownPaymentAmount() {
        String val = null;
        try {
            val = formatCurrency(deal.getRequiredDownpayment());
        }
        catch (Exception e){}
        return getTag("RequiredDownPaymentAmount", val);
    }

    /**
     * Rel 3.1 - Sasa
     * @return productType from productTypeId as Node
     */
    protected Node extractProductType() {
    	String val = null;
    	try{
    		val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ProductType", deal.getProductTypeId(), lang);
    	}
    	catch(Exception e){}
    	return getTag("productType", val);
    }

	protected Node exAlternativeID() {
		String returnValue = "";
		try {
			SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();
			returnValue = sobp.getAlternativeId();
		} catch (FinderException e) {
			srk.getSysLogger().warning(this.getClass(), "NBCDataProvider.exAlternativeID() failed to retrieve alternative ID: " + e.getMessage());
		} catch (RemoteException e) {
			srk.getSysLogger().warning(this.getClass(), "NBCDataProvider.exAlternativeID() failed to retrieve alternative ID: " + e.getMessage());
		}
		return getTag("alternativeID", returnValue);
	}

	protected Node exSOBBusinessID() {
		String returnValue = "";
		try {
			SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();
			returnValue = sobp.getSOBPBusinessId();
		} catch (FinderException e) {
			srk.getSysLogger().warning(this.getClass(), "NBCDataProvider.exSOBBusinessID() failed to retrieve SOBBusinessID: " + e.getMessage());
		} catch (RemoteException e) {
			srk.getSysLogger().warning(this.getClass(), "NBCDataProvider.exSOBBusinessID() failed to retrieve SOBBusinessID: " + e.getMessage());
		}
		return getTag("SOBBusinessID", returnValue);
	}

	protected Node exBestRate() {
		String returnValue = "";
		PricingRateInventory prInven;
		try {
			prInven = deal.getPricingRateInventory();
			returnValue = formatInterestRate(prInven.getBestRate());
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting BestRate"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return getTag("bestRate", returnValue);
	}

	/**
	 * If the PricingRateInventory.getMaximumDiscountAllowed value is < 0, return "Cap Rate",
	 * otherwise return "Best Rate".
	 *
	 * This method is dependent on the lang variable being set in this class instance.
	 * @return
	 */
	protected Node exBestRateLabel() {
		String returnValue = "";
		PricingRateInventory prInven;
		try {
			prInven = deal.getPricingRateInventory();
			double maxDiscount = prInven.getMaximumDiscountAllowed();
			if (maxDiscount < 0) {
		        switch (lang)
		        {
		            case Mc.LANGUAGE_PREFERENCE_ENGLISH:
		                 returnValue = Mc.DEAL_SUMMARY_CAPRATE_EN;
		                 break;
		            case Mc.LANGUAGE_PREFERENCE_FRENCH:
		                 returnValue = Mc.DEAL_SUMMARY_CAPRATE_FR;
		                 break;
		            default:
		                 break;
		        }
			} else {
		        switch (lang)
		        {
		            case Mc.LANGUAGE_PREFERENCE_ENGLISH:
		                 returnValue = Mc.DEAL_SUMMARY_BESTRATE_EN;
		                 break;
		            case Mc.LANGUAGE_PREFERENCE_FRENCH:
		                 returnValue = Mc.DEAL_SUMMARY_BESTRATE_FR;
		                 break;
		            default:
		                 break;
		        }
			}
		} catch (Exception e) {
			String msg = "NBCDataProvider failed while extracting MaximumDiscountAllowed"
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
		}
		return getTag("bestRateLabel", returnValue);
	}

	protected Node exProductCode() {
		return getTag("productCode", mtgProd.getMPBusinessId());
	}

	protected Node exInterestRateChangeFreq() {
		return getTag("interestRateChangeFreq", String.valueOf((int)pp.getInterestRateChangeFrequency()));
	}

	//Ticket 616--start//
	/**
	 * Add the affiliation program description node
	 */
    protected Node extractAffiliationProgram() {
		String affiliationDesc = null;

		try {
			int affiliationProgramID = deal.getAffiliationProgramId();
			affiliationDesc = getAffiliationDesc(affiliationProgramID);
		} catch (Exception e) {
			srk.getSysLogger().error("GenDealSummary.extractAffiliationProgram: Error occurred - " + e.getMessage());
		}

		return getTag("AffiliationProgram", affiliationDesc);
	}

    /**
     * Given an affiliation program ID, retrieve the affiliation program description
     * @param affiliationID
     * @return String representing the affiliation program description
     */
	private String getAffiliationDesc(int affiliationID) {
		String returnValue = null;
		JdbcExecutor jExec = srk.getJdbcExecutor();
		StringBuffer sqlb = new StringBuffer();
		sqlb
				.append("SELECT AFFILIATIONPROGRAMDESC FROM AFFILIATIONPROGRAM WHERE AFFILIATIONPROGRAMID = ");
		sqlb.append(affiliationID);

		String sql = new String(sqlb);
		int key = 0;
		try {
			key = jExec.execute(sql);
			for (; jExec.next(key);) {
				returnValue = jExec.getString(key, "AFFILIATIONPROGRAMDESC");
				break;
			}
		} catch (Exception e) {
			StackTraceElement[] trace = e.getStackTrace();
			StringBuffer stackTraceBuffer = new StringBuffer();
			for (int i = 0; i < trace.length; i++) {
				stackTraceBuffer.append("\t" + trace[i].toString() + "\n");
			}
			srk.getSysLogger().error(e.toString());
			srk.getSysLogger().error(stackTraceBuffer.toString());
		} finally {
			try {
				if (key != 0) {
					jExec.closeData(key);
				}
			} catch (Exception e) {
				StackTraceElement[] trace = e.getStackTrace();
				StringBuffer stackTraceBuffer = new StringBuffer();
				for (int i = 0; i < trace.length; i++) {
					stackTraceBuffer.append("\t" + trace[i].toString() + "\n");
				}
				srk.getSysLogger().error(e.toString());
				srk.getSysLogger().error(stackTraceBuffer.toString());
			}
		}
		return returnValue;
	}

	// Ticket 616--end//

    private String cleanSpecialChars(String input)
    {
        StringBuffer retBuf = new StringBuffer("");
        char comp1 = 0x2E;
        //char comp2 = 0xC2;
        char comp3 = 0xB6;
        char comp4 = 0x20;

        for (int x=0; x < input.length(); x++)
        {
            char c = input.charAt(x);

            if (c == comp1)
                retBuf.append("\\~");
            else if (c == comp3)
                retBuf.append("\\par ");
            else if (c == comp4)
                retBuf.append("\\~");
            else if (c == ' ')
                retBuf.append("\\~");
            else if (!Character.isISOControl(c))
                retBuf.append(c);
        }

        return retBuf.toString();
    }

    /***************************************************************************
     **************MCM Impl team changes - 31-Jul-2008 - Starts - XS_16.23 /
     **************************************************************************/

    /**
     * Description : This Method Returns Component Details Section, Component
     * Summary Section elements Node
     * @version 1.0 XS_16.23 01-Aug-2008 Initial Vesrion
     * @return element Node of Component Details Section, Component Summary
     *         Section
     */
    protected Node extractComponentDetailsSection()
    {
        Node elemNode = null;
        try
        {
            Node componentNode;
            Node componentSummaryNode ;

            List componentsList = (List)deal.getComponents();

            if (componentsList.isEmpty())
               return null;
            elemNode = doc.createElement("Components");

            // ComponentSummary Information Starts
            ComponentSummary componentSummary = new ComponentSummary(srk, deal.getDealId(), deal.getCopyId());

            componentSummaryNode = doc.createElement("ComponentSummary");
            addTo(componentSummaryNode, "TotalAmount", formatCurrency(componentSummary.getTotalAmount()));
            //UnAllocate Amount
            double unAllocAmount = deal.getTotalLoanAmount() - componentSummary.getTotalAmount();
            addTo(componentSummaryNode, "UnAllocatedAmount", formatCurrency(unAllocAmount));
            addTo(componentSummaryNode, "TotalCashBackAmount", formatCurrency(componentSummary.getTotalCashbackAmount()));
            //ComponentSummary Information Ends

            elemNode.appendChild(componentSummaryNode);

            Iterator componentsItr = componentsList.iterator();
            while (componentsItr.hasNext())
            {
                Component components = (Component)componentsItr.next();

                MtgProdPK mtgPK = new MtgProdPK(components.getMtgProdId());
                MtgProd compMtgProd = new MtgProd(srk, null);
                compMtgProd =  compMtgProd.findByPrimaryKey(mtgPK);

                componentNode = doc.createElement("Component");

                String componentType = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ComponentType", components.getComponentTypeId() , lang);
                //Component Related Information
                addTo(componentNode, "ComponentType", componentType);
                addTo(componentNode, "AdditionalInformation", components.getAdditionalInformation());
                addTo(componentNode, "RepaymentType", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "RepaymentType", components.getRepaymentTypeId() , lang));
                addTo(componentNode, "Product", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MTGPROD", components.getMtgProdId(), lang));
                //FXP24068: addTo(componentNode, "PostedRate", formatRatio(components.getPostedRate()));
                addTo(componentNode, "PostedRate", formatInterestRate(components.getPostedRate()));
                addTo(componentNode, "PaymentTerm" ,BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentTerm", compMtgProd.getPaymentTermId() , lang));
                //Mortgage Component Related Information Starts
                if(components.getComponentTypeId()==Mc.COMPONENT_TYPE_MORTGAGE){
                    ComponentMortgage componentMtg = new ComponentMortgage(srk,components.getComponentId(), components.getCopyId());

                        //Line 2
                    addTo(componentNode, "MortgageAmount", formatCurrency(componentMtg.getMortgageAmount()));
                    addTo(componentNode, "MIPremium", formatCurrency(deal.getMIPremiumAmount()));

                    addTo(componentNode, "AllocateMIPremium", BXResources.getGenericMsg(((("Y").equalsIgnoreCase(componentMtg.getMiAllocateFlag())? Xc.YES_LABEL: Xc.NO_LABEL)), lang));

                    addTo(componentNode, "TotalMortgageAmount", formatCurrency(componentMtg.getTotalMortgageAmount()));
                    addTo(componentNode, "AmortizationPeriod", DocPrepLanguage.getYearsAndMonths(
                            componentMtg.getAmortizationTerm(), lang));

                    //Line 3
                    addTo(componentNode, "EffectiveAmortization", DocPrepLanguage.getYearsAndMonths(
                            componentMtg.getEffectiveAmortizationMonths(), lang));
                    addTo(componentNode, "ActualPaymentTerm",  DocPrepLanguage.getYearsAndMonths(
                            componentMtg.getActualPaymentTerm(), lang));

                   // addTo(componentNode, "PaymentTerm" ,BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentTerm", compMtgProd.getPaymentTermId() , lang));
                    addTo(componentNode, "PrepaymentOption", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PrePaymentOptions", componentMtg.getPrePaymentOptionsId() , lang));
                    //Line 4
                    addTo(componentNode, "PrivilegePaymentOption", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PrivilegePayment", componentMtg.getPrivilegePaymentId() , lang));
                    addTo(componentNode, "PaymentFrequency",BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency", componentMtg.getPaymentFrequencyId() , lang));
                    addTo(componentNode, "CommissionCode", componentMtg.getCommissionCode());
                    //FXP24068: addTo(componentNode, "CashBackPercent", formatRatio(componentMtg.getCashBackPercent()));
                    addTo(componentNode, "CashBackPercent", formatInterestRate(componentMtg.getCashBackPercent()));
                    addTo(componentNode, "CashBackAmount", formatCurrency(componentMtg.getCashBackAmount()));
                    //Line 5
                    addTo(componentNode, "RateLockedIn", BXResources.getGenericMsg(((("Y").equalsIgnoreCase(componentMtg.getRateLock())? Xc.YES_LABEL: Xc.NO_LABEL)), lang));

                    //FXP24068: from formatRatio to formatInterestRate
                    addTo(componentNode, "Discount", formatInterestRate(componentMtg.getDiscount()));
                    addTo(componentNode, "Premium", formatInterestRate(componentMtg.getPremium()));
                    //Line 6
                    addTo(componentNode, "BuyDownRate", formatInterestRate(componentMtg.getBuyDownRate()));
                    addTo(componentNode, "NetRate", formatInterestRate(componentMtg.getNetInterestRate()));
                    addTo(componentNode, "PAndIPayment", formatCurrency(componentMtg.getPAndIPaymentAmount()));
                    addTo(componentNode, "AdditionalPrincipalPayment", formatCurrency(componentMtg.getAdditionalPrincipal()));
                    addTo(componentNode, "AllocateTaxEscrow", BXResources.getGenericMsg(((("Y").equalsIgnoreCase(componentMtg.getPropertyTaxAllocateFlag())? Xc.YES_LABEL: Xc.NO_LABEL)), lang));
                    //Line 7
                    addTo(componentNode, "TaxEscrow", (("Y").equalsIgnoreCase(componentMtg.getPropertyTaxAllocateFlag())?formatCurrency(componentMtg.getPropertyTaxEscrowAmount()):formatCurrency(0.00)));
                    addTo(componentNode, "TotalPayment", formatCurrency(componentMtg.getTotalPaymentAmount()));
                    addTo(componentNode, "HoldBackAmount", formatCurrency(componentMtg.getAdvanceHold()));
                    addTo(componentNode, "RateGuaranteePeriod", String.valueOf(componentMtg.getRateGuaranteePeriod()));
                    addTo(componentNode, "ExistingAccount", BXResources.getGenericMsg(((("Y").equalsIgnoreCase(componentMtg.getExistingAccountIndicator())? Xc.YES_LABEL: Xc.NO_LABEL)), lang));
                    //Line 8
                    addTo(componentNode, "ExistingAccountReference", componentMtg.getExistingAccountNumber());
                    addTo(componentNode, "FirstPaymentDate", formatDate(componentMtg.getFirstPaymentDate()));
                    addTo(componentNode, "MaturityDate", formatDate(componentMtg.getMaturityDate()));

                }
                //Mortgage Component Related Information Ends

                 //Line Of Credit Component Related Information Starts
                else if (components.getComponentTypeId()==Mc.COMPONENT_TYPE_LOC){
                    ComponentLOC componentLoc = new ComponentLOC(srk,components.getComponentId(), components.getCopyId());

                    //Line 2
                    addTo(componentNode, "LOCAmount", formatCurrency(componentLoc.getLocAmount()));
                    addTo(componentNode, "MIPremium", formatCurrency(deal.getMIPremiumAmount()));
                    addTo(componentNode, "AllocateMIPremium", BXResources.getGenericMsg(((("Y").equalsIgnoreCase(componentLoc.getMiAllocateFlag())?Xc.YES_LABEL: Xc.NO_LABEL)), lang));
                    addTo(componentNode, "TotalLOCAmount", formatCurrency(componentLoc.getTotalLocAmount()));
                    addTo(componentNode, "PaymentFrequency",BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency", componentLoc.getPaymentFrequencyId() , lang));
                   //Line 3
                    addTo(componentNode, "CommissionCode", componentLoc.getCommissionCode());
                    addTo(componentNode, "ExistingAccount", BXResources.getGenericMsg(((("Y").equalsIgnoreCase(componentLoc.getExistingAccountIndicator())?Xc.YES_LABEL: Xc.NO_LABEL)), lang));
                    addTo(componentNode, "ExistingAccountReference", componentLoc.getExistingAccountNumber());
                    addTo(componentNode, "FirstPaymentDate", formatDate(componentLoc.getFirstPaymentDate()));
                    //Line 4
                    //FXP24068: from formatRatio to formatInterestRate
                    addTo(componentNode, "Discount", formatInterestRate(componentLoc.getDiscount()));
                    addTo(componentNode, "Premium", formatInterestRate(componentLoc.getPremium()));
                    addTo(componentNode, "NetRate", formatInterestRate(componentLoc.getNetInterestRate()));
                    //Line 5
                    //Todo: add missing pandi paymentamount
                    //Bug Fix : 22943 Added PandIPaymentAmount
                    addTo(componentNode, "PandIPaymentAmount", formatCurrency(componentLoc.getPAndIPaymentAmount()));
                    addTo(componentNode, "AllocateTaxEscrow", BXResources.getGenericMsg(((("Y").equalsIgnoreCase(componentLoc.getPropertyTaxAllocateFlag())?Xc.YES_LABEL: Xc.NO_LABEL)), lang));
                    addTo(componentNode, "TaxEscrow", (("Y").equalsIgnoreCase(componentLoc.getPropertyTaxAllocateFlag())?formatCurrency(componentLoc.getPropertyTaxEscrowAmount()):formatCurrency(0.00)));
                    addTo(componentNode, "TotalPayment", formatCurrency(componentLoc.getTotalPaymentAmount()));
                    addTo(componentNode, "HoldBackAmount", formatCurrency(componentLoc.getAdvanceHold()));

                }
                //Line Of Credit Component Related Information Ends

                //Loan Component Related Information Starts
                else if (components.getComponentTypeId()==Mc.COMPONENT_TYPE_LOAN){
                    ComponentLoan componentLoan = new ComponentLoan(srk,components.getComponentId(), components.getCopyId());

                    //Line 2
                    addTo(componentNode, "LoanAmount", formatCurrency(componentLoan.getLoanAmount()));

                    addTo(componentNode, "ActualPaymentTerm", DocPrepLanguage.getYearsAndMonths(
                            componentLoan.getActualPaymentTerm(), lang));
                    addTo(componentNode, "PaymentFrequency",BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency", componentLoan.getPaymentFrequencyId() , lang));
                   //Line 3
                    //FXP24068: from formatRatio to formatInterestRate
                    addTo(componentNode, "Discount", formatInterestRate(componentLoan.getDiscount()));
                    addTo(componentNode, "Premium", formatInterestRate(componentLoan.getPremium()));
                    addTo(componentNode, "NetRate", formatInterestRate(componentLoan.getNetInterestRate()));
                    addTo(componentNode, "TotalPayment", formatCurrency(componentLoan.getPAndIPaymentAmount()));

                }
                //Loan Component Related Information Ends

                //Credit Card Component Related Information Starts
                else if (components.getComponentTypeId()==Mc.COMPONENT_TYPE_CREDITCARD){
                    ComponentCreditCard componentCC = new ComponentCreditCard(srk,components.getComponentId(), components.getCopyId());

                    //Line 2
                    addTo(componentNode, "CreditCardAmount", formatCurrency(componentCC.getCreditCardAmount()));
                    /****MCM Impl Team Bug Fix : FXP22639 Starts */
                    //PricingRateInventory priceRateInv = new PricingRateInventory(srk, components.getPricingRateInventoryId());
                    //FXP24068: from formatRatio to formatInterestRate
                    addTo(componentNode, "InterestRate", formatInterestRate(components.getPostedRate()));
                    /****MCM Impl Team Bug Fix : FXP22639 Ends */
                }
                //Credit Card Component Related Information Ends

                //Over Draft Component Related Information Starts
                else if (components.getComponentTypeId()==Mc.COMPONENT_TYPE_OVERDRAFT){
                    ComponentOverdraft componentOverdraft = new ComponentOverdraft(srk,components.getComponentId(), components.getCopyId());

                    //Line 2
                    addTo(componentNode, "OverDraftAmount", formatCurrency(componentOverdraft.getOverDraftAmount()));
                    /****MCM Impl Team Bug Fix : FXP22639 Starts */
                   // PricingRateInventory priceRateInv = new PricingRateInventory(srk, components.getPricingRateInventoryId());
                    //FXP24068: from formatRatio to formatInterestRate
                    addTo(componentNode, "InterestRate", formatInterestRate(components.getPostedRate()));
                    /****MCM Impl Team Bug Fix : FXP22639 Ends */
                }
                //Over Draft Component Related Information Ends
                elemNode.appendChild(componentNode);
             }

        }
        catch (Exception e)
        {
        	srk.getSysLogger().error("GenDealSummary.extractComponentDetailsSection: Error occurred - " + e.getMessage());
        }

        return elemNode;
    }
    /**
     * Description : This Method Returns Qualifying Details element Node
     * @version 1.0 XS_16.23 01-Aug-2008 Initial Vesrion
     * @version 1.1 Added a flag for Qualifying Details Check
     * @version 1.2 Corrected the logic for adding Not Available and Qualifying Details data
     * @return element Node of Qualifying Details Section
     */
    protected Node extractQualifyingDetailsSection()
    {
        Node qualifyingDetNode = null;
        try
        {
            String usePOSQualifyDetail = PropertiesCache.getInstance()
            .getProperty(srk.getExpressState().getDealInstitutionId(),
                    "com.basis100.deal.usePOSQualifyDetail");
            // If 'com.basis100.deal.usePOSQualifyDetail' = 'Y'
            if ("Y".equalsIgnoreCase(usePOSQualifyDetail))
            {
            qualifyingDetNode = doc.createElement("QualifyingDetails");
            QualifyDetail qualifyDetail = null;
            boolean qualifyingDetFlag = false;
            try
            {
                qualifyDetail = new QualifyDetail(srk, deal.getDealId());
                qualifyingDetFlag = true;
            }
            catch (FinderException fe)
            {
                qualifyingDetFlag = false;
            }

            	//If Qualifying Details record does not exists for deal then add 'Not Available'
            	if(qualifyingDetFlag == false)
            	{
            		addTo(qualifyingDetNode, "SectionHeader", "Not Available");
            	}
            	//Else add all details for Qualifying Deails
            	else
            	{
            		addTo(qualifyingDetNode, "QualifyRate", formatInterestRate(qualifyDetail
            				.getQualifyRate()));

            		String interestCompound = BXResources.getPickListDescription(srk
            				.getExpressState().getDealInstitutionId(),
            				"InterestCompound", qualifyDetail
            				.getInterestCompoundingId(), lang);

            		addTo(qualifyingDetNode, "CompoundingPeriod", BXResources
            				.getPickListDescription(srk.getExpressState()
            						.getDealInstitutionId(), "COMPOUNDPERIOD",
                        qualifyDetail.getInterestCompoundingId(), lang));

            		addTo(qualifyingDetNode, "AmortizationTerm", DocPrepLanguage
            				.getYearsAndMonths(qualifyDetail.getAmortizationTerm(),
            						lang));

            		addTo(qualifyingDetNode, "RepaymentType", BXResources
            				.getPickListDescription(srk.getExpressState()
            						.getDealInstitutionId(), "RepaymentType",
            						qualifyDetail.getRepaymentTypeId(), lang));

            		addTo(qualifyingDetNode, "Payment", formatCurrency(qualifyDetail
            				.getPAndIPaymentAmountQualify()));
            		addTo(qualifyingDetNode, "GDS", formatRatio(qualifyDetail
            				.getQualifyGds()));
            		addTo(qualifyingDetNode, "TDS", formatRatio(qualifyDetail
            				.getQualifyTds()));
            	}//end of if(qualifyingDetFlag == false) else

            }//end of if ("Y".equalsIgnoreCase(usePOSQualifyDetail))
        }
        catch (Exception e)
        {
        	srk.getSysLogger().error("GenDealSummary.extractQualifyingDetailsSection: Error occurred - " + e.getMessage());
        }

        return qualifyingDetNode;
    }
    /***************************************************************************
     **************MCM Impl team changes - 31-Jul-2008 - Ends - XS_16.23 /
     **************************************************************************/

    //4.4 Submission Agent
    protected Node extractSource2Name()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Name", "");

      	  	SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Name", "");
      	  	else
      	  	{
	            if (sobp.getContact() != null)
	            {
	                Contact c = sobp.getContact();
	
	                if (!isEmpty(c.getContactFirstName()) &&
	                    !isEmpty(c.getContactLastName()))
	                {
	                    val = new String();
	
	                    if (!isEmpty(c.getContactFirstName()))
	                       val += c.getContactFirstName() + format.space();
	
	                    if (!isEmpty(c.getContactLastName()))
	                       val += c.getContactLastName();
	                }
	            }
      	  	}
        }
        catch (Exception e){}

        return getTag("Source2Name", val);
    }

    protected Node extractSource2Address()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Address", "");
      	  	
      	  		SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Address", "");
      	  	else
      	  	{
      	  		Contact contact = sobp.getContact();
      	  		Addr addr = contact.getAddr();

      	  		val = new String();

	            if (!isEmpty(addr.getAddressLine1()))
	               val += addr.getAddressLine1();
	
	            if (!isEmpty(addr.getCity()))
	               val += format.space() + addr.getCity();
	
	            Province prov = new Province(srk, addr.getProvinceId());
	
	            val += format.space() + prov.getProvinceAbbreviation();
      	  	}
        }
        catch (Exception e){}

        return getTag("Source2Address", val);
    }

    
    protected Node extractSource2Phone()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Phone", "");
      	  	
        	SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Phone", "");
      	  	else
      	  	{
	            Contact contact = sobp.getContact();
	            //Addr addr = contact.getAddr();
	
	            val = StringUtil.getAsFormattedPhoneNumber(contact.getContactPhoneNumber(),
	                                                       contact.getContactPhoneNumberExtension());
      	  	}
        }
        catch (Exception e){}

        return getTag("Source2Phone", val);
    }

    protected Node extractSource2Fax()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Fax", "");

        	SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Fax", "");
      	  	else
      	  	{
	            Contact contact = sobp.getContact();
	            //Addr addr = contact.getAddr();
	
	            val = StringUtil.getAsFormattedPhoneNumber(contact.getContactFaxNumber(), null);
      	  	}
        }
        catch (Exception e){}

        return getTag("Source2Fax", val);
    }

    
    protected Node extractLicenseNumber()
    {
        String val = null;

        try
        {
            SourceOfBusinessProfile sob =
                new SourceOfBusinessProfile(srk, deal.getSourceOfBusinessProfileId());

            val = sob.getSobLicenseRegistrationNumber();
        }
        catch (Exception e){}

        return getTag("LicenseNumber", val);
    }
    
    protected Node extractLicense2Number()
    {
        String val = null;

        if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
  	  		return getTag("License2Number", "");
        
        try
        {
            SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
            
            val = sobp.getSobLicenseRegistrationNumber();
        }
        catch (Exception e){}

        return getTag("License2Number", val);
    }
    
    protected Node extractWaiverDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getFinancingWaiverDate());
        }
        catch (Exception e){}

        return getTag("WaiverDate", val);
    }
    
    /**
     * Express 5.0 Qualify Rate change
     * @return
     */
    protected Node extractQualifyRate()
    {
        String val = null;

        try
        {
            val = formatInterestRate(deal.getGDSTDS3YearRate());

            PricingLogic2 pLogic = new PricingLogic2();
            int qualProd = deal.getOverrideQualProd();
            List<RateInfo> rates = pLogic.getLenderProductRates(srk, qualProd, deal.getDealId(), deal.getCopyId());
            RateInfo rateInfo = rates.get(0);
            double rate = rateInfo.getPostedRate();
            double rateDeal = deal.getGDSTDS3YearRate();
            if(rate != rateDeal)
                val = val + " *";    
        }       
        catch (Exception e){}

        return getTag("QualifyingRate", val);
    }
    
    /**
     * Express 5.0 Qualify Product
     * @return
     */
    protected Node extractQualifyProduct()
    {
        String val = null;
        try
        {
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), 
                    "MTGPROD", deal.getOverrideQualProd(), lang);
        }
        catch (Exception e){}

        return getTag("QualifyProduct", val);
    }
}

