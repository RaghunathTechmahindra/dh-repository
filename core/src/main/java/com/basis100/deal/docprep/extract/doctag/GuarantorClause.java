package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;
/**
 *If the Primary Property.ProvinceID(Description) = "British Columbia",
 *  Set value to 'Covenantor'Else	Set value to 'Guarantor'
 */

public class GuarantorClause extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal.getDealId(), 
              deal.getCopyId(), deal.getInstitutionProfileId());
      int id = prim.getProvinceId();

      if(id == Mc.PROVINCE_BRITISH_COLUMBIA)
      {
        val = "Covenantor";
      }
      else
      {
        val = "Guarantor";
      }

      fml.addValue(val, fml.TERM);
    }
    catch (Exception e)
    {
        if ((e instanceof NullPointerException) || (e instanceof FinderException))
        {
            fml.addValue("Guarantor", fml.TERM);
        }
        else
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);
            throw new ExtractException(msg);
        }
    }

    return fml;
  }
}
