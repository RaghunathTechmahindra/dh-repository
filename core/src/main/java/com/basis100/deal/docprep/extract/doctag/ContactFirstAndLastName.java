package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.entity.*;


public class ContactFirstAndLastName extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    
    Contact con = (Contact)de;

    try
    {
      String firstname = con.getContactFirstName();
      String lastname = con.getContactLastName();

      if(firstname == null || lastname == null ||
          firstname.length() == 0 || lastname.length()== 0)
            return new FMLQueryResult();

      val = firstname + format.space() + lastname;

      fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(e);
      throw new ExtractException("ERROR: Unable to extract Contact first or last name");
    }

    return fml;
  }
}
