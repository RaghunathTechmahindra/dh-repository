package com.basis100.deal.docprep;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

import java.io.*;
import java.util.*;

import java.awt.event.*;

import com.basis100.deal.docprep.gui.*;
import com.basis100.deal.util.StringUtil; 
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;

public class DocPrepMainController implements java.awt.event.ActionListener, DPGuiRequestListener
{

  private Vector listeners;
  private DocGenHandlerController docGenHandlerController;
  private SysLogger logger;
  private DPAlertManager dpAlertManager;
  private boolean monitoringStopped;
  // ---------- CR, 19-Jul-04, PGP for DJ  ---------
  // changed for MLM - Sep 20, 2007 Midori
    private Map<Integer, ByteArrayInputStream[]> pgpKeyInputStreams = new HashMap<Integer, ByteArrayInputStream[]>();

  /**
   * Constructor.
   */
  public DocPrepMainController() throws RemoteException, FinderException
  {
    // initialize BXResources. We do it here just because BX resources
    // will not initialize it's own logger. If the logger is not initialized
    // and we do not have resource available it will trigger exception.
    // to avoid that we have to make a fake initialization of BX resources here.
    BXResources bxResources = new BXResources();

    monitoringStopped = true;
    docGenHandlerController = new DocGenHandlerController();
    listeners = new Vector();
    logger = ResourceManager.getSysLogger("DP-Ctrl");
    dpAlertManager = new DPAlertManager();
    
    try
    {
        // addded for ML
        // first, get all institutionProfiles 
        SessionResourceKit srk = new SessionResourceKit("DocPrepMainController");
        InstitutionProfile inst = new InstitutionProfile(srk);        
        Collection insts = inst.getAllInstitutions();
        
        // then load if PGP is required or not for each institution
        // if yes, then load Keys and store in map
        Iterator instIter = insts.iterator();
        while (instIter.hasNext())
        {
            inst = (InstitutionProfile) instIter.next();
            int institutionId = inst.getInstitutionProfileId();            
        
            // ---------- CR, 19-Jul-04, PGP for DJ  ---------

            String sUsePGP = PropertiesCache.getInstance().getProperty(institutionId,"com.filogix.docprep.PGP.in.use", "N");
            logger.trace("PGP in use is set to " + sUsePGP);
        
            if (sUsePGP.equalsIgnoreCase("y")){
        
              String sKeyPath = PropertiesCache.getInstance().getProperty(institutionId,"com.filogix.docprep.PGP.keys");
              logger.trace("loading pgp keys from " + sKeyPath + " folder");
        
              try {
                loadPGPKeys(sKeyPath, institutionId);
              }
              catch (DocPrepException dpe){
                logger.error("PGP key does not exist: "+ sKeyPath + ": " + dpe.getMessage());
              }
              catch (FileNotFoundException ioe){
                logger.error("PGP FileNotFoundException: "+ sKeyPath + ": " + ioe.getMessage());
              }
              catch (IOException ioe){
                logger.error("error reading from PGP key :" + sKeyPath + ": " + ioe.getMessage());
              }
            }
            // ---------- CR, 19-Jul-04, PGP for DJ end ---------
        }
    }
    catch(RemoteException re)
    {
        logger.error(this.getClass().toString() 
                     + " DocPrepMainController @Constructor: "
                     + " Faild retreive all InstitionProfile from DB");
        throw re;
    }
    catch(FinderException fe)
    {
        logger.error(this.getClass().toString() 
                     + " DocPrepMainController @Constructor: "
                     + " Faild retreive all InstitionProfile from DB");
        throw fe;
    }
        
  }

  // ---------- CR, 19-Jul-04, PGP for DJ  ---------
  // read and cash pgp keys
  protected void loadPGPKeys(String sKeyPath, int institutionProfileId) throws FileNotFoundException,
      IOException, DocPrepException {
    logger.trace("--------------------- Start of loading PGP keys --------------------- ");
    File keyFiles = new File(sKeyPath);
    String[] keyFilesArray = keyFiles.list();
    if (keyFilesArray == null) {
      throw new DocPrepException("PGP key file(s) not found");
    }

    int ch;

    ByteArrayInputStream[] pgpKeyInputStream = new ByteArrayInputStream[keyFilesArray.length];
    for (int i = 0; i < pgpKeyInputStream.length; i++) {
      String fileName = sKeyPath + "\\" + keyFilesArray[i];

        FileReader keyFileReader = new FileReader(fileName);
        ByteArrayOutputStream keyBAOutStream = new ByteArrayOutputStream();
        int chars = 0;
        while((ch = keyFileReader.read()) != -1){
          keyBAOutStream.write(ch);
          chars++;
        }
        keyFileReader.close();
        logger.trace("Converted " + chars + " chars from " + fileName);
        pgpKeyInputStream[i] = new ByteArrayInputStream(keyBAOutStream.toByteArray());
      }
      
      pgpKeyInputStreams.put(new Integer(institutionProfileId), pgpKeyInputStream );
      logger.trace("--------------------- End of loading PGP keys --------------------- ");
  }

  public InputStream[] getPGPkeyInputStream(int institutionId){
      Object o = this.pgpKeyInputStreams.get( new Integer(institutionId));
      if ( o == null ) return null;
    return (InputStream[])o;
  }
  // ---------- CR, 19-Jul-04, PGP for DJ end ---------

  /**
   * Returns true if process of monitoring documentqueue is stopped. Otherwise returns false.
   */
  public boolean isMonitoringStopped(){
    return monitoringStopped;
  }

  /**
   * Monitoring of documentqueue has been turned off.
   */
  public void setMonitoringStopped(boolean pVal){
    monitoringStopped = pVal;
  }

  /**
   * Trigger the request to increase the number of worker threads.
   * This number can grow until the limit gets reached.
   */
  public void increaseWrkThreadSize(){
    docGenHandlerController.increaseSize();
    dpAlertManager.sendEmailAlert( docGenHandlerController.extractStatistics(), "Increase the number of worker threads.");
  }

  /**
   * Decreases the number of allowed worker threads to be initiated.
   * It will work until the minimum number of worker threads gets reached.
   */
  public void decreaseWrkThreadSize(){
    docGenHandlerController.decreaseSize();
  }

  /**
   * Adds action listener to this object.
   */
  public void addListener(ActionListener pList){
    listeners.addElement(pList);
  }

  /**
   * Notifies all registered listeners and triggers event.
   */
  private void notifyListeners(ActionEvent ev){
    Iterator it = listeners.iterator();
    while( it.hasNext() ){
      ((ActionListener) it.next()).actionPerformed(ev);
    }
  }

  /**
   * DPGuiRequestListener interface implementation.
   */
  public void DPGuiRequest(DPGuiRequestEvent event){
    System.out.println("Controller has received event from gui");
    Object obj = event.getSource();
    if( obj instanceof DPGuiRenderer ){
      switch( event.getType() ){
        case DPGuiRequestEvent.RT_REQUEST_FOR_STATISTICS : {
          ((DPGuiRenderer)obj).updateGui(new UpdateGuiEvent(extractStatistics() , 1, "SHOW_STATISTICS") );
          break;
        }
      }
    }
  }

  /**
   * ActionListener interface implementation.
   */
	public void actionPerformed(java.awt.event.ActionEvent ev) 
	{ 
		Object obj = ev.getSource(); 
		if (obj instanceof HandlerWorkerHolder) 
		{ 
			HandlerWorkerHolder hwh = (HandlerWorkerHolder) obj; 
			if (hwh.isInException()) 
			{ 
				docGenHandlerController.addToFailedList(hwh); 
				docGenHandlerController.removeWorker(hwh); 
				handleError(hwh); 
			} 
			else if (ev.getActionCommand().equals("SUCCESS-Finished")) 
			{ 
				docGenHandlerController.removeWorker(hwh); 
			} 
			notifyListeners(ev); 
		} 
	} 
 
	public void handleError(HandlerWorkerHolder hwh) 
	{ 
			logHandlerError(hwh); 
			dpAlertManager.sendEmailAlert(hwh.generateInfo()); 
	} 

  /**
   * When worker thread throws an exception this must be logged. We log
   * exception because alert system can fail for a lot of reasons, so we
   * want to keep track of the exception that handler faced.
   */
  private void logHandlerError(HandlerWorkerHolder pHwh){
    StringBuffer sb = new StringBuffer("");
    sb.append("***** Exception in worker handler number : WT" + pHwh.getWorkerNumberId() + " ***** \n");
    sb.append("========================================================================= \n");
    sb.append( pHwh.generateInfo());
    sb.append("\n");
    sb.append("========================================================================= \n");

    logger.error(sb.toString());
  }

  /**
   * Returns docprep session statistics.
   */
  public String extractStatistics(){
    return docGenHandlerController.extractStatistics();
  }

  /**
   * Returns information about threads that failed.
   */
  public String extractFailureMessages(){
    return docGenHandlerController.extractFailedInfo();
  }

  /**
   * Before we start new thread we want to make sure that there is enough
   * space. If the maximum number of allowed thread has been reached, we will
   * have to wait for other threads to finish their work.
   */
  public boolean canLaunchNewRequest(){
    return docGenHandlerController.isThereFreeSpot();
  }

  /**
   * Method creates new worker thread.
   * @throws DocPrepException  
   */
	public void launchNewProcess(HandlerWorkerHolder pHolder) throws DocPrepException 
	{
		if (docGenHandlerController.isThereFreeSpot() == false) 
		{ 
			logger.error("There is no empty space. Could not create new empty thread"); 
			return; 
		} 
	 
	    // count up the number of running worker thread 
	    // this is removed either in DocumentGenerationHandler.run or catch statement below 
		docGenHandlerController.addWorker(pHolder); 
	 
		DocumentGenerationHandler handler = null; 
		try 
		{ 
		  DocumentGenerationManager.debug("\n\nDOCPREP: Document Requested. "  
						  + new Date() + "\n" + pHolder.generateInfo()); 
		  handler = new DocumentGenerationHandler( pHolder, this) ; 
	    } 
		catch (Throwable t)
		{ 
			logger.error(StringUtil.stack2string(t)); 
			// number of thread in docGenHandlerController has to be reduced 
			docGenHandlerController.addToFailedList(pHolder); 
			docGenHandlerController.removeWorker(pHolder); 
			handleError(pHolder); 
			throw new DocPrepException(t.toString()); 
		}
	 
		// we cannot catch error that happens in handler.run with try-catch statement here.  
		// handler.run catches Throwable inside 
		handler.start(); 
	}
}
