package com.basis100.deal.docprep.extract.data;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Property;
import com.basis100.entity.FinderException;
import com.basis100.xml.XmlToolKit;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class DisclosureExtractor extends GENXExtractor {

    DisclosureDataProvider ddp;
    private Deal mDeal;
    private int lang;
   
    double totalCheckedFees = 0;

    double totalFees = 0d;

    private final int[] propertyTax = { 0 }; // EscrowTypeID - move this to MC.java

    private final int[] borrowerTypeId = { 0, 1 }; // BorrowerTypeID - move this to MC.java

    private final int[] pMTG_BROKER_FEES = { Mc.FEE_TYPE_BROKER_FEE };

    private final int[] pHOLDBACK_FEES = { Mc.FEE_TYPE_PROPERTY_TAX_HOLDBACK_FINAL };

    private final int[] pMTG_BROKER_ORG_FEES = { Mc.FEE_TYPE_ORIGINATION_FEE };

    private final int[] pMTG_INSURANCE_FEE = { Mc.FEE_TYPE_INSPECTION_FEE };

    private final int[] pMTG_INSUR_FEE = { Mc.FEE_TYPE_CMHC_FEE, Mc.FEE_TYPE_GE_CAPITAL_FEE };

    private final int[] pOTHER_CHARGES = { Mc.FEE_TYPE_BRIDGE_FEE, Mc.FEE_TYPE_BUYDOWN_FEE, Mc.FEE_TYPE_STANDBY_FEE,
        Mc.FEE_TYPE_ORIGINATION_FEE, Mc.FEE_TYPE_TITLE_INSURANCE_FEE, Mc.FEE_TYPE_INSPECTION_FEE,
        Mc.FEE_TYPE_CANCELLATION_FEE, Mc.FEE_TYPE_COUREER_FEE, Mc.FEE_TYPE_WIRE_FEE,
        Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED, Mc.FEE_TYPE_FINDER_FEE };

    private final int[] pOTHER_BRK_FEE = { Mc.FEE_TYPE_BRIDGE_FEE, Mc.FEE_TYPE_BUYDOWN_FEE, Mc.FEE_TYPE_STANDBY_FEE,
        Mc.FEE_TYPE_TITLE_INSURANCE_FEE, Mc.FEE_TYPE_CANCELLATION_FEE, Mc.FEE_TYPE_COUREER_FEE, Mc.FEE_TYPE_WIRE_FEE,
        Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED, Mc.FEE_TYPE_FINDER_FEE, Mc.FEE_TYPE_AGENT_REFERRAL_FEE,
        Mc.FEE_TYPE_BROKER_FEE, Mc.FEE_TYPE_BONUS_FEE, Mc.FEE_TYPE_APPLICATION_FEE };

    private final int[] pOTHER_LENDER_FEES = { Mc.FEE_TYPE_BRIDGE_FEE, Mc.FEE_TYPE_BUYDOWN_FEE,
        Mc.FEE_TYPE_STANDBY_FEE, Mc.FEE_TYPE_CANCELLATION_FEE, Mc.FEE_TYPE_COUREER_FEE, Mc.FEE_TYPE_WIRE_FEE,
        Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED, Mc.FEE_TYPE_FINDER_FEE };

    private final int[] pAPPRAISAL_FEES = { Mc.FEE_TYPE_APPRAISAL_FEE, Mc.FEE_TYPE_INSPECTION_FEE,
        Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL, Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE };

    private final int[] pAPPLICATION_FEES = { Mc.FEE_TYPE_APPLICATION_FEE };

    private final int[] pOTHER1_FEES = { Mc.FEE_TYPE_CMHC_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PREMIUM };

    private final int[] pOTHER2_FEES = { Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM };

    private final int[] pOTHER3_FEES = { Mc.FEE_TYPE_CMHC_FEE, Mc.FEE_TYPE_GE_CAPITAL_FEE,
        Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE, Mc.FEE_TYPE_TITLE_INSURANCE_FEE };

    private final int[] pMI_FEE_ID = { Mc.FEE_TYPE_CMHC_FEE, Mc.FEE_TYPE_GE_CAPITAL_FEE };

    private final int[] pLEGAL_FEE = { Mc.FEE_TYPE_CTFS_SOLICITOR_FEE, Mc.FEE_TYPE_WIRE_FEE };

    private final int[] pLEGAL_FEES = { Mc.FEE_TYPE_CTFS_SOLICITOR_FEE };

    private final int[] pINSPEC_FEES = { Mc.FEE_TYPE_APPRAISAL_FEE, Mc.FEE_TYPE_INSPECTION_FEE,
        Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL };

    private final int[] pAPPRAISAL_FEE = { Mc.FEE_TYPE_APPRAISAL_FEE, Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL };

    private final int[] pBONUS_DISCOUNT = { Mc.FEE_TYPE_BUYDOWN_FEE, Mc.FEE_TYPE_CMHC_FEE };

    private final int[] pBONUS = { Mc.FEE_TYPE_BONUS_FEE };

    public static final int[] pBONUS_FEES = { Mc.FEE_TYPE_BONUS_FEE };

    /**
     * Class constructor
     */
    public DisclosureExtractor() {
    }

    /**
     * Called by DocumentGeneratorHandler for all document types
     * 
     * @throws Exception
     */
    protected void buildDataDoc() throws Exception {
        buildXMLDataDoc();
    }

    /**
     * Builds entire xml structure for Disclosure Document (called Statement of Mortgage as well) for Ontario
     * 
     * @throws Exception
     */
    protected void buildXMLDataDoc() throws Exception {
        doc = XmlToolKit.getInstance().newDocument();

        Element docRoot = doc.createElement("Document");
        docRoot = buildBaseTags(docRoot);
        doc.appendChild(docRoot);
    }

    /**
     * Extracts all the necessery data from deal to make Disclosure document
     * 
     * @param root Element
     * @return Element
     * @throws Exception
     */
    private Element buildBaseTags(Element root) throws Exception {
        Property pp = getPrimaryProperty();

        ddp = new DisclosureDataProvider(srk, deal, lang);
        ddp.setDocumentFormat(format);
        ddp.setDocumentTypeId(format.getDocumentProfile().getDocumentTypeId()); // Setting the DocumentTypeId for the Deal
        try {
            // addTo(root, getTag("Logo_Image", mXslDirLocation));
            addTo(root, getTag("DSCL_CURRENT_DATE", ddp.getDay(formatDate(new Date()))));
            addTo(root, getTag("DSCL_CURRENT_MONTH", ddp.getMonth(formatDate(new Date()))));
            addTo(root, getTag("DSCL_CURRENT_YEAR", ddp.getYear(formatDate(new Date()))));
            addTo(root, getTag("DSCL_CURRENT_YR", ddp.getYr(formatDate(new Date()))));
            addTo(root, getTag("DSCL_CURRENT_TIME", ddp.getTime()));
            addTo(root, getTag("DISC_DATA_1", formatDate(new Date())));
            addTo(root, getTag("DISC_PRTYDESC_1", ddp.exQuestion("040H")));
           // addTo(root, getTag("DSCL_BLNCEENDTRM_1", formatCurrency(ddp.exBalanceRemainingAtEndOfTerm())));
           addTo(root, getTag("DISC_LEGALFEES_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pLEGAL_FEES))));
            addTo(root, getTag("DISC_TOTALDED_1", formatCurrency(bonusDeductions())));
            // Added formatInterestRate for formatting the Interest Rate TAR # 1612
            if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
                addTo(root, getTag("DISC_APR_1",
                    getFormattedInterestRateWithoutPercentage(ddp.exTotalCostOfBorrowing())));
            } else {
                addTo(root, getTag("DISC_INITALAPR_1", getFormattedInterestRateWithoutPercentage(ddp
                    .exTotalCostOfBorrowing())));
            }
            addTo(root, getTag("DSCL_BLNCEENDTRM_1", formatCurrency(deal.getBalanceRemainingAtEndOfTerm())));
            addTo(root, getTag("DSCL_LEND_1", ddp.exLenderFullName()));
            addTo(root, getTag("DSCL_LENDADD_1", ddp.exLenderAddress()));
            // Changed fomatCurrency overrided in DiscloseureExtractor for Bug # 1617
            addTo(root, getTag("DSCL_NETLOAN_1", formatCurrency(ddp.exTotalLoanAmount(Mc.LIEN_POSITION_FIRST))));
            // Changed fomatCurrency overrided in DiscloseureExtractor for Bug # 1617
            addTo(root, getTag("DSCL_NETLOAN_2", formatCurrency(ddp.exTotalLoanAmount(Mc.LIEN_POSITION_SECOND))));
            // Changed fomatCurrency overrided in DiscloseureExtractor for Bug # 1617
            addTo(root, getTag("DSCL_NETLOAN_3", formatCurrency(ddp.exTotalLoanAmount(Mc.LIEN_POSITION_THIRD))));

            String value = format.getNotApplicable();
            if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
                value = getFormattedInterestRateWithoutPercentage(ddp.exTotalCostOfBorrowing());
            }
            addTo(root, getTag("DSCL_RATE_1", value));

            value = format.getNotApplicable();
            if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
                value = getFormattedInterestRateWithoutPercentage(ddp.exTotalCostOfBorrowing());
            }
            addTo(root, getTag("DSCL_RATE_2", value));

            addTo(root, getTag("DSCL_PAYFREQ_1", ddp.exPaymentFrequency()));
            addTo(root, getTag("DSCL_BALREM_1", "")); // dummy calc engine
            addTo(root, getTag("DSCL_TOTFEES_2", formatCurrency(totalFees())));
            addTo(root, getTag("DSCL_FEES_1", formatCurrency(totalFees())));
            addTo(root, getTag("DSCL_RATE_3", getFormattedInterestRateWithoutPercentage(ddp.exTotalCostOfBorrowing())));
            //addTo(root, getTag("DSCL_BALREM_1", formatCurrency(ddp.exBalanceRemainingAtEndOfTerm())));
            //addTo(root, getTag("DSCL_BALREM_2", formatCurrency(ddp.exBalanceRemainingAtEndOfTerm())));
            addTo(root, exCoBorrowerNamesAddressLoop("borrowerloopNL")); // TAR # 1612
            addTo(root, exAllBorrowerNamesAddressLoop("borrowerloopNS"));
            int provId = pp.getProvinceId();
            Date discDate = null;
            Calendar cl = null;
            switch (provId) {
            case Mc.PROVINCE_ALBERTA:
    		  String discFees1 = "0";
    		  if(!(ddp.exQuestion("101Q090V").equals(null))&&(!(ddp.exQuestion("101Q090V").trim().length()==0))) {
    			  discFees1 = ddp.exQuestion("101Q090V");
    		  }
    		  addTo(root, getTag("DSCL_DISCFEES_1", formatCurrency(Double.parseDouble(discFees1))));
                addTo(root, getTag("DISC_CNSTRCTINMTGINTST_2", ddp.exQuestion("101Q080U")));
                addTo(root, getTag("DSCL_ANNLPERCRTEVRTNS_3", ddp.exQuestion("101Q10AC")));
                addTo(root, getTag("DSCL_PYMTVRTNS_3", ddp.exQuestion("101Q10AD")));
                addTo(root, getTag("DSCL_TOTALFEES_1", formatCurrency(totalOtherCharges())));
                addTo(root, getTag("AB_DSCL_FEES_1", formatCurrency(totalFees())));
                break;
            case Mc.PROVINCE_PRINCE_EDWARD_ISLAND:
                /*
                 * Code fix for Bug# 1617 - start Added code for NULL check and FormattedDate
                 */
                String exQuestion_year = ddp.exQuestion("106Q010A_Y");
                String exQuestion_month = ddp.exQuestion("106Q010A_M");
                String exQuestion_day = ddp.exQuestion("106Q010A_D");

                if ((exQuestion_year != null && exQuestion_year.trim().length() > 0)
                    && (exQuestion_month != null && exQuestion_month.trim().length() > 0)
                    && (exQuestion_day != null && exQuestion_day.trim().length() > 0)) {
                    cl = Calendar.getInstance();
                    cl.set(Integer.parseInt(ddp.exQuestion("106Q010A_Y")), Integer.parseInt(ddp
                        .exQuestion("106Q010A_M")) - 1, Integer.parseInt(ddp.exQuestion("106Q010A_D")));
                    discDate = cl.getTime();
                    addTo(root, getTag("DSCL_DATE_1", formatDate(discDate)));
                } /*
                     * else { addTo(root, getTag("DSCL_DATE_1", "")); }
                     */
                addTo(root, getTag("DSCL_TERMVAR_1", ddp.exQuestion("106Q080V")));
                addTo(root, getTag("DSCL_RATEVAR_1", ddp.exQuestion("106Q10AB")));
                addTo(root, getTag("DSCL_REPAYTERMS_1", ddp.exQuestion("106Q11AC")));
                addTo(root, getTag("DSCL_OVERDUE_1", ddp.exQuestion("106Q12AD")));
                break;
            case Mc.PROVINCE_NEWFOUNDLAND:
                addTo(root, getTag("DSCL_ANNLPERCRTEVRTNS_2", ddp.exQuestion("103Q050U")));
                addTo(root, getTag("DSCL_PYMTVRTNS_2", ddp.exQuestion("103Q080X")));
                addTo(root, getTag("DSCL_PREPYMTAMTCNDTS_1", ddp.exQuestion("103Q12AE")));
                break;
            case Mc.PROVINCE_NOVA_SCOTIA:
                if (ddp.exQuestion("104Q010A_Y") != "" && ddp.exQuestion("104Q010A_D") != ""
                    && ddp.exQuestion("104Q010A_M") != "") {
                    cl = Calendar.getInstance();
                    cl.set(Integer.parseInt(ddp.exQuestion("104Q010A_Y")), Integer.parseInt(ddp
                        .exQuestion("104Q010A_M")) - 1, Integer.parseInt(ddp.exQuestion("104Q010A_D")));
                    discDate = cl.getTime();
                    addTo(root, getTag("DISC_DATA_2", formatDate(discDate)));
                } else {
                    addTo(root, getTag("DISC_DATA_2", ""));
                }
                addTo(root, getTag("DISC_MTHODCALC_1", ddp.exQuestion("104Q090W")));
                addTo(root, getTag("DISC_CNSTRCTINMTGINTST_1_Y", ddp.exQuestion("104Q14AE_Y")));
                addTo(root, getTag("DISC_CNSTRCTINMTGINTST_1_N", ddp.exQuestion("104Q14AE_N")));
                addTo(root, getTag("DSCL_ANNLPERCRTEVRTNS_1", ddp.exQuestion("104Q15AF")));
                addTo(root, getTag("DSCL_PYMTVRTNS_1", ddp.exQuestion("104Q16AG")));
                addTo(root, getTag("DISC_CHANGESIFMTGNOTREPAID_1", ddp.exQuestion("104Q17AH")));
                addTo(root, getTag("DISC_MUNICIPALTXSPYDBYLNDR_Y", ddp.exQuestion("104Q17AI_Y")));// CR
                addTo(root, getTag("DISC_MUNICIPALTXSPYDBYLNDR_N", ddp.exQuestion("104Q17AI_N")));// CR
                addTo(root, getTag("DISC_MTHDOFPAYMETIFTXSPAIDBYLNDR_1", ddp.exQuestion("104Q18AJ")));
            }
            addTo(root, getTag("DSCL_MTGAMT_1", formatCurrency(ddp.exTotalLoanAmount())));
            addTo(root, getTag("DSCL_BONUS_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pBONUS_FEES))));
            addTo(root, getTag("DSCL_APPLCTNFEE_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pAPPLICATION_FEES))));
            addTo(root, getTag("DSCL_INSPCTNFEE_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pAPPRAISAL_FEE))));
            addTo(root, getTag("DSCL_INTADJSTMNT_1", formatCurrency(ddp.exPerdiumAmount())));
            addTo(root, getTag("DSCL_INTSTDAYS_1", ddp.exIadNumOfDays()));
            addTo(root, getTag("DSCL_MTGPAYMTAMT_1", formatCurrency(ddp.exPandIPayment())));
            addTo(root, getTag("DSCL_PYMTWTHPRPTYTAXS_1", ddp
					.exPaymentIncludeTax()));
            addTo(root, getTag("DSCL_MTGDUE_1", formatDate(ddp.exMaturityDate()))); // TAR : 1612 - Formatting
                                                                                    // changes to the date.
            addTo(root, getTag("DSCL_MTGTERM_1", String.valueOf(ddp.exActualPaymentTerm())));
            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setMaximumFractionDigits(2);
            addTo(root, getTag("DSCL_MTGTERM_2", String.valueOf(decimalFormat.format(ddp.exActualPaymentTerm() / 12d))));
            addTo(root, getTag("DSCL_MTGAMRTIZTON_1", ddp.exAmortizationTermInYears()));

            addTo(root, getTag("DSCL_PYMTDAY_1", ddp.getDay(formatDate(ddp.exFirstPaymentDate()) + " "
                + ddp.getMonth(formatDate(ddp.exFirstPaymentDate())) + " ,")));
            addTo(root, getTag("DSCL_PYMTDT_1_DAY", ddp.getDay(formatDate(ddp.exFirstPaymentDate()))));
            addTo(root, getTag("DSCL_PYMTDT_1_MONTH", ddp.getMonth(formatDate(ddp.exFirstPaymentDate()))));
            addTo(root, getTag("DSCL_PYMTDT_1_YEAR", ddp.getYear(formatDate(ddp.exFirstPaymentDate()))));
            addTo(root, getTag("DSCL_PYMTDT_1", formatDate(ddp.exFirstPaymentDate())));
            addTo(root, getTag("DSCL_PYMTYR_1", ddp.getYr(formatDate(ddp.exFirstPaymentDate()))));

            addTo(root, getTag("DISC_INTRSTADJ_1", formatCurrency(ddp.exInterimInterestAmount())));
            addTo(root, getTag("DISC_LOANAMTPAIDTOMGTEE_1", formatCurrency(ddp.exNetLoanAmount(totalCheckedFees))));
            addTo(root, getTag("DSCL_AMOUNT_1", formatCurrency(ddp.exTotalLoanAmount(Mc.LIEN_POSITION_FIRST))));
            addTo(root, getTag("PE_DSCL_AMOUNT_2", formatCurrency(ddp.exTotalLoanAmount(Mc.LIEN_POSITION_SECOND))));
            addTo(root, getTag("DSCL_AMOUNT_2", formatCurrency(ddp.exTotalLoanAmount() - totalFees())));
            addTo(root, getTag("DSCL_AMOUNT_3", formatCurrency(ddp.exTotalLoanAmount(Mc.LIEN_POSITION_THIRD))));
            // Changed formatCurrency to overrided formatCurrency() for Bug # 1617
            addTo(root, getTag("DSCL_PAYAMT_1", formatCurrency(ddp.exPandIPayment())));
            // Changed formatCurrency to overrided formatCurrency() for Bug # 1617
            addTo(root, getTag("DSCL_MIFEE_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pMI_FEE_ID))));
            addTo(root, getTag("DSCL_PAYMENTS_1", String.valueOf(ddp.exCalcNumOfPaymentsPerYear())));
            addTo(root, getTag("DSCL_INSURFEES_1", formatCurrency(deal.getMIPremiumAmount())));
            // Commented this as the Co-borrower name tag is already stored in DSC_BORRWER_2
            // addTo(root, getTag("DSCL_BRWRACK_2", ddp.exCoBorrowerName()));
            addTo(root, getTag("DSCL_BORROW_1", ddp.exPrimaryBorrowerName()));
            addTo(root, getTag("DSCL_BORROW_2", ddp.exCoBorrowerNames()));
            addTo(root, getTag("DSCL_BOR_COBOR_1", ddp.exAllBorrowerNames()));
            // Commented this as the Co-borrower name tag is already stored in DSC_BORRWER_2
            // addTo(root, getTag("DSCL_BOR_COBOR_2", ddp.exCoBorrowerNames()));
            addTo(root, exAllBorrowerNamesLoop1("borrowerloop1"));
            addTo(root, exAllBorrowerNamesLoop2("borrowerloop2"));
            addTo(root, getTag("DSCL_MTGINSRNCE_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pMTG_INSUR_FEE))));
            addTo(root, getTag("DSCL_BRKCOMSN_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pMTG_BROKER_FEES))));
            addTo(root, getTag("DSCL_OTHRCHRGS_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pOTHER_CHARGES))));
            // commented this method as it is not required in NS Document, The field is always blank
            // addTo(root, getTag("DISC_INSURANCEFEE_1",
            // formatCurrency(ddp.exSelectedFeesTotalAmt(pMTG_INSUR_FEE))));
            addTo(root, getTag("DISC_INSPCTNFEE_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pINSPEC_FEES))));
            addTo(root, getTag("DSCL_OTHERFEE_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pOTHER_LENDER_FEES))));
            addTo(root, getTag("DISC_ADRSBRWER_1", ddp.exPrimaryBorrowerAddress()));
            if ((ddp.exPaymentIncludeTax() != null)
					&& (ddp.exPaymentIncludeTax().equalsIgnoreCase("Yes"))) {
				addTo(root, getTag("DSCL_PRPTYTAXS_1", formatCurrency(ddp
						.exEscrowTotalAmt(propertyTax))));
			}
            addTo(root, getTag("DSCL_PREPYMTAMT_1", ddp.exPrepaymentAmount()));
            addTo(root, getTag("DSCL_ADRSPRPTY_1", ddp.exPrimaryPropertyAddressLine1(pp)));
            addTo(root, getTag("DSCL_LNDR_OFFCE_1", ddp.exLenderFullName()));
            addTo(root, getTag("DSCL_NMEMTGBRKR_1", ddp.exMrtgBrkName()));
            addTo(root, getTag("DSCL_ADRSMTGBRKR_1", ddp.exMrtgBrkAddr()));
            addTo(root, getTag("DISC_TYPMTG_1", ddp.exMortgageTypeOne()));
            addTo(root, getTag("DISC_TYPMTG_2", ddp.exLienPosition()));
            addTo(root, getTag("DISC_TYPMTG_2", ddp.exMortgageTypeTwo()));
            addTo(root, getTag("DISC_TYPMTG_3", ddp.exMortgageTypeThree()));
            addTo(root, getTag("DISC_TYPMTG_OTHER", ddp.exMortgageTypeOther()));
            addTo(root, getTag("DISC_OTHERCHARGES_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pOTHER_LENDER_FEES))));
            addTo(root, getTag("DSCL_LENDADDR_1", ddp.exLenderAddress()));
            addTo(root, getTag("DSCL_DWELLTYPE_1", ddp.exDwellingType()));
            addTo(root, getTag("DSCL_LGLFEE_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pLEGAL_FEE))));
            // commented this method as it is not required in NS Document, The field is always blank
            // addTo(root, getTag("DISC_BONUS_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pBONUS))));
            addTo(root, getTag("DSCL_BROKFEES_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pMTG_BROKER_ORG_FEES))));
            addTo(root, getTag("DSCL_OTHERFEES_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pOTHER_BRK_FEE))));
            addTo(root, getTag("DSCL_PTPYTXHDBK_1", formatCurrency(ddp.exSelectedFeesTotalAmt(pHOLDBACK_FEES))));
            // Commented both the call to primary address as the primary borrower address was already
            // available in "DISC_ADRSBRWER_1"
            // addTo(root, getTag("DSCL_BRWRADDRS_1", ddp.exPrimaryBorrowerAddress()));
            // addTo(root, getTag("DSCL_BRWRADDRS_2", ddp.exPrimaryBorrowerAddress()));
            addTo(root, getTag("DISC_PYMTFREQNCY_1", ddp.exPaymentFreqWeekly()));
            addTo(root, getTag("DISC_PYMTFREQNCY_2", ddp.exPaymentFreqBiWeekly()));
            addTo(root, getTag("DISC_PYMTFREQNCY_3", ddp.exPaymentFreqMonthly()));
            // Fix for 1607 - the text 'Semi-Monthly' needs to be printed only if the payment frequency id is
            // Semi-Monthly
            if (deal.getPaymentFrequencyId() == Mc.PAY_FREQ_SEMIMONTHLY) {
                addTo(root, getTag("DISC_PYMTFREQNCY_4", ddp.exPaymentFreqText()));
            }
            addTo(root, getTag("NL_DISC_PYMTFREQNCY_1", ddp.exPaymentFreqText()));
            addTo(root, getTag("DSCL_TOTLDED_1", formatCurrency(getTotalCheckedFees())));
            value = format.getNotApplicable();
            if (deal.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
            	value = getFormattedInterestRateWithoutPercentage(ddp.exNetInterestRateFixed());
            }
            addTo(root, getTag("DSCL_NETINTRATE_1", value));
            value = format.getNotApplicable();
            
            if (deal.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
            	value = getFormattedInterestRateWithoutPercentage(ddp.exNetInterestRateVariable());
            }
            addTo(root, getTag("DSCL_NETINTRATE_2", value));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    /**
     * Common method for fee processing. Creates feeDescription by concatenating names of all the fees IDs
     * listed in pFeesGroup array and totals their amounts.
     * 
     * @param tagRoot String
     * @param pFeesGroup int[]
     */
    private void addFeesCommon(String tagRoot, int[] pFeesGroup, Element root) {
        double fee = 0;

        fee = ddp.exSelectedFeesTotalAmt(pFeesGroup);
        if (fee != 0d) {
            totalFees += fee;
            addTo(root, getTag("fee" + tagRoot + "Desc", ddp.exSelectedFeesDescription(pFeesGroup)));
            addTo(root, getTag("fee" + tagRoot + "Amt", formatCurrency(fee)));
            if (fee > 0d) {
                addTo(root, getTag("isFee" + tagRoot + "AmtDeducted", "X"));
                totalCheckedFees += fee;
            }
        }
    }

    /**
     * feeBonus processor
     * 
     * @param tagRoot
     * @param root
     */
    private void addBonusFees(String tagRoot, Element root) {
        double fee = 0;
        try {
            fee = Double.parseDouble(ddp.exQuestion("401", "060S"));
            if (fee != 0d) {
                totalFees += fee;
                addTo(root, getTag("fee" + tagRoot + "Desc", ""));
                addTo(root, getTag("fee" + tagRoot + "Amt", formatCurrency(fee)));
                if (fee > 0d) {
                    addTo(root, getTag("isFee" + tagRoot + "AmtDeducted", "X"));
                    totalCheckedFees += fee;
                }
            }
        } catch (Exception e) {
        }
    }

    /**
     * feeOtherFee1 processor
     * 
     * @param root
     */
    private void addOther1Fees(Element root) {
        addOther1_or_Other2Fees("OtherFee1", pOTHER1_FEES, deal.getMIPremiumAmount(), root);
    }

    /**
     * feeOtherFee2 processor
     * 
     * @param root
     */
    private void addOther2Fees(Element root) {
        addOther1_or_Other2Fees("OtherFee2", pOTHER2_FEES, deal.getMIPremiumPST(), root);
    }

    /**
     * feeOtherFee1 and feeOtherFee2 processor. Difference is in amt passed and ID in the pFeesGroup.
     * 
     * @param tagRoot
     * @param pFeesGroup
     * @param amt
     * @param root
     */
    private void addOther1_or_Other2Fees(String tagRoot, int[] pFeesGroup, double amt, Element root) {
        double fee = 0;
        try {
            if (deal.getMIUpfront() == "N")
                fee = ddp.exSelectedFeesTotalAmt(pFeesGroup);
            else if (amt > 0d)
                fee = amt;

            if (fee != 0d) {
                totalFees += fee;
                addTo(root, getTag("fee" + tagRoot + "Desc", ""));
                addTo(root, getTag("fee" + tagRoot + "Amt", formatCurrency(fee)));
                if (fee > 0d) {
                    addTo(root, getTag("isFee" + tagRoot + "AmtDeducted", "X"));
                    totalCheckedFees += fee;
                }
            }
        } catch (Exception e) {
        }
        ;
    }

    /**
     * Retrurns lenderShortName and lenderAddress as one string
     * 
     * @param ddp
     * @return
     */
    private String lenderFirmNameAndAddress(DisclosureDataProvider ddp) {
        return ddp.exLenderFullName() + format.space() + ddp.exLenderAddress();
    }

    /**
     * 
     * @param root
     * @param nodeName
     * @param nodeType
     * @param nodeText
     */
    private void pCreateNode(Node root, String nodeName, String nodeType, String nodeText) {
        if (nodeText != null && nodeText.trim().length() != 0) {
            Node xNode = getTag(nodeName, nodeText);
            ((Element) xNode).setAttribute("type", nodeType);
            addTo(root, xNode);
        }
    }

    /**
     * totalFees Private method that calculates the sum of mortgage insurance fees, appraisal inspectiion fees
     * and other charges. To calculate the other charges, the method in turn calls totalOtherCharges
     * 
     * @return double - Value that contains total fees which includes mortgage insurance fees, appraisal
     *         inspection fees and other charges
     */
    private double totalFees() {
        double totalFee = 0d;
        try {
            totalFee = deal.getMIPremiumAmount() + ddp.exSelectedFeesTotalAmt(pINSPEC_FEES) + totalOtherCharges();
        } catch (Exception e) {
            String msg = "DisclosureExtractor failed while calculating the totalFees: "
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        return totalFee;
    }

    /**
     * totalOtherCharges: Private method that calculates sum of bonus discount, agent broker fees and other
     * fees. The Bonus discount value is retrieved the amount entered by the user in the disclosure document
     * screen and adds it along with the agent broker fee and other charges
     * 
     * @return double - Value that contains sum of the bonus discount, agent broker and other fees
     */
    private double totalOtherCharges() {
        double totalFee = 0d;
        double bdFee = 0d;
        if (!(ddp.exQuestion("101Q090V").equals(null)) && (!(ddp.exQuestion("101Q090V").trim().length() == 0)))
            bdFee = Double.parseDouble(ddp.exQuestion("101Q090V"));
        double agentBrokFee = ddp.exSelectedFeesTotalAmt(pMTG_BROKER_ORG_FEES);
        double otherFee = ddp.exSelectedFeesTotalAmt(pOTHER_BRK_FEE);
        try {
            totalFee = bdFee + agentBrokFee + otherFee;
        } catch (Exception e) {
            String msg = "DisclosureExtractor failed while calculating the totalOtherCharges: "
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            srk.getSysLogger().warning(this.getClass(), msg);
        }
        ;
        return totalFee;
    }

    /**
     * bonusDeductions: Private method that calculates sum of inspection fees, applciation fees, interim
     * interest amount and other lender fees. The method to calculate these fees, in turn calls
     * exSelectedFeesTotalAmt of <code>DisclosureDataProvider</code>, by passing the appropriate fee type
     * 
     * @return double - Value that contains sum of the inspection fees, application fees, interim interest
     *         amount and other lender fees for a given deal
     */
    private double bonusDeductions() {
        double _totalFee = 0;
        // Added code for bug# 1612 - start
        // commented the bonus and Mtg Insurance are not part of total Deductions - start
        _totalFee = // ddp.exSelectedFeesTotalAmt(pBONUS)+ ddp.exSelectedFeesTotalAmt(pMTG_INSUR_FEE)
        // commented the bonus and Mtg Insurance are not part of total Deductions - end
        ddp.exSelectedFeesTotalAmt(pINSPEC_FEES) + ddp.exSelectedFeesTotalAmt(pAPPLICATION_FEES)
        // +ddp.exSelectedFeesTotalAmt(pLEGAL_FEES)
            + ddp.exInterimInterestAmount() + ddp.exSelectedFeesTotalAmt(pOTHER_LENDER_FEES);
        // Added code for bug# 1612 - end
        return _totalFee;
    }

    /* Changed formatCurrency to overrided formatCurrency() for Bug # 1617 */

    protected String formatCurrency(double amt) {
        if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) {
            // English, don't use dollar sign - hard coded into template
            return DocPrepLanguage.getInstance().getFormattedCurrencyWithoutDollar(amt, lang);
        } else {
            // French
            return DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang);
        }
        /* Changes Bug# 1617 - end */
    }

    /* Added exCoBorrowerNamesLoop to generate multiple co-borrowers tag for Bug # 1606 - Start */
    /**
     * exBorrowerNamesLoop: Returns Node containing all co-borrowers associated with the deal. It gets the
     * co-borrowers by calling exCoBorrowerNamesforLoop() method of DisclosureDataProvider.
     * 
     * @param tagName - String that contains the tag name to be created in the XML
     * @return Node - containing all the co-borrower names
     */
    protected Node exCoBorrowerNamesLoop(String tagName) {
        Node elemNode = null;
        Node lineNode;

        if (elemNode == null) {
            elemNode = doc.createElement(tagName);
        }
        String[] borrowers = ddp.exCoBorrowerNamesforLoop();

        if (borrowers != null) {
            for (int lineCount = 0; lineCount < borrowers.length; lineCount++) {
                lineNode = doc.createElement("Borrower");
                if (borrowers[lineCount] != null && !borrowers[lineCount].equals("")) {
                    addTo(lineNode, "name", borrowers[lineCount]);
                    elemNode.appendChild(lineNode);
                }
            }
        }
        return elemNode;
    }

    /* Added exCoBorrowerNamesLoop to generate multiple co-borrowers tag for Bug # 1606 - End */

    /* Added exCoBorrowerNamesLoop to generate multiple co-borrowers tag for Bug # 1612 - Start */
    /**
     * exBorrowerNamesLoop Returns Node containing all co-borrowers
     * 
     * @param tagName - Name of the tag
     * @return Node
     */
    protected Node exCoBorrowerNamesAddressLoop(String tagName) {
        Node elemNode = null;
        Node lineNode;

        if (elemNode == null) {
            elemNode = doc.createElement(tagName);
        }
        String[] borrowers = ddp.exCoBorrowerNamesforLoop();
        String[] borrowerAddr = ddp.exCoBorAddress();
        if (borrowers != null) {
            for (int lineCount = 0; lineCount < borrowers.length; lineCount++) {
                lineNode = doc.createElement("Borrower");
                if (borrowers[lineCount] != null && !borrowers[lineCount].equals("")) {
                    addTo(lineNode, "name", borrowers[lineCount]);
                }
                if (borrowerAddr[lineCount] != null && !borrowerAddr[lineCount].equals("")) {
                    addTo(lineNode, "address", borrowerAddr[lineCount]);
                }
                elemNode.appendChild(lineNode);
            }
        }
        return elemNode;
    }

    /* Added exCoBorrowerNamesLoop to generate multiple co-borrowers tag for Bug # 1612 - End */

    /**
     * exBorrowerNamesLoop1: Returns Node containing primary borrower and some co-borrowers associated with
     * the deal. It gets the co-borrowers by calling exCoBorrowerNamesforLoop() method of
     * DisclosureDataProvider and returns a node containing primary borrower and co-borrowers which are having
     * odd sequence no.
     * 
     * @param tagName - String that contains the tag name to be created in the XML
     * @return Node - containing primary borrower and co-borrower names having odd sequence no.
     */
    protected Node exAllBorrowerNamesLoop1(String tagName) {
        Node elemNode = null;
        Node lineNode;

        String[] borrowers = ddp.exCoBorrowerNamesforLoop();
        if (borrowers != null) {
            elemNode = doc.createElement(tagName);
            if (ddp.exPrimaryBorrowerName() != null && !isEmpty(ddp.exPrimaryBorrowerName())) {
                lineNode = doc.createElement("Borrower");
                addTo(lineNode, "name", ddp.exPrimaryBorrowerName());
                elemNode.appendChild(lineNode);
            }

            for (int lineCount = 1; lineCount < borrowers.length; lineCount += 2) {
                lineNode = doc.createElement("Borrower");
                addTo(lineNode, "name", borrowers[lineCount]);
                elemNode.appendChild(lineNode);
            }
        }
        return elemNode;
    }

    /**
     * exBorrowerNamesLoop2: Returns Node containing some co-borrowers associated with the deal. It gets the
     * co-borrowers by calling exCoBorrowerNamesforLoop() method of DisclosureDataProvider and returns a node
     * containing co-borrowers which are having even sequence no.
     * 
     * @param tagName String which contains the tag name to be created in the XML
     * @return Node - containing co-borrower names having even sequence no.
     */
    protected Node exAllBorrowerNamesLoop2(String tagName) {
        Node elemNode = null;
        Node lineNode;

        String[] borrowers = ddp.exCoBorrowerNamesforLoop();
        if (borrowers != null) {
            elemNode = doc.createElement(tagName);

            for (int lineCount = 0; lineCount < borrowers.length; lineCount += 2) {
                lineNode = doc.createElement("Borrower");
                addTo(lineNode, "name", borrowers[lineCount]);
                elemNode.appendChild(lineNode);
            }
        }
        return elemNode;
    }

    /*
     * Added exCoBorrowerNamesLoop to generate multiple co-borrowers tag for Bug # 1606 - End
     */

    /**
     * Returns the Total Checked Fees
     * 
     * @return double
     */
    // TAR : 1612 - Added total Checked fee method.
    protected double getTotalCheckedFees() {
        double val = 0d;

        val = ddp.exSelectedFeesTotalAmt(pBONUS_FEES) + ddp.exSelectedFeesTotalAmt(pAPPLICATION_FEES)
            + ddp.exSelectedFeesTotalAmt(pAPPRAISAL_FEE) + ddp.exSelectedFeesTotalAmt(pMTG_INSUR_FEE)
            + ddp.exSelectedFeesTotalAmt(pHOLDBACK_FEES) + ddp.exSelectedFeesTotalAmt(pMTG_BROKER_FEES)
            + ddp.exSelectedFeesTotalAmt(pOTHER_CHARGES);

        return val;
    }

    /*
     * Added exCoBorrowerNamesLoop to generate multiple co-borrowers tag for Bug # 1606 - End
     */
    /**
     * exAllBorrowerNamesAddressLoop: Returns Node containing all borrowers and their corresponding addresses.
     * The method, in turn calls, exPrimaryBorrowerName, exPrimaryBorrowerAddress, exCoBorrowerNamesForLoop
     * and exCoBorAddress of <code>DisclosureDataProvider</code> class to retrieve information of all the
     * borrowers. Using the above information, the node is created in the following format: <tagName>
     * <Borrower> <name>Primary borrower Name </name> <address>Primary Borrower Address</address> </Borrower>
     * <Borrower> <name> coBorrower Name</name> <address>Co-Borrower Address</address> </Borrower>
     * .......... Repeated if more co-borrower's exist </tagName>
     * 
     * @param tagName - Name of the tag, with which the node is created
     * 
     * @return Node - Node containing name and address details of the borrower in the format mentioned above
     */
    protected Node exAllBorrowerNamesAddressLoop(String tagName) {
        Node elemNode = doc.createElement(tagName);
        Node lineNode = doc.createElement("Borrower");
        String priBorrower = ddp.exPrimaryBorrowerName();
        String priBorrowerAddr = ddp.exPrimaryBorrowerAddress();
        String[] coBorrowers = ddp.exCoBorrowerNamesforLoop();
        String[] coBorrowerAddr = ddp.exCoBorAddress();
        if (priBorrower != null && !priBorrower.equals("")) {
            addTo(lineNode, "name", priBorrower);
        }
        if (priBorrowerAddr != null && !priBorrowerAddr.equals("")) {
            addTo(lineNode, "address", priBorrowerAddr);
        }
        elemNode.appendChild(lineNode);

        // Browse through the co-borrower information and add it to the node
        if (coBorrowers != null) {
            for (int lineCount = 0; lineCount < coBorrowers.length; lineCount++) {
                lineNode = doc.createElement("Borrower");
                if (coBorrowers[lineCount] != null && !coBorrowers[lineCount].equals("")) {
                    addTo(lineNode, "name", coBorrowers[lineCount]);
                }
                if (coBorrowerAddr != null) {
                    if (coBorrowerAddr[lineCount] != null && !coBorrowerAddr[lineCount].equals("")) {
                        addTo(lineNode, "address", coBorrowerAddr[lineCount]);
                    }
                }
                elemNode.appendChild(lineNode);
            }
        }
        return elemNode;
    }
}
