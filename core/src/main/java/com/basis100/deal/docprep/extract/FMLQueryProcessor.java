package com.basis100.deal.docprep.extract;

import org.w3c.dom.*;
import MosSystem.Mc;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.deal.entity.*;
import com.basis100.xml.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.docprep.*;
import com.basis100.picklist.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

import java.util.*;
import java.io.*;


public class FMLQueryProcessor
{

  public static boolean debug = false;

  private SysLogger logger;
  private SessionResourceKit srk;

  private static String[] stdelim = {"if", "then", "else"};
  private static int[] operators = {60, 61, 62};
  private static int or = 166;
  private static int and = 38;

  public FMLQueryProcessor(SessionResourceKit srk)
  {
    this.srk = srk;
    logger = srk.getSysLogger();
  }

  public FMLQueryResult processFunction(FMLQueryResult fml, Element functionNode, FMLQueryContext ctx)
  {

    String type = "";

    if(functionNode != null)
       type = functionNode.getAttribute("type");

    if(type.equals("pattern"))
    {
      return processPattern(fml, functionNode, ctx);
    }
    if(type.equals("ifthenelse"))
    {
      try
      {
        return processITE(fml, functionNode, ctx);
      }
      catch(Exception e)
      {
    	  String msg = e.getMessage();

    	  if(msg == null) msg = "Unknown Error";

    	  msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

    	  logger.error(msg);

      }

    }

    return fml;
  }


  private FMLQueryResult processPattern(FMLQueryResult fml, Element fnode, FMLQueryContext ctx)
  {

     String pattern = fnode.getAttribute("param");

     String result = replacePattern(fml, pattern, ctx);
     
     fml.reset();
     fml.addString(result);

     return fml;

  }

  //--------------------- add by clement new feature-----------------------------
  
  public String replacePattern(FMLQueryResult fml, String pattern, FMLQueryContext ctx)
  {
	  int lang = ctx.getLanguage();
	  
     StringTokenizer tk = new StringTokenizer(pattern, "?", true);

     StringBuffer result = new StringBuffer();

     String currentToken = null;

     String lastToken = null;

     boolean in = false;

     while(tk.hasMoreTokens())
     {
       lastToken = currentToken;

       currentToken = tk.nextToken();

       if(currentToken.equals("?"))
       {
         if(in)
          in = false;
         else
          in = true;

         if(lastToken != null && lastToken.equals("?"))   //escape with ??
         {
           result.append("?");
           lastToken = null;
         }

         continue;
       }

       String val = fml.getValue(currentToken,lang);
       
       if(val == null && !in)
        result.append(currentToken);
       else if(val != null && in)
        result.append(val);
     }
	  return result.toString();
  }

  //-------------------------------------------------------------------------------

  private FMLQueryResult processITE(FMLQueryResult fml, Element fnode, FMLQueryContext ctx)throws Exception
  {
    int lang = ctx.getLanguage();

    List deferred = new ArrayList();

    String thenval = null;
    String elseval = null;

    String param = fnode.getAttribute("param");

    StreamTokenizer st = getTokenizer(param);

    List conds = new ArrayList();
    List bools = new ArrayList();


    BooleanStatement cs = null;


    String cword = null;
    int ttype = -1;
    int stcount = 0;

    while(st.nextToken() != st.TT_EOF)
    {
       cword = st.sval;
       ttype = st.ttype;
       String curvar = null;
       String curval = null;
       boolean isvar = isVar(cword);

       if(isvar)
       {
         curval = fml.getValue(parseVar(cword),lang);

         if(curval == null)
         {
           deferred.add(parseVar(cword));  //unlikely - may have to be an error
         }
       }
       else if(cword != null)
       {
         curval = cword;
       }

       if(ttype == st.TT_WORD && isStatementDelim(cword))
       {
         stcount++;

         continue;
       }

       if(stcount == 0) throw new Exception(); // stmt must start with keyword

       if(stcount == 1)
       {
         //start conditional stmts
         if(cs == null )
         {
           cs = new BooleanStatement();
           cs.add(curval);
         }
         else if(isOp(ttype))
         {
           cs.add((char)ttype);
         }
         else
         {
           cs.add(curval);
         }

         if(cs.filled() && (bools.size() == conds.size()))
         {
           conds.add(cs);
         }

         if(ttype == or || ttype == and)
         {
           cs = null;
           bools.add(new Integer(ttype));
         }
       }

       if(stcount == 2)
       {
         if(isvar)
         {
           thenval = fml.getValue(curvar,lang);

           if(thenval == null)
           {
             thenval = cword;
           }
         }
         else if(cword != null)
         {
           thenval = cword;
         }
       }

       if(stcount == 3)
       {
         if(isvar)
         {
           elseval = fml.getValue(curvar,lang);

           if(elseval == null)
           {
             elseval = cword;
           }
         }
         else if(cword != null)
         {
           elseval = cword;
         }
       }
    }


    boolean result = getConditionsResult(conds, bools);


    FMLQueryResult fmlout = new FMLQueryResult();
    FMLQueryResult sub = null;
    FMLQueryHandler fqh = null;

    if(result)
    {
      if(isVar(thenval))
      {
        fqh = new FMLQueryHandler(srk);
        sub = fqh.handleSubQuery(fml.getName(), fnode, ctx);
        fmlout.addString(sub.getValue(parseVar(thenval), lang));
      }
      else
      {
        fmlout.addString(thenval);
      }

    }
    else
    {

      if(isVar(elseval))
      {
        fqh = new FMLQueryHandler(srk);
        sub = fqh.handleSubQuery(fml.getName(), fnode, ctx);
        fmlout.addString(sub.getValue(parseVar(elseval), lang));
      }
      else
      {
        fmlout.addString(elseval);
      }

    }

    return fmlout;
  }



  boolean getConditionsResult(List conds, List bools) throws Exception  //redo this method as tree
  {
     Iterator c = conds.iterator();
     Iterator b = bools.iterator();

     boolean rt = false;
     boolean lt = false;


     BooleanStatement right;
     BooleanStatement left;

     while(c.hasNext())
     {
       right = (BooleanStatement)c.next();
       rt = right.result();

       if(bools.isEmpty()) return rt;
       else
       {
          if(b.hasNext() && c.hasNext())
          {
            left = (BooleanStatement)c.next();
            Integer i = (Integer)b.next();

            if(i.intValue() == or)
             rt = (rt || lt);
            if(i.intValue() == and)
             rt = (rt && lt);
          }

       }

     }
     return rt;

  }

  private String parseVar(String in)
  {
     if(isVar(in))
      return in.trim().substring(1);
     else
      return null;
  }

  private boolean isVar(String in)
  {
    return (in != null && in.startsWith("@"));
  }


  private StreamTokenizer getTokenizer(String in) throws Exception
  {
    StreamTokenizer st = new StreamTokenizer(new BufferedReader(new StringReader(in)));
    st.resetSyntax();
    st.wordChars(33, 37);
    st.wordChars(39, 59);
    st.wordChars(63, 90);
    st.wordChars(97, 122);

    st.ordinaryChars(60,62);
    st.ordinaryChar(or);
    st.ordinaryChar(and);
    st.quoteChar('\'');

    return st;
  }

  private boolean isStatementDelim(String in)
  {
    if(in == null) return false;

    for(int i = 0; i < stdelim.length; i++)
    {
      if(in.equals(stdelim[i]))
       return true;
    }

    return false;

  }

  private boolean isOp(int in)
  {
    return (in >= 60 && in <= 62);

  }

  /**
   * Represents a boolean statement that fills left to right as
   * 'right op left'  where operators include = < > and right and left values
   * are Strings.
   */
  class BooleanStatement
  {
    String right = null;
    String left = null;
    char op;

    public void add(String value)
    {
      if(value == null)
       return;

      if(right == null)
       right = value;
      else
       left = value;
    }

    public void add(char value)
    {
      op = value;
    }

    public boolean filled()
    {
      return (left != null);
    }

    public boolean result()throws Exception
    {
       return handleBooleanComparison(right, op, left);
    }

    public boolean handleBooleanComparison(String a, char op, String b) throws Exception
    {

      int comp = a.compareToIgnoreCase(b);

      switch(op)
      {
        case '=':
          return (comp == 0);

        case '>':
          return (comp > 0);

        case '<':
          return (comp < 0);

        default:
         throw new Exception("Invalid Operator");
      }
  }

  }





  public static void main(String[] args)  throws Exception
  {
    System.out.println("Init ...");
    /*PropertiesCache.addPropertiesFile("c:\\mos\\admin\\mossys.properties");
    ResourceManager.init("c:\\mos\\admin\\","MOS");
    SessionResourceKit srk = new SessionResourceKit("0");
    System.out.println("Init end.");  */

    int val = 0;

    val = Character.getNumericValue(' ');




   




    StreamTokenizer st = new StreamTokenizer(new BufferedReader(new StringReader("if p:o) @1a = 'Ontario' then ?@2a? else ?null?")));

    st.resetSyntax();
    st.wordChars(63, 90);
    st.wordChars(97, 122);
    st.wordChars(33, 59);
    st.ordinaryChars(60,62);
    st.quoteChar('\'');


    double curd = -1;
    String curs = null;

    while(st.nextToken() != st.TT_EOF)
    {
       curd = st.nval;
       curs = st.sval;


       if(st.ttype == st.TT_NUMBER)
       System.out.println("num: " + curd);
       else if(st.sval != null)
       System.out.println("str: " + curs);
       else
       System.out.println("other: " + st.ttype);


    }


  }





}

