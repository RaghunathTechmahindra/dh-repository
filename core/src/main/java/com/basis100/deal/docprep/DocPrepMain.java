package com.basis100.deal.docprep;

import java.io.File;

import com.basis100.log.SysLogger;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;

import config.Config;

/**
 *  Startup class for the document preparation back end systems including
 *  Queue Monitoring, Data Extraction and delivery.
 */
public class DocPrepMain
{
  public static final int DEFAULT_MONITOR_DELAY = 8;

  public static int init(String path)
  {

    try
    {
      //System.setProperty("file.encoding", "UTF-8");
      PropertiesCache.addPropertiesFile(path);
      PropertiesCache resources = PropertiesCache.getInstance();

      String initLocation; 
            
      File locate = new File("admin"); 

      String rootDir = locate.getAbsolutePath(); 

          if(rootDir.endsWith("/") || rootDir.endsWith("\\"))  
        	  initLocation = rootDir;   
           else        
        	   initLocation = rootDir + "/";
                     
      System.out.println("DOCPREP: INFO: INIT FILES LOCATION: " + initLocation);

//      String poolname =  resources.getProperty("com.basis100.resource.connectionpoolname");

//      System.out.println("DOCPREP: INFO: POOLNAME: " + poolname);

      if(!initLocation.endsWith(System.getProperty("file.separator")))
      initLocation += System.getProperty("file.separator");
//      ResourceManager.init(initLocation, poolname);
      ResourceManager.init(initLocation);

      System.out.println("DOCPREP: INFO: RESOURCE MANAGER INIT COMPLETE. ");

      SysLogger logger = ResourceManager.getSysLogger("DocPrep");
      PicklistData.init();

      logger.info("DOCPREP: INFO: resource cache init complete.");
      System.out.println("DOCPREP: INFO: PICKLIST INIT COMPLETE. ");

      return DocumentGenerationManager.getInstance().getMonitorInterval();

    }
    catch(Exception e)
    {
      String msg = "**********************************************\n";
      msg += "Failed to initialize the DocPrep System!\n";
      if(e.getMessage()!= null) msg+= e.getMessage();
      System.out.println(msg);
      e.printStackTrace();
    }

    return DEFAULT_MONITOR_DELAY;
  }

  public static void main(String[] args)
  {
   try
   {
      String path = null;

      if(args.length > 0)
      {
        path = args[0];
      }

      //if(path==null){path=new String("\\mos\\admin\\mossys.properties");}
      if(path == null){
        path = Config.getProperty("SYS_PROPERTIES_LOCATION_DOCPREP");
        path +=  "mossys.properties";
      }
      int interval = init(path);

      java.util.Date today = new java.util.Date();

      DocRequestQueueMonitor monitor = new DocRequestQueueMonitor(interval);
      //monitor.startMonitor();

      String lStartSweeper = PropertiesCache.getInstance().getProperty(-1, "com.basis100.sweeper.start","Y");
      if(lStartSweeper.equalsIgnoreCase("y")){
        DocRequestQueueSweeper sweeper = new DocRequestQueueSweeper(10);
        sweeper.startSweeper();
      }
      //DocRequestQueueSweeper sweeper = new DocRequestQueueSweeper(10);
      //sweeper.startSweeper();

      System.out.println("\n___________________________________________________________________\n");
      System.out.println("\tDocument Generation Running. ");
      System.out.println("\tBuild #9.6.0.4 :: " + today);
      System.out.println("\tDocument Queue Monitor Thread Active.");
      if(lStartSweeper.equalsIgnoreCase("y")){
        System.out.println("\tSweepr is running.");
      }else{
        System.out.println("\tSweeper is NOT running.");
      }
      System.out.println("_______________________________________________________________________\n");
   }
   catch(Throwable t)
   {
     System.out.println("DOCPREP: ERROR: Failure in DocPrep Module.");
     t.printStackTrace();
   }

  }

}

