package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
/**
 * Title: ComponentNote
 * <p>
 * Description: This class is used to extract the component note for the preapproval certificate.
 * @author MCM Impl Team.
 * @version 1.0 13-Aug-2008 XS_16.5 Initial version
 * @version 1.1 21-Aug-2008 XS_16.26 Language check added for component note
 * @version 1.2 21-Nov-2008 FXP23545 Modified to read the text from BXResources
 */
public class ComponentNote extends DocumentTagExtractor
{
    /**
     * Description: This mthod returns the component note based on the deal product type
     * @author MCM Impl Team.
     * @version 1.0 13-Aug-2008 XS_16.5 Initial version
     * @version 1.1 21-Aug-2008 XS_16.26 Language check added for component note
     */
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();
        int clsid = de.getClassId();

        try
        {
            if(clsid == ClassId.DEAL)
            {
                Deal deal = (Deal) de;
                String componentNote="";
                String componentEligibleFlag = deal.getMtgProd()
                        .getComponentEligibleFlag();
                if (componentEligibleFlag.equals("Y"))
                {
                    componentNote = BXResources.getSysMsg("COMPONENT_NOTE_MESSAGE", lang);
                }
                else
                {
                    componentNote = "";
                }
                fml.addValue(componentNote, fml.STRING);
            }

        }
        catch(Exception e)
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);
            throw new ExtractException(msg);
        }

        return fml;
    }
}
