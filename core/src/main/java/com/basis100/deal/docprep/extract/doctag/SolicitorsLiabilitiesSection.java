package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.collections.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.calc.*;
import java.util.*;
import MosSystem.Mc;


public class SolicitorsLiabilitiesSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    List out = null;
    List outFiltered = null;
    int clsid = de.getClassId();

    try
    {

      if(clsid == ClassId.DEAL)
      {
        Liability l = new Liability(srk,null);

        Deal deal = (Deal)de;
        out = (List)l.findByDealAndPayoffType( (DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS ) ;
        List outClosed = (List)l.findByDealAndPayoffType( (DealPK)deal.getPk(), Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE  ) ;
        out.addAll(outClosed);

        outFiltered = filterLiabilities(out);

        fml.setValues(outFiltered,fml.ENTITY);
        return fml;
      }

    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }


   return fml;
  }

  //========================================================================================
  // filtering liabilities comes as per of Joe's request. If the liability's percentoutgds
  // is less than 2 then it is"normal" liability, otherwise it is considered to be
  // credit bureau liability.
  //=========================================================================================
  private List filterLiabilities(List parList){
    ListIterator li = parList.listIterator();
    List retList = new java.util.ArrayList();

    while( li.hasNext() ){
      Liability lLiability = (Liability)li.next();
      //System.out.println("ZR: liability :" + lLiability.getPercentOutGDS() );
      if( lLiability.getPercentOutGDS() < 2 ){
        retList.add(lLiability);
      }
    }
    return retList;
  }

}
