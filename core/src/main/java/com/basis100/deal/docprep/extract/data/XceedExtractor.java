package com.basis100.deal.docprep.extract.data;

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.xml.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.util.collections.*;
import com.basis100.picklist.*;
import java.util.*;
import MosSystem.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.pk.*;
import com.basis100.entity.FinderException;

/**
 * Base class for Xceed document types
 *
 */
public abstract class XceedExtractor extends GENXExtractor
{
    protected abstract void buildXMLDataDoc() throws Exception;

    /**
     * Retrieves the last name of the primary borrower from the deal and formats an XML tag with it.
     *
     * @return The completed last name XML tag.
     *
     */
    protected Node extractBorrowerLastName()
    {
        String val = null;

        try
        {
            val = getPrimaryBorrower().getBorrowerLastName();
        }
        catch (Exception e)  {}

        return getTag("BorrowerLastName", val);
    }

    protected Node extractApprovalDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getCommitmentIssueDate());
        }
        catch (Exception e)  {}

        return getTag("ApprovalDate", val);
    }

    protected Node extractLenderFund()
    {
        return getTag("LenderNameFund", "Xceed Funding Corporation");
    }

    protected Node extractCareOf()
    {
        String val = null;

        try
        {
            val = (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS) ?
                  "From:" : "C/O:";
        }
        catch (Exception e){}

        return getTag("CareOf", val);
    }
}