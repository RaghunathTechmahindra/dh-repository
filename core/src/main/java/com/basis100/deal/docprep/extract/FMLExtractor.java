package com.basis100.deal.docprep.extract;

import org.w3c.dom.*;
import MosSystem.Mc;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;

import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;

import com.basis100.xml.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.util.*;

import java.util.*;
import java.io.*;

 public class FMLExtractor implements Extractor
 {

   private ExtractDom extractInfo;

   private SessionResourceKit srk;
   private SysLogger logger;
 //  private DocumentContext ctx;
   private Document xmlDocument;
   private Document fmlDocument;
   private String codePackage;
   private int lang;
 //  private DocumentFormat format;

   public static int LOG = 1;
   public static int CRITICAL = 2;
   public static int IGNORE = 3;


   public FMLExtractor(SessionResourceKit srk, ExtractDom extractInfo)
   {
     this.srk = srk;
     logger = srk.getSysLogger();
     fmlDocument = extractInfo.getDocument();
     this.extractInfo = extractInfo;
     codePackage = extractInfo.getCodePackage();
   }

   public String extractXmlString(DocumentGenerationHandler handler) throws ExtractException
   {
      try
      {
        buildDocument(handler);

        XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

        return xmlwr.printString(xmlDocument);

      }
      catch(Exception e)
      {
         throw new ExtractException( "DocPrep - FML Data Extraction Failed." );
      }

   }

   private Document buildDocument(DocumentGenerationHandler handler)throws Exception
   {
      DocumentProfile profile = handler.getDocumentProfile();

      MasterDeal md = new MasterDeal(srk,null,handler.getDealId());

      Deal deal = new Deal(srk, null, md.getDealId(), md.getGoldCopyId());
      DocumentContext ctx = new DocumentContext(profile,deal);

      lang = determineDocumentLanguage(extractInfo, deal);

      DocumentFormat format = ctx.getFormat();

      //build the document - create rootnode

      xmlDocument = XmlToolKit.getInstance().newDocument();
      Element root = xmlDocument.createElement("Document");
      root.setAttribute("type", String.valueOf(profile.getDocumentTypeId()));
      root.setAttribute("lang", String.valueOf(lang));
      xmlDocument.appendChild(root);

      List nodes = DomUtil.getElementList(fmlDocument);
      ListIterator it = nodes.listIterator();
      Element current = null;
      String name = null;

      while(it.hasNext())
      {
        current = (Element)it.next();

        Element newNode = null;

        if(current.getNodeType() == Node.ELEMENT_NODE)
        {
          newNode = buildNodeFrom(current, deal, ctx);
        }

        if(newNode != null)
        {
           name = newNode.getNodeName();

           if(name.equals("TEMP"))
           {
             DomUtil.moveChildren(newNode, root, xmlDocument);
           }
           else
           {
             root.appendChild(newNode);
           }
        }

      }

      return xmlDocument;
   }

   /**
    *  Builds an tidx Node from the parameter mdfml Node
    */
   public Element buildNodeFrom(Element node, DealEntity de, DocumentContext ctx) throws Exception
   {
     //get tag name and determine parent type
      String nodeType = node.getNodeName();
      String parentType = node.getParentNode().getNodeName();

    try
    {
      if(parentType.equals("Document"))
      {
        if(nodeType.equals("Tag"))
        {
           return buildTagNode(node, de, ctx);
        }
        else if(nodeType.equals("Section"))
        {
           return buildSectionNode(node, de, ctx);
        }
        else if(nodeType.equals("Table"))
        {
           return buildTableNode(node, de, ctx);
        }
        else if(nodeType.equals("ReplaceableSection"))
        {
           return buildReplaceableSectionNode(node, de, ctx);
        }
      }
    }
    catch(ClassNotFoundException cnfe)
    {
      String msg = "DOCPREP: Query Class not found: " + cnfe.getMessage();
      logger.error(msg);
      System.out.println(msg);
    }

    return null;
  }

  public int getAbsentErrorType(Node node)
  {
    if(!node.getNodeName().equals("Tag"))return -1;

    Node ae = DomUtil.getFirstNamedChild(node,"AbsentError");

    String type = DomUtil.getAttributeValue(ae,"type");

    if(type.equals("LogOnly"))
      return this.LOG;
    else if(type.equals("Critical"))
      return this.CRITICAL;
    else if(type.equals("Ignore"))
      return this.IGNORE;
    else return -1;
  }


  public Element buildTagNode(Node node, DealEntity deal, DocumentContext ctx)throws Exception, ClassNotFoundException
  {
    String tagName = getAttribute(node,"name");
    FMLQueryContext queryctx = null;
    FMLQueryResult res = null;
    FMLQueryHandler queryHandler = new FMLQueryHandler(srk);

    if(deal != null)
    {
      queryctx = new FMLQueryContext(deal, codePackage, lang, ctx.getFormat());
      res = queryHandler.handleQuery(tagName, node, queryctx);
    }
    else
    {
      res = new FMLQueryResult();
    }

    if(!res.hasNonEmptyValue())
    {
       String absentError = getChildNodeValue(node,"AbsentError");
       int absentAction = getAbsentErrorType(node);

       if(absentAction == LOG)
       {
          srk.getSysLogger().error("DocPrep Error: tagName: " + tagName + ": " + absentError);
       }
       else if(absentAction == IGNORE)
       {
         srk.getSysLogger().error("DocPrep Info: tagName: " + tagName + ": " + absentError);
       }
       else if(absentAction == CRITICAL)
       {
          String msg = "DocPrep: Critical Error in Data extraction...\n" +
                        "tagName: " + tagName + " - " + absentError;

          throw new DocPrepException(msg);

       }
    }

    return tagNode(tagName,res);
  }

  private Element tagNode(String tagName, FMLQueryResult fml)
   throws Exception
  {
     if(tagName == null)
       return null;

     List values = fml.getStringValues(lang);

     int valnodes = values.size();

     //create the DocTag element
     Element container = xmlDocument.createElement("DocTag");
     setAttribute(container, "name", tagName);
     int occurs = 0;

     if(valnodes > 0)
     {
        Iterator it = values.iterator();
        String data = null;

        while(it.hasNext())
        {
           data = (String)it.next();
           
           if(data == null || data.trim().length() == 0)
           {
             continue;
           }

           Node valueNode = xmlDocument.createElement("TagValue");

           Node text = xmlDocument.createTextNode(data);

           occurs++;

           valueNode.appendChild(text);

           container.appendChild(valueNode);
        }

     }

     setAttribute(container, "occurence", String.valueOf(occurs));

     return container;
  }
  /**
   *  builds a section node with child section sets and doc tags
   */
  public Element buildSectionNode(Node node, DealEntity deal, DocumentContext ctx)throws Exception, ClassNotFoundException
  {
    FMLQueryHandler queryHandler = new FMLQueryHandler(srk);

      //get the section name
    String name = getAttribute(node,"name");

    boolean hide = false;

    String hatt = getAttribute(node,"hide");

    if(hatt != null && hatt.equals("true"))
     hide = true;

    FMLQueryContext queryctx = new FMLQueryContext(deal, codePackage, lang, ctx.getFormat());
    queryctx.isSection(true);
    FMLQueryResult res = queryHandler.handleQuery(name, node, queryctx);

    //get the format instructions
    String formatAttr = getAttribute(node,"format");

    //get the value

    List entities = (List)res.getEntities();
    int size = entities.size();
    String occ = String.valueOf(size);

    //create the root element and set it's attribs
    Element docSection = xmlDocument.createElement("DocSection");
    setAttribute(docSection, "name",name);
    setAttribute(docSection, "occurence",occ);
    setAttribute(docSection, "format",formatAttr);

    List childTags = DomUtil.getNamedChildren(node, "Tag");
    List childSections = DomUtil.getNamedChildren(node, "Section");
    List allTags = new ArrayList(childTags);
    allTags.addAll(childSections);

    Element tempContainer = xmlDocument.createElement("TEMP") ;

    if(size <= 1 && hide)
    {
      DealEntity inputEntity = null;

      if(size == 1)
       inputEntity = (DealEntity)entities.get(0);

       Iterator tit = childTags.iterator();
       Element currentEl = null;

       while(tit.hasNext())
       {
         currentEl = (Element)tit.next();

         tempContainer.appendChild(buildTagNode(currentEl,inputEntity, ctx));
       }

       return tempContainer;
    }

    Iterator it = entities.iterator();

    DealEntity next = null;
    int count = 1;
    String ord = null;

    while(it.hasNext())
    {
      next = (DealEntity)it.next();

      Iterator tagit = allTags.iterator();

      ord = String.valueOf(count++);

      Element sectionSet = xmlDocument.createElement("SectionSet");
      setAttribute(sectionSet, "ordinal",ord);
      Node currentTag = null;
      String currentType = null;

      while(tagit.hasNext())
      {
         currentTag = (Node)tagit.next();
         currentType = currentTag.getNodeName();

         if(currentType.equals("Tag"))
         {
           Node tag = this.buildTagNode(currentTag,next,ctx);
           sectionSet.appendChild(tag);
         }
         else if(currentType.equals("Section"))
         {
           Node sect = this.buildSectionNode(currentTag, next, ctx);
           sectionSet.appendChild(sect);
         }
      }

      docSection.appendChild(sectionSet);
    }

    return docSection;

  }

  public Element buildReplaceableSectionNode(Node node, DealEntity deal, DocumentContext ctx)throws Exception, ClassNotFoundException
  {

 
     //get the section name
    String name = getAttribute(node,"name");
     //get the format instructions
    String formatAttr = getAttribute(node,"format");
     //get the occurenceType
    String occurenceType = getAttribute(node, "occurrenceType");
    //get the code
    String query = getAttribute(node,"query");

    if(codePackage != null)
    query = codePackage + "." + query;

    Class queryclass = Class.forName(query);
    ReplaceableSectionTagExtractor sectionQuery = (ReplaceableSectionTagExtractor)queryclass.newInstance();

    //run the section query
    FMLQueryResult result = sectionQuery.extract((Deal)deal, lang, ctx.getFormat(), srk);
    //get info from result
    int action = result.getAction();


    List entities = (List)result.getEntities();

    // create the container element
    Element section = xmlDocument.createElement("ReplaceableSection");
    setAttribute(section, "name", name);
    setAttribute(section, "format", formatAttr);
    setAttribute(section, "occurence", String.valueOf(entities.size()));

    String replacePath = result.getFileResult();;

    if(action != ReplaceableSectionTagExtractor.DONT_REPLACE)
    {

      if(replacePath == null)
      {
        Node repNode = DomUtil.getFirstNamedChild(node,"Replacement");

        replacePath = getAttribute(repNode,"path");
      }

      replacePath = adjustForLanguage(replacePath,lang);

    }

    // create children - either SectionSet or Replacement



    if(action == ReplaceableSectionTagExtractor.DONT_REPLACE)
    {
      // set the attributes
      setAttribute(section, "replace", "false");
      setAttribute(section, "parse", "false");

      // dont replace so we are creating SectionSets filled with DocTags
      Iterator it = entities.iterator();

      DealEntity next = null;
      int count = 1;
      String ord = null;

      while(it.hasNext())
      {
        next = (DealEntity)it.next();


        List childTags = DomUtil.getNamedDescendants(node, "Tag");

        Iterator si = childTags.iterator();

        ord = String.valueOf(count);
        Element sectionSet = xmlDocument.createElement("SectionSet");
        sectionSet.setAttribute("ordinal",ord);

        while(si.hasNext())
        {
           Node tag = this.buildTagNode((Node)si.next(), next, ctx);
           sectionSet.appendChild(tag);
        }

        section.appendChild(sectionSet);
      }

    }
    else if(action == ReplaceableSectionTagExtractor.REPLACE_ONLY)
    {
       // set the attributes
      setAttribute(section,"replace", "true");

      setAttribute(section,"parse", "false");
        // replace only so we are creating Replacement elements
      // get the info from the replacement node in the description


      Iterator it = entities.iterator();

      while(it.hasNext())
      {
        Element replacement = xmlDocument.createElement("Replacement");

        if(replacePath != null && replacePath.length() > 0)
          replacement.setAttribute("path",replacePath);

        section.appendChild(replacement);
        it.next();
      }
    }
    else if(action == ReplaceableSectionTagExtractor.REPLACE_AND_PARSE)
    {
      // set the attributes
      setAttribute(section,"replace", "true");

      setAttribute(section,"parse", "true");
      
      // replace and parse so we are creating Replacement elements and child tags
      // get the info from the replacement node in the description

      Iterator it = entities.iterator();

      DealEntity next = null;
      int count = 1;
      String ord = null;

      while(it.hasNext())
      {
        next = (DealEntity)it.next();

        List childTags = DomUtil.getNamedDescendants(node, "Tag");

        Iterator si = childTags.iterator();

        ord = String.valueOf(count++);
        Element rep = xmlDocument.createElement("Replacement");
        setAttribute(rep,"path",replacePath);
        setAttribute(rep,"ordinal",ord);


        while(si.hasNext())
        {
           Node tag = this.buildTagNode((Node)si.next(), next, ctx);
           rep.appendChild(tag);
        }

        section.appendChild(rep);
      }
    }
    else
    {
      setAttribute(section,"replace", "false");
      setAttribute(section,"parse", "false");
    }



    return section;

  }

  public Element buildTableNode(Node node, DealEntity deal, DocumentContext ctx) throws Exception, ClassNotFoundException
  {

    List sectionTypes = DomUtil.getNamedDescendants(node, "Section");
    int numcols = sectionTypes.size();
    String name = getAttribute(node,"name");

    //create the root element and set it's attribs
    Element table = xmlDocument.createElement("DocTable");
    setAttribute(table,"name", name);
    setAttribute(table,"columns",String.valueOf(numcols));

    Iterator it = sectionTypes.iterator();
         
    Node sectionNode = null;
    Node docSectionNode = null;

    while(it.hasNext())
    {
      sectionNode = (Node)it.next();
      docSectionNode = buildSectionNode(sectionNode, deal, ctx);

      if(docSectionNode != null)
       table.appendChild(docSectionNode);
    }

    return table;
  }


  public String getChildNodeValue(Node node, String tagname)
  {
    return DomUtil.getChildValue(node,tagname);
  }

  public String getAttribute(Node node,String name)
  {
    return DomUtil.getAttributeValue(node,name);
  }

  private void setAttribute(Element el, String attribute, String name)
    throws Exception
  {
    if(attribute == null)
       new Exception("mdfml file: critical attribute missing: " + el.getTagName());

    if(attribute.equals("name") || attribute.equals("path")
      || attribute.equals("occurence") || attribute.equals("columns"))
    {
     if(name == null || name.length() == 0)
      throw new Exception("mdfml file: critical attribute missing: " + attribute);
    }
    else if(attribute.equals("format"))
    {
      if(name == null || name.length() == 0)
        name = "removeTag";
    }
    else if( attribute.equals("replace")|| attribute.equals("parse"))
    {
      if(name == null || !(name.equals("true") || name.equals("false")))
        throw new Exception("mdfml file: critical attribute missing or invalid: " + attribute);
    }
    else if( attribute.equals("ordinal"))
    {
      if(name == null || name.length() == 0)
        return ;
    }
 /* else if( )
    {
      if(name == null)
        throw new Exception("mdfml file: critical attribute missing or invalid: " + attribute);

    } */

    el.setAttribute(attribute,name);
  }

  private int determineDocumentLanguage(ExtractDom handler, Deal deal)
  {
    String str = handler.getPreferredLangAttrib();

    if(str.equalsIgnoreCase("PrimaryBorrower"))
    {
      int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);

      if(lang == -1)
      {
        String message = "DOCPREP: ERROR: Unable to determine document language: ";
        message += "Document schema indicates use Primary Borrower - is this flag set for DealId: ";
        message += deal.getDealId();
        logger.error(message);
        System.out.println(message);
        return 0;
      }
      else
      {
        return lang;
      }
    }
    else if(str.equalsIgnoreCase("French"))
    {
      return DocPrepLanguage.FRENCH;
    }

    return DocPrepLanguage.ENGLISH;

  }

  private String adjustForLanguage(String replaceableSection, int lang)
  {

     if(replaceableSection == null || lang < 0) return replaceableSection;

     replaceableSection = replaceableSection.trim();
     int len = replaceableSection.length();

     if(len == 0) return replaceableSection;

     if(replaceableSection.endsWith("_f") || replaceableSection.endsWith("_F"))
     {
       len -= 2;
       replaceableSection = replaceableSection.substring(0,len);
     }

     if(lang == 1) return replaceableSection + "_f";
     else return replaceableSection;

  }

  

}




