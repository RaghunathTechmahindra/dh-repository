package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class SolicitorsPackEmployerPhone extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    try
    {

      if(de.getClassId() == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        fml = (new EmployerPhone()).extract(de, lang, format, srk);

        String val = fml.getFirstStringValue(lang);

        ConditionHandler ch = new ConditionHandler(srk);
        String verb = ch.getVariableVerbiage(deal,Dc.BUSINESS_VERB,lang);

        if(val != null && verb != null)
        {
          fml.addValue(verb + val, fml.STRING);
          return fml;
        }

      }
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return new FMLQueryResult();
  }
}
