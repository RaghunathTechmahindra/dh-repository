package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.deal.calc.*;
import MosSystem.*;

public class ProductType extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();
        int clsid = de.getClassId();

        try
        {
            if(clsid == ClassId.DEAL)
            {
                Deal deal = (Deal)de;

                MtgProd prod = deal.getMtgProd();

                if (prod == null)
                   return null;

                fml.addString(prod.getMPBusinessId(), null);
            }
        }
        catch (Exception e)
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);

            if (e instanceof com.basis100.entity.FinderException)
               fml.addString("", null);
            else
               throw new ExtractException(msg);
        }

        return fml;
    }
}
