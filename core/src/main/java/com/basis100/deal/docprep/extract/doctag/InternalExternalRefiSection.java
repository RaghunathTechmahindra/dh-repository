package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class InternalExternalRefiSection extends ReplaceableSectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de , int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Deal deal = (Deal)de;

    int dp = deal.getDealPurposeId();

    if(dp == Mc.DEAL_PURPOSE_REFI_EXTERNAL)
    {
        return new FMLQueryResult(deal, REPLACE_AND_PARSE , "Refinance_A");
    }
    else if(dp ==  Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
    {
       return new FMLQueryResult(deal, REPLACE_AND_PARSE , "Refinance_B");
    }

    return new FMLQueryResult();
  }
}
