package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class BranchFax extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";

    Deal deal = (Deal)de;

    int bid = deal.getBranchProfileId();
    int lid = deal.getLenderProfileId();

    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      LenderToBranchAssoc assoc = new LenderToBranchAssoc(srk);
      List list =(List)assoc.findByBranchAndLender(new BranchProfilePK(bid),new LenderProfilePK(lid));

      if(!list.isEmpty())
      {
        assoc = (LenderToBranchAssoc)list.get(0);

        if(assoc == null)  return new FMLQueryResult();

        if(lang == DocPrepLanguage.ENGLISH)
        {
          val = assoc.getETFaxNumber();

          if(val == null || val.length() < 10)
          {
            val = assoc.getELFaxNumber();

            if(val == null || val.length() < 10)
            {
              return new FMLQueryResult();
            }
          }

        }
        else if(lang == DocPrepLanguage.FRENCH)
        {
           val = assoc.getFTFaxNumber();

          if(val == null || val.length() < 10)
          {
            val = assoc.getFLFaxNumber();

            if(val == null || val.length() < 10)
            {
              return new FMLQueryResult();
            }
          }
        }


        ComponentResult  result = new ComponentResult();
        result.addResult("Fax: ",fml.TERM);
        result.addResult(val,fml.PHONE_NUMBER);
        fml.addComponentResult(result);
      }

    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
