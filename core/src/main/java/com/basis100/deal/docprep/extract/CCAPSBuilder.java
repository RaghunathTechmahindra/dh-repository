package com.basis100.deal.docprep.extract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docprep.DocumentGenerationHandler;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.AddressLineParser;
import com.basis100.deal.util.StreetAddress;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.BorrowerNumBorrowerComparator;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;

/**
 * <p>Title: </p>
 *
 * <p>Description: this class is based on UploadExtractor but does not implement Extractor interface </p>
 * <p>Copyright: Copyright filogix(c) 2005</p>
 * <p>Company: filogix inc.</p>
 *
 * @author Catherine Rutgaizer
 * @version 1.0 (Initial version Jan 05)
 */
public class CCAPSBuilder
{
  public static boolean             _debug              = true;

  // ------------------ constant values -------------------------------

  private static final String       DOCUMENT_NODE_NAME = "Document";
  private static final String       BORROWER_NODE_NAME = "Borrower";
  private static final String       DEAL_NODE_NAME     = "Deal";
  private static final String       BORROWER_NUMBER    = "borrowerNumber";
  private static final String       EMPLOYMENT_HISTORY = "EmploymentHistory";
  private static final String       JOB_TITLE_NODE     = "jobTitle";
  private static final String       NEW_JOB_TITLE_NODE = "JobTitle";  // Note: capital "J"!!

  // ------------------ end constant values ---------------------------

  private SessionResourceKit        _srk;
  private SysLogger                 _logger;
  private ExtractDom                _template;
  private Deal                      _deal;
  private DocumentGenerationHandler _dgHandler;

  private List                      _borrowers      = null;
  private Node                      _sortedBorrowersNode;
  private UploadBuilder             _generator;

  // control section
  private int                       _maxApplications    = 0;

  public CCAPSBuilder(SessionResourceKit srk, ExtractDom dom, DocumentGenerationHandler dgHandler)
  {
    this._srk = srk;
    this._logger = srk.getSysLogger();
    this._template = dom;
    this._dgHandler = dgHandler;

    _deal = dgHandler.getDeal();
  }

  /**
   * buildApps
   *
   * @return a Collection of CCAPS application xml strings, max of 3;
   * map key contains an applicationSequenceNumber
   * @throws DocPrepException
   */
  public Map buildApps() throws DocPrepException
  {
    Map result = new TreeMap();

    try
    {
      // 1) get borrowers
      getSortedBorrowers();

      // 2) build control nodes
      Map sectMap = getControlSections();
      _maxApplications = sectMap.size();

      for (int i = 1; i < _maxApplications + 1; i++)
      {
        // 3) extract upload dom based on provided template
        Document appDoc = extractDom();
        // logger.trace(this.getClass() + ": " + extractXmlString(appDoc));

        // sort borrower nodes
        // -----------------------------
        // Catherine, Feb 14, 2005 : according to Dave K. there is no need to sort borrower nodes in <Deal> section 
        replaceBorrowerNodes(appDoc);
        renameJobTitleNode(appDoc);                                                           // PVCS # 963

        // get deal node
        Node docRootNode = DomUtil.getFirstNamedDescendant(appDoc, DOCUMENT_NODE_NAME); 
        // get Control section node
        AppControlSection thisCtrl = (AppControlSection)sectMap.get(new Integer(i));
        
        docRootNode.insertBefore((Node) thisCtrl.getSectionNode(appDoc), docRootNode.getFirstChild());

        result.put(new Integer(i), extractXmlString(appDoc));
      }
    }
    catch (ExtractException ee)
    {
      String lMsg = "Could not build CCAP applications: " + ee.getClass() + ": " + ee.getMessage();
      _logger.error(lMsg);
      throw new DocPrepException(lMsg);
    }
    return result;
  }

  private void getSortedBorrowers() throws DocPrepException
  {
    try
    {
      // get borrowers and sort the list
      _borrowers = new ArrayList(_deal.getBorrowers());
      /* // debug
       Iterator it = srtdBorrwList.iterator();
       while (it.hasNext()){
       Borrower br = (Borrower) it.next();
       System.out.println("borrowerNum =  " + br.getBorrowerNumber());
       }
       System.out.println("======================================");
       //*/

      Collections.sort(_borrowers, new BorrowerNumBorrowerComparator());
      /* // debug
       Iterator it2 = srtdBorrwList.iterator();
       while (it2.hasNext()){
       Borrower br = (Borrower) it2.next();
       System.out.println("borrowerNum =  " + br.getBorrowerNumber());
       }
       //*/
    } catch (Exception e)
    {
      String lMsg = "Could not get Borrowers: " + e.getMessage();
      _logger.error(lMsg);
      throw new DocPrepException(lMsg);
    }
  }

  /**
   * @param doc
   * @return
   */
  private String extractXmlString(Document doc)
  {
    return _generator.toXMLString(doc, _dgHandler.getLanguagePreferenceId());
  }

  /**
   * getControlSections
   *
   * @return Map
   * returns HashMap of Nodes
   */
  private Map getControlSections()
  {
    Map resultMap = new HashMap(3);

    boolean bJnt = false;
    int iTotBors;
    int iNumBors;
    int iAppCount = 0;
    Borrower br1 = null, br2 = null;
    AppControlSection ctrl = null;

    iTotBors = _borrowers.size();
    Iterator it = _borrowers.iterator();
    Object[] arBors = _borrowers.toArray();

    /*// debug
     while (it.hasNext()){
     myDebug("borrower =" + ((Borrower)it.next()).getBorrowerNumber());
     }
     // debug end //*/

    if (iTotBors > 4)
    {
      iNumBors = 4;
    } else
    {
      iNumBors = iTotBors;
    }
    debugPrint("Total borrowers = " + iTotBors + "; number of borrowers = " + iNumBors);

    int iCurBor = 0;

    while ((iAppCount < 3) && (iCurBor + 1 <= arBors.length))
    {
      debugPrint("--------------------- Processing application #" + (iAppCount + 1)); // debug
      br1 = (Borrower) arBors[iCurBor];
      if ((iCurBor + 2 ) <= iNumBors ) {
        br2 = (Borrower) arBors[iCurBor + 1];
      }
      
      debugPrint("Br1.BorrowerNumber = " + br1.getBorrowerNumber());
      if ((iCurBor + 2 ) <= iNumBors ) {
        debugPrint("Br2.BorrowerNumber = " + br2.getBorrowerNumber());
      }  

      // 1) determine if joint
      if ( br2 == null || (iCurBor + 2) > iNumBors) // && ((iCurBor + 1 ) <= iNumBors ))
      {
        bJnt = false;
      } else
      {
        bJnt = isJoint(br1, br2);
        debugPrint("isJoint = " + bJnt); // debug
      }
      ctrl = new AppControlSection(++iAppCount);

      switch (iCurBor)
      {
        case 0:
        {
          ctrl.setPrimaryAppInd("P");
          break;
        }
        default:
        {
          ctrl.setPrimaryAppInd("C");
          break;
        }
      }
      debugPrint("Primary Application index = " + ctrl.getPrimaryAppInd()); // debug

      ctrl.setPrimeApplicant(br1.getBorrowerNumber());

      // 2) identify app type (I, IO, J, JO)
      if (bJnt)
      {
        if (iNumBors == 2)
        {
          ctrl.setType("J");
        } else
        {
          ctrl.setType("JO");
        }
        ctrl.setCoApplicant(br2.getBorrowerNumber());
        iCurBor += 2;
      } else
      {
        if (iNumBors == 1)
        {
          ctrl.setType("I");
        } else
        {
          ctrl.setType("IO");
        }
        iCurBor += 1;
      }
      debugPrint("Application type = " + ctrl.getType()); // debug

      resultMap.put(new Integer(iAppCount), ctrl);
    }

    int iSize = resultMap.size();
    // debug ----------
    debugPrint(iSize + " CCAPS application(s) created");

    Iterator mIt = resultMap.keySet().iterator();
    while (mIt.hasNext())
    {
      debugPrint("key = " + mIt.next());
    }
    // debug end ----------

    // 3) notUsedBorrowerList
    debugPrint("Adding not used borrowers: "); // debug
    ctrl = (AppControlSection) resultMap.get(new Integer(1));

    int j = 1;
    while ((iCurBor + 1 <= iTotBors) && (arBors.length != 1)) 
    {
      int num = ((Borrower) arBors[iCurBor]).getBorrowerNumber();
      ctrl.getNotUsed().add("" + num);
      debugPrint("            NotUsed [" + j + "]" + " = " + num); //    	debug
      iCurBor++;
      j++;
    }

    // 4) max apps and related applicants
    
    if (iSize > 1)
    {
      int prAp1, prAp2, prAp3;
      prAp1 = ((AppControlSection) resultMap.get(new Integer(1))).getPrimeApplicant();
      prAp2 = ((AppControlSection) resultMap.get(new Integer(2))).getPrimeApplicant();

      ((AppControlSection) resultMap.get(new Integer(1))).setRelatedApplicant(prAp2);
      ((AppControlSection) resultMap.get(new Integer(1))).setMaxApps(iSize);
      ((AppControlSection) resultMap.get(new Integer(2))).setMaxApps(iSize);

      switch (iSize)
      {
        case 2:
        {
          ((AppControlSection) resultMap.get(new Integer(2))).setRelatedApplicant(prAp1);
          break;
        }

        case 3:
        {
          prAp3 = ((AppControlSection) resultMap.get(new Integer(3))).getPrimeApplicant();
          ((AppControlSection) resultMap.get(new Integer(2))).setRelatedApplicant(prAp3);
          ((AppControlSection) resultMap.get(new Integer(3))).setRelatedApplicant(prAp1);

          ((AppControlSection) resultMap.get(new Integer(3))).setMaxApps(iSize);
          break;
        }
      }
    } else {  // one applicant only
      ((AppControlSection) resultMap.get(new Integer(1))).setMaxApps(iSize);      
    }

    return resultMap;
  }

  /**
   * extractDom - gets the original upload DOM
   *
   * @throws ExtractException
   */
  private Document extractDom() throws ExtractException
  {
    Document doc = null;
    try
    {
      DocumentContext ctx = new DocumentContext(_dgHandler.getDocumentProfile(), _deal);
      _generator = new UploadBuilder(_template, _srk);
      doc = _generator.generateDocument(_deal.getDealId(), _deal.getCopyId(), _dgHandler.getLanguagePreferenceId());
      return doc;
    } catch (Exception e)
    {
      _logger.error(StringUtil.stack2string(e));
      String msg = e.getMessage();
      _logger.error(msg);
      throw new ExtractException("DOCPREP: UploadExtractor failed: " + msg);
    }
  }

  /**
   * sortBorrowers
   *
   * sorts borrower nodes based on Dave's requirements (7jan05):
   * The borrowers must be ordered by Deal.Borrower.primaryBorrowerFlag (descending)
   * then Deal.Borrower.borrowerNumber (ascending):
   * 	primaryBorrowerFlag values are Y,N,C (Yes, No, Co-applicant)
   *
   * then replaces original Borrower nodes with sorted nodes
   */

  private void replaceBorrowerNodes(Document app)
  {
    Collection newNodes;
    try
    {
      NodeList orig = app.getElementsByTagName(BORROWER_NODE_NAME);

      List removedNodes = new ArrayList();
      Node parent = DomUtil.getFirstNamedDescendant(app, DEAL_NODE_NAME);

      Node ndBefore = orig.item(orig.getLength() - 1).getNextSibling();
      
      Node nd = null;
      while (orig.getLength() > 0)
      {
        nd = parent.removeChild(orig.item(orig.getLength() - 1));
        removedNodes.add(nd);
      }
      // System.out.println("removed nodes count = " + removedNodes.size());
      newNodes = getSortedBorrowerNodes(removedNodes);

      int j = 0;
      Node dup = null;
      Iterator ndIt = newNodes.iterator();
      while (ndIt.hasNext())
      {
        dup = (Node)ndIt.next();
        ndBefore.getParentNode().insertBefore(dup, ndBefore);
        j++;
      }
      // System.out.println(j + " node(s) inserted");  // debug
    }
    catch (Error er) {
      String lMsg = "error: " + er.getClass() + ": " + er.getMessage();
      _logger.error(lMsg);
    } 
    catch (Exception ex)
    {
      String lMsg = this.getClass() + ": dealId = " + _deal.getDealId() + ", copyid = " + _deal.getCopyId() + ". Exception: " + ex.getMessage();
      _logger.error(lMsg);
      _logger.error(StringUtil.stack2string(ex));
    }
  }

  private Collection getSortedBorrowerNodes(List brwNodesList) throws DocPrepException
  {
    int brNum, val;

    ArrayList brwNodesSortedList = new ArrayList(_borrowers.size());

    try
    {
      _logger.trace(this.getClass() + ": " + brwNodesList.size() + " nodes found");

      Node brNode = null;
      String st = null;

      Iterator brsIt = _borrowers.iterator();
      while (brsIt.hasNext())
      {
        brNum = ((Borrower) brsIt.next()).getBorrowerNumber();
//        System.out.println("brNum = " + brNum);            // debug
//        logger.debug("brNum = " + brNum);                  // debug
        
        Iterator nodeIt = brwNodesList.iterator();
        while (nodeIt.hasNext())
        {
          brNode = (Node) nodeIt.next();
          st = DomUtil.getElementText((Element) DomUtil.getFirstNamedChild(brNode, BORROWER_NUMBER));
          val = Integer.parseInt(st);
          // ------------------
//          System.out.println("val = " + val);              // debug             
//          logger.debug("val = " + val);                    // debug
          // ------------------
          if (val == brNum)
          {
            brwNodesSortedList.add(brNode);
            break;
          }
        }
      }
    } catch (Exception e)
    {
      String lMsg = "Cannot get sorted borrower nodes: " + e.getMessage();
      _logger.error(lMsg);
      throw new DocPrepException(lMsg);
    } finally
    {
      _logger.trace(this.getClass() + ": " + brwNodesSortedList.size() + " nodes sorted");
    }
    return brwNodesSortedList;
  }

  private boolean isJoint(Borrower b1, Borrower b2)
  {
    boolean result = false;

    if (((b1.getMaritalStatusId() == 2) || (b1.getMaritalStatusId() == 3)) && (b2.getMaritalStatusId() == b1.getMaritalStatusId()))
    {
      try
      {
        String mergedAdr1 = mergeAddress(b1);
        debugPrint("Bor1 address = " + mergedAdr1);
        String mergedAdr2 = mergeAddress(b2);
        debugPrint("Bor2 address = " + mergedAdr2);

        if (mergedAdr1.equals(mergedAdr2))
        {
          result = true;
        }
      } catch (Exception e)
      {
        _logger.error(this.getClass() + "Cannot parser borrower address line: ");
        _logger.error(StringUtil.stack2string(e));
      }
    }

    return result;
  }

  private String mergeAddress(Borrower pBor) throws Exception
  {
    int lang;
    StreetAddress sta = null;
    String result = " ";

    // 1) parse current address
    AddressLineParser parser = new AddressLineParser(_srk);

    BorrowerAddress pBrAddr = getCurrentAddress(pBor);
    Addr pAddr = pBrAddr.getAddr();
    switch (pAddr.getProvinceId())
    {
      case Mc.PROVINCE_QUEBEC:
      {
        lang = Mc.LANGUAGE_PREFERENCE_FRENCH;
        break;
      }
      default:
      {
        lang = Mc.LANGUAGE_PREFERENCE_ENGLISH;
        break;
      }
    }
    parser.parse(pAddr.getAddressLine1(), lang);
    sta = parser.getAddress();

    // 2) merge
    StringBuffer stb = new StringBuffer();
    if (sta.getUnit() != null)
    {
      stb.append(sta.getUnit());
    }
    if (sta.getStreetNum() != null)
    {
      stb.append(sta.getStreetNum());
    }
    if (sta.getStreetName() != null)
    {
      //    remove all spaces from the street name
      String val = sta.getStreetName();
      debugPrint("Original street name = " + val); // debug
      int idx;
      while ((idx = val.indexOf(" ")) != -1)
      {
        val = val.substring(0, idx) + val.substring(idx + 1);
      }
      debugPrint("Resulting street name = " + val); // debug

      stb.append(val);
    }
    if (sta.getStreetType() != null)
    {
      stb.append(sta.getStreetType());
    }
    if (sta.getStreetDir() != null)
    {
      stb.append(sta.getStreetDir());
    }
    if (!isEmpty(pAddr.getAddressLine2()))
    {
      stb.append(pAddr.getAddressLine2());
    }
    if (!isEmpty(pAddr.getCity()))
    {
      stb.append(pAddr.getCity());
    }
    if (!isEmpty(pAddr.getPostalFSA()))
    {
      stb.append(pAddr.getPostalFSA());
    }
    if (!isEmpty(pAddr.getPostalLDU()))
    {
      stb.append(pAddr.getPostalLDU());
    }
    if (!isEmpty("" + pAddr.getProvinceId()))
    {
      stb.append("" + pAddr.getProvinceId());
    }
    result = stb.toString();

    return result;
  }

  private BorrowerAddress getCurrentAddress(Borrower pBor) throws Exception
  {
    Collection pCol = pBor.getBorrowerAddresses();
    Iterator it = pCol.iterator();

    BorrowerAddress bAdr;
    while (it.hasNext())
    {
      bAdr = (BorrowerAddress) it.next();
      if (bAdr.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT)
      {
        return bAdr;
      }
    }
    return null;
  }

  //================ DOM utility methods===================================

  public boolean isEmpty(String str)
  {
    return (str == null || str.trim().length() == 0);
  }

  public boolean isNotEmpty(String pStr)
  {
    if (pStr == null)
    {
      return false;
    }
    if (pStr.trim().length() == 0)
    {
      return false;
    }
    return true;
  }

  public boolean addToDocument(Document doc, Node parentNode, String tagName, String data)
  {
    if (parentNode != null && !isEmpty(tagName) && !isEmpty(data))
    {
      data = data.trim();

      parentNode.appendChild(getTag(doc, tagName, data));
      return true;
    }
    return false;
  }

  /**
   * Utility method that returns a new <code>Node</code> with data of <code>data</code>
   * and a name of <code>tagName</code>.
   *
   * @param tagName the name of the child node to be added.
   * @param data the child node text to be added.
   *
   * @return the new <code>Node</code> or <code>null</code> if an error is encountered.
   */
  protected Node getTag(Document doc, String tagName, String data)
  {
    if (!isEmpty(tagName) && !isEmpty(data))
    {
      data = data.trim();

      Node elemNode = doc.createElement(tagName);
      elemNode.appendChild(doc.createTextNode(data));

      return elemNode;
    }
    return null;
  }

  class AppControlSection
  {
    int       seq;
    String    primaryAppInd;
    String    type;
    int       primeApplicant;
    int       coApplicant;
    int       relatedApplicant;
    int       maxApps;
    ArrayList notUsed;

    public AppControlSection(int seqNum)
    {
      this.seq = seqNum;
      notUsed = new ArrayList();
    }

    /**
     * @param string
     */
    public int getCoApplicant()
    {
      return coApplicant;
    }

    public void setCoApplicant(int coApplicant)
    {
      this.coApplicant = coApplicant;
    }

    public int getMaxApps()
    {
      return maxApps;
    }

    public void setMaxApps(int maxApps)
    {
      this.maxApps = maxApps;
    }

    public ArrayList getNotUsed()
    {
      return notUsed;
    }

    public String getPrimaryAppInd()
    {
      return primaryAppInd;
    }

    public void setPrimaryAppInd(String primaryAppInd)
    {
      this.primaryAppInd = primaryAppInd;
    }

    public int getPrimeApplicant()
    {
      return primeApplicant;
    }

    public void setPrimeApplicant(int primeApplicant)
    {
      this.primeApplicant = primeApplicant;
    }

    public int getRelatedApplicant()
    {
      return relatedApplicant;
    }

    public void setRelatedApplicant(int relatedApplicant)
    {
      this.relatedApplicant = relatedApplicant;
    }

    public String getType()
    {
      return type;
    }

    public void setType(String type)
    {
      this.type = type;
    }

    public Node getSectionNode(Document pDoc)
    {
      Element cRoot = pDoc.createElement("CCAPSApplicationControl");
      Element appCtrl = pDoc.createElement("applicationControl");

      addToDocument(pDoc, appCtrl, "applicationSequenceNumber", "" + seq);
      addToDocument(pDoc, appCtrl, "primaryApplicationIndicator", primaryAppInd);
      addToDocument(pDoc, appCtrl, "applicationType", type);
      addToDocument(pDoc, appCtrl, "primeApplicant", "" + primeApplicant);
      if (coApplicant != 0)
      {
        addToDocument(pDoc, appCtrl, "coApplicant", "" + coApplicant);
      }
      if (relatedApplicant != 0)
      {
        addToDocument(pDoc, appCtrl, "relatedApplicant", "" + relatedApplicant);
      }

      cRoot.appendChild(appCtrl);

      addToDocument(pDoc, cRoot, "maxApplications", "" + maxApps);

      Iterator it = notUsed.iterator();
      if ((seq == 1) && (notUsed != null) && (notUsed.size() > 0))
      {
        Element notUsedEl = pDoc.createElement("notUsedBorrowerList");

        while (it.hasNext())
        {
          addToDocument(pDoc, notUsedEl, "notUsedBorrower", (String) it.next());
        }

        cRoot.appendChild(notUsedEl);
      }

      return cRoot;
    }
  }

  private void debugPrint(String str)
  {
    _logger.debug(this.getClass() + ": " + str);
  }
  
  /*
   * @author crutgaizer
   *
   * find "jobTitle" nodes in each "EmploymentHistory"
   * and change the element name of the first "jobTitle" to "JobTitle" (capital "J"!!)
   * Document.renameNode() - DOM level 3, is not supported by the implementation
   * PVCS # 963
   */
  private void renameJobTitleNode(Document lDoc){
    Node docRootNode = DomUtil.getFirstNamedDescendant(lDoc, DOCUMENT_NODE_NAME);
    List empHistList = DomUtil.getNamedDescendants(docRootNode, EMPLOYMENT_HISTORY);
    Iterator eit = empHistList.iterator();
    
    Node curEmpHist, origJT, newJT, removedNd, ndBefore;
    
    while (eit.hasNext()){
      curEmpHist = (Node)eit.next();
      origJT = DomUtil.getFirstNamedChild(curEmpHist, JOB_TITLE_NODE);
      
      if (origJT != null) {
        ndBefore = origJT.getNextSibling();
	    String jtText = DomUtil.getNodeText(origJT); 
	    newJT = lDoc.createElement(NEW_JOB_TITLE_NODE);
	    newJT.appendChild(lDoc.createTextNode(jtText));
	    // drop the existing
	    removedNd = curEmpHist.removeChild(origJT);
	    // insert a new one
	    ndBefore.getParentNode().insertBefore(newJT, ndBefore);
      }
    }
  }
}

