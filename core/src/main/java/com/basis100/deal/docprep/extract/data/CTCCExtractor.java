package com.basis100.deal.docprep.extract.data;

import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.log.*;
import com.basis100.xml.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.util.collections.*;
import com.basis100.picklist.*;
import java.util.*;
import java.text.*;
import MosSystem.*;
import com.basis100.deal.util.StringUtil;

/**
 * Title:        Your Product Name
 * Description:  Your description
 * Copyright:    Copyright (c) 1999
 * Company:
 * @author
 * @version
 */

public abstract class CTCCExtractor extends XSLExtractor
{
    protected abstract void buildXMLDataDoc() throws Exception;

    /**
     * The main exposed method that is called when a client desires the given document
     * type to be created.  Since the same logic is executed by each child class, the
     * logic was placed here.  This method calls buildXMLDataDoc(), which is
     * abstract and must be overloaded by the child class to handle how the document
     * is actually created.
     *
     */
    public String extractXMLData(DealEntity de,
                             int pLang,
                             DocumentFormat pFormat,
                             SessionResourceKit pSrk) throws Exception
    {
        deal = (Deal) de;
        lang = pLang;
        format = pFormat;
        srk = pSrk;

        buildXMLDataDoc();
        XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

        return xmlwr.printString(doc);
    }
}