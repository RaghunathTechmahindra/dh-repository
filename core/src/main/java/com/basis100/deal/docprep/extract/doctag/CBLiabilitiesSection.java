package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.collections.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.calc.*;
import java.util.*;

public class CBLiabilitiesSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    List outAll = null;
    List outFiltered = null;
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;
        outAll = (List)bor.getLiabilities();
        outFiltered = filterLiabilities(outAll);

        fml.setValues(outFiltered,fml.ENTITY);
        return fml;
      }
      else if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        outAll = (List)deal.getLiabilities();
        outFiltered = filterLiabilities(outAll);

        fml.setValues(outFiltered,fml.ENTITY);
        return fml;
      }
      else
      {
        fml.setValues(new ArrayList(),fml.ENTITY);
        return fml;
      }
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }



  }
  //========================================================================================
  // filtering CB liabilities comes as per of Joe's request (bug issue #155).
  // If the liability's percentoutgds
  // is 2 or greater it is considered to be
  // credit bureau liability.
  //=========================================================================================
  private List filterLiabilities(List parList){
    ListIterator li = parList.listIterator();
    List retList = new java.util.ArrayList();

    while( li.hasNext() ){
      Liability lLiability = (Liability)li.next();
      if( lLiability.getPercentOutGDS() >= 2 ){
        retList.add(lLiability);
      }
    }
    return retList;
  }

}