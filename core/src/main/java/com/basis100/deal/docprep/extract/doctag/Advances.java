package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class Advances extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      int id = deal.getSpecialFeatureId();

      //String desc = PicklistData.getDescription("SpecialFeature",id);
      //ALERT:BX:ZR Release2.1.docprep
      String desc = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "SpecialFeature", id, lang);

      if(desc != null && desc.equals("Progress"))
      {
        fml.addValue("final advance", fml.STRING);
      }
      else
      {
        fml.addValue("advance", fml.STRING);
      }

      return fml;

    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }
}
