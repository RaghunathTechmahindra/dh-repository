package com.basis100.deal.docprep.extract.data;

/**
 * 25/Jul/2006 DVG #DG470 #3930  Concentra - Build Fixes
 *  - also fixes #2399
 * 09/Nov/2005 DVG #DG358 #2399  Condition 705 on Broker outstanding conditions template
 * 11/Jul/2005 DVG #DG252 #1684  Cervus - AME contact info displayed on document
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.conditions.ConditionParser;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.ConditionDescriptionComparator;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.xml.XmlToolKit;


/**
 * Generates XML for Broker Conditions Outstanding document type
 */
public class GENXCondOutstExtractor extends GENXExtractor
{
  /** Variable to hold the condition ids to be excluded from the document. */
  protected int[] exclusionList = null;

  /**
   *
   *
   * @param dt TBD
   *
   * @return TBD
   */
  protected String getBorrowerNameFromDT(DocumentTracking dt)
  {
    String name = null;

    if (dt.getTagContextClassId() == ClassId.BORROWER)
    {
      int id = dt.getTagContextId();

      try
      {
        Borrower b = new Borrower(srk, null, id, deal.getCopyId());

        if (!isEmpty(b.getBorrowerFirstName()) &&
              !isEmpty(b.getBorrowerLastName()))
        {
          name = b.getBorrowerFirstName() + format.space() +
                 b.getBorrowerLastName();
        }
      }
      catch (Exception e)
      {
      }
    }

    return name;
  }

  /**
   * Returns whether <code>cond</code> should be excluded from the document.
   *
   * @param cond the condition to check.
   *
   * @return whether the condition should be excluded or not.
   */
  protected boolean isConditionExcluded(Condition cond)
  {
    if (cond == null)
    {
      return false;
    }

    if (exclusionList == null)
    {
      exclusionList = getExclusionList();
    }

    for (int i = 0; i < exclusionList.length; i++)
    {
      if (cond.getConditionId() == exclusionList[i])
      {
        return true;
      }
    }

    return false;
  }

  /**
   *
   *
   * @return TBD
   */
  protected int[] getExclusionList()
  {
    List list     = new ArrayList();
    int[] intList = new int[0];

    String exclusionList = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.condition.excludelist",
                                                                     "");

    if (exclusionList.trim().length() == 0)
    {
      return intList;
    }

    try
    {
      StringTokenizer st = new StringTokenizer(exclusionList, ",");
      String token;

      while (st.hasMoreTokens())
      {
        token = st.nextToken();

        list.add(new Integer(token));
      }
    }
    catch (Exception e)
    {
      String msg = "Error in GENXCondOutstExtractor.getExclusionList()!\n" +
                   "Ensure that the property contains all NUMBERS separated with commas." +
                   "  Example: 1,2,3,4.\n" + e;

      System.out.println(msg);
    }

    intList = new int[list.size()];

    for (int i = 0; i < list.size(); i++)
    {
      intList[i] = ((Integer) list.get(i)).intValue();
    }

    return intList;
  }

  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }
  /**
   * Builds the XML doc from various records in the database.
   *
   * @throws Exception DOCUMENT ME!
   */
  protected void buildXMLDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();

    Element root = doc.createElement("Document");
    root.setAttribute("type", "1");
    root.setAttribute("lang", "1");
    doc.appendChild(root);

    //==========================================================================
    // Language stuff
    //==========================================================================
    addTo(root, getLanguageTag());

    //==========================================================================
    // Source Firm Name
    //==========================================================================
    addTo(root, extractSourceFirmName());

    //==========================================================================
    // Lender Name for header (short)
    //==========================================================================
    addTo(root, extractLenderName(true));

    //==========================================================================
    // Source Name
    //==========================================================================
    addTo(root, extractSourceName());

    //==========================================================================
    // Source Phone
    //==========================================================================
    addTo(root, extractSourcePhone());

    //==========================================================================
    // Source Fax
    //==========================================================================
    addTo(root, extractSourceFax());

    //==========================================================================
    // Reference Source App Num
    //==========================================================================
    addTo(root, extractReferenceSourceAppNum());

    //==========================================================================
    // Current Date
    //==========================================================================
    addTo(root, extractCurrentDate());

    //==========================================================================
    // Deal Num (application Id)
    //==========================================================================
    addTo(root, extractApplicationId("DealNum"));

    //==========================================================================
    // servicingMortgageNumber
    //==========================================================================
    addTo( root, extractServicingMortgageNumber() );

    //==========================================================================
    // MI Policy Number
    //==========================================================================
    addTo(root, extractMIPolicyNum());

    //==========================================================================
    // MI Status
    //==========================================================================
    addTo(root, extractMIStatus());

    //==========================================================================
    // Client Name
    //==========================================================================
    addTo(root, extractClientName());

    //==========================================================================
    // Est Closing Date
    //==========================================================================
    addTo(root, extractEstClosingDate());

    //==========================================================================
    // Conditions
    //==========================================================================
    addTo(root, extractConditions());

    //==========================================================================
    // Lender Name
    //==========================================================================
    addTo(root, extractLenderName());

    //==========================================================================
    // Branch Address Complete
    //==========================================================================
    addTo(root, extractBranchAddressComplete());

    //==========================================================================
    // Branch Phone
    //==========================================================================
    addTo(root, extractBranchPhone(null, false));

    //==========================================================================
    // Branch Fax
    //==========================================================================
    addTo(root, extractBranchFax(null, false));

    //==========================================================================
    // Branch Toll Phone
    //==========================================================================
    addTo(root, extractBranchPhone(null, true));

    //==========================================================================
    // Branch Toll Fax
    //==========================================================================
    addTo(root, extractBranchFax(null, true));

    //==========================================================================
    // Conditions Met Days
    //==========================================================================
    addTo(root, extractConditionsMetDays());

    // <!-- Catherine, 7-Apr-05, pvcs#817  -->
    //==========================================================================
    // Conditions Met Days
    //==========================================================================
    addTo(root, extractLatestConditionNotes());
    // <!-- Catherine, 7-Apr-05, pvcs#817 end -->

    //==========================================================================
    // Current User
    //==========================================================================
    addTo(root, extractCurrentUser());

    //==========================================================================
    // Footer
    //==========================================================================
    addTo(root, extractFooter());   //#DG470
    //#DG470 addTo(root, condVarVerb(705, "BrokerConditionFooter"));  //#DG358

  }

  /**  This differs from the super class because we need it in the format
   *   <lastname>, <firstname>.
   *
   * @return Node
   */
  protected Node extractClientName()
  {
    Borrower b = getPrimaryBorrower();

    if (b == null)
    {
      return null;
    }

    String val = null;

    if (!isEmpty(b.getBorrowerFirstName()) &&
          !isEmpty(b.getBorrowerLastName()))
    {
      val = b.getBorrowerLastName() + "," + format.space() +
            b.getBorrowerFirstName();
    }

    return getTag("ClientName", val);
  }

  /**
   * Retrieves "Committment and Document Tracking" conditions from the deal and
   * formats XML tags with it.  Whether all or a subset of conditions are
   * processed is determined by the
   * <code>com.basis100.docprep.bco.showall</code> property.
   *
   * @return The completed conditions XML tag.
   */
  protected Node extractConditions()
  {
    Node elemNode = null;

    try
    {
      List dts = (List) deal.getDocumentTracks();

      if (dts.isEmpty())
      {
        return null;
      }

      boolean bShowAll = PropertiesCache.getInstance()
                                        .getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.bco.showall",
                                                     "N").equalsIgnoreCase("Y");

      boolean bShowRole = PropertiesCache.getInstance()
                                         .getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.show.responsibilityrole.tag",
                                                      "Y").equalsIgnoreCase("Y");

      String allowedStatuses = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.bco.show.statuses",
                                                                         "1,2,4,5,7");

      String conditionTypes = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.bco.show.condition.types",
                                                                        "0,1");

      boolean bLabelOnly = PropertiesCache.getInstance()
                                          .getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.bco.label.only",
                                                       "N").equalsIgnoreCase("Y");

      String labelSuffixWanted = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.bco.label.suffix.wanted",
                                                                           "N");
      // ALERT:ZIVKO: this is the spot to put bug fix for conditions outstanding in French
      //String labelSuffixEdited = "  " +
      //                           PropertiesCache.getInstance().getProperty("com.basis100.docprep.bco.label.suffix.edited",
      //                                                                     "");
      String labelSuffixEdited = "  " + BXResources.getDocPrepIngMsg("COM_BASIS100_DOCPREP_BCO_LABEL_SUFFIX_EDITED",lang);

      //String labelSuffix = "  " +
      //                     PropertiesCache.getInstance().getProperty("com.basis100.docprep.bco.label.suffix",
      //                                                               "");
      String labelSuffix = "  " + BXResources.getDocPrepIngMsg("COM_BASIS100_DOCPREP_BCO_LABEL_SUFFIX",lang);
      //  This ensures that there are spaces between any carriage return
      //  characters, otherwise it won't be parsed correctly later on.
      labelSuffix       = formatLabel(labelSuffix);
      labelSuffixEdited = formatLabel(labelSuffixEdited);

      StatusHelper sh   = new StatusHelper(allowedStatuses);
      ConditionTypeHelper cth = new ConditionTypeHelper(conditionTypes);

      //  Sort by Requested first, then alphabetical after that...
      //Collections.sort(dts, new DocumentStatusComparator());
      Collections.sort(dts, new ConditionDescriptionComparator());

      Iterator i           = dts.iterator();
      int sourceId;
      int condTypeId;
      int statusId;
      String role;
      Node descNode;
      ConditionParser p    = new ConditionParser();
      String conditionsOut;
      String status;
      DocumentTracking dt;
      Condition cond;
      int roleid;

      while (i.hasNext())
      {
        Node lineNode = doc.createElement("Condition");

        dt   = (DocumentTracking) i.next();
        cond = new Condition(srk, dt.getConditionId());

        condTypeId = cond.getConditionTypeId();
        sourceId   = dt.getDocumentTrackingSourceId();
        statusId   = dt.getDocumentStatusId();
        roleid     = dt.getDocumentResponsibilityRoleId();

        // ALERT:ZIVKO this must be fixed for Freedom (formerly known as French) version
        //role = PicklistData.getDescription("ConditionResponsibilityRole", roleid);
        role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", roleid, lang);

        if (((sourceId == Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED) ||
              (sourceId == Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED) ||
              (sourceId == Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM)) &&
              ((cth.wantCommitmentAndDocTracking() &&
              (condTypeId == Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING)) ||
              (cth.wantDocTrackingOnly() &&
              (condTypeId == Mc.CONDITION_TYPE_DOC_TRACKING_ONLY)) ||
              (cth.wantCommitmentOnly() &&
              (condTypeId == Mc.CONDITION_TYPE_COMMITMENT_ONLY))) &&
              ((roleid == Mc.CRR_PARTY_TYPE_APPRAISER) ||
              (roleid == Mc.CRR_PARTY_TYPE_SOLICITOR) ||
              (roleid == Mc.CRR_SOURCE_OF_BUSINESS) ||
              (roleid == Mc.CRR_USER_TYPE_APPLICANT)))
        {
          if (bShowAll || (sh.wantOpen() && (statusId == Mc.DOC_STATUS_OPEN)) ||
                (sh.wantRequested() && (statusId == Mc.DOC_STATUS_REQUESTED)) ||
                (sh.wantReceived() && (statusId == Mc.DOC_STATUS_RECEIVED)) ||
                (sh.wantApproved() && (statusId == Mc.DOC_STATUS_APPROVED)) ||
                (sh.wantInsufficient() &&
                (statusId == Mc.DOC_STATUS_INSUFFICIENT)) ||
                (sh.wantWaived() && (statusId == Mc.DOC_STATUS_WAIVED)) ||
                (sh.wantRejected() && (statusId == Mc.DOC_STATUS_REJECTED)) ||
                (sh.wantCaution() && (statusId == Mc.DOC_STATUS_CAUTION)) ||
                (sh.wantWaiveRequested() &&
                (statusId == Mc.DOC_STATUS_WAIVE_REQUESTED)))
          {
            if (isConditionExcluded(cond))
            {
              continue;
            }

            if (elemNode == null)
            {
              elemNode = doc.createElement("Conditions");
            }

            conditionsOut = (bLabelOnly) ? dt.getDocumentLabelByLanguage(lang)
                                         : dt.getDocumentTextByLanguage(lang);

            //status = PicklistData.getDescription("DocumentStatus", statusId);
            //ALERT:BX:ZR Release2.1.docprep
            status = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DocumentStatus", statusId, lang);

            if (conditionsOut == null)
            {
              conditionsOut = "";
            }

            //  Added to include the borrower's name for conditions
            //  that have a borrower as their context.
            if (bLabelOnly)
            {
              String name = getBorrowerNameFromDT(dt);

              //  Will return null if the context isn't borrower
              if (!isEmpty(name))
              {
                conditionsOut += (" for " + name);
              }
            }

            //else if (bShowRole)
            if (bShowRole)
            {
              //conditionsOut = convertCRtoRTF(conditionsOut, format);
              conditionsOut += (format.space() + "(" + role + ")");
            }

            // now send the text for RTF conversion
            //conditionsOut = convertCRtoRTF(conditionsOut, format);
            //  JS - changed to make the XML data not RTF-specific.  This
            //  new way adheres to other XML generation techniques,
            //  where the lines previously divided by '\par',
            //  are now divided into child nodes.  This allows the
            //  XSL to format to the respective document type.
            descNode      = doc.createElement("Description");
            conditionsOut = p.parseText(conditionsOut, lang, deal, srk);

            if (labelSuffixWanted.equalsIgnoreCase("y"))
            {
              try
              {
                srk.getSysLogger().info("Label suffix wanted.");

                Date dtDate = dt.getDocumentRequestDate();
                long dtTime;

                if (dtDate == null)
                {
                  dtTime = -1;
                }
                else
                {
                  dtTime = dt.getDocumentRequestDate().getTime();
                }

                long dealTime;
                Date commDate = deal.getCommitmentIssueDate();

                if (commDate == null)
                {
                  dealTime = -1;
                }
                else
                {
                  dealTime = commDate.getTime();
                }

                long lDiff;
                srk.getSysLogger().info("Document tracking time is :" + dtTime +
                                        "  Commitment issue time is:" +
                                        dealTime);

                if ((dtTime != -1) && (dealTime != -1))
                {
                  lDiff = dtTime - dealTime;

                  if (lDiff == 0)
                  {
                    conditionsOut += labelSuffixEdited;
                  }
                  else
                  {
                    conditionsOut += labelSuffix;
                  }
                }
                else if (dtTime == -1)
                {
                  conditionsOut += labelSuffixEdited;
                }
                else
                {
                  conditionsOut += labelSuffix;
                }
              }
              catch (Exception timeException)
              {
                srk.getSysLogger().info("Exception:" +
                                        this.getClass().getName());

                StringWriter swr = new StringWriter();
                timeException.printStackTrace(new PrintWriter(swr));
                srk.getSysLogger().info(swr.toString());

                // in case of exception make as per original exception
                conditionsOut += labelSuffix;
              }
            }

            //if (!isEmpty(labelSuffix))
            //    conditionsOut += "  " + labelSuffix;
            lineNode.appendChild(convertCRToLines(descNode, conditionsOut));

            addTo(lineNode, "Status", status);

            addTo(elemNode, lineNode);
          }
        }
      }
    }
    catch (Exception e)
    {
    }

    return elemNode;
  }

  /**
   *
   *
   * @return TBD
   */
  protected Node extractConditionsMetDays()
  {
    String val = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.bco.conditions.met.days");

    return getTag("ConditionsMetDays", val);
  }

  // <!-- Catherine, 7-Apr-05, pvcs#817  -->
  protected Node extractLatestConditionNotes()
  {
    String val = "";
    DealNotes dn = null;
    try
    {
      dn = new DealNotes(srk);
      // bilingual deal notes
      val = dn.findLastConditionNoteText(deal.getDealId(), lang);
      dn = null;
    }
    catch(Exception ex)
    {
    }

    // <!-- Midori, 20-Mar-06, pvcs#2815 -->
    return (val == null ? null :
        convertCRToLines(doc.createElement("ConditionNote"), val));
//    return getTag("ConditionNote", val);
    // <!-- Midori, 20-Mar-06, pvcs#2815 end -->
  }
  // <!-- Catherine, 7-Apr-05, pvcs#817 end -->

  /**
   *
   *
   * @return TBD
   */
  protected Node extractCurrentUser()
  {
    String val = null;
    Node node  = null;

    try
    {
      String currentUser = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.bco.current.user", "Y");

      //  There have become a couple of ways to do this.
      //  Some clients require that the administrator appear as the
      //  current user.  Others require the user that actually pushed
      //  the button to request the document be the current user.
      //  A property has been added to determine what the client
      //  requires as the current user.
      UserProfile up = new UserProfile(srk);

      //#DG252 if produced by AME always use admin  - rewritten
      up.findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(), deal.getInstitutionProfileId()));
      if ((!currentUser.equalsIgnoreCase("Y")) || up.getUserTypeId() == Mc.USER_TYPE_AUTO)
      {
        up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), deal.getInstitutionProfileId()));
      }

      Contact c = up.getContact();

      //CurrentUser
      node = doc.createElement("CurrentUser");

      //  Name
      String name = "";

      if (!isEmpty(c.getContactFirstName()))
      {
        name += c.getContactFirstName();
      }

      if (!isEmpty(c.getContactLastName()))
      {
        name += (format.space() + c.getContactLastName());
      }

      addTo(node, "Name", name);

      //  Phone
      try
      {
        val = StringUtil.getAsFormattedPhoneNumber(c.getContactPhoneNumber(),
                                                   c.getContactPhoneNumberExtension());
      }
      catch (Exception e)
      {
      }

      addTo(node, "Phone", val);

      //  Fax
      try
      {
        val = StringUtil.getAsFormattedPhoneNumber(c.getContactFaxNumber(), null);
      }
      catch (Exception e)
      {
      }

      addTo(node, "Fax", val);

      //  Extention separated out
      try
      {
        val = c.getContactPhoneNumberExtension();
      }
      catch (Exception e)
      {
      }

      addTo(node, "Extension", val);

      //  E-mail addess
      val = c.getContactEmailAddress();
      if(val == null){ val = ""; }
      addTo(node, "EMailAddress", val);

    }
    catch (Exception e)
    {
    }

    return node;
  }

  /**
   * extract BrokerConditionFooter replacing tags
   *
   * @return Node
   */
  protected Node extractFooter()
  {
    String val = null;
    try
    {
      Condition c = new Condition(srk, 705);

      val = c.getConditionTextByLanguage(lang);
      //#DG470 add tag replacement
      val = new ConditionParser().parseText(val, lang, deal, srk);
    }
    catch(Exception e) {
      srk.getSysLogger().info("Exception in extractFooter:" +e);
    }
    return getTag("BrokerConditionFooter", val);
  }

  /**
   * Ensures that the label will get parsed correctly on multiple carriage
   * returns by inserting a space in between 2 subsequent indicators (�� -> �
   * �).
   *
   * @param pStr DOCUMENT ME!
   *
   * @return TBD
   */
  protected String formatLabel(String pStr)
  {
    int ind;
    StringBuffer retBuf = new StringBuffer("");

    while ((ind = pStr.indexOf("��")) != -1)
    {
      retBuf.append(pStr.substring(0, ind));
      retBuf.append("� �");
      pStr = pStr.substring(ind + 2);
    }

    retBuf.append(pStr);

    return retBuf.toString();
  }

  /**
   * Retrieves the Source Firm Name from the deal and formats an XML tag with
   * it.
   *
   * @return The completed Source Firm Name XML tag.
   */
  private Node extractSourceFirmName()
  {
    String val = null;

    try
    {
      SourceFirmProfile sf = new SourceFirmProfile(srk,
                                                   deal.getSourceFirmProfileId());
      val = sf.getSourceFirmName();
    }
    catch (Exception e)
    {
    }

    return getTag("SourceFirmName", val);
  }

  /**
   *
   *
   * @param inList TBD
   *
   * @return TBD
   */
  private List makeConditionSectDescr(List inList)
  {
    ArrayList outList    = new java.util.ArrayList();
    Iterator inIterator  = inList.iterator();
    Iterator outIterator;

    while (inIterator.hasNext())
    {
      DocumentTracking tracking = (DocumentTracking) inIterator.next();
      String descr              = null;

      try
      {
        descr = tracking.getSectionDescription();
      }
      catch (Exception exc)
      {
        continue;
      }

      if (descr == null)
      {
        continue;
      }

      boolean found = false;
      outIterator = outList.iterator();

      String oneDesc;

      while (outIterator.hasNext())
      {
        oneDesc = (String) outIterator.next();

        if (descr.trim().compareTo(oneDesc.trim()) == 0)
        {
          found = true;

          break;
        }
      }

      if (found == false)
      {
        outList.add(descr);
      }
    }

    Collections.sort(outList);

    return outList;
  }

  /**
   * Used to parse comma-delimited condition type indicators from a properties
   * file.
   */
  public class ConditionTypeHelper
  {
    protected boolean commitmentAndDocTracking = false;
    protected boolean commitmentOnly  = false;
    protected boolean docTrackingOnly = false;

    //  Parses <code>str</code> into various condition type indicators.
    //  This method expects the string to have a format of num,num,num... etc.
    //  Example: 0,1 would mean that status Commitment and Doc Tracking and
    //  Doc Tracking Only would be set to true and the rest false.
    public ConditionTypeHelper(String str)
    {
      if (!isEmpty(str))
      {
        StringTokenizer st = new StringTokenizer(str, ",");
        int status;

        while (st.hasMoreTokens())
        {
          status = -1;

          try
          {
            status = Integer.parseInt(st.nextToken());
          }
          catch (NumberFormatException nfe)
          {
          }

          switch (status)
          {
            case 0:
              commitmentAndDocTracking = true;

              break;

            case 1:
              docTrackingOnly = true;

              break;

            case 2:
              commitmentOnly = true;

              break;
          }
        }
      }
    }

    public boolean wantCommitmentAndDocTracking()
    {
      return commitmentAndDocTracking;
    }

    public boolean wantCommitmentOnly()
    {
      return commitmentOnly;
    }

    public boolean wantDocTrackingOnly()
    {
      return docTrackingOnly;
    }
  }

  protected Node extractSourceName()
  {
      String val = null;

      try
      {
    	  SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
    	  
    	  //for older deals, SourceOfBusinessProfile2ndary may not be set.
    	  if (sobp == null)
    		  sobp = deal.getSourceOfBusinessProfile();

          if (sobp.getContact() != null)
          {
              Contact c = sobp.getContact();

              if (!isEmpty(c.getContactFirstName()) &&
                  !isEmpty(c.getContactLastName()))
              {
                  val = new String();

                  if (!isEmpty(c.getContactFirstName()))
                     val += c.getContactFirstName() + format.space();

                  if (!isEmpty(c.getContactLastName()))
                     val += c.getContactLastName();
              }
          }
      }
      catch (Exception e){}

      return getTag("SourceName", val);
  }
  
  protected Node extractSourcePhone()
  {
      String val = null;

      try
      {
    	  SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
    	  
    	  //for older deals, SourceOfBusinessProfile2ndary may not be set.
    	  if (sobp == null)
    		  sobp = deal.getSourceOfBusinessProfile();

          Contact contact = sobp.getContact();
          //Addr addr = contact.getAddr();

          val = StringUtil.getAsFormattedPhoneNumber(contact.getContactPhoneNumber(),
                                                     contact.getContactPhoneNumberExtension());
      }
      catch (Exception e){}

      return getTag("SourcePhone", val);
  }
  
  protected Node extractSourceFax()
  {
      String val = null;

      try
      {
    	  SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
    	  
    	  //for older deals, SourceOfBusinessProfile2ndary may not be set.
    	  if (sobp == null)
    		  sobp = deal.getSourceOfBusinessProfile();

          Contact contact = sobp.getContact();
          //Addr addr = contact.getAddr();

          val = StringUtil.getAsFormattedPhoneNumber(contact.getContactFaxNumber(), null);
      }
      catch (Exception e){}

      return getTag("SourceFax", val);
  }

  /**
   * Used to parse comma-delimited status indicators from a properties file.
   */
  public class StatusHelper
  {
    protected boolean approved = false;
    protected boolean caution      = false;
    protected boolean insufficient = false;
    protected boolean open         = false;
    protected boolean received = false;
    protected boolean rejected  = false;
    protected boolean requested = false;
    protected boolean waiveRequested = false;
    protected boolean waived         = false;

    //  Parses <code>str</code> into various status indicators.
    //  This method expects the string to have a format of num,num,num... etc.
    //  Example: 0,1,2 would mean that status Open, Requested and Received
    //  would be set to true and the rest false.
    public StatusHelper(String str)
    {
      if (!isEmpty(str))
      {
        StringTokenizer st = new StringTokenizer(str, ",");
        int status;

        while (st.hasMoreTokens())
        {
          status = -1;

          try
          {
            status = Integer.parseInt(st.nextToken());
          }
          catch (NumberFormatException nfe)
          {
          }

          switch (status)
          {
            //  0	Open
            case 0:
              open = true;

              break;

            //  1	Requested
            case 1:
              requested = true;

              break;

            //  2	Received
            case 2:
              received = true;

              break;

            //  3	Approved
            case 3:
              approved = true;

              break;

            //  4	Insufficient
            case 4:
              insufficient = true;

              break;

            //  5	Waived
            case 5:
              waived = true;

              break;

            //  6	Rejected
            case 6:
              rejected = true;

              break;

            //  7	Caution
            case 7:
              caution = true;

              break;

            //  8	Waive Requested
            case 8:
              waiveRequested = true;

              break;
          }
        }
      }
    }

    public boolean wantApproved()
    {
      return approved;
    }

    public boolean wantCaution()
    {
      return caution;
    }

    public boolean wantInsufficient()
    {
      return insufficient;
    }

    public boolean wantOpen()
    {
      return open;
    }

    public boolean wantReceived()
    {
      return received;
    }

    public boolean wantRejected()
    {
      return rejected;
    }

    public boolean wantRequested()
    {
      return requested;
    }

    public boolean wantWaiveRequested()
    {
      return waiveRequested;
    }

    public boolean wantWaived()
    {
      return waived;
    }
  }
}
