package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import MosSystem.Mc;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;



public class LoanAmount extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    double amt = 0.0;
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
         Deal deal = (Deal)de;
         int id = deal.getDealPurposeId();
         int mindid = deal.getMIIndicatorId();

         switch(id){
           case Mc.DEAL_PURPOSE_PORTABLE : {
             amt = deal.getNetLoanAmount();
             break;
           }
           case Mc.DEAL_PURPOSE_PORTABLE_DECREASE : {
             amt = deal.getNetLoanAmount();
             break;
           }
           default : {
             if((mindid == Mc.MII_REQUIRED_STD_GUIDELINES || mindid == Mc.MII_UW_REQUIRED)
                  && deal.getMIUpfront().equals("N"))
             {
               amt = deal.getNetLoanAmount();
             }
             else
             {
               amt = deal.getTotalLoanAmount();
             }
             break;
           }  // end of default
         } // end switch
//==============================================================================
// PROGRAMMERS NOTE:  The following section of code has been removed because
// the specification has changed.  Look also at LoanAmountVerb class.
//==============================================================================
/*
         if( id == Mc.DEAL_PURPOSE_PORTABLE  || id == Mc.DEAL_PURPOSE_PORTABLE_DECREASE)
         {
           amt = deal.getNetLoanAmount();
         }
         else if( id == Mc.DEAL_PURPOSE_PORTABLE_INCREASE || id == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
         {
            ;//do nothing
         }
         else if( (id != Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT) && (id != Mc.DEAL_PURPOSE_PORTABLE_INCREASE) &&
                  (id != Mc.DEAL_PURPOSE_PORTABLE) && (id != Mc.DEAL_PURPOSE_PORTABLE_DECREASE)
              )
         {
           if((mindid == Mc.MII_REQUIRED_STD_GUIDELINES || mindid == Mc.MII_UW_REQUIRED)
                && deal.getMIUpfront().equals("N"))
           {
             amt = deal.getNetLoanAmount();
           }
           else
           {
             amt = deal.getTotalLoanAmount();
           }
         }
*/
       }
       else if(clsid == ClassId.BRIDGE)
       {
         amt = ((Bridge)de).getNetLoanAmount();
       }

      fml.addCurrency(amt);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
