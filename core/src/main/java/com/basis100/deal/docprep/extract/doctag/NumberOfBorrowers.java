package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
 import com.basis100.picklist.*;
import com.basis100.deal.calc.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class NumberOfBorrowers extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = null;

    try
    {
      if(de.getClassId() == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;
        deal = new Deal(srk,null,bor.getDealId(), bor.getCopyId());
      }
      else if(de.getClassId() == ClassId.DEAL)
      {
        deal = (Deal)de;
      }

       fml.addValue(deal.getNumberOfBorrowers());

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
