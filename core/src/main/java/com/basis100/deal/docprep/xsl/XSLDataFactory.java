package com.basis100.deal.docprep.xsl;

/**
 * Created for Rel 3.1 Express
 */
import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.data.DealExtractor;
import com.basis100.deal.docprep.extract.data.XSLExtractor;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

public class XSLDataFactory {
	private DocumentProfile mProfile;
	private SysLogger mLogger;
	private Deal mDeal;
	private SessionResourceKit mSrk;
	
	/**
	 * Constructor
	 * 
	 * @param pDeal
	 * @param pProfile
	 * @param pSrk
	 */
	public XSLDataFactory(Deal pDeal, DocumentProfile pProfile, SessionResourceKit pSrk){
		mDeal = pDeal;
		mProfile = pProfile;
		mSrk = pSrk;
		mLogger = mSrk.getSysLogger();
	}
	
	/**
	 * Rel 3.1 - Sasha
	 * Extracts data from DB using extractor class defined in the DocumentProfile.DocumentSchemaPath field
	 * 
	 * @return
	 * @throws DocPrepException
	 */
	public String ExtractDataForXsl() throws DocPrepException {
		String extrStr = null;
		try
		{
			DocumentFormat df = new DocumentFormat( mProfile );
			String lClName = mProfile.getDocumentSchemaPath();
			Class queryclass = Class.forName( lClName );
			
			String sVersion = mProfile.getDocumentVersion();
			if(sVersion == null){
				mLogger.trace(this.getClass(), "Extract data for XSL using class " + lClName + "and version: null");
				
				XSLExtractor tagquery = (XSLExtractor)queryclass.newInstance();
				extrStr = tagquery.extractXMLData((DealEntity)mDeal, mProfile.getLanguagePreferenceId(), df, mSrk);
			}
			else{
				mLogger.trace(this.getClass(), "Extract data for XSL using class " + lClName + "and version: " + sVersion);
				
				//--> Bug fix to handle special case of "Genx2.0_std" for Solitior package
				//--> By Billy 25June2004
				if(sVersion.trim().equalsIgnoreCase("genx2.0") || sVersion.trim().toUpperCase().startsWith("GENX2.0")){
					DealExtractor extractor = (DealExtractor)queryclass.newInstance();
					extrStr = extractor.extractXMLData((DealEntity)mDeal, mProfile.getLanguagePreferenceId(), df, mSrk);
				}else{
					XSLExtractor tagquery = (XSLExtractor)queryclass.newInstance();
					extrStr = tagquery.extractXMLData((DealEntity)mDeal, mProfile.getLanguagePreferenceId(), df, mSrk);
				}
			}
		} 
		catch(Exception exc){
            
			String msg = "DocumentGenerationHandler.ExtractForXSL: ";
			throw new DocPrepException( msg + exc.getMessage());
		}
		return extrStr;		
	}
}
