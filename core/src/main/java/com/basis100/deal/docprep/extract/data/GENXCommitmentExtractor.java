package com.basis100.deal.docprep.extract.data;

/**
 * 30/Jun/2006 DVG #DG454 #3559  CTFS - Customization of Express Documents
 * 24/Oct/2005 DVG #DG344 #2286  CR#132 Solicitors Package changes
  #1670  Concentra - CR #158 (Solicitors package)
  - total conversion from the xml to the xsl format
 added deal and brokerconditions tags
 * 09/Sep/2005 DVG #DG312 #1965  Xeed Solicitor Package Middle Name initial is not shown
 * 30/Aug/2005 DVG #DG304 #1941  Commitment Letter changes
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.xml.*;
import com.basis100.deal.conditions.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.picklist.*;
import java.util.*;
import MosSystem.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.util.BusinessCalendar;

/**
 * <PRE>
 * Creates an XML document for use as a Commitment Letter
 * Copyright (c) 2002 Basis100, Inc
 * </PRE>
 * @author       John Schisler
 * @version 1.0
 * @author MCM Impl Team 
 * Change log:
 * @version 1.1 10-July-2008 XS_16.17 Updated extractConditions() method to 
 *              add '<ConditionSection>' ellement within every '<Condition>'.
 * @version 1.2 06-Oct-2008 Bug Fix :FXP22542 current.getSectionCode change to current.loadSectionCode().
 */

public class GENXCommitmentExtractor extends GENXExtractor
{
    public GENXCommitmentExtractor()
    {
        super();
    }

    public GENXCommitmentExtractor(XSLExtractor x)
    {
        super(x);
    }

    //#DG238
    /**
     * For compatibility after making this class inherit from DealExtractor/XSLExtractor
     * @throws Exception
     */
    protected void buildDataDoc() throws Exception {
      buildXMLDataDoc();
    }
    /**
     * Builds the XML doc from various records in the database.
     *
     */
    protected void buildXMLDataDoc() throws Exception
    {
        doc = XmlToolKit.getInstance().newDocument();

        Element root = doc.createElement("Document");

        //#DG238/#DG312 make a full blown deal tree available so that this xml can be used for all templates
        createDealTree(root);

        root.setAttribute("type","1");
        root.setAttribute("lang","1");

        root.appendChild(buildBaseTags(doc));
        doc.appendChild(root);
    }

    /**
     * To make life easier for documents other than the commitment letter
     * that want to use these generated tags, all nodes are placed under a
     * &lt;CommitmentLetter&gt; Node.  This way, there won't be any tag conflicts.
     *
     * @param pDoc <code>Document</code> to use for building nodes.
     * @return The completed node tree.
     * @version 1.0
     * @author MCM Impl Team 
     * Change log:
     * @version 1.1 10-July-2008 XS_16.17 Added productType tag.
     */
    protected Element buildBaseTags(Document pDoc)
    {
        Node elemNode;
        Element root = pDoc.createElement("CommitmentLetter");

        //==========================================================================
        // Language stuff
        //==========================================================================
        addTo(root, getLanguageTag());
        //==========================================================================
        // Lender Name
        //==========================================================================
        addTo(root, extractLenderName());
        
        /***** CTFS CR224B (FXP20818) merge start *****/
        // Lender phone number
        //==========================================================================
        addTo(root, extractLenderPfoneNumber());
        //==========================================================================
        // Lender fax number
        //==========================================================================
        addTo(root, extractLenderFaxNumber());
        //==========================================================================
        /***** CTFS CR224B (FXP20818) merge end *****/
        
        //==========================================================================
        // Lender Id
        //==========================================================================
        addTo(root, extractLenderProfileId());

        //==========================================================================
        // Branch Address
        //==========================================================================
        elemNode = extractBranchAddressSection();

        if (elemNode != null)
        {
            //==========================================================================
            // Branch Phone Number
            //==========================================================================
            addTo(elemNode, extractBranchPhone(null, true, "Phone"));

            //==========================================================================
            // Branch Fax Number
            //==========================================================================
            addTo(elemNode, extractBranchFax(null, true, "Fax"));

            root.appendChild(elemNode);
        }

        //==========================================================================
        // Underwriter
        //==========================================================================
        addTo(root, extractUnderwriter());

        //==========================================================================
        // Current Date
        //==========================================================================
        addTo(root, extractCurrentDate());

        //==========================================================================
        // Branch Current Time
        //==========================================================================
        addTo(root, extractBranchCurrentTime());

        //==========================================================================
        // Broker Source Number
        //==========================================================================
        addTo(root, extractBrokerSourceNum());

        //==========================================================================
        // Deal Num
        //==========================================================================
        addTo(root, extractDealNum());

        //==========================================================================
        // Client address section
        //==========================================================================
        addTo(root, extractClientAddressSection());

        //==========================================================================
        // Borrower Names Line
        //==========================================================================
        addTo(root, extractBorrowerNamesLine());

        //==========================================================================
        // Broker Firm Name
        //==========================================================================
        addTo(root, extractBrokerFirmName());

        //==========================================================================
        // Broker Agent Name
        //==========================================================================
        addTo(root, extractBrokerAgentName());

        //==========================================================================
        // Broker Address Section
        //==========================================================================
        addTo(root, extractBrokerAddressSection());

        //==========================================================================
        // Borrower Names
        //==========================================================================
        addTo(root, extractBorrowerNames());

        //==========================================================================
        // Guarantor Clause
        //==========================================================================
        addTo(root, extractGuarantorClause());

        //==========================================================================
        // Guarantor Names
        //==========================================================================
        addTo(root, extractGuarantorNames());

        //==========================================================================
        // Deal purpose
        //==========================================================================
        addTo(root, extractDealPurpose());

        //==========================================================================
        // Properties (include legal lines)
        //==========================================================================
        addTo(root, extractProperties(true));

        //==========================================================================
        // Commitment Issue Date
        //==========================================================================
        addTo(root, extractCommitmentIssueDate());

        //==========================================================================
        // Commitment Expiry Date
        //==========================================================================
        addTo(root, extractCommitmentExpiryDate());

        //==========================================================================
        // Advance Date
        //==========================================================================
        addTo(root, extractAdvanceDate());

        //==========================================================================
        // IAD Date
        //==========================================================================
        addTo(root, extractIADDate());

        //==========================================================================
        // First Payment Date
        //==========================================================================
        addTo(root, extractFirstPaymentDate());

        //==========================================================================
        // Maturity Date
        //==========================================================================
        addTo(root, extractMaturityDate());

        //==========================================================================
        // Product Name
        //==========================================================================
        addTo(root, extractProduct());
        //==========================================================================
        /***************MCM Impl Team XS_16.17 Changes Starts**********************/
        // Product Type
        //==========================================================================
        addTo(root, extractProductType());
        /***************MCM Impl Team XS_16.17 Changes Starts**********************/
        //==========================================================================
        // LTV
        //==========================================================================
        addTo(root, extractLTV());

        //==========================================================================
        // Loan Amount
        //==========================================================================
        addTo(root, extractLoanAmount());

        //==========================================================================
        // Premium
        //==========================================================================
        addTo(root, extractPremium());

        //==========================================================================
        // Total Amount
        //==========================================================================
        addTo(root, extractTotalAmount());

        //==========================================================================
        // Total Payment
        //==========================================================================
        addTo(root, extractTotalPayment());

        //==========================================================================
        // Annual Taxes
        //==========================================================================
        addTo(root, extractAnnualTaxes());

        //==========================================================================
        // Interest Rate
        //==========================================================================
        addTo(root, extractInterestRate());

        //==========================================================================
        // Term
        //==========================================================================
        addTo(root, extractTerm());

        //==========================================================================
        // Amortization
        //==========================================================================
        addTo(root, extractAmortization());

        //==========================================================================
        // Payment Frequency
        //==========================================================================
        addTo(root, extractPaymentFrequency());

        //==========================================================================
        // Tax Payor
        //==========================================================================
        addTo(root, extractTaxPayor());

        //==========================================================================
        // Compounding Frequency
        //==========================================================================
        addTo(root, extractCompoundingFrequency());

        //==========================================================================
        // PST
        //==========================================================================
        addTo(root, extractPST());

        //==========================================================================
        // Fees
        //==========================================================================
        addTo(root, extractFees());

        //==========================================================================
        // 10 business days prior to Commitment Advance Date  #DG454 #3559
        //==========================================================================
        addTo(root, extractPriorAdvanceDate());

        //==========================================================================
        // Commitment Return Date
        //==========================================================================
        addTo(root, extractCommitmentReturnDate());

        //==========================================================================
        // Privilege
        //==========================================================================
        addTo(root, extractPrivilege());

        //==========================================================================
        // Prepayment
        //==========================================================================
        addTo(root, extractPrepayment());

        //==========================================================================
        // Conditions
        //==========================================================================
        addTo(root, extractConditions());

        //==========================================================================
        // Broker Conditions
        //==========================================================================
        addTo(root, extractBrokerConditions());  //#DG344
        
        /***** CTFS CR224B (FXP20818) merge start *****/
        addTo(root, extractTotalPayments());
        
        addTo(root, extractSOBRegionId());
        addTo(root, extractSOBRegionDescription());
        addTo(root, extractPaymentDueOn());
        addTo(root, extractNetAdvance());
        
        addTo(root, extractEffectiveAmortization());
        addTo(root, extractInternalRatePercentage());
        addTo(root, extractAdjustmentFactor());
        /***** CTFS CR224B (FXP20818) merge end *****/
        
        return root;
    }

    /**
     * Retrieves and formats the Commitment Issue Date from the deal and
     * formats an XML tag with it.
     *
     * @return The completed Commitment Issue Date XML tag.
     *
     */
    private Node extractCommitmentIssueDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getCommitmentIssueDate());
        }
        catch (Exception e) {}

        return getTag("CommitmentIssueDate", val);
    }

    /**
     * Retrieves and formats the Propertise from the deal and
     * formats repeating XML tags with them.
     *
     * @param includeLegalLines Whether to include each property's legal lines
     * @return The completed Properties XML tag.
     *
     */
    protected Node extractProperties(boolean includeLegalLines)
    {
        Node elemNode = null;

        try
        {
            List c = (List)deal.getProperties();

            if (c.isEmpty())
               return null;

            Collections.sort(c, new PrimaryPropertyComparator());

            Iterator i = c.iterator();

            elemNode = doc.createElement("Properties");

            while (i.hasNext())
            {
                Property p =(Property)i.next();

                Node pNode = doc.createElement("Property");

                //  Address Line 1
                String add1 = "";

                if (!isEmpty(p.getPropertyStreetNumber()))
                   add1 += p.getPropertyStreetNumber();

                if (!isEmpty(p.getPropertyStreetName()))
                   add1 += format.space() + p.getPropertyStreetName();

                if (!isEmpty(p.getStreetTypeDescription()))
                   add1 += format.space() + p.getStreetTypeDescription();

                if (p.getStreetDirectionId() != 0)
                   add1 += format.space() +
                        //ALERT:BX:ZR Release2.1.docprep
                        BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection", p.getStreetDirectionId() , lang);
                        //PicklistData.getDescription("StreetDirection",
                        //    p.getStreetDirectionId());

                addTo(pNode, "AddressLine1", add1);

                //  Address line 2
                addTo(pNode, "AddressLine2", p.getPropertyAddressLine2());

                //  Address city
                addTo(pNode, "City", p.getPropertyCity());

                //  Address province
                Province prov = new Province(srk, p.getProvinceId());

                addTo(pNode, "Province", prov.getProvinceName());

                //  Address postal code
                addTo(pNode, extractPostalCode(p));

                if (includeLegalLines)
                    addLegalLines(pNode, p);

                elemNode.appendChild(pNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    private Node extractCompoundingFrequency()
    {
        String val = null;

        try
        {
            Property prop = getPrimaryProperty();

            if (prop != null)
            {
                val = "monthly";

                if (prop.getDwellingTypeId() != 5)
                   val = "semi-" + val;
            }
        }
        catch (Exception e){}

        return getTag("CompoundingFrequency", val);
    }

    protected Node extractTotalAmount()
    {
        String val = null;

        try
        {
            int id = deal.getDealPurposeId();
            int miIndID = deal.getMIIndicatorId();
            double amt;

            if( id == Mc.DEAL_PURPOSE_PORTABLE_INCREASE ||
                id == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
            {
                amt = deal.getTotalLoanAmount();
            }
            else if((miIndID == Mc.MII_REQUIRED_STD_GUIDELINES ||
                     miIndID == Mc.MII_UW_REQUIRED) &&
                     deal.getMIUpfront().equals("N"))
            {
                amt = deal.getTotalLoanAmount();
            }
            else
                amt = deal.getNetLoanAmount();

            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("TotalAmount", val);
    }

    private Node extractOpenClosedStatus()
    {
        String val = null;

        try
        {
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

            MtgProd pr = new MtgProd(srk,null);

            List all = (List)pr.findByPricingProfile(pp.getPricingProfileId());

            if (all.size() > 0)
            {
                pr = (MtgProd)all.get(0);
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PriviledgePayment", pr.getPrivilegePaymentId() , lang);
                //val = PicklistData.getDescription("PriviledgePayment",
                //         pr.getPrivilegePaymentId());
            }
        }
        catch (Exception e) {}

        return getTag("OpenClosedStatus", val);
    }
/**
 * 
 * @author MCM Impl Team 
 * Change log:
 * @version 1.1 10-July-2008 XS_16.17 Updated extractConditions() method to 
 *                                     add '<ConditionSection>' ellement within every <Condition> element.
 */
    protected Node extractConditions()
    {
        Node elemNode = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            List dts = (List)ch.retrieveCommitmentLetterDocTracking(deal,lang);

            if (dts.isEmpty())
               return null;

            boolean bShowRole =
                PropertiesCache.getInstance().
                    getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.show.responsibilityrole.tag", "Y").
                        equalsIgnoreCase("Y");

            Iterator i = dts.iterator();

            String role;
            //ConditionParser parser = new ConditionParser();
            Condition cond;
            DocumentTracking current;
            StringBuffer buf;
            ConditionParser p = new ConditionParser();

            while (i.hasNext())
            {
               buf = new StringBuffer();
               current = (DocumentTracking)i.next();

               //  No longer need the index of the item, since the XSL will
               //  take care of it for us, which allows for better formatting
               //buf.append(++index).append(".").append(format.space());

               buf.append(current.getDocumentTextByLanguage(lang)).append(format.space());

               if(bShowRole)
               {
                 role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", current.getDocumentResponsibilityRoleId() , lang);
                 //ALERT:BX:ZR Release2.1.docprep
                 //role = PicklistData.getDescription("ConditionResponsibilityRole",
                 //          current.getDocumentResponsibilityRoleId());
                 //role = DocPrepLanguage.getInstance().getTerm(role, lang);
                 buf.append('(').append(role).append(')');
               }

                if (elemNode == null)
                   elemNode = doc.createElement("Conditions");

                   //  JS - changed to make the XML data not RTF-specific.  This
                   //  new way adheres to other XML generation techniques,
                   //  where the lines previously divided by '\par'
                   //  are now divided into child nodes.  This allows the
                   //  XSL to format to the respective document type's syntax.
//                addTo(elemNode, "Condition", convertCRtoRTF(buf.toString(), format));

                   /*#304 add condition id
                   elemNode.appendChild(
                       convertCRToLines(doc.createElement("Condition"),
                       p.parseText(buf.toString(),lang,deal,srk)));
                    */
                  Node condNode = doc.createElement("Condition");
                  condNode.appendChild( getTag("conditionId", String.valueOf(current.getConditionId())));
                  
                  cond = new Condition(srk, current.getConditionId());
                  condNode.appendChild( getTag("conditionTypeId", String.valueOf(cond.getConditionTypeId())));
                  
                  //According to Express Commitment Letter Condition Ordering for CMLI, add ConditionResponsibilityRoleId
                  condNode.appendChild( getTag("sectionCode", current.getSectionCode()));
                  condNode.appendChild( getTag("sequenceId", String.valueOf(current.getSequenceId())));
                  condNode.appendChild( getTag("conditionResponsibilityRoleId", String.valueOf(current.getDocumentResponsibilityRoleId())));
                  
                  /*********************MCM Impl Team XS_16.17 changes statrs*****************************/
                  //@version 1.2 06-Oct-2008 Bug Fix :FXP22542 current.getSectionCode change to current.loadSectionCode().
                  addTo(condNode,"ConditionSection", current.loadSectionCode());
                  /*********************MCM Impl Team XS_16.17 changes ends*****************************/
                  convertCRToLines(condNode, p.parseText(buf.toString(),lang,deal,srk));
                  elemNode.appendChild(condNode);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    private Node extractBrokerConditions()
    {
        Node elemNode = null;

        BrokerApprovalConditionHandler bach = new BrokerApprovalConditionHandler(srk);
        Map condMap = bach.extractBrokerApprovalConditionNodes(deal,format);

        if( condMap != null )
        {

            try
            {
                Set keys = condMap.keySet();

                if (keys.isEmpty())
                   return null;

                Iterator it = keys.iterator();
                Integer currKey = null;
                String text = null;
                Integer lConditionKey;
                ConditionContainer conditionContainer;
                ConditionParser p;
                String outText;

                elemNode = doc.createElement("BrokerConditions");

                while(it.hasNext())
                {
                    StringBuffer outBuf = new StringBuffer();

                    currKey = (Integer)it.next();
                    conditionContainer = (ConditionContainer)condMap.get(currKey);
                    lConditionKey = conditionContainer.getCondIdCode();
                    text = conditionContainer.getCondText();

                    //  HACK HACK
                    //  For some reason, we sometimes get a condition back as nullnull,
                    //  so I check for it and skip it if encountered
                    if (!text.equals("nullnull"))
                    {
                        p = new ConditionParser();
                        outText = p.parseText(text, lang, deal, srk);

/*                        outText = (format.getType().startsWith("rtf")) ?
                                     convertCRtoRTF(outText, format) :
                                     outText;
*/
                        //#DG344 no need outBuf.append(++index).append(".").append(format.space());
                        outBuf.append(outText).append(format.space());

                        //addTo(elemNode, "Condition", outBuf.toString());
                        Element condNode = doc.createElement("Condition");
                        condNode.appendChild( getTag("conditionId", String.valueOf(lConditionKey)));  //#DG344
                        addTo(elemNode,convertCRToLines(condNode, outBuf.toString()));
                    }
                }
            }
            catch (Exception e){}
        }
        return elemNode;
    }

    private Node extractCommitmentReturnDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getReturnDate());
        }
        catch (Exception e) {}

        return getTag("CommitmentReturnDate", val);
    }

    //#DG454 #3559
    private Node extractPriorAdvanceDate()
    {
        String val = null;
        final int daysToAdd = -10;   // 10 business days prior

        try
        {
          BusinessCalendar bCal = new BusinessCalendar(srk);

          // Get timezone of the branch of the Deal
          BranchProfile theBranch = new BranchProfile(srk, deal.getBranchProfileId());

          // Get Number of Minutes for the Num of Business Day to add
          int daysInMinutes = daysToAdd * bCal.getBusinessDayDurationInMinutes();

          Calendar tmpCDate = Calendar.getInstance();
          tmpCDate.setTime(deal.getEstimatedClosingDate());
          tmpCDate.set(Calendar.HOUR_OF_DAY, 12);

          Date resultDate = bCal.calcBusDate(tmpCDate.getTime(), theBranch.getTimeZoneEntryId(), daysInMinutes);
          val = formatDate(resultDate);

        }
        catch (Exception e) {}

        return getTag("priorAdvanceDate", val);
    }

    protected Node extractTotalPayment()
    {
        String val = null;

        try
        {
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

            String rc = pp.getRateCode();

            val = (rc.equalsIgnoreCase("UNCNF")) ?
                  "Unconfirmed" :
                  formatCurrency(deal.getTotalPaymentAmount());
        }
        catch (Exception e){}

        return getTag("TotalPayment", val);
    }

    /***** CTFS CR224B (FXP20818) merge start *****/
    protected Node extractLenderPfoneNumber() {
        String val = null;
        try {
            LenderProfile lp = new LenderProfile(srk, deal.getLenderProfileId());
             //Contact contact = new Contact(srk, lp.getContactId(), deal
             //       .getCopyId());
            Contact contact = new Contact(srk, lp.getContactId(), 1);
            val = contact.getContactPhoneNumber();
        } catch (Exception e) {
            System.out.println("Exception:: " + e.getMessage());
            e.printStackTrace();
        }
        return getTag("LenderPhoneNumber", val);
    }

    protected Node extractLenderFaxNumber() {
        String val = null;
        try {
            LenderProfile lp = new LenderProfile(srk, deal.getLenderProfileId());
           //Contact contact = new Contact(srk, lp.getContactId(), deal
            //       .getCopyId());
           Contact contact = new Contact(srk, lp.getContactId(), 1);
            val = contact.getContactFaxNumber();

        } catch (Exception e) {

        }
        return getTag("LenderFaxNumber", val);
    }
    /***** CTFS CR224B (FXP20818) merge end *****/
    
    /**
     * Allows outside classes to build typical commitment letter tags.
     * For example, the solicitor's package has the commitment letter within it,
     * so to avoid redoing the same logic, we are going to use the same logic
     * the actual C/L data build process uses.
     *
     * We need to pass the Document because if you try to build part of a document
     * node structure and then try to add to it using another document, it
     * throws an exception.  To avoid this, we have the user pass the document
     * that they are using to construct Nodes.
     *
     * @param pDoc <code>Document</code> to use for building nodes.
     * 
     * @return The completed node tree.
     */
    public Element getCommitmentTags(Document pDoc)
    {
        //  Add all the basic tags
        Element root = buildBaseTags(pDoc);

        //  Add additional tags

        //==========================================================================
        // Guarantor Clause
        // The description in the spec describes the guarantor verb, so we are
        // using that.
        //==========================================================================
        addTo(root, extractGuarantorVerb());

        //==========================================================================
        // IAD Amount
        //==========================================================================
        addTo(root, extractIADAmount());

        //==========================================================================
        // Loan Amount
        //==========================================================================
        addTo(root, extractLoanAmount());

        //==========================================================================
        // MI Premium Amount
        //==========================================================================
        addTo(root, extractMIPremiumAmount());

        //==========================================================================
        // Admin fee
        // Total Amount
        // Be sure to keep these in order because the fees are needed in the
        // total amount calulation.
        //==========================================================================
        addTo(root, extractAdminFee());
        addTo(root, extractTotalAmount());

        //==========================================================================
        // Interest Rate
        //==========================================================================
        addTo(root, extractInterestRate());

        //==========================================================================
        // P & I Payment
        //==========================================================================
        addTo(root, extractPAndIPayment());

        //==========================================================================
        // Tax Portion
        //==========================================================================
        addTo(root, extractTaxPortion());

        //==========================================================================
        // Closing Text
        //==========================================================================
        addTo(root, extractClosingText());

        return root;
    }
}
