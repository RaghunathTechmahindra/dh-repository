package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

public class EmiliWeb extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      int miid = deal.getMortgageInsurerId();
      int mindid = deal.getMIIndicatorId();

      if((mindid == Mc.MII_REQUIRED_STD_GUIDELINES || mindid == Mc.MII_UW_REQUIRED)
         && (miid == Mc.MI_INSURER_CMHC))
      {
          ConditionHandler ch = new ConditionHandler(srk);
          val = ch.getVariableVerbiage(deal, Dc.EMILIWEB,lang);
      }

      if(val != null || val.length() > 0)
       fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
