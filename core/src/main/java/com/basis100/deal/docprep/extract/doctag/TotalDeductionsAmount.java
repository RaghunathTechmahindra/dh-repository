package com.basis100.deal.docprep.extract.doctag;

import java.util.*;

import MosSystem.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.entity.*;
import com.basis100.resources.*;


/**
 * set value to <Premium> + value <PST> + (sum of<FeeAmount>) + value <TaxHoldback>
*/

public class TotalDeductionsAmount extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    double amt = 0.0;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    try
    {
      int mid = deal.getMIIndicatorId();
      if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
            && deal.getMIUpfront().equals("N"))
      {
        amt = deal.getMIPremiumAmount();
      }

      //========================================================================
      // Another change in logic based on conversation with Robert Barker on
      // May 21, 2002. To calculate the PST amount on MI we have to go to
      // dealfee table and cumulate all amounts that are sitting there. If there
      // is nothing there we have to grab amunt from deal table.
      // Hope that this is final change on this issue.
      // Note posted by Zivko on May 21, 2002
      //========================================================================
      DealFee pstFee = new DealFee(srk,null);
      int pstFeeTypes[] = { Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM};
      List pstFees = (List)pstFee.findByDealAndTypes((DealPK)deal.getPk(), pstFeeTypes);
      double miPSTAmount ;
      if(pstFees.size() > 0)
      {
        pstFee = (DealFee)pstFees.get(0);
        miPSTAmount = pstFee.getFeeAmount();
      }else{
        miPSTAmount = deal.getMIPremiumPST();
      }

      //========================================================================
      // change requested by Rober Barker March 15, 2002
      // see the comment marked as <COMMENT> at the end of this file
      //========================================================================
      //double miPSTAmount ;
      //miPSTAmount = deal.getMIPremiumPST();
      amt += miPSTAmount;
/*
      MIPremium1 mip = new MIPremium1();
      fml = mip.extract(de, lang, format ,srk);

      if(fml.hasNonEmptyValue())
      {
       stramt = fml.getFirstStringValue(lang);
       amt = Double.parseDouble(stramt);  // ALERT trouble spot
      }
*/
      DealFee fee = new DealFee(srk,null);

      //========================================================================
      // after the discussion with David on May 31, 2002 the list has to change.
      // It is because fee types 13 and 19 have been covered in the code above.
      //========================================================================
      //int[] types = { 0,1,2,3,4,9,10,13,15,19,21,22,25,26,27};
      //========================================================================
      // after the discussion with Derek on Feb 10, 2003 the list has to change.
      // Teranet fee (28) has to be added
      //========================================================================
      //int[] types = { 0,1,2,3,4,9,10,15,21,22,25,26,27};
      int[] types = { 0,1,2,3,4,9,10,15,21,22,25,26,27,28};

      List fees = (List)fee.findByDealAndTypes((DealPK)de.getPk(), types );

      Iterator it = fees.iterator();
      DealFee current = null;

      while(it.hasNext())
      {
        current = (DealFee)it.next();
        // tracker issue #604
        if( !(current.getFeeStatusId() == Mc.FEE_STATUS_WAIVED) ){
          amt += current.getFeeAmount();
        }
      }

     fml.addCurrency(amt);

    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}


/*
<COMMENT>

 Hi Zivko......I have spoken to Joe & David about the fact that the MI amounts
 are stored in two different spots in the database. It is done this way so that
 we can populate the various documents with amounts prior to the actual MI
 approval being received. Once the approval has been rec'd we always use
 the DealFee amounts versus the Deal amounts.

Until we change the data storage of these amounts - if we do in fact change
them - we will leave the logic as is.

Please ammend the <TotalDeductionsAmount> tag to include MI Premium PST in
the calculation if it exists on the deal record.

Thanks,
Rob

-----Original Message-----
From: Robert Barker
To: Zivko Radulovic
Cc: Robert Barker
Sent: 3/12/02 2:48 PM
Subject: fix required in CTCC Solicitor's Package


Hi Zivko,

There is a fix required in CTCC's Solicitor's Package. It is on the
Solicitor's Guide template...there is a tag <TotalDeductionsAmount> that
is a calculation. In the calc, it seems to be missing picking up the
<PST> tag.

<TotalDeductionsAmount> = <Premium> + <PST> + <FeeAmount>   It doesn't
appear to include the <PST> tag.

I've attached the template and field mapping logic - hightlighted the
affected arears in yellow.

Thanks,
Rob

</COMMENT>
*/
