package com.basis100.deal.docprep.extract.data;
/**
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.log.*;
import com.basis100.xml.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.util.collections.*;
import com.basis100.picklist.*;
import java.util.*;
import java.text.*;
import MosSystem.*;

/**
 * Title:        Your Product Name
 * Description:  Your description
 * Copyright:    Copyright (c) 1999
 * Company:
 * @author
 * @version
 */

public class CTCCDisburLtrExtractor extends CTCCExtractor
{
  private static final int TASK_DISBURSE_FUND = 9;

  private double totalFeeAmount = 0d;
  protected Element root;

  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }
  protected void buildXMLDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();

    root = doc.createElement("Document");
    root.setAttribute("type","1");
    root.setAttribute("lang","1");
    doc.appendChild(root);

    Node elemNode;
    Node textNode;
    //==========================================================================
    // lender name
    //==========================================================================
    elemNode = extractLenderName();
    root.appendChild(elemNode);
    //==========================================================================
    // servicing mortgage number
    //==========================================================================
    extractServicingMortgageNumber();
    //==========================================================================
    // Premium
    //==========================================================================
    elemNode = extractPremium();
    root.appendChild(elemNode);
    //==========================================================================
    // CurrentDate
    //==========================================================================
    elemNode = extractCurrentDate();
    root.appendChild(elemNode);
    //==========================================================================
    // PST
    //==========================================================================
    elemNode = extractPST();
    root.appendChild(elemNode);
    //==========================================================================
    // IADAmount
    //==========================================================================
    elemNode = extractIADAmount();
    root.appendChild(elemNode);
    //==========================================================================
    // existingMortgage
    //==========================================================================
    elemNode = extractExistingMortgage();
    root.appendChild(elemNode);
    //==========================================================================
    // userName
    //==========================================================================
    elemNode = extractUserName();
    root.appendChild(elemNode);
    //==========================================================================
    // Properties
    //==========================================================================
    elemNode = extractProperties();
    root.appendChild(elemNode);
    //==========================================================================
    // Borrowers
    //==========================================================================
    extractBorrowers();
    //==========================================================================
    // Guarantors
    //==========================================================================
    extractGuarantors();
    //==========================================================================
    // PrimaryProperty
    //==========================================================================
    elemNode = extractPrimaryProperty();
    root.appendChild(elemNode);
    //==========================================================================
    // tax department data
    //==========================================================================
    elemNode = extractTaxDepartmentData();
    root.appendChild(elemNode);
    //==========================================================================
    // solicitor data
    //==========================================================================
    elemNode = extractSolicitorData();
    root.appendChild(elemNode);
    //==========================================================================
    // branch data
    //==========================================================================
    elemNode = extractBranchData();
    root.appendChild(elemNode);
    //==========================================================================
    // FEES
    //==========================================================================
    elemNode = extractFees();
    root.appendChild(elemNode);
    //==========================================================================
    // netAdvance
    // VERY IMPORTANT NOTE:  call for net advance MUST come after FEES
    // because during the fee processing we calculate the total fee amount.
    //==========================================================================
    elemNode = extractNetAdvance();
    root.appendChild(elemNode);
    //==========================================================================
    // Loan Amount
    //==========================================================================
    elemNode = extractLoanAmount();
    root.appendChild(elemNode);
    //==========================================================================
    // Guarantor Clause
    //==========================================================================
    elemNode = extractGuarantorClause();
    root.appendChild(elemNode);

    //==========================================================================
    // Interim final
    //==========================================================================
    addTo(root, extractInterimFinal());

    //==========================================================================
    // payment frequency
    //==========================================================================
    addTo(root, extractPaymentFrequency());

    //==========================================================================
    // P and I payment
    //==========================================================================
    addTo(root, extractPAndIPayment("pAndIPayment"));

    //==========================================================================
    // first payment date
    //==========================================================================
    addTo(root, extractFirstPaymentDate());

    //==========================================================================
    // maturity date
    //==========================================================================
    addTo(root, extractMaturityPaymentDate());

    //==========================================================================
    //interest rate
    //==========================================================================
    addTo(root, extractInterestRate());

    //==========================================================================
    // insurance tag This tag is needed for XSL test only.
    //==========================================================================
    addTo(root, extractInsuranceTag());

    //==========================================================================
    // term
    //==========================================================================
    addTo(root, extractTerm("term"));

    //==========================================================================
    // lender section
    //==========================================================================
    if (deal.getTaxPayorId() == Mc.TAX_PAYOR_LENDER   ){
      elemNode = extractLenderSection();
      root.appendChild(elemNode);
    }
    //==========================================================================
    //LOB section
    //==========================================================================
    if(deal.getLineOfBusinessId() == Mc.LOB_B){
      elemNode = doc.createElement("isLOB");
      textNode = doc.createTextNode("Y");
      elemNode.appendChild(textNode);
      root.appendChild(elemNode);

      elemNode = extractLOBSection();
      root.appendChild(elemNode);
      Node elemNodeDuplicate = elemNode.cloneNode(true);
      root.appendChild(elemNodeDuplicate);
    }else{
      elemNode = extractLOBSection();
      root.appendChild(elemNode);
    }
    //return retDoc;
  }

  private Node extractLOBSection(){
    Node retNode = doc.createElement("LOBSection")   ;
    Node elemNode;
    elemNode = extractLenderName();
    retNode.appendChild(elemNode);

    return retNode;
  }

  private Node extractLenderSection(){
    Node retNode = doc.createElement("LenderSection")   ;
    Node elemNode;
    elemNode = extractLenderName();
    retNode.appendChild(elemNode);

    return retNode;
  }

  private Node extractLenderName(){
    Node elemNode = doc.createElement("lenderName");
    Node textNode;
    LenderProfile lp;
    try{
      lp = new LenderProfile(srk, deal.getLenderProfileId());
    }catch(Exception exc){
      return elemNode;
    }
    if(lp.getLenderName() == null ){
      textNode = doc.createTextNode("");
    }else{
      textNode = doc.createTextNode(lp.getLenderName());
    }
    elemNode.appendChild(textNode);
    return elemNode;
  }

  private Node extractUserName() throws Exception
  {
    String message;
    Node retNode = doc.createElement("UserData")   ;
    Node userNameNode = doc.createElement("UserName");
    Node userExtensionNode = doc.createElement("UserExtension");
    Node textNodeUserName;
    Node textNodeUserExtension;

    JdbcExecutor jExec = srk.getJdbcExecutor();

    //==========================================================================
    // step 1: find record in assignedtaskworkqueue table
    //==========================================================================
    String sql01 = "Select * from assignedtasksworkqueue where dealid = ";
    sql01 += deal.getDealId() + " and taskid = " +  this.TASK_DISBURSE_FUND ;

    int userProfileID = -1;
    boolean gotRecord = false;
    try{
      int key = jExec.execute(sql01);

      for (; jExec.next(key); )
      {
        userProfileID = jExec.getInt(key,"userprofileid");
        gotRecord = true;
        break; // only one record, please.
      }
      jExec.closeData(key);
    } catch (Exception exc){
      message = exc.getMessage();
      message += "\n Method: extractUserName Class: CTCCDisbursLtrExtractor DealId: ";
      message += deal.getDealId() + " TaskId: " +  this.TASK_DISBURSE_FUND + "\n";
      message += "SQL statement used: " + sql01;
      throw new Exception(message);
    }

    //==================================================================================
    // product team claims that this is not even theoretical possibility. They claim
    // that record in assignedtisksworkqueue table MUST be there.
    //==================================================================================
    if( gotRecord == false ){
      if(deal.getFunderProfileId() != 0){
        userProfileID = deal.getFunderProfileId();
      }else if (deal.getUnderwriterUserId() != 0){
        userProfileID = deal.getUnderwriterUserId();
      }else{
        message = "Method: extractUserName Class: CTCCDisbursLtrExtractor DealId:";
        message += deal.getDealId() + " TaskId: " +  this.TASK_DISBURSE_FUND + "\n";
        message += "SQL statement used: " + sql01;
        message += "\n User profile record not found.\n";
        message += "Funder and underwriter ids could not be identified as well.\n";
        throw new Exception(message);
      }
    }

    //==========================================================================
    // step 2: find user profile, based on id found in step 1
    //==========================================================================
    UserProfile up;
    try{
      up = new UserProfile(srk);
      // added instituionProfileId from Deal for ML
      up = up.findByPrimaryKey( new UserProfileBeanPK(userProfileID, deal.getInstitutionProfileId()) );
    } catch(Exception exc1){
      message = exc1.getMessage();
      message += "\n Method: extractUserName Class: CTCCDisbursLtrExtractor UserProfileId: ";
      message += userProfileID;
      throw new Exception(message);
    }

    Contact contact = up.getContact();

    String userNameText = contact.getContactFirstName() + " " + contact.getContactLastName();
    textNodeUserName = doc.createTextNode(userNameText);
    userNameNode.appendChild(textNodeUserName);
    retNode.appendChild(userNameNode);

    if( contact.getContactPhoneNumberExtension() != null ){
      String userExtensionText = "" + contact.getContactPhoneNumberExtension();
      textNodeUserExtension = doc.createTextNode(userExtensionText);
      userExtensionNode.appendChild(textNodeUserExtension);
      retNode.appendChild(userExtensionNode);
    }

    return retNode;
  }

  private Node extractExistingMortgage(){
    Node retNode = doc.createElement("existingMortgage")   ;
    Node textNode;

    int purpose = deal.getDealPurposeId();
    double retVal;
    if(purpose == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT || purpose == Mc.DEAL_PURPOSE_REFI_EXTERNAL ){
      retVal = deal.getExistingLoanAmount();
    }else{
      retVal = 0d;
    }
    String retStr = DocPrepLanguage.getInstance().getFormattedCurrency(retVal,lang);

    if(retStr != null){
      textNode = doc.createTextNode(retStr);
      retNode.appendChild(textNode);
    }
    return retNode;
  }

  private Node extractPST(){
    Node retNode = doc.createElement("PST")   ;
    Node textNode;
    double retVal = deal.getMIPremiumPST();
    String retStr = DocPrepLanguage.getInstance().getFormattedCurrency(retVal,lang);

    if(retStr != null){
      textNode = doc.createTextNode(retStr);
      retNode.appendChild(textNode);
    }
    return retNode;
  }

  private Node extractPremium(){
    Node retNode = doc.createElement("premium")   ;
    Node textNode;
    double retVal = deal.getMIPremiumAmount();
    String retStr = DocPrepLanguage.getInstance().getFormattedCurrency(retVal,lang);

    if(retStr != null){
      textNode = doc.createTextNode(retStr);
      retNode.appendChild(textNode);
    }
    return retNode;
  }

  private Node extractPrimaryProperty(){
    Node propNode = doc.createElement("PrimaryProperty");
    Node elNode;
    Node textNode;

    try{
      List prop = (List)deal.getProperties();
      Iterator it = prop.iterator();
      while( it.hasNext() ){
        Property oneProperty = (Property)it.next();
        if(oneProperty.isPrimaryProperty() == false){
          continue;
        }

        String streetNum = oneProperty.getPropertyStreetNumber();
        String streetName = oneProperty.getPropertyStreetName();
        //String streetType = PicklistData.getDescription("StreetType",oneProperty.getStreetTypeId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetType = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",oneProperty.getStreetTypeId(), lang);

        //String streetDir  = PicklistData.getDescription("StreetDirection",oneProperty.getStreetDirectionId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetDir = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection",oneProperty.getStreetDirectionId(), lang);

        String val = "";

        if(streetNum != null && streetNum.length() > 0)
          val += streetNum + format.space();
        if(streetName != null && streetName.length() > 0)
          val += streetName + format.space();
        if(streetType != null && streetType.length() > 0)
          val += streetType + format.space();
        if(streetDir != null && streetDir.length() > 0)
          val += streetDir + format.space();
        elNode = doc.createElement("propertyAddress1");
        textNode = doc.createTextNode(val);
        elNode.appendChild(textNode);
        propNode.appendChild(elNode);

        if( val != null ){
          elNode = doc.createElement("propertyAddress1Upper");
          textNode = doc.createTextNode( val.toUpperCase() );
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);
        }

        val = "";
        String addr2 = oneProperty.getPropertyAddressLine2();
        if( addr2 != null ){
          val = addr2;
          elNode = doc.createElement("propertyAddress2");
          textNode = doc.createTextNode(val);
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);

          elNode = doc.createElement("propertyAddress2Upper");
          textNode = doc.createTextNode( val.toUpperCase() );
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);
        }

        String addrCity = oneProperty.getPropertyCity();
        if( addrCity != null ){
          elNode = doc.createElement("propertyAddressCity");
          textNode = doc.createTextNode(addrCity);
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);

          elNode = doc.createElement("propertyAddressCityUpper");
          textNode = doc.createTextNode(addrCity.toUpperCase());
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);

        }

        //String addrProv = PicklistData.getDescription("Province",oneProperty.getProvinceId());
        //ALERT:BX:ZR Release2.1.docprep
        String addrProv = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province",oneProperty.getProvinceId(), lang);

        if( addrProv != null ){
          elNode = doc.createElement("propertyAddressProvince");
          textNode = doc.createTextNode(addrProv);
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);

          elNode = doc.createElement("propertyAddressProvinceUpper");
          textNode = doc.createTextNode( addrProv.toUpperCase() );
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);
        }

        String addrPCode = oneProperty.getPropertyPostalFSA() + " " + oneProperty.getPropertyPostalLDU();
        if( addrPCode != null ){
          elNode = doc.createElement("propertyAddressPostal");
          textNode = doc.createTextNode(addrPCode);
          elNode.appendChild(textNode);
          propNode.appendChild(elNode);
        }

      } // end while
    }catch(Exception exc){
      return propNode;
    }
    return propNode;
  }

  private Node extractInsuranceTag(){
    Node retNode = doc.createElement("InsuranceTag");
    Node textNode;
    if( deal.getDealPurposeId() == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT ||
        deal.getDealPurposeId() == Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT){
        textNode = doc.createTextNode("Y");
        retNode.appendChild(textNode);
        return retNode;
    }else{
      return null;
    }
  }


  private Node extractLoanAmount(){
    Node retNode = doc.createElement("LoanAmount");
    Node textNode;
    double totalLoanAmount = deal.getTotalLoanAmount();

    String retStr = DocPrepLanguage.getInstance().getFormattedCurrency(totalLoanAmount,lang);

    if(retStr != null){
      textNode = doc.createTextNode(retStr);
      retNode.appendChild(textNode);
    }
    return retNode;
  }

  private Node extractGuarantorClause(){
    Node retNode = doc.createElement("guarantorClause");
    Node textNode;

    String val = "";
    try
    {
      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal.getDealId(), 
          deal.getCopyId(), deal.getInstitutionProfileId());
      int id = prim.getProvinceId();

      if(id == Mc.PROVINCE_BRITISH_COLUMBIA)
      {
        val = "Covenantor";
      }
      else
      {
        val = "Guarantor";
      }
    }catch(Exception exc){
      return retNode;
    }
    textNode = doc.createTextNode(val);
    retNode.appendChild(textNode);
    return retNode;
  }

  private Node extractNetAdvance(){
    Node retNode = doc.createElement("netAdvance");
    Node textNode;
    double totalLoanAmount = deal.getTotalLoanAmount();
    double premium = deal.getMIPremiumAmount();
    double pst = deal.getMIPremiumPST();
    double iadAmount = deal.getInterimInterestAmount();
    double existingMortgage = deal.getExistingLoanAmount();
    double feeAmt = this.totalFeeAmount;

    double retVal = totalLoanAmount - premium - pst - iadAmount - existingMortgage - feeAmt;

    String retStr = DocPrepLanguage.getInstance().getFormattedCurrency(retVal,lang);

    if(retStr != null){
      textNode = doc.createTextNode(retStr);
      retNode.appendChild(textNode);
    }
    return retNode;
  }

  private Node extractFees(){
    Node retNode = doc.createElement("Fees");
    Node oneFeeNode ;
    Node oneFeeVerbNode;
    Node oneFeeAmountNode;
    Node textNode;

    String feeVerbStr,feeAmountStr;
    this.totalFeeAmount = 0d;
    int payor = deal.getMIPayorId();
    String uf = deal.getMIUpfront();
    double oneAmt;
    try{
      Collection fees = deal.getDealFees();
      Iterator it = fees.iterator();
      int feeStatusId ;
      int feeTypes[] = { Mc.FEE_TYPE_APPRAISAL_FEE_PAYABLE,
                        Mc.FEE_TYPE_CMHC_FEE_PAYABLE,
                        Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE ,
                        Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE,
                        Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE,
                        Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE,
                        Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE};
      String type;
      String fpm ;
      while(it.hasNext()){
        DealFee oneFee = (DealFee) it.next();
        oneAmt = oneFee.getFeeAmount();
        feeStatusId = oneFee.getFeeStatusId();
        if( oneFee.getPayableIndicator() || oneAmt == 0d || !isFeeOfRequiredType(oneFee.getFee(), feeTypes) ){
          continue;
        }

        oneFeeNode = doc.createElement("Fee")    ;

        if( feeStatusId == Mc.FEE_STATUS_REQUIRED || feeStatusId == Mc.FEE_STATUS_RECIEVED ){
          if( oneFee.getFee().getFeeTypeId() == Mc.FEE_TYPE_CMHC_PREMIUM || oneFee.getFee().getFeeTypeId() == Mc.FEE_TYPE_CMHC_FEE ){
            if( uf.equals("N") == false && payor == Mc.MI_PAYOR_BORROWER ){
              //type = PicklistData.getDescription("FeeType",oneFee.getFee().getFeeTypeId() );
              //ALERT:BX:ZR Release2.1.docprep
              type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType",oneFee.getFee().getFeeTypeId(), lang);

              //fpm = PicklistData.getDescription("FeePaymentMethod",oneFee.getFeePaymentMethodId() );
              //ALERT:BX:ZR Release2.1.docprep
              fpm = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeePaymentMethod",oneFee.getFeePaymentMethodId(), lang);

              feeVerbStr = type + "(" + fpm + ")";
              oneFeeVerbNode = doc.createElement("FeeVerb");
              textNode = doc.createTextNode(feeVerbStr);
              oneFeeVerbNode.appendChild(textNode);
              oneFeeNode.appendChild(oneFeeVerbNode);

              feeAmountStr = DocPrepLanguage.getInstance().getFormattedCurrency(oneAmt,lang);
              oneFeeAmountNode = doc.createElement("FeeAmount");
              textNode = doc.createTextNode(feeAmountStr);
              oneFeeAmountNode.appendChild(textNode);
              oneFeeNode.appendChild(oneFeeAmountNode);

              retNode.appendChild(oneFeeNode);

              this.totalFeeAmount += oneAmt;
              continue;
            }
          }else{
            //type = PicklistData.getDescription("FeeType",oneFee.getFee().getFeeTypeId() );
            //ALERT:BX:ZR Release2.1.docprep
            type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType",oneFee.getFee().getFeeTypeId() , lang);

            //fpm = PicklistData.getDescription("FeePaymentMethod",oneFee.getFeePaymentMethodId() );
            //ALERT:BX:ZR Release2.1.docprep
            fpm = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeePaymentMethod",oneFee.getFeePaymentMethodId() , lang);

            feeVerbStr = type + "(" + fpm + ")";
            oneFeeVerbNode = doc.createElement("FeeVerb");
            textNode = doc.createTextNode(feeVerbStr);
            oneFeeVerbNode.appendChild(textNode);
            oneFeeNode.appendChild(oneFeeVerbNode);

            feeAmountStr = DocPrepLanguage.getInstance().getFormattedCurrency(oneAmt,lang);
            oneFeeAmountNode = doc.createElement("FeeAmount");
            textNode = doc.createTextNode(feeAmountStr);
            oneFeeAmountNode.appendChild(textNode);
            oneFeeNode.appendChild(oneFeeAmountNode);

            retNode.appendChild(oneFeeNode);
            this.totalFeeAmount += oneAmt;
            continue;
          }
        }
      }
    }catch(Exception exc){
      return retNode;
    }

    //==========================================================================
    // now the second part which has to be appended to the previous node
    //==========================================================================
    try{
      if(payor == Mc.MI_PAYOR_BORROWER && deal.getMIPremiumAmount() > 0)
      {
         String payStr = "Take from First Advance";

         if(uf != null && uf.equalsIgnoreCase("Y") )
         {
            payStr = "Cheque";
         }
        ConditionHandler ch = new ConditionHandler(srk);
        String vv = ch.getVariableVerbiage(deal,Dc.PST_VERB,lang);

        if(vv != null){
          feeVerbStr = vv + " ( " + payStr + " ) ";
        }else{
          feeVerbStr = "";
        }
        oneFeeNode = doc.createElement("Fee")    ;

        oneFeeVerbNode = doc.createElement("FeeVerb");
        textNode = doc.createTextNode(feeVerbStr);
        oneFeeVerbNode.appendChild(textNode);
        oneFeeNode.appendChild(oneFeeVerbNode);

        oneAmt = deal.getMIPremiumPST();
        feeAmountStr = DocPrepLanguage.getInstance().getFormattedCurrency(oneAmt, lang);
        oneFeeAmountNode = doc.createElement("FeeAmount");
        textNode = doc.createTextNode(feeAmountStr);
        oneFeeAmountNode.appendChild(textNode);
        oneFeeNode.appendChild(oneFeeAmountNode);

        retNode.appendChild(oneFeeNode);
        this.totalFeeAmount += oneAmt;
      }
    }catch(Exception exc1){
      return retNode;
    }
    return retNode;
  }

  private double calculateFeeAmount(SessionResourceKit srk, Deal pDeal) throws Exception
  {
    double retVal = 0d;
    double rate;
    Property prop = new Property(srk,null);
    prop = prop.findByPrimaryProperty(pDeal.getDealId(),
        pDeal.getCopyId(), pDeal.getInstitutionProfileId());
    if(prop != null){
      Province prov = new Province( srk,prop.getProvinceId() );
      rate = prov.getProvinceTaxRate();
    }
    return retVal;
  }

  private void extractGuarantors(){
    Node guarNodeUpper = doc.createElement("guarantorsUpper");
    Node guarNode = doc.createElement("guarantors");
    Node oneGuarNode;
    Node oneGuarNodeUpper;
    Node textNode;

    try{
      List borList = (List) deal.getBorrowers();
      Iterator it = borList.iterator();
      while(it.hasNext()){
        Borrower bor = (Borrower) it.next();
        if(bor.isGuarantor() == false){
          continue;
        }
        String outStr = bor.getBorrowerFirstName() + " " + bor.getBorrowerLastName();
        oneGuarNode = doc.createElement("guarantor");
        textNode = doc.createTextNode(outStr);
        oneGuarNode.appendChild(textNode);
        guarNode.appendChild(oneGuarNode);

        if(outStr != null){
          oneGuarNodeUpper = doc.createElement("guarantorUpper");
          textNode = doc.createTextNode(outStr);
          oneGuarNodeUpper.appendChild(textNode);
          guarNodeUpper.appendChild(oneGuarNodeUpper);
        }
      }
    }catch(Exception exc){
      guarNodeUpper = null;
      guarNode = null;
    }
    if (guarNodeUpper != null){
      root.appendChild(guarNodeUpper);
    }
    if (guarNode != null){
      root.appendChild(guarNode);
    }

  }


  private void extractBorrowers(){
    Node borNode = doc.createElement("borrowers");
    Node borNodeUpper = doc.createElement("borrowersUpper");
    Node oneBorNode;
    Node oneBorNodeUpper;
    Node textNode;

    try{
      List borList = (List) deal.getBorrowers();
      //========================================================
      // primary borrower first
      //========================================================
      Iterator it = borList.iterator();
      while(it.hasNext()){
        Borrower bor = (Borrower) it.next();
        if(bor.isPrimaryBorrower() == false){
          continue;
        }
        if(bor.isGuarantor()){
          continue;
        }
        String outStr = bor.getBorrowerFirstName() + " " + bor.getBorrowerLastName();
        oneBorNode = doc.createElement("borrower");
        textNode = doc.createTextNode(outStr);
        oneBorNode.appendChild(textNode);
        borNode.appendChild(oneBorNode);

        if( outStr != null ){
          oneBorNodeUpper = doc.createElement("borrowerUpper");
          textNode = doc.createTextNode( outStr.toUpperCase() );
          oneBorNodeUpper.appendChild(textNode);
          borNodeUpper.appendChild(oneBorNodeUpper);
        }

      }
      //==========================================================
      // now, all other borrowers
      //===========================================================
      it = borList.iterator();
      while(it.hasNext()){
        Borrower bor = (Borrower) it.next();
        if(bor.isPrimaryBorrower() == true){
          continue;
        }
        if(bor.isGuarantor()){
          continue;
        }
        String outStr = bor.getBorrowerFirstName() + " " + bor.getBorrowerLastName();
        oneBorNode = doc.createElement("borrower");
        textNode = doc.createTextNode(outStr);
        oneBorNode.appendChild(textNode);
        borNode.appendChild(oneBorNode);

        if( outStr != null ){
          oneBorNodeUpper = doc.createElement("borrowerUpper");
          textNode = doc.createTextNode( outStr.toUpperCase() );
          oneBorNodeUpper.appendChild(textNode);
          borNodeUpper.appendChild(oneBorNodeUpper);
        }
      }
    }catch(Exception exc){
      borNode = null;
      borNodeUpper = null;
    }
    if( borNode != null ){
      root.appendChild(borNode);
    }
    if(borNodeUpper != null){
      root.appendChild(borNodeUpper);
    }
  }

  private Node extractProperties(){
    Node propNode = doc.createElement("properties");
    Node onePropNode;
    Node elNode;
    Node textNode;

    try{
      List prop = (List)deal.getProperties();
      Collections.sort(prop,new PrimaryPropertyComparator());
      Iterator it = prop.iterator();
      while( it.hasNext() ){
        onePropNode = doc.createElement("property");
        Property oneProperty = (Property)it.next();

        String streetNum = oneProperty.getPropertyStreetNumber();
        String streetName = oneProperty.getPropertyStreetName();
        //String streetType = PicklistData.getDescription("StreetType",oneProperty.getStreetTypeId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetType = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",oneProperty.getStreetTypeId() , lang);

        //String streetDir  = PicklistData.getDescription("StreetDirection",oneProperty.getStreetDirectionId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetDir = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection",oneProperty.getStreetDirectionId() , lang);

        String val = "";

        if(streetNum != null && streetNum.length() > 0)
          val += streetNum + format.space();
        if(streetName != null && streetName.length() > 0)
          val += streetName + format.space();
        if(streetType != null && streetType.length() > 0)
          val += streetType + format.space();
        if(streetDir != null && streetDir.length() > 0)
          val += streetDir + format.space();
        elNode = doc.createElement("propertyAddress1");
        textNode = doc.createTextNode(val);
        elNode.appendChild(textNode);
        onePropNode.appendChild(elNode);

        val = "";
        String addr2 = oneProperty.getPropertyAddressLine2();
        if( addr2 != null ){
          val = addr2;
          elNode = doc.createElement("propertyAddress2");
          textNode = doc.createTextNode(val);
          elNode.appendChild(textNode);
          onePropNode.appendChild(elNode);
        }

        String addrCity = oneProperty.getPropertyCity();
        if( addrCity != null ){
          elNode = doc.createElement("propertyAddressCity");
          textNode = doc.createTextNode(addrCity);
          elNode.appendChild(textNode);
          onePropNode.appendChild(elNode);
        }

        //String addrProv = PicklistData.getDescription("Province",oneProperty.getProvinceId());
        //ALERT:BX:ZR Release2.1.docprep
        String addrProv = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province",oneProperty.getProvinceId() , lang);

        if( addrProv != null ){
          elNode = doc.createElement("propertyAddressProvince");
          textNode = doc.createTextNode(addrProv);
          elNode.appendChild(textNode);
          onePropNode.appendChild(elNode);
        }

        String propLegalLine1 = oneProperty.getLegalLine1();
        if( propLegalLine1 != null ){
          elNode = doc.createElement("LegalLine1");
          textNode = doc.createTextNode(propLegalLine1);
          elNode.appendChild(textNode);
          onePropNode.appendChild(elNode);
        }

        String propLegalLine2 = oneProperty.getLegalLine2();
        if( propLegalLine2 != null ){
          elNode = doc.createElement("LegalLine2");
          textNode = doc.createTextNode(propLegalLine2);
          elNode.appendChild(textNode);
          onePropNode.appendChild(elNode);
        }

        String propLegalLine3 = oneProperty.getLegalLine3();
        if( propLegalLine3 != null ){
          elNode = doc.createElement("LegalLine3");
          textNode = doc.createTextNode(propLegalLine3);
          elNode.appendChild(textNode);
          onePropNode.appendChild(elNode);
        }

        /*
        Commented out because postal code for property is not required
        String addrPCode = oneProperty.getPropertyPostalFSA() + " " + oneProperty.getPropertyPostalLDU();
        if( addrPCode != null ){
          elNode = pDoc.createElement("propertyAddressPCode");
          textNode = pDoc.createTextNode(addrProv);
          elNode.appendChild(textNode);
          onePropNode.appendChild(elNode);
        }
        */

        //======================================================================
        // now append these nodes to the properties node
        //======================================================================
        propNode.appendChild(onePropNode);
      } // end while
    }catch(Exception exc){
      return propNode;
    }
    return propNode;
  }

  private Node extractPaymentFrequency(){
    Node retNode = doc.createElement("paymentFrequency");
    Node textNode;

    String retValStr = null;
    String retValStrTranslated = null;
    try{
      int id = deal.getPaymentFrequencyId();
      //retValStr = PicklistData.getDescription("PaymentFrequency",id);
      //ALERT:BX:ZR Release2.1.docprep
      retValStr = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency",id , lang);

      retValStrTranslated = DocPrepLanguage.getInstance().getTerm(retValStr,lang);
    }catch(Exception exc){
      return null;
    }

    textNode = doc.createTextNode(retValStrTranslated);
    retNode.appendChild(textNode);
    return retNode;
  }

  private Node extractInterimFinal(){
    Node retNode = doc.createElement("interimFinal");
    Node textNode;

    boolean interimFound = false;
    boolean finalFound = false;
    String retValStr = null;

    try{
      Collection fees = deal.getDealFees();
      Iterator it = fees.iterator();
      while( it.hasNext() ){
        DealFee df = (DealFee) it.next();
        int ft = df.getFee().getFeeTypeId();
        if(ft == Mc.FEE_TYPE_PROPERTY_TAX_HOLDBACK_INTERIM){
          interimFound = true;
        }else if(ft == Mc.FEE_TYPE_PROPERTY_TAX_HOLDBACK_FINAL){
          finalFound = true;
        }
      }
      //========================================================================
      // the following is theoretical case according to the product team. Both
      // fees should NEVER be found in the dealfee table.
      //========================================================================
      if(interimFound && finalFound){
        srk.getSysLogger().error("Class:CTCCDisburLtrExtractor Method:extractInterimFinal");
        return null;
      }
      ConditionHandler ch = new ConditionHandler(srk);
      if(interimFound){
        retValStr = ch.getVariableVerbiage(deal, Dc.PROPERTY_TAX_HOLDBACK_INTERIM,lang);
      }
      if(finalFound){
        retValStr = ch.getVariableVerbiage(deal, Dc.PROPERTY_TAX_HOLDBACK_FINAL,lang);
      }
    }catch(Exception exc){
      return null;
    }

    if( retValStr != null ){
      textNode = doc.createTextNode(retValStr);
      retNode.appendChild(textNode);
    } else {
      textNode = doc.createTextNode("");
      retNode.appendChild(textNode);
    }
    return retNode;
  }


  private Node extractInterestRate(){
    Node retNode = doc.createElement("interestRate");
    Node textNode;

    String retValStr = null;

    try{
      //======================================================================================
      // PROGRAMMER'S NOTE: Deal table - column pricingprofileid does not contain
      // pricing profile id, it contains pricing rate inventory id. So the path goes:
      // take pricingprofileid from deal, based on that key find the pricingrateinventoryid
      // in the pricingrateinventory table. From that record pick pricingprofileid value and
      // that is the key to search in the pricingprofile table.
      // Method findByPricingRateInventory in the PricingProfile class does exactly that.
      //=====================================================================================
      //PricingProfile pp = new PricingProfile(srk,deal.getPricingProfileId());
      PricingProfile pp = new PricingProfile(srk);
      pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );
      String rc = pp.getRateCode();

      if(rc.equalsIgnoreCase("UNCNF"))
      {
        ConditionHandler ch = new ConditionHandler(srk);
        retValStr = ch.getVariableVerbiage(deal, Dc.UNCONFIRMED_RATE,lang);
      }
      else if(deal.getDiscount() != 0 && deal.getPremium() != 0)
      {
        retValStr = "*" + format.space();
      }
      else
      {
        retValStr = DocPrepLanguage.getInstance().getFormattedInterestRate(deal.getNetInterestRate(),lang);
      }
    }catch(Exception exc){
      return null;
    }

    textNode = doc.createTextNode(retValStr);
    retNode.appendChild(textNode);
    return retNode;
  }

  private Node extractMaturityPaymentDate(){
    Node retNode = doc.createElement("maturityDate");
    Node textNode;

    Date fpd = deal.getMaturityDate();

    String retValStr = DocPrepLanguage.getInstance().getFormattedDate(fpd,lang);

    textNode = doc.createTextNode(retValStr);
    retNode.appendChild(textNode);
    return retNode;
  }

  private Node extractFirstPaymentDate(){
    Node retNode = doc.createElement("firstPaymentDate");
    Node textNode;

    Date fpd = deal.getFirstPaymentDate();

    String retValStr = DocPrepLanguage.getInstance().getFormattedDate(fpd,lang);

    textNode = doc.createTextNode(retValStr);
    retNode.appendChild(textNode);
    return retNode;
  }

/*
  private Node extractCurrentDate(){
    Node retNode = doc.createElement("CurrentDate");
    Node textNode;

    Date date = new Date();
    String outStr = DocPrepLanguage.getInstance().getFormattedDate(date,lang);

    textNode = doc.createTextNode(outStr);
    retNode.appendChild(textNode);
    return retNode;
  }
*/
  private Node extractBranchData()
  {
    Node retNode = doc.createElement("BranchData");
    Node elemNode;
    Node textNode;
    try
    {
      BranchProfile bp = new BranchProfile(srk,deal.getBranchProfileId());

      Contact cont = bp.getContact();
      Addr addr = cont.getAddr();

      String outStr;

      elemNode = doc.createElement("BranchAddress1");
      outStr = addr.getAddressLine1();
      textNode = doc.createTextNode(outStr);
      elemNode.appendChild(textNode);
      retNode.appendChild(elemNode);

      outStr = addr.getAddressLine2();
      if(outStr != null ){
        if(!outStr.trim().equals("")){
          elemNode = doc.createElement("BranchAddress2");
          textNode = doc.createTextNode(outStr);
          elemNode.appendChild(textNode);
          retNode.appendChild(elemNode);
        }
      }

      outStr = addr.getCity();
      if(outStr != null){
        elemNode = doc.createElement("BranchAddressCity");
        textNode = doc.createTextNode(outStr);
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);
      }

      //outStr = PicklistData.getDescription("Province", addr.getProvinceId() );
      //ALERT:BX:ZR Release2.1.docprep
      outStr = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province", addr.getProvinceId() , lang);

      if(outStr != null){
        elemNode = doc.createElement("BranchAddressProvince");
        textNode = doc.createTextNode(outStr);
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);
      }

      outStr = addr.getPostalFSA() + " " + addr.getPostalLDU();
      if(outStr != null){
        elemNode = doc.createElement("BranchAddressPostal");
        textNode = doc.createTextNode(outStr);
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);
      }

    } catch(Exception exc){
      return retNode;
    }
    return retNode;
  }


  private Node extractSolicitorData()
  {
    Node retNode = doc.createElement("SolicitorData");
    Node elemNode;
    Node textNode;
    try
    {
      PartyProfile pp = new PartyProfile(srk);
      pp.setSilentMode(true);
      pp = pp.findByDealSolicitor(deal.getDealId());

      Contact cont = new Contact(srk, pp.getContactId(), 1);
      Addr addr = cont.getAddr();

      elemNode = doc.createElement("SolicitorName");
      String outStr = "" + cont.getContactFirstName() + " " + cont.getContactLastName();
      textNode = doc.createTextNode(outStr);
      elemNode.appendChild(textNode);
      retNode.appendChild(elemNode);

      // the same element (SolicitorName) but in upper case
      elemNode = doc.createElement("SolicitorNameUpper");
      outStr = "" + cont.getContactFirstName() + " " + cont.getContactLastName();
      textNode = doc.createTextNode( outStr.toUpperCase() );
      elemNode.appendChild(textNode);
      retNode.appendChild(elemNode);

      elemNode = doc.createElement("SolicitorAddress1");
      outStr = addr.getAddressLine1();
      textNode = doc.createTextNode(outStr);
      elemNode.appendChild(textNode);
      retNode.appendChild(elemNode);

      // the same element (SolicitorAddress1) but in uppercase
      elemNode = doc.createElement("SolicitorAddress1Upper");
      outStr = addr.getAddressLine1();
      textNode = doc.createTextNode( outStr.toUpperCase() );
      elemNode.appendChild(textNode);
      retNode.appendChild(elemNode);

      outStr = addr.getAddressLine2();
      if(outStr != null ){
        if(!outStr.trim().equals("")){
          elemNode = doc.createElement("SolicitorAddress2");
          textNode = doc.createTextNode(outStr);
          elemNode.appendChild(textNode);
          retNode.appendChild(elemNode);

          // the same node (SolicitorAddress2) but in uppercase
          elemNode = doc.createElement("SolicitorAddress2Upper");
          textNode = doc.createTextNode( outStr.toUpperCase() );
          elemNode.appendChild(textNode);
          retNode.appendChild(elemNode);

        }
      }

      outStr = addr.getCity();
      if(outStr != null){
        elemNode = doc.createElement("SolicitorAddressCity");
        textNode = doc.createTextNode(outStr);
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);

        // the same node (SolicitorAddressCity) but in uppercase
        elemNode = doc.createElement("SolicitorAddressCityUpper");
        textNode = doc.createTextNode( outStr.toUpperCase() );
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);

      }

      //outStr = PicklistData.getDescription("Province", addr.getProvinceId() );
      //ALERT:BX:ZR Release2.1.docprep
      outStr = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province", addr.getProvinceId() , lang);

      if(outStr != null){
        elemNode = doc.createElement("SolicitorAddressProvince");
        textNode = doc.createTextNode(outStr);
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);

        // the same node (SolicitorAddressProvince) but in uppercase
        elemNode = doc.createElement("SolicitorAddressProvinceUpper");
        textNode = doc.createTextNode( outStr.toUpperCase() );
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);

      }

      outStr = addr.getPostalFSA() + " " + addr.getPostalLDU();
      if(outStr != null){
        elemNode = doc.createElement("SolicitorAddressPostal");
        textNode = doc.createTextNode(outStr);
        elemNode.appendChild(textNode);
        retNode.appendChild(elemNode);
      }

    } catch(Exception exc){
      return retNode;
    }
    return retNode;
  }

  private Node extractTaxDepartmentData()
  {

    Node retNode = doc.createElement("TaxDepartmentData")   ;
    Node tdNameNode = doc.createElement("TaxDepartmentName");
    Node tdAddress1Node = doc.createElement("TaxDepartmentAddress1");
    Node tdAddress2Node = doc.createElement("TaxDepartmentAddress2");
    Node tdCityNode = doc.createElement("TaxDepartmentCity");
    Node tdProvinceNode = doc.createElement("TaxDepartmentProvince");
    Node tdPostalNode = doc.createElement("TaxDepartmentPostal");

    Node tdNameNodeUpper = doc.createElement("TaxDepartmentNameUpper");
    Node tdAddress1NodeUpper = doc.createElement("TaxDepartmentAddress1Upper");
    Node tdAddress2NodeUpper = doc.createElement("TaxDepartmentAddress2Upper");
    Node tdCityNodeUpper = doc.createElement("TaxDepartmentCityUpper");
    Node tdProvinceNodeUpper = doc.createElement("TaxDepartmentProvinceUpper");

    Node textNode;

    Collection parties;
    try{
      PartyProfile partyProfile = new PartyProfile(srk);
      DealPK dealpk = new DealPK(deal.getDealId(), deal.getCopyId());
      parties = partyProfile.findByDealAndType(dealpk, Mc.PARTY_TYPE_MUNICIPALITY );
    } catch(Exception exc){
      return retNode;
    }

    Iterator it = parties.iterator();
    while ( it.hasNext() ){
      PartyProfile pp = (PartyProfile)it.next();

      if(pp.getPartyCompanyName() != null){
        textNode = doc.createTextNode( pp.getPartyCompanyName() );
        tdNameNode.appendChild(textNode);
        retNode.appendChild(tdNameNode);

        textNode = doc.createTextNode( pp.getPartyCompanyName().toUpperCase() );
        tdNameNodeUpper.appendChild(textNode);
        retNode.appendChild(tdNameNodeUpper);
      }

      try{
        Contact contact = pp.getContact();
        Addr addr = contact.getAddr();

        if( addr.getAddressLine1() != null ){
          textNode = doc.createTextNode(addr.getAddressLine1() );
          tdAddress1Node.appendChild(textNode);
          retNode.appendChild(tdAddress1Node);

          textNode = doc.createTextNode(addr.getAddressLine1().toUpperCase() );
          tdAddress1NodeUpper.appendChild(textNode);
          retNode.appendChild(tdAddress1NodeUpper);
        }

        if(addr.getAddressLine2() != null ){
          if(!addr.getAddressLine2().trim().equals("")){
            textNode = doc.createTextNode(addr.getAddressLine2() );
            tdAddress2Node.appendChild(textNode);
            retNode.appendChild(tdAddress2Node);

            textNode = doc.createTextNode(addr.getAddressLine2().toUpperCase() );
            tdAddress2NodeUpper.appendChild(textNode);
            retNode.appendChild(tdAddress2NodeUpper);
          }
        }

        if( addr.getCity() != null ){
          textNode = doc.createTextNode( addr.getCity() );
          tdCityNode.appendChild(textNode);
          retNode.appendChild(tdCityNode);

          textNode = doc.createTextNode( addr.getCity().toUpperCase() );
          tdCityNodeUpper.appendChild(textNode);
          retNode.appendChild(tdCityNodeUpper);
        }

        //String prov = PicklistData.getDescription("Province",addr.getProvinceId());
        //ALERT:BX:ZR Release2.1.docprep
        String prov = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province", addr.getProvinceId() , lang);

        if( prov != null ){
          textNode = doc.createTextNode(prov );
          tdProvinceNode.appendChild(textNode);
          retNode.appendChild(tdProvinceNode);

          textNode = doc.createTextNode(prov.toUpperCase() );
          tdProvinceNodeUpper.appendChild(textNode);
          retNode.appendChild(tdProvinceNodeUpper);
        }

        textNode = doc.createTextNode(addr.getPostalFSA() + " " + addr.getPostalLDU() );
        tdPostalNode.appendChild(textNode);
        retNode.appendChild(tdPostalNode);

      } catch(Exception exc1){
        return retNode;
      }
    }

    return retNode;
  }

  private void extractServicingMortgageNumber(){
    Node elemNode = doc.createElement("servicingMortgageNumber");
    Node textNode;
    if(deal.getServicingMortgageNumber() == null){
      textNode = doc.createTextNode("");
    }else{
      textNode = doc.createTextNode(deal.getServicingMortgageNumber());
    }
    elemNode.appendChild(textNode);
    root.appendChild(elemNode);
  }
}



