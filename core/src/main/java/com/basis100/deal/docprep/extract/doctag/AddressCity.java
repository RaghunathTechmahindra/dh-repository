package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class AddressCity extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        Borrower primary = new Borrower(srk,null);
        primary = primary.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
        return extract(primary, lang, format, srk);
      }
      else if(clsid == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;
        BorrowerAddress baddr = new BorrowerAddress(srk);
        baddr = baddr.findByCurrentAddress(bor.getBorrowerId(),bor.getCopyId());

        return extract(baddr, lang, format, srk);
      }
      else if(clsid == ClassId.BORROWERADDRESS)
      {
        BorrowerAddress baddr = (BorrowerAddress)de;
        Addr add = baddr.getAddr();
        String val = add.getCity();

        if(val != null || val.length() > 0)
        {
          fml.addValue(val,fml.STRING);
        }
      }
      else if(clsid == ClassId.PROPERTY)
      {
        Property p = (Property)de;
        String val = p.getPropertyCity();

        if(val != null && val.length() > 0)
            fml.addValue(val,fml.STRING);
      }
       else if(de.getClassId() == ClassId.PARTYPROFILE)
      {
        PartyProfile party =(PartyProfile)de;
        Contact c = new Contact(srk,party.getContactId(),1);
        Addr addr = c.getAddr();

        String val = addr.getCity();

        if(val != null || val.length() != 0)
        {
          fml.addValue(val,fml.STRING);
        }

      }
      else if(de.getClassId() == ClassId.EMPLOYMENTHISTORY)
      {
        EmploymentHistory hist =(EmploymentHistory)de;
        Contact c = hist.getContact();
        Addr addr = c.getAddr();

        if(addr != null )
        {
          String lCity = addr.getCity();
          if( lCity != null && lCity.length()> 0){
             fml.addValue(lCity, fml.STRING);
          }
        }
      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
