package com.basis100.deal.docprep.rtf;

import java.util.*;
/**
 * Class ReportPortions keeps template portions in
 * hashtable. There are two separate strings which
 * hold start of the rtf file and end of the rtf file.
*/
public class ReportPortions {
  private java.util.Hashtable templateList;
  private String rtfFileStart;
  private String rtfFileEnd;

  /**
   * Constructor. Creates new hashtable.
  */
  public ReportPortions() {
    templateList = new java.util.Hashtable();
    rtfFileStart = null;
    rtfFileEnd = null;
  }

  /**
   * Returns start portions of the rtf file.
  */
  public String getRtfFileStart()
  {
    return rtfFileStart;
  }

  /**
   * Returns closing portion of the rtf file.
  */
  public String getRtfFileEnd()
  {
    return rtfFileEnd;
  }

  /**
   * Method stores template portion in the hash table based on teh key with
   * only two exceptions. If the key has value rtffilestart or rtffileend
   * the value of template portion gets stored in separate strings.
   * @param parKey id code of template portion
   * @param parPortion template text.
  */
  public void addTemplatePortion(String parKey, String parPortion) throws Exception
  {
    parKey = parKey.trim();
    if(parKey.equalsIgnoreCase("rtffilestart") )
    {
      rtfFileStart = parPortion;
      //return;
    }
    if(parKey.equalsIgnoreCase("rtffileend") )
    {
      rtfFileEnd = parPortion;
      //return;
    }

    if( templateList.contains(parKey) )
    {
      throw new Exception("Template portion : " + parKey + "  already exists.");
    }
    else
    {
      templateList.put(parKey, parPortion);
    }
  }

  /**
   * Method returns the text for the template portion which matches the
   * required key.
  */
  public String retrieveTemplatePortion(String parKey)
  {
    String retVal = (String) templateList.get(parKey);
    return retVal;
  }

}