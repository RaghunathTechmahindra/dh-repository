package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class Privilege extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {

      ConditionHandler ch = new ConditionHandler(srk);

      int id = deal.getPrivilegePaymentId();

      //String option = PicklistData.getDescription("PrivilegePayment", id);
      //ALERT:BX:ZR Release2.1.docprep
      String option  = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PrivilegePayment",  id, lang);

      if(option.equalsIgnoreCase("Open"))
      {
        val = ch.getVariableVerbiage(deal,Dc.PRIVILEGE_OPEN,lang);

      }
      else if(option.equalsIgnoreCase("Not Allowed"))
      {
        val = ch.getVariableVerbiage(deal,Dc.PRIVILEGE_NOT_ALLOWED,lang);

      }
      else
      {
        val = ch.getVariableVerbiage(deal,Dc.PRIVILEGE,lang);

      }

      fml.addValue(val, fml.STRING);
      return fml;
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch (Exception e)
    {
      fml = null;

      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
  }
}
