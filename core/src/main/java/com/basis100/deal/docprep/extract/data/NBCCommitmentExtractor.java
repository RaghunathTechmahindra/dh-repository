/**
 *
 */
package com.basis100.deal.docprep.extract.data;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Province;
import com.basis100.deal.util.collections.BorrowerTypeComparator;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.xml.XmlToolKit;

import com.basis100.deal.docprep.ExpressDocprepHelper;

/**
 * <p>
 * Title: NBCCommitmentExtractor.java
 * </p>
 * <p>
 * Description: It extends the GENXExtractor & creates a consolidated xml
 * containing the data required for all the non-disclosure documents
 * FXP16891 - NBC CR007 Commitment Letter Changes - 01 Aug,2007
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2007
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * @author Venkata
 * @version 1.0
 */
public class NBCCommitmentExtractor extends GENXExtractor
{
  NBCDataProvider nbcDP;

  //Added constant for Bug # 1615
  private static final int INT_ZERO_CHECK = 0;

  private static final int VAR_INT_RATE = 2;

  private static final int INT_RATE_TO_PRIME = 1;
 
  public NBCCommitmentExtractor()
  {
      super();
  }

  public NBCCommitmentExtractor(XSLExtractor x)
  {
      super(x);
  }


  /*
     * Called by DocumentGeneratorHandler for all document types
     * @see com.basis100.deal.docprep.extract.data.DealExtractor#buildDataDoc()
     */
  protected void buildDataDoc() throws Exception
  {
	buildXMLDataDoc();
  }

  /*
     * Builds entire xml structure for NBC Non-Disclosure Documents
     * @see com.basis100.deal.docprep.extract.data.GENXExtractor#buildXMLDataDoc()
     */
  protected void buildXMLDataDoc() throws Exception
  {
	doc = XmlToolKit.getInstance().newDocument();

	Element docRoot = doc.createElement("Document");
	
	//	Venkata - 2 August, 2007
	//make a full blown deal tree available so that this xml can be used for all templates
	createDealTree(docRoot);
	
	docRoot.appendChild(buildBaseTags(doc));
	doc.appendChild(docRoot);
  }

  /**
     * buildBaseTags Adds xml tags containing the data (based on the entity
     * columns & the conditions mentioned)to the existing root tag
     * @param root
     * @return
     */
  private Element buildBaseTags(Document pDoc)
  {
	Property pp = getPrimaryProperty();

	nbcDP = new NBCDataProvider(srk, deal, lang);
	nbcDP.setDocumentFormat(format);
	
	Element root = pDoc.createElement("CommitmentLetter");
    
	/* Added code fix for SCR No. # 1134 --
     */
    String imgLocation = ExpressDocprepHelper.getTemplateImageLocation();
	try
	{
	  // For logo image paths
					addTo(root, getTag("Logo_Image", imgLocation));
      /* End of 1134 fix */

	  // For data mapping
					//addTo(root, getTag("term2", nbcDP.exTerm2()));
					addTo(root, getTag("Language",lang+""));
					addTo(root, getTag("amortPeriod", nbcDP.exAmortizationTerm()));
					addTo(root, getTag("commitValUntil", nbcDP.exCommitmentExpirationDate()));
					addTo(root, getTag("discount", nbcDP.exFixedDiscount()));
					addTo(root, getTag("downPayment", nbcDP.exDownPayment()));
					addTo(root, getTag("dateFirstPayment", nbcDP.exFirstPaymentDate()));
					addTo(root, getTag("intAdjDate", nbcDP.exInterimInterestAdjustmentDate()));
					addTo(root, getTag("matDate", nbcDP.exMaturityDate()));
					addTo(root, getTag("amtDedPrinAmtBor", nbcDP.exMIPremiumAmount()));
					addTo(root, getTag("loanInsType1", nbcDP
						  .exLoanInsType(Mc.MI_INSURER_BLANK)));
					addTo(root, getTag("loanInsType2", nbcDP
						  .exLoanInsType(Mc.MI_INSURER_CMHC)));
					addTo(root, getTag("loanInsType3", nbcDP.exLoanInsType(Mc.MI_INSURER_GE)));

					addTo(root, getTag("fxRate", (nbcDP
						  .exNetInterestRate(Mc.INTEREST_TYPE_FIXED)!=null)?(String.valueOf(nbcDP
						  .exNetInterestRate(Mc.INTEREST_TYPE_FIXED))):("")));
					  // SCR#1494 fix - start
					addTo(root, getTag("varRate", (nbcDP
							  .exNetInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE)!=null)?(String.valueOf(nbcDP
							  .exNetInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE))):("")));
					  // SCR#1494 fix - end
					addTo(root, getTag("availAmt", nbcDP.exNetLoanAmount()));
					addTo(root, getTag("diffMorRateNSpread", nbcDP.exDiffMortRateSpread()));
					addTo(root, getTag("refPaymentIC", nbcDP.exRefPayment()));
					addTo(root, getTag("monPayment", nbcDP.exMonthlyPayment()));
					addTo(root, getTag("fixedPayAmt", nbcDP.exFixedPayAmount()));
					addTo(root, getTag("empProgPay", nbcDP.exEmployeeProgPayment()));
					addTo(root, getTag("paymentFreq1", nbcDP.exPaymentFreq(
						 Mc.PAY_FREQ_MONTHLY, Mc.PAY_FREQ_MONTHLY)));
					addTo(root, getTag("paymentFreq2", nbcDP.exPaymentFreq(
					  Mc.PAY_FREQ_BIWEEKLY, Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)));
				    addTo(root, getTag("paymentFreq3", nbcDP.exPaymentFreq(
					   Mc.PAY_FREQ_WEEKLY, Mc.PAY_FREQ_ACCELERATED_WEEKLY)));
					addTo(root, getTag("cricRate", nbcDP.exCriticalRate()));

					int paymentTermTypeId = findPaymentTermTypeId(srk, deal.getPaymentTermId());
					addTo(root, getTag("openRateLoan", nbcDP.exRateLoan("open", paymentTermTypeId)));
					addTo(root, getTag("closedRateLoan", nbcDP.exRateLoan("closed", paymentTermTypeId)));
					addTo(root, getTag("intRateFixed", nbcDP.exInterestRateFixed()));

					addTo(root, getTag("calcField", nbcDP.exCalcField()));
					addTo(root, getTag("intRateSpread", nbcDP.exIntRateSpread()));
					addTo(root, getTag("intRateSpreadVal", nbcDP.exPrimeBaseAdj()));
					addTo(root, getTag("varRateToPrime", nbcDP.exVarRateToPrime()));
					addTo(root, getTag("varIntRate", nbcDP.exProdInterestRate(VAR_INT_RATE)));
					addTo(root, getTag("intRatePrime", nbcDP
									.exProdInterestRate(INT_RATE_TO_PRIME)));
					addTo(root, getTag("infoSol", nbcDP.exSolInstrn()));

					addTo(root, getTag("teaserRate", nbcDP.exTeaserRate()));
					addTo(root, getTag("teaserPayAmt", nbcDP.exTeaserPIAmount()));
					addTo(root, getTag("teaserTerm", nbcDP.exTeaserTerm()));
					addTo(root, getTag("princAmtBor", nbcDP.exTotalLoanAmount()));
					addTo(root, getTag("purPrice", nbcDP.exTotalPurchasePrice()));
					addTo(root, getTag("regAmtCharge", nbcDP.exRegCharge()));
					addTo(root, getTag("borName5", exAllBorrowers(nbcDP)));
					addTo(root, getTag("intRevFreq", nbcDP.exIntRevFreq()));
					addTo(root, getTag("intRateRevFreq1", nbcDP.exIntRateRevFreq1()));
					addTo(root, getTag("intRateRevFreq2", nbcDP.exIntRateRevFreq2()));

					addTo(root, getTag("term2", nbcDP.exTerm2()));
					addTo(root, getTag("propAddress", extractPropertyAddress("propAddress")));
	  
					addTo(root, getTag("cashbackAmt1", nbcDP.exCashBkAmt()));

					addTo(root, getTag("date1", formatDate(new Date())));
					addTo(root, getTag("branchAddress", nbcDP.exCTPHAddress()));

					addTo(root, getTag("propTaxInstall", formatCurrency(nbcDP.exEscrowPaymentAmount(Mc.ESCROW_PAYMENT_TERM))));

					addTo(root, getTag("other1", formatCurrency(nbcDP
						  .exEscrowPaymentAmount(Mc.ESCROWTYPE_ADMIN_FEE))));
					addTo(root, getTag("totInsPrem", formatCurrency(nbcDP
						  .exEscrowPaymentAmount(Mc.ESCROWTYPE_CREDIT_LIFE))));

	  
					addTo(root, getTag("intRateRenewAuto1", nbcDP.exIntRateAutoRenew()));
					addTo(root, getTag("intRateRenNotAuto1", nbcDP.exIntRateNotAutoRenew()));
					addTo(root, getTag("cappedRate", nbcDP.exCappedRate()));

					addTo(root, getTag("budgetOpt", nbcDP.exBudgetOpt()));
						  // Changed method call for codefix BUG# 1615
					if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC 
						&& deal.getInterestTypeId()!= INT_ZERO_CHECK){
					  addTo(root, getTag("spreadGT0", nbcDP.exSpreadGT0()));
					 }
	
					addTo(root, getTag("alwUnchkd", nbcDP.exAlwUnChkd()));

					String[] Text= nbcDP.exDocTrackText();
					for(int loopCntr=0;loopCntr<Text.length;loopCntr++)
						addTo(root,getTag("docTrackText", Text[loopCntr]));
					addTo(root, getTag("loanAmount", nbcDP.exLoanAmount()));

					addTo(root, getTag("intRateActDate", nbcDP.exInterestRateActDate()));
					addTo(root,getTag("dateT26a",exDateforT26a()));
	}
	catch (Exception e)
	{
	  e.printStackTrace();
	  String msg = "NBCExtractor failed while building tags"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}
	return root;
  }

  
  /**
     * exPropStreet Returns the Property Street details concatenated together &
     * separated by commmas
     * @return
     * Change: Removed commas for proper formatting, defect 1472
     */
  protected String exPropStreet()
  {
	String streetAddress = "";
	try
	{
	  Property p = getPrimaryProperty();

	  if (!isEmpty(p.getPropertyStreetName()))
	  {
		streetAddress += format.space() + p.getPropertyStreetName();
	  }
	  if (!isEmpty(p.getStreetTypeDescription()))
	  {
		streetAddress += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "STREETTYPE", p.getStreetTypeId(), lang);
	  }
	  streetAddress += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "STREETDIRECTION", p.getStreetDirectionId(), lang);
	}

	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Property Street"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return streetAddress;
  }

  /**
     * extractAddressLine Fetches the variuos address fields, concatenates them
     * together separated by commas & returns the final complete address
     * @param p
     *            (Property)
     * @return
     */
  protected String extractAddressLine(Property p, int lang)
  {
	String val = null;

	try
	{
	  val = "";

	  if (!isEmpty(p.getPropertyStreetNumber()))
	  {
		val += p.getPropertyStreetNumber();
	  }

	  if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) {
		 // Street_number Street_name Street_type, Unit#, Address_line2
    	  
    		val +=  exPropStreet();

    	  val += ", ";

    	  if (!isEmpty(p.getUnitNumber()))
    	  {
    		val += p.getUnitNumber();
    		val += ", ";
    	  }

	  } else  {
		  // proper French format:
	  	  // Street_number Street_type Street_name Unit#, Address_line2
	  		
	  	  val += exPropStreet();
	  	  
	  	  if (!isEmpty(p.getUnitNumber()))
		  {
			val += format.space() + p.getUnitNumber();
		  }
	  	  val += ", ";
	  }

	  if (!isEmpty(p.getPropertyAddressLine2()))
	  {
		val += p.getPropertyAddressLine2();
		val += ", ";
	  }

	  if (!isEmpty(p.getPropertyCity()))
	  {
		val += p.getPropertyCity();
		val += ", ";
	  }

	  Province prov = new Province(srk, p.getProvinceId());

	  if (!isEmpty(prov.getProvinceName()))
	  {
		val += prov.getProvinceName();
		val += " ";
	  }

	  if (!isEmpty(p.getPropertyPostalFSA()))
	  {
		val += p.getPropertyPostalFSA();
		val += " ";
	  }
	  if (!isEmpty(p.getPropertyPostalLDU()))
	  {
		val += p.getPropertyPostalLDU();
	  }
	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Address Line"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return val;
  }



  /**
     * extractPropertyAddress Instantiates the Property Class & calls the
     * extractAddressLine method to get the Final Address
     * @param tagName
     * @return
     */
  protected String extractPropertyAddress(String tagName)
  {
	String finalAddress = null;
	try
	{
	  Property p = getPrimaryProperty();
	  finalAddress = extractAddressLine(p, lang);

	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Property Address"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return finalAddress;
  }



  
  
  public String exAllBorrowers(NBCDataProvider ndp)
  {
	String val = "";
	val = ndp.exPrimaryBorrowerName();
	val += ndp.exCoBorrowersName() == null ? "" : ", "
		+ ndp.exCoBorrowersName();
	return val;

  }

  public Node exDocTrackText(String[] text)
  {
	Node elemNode = null;

	if (elemNode == null)
	{
	  elemNode = doc.createElement("docTrackText");
	}
	for ( int textCounter = 0; textCounter < text.length; textCounter++ )
	{
	  if (textCounter < text.length)
		addTo(elemNode, "data", text[textCounter]);
	   elemNode.appendChild(elemNode);
	}
		return elemNode;
  }

 
  /* code fix 1615 - start
	 * Added check for Producttypeid == sloc and returns the current date
	 */
	protected String exDateforT26a() {
	  String dt="";
	  if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC)
		dt= formatDate(new Date());
	  return dt;
	}
	
	/**
     * Allows outside classes to build typical commitment letter tags.
     * For example, the solicitor's package has the commitment letter within it,
     * so to avoid redoing the same logic, we are going to use the same logic
     * the actual C/L data build process uses.
     *
     * We need to pass the Document because if you try to build part of a document
     * node structure and then try to add to it using another document, it
     * throws an exception.  To avoid this, we have the user pass the document
     * that they are using to construct Nodes.
     *
     * @param pDoc <code>Document</code> to use for building nodes.
     * @return The completed node tree.
     */
    public Element getCommitmentTags(Document pDoc)
    {
        //  Add all the basic tags
    	
    	Element root = buildBaseTags(pDoc);

        return root;
    }
}
