/**
 *
 */
package com.basis100.deal.docprep.extract.data;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Province;
import com.basis100.deal.util.collections.BorrowerTypeComparator;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.xml.XmlToolKit;

import com.basis100.deal.docprep.ExpressDocprepHelper;

/**
 * <p>
 * Title: NBCExtractor.java
 * </p>
 * <p>
 * Description: It extends the GENXExtractor & creates a consolidated xml
 * containing the data required for all the non-disclosure documents
 * </p>
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * <p>
 * Company: Filogix Inc.
 * </p>
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public class NBCExtractor extends GENXExtractor
{
  NBCDataProvider nbcDP;

  //private static final int ESCROW_ADMIN_FEE = 7;

  //private static final int ESCROW_CREDIT_LIFE = 3;
  //Added constant for Bug # 1615
  private static final int INT_ZERO_CHECK = 0;

  private static final int VAR_INT_RATE = 2;

  private static final int INT_RATE_TO_PRIME = 1;

  private static final int PRIME_RATE_AMOUNT = 0;

  // FeeID = 0 & 22
  private final int[] pAPPRAISAL_FEES =
  { Mc.FEE_TYPE_APPRAISAL_FEE, Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL };

  // FeeID = 3 or 15
  private final int[] pOTHER3_FEES =
  { Mc.FEE_TYPE_CMHC_FEE, Mc.FEE_TYPE_GE_CAPITAL_FEE };

  double totalCheckedFees = 0;

  /*
     * Called by DocumentGeneratorHandler for all document types
     * @see com.basis100.deal.docprep.extract.data.DealExtractor#buildDataDoc()
     */
  protected void buildDataDoc() throws Exception
  {
	buildXMLDataDoc();
  }

  /**
   * Venkata, FXP16891 - NBC CR007 Commitment Letter Changes - 01 Aug,2007
   * Made this into a separate method so that super classes have the chance to
   * specify which commitment letter data they want.
   */
  protected void addCLTags(Node root)
  {
      root.appendChild(new NBCCommitmentExtractor((XSLExtractor)this).getCommitmentTags(doc));
  }
  
  /*
     * Builds entire xml structure for NBC Non-Disclosure Documents
     * @see com.basis100.deal.docprep.extract.data.GENXExtractor#buildXMLDataDoc()
     */
  protected void buildXMLDataDoc() throws Exception
  {
	  
	doc = XmlToolKit.getInstance().newDocument();

	Element docRoot = doc.createElement("Document");
	
	//Venkata - 2 August, 2007
	//make a full blown deal tree available so that this xml can be used for all templates
	createDealTree(docRoot);
	
	docRoot.appendChild(buildBaseTags(doc));
	 try
     {
         addCLTags(docRoot);
     }
     catch (Exception e)
     {
         e.printStackTrace();
         System.out.println(e);
     }
	
	doc.appendChild(docRoot);
  }

  /**
     * buildBaseTags Adds xml tags containing the data (based on the entity
     * columns & the conditions mentioned)to the existing root tag
     * @param root
     * @return
     */
  // Venkata, FXP16891 - NBC CR007 Commitment Letter Changes - 01 Aug,2007
  // Modified the method signature
  // and commented the tags that are not being used in the Solicitors XSL.
  //private Element buildBaseTags(Element root)
  private Element buildBaseTags(Document pDoc)
  {
	Property pp = getPrimaryProperty();

	nbcDP = new NBCDataProvider(srk, deal, lang);
	nbcDP.setDocumentFormat(format);
	
	Element root = pDoc.createElement("SolicitorsPackage");
	
    /* Added code fix for SCR No. # 1134 --
     * Modified code to get the absolute path for images
     */
	String imgLocation = ExpressDocprepHelper.getTemplateImageLocation();
	
	try
	{
	  // For logo image paths
	  addTo(root, getTag("Logo_Image", imgLocation));
      /* End of 1134 fix */

	  // For documents triggering
	  /*
	  addTo(root, getTag("YN_12641", "true"));
	  addTo(root, getTag("YN_14597", "true"));
	  addTo(root, getTag("YN_15021", "true"));
	  addTo(root, getTag("YN_15721", "true"));
	  addTo(root, getTag("YN_17918", "true"));
	  */

 
	  //Venkata - 2 Aug-07, Moved the following logic to the template itself
	  /*addTo(root, getTag("YN_12641", "true"));
	  //addTo(root, getTag("YN_12641", Boolean.toString(nbcDP.exStDisclosure())));
	  addTo(root, getTag("YN_14597", Boolean.toString(nbcDP.exAgTermLoanImmHyp())));
	  addTo(root, getTag("YN_15021", Boolean.toString(nbcDP.exModSubrogn())));
	  addTo(root, getTag("YN_15721", Boolean.toString(nbcDP.exLOCredit())));
	  addTo(root, getTag("YN_17918", Boolean.toString(nbcDP.exAgMtgLoan())));*/
	  

	  // For data mapping

	  addTo(root, getTag("amortPeriod", nbcDP.exAmortizationTerm()));
	  ////addTo(root, getTag("commitValUntil", nbcDP.exCommitmentExpirationDate()));
	  addTo(root, getTag("newLoanRef", nbcDP.exNewLoanRefinancing()));
	  addTo(root, getTag("transfer", nbcDP.exTrasfer()));
	  addTo(root, getTag("discount", nbcDP.exFixedDiscount()));
	  ////addTo(root, getTag("downPayment", nbcDP.exDownPayment()));
	  addTo(root, getTag("expDateDisburse", nbcDP.exDateDisburse()));
	  addTo(root, getTag("dateFirstPayment", nbcDP.exFirstPaymentDate()));
	  addTo(root, getTag("dateFirstPmtFrmttd", nbcDP.exFirstPmtDateFrmttd()));
	  addTo(root, getTag("paySchedule", nbcDP.exPaymentSchedule()));
	  addTo(root, getTag("intAdjDate", nbcDP.exInterimInterestAdjustmentDate()));
	  addTo(root, getTag("intAdjDateFrmttd", nbcDP.exInterimIntAdjDateFrmttd()));
	  addTo(root, getTag("MIFeesCP", nbcDP.exMIFeesCP()));
	  addTo(root, getTag("MIFeesCP1", nbcDP.exMIFeesCP1()));
	  addTo(root, getTag("matDate", nbcDP.exMaturityDate()));
	  addTo(root, getTag("matDateFrmttd", nbcDP.exMaturityDateFrmttd()));
	  addTo(root, getTag("amtDedPrinAmtBor", nbcDP.exMIPremiumAmount()));
	  addTo(root, getTag("MIFeesTax", nbcDP.exMIFeesTax()));
	  addTo(root, getTag("loanInsType1", nbcDP
		  .exLoanInsType(Mc.MI_INSURER_BLANK)));
	  addTo(root, getTag("loanInsType2", nbcDP
		  .exLoanInsType(Mc.MI_INSURER_CMHC)));
	  addTo(root, getTag("loanInsType3", nbcDP.exLoanInsType(Mc.MI_INSURER_GE)));
	  addTo(root, getTag("perFlexLine1", nbcDP.exPerFlexLine1()));
	  addTo(root, getTag("perFlexLine2", nbcDP.exPerFlexLine2()));
	  addTo(root, getTag("allInOne", nbcDP.exAllInOne()));

	  addTo(root, getTag("fxRate", (nbcDP
		  .exNetInterestRate(Mc.INTEREST_TYPE_FIXED)!=null)?(String.valueOf(nbcDP
		  .exNetInterestRate(Mc.INTEREST_TYPE_FIXED))):("")));
              // SCR#1494 fix - start
              addTo(root, getTag("varRate", (nbcDP
                      .exNetInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE)!=null)?(String.valueOf(nbcDP
                      .exNetInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE))):("")));
              // SCR#1494 fix - end
	  addTo(root, getTag("availAmt", nbcDP.exNetLoanAmount()));
	  addTo(root, getTag("diffMorRateNSpread", nbcDP.exDiffMortRateSpread()));
	  addTo(root, getTag("refPaymentIC", nbcDP.exRefPayment()));
	  addTo(root, getTag("monPayment", nbcDP.exMonthlyPayment()));
      addTo(root, getTag("fixedPayAmt", nbcDP.exFixedPayAmount()));
	  addTo(root, getTag("empProgPay", nbcDP.exEmployeeProgPayment()));
	  addTo(root, getTag("payAmtPI", nbcDP.exPandIAmt()));
	  addTo(root, getTag("paymentFreq1", nbcDP.exPaymentFreq(
		  Mc.PAY_FREQ_MONTHLY, Mc.PAY_FREQ_MONTHLY)));
	  addTo(root, getTag("paymentFreq2", nbcDP.exPaymentFreq(
		  Mc.PAY_FREQ_BIWEEKLY, Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)));
	  addTo(root, getTag("paymentFreq3", nbcDP.exPaymentFreq(
		  Mc.PAY_FREQ_WEEKLY, Mc.PAY_FREQ_ACCELERATED_WEEKLY)));
	  addTo(root, getTag("cricRate", nbcDP.exCriticalRate()));
	  addTo(root, getTag("prepayOT", nbcDP.exPrepayment("open")));
	  addTo(root, getTag("prepayCT", nbcDP.exPrepayment("closed")));

      int paymentTermTypeId = findPaymentTermTypeId(srk, deal.getPaymentTermId());
	  addTo(root, getTag("openRateLoan", nbcDP.exRateLoan("open", paymentTermTypeId)));
	  addTo(root, getTag("closedRateLoan", nbcDP.exRateLoan("closed", paymentTermTypeId)));
	  addTo(root, getTag("intRateFixed", nbcDP.exInterestRateFixed()));

	  addTo(root, getTag("calcField", nbcDP.exCalcField()));
	  addTo(root, getTag("varRateSpread", nbcDP.exVarRateSpread()));

	  addTo(root, getTag("intRateSpread", nbcDP.exIntRateSpread()));
	  addTo(root, getTag("intRateSpreadVal", nbcDP.exPrimeBaseAdj()));
	  ////addTo(root, getTag("varRateToPrime", nbcDP.exVarRateToPrime()));
	  addTo(root, getTag("showVIR", nbcDP.exShowVIR()));
	  addTo(root, getTag("varIntRate", nbcDP.exProdInterestRate(VAR_INT_RATE)));
	  ////addTo(root, getTag("intRatePrime", nbcDP
		////  .exProdInterestRate(INT_RATE_TO_PRIME)));
	  addTo(root, getTag("primeRateAmt", nbcDP
		  .exProdInterestRate(PRIME_RATE_AMOUNT)));
	  addTo(root, getTag("constAdvProgAdv", nbcDP.exConstAdv()));
	  addTo(root, getTag("maxDisclosableAmt", nbcDP.exMaxDisclosableAmount()));
	  ////addTo(root, getTag("infoSol", nbcDP.exSolInstrn()));
	  addTo(root, getTag("expertNo", nbcDP.exSourceApplicationId()));
	  if(nbcDP.exProdInterestRate(VAR_INT_RATE) != ""){
		//added check for variable interest rate not populating  - code fix Bug # 1698 - start
		Double netInterestRate = nbcDP.exNetInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE);
		if(netInterestRate!=null){
		  addTo(root, getTag("fixedPromRate", String.valueOf(nbcDP
		  .exNetInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE))));
		//added check for variable interest rate not populating  - code fix Bug # 1698 - end  
		}
	  }
	  addTo(root, getTag("teaserRate", nbcDP.exTeaserRate()));
	  addTo(root, getTag("teaserPayAmt", nbcDP.exTeaserPIAmount()));
	  addTo(root, getTag("promPeriod", nbcDP.exPromPeriod()));
	  addTo(root, getTag("teaserTerm", nbcDP.exTeaserTerm()));
	  addTo(root, getTag("princAmtBor", nbcDP.exTotalLoanAmount()));
	  ////addTo(root, getTag("purPrice", nbcDP.exTotalPurchasePrice()));
	  addTo(root, getTag("regAmtCharge", nbcDP.exRegCharge()));
	  addTo(root, getTag("totCostBorrowing", nbcDP.exTotalCostBorrowing()));
	  if(nbcDP.exTotAllPmts() != null && nbcDP.exTotAllPmts().trim().length() > 0)
	  addTo(root, getTag("totAllPaymentPI", formatCurrency(new Double(nbcDP.exTotAllPmts()).doubleValue())));
	  addTo(root, getTag("borName1", nbcDP.exPrimaryBorrowerName()));
	  addTo(root, getTag("borName2", nbcDP.exCoBorrowersName()));
	  addTo(root, getTag("borName3", nbcDP.exPrimaryBorrowerName()));

	  // addTo(root, extractGuarantorNames("guarantorName"));
	  ////String[] coBorrNames = nbcDP.exCoBorName();
	  ////if(coBorrNames.length>0)
		////addTo(root, getTag("guarantorName", coBorrNames[0]));  //nbcDP.exGuarantorNames()));
	  ////else
		////addTo(root, getTag("guarantorName", " "));
	  ////addTo(root, getTag("borName4", exAllBorrowers(nbcDP)));
	  ////addTo(root, getTag("borName5", exAllBorrowers(nbcDP)));
	  addTo(root, getTag("borName6", nbcDP.exPrimaryBorrowerName()));

	  String[] coBorrowersAddress = nbcDP.exCoBorNameAddress();
	  // Repeat for multiple Co-Borrowers
	  ////for ( int coBorrowerCount = 0; coBorrowerCount < coBorrowersAddress.length; coBorrowerCount++ )
	  ////{
		////addTo(root, getTag("coBorAdd" + (coBorrowerCount + 1),
		////	coBorrowersAddress[coBorrowerCount]));
	  ////}

	  String[] coBorrowersName = nbcDP.exCoBorName();
	  // Repeat for multiple Co-Borrowers
	  for ( int coBorrowerCount = 0; coBorrowerCount < coBorrowersName.length; coBorrowerCount++ )
	  {
		addTo(root, getTag("coBorName" + (coBorrowerCount + 1),
			coBorrowersName[coBorrowerCount]));
	  }

	  // addTo(root, extractQuebecGuarantorNames("guarantorName1", pp));
	  addTo(root, getTag("guarantorName1", nbcDP.exQuebecGuarantorNames(pp)));
	  addTo(root, getTag("guarantorName2", nbcDP.exGuarantorNames()));
	  // addTo(root, extractGuarantorNames("guarantorName2"));
	  // addTo(root, exGuarantorName3("guarantorName3"));
	  addTo(root, getTag("guarantorName3", nbcDP.exGuarantorNameWithAddress()));
	  addTo(root, getTag("adminFees", String.valueOf(formatCurrency(nbcDP
		  .exAdministrationFees(Mc.ESCROWTYPE_ADMIN_FEE)))));
	  addTo(root, getTag("fixedIntRate", nbcDP
		  .exInterestRate(Mc.INTEREST_TYPE_FIXED)));
	 /*addTo(root, getTag("varIntRate", nbcDP
		  .exInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE)));*/
	  addTo(root, getTag("varIntRate1", nbcDP
		  .exInterestRate(Mc.INTEREST_TYPE_ADJUSTABLE)));
	  addTo(root, getTag("intRateRevFreq", nbcDP.exIntRateRevFreq()));

	  addTo(root, getTag("varRateAdjFreq", nbcDP.exVarRateAdjFreq()));
	  addTo(root, getTag("term1", nbcDP.exTerm1()));
	  addTo(root, getTag("fixedRateLoanTerm", nbcDP.exFixedRateLoanTerm()));
	  addTo(root, getTag("intRevFreq", nbcDP.exIntRevFreq()));
	  addTo(root, getTag("intRateRevFreq1", nbcDP.exIntRateRevFreq1()));
	  addTo(root, getTag("intRateRevFreq2", nbcDP.exIntRateRevFreq2()));

	  addTo(root, getTag("term2", nbcDP.exTerm2()));
	  addTo(root, getTag("propAddress", extractPropertyAddress("propAddress")));
	  addTo(root, getTag("POSConctName", nbcDP.exPOSContactName()));
	  ////addTo(root, getTag("mtgCheck", nbcDP.exMtgCheck()));
	  ////addTo(root, getTag("locCheck", nbcDP.exLocCheck()));
	  ////addTo(root, getTag("cltTelNoHome", nbcDP.exBorHomPhoneNo()));
	  ////addTo(root, getTag("cltTelNoWork", nbcDP.exBorWorkPhoneNo()));
	  ////addTo(root, getTag("propStreet", exPropStreet()));
	  ////addTo(root, getTag("propApt", nbcDP.exPropApt()));
	  ////addTo(root, getTag("propCity", nbcDP.exPropCity()));
	  ////addTo(root, getTag("propLotNo", nbcDP.exPropLotNo()));
	  ////addTo(root, getTag("propPostCode", nbcDP.exPropPostalCode()));
	  ////addTo(root, getTag("propProvince", nbcDP.exPropProvince()));
	  ////addTo(root, getTag("resSingleUnit", nbcDP.exResSingleUnit()));
	  ////addTo(root, getTag("resDuplex", nbcDP.exResDuplex()));
	  ////addTo(root, getTag("resTriplex", nbcDP.exResTriplex()));
	  ////addTo(root, getTag("resQplex", nbcDP.exResQplex()));
	  ////addTo(root, getTag("commUnit", nbcDP.exCommUnit()));
	  ////addTo(root, getTag("indUnit", nbcDP.exIndUnit()));
	  ////addTo(root, getTag("vacLot", nbcDP.exVacLot()));
	  ////addTo(root, getTag("farmUnit", nbcDP.exFarmUnit()));
	  ////addTo(root, getTag("firstMtg", nbcDP.exFirstMtg()));
	  ////addTo(root, getTag("secondMtg", nbcDP.exSecondMtg()));
	  addTo(root, getTag("cashbackAmt1", nbcDP.exCashBkAmt()));

	  addTo(root, getTag("date1", formatDate(new Date())));
	  //changed to not populate the value for Variable field when Fixed type interest rate
	  if((nbcDP.exProdInterestRate(VAR_INT_RATE) != null)&&(nbcDP.exProdInterestRate(VAR_INT_RATE) != "") && deal.getProductTypeId() == Mc.PRODUCT_TYPE_MORTGAGE){
		addTo(root, getTag("dateFrmtted", nbcDP.exFrmttdCurrDate()));
	  }
	  addTo(root, getTag("CTPHAdd", nbcDP.exCTPHAddress()));
	  addTo(root, getTag("otherSpec", nbcDP.exOtherSpec()));
	  addTo(root, getTag("totPayAmt", nbcDP.exTotPayAmt()));
	  addTo(root, getTag("branchAddress", nbcDP.exCTPHAddress()));
	  addTo(root, getTag("alwChkd", nbcDP.exAlwChkd()));
	  addFeesCommon("appFees", pAPPRAISAL_FEES, root);
	  addFeesCommon("mortInsFees", pOTHER3_FEES, root);
	  addTo(root, getTag("solFees1", nbcDP.exSolFees()));


	  //addTo(root, getTag("propTaxInstall", nbcDP.exPropTaxInstall()));
	  addTo(root, getTag("propTaxInstall", formatCurrency(nbcDP.exEscrowPaymentAmount(Mc.ESCROW_PAYMENT_TERM))));

	  addTo(root, getTag("other1", formatCurrency(nbcDP
		  .exEscrowPaymentAmount(Mc.ESCROWTYPE_ADMIN_FEE))));
	  addTo(root, getTag("totInsPrem", formatCurrency(nbcDP
		  .exEscrowPaymentAmount(Mc.ESCROWTYPE_CREDIT_LIFE))));

	  addTo(root, getTag("prop4UOrLess", nbcDP.exPropertyUnits(0, pp)));
	  addTo(root, getTag("prop4UOrMore", nbcDP.exPropertyUnits(1, pp)));
	  addTo(root, getTag("interest1", nbcDP.exIntOverTerm()));
	  addTo(root, getTag("annCostBorRate", nbcDP.exAnnualCostBorrowing()));
	  if(nbcDP.exBalRemEOT() != null && nbcDP.exBalRemEOT().trim().length() > 0)
	  addTo(root, getTag("balOwingMat", formatCurrency(new Double(nbcDP.exBalRemEOT()).doubleValue())));
	  addTo(root, getTag("intRateRenewAuto1", nbcDP.exIntRateAutoRenew()));
	  addTo(root, getTag("intRateRenNotAuto1", nbcDP.exIntRateNotAutoRenew()));
	  addTo(root, getTag("intRateRebVal", nbcDP.exTeaserCalc()));
	  addTo(root, getTag("cappedRate", nbcDP.exCappedRate()));

	  addTo(root, getTag("budgetOpt", nbcDP.exBudgetOpt()));
	  // Changed method call for codefix BUG# 1615
	  if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC 
			&& deal.getInterestTypeId()!= INT_ZERO_CHECK){
	  ////addTo(root, getTag("spreadGT0", nbcDP.exSpreadGT0()));
	  }
	  addTo(root, getTag("primeRate", nbcDP.exSpreadGT0()));
	  ////addTo(root, getTag("propNo", exPropNo()));

	  // Commented as Appraisal order Related tables are currently not
	  // present in Schema's
	  // Uncommented, 10/28/2006
         ////addTo(root, getTag("ownerName", nbcDP.exOwnerName()));
         ////addTo(root, getTag("ctPerOwnerName", nbcDP.exCntactName()));
         ////addTo(root, getTag("ctPerOwnTelHome", nbcDP.exCntHomePhNo()));
         ////addTo(root, getTag("ctPerOwnTelWork", nbcDP.exCntWorkPhNo()));
         ////addTo(root, getTag("ctPerOwnTelCell", nbcDP.exCntCellPhNo()));
         ////addTo(root, getTag("specInstrn", nbcDP.exSpInstrn()));


	  addTo(root, getTag("alwUnchkd", nbcDP.exAlwUnChkd()));

	  ////String[] Text= nbcDP.exDocTrackText();
	//// for(int loopCntr=0;loopCntr<Text.length;loopCntr++)
	  ////addTo(root,getTag("docTrackText"+loopCntr, Text[loopCntr]));
	  ////addTo(root, getTag("loanAmount", nbcDP.exLoanAmount()));
	  // Added Reference Source application number for Appraisal Review
	  // Document - NBC Implementation PP - Start
	  ////addTo(root, getTag("RefSourceApplicationNo", exrefSSourceApplicationNo()));
	  // Added Reference Source application number for Appraisal Review
	  // Document - NBC Implementation PP - End

	  addTo(root, getTag("intRateActDate", nbcDP.exInterestRateActDate()));

	  /*
         * Already Included all of these. Have not deleted to confirm
         * addTo(root, getTag("openRateLoan1", nbcDP.exOpenRateLoan()));
         * addTo(root, getTag("closedRateloan1", nbcDP.exClosedRateLoan()));
         * addTo(root, getTag("maxDisAmt", "15000")); addTo(root,
         * getTag("propAddress", extractAddressLine(pp))); addTo(root,
         * getTag("cappedRate1", "0")); addTo(root, getTag("totPayAmt",
         * "350000")); addTo(root, getTag("cashbackAmt1", "4500")); addTo(root,
         * getTag("intRateActDate", nbcDP.exInterestRateActDate())); addTo(root,
         * getTag("intRateSpread", nbcDP.exIntRateSpread())); addTo(root,
         * getTag("cappedRate2", "6.45")); addTo(root, getTag("check2", "1"));
         * addTo(root, getTag("check3", "0")); addTo(root,
         * getTag("cashbackAmt2", "15000")); addTo(root,
         * getTag("intRateRebVal1", "0")); addTo(root, getTag("cappedRate3",
         * "0")); addTo(root, getTag("check4", "1")); addTo(root,
         * getTag("feeReimCashback", "0")); addTo(root, getTag("intRateRebVal2",
         * "0")); addTo(root, getTag("intRateSpreadYN", "Y")); addTo(root,
         * getTag("budgetOpt1", "1"));
         */
	  // addTo(root, exGuarantorName3("guarantorName3"));
	  /* Added tags for Looping on Guarenter and Borrowers-start--ravi */
	  ////addTo(root, getTag("17975borrowerloop", nbcDP.exInterestRateActDate()));
	  addTo(root, getTag("15721borrowerloop", nbcDP.exInterestRateActDate()));
	  addTo(root, getTag("17918borrowerloop", nbcDP.exInterestRateActDate()));
	  addTo(root, getTag("15021borrowerloop", nbcDP.exInterestRateActDate()));
	  addTo(root, getTag("14597borrowerloop", nbcDP.exInterestRateActDate()));
	  ////addTo(root, exBorrowerNamesLoop(5, "borrowerloop17975"));
	  addTo(root, exBorrowerNamesLoop(5, "borrowerloop15721"));
	  addTo(root, exBorrowerNamesLoop(2, "borrowerloop17918"));
	  addTo(root, exBorrowerNamesLoop(2, "borrowerloop15021"));
	  addTo(root, exBorrowerNamesLoop(2, "borrowerloop14597"));

	  addTo(root, exGuarantorNamesLoop(1, "guarantorloop17918"));
	  addTo(root, exGuarantorNamesLoop(1, "guarantorloop14597"));
	  addTo(root, exGuarantorNamesLoop(1, "guarantorloop15021"));

	  addTo(root, getTag("", nbcDP.exProvinceIsQuebec()));
	  addTo(root, getTag("guarantorIsAvailable", nbcDP.exGuarantorIsAvailable()));
	  
	  //added tag for bug 1615 - date generted only if producttypeID=SLOC
	  ////addTo(root,getTag("dateT26a",exDateforT26a()));
	}
	catch (Exception e)
	{
	  e.printStackTrace();
	  String msg = "NBCExtractor failed while building tags"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}
	return root;
  }

  /**
     * exrefSSourceApplicationNo Returns Source Application Id
     * @return String Source Application Id
     */

  protected String exrefSSourceApplicationNo(){
	return (deal.getSourceApplicationId());

  }

  /**
     * exPrimaryBorrowerName1 Returns primary borrower's name in the desired
     * format
     * @return String <Borrower First Name> <Middle Initial> <Last Name>
     */
  protected String exPrimaryBorrowerName6()
  {
	String val = "";
	try
	{
	  Property p = getPrimaryProperty();
	  Borrower b = new Borrower(srk, null);
	  b = b.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());

	  if ((b.isPrimaryBorrower()) && (b.getBorrowerTypeId() == Mc.BT_BORROWER))
	  {
		val = nbcDP.formBorrowerName(b.getBorrowerFirstName(), b
			.getBorrowerMiddleInitial(), b.getBorrowerLastName());
	  }

	  val += extractAddressLine(p, lang);
	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Borrower name6"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}
	return val;
  }

  /**
     * exGuarantorName3 Returns a node containing the GuarantorName & address in
     * one line for all the guarantors in the deal
     * @param tagName
     * @return
     */
  protected Node exGuarantorName3(String tagName)
  {
	Node elemNode = null;
	String firstname = "";
	String midInitial = "";
	String lastname = "";
	String finalname = "";
	Collection borrowersList;

	try
	{
	  Property p = getPrimaryProperty();
	  List borrowers = getBorrowers();

	  if (borrowers.isEmpty())
		return null;

	  Iterator i = borrowers.iterator();

	  while (i.hasNext())
	  {
		Borrower b = (Borrower) i.next();

		if (b.getBorrowerTypeId() == Mc.BT_GUARANTOR)
		{
		  if (elemNode == null)
			elemNode = doc.createElement(tagName);

		  if (!isEmpty(midInitial))
		  {
			finalname = firstname + format.space() + midInitial + "."
				+ format.space() + lastname;
		  }
		  else
		  {
			finalname = firstname + format.space() + lastname;
		  }

		  borrowersList = deal.getBorrowers();

		  Iterator borrowerIt = borrowersList.iterator();
		  while (borrowerIt.hasNext())
		  {
			Borrower borrower = (Borrower) borrowerIt.next();

			Collection borrowerAddresses = borrower.getBorrowerAddresses();
			if (borrowerAddresses != null)
			{
			  Iterator borrowerAddressesIt = borrowerAddresses.iterator();
			  while (borrowerAddressesIt.hasNext())
			  {
				BorrowerAddress badd = (BorrowerAddress) borrowerAddressesIt
					.next();
				// Checks If Borrower Address Type is 'Current'
				if (badd.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT)
				{
				  Addr add = badd.getAddr();
				  finalname += nbcDP.formatBorAddress(add);
				  break;
				}
			  }
			}

		  }

		  // finalname += extractAddressLine(p);
		  addTo(elemNode, "Name", finalname);
		}
	  }
	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Borrower name3"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}
	return elemNode;
  }

  /**
     * exPropNo Returns the Property Street Number
     * @return String PropertyStreetNumber
     */
  protected String exPropNo()
  {
	Property p = getPrimaryProperty();
	return p.getPropertyStreetNumber();
  }

  /**
     * exPropStreet Returns the Property Street details concatenated together &
     * separated by commmas
     * @return
     * Change: Removed commas for proper formatting, defect 1472
     */
  protected String exPropStreet()
  {
	String streetAddress = "";
	try
	{
	  Property p = getPrimaryProperty();

	  if (!isEmpty(p.getPropertyStreetName()))
	  {
		streetAddress += format.space() + p.getPropertyStreetName();
	  }
	  if (!isEmpty(p.getStreetTypeDescription()))
	  {
		  streetAddress += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "STREETTYPE", p.getStreetTypeId(), lang);
	  }
	  streetAddress += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "STREETDIRECTION", p.getStreetDirectionId(), lang);
	}

	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Property Street"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return streetAddress;
  }

  /**
     * extractAddressLine Fetches the variuos address fields, concatenates them
     * together separated by commas & returns the final complete address
     * @param p
     *            (Property)
     * @return
     */
  protected String extractAddressLine(Property p, int lang)
  {
	String val = null;

	try
	{
	  val = "";

	  if (!isEmpty(p.getPropertyStreetNumber()))
	  {
		val += p.getPropertyStreetNumber();
	  }

	  if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) {
		 // Street_number Street_name Street_type, Unit#, Address_line2
    	  
    		val +=  exPropStreet();

    	  val += ", ";

    	  if (!isEmpty(p.getUnitNumber()))
    	  {
    		val += p.getUnitNumber();
    		val += ", ";
    	  }

	  } else  {
		  // proper French format:
	  	  // Street_number Street_type Street_name Unit#, Address_line2
	  		
	  	  val += exPropStreet();
	  	  
	  	  if (!isEmpty(p.getUnitNumber()))
		  {
			val += format.space() + p.getUnitNumber();
		  }
	  	  val += ", ";
	  }

	  if (!isEmpty(p.getPropertyAddressLine2()))
	  {
		val += p.getPropertyAddressLine2();
		val += ", ";
	  }

	  if (!isEmpty(p.getPropertyCity()))
	  {
		val += p.getPropertyCity();
		val += ", ";
	  }

	  Province prov = new Province(srk, p.getProvinceId());

	  if (!isEmpty(prov.getProvinceName()))
	  {
		val += prov.getProvinceName();
		val += " ";
	  }

	  if (!isEmpty(p.getPropertyPostalFSA()))
	  {
		val += p.getPropertyPostalFSA();
		val += " ";
	  }
	  if (!isEmpty(p.getPropertyPostalLDU()))
	  {
		val += p.getPropertyPostalLDU();
	  }
	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Address Line"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return val;
  }



  /**
     * extractPropertyAddress Instantiates the Property Class & calls the
     * extractAddressLine method to get the Final Address
     * @param tagName
     * @return
     */
  protected String extractPropertyAddress(String tagName)
  {
	String finalAddress = null;
	try
	{
	  Property p = getPrimaryProperty();
	  finalAddress = extractAddressLine(p, lang);

	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Property Address"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return finalAddress;
  }



  /**
     * Common method for fee processing. Creates feeDescription by concatenating
     * names of all the fees IDs listed in pFeesGroup array and totals their
     * amounts.
     * @param tagRoot
     *            String
     * @param pFeesGroup
     *            int[]
     */
  private void addFeesCommon(String tagRoot, int[] pFeesGroup, Element root)
  {
	{
	  addTo(root, getTag(tagRoot, formatCurrency(nbcDP
		  .exSelectedFeesTotalAmt(pFeesGroup))));
	}
  }

  /**
     * Retrieves and formats the Guarantor Names from the deal and formats
     * repeating XML tags with it.
     * @return The completed Borrower Names XML tag.
     */
  protected Node extractGuarantorNames(String tagName)
  {
	Node elemNode = null;
	String firstname = "";
	String midInitial = "";
	String lastname = "";
	String finalname = "";

	try
	{
	  List borrowers = getBorrowers();

	  if (borrowers.isEmpty())
		return null;

	  Iterator i = borrowers.iterator();

	  while (i.hasNext())
	  {
		Borrower b = (Borrower) i.next();

		if (b.getBorrowerTypeId() == Mc.BT_GUARANTOR)
		{
		  if (elemNode == null)
			elemNode = doc.createElement(tagName);

		  if (!isEmpty(midInitial))
		  {
			finalname = firstname + format.space() + midInitial + "."
				+ format.space() + lastname;
		  }
		  else
		  {
			finalname = firstname + format.space() + lastname;
		  }
		  addTo(elemNode, "Name", finalname);
		}
	  }
	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Guarantor Names"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}
	return elemNode;
  }

  /**
     * Retrieves and formats the Guarantor Names from the deal and formats
     * repeating XML tags with it.
     * @return The completed Borrower Names XML tag.
     */
  protected Node extractQuebecGuarantorNames(String tagName, Property pp)
  {
	Node elemNode = null;
	String firstname = "";
	String midInitial = "";
	String lastname = "";
	String finalname = "";

	try
	{
	  List borrowers = getBorrowers();

	  if (borrowers.isEmpty())
		return null;

	  Iterator i = borrowers.iterator();

	  while (i.hasNext())
	  {
		Borrower b = (Borrower) i.next();

		if (b.getBorrowerTypeId() == Mc.BT_GUARANTOR
			&& pp.getPrimaryPropertyFlag() == 'Y'
			&& pp.getProvinceId() == Mc.PROVINCE_QUEBEC)
		{
		  if (elemNode == null)
			elemNode = doc.createElement(tagName);

		  if (!isEmpty(midInitial))
		  {
			finalname = firstname + format.space() + midInitial + "."
				+ format.space() + lastname;
		  }
		  else
		  {
			finalname = firstname + format.space() + lastname;
		  }
		  addTo(elemNode, "Name", finalname);
		}
	  }
	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Quebec Guarantor Names"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return elemNode;
  }

  /**
     * Retrieves and formats the Borrower Names from the deal and formats
     * repeating XML tags with it.
     * @return The completed Borrower Names XML tag.
     */
  protected Node extractBorrowerNames(String tagName)
  {
	Node elemNode = null;
	Borrower primaryBorrower = null;
	String firstname = "";
	String midInitial = "";
	String lastname = "";
	String finalname = "";

	try
	{
	  List borrowers = getBorrowers();

	  if (borrowers.isEmpty())
		return null;

	  if (borrowers.contains(getPrimaryBorrower()))
	  {
		primaryBorrower = (Borrower) borrowers.remove(borrowers
			.indexOf(getPrimaryBorrower()));
	  }

	  firstname = primaryBorrower.getBorrowerFirstName();
	  midInitial = primaryBorrower.getBorrowerMiddleInitial();
	  lastname = primaryBorrower.getBorrowerLastName();

	  if (!isEmpty(firstname) && !isEmpty(lastname))
	  {
		if (elemNode == null)
		  elemNode = doc.createElement(tagName);
		if (!isEmpty(midInitial))
		{
		  finalname = firstname + format.space() + midInitial + "."
			  + format.space() + lastname;
		}
		else
		{
		  finalname = firstname + format.space() + lastname;
		}
		addTo(elemNode, "Name", finalname);
	  }

	  Collections.sort(borrowers, new BorrowerTypeComparator());

	  Iterator it = borrowers.iterator();

	  Borrower bor = null;

	  while (it.hasNext())
	  {
		bor = (Borrower) it.next();

		if (bor.getBorrowerTypeId() == Mc.BT_BORROWER)
		{
		  firstname = bor.getBorrowerFirstName();
		  midInitial = bor.getBorrowerMiddleInitial();
		  lastname = bor.getBorrowerLastName();

		  if (!isEmpty(firstname) && !isEmpty(lastname))
		  {
			if (elemNode == null)
			  elemNode = doc.createElement(tagName);
			if (!isEmpty(midInitial))
			{
			  finalname = firstname + format.space() + midInitial + "."
				  + format.space() + lastname;
			}
			else
			{
			  finalname = firstname + format.space() + lastname;
			}
			addTo(elemNode, "Name", finalname);
		  }
		}
	  }
	}
	catch (Exception e)
	{
	  String msg = "NBCExtractor failed while extracting Borrower Names"
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}

	return elemNode;
  }

  /**
     * exCoBorrowerNames Extracts the name of the Primary Borrower & the
     * Co-Borrowers associated with it.
     * @return
     */
  protected String exCoBorrowerNames()
  {
	StringBuffer fullName = null;
	Collection borrowersList = null;
	try
	{
	  borrowersList = deal.getBorrowers();

	  Iterator it = borrowersList.iterator();

	  while (it.hasNext())
	  {
		Borrower borrower = (Borrower) it.next();

		if (borrower.getBorrowerTypeId() == 0
			&& borrower.getPrimaryBorrowerFlag() == "Y")
		{
		  if (fullName != null)
		  {
			fullName.append(", ");
		  }
		  else
		  {
			fullName = new StringBuffer();
		  }

		  if (borrower.getBorrowerFirstName() != null)
		  {
			fullName.append(borrower.getBorrowerFirstName());
			fullName.append(" ");
		  }

		  if (borrower.getBorrowerMiddleInitial() != null)
		  {
			fullName.append(borrower.getBorrowerMiddleInitial());
			fullName.append(". ");
		  }

		  if (borrower.getBorrowerLastName() != null)
		  {
			fullName.append(borrower.getBorrowerLastName());
		  }
		}
		else
		  if (borrower.getBorrowerTypeId() == 0
			  && borrower.getPrimaryBorrowerFlag() != "Y")
		  {
			if (fullName != null)
			{
			  fullName.append(", ");
			}
			else
			{
			  fullName = new StringBuffer();
			}

			if (borrower.getBorrowerFirstName() != null)
			{
			  fullName.append(borrower.getBorrowerFirstName());
			  fullName.append(" ");
			}

			if (borrower.getBorrowerMiddleInitial() != null)
			{
			  fullName.append(borrower.getBorrowerMiddleInitial());
			  fullName.append(". ");
			}

			if (borrower.getBorrowerLastName() != null)
			{
			  fullName.append(borrower.getBorrowerLastName());
			}
		  }
	  }
	}
	catch (Exception e)
	{
	  String msg = "DisclosureDataProvider failed while totaling fee amount."
		  + (e.getMessage() != null ? "\n" + e.getMessage() : "");
	  srk.getSysLogger().warning(this.getClass(), msg);
	}
	return fullName == null ? "" : fullName.toString();
  }

  public String exAllBorrowers(NBCDataProvider ndp)
  {
	String val = "";
	val = ndp.exPrimaryBorrowerName();
	val += ndp.exCoBorrowersName() == null ? "" : ", "
		+ ndp.exCoBorrowersName();
	return val;

  }

  public Node exDocTrackText(String[] text)
  {
	Node elemNode = null;

	if (elemNode == null)
	{
	  elemNode = doc.createElement("docTrackText");
	}
	for ( int textCounter = 0; textCounter < text.length; textCounter++ )
	{
	  if (textCounter < text.length)
		addTo(elemNode, "data", text[textCounter]);
	   elemNode.appendChild(elemNode);
	}
		return elemNode;

  }

  protected Node exGuarantorNamesLoop(int min_no_of_Guarenters, String tagName)
  {
	Node elemNode = null;
	Node lineNode;
	int maxLineCount = 0;
	 int size=0;

	if (elemNode == null)
	{
	  elemNode = doc.createElement(tagName);
	}
	String[] guaranters = nbcDP.GuaranterNamesforLoop();
	if(tagName.equals("guarantorloop14597")){
	  if(guaranters.length<2){
		size=1;
	  }
	  else{
		size=guaranters.length-1;
	  }
	  for(int cntr=1;cntr<=size;cntr++){
		  lineNode = doc.createElement("Guaranter");
		  if(cntr<guaranters.length)
		  {
			addTo(lineNode, "name", guaranters[cntr]);
    	  }
		  else
		  {
			addTo(lineNode, "name", "");
		  }
    	  elemNode.appendChild(lineNode);
	  }

	}
	else
    if ((nbcDP.GuaranterList()-min_no_of_Guarenters)>0)
    	{
    	  maxLineCount = nbcDP.GuaranterList();
    	 for ( int lineCount = 0; lineCount < maxLineCount; lineCount++ )
      	{
      	  lineNode = doc.createElement("Guaranter");
      	  addTo(lineNode, "name", guaranters[lineCount]);
      	  elemNode.appendChild(lineNode);
      	}
	}
	else
	{

	  maxLineCount = min_no_of_Guarenters;
		for ( int lineCount = 0; lineCount < maxLineCount; lineCount++ )
    	{
		  lineNode = doc.createElement("Guaranter");
    		  if(lineCount<guaranters.length)
    		  {
    			addTo(lineNode, "name", guaranters[lineCount]);
        	  }
    		  else
    		  {
    			addTo(lineNode, "name", "");
    		  }
		  elemNode.appendChild(lineNode);
    	}
	}

	return elemNode;
  }

  protected Node exBorrowerNamesLoop(int min_no_of_Borrowers, String tagName)
  {
	Node elemNode = null;
	Node lineNode;
	int maxLineCount = 0;
	if (elemNode == null)
	{
	  elemNode = doc.createElement(tagName);
	}
	String[] borrowers = nbcDP.BorrowerNamesforLoop();
	if ((borrowers.length - min_no_of_Borrowers) > 0){
	  maxLineCount = borrowers.length;
	}
	else{
		maxLineCount = min_no_of_Borrowers;
	}
	if(maxLineCount<3){
	  maxLineCount=1;
	}else{
	  maxLineCount--;
	}
	for ( int lineCount = 0; lineCount < maxLineCount; lineCount++ )
		{
		  lineNode = doc.createElement("Borrower");
  		  if(lineCount<borrowers.length){
  			addTo(lineNode, "name", borrowers[lineCount]);
      	  }
  		  else {
  			addTo(lineNode, "name", "");
  		  }
		  elemNode.appendChild(lineNode);
  	}

	return elemNode;
  }
  
  /* code fix 1615 - start
	 * Added check for Producttypeid == sloc and returns the current date
	 */
	protected String exDateforT26a() {
	  String dt="";
	  if (deal.getProductTypeId() == Mc.PRODUCT_TYPE_SECURED_LOC)
		dt= formatDate(new Date());
	  return dt;
	}
}
