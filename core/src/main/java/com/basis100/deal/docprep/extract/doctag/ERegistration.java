package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class ERegistration extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      Property p = new Property(srk,null);

      p = p.findByPrimaryProperty(deal
              .getDealId(), deal.getCopyId(), deal.getInstitutionProfileId());
      //ALERT:BX:ZR Release2.1.docprep
      // just marker for call for picklistdata class, but no changes have been made
      if((PicklistData.getDescription("Province",p.getProvinceId())).equals("Ontario"))
      {
        ConditionHandler ch = new ConditionHandler(srk);

        val = ch.getVariableVerbiage(deal, "Ontario eRegistration Clause",lang);

        fml.addValue(val, fml.STRING);
      }
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }


    return fml;
  }
}
