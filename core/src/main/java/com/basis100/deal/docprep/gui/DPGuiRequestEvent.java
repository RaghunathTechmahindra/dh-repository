package com.basis100.deal.docprep.gui;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public final class DPGuiRequestEvent extends java.awt.event.ActionEvent
{

  public static final int REQUEST_TYPE_COMMAND = 1;
  public static final int REQUEST_TYPE_REQUEST_FOR_INFO = 2;
  public static final int RT_REQUEST_FOR_STATISTICS = 3;
  private int type;

  public DPGuiRequestEvent(Object pSource, int pType){
    super(pSource,0,"");
    type = pType;
  }

  public int getType(){
    return type;
  }

}