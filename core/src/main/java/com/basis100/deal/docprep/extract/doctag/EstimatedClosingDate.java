package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import MosSystem.*;

public class EstimatedClosingDate  extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
       if(clsid == ClassId.DEAL){
          Deal deal = (Deal)de;
          fml.addValue( deal.getEstimatedClosingDate() );
       }else{
          String exceptionMessage = this.getClass().getName() + " : Unknown entity: " + clsid;
          srk.getSysLogger().error(exceptionMessage);
          throw new ExtractException(exceptionMessage);
       }
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
   catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}