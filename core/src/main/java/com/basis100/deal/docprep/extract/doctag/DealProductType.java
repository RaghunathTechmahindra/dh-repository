package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.DocumentTagExtractor;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
/**
 * @version 1.0
 * MCM Impl Team.
 * XS_16.4 | 11-Aug-2008 | 
 * This is the tag Extractor class, being used in preapprovalcertificate.mdfml to retrieve the ProductType value from the picklist.   
 * 
 *  
 */
public class DealProductType extends DocumentTagExtractor 
{
	/**
	 * MCM Impl Team | XS_16.4 | 11-Aug-2008 | This method retrieves the 'ProductType' 
	 *                 value from the picklist for particular deal.
	 */
	public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
	  {
	    debug("Tag -> " + this.getClass().getName());

	    FMLQueryResult fml = new FMLQueryResult();
	    Deal deal = (Deal)de;

	    try
	    {
	        MtgProd mtgProd  = deal.getMtgProd();
	        String prodType = BXResources.
            getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PRODUCTTYPE",
            		mtgProd.getProductTypeId(),
                    lang);        

	      fml.addString(prodType);
	    }
	    catch(NullPointerException npe)
	    {
	       return fml;
	    }
	    catch(Exception e)
	    {
	       String msg = e.getMessage();

	      if(msg == null) msg = "Unknown Error";

	      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

	      srk.getSysLogger().error(msg);
	      throw new ExtractException(msg);
	    }

	    return fml;

	  }
	}
