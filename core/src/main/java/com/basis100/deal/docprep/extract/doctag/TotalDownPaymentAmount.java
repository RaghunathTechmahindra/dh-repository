package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

/**
 * Title:        Total Down Payment Amount
 * Description:  Extracts and formats a total down payment amount XML tag
 * Copyright:    Copyright (c) 2002
 * Company:      Basis100, Inc
 * @author       John Schisler
 * @version 1.0
 */
public class TotalDownPaymentAmount extends DocumentTagExtractor
{
    /* We only want downpayment sources of type 1, 2, 5 and 7, which
     * translate into 'Personal Cash', 'RRSP', 'Unknown', and 'Other',
     * respectively.  Until we can move these constants somewhere more
     * global, they will remain here for easier reading of code logic.
     */
    private final static int SOURCE_PERSONAL_CASH = 1;
    private final static int SOURCE_RRSP = 2;
    private final static int SOURCE_UNKNOWN = 5;
    private final static int SOURCE_OTHER = 7;

    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk) throws com.basis100.deal.docprep.extract.ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();

        double amt = 0.0;

        try
        {
            Deal deal = (Deal) de;

            List prps = (List)deal.getDownPaymentSources();
            Iterator it = prps.iterator();

            while(it.hasNext())
            {
                DownPaymentSource dps = (DownPaymentSource)it.next();

                switch(dps.getDownPaymentSourceTypeId())
                {
                    case SOURCE_PERSONAL_CASH:
                    case SOURCE_RRSP:
                    case SOURCE_UNKNOWN:
                    case SOURCE_OTHER:
                         amt += dps.getAmount();
                         break;

                    default:
                        break;
                }
            }

        }
        catch(FinderException fe)
        {
            return fml;
        }
        catch(NullPointerException npe)
        {
            return fml;
        }
        catch (Exception e)
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);
            throw new ExtractException(msg);
        }

        fml.addCurrency(amt);

        /////////////////////////////
        //  DEBUG
        //System.out.println("Down payment amount: " + amt);

        return fml;
    }
}