package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class PrivilegeLeft extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      int id = deal.getPrivilegePaymentId();

      //String option = PicklistData.getDescription("PrivilegePayment", id);
      //ALERT:BX:ZR Release2.1.docprep
      String option  = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PrivilegePayment",  id, lang);

      if(option != null)
      {
         option = option.substring(0,2);
         //dblVal = Double.parseDouble(option);
      }
/*
         try{
           dblVal = Double.parseDouble(option);
         }catch(Exception dblParseExc){
           String dblParseErrMsg = dblParseExc.getMessage();
           if(dblParseErrMsg == null ){dblParseErrMsg = "Unknown error" ;}
           dblParseErrMsg = " DOCPREP ERROR: Unable to parse double value: " + option + " class = " + this.getClass().getName() +  dblParseErrMsg;
           srk.getSysLogger().error(dblParseErrMsg);
           throw new Exception(dblParseErrMsg);
         }
*/
      //========================================================================
      // PROGRAMMER'S NOTE
      // the following lines are removed because of the request where we
      // should not show the value in format:  15.000 %, but we should showit
      // in format 15 %.
      //========================================================================
      //double dblVal = Double.parseDouble(option);
      //fml.addRate(dblVal);
      option = option + " %";
      fml.addValue(option, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
/*
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
*/
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}




