package com.basis100.deal.docprep.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.swing.border.EtchedBorder;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Vector;
import java.util.Iterator;
import com.basis100.resources.PropertiesCache;
import com.basis100.deal.docprep.ExpressDocprepHelper;

public class GeneralInfoPanel extends JPanel
    implements ActionListener, DPGuiRenderer, HyperlinkListener
{
  private final String sStatXSL    = "statistics.xsl";
  private final String sActiveXSL  = "activeStat.xsl";
  private final String sFailedXSL  = "failedStat.xsl";
  private final String sWorkingXSL = "workingStat.xsl";

  private JButton statisticsBtn;
  private Dimension pnlSize;
  private JEditorPane editorPane;
  private String xslFolder;
  private String xml = " ";
  private String xslFile = sStatXSL;
  private Vector dpGuiListeners;
  private HistoryHolder history;
  private JToolBar toolBar;

  private boolean DEBUG = true;

  public GeneralInfoPanel(Dimension dm) {
    super();
    this.pnlSize = dm;
    init();
  }

  public GeneralInfoPanel() {
    super();
    this.pnlSize = (new Dimension(400, 300));
    init();
  }

  private void init(){
    this.setLayout(new BorderLayout());
    this.setPreferredSize(pnlSize);
    this.setMinimumSize(new Dimension(10, 10));

    dpGuiListeners = new Vector();

    // 1) set history
    int capacity = Integer.parseInt(PropertiesCache.getInstance().getUnchangedProperty(-1, "com.filogix.docprep.gui.history.capacity","20"));
    history = new HistoryHolder(capacity);

    // 2) create toolbar
    createToolBar();
    add(toolBar, BorderLayout.NORTH);

    // 3) create a JEditorPane
    editorPane = createEditorPane();
    JScrollPane editorScrollPane = new JScrollPane(editorPane);
    editorScrollPane.setVerticalScrollBarPolicy(
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    add(editorScrollPane, BorderLayout.CENTER);
  }

  // GUI components
  private void createToolBar(){

     toolBar = new JToolBar();
     toolBar.setFloatable(false);

     toolBar.setBorder(BorderFactory.createLineBorder(Color.black));
     toolBar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
     toolBar.addSeparator();
     addButtonsTo();
   }

  private void addButtonsTo(){
    statisticsBtn = new JButton("Statistics");
    statisticsBtn.setSize(23, 23);
    statisticsBtn.setPreferredSize(new Dimension(23, 23));
    statisticsBtn.setMinimumSize(new Dimension(23, 23));
    statisticsBtn.addActionListener(this);
    toolBar.add(statisticsBtn);
    toolBar.addSeparator();
  }

  private JEditorPane createEditorPane(){
    HTMLEditorKit htmlKit = new HTMLEditorKit();
    JEditorPane edp = new JEditorPane();
    HTMLDocument htmlDoc = (HTMLDocument) htmlKit.createDefaultDocument();
    edp.setEditorKit(htmlKit);
    edp.setDocument(htmlDoc);
    edp.setContentType("text/html");
    edp.setEditable(false);
    edp.addHyperlinkListener(this);
    return edp;
  }

  public void addGuiRequestListener(DPGuiRequestListener pListener){
    dpGuiListeners.addElement(pListener);
  }

  private void triggerGuiRequest(DPGuiRequestEvent event){
    Iterator it = dpGuiListeners.iterator();
    while(it.hasNext()){
      ((DPGuiRequestListener)it.next()).DPGuiRequest(event);
    }
  }

  public void updateGui(UpdateGuiEvent event){
    System.out.println("I am updating gui now through update gui event...." + event.getActionCommand());
    xml = (String)event.getSource();
    if (DEBUG){ System.out.println(xml);}

    if(event.getActionCommand().equals("SHOW_STATISTICS")){
      xslFile = sStatXSL;
    }
    if(event.getActionCommand().equals("SHOW_ACTIVE_DETAILES")){
      xslFile = sActiveXSL;
    }
    if(event.getActionCommand().equals("SHOW_FAILED_DETAILES")){
      xslFile = sFailedXSL;
    }
    if(event.getActionCommand().equals("SHOW_WORKING_DETAILES")){
      xslFile = sWorkingXSL;
    }
    processPage();
  }

  private void processPage(){
    String html = xslMerge();
    if (DEBUG){ System.out.println(html);}

    String sTmp = html.substring(0, html.indexOf("<head>")).concat(
        html.substring(html.indexOf("</head>") + 7, html.length())).replace('\n',' ');
    if (DEBUG){ System.out.println(sTmp);}

    history.addPage(sTmp);

    setEditorPaneText(sTmp);
  }

  public void actionPerformed(ActionEvent ae) {
    Object obj = ae.getSource();
    if (obj == statisticsBtn) {
      DPGuiRequestEvent ev = new DPGuiRequestEvent(this,
          DPGuiRequestEvent.RT_REQUEST_FOR_STATISTICS);
      triggerGuiRequest(ev);
      return;
    }
  }

  public void hyperlinkUpdate(HyperlinkEvent e){
    if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED){
      if (DEBUG) {System.out.println("Hyperlink has been updated");}
      if (DEBUG) {System.out.println("URL:"+ e.getURL() );}
      if (DEBUG) {System.out.println("Description:"+ e.getDescription());}
      if (DEBUG) {System.out.println("type:" + e.getEventType());}

      String desc = e.getDescription();

      // processing of the history buttons
      if (desc == "GO_HISTORY_PREV")  {setEditorPaneText( (String)history.getPrev());}
      if (desc == "GO_HISTORY_NEXT")  {setEditorPaneText( (String)history.getNext());}
      if (desc == "GO_HISTORY_FIRST") {setEditorPaneText( (String)history.getFirst());}
      if (desc == "GO_HISTORY_LAST")  {setEditorPaneText( (String)history.getLast());}
    }
  }

  private void setEditorPaneText(String input){
    editorPane.setText(input);
  }

  private String getStylesheetFolder(){
	  String folder = ExpressDocprepHelper.getGuiFolder();

    if(!folder.endsWith(System.getProperty("file.separator"))){
      folder += System.getProperty("file.separator");
    }
    return folder;
  }

  private String xslMerge(){
    if (xslFolder == null){
      xslFolder = getStylesheetFolder();
    }

    File xslInFile = new File(xslFolder + xslFile);
    if (DEBUG){ System.out.println("Stylesheet in use: " + xslInFile);}
    Source xmlSource = new StreamSource(new StringReader(xml));
    Source xslSource = new StreamSource(xslInFile);

    try {
      if (DEBUG){ System.out.println("TRANSFORMATION STARTED");}
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformer = tFactory.newTransformer(xslSource);
      StringWriter swr = new StringWriter();
      transformer.transform(xmlSource, new StreamResult(swr));
      if (DEBUG){ System.out.println("TRANSFORMATION ENDED");}
      return swr.toString();
    }
    catch (TransformerException te){
      if (DEBUG){
        System.out.println(te.getMessage());
      }
      return "";
    }
  }
}