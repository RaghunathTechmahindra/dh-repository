package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.security.*;
import com.basis100.deal.validation.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.entity.*;
import MosSystem.Sc;

public class BusinessRule  extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;
    Vector prefVect = new java.util.Vector();
    HashMap globMap = new java.util.HashMap();
    try
    {
      PassiveMessage pm = new PassiveMessage();
      PassiveMessage borrPm = new PassiveMessage();
      PassiveMessage propPm = new PassiveMessage();
      pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
      //BusinessRuleExecutor brExec = new BusinessRuleExecutor();
      PagelessBusinessRuleExecutor plBrExec = new PagelessBusinessRuleExecutor();

      DealPK dealpk = (DealPK)deal.getPk();
      prefVect.addElement("DE-%");
      prefVect.addElement("DI-%");
      prefVect.addElement("IC-%");
      prefVect.addElement("UN-%");
      prefVect.addElement("MI-%");
      globMap.put("CurrentBorrowerId", new Integer(-1));
      globMap.put("CurrentPropertyId", new Integer(-1));

      UserProfileBeanPK upbPK = new UserProfileBeanPK(deal.getUnderwriterUserId(), 
                                                      deal.getInstitutionProfileId());
      UserProfile up = new UserProfile(srk);
      try{
        up = up.findByPrimaryKey(upbPK);
      }catch(RemoteException rExc){
        throw new Exception(this.getClass().getName() + "Remote exception while trying to find user profile.\n + " + rExc.getMessage() );
      }catch(FinderException fExc){
        throw new Exception(this.getClass().getName() + "Finder Exception exception while trying to find user profile.\n" + fExc.getMessage());
      }
      try{
        globMap.put( "UserTypeId", new Integer(up.getUserTypeId()) );
      }catch(Exception exc){
        throw new Exception(this.getClass().getName() + ":(1) ERROR- Could not create userTypeId.\n" + exc.getMessage() );
      }
// validate(DealPK pk, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs, HashMap globPairs)
      plBrExec.validate(deal,srk,pm,prefVect,globMap, lang);

      // System.out.println("Dealid " + deal.getDealId() + " Copy :" + deal.getCopyId());
      // brExec.BREValidator(dealpk, srk, pm,"DI-%") ;
      // brExec.BREValidator(dealpk, srk, pm,"DE-%") ;
      // brExec.BREValidator(dealpk, srk, pm,"IC-%") ;
      // brExec.BREValidator(dealpk, srk, pm,"UN-%") ;
      // brExec.BREValidator(dealpk, srk, pm,"MI-%") ;
      // borrower related business rules
      Collection borrCol = deal.getBorrowers();
      Iterator itBor = borrCol.iterator();
      Borrower lBorrower;
      Property lProperty;
      globMap = new java.util.HashMap();
      while( itBor.hasNext() ){
        borrPm = new PassiveMessage();
        lBorrower = (Borrower)itBor.next();
        pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
        //pm.addMsg(Sc.PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM + lBorrower.formFullName(),pm.INFO);
        pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM", lang) + lBorrower.formFullName(),pm.INFO);
        pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
        globMap.put("CurrentBorrowerId", new Integer( lBorrower.getBorrowerId() ));
        globMap.put("CurrentPropertyId", new Integer(-1));
        try{
          globMap.put( "UserTypeId", new Integer(up.getUserTypeId()) );
        }catch(Exception exc){
          throw new Exception(this.getClass().getName() + ":(2) ERROR- Could not create userTypeId.\n" + exc.getMessage() );
        }

        prefVect.removeAllElements();
        prefVect.add("DEA-%");
        //--DJ_LDI_CR--start--//
        prefVect.add("LDI-%");
        //--DJ_LDI_CR--end--//
        plBrExec.validate(deal,srk,borrPm,prefVect,globMap, lang);
        //brExec.setCurrentProperty(-1);
        //brExec.setCurrentBorrower( lBorrower.getBorrowerId() );
        //PassiveMessage borrPm = brExec.BREValidator(dealpk, srk, null,"DEA-%");
        pm.addAllMessages(borrPm);
      }

      // Property related business rules
      Collection propCol = deal.getProperties();
      Iterator itProp = propCol.iterator();
      globMap = new java.util.HashMap();
      while( itProp.hasNext() ){
        propPm = new PassiveMessage();
        lProperty = (Property)itProp.next();
        pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
        pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM",lang) + lProperty.formPropertyAddress(),pm.INFO);
        pm.addMsg("--------------------------------------------------------------------------------",pm.SEPARATOR);
        globMap.put("CurrentBorrowerId", new Integer(-1));
        globMap.put("CurrentPropertyId", new Integer(lProperty.getPropertyId() ));
        try{
          globMap.put( "UserTypeId", new Integer(up.getUserTypeId()) );
        }catch(Exception exc){
          throw new Exception(this.getClass().getName() + ":(2) ERROR- Could not create userTypeId.\n" + exc.getMessage() );
        }

        prefVect.removeAllElements();
        prefVect.add("DEP-%");
        //--DJ_#548_Ticket--start--//
        // Appraisal only set of BR.
        prefVect.add("AR-%");
        //--DJ_#548_Ticket--end--//
        //plBrExec.validate(dealpk,srk,propPm,prefVect,globMap);
        plBrExec.validate(deal,srk,propPm,prefVect,globMap, lang);
        //brExec.setCurrentBorrower(-1);
        //brExec.setCurrentProperty(lProperty.getPropertyId());
        //PassiveMessage propPm = brExec.BREValidator(dealpk, srk, null,"DEP-%");
        pm.addAllMessages(propPm);
      }
      String outMsg;
      for(int i=0; i<pm.getNumMessages(); i++){
        outMsg = (String)pm.getMsgs().elementAt(i);
        if(  ((Integer)pm.getMsgTypes().elementAt(i) ).intValue() == PassiveMessage.CRITICAL  ){
          outMsg = "C " + outMsg;
        } else if (  ((Integer)pm.getMsgTypes().elementAt(i) ).intValue() == PassiveMessage.NONCRITICAL ){
          outMsg = "- " + outMsg;
        }
        fml.addValue( outMsg , fml.STRING);
      }

    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    return fml;
   }

}


