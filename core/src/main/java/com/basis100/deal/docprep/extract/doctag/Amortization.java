package com.basis100.deal.docprep.extract.doctag;

import MosSystem.Mc;

import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.DocumentTagExtractor;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.YearsAndMonths;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.entity.FinderException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * @version 1.0 - MCM Impl Team.| XS_16.4 | 11-Aug-2008 | 
 *                                This is the tag Extractor class being used in preapprovalcertificate.mdfml 
 *                                to retrieve the Amortization value from Database.   
 * 
 *  
 */

public class Amortization extends DocumentTagExtractor {
	/**
	 * MCM Impl Team | XS_16.4 | 11-Aug-2008 | This method returns value for 'Amortization' field
	 * 
	 * if deal product is component eligible or UnderWriteTypeAs 'LOC' then
	 *      Amortization  = 'N/A*'
	 * else
	 *       Amortization = deal.AmortizationTerm.  
	 */
	public FMLQueryResult extract(DealEntity de, int lang,DocumentFormat format, SessionResourceKit srk) throws ExtractException 
	{
		debug("Tag -> " + this.getClass().getName());

	    FMLQueryResult fml = new FMLQueryResult();

	    Deal deal = (Deal)de;

	    try
	    {
	    	MtgProd mtgProd  = deal.getMtgProd();
	    	String compEligibleFlag = mtgProd.getComponentEligibleFlag();      
	        
	        if("Y".equalsIgnoreCase(compEligibleFlag))
		    {
	        	fml.addValue(BXResources.getGenericMsg(BXResources.NOT_AVAILABLE_LABEL, lang), fml.STRING);
		    }	        
	        else 
	        {
	        	if(mtgProd.getUnderwriteAsTypeId() == Mc.COMPONENT_TYPE_LOC)
		        {
	        		fml.addValue(BXResources.getGenericMsg(BXResources.NOT_AVAILABLE_LABEL, lang), fml.STRING);
		        }
		        else
		        {
			        int amortization = deal.getAmortizationTerm();
			        YearsAndMonths yearsAndMonths = new YearsAndMonths(amortization);
			        fml.addValue(yearsAndMonths.getValue(lang),fml.STRING);
		        }
	        }

	    }
	    catch(NullPointerException npe)
	    {
	       return new FMLQueryResult();
	    }
	    catch(FinderException fe)
	    {
	       return new FMLQueryResult();
	    }
	    catch(Exception e)
	    {
	      String msg = e.getMessage();

	      if(msg == null) msg = "Unknown Error";

	      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

	      srk.getSysLogger().error(msg);
	      throw new ExtractException(msg);
	    }

	    return fml;
	  }
	}
