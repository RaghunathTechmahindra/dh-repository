package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

/**
 * <p>
 * Title: PrimeIndexStr
 * </p>
 * <p>
 * Description: This class is used to set the message for
 * ComponentPrimeIndexRateProfilePrimeName based on the language preference.
 * </p>
 * @version 1.0 Oct 29, 2008
 */
public class PrimeIndexStr extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang,
            DocumentFormat format, SessionResourceKit srk)
    throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());
        String val = "";
        FMLQueryResult fml = new FMLQueryResult();
        try
        {
            if (de.getClassId() == ClassId.COMPONENT)
            {
                Component components = (Component) de;
                String primeIndexName = components.getPrimeIndex(components
                        .getComponentId(), components.getCopyId());

                if (("not in use").equalsIgnoreCase(primeIndexName))
                {
                    // Setting the display string for french
                    if (lang == 1)
                        val = "taux de base n'affecte pas ce taux";
                    // Setting the display string for English
                    else if (lang == 0)
                        val = "not based on Prime";
                }
                else
                {
                    val = primeIndexName;
                }

            }
        }
        catch (Exception e)
        {
            srk.getSysLogger().error(e);
            throw new ExtractException(
            "ERROR: Unable to extract ComponentPrimeIndexRateProfilePrimeName");
        }
        fml.addValue(val, fml.STRING);
        return fml;

    }
}
