package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.entity.*;

//==============================================================================
// VERY IMPORTANT NOTE: this class will extract the credit bureau report from
// database but unlike other classes in this package will process that text and
// replace certain characters with their RTF counterparts. So, it means that
// this class will not be suitable for purposes other than RTF reporting. In such case
// make extension of this class and override required methods or create new class.
// This approach has been taken because of existance of different characters
// and our xml parser (xerces) does not preserve spaces properly.
// This solution shoould be in place until the final resolution for the problem
// gets found.
//==============================================================================

public class CreditBureauReport extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    if(clsid == ClassId.CREDITBUREAUREPORT){
        try
        {
          com.basis100.deal.entity.CreditBureauReport cbr = (com.basis100.deal.entity.CreditBureauReport)de;
          Collection creditBureauRpts = cbr.findByDealid( cbr.getDealId() );

          Iterator it = creditBureauRpts.iterator();

          while( it.hasNext() ){
           cbr = (com.basis100.deal.entity.CreditBureauReport)it.next();
           if(cbr.getCreditReport() != null ){
             String toBeParsed = cleanSpecialChars( cbr.getCreditReport() );
             //fml.addValue( cbr.getCreditReport(), fml.STRING);
             fml.addValue( toBeParsed, fml.STRING);
           }
          }
        }
        catch (Exception e)
        {
          String msg = e.getMessage();

          if(msg == null) msg = "Unknown Error";

          msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

          srk.getSysLogger().error(msg);
          throw new ExtractException(msg);
        }
    }

    return fml;
  }

  private String cleanSpecialChars(String input)
  {
    StringBuffer retBuf = new StringBuffer("");
    int x=0;
    char comp1 = 0x2E;
    char comp2 = 0xC2;
    char comp3 = 0xB6;
    char comp4 = 0x20;
    char comp5 = 0x0A; // CR, #502


    for(x=0; x<input.length(); x++)
    {
      char c = input.charAt(x);

      if(c ==comp1 ) {
        retBuf.append("\\~");
       continue;
      }
      if( c==comp2) {
       continue;
      }
      if( c == comp3 ) {
        retBuf.append("\\par ");
       continue;
      }
      if( c == comp4 ) {
        retBuf.append("\\~");
       continue;
      }

      // CR, #502 ---------
      if( c == comp5 ) {
        retBuf.append("\\par ");
       continue;
      }
      // CR, #502 end ---------
      if((c == ' '))
      {
        retBuf.append("\\~")  ;
        continue;
      }

      if(!Character.isISOControl(c))
      {
        retBuf.append(c);
        continue;
      }

    }
    return retBuf.toString();
  }


}
