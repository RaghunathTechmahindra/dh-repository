package com.basis100.deal.docprep.deliver;

import java.io.IOException;
import java.io.StringReader;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.XmlToolKit;

/**
 * Created on 22-Feb-2005
 * <p> Title: DatxResponseParser </p>
 *
 * <p> Copyrihgt: Filogix Inc. (c) 2005</p>
 *  
 * <p> Company: Filogix Inc.</p>
 *
 *  * @author Catherine Rutgaizer
 * @version 1.0 (Initial version - 22 Feb 05)
 * 
 */
public class DatxResponseParser extends DefaultHandler
{
  public static final String STATUS_NODE             = "STATUS";
  public static final String CONDITION               = "_Condition";
  public static final String CONDITION_FAILED        = "FAILED";
  public static final String CONDITION_COMPLETED     = "COMPLETED";  
  public static final String CONDITION_ACKNOWLEGMENT = "ACKNOWLEGMENT";  
  public static final String REFERENCE_KEY           = "Reference key";
  public static final String CONVERSATION_KEY_NODE   = "CONVERSATIONKEY";
  public static final String CODE                    = "_Code";
  public static final String DESCRIPTION             = "_Description";
  
  private SessionResourceKit _srk;
  private String _statusCondition;
  private String _conversationKey;
  private String _referenceKey;
  private String _description;
  private String _code;
  
  private boolean _isConversationKey;
  
  public DatxResponseParser(){
    super();
  }
  
  public String getConversationKey()
  {
    return _conversationKey;
  }
  public String getReferenceKey()
  {
    return _referenceKey;
  }
  public String getStatusCondition()
  {
    return _statusCondition;
  }
  public String getCode()
  {
    return _code;
  }
  public String getDescription()
  {
    return _description;
  }
  
  public void read(String response) throws IOException, SAXException{
    XmlToolKit.getInstance().setSAX2Driver();
    
    XMLReader xr = XMLReaderFactory.createXMLReader();
    xr.setContentHandler(this);
    xr.setErrorHandler(this);
    
    xr.parse(new InputSource(new StringReader(response)));
  }
  
  public void startDocument() throws SAXException
  {
  }

  public void endDocument() throws SAXException
  {
  }

  public void startElement(String namespaceURI, String sName, String qName,  Attributes attrs) throws SAXException
  {
    if ("".equals(namespaceURI)){                                                   // any
      if (sName == STATUS_NODE){
        _statusCondition = attrs.getValue(CONDITION);
        
        _description = attrs.getValue(DESCRIPTION);

        int ip = _description.indexOf(REFERENCE_KEY);
        _referenceKey = _description.substring(ip + 14);
      }
      
      if (sName == CONVERSATION_KEY_NODE){
        _isConversationKey = true;
      }
    }
  }

  public void endElement(String namespaceURI, String sName, String qName) throws SAXException
  {
    if (sName == CONVERSATION_KEY_NODE){
      _isConversationKey = false;
    }
  }

  public void characters(char buf[], int offset, int len) throws SAXException
  {
    if (_isConversationKey){
      _conversationKey = new String(buf, offset, len);  
    }
  }
  
  // ------------------------------ Helper methods ------------------------------
  public boolean isFailed(){
    return (_statusCondition.equalsIgnoreCase(CONDITION_FAILED));
  }
  
  public boolean isAcknowlegement(){
    return (_statusCondition.equalsIgnoreCase(CONDITION_ACKNOWLEGMENT));
  }
  
  public boolean isCompleted(){
    return (_statusCondition.equalsIgnoreCase(CONDITION_COMPLETED));
  }
  
}