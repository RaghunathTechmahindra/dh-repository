package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.collections.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.calc.*;
import java.util.*;


public class EmploymentIncomeSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {

      if(clsid == ClassId.EMPLOYMENTHISTORY)
      {

        EmploymentHistory eh = (EmploymentHistory)de;

        Income inc =  eh.getIncome();

        fml.addValue(inc);
        return fml;
      }
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    return fml;
  }
}
