package com.basis100.deal.docprep.extract.doctag;

import MosSystem.Mc;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.picklist.BXResources;
/**
 * @version 1.0 - Existing version
 * @version 1.1 | MCM Impl Team. | XS_16.4 | 11-Aug-2008 | 
 *                                 modified extract(..) method to return �Payment Frequency� value as �N/A*� 
 *                                 when DealProductType is component eligible and UnderWriteTypeAs is Mortgage  
 *  
 */
public class PaymentFrequency extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
    	/**** MCM Impl Team | XS_16.4 | 11-Aug-2008 | 
    	 *                    Logic added to display 'N/A*' for 'Payment Frequency' field 
    	 *                    when the deal product is component eligible 
    	 *                    and UnderWriteTypeAs is 'Mortgage'. - Starts  
         * ****/
        MtgProd mtgProd  = deal.getMtgProd();
        String compEligibleFlag = mtgProd.getComponentEligibleFlag();      
        
        if("Y".equalsIgnoreCase(compEligibleFlag) && mtgProd.getUnderwriteAsTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
        {
        	fml.addValue(BXResources.getGenericMsg(BXResources.NOT_AVAILABLE_LABEL, lang), fml.STRING);  	       	     
        }
        else
        {
	    	int id = deal.getPaymentFrequencyId();
		    //val = PicklistData.getDescription("PaymentFrequency",id);
		    //ALERT:BX:ZR Release2.1.docprep
		    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency",  id, lang);
		    fml.addValue(val, fml.TERM);
        }
        /**** MCM Impl Team | XS_16.4 | 11-Aug-2008 | Ends ***/
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;

  }
}
