package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;


public class MIPST extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;

        DealFee fee = new DealFee(srk,null);

        int types[] = { Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM};

        List fees = (List)fee.findByDealAndTypes((DealPK)deal.getPk(), types);
        double amt;
        if(fees.size() > 0)
        {
          fee = (DealFee)fees.get(0);
          amt = fee.getFeeAmount();
          fml.addCurrency(amt);
        }else{
          //====================================================================
          // the following change has been made as per of Product team's request
          // Request goes as: If no fee has been found than use the MIpremiumPST
          // amount from the deal class.
          //====================================================================
          amt = deal.getMIPremiumPST();
          fml.addCurrency(amt);
        }
      }

    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
