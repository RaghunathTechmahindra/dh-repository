package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class ProvincialClause extends DocumentTagExtractor
{
  private ConditionHandler ch ;
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      int mid = deal.getMIIndicatorId();

      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal.getDealId(), deal
          .getCopyId(), deal.getInstitutionProfileId());
      int ptid = prim.getPropertyTypeId();
      int prid = prim.getProvinceId();

      //ConditionHandler ch = new ConditionHandler(srk);
      ch = new ConditionHandler(srk);

      // set the two sub booleans (below province)
      boolean propType = false;
      boolean insuranceAndindicator = false;

      if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
          && deal.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
      {
        insuranceAndindicator = true;
      }

      if(ptid == Mc.PROPERTY_TYPE_CONDOMINIUM || ptid == Mc.PROPERTY_TYPE_FREEHOLD_CONDO
         || ptid == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO || ptid == Mc.PROPERTY_TYPE_TOWNHOUSE_CONDO)
      {
        propType = true;
      }



      if(  prid == Mc.PROVINCE_NUNAVUT )
      {

      }
      else if( prid == Mc.PROVINCE_NORTHWEST_TERRITORIES )
      {
        val += extractConditionText(deal, Dc.NWT_DELETIONS_CLAUSE ,lang);
        val += extractConditionText(deal, Dc.NWT_LENDER_CLAUSE ,lang);
        val += extractConditionText(deal, Dc.NWT_OFFER_LETTER_CLAUSE ,lang);
        if(deal.getDealTypeId() == Mc.DEAL_TYPE_CONVENTIONAL){
          val += extractConditionText(deal, Dc.NWT_CONVENTIONAL_CLAUSE,lang);
        }
        if( insuranceAndindicator ){
          val += extractConditionText(deal, Dc.NWT_INSURED_CLAUSE,lang);
        }
      }
      else if( prid ==  Mc.PROVINCE_YUKON ){

        val += extractConditionText(deal, Dc.YUKON_DELETIONS_CLAUSE ,lang);
        val += extractConditionText(deal, Dc.YUKON_LENDER_CLAUSE,lang);
        val += extractConditionText(deal, Dc.YUKON_OFFER_LETTER_CLAUSE,lang);

        if(deal.getDealTypeId() == Mc.DEAL_TYPE_CONVENTIONAL){
          val += extractConditionText(deal, Dc.YUKON_CONVENTIONAL_CLAUSE,lang);
        }
        if( insuranceAndindicator ){
          val += extractConditionText(deal, Dc.YUKON_INSURED_CLAUSE,lang);
        }
      }
      else if(prid == Mc.PROVINCE_ALBERTA)
      {
        if(insuranceAndindicator)
        {

          if(propType)
          {
             val += extractConditionText(deal, Dc.ALBERTA_INSURED_CONDO_CLAUSE,lang);
          }
          else
          {
             val += extractConditionText(deal, Dc.ALBERTA_INSURED_CLAUSE,lang);
          }
        }
        else
        {
          val += extractConditionText(deal,Dc.ALBERTA_CONVENTIONAL_CLAUSE,lang);
        }

         val += format.carriageReturn();
         val += extractConditionText(deal,Dc.ALBERTA_CLAUSE,lang);
         val += format.carriageReturn();
         val += extractConditionText(deal,Dc.ALBERTA_DISCLOSURE_CLAUSE,lang);      
      }
      else if(prid == Mc.PROVINCE_BRITISH_COLUMBIA)
      {
         val += extractConditionText(deal,Dc.BC_HEADER_CLAUSE,lang);
         val += format.carriageReturn();
         val += extractConditionText(deal,"BC Lender Clause",lang);

          if(insuranceAndindicator)
          {
            if(propType){
               val += format.carriageReturn();
               val += extractConditionText(deal,Dc.BC_INSURED_CONDO,lang);
            }else{
               val += format.carriageReturn();
               val += extractConditionText(deal,Dc.BC_INSURED_CLAUSE,lang);
            }
          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal, Dc.BC_CONVENTIONAL_CLAUSE,lang);
          }

         val += format.carriageReturn();
         val += extractConditionText(deal, Dc.BC_LAND_TITLE_CLAUSE,lang);
         val += format.carriageReturn();
         val += extractConditionText(deal,Dc.BC_FORM_DATE_CLAUSE,lang);
         val += format.carriageReturn();
         val += extractConditionText(deal,Dc.BC_ADDRESS ,lang);
         val += format.carriageReturn();
         val += extractConditionText(deal,Dc.BC_DISCLOSURE_CLAUSE ,lang);
      }
      else if(prid == Mc.PROVINCE_MANITOBA)
      {
         //val += ch.getVariableVerbiage(deal,"Manitoba Header Clause Clause",lang);
         // Zivko changed this because we had word Clause twice.
         val += extractConditionText(deal,"Manitoba Header Clause",lang);

          if(insuranceAndindicator)
          {
             val += format.carriageReturn();
             val += extractConditionText(deal,"Manitoba Insured Clause",lang);
          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal,"Manitoba Conventional Clause",lang);
          }

         val += format.carriageReturn();
         val += extractConditionText(deal,"Manitoba Form Date Clause",lang);

      }
      else if(prid == Mc.PROVINCE_NEW_BRUNSWICK)
      {
          if(insuranceAndindicator)
          {
             if(propType)
              {
               val += extractConditionText(deal,"NB Insured Condo Clause",lang);

              }
              else
              {
               val += extractConditionText(deal,"NB Insured Clause",lang);
              }
          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal,"NB Conventional Clause",lang);
          }

          val += format.carriageReturn();
          val += extractConditionText(deal,"NB Lender Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"NB Deletions Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"NB Offer Letter Clause",lang);

          val += format.carriageReturn();
          val += extractConditionText(deal,Dc.NB_DISCLOSURE_CLAUSE,lang);       

      }
      else if(prid == Mc.PROVINCE_NEWFOUNDLAND )
      {
         if(insuranceAndindicator)
          {
             val += format.carriageReturn();
             val += extractConditionText(deal,"NF Insured Clause",lang);
          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal,"NF Conventional Clause",lang);

          }

          val += format.carriageReturn();
          val += extractConditionText(deal,"NF Lender Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"NF Deletions Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"NF Offer Letter Clause",lang);

          val += format.carriageReturn();
          val += extractConditionText(deal,Dc.NF_DISCLOSURE_CLAUSE, lang);        

      }
      else if(prid == Mc.PROVINCE_NOVA_SCOTIA)
      {
         if(insuranceAndindicator)
          {
              if(propType)
              {
               val += extractConditionText(deal,"Nova Scotia Insured Condo Clause",lang);
              }
              else
              {
               val += extractConditionText(deal,"Nova Scotia Insured Clause",lang);
              }
          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal,"Nova Scotia Conventional Clause",lang);

          }

          val += format.carriageReturn();
          val += extractConditionText(deal,"Nova Scotia Lender Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"Nova Scotia Deletions Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"Nova Scotia Offer Letter Clause",lang);

          val += format.carriageReturn();
          val += extractConditionText(deal,Dc.NS_DISCLOSURE_CLAUSE,lang);       
      }
      else if(prid == Mc.PROVINCE_ONTARIO)
      {
         val += extractConditionText(deal,Dc.ONTARIO_HEADER_CLAUSE,lang);

         val += format.carriageReturn();
         val += extractConditionText(deal,Dc.ONTARIO_LENDER_CLAUSE,lang);

         val += format.carriageReturn();
         val += extractConditionText(deal,Dc.ONTARIO_EREGISTRATION_CLAUSE,lang);


         if(insuranceAndindicator)
          {
             val += format.carriageReturn();
             val += extractConditionText(deal, Dc.ONTARIO_INSURED_CLAUSE,lang);
          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal,Dc.ONTARIO_CONVENTIONAL_CLAUSE,lang);

          }

          val += format.carriageReturn();
          val += extractConditionText(deal, Dc.ONTARIO_LRRA_CLAUSE,lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,Dc.ONTARIO_SPOUSAL_CLAUSE,lang);


          val += format.carriageReturn();
          val += extractConditionText(deal, Dc.ONTARIO_FORM_DATE_CLAUSE,lang);

          val += format.carriageReturn();
          val += extractConditionText(deal, Dc.ONTARIO_ADDRESS,lang);

          val += format.carriageReturn();
          val += extractConditionText(deal, Dc.ONTARIO_TERANET,lang);

      }
      else if(prid == Mc.PROVINCE_PRINCE_EDWARD_ISLAND)
      {
         if(insuranceAndindicator)
         {
             val += format.carriageReturn();
             val += extractConditionText(deal,"PEI Insured Clause",lang);

          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal,"PEI Conventional Clause",lang);

          }

          val += format.carriageReturn();
          val += extractConditionText(deal,"PEI Lender Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"PEI Deletions Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"PEI Offer Letter Clause",lang);

          val += format.carriageReturn();
          val += extractConditionText(deal,Dc.PEI_DISCLOSURE_AND_TAX,lang);

      }
      else if(prid == Mc.PROVINCE_SASKATCHEWAN)
      {
         if(insuranceAndindicator)
          {
              val += extractConditionText(deal,"SK Lender Clause",lang);


              if(propType)
              {
               val += extractConditionText(deal,"SK Insured Condo Clause",lang);

              }
              else
              {
               val += extractConditionText(deal,"SK Insured Clause",lang);

              }
          }
          else
          {
            val += format.carriageReturn();
            val += extractConditionText(deal,"SK Conventional Clause",lang);

          }

          val += format.carriageReturn();
          val += extractConditionText(deal,"SK Lender Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"SK Deletions Clause",lang);


          val += format.carriageReturn();
          val += extractConditionText(deal,"SK Offer Letter Clause",lang);

      }

      String outVal = this.convertCRtoRTF(val, format);
      fml.addValue(outVal, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }

  private String convertCRtoRTF(String pStr, DocumentFormat format){
    if(! format.getType().startsWith("rtf") ){ return pStr; }
    int ind;
    StringBuffer retBuf = new StringBuffer("");
    while( (ind = pStr.indexOf("�")) != -1 ){
      retBuf.append(pStr.substring(0,ind));
      retBuf.append( format.carriageReturn() );
      pStr = pStr.substring(ind + 1);
    }
    retBuf.append(pStr);
    return retBuf.toString() ;
  }

  private String extractConditionText(Deal pDeal, String condTitle, int pLang) throws Exception
  {
    String tempText =  ch.getVariableVerbiage(pDeal,condTitle,pLang);

    if(tempText == null){
      return "";
    }else{
      return tempText;
    }
  }

  private String extractConditionText(Deal pDeal, int condId, int pLang) throws Exception
  {
    String tempText =  ch.getVariableVerbiage(pDeal,condId,pLang);

    if(tempText == null){
      return "";
    }else{
      return tempText;
    }

  }


}
