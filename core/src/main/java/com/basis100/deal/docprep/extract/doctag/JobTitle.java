package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;

public class JobTitle extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    try
    {

      if(de.getClassId() == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;
        EmploymentHistory hist = new EmploymentHistory(srk);
        hist = hist.findByCurrentEmployment((BorrowerPK)bor.getPk());
        return extract(hist, lang, format, srk);
      }
      else if(de.getClassId() == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        Borrower bor = new Borrower(srk,null);
        bor = bor.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
        EmploymentHistory hist = new EmploymentHistory(srk);
        hist = hist.findByCurrentEmployment((BorrowerPK)bor.getPk());
        return extract(hist, lang, format, srk);
      }
      else if(de.getClassId() == ClassId.EMPLOYMENTHISTORY)
      {
        EmploymentHistory hist =(EmploymentHistory)de;

        //val = PicklistData.getDescription("JobTitle",hist.getJobTitleId());
        //======================================================================
        // PROGRAMMER'S NOTE:  posted by zivko
        // the above line has been commented out and replaced because of
        // the approach that we no longer search for jobtitle in jobtitle table.
        // what we have to do is: take jobtitle directly from employmenthistory
        // table.
        // Related to bug 1578.
        //======================================================================
        val = hist.getJobTitle();
        if(val != null || val.length() != 0)
        {
          fml.addValue(val,fml.STRING);
        }

      }
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    
    return fml;
  }
}
