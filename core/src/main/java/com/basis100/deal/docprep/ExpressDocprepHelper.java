/*
 * @(#)ExpressDocprepHelper.java    2008-3-30
 *
 * Copyright (C) 2008 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.docprep;

import java.net.URL;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ExpressDocprepHelper is a helper class for Express Docprep.
 *
 * @version   1.0 2008-3-30
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressDocprepHelper {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(ExpressDocprepHelper.class);

    private final static String _templatesFolder;
    private final static String _mdfmlFolder;
    private final static String _guiFolder;

    static {
        // query the templates path.
        URL templatesFolderUrl =
            ExpressDocprepHelper.class.getResource("/docgen/templates/");
        if (templatesFolderUrl == null) {
            _logger.warn("Could NOT find '/docgen/templates' in classpath!");
            _templatesFolder = "";
        } else {
            _templatesFolder = templatesFolderUrl.toString().substring(5);
            _logger.info("Path to '/docgen/templates' is [{}].",
                         _templatesFolder);
        }

        // query the mdfml path.
        URL mdfmlFolderUrl =
            ExpressDocprepHelper.class.getResource("/docgen/mdfml/");
        if (mdfmlFolderUrl == null) {
            _logger.warn("Could NOT find '/docgen/mdfml' in classpath!");
            _mdfmlFolder = "";
        } else {
            _mdfmlFolder = mdfmlFolderUrl.toString().substring(5);
            _logger.info("Path to '/docgen/mdfml' is [{}].",
                         _mdfmlFolder);
        }

        // query the GUI path.
        URL guiFolderUrl =
            ExpressDocprepHelper.class.getResource("/docgen/gui/");
        if (guiFolderUrl == null) {
            _logger.warn("Could NOT find '/docgen/gui/' in classpath!");
            _guiFolder = "/docgen/gui/";
        } else {
            _guiFolder = guiFolderUrl.toString().substring(5);
            _logger.info("Path to '/docgen/gui' is [{}].",
                         _guiFolder);
        }
    }

    /**
     * return the full path to the gui template folder.
     */
    public static String getGuiFolder() {

        return _guiFolder;
    }

    /**
     * return the full path of the templates folder.
     */
    public static String getTemplatesFolder() {
        return _templatesFolder;
    }

    /**
     * return the template as file.
     */
    public static File getTemplateAsFile(String templateName) {

        return new File(getTemplatesFolder() + templateName);
    }

    /**
     * return the template as stream.
     */
    public static InputStream getTemplateAsStream(String templateName)
        throws FileNotFoundException {

        return new FileInputStream(getTemplatesFolder() + templateName);
    }

    /**
     * return the full path of the mdfml folder.
     */
    public static String getMdfmlFolder() {
        return _mdfmlFolder;
    }

    /**
     * return the metadata as file.
     */
    public static File getMdfmlAsFile(String mdfmlName) {

        return new File(getMdfmlFolder() + mdfmlName);
    }

    /**
     * return the metadata as stream.
     */
    public static InputStream getMdfmlAsStream(String mdfmlName)
        throws FileNotFoundException {

        return new FileInputStream(getMdfmlFolder() + mdfmlName);
    }

    /**
     * return the full path of the logo images folder. specifically for XSL
     * templates.
     */
    public static String getTemplateImageLocation() {

        // at this time, we are using the templates folder to keep all images
        // used by XSL templates.
        if (_templatesFolder.indexOf(":") == 2) {
            _logger.info("Logo image folder for Windows: [{}].",
                         _templatesFolder.substring(1));
            return _templatesFolder.substring(1);
        }

        return _templatesFolder;
    }
}