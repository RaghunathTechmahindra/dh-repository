package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class InterestIdentifier extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Deal deal = (Deal)de;
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      LenderProfile lp = deal.getLenderProfile();

      if(lp != null)
      {
        String shortName = lp.getLPShortName();

        if(shortName != null && shortName.equals("ING"))
        {
          String lenderName = lp.getLenderName();
          lenderName += "'s";
          fml.addValue(lenderName, fml.STRING);
          return fml;
        }
      }

      fml.addValue("our", fml.STRING);
      return fml;

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }
}
