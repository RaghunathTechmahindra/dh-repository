package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.deal.calc.*;
import MosSystem.*;

public class Obligations extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();
        int clsid = de.getClassId();

        try
        {
            if(clsid == ClassId.DEAL)
            {
                Deal deal = (Deal)de;

                List borrowers = (List)deal.getBorrowers();

                if (borrowers.isEmpty())
                   return null;

                Iterator i = borrowers.iterator();
                double amt = 0d;

                while (i.hasNext())
                {
                    Borrower b = (Borrower)i.next();

                    if (b.getBorrowerTypeId() == Mc.BT_BORROWER)
                    {
                        amt += b.getTotalIncomeAmount();
                    }
                }

                PricingProfile pp = new PricingProfile(srk);
                pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

                MtgProd pr = new MtgProd(srk,null);

                List all = (List)pr.findByPricingProfile(pp.getPricingProfileId());

                if(all.size() > 0)
                {
                    pr = (MtgProd)all.get(0);
                    //amt *= pr.getMaximumAmount() / 100;
                    amt *= pr.getMiscellaneousRate() / 100;
                }

                amt /= 12;

                fml.addCurrency(amt);
            }
        }
        catch (Exception e)
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);
            throw new ExtractException(msg);
        }

        return fml;
    }
}
