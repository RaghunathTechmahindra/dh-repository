package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

public class BranchCurrentTime extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";

    Deal deal = (Deal)de;

    int bid = deal.getBranchProfileId();

    FMLQueryResult fml = new FMLQueryResult();

    try
    {
        BranchProfile bp = new BranchProfile(srk,bid);

        int tzid = bp.getTimeZoneEntryId();

        TimeZoneEntry tze = new TimeZoneEntry(srk,tzid);
        String timeZoneSysId = tze.getTzeSysId();

        TimeZone tz = TimeZone.getTimeZone(timeZoneSysId);

        Calendar cal = Calendar.getInstance(tz);

        int hrs = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
        int ampm = cal.get(Calendar.AM_PM);
        val = "" + hrs + ":" + min + ":" + sec + " " ;
        if( ampm == 0 ){
          val += "AM";
        }else{
          val += "PM";
        }
        fml.addValue(val,fml.STRING);
        //System.out.println("VAL:" + val);
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }

}

/*
      LenderToBranchAssoc assoc = new LenderToBranchAssoc(srk);
      List list =(List)assoc.findByBranchAndLender(new BranchProfilePK(bid),new LenderProfilePK(lid));

      if(!list.isEmpty())
      {
        assoc = (LenderToBranchAssoc)list.get(0);

        if(assoc == null)  return new FMLQueryResult();

        if(lang == DocPrepLanguage.ENGLISH)
        {
          val = assoc.getETFaxNumber();

          if(val == null || val.length() < 10)
          {
            val = assoc.getELFaxNumber();

            if(val == null || val.length() < 10)
            {
              return new FMLQueryResult();
            }
          }

        }
        else if(lang == DocPrepLanguage.FRENCH)
        {
           val = assoc.getFTFaxNumber();

          if(val == null || val.length() < 10)
          {
            val = assoc.getFLFaxNumber();

            if(val == null || val.length() < 10)
            {
              return new FMLQueryResult();
            }
          }
        }


        ComponentResult  result = new ComponentResult();
        result.addResult("Fax:",fml.TERM);
        result.addResult(val,fml.PHONE_NUMBER);
        fml.addComponentResult(result);
      }

*/