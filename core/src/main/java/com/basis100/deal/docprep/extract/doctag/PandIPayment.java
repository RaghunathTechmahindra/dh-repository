package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;
import com.basis100.picklist.BXResources;
/**
 * @version 1.0 - Existing version
 * @version 1.1 | MCM Impl Team. | XS_16.4 | 11-Aug-2008 | 
 *                                 modified extract(..) method to return �Basic Payment Amount� value 
 *                                 as �N/A*� when DealProductType is component eligible 
 *                                 and UnderWriteTypeAs is Mortgage  
 *  
 */
public class PandIPayment extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      //======================================================================================
      // PROGRAMMER'S NOTE: Deal table - column pricingprofileid does not contain
      // pricing profile id, it contains pricing rate inventory id. So the path goes:
      // take pricingprofileid from deal, based on that key find the pricingrateinventoryid
      // in the pricingrateinventory table. From that record pick pricingprofileid value and
      // that is the key to search in the pricingprofile table.
      // Method findByPricingRateInventory in the PricingProfile class does exactly that.
      //=====================================================================================
      //PricingProfile pp = new PricingProfile(srk,deal.getPricingProfileId());
      
    	 /**** MCM Impl Team | XS_16.4 | 11-Aug-2008 | 
    	  *                   Logic added to display 'N/A*' for 'Basic Payment Amount' field 
    	  *                   when the deal product is component eligible 
    	  *                   and UnderWriteTypeAs is 'Mortgage'.  - Starts  
          * ****/
        MtgProd mtgProd  = deal.getMtgProd();
        String compEligibleFlag = mtgProd.getComponentEligibleFlag();       
        
        if("Y".equalsIgnoreCase(compEligibleFlag) && mtgProd.getUnderwriteAsTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
        {
        	fml.addValue(BXResources.getGenericMsg(BXResources.NOT_AVAILABLE_LABEL, lang), fml.STRING);  	       	     
        }
        else
        {
	      PricingProfile pp = new PricingProfile(srk);
	      pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );
	
	      String rc = pp.getRateCode();
	
	      if(rc.equalsIgnoreCase("UNCNF"))
	       fml.addValue("Unconfirmed", fml.STRING);
	      else
	       fml.addCurrency(deal.getPandiPaymentAmount());
        }
        /**** MCM Impl Team | XS_16.4 | 11-Aug-2008 | Ends ***/
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
