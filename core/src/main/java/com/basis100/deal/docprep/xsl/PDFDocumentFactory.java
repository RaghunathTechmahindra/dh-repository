package com.basis100.deal.docprep.xsl;

/**
 * 24/Oct/2007 DVG #DG650 DEJ18897: Desjardin DocPrep Queue taking too long 
 * Created for Rel 3.1 Express
 */
import java.util.Vector;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

public class PDFDocumentFactory {
	private Deal mDeal;
	private SysLogger mLogger;
	private SessionResourceKit mSrk;
	private DocumentProfile mDp;
	private String mXmlString;
	
	public PDFDocumentFactory(int pDealNo, int pLang, int pDocumentTypeId, SessionResourceKit pSrk) throws Exception {
		mSrk = pSrk;
		mLogger = mSrk.getSysLogger();
		
		mDeal = (new Deal(mSrk, null)).findByNonTransactionalRecommendedScenario(pDealNo);
		mDp = (new DocumentProfile(mSrk)).findByDocumentTypeIdAndLanguageId(pDocumentTypeId, pLang);
		
		mXmlString = "";
		
		if( mDp == null ) {
			String msg = "Could not generate DocumentProfile for document type " + pDocumentTypeId + " and language " + pLang;
			mSrk.getSysLogger().error(this.getClass(), msg);
			throw new Exception(msg);			
		}
	}
	
	/**
	 * Extracts data from DB and then generates vector of PDF documents as byte arrays
	 * 
	 * @return
	 * @throws DocPrepException
	 */
	public Vector produceDocuments() throws DocPrepException {
		Vector vPdfBytes = null;
		
		//Extract data from DB
		XSLDataFactory xslDataFactory = new XSLDataFactory(mDeal, mDp, mSrk);
		mXmlString = xslDataFactory.ExtractDataForXsl();
		
		if( mXmlString != null ) {
			//Generate PDFs as a vector of byte arrays
                        // modified for ML: added institutionProfileId
			//#DG650 XSLMerger xslMerger = new XSLMerger(mDeal.getDealId(), mDeal.getInstitutionProfileId(), mLogger, mDp );
			XSLMerger xslMerger = new XSLMerger(mDeal, mSrk, mDp );
			vPdfBytes = xslMerger.getPDFDocuments(mXmlString);
		}
		else {
			String msg = "XSLDataFactory has returned an empty xmlString for deal " + mDeal.getDealId() 
                                + " and document type " + mDp.getDocumentTypeId();
			throw new DocPrepException(msg);
		}
		return vPdfBytes;
	}

	/**
	 * 
	 * @return
	 */
	public String getXmlString() {
		return mXmlString;
	}
	
	
}
