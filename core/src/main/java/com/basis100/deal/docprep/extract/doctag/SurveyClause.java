package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;

import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class SurveyClause extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";

    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      int id = deal.getDealPurposeId();
      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal.getDealId(), deal
          .getCopyId(), deal.getInstitutionProfileId());
      int ptid = prim.getPropertyTypeId();

      if( id == Mc.DEAL_PURPOSE_PORTABLE_INCREASE || id == Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT
         || ptid == Mc.PROPERTY_TYPE_CONDOMINIUM || ptid == Mc.PROPERTY_TYPE_FREEHOLD_CONDO
         || ptid == Mc.PROPERTY_TYPE_LEASEHOLD_CONDO || ptid == Mc.PROPERTY_TYPE_TOWNHOUSE_CONDO)
       {

       }
       else
       {
          ConditionHandler ch = new ConditionHandler(srk);

          // the following has been removed because it has been removed from spec.
          //val += ch.getVariableVerbiage(deal,Dc.SURVEYVERB,lang);
          //val += format.carriageReturn();

          int prid = prim.getProvinceId();

          if(prid == Mc.PROVINCE_ALBERTA)
          {
             val += ch.getVariableVerbiage(deal,"Alberta Survey Clause",lang);

          }
          else if(prid == Mc.PROVINCE_BRITISH_COLUMBIA)
          {
             val += ch.getVariableVerbiage(deal,"BC Survey Clause",lang);

          }
          else if(prid == Mc.PROVINCE_MANITOBA
                  || prid == Mc.PROVINCE_SASKATCHEWAN)
          {
             val += ch.getVariableVerbiage(deal,"Man/Sask Survey Clause",lang);

          }
          else if(prid == Mc.PROVINCE_ONTARIO)
          {
             val += ch.getVariableVerbiage(deal,"Ontario Survey Clause",lang);

          }
          else if( prid == Mc.PROVINCE_NEW_BRUNSWICK
                  || prid == Mc.PROVINCE_NEWFOUNDLAND
                  || prid == Mc.PROVINCE_NOVA_SCOTIA
                  || prid == Mc.PROVINCE_PRINCE_EDWARD_ISLAND
                  || prid == Mc.PROVINCE_QUEBEC
                  || prid ==  Mc.PROVINCE_YUKON
                  || prid == Mc.PROVINCE_NUNAVUT
                  || prid == Mc.PROVINCE_NORTHWEST_TERRITORIES)
          {
             val += ch.getVariableVerbiage(deal,"Default", lang);
          }

       }

       if ((val == null)||(val.equals("null"))){
         return fml;
       }

       fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
