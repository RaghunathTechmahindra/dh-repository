package com.basis100.deal.docprep.extract;

import com.basis100.deal.xml.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;

public class FMLQueryContext
{
  private DealEntity entityCtx;
  private String codePackage;
  private int language;
  private DocumentFormat documentFormat;

  private boolean section = false;
  private int action = -1;

  public FMLQueryContext(DealEntity entity, String codePath, int lang, DocumentFormat fmt)
  {
     entityCtx = entity;
     codePackage = codePath;
     language = lang;
     documentFormat = fmt;
  }

  public DealEntity getEntityCtx()
  {
     return entityCtx;
  }


  public String getWhere()
  {
    if(entityCtx != null && entityCtx.getPk() != null)
     return entityCtx.getPk().getWhereClause();

    return null;
  }

  public String getName()
  {
    if(entityCtx != null)
     return entityCtx.getEntityTableName();

    return null;
  }

  public String getCodePackage()
  {
    if(codePackage == null) codePackage = "";
    return codePackage;
  }

  public int getLanguage()
  {
    return language;
  }

  public DocumentFormat getDocumentFormat()
  {
    return documentFormat;
  }

  public void isSection(boolean yn)
  {
    section = yn;
  }

  public boolean isSection()
  {
    return section;
  }

  public int getAction()
  {
    return action;
  }

  public void setAction(int act)
  {
    action = act;
  }
}