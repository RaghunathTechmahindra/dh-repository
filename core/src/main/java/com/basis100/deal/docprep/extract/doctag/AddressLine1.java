package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
/**
 *   if the recieved class is:<br>
 *   - Deal - get address info from primary borrower.<br>
 *   - Borrower - get address info from borrower current address.<br>
 *   - BorrowerAddress - get address info from BorrowerAddress.<br>
 *   - Property - get address info from Property.<br>
 *
 */

public class AddressLine1 extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());


    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        Borrower primary = new Borrower(srk,null);
        primary = primary.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
        return extract(primary, lang, format, srk);
      }
      else if(clsid == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;
        BorrowerAddress baddr = new BorrowerAddress(srk);
        baddr = baddr.findByCurrentAddress(bor.getBorrowerId(),bor.getCopyId());

        return extract(baddr, lang, format, srk);
      }
      else if(clsid == ClassId.BORROWERADDRESS)
      {
        BorrowerAddress baddr = (BorrowerAddress)de;
        Addr add = baddr.getAddr();
        String val = add.getAddressLine1();

        if(val != null || val.length() != 0)
        {
          fml.addValue(val,fml.STRING);
        }
      }
      else if(clsid == ClassId.PROPERTY)
      {
        Property p = (Property)de;

        String lUnitNum = p.getUnitNumber();
        String streetNum = p.getPropertyStreetNumber();
        String streetName = p.getPropertyStreetName();
        //String streetType = PicklistData.getDescription("StreetType",p.getStreetTypeId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetType = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType", p.getStreetTypeId(), lang);

        //String streetDir  = PicklistData.getDescription("StreetDirection",p.getStreetDirectionId());
        //ALERT:BX:ZR Release2.1.docprep
        String streetDir = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection", p.getStreetDirectionId(), lang);

        String val = "";

        if(lUnitNum != null && lUnitNum.length() > 0){
          val += lUnitNum + format.space() + "-" + format.space();
        }
        if(streetNum != null && streetNum.length() > 0)
          val += streetNum + format.space();
        if(streetName != null && streetName.length() > 0)
          val += streetName + format.space();
        if(streetType != null && streetType.length() > 0)
          val += streetType + format.space();
        if(streetDir != null && streetDir.length() > 0)
          val += streetDir + format.space();

        if(val.length() > 0) fml.addValue(val,fml.STRING);
      }
      else if(de.getClassId() == ClassId.EMPLOYMENTHISTORY)
      {
        EmploymentHistory hist =(EmploymentHistory)de;
        Contact c = hist.getContact();
        Addr addr = c.getAddr();

        String val = addr.getAddressLine1();

        if(val != null || val.length() != 0)
        {
          fml.addValue(val,fml.STRING);
        }

      }
      else if(de.getClassId() == ClassId.PARTYPROFILE)
      {
        PartyProfile party =(PartyProfile)de;
        Contact c = new Contact(srk,party.getContactId(),1);
        Addr addr = c.getAddr();

        String val = addr.getAddressLine1();

        if(val != null || val.length() != 0)
        {
          fml.addValue(val,fml.STRING);
        }

      }

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
