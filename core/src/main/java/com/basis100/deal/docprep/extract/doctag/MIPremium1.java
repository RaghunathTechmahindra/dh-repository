package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;
/**
  Set value to NULL
If the Deal.DealPurpose is not one of ("Refinance Existing Client", "Port Increase", "Portable" or "Portable Decrease")
	If Deal.MIIndicator  = "Rqd. Standard Guidelines" or "UW Required", and Deal.MIUpfront = "N"
		Set value to Deal.MIPremium

If the Deal.DealPurpose = "Refinance Existing Client" or "Port Increase"
	If Deal.MIIndicator  = "Rqd. Standard Guidelines" or "UW Required", and Deal.MIUpfront = "N"
	Set value to Deal.MIPremium	

If the Deal.DealPurpose = "Portable" or "Portable Decrease"
	If Deal.MIIndicator  = "Rqd. Standard Guidelines" or "UW Required", and Deal.MIUpfront = "N"
		Set value to Deal.MIPremium

If value is NULL, remove tag on template


*/


public class MIPremium1 extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;
    
    try
    {
       double amt = 0.0;
       int mid = deal.getMIIndicatorId();

       // The following is new logic
       //Set value to $0.00
       //If Deal.MIIndicator  = (�Rqd. Standard Guidelines� or Deal.MIIndicator  =  �UW Required�) and Deal.MIUpfront = �N�
	     //     Set value to Deal.MIPremium
       if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
            && deal.getMIUpfront().equals("N"))
       {
         amt = deal.getMIPremiumAmount();
       }
       fml.addCurrency(amt);
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
