package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;

public class PropertyLocation extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.PROPERTY)
      {
          Property p = (Property)de;
          int id = p.getPropertyLocationId();

          //ALERT:BX:ZR Release2.1.docprep
          //fml.addValue(PicklistData.getDescription("PropertyLocation",id), fml.TERM);
          fml.addValue(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PropertyLocation",  id, lang), fml.TERM);
      }

    }
    catch (Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
