package com.basis100.deal.docprep.rtf;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;

/**
 * Class ReportCreator joins template and data.
 * MCM Impl Team changes
 * @version 1.1 13-Aug-2008 XS_16.5 Added new action type hidesection and added 
 *                          logic to hide the section if actiontype is hidesection
 */
public class ReportCreator
{
  private static final String C_RTF_CR = "\\par ";
  private static final String C_BASIS_S = ":BASIS:";
  private static final String C_BASIS_E = ":ENDBASIS:";
  private static final int C_BASIS_S_L = 7;
  private static final int C_BASIS_E_L = 10;
  private static final String C_EOL = "\r\n";

  private static final String C_SWITCH_RTF = "SWITCH_RTF";
  private static final String C_RPTTEXT = "rpttext";
  private static final String C_SWITCH_ACTION = "switchaction";
  private static final String C_SWITCH_STATUS_OFF = "off";
  private static final String C_SWITCH_STATUS_ON = "on";
  private static final String C_SWITCH_NAME = "switchname";
  private static final String C_SWITCH_ACTION_STATUS = "switchstatus";
  private static final String C_DOC_SECTION = "docsection";
  private static final String C_DOC_TABLE = "doctable";
  private static final String C_REPLACEABLE_SECTION = "replaceablesection";
  private static final String C_REPLACEMENT = "replacement";
  private static final String C_SECTION_SET = "sectionset";
  private static final String C_ID_CODE = "idcode";
  private static final String C_NAME = "name";
  private static final String C_DOC_TAG = "doctag";
  private static final String C_IMAGE = "image";

  private File templateFile;
  private ConditionalActionList conditionalActionList;
  private ReportConstantList  reportConstantList;
  private SwitchManager switchManager;
  private SysLogger logger;
  private boolean CHAR_REPLACEMENTS_SWITCH;
  /**
   * Constructor.
  */
  public ReportCreator()
  {
    conditionalActionList = null;
    reportConstantList = null;
    switchManager = new SwitchManager();
    logger = ResourceManager.getSysLogger("DocPrep");
    CHAR_REPLACEMENTS_SWITCH = false;
  }

  /**
  * The starting point in the process of merging data and template.
  */
  public StringBuffer makeRTFReport(String pDataStr, File pTemplateFile) throws Exception
  {
    StringBuffer retBuff = null;
    Document lDataDoc, lTemplateDoc;
    logger.debug("Creating DATA document.");
    lDataDoc = getDataDocument(pDataStr);
    logger.debug("Creating TEMPLATE document.");
    lTemplateDoc = getTemplateDocument(pTemplateFile);
    logger.debug("Setting the template file.");
    setTemplateFile(pTemplateFile);
    NodeList lDataList, lTemplateList;
    ReportPortions lReportPortions;
    logger.debug("Extracting data nodes.");
    lDataList = extractDataNodes(lDataDoc);
    logger.debug("Extracting template nodes.");
    lTemplateList = extractTemplateNodes(lTemplateDoc);
    logger.debug("Extracting template portions.");
    lReportPortions = extractTemplatePortions(lTemplateDoc);
    logger.debug("Extracting conditional actions.");
    extractConditionalActions(lTemplateDoc);
    logger.debug("Extracting report constants.");
    extractReportConstants(lTemplateDoc);
    logger.debug("Merging started.");
    retBuff = mergeDataAndTpl( lDataList, lTemplateList, lReportPortions );
    logger.debug("Merging finished.");
    return retBuff;
  }

  /**
   * Main merging loop. Method called by makeRTFReport.
  */
  private StringBuffer mergeDataAndTpl(NodeList pDataList, NodeList pTplList, ReportPortions pRptPortions) throws Exception
  {
    Node lTplNode, lDocSectionNode, lDocTableNode, lReplSectNode;
    String lNodeName;
    String lRptTextId, lDocSectionId, lDocTableId, lReplSectId;
    String lSwitchStatus, lSwitchName;

    StringBuffer retValBuff = new StringBuffer("");
    for(int i=0; i< pTplList.getLength(); i++){
      lTplNode = pTplList.item(i);
      lNodeName = lTplNode.getNodeName();
      if(lNodeName.equalsIgnoreCase(C_RPTTEXT) ){
        lRptTextId = DomUtil.getAttributeValue(lTplNode, C_ID_CODE);
        logger.debug("Replace basis words for : " + lRptTextId);
        retValBuff.append( replaceBasisWords(pDataList,pRptPortions.retrieveTemplatePortion(lRptTextId)) );
      } else if( lNodeName.equalsIgnoreCase( C_SWITCH_ACTION ) ){
        lSwitchName = DomUtil.getAttributeValue(lTplNode, C_SWITCH_NAME);
        lSwitchStatus = DomUtil.getAttributeValue(lTplNode,C_SWITCH_ACTION_STATUS );
        logger.debug("Process switch action : SWITCH NAME=" + lSwitchName + "SWITCH STATUS=" + lSwitchStatus);
        processSwitchAction( lSwitchName, lSwitchStatus);
      } else if( lNodeName.equalsIgnoreCase( C_DOC_TABLE )){
        lDocTableId = DomUtil.getAttributeValue(lTplNode,C_ID_CODE);
        logger.debug("Process doc table for : " + lDocTableId);
        lDocTableNode = findDocTableByAttrName(pDataList, lDocTableId);
        if( lDocTableNode == null ){
           logger.debug("Doc table : " + lDocTableId + " does not exist.");
           continue;
        }
        retValBuff.append(processDocTable(lDocTableNode, lTplNode, pRptPortions));
      } else if(lNodeName.equalsIgnoreCase(C_REPLACEABLE_SECTION)){
        lReplSectId = DomUtil.getAttributeValue(lTplNode, C_ID_CODE);
        logger.debug("Process replaceable section for : " + lReplSectId);
        lReplSectNode = findReplSectByAttrName(pDataList,lReplSectId);
        if( lReplSectNode == null ){
          logger.debug("Replaceable section : " + lReplSectId + " does not exist.");
          continue;
        }
        retValBuff.append(processReplaceableSection(lReplSectNode, lTplNode, pRptPortions));
      } else if(lNodeName.equalsIgnoreCase(C_DOC_SECTION)){
        lDocSectionId = DomUtil.getAttributeValue(lTplNode,C_ID_CODE);
        logger.debug("Process doc section for : " + lDocSectionId);
        lDocSectionNode = findDocSectionByName(pDataList, lDocSectionId );
        if( lDocSectionNode == null ) {
          logger.debug("Doc section : " + lDocSectionId + " does not exist.");
          continue;
        }
        retValBuff.append(processDocSection(lDocSectionNode,lTplNode, pRptPortions));
      } else if(lNodeName.equalsIgnoreCase(C_SECTION_SET)){
        logger.debug("Process section set.");
        retValBuff.append(mergeDataAndTpl(pDataList, lTplNode.getChildNodes(), pRptPortions));
      }
    }
    return retValBuff;
  }

  private void processSwitchAction(String pSwitchName,String pSwitchStatus) throws Exception
  {
     if( pSwitchStatus.equalsIgnoreCase("off") ){
        switchManager.turnSwitchOFF(pSwitchName);
     }else if( pSwitchStatus.equalsIgnoreCase("on") ){
        switchManager.turnSwitchON(pSwitchName);
     } else {
        // it can be only on or off
        throw new Exception("Unkown switch status (" + pSwitchStatus + ") for switch (" + pSwitchName + ")" );
     }
  }

  private void setTemplateFile(File pTemplateFile){
    templateFile = pTemplateFile;
  }

  private String getTemplatePath(){
    String rv;
    String lPath = templateFile.getPath();
    String lName = templateFile.getName();
    int rpos = lPath.lastIndexOf(lName);
    if(rpos == -1){return "";}
    rv = lPath.substring(0,rpos);
    return rv;
  }

  private String processDocSection( Node pDocSectionNode, Node pTplNode, ReportPortions pRptPortions ) throws Exception
  {
    StringBuffer retBuff = new StringBuffer("");

    String lFormatStr = DomUtil.getAttributeValue(pDocSectionNode, "format");
    int lOccurence = Integer.parseInt( DomUtil.getAttributeValue(pDocSectionNode, "occurence") );
    Vector dataSectSetVect = findSectionSets( pDocSectionNode.getChildNodes() );
    for( int i=0; i < dataSectSetVect.size(); i++ ){
      retBuff.append(mergeDataAndTpl( ((Node)dataSectSetVect.elementAt(i)).getChildNodes(), pTplNode.getChildNodes(), pRptPortions ));
    }
    return new String(retBuff);
  }


  private String readReplacementFile(String pFileName) throws Exception
  {
    File lFile = new File(pFileName);
    if( !(lFile.exists()) || !(lFile.canRead()) ){
       logger.error("ERROR: method findReplacementFile - file:" + pFileName + " does not exist.");
    }
    FileReader fr = new FileReader(lFile);
    LineNumberReader lnr = new LineNumberReader(fr);
    String oneLine;
    StringBuffer retBuf = new StringBuffer("");
    while ( (oneLine = lnr.readLine()) != null ){
      retBuf.append(oneLine);
    }
    return new String(retBuf);
  }

  private String parseReplacementFile(String pFileName, Node pDataNode) throws Exception
  {
    File lFile = new File(pFileName);
    if( !(lFile.exists()) || !(lFile.canRead()) ){
       logger.error("ERROR: method parseReplacementFile - file:" + pFileName + " does not exist.");
    }
    Document lTplDocument = getTemplateDocument(lFile);
    NodeList lTplNodeList = extractTemplateNodes(lTplDocument);
    ReportPortions lReportPortions = extractTemplatePortions(lTplDocument);
    return new String(mergeDataAndTpl(pDataNode.getChildNodes(),lTplNodeList, lReportPortions));
  }

  private String processReplaceableSection(Node pDataNode, Node pTplNode,ReportPortions pRptPortions) throws Exception
  {
    StringBuffer retBuff = new StringBuffer("");
    boolean lParse = false;
    boolean lReplace = false;
    if(DomUtil.getAttributeValue(pDataNode,"replace").equalsIgnoreCase("true")){
      lReplace = true;
    }
    if(DomUtil.getAttributeValue(pDataNode,"parse").equalsIgnoreCase("true")){
      lParse = true;
    }
    int lOccurence = Integer.parseInt( DomUtil.getAttributeValue(pDataNode,"occurence") );
    NodeList dataSectionList;
    NodeList tplSectionList ;
    Node dataNode, templateNode;
    dataSectionList = pDataNode.getChildNodes();
    if(! lReplace ){
      // replacement is not required
      tplSectionList = pTplNode.getChildNodes();
      for(int i=0; i<dataSectionList.getLength(); i++){
        dataNode = dataSectionList.item(i);
        if( !(dataNode.getNodeName().equalsIgnoreCase(C_SECTION_SET)) ){ continue; }
        for(int j=0; j<tplSectionList.getLength(); j++ ){
          templateNode = tplSectionList.item(j);
          if( !(templateNode.getNodeName().equalsIgnoreCase(C_SECTION_SET)) ){ continue; }
          retBuff.append(mergeDataAndTpl( dataNode.getChildNodes(),templateNode.getChildNodes(), pRptPortions ));
        }
      }
    } else {
      // replacement is required.
      Node tempNode;
      String replFileName;
      for(int i=0; i<dataSectionList.getLength(); i++){
        tempNode = dataSectionList.item(i);
        if(!(tempNode.getNodeName().equalsIgnoreCase("replacement"))){ continue; }
        replFileName = getTemplatePath() + DomUtil.getAttributeValue(tempNode,"path") + ".xml";
        if( lParse ){
          retBuff.append( parseReplacementFile(replFileName,tempNode));
        }else{
          retBuff.append( readReplacementFile(replFileName));
        }
      }
    }
    return new String(retBuff);
  }

  /**
   * Method merges two columns that belong to one row into one row.
   * This method gets called by processDocTable method.
  */
  private String generateTableRow(Vector pDataVect, Node pTplNode, ReportPortions pRptPortions) throws Exception{
    Node firstNode = (Node) pDataVect.elementAt(0);
    Node lNode= null;
    NodeList lNodeList = null;
    Node lTagNode = null;

    for(int i=1; i<pDataVect.size(); i++){
      lNode = (Node)pDataVect.elementAt(i);
      if(!(lNode.getNodeName().equalsIgnoreCase(C_SECTION_SET))){ continue; }

      lNodeList = lNode.getChildNodes();

      for(int j= lNodeList.getLength() - 1; j>=0; j-- ){
        lTagNode = lNodeList.item(j);
        if(!(lTagNode.getNodeName().equalsIgnoreCase(C_DOC_TAG))){continue;}
        firstNode.appendChild(lTagNode);
      }
    }
    return new String(mergeDataAndTpl(firstNode.getChildNodes(),pTplNode.getChildNodes(),pRptPortions));
  }

  private String processDocTable( Node pTableData, Node pTplNode, ReportPortions pRptPortions ) throws Exception{
    // find how many columns we have
    StringBuffer retBuff = new StringBuffer("");
    int lNumColumns = Integer.parseInt(DomUtil.getAttributeValue(pTableData,"columns"));
    int lNumDocSections = 0;
    int lCount;
    // find how many Doc Sections we have
    NodeList lDocSections = pTableData.getChildNodes();
    for(lCount=0; lCount<lDocSections.getLength(); lCount++){
      if(lDocSections.item(lCount).getNodeName().equalsIgnoreCase(C_DOC_SECTION)){lNumDocSections++;}
    }
    if(! (lNumColumns == lNumDocSections) ){
      logger.error("ERROR: method - processDocTable - Number of columns does not match number of doc sections. " + "Number of columns = " + lNumColumns + "  Number of doc sections = " + lNumDocSections);
      throw new Exception("ERROR - number of columns does not match number of doc sections");
    }
    // find how many rows we have
    Node tempNode;
    int tempOccur;
    int maxOccur = 0;
    Vector columnVect = new Vector();
    for(int i=0; i<lDocSections.getLength(); i++){
      tempNode = lDocSections.item(i);
      if(!(tempNode.getNodeName().equalsIgnoreCase(C_DOC_SECTION))){continue;}
      columnVect.addElement(tempNode);
      tempOccur = Integer.parseInt(DomUtil.getAttributeValue(tempNode, "occurence"));
      if(tempOccur > maxOccur){maxOccur = tempOccur;}
    }
    Node tempSectionNode;
    NodeList sectionNodeList;
    int sectionCounter;
    Vector mergeVector = new java.util.Vector() ;
    for(int j=1; j<=maxOccur; j++){
      mergeVector = new java.util.Vector();
      for(int k=0; k<columnVect.size(); k++){
        tempNode = (Node) columnVect.elementAt(k);
        tempOccur = Integer.parseInt(DomUtil.getAttributeValue(tempNode, "occurence"));
        if( j > tempOccur ){continue; }
        sectionNodeList = tempNode.getChildNodes();
        sectionCounter = 0;
        for(int l=0; l<sectionNodeList.getLength(); l++){
          tempSectionNode = sectionNodeList.item(l);
          if(!(tempSectionNode.getNodeName().equalsIgnoreCase(C_SECTION_SET))){continue;}
          sectionCounter++;
          if(sectionCounter == j){
            // this is our guy
            mergeVector.addElement(tempSectionNode);
          }
        } // end for l
      } // end for k
      retBuff.append(generateTableRow(mergeVector,pTplNode, pRptPortions));
    } // end for j
    return new String(retBuff);
  }

  /**
   * Finds replaceable section by name.
  */
  private Node findReplSectByAttrName(NodeList pNodeList, String pName){
    String nameAttrVal;
    Node retVal ;
    for(int i=0; i<pNodeList.getLength() ; i++){
      retVal = pNodeList.item(i);
      if(retVal.getNodeName().equalsIgnoreCase("ReplaceableSection") ){
        nameAttrVal = DomUtil.getAttributeValue(retVal,"name");
        if(nameAttrVal == null ){ continue; }
        if( nameAttrVal.equalsIgnoreCase(pName) ){ return retVal; }
      }
    }
    logger.debug("Replaceable section " + pName + " NOT found.");
    return null;
  }

  /**
   * Finds document table by name.
  */
  private Node findDocTableByAttrName(NodeList pNodeList, String pName){
    String nameAttrVal;
    Node retVal = null;
    for(int i=0; i<pNodeList.getLength(); i++){
      retVal = pNodeList.item(i);
      if(retVal.getNodeName().equalsIgnoreCase(C_DOC_TABLE) ){
        nameAttrVal = DomUtil.getAttributeValue(retVal,C_NAME);
        if( nameAttrVal == null ){ continue; }
        if( nameAttrVal.equalsIgnoreCase(pName) ){return retVal; }
      }
    }
    logger.debug("Doc table " + pName + " NOT found.");
    return null;
  }

  private Node findDocSectionByName(NodeList pNodeList, String pName){
    Node retVal;
    logger.debug("Searching for doc section : " + pName);
    for(int i=0; i<pNodeList.getLength();i++){
      retVal = pNodeList.item(i);
      if(retVal.getNodeName().equalsIgnoreCase(C_DOC_SECTION) && DomUtil.getAttributeValue(retVal,C_NAME).equalsIgnoreCase(pName) ){
        logger.debug("Doc section " + pName + " found.");
        return retVal;
      }
    }
    logger.debug("Doc section " + pName + " NOT found.");
    return null;
  }

  /**
   * Returns section sets from the node.
  */
  private Vector findSectionSets(NodeList pNodeList){
    Node lNode;
    Vector retVect = new java.util.Vector();
    for( int i=0; i<pNodeList.getLength(); i++ ){
      lNode = pNodeList.item(i);
      if(lNode.getNodeName().equalsIgnoreCase(C_SECTION_SET) ){
        retVect.addElement(lNode);
      }
    }
    return retVect;
  }

  private Node findDataNodeByAttrName(NodeList pNodeList, String pName){
    Node retVal;
    String nameAttrVal;
    for(int i=0; i<pNodeList.getLength(); i++){
      retVal = pNodeList.item(i);
      if(! (retVal.getNodeName().equalsIgnoreCase(C_DOC_TAG)) ){
        continue;
      }
      nameAttrVal = DomUtil.getAttributeValue(retVal,C_NAME);
      if(nameAttrVal == null){
        continue;
      }
      if(nameAttrVal.equalsIgnoreCase(pName) ){
        return retVal;
      }
    }
    return null;
  }

  //=================================================================================================
  // programmers note: auxilary method used by processconditional statement
  //=================================================================================================
  private String findNodeOccurenceByName(NodeList parDataList, String parSearchName) throws Exception
  {
    StringBuffer retBuf = new StringBuffer("");
    Node lNode;
    lNode = findDataNodeByAttrName(parDataList,parSearchName);
    if(!(lNode== null)){
      retBuf.append(DomUtil.getAttributeValue(lNode, "occurence"));
      return new String(retBuf);
    }
    lNode = findDocSectionByName(parDataList, parSearchName);
    if( !(lNode == null) ){
      retBuf.append(DomUtil.getAttributeValue(lNode, "occurence"));
      return new String(retBuf);
    }
    return new String(retBuf);
  }
/**
 * 
   * This method will process the all conditional statemets
   * @version 1.1 13-11-2008 XS_16.5 Updated this method add logic to hide the sections
 */
  private String processConditionalStatement(NodeList pDataList, String pVariable) throws Exception
  {
    StringBuffer retBuff = new StringBuffer("");
    Vector actions = conditionalActionList.getAllActions(pVariable);
    ConditionalAction action;
    int actionEventType;
    int lOccurence;
    String lStrOccurence;
    //==========================================================================
    // first process on_occurence event
    //==========================================================================
    for(int i=0; i<actions.size(); i++){
      action = (ConditionalAction) actions.elementAt(i);
      actionEventType = action.getEventType();
      switch( actionEventType ){
        case ConditionalAction.ON_OCCURENCE :{
          lStrOccurence = findNodeOccurenceByName(pDataList,action.getMonitoredVarName());
          if( lStrOccurence.equals("") ){
            continue;
          } else {
            lOccurence = Integer.parseInt(lStrOccurence);
          }
          if(!(action.isOccComparisonSatisfied(lOccurence))){
            continue;
          }
          if(action.getActionType() == ConditionalAction.AT_TURN_SWITCH_ON ){
            switchManager.turnSwitchON( action.getActionParamOne() );
          }else if(action.getActionType() == ConditionalAction.AT_TURN_SWITCH_OFF){
            switchManager.turnSwitchOFF(action.getActionParamOne());
          }else if(action.getActionType() == ConditionalAction.AT_REPLACE_WITH_VAR){
            retBuff.append(findDataReplacement(pDataList,action.getActionParamOne()));
          }
          /************MCM Impl Team XS_16.5 Changes Starts*****************************/
          //if conditional action type is hidesection then return the value as hide.
          else if(action.getActionType() == ConditionalAction.AT_HIDESECTION){
              retBuff.append("hide");
          /************MCM Impl Team XS_16.5 Changes Ends*****************************/     
          }else if(action.getActionType() == ConditionalAction.AT_REPLACE_WITH_CONST){
            if(!(action.getActionParamOne().equalsIgnoreCase("EMPTY_STRING"))){
              retBuff.append(action.getActionParamOne());
            }
          }
          break;
        }
        default :{ continue; }
      } // end switch
    }// end for
    //=======================================================================================//
    // secondly process ON_SWITCH event                                                      //
    //=======================================================================================//
    for(int i=0; i<actions.size(); i++){
      action = (ConditionalAction) actions.elementAt(i);
      actionEventType = action.getEventType();
      switch( actionEventType ){
        case ConditionalAction.ON_SWITCH :{
          if(!(switchManager.isSwitchOK(  action.getSwitchName(), action.getSwitchStatus() ))  ){
            continue;
          }
          if(action.getActionType() == ConditionalAction.AT_TURN_SWITCH_ON ){
            switchManager.turnSwitchON( action.getActionParamOne() );
          }else if(action.getActionType() == ConditionalAction.AT_TURN_SWITCH_OFF){
            switchManager.turnSwitchOFF(action.getActionParamOne());
          }else if(action.getActionType() == ConditionalAction.AT_REPLACE_WITH_VAR){
            retBuff.append(findDataReplacement(pDataList,action.getActionParamOne()));
          }else if(action.getActionType() == ConditionalAction.AT_REPLACE_WITH_CONST){
              if(!(action.getActionParamOne().equalsIgnoreCase("EMPTY_STRING"))){
                retBuff.append(action.getActionParamOne());
              }
          }
          break;
        }
        default :{ continue; }
      } // end switch
    }// end for
    return new String(retBuff);
  }

  private String findDataReplacement(NodeList pDataList, String pVariable) throws Exception
  {
    StringBuffer retBuff;
    String retVal;
    if( reportConstantList.isConstantEntered(pVariable) ){
      return reportConstantList.getConstant(pVariable).getConstText();
    }
    if(conditionalActionList.isActionConditional(pVariable)){
      return processConditionalStatement(pDataList,pVariable);
    }
    Node lDataNode = findDataNodeByAttrName(pDataList, pVariable);
    int lOccurence = 0;
    String lOccurStr;
    String lFormat;
    if(! ( lDataNode == null ) ){
       lOccurStr = DomUtil.getAttributeValue(lDataNode, "occurence");
       if( !(lOccurStr == null) ){
         lOccurence = Integer.parseInt(lOccurStr);
       }
    }
    lFormat = DomUtil.getAttributeValue(lDataNode, "format");
    //===================================================================
    // now we have occurence and format, so go and do the job
    //===================================================================
    // 1. If the occurence is 0, apply the replacement format
    if(lOccurence == 0){
      if(lFormat.equalsIgnoreCase("removetag")){
        return " ";
      }
    }
    // 2. get the list of TagValue nodes and make sure that the number of
    // tag value nodes matches the number stated in the occurence attribute
    List valueNodes = DomUtil.getNamedChildren(lDataNode, "TagValue");
    if( ! ( lOccurence == valueNodes.size() ) ){
      logger.error("" + lOccurence + " nodes expected but " + valueNodes.size() + " found for tag " + pVariable);
      throw new Exception("" + lOccurence + " nodes expected but " + valueNodes.size() + " found for tag " + pVariable);
    }
    // 3. if the occurence is once just return the replacement
    if(lOccurence == 1){
      retVal = DomUtil.getNodeText( (Node)valueNodes.get(0)  );
      if(retVal == null){
        return " ";
      } else{
        return retVal;
      }
    } else {
      // 4. if the occurence is more than once append the carriage return
      // (in rtf it is \par tag) at the end of each replacement word, except
      // at the end of last one.
      retBuff = new StringBuffer("");
      for(int i=0; i<valueNodes.size(); i++){
        //retBuff.append((Node)valueNodes.get(i));
        retBuff.append(DomUtil.getNodeText((Node)valueNodes.get(i)));
        if( i < ( valueNodes.size() - 1 ) ){
          retBuff.append(C_RTF_CR);
        }
      }
      return new String(retBuff);
    }
  }
/**
 * 
 * This method replaces the basis word.
 *  @version 1.1 13-Aug-2008 XS_16.5 Added logic to hide the sections
 */
  private String replaceBasisWords(NodeList pDataList, String pTplText){
    String lProcText = new String(pTplText);
    boolean hideSection=false;

    if( pTplText.indexOf(C_BASIS_S)  == -1){return lProcText;}
    int lStInd, lEndInd,lTextStart,lTextEnd, lSPos;
    String replaceableWord, replacementWord;
    StringBuffer retBuf = new StringBuffer("");
    for(;;){
      lStInd = lProcText.indexOf(C_BASIS_S);
      if(lStInd == -1){
        retBuf.append(lProcText);
        break;
      }
      //============================================
      // now append to the out buffer evrything
      // from the begining of the string to the
      // index of BASIS keyword.
      //============================================
      retBuf.append( lProcText.substring(0,lStInd) );
      //============================================
      // now try to process basis keyword and replace
      // it with the real value.
      //============================================
      lEndInd = lProcText.indexOf(C_BASIS_E);
      lTextStart = lStInd + C_BASIS_S_L;
      lTextEnd = lEndInd ;
      lSPos = lEndInd + C_BASIS_E_L;
      replaceableWord = lProcText.substring(lTextStart,lTextEnd);
      replaceableWord = replaceableWord.trim().toUpperCase();

      if( replaceableWord.equals(C_SWITCH_RTF) ){
        switchCharacterReplacement();
        lProcText = new String(lProcText.substring(lSPos));
        continue;
      }
      replacementWord = null;
      try{
        replacementWord = findDataReplacement( pDataList, replaceableWord );
        if(CHAR_REPLACEMENTS_SWITCH){
          retBuf.append( cleanSpecialChars(replacementWord) );
        }else{
          retBuf.append( replacementWord );
        }
        /***************************MCM Impl Team Changes Starts ******************************/
        //if replacement word is hide then the set hide section flag is true.
        if(replacementWord.equals("hide")){
            hideSection=true;
       }
        /***************************MCM Impl Team Changes Endss ******************************/        

      } catch (Exception exc){
        logger.error("Could not find data replacement for: " + replaceableWord);
        //==================================================
        // ALERT:NOTE:
        // remove comment from next line for final release.
        //==================================================
        //throw new Exception("Could not find data replacement for: " + replaceableWord);
        System.out.println("Exception in find data replacement " + replaceableWord);
      }
      //================================================
      // now, the text that we have to process is the
      // remainder of the original text, which starts
      // immediately after replaceable word.
      //================================================
      lProcText = new String(lProcText.substring(lSPos));
    }// end forever loop
    /*******************MCM Impl Team Changes XS_16.5 Starts************************/
    //if hide section is true then delete retBuf buffer content so that this part of the section is not displayed
    if(hideSection)
    {
        retBuf.delete(0,retBuf.length());
    }
    /*******************MCM Impl Team Changes XS_16.5 Ends************************/
    return new String(retBuf);
  }


  protected NodeList extractDataNodes(Document pDoc) throws Exception
  {
    NodeList retVal = pDoc.getDocumentElement().getChildNodes();
    return retVal;
  }

  protected NodeList extractTemplateNodes(Document pDoc) throws Exception
  {
    NodeList lList = pDoc.getDocumentElement().getChildNodes();
    Node lNode;
    for(int i=0; i<lList.getLength(); i++)
    {
      lNode = lList.item(i);
      if(lNode.getNodeName().trim().equalsIgnoreCase("template") )
      {
        return lNode.getChildNodes();
      }
    }
    return null;
  }

  private Document getDataDocument(String parStr ) throws Exception
  {
    StringReader lStrReader ;
    try
    {
      lStrReader = new StringReader(parStr);
    }
    catch(Exception exc)
    {
      logger.error("Could not create string reader - method getDataDocument");
      throw new Exception("ERROR - method getDataDocument(String) - Could not create string reader.");
    }

    Document retVal = null;
    try
    {
      DocumentBuilder docBuilder = XmlToolKit.getInstance().sunDocumentBuilder();
      retVal = docBuilder.parse ( new InputSource(lStrReader) );
      retVal.getDocumentElement().normalize();
    } catch (SAXParseException err) {
      String msg = "** Parsing error document - line : " + err.getLineNumber ();
      logger.error( msg );
      throw new Exception("Error parsing document - line : " + err.getLineNumber ());
    } catch (SAXException e) {
      Exception x = e.getException ();
      ((x == null) ? e : x).printStackTrace ();
      logger.error("Error parsing document SAX exception - method getDataDocument.");
      throw new Exception("Error parsing document SAX exception.");
    } catch (Throwable t) {
      t.printStackTrace ();
      logger.error("Error parsing document Throwable catch - method getDataDocument.");
      throw new Exception("Error parsing document throwable exception.");
    }
    return retVal;
  }


  private Document getTemplateDocument(File parFile) throws Exception
  {
    Document retVal = null;
    try
    {

      if(!parFile.exists())
       logger.debug("Template file:" + parFile.getName() + " does not exist.");

      if(!parFile.canRead() || !parFile.canWrite())
       logger.debug("Serious error with template file:" + parFile.getName() );

      DocumentBuilder docBuilder = XmlToolKit.getInstance().sunDocumentBuilder();

      retVal = docBuilder.parse( parFile );
      retVal.getDocumentElement().normalize();
    } catch (SAXParseException err) {
      logger.error("** Parsing error template - line : " + err.getLineNumber () );
      throw new Exception("Error parsing template - line : " + err.getLineNumber ());
    } catch (SAXException e) {
      Exception x = e.getException ();
      ((x == null) ? e : x).printStackTrace ();
      throw new Exception("Error parsing template SAX exception.");
    } catch (Throwable t) {
      t.printStackTrace ();
      throw new Exception("Error parsing template throwable exception.");
    }
    return retVal;
  }

  protected void  getAllActionsForVar(Node pNode) throws Exception
  {
    NodeList actions;
    Node lActionNode;
    String lActionName= null;
    String lParameter = null;
    String lVariableName, lNumOccurence, lComparisonStr, lSwitchName, lSwitchStatus,  lMonitoredVar;
    actions = pNode.getChildNodes();
    lVariableName = DomUtil.getAttributeValue(pNode,"name");
    NodeList itemList;
    Node itemNode;
    ConditionalAction lConditionalAction;
    for(int i=0; i<actions.getLength();i++){
      lActionNode = actions.item(i);
      if(lActionNode.getNodeName().trim().equalsIgnoreCase("on_occurence")){
        lNumOccurence = DomUtil.getAttributeValue(lActionNode,"number");
        lComparisonStr = DomUtil.getAttributeValue(lActionNode,"comparison");
        lMonitoredVar = DomUtil.getAttributeValue(lActionNode,"monitoredvar");
        itemList = lActionNode.getChildNodes();
        for(int j=0; j<itemList.getLength(); j++){
          itemNode = itemList.item(j);
          if(itemNode.getNodeName().equalsIgnoreCase("actionname")){
            lActionName = DomUtil.getNodeText(itemNode);
          } else if(itemNode.getNodeName().equalsIgnoreCase("param1")) {
            lParameter = DomUtil.getNodeText(itemNode);
          }
        }
        lConditionalAction = new ConditionalAction();
        lConditionalAction.setVarName(lVariableName);
        lConditionalAction.setComparisonExpression(lComparisonStr);
        lConditionalAction.setOccurenceNumber(Integer.parseInt(lNumOccurence));
        lConditionalAction.setActionName(lActionName);
        lConditionalAction.setActionParamOne(lParameter);
        lConditionalAction.setEventType(ConditionalAction.ON_OCCURENCE);
        lConditionalAction.setMonitoredVarName(lMonitoredVar);
        conditionalActionList.addNewAction(lConditionalAction);
      } else if(lActionNode.getNodeName().trim().equalsIgnoreCase("on_switch")){
        lSwitchName = DomUtil.getAttributeValue(lActionNode,"name");
        lSwitchStatus = DomUtil.getAttributeValue(lActionNode,"status");
        itemList = lActionNode.getChildNodes();
        for(int j=0; j<itemList.getLength(); j++){
          itemNode = itemList.item(j);
          if(itemNode.getNodeName().equalsIgnoreCase("actionname")){
            lActionName = DomUtil.getNodeText(itemNode);
          } else if(itemNode.getNodeName().equalsIgnoreCase("param1")) {
            lParameter = DomUtil.getNodeText(itemNode);
          }
        }
        lConditionalAction = new ConditionalAction();
        lConditionalAction.setVarName(lVariableName);
        lConditionalAction.setSwitchName(lSwitchName);
        if(lSwitchStatus.equalsIgnoreCase("ON")){
          lConditionalAction.setSwitchStatus(true);
        } else {
          lConditionalAction.setSwitchStatus(false);
        }
        lConditionalAction.setActionName(lActionName);
        lConditionalAction.setActionParamOne(lParameter);
        lConditionalAction.setEventType(ConditionalAction.ON_SWITCH);
        conditionalActionList.addNewAction(lConditionalAction);
      }
    }
  }

  protected void extractReportConstants(Document pDoc) throws Exception
  {
    reportConstantList = new ReportConstantList();
    NodeList lList = pDoc.getDocumentElement().getChildNodes();
    NodeList lConstantList, lRptConstList;
    Node lNode, lConstNode, lRptTextNode;
    String lName ;
    for(int i=0; i<lList.getLength(); i++){
      lNode = lList.item(i);
      if(!(lNode.getNodeName().trim().equalsIgnoreCase("reportconstants")) ){continue;}
      // lNode contains all report constants
      lConstantList = lNode.getChildNodes();
      for( int j=0; j<lConstantList.getLength(); j++ ){
        lConstNode = lConstantList.item(j);
        if( !(lConstNode.getNodeName().equalsIgnoreCase("reportconstant") ) ){continue;}
        lName = DomUtil.getAttributeValue( lConstNode, "constname" );
        lRptConstList = lConstNode.getChildNodes();
        for(int k=0; k<lRptConstList.getLength(); k++){
          lRptTextNode = lRptConstList.item(k);
          if( !(lRptTextNode.getNodeName().equalsIgnoreCase("consttext")) ){continue;}
          String lText = new String("");
          lText = DomUtil.getNodeText(lRptTextNode);
          ReportConstant lRptConst = new ReportConstant(lName, lText);
          reportConstantList.addNewConstant(lRptConst);
          break;  // there should be only one text for report constant
        } // end for (k)
      } // end for (j)
    } // end for (i)
  }

  protected void extractConditionalActions(Document pDoc) throws Exception
  {
    conditionalActionList = new ConditionalActionList();
    NodeList lList = pDoc.getDocumentElement().getChildNodes();
    NodeList lConditionList;
    Node lNode, lActionNode;
    for(int i=0; i<lList.getLength(); i++){
      lNode = lList.item(i);
      if(!(lNode.getNodeName().trim().equalsIgnoreCase("conditionalactions")) ){continue;}
      lConditionList = lNode.getChildNodes();
      for(int j=0; j<lConditionList.getLength();j++){
        lActionNode = lConditionList.item(j);
        if(  !(lActionNode.getNodeName().trim().equalsIgnoreCase("conditionalvar"))  ){continue;}
        getAllActionsForVar(lActionNode);
      }
    }
  }

  protected ReportPortions extractTemplatePortions(Document pDoc) throws Exception
  {
    ReportPortions retVal = new ReportPortions();
    NodeList lList = pDoc.getDocumentElement().getChildNodes();
    NodeList portionList ;
    Node lNode, portionNode;
    String tmpText, tmpKey;
    for(int i=0; i<lList.getLength(); i++)
    {
      lNode = lList.item(i);
      if( ! (lNode.getNodeName().trim().equalsIgnoreCase("templateportions")) ) {continue;}
      portionList = lNode.getChildNodes();
      for(int j=0; j<portionList.getLength(); j++ ){
        portionNode = portionList.item(j);
        if(!(portionNode.getNodeName().trim().equalsIgnoreCase("templatePortion"))){continue;}
        tmpText = DomUtil.getNodeText(portionNode);
        tmpKey = DomUtil.getAttributeValue(portionNode,"idcode");
        if( tmpKey == null){
          logger.error("ERROR - method extract template portions - missing TEXT for template portion : " + tmpKey);
          throw new Exception("ERROR - missing key for template portion !!! ");
        }
        if( tmpText == null){
          logger.error("ERROR - method extract template portions - missing TEXT for template portion with key : " + tmpKey);
          throw new Exception("ERROR - text is missing for template portion " + tmpKey + " !!!");
        }
        retVal.addTemplatePortion( tmpKey, tmpText  );
      }
    }
    return retVal;
  }

  //==================================================
  // for test purposes
  //==================================================
  public static void main(String[] args)
  {
    try{
      ReportCreator lReportCreator = new ReportCreator();
      //File templateFile = new File("d:\\rtftests\\templates\\production\\commitmentMCAP.rtf");
      File templateFile = new File("d:\\rtftests\\templates\\production\\commitmentGENX.rtf");
      File dataFile= new File("d:\\mosproject\\admin\\docgen\\Commitment Letter MCAP Mortgage - Englishlish.xml");
      StringWriter strWr = new StringWriter();
      FileReader fr = new FileReader(dataFile);
      LineNumberReader lnr = new LineNumberReader(fr);
      String oneLine ;
      for(;;){
         oneLine = lnr.readLine();
         if(oneLine == null){
            break;
         }
         strWr.write(oneLine.trim());
      }
      lnr.close();
      fr.close();

      String dataAsStr = strWr.toString();
      StringBuffer retBuff = lReportCreator.makeRTFReport(dataAsStr,templateFile);
      String outStr = new String(retBuff);
      System.out.println(outStr);
      FileWriter fwr = new FileWriter("d:\\rtftests\\templates\\output\\commitmentGENX.xml");
      fwr.write(outStr);
      fwr.flush();
      fwr.close();
    } catch (Exception exc){
      System.out.println("Method main report creator error");
    }
  }

  private String cleanSpecialChars(String input)
  {
    StringBuffer retBuf = new StringBuffer("");
    for(int x=0; x<input.length(); x++)
    {
      char c = input.charAt(x);
      // Convert Return Chars
      if((c == '�') || (c == 10))
      {
        retBuf.append("\\par ");
      }
      else if( (c == '\'') )
      {
        retBuf.append("\\rquote ");
      }else if((c == ' '))
      {
        retBuf.append("\\~ ")  ;
      }
      else if(!Character.isISOControl(c))
      {
        retBuf.append(c);
      }
    }
    return retBuf.toString() ;
  }

  private void switchCharacterReplacement(){
    if(CHAR_REPLACEMENTS_SWITCH){
      CHAR_REPLACEMENTS_SWITCH = false;
      return;
    }else {
      CHAR_REPLACEMENTS_SWITCH = true;
      return;
    }
  }

}
//==============================================================================
//  END OF CLASS
//==============================================================================
/*
  private Document getDataDocument(File parFile) throws Exception
  {
    Document retVal = null;
    try
    {
      DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
      retVal = docBuilder.parse ( parFile );
      retVal.getDocumentElement().normalize();
    }
    catch (SAXParseException err)
    {
      String msg = "** Parsing error document - line : " + err.getLineNumber ();
      System.out.println ( msg );
      System.out.println( err.getMessage());
      throw new Exception("Error parsing document - line : " + err.getLineNumber ());
    } catch (SAXException e) {
      Exception x = e.getException ();
      System.out.println(e.getMessage());
      ((x == null) ? e : x).printStackTrace ();
      throw new Exception("Error parsing document SAX exception.");
    } catch (Throwable t) {
      t.printStackTrace ();
      throw new Exception("Error parsing document throwable exception.");
    }
    return retVal;
  }
*/
/*
  public void makeRTFReport(File pDataFile, File pTemplateFile, File pOutputFile) throws Exception
  {
    Document lDataDoc, lTemplateDoc;
    lDataDoc = getDataDocument(pDataFile);
    lTemplateDoc = getTemplateDocument(pTemplateFile);
    setTemplateFile(pTemplateFile);
    NodeList lDataList, lTemplateList;
    ReportPortions lReportPortions;
    lDataList = extractDataNodes(lDataDoc);
    lTemplateList = extractTemplateNodes(lTemplateDoc);
    lReportPortions = extractTemplatePortions(lTemplateDoc);
    String retStr = new String(mergeDataAndTpl(lDataList, lTemplateList,lReportPortions ));
    //File dataFile = new File("d:\\rtftests\\templates\\output\\zzzout.rtf");
    FileWriter fwr = new FileWriter(pOutputFile);
    fwr.write(retStr);
    fwr.close();
  }

*/
/*
  private Node findDocSectionTplById(NodeList pNodeList, String pId){
    Node retVal;
    for(int i=0; i<pNodeList.getLength();i++){
      retVal = pNodeList.item(i);
      if(retVal.getNodeName().equalsIgnoreCase(C_DOC_SECTION) && DomUtil.getAttributeValue(retVal,C_ID_CODE).equalsIgnoreCase(pId) ){
        return retVal;
      }
    }
    return null;
  }
*/







