package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import MosSystem.Mc;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;



public class BridgeLoanAmount extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    double amt = 0.0;
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
         Deal deal = (Deal)de;
         Bridge br = deal.getBridge();

         if(br != null)
         {
           return extract(br, lang, format, srk);
         }

       }
       else if(clsid == ClassId.BRIDGE)
       {
         amt = ((Bridge)de).getNetLoanAmount();
       }

      fml.addCurrency(amt);
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
