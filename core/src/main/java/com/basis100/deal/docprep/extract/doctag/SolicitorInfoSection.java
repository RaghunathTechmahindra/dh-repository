package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class SolicitorInfoSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity dl, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Deal deal = (Deal)dl;
    FMLQueryResult fml = new FMLQueryResult();


    try
    {

       PartyProfile pp = new PartyProfile(srk);
       pp.setSilentMode(true);
       pp = pp.findByDealSolicitor(deal.getDealId());
       fml.addValue(pp);

    }
    catch(NullPointerException npe)
    {

       return fml;
    }
    catch(FinderException fe)
    {

       return fml;
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }


    return fml;
  }
}
