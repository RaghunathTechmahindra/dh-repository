package com.basis100.deal.docprep.extract.data;

/**
 * 24/Jan/2008 DVG #DG688 LEN217161: Chantal Blanchard MI application fee from AIG does not display in SOLPAK 
 * 30/Oct/2006 DVG #DG534 #5139  CTFS CR 19 MI Premium on Conventional Apps
 * 04/May/2006 SL         #2819  CMHC fee missing from commitment
 * 09/Nov/2005 DVG #DG358 #2399  Condition 705 on Broker outstanding conditions template
 * 24/Oct/2005 DVG #DG344 #2286  CR#132 Solicitors Package changes
  #1670  Concentra - CR #158 (Solicitors package)
  - total conversion from the xml to the xsl format
 * 13/Jul/2005 DVG #DG258 #1793  Add Fee Tag to next build  #1791
*/

import java.math.BigDecimal;
import java.util.*;

import org.w3c.dom.*;

import MosSystem.Mc;

import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.*;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.collections.BorrowerTypeComparator;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;

/**
 * <PRE>
 * Base class for all GENX document extractors.  It contains common extraction logic.
 * Copyright (c) 2002 Basis100, Inc
 * </PRE>
 * @author       John Schisler
 * @version 1.0
 * change log:
 * @author MCM Impl Team
 * @version 1.1 11-July-2008 Added new methods for extracting the ProductType() and UnderWriteAs() 
 * 
 *
 */
public abstract class GENXExtractor extends XSLExtractor
{
    //  List of Borrowers so that we don't have to keep accessing database
    private List bList = null;

    //  Primary borrower
    private Borrower pBorrower = null;

    //  Primary property
    private Property pProperty = null;

    //  Total Fees
    protected double totalFees = 0d;

    //  Whether or not to keep a total of the fees
    protected boolean bKeepFeeTotal = false;

    protected abstract void buildXMLDataDoc() throws Exception;

    public GENXExtractor(){}

    /**
     * Constructor used to initialize data.  This is used in cases where
     * one extractor needs another to build upon its data.  In order to built
     * the correct deal, format, lang, etc, I just made it so you pass any object
     * that inherits from XSLExtractor and it sets up the new object's data.  The doc is
     * included too so that you won't get any exceptions thrown due to multiple documents
     * trying to add nodes to a common tree.
     *
     * @param x object used to set this object's key data items.
     */
    public GENXExtractor(XSLExtractor x)
    {
        deal = x.deal;
        lang = x.lang;
        format = x.format;
        srk = x.srk;
        doc = x.doc;
    }

    /**
     * The main exposed method that is called when a client desires the given document
     * type to be created.  Since the same logic is executed by each child class, the
     * logic was placed here.  This method calls buildXMLDataDoc(), which is
     * abstract and must be overloaded by the child class to handle how the document
     * is actually created.
     *
     */
    public String extractXMLData(DealEntity de,
                             int pLang,
                             DocumentFormat pFormat,
                             SessionResourceKit pSrk) throws Exception
    {
        deal = (Deal) de;
        lang = pLang;
        format = pFormat;
        srk = pSrk;

        buildXMLDataDoc();
        XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

        //Venkata, Aug 22, 2007 - Setting the encoding to ISO-8859-1 always to avoid
        // exceptions during transformation, because of some extra characters during user input
        /*if (lang == Mc.LANGUAGE_PREFERENCE_FRENCH){
           xmlwr.setEncoding("iso-8859-1"); }
        else {
        	xmlwr.setEncoding("UTF-8");
         }
         */
        xmlwr.setEncoding("iso-8859-1");
        return xmlwr.printString(doc);
    }


    /**
     * Retrieves the <code>LenderToBranchAssoc</code> object based on
     * <code>branchid</code> and <code>lenderid</code>.  If none or more than
     * one <code>LenderToBranchAssoc</code> object is found, <code>null</code>
     * is returned.
     *
     * @param branchid The Branch ID to use for the query.
     * @param lenderid The Lender ID to use for the query.
     *
     * @return The found <code>LenderToBranchAssoc</code> object or null if
     *         data was not found that matched the inputs.
     */
    protected LenderToBranchAssoc getLBA(int branchid, int lenderid)
    {
        Collection c = null;

        try
        {
             c = new LenderToBranchAssoc(srk).findByBranchAndLender(
                     new BranchProfilePK(branchid),
                     new LenderProfilePK(lenderid));
        }
        catch (Exception e) {}

        //  We only expect 1 object back in the collection,
        //  so we return null if more than 1 item exists.
        //  TODO:  Verify that this is correct logic.
        if (c == null || c.size() != 1)
           return null;
        else
           return (LenderToBranchAssoc)c.iterator().next();//return first element in collection
    }

    /**
     *  Passthrough that passes null for the tag name, meaning use the default value.
     *
     *  @see #extractBranchPhone(String, boolean, String)
     *  @param prefix any prefix to place before the formatted phone number.
     *  @param bTollFree Specifies whether or not to use the toll free phone number.
     *  @return The completed Branch Phone Number XML tag.
     *
     */
    protected Node extractBranchPhone(String prefix, boolean bTollFree)
    {
        return extractBranchPhone(prefix, bTollFree, null);
    }

     /**
     * Retrieves the Branch Phone Number from the deal and formats an XML tag with it.
     * Depending on the document language preference, the English or French phone number
     * is used.
     *
     * @param prefix any prefix to place before the formatted phone number.
     * @param bTollFree Specifies whether or not to use the toll free phone number.
     * @return The completed Branch Phone Number XML tag.
     *
     */
    protected Node extractBranchPhone(String prefix, boolean bTollFree, String tagName)
    {
        Node elemNode = null;

        String phone = null;

        LenderToBranchAssoc ltba = getLBA (deal.getBranchProfileId(),
                                           deal.getLenderProfileId());

        if (ltba != null)
        {
            try
            {
                if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH)
                   phone = (bTollFree) ?
                           StringUtil.getAsFormattedPhoneNumber(ltba.getETPhoneNumber(), null) :
                           StringUtil.getAsFormattedPhoneNumber(ltba.getELPhoneNumber(), null);
                else
                   phone = (bTollFree) ?
                           StringUtil.getAsFormattedPhoneNumber(ltba.getFTPhoneNumber(), null) :
                           StringUtil.getAsFormattedPhoneNumber(ltba.getFLPhoneNumber(), null);
            }
            catch (Exception e)
            {
                return elemNode;
            }
        }

        if (phone != null && phone.trim().length() > 0)
        {
            if (tagName == null)
               elemNode = (bTollFree) ? doc.createElement("BranchTollPhone") :
                                        doc.createElement("BranchPhone");
            else
               elemNode = doc.createElement(tagName);

            if (prefix != null && prefix.trim().length() > 0)
               phone = prefix + phone;

            elemNode.appendChild(doc.createTextNode(phone));
        }

        return elemNode;
    }

    protected Node extractBranchFax(String prefix, boolean bTollFree)
    {
        return extractBranchFax(prefix, bTollFree, null);
    }

     /**
     * Retrieves the Branch Fax Number from the deal and formats an XML tag with it.
     * Depending on the document language preference, the English or French fax number
     * is used.
     *
     * @param prefix any prefix to place before the formatted fax number.
     * @param bTollFree Specifies whether or not to use the toll free fax number.
     * @return The completed Branch Fax Number XML tag.
     *
     */
    protected Node extractBranchFax(String prefix, boolean bTollFree, String tagName)
    {
        Node elemNode = null;

        String fax = null;

        LenderToBranchAssoc ltba = getLBA (deal.getBranchProfileId(),
                                           deal.getLenderProfileId());

        if (ltba != null)
        {
            try
            {
                if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH)
                   fax =  (bTollFree) ?
                          StringUtil.getAsFormattedPhoneNumber(ltba.getETFaxNumber(), null) :
                          StringUtil.getAsFormattedPhoneNumber(ltba.getELFaxNumber(), null);
                else
                   fax = (bTollFree) ?
                          StringUtil.getAsFormattedPhoneNumber(ltba.getFTFaxNumber(), null) :
                          StringUtil.getAsFormattedPhoneNumber(ltba.getFLFaxNumber(), null);
            }
            catch (Exception e)
            {
                return elemNode;
            }
        }

        if (fax != null && fax.trim().length() > 0)
        {
            if (tagName == null)
               elemNode = (bTollFree) ? doc.createElement("BranchTollFax") :
                                        doc.createElement("BranchFax");
            else
               elemNode = doc.createElement(tagName);


            if (prefix != null && prefix.trim().length() > 0)
               fax = prefix + fax;

            elemNode.appendChild(doc.createTextNode(fax));
        }

        return elemNode;
    }

    /**
     * Retrieves the Client Name from the deal and formats an XML tag with it.
     *
     * @return The completed Client Name XML tag.
     *
     */
    protected Node extractClientName()
    {
        Borrower b = getPrimaryBorrower();

        if (b == null)
           return null;

        String val = null;

        if (!isEmpty(b.getBorrowerFirstName()) && !isEmpty(b.getBorrowerLastName()))
        {
            val = new String();

            if (!isEmpty(b.getBorrowerFirstName()))
               val += b.getBorrowerFirstName() + format.space();

            if (!isEmpty(b.getBorrowerLastName()))
               val += b.getBorrowerLastName();
        }

        return getTag("ClientName", val);
    }

    /**
     * Returns the primary borrower for the current deal.
     *
     * @return The primary <code>Borrower</code>.
     *
     */
    protected Borrower getPrimaryBorrower()
    {
        if (pBorrower == null)
        {
            try
            {
                pBorrower = new Borrower(srk,null);
                pBorrower = pBorrower.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
            }
            catch (Exception e) {}
        }

        return pBorrower;
    }

    /**
     * Returns the current address for <code>b</code>.
     *
     * @return The current address.
     *
     */
    protected BorrowerAddress getCurrentAddressForBorrower(Borrower b)
    {
        if (b == null)
           return null;

        try
        {
            BorrowerAddress ba = new BorrowerAddress(srk);

            ba.findByCurrentAddress(b.getBorrowerId(), b.getCopyId());

            return ba;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * A passthrough method that calls extractLenderName() with
     * a value of false, meaning that we don't want a short lender name.
     *
     * @return The completed Lender Name XML tag.
     */
    protected Node extractLenderName()
    {
        return extractLenderName(false);
    }

    /**
     * Retrieves the lender name from the deal and formats an XML tag with it.
     *
     * @param bShortName Specifies whether or not to format the XML tag with the
     *                   short version of the lender name.
     * @return The completed Lender Name XML tag.
     *
     */
    protected Node extractLenderName(boolean bShortName)
    {
        Node elemNode = (bShortName) ?
                             doc.createElement("LenderNameShort") :
                             doc.createElement("LenderName");
        Node textNode;
        LenderProfile lp;
        String name = "";

        try
        {
            lp = new LenderProfile(srk, deal.getLenderProfileId());
            //name = bShortName ? lp.getLPShortName() : lp.getLenderName();
            // call BX resources for lender long name.
            name = bShortName ? lp.getLPShortName() : BXResources.getPickListDescription(deal.getInstitutionProfileId(), "LenderProfile", deal.getLenderProfileId(), lang);

            if (name != null)
               name = name.trim();
        }
        catch(Exception exc)
        {
            return elemNode;
        }

        textNode = doc.createTextNode(name);

        elemNode.appendChild(textNode);
        return elemNode;
    }

    protected Node extractLenderProfileId()
    {
      return getTag("LenderProfileId", "" + deal.getLenderProfileId() );
    }

    protected Node extractAddressSection(Addr addr, String tagName)
    {
        Node elemNode = null;

        try
        {
            if (addr != null)
            {
                elemNode = doc.createElement(tagName);

                if (!isEmpty(addr.getAddressLine1()))
                    addTo(elemNode, "Line1", addr.getAddressLine1());

                if (!isEmpty(addr.getAddressLine2()))
                    addTo(elemNode, "Line2", addr.getAddressLine2());

                if (!isEmpty(addr.getCity()))
                    addTo(elemNode, "City", addr.getCity());

                Province prov = new Province(srk, addr.getProvinceId());

                addTo(elemNode, "Province", prov.getProvinceName());

                addTo(elemNode, extractPostalCode(addr, "Postal"));
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    /**
     * Returns a branch address section XML tag for the current deal.
     *
     * @return the completed branch address section tag.
     *
     */
    protected Node extractBranchAddressSection()
    {
        Node elemNode = null;

        try
        {
            BranchProfile b = new BranchProfile(srk, deal.getBranchProfileId());
            Addr addr = b.getContact().getAddr();

            elemNode = extractAddressSection(addr, "BranchAddress");
        }
        catch (Exception e){}

        return elemNode;
    }

    /**
     * Retrieves the current time from where the database is running.
     * For the most part, this would be equivalent to
     * <code>new java.util.Date()<code>.
     */
    protected String getBranchCurrentTime()
    {
        int bid = deal.getBranchProfileId();
        String val = null;

        try
        {
            BranchProfile bp = new BranchProfile(srk,bid);

            int tzid = bp.getTimeZoneEntryId();

            TimeZoneEntry tze = new TimeZoneEntry(srk,tzid);
            String timeZoneSysId = tze.getTzeSysId();

            TimeZone tz = TimeZone.getTimeZone(timeZoneSysId);

            Calendar cal = Calendar.getInstance(tz);

            int hrs = cal.get(Calendar.HOUR_OF_DAY);
            int min = cal.get(Calendar.MINUTE);
            int sec = cal.get(Calendar.SECOND);
            int ampm = cal.get(Calendar.AM_PM);

            String strHours = String.valueOf(hrs);
            String strMin = String.valueOf(min);
            String strSec = String.valueOf(sec);

            val = (hrs < 10 ? "0" + strHours : strHours) + ":" +
                  (min < 10 ? "0" + strMin : strMin) + ":" +
                  (sec < 10 ? "0" + strSec : strSec) + " " ;

            val += (ampm == 0) ? "AM" : "PM";
        }
        catch (Exception e) {}

        return val;
    }

    /**
     * Retrieves the Current Time at the deal's Branch and formats an XML tag with it.
     *
     * @return The completed Branch Current Time XML tag.
     *
     */
    protected Node extractBranchCurrentTime()
    {
        String val = null;

        try
        {
            val = getBranchCurrentTime();
        }
        catch (Exception e){}

        return getTag("BranchCurrentTime", val);
    }

    /**
     * Retrieves the Client Address information from the deal and formats an XML tag with it.
     *
     * @return A complete client adress section XML tag.
     *
     */
    protected Node extractClientAddressSection()
    {
        Node elemNode = null;

        try
        {

            BorrowerAddress ba = getCurrentAddressForBorrower(getPrimaryBorrower());
            Addr addr = ba.getAddr();

            elemNode = this.extractAddressSection(addr, "ClientAddress");
        }
        catch (Exception e){}

        return elemNode;
    }

    //#DG534 rewritten
    /**
     * Retrieves and calculates deal fees and formats an XML tag with it.
     *
     * @return Node The completed fees XML tag.
     */
    protected Node extractFees() {
      Node retNode = null;

      //int payor = deal.getMIPayorId();
      //boolean MIUpfront = deal.getMIUpfront() != null && deal.getMIUpfront().equalsIgnoreCase("Y");

      try
      {
        retNode = getFees();
      }
      catch(Exception exc)
      {
        return null;
      }

      // #2232, 2228 GE, Catherine, 7-Oct-05 --- begin ------------------------
      // remove this as per Derek's request
      /*
        //==========================================================================
        // now the second part which has to be appended to the previous node
        //==========================================================================
        try
        {
            if(payor == Mc.MI_PAYOR_BORROWER && deal.getMIPremiumAmount() > 0)
            {
                String payStr = "Take from First Advance";

                if(MIUpfront )
                {
                    payStr = "Cheque";
                }

                ConditionHandler ch = new ConditionHandler(srk);
                String vv = ch.getVariableVerbiage(deal, Dc.PST_VERB, lang);

                if (!isEmpty(vv))
                {
                    feeVerbStr = vv + format.space() + "(" + format.space()
                                 + payStr +
                                 format.space() + ")" + format.space();
                }
                else
                    feeVerbStr = "";

                dealFeeNode = doc.createElement("Fee");

                addTo(dealFeeNode, "FeeVerb", feeVerbStr);

                feeAmt = deal.getMIPremiumPST();
                feeAmountStr = formatCurrency(feeAmt);
                addTo(dealFeeNode, "FeeAmount", feeAmountStr);

                if (bKeepFeeTotal)
                   totalFees += feeAmt;

                if (retNode == null)
                   retNode = doc.createElement("Fees");

                retNode.appendChild(dealFeeNode);
            }
        }
        catch(Exception exc1){}
*/
// #2232, GE, Catherine, 7-Oct-05 --- end ------------------------
       return retNode;
    }

    /**
     *  scan deal fees looking for qualifying ones
     * @return Node
     * @throws Exception
     */
    private Node getFees() throws Exception {
      Node retNode = doc.createElement("Fees");
      int payor = deal.getMIPayorId();
      boolean MIUpfront = deal.getMIUpfront() != null && deal.getMIUpfront().equalsIgnoreCase("Y");

      Iterator it = deal.getDealFees().iterator();

      while(it.hasNext())
      {
        DealFee dealFee = (DealFee) it.next();
        double feeAmt = dealFee.getFeeAmount();
        int feeStatusId = dealFee.getFeeStatusId();
        Fee fee = dealFee.getFee();

        // #2232, #2228 GE, Catherine, 7-Oct-05 --- begin ------------------------
        // we have two PAYABLEINDICATOR fields: one on Fee table and one on DealFee table
        // DealFee.PAYABLEINDICATOR is null for some reason (?)
        // the code should check Fee.PAYABLEINDICATOR instead
        boolean isPayable = fee.getPayableIndicator();
        int feeType = fee.getFeeTypeId();

        /*
         * Modifying the fee status to include waived status, remove the not condition and
         * adding new fee types as part of QC Ticket # 142
         */
        //also see Ticket 2819
        /*if (isPayable || feeAmt == 0d                 // #2232, GE, Catherine, 7-Oct-05 --- end ------------------------
            ||  (feeStatusId == Mc.FEE_STATUS_REQUIRED || feeStatusId == Mc.FEE_STATUS_RECIEVED
            		|| feeStatusId == Mc.FEE_STATUS_WAIVED)
            || ( MIUpfront && payor == Mc.MI_PAYOR_LENDER
                 && (feeType == Mc.FEE_TYPE_CMHC_PREMIUM
                 || feeType == Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE
                 || feeType == Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM
                 || feeType == Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE
                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_FEE
                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE
                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PREMIUM
                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE
                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM
                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE
                 || feeType == Mc.FEE_TYPE_CMHC_FEE
                 || feeType == Mc.FEE_TYPE_CMHC_FEE_PAYABLE
                 || feeType == Mc.FEE_TYPE_AIG_UG_FEE
                 || feeType == Mc.FEE_TYPE_AIG_UG_FEE_PAYABLE
                 || feeType == Mc.FEE_TYPE_AIG_UG_PREMIUM
                 || feeType == Mc.FEE_TYPE_AIG_UG_PREMIUM_PAYABLE
                 || feeType == Mc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM
                 || feeType == Mc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM_PAYABLE))
            )*/
        /*
         * Modifying the fee status to include waived status, remove the not condition and
         * adding new fee types as part of QC Ticket # 142
         */
        	if (isPayable ||  (feeStatusId == Mc.FEE_STATUS_REQUIRED || feeStatusId == Mc.FEE_STATUS_RECIEVED)
	            || (payor == Mc.MI_PAYOR_LENDER
	                 && (feeType == Mc.FEE_TYPE_CMHC_FEE //3
	                 || feeType == Mc.FEE_TYPE_CMHC_PREMIUM //11
	                 || feeType == Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM //13
	                 || feeType == Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE //29
	                 || feeType == Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE //28
	                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_FEE //15
	                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PREMIUM //17 
	                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM //19
	                 || feeType == Mc.FEE_TYPE_AIG_UG_FEE //37
	                 || feeType == Mc.FEE_TYPE_AIG_UG_PREMIUM //39
	                 || feeType == Mc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM)) //41
            )
        
          continue;

        Node dealFeeNode = doc.createElement("Fee");
        String type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", feeType , lang);
        String fpm = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeePaymentMethod", dealFee.getFeePaymentMethodId() , lang);

        String feeVerbStr = type + format.space() + "(" + fpm + ")" + format.space();
        addTo(dealFeeNode, "FeeVerb", feeVerbStr);

        addTo(dealFeeNode, "FeeAmount", formatCurrency(feeAmt));

        if (bKeepFeeTotal)
          totalFees += feeAmt;

        retNode.appendChild(dealFeeNode);
      } // end while

      return retNode.hasChildNodes() ? retNode: null;
    }

    /**
     * Utility method that formats <code>amt</code> using the current doc language
     * settings.
     *
     * @param amt number to format into the relevant currency style
     *
     * @return The formatted number.
     */
    protected String formatCurrency(double amt)
    {
        return DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang);
    }

    /**
     * Utility method that formats <code>rate</code> using the current doc language.
     * settings.
     *
     * @param amt number to format into the relevant rate style
     *
     * @return The formatted number.
     */
    protected String formatInterestRate(double rate)
    {
        return DocPrepLanguage.getInstance().getFormattedInterestRate(rate, lang);
    }

    /* Changes TAR # 1612 - Start*/
    /**
     * getFormattedInterestRateWithoutPercentage: Returns formatted String containing the interest rate with
     * four places after the decimal. A call is made to the getFormattedInterestRate()
     * of DocPrepLangauge.
     * 
     * @param rate -    String that contains the Interest Rate that needs to be formatted.
     * @return String - Formatted interest rate with four places after decimal.
     */
    
    protected String getFormattedInterestRateWithoutPercentage(double rate)
    {
    	return DocPrepLanguage.getInstance().getFormattedInterestRateWithoutPercentage(rate, lang);    	
    }
    /* Changes TAR # 1612 - End*/
    
    /**
     * Utility method that formats <code>rate</code> using the current doc language.
     * settings.
     *
     * @param amt number to format into the relevant rate style
     *
     * @return The formatted number.
     */
    protected String formatRatio(double rate)
    {
        return DocPrepLanguage.getInstance().getFormattedRatio(rate, lang);
    }

    /**
     * Utility method that formats <code>date</code> using the current doc language.
     * settings.
     *
     * @param date the date to format into the relevant date style
     *
     * @return The formatted date.
     */
    protected String formatDate(Date date)
    {
        try
        {
            return DocPrepLanguage.getInstance().getFormattedDate(date, lang);
        }
        catch (Exception e) {}

        return null;
    }

    /**
     * Retrieves a list of borrowers on the current deal and returns it.
     * On the first call, the borrower list is stored into a class-level variable.
     * This allows subsequent calls to not have to access the database.
     *
     * @return The borrowers on the current deal.
     */
    protected List getBorrowers()
    {
        if (bList == null)
        {
            try
            {
                bList = (List)deal.getBorrowers();
            }
            catch (Exception e){}
        }

        //  Doing this provides a read-only original list
        return new ArrayList(bList);
    }

    /**
     * Retrieves the primary property on the current deal and returns it.
     * On the first call, the primary property is stored into a class-level variable.
     * This allows subsequent calls to not have to access the database.
     *
     * @return The primary property on the current deal.
     */
    protected Property getPrimaryProperty()
    {
        if (pProperty == null)
        {
            try
            {
                pProperty = new Property(srk,null);

                pProperty = pProperty.findByPrimaryProperty(deal
                    .getDealId(), deal.getCopyId(), deal.getInstitutionProfileId());
            }
            catch (Exception e) {}
        }

        return pProperty;
    }

    protected Node extractApplicationId()
    {
        return extractApplicationId("ApplicationId");
    }

    /**
     * Retrieves the Application Id from the deal and formats an XML tag with it.
     *
     * @return The completed Deal Num XML tag.
     *
     */
    protected Node extractApplicationId(String tagName)
    {
        String val = null;

        try
        {
            val = deal.getApplicationId();
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    /**
     * Retrieves the Deal Type from the deal and formats an XML tag with it.
     *
     * @return The completed Deal Type XML tag.
     *
     */
    protected Node extractDealType()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("DealType", deal.getDealTypeId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DealType", deal.getDealTypeId() , lang);
        }
        catch (Exception e){}

        return getTag("DealType", val);
    }

    /**
     * Retrieves and formats the Deal Purpose from the deal and
     * formats an XML tag with it.
     *
     * @return The completed Deal Purpose XML tag.
     *
     */
    protected Node extractDealPurpose()
    {
        String val = null;

        try
        {
            //  TODO:  How do I get this into another language?
            //val = PicklistData.getDescription("DealPurpose", deal.getDealPurposeId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "DealPurpose", deal.getDealPurposeId() , lang);
        }
        catch (Exception e){}

        return getTag("DealPurpose", val);
    }

    /**
     * Retrieves the Actual Closing Date from the deal and formats an XML tag with it.
     *
     * @return The completed Estimated Closing Date XML tag.
     *
     */
    protected Node extractActualClosingDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getActualClosingDate());
        }
        catch (Exception e){}

        return getTag("ActualClosingDate", val);
    }

    /**
     * Retrieves the Estimated Closing Date from the deal and formats an XML tag with it.
     *
     * @return The completed Estimated Closing Date XML tag.
     *
     */
    protected Node extractEstClosingDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getEstimatedClosingDate());
        }
        catch (Exception e){}

        return getTag("EstClosingDate", val);
    }

    /**
     * Retrieves the purchase price for the primary property on the deal
     * and formats an XML tag with it.
     *
     * @return The completed purchase price XML tag.
     *
     */
    protected Node extractPurchasePrice()
    {
        return extractPurchasePrice(getPrimaryProperty());
    }

    /**
     * Retrieves the purchase price for <code>p</code> and formats an XML tag with it.
     *
     * @return The completed purchase price XML tag.
     *
     */
    protected Node extractPurchasePrice(Property p)
    {
        String val = null;

        try
        {
            val = formatCurrency(p.getPurchasePrice());
        }
        catch (Exception e){}

        return getTag("PurchasePrice", val);
    }

    protected Node extractTotalLoanAmount()
    {
        return extractTotalLoanAmount("TotalLoanAmount");
    }

    /**
     * Retrieves the total loan amount on the deal and formats an XML tag with it.
     *
     * @return The completed total loan amount XML tag.
     *
     */
    protected Node extractTotalLoanAmount(String tagName)
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getTotalLoanAmount());
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractAmortization()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getYearsAndMonths(deal.getAmortizationTerm(), lang);
        }
        catch (Exception e){}

        return getTag("Amortization", val);
    }

    /**
     * Retrieves the effective amortization for the deal and formats an XML tag with it.
     *
     * @param tagName the tag name to be used in the XML tag generation.
     * @return The completed amortization XML tag.
     *
     */
    protected Node extractEffectiveAmortization()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getYearsAndMonths(
                    deal.getEffectiveAmortizationMonths(), lang);
        }
        catch (Exception e){}

        return getTag("EffectiveAmortization", val);
    }

    /**
     * Retrieves the payment frequency for the deal and formats an XML tag with it.
     *
     * @return The completed payment frequency XML tag.
     *
     */
    protected Node extractPaymentFrequency()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("PaymentFrequency", deal.getPaymentFrequencyId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency", deal.getPaymentFrequencyId() , lang);

            //val = DocPrepLanguage.getInstance().getTerm(val, lang);
        }
        catch (Exception e){}

        return getTag("PaymentFrequency", val);
    }

    /**
     * Retrieves the LTV for the deal and formats an XML tag with it.
     *
     * @return The completed LTV XML tag.
     *
     */
    protected Node extractLTV()
    {
        String val = null;

        try
        {
            val = formatRatio(deal.getCombinedLTV());
        }
        catch (Exception e){}

        return getTag("LTV", val);
    }

    protected Node extractFirstPaymentDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getFirstPaymentDate());
        }
        catch (Exception e){}

        return getTag("FirstPaymentDate", val);
    }

    protected Node extractFirstPaymentDateMonthly()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getFirstPaymentDateMonthly());
        }
        catch (Exception e){}

        return getTag("FirstPaymentDateMonthly", val);
    }

    protected Node extractMaturityDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getMaturityDate());
        }
        catch (Exception e){}

        return getTag("MaturityDate", val);
    }

    protected Node extractPrivilege()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);

            switch (deal.getPrivilegePaymentId())
            {
                case Mc.PRIVILEGE_PAYMENT_OPEN:
                     val = ch.getVariableVerbiage(deal, Dc.PRIVILEGE_OPEN, lang);
                     break;

                case Mc.PRIVILEGE_PAYMENT_NOT_ALLOWED:
                     val = ch.getVariableVerbiage(deal, Dc.PRIVILEGE_NOT_ALLOWED, lang);
                     break;

                default:
                     val = ch.getVariableVerbiage(deal, Dc.PRIVILEGE, lang);
                     break;
            }
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("Privilege"), val));
    }

    /**
     * Retrieves the Source Name from the deal and formats an XML tag with it.
     *
     * @return The completed Source Name XML tag.
     *
     */
    protected Node extractSourceName()
    {
        String val = null;

        try
        {
            SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();

            if (sobp.getContact() != null)
            {
                Contact c = sobp.getContact();

                if (!isEmpty(c.getContactFirstName()) &&
                    !isEmpty(c.getContactLastName()))
                {
                    val = new String();

                    if (!isEmpty(c.getContactFirstName()))
                       val += c.getContactFirstName() + format.space();

                    if (!isEmpty(c.getContactLastName()))
                       val += c.getContactLastName();
                }
            }
        }
        catch (Exception e){}

        return getTag("SourceName", val);
    }

    /**
     * Retrieves the Reference Source App Num from the deal and formats an XML tag with it.
     *
     * @return The completed Reference Source App Num XML tag.
     *
     */
    protected Node extractReferenceSourceAppNum()
    {
        String val = null;

        try
        {
            val = deal.getSourceApplicationId();
        }
        catch (Exception e){}

        return getTag("ReferenceSourceAppNum", val);
    }

    /**
     * Calls <code>extractPostalCode(Addr, String)</code> with a tag name of "PostalCode"
     *
     * @param address the address to extract the postal code information from.
     * @return The completed postal code XML tag.
     */
    protected Node extractPostalCode(Addr address)
    {
        return extractPostalCode(address, "PostalCode");
    }

    /**
     * Creates a postal code XML tag with a name of <code>tagName<code>, using the
     * information provided in <code>address</code>.
     *
     * @param address the address to extract the postal code information from.
     * @param tagName the name to give the generated XML tag.
     * @return The completed postal code XML tag.
     *
     */
    protected Node extractPostalCode(Addr address, String tagName)
    {
        if (address == null || tagName == null)
           return null;

        String val = null;

        try
        {
            val = new String();

            if (!isEmpty(address.getPostalFSA()))
               val += address.getPostalFSA();

            if (!isEmpty(address.getPostalLDU()))
               val += format.space()+ address.getPostalLDU();
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    /**
     * Calls <code>extractPostalCode(Property, String)</code> with a tag name of "PostalCode"
     *
     * @param prop the property to extract the postal code information from.
     * @return The completed postal code XML tag.
     */
    protected Node extractPostalCode(Property prop)
    {
        return extractPostalCode(prop, "PostalCode");
    }

    /**
     * Creates a postal code XML tag with a name of <code>tagName<code>, using the
     * information provided in <code>prop</code>.
     *
     * @return The completed postal code XML tag.
     *
     */
    protected Node extractPostalCode(Property prop, String tagName)
    {
        if (prop == null || tagName == null)
           return null;

        String val = null;

        try
        {
            val = new String();

            if (!isEmpty(prop.getPropertyPostalFSA()))
               val += prop.getPropertyPostalFSA();

            if (!isEmpty(prop.getPropertyPostalLDU()))
               val += format.space() + prop.getPropertyPostalLDU();
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    /**
     * Adds a property's legal lines to <code>parentNode</code>.
     *
     * @param parentNode the node to add the legal lines to.
     * @param p the property to extract the legal lines from.
     * @return whether the addition was successful.
     */
    protected boolean addLegalLines(Node parentNode, Property p)
    {
        if (parentNode == null || p == null)
           return false;

        try
        {
            if (addTo(parentNode, "LegalLine1", p.getLegalLine1()) &&
                addTo(parentNode, "LegalLine2", p.getLegalLine2()) &&
                addTo(parentNode, "LegalLine3", p.getLegalLine3()))
                return true;
        }
        catch (Exception e){}

        return false;
    }

    /**
     * Retrieves and formats the Branch Address from the deal and formats an XML tag with it.
     * Example formatted string: 240, 630 - 4th Avenue S.W., Calgary, AB &nbsp;T2P0J9
     *
     * @return The completed Branch Address XML tag.
     *
     */
    protected Node extractBranchAddressComplete()
    {
        String val = null;

        try
        {
            BranchProfile b = new BranchProfile(srk, deal.getBranchProfileId());
            Addr addr = b.getContact().getAddr();

            if (addr != null)
            {
                Province prov = new Province(srk, addr.getProvinceId());

                //  TODO:  more null checks required?
                val = addr.getAddressLine1() + "," + format.space();

                if (!isEmpty(addr.getAddressLine2()))
                   val += addr.getAddressLine2() + "," + format.space();

                val += addr.getCity() + "," + format.space() +
                              prov.getProvinceAbbreviation() +
                              format.space() + format.space() +
                              addr.getPostalFSA() + addr.getPostalLDU();
            }
        }
        catch (Exception e){}

        return getTag("BranchAddressComplete", val);
    }

    /**
     * Retrieves the Mortgage Insurance Policy Num from the deal and formats an XML tag with it.
     *
     * @return The completed Mortgage Insurance Policy Num XML tag.
     *
     */
    protected Node extractMIPolicyNum()
    {
        String val = null;

        try
        {
            val = deal.getMIPolicyNumber();
        }
        catch (Exception e){}

        return getTag("MIPolicyNumber", val);
    }

    /**
     * Retrieves the Mortgage Insurance Status from the deal and formats an XML tag with it.
     *
     * @return The completed Mortage Insurance Status XML tag.
     *
     */
    protected Node extractMIStatus()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("MIStatus", deal.getMIStatusId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MIStatus", deal.getMIStatusId() , lang);
        }
        catch (Exception e){}

        return getTag("MIStatus", val);
    }

    /**
     * Retrieves and formats the Borrower Names from the deal and
     * formats repeating XML tags with it.
     *
     * @return The completed Borrower Names XML tag.
     *
     */
    protected Node extractBorrowerNames()
    {
        Node elemNode = null;

        try
        {
            List borrowers = getBorrowers();

            if(borrowers.isEmpty())
                return null;

            Collections.sort(borrowers, new BorrowerTypeComparator());

            Iterator it = borrowers.iterator();

            Borrower bor = null;
            String firstname = null;
            String lastname = null;

            while(it.hasNext())
            {
                bor = (Borrower)it.next();

                if(bor.getBorrowerTypeId() == Mc.BT_BORROWER)
                {
                    firstname = bor.getBorrowerFirstName();
                    lastname = bor.getBorrowerLastName();
                    String suffix = BXResources.getPickListDescription(srk
                            .getExpressState().getDealInstitutionId(),
                            "SUFFIX", bor.getSuffixId(), lang);

                    if (!isEmpty(firstname) && !isEmpty(lastname))
                    {
                        if (elemNode == null)
                           elemNode = doc.createElement("BorrowerNames");

                        addTo(elemNode, "Name", firstname + format.space()
                                + lastname + format.space() + suffix);
                    }
                }
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractBorrowerNamesLine()
    {
        String line = null;

        try
        {
            List borrowers = getBorrowers();

            if (borrowers.isEmpty())
               return null;

            Iterator it = borrowers.iterator();
            String firstname, lastname;
            Borrower bor;
            String val = null;

            while (it.hasNext())
            {
                if (line == null)
                   line = new String();

                bor = (Borrower)it.next();

                if (bor.getBorrowerTypeId() == Mc.BT_BORROWER)
                {
                    firstname = bor.getBorrowerFirstName();
                    lastname = bor.getBorrowerLastName();
                    String suffix = BXResources.getPickListDescription(srk
                            .getExpressState().getDealInstitutionId(),
                            "SUFFIX", bor.getSuffixId(), lang);
                    
                    if (!isEmpty(firstname) && !isEmpty(lastname))
                    {
                        val = firstname + format.space() + lastname
                                + format.space() + suffix;
                        line += val + (it.hasNext()?",":"") + format.space();
                    }
                }
            }

            if (line.length() > 0)
            {
                line = line.trim();

                if (line.endsWith(","))
                    line = line.substring(0,line.length()-1);
            }
        }
        catch (Exception e){}

        return getTag("BorrowerNamesLine", line);
    }

    /**
     * Retrieves and formats the Guarantor Clause from the deal and
     * formats an XML tag with it.
     *
     * @return The completed Guarantor Clause XML tag.
     *
     */
    protected Node extractGuarantorClause()
    {
        String gc = null;

        try
        {
            Property p = getPrimaryProperty();

            if (p != null)
               gc = (p.getProvinceId() == Mc.PROVINCE_BRITISH_COLUMBIA) ?
                  (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? "Covenantor" : "Covenantant") :
                  (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? "Guarantor" : "Garant");
        }
        catch (Exception e){}

        return getTag("GuarantorClause", gc);
    }

    protected Node extractGuarantorVerb()
    {
        String val = null;

        try
        {
            Property prop = getPrimaryProperty();

            ConditionHandler ch = new ConditionHandler(srk);

            if(prop.getProvinceId() == Mc.PROVINCE_BRITISH_COLUMBIA)
                val = ch.getVariableVerbiage(deal, Dc.COVENANTOR_VERB,lang);
            else
                val = ch.getVariableVerbiage(deal, Dc.GUARANTOR_VERB,lang);
        }
        catch (Exception e){}

        return getTag("GuarantorVerb", val);
    }

    /**
     * Retrieves and formats the Guarantor Names from the deal and
     * formats repeating XML tags with it.
     *
     * @return The completed Borrower Names XML tag.
     *
     */
    protected Node extractGuarantorNames()
    {
        Node elemNode = null;

        try
        {
            List borrowers = getBorrowers();

            if (borrowers.isEmpty())
               return null;

            Iterator i = borrowers.iterator();

            while (i.hasNext())
            {
                Borrower b =(Borrower)i.next();

                if (b.getBorrowerTypeId() == Mc.BT_GUARANTOR)
                {
                    if (elemNode == null)
                       elemNode = doc.createElement("GuarantorNames");

                    StringBuffer name = new StringBuffer();
                    String suffix = BXResources.getPickListDescription(srk
                            .getExpressState().getDealInstitutionId(),
                            "SUFFIX", b.getSuffixId(), lang);
                    
                    if (!isEmpty(b.getBorrowerFirstName()))
                       name.append(b.getBorrowerFirstName());

                    if (!isEmpty(b.getBorrowerLastName()))
                       name.append(format.space()).append(b.getBorrowerLastName());

                    if (!isEmpty(suffix))
                        name.append(format.space()).append(suffix);
                    addTo(elemNode, "Name", name.toString());
                }
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractPropertyAddress()
    {
        Node addrNode = null;

        try
        {
            Property p = getPrimaryProperty();

            addrNode = doc.createElement("PropertyAddress");

            addTo(addrNode, extractAddressLine1(p, "Line1"));
            addTo(addrNode, "Line2", p.getPropertyAddressLine2());
            addTo(addrNode, "City", p.getPropertyCity());
            addTo(addrNode, extractAddressProvinceId(p, "ProvinceId"));
            addTo(addrNode, extractAddressProvince(p, "Province"));
            addTo(addrNode, extractPostalCode(p));
        }
        catch (Exception e){}

        return addrNode;
    }

    protected Node extractAddressLine1(Property p)
    {
        return extractAddressLine1(p, "AddressLine1");
    }

    //#DG688 rewritten to use stringbuffer
    protected Node extractAddressLine1(Property p, String tagName)
    {
    	StringBuffer val = new StringBuffer(88);
        try
        {

            if(  !isEmpty(p.getUnitNumber())  ){
              val.append(p.getUnitNumber()).append("-");
            }

            if (!isEmpty(p.getPropertyStreetNumber()))
               val.append(p.getPropertyStreetNumber());

            if (!isEmpty(p.getPropertyStreetName()))
               val.append(format.space()).append(p.getPropertyStreetName());

            if (!isEmpty(p.getStreetTypeDescription())){
              val.append(format.space()).append(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, p.getStreetTypeId(), lang));
            }

            if (p.getStreetDirectionId() != 0)
               val.append(format.space()).append(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_STREET_DIRECTION, p.getStreetDirectionId(), lang));
        }
        catch (Exception e){}

        return getTag(tagName, val.toString());
    }

    protected Node extractAddressLine2(Property p)
    {
        return extractAddressLine2(p, "AddressLine2");
    }

    protected Node extractAddressLine2(Property p, String tagName)
    {
        String val = null;

        try
        {
            val = p.getPropertyAddressLine2();
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractAddressCity(Property p)
    {
        return extractAddressCity(p, "AddressCity");
    }

    protected Node extractAddressCity(Property p, String tagName)
    {
        String val = null;

        try
        {
            val = p.getPropertyCity();
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractAddressProvince(Property p)
    {
        return extractAddressProvince(p, "AddressProvince");
    }

    protected Node extractAddressProvinceId(Property p, String tagName){
        String val = null;

        try
        {
            val = "" + p.getProvinceId();
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractAddressProvince(Property p, String tagName)
    {
        String val = null;

        try
        {
            Province prov = new Province(srk, p.getProvinceId());

            val = prov.getProvinceName();
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractOccupancyType(Property p)
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("OccupancyType", p.getOccupancyTypeId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "OccupancyType",  p.getOccupancyTypeId() , lang);
        }
        catch (Exception e){}

        return getTag("OccupancyType", val);
    }

    protected Node extractPropertyType(Property p)
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("PropertyType", p.getPropertyTypeId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PropertyType",  p.getPropertyTypeId() , lang);
        }
        catch (Exception e){}

        return getTag("PropertyType", val);
    }

    protected Node extractPropertyStructureAge(Property p)
    {
        String val = null;

        try
        {
            val = Integer.toString(p.getStructureAge());
        }
        catch (Exception e){}

        return getTag("PropertyStructureAge", val);
    }

    protected Node extractNewConstruction(Property p)
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("NewConstruction", p.getNewConstructionId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "NewConstruction",  p.getNewConstructionId() , lang);
        }
        catch (Exception e){}

        return getTag("NewConstruction", val);
    }

    protected Node extractAppraisalDate(Property p)
    {
        String val = null;

        try
        {
            val = formatDate(p.getAppraisalDateAct());
        }
        catch (Exception e){}

        return getTag("AppraisalDate", val);
    }

    protected Node extractLandValue(Property p)
    {
        String val = null;

        try
        {
            val = formatCurrency(p.getLandValue());
        }
        catch (Exception e){}

        return getTag("LandValue", val);
    }

    protected Node extractAdvanceDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getEstimatedClosingDate());
        }
        catch (Exception e){}

        return getTag("AdvanceDate", val);
    }

    protected Node extractNetRate()
    {
        return extractNetRate("NetRate");
    }

    protected Node extractNetRate(String tagName)
    {
        String val = null;

        try
        {
            val = formatInterestRate(deal.getNetInterestRate());
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractIADDate()
    {
        String val = null;

        try
        {
            val = formatDate(deal.getInterimInterestAdjustmentDate());
        }
        catch (Exception e){}

        return getTag("IADDate", val);
    }

    protected Node extractTerm()
    {
        return extractTerm("PaymentTerm");
    }

    protected Node extractAnnualTaxes()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getCombinedTotalAnnualTaxExp());
        }
        catch (Exception e){}

        return getTag("AnnualTaxes", val);
    }

    protected Node extractTotalPayment()
    {
        return extractTotalPayment("TotalPayment");
    }

    protected Node extractTotalPayment(String tagName)
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getTotalPaymentAmount());
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractBrokerFirmName()
    {
        String val = null;

        try
        {
                SourceFirmProfile sfp = new SourceFirmProfile(srk,
                                            deal.getSourceFirmProfileId());

                val = sfp.getSourceFirmName();
        }
        catch (Exception e){}

        return getTag("BrokerFirmName", val);
    }


    protected Node extractBrokerAgentName()
    {
        String val = null;

        try
        {
            SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();

            Contact contact = sobp.getContact();

            val = "";

            if (!isEmpty(contact.getContactFirstName()))
               val += contact.getContactFirstName() + format.space();

            if (!isEmpty(contact.getContactLastName()))
               val += contact.getContactLastName();
        }
        catch (Exception e){}

        return getTag("BrokerAgentName", val);
    }

    protected Node extractProduct()
    {
        String val = null;

        try
        {
            //val = deal.getMtgProd().getMtgProdName();
            // &&&
            val = (String) BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MTGPROD", deal.getMtgProd().getMtgProdId(), lang);
        }
        catch (Exception e){}

        return getTag("Product", val);
    }

    protected Node extractActualAppraisalValue(Property p)
    {
        return extractActualAppraisalValue(p, "ActualAppraisalValue");
    }

    protected Node extractActualAppraisalValue(Property p, String tagName)
    {
        String val = null;

        try
        {
            val = formatCurrency(p.getActualAppraisalValue());
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractBrokerAddressSection()
    {
        Node elemNode = null;

        try
        {
            SourceFirmProfile sfp = new SourceFirmProfile(srk, deal.getSourceFirmProfileId());

            Contact c = sfp.getContact();
            Addr addr = c.getAddr();

            if (addr == null)
               return null;

            elemNode = doc.createElement("BrokerAddress");

            addTo(elemNode, "Line1", addr.getAddressLine1());
            addTo(elemNode, "Line2", addr.getAddressLine2());
            addTo(elemNode, "City", addr.getCity());

            Province prov = new Province(srk, addr.getProvinceId());

            addTo(elemNode, "Province", prov.getProvinceName());

            //  Postal Code
            addTo(elemNode, extractPostalCode(addr));

            if (!isEmpty(c.getContactPhoneNumber()))
            {
                String str = StringUtil.getAsFormattedPhoneNumber(c.getContactPhoneNumber(),
                        c.getContactPhoneNumberExtension());

                addTo(elemNode, "Phone", str);
            }

            if (!isEmpty(c.getContactFaxNumber()))
            {
                String str = StringUtil.getAsFormattedPhoneNumber(c.getContactFaxNumber(), null);

                addTo(elemNode, "Fax", str);
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    /**
     * Retrieves the Deal Number from the deal and formats an XML tag with it.
     *
     * @return The completed Deal Number XML tag.
     *
     */
    protected Node extractDealNum()
    {
        String val = null;

        try
        {
            val = Integer.toString(deal.getDealId());
        }
        catch (Exception e)  {}

        return getTag("DealNum", val);
    }

   /**
     * Retrieves the underwriter name from the deal and formats an XML tag with it.
     *
     * @return The completed Underwriter Name XML tag or <code>null</code> if
     *         the underwriter information isn't available.
     *
     */
    protected Node extractUnderwriter()
    {
        String val = null;

        try
        {
            UserProfile up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(), 
                                                           deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            val = new String();

            if (!isEmpty(c.getContactFirstName()))
               val += c.getContactFirstName();

            if (!isEmpty(c.getContactLastName()))
               val += format.space() + c.getContactLastName();
        }
        catch(Exception exc){}

        return getTag("Underwriter", val);
    }

    protected Node extractBrokerSourceNum()
    {
        String val = null;

        try
        {
            val = deal.getSourceApplicationId();
        }
        catch (Exception e){}

        return getTag("BrokerSourceNum", val);
    }

    protected Node extractNetLoanAmount()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getNetLoanAmount());
        }
        catch (Exception e) {}

        return getTag("NetLoanAmount", val);
    }

    protected Node extractLoanAmount()
    {
        String val = null;

        try {
          val = formatCurrency(calcLoanAmount());   //#DG258
        }
        catch (Exception e){}

        return getTag("LoanAmount", val);
    }

    //#DG258 will be used to calculate net amount for sol.pkg
    /**
     * calculate loan amount
     * @return double
     */
    protected double calcLoanAmount() {
      int id = deal.getDealPurposeId();
      int miIndID = deal.getMIIndicatorId();
      double amt;

      switch(id)
      {
          case Mc.DEAL_PURPOSE_PORTABLE:
          case Mc.DEAL_PURPOSE_PORTABLE_DECREASE:
               amt = deal.getNetLoanAmount();
               break;

          default:
               if ((miIndID == Mc.MII_REQUIRED_STD_GUIDELINES ||
                    miIndID == Mc.MII_UW_REQUIRED) &&
                    deal.getMIUpfront().equals("N"))
               {
                  amt = deal.getNetLoanAmount();
               }
               else
                  amt = deal.getTotalLoanAmount();

               break;
      }
      return amt;
    }

    protected Node extractPremium()
    {
        String val = null;

        try
        {
            double amt = 0.0;
            int mid = deal.getMIIndicatorId();

            if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED) &&
                deal.getMIUpfront().equals("N"))
            {
                amt = deal.getMIPremiumAmount();
            }

            if (bKeepFeeTotal)
               totalFees += amt;

            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("Premium", val);
    }

    protected Node extractMIPremiumAmount()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getMIPremiumAmount());
        }
        catch (Exception e){}

        return getTag("MIPremiumAmount", val);
    }

    //#DG534 rewritten - now retrieves pst fees based
    // on the fee description in the database
    /**
     * extracts Pst for premium
     *
     * @return Node
     */
    protected Node extractPST()    {
      double amt = 0;
    	getfee: try {

    		boolean MIUpfront = deal.getMIUpfront() != null && deal.getMIUpfront().equalsIgnoreCase("Y");
        if ( MIUpfront && deal.getMIPayorId() == Mc.MI_PAYOR_LENDER)
          break getfee;

        DealFee fee = new DealFee(srk,null);
        //#DG688
        //int MIPstTypeIds[] = {
        //    Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM,
        //    Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE,
        //    Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM,
        //    Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE   };
        //List fees = (List)fee.findByDealAndTypes((DealPK)deal.getPk(), MIPstTypeIds);
        List fees = (List)fee.findByDealAndTypes((DealPK)deal.getPk(), DealCalcUtil.MI_PST_FEES);

        if(fees.size() > 0)
        {
        	fee = (DealFee)fees.get(0);
        	amt = fee.getFeeAmount();
        }
        else
        	amt = deal.getMIPremiumPST();

        if (bKeepFeeTotal)
        	totalFees += amt;
    	}
    	catch (Exception e){
    		return null;
      }
    	String val = formatCurrency(amt);
    	return getTag("PST", val);
    }

    protected Node extractFRVRDescription()
    {
        String val = null;

        try
        {
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory( deal.getPricingProfileId() );

            MtgProd pr = new MtgProd(srk,null);

            List all = (List)pr.findByPricingProfile(pp.getPricingProfileId());

            if(all.size() > 0)
            {
                pr = (MtgProd)all.get(0);
                //val = PicklistData.getDescription("InterestType",  pr.getInterestTypeId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "InterestType", pr.getInterestTypeId() , lang);
            }
        }
        catch (Exception e){}

        return getTag("FixedRateVariableRateDescription", val);
    }

    protected Node extractPAPurchasePrice()
    {
        return extractPAPurchasePrice("PAPurchasePrice");
    }

    protected Node extractPAPurchasePrice(String tagName)
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getPAPurchasePrice());
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractInterestRate()
    {
        String val = null;

        try
        {
            PricingProfile pp = new PricingProfile(srk);
            pp = pp.findByPricingRateInventory( deal.getPricingProfileId());
            val = new String();

            if(pp != null && pp.getRateCode().equalsIgnoreCase("UNCNF"))
            {
                ConditionHandler ch = new ConditionHandler(srk);
                val += ch.getVariableVerbiage(deal, Dc.UNCONFIRMED_RATE,lang);
            }
            else if(deal.getDiscount() != 0 && deal.getPremium() != 0)
            {
                val = "*" + format.space();
            }

            val += formatInterestRate(deal.getNetInterestRate());
        }
        catch (Exception e){}

        return getTag("InterestRate", val);
    }

    protected Node extractCommitmentExpiryDate()
    {
        String val = null;

        try
        {
            val = DocPrepLanguage.getInstance().
                getFormattedDate(deal.getCommitmentExpirationDate(), lang);
        }
        catch (Exception e){}

        return getTag("CommitmentExpiryDate", val);
    }

    protected Node extractFunderName()
    {
        String val = null;

        try
        {
            UserProfile up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getFunderProfileId(), 
                                                           deal.getInstitutionProfileId()));

            Contact c = up.getContact();

            val = new String();

            if (!isEmpty(c.getContactFirstName()))
               val += c.getContactFirstName();

            if (!isEmpty(c.getContactLastName()))
               val += format.space() + c.getContactLastName();
        }
        catch (Exception e)  {}

        return getTag("FunderName", val);
    }

    protected Node extractSolicitorSection()
    {
        Node elemNode = null;

        try
        {
            PartyProfile pp = new PartyProfile(srk);
            pp = pp.findByDealSolicitor(deal.getDealId());

            Contact cont = pp.getContact();
            Addr addr = cont.getAddr();

            String val = new String();
            elemNode = doc.createElement("Solicitor");

            //  Name
            if (!isEmpty(cont.getContactFirstName()))
               val += cont.getContactFirstName();

            if (!isEmpty(cont.getContactLastName()))
               val += format.space() + cont.getContactLastName();

            addTo(elemNode, getTag("Name", val));

            //  Firm
            addTo(elemNode, "Firm", pp.getPartyCompanyName());

            //  Address line 1
            addTo(elemNode, "Address1", addr.getAddressLine1());

            //  Address line 2
            addTo(elemNode, "Address2", addr.getAddressLine2());

            //  City
            addTo(elemNode, "AddressCity", addr.getCity());

            //  Province
            //val = PicklistData.getDescription("Province", addr.getProvinceId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province", addr.getProvinceId() , lang);

            addTo(elemNode, "AddressProvince", val);

            //  Postal Code
            addTo(elemNode, extractPostalCode(addr, "AddressPostal"));

            if (!isEmpty(cont.getContactPhoneNumber()))
            {
                val = StringUtil.getAsFormattedPhoneNumber(cont.getContactPhoneNumber(),
                        cont.getContactPhoneNumberExtension());

                addTo(elemNode, "Phone", val);
            }

            if (!isEmpty(cont.getContactFaxNumber()))
            {
                val = StringUtil.getAsFormattedPhoneNumber(cont.getContactFaxNumber(), null);

                addTo(elemNode, "Fax", val);
            }
        }
        catch(Exception exc){}

        return elemNode;
    }

    protected Node extractClosingText()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal,Dc.CLOSING_TEXT,lang);
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("ClosingText"), val));
    }

    protected Node extractPrepayment()
    {
        String val = null;

        try
        {
            ConditionHandler ch = new ConditionHandler(srk);

            switch (deal.getPrePaymentOptionsId())
            {
                case Mc.PREPAYMENT_OPTIONS_NHA:
                    val = ch.getVariableVerbiage(deal, Dc.PREPAYMENT_NHA, lang);

                    //==================================================================
                    // ALERT:
                    // Zivko made this change on 17 Sep 2002, based on request from
                    // Derek. Condition 66 (PREPAYMENT_NHA_LONGTERM) should be triggered
                    // for actual payment term of 5 years (60 months).
                    //===================================================================
                    //if (deal.getActualPaymentTerm() > 36)
                    if (deal.getActualPaymentTerm() > 60)
                    {
                        val +=  ch.getVariableVerbiage(deal, Dc.PREPAYMENT_NHA_LONGTERM, lang);
                    }
                    break;

                case Mc.PREPAYMENT_OPTIONS_OPEN_WITH_PENALTY:
                    val = ch.getVariableVerbiage(deal, Dc.PREPAYMENT_OPEN_WITH_PENALTY, lang);

                    if (deal.getActualPaymentTerm() > 60)
                    {
                        val += ch.getVariableVerbiage(deal, Dc.PREPAYMENT_OPEN_PENALTY_LONG_TERM, lang);
                    }
                    break;

                case Mc.PREPAYMENT_OPTIONS_OPEN:
                     val = ch.getVariableVerbiage(deal, Dc.PREPAYMENT_OPEN, lang);
                     break;

                case Mc.PREPAYMENT_OPTIONS_NOT_ALLOWED:
                     val = ch.getVariableVerbiage(deal, "PrePayment Not Allowed", lang);
                     break;

                case Mc.PREPAYMENT_OPTIONS_3_MONTH_PENALTY:
                     val = ch.getVariableVerbiage(deal, Dc.PREPAYMENT_3_MONTH_PENALTY, lang);
                     break;

                case Mc.PREPAYMENT_OPTIONS_2_MONTH_PENALTY:
                     val = ch.getVariableVerbiage(deal, Dc.PREPAYMENT_2_MONTH_PENALTY, lang);
                     break;

                default:    break;
            }
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("Prepayment"), val));
    }

    protected Node extractTaxPayor()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("TaxPayor", deal.getTaxPayorId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "TaxPayor", deal.getTaxPayorId() , lang);
        }
        catch (Exception e){}

        return getTag("TaxPayor", val);
    }

    protected Node extractCharge()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("LienPosition", deal.getLienPositionId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LienPosition", deal.getLienPositionId() , lang);
        }
        catch (Exception e)  {}

        return getTag("Charge", val);
    }

    protected Node extractRepaymentType()
    {
        String val = null;

        try
        {
            //val = PicklistData.getDescription("RepaymentType", deal.getRepaymentTypeId());
            //ALERT:BX:ZR Release2.1.docprep
            val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "RepaymentType", deal.getRepaymentTypeId() , lang);
        }
        catch (Exception e)  {}

        return getTag("RepaymentType", val);
    }

    protected Node extractSourcePhone()
    {
        String val = null;

        try
        {
            SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();

            Contact contact = sobp.getContact();
            //Addr addr = contact.getAddr();

            val = StringUtil.getAsFormattedPhoneNumber(contact.getContactPhoneNumber(),
                                                       contact.getContactPhoneNumberExtension());
        }
        catch (Exception e){}

        return getTag("SourcePhone", val);
    }

    protected Node extractSourceFax()
    {
        String val = null;

        try
        {
            SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile();

            Contact contact = sobp.getContact();
            //Addr addr = contact.getAddr();

            val = StringUtil.getAsFormattedPhoneNumber(contact.getContactFaxNumber(), null);
        }
        catch (Exception e){}

        return getTag("SourceFax", val);
    }

    protected Node extractAdminFee()
    {
        String val = null;

        try
        {
            List fees = (List)deal.getDealFees();
            double amt = 0;

            Iterator i = fees.iterator();

            while (i.hasNext())
            {
                DealFee df = (DealFee)i.next();

                if (df.getFee().getFeeTypeId() == Mc.FEE_TYPE_APPLICATION_FEE)
                   amt += df.getFeeAmount();
            }

            totalFees += amt;

            val = formatCurrency(amt);
        }
        catch (Exception e){}

        return getTag("AdminFee", val);
    }

    protected Node extractTaxPortionMonthly(){
      double rv =0d;
      switch( deal.getPaymentFrequencyId() ){
        case Mc.PAY_FREQ_MONTHLY :{
          rv = deal.getEscrowPaymentAmount();
          break;
        }
        case Mc.PAY_FREQ_SEMIMONTHLY :{
          rv =  (deal.getEscrowPaymentAmount() * 24 )/12;
          break;
        }
        case Mc.PAY_FREQ_BIWEEKLY :{
          rv =  (deal.getEscrowPaymentAmount() * 26 )/12;
          break;
        }
        case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY :{
          rv =  (deal.getEscrowPaymentAmount() * 26 )/12;
          break;
        }
        case Mc.PAY_FREQ_WEEKLY :{
          rv =  (deal.getEscrowPaymentAmount() * 52 )/12;
          break;
        }
        case Mc.PAY_FREQ_ACCELERATED_WEEKLY :{
          rv =  (deal.getEscrowPaymentAmount() * 52 )/12;
          break;
        }
      }

      return getTag("TaxPortionMonthly", formatCurrency(rv));
    }

    protected Node extractTaxPortion()
    {
        String val = null;

        try
        {
            EscrowPayment ep = new EscrowPayment(srk, null);

            List list = (List)ep.findByDeal((DealPK)deal.getPk());

            if (!list.isEmpty())
            {
                Iterator i = list.iterator();

                while (i.hasNext())
                {
                    ep = (EscrowPayment)i.next();

                    if (ep.getEscrowTypeId() == 0)
                    {
                        val = formatCurrency(ep.getEscrowPaymentAmount());
                        break;
                    }
                }
            }
        }
        catch (Exception e){}

        return getTag("TaxPortion", val);
    }

    protected Node extractAdvanceHold()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getAdvanceHold());
        }
        catch(Exception exc){}

        return getTag("AdvanceHold", val);
    }

    protected Node extractReferenceNum()
    {
        return extractServicingMortgageNumber("ReferenceNum");
    }

    protected Node extractServicingMortgageNumber()
    {
        return extractServicingMortgageNumber("ServicingMortgageNumber");
    }

    protected Node extractServicingMortgageNumber(String tagName)
    {
        String val = null;

        try
        {
            val = deal.getReferenceDealNumber();

        }
        catch (Exception e){}

        return getTag(tagName, val);
    }

    protected Node extractBrokerEmail()
    {
        String val = null;

        try
        {
            if (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS)
            {
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                          deal.getInstitutionProfileId()));

                Contact c = up.getContact();

                if (!isEmpty(c.getContactEmailAddress()))
                    val = "Email: " + c.getContactEmailAddress();
            }
        }
        catch (Exception e){}

        return getTag("BrokerEmail", val);
    }


    //#DG344
    /**
     * make it convenient for all XSLExtractors to create the deal tree
     * @param parent Element
     * @throws DOMException
     */
    protected void createDealTree(Element parent) throws DOMException {
      //#DG238/#DG312 make a full blown deal tree available so that this xml can be used for all templates
      //addActionListener(this);
      ratioFieldList = DocumentGenerationManager.getInstance().getRatioFieldList();
      DealExtractor thisAsDE = ( (DealExtractor)this);
      thisAsDE.deal = deal;
      thisAsDE.lang = lang;
      thisAsDE.format = format;
      thisAsDE.srk = srk;
      thisAsDE.doc = doc;

      Element dealNode = createSubTree(doc, deal.getEntityTableName(), srk, deal, true, true, lang, true);
      parent.appendChild(dealNode);
      //#DG238 end
    }

  //#DG358 facilitate production of condition verbiage
  /**
   * create a node <code>tagName</code> with the text taken from the condition
   * verbiage of given <code>condId</code>
   * @param condId int condition id to retrieve
   * @param tagName String name of node to create
   * @return Node the node
   */
  protected Node condVarVerb(int condId, String tagName) {
    String val = null;
    try {
      ConditionHandler ch = new ConditionHandler(srk);
      val = ch.getVariableVerbiage(deal, condId, lang);
    }
    catch (Exception e) {}

    return val == null ? null :
        convertCRToLines(doc.createElement(tagName), val);
  }
  /****************MCM Impl Team ************************/
  /**
   * This method  returns the UnderWriteAs element tag with the value.
   * @version 1.1 11-July-2008 XS_16.17 Initial version
   */
  protected Node extracUnderWriteAs() {
    String val=null;
     try
    {
         val=BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "UnderwriteAsType", deal.getMtgProd().getUnderwriteAsTypeId(), lang);
    }     
    catch (Exception e)
    {
        e.printStackTrace();
    }
    return getTag("UnderWriteAs",val);
   
    }
/**
 * This method returns the product type tag
 * @version 1.1 11-July-2008 XS_16.17 Initial version
 */
  protected Node extractProductType() {
      String val=null;
       try
      {
           val=BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ProductType", deal.getMtgProd().getProductTypeId(), lang);
           //val=Integer.toString(deal.getMtgProd().getProductTypeId());
      }     
      catch (Exception e)
      {
          e.printStackTrace();
      }
      return getTag("ProductType",val);
     
      }
  /******************************************************/
  
  /***** CTFS CR224B (FXP20818) merge start *****/
  protected Node extractTotalPayments() {
      
      double val = 0;
      String totalPayments = "";
      try {
          double interimInterestAmount = deal.getInterimInterestAmount();
          int paymentFrequenceId = deal.getPaymentFrequencyId();
          double pandiPaymentAmount = deal.getPandiPaymentAmount();
          //int paymentTermId = deal.getPaymentTermId();
          int ptValue = deal.getActualPaymentTerm()/12;
      
          int multyplier = 0;
          if(paymentFrequenceId == 2 || paymentFrequenceId == 3){
              multyplier = 26;
          }
          else
              if(paymentFrequenceId == 4 || paymentFrequenceId == 5) {
                  multyplier = 52;
              }
              else 
                  if(paymentFrequenceId == 0) {
                      multyplier = 12;
                  }
                  else 
                      if(paymentFrequenceId == 1) {
                          multyplier = 24;
                      }
      
          val = interimInterestAmount + (pandiPaymentAmount * multyplier * ptValue);
          
//        double pandiPaymentAmountMonthly = deal.getPandIPaymentAmountMonthly();
//        int ptValue = deal.getActualPaymentTerm();
//        
//        val = pandiPaymentAmountMonthly * ptValue;
      
          totalPayments = formatCurrency(val);
      }
      catch(Exception e){}
      
      return getTag("TotalPayments", totalPayments);
      
  }
  
  protected Node extractSOBRegionId() {
      
      int sobRegionId = -1;
      try {
          int sobProfileId = deal.getSourceOfBusinessProfileId();
          SourceOfBusinessProfile sourceOfBusinessProfile = new SourceOfBusinessProfile(srk, sobProfileId);
          
          if(sourceOfBusinessProfile != null) {
              sobRegionId = sourceOfBusinessProfile.getSOBRegionId();
          }
      }
      catch(Exception e){}
      
      return getTag("SOBRegionId", ""+sobRegionId);
  }
  
  protected Node extractSOBRegionDescription() {
      
      int sobRegionId = -1;
      String sobRegionDescription = "";
      try {
          int sobProfileId = deal.getSourceOfBusinessProfileId();
          SourceOfBusinessProfile sourceOfBusinessProfile = new SourceOfBusinessProfile(srk, sobProfileId);
          
          if(sourceOfBusinessProfile != null) {
              sobRegionId = sourceOfBusinessProfile.getSOBRegionId();
          }
          
          sobRegionDescription = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "SOBREGION", sobRegionId , lang);
      }
      catch(Exception e){}
      
      return getTag("SOBRegionDescription", sobRegionDescription);
  }
  
  protected Node extractPaymentDueOn() {
      
      int paymentFrequencyId = -1;
      String paymentDueOn = "";
      
      try {
          
          paymentFrequencyId = deal.getPaymentFrequencyId();
          Date firstPaymentDate = deal.getFirstPaymentDate();
          if(paymentFrequencyId == 4) {
              paymentDueOn = "every week";
          }
          else
              if(paymentFrequencyId == 2) {
                  paymentDueOn = "every second week";
              }
              else
                  if(paymentFrequencyId == 1) {
                      paymentDueOn = "twice each month";
                  }
                  else
                      if(paymentFrequencyId == 0) {
                          Calendar c = Calendar.getInstance();
                          c.setTime(firstPaymentDate);
                          
                          paymentDueOn = "on the " + c.get(Calendar.DAY_OF_MONTH) + " of each month";
                      }
          
      }
      catch(Exception e) {}
      
      return getTag("PaymentDueOn", paymentDueOn);
      
  }
  
  protected Node extractNetAdvance() {
      
      String val = "";
      try {
          
          double netLoanAmount = deal.getNetLoanAmount();
          //double insurancePremium = deal.getMIPremiumAmount();
          double miPremiumPST = deal.getMIPremiumPST();
          
          double res = netLoanAmount - miPremiumPST;
          val = formatCurrency(res);
          
      }
      catch(Exception e) {}
      
      return getTag("netAdvance", val);
      
  }
  
  protected Node extractInternalRatePercentage() {
      
      String val = "";
      try {
          
          PricingProfile pp = new PricingProfile(srk);
          PricingRateInventory pri = pp.findLatestInventoryByPricingProfileId(20);
          double irp = pri.getInternalRatePercentage();
          val = String.valueOf(irp) + "%";
          
      }
      catch(Exception e) {}
      
      return getTag("InternalRatePercentage", val);
      
  }
  
  protected Node extractAdjustmentFactor() {
      
      String val = "";
      try {
          double netInterestRate = deal.getNetInterestRate();
          PricingProfile pp = new PricingProfile(srk);
          PricingRateInventory pri = pp.findLatestInventoryByPricingProfileId(20);
          double irp = pri.getInternalRatePercentage();
          
          BigDecimal res = new BigDecimal(netInterestRate - irp).setScale(2, BigDecimal.ROUND_CEILING);
          val = "" + Math.abs(res.doubleValue());
      }
      catch(Exception e) {}
      
      return getTag("adjustmentFactor", val);
      
  }
  /***** CTFS CR224B (FXP20818) merge end *****/
}
