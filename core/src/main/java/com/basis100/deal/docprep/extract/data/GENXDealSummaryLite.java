package com.basis100.deal.docprep.extract.data;

/**
 * 18.Nov.2008 DVG #DG776 FXP23487: vMCM_express_Deal Summary Lite: Broker notes show up in French, but the applicant preferred language is english     
 */

import com.basis100.deal.entity.*;
import com.basis100.xml.*;
import org.w3c.dom.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.pk.ComponentCreditCardPK;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentLoanPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentOverdraftPK;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.util.collections.*;
import com.basis100.picklist.*;

import java.util.*;
import MosSystem.*;

import com.basis100.resources.*;
import com.basis100.deal.util.*;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

import com.filogix.util.Xc;

/**
 * Generates XML for Deal Summary document type
 *
 */
public class GENXDealSummaryLite extends GENXDealSummary
{
    /**
     * Builds the XML doc from various records in the database.
     * Modified for 16.10/16.24/16.23a
     */
    protected void buildXMLDataDoc() throws Exception
    {
        doc = XmlToolKit.getInstance().newDocument();
        Element root = doc.createElement("Document");
        root.setAttribute("type","1");
        root.setAttribute("lang","1");
        doc.appendChild(root);

        //==========================================================================
        // Language stuff
        //==========================================================================
        addTo(root, getLanguageTag());

        //==========================================================================
        // Current Date
        //==========================================================================
        addTo(root, extractCurrentDate());

        //==========================================================================
        // Reference #
        //==========================================================================
        addTo(root, extractReferenceNum());

        //==========================================================================
        // Deal Num
        //==========================================================================
        addTo(root, extractDealNum());

        //==========================================================================
        // Borrower Name
        //==========================================================================
        addTo(root, extractBorrowerName());

        //==========================================================================
        // Estimated Closing Date
        //==========================================================================
        addTo(root, extractEstClosingDate());

        //==========================================================================
        // Reference source app
        //==========================================================================
        addTo(root, extractReferenceSourceAppNum());

        //==========================================================================
        // Source Name
        //==========================================================================
        addTo(root, extractSourceName());

        //==========================================================================
        // Deal Status
        //==========================================================================
        addTo(root, extractDealStatus());

        //==========================================================================
        // Deal Status Date
        //==========================================================================
        addTo(root, extractDealStatusDate());

        //==========================================================================
        // Deal Purpose
        //==========================================================================
        addTo(root, extractDealPurpose());

        //==========================================================================
        // Total Loan Amount
        //==========================================================================
        addTo(root, extractTotalLoanAmount());

        //==========================================================================
        // MI Premium
        //==========================================================================
        addTo(root, extractMIPremium());

        //==========================================================================
        // Special Feature
        //==========================================================================
        addTo(root, extractSpecialFeature());

        //==========================================================================
        // Purchase Price
        //==========================================================================
        addTo(root, extractPurchasePrice());

        //==========================================================================
        // Payment Term
        //==========================================================================
        addTo(root, extractPaymentTerm());
        
        // ---------------------- pvcs #1065 (2), change for DJ_DealSummaryLite by Catherine, 08-Mar-05 ----------
        //==========================================================================
        // Actual Payment Term
        //==========================================================================
        addTo(root, extractActualPaymentTerm());

        // ---------------------- pvcs #1065 (2), change for DJ_DealSummaryLite by Catherine, 08-Mar-05 ----------

        // --------- CR, 15-Jul-04, added Amortization (Derek for BMO)
        //==========================================================================
        // Amortization
        //==========================================================================
        addTo(root, extractAmortization());
        // --------- CR, 15-Jul-04

        //==========================================================================
        // Effective Amortization
        //==========================================================================
        addTo(root, extractEffectiveAmortization());

        //==========================================================================
        // Payment Frequency
        //==========================================================================
        addTo(root, extractPaymentFrequency());

        //==========================================================================
        // Posted Interest Rate
        //==========================================================================
        addTo(root, extractPostedInterestRate());

        //==========================================================================
        // Down Payment Section
        //==========================================================================
        addTo(root, extractDownPaymentsSection());

        //==========================================================================
        // Net Interest Rate
        //==========================================================================
        addTo(root, extractNetRate());

        //==========================================================================
        // Discount
        //==========================================================================
        addTo(root, extractDiscount());

        //==========================================================================
        // Premium
        //==========================================================================
        addTo(root, extractPremium());

        //==========================================================================
        // Buydown Rate
        //==========================================================================
        addTo(root, extractBuydownRate());

        //==========================================================================
        // P&I Payment
        //==========================================================================
        addTo(root, extractPandIPayment());

        //==========================================================================
        // Total Escrow Payment
        //==========================================================================
        addTo(root, extractTotalEscrowPayment());

        //==========================================================================
        // Total Payment
        //==========================================================================
        addTo(root, extractTotalPayment());

        //==========================================================================
        // Product
        //==========================================================================
        addTo(root, extractProduct());

        //==========================================================================
        // Estimated Closing Date
        //==========================================================================
        addTo(root, extractEstClosingDate());

        //==========================================================================
        // Lender
        //==========================================================================
        addTo(root, extractLender());

        //==========================================================================
        // Insurance Reference #
        //==========================================================================
        addTo(root, extractMIPolicyNum());

        //==========================================================================
        // Insurance Status
        //==========================================================================
        addTo(root, extractMIStatus());

        //==========================================================================
        // Credit Score
        //==========================================================================
        addTo(root, extractCreditScore());

        //==========================================================================
        // LTV Ratio
        //==========================================================================
        addTo(root, extractLTV());

        //==========================================================================
        // Combined GDS
        //==========================================================================
        addTo(root, extractCombinedGDS());

        //==========================================================================
        // Combined 3 Year GDS
        //==========================================================================
        addTo(root, extractCombined3YrGDS());

        //==========================================================================
        // Combined TDS
        //==========================================================================
        addTo(root, extractCombinedTDS());

        //==========================================================================
        // Combined 3 Year TDS
        //==========================================================================
        addTo(root, extractCombined3YrTDS());

        // ---------------------- pvcs #1065 (1), change for DJ_DealSummaryLite by Catherine, 08-Mar-05 ----------
        //==========================================================================
        // Combined GDS Borrower and TDS Borrower
        //==========================================================================
        addTo(root, extractCombinedGDSBorrower());
        addTo(root, extractCombinedTDSBorrower());
        
        // ---------------------- pvcs #1065 (1), change for DJ_DealSummaryLite by Catherine, 08-Mar-05 end ----------
        
        //==========================================================================
        // Combined Total Income
        //==========================================================================
        addTo(root, extractCombinedTotalIncome());

        //==========================================================================
        // Combined Total Assets
        //==========================================================================
        addTo(root, extractCombinedTotalAssets());

        //==========================================================================
        // Combined Total Liabilities
        //==========================================================================
        addTo(root, extractCombinedTotalLiabilities());

        //==========================================================================
        // Bureau Summary
        //==========================================================================
        addTo(root, extractBureauSummary());

        //==========================================================================
        // Broker Notes Section
        //==========================================================================
        addTo(root, extractBrokerNotes());

        //==========================================================================
        // Property
        //==========================================================================
        Property p = getPrimaryProperty();

        addTo(root, extractPropertyAddress());
        addTo(root, extractNewConstruction(p));
        addTo(root, extractOccupancyType(p));
        addTo(root, extractPropertyStructureAge(p));

        //==========================================================================
        // Applicant details section
        //==========================================================================
        addTo(root, extractApplicantDetailsSection());

        //==========================================================================
        // Lender
        //==========================================================================
        addTo(root, extractLender());

        //==========================================================================
        // Branch Address Complete
        //==========================================================================
        addTo(root, extractBranchAddressComplete());

        //==========================================================================
        // Branch Phone
        //==========================================================================
        addTo(root, extractBranchPhone(null, false));

        //==========================================================================
        // Branch Fax
        //==========================================================================
        addTo(root, extractBranchFax(null, false));

        //==========================================================================
        // Branch Toll Phone
        //==========================================================================
        addTo(root, extractBranchPhone(null, true));

        //==========================================================================
        // Branch Toll Fax
        //==========================================================================
        addTo(root, extractBranchFax(null, true));

        //==========================================================================
        // Parameter-determined section
        // Bureau Section
        // Business Rules
        //  TODO:  Defaults?
        //==========================================================================
        boolean bIncludeBureau =
            PropertiesCache.getInstance().
                getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.dealsummarylite.includebureau", "N").
                    equalsIgnoreCase("Y");

        boolean bIncludeBusinessRules =
            PropertiesCache.getInstance().
                getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.docprep.dealsummarylite.includebusinessrules", "N").
                    equalsIgnoreCase("Y");

        //==========================================================================
        // Credit Bureau Report
        //==========================================================================
        if (bIncludeBureau)
           addTo(root, extractCreditBureauReport());

        //==========================================================================
        // Business Rules
        //==========================================================================
        if (bIncludeBusinessRules)
           addTo(root, extractBusinessRulesSection());
        
        //------> Rel 3.1 - Sasa
        addTo(root, extractProductType());
        addTo(root, extractCharge());
        //------> End of Rel 3.1 - Sasa
        
        /*********** MCM Impl Team XS_16.24 starts *********/ 
        ComponentSummary compSummary = getComponentSummary();

        /***************  artf765172 Unallocated Amount fix starts  *******************/
        if(compSummary != null) {
            addTo(root, getTag("ComponentExist", "Y"));
            addTo(root, extractTotalAmount(compSummary));
            addTo(root, extractTotalCashBackAmount(compSummary));
            addTo(root, extractUnallocateAmount(compSummary));
        }
        /***************  artf765172 Unallocated Amount fix starts  *******************/
        
        addTo(root, extractComponent());
       
        boolean showQualifyDetail = "Y".equalsIgnoreCase(
                PropertiesCache.getInstance().getProperty(
                srk.getExpressState().getDealInstitutionId(),
                "com.basis100.deal.usePOSQualifyDetail"));
        
        if(showQualifyDetail) {
            addTo(root, extractQualifyTitle());
            addTo(root, extractQualifyDetail());
        }
        /*********** MCM Impl Team XS_16.24 ends *********/ 
        
  		//4.4 Submission agent...
		addTo(root, extractSource2Name());
		addTo(root, extractSource2Address());
		addTo(root, extractSource2Phone());
		addTo(root, extractSource2Fax());
        
        //Qualify rate
        //==========================================================================
        // Qualify Rate
        //==========================================================================
        addTo(root, extractQualifyRate());
        
        //==========================================================================
        // Qualify Product
        //==========================================================================
        addTo(root, extractQualifyProduct());

    }

    protected Node extractApplicantDetailsSection() {
        Node elemNode = null;

        try
        {
            List applicants = getBorrowers();

            if (applicants.isEmpty())
               return null;

             // --------------------- 6-Oct-04
             // Catherine for Desjardins: sort borrowers in a usual DJ-specific manner
             boolean bSortBorrowers =
                 PropertiesCache.getInstance().
                     getProperty(srk.getExpressState().getDealInstitutionId(), "com.filogix.docprep.dealsummarylite.sortBorrowers", "N").equalsIgnoreCase("Y");
            if (bSortBorrowers) {
              Collections.sort(applicants, new DJBorrowerOrderComparator());
            }
            // --------------------- end

            Collections.sort(applicants, new PrimaryBorrowerComparator());

            Iterator i = applicants.iterator();
            String name, val;

            elemNode = doc.createElement("ApplicantDetails");

            while (i.hasNext())
            {
                Borrower b = (Borrower)i.next();

                Node lineNode = doc.createElement("Applicant");

                //  Primary Borrower?
                addTo(lineNode, "PrimaryBorrower", b.isPrimaryBorrower() ? "Y" : "N");

                // Catherine for DJ: this is done to allower borrowers sorting inside the template
                addTo(lineNode, "borrowerId", "" + b.getBorrowerId());
                //  -------------- Catherine for DJ #620 --------------
                // Guarantor, bankruptcy status, guarantorotherLoans
                addTo(lineNode, "borrowerTypeId", "" + b.getBorrowerTypeId());
                addTo(lineNode, "bankruptcyStatusID", ""+ b.getBankruptcyStatusId());
                addTo(lineNode, "guarantorOtherLoans", ""+ b.getGuarantorOtherLoans());
                //  -------------- Catherine for DJ #620 end --------------

                //  Applicant Name
                name = new String();

                if (!isEmpty(b.getBorrowerFirstName()))
                   name += format.space() + b.getBorrowerFirstName();

                if (!isEmpty(b.getBorrowerMiddleInitial()))
                   name += format.space() + b.getBorrowerMiddleInitial();

                if (!isEmpty(b.getBorrowerLastName()))
                   name += format.space() + b.getBorrowerLastName();

                String suffix = BXResources.getPickListDescription(srk
                        .getExpressState().getDealInstitutionId(),
                        "SUFFIX", b.getSuffixId(), lang);
                
                if (!isEmpty(suffix))
                    name += format.space() + suffix;
                
                addTo(lineNode, "Name", name);

                //  Age
                addTo(lineNode, "Age", Integer.toString(b.getAge()));

                //  Net Worth
                addTo(lineNode, "NetWorth", formatCurrency(b.getNetWorth()));

                //  Total Income
                addTo(lineNode, "TotalIncome", formatCurrency(b.getTotalIncomeAmount()));

                //  Marital Status
                //val = PicklistData.getDescription("MaritalStatus", b.getMaritalStatusId());
                //ALERT:BX:ZR Release2.1.docprep
                val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "MaritalStatus", b.getMaritalStatusId() , lang);

                addTo(lineNode, "MaritalStatus", val);

                //  Address
                BorrowerAddress ba = new BorrowerAddress(srk);

                // Catherine, Oct 1, 04 ----------------------------
                // fix a problem when data extraction fails if there is no primary addres for a borrower
                try {
                  ba.findByCurrentAddress(b.getBorrowerId(), b.getCopyId());
                  Node addrNode = doc.createElement("Address");
                  Addr addr = ba.getAddr();

                  addTo(addrNode, "Line1", addr.getAddressLine1());
                  addTo(addrNode, "Line2", addr.getAddressLine2());
                  addTo(addrNode, "City", addr.getCity());

                  Province prov = new Province(srk, addr.getProvinceId());

                  addTo(addrNode, "Province", prov.getProvinceName());

                  //  PostalCode
                  addTo(addrNode, extractPostalCode(addr));

                  //  Phone number
                  val = StringUtil.getAsFormattedPhoneNumber(b.
                      getBorrowerHomePhoneNumber(), null);

                  addTo(addrNode, "PhoneNumber", val);

                  addTo(lineNode, addrNode);
                } catch (FinderException fe){}

                // Catherine, Oct 1, 04 end ----------------------------

                //  ASSETS
                addTo(lineNode, extractAssetsSection(b));

                //  LIABILITIES
                addTo(lineNode, extractLiabilitiesSection(b));

                // other income,
                //  -------------- Catherine for DJ #620  --------------
                addTo(lineNode, extractOtherIncome(b));
                //  -------------- Catherine for DJ #620 end --------------

                //  EMPLOYMENT INFO
                addTo(lineNode, extractApplicantEmploymentSection(b));

                elemNode.appendChild(lineNode);
            }
        }
        catch (Exception e){
          e.printStackTrace();
        }

        return elemNode;
    }


    protected Node extractBureauSummary()
    {
        String val = null;

        try
        {
            Borrower b = getPrimaryBorrower();

            val = b.getCreditBureauSummary();
        }
        catch (Exception e){}

        return (val == null ? null :
                convertCRToLines(doc.createElement("BureauSummary"), val));
    }

    protected Node extractBrokerNotes()
    {
        Node elemNode = null;

        try
        {
        	//#DG776
            //List notes = (List)deal.getDealNotes();
        	DealNotes dn = new DealNotes(srk);
            Collection notes = dn.findByDealLang((DealPK) deal.getPk(), lang, LANGUAGE_UNKNOW);

            if (notes.isEmpty())
               return null;

            Iterator i = notes.iterator();

            while (i.hasNext())
            {
                dn = (DealNotes)i.next();

                if (dn.getDealNotesCategoryId() == Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION)
                {
                    if (elemNode == null)
                       elemNode = doc.createElement("BrokerNotes");

                    addTo(elemNode,
                       convertCRToLines(doc.createElement("Note"), dn.getDealNotesText()));
                }
            }
        }
        catch (Exception e){}

        return elemNode;
    }

    protected Node extractMIPremium()
    {
        String val = null;

        try
        {
            val = formatCurrency(deal.getMIPremiumAmount());
        }
        catch (Exception e){}

        return getTag("MIPremium", val);
    }
    
    // Tracker #2800 - Sasa
    // Hijacked method from the base GENXExtractor class.
    // This class is used as a base class for all the other documets and changing its method
    // would affect those documents as well. We would have to investigate possible implications
    // but we didn't get request to change those. This is why this method is overwritten here.
    //
    protected Node extractAddressLine1(Property p, String tagName ) {
        String val = null;

        try {
            val = "";

            if(  !isEmpty(p.getUnitNumber())  ){
              val += p.getUnitNumber();
              val += "-";
            }

            if (!isEmpty(p.getPropertyStreetNumber()))
               val += p.getPropertyStreetNumber();

            if( lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ) {
            	if (!isEmpty(p.getPropertyStreetName()))    val += format.space() + p.getPropertyStreetName();
            	if (!isEmpty(p.getStreetTypeDescription())) val += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",  p.getStreetTypeId() , lang);
            }
            else {
            	if (!isEmpty(p.getStreetTypeDescription())) val += format.space() + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",  p.getStreetTypeId() , lang);
            	if (!isEmpty(p.getPropertyStreetName()))    val += format.space() + p.getPropertyStreetName();
            }

            if (p.getStreetDirectionId() != 0)
               val += format.space() +
                        BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection",  p.getStreetDirectionId() , lang);
        }
        catch (Exception e){}

        return getTag(tagName, val);
    }    
    
    /*********** MCM Impl Team XS_16.24 starts **********/ 
    /**
     * This method returns the ComponentSummary
     * 
     * @version 1.0 28-July-2008 XS_16.24 Initial version
     */

    private ComponentSummary compSummary = null;
    private Component comp = null;

    /**
     * Description : This Method return ComponentSummary
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     * @version 1.1 ticket artf765172 fix
     */
    protected ComponentSummary getComponentSummary() {

        try {
            compSummary = new ComponentSummary(srk);
            ComponentSummaryPK compSumPk = new ComponentSummaryPK(deal
                    .getDealId(), deal.getCopyId());
            compSummary = compSummary.findByPrimaryKey(compSumPk);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return compSummary;
    }

    /**
     * Description : This Method extract totalAmount from ComponentSummary
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    protected Node extractTotalAmount(ComponentSummary p) {
        String val = null;
        try {
            val = formatCurrency(p.getTotalAmount());
        } catch (Exception e) {
        }
        return getTag("TotalAmountComp", val);
    }

    /**
     * Description : This Method extract totalcashbackAmount from
     * ComponentSummary
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    protected Node extractTotalCashBackAmount(ComponentSummary p) {
        String val = null;
        try {
            val = formatCurrency(p.getTotalCashbackAmount());
        } catch (Exception e) {
        }

        return getTag("TotalCashBackAmount", val);
    }

    /**
     * Description : This Method extract unallocate Amount from
     * ComponentSummary
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    protected Node extractUnallocateAmount(ComponentSummary p) throws Exception {
        String val = null;

        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Deal deal = new Deal(srk, null);
        deal.findByPrimaryKey(dpk);

        try {
            val = formatCurrency(deal.getTotalLoanAmount() - p.getTotalAmount());
        } catch (Exception e) {
        }

        return getTag("UnallocateAmount", val);
    }

    /**
     * Description : This Method get components from deal
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    protected Collection getComponent() {

        Collection con = null;
        try {
            comp = new Component(srk);
            DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
            con = comp.findByDeal(dpk);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }


    /**
     * Description : This Method extract all component from deal
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    protected Node extractComponent() throws Exception {
        Node elemNode = null;

        Collection con = getComponent();
        // elemNode = doc.createElement("ComponentDetails");
        elemNode = doc.createElement("Component");
        Iterator iter = con.iterator();

        while (iter.hasNext()) {
            Component comp = (Component) iter.next();

            int componentType = comp.getComponentTypeId();

            switch (componentType) {
            case Mc.COMPONENT_TYPE_MORTGAGE: {
                Node mort = extractMortgageComponent(comp);
                addTo(elemNode, mort);
                break;
            }
            case Mc.COMPONENT_TYPE_LOC: {
                Node loc = extractLocComponent(comp);
                addTo(elemNode, loc);
                break;
            }
            case Mc.COMPONENT_TYPE_LOAN: {
                Node loan = extractLoanComponent(comp);
                addTo(elemNode, loan);
                break;
            }
            case Mc.COMPONENT_TYPE_CREDITCARD: {
                Node cc = extractCreditCardComponent(comp);
                addTo(elemNode, cc);
                break;
            }
            case Mc.COMPONENT_TYPE_OVERDRAFT: {
                Node od = extractOverdraftComponent(comp);
                addTo(elemNode, od);
                break;
            }
            default: {
                // for default we don't extract component.
                break;
            }
            }
        }
        return elemNode;
    }


    /**
     * Description : This Method extract MortgageComponent 
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     * @version 1.1 20-Aug-2008 fix for PropertyTaxEscrowAmount
     */
    private Node extractMortgageComponent(Component comp) throws Exception {

        ComponentMortgage compMort = new ComponentMortgage(srk);
        ComponentMortgagePK cpk = new ComponentMortgagePK(comp
                .getComponentId(), comp.getCopyId());
        compMort = compMort.findByPrimaryKey(cpk);

        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Deal deal = new Deal(srk, null);
        deal.findByPrimaryKey(dpk);

        MtgProd mtg = new MtgProd(srk, null);
        mtg.findByPrimaryKey(new MtgProdPK(comp.getMtgProdId()));

        Node compNode = doc.createElement("Mortgage");
        addTo(compNode, "ComponentTypeMortgage", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "COMPONENTTYPE", comp
                        .getComponentTypeId(), lang));
        addTo(compNode, "ProductMortgage", BXResources.getPickListDescription(
                srk.getExpressState().getDealInstitutionId(), "MTGPROD", comp
                        .getMtgProdId(), lang));
        addTo(compNode, "MortgageAmount", ""
                + formatCurrency(compMort.getMortgageAmount()));
        addTo(compNode, "PostedRateMortgage", ""
                + formatInterestRate(comp.getPostedRate()));
        
        double miPremiumAmount = deal.getMIPremiumAmount();
        if (compMort.getMiAllocateFlag() != null
                && !compMort.getMiAllocateFlag().equalsIgnoreCase("Y"))
            miPremiumAmount = 0;
        
        addTo(compNode, "MiPremiumMortgage", formatCurrency(miPremiumAmount));
        addTo(compNode, "DiscountMortgage", ""
                + formatInterestRate(compMort.getDiscount()));

        String miAllocateFlag = "";
        if (compMort.getMiAllocateFlag() != null) {
            String label = (compMort.getMiAllocateFlag().equalsIgnoreCase("Y") ? Xc.YES_LABEL
                    : Xc.NO_LABEL);
            miAllocateFlag = BXResources.getGenericMsg(label, lang);
        }

        addTo(compNode, "AllocateMiPremiumMortgage", miAllocateFlag);

        addTo(compNode, "PremiumMortgage",
                formatInterestRate(compMort.getPremium()));
        addTo(compNode, "TotalLocAmountMortgage", formatCurrency(compMort
                .getTotalMortgageAmount()));
        addTo(compNode, "BuyDownRateMortgage", formatInterestRate(compMort
                .getBuyDownRate()));

        addTo(compNode, "AmoritizationPeriodMortgage", DocPrepLanguage
                .getYearsAndMonths(compMort.getAmortizationTerm(), lang));
        addTo(compNode, "NetRateMortgage", formatInterestRate(compMort
                .getNetInterestRate()));

        addTo(compNode, "EffectiveAmoritizationMortgage", DocPrepLanguage
                .getYearsAndMonths(compMort.getEffectiveAmortizationMonths(),
                        lang));
        addTo(compNode, "PIPaymentMortgage", formatCurrency(compMort
                .getPAndIPaymentAmount()));
        addTo(compNode, "PaymentTermMortgage", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "PAYMENTTERM", mtg
                        .getPaymentTermId(), lang));
        addTo(compNode, "AdditionalPaymentMortgage", formatCurrency(compMort
                .getAdditionalPrincipal()));
        addTo(compNode, "ActualPaymentTermMortgage", DocPrepLanguage
                .getYearsAndMonths(compMort.getActualPaymentTerm(), lang));

        String taxEscrow = "";
        if (compMort.getPropertyTaxAllocateFlag() != null) {
            String label = (compMort.getPropertyTaxAllocateFlag().equalsIgnoreCase("Y") ? Xc.YES_LABEL
                    : Xc.NO_LABEL);
            taxEscrow = BXResources.getGenericMsg(label, lang);
        }
         
        addTo(compNode, "AllocateTaxEscrowMortgage", taxEscrow);

        addTo(compNode, "PaymentFreqMortgage", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "PAYMENTFREQUENCY", compMort
                        .getPaymentFrequencyId(), lang));
        
        double escrowAmount = compMort.getPropertyTaxEscrowAmount();
        if (compMort.getPropertyTaxAllocateFlag() != null
                && !compMort.getPropertyTaxAllocateFlag().equalsIgnoreCase("Y"))
            escrowAmount = 0;
        addTo(compNode, "TaxEscrowMortgage", formatCurrency(escrowAmount));
        
        addTo(compNode, "PrePaymentOptionMort", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "PREPAYMENTOPTIONS", compMort
                        .getPaymentFrequencyId(), lang));
        addTo(compNode, "TotalPaymentMort", formatCurrency(compMort
                .getTotalPaymentAmount()));
        addTo(compNode, "PriPaymentOptionMort", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "PRIVILEGEPAYMENT", compMort
                        .getPrivilegePaymentId(), lang));
        addTo(compNode, "HoldBackAmountMort", formatCurrency(compMort
                .getAdvanceHold()));

        addTo(compNode, "CommissionCodeMort", compMort.getCommissionCode());
        addTo(compNode, "RatePeriodMort", ""
                + compMort.getRateGuaranteePeriod());

        addTo(compNode, "CashBackMort", formatInterestRate(compMort
                .getCashBackPercent()));

        String indicator = "";
        if (compMort.getExistingAccountIndicator() != null) {
            String label = (compMort.getExistingAccountIndicator().equalsIgnoreCase("Y") ? Xc.YES_LABEL
                    : Xc.NO_LABEL);
            indicator = BXResources.getGenericMsg(label, lang);
        }
        
        addTo(compNode, "ExistingAccountMort", indicator);
        
        addTo(compNode, "CashBackAmountMort", formatCurrency(compMort.getCashBackAmount()));
        addTo(compNode, "ExistingAccountRefMort", compMort.getExistingAccountNumber());

        addTo(compNode, "RepaymentTypeMort", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "REPAYMENTTYPE", comp
                        .getRepaymentTypeId(), lang));

        addTo(compNode, "FirstPaymentDateMort", formatDate(compMort
                .getFirstPaymentDate()));

        String rateLock = "";
        if (compMort.getRateLock() != null) {
            String label = (compMort.getRateLock().equalsIgnoreCase("Y") ? Xc.YES_LABEL
                    : Xc.NO_LABEL);
            rateLock = BXResources.getGenericMsg(label, lang);
        }
        
        addTo(compNode, "RateLockInMort", rateLock);
        addTo(compNode, "MaturityDateMort", formatDate(compMort
                .getMaturityDate()));

        addTo(compNode, "AdditionalDetailsMort", comp
                .getAdditionalInformation());

        return compNode;
    }


    /**
     * Description : This Method extract LOC Component 
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    private Node extractLocComponent(Component comp) throws Exception {
        ComponentLOC compLoc = new ComponentLOC(srk);
        ComponentLOCPK cpk = new ComponentLOCPK(comp
                .getComponentId(), comp.getCopyId());
        compLoc = compLoc.findByPrimaryKey(cpk);

        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Deal deal = new Deal(srk, null);
        deal.findByPrimaryKey(dpk);

        Node compNode = doc.createElement("Loc");
        addTo(compNode, "ComponentTypeLoc", BXResources.getPickListDescription(
                srk.getExpressState().getDealInstitutionId(), "ComponentType",
                comp.getComponentTypeId(), lang));
        addTo(compNode, "ProductLoc", BXResources.getPickListDescription(srk
                .getExpressState().getDealInstitutionId(), "MTGPROD", comp
                .getMtgProdId(), lang));
        addTo(compNode, "LocAmount", formatCurrency(compLoc.getLocAmount()));
        addTo(compNode, "PostedRateLoc", formatInterestRate(comp
                .getPostedRate()));

        double miPremiumAmount = deal.getMIPremiumAmount();
        if (compLoc.getMiAllocateFlag() != null
                && !compLoc.getMiAllocateFlag().equalsIgnoreCase("Y"))
            miPremiumAmount = 0;
        
        addTo(compNode, "MiPremiumLoc", formatCurrency(miPremiumAmount));
        addTo(compNode, "DiscountLoc",
                formatInterestRate(compLoc.getDiscount()));

        String miPremium = "";
        if (compLoc.getMiAllocateFlag() != null) {
            String label = (compLoc.getMiAllocateFlag().equalsIgnoreCase("Y") ? Xc.YES_LABEL
                    : Xc.NO_LABEL);
            miPremium = BXResources.getGenericMsg(label, lang);
        }
        
        addTo(compNode, "AllocateMiPremiumLoc", miPremium);

        addTo(compNode, "PremiumLoc", formatInterestRate(compLoc.getPremium()));
        addTo(compNode, "TotalLocAmountLoc", formatCurrency(compLoc.getTotalLocAmount()));
        addTo(compNode, "NetRateLoc", formatInterestRate(compLoc
                .getNetInterestRate()));
        addTo(compNode, "PaymentFrequencyLoc", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "PAYMENTFREQUENCY", compLoc
                        .getPaymentFrequencyId(), lang));
        addTo(compNode, "InterestOnlyPaymentLoc", formatCurrency(compLoc
                .getPAndIPaymentAmount()));

        addTo(compNode, "CommissionCodeLoc", compLoc.getCommissionCode());

        String propertyTax = "";
        if (compLoc.getPropertyTaxAllocateFlag() != null) {
            String label = (compLoc.getPropertyTaxAllocateFlag().equalsIgnoreCase("Y") ? Xc.YES_LABEL
                    : Xc.NO_LABEL);
            propertyTax = BXResources.getGenericMsg(label, lang);
        }
        addTo(compNode, "AllocateTaxEscrowLoc", propertyTax);

        addTo(compNode, "RepaymentTypeLoc", BXResources.getPickListDescription(
                srk.getExpressState().getDealInstitutionId(), "REPAYMENTTYPE",
                comp.getRepaymentTypeId(), lang));

        double escrowAmount = compLoc.getPropertyTaxEscrowAmount();
        if (compLoc.getPropertyTaxAllocateFlag() != null
                && !compLoc.getPropertyTaxAllocateFlag().equalsIgnoreCase("Y"))
            escrowAmount = 0;

        addTo(compNode, "TaxEscrowLoc", formatCurrency(escrowAmount));

        String accountLoc = "";
        if (compLoc.getExistingAccountIndicator() != null) {
            String label = (compLoc.getExistingAccountIndicator().equalsIgnoreCase("Y") ? Xc.YES_LABEL
                    : Xc.NO_LABEL);
            accountLoc = BXResources.getGenericMsg(label, lang);
        }        
        
        addTo(compNode, "ExistingAccountLoc", accountLoc);

        addTo(compNode, "TotalPaymentLoc", formatCurrency(compLoc
                .getTotalPaymentAmount()));
        addTo(compNode, "AccountRefLoc", compLoc.getExistingAccountNumber());
        addTo(compNode, "HoldbackAmountLoc", formatCurrency(compLoc
                .getAdvanceHold()));

        addTo(compNode, "FirstPaymentDateLoc", formatDate(compLoc
                .getFirstPaymentDate()));
        addTo(compNode, "AdditionalDetailsLoc", comp.getAdditionalInformation());
        return compNode;
    }


    /**
     * Description : This Method extract Loan Component 
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    private Node extractLoanComponent(Component comp) throws Exception {
        ComponentLoan compLoan = new ComponentLoan(srk);
        ComponentLoanPK cpk = new ComponentLoanPK(comp.getComponentId(), comp
                .getCopyId());
        compLoan.findByPrimaryKey(cpk);

        MtgProd mtg = new MtgProd(srk, null);
        mtg.findByPrimaryKey(new MtgProdPK(comp.getMtgProdId()));

        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Deal deal = new Deal(srk, null);
        deal.findByPrimaryKey(dpk);
        Node compNode = doc.createElement("Loan");
        addTo(compNode, "ComponentTypeLoan", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "ComponentType", comp
                        .getComponentTypeId(), lang));
        addTo(compNode, "ProductLoan", BXResources.getPickListDescription(srk
                .getExpressState().getDealInstitutionId(), "MTGPROD", comp
                .getMtgProdId(), lang));
        addTo(compNode, "LoanAmount", formatCurrency(compLoan.getLoanAmount()));
        addTo(compNode, "PostedRateLoan", formatInterestRate(comp
                .getPostedRate()));

        addTo(compNode, "PaymentTermLoan", BXResources.getPickListDescription(
                srk.getExpressState().getDealInstitutionId(), "PAYMENTTERM",
                mtg.getPaymentTermId(), lang));

        addTo(compNode, "DiscountLoan", formatInterestRate(compLoan
                .getDiscount()));

        addTo(compNode, "ActualPaymentTermLoan", DocPrepLanguage.getYearsAndMonths(
                compLoan.getActualPaymentTerm(), lang));
        addTo(compNode, "PremiumLoan",
                formatInterestRate(compLoan.getPremium()));
        addTo(compNode, "PaymentFrequencyLoan", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "PAYMENTFREQUENCY", compLoan
                        .getPaymentFrequencyId(), lang));
        addTo(compNode, "NetRateLoan", formatInterestRate(compLoan
                .getNetInterestRate()));
        addTo(compNode, "TotalPaymentLoan", formatCurrency(compLoan
                .getPAndIPaymentAmount()));

        addTo(compNode, "AdditionalDetailsLoan", comp
                .getAdditionalInformation());

        return compNode;
    }


    /**
     * Description : This Method extract Credit Card Component 
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    private Node extractCreditCardComponent(Component comp) throws Exception {
        ComponentCreditCard compCC = new ComponentCreditCard(srk);
        ComponentCreditCardPK cpk = new ComponentCreditCardPK(comp
                .getComponentId(), comp.getCopyId());
        compCC.findByPrimaryKey(cpk);

        MtgProd mtg = new MtgProd(srk, null);
        mtg.findByPrimaryKey(new MtgProdPK(comp.getMtgProdId()));

        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Deal deal = new Deal(srk, null);
        deal.findByPrimaryKey(dpk);
        Node compNode = doc.createElement("CreditCard");
        addTo(compNode, "ComponentTypeCC", BXResources.getPickListDescription(
                srk.getExpressState().getDealInstitutionId(), "ComponentType",
                comp.getComponentTypeId(), lang));
        addTo(compNode, "ProductCC", BXResources.getPickListDescription(srk
                .getExpressState().getDealInstitutionId(), "MTGPROD", comp
                .getMtgProdId(), lang));
        addTo(compNode, "CreditCardAmount", formatCurrency(compCC
                .getCreditCardAmount()));
        addTo(compNode, "InterestRateCC", formatInterestRate(comp
                .getPostedRate()));

        addTo(compNode, "AdditionalDetailsCC", comp.getAdditionalInformation());

        return compNode;
    }


    /**
     * Description : This Method extract Overdraft Component 
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    private Node extractOverdraftComponent(Component comp) throws Exception {
        ComponentOverdraft compOverDraft = new ComponentOverdraft(srk);
        ComponentOverdraftPK cpk = new ComponentOverdraftPK(comp
                .getComponentId(), comp.getCopyId());
        compOverDraft.findByPrimaryKey(cpk);

        MtgProd mtg = new MtgProd(srk, null);
        mtg.findByPrimaryKey(new MtgProdPK(comp.getMtgProdId()));

        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Deal deal = new Deal(srk, null);
        deal.findByPrimaryKey(dpk);
        Node compNode = doc.createElement("OverDraft");
        addTo(compNode, "ComponentTypeOverDraft", BXResources
                .getPickListDescription(srk.getExpressState()
                        .getDealInstitutionId(), "ComponentType", comp
                        .getComponentTypeId(), lang));
        addTo(compNode, "ProductOverDraft", BXResources.getPickListDescription(
                srk.getExpressState().getDealInstitutionId(), "MTGPROD", comp
                        .getMtgProdId(), lang));
        addTo(compNode, "OverDraftAmount", formatCurrency(compOverDraft
                .getOverDraftAmount()));
        addTo(compNode, "InterestRateOverDraft", formatInterestRate(comp
                .getPostedRate()));

        addTo(compNode, "AdditionalDetailsOverDraft", comp
                .getAdditionalInformation());

        return compNode;
    }


    /**
     * Description : This Method extract Qualify Detail 
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     */
    protected Node extractQualifyDetail() throws Exception {

        QualifyDetail qf = new QualifyDetail(srk);
        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Collection con = qf.findByDeal(dpk);
        
        Iterator iter = con.iterator();
        Node elemNode = null;
        while (iter.hasNext()) {
            QualifyDetail qfIter = (QualifyDetail) iter.next();
            elemNode = doc.createElement("QualifyDetail");
            addTo(elemNode, "QualifyRate", ""
                    + formatInterestRate(qfIter.getQualifyRate()));
            addTo(elemNode, "QualifyPayment", formatCurrency(qfIter
                    .getPAndIPaymentAmountQualify()));

            String qualifyPeriod = null;

            if (qfIter.getInterestCompoundingId() == Mc.INTEREST_COMPOUNDID_MONTHLY)
                qualifyPeriod = "Monthly";
            else if (qfIter.getInterestCompoundingId() == Mc.INTEREST_COMPOUNDID_SEMIANNUAL)
                qualifyPeriod = "Semi-Annually";

            addTo(elemNode, "QualifyPeriod", qualifyPeriod);

            addTo(elemNode, "QualifyGDS", formatRatio(qfIter.getQualifyGds()));
            addTo(elemNode, "QualifyAmortization", DocPrepLanguage
                    .getYearsAndMonths(qfIter.getAmortizationTerm(), lang));
            addTo(elemNode, "QualifyTDS", formatRatio(qfIter.getQualifyTds()));
            addTo(elemNode, "QualifyRepayment", BXResources
                    .getPickListDescription(srk.getExpressState()
                            .getDealInstitutionId(), "REPAYMENTTYPE", qfIter
                            .getRepaymentTypeId(), lang));
        }
        return elemNode;
    }
    

    /**
     * Description : This Method extract Qualify Detail Title
     * 
     * @version 1.0 XS_16.24 07-Aug-2008 Initial Vesrion
     * @version 1.1 XS_16.23 14-Aug-2008 
     * @version 1.2 FXP24069 ticket fix
     */
    protected Node extractQualifyTitle() {

        Node elemNode = null;
        QualifyDetail qualifyDetail = null;       
        try {
            qualifyDetail = new QualifyDetail(srk, deal.getDealId());
        } catch (FinderException e) {
            elemNode = doc.createElement("QualifyTitle");
            addTo(elemNode, "QualifyBanner", "NA");
        } catch (RemoteException e) {        
            e.printStackTrace();
        }
        return elemNode;
    }
    /*********** MCM Impl Team XS_16.24 ends *********/ 
    
    //4.4 Submission Agent
    protected Node extractSource2Name()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Name", "");

      	  	SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Name", "");
      	  	else
      	  	{
	            if (sobp.getContact() != null)
	            {
	                Contact c = sobp.getContact();
	
	                if (!isEmpty(c.getContactFirstName()) &&
	                    !isEmpty(c.getContactLastName()))
	                {
	                    val = new String();
	
	                    if (!isEmpty(c.getContactFirstName()))
	                       val += c.getContactFirstName() + format.space();
	
	                    if (!isEmpty(c.getContactLastName()))
	                       val += c.getContactLastName();
	                }
	            }
      	  	}
        }
        catch (Exception e){}

        return getTag("Source2Name", val);
    }

    protected Node extractSource2Address()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Address", "");
      	  	
      	  		SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Address", "");
      	  	else
      	  	{
      	  		Contact contact = sobp.getContact();
      	  		Addr addr = contact.getAddr();

      	  		val = new String();

	            if (!isEmpty(addr.getAddressLine1()))
	               val += addr.getAddressLine1();
	
	            if (!isEmpty(addr.getCity()))
	               val += format.space() + addr.getCity();
	
	            Province prov = new Province(srk, addr.getProvinceId());
	
	            val += format.space() + prov.getProvinceAbbreviation();
      	  	}
        }
        catch (Exception e){}

        return getTag("SourceAddress", val);
    }

    
    protected Node extractSource2Phone()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Phone", "");
      	  	
        	SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Phone", "");
      	  	else
      	  	{
	            Contact contact = sobp.getContact();
	            //Addr addr = contact.getAddr();
	
	            val = StringUtil.getAsFormattedPhoneNumber(contact.getContactPhoneNumber(),
	                                                       contact.getContactPhoneNumberExtension());
      	  	}
        }
        catch (Exception e){}

        return getTag("Source2Phone", val);
    }

    protected Node extractSource2Fax()
    {
        String val = null;

        try
        {
      	  	if (deal.getSourceOfBusinessProfileId() == deal.getSourceOfBusinessProfile2ndaryId())
      	  		return getTag("Source2Fax", "");

        	SourceOfBusinessProfile sobp = deal.getSourceOfBusinessProfile2ndary();
      	  	if (sobp == null)
      	  		return getTag("Source2Fax", "");
      	  	else
      	  	{
	            Contact contact = sobp.getContact();
	            //Addr addr = contact.getAddr();
	
	            val = StringUtil.getAsFormattedPhoneNumber(contact.getContactFaxNumber(), null);
      	  	}
        }
        catch (Exception e){}

        return getTag("Source2Fax", val);
    }

}
