package com.basis100.deal.docprep.extract.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.basis100.deal.docprep.ExpressDocprepHelper;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class RatioFieldList {
  private boolean LOADED = false;
  private HashMap ratioMap;

  public RatioFieldList() {
    if (!(LOADED && (ratioMap != null))){
      ratioMap = new HashMap();
      load();
    }
    LOADED = true;
  }

  public boolean isRatioField(String entityName, String fieldName){
    if (ratioMap.containsKey(entityName.toUpperCase())){
      RatioFieldEntity rfe = (RatioFieldEntity)(ratioMap.get(entityName.toUpperCase()));
      return (rfe.exists(fieldName));
    }
    return false;
  }

  protected void load(){

    // 1) load XML
    DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();

    try
    {
      /**
         * Parses an XML document after reading a resource of the specified name from the search path.
      */
   	
      Document ratioDoc = parser.parse(ExpressDocprepHelper.getMdfmlAsStream("ratioFields.xml"));
   	
      Node doc = DomUtil.getFirstNamedChild(ratioDoc, "mos-ratio-fields");

      List entityList = DomUtil.getChildElements(doc);

      Element domElement;
      String entityName;

      Iterator it = entityList.iterator();

      while (it.hasNext()){
        domElement = (Element)it.next();
        entityName = domElement.getAttribute("name").toUpperCase();

        RatioFieldEntity ratioEntity = new RatioFieldEntity(entityName);
        // System.out.println("Entity = " + entityName);

        List fieldList = DomUtil.getChildElements(domElement);
        Iterator fit = fieldList.iterator();
        Element fieldElement;
        String fldName;

        while (fit.hasNext()){
          fieldElement = (Element)fit.next();
          fldName = DomUtil.getElementText(fieldElement);
          ratioEntity.add(fldName);
          // System.out.println("        ratio field = " + fldName);
        }
        ratioMap.put(entityName, ratioEntity);
      }

    } catch (Exception e) {
      System.out.print("Error: ");
      e.printStackTrace();
    }
  }

  /** prints list of  ratio fields for a given entity
   *
   */

  public void printFieldsByEntity(String entityName){
    System.out.println("to be implemented");
  }

  /** prints all ratio fields
   *
   */

  public void printFields(){
    System.out.println("to be implemented");
  }

  class RatioFieldEntity {
    String entityName;
    ArrayList fieldList;

    RatioFieldEntity(String name){
      this.entityName = name;
      this.fieldList = new ArrayList();
    }

    void add(String fieldName){
      this.fieldList.add(fieldName);
    }

    boolean exists(String fieldName){
      return this.fieldList.contains(fieldName);
    }
  }
}