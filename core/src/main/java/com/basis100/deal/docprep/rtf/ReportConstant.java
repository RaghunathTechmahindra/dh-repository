package com.basis100.deal.docprep.rtf;

public class ReportConstant {
  private String constName;
  private String constText;

  public ReportConstant(String pName, String pText){
    constName = new String( pName.trim().toLowerCase() );
    constText = new String( pText );
  }

  public String getConstName(){
    return constName;
  }

  public String getConstText(){
    return constText;
  }

  /**
   * Note that this method trims the parameter and makes it lowercase.
  */
  public void setConstName(String nameToSet){
    constName = new String(nameToSet.trim().toLowerCase());
  }

  public void setConstText(String textToSet){
    constText = new String(textToSet);
  }
}