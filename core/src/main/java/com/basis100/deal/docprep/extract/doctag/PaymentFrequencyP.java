package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

public class PaymentFrequencyP extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());
    int id;
    String val = "";
    FMLQueryResult fml = new FMLQueryResult();
    
    int clsid = de.getClassId();

    try
    {
    	if(clsid == ClassId.DEAL)
        {	
    		Deal deal = (Deal)de;
    		id = deal.getPaymentFrequencyId();
    		val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency", id, lang);
        }
    	else if(clsid == ClassId.PROPERTY)
    	{
    		Property Pro = (Property)de;
    		Deal deal = new Deal(srk,null,Pro.getDealId(),Pro.getCopyId());
    	    deal = deal.findByPrimaryKey((DealPK)deal.getPk());    

    	      
    	    if(deal != null)
    	    {
    	    	id = deal.getPaymentFrequencyId();
    	    	val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentFrequency", id, lang);
    	    }
    	}
    	
    	fml.addValue(val, fml.TERM);
      
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(FinderException fe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
