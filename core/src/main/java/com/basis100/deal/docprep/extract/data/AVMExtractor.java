package com.basis100.deal.docprep.extract.data;

import java.io.File;
import java.io.FileWriter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.basis100.deal.docprep.ExpressDocprepHelper;
import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docprep.extract.ExtractDom;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.UploadBuilder;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Request;

import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;

public class AVMExtractor {

	private Request mRequest;

	private int mLangId;
	
	private Deal mDeal;	
	private SysLogger mLogger;
	private SessionResourceKit mSrk;
	private DocumentProfile mProfile;
	
	private String mResourcePath;
	
	private UploadBuilder mUploadBuilder;
	
	private Document mDoc;

	
	/**
	 * Constructor
	 *
	 */
	public AVMExtractor() {}
	
	public AVMExtractor(SessionResourceKit pSrk, int pRequestId,  int copyId, int pLangId ) throws Exception {
		
		try {
			mRequest = new Request(pSrk, pRequestId, copyId);

			mLangId = pLangId;
			
			mSrk = pSrk;	
			mLogger = pSrk.getSysLogger();
			mDeal = new Deal(pSrk, null, mRequest.getDealId(), mRequest.getCopyId());

			int docTypeId = mRequest.findDocumentTypeId(mRequest.getPayloadTypeId());
			mProfile = (new DocumentProfile(pSrk)).findByDocumentTypeId(docTypeId);
			String rootDir;
			mResourcePath = mProfile.getDocumentSchemaPath().toLowerCase();

 		    rootDir = ExpressDocprepHelper.getMdfmlFolder();

		    if(rootDir.endsWith("/") || rootDir.endsWith("\\"))  
		    	mResourcePath = rootDir + mResourcePath;  
		     else  
		    	 mResourcePath = rootDir + "/"+ mResourcePath; 
			
			mUploadBuilder = new UploadBuilder(new ExtractDom(new File(mResourcePath)), mSrk);
		} 
		catch (Exception e) {
			mLogger.error(StringUtil.stack2string(e));
		    throw new DocPrepException("DOCPREP: AVMextractor failed: " + e.getMessage());			
		}
	}	
	
	
	public String buildXML() throws Exception {
		String xmlDoc = null;
		
		Document doc = extractDom();
		mDoc = doc;
		createServiceRequestNode(doc);
		xmlDoc = mUploadBuilder.toXMLString(doc, mLangId);
		
		return xmlDoc;
	}
	
	/**
	 * extractDom - gets the original upload DOM
	 *
	 * @throws ExtractException
	 */
	private Document extractDom() throws ExtractException {
	    Document doc = null;
	    try {
	      doc = mUploadBuilder.generateDocument(mDeal.getDealId(), mDeal.getCopyId(), mLangId);
	      return doc;
	    } 
	    catch (Exception e) {
	      mLogger.error(StringUtil.stack2string(e));
	      String msg = e.getMessage();
	      mLogger.error(msg);
	      throw new ExtractException("DOCPREP: AVMextractor failed: " + msg);
	    }
	}
	
	/**
	 * 
	 * @param pRequestId
	 * @throws Exception
	 */
	private Node createServiceRequestNode(Document pDoc) throws Exception {
		
		mSrk.getSysLogger().debug(this.getClass(), "Starting AVM extraction.");
		AVMDataProvider avm = new AVMDataProvider(mSrk, mRequest, mLangId);
		avm.setDomDocument(mDoc);
		
		Element srRoot = mDoc.createElement("ServiceRequest");
		
//		avm.addTo(srRoot, "serviceRequestId", Integer.toString(mRequest.getRequestId()));
//
//		avm.addTo(srRoot, "serviceProviderId", Integer.toString(avm.exServiceProviderId()));
//		
//		avm.addTo(srRoot, "serviceProviderName", avm.exServiceProviderName());
//		avm.addTo(srRoot, "serviceTypeId", Integer.toString(avm.exServiceTypeId()));
//		avm.addTo(srRoot, "serviceTypeName", avm.exServiceTypeName());
//		avm.addTo(srRoot, "serviceSubTypeId",Integer.toString(avm.exServiceSubTypeId()));
//		avm.addTo(srRoot, "serviceSubTypeName", avm.exServiceSubTypeName());
//		avm.addTo(srRoot, "serviceSubSubTypeId", Integer.toString(avm.exServiceSubSubTypeId()));
//		avm.addTo(srRoot, "serviceSubSubTypeName", avm.exServiceSubSubTypeName());
//		avm.addTo(srRoot, "productName", avm.exProductName());
//		avm.addTo(srRoot, "transactionTypeId", Integer.toString(avm.exTransactionTypeId()));
//		avm.addTo(srRoot, "transactionTypeName", avm.exTransactionTypeName());
//		avm.addTo(srRoot, "datxTimeout", Integer.toString(avm.exDatxTimeout()));
//		avm.addTo(srRoot, "serviceProviderfRefNum", avm.exServiceProviderRefNo());
//
//		Element requestData = pDoc.createElement("serviceRequestData");
//		
//		//ServiceRequestContact table
//		Element contactInfo = pDoc.createElement("contactInfo");
//		avm.addTo(contactInfo, "firstName", avm.exSRContactFirstName());
//		avm.addTo(contactInfo, "lastName", avm.exSRContactLastName());
//		avm.addTo(contactInfo, "email", avm.exSRContactEmail());
//		avm.addTo(contactInfo, "homePhoneNumber", avm.exSRContactHomePhone());
//		avm.addTo(contactInfo, "workPhoneNumber", avm.exSRContactWorkPhone());
//		avm.addTo(contactInfo, "workPhoneExtension", avm.exSRContactWorkExtension());
//		avm.addTo(contactInfo, "cellPhoneNumber", avm.exSRContactCellPhone());
//		avm.addTo(contactInfo, "comment", avm.exSRContactComments());
//		requestData.appendChild(contactInfo);
//		
//		avm.addTo(requestData, "specialInstructions", avm.exSpecialInstructions());
//		avm.addTo(requestData, "propertyId", Integer.toString(avm.exPropertyId()));
//		requestData.appendChild( avm.exCurrentUser(pDoc) );
//		
//		srRoot.appendChild(requestData);
////		pDoc.appendChild(srRoot);		
////        docRootNode.insertBefore((Node) thisCtrl.getSectionNode(appDoc), docRootNode.getFirstChild());
//		
//		pDoc.insertBefore(srRoot, pDoc.getFirstChild());


		addTo(srRoot, "serviceRequestId", Integer.toString(mRequest.getRequestId()));

		addTo(srRoot, "serviceProviderId", Integer.toString(avm.exServiceProviderId()));
		
		addTo(srRoot, "serviceProviderName", avm.exServiceProviderName());
		addTo(srRoot, "serviceTypeId", Integer.toString(avm.exServiceTypeId()));
		addTo(srRoot, "serviceTypeName", avm.exServiceTypeName());
		addTo(srRoot, "serviceSubTypeId",Integer.toString(avm.exServiceSubTypeId()));
		addTo(srRoot, "serviceSubTypeName", avm.exServiceSubTypeName());
		addTo(srRoot, "serviceSubSubTypeId", Integer.toString(avm.exServiceSubSubTypeId()));
		addTo(srRoot, "serviceSubSubTypeName", avm.exServiceSubSubTypeName());
		addTo(srRoot, "productName", avm.exProductName());
		addTo(srRoot, "transactionTypeId", Integer.toString(avm.exTransactionTypeId()));
		addTo(srRoot, "transactionTypeName", avm.exTransactionTypeName());
		addTo(srRoot, "datxTimeout", Integer.toString(avm.exDatxTimeout()));
		addTo(srRoot, "serviceProviderfRefNum", avm.exServiceProviderRefNo());

		Element requestData = mDoc.createElement("serviceRequestData");
		
		//ServiceRequestContact table
		Element contactInfo = mDoc.createElement("contactInfo");
		addTo(contactInfo, "firstName", avm.exSRContactFirstName());
		addTo(contactInfo, "lastName", avm.exSRContactLastName());
		addTo(contactInfo, "email", avm.exSRContactEmail());
		addTo(contactInfo, "homePhoneNumber", avm.exSRContactHomePhone());
		addTo(contactInfo, "workPhoneNumber", avm.exSRContactWorkPhone());
		addTo(contactInfo, "workPhoneExtension", avm.exSRContactWorkExtension());
		addTo(contactInfo, "cellPhoneNumber", avm.exSRContactCellPhone());
		addTo(contactInfo, "comment", avm.exSRContactComments());
		requestData.appendChild(contactInfo);
		
		addTo(requestData, "specialInstructions", avm.exSpecialInstructions());
		addTo(requestData, "propertyId", Integer.toString(avm.exPropertyId()));
		requestData.appendChild( avm.exCurrentUser(pDoc) );
		
		srRoot.appendChild(requestData);
//		pDoc.appendChild(srRoot);		
//        docRootNode.insertBefore((Node) thisCtrl.getSectionNode(appDoc), docRootNode.getFirstChild());
		
		Node docRootNode = DomUtil.getFirstNamedDescendant(mDoc, "Document");
		docRootNode.insertBefore(srRoot, docRootNode.getFirstChild());		
		return srRoot;
	}

    protected boolean addTo(Node parentNode, String tagName, String data) {
        try
        {
            if (parentNode != null && !isEmpty(tagName) && !isEmpty(data))
            {
                data = data.trim();

                parentNode.appendChild(getTag(tagName, data));
                return true;
            }
        }
        catch (Exception e) {}

        return false;
    }

    protected Node getTag(String tagName, String data)
    {
        //  DEBUG
        //  This ensures that SOMETHING gets put in the field.  This is useful
        //  for debugging XML data source.
        //if (data == null || data.trim().length() == 0)
           //data = "ABC123";

        try
        {
            if (!isEmpty(tagName) && !isEmpty(data))
            {
                data = data.trim();

                Node elemNode = mDoc.createElement(tagName);
                elemNode.appendChild(mDoc.createTextNode(data));

                return elemNode;
            }
        }
        catch (Exception e) {}

        return null;
    }
    protected boolean isEmpty(String str)
    {
        return (str == null || str.trim().length() == 0);
    }

	public static void main(String[] args)throws Exception {
		 String adminPath = "C:\\FXP\\Resources\\admin";
	     ResourceManager.init(adminPath, "DEV_3.1");

		 PropertiesCache.addPropertiesFile(adminPath + "\\mossys.properties");
//		 SessionResourceKit srk = new SessionResourceKit("dvg"); //5255");
		 SessionResourceKit srk = new SessionResourceKit("218"); //5255");		 
		 
		 AVMExtractor avmE = new AVMExtractor(srk, 69, 1, 0);
		 String xmlString = avmE.buildXML();
		 
	     System.out.println("Writing Xml file");
	     File xmlInFile = new File("c:\\work\\a.xml");
	     FileWriter fwr = new FileWriter(xmlInFile);
	     fwr.write(xmlString);
	     fwr.close();
	     System.out.println("Writing Xml file - finished");		 
	}    
}