package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class VerifyIncomeSalary extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();

        try
        {
            if (de.getClassId() == ClassId.BORROWER)
            {
                Borrower b = (Borrower)de;
                double amt = 0.0;

                EmploymentHistory hist =
                    new EmploymentHistory(srk).findByCurrentEmployment((BorrowerPK)b.getPk());

                Income inc = hist.getIncome();

                if (inc.getIncomeTypeId() == Mc.INCOME_SALARY &&
                    inc.getMonthlyIncomeAmount() > 0)
                {
                    amt = inc.getAnnualIncomeAmount();
                }

                fml.addCurrency(amt);
            }
        }
        catch (Exception e)
        {
            srk.getSysLogger().error(e.getMessage());
            srk.getSysLogger().error(e);
            throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
        }

        return fml;
    }
}
