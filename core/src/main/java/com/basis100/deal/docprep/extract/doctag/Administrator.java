package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;

public class Administrator extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      int upId = deal.getAdministratorId();
      UserProfile up = new UserProfile(srk);

      up = up.findByPrimaryKey(new UserProfileBeanPK(upId, deal.getInstitutionProfileId()));

      Contact contact = up.getContact();
      String outVal = "";

      if(contact != null)
      {
        String firstname = contact.getContactFirstName();
        String lastname = contact.getContactLastName();

        if (firstname != null && firstname.length() > 0)
        {
           outVal += firstname;
           if (outVal != null && outVal.length() > 0)
              outVal += " ";
        }

        if (lastname != null && lastname.length() > 0)
           outVal += lastname;

      }

      fml.addValue(outVal,fml.STRING);
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}