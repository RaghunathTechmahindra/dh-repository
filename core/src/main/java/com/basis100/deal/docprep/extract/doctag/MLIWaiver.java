package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class MLIWaiver extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    Deal deal = (Deal)de;

    try
    {
      Condition cond = new Condition(srk,Dc.MLI_WAIVER);
      if( cond.getConditionTypeId() == Mc.CONDITION_TYPE_VARIABLE_VERBIAGE){
        ConditionHandler ch = new ConditionHandler(srk);
        val = ch.getVariableVerbiage(deal, Dc.MLI_WAIVER,lang);
        fml.addValue(val, fml.STRING);
      }
    //==========================================================================
    // NOTE commented out because the logic has changed. This condition should
    // be generated for any lender, as long as the condition type is 3.
    // tracker issue 218.
    // comment posted on: 2002-01-30
    //==========================================================================
    /*
      int id  =  deal.getLenderProfileId();
      LenderProfile lp = new LenderProfile(srk);
      lp.findByPrimaryKey(new LenderProfilePK(id));

      String lpsn = lp.getLPShortName();

      if(lpsn.equalsIgnoreCase("MCAP") ||
         lpsn.equalsIgnoreCase("Clarica") ||
         lpsn.equalsIgnoreCase("ING") )
      {

        ConditionHandler ch = new ConditionHandler(srk);
        val = ch.getVariableVerbiage(deal, Dc.MLI_WAIVER,lang);
      }

      fml.addValue(val, fml.STRING);
       */
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
