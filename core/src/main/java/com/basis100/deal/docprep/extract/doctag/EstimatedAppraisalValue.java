package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;

public class EstimatedAppraisalValue extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.PARTYPROFILE)
      {
        PartyProfile pp = (PartyProfile)de;

        MasterDeal md = new MasterDeal(srk,null,pp.getDealId());

        int copy = md.getGoldCopyId();

        Deal d = new Deal(srk,null,pp.getDealId(),copy);

        int sfid = PicklistData.getPicklistIdValue("SpecialFeature", "Pre-Approval");

        if(sfid == d.getSpecialFeatureId())
        {
         // fml.addValue(0.0,fml.CURRENCY);  //ALERT ALERT spec says deal.preApprovalPurchasePrice ????
        }
        else
        {
          Property prop = new Property(srk,null,pp.getPropertyId(),copy);
          fml.addCurrency(prop.getEstimatedAppraisalValue());
        }

      }
      else if(clsid == ClassId.DEAL)
      {
        Deal deal = (Deal)de;
        double amt = deal.getTotalEstAppraisedValue();
        fml.addCurrency(amt);
      }
     

    }
    catch (Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
