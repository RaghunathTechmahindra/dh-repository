package com.basis100.deal.docprep.gui;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import com.basis100.deal.docprep.gui.GeneralInfoPanel;
import com.basis100.deal.docprep.gui.PropertiesPanel;

public class MainAreaPanel extends JPanel implements ActionListener {

    private JToolBar toolBar;
    private JTabbedPane tabbedPane;
    private GeneralInfoPanel panel1;
    private PropertiesPanel panel2;

    public MainAreaPanel() {
        super(new BorderLayout());

        // 1) tool bar for control buttons  --------------------------------
        // addToolBar();

        // 3) tabbedPane ----------------------------------------------------
        tabbedPane = new JTabbedPane();
        // tabbedPane.setPreferredSize(new Dimension(800, 570));
        // the size will be defined by its children panels

//        ImageIcon icon = createImageIcon("images/middle.gif");

        //JPanel panel0 = new JPanel();
        //tabbedPane.addTab("Command", icon, panel0, "Does nothing");

        panel1 = new GeneralInfoPanel(new Dimension(400, 300));
        tabbedPane.addTab("General Info", /* icon */ null, panel1, "Docprep Statistics");
        // tabbedPane.setMnemonicAt(0, KeyEvent.VK_1); // 1.4

        panel2 = new PropertiesPanel(new Dimension(400,300));
        tabbedPane.addTab("Properties", /* icon */ null, panel2, "Mos System Properties");
        // tabbedPane.setMnemonicAt(1, KeyEvent.VK_2); // 1.4

        //JPanel panel3 = makeTextPanel("Panel #3");
        //tabbedPane.addTab("Control", icon, panel3,"Still does nothing");
        // tabbedPane.setMnemonicAt(2, KeyEvent.VK_3); // 1.4

        //JPanel panel4 = makeTextPanel("Panel #4");
        //tabbedPane.addTab("Output", icon, panel4,"System output (System.out)");
        // tabbedPane.setMnemonicAt(3, KeyEvent.VK_4); // 1.4

        //Add the tabbed pane to this panel.
        add(tabbedPane, BorderLayout.CENTER);

        //Uncomment the following line to use scrolling tabs.
        //tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    }

    public void assignDPGuiListener(DPGuiRequestListener listener){
      panel1.addGuiRequestListener(listener);
    }

    private void addToolBar(){

      toolBar = new JToolBar();
      toolBar.setFloatable(false);
      toolBar.setPreferredSize(new Dimension(800, 30));
      toolBar.setBorder(BorderFactory.createLineBorder(Color.black));
      toolBar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
      //addToolBarButtons();

      add(toolBar, BorderLayout.NORTH);
    }

    public void actionPerformed(java.awt.event.ActionEvent ev){
/*
      Object obj = ev.getSource();
      if( obj == startButton ){
        try{
        }catch(Exception exc){
          System.out.println("Serious error - could not start the process");
          System.exit(0);
        }
      }
 */
    }

    protected JPanel makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new java.awt.GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }

    /** Returns an ImageIcon, or null if the workPath was invalid.
    protected static ImageIcon createImageIcon(String workPath) {
        java.net.URL imgURL = MainAreaPanel.class.getResource(workPath);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + workPath);
            return null;
        }
    }
*/
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Make sure we have nice window decorations.
        // JFrame.setDefaultLookAndFeelDecorated(true); // v.1.4 specific

        //Create and set up the window.
        JFrame frame = new JFrame("DocPrep Control Center");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        JComponent newContentPane = new MainAreaPanel();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.getContentPane().add(new MainAreaPanel(),
                                 BorderLayout.CENTER);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
