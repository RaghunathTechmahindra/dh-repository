package com.basis100.deal.docprep.deliver;

import com.basis100.mail.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;

import java.io.*;
import java.util.*;
import com.basis100.log.*;


public class MailDeliver
{
    //prevent non-static use of class
    private MailDeliver(){};

    public static synchronized boolean send(SimpleMailMessage message) throws DocPrepDeliveryException, Exception
    {
        return send(message, (String)null, (String)null);
    }

    public static synchronized boolean send(SimpleMailMessage message, Vector pAttVect, String docFullName) throws DocPrepDeliveryException, Exception
    {
        SysLogger logger = ResourceManager.getSysLogger("0");
        logger.info("Attempt to deliver e-mail message to : "+ message.getTo() );
        
        // #910 Catherine, 30Jan05, fix for BMO (document should have only documentfullname)
        // if this property is "N", attachment processing goes another route
        boolean bExcl = (PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.rtfname.include.dealid", "Y").equalsIgnoreCase("N"));
        
        if (bExcl) {
          Iterator it = pAttVect.iterator();
          while( it.hasNext() ){
            File lFile = (File)it.next();
            String sName = lFile.getName();
            String sPath = lFile.getParent();
            int idx = sName.lastIndexOf(".");
            String sExt = sName.substring(idx);  
            sName = sPath + "\\" + docFullName + sExt;
            send(message, sName, lFile);
            logger.debug("sending attachement " + sName);
            System.out.println("sending attachement " + sPath + docFullName + sExt);
          }
          return true;  // exit  
        }        
        // #910 end -------------------------------------
        
        try
        {
            Iterator it = pAttVect.iterator();
            while( it.hasNext() ){
              File lFile = (File)it.next();
              message.attachFile( lFile );
              logger.trace("DOCPREP: MailDeliver - attaching file :" + lFile.getAbsolutePath() );
            }
            logger.trace("DOCPREP: MailDeliver - sending the message...");
            message.send(null);
            logger.trace("DOCPREP: MailDeliver - message sent");
            return true;
        }
        catch(Exception io)
        {
            logger.trace("DOCPREP: MailDeliver - exception !!!");
            //throw new DocPrepDeliveryException("Mail Delivery Failed: " + io.getMessage());
            //  SCR #458  - Rethrow the original exception to be dealt with higher up.
            throw io;
        }
    }

    public static synchronized boolean send(SimpleMailMessage message, String newFileName, File attachment) throws DocPrepDeliveryException, Exception
    {
        SysLogger logger = ResourceManager.getSysLogger("0");
        File tempFile = new File(newFileName); 
        try
        {
            if (attachment != null && newFileName != null)
            {
              	copyFile(attachment, tempFile);
              	
                // tempFile.renameTo(new File(newFileName));
                message.attachFile(tempFile);
            }

            logger.trace("DOCPREP: MailDeliver - sending the message...");
            message.send(null);
            logger.trace("DOCPREP: MailDeliver - message sent");

            //  Delete the original file, since it was intended to be a
            //  temporary file anyways...
            tempFile.delete();
            return true;
        }
        catch(Exception io)
        {
            logger.trace("DOCPREP: MailDeliver - exception !!!");
            //throw new DocPrepDeliveryException("Mail Delivery Failed: " + io.getMessage());
            //  SCR #458  - Rethrow the original exception to be dealt with higher up.
            throw io;
        }
    }

    public static synchronized boolean send(SimpleMailMessage message, String data, String filename) throws DocPrepDeliveryException, Exception
    {
        SysLogger logger = ResourceManager.getSysLogger("0");

        try
        {
            File toAttach = null;

            if (data != null && filename != null)
            {
                toAttach = new File(filename);

                FileOutputStream str = new FileOutputStream(toAttach);
                OutputStreamWriter writer = new OutputStreamWriter(str);

                logger.trace("DOCPREP: MailDeliver - write the data.");
                writer.write(data);
                writer.flush();
                writer.close();

                logger.trace("DOCPREP: MailDeliver - attach the data.");
                message.attachFile(toAttach);
            }

            logger.trace("DOCPREP: MailDeliver - sending the message...");
            message.send(null);
            logger.trace("DOCPREP: MailDeliver - message sent");

            return (toAttach == null ? true : toAttach.delete());
        }
        catch(Exception io)
        {
            logger.trace("DOCPREP: MailDeliver - exception !!!");
            //throw new DocPrepDeliveryException("Mail Delivery Failed: " + io.getMessage());
            //  SCR #458  - Rethrow the original exception to be dealt with higher up.
            throw io;
        }
    }
    
    private static void copyFile(File in, File out) throws Exception {
      FileInputStream fis  = new FileInputStream(in);
      FileOutputStream fos = new FileOutputStream(out);
      byte[] buf = new byte[1024];
      int i = 0;
      while((i=fis.read(buf))!=-1) {
        fos.write(buf, 0, i);
        }
      fis.close();
      fos.close();
      }    
}