package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.picklist.*;
import java.util.*;
import com.basis100.deal.calc.*;

public class LenderName extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();

    int id = de.getClassId();

    try
    {
      if(id == ClassId.DEAL)
      {
        Deal deal = (Deal)de;

        //LenderProfile lp = new LenderProfile(srk, deal.getLenderProfileId());
        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LenderProfile", deal.getLenderProfileId(), lang);

        //val = lp.getLenderName();

        fml.addValue(val, fml.STRING);
      }
      else if(id == ClassId.PROPERTY)
      {
        Property property = (Property)de;

        Deal d = new Deal(srk, null, property.getDealId(), property.getCopyId());

        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LenderProfile", d.getLenderProfileId(), lang);
        //LenderProfile lp = new LenderProfile(srk, d.getLenderProfileId());

        //val = lp.getLenderName();

        fml.addValue(val, fml.STRING);
      }
      else if(id == ClassId.BORROWER)
      {
        Borrower bor = (Borrower)de;

        Deal d = new Deal(srk, null, bor.getDealId(), bor.getCopyId());

        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LenderProfile", d.getLenderProfileId(), lang);
        //LenderProfile lp = new LenderProfile(srk, d.getLenderProfileId());

        //val = lp.getLenderName();

        fml.addValue(val, fml.STRING);
      }
      else if(id == ClassId.EMPLOYMENTHISTORY)
      {
        EmploymentHistory eh = (EmploymentHistory) de;
        Deal dealEh = new Deal(srk,null);
        dealEh = dealEh.findByEmploymentHistory(eh);
        //LenderProfile lpEh = new LenderProfile(srk,dealEh.getLenderProfileId());
        val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "LenderProfile", dealEh.getLenderProfileId(), lang);
        //val = lpEh.getLenderName();
        fml.addValue(val, fml.STRING);
      }

    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
