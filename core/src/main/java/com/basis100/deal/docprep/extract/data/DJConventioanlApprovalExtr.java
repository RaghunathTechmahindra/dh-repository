package com.basis100.deal.docprep.extract.data;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.docprep.*;
import com.basis100.resources.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.picklist.*;
import org.w3c.dom.*;
import com.basis100.xml.*;
import MosSystem.*;
import java.util.*;

public final class DJConventioanlApprovalExtr extends DealExtractor
{
  private static final int LIABILITY_GROUP_OTHER = 0;
  private static final int LIABILITY_GROUP_CREDIT_CARD = 1;
  private static final int LIABILITY_GROUP_LOANS = 2;
  private static final int LIABILITY_GROUP_MUNICIPALITY = 3;
  private static final int LIABILITY_GROUP_HEATING = 4;
  private static final int LIABILITY_GROUP_CONDO = 5;
  //============================================================================
  // implemenatation of abstract methods
  //============================================================================
  public String extractXMLData(DealEntity de, int pLang, DocumentFormat pFormat, SessionResourceKit pSrk) throws Exception
  {
        deal = (Deal) de;
        lang = pLang;
        format = pFormat;
        srk = pSrk;

        buildDataDoc();

        XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

        if (pLang == Mc.LANGUAGE_PREFERENCE_FRENCH)
           xmlwr.setEncoding("iso-8859-1");
        return xmlwr.printString(doc);

        //return doc.toString();
  }

  protected void buildDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();
    Element root = doc.createElement("Document");
    root.setAttribute("type","1");
    root.setAttribute("lang","1");

    specElem = doc.createElement("specialRequirementTags");
    buildDealTags();


    root.appendChild( dealElement ) ;
    root.appendChild( dealAdditionalElement ) ;

    root.appendChild( buildGeneralTags()  );
    root.appendChild( buildSpecificTags() );
    doc.appendChild(root);
  }

  protected Node buildSpecificTags() throws Exception
  {
    addTo(specElem, extractBorrowerNames());
    addTo(specElem, extractRealEstateData());
    addTo(specElem, discountOrPremium());
    //addTo(specElem,processActualPaymentTerm());
    addTo(specElem, processAmortizationTerm());
    addTo(specElem, createLiabilitiyNodes());
    return specElem;
  }

  protected int findLiabilityGroup(Liability pLiab){
          switch(pLiab.getLiabilityTypeId()){
            // credit card
            case 2 : {
              return LIABILITY_GROUP_CREDIT_CARD;
            }
            // loans
            case 3 : {
              return LIABILITY_GROUP_LOANS;
            }
            case 4 : {
              return LIABILITY_GROUP_LOANS;
            }
            case 7 : {
              return LIABILITY_GROUP_LOANS;
            }
            case 10 : {
              return LIABILITY_GROUP_LOANS;
            }
            case 13 : {
              return LIABILITY_GROUP_LOANS;
            }
            // municipal
            case 17 : {
              return LIABILITY_GROUP_MUNICIPALITY;
            }
            // condo
            case 18 : {
              return LIABILITY_GROUP_CONDO;
            }
            // heating
            case 19 : {
              return LIABILITY_GROUP_HEATING;
            }
            default :{
              return LIABILITY_GROUP_OTHER;
            }
          }
  }

  protected Node createLiabilitiyNodes(){
    Node rvNode = doc.createElement("variousLiabilities");
    double variosLiabilitiesTotal = 0d;

    Node creditCardNode = doc.createElement("creditCardLiabilities");
    double creditCardTotal = 0d;

    Node loansNode = doc.createElement("loanLiabilities");
    double loansTotal = 0d;

    Node othersNode = doc.createElement("otherLiabilities");
    double otherTotal = 0d;

    Node singleNode;
    double heatingTotal = 0d;
    double municipalTotal = 0d;
    double condoTotal = 0d;
    Collection coll;
    try{
      coll = deal.getBorrowers();
    } catch (Exception exc){
      return rvNode;
    }
    if(coll == null){
      return rvNode;
    }
    Iterator it = coll.iterator();
    while( it.hasNext() ){
      Borrower borr = (Borrower)it.next();
      if(borr.isGuarantor() ) { continue; }
      try{
        Collection lBorLiabCol = borr.getLiabilities();
        Iterator lBorLiabIt = lBorLiabCol.iterator();
        while( lBorLiabIt.hasNext() ){
          Liability lLiab = (Liability)lBorLiabIt.next();
          switch( findLiabilityGroup(lLiab) ){
            case LIABILITY_GROUP_CREDIT_CARD :{
              creditCardTotal += lLiab.getLiabilityMonthlyPayment();
              variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();

              singleNode = doc.createElement("creditCardLiability");
              addTo(singleNode,"amount", formatCurrency( lLiab.getLiabilityMonthlyPayment() ) );
              addTo(creditCardNode,singleNode);

              //singleNode = doc.createElement("liabilityId");
              addTo(singleNode,"liabilityId", "" + lLiab.getLiabilityId() );
              addTo(creditCardNode,singleNode);
              break;
            }
            case LIABILITY_GROUP_LOANS :{
              loansTotal += lLiab.getLiabilityMonthlyPayment();
              variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();

              singleNode = doc.createElement("loanLiability");
              addTo(singleNode,"amount", formatCurrency( lLiab.getLiabilityMonthlyPayment() ) );
              addTo(loansNode,singleNode);

              //singleNode = doc.createElement("liabilityId");
              addTo(singleNode,"liabilityId",""+ lLiab.getLiabilityId() );
              addTo(loansNode,singleNode);

              break;
            }
            case LIABILITY_GROUP_CONDO :{
              condoTotal += lLiab.getLiabilityMonthlyPayment();
              variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();
              break;
            }
            case LIABILITY_GROUP_HEATING :{
              heatingTotal += lLiab.getLiabilityMonthlyPayment();
              variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();
              break;
            }
            case LIABILITY_GROUP_MUNICIPALITY :{
              municipalTotal += lLiab.getLiabilityMonthlyPayment();
              variosLiabilitiesTotal +=lLiab.getLiabilityMonthlyPayment();
              break;
            }
            case LIABILITY_GROUP_OTHER :{
              otherTotal += lLiab.getLiabilityMonthlyPayment();
              variosLiabilitiesTotal += lLiab.getLiabilityMonthlyPayment();

              singleNode = doc.createElement("otherLiability");
              addTo(singleNode,"amount", formatCurrency( lLiab.getLiabilityMonthlyPayment() ) );
              addTo(othersNode,singleNode);

              //singleNode = doc.createElement("liabilityId");
              addTo(singleNode,"liabilityId",""+ lLiab.getLiabilityId() );
              addTo(othersNode,singleNode);

              break;
            }
          }// end switch
        }
      }catch(Exception exc){
        // do nothing here, skip to the next borrower
      }
    }
    // now bring out all the totals
    addTo(rvNode,creditCardNode);
    //singleNode = doc.createElement("creditCardLiabilityTotal");
    addTo(rvNode,"creditCardLiabilityTotal",formatCurrency(creditCardTotal));
    //addTo(rvNode,singleNode);

    addTo(rvNode,loansNode);
    //singleNode = doc.createElement("loansLiabilityTotal");
    addTo(rvNode,"loansLiabilityTotal",formatCurrency(loansTotal));
    //addTo(rvNode,singleNode);

    addTo(rvNode,othersNode);
    //singleNode = doc.createElement("otherLiabilityTotal");
    addTo(rvNode,"otherLiabilityTotal",formatCurrency(otherTotal));
    //addTo(rvNode,singleNode);

    //singleNode = doc.createElement("municipalLiabilityTotal");
    addTo(rvNode,"municipalLiabilityTotal",formatCurrency(municipalTotal));
    //addTo(rvNode,singleNode);

    //singleNode = doc.createElement("condoLiabilityTotal");
    addTo(rvNode,"condoLiabilityTotal",formatCurrency(condoTotal));
    //addTo(rvNode,singleNode);

    //singleNode = doc.createElement("heatingLiabilityTotal");
    addTo(rvNode,"heatingLiabilityTotal",formatCurrency(heatingTotal));
    //addTo(rvNode,singleNode);

    //singleNode = doc.createElement("variousLiabilityTotal");
    addTo(rvNode,"variousLiabilityTotal",formatCurrency(variosLiabilitiesTotal));
    //addTo(rvNode,singleNode);

    return rvNode;
  }

  protected Node processAmortizationTerm(){
    Node rvNode = doc.createElement("amortizationTermProcessed");

    if(  isDivisibleByNum(deal.getAmortizationTerm(),12)  ){
      addTo(rvNode,getTag("termNumber",""+ deal.getAmortizationTerm()/12));
      addTo(rvNode,getTag("termMeasure", "YEARS"));
    }else{
      addTo(rvNode,getTag("termNumber",""+ deal.getAmortizationTerm()));
      addTo(rvNode,getTag("termMeasure", "MONTHS"));
    }
    return rvNode;
  }

  protected boolean isDivisibleByNum(int pNum1, int pNumDiv){
    double lNum;
    lNum = pNum1 / pNumDiv;
    int lInt = (new Double(lNum)).intValue();
    if( (lNum - lInt ) == 0 ){
      return true;
    }
    return false;
  }

  protected Node processActualPaymentTerm(){
    Node rvNode = doc.createElement("actualPaymentTermProcessed");

    if(  isDivisibleByNum(deal.getActualPaymentTerm(),12)  ){
      addTo(rvNode,getTag("termNumber",""+ deal.getActualPaymentTerm()/12));
      addTo(rvNode,getTag("termMeasure", "YEARS"));
    }else{
      addTo(rvNode,getTag("termNumber",""+ deal.getActualPaymentTerm()));
      addTo(rvNode,getTag("termMeasure", "MONTHS"));
    }
    return rvNode;
  }

  protected Node discountOrPremium() throws Exception
  {
    Node dealOrPremiumNode = doc.createElement("discountOrPremium");
    if(deal.getDiscount() == 0d && deal.getPremium() == 0d){
      return getTag("discountOrPremium","NONE");
    }else if(deal.getDiscount() > 0d){
      return getTag("discountOrPremium","DISCOUNT");
    }else if(deal.getPremium()>0d){
      return getTag("discountOrPremium","PREMIUM");
    }
    return getTag("discountOrPremium","ERROR");
  }

  protected Node extractRealEstateData() throws Exception
  {
    Node realEstateNode = doc.createElement("realEstateData");
    Node partyProfileNode = doc.createElement("partyProfile");
    PartyProfile pp = new PartyProfile(srk);
    // ALERT party type solicitor must change to party type real estate
    Collection col = pp.findByDealAndType( new DealPK(deal.getDealId(), 
           deal.getCopyId()), Mc.PARTY_TYPE_SOLICITOR );
    if(col.isEmpty()){return null;}
    Iterator it = col.iterator();
    while( it.hasNext() ){
      PartyProfile profile = (PartyProfile) it.next();
      addTo(partyProfileNode,"partyCompanyName",profile.getPartyCompanyName());
      addTo(partyProfileNode,"partyName",profile.getPartyName());
      addTo(partyProfileNode, extractContactData(profile.getContact()));
      addTo(realEstateNode,partyProfileNode);
      break;   // one record only
    }
    return realEstateNode;
  }

  protected Node extractBorrowerNames() throws Exception
  {
    Element bNodeList = doc.createElement("OrderedBorrowers");

    Collection pCol = deal.getBorrowers();
    Vector bVect = sortBorrowers(pCol);
    int lCount = 0;
    for(int i=0; i<bVect.size(); i++){
      if(lCount == 4){break; }
      Iterator pColIt = pCol.iterator();
      Node bNode ;
      while( pColIt.hasNext() ){
        Borrower b = (Borrower) pColIt.next();
        if( b.getBorrowerId() == ((Integer) bVect.elementAt(i)).intValue() ){
          bNode = extractBorrowerData(b);
          addTo(  bNode, "totalIncomeAmountMonthly", formatCurrency(b.getTotalIncomeAmount()/12) );
          addTo(  bNode, "monthsAtCurrentAddress", "" +extractMaxMonthsOnCurrentAddr(b) );
          addTo(  bNode, "monthsAtCurrentEmployment", "" +extractMaxMonthsOnCurrentEmpl(b) );
          addTo(bNodeList,bNode);
          //addTo(bNode,"FirstName",b.getBorrowerFirstName());
          //addTo(bNode,"LastName",b.getBorrowerLastName());

          //String val = BXResources.getPickListDescription("Salutation",b.getSalutationId(),lang);
          //addTo(bNode , "borrowerSalutation", val );

          //addTo(bNodeList, bNode);
          lCount++;
          break;
        }
      }
    }

    return bNodeList;
  }


  /**
   * Sorts borrowers like this:  First place takes primary borrower
   * followed by other regular borrowers ordered by borrower id.
   */
  protected Vector sortBorrowers(Collection pColl){
    Vector retVect = new Vector();
    Iterator it = pColl.iterator();
    while(it.hasNext()){
      Borrower b = (Borrower) it.next();
      if(b.isGuarantor()){continue;}
      if(b.isPrimaryBorrower()){
        retVect.insertElementAt(new Integer(b.getBorrowerId()), 0);
      }
    }
    if(retVect.size() == 0){
      return retVect;
    }
    it = pColl.iterator();
    while ( it.hasNext() ){
      Borrower b = (Borrower) it.next();
      if( b.isPrimaryBorrower() ){ continue; }
      if( b.isGuarantor() ){ continue; }
      boolean inserted = false;
      for( int i=1;i<retVect.size(); i++ ){
        if(b.getBorrowerId() < ((Integer)retVect.elementAt(i)).intValue() ){
          retVect.insertElementAt(new Integer(b.getBorrowerId()),i);
          inserted = true;
          break;
        }
      }
      if( !inserted ){
        retVect.addElement(new Integer(b.getBorrowerId()));
      }
    }
    return retVect;
  }

}

// =============================================================================
// list of created nodes
// =============================================================================


