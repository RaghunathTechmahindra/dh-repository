package com.basis100.deal.docprep.extract;

import com.basis100.deal.docprep.*;

public class UnsupportedFormatException extends Exception
{

 /**
  * Constructs a <code>UnsupportedFormatException</code> with no specified detail message.
  */
  public UnsupportedFormatException()
  {
	  super();
  }

 /**
  * Constructs a <code>UnsupportedFormatException</code> with the specified message.
  * @param   message  the related message.
  */
  public UnsupportedFormatException(String message)
  {
    super(message);
  }
}

