package com.basis100.deal.docprep;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

import java.sql.*;
import java.util.*;
import java.awt.event.*;

// PROGRAMMERS NOTE: this class has changed for multiple unresolved exception fix
/**
 *  Monitor and Manage DocumentQueue. Monitors DocumentQueue Database table for records where
 *  the minimum record whose pk > 0 is considered the queue head.
 *
 *  The monitor attempts to increase throughput during peak requests by deacreasing monitor
 *  intervals by half to a minimum of 500 milliseconds. If a request is found the the monitor
 *  cuts the interval in half. Each successive time a queue entry is found the monitor decreases
 *  the interval by half again until the minimum is reached. The monitor interval then rests
 *  at the minimum for remaining requests until an empty queue exists. Once the queue is
 *  empty the monitor returns to the parameterized polling interval.
 *
 *  Design Note: This class may be altered to wait for notification of document request
 *  but by using the DB table as a true producer consumer model queue - loose coupling is
 *  maintained between the requesting program the Document Generation System.
 *
 */
public class DocRequestQueueMonitor
{
    public SessionResourceKit srk;
    private Thread monitorThread = null;

    public SysLogger logger;


    private int monitorInterval = 8; //default = 8 set by parameter intervalsec in application.xml
    private int intervalMin;
    private int intervalAct;

    private boolean stopped = true;

    public static boolean debug = true;
    private static int INTERVAL_ABSOLUTE_MIN = 500; //milliseconds

    //private int workNumber;
    //private DocGenHandlerController docGenHandlerController;
    private Vector actionListeners;
    public DocRequestQueueMonitor(int interval)
    {
      //workNumber = 0;

      actionListeners = new Vector();
      //docGenHandlerController = new DocGenHandlerController();
      srk = new SessionResourceKit("DocRequestMonitor");
      srk.setIndefiniteUseJdbcConnection();
      logger = srk.getSysLogger();
      logger.setJdbcTraceOn(false);

      monitorInterval = interval * 1000;

      initIntervalRange(monitorInterval);

    }

/*
   public void run()
   {
      int errorCount = 0;
      DocumentQueue entry = null;

      synchronized (this)
      {
          while(true)
          {
            try
            {

              System.out.println(""+ docGenHandlerController.getNumberOfFreeSpots());
              if( docGenHandlerController.isThereFreeSpot() == false){
                this.wait(intervalAct);
                continue;
              }

              JdbcExecutor jExec = srk.getJdbcExecutor();
              // DocumentQueue entry = new DocumentQueue(srk);
              // entry = entry.findNextEntry();
              entry = checkQueue(jExec);

              if(entry != null)
              {
                workNumber++;
                entry.setRequestStatusId(Dc.DOCUMENT_REQUEST_STATUS_IN_PROCESS);
                entry.ejbStore();

                StringBuffer msgBuf = new StringBuffer("\n=========================================================\n");
                msgBuf.append("DOCPREP IINITALIZE REQUEST :").append(entry.getDocumentQueueId() ).append("\n");
                msgBuf.append("Deal Id:").append("(").append(entry.getDealId()).append(")\n");
                msgBuf.append("Copy Id:").append("(").append(entry.getDealCopyId()).append(")\n");
                msgBuf.append("Requestor user id:").append("(").append(entry.getRequestorUserId()).append(")\n");
                msgBuf.append("Document type:").append("(").append(entry.getDocumentTypeId()).append(")\n");
                msgBuf.append("Document format:").append("(").append(entry.getDocumentFormat()).append(")\n");
                msgBuf.append("Document version:").append("(").append(entry.getDocumentVersion()).append(")\n");
                msgBuf.append("E mail address:").append("(").append(entry.getEmailAddress()).append(")\n");
                msgBuf.append("Language:").append("(").append(entry.getLanguagePreferenceId()).append(")\n");
                msgBuf.append("Lender profile:").append("(").append(entry.getLenderProfileId()).append(")\n");
                msgBuf.append("Monitor interval:").append("(").append( monitorInterval ).append(")\n");
                msgBuf.append("=========================================================");
                logger.debug( msgBuf.toString() );
                intervalAct = shrinkInterval(intervalAct);
                HandlerWorkerHolder handlerWorkerHolder = new HandlerWorkerHolder(workNumber);
                handlerWorkerHolder.setDocumentQueueId(entry.getDocumentQueueId());
                initiateRequest( handlerWorkerHolder);
                System.out.println(""+ docGenHandlerController.getNumberOfFreeSpots());
              }
              else
              {
                intervalAct = monitorInterval;
              }

              if (stopped)
                break;

              srk.freeResources();
              this.wait(intervalAct);

              if (stopped)
                break;
            }
            catch(Exception e)
            {
              String msg = "Document Queue Monitor: Exception while monitoring queue.";
              logger.error(msg);
              logger.error(e);

              if (handleMonitorFailure(logger,e, ++errorCount) == true)
              {
                // stop
                stopped = true;
                break;
              }

              continue;
            }
          } // end while

         monitorThread = null;

         this.notifyAll();
      }

      if (stopped == false)
      {
        stopped = true;
        logger.error("Document Queue Monitor: Stopped due to error.");
      }

      // free resources
      srk.freeResources();
  }
*/

  public void processDocumentRequest(HandlerWorkerHolder holder, ActionListener pListener)throws Exception
  {
    initiateRequest( holder, pListener );
  }

  public DocumentQueue extractNextRequest() throws Exception
  {
    JdbcExecutor jExec = srk.getJdbcExecutor();
    //reset VPD
    srk.getExpressState().cleanDealIds();
    DocumentQueue rv = checkQueue(jExec);
    rv.setRequestStatusId(Dc.DOCUMENT_REQUEST_STATUS_IN_PROCESS);
    rv.ejbStore();
    return rv;
  }
    /**
    *  checks the queue table for any new Document Generation Requests
    */
    private DocumentQueue checkQueue(JdbcExecutor jExec) throws Exception
    {

      String sql = "Select MIN(documentQueueId) from DocumentQueue where documentQueueId > 0 AND " +
                    "requestStatusId = " + Dc.DOCUMENT_REQUEST_STATUS_NEW;

      int id = -1;

      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        id = jExec.getInt(key,1);
        break; // one record at a time;
      }

      jExec.closeData(key);

      if(id <= 0) return null;

      DocumentQueue queue = null;

      try
      {
        queue = new DocumentQueue(srk);
        queue = queue.findByPrimaryKey(new DocumentQueuePK(id));
      }
      catch(Exception ex)
      {
        return null;
      }

      return queue;
    }


  /**
   *  initiates a single document request by handing the request to
   *  the DocumentGenerationManager singleton and removes the request from
   *  the queue
   *
   *  @throw DocPrepException if the removal fails
   */
    public void initiateRequest(  HandlerWorkerHolder pHolder,  ActionListener pListener ) throws DocPrepException, Exception
    {
      try
      {
        DocumentGenerationManager.getInstance().generateDocument(  pHolder, pListener );
      }
      catch(Exception ex)
      {
        logger.error("DOCPREP: QueueMonitor.initiateRequest error: " + ex);
        notifyListeners(new ActionEvent(pHolder,1,"ERROR"));
        throw new DocPrepException("ERROR: Failed to initiate document generation");
      }
    }

/*
   public void startMonitor()
   {

    if(stopped == true )
    {
      stopped = false;

      monitorThread = new Thread(this);
      monitorThread.start();

      return;
    }
  }
*/
/*
  public void stopMonitor()
  {
    // signal stop and wait for confirmation (e.g. don't want to kill an ingestion in progress)
    stopped = true;

    while(monitorThread != null)
    {
        synchronized (this)
        {
            try
            {
              this.notifyAll();
              this.wait(60000);  // allow for long duration transaction (60 seconds - very conservative)
            }
            catch(Exception e)
            {
              logger.error("Document Queue Monitor: Exception waiting for stop confirmation");
              logger.error(e);
              return;
            }

            if (monitorThread != null)
            {
              logger.error("Document Queue Monitor: Failed to receive stop confirmation");
              return;
            }

            logger.trace("Document Queue Monitor - stopped");
            return;
        }
    }

    logger.trace("Document Queue Monitor - stopped (wasn't running!)");
  }
*/

  // result: true - stop monitor, false - ignore failure (for now!)
  private boolean handleMonitorFailure(SysLogger log, Exception e, int count)
  {
    String msg = null;

    if(count > 5)
    {
      msg = "Document Queue Monitor: Multiple Unresolved Exceptions - stopping monitor";
      log.error(msg);
      log.mail(msg);
      return true;
    }

    if(monitorThread == null)
    {
      msg = "Document Queue Monitor: Null monitor thread encountered - stopping monitor";
      log.error(msg);
      log.mail(msg);
      return true;
    }

    return false;
  }

  public void freeResources(){
    try{
      srk.freeResources();
    }catch(Exception exc){
      System.out.println("Could not free reasources...");
    }
  }
  //Note added this functionality because maximum queue throughput was very low during
  // usage peaks. Discovered during stress testing and memory management.
  private void initIntervalRange(int interval)
  {
    if(interval >= 5000)
    {
      this.intervalMin = (int)(interval * 0.1);
    }
    else
    {
      this.intervalMin = INTERVAL_ABSOLUTE_MIN;
    }
  }

  private int shrinkInterval(int interval)
  {
    int newsize = (int)(interval * 0.5);

    if(newsize < intervalMin)
     return intervalMin;
    else
     return newsize;
  }

  public void addActionListener(ActionListener pList){
    actionListeners.addElement(pList);
  }

  private void notifyListeners(ActionEvent event){
    Iterator it = actionListeners.iterator();
    while(it.hasNext()){
      ((ActionListener)it.next()).actionPerformed(event);
    }
  }
/*
  public void actionPerformed(ActionEvent event){
    Object obj = event.getSource();
  }
*/

}

