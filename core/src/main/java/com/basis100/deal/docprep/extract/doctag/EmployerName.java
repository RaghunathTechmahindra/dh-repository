package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class EmployerName extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();

        int clsid = de.getClassId();

        try
        {
            if (clsid == ClassId.BORROWER)
            {
                Borrower borrower = (Borrower)de;
                EmploymentHistory eh = new EmploymentHistory(srk);
                eh = eh.findByCurrentEmployment(new BorrowerPK(borrower.getBorrowerId(),
                                                               borrower.getCopyId()) );

                String val = eh.getEmployerName();
                if(val != null){
                  fml.addValue(val,fml.STRING);
                }else{
                  return fml;
                }
            }else if(clsid == ClassId.EMPLOYMENTHISTORY){
                EmploymentHistory employmentHistory = (EmploymentHistory) de;
                String rv = employmentHistory.getEmployerName();
                if(rv != null){
                  fml.addValue(rv, fml.STRING);
                }else{
                  return fml;
                }
            }
        }
        catch (Exception e)
        {
            srk.getSysLogger().error(e.getMessage());
            srk.getSysLogger().error(e);
            throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
        }

        return fml;
    }

}