package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class BorrowerNamesLine extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Borrower bor = null;
    String firstname = null;
    String lastname = null;

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    try
    {
      Collection borrowers = deal.getBorrowers();

      if(borrowers.isEmpty())
        return new FMLQueryResult();

      Iterator it = borrowers.iterator();
      String line = "";

      while(it.hasNext())
      {
        bor = (Borrower)it.next();

        if(bor.getBorrowerTypeId() == Mc.BT_BORROWER)
        {
          firstname = bor.getBorrowerFirstName();
          lastname = bor.getBorrowerLastName();

          if((firstname != null && lastname != null) &&
            (firstname.length() > 0 && lastname.length() > 0))
          {
            val = firstname + format.space() + lastname;
            line += val + (it.hasNext()?",":"") + format.space();
          }
        }
      }

      if(line.length() > 0) {
        line = line.trim();
        if( line.endsWith(",")){
          line = line.substring(0,line.length()-1);
        }
      }

      fml.addString(line);
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
