package com.basis100.deal.docprep.deliver;

import com.basis100.deal.docprep.*;

/**
 *  Thrown in the event of a critical error while attempting Document Delivery via
 *  email, file system read-write, ftp.
 */

public class DocPrepDeliveryException extends DocPrepException
{


 /**
  * Constructs a <code>DocPrepDeliveryException</code> with no specified detail message.
  */

  public DocPrepDeliveryException()
  {
	  super();
  }

  /**
  * Constructs a <code>DocPrepDeliveryException</code> with the specified message.
  * @param   msg  the related message.
  */
  public DocPrepDeliveryException(String msg)
  {
    super(msg);
  }
}


