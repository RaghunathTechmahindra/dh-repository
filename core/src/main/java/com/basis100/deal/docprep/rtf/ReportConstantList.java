package com.basis100.deal.docprep.rtf;

import java.util.*;

public class ReportConstantList {
  private Hashtable constList;

  public ReportConstantList() {
    constList = new Hashtable();
  }

  public void addNewConstant(ReportConstant constToAdd) throws Exception
  {
    if(  constList.containsKey( constToAdd.getConstName() )  ){
      throw new Exception("Report constant " + constToAdd.getConstName() + "  already exists.");
    } else {
      constList.put(constToAdd.getConstName(), constToAdd);
    }
  }

  public ReportConstant getConstant(String pName) throws Exception
  {
     if(isConstantEntered(pName)){
       return (ReportConstant)constList.get(pName.trim().toLowerCase());
     }
     throw new Exception("Constant " + pName + "  does not exist.");
  }

  public boolean isConstantEntered(String pName){
    if(constList.containsKey(pName.trim().toLowerCase())){
      return true;
    } else {
      return false;
    }
  }
}