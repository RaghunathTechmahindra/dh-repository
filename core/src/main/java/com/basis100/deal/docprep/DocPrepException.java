package com.basis100.deal.docprep;


/**
 *  Thrown in the event of a critical error in DocumentGeneration
 */

public class DocPrepException extends Exception
{

 /**
  * Constructs a <code>DocPrepException</code> with no specified detail message.
  */

  public DocPrepException()
  {
	  super();
  }

  /**
  * Constructs a <code>DocPrepException</code> with the specified message.
  * @param   msg  the related message.
  */
  public DocPrepException(String msg)
  {
    super(msg);
  }
}


