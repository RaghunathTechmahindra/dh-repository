package com.basis100.deal.docprep.extract.doctag;

/**
* 03/Mar/2005 DVG #DG158 #922  New tags required,
         2 new classes created as new conditions inheriting from the original ones
 */

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class DebtsFromProceeds extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        boolean lbNoRent = format.getVersion().equals("@@NORENT@@");  //#DG158 flag no rent
        FMLQueryResult fml = new FMLQueryResult();

        try
        {
            if(de.getClassId() == ClassId.DEAL)
            {
              int pOffTypes[] = {Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS,Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE};
              Deal deal = (Deal) de;
              Liability oneLiability = new Liability(srk,null);
              Collection dealLiabilities = oneLiability.findByDealAndPayoffTypes((DealPK)deal.getPk(),pOffTypes);
              Iterator dealLiabilitiesIterator = dealLiabilities.iterator();
              StringBuffer outBuffer = new StringBuffer();;
              while( dealLiabilitiesIterator.hasNext() ){
                oneLiability = (Liability) dealLiabilitiesIterator.next();

                if(lbNoRent && (oneLiability.getLiabilityTypeId() == Mc.LIABILITY_TYPE_RENT))  //#DG158
                  continue;

                double lAmount = oneLiability.getLiabilityAmount();
                if(oneLiability.getPercentOutGDS() < 2d){
                  outBuffer.append(oneLiability.getLiabilityDescription());
                  outBuffer.append(lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? " debt of " : " au montant de ");
                  outBuffer.append(DocPrepLanguage.getInstance().getFormattedCurrency(lAmount, lang));
                  outBuffer.append(", ");
                  outBuffer.append(format.carriageReturn());
                }
              }
              fml.addString( this.cleanLastComma( outBuffer.toString() ));
            } else if (de.getClassId() == ClassId.BORROWER)
            {
                Borrower borrower = (Borrower)de;
                Liability liability = new Liability(srk, null);
                Collection c = liability.findByBorrower((BorrowerPK)borrower.getPk());

                if (!c.isEmpty())
                {
                    double amt;
                    Liability liab = null;
                    StringBuffer buff = new StringBuffer();

                    Iterator i = c.iterator();

                    while (i.hasNext())
                    {
                        liab = (Liability)i.next();

                        if(lbNoRent && (liab.getLiabilityTypeId() == Mc.LIABILITY_TYPE_RENT))  //#DG158
                          continue;

                        if (liab.getLiabilityPayOffTypeId() == Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS ||
                           liab.getLiabilityPayOffTypeId() == Mc.LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE)
                        {
                            amt = liab.getLiabilityAmount();

                            if(liab.getPercentOutGDS() < 2d){
                              buff.append(liab.getLiabilityDescription());

                              buff.append(lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? " debt of " : " au montant de ");

                              buff.append(DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang));
                              buff.append(", ");
                              buff.append(format.carriageReturn());
                            }
                        }
                    }
                    fml.addString(this.cleanLastComma(  buff.toString())  );
                }
            }
        }
        catch (Exception e)
        {
            srk.getSysLogger().error(e.getMessage());
            srk.getSysLogger().error(e);
            throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
        }

        return fml;
    }

    private String cleanLastComma(String inputStr){
      if(inputStr != null){
        String processStr = inputStr.trim();
        if (inputStr.endsWith(",")){
          processStr = processStr.substring(0,processStr.length()-1);
          return processStr;
        }
      }
      return inputStr;
    }

}
