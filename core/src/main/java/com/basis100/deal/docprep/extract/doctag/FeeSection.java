package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;


public class FeeSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;

    boolean enforceCMHC = false;
    boolean enforceGE = false;
    try
    {
      List fees = (List)deal.getDealFees();

      enforceCMHC = this.shouldForceMICMHC(deal,fees);
      enforceGE = this.shouldForceMIGE(deal,fees);

      Iterator fit = fees.iterator();
      DealFee current = null;

      while(fit.hasNext())
      {
        current = (DealFee)fit.next();

        Fee f = current.getFee();

        // Ticket #613
        if( shouldFeeBeListed(deal.getInstitutionProfileId(), f) )
        {
          if(!f.getPayableIndicator() && (current.getFeeAmount() != 0))
          {
            fml.addValue(current);
          }
        }

        //if(!f.getPayableIndicator() && (current.getFeeAmount() != 0))
        //{
        //  fml.addValue(current);
        //}
      }

      //========================================================================
      // now force inserting CMHC or GE fee into the system; Why? because of the
      // request appended at the end of this file.
      //========================================================================
      if(enforceCMHC){
        Fee oneFee = new Fee(srk,null,3);  // find the constant if exists
        if(oneFee != null){
          fml.addValue(oneFee);
        }
      }
      if(enforceGE){
        Fee oneFee = new Fee(srk,null,15);  // find the constant if exists
        if(oneFee != null){
          fml.addValue(oneFee);
        }
      }
      return fml;

    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }

  private boolean shouldForceMICMHC(Deal pDeal, List pFees) throws Exception
  {
    if( ! (pDeal.getMIPremiumAmount() > 0d)){return false;}
    if( pDeal.getMortgageInsurerId() != Mc.MI_INSURER_CMHC ){return false;}

    Iterator it = pFees.iterator();
    while( it.hasNext() ){
      DealFee df = (DealFee)it.next();
      if( df.getFee().getFeeTypeId() == Mc.FEE_TYPE_CMHC_FEE ) {return false;}
    }
    return true;
  }

  private boolean shouldForceMIGE(Deal pDeal, List pFees) throws Exception
  {
    if( ! (pDeal.getMIPremiumAmount() > 0d)){return false;}
    if( pDeal.getMortgageInsurerId() != Mc.MI_INSURER_GE ){return false;}

    Iterator it = pFees.iterator();
    while( it.hasNext() ){
      DealFee df = (DealFee)it.next();
      if( df.getFee().getFeeTypeId() == Mc.FEE_TYPE_GE_CAPITAL_FEE ) {return false;}
    }
    return true;
  }

  /**
   * Not all fees should be listed in commitment letter(CL). Fee ids that we do not want to
   * show in CL should be listed in mossys.properties file in the following format:
   * com.basis100.commitment.suppress.fees = 1,2,3 where 1,2 and 3 represent fee id codes.
   * In case that no such property exists or it is empty fee will be shown in CL.
   * @param pFee  Fee that has to be evaluated.
   */
   // Ticket #613
  private boolean shouldFeeBeListed(int institutionProfileID, Fee pFee){
    String feeList = PropertiesCache.getInstance().getProperty(institutionProfileID, "com.basis100.commitment.suppress.fees");
    if(feeList == null || feeList.trim().equals("")){
      return true;
    }
    StringTokenizer tok = new StringTokenizer(feeList,",");
    int oneFeeId;
    while(tok.hasMoreTokens()){
      oneFeeId = Integer.parseInt( (String)tok.nextToken() ) ;
      if( pFee.getFeeId() == oneFeeId ){
        return false;
      }
    }
    return true;
  }

}


/*
-----Original Message-----
From: David Coleman
To: Zivko Radulovic; Paul Lewis; Joe Brancati; Ceri Williams; Robert Barker
Cc: Brant Eichfuss; Debbie Jaipargas; Lavern Dowsley
Sent: 1/16/02 11:48 AM
Subject: Commitment letter <FEE>/<FEEVERB>

COOP is concerned that the commitment letter when produced before MI
approval has been received is not displaying the MI application fee.
Since we have code in the MI Process that deletes all MI related fees
when requesting MI, it would not be prudent to insert the MI application
fee early.  Therefore we can only 'fudge' the fee in the commitment
letter

Need a change to tag population

IF deal.MIPremium > 0 and deal.mortgageInsurerId = 1 (CMHC) and a deal
fee of type 3 (CMHC Fee) not in DealFee table then
	insert into FEEVERB fee.feeDescription where fee.feeId = 3
	insert into FEE fee.defaultFeeAmount where fee.feeId = 3

ElseIf deal.MIPremium > 0 and deal.mortgageInsurerId = 2 (GE Capital)
and a deal fee of type 15 (GE Fee) not in DealFee table then
	insert into FEEVERB fee.feeDescription where fee.feeId = 15
	insert into FEE fee.defaultFeeAmount  where fee.feeId = 15



this will allow client to have some idea of what the Mortgage Fees will
be once the MI Approval comes back.  When the MI Approval comes back it
automatically sets up the Premium Fee, PST Fee and MI Application Fee.
Once this has been done the above code will detect the presence of the
fee and skip the insertion.



David Coleman
Senior Business Analyst
Basis100 Inc.  http://www.basis100.com
33 Yonge Street, Suite 900
Toronto, Ontario, CANADA M5E1G4
Ph. 416 364 6085 x 138 Fax 416 364 2941
*/
