package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.calc.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.picklist.BXResources;

/**
 *
 * <p>Title: new tags for AGF </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author Catherine Rutgaizer, Dec 2004
 * i know that this code is terrible
 * @version 1.0
 *
 */
// #756 - AGF: Solicitor Package Changes
public class BranchAddressProvince extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());
    FMLQueryResult fml = new FMLQueryResult();

    String val = null;
    Deal deal = null;

    try
    {
      if (de.getClassId() != ClassId.DEAL){
        return fml;
      }

      deal = (Deal)de;
      BranchProfile b = new BranchProfile(srk, deal.getBranchProfileId());
      if( b == null ){ return fml; }

      Contact c = b.getContact();
      if( c == null ){ return fml; }

      Addr addr = c.getAddr();
      val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "Province", addr.getProvinceId(),lang);

      if(val != null || val.length() != 0)
      {
        fml.addValue(val,fml.STRING);
      }
      return fml;
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

  }
}
