package com.basis100.deal.docprep.extract;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;
import com.basis100.xml.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.docprep.DocPrepLanguage;
import org.w3c.dom.*;

/**
 * immutable wrapper for results of tag extraction queries  <br>
 * The FMLQueryResult is one way English - in other words - <br>
 * all other locales (only french in this case are built (translated) from English.
 * This works fine with the MOS db (in english)
 *
 * Null results are handled as follows: If the English value is null or an empty string
 * its stored value is an empty String.
 */
public class FMLQueryResult
{

  private ArrayList values;
  private ArrayList entities;
  private String fileResult; // path to file to taken as result

  private boolean hasNonEmptyValues = false;

  private Map idMap;
  private String name = "";

  public static final int STRING = 0;
  public static final int CURRENCY = 1;
  public static final int DATE = 2;
  public static final int ENTITY = 3;
  public static final int RATE = 4;
  public static final int TERM = 5;
  public static final int YEARS_AND_MONTHS = 6;
  public static final int PHONE_NUMBER = 7;
  public static final int RATIO = 8;
  public static final int TIME = 9;

  boolean debug = true;

  int action = -1;

  public FMLQueryResult()
  {
    values = new ArrayList();
    entities = new ArrayList();
    idMap = new HashMap();
  }

  public FMLQueryResult(String tagName)
  {
    values = new ArrayList();
    entities = new ArrayList();
    idMap = new HashMap();
    name = tagName;
  }

  public FMLQueryResult(DealEntity e, int action)
  {
    this();
    this.action = action;
  }

  public FMLQueryResult(DealEntity e, int action, String filePath)
  {
    this(e,action);
    this.fileResult = filePath;
  }

  public boolean hasValue()
  {
    return !(values.isEmpty());
  }

  public boolean hasNonEmptyValue()
  {
    return this.hasNonEmptyValues;
  }

  public boolean hasEntity()
  {
    return !(entities.isEmpty());
  }

  public String getFirstStringValue(int lang)
  {
    if(values.isEmpty()) return null;

    FMLResultValue resultval =(FMLResultValue)values.get(0);

    return resultval.getValue(lang);
  }

  public String getValue(String id, int lang)
  {
    if(idMap.containsKey(id))
    {
      FMLResultValue resultval =(FMLResultValue)idMap.get(id);
      return resultval.getValue(lang);
    }

    return null;
  }

  private void storeValue(String en, String fr, String id)
  {
    FMLResultValue resultval =  new FMLResultValue(en,fr,id);

    if(!resultval.isEmptyVal())
     this.hasNonEmptyValues = true;

    values.add(resultval);

    if(id != null)
     idMap.put(id,resultval);
  }


  public void add(FMLQueryResult other)     //ALERT track valid addition but allow 'rollback'
  {
    this.values.addAll(other.getValues());

    this.entities.addAll(other.getEntities());

    this.idMap.putAll(other.getIdMap());
  }

  public void addYearsAndMonths(YearsAndMonths val)
  {
    addYearsAndMonths(val,null);
  }

  public void addYearsAndMonths(YearsAndMonths val, String id)
  {
    if(val == null) return;

    String en = val.getValue(DocPrepLanguage.ENGLISH);
    String fr = val.getValue(DocPrepLanguage.FRENCH);

    storeValue(en, fr, id);
  }

  public void addValue(Date d)
  {
    addValue(d,null);
  }

  public void addValue(Date d, String id)
  {
    if(d == null) return;

    String en = DocPrepLanguage.getInstance().getFormattedDate(d,DocPrepLanguage.ENGLISH);
    String fr = DocPrepLanguage.getInstance().getFormattedDate(d,DocPrepLanguage.FRENCH);
    storeValue(en,fr,id);
  }

  public void addValue(DealEntity ent)
  {
    if(ent != null)
     entities.add(ent);
  }

  public void addAll(List ents)
  {
     entities.addAll(ents);
  }


  public void addString(String str)
  {
    addValue(str,STRING,null);
  }


  public void addString(String str, String id)
  {
    addValue(str,STRING,id);
  }


  public void addTerm(String term, String id)
  {
    String en = DocPrepLanguage.getInstance().getTerm(term,DocPrepLanguage.ENGLISH);
    String fr = DocPrepLanguage.getInstance().getTerm(term,DocPrepLanguage.FRENCH);
    storeValue(en,fr,id);
    return;
  }


  public void addPhoneNumber(String phone, String id)
  {
    addPhoneNumber(phone,null, id);
  }

  public void addPhoneNumber(String phone, String ext, String id)
  {
    String en = "";
    try
    {
      en = StringUtil.getAsFormattedPhoneNumber(phone, ext);
    }
    catch(Exception e){;}

    storeValue(en,en,id);
    return;
  }

  public void addRate(double d)
  {
    addRate(d,null);
  }

  public void addRate(double d, String id)
  {
     String en = DocPrepLanguage.getInstance()
        .getFormattedInterestRate(d,DocPrepLanguage.ENGLISH);
     String fr = DocPrepLanguage.getInstance()
        .getFormattedInterestRate(d,DocPrepLanguage.FRENCH);

     storeValue(en, fr, id);
  }

  public void addRatio(double d)
  {
    addRate(d,null);
  }

  public void addRatio(double d, String id)
  {
     String en = DocPrepLanguage.getInstance()
        .getFormattedRatio(d,DocPrepLanguage.ENGLISH);
     String fr = DocPrepLanguage.getInstance()
        .getFormattedRatio(d,DocPrepLanguage.FRENCH);

     storeValue(en, fr, id);
  }

  public void addCurrency(double d)
  {
    addCurrency(d, null);
  }

  public void addCurrency(double d, String id)
  {
     String en = DocPrepLanguage.getInstance().getFormattedCurrency(d,DocPrepLanguage.ENGLISH);
     String fr = DocPrepLanguage.getInstance().getFormattedCurrency(d,DocPrepLanguage.FRENCH);

     storeValue(en, fr, id);
  }

  public void addValue(int val)
  {
    addValue(val,null);
  }

  public void addValue(int val, String id)
  {
    String sval = String.valueOf(val);
    storeValue(sval,sval,id);
  }

  public void addValue(String str, int type)
  {
    addValue(str,type,null);
  }


  public void addValue(String str, int type, String id)
  {
    if(str == null)
     return;

    if(type == TERM)
    {
      addTerm(str,id);
      return;
    }
    else if(type == CURRENCY)
    {
      try
      {
        double dbl = Double.parseDouble(str);
        addCurrency(dbl,id);
      }
      catch(Exception e)
      {
        addValue(str,STRING,id);
      }

    }
    else if(type == DATE)
    {
      try
      {
        if(str.length() > 19)
         str = str.substring(0,19);  //strip off nanoseconds

        addValue(TypeConverter.dateFrom(str));
      }
      catch(Exception e)
      {
        addValue(str,STRING,id);
      }

    }
    else if(type == TIME)
    {
      try
      {
        if(str.length() > 19)
         str = str.substring(0,19);  //strip off nanoseconds

        addValue(TypeConverter.dateFrom(str));
      }
      catch(Exception e)
      {
        addValue(str,STRING,id);
      }

    }
    else if(type == YEARS_AND_MONTHS)
    {
      try
      {
        addYearsAndMonths(new YearsAndMonths(str),id);
      }
      catch(Exception e)
      {
        addValue(str,STRING,id);
      }
    }
    else if(type == RATE)
    {
      try
      {
        double dbl = Double.parseDouble(str);
        addRate(dbl, id);
      }
      catch(Exception e)
      {
        addValue(str,STRING,id);
      }
    }
    else if(type == RATIO)
    {
      try
      {
        double dbl = Double.parseDouble(str);
        addRatio(dbl, id);
      }
      catch(Exception e)
      {
        addValue(str,STRING,id);
      }
    }
    else if(type == PHONE_NUMBER)
    {
      try
      {
        addPhoneNumber(str,id);
      }
      catch(Exception e)
      {
        addString(str,id);
      }
    }
    else
    {
      storeValue(str,str,id);
    }

  }


  public void addValue(boolean val, String id)
  {
    String str = TypeConverter.stringFromBoolean(val,"No","Yes");

    if(str == null) return;

    String en = DocPrepLanguage.getInstance().getTerm(str,DocPrepLanguage.ENGLISH);

    String fr = DocPrepLanguage.getInstance().getTerm(str,DocPrepLanguage.FRENCH);

    storeValue(en, fr, id);
    return;
  }

  public void addBoolean(boolean val, String id)
  {
    String en = TypeConverter.stringFromBoolean(val,"No","Yes");
    String fr = DocPrepLanguage.getInstance().getTerm(en,DocPrepLanguage.FRENCH);

    storeValue(en,fr,id);
  }


  public void addComponentResult(ComponentResult result)
  {
     addComponentResult(result,null);
  }

  public void addComponentResult(ComponentResult result, String id)
  {
    String fr = "";
    String en = "";

    while(result.hasNext())
    {
      Object next = result.next();
      int type = result.type();

      if(type == DATE)
      {
        en += DocPrepLanguage.getInstance().getFormattedDate((Date)next,DocPrepLanguage.ENGLISH);
        fr += DocPrepLanguage.getInstance().getFormattedDate((Date)next,DocPrepLanguage.FRENCH);
      }
      else if(type == CURRENCY)
      {
        Double val = (Double)next;
        en += DocPrepLanguage.getInstance().getFormattedCurrency(val.doubleValue(),DocPrepLanguage.ENGLISH);
        fr += DocPrepLanguage.getInstance().getFormattedCurrency(val.doubleValue(),DocPrepLanguage.FRENCH);
      }
      else if(type == RATE)
      {
        Double val = (Double)next;
        en += DocPrepLanguage.getInstance().getFormattedInterestRate(val.doubleValue(),DocPrepLanguage.ENGLISH);
        fr += DocPrepLanguage.getInstance().getFormattedInterestRate(val.doubleValue(),DocPrepLanguage.FRENCH);
      }
      else if(type == RATIO)
      {
        Double val = (Double)next;
        en += DocPrepLanguage.getInstance().getFormattedRatio(val.doubleValue(),DocPrepLanguage.ENGLISH);
        fr += DocPrepLanguage.getInstance().getFormattedRatio(val.doubleValue(),DocPrepLanguage.FRENCH);
      }
      else if(type == TERM)
      {
        String str = (String)next;

        en += DocPrepLanguage.getInstance().getTerm(str,DocPrepLanguage.ENGLISH);
        fr += DocPrepLanguage.getInstance().getTerm(str,DocPrepLanguage.FRENCH);
      }
      else if(type == PHONE_NUMBER)
      {
        String str = (String)next;

        en += handlePhoneNumber(str,null);
        fr += handlePhoneNumber(str,null);   //fr += en;

      }
      else if(type == STRING)
      {
        en += (String)next;
        fr += (String)next;
      }
    }

    storeValue(en,fr,id);

  }

  public void setValues(Collection c, int type)
  {
    if(type == ENTITY)
    {
      entities = new ArrayList(c);
    }
  }

  public List getStringValues(int lang)
  {
    List out = new ArrayList();

    Iterator it = values.iterator();

    FMLResultValue rv = null;

    while(it.hasNext())
    {
      rv = (FMLResultValue)it.next();
      out.add(rv.getValue(lang));
    }
    return out;
  }


  public String getFirstNormalizedValue(int lang)
  {
    List values = (ArrayList)getStringValues(lang);

    if(values.size() > 0)
    {
      return XmlWriter.putEntities((String)values.get(0));
    }

    return null;
  }

  public String handlePhoneNumber(String nbr, String ext)
  {
    try
    {
      return StringUtil.getAsFormattedPhoneNumber(nbr, ext);
    }
    catch(Exception e)
    {
      return "";
    }
  }



  public List getValues()
  {
    return this.values;
  }

  public Map getIdMap()
  {
    return this.idMap;
  }

  public List getEntities()
  {
    return this.entities;
  }

  public void setAction(int act)
  {
    this.action = act;
  }

  public int getAction()
  {
    return this.action;
  }

  public String getFileResult()
  {
    return this.fileResult;
  }

  public void setName(String newName)
  {
    name = newName;
  }

  public String getName()
  {
    return name;
  }



  public void reset()
  {
    values = new ArrayList();
    entities = new ArrayList();
    idMap = new HashMap();
  }

  //this container will have to be expanded (or scrapped) to encompass further locales
  //for  now it is one way English - in other words all other locales (only french in this case
  //are built (translated) from english

  class FMLResultValue
  {
     String[] vals;
     String id;
     boolean emptyVal = false;

     public FMLResultValue(String en, String fr, String idval)
     {
       vals = new String[2];
       if(en == null || en.length() == 0)
       {
          en = "";
          emptyVal = true;
       }
       if(fr == null) fr = en;
       if(idval == null) idval = en;

       vals[0] = en;
       vals[1] = fr;

       id = idval;
     }


     public String getId()
     {
       return id;
     }

     public String getValue(int lang)
     {
       try
       {
         return vals[lang];
       }
       catch(ArrayIndexOutOfBoundsException e)
       {
         return vals[0];
       }
     }

     public boolean isEmptyVal()
     {
       return emptyVal;
     }



     /**
    *  @return true if this CalcParam Classid and field are equal the params
    *  else return false;
    */
    public boolean equals(Object object)
    {
      if(this == object)
        return true;
      else if(object == null || getClass() != object.getClass() )
        return false;

      FMLResultValue other  = (FMLResultValue)object;


      if(other.getId().equals(this.id))
        return true;

       return false;
    }

    public int hashCode()
    {
      return id.hashCode();
    }


  }


}
