package com.basis100.deal.docprep.extract.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;

import MosSystem.Mc;

import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DisclosureQuestion;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Province;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.date.DateUtilCommon;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class DisclosureDataProvider {

  	private static final String CHECKED_CHKBOX = "1";
    // Added Total Checked Fee for summation of all the fees for the Form.
  	private static double totalCheckedFees = 0;
	private static final String UNCHECKED_CHKBOX = "0";
	
	public static final int [] pMTG_BROKER_FEES = {
			Mc.FEE_TYPE_BROKER_FEE
	};
	
	//Sasha - #4683, removed Mc.FEE_TYPE_CASH_BACK
	public static final int [] pOTHER_LENDER_FEES = {
			Mc.FEE_TYPE_BRIDGE_FEE, Mc.FEE_TYPE_BUYDOWN_FEE, Mc.FEE_TYPE_STANDBY_FEE,
			Mc.FEE_TYPE_AGENT_REFERRAL_FEE, Mc.FEE_TYPE_ORIGINATION_FEE, Mc.FEE_TYPE_APPLICATION_FEE,
			Mc.FEE_TYPE_CANCELLATION_FEE, Mc.FEE_TYPE_COUREER_FEE
	};
	public static final int [] pAPPRAISAL_FEES = {
			Mc.FEE_TYPE_APPRAISAL_FEE, Mc.FEE_TYPE_INSPECTION_FEE,
			Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL
	};
	public static final int [] pOTHER1_FEES = {
			Mc.FEE_TYPE_CMHC_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PREMIUM, 
			Mc.FEE_TYPE_AIG_UG_PREMIUM
	};
	public static final int [] pOTHER2_FEES = {
			Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM,
			Mc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM
	};
	public static final int [] pOTHER3_FEES = {
			Mc.FEE_TYPE_CMHC_FEE, Mc.FEE_TYPE_GE_CAPITAL_FEE, Mc.FEE_TYPE_AIG_UG_FEE,
	};
    public static final int[] pBONUS_FEES = { Mc.FEE_TYPE_BONUS_FEE };

	/*********************************************************************************************************
     * Since this class can be called from front-end as well as back-end, there is no guarantee that the
     * locale used will be the same between a front-end display and a back-end generation of the document.
     * Therefore, we introduce internal date format so that a client would know how to extract a date from the
     * string sent by extractor.
     */
    public static final String DDP_DATE_FORMAT = "dd-MMMM-yyyy";
	
	private Deal mDeal;
	private SessionResourceKit mSrk;
	private int mLangId;
	private DocumentFormat mFormat;
    private int mDocumentTypeId;
    // private int lang;
	private double mDaysInYear;
	private double mBalanceAtEndOfTerm;
	private double mPaymentsPerYear;
    // private Deal deal;
    private SessionResourceKit srk;
	private Collection borrowersList;
	Collection dealFees;
	
	
	/**
	 * Constructor
	 * 
	 * @param pSrk SessionResourceKit
	 * @param pDeal Deal
	 * @param pLang int
	 * @throws Exception 
	 */
	public DisclosureDataProvider(SessionResourceKit pSrk, Deal pDeal, int pLang) {
		String decimalYear = PropertiesCache.getInstance().getProperty(pDeal.getInstitutionProfileId(), "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
		
		mDeal = pDeal;
		mSrk = pSrk;
		mLangId = pLang;
		
		mBalanceAtEndOfTerm = -1d;
		mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
		mPaymentsPerYear = pCalcNumOfPaymentsPerYear(mDeal.getPaymentFrequencyId(), mDaysInYear);
		
		try {
			borrowersList = mDeal.getBorrowers();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			dealFees = mDeal.getDealFees();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Setters
	 *
	 */
    protected void setDocumentFormat(DocumentFormat pFormat) {
        mFormat = pFormat;
    }

    protected void setDocumentTypeId(int pDocumentTypeId) {
        mDocumentTypeId = pDocumentTypeId;
    }

   /**
    * Returns shortLenderName
    * 
    * @return String
    */
   protected String exLenderShortName() {
	   String val = null;
        try {
		   val = (new LenderProfile(mSrk, mDeal.getLenderProfileId())).getLPShortName();
	   }
	   catch (Exception e) {
		   String msg = "DisclosureDataProvider failed while extracting lender's short name." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return val;
   }
   /**
    * Returns RegistrationName
    * 
    * @return String
    */
   
   protected String exLenderRegistrationName() {
	   String val = null;
	   try{
		   val = (new LenderProfile(mSrk, mDeal.getLenderProfileId())).getRegistrationName();
	   }
	   catch (Exception e) {
		   String msg = "DisclosureDataProvider failed while extracting lender's short name." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return val;
   }


   // TAR # 1612 - Added method for getting Mortgage Broker Name
   /**
    * Returns mrtgBrkName
    * 
    * @return String
    */
   
   protected String exMrtgBrkName(){
	   String val = null;
	   try{
		   Contact contact = (new SourceOfBusinessProfile(mSrk, mDeal.getSourceOfBusinessProfileId())).getContact(); // TAR : 1612 - Formatted the name
		   if(contact != null){
			   val = formBorrowerName(contact.getContactFirstName(),contact.getContactMiddleInitial(),contact.getContactLastName());
		   }
	   }
	   catch (Exception e) {
		   String msg = "DisclosureDataProvider failed while extracting Mortgage Broker's short name." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return val;	   
   }
   
   // TAR # 1612 - Added method for getting Mortgage Broker Addres
    /**
     * The method is used to retrieve the address of the Mortgage Broker. The method in turn calls
     * pFormatAddress to format the address in the standard format
     * 
     * @return String - Formatted address of the mortgage broker
     */
    protected String exMrtgBrkAddr() {
        String val = new String();
        try {
            Addr addr = (new SourceOfBusinessProfile(mSrk, mDeal.getSourceOfBusinessProfileId())).getContact()
                .getAddr();
            if (addr != null) {
                val = pFormatAddress(addr);
            }
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting Mrtg Broker address line."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            mSrk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }
   
    /**
     * The method is used to calculatee number of payments per year. The method uses mPaymentsPerYear
     * value to calculate the value.
     * 
     * @return long - Number of payments per year calculated value.
     */
   protected long exCalcNumOfPaymentsPerYear() {
		return Math.round(mPaymentsPerYear
				* (mDeal.getActualPaymentTerm() / 12d));
	}
   
   /**
    * Returns dealId
    * 
    * @return int
    */
   protected int exDealId() {
	   int val = 0;
	   try {
		   val = mDeal.getDealId();
	   }
	   catch (Exception e){
		   String msg = "DisclosureDataProvider failed while extracting deal id." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	 
	   }
	   return val;
   }

   /**
    * Returns legalLines of a primary property (if any) or null
    *
    * @param pProperty Property
    * @return String
    */
   protected String exLegalLines(Property pPrimary) {
	   String line1 = null;
	   try {
		   if(pPrimary.isPrimaryProperty()) {
			   line1 = pPrimary.getLegalLine1();

			   String line2 = pPrimary.getLegalLine2();
			   if (line2 != null) line1 += mFormat.space() + line2;

			   String line3 = pPrimary.getLegalLine3();
			   if (line3 != null) line1 += mFormat.space() + line3;
		   }
	   }
	   catch (Exception e) {
		   String msg =  "DisclosureDataProvider failed while extracting legal lines." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return line1;
   }

   /**
   	* Returns an address of a primary property formated for Express
   	*
   	* @param p Property
   	* @return String
   	*/
   protected String exPrimaryPropertyAddressLine1(Property p) {
	   String val = new String();
	   try {


		   if(!pIsEmpty(p.getUnitNumber())){
			   val = p.getUnitNumber() + " - ";
		   }

		   if (!pIsEmpty(p.getPropertyStreetNumber())){
			   val += p.getPropertyStreetNumber()  + mFormat.space();
		   }

		   if (!pIsEmpty(p.getPropertyStreetName())){
			   val +=  p.getPropertyStreetName() + mFormat.space();
		   }
		  
		   if (!pIsEmpty(p.getStreetTypeDescription())){
		  			   val +=  BXResources.getPickListDescription(mSrk.getExpressState().getDealInstitutionId(), "StreetType",  p.getStreetTypeId() , mLangId) + mFormat.space();
		  		   }	
		   
		   if (p.getStreetDirectionId() != 0){
			   val += pReadBXResources("StreetDirection", Integer.toString(p.getStreetDirectionId())) ;
		   }


		   if(!val.equals("")){
			   val += ", ";
		   }
		   if (!pIsEmpty(p.getPropertyCity())){
			   val +=  p.getPropertyCity() + ", ";
		   }
		   
		   
		   //merged from StarTeam
		   if (p.getProvinceId() != 0 ) {
			   Province prov = new Province(mSrk, p.getProvinceId());
			   if (!pIsEmpty(prov.getProvinceName()))
				   val +=  prov.getProvinceName()  + mFormat.space() ;
		   }
		   
//		 merged from StarTeam
		   if (!pIsEmpty(p.getPropertyPostalFSA()) & !pIsEmpty(p.getPropertyPostalLDU())) {
			   val +=  p.getPropertyPostalFSA() + mFormat.space() + p.getPropertyPostalLDU();
		   }
		   
	   }
	  
	   catch (Exception e){
		   String msg = "DisclosureDataProvider failed while extracting primary property address line." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return val;
   }

   /**
     * Returns a response to the question 401020E
    * 
    * @param questionID
    * @return
    */
   protected String exQuestion(String questionID) { 
	   String val = "";
	   String errMsg = "DisclosureDataProvider failed while extracting user entry for the question " + questionID + ".";
	   try {
		   DisclosureQuestion dq = new  DisclosureQuestion(mSrk);
		   dq.findByDealIdDocumentTypeIdAndQuestionCode(mDeal.getDealId(), mDocumentTypeId, questionID); // Modified the method, passing DocumentTypeId rather than picking it from the SubString of the Question Id		   
		   val = dq.getResponse()==null?"":dq.getResponse();
		  
	   } 
	   catch (RemoteException e) {
		   mSrk.getSysLogger().warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));
	   } 
	   catch (FinderException e) {
		   mSrk.getSysLogger().warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));
	   } 
	   catch (Exception e) {
		   mSrk.getSysLogger().warning(this.getClass(), errMsg  + (e.getMessage() != null ? "\n" + e.getMessage() : ""));		   
	   }
	   return val;
   }

   /**
    * Returns a response to the question
    * 
    * @param documentID
    * @param questionID
    * @return
    * @throws RemoteException
    * @throws FinderException
    */
   protected String exQuestion(String documentID, String questionID)
   throws RemoteException,  FinderException {
   	
   	String val = null;
   	DisclosureQuestion dq = new DisclosureQuestion(mSrk);
   	String errMsg = "DisclosureDataProvider failed while extracting user entry for the question " + questionID + " and "+ "documentID"+documentID + ".";
   	
   	try {
   		dq.findByDealIdAndQuestionCode(mDeal.getDealId(), documentID + questionID);
   		val = dq.getResponse();
        } catch (RemoteException e) {
            mSrk.getSysLogger()
                .warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));
        } catch (FinderException e) {
            mSrk.getSysLogger()
                .warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));
        } catch (Exception e) {
            mSrk.getSysLogger()
                .warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));
	   }
       return val;
    }
   
   /**
    * Returns commitment issue date
    *
    * @return Date
    */
   protected Date exCommitmentIssueDate() {
	   Date val = null;
	   try {
		   val = mDeal.getCommitmentIssueDate();
	   }
	   catch (Exception e) {
		   String msg = "DisclosureDataProvider failed while extracting commitment issue date." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);		   
	   }
	   return val;
   }

   /**
    * Returns lien position description of a mortgage
    *
    * @return String
    */
   protected String exLienPosition() {
	   String val = null;
	   try {
		   val = pReadBXResources("LienPosition", Integer.toString(mDeal.getLienPositionId()));      
	   }
	   catch (Exception e) {
		   String msg = "DisclosureDataProvider failed while extracting lien position." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);
	   }
	   return val;
   }
  
   /**
    * Returns lien position description of a mortgage
    *
    * @return String
    */
   protected String exMortgageTypeOne() {
   	String val = null;
   	try {
   		if(mDeal.getLienPositionId() == Mc.LIEN_POSITION_FIRST)
   			val = CHECKED_CHKBOX;
   		else
   			val = UNCHECKED_CHKBOX;
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting lien position." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
   
   /**
    * Returns lien position description of a mortgage
    *
    * @return String
    */
   protected String exMortgageTypeTwo() {
   	String val = null;
   	try {
   		if(mDeal.getLienPositionId() == Mc.LIEN_POSITION_SECOND)
   			val = CHECKED_CHKBOX;
   		else
   			val = UNCHECKED_CHKBOX;
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting lien position." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
   
   /**
    * Returns lien position description of a mortgage
    *
    * @return String
    */
   protected String exMortgageTypeThree() {
   	String val = null;
   	try {
   		if(mDeal.getLienPositionId() == Mc.LIEN_POSITION_THIRD)
   			val = CHECKED_CHKBOX;
   		else
   			val = UNCHECKED_CHKBOX;
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting lien position." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
   
   /**
    * Returns lien position description of a mortgage
    *
    * @return String
    */
   protected String exMortgageTypeOther() {
	   String val = null;
	   try {
            if (mDeal.getLienPositionId() != Mc.LIEN_POSITION_FIRST
              && mDeal.getLienPositionId() != Mc.LIEN_POSITION_SECOND
              && mDeal.getLienPositionId() != Mc.LIEN_POSITION_THIRD){
                val = CHECKED_CHKBOX;
			 
          }
          else{
			 val = UNCHECKED_CHKBOX;
	   }
         
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting lien position."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);
	   }
	   return val;
   }
   
   
   /**
    * Retrieves the total loan amount on the deal
    *
    * @return double
    */
   protected double exTotalLoanAmount() {
   	double val = 0d;
   	try {
   		val = mDeal.getTotalLoanAmount();          
   	}
   	catch (Exception e){
   		String msg = "DisclosureDataProvider failed while extracting total loan amount." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	  
   	}
   	return val;
   }
  

   /**
    * Retrieves the total loan amount on the deal for the requested lineId position
    *
    * @return double
    */
   protected double exTotalLoanAmount(int lienId) {
   	double val = 0d;
   	try {
   		if(mDeal.getLienPositionId() == lienId) {
   			val = mDeal.getTotalLoanAmount();
   		}
   	}
   	catch (Exception e){
   		String msg = "DisclosureDataProvider failed while extracting total loan amount." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	  
   	}
   	return val;
   }   
  

   /**
    * Returns deal.netInterestRate for interestTypeId = 0
    *
    * @return double
    */
   protected double exNetInterestRateFixed() {
   	return exNetInterestRate("fixed");
   }

   /**
    * Returns deal.netInterestRate for interestTypeId != 0 
    *
    * @return double
    */
   protected double exNetInterestRateVariable() {
   	return exNetInterestRate("variable");
   }

   /**
    * Returns deal.netInterestRate for interestTypeId = 0 or for
    * interestTypeId != 0
    *
    * @param which String
    *    which = "fixed" or
    *    which = "variable"
    *
    * @return double
    */
   protected double exNetInterestRate(String which) {
   	double val = 0;
   	try{
   		MtgProd mp = new MtgProd( mSrk, null, mDeal.getMtgProdId());
   		if( which.equalsIgnoreCase("fixed")) {
   			if (mp.getInterestTypeId() == Mc.INTEREST_TYPE_FIXED) {
   				val = mDeal.getNetInterestRate();
   			}
   		}
   		else if( which.equalsIgnoreCase("variable")) {
   			if (mp.getInterestTypeId() != Mc.INTEREST_TYPE_FIXED) {
   				val = mDeal.getNetInterestRate();
   			}
   		}
   	}
   	catch( Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting net interest rate." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }

   /**
    * Returns "Monthly" or "Semi Annualy"
    *
    * @return String
    */
   protected String exInterestCompoundingDescription() {
   	String val = null;
   	try{
   		MtgProd mp = new MtgProd(mSrk, null, mDeal.getMtgProdId());
   		if( mp.getInterestCompoundingId() == Mc.INTEREST_COMPOUNDID_MONTHLY )
   			val = "Monthly";
   		else if( mp.getInterestCompoundingId() == Mc.INTEREST_COMPOUNDID_SEMIANNUAL )
   			val = "Semi-Annually";
   	}
   	catch( Exception e ){
   		String msg = "DisclosureDataProvider failed while extracting compounding interest description" + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	 
   	}
   	return val;
   }


   /**
    * Returns payment frequency from deal.paymentFrequencyId as a string
    *
    * @return String
    */
   protected String exPaymentFrequency() {
	   String ret = null;
   	   try {
   		   ret = pReadBXResources("PaymentFrequency", Integer.toString(mDeal.getPaymentFrequencyId()));
   	   }
   	   catch( Exception e ) {
   		   String msg = "DisclosureDataProvider failed while extracting payment frequency." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		   mSrk.getSysLogger().warning(this.getClass(), msg);
   	   }
	   return ret;
   }

   /**
    * Returns toal payment amount
    *
    * @return String
    */
   protected double exTotalPaymentAmount() {
	   double val = 0d;
	   try {
		   val = mDeal.getPandiPaymentAmount();		   
	   }
	   catch (Exception e){
		   String msg = "DisclosureDataProvider failed while extracting total payment amount." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return val;
   }

   /**
    * Returns amortization term of the mortgage in years
    *
    * @return String
    */
   protected String exAmortizationTermInYears() {
   	String val = null;
   	try {
   		val = Integer.toString(mDeal.getAmortizationTerm()/12);
   	}
   	catch (Exception e){
   		String msg = "DisclosureDataProvider failed while extracting amortizationterm in years." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }

   /**
    * Returns "only" if repaymentTypeId = 1 or 2, otherwise "Including"
    *
    * @return String
   	*/
   protected String exRepaymentType() {
   	String val = null;
   	try {
   		val = pReadBXResources("RepaymentType", Integer.toString(mDeal.getRepaymentTypeId()));
   		if( val.equals("1") || val.equals("2"))
   			val = "Only";
   		else
   			val = "Including";
   	}
   	catch (Exception e)  {
   		String msg = "DisclosureDataProvider failed while extracting repayment type." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }

   
   /**
    * Checks the current deal's fees against feeTypeIDs included in the input array and
    * sums those that have been used, ignoring those with "WAIVED" status
    *
    * @param pFeeArray int[]
    * @return double
    */
   protected double exSelectedFeesTotalAmt(int[] pFeeArray) {
	   double val = 0d;
	   try{
		   Collection dealFees = mDeal.getDealFees();
		   Iterator it = dealFees.iterator();
		   double feeTotal = 0;

		   while(it.hasNext()) {
			   DealFee oneFee = (DealFee) it.next();
			   int feeTypeID = oneFee.getFee().getFeeTypeId();
			   int feeStatusID = oneFee.getFeeStatusId();
			   for(int i=0; i<pFeeArray.length; i++ ) {
				   if (feeTypeID == pFeeArray[i] & feeStatusID != Mc.FEE_STATUS_WAIVED) {
					   feeTotal += oneFee.getFeeAmount();
					   break;
				   }
			   }
		   }
		   val = feeTotal;
		   totalCheckedFees += val; // TAR : 1612 - Added total Checked fee
	   }
	   catch(Exception e){
		   String msg = "DisclosureDataProvider failed while totaling fee amount." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return val;
   }
   
   
   /**
    * Checks the current deal's fees against feeTypeIDs included in the input array and
    * sums those that have been used
    *
    * @param pFeeArray int[]
    * @return double
    */
   protected String exDwellingType() {
	   String description="";
	   try{
		   Collection properties = mDeal.getProperties();
		   Iterator it = properties.iterator();		   
		   
		   while(it.hasNext()) {
			   Property _prop = (Property) it.next();
			   int typeId = _prop.getDwellingTypeId();
			   description = pReadBXResources("DWELLINGTYPE", Integer.toString(typeId));
			   break;
			   }				   
		   }		   
	   catch(Exception e){
		   String msg = "DisclosureDataProvider failed while totaling fee amount." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return description;
   }
   
   /**
    * Checks the current deal's fees against feeTypeIDs included in the input array and
    * sums those that have been used
    *
    * @param pFeeArray int[]
    * @return double
    */
   protected double exEscrowTotalAmt(int[] pEscrowArray) {
	   double val = 0d;
	   try{
		   Collection escPaymentList = mDeal.getEscrowPayments();
		   Iterator it = escPaymentList.iterator();
		   double feeTotal = 0;

		   while(it.hasNext()) {
			   EscrowPayment escrowPayment = (EscrowPayment) it.next();
			   int paymentTypeID = escrowPayment.getEscrowTypeId();
			   for(int i=0; i<pEscrowArray.length; i++ ) {
				   if (paymentTypeID == pEscrowArray[i]) {
					   feeTotal += escrowPayment.getEscrowPaymentAmount();
					   break;
				   }
			   }
		   }
		   val = feeTotal;
	   }
	   catch(Exception e){
		   String msg = "DisclosureDataProvider failed while totaling fee amount." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
	   return val;
   }

   /**
     * formBorrowerName - Utility method that format the
    * 
     * @param brFirstName
     * @param brMiddleInitial
     * @param brLastName
     * @return
    */
    private String formBorrowerName(String brFirstName, String brMiddleInitial, String brLastName) {
        StringBuffer val = new StringBuffer();
        if (!pIsEmpty(brFirstName) && !pIsEmpty(brLastName)) {
            val.append(brFirstName);
            val.append(mFormat.space());
            if (!pIsEmpty(brMiddleInitial)) {
                val.append(brMiddleInitial);
                val.append(".");
                val.append(mFormat.space());
   	}
            val.append(brLastName);
   	}
        return val.toString();
   }
   
   /**
     * Returns primary borrower's first and last name
    * 
     * @return String
    */
    protected String exPrimaryBorrowerName() {
        String val = null;
		try {
            if (borrowersList != null) {
                Iterator borrowerIt = borrowersList.iterator();
                while (borrowerIt.hasNext()) {
                    Borrower borrower = (Borrower) borrowerIt.next();
                    if (borrower.getBorrowerTypeId() == Mc.BT_BORROWER
                        && borrower.getPrimaryBorrowerFlag().equalsIgnoreCase("Y")) {
                        val = formBorrowerName(borrower.getBorrowerFirstName(), borrower.getBorrowerMiddleInitial(),
                            borrower.getBorrowerLastName());
                        break;
                    }
			}
		} 
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting name of primary borrower."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
			mSrk.getSysLogger().warning(this.getClass(), msg);
		}
        return val;
	}
   
	/**
     * Returns current address of a primary borrower formatted as: addressLine1 + " " + addressLine2 + ", " +
     * city + ", " + province
	 * 
     * @return String
	 */
    protected String exPrimaryBorrowerAddress() {
        String val = null;
		try {
            Borrower b = new Borrower(mSrk, null);
            b = b.findByPrimaryBorrower(mDeal.getDealId(), mDeal.getCopyId());

            Collection ba = b.getBorrowerAddresses();
            BorrowerAddress oneAddr;
            Iterator it = ba.iterator();

            while (it.hasNext()) {
                oneAddr = (BorrowerAddress) it.next();
                if (oneAddr.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT) {
                    val = pFormatAddress(oneAddr.getAddr());
		} 
            }
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting address of primary borrower."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
			mSrk.getSysLogger().warning(this.getClass(), msg);
		}
        return val;
	}

   /**
     * Checks the Borrowers name included in the input array and sums those that have been used
	 *
     * @param borrowerTypeId int[]
     * @return String
	 */
    protected String exCoBorrowerNames() {
        StringBuffer fullName = null;
		try {
            String[] coBorrowerArray = exCoBorrowerNamesforLoop();
            if (coBorrowerArray != null && coBorrowerArray.length > 0) {
                for (int coBorrowerCount = 0; coBorrowerCount < coBorrowerArray.length; coBorrowerCount++) {
                    if (fullName != null) {
                        fullName.append(",");
                        fullName.append(mFormat.space());
                        fullName.append(coBorrowerArray[coBorrowerCount]);
                    } else {
                        fullName = new StringBuffer();
                        fullName.append(coBorrowerArray[coBorrowerCount]);
                    }
                }
		} 
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while totaling fee amount."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
			mSrk.getSysLogger().warning(this.getClass(), msg);
		}
        return fullName == null ? "" : fullName.toString();
	}

	/**
     * This method returns an array of all the CoBorrowers for the given deal in the format FirstName
     * MiddleName, Last Name. This method in turns calls the formBorrowerName tag for each of the co borrowers
	 * 
     * @return String[] - Array of all the co-borrower name
	 */
    protected String[] exCoBorrowerNamesforLoop() {
        ArrayList val = null;
        String[] borrowerNamesList = null;
        String borrowerFullName = null;
        try {
            if (borrowersList != null) {
                val = new ArrayList();
                Iterator borrowerIt = borrowersList.iterator();
		
                while (borrowerIt.hasNext()) {
                    Borrower borrower = (Borrower) borrowerIt.next();
                    if (borrower.getBorrowerTypeId() == Mc.BT_BORROWER
                        && !(borrower.getPrimaryBorrowerFlag().equalsIgnoreCase("Y"))) {
			
                        borrowerFullName = formBorrowerName(borrower.getBorrowerFirstName(), borrower
                            .getBorrowerMiddleInitial(), borrower.getBorrowerLastName());
                        if (!pIsEmpty(borrowerFullName)) {
                            val.add(borrowerFullName);
                        }
                    }
                }
                borrowerNamesList = new String[val.size()];
                for (int loopcntr = 0; loopcntr < val.size(); loopcntr++) {
                    borrowerNamesList[loopcntr] = (String) val.get(loopcntr);
                }
			
				} 
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting all the borrower names"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            mSrk.getSysLogger().warning(this.getClass(), msg);
				}
        return borrowerNamesList;
				}					   	

    /**
     * exAllBorrowerName - This method extractors the name of all the borrowers (primary and co-bororwers
     * included). The method in turn calls exPrimaryBorrowerName and exCoBorrowerNames to extract the primary
     * and co-borrowers name respectively
     * 
     * @return String - containing all the borrower names
     */
    protected String exAllBorrowerNames() {
        StringBuffer fullName = null;
        try {
            String primaryBorrowerName = exPrimaryBorrowerName();
            String coBorrwersName = exCoBorrowerNames();
           
            if (!pIsEmpty(primaryBorrowerName)) {
                fullName = new StringBuffer(primaryBorrowerName);
				}
            if (!pIsEmpty(coBorrwersName)) {
                if (fullName == null) {
                    fullName = new StringBuffer();
                } else {
                    fullName.append(",");
                    fullName.append(mFormat.space());
				}		
                fullName.append(coBorrwersName);
			}									
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting all the borrower names"
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            mSrk.getSysLogger().warning(this.getClass(), msg);
		}		
        return fullName == null ? "" : fullName.toString();
	}
	
   /**
    * Finds default fee amount for a fee represented by feeTypeId
    * 
    * @param feeTypeId
    * @return
    */
   protected double exDefaultFeeAmount(int feeTypeId) {
   	double val = 0d;
        try {
   		val = (new Fee(mSrk, null)).findFirstByType(feeTypeId).getDefaultFeeAmount();
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting default fee amount."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);    	
	   }
   	return val;
   }
   
   /**
     * Checks the current deal's fees against feeIDs included in pFeeArray and formas comma-delimitted string
     * of descriptions
    *
    * @param pFeeArray int[]
    * @return String
    */
   protected String exSelectedFeesDescription(int[] pFeeArray) {
	   String val = null;
	   try {
		   Collection dealFees = mDeal.getDealFees();
		   Iterator it = dealFees.iterator();

		   val = "";
		   while (it.hasNext()) {
			   DealFee oneFee = (DealFee) it.next();
			   int feeTypeID = oneFee.getFee().getFeeTypeId();
			   for (int i = 0; i < pFeeArray.length; i++) {
				   if (feeTypeID == pFeeArray[i]) {
					   String desc = pReadBXResources("FeeType", Integer.toString(oneFee.getFee().getFeeTypeId()));
					   val += desc + ", " + mFormat.space();
					   break;
				   }
			   }
		   }
            if (!pIsEmpty(val)) {
			   val = val.trim();
			   val = val.substring(0, val.length()-1);
		   }
	   }
	   catch( Exception e ){
		   String msg =  "DisclosureDataProvider failed during extraction of fee descriptions." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);
	   }
	   return val;
   }

   /**
    * Returns deal.netLoanAmount minus selected fees
    *
    * @return String
    */
   protected double exNetLoanAmount(double totalCheckedFees)  {
	   return mDeal.getNetLoanAmount() - totalCheckedFees;	   
   }

   /**
    * Returns deal.actualPaymentTerm
    *
    * @return String
    */
   protected int exActualPaymentTerm() {
        return (int) (mDeal.getActualPaymentTerm()); // TAR - 1612 : Removed the division by 12 , as months need to be displayed.
   }
  
   /**
    * Calculates and returns total cost of borrowing as a double
    * 
    * @param pPrimaryProperty
    * @return
    */
   protected double exTotalCostOfBorrowing(Property pPrimaryProperty) {
	   double effectiveRate = 0d;
	   
	   try {
		   double minRate = 0d;
		   double maxRate = 1d;
		   double tmpRate = 0.5d;		   
		   double tmpAmount = 0d;
		   
		   double payment = mDeal.getPandIPaymentAmountMonthly();
			//ticket #14476
		   //double netLoanAmount = mDeal.getNetLoanAmount();
           double netLoanAmount = mDeal.getTotalLoanAmount();
           //ticket #14476
		   double totalFees = pExTotalAllFeesAmt(pPrimaryProperty);

		   netLoanAmount -= totalFees;
		   
		   // To make an execution a bit faster, the method uses cashed mBalanceAtEndOfTerm if present
		   double balance;
		   if( !(mBalanceAtEndOfTerm < 0d))
			   balance = mBalanceAtEndOfTerm;
		   else
			   balance = pExBalanceRemainingAtEndOfTerm();
		   
		   if( !( mDeal.getActualPaymentTerm() == 0d 
				  || mDeal.getNetInterestRate() == 0d 
				  || netLoanAmount < 0d
				  || netLoanAmount <= payment)) {
			   
			   double term = mDeal.getActualPaymentTerm() * mPaymentsPerYear / 12d;
			   
			   while (tmpRate != 0d ) {
				   tmpAmount = ((1 - Math.pow(1+tmpRate, (-1)*term)) / tmpRate) * payment;				   
				   tmpAmount += (balance / Math.pow( 1+tmpRate, term));
				   
					if( tmpAmount > 99999999.99 || Math.abs( netLoanAmount - tmpAmount ) < 0.0000001)
					   break;
				   
				   if( netLoanAmount > tmpAmount)
					   maxRate = tmpRate;
				   else if( netLoanAmount < tmpAmount)
					   minRate = tmpRate;
				   
				   tmpRate = (maxRate + minRate) /2;
			   }
			   effectiveRate = (Math.pow(1+tmpRate, mPaymentsPerYear) - 1) * 100d;
			   if( effectiveRate > 999.999d )
				   effectiveRate = 0d;
		   }
		   effectiveRate = Math.round(effectiveRate * 100000d) / 100000d;
	   } 
	   catch (Exception e) {
		   String msg =  "DisclosureDataProvider failed while calculating a total cost of borrowing. " + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);
	   }
	   return effectiveRate;
   }
   /**
    * Calculates total cost of borrowing.
    * 
    * @return double
    */
   protected double exBalanceRemainingAtEndOfTerm() {
	   double balance;
	   //To make an execution a bit faster, the method uses cashed mBalanceAtEndOfTerm if present
	   if( !(mBalanceAtEndOfTerm < 0))
		   balance = mBalanceAtEndOfTerm;
	   else
		   balance = pExBalanceRemainingAtEndOfTerm();
	   
	   return balance;
   }
   
   /**
    * Actual total cost of borrowing calculation takes place here. It also sets a value of 
    * mBalanceAtEndOfTerm field.
    * 
    * @return double
    */
   private double pExBalanceRemainingAtEndOfTerm() {
	   double balance = 0d;
	   String errMsg = "DOCPREP: EXCEPTION: DisclosureDataProvider failed during calcualtion of balance remaining at the end of term. ";
	   
	   try {
		   int repaymentTypeId = mDeal.getRepaymentTypeId();
		   if( repaymentTypeId == 2 || repaymentTypeId == 3 ) {
			   balance = mDeal.getTotalLoanAmount();
		   }
		   else if( mDeal.getActualPaymentTerm() == mDeal.getAmortizationTerm()) {
			   balance = 0d;
		   }
		   else {
		   	int compoundId = -1;
			   try{
				   compoundId = (new MtgProd(mSrk, null, mDeal.getMtgProdId())).getInterestCompoundingId();
			   }
			   catch( Exception e) {
				   mSrk.getSysLogger().warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));			   	
			   }
			   if( compoundId == -1 )
			   	throw new Exception();
			   
			   double compound = Double.parseDouble(pReadBXResources("INTERESTCOMPOUND", Integer.toString(compoundId)));
			   
			   double totalPayments = mDeal.getActualPaymentTerm() / 12 * mPaymentsPerYear;
			   double interestFactor = Math.pow(1 + mDeal.getNetInterestRate()/(100*compound), compound/mPaymentsPerYear) - 1;

			   double interest, principal;
			   balance = mDeal.getTotalLoanAmount();
			   for( int i = 1; i<= totalPayments; i++) {
 				   interest = Math.round(balance * interestFactor * 100d) / 100d;
				   principal = Math.round((mDeal.getPandiPaymentAmount() - interest) * 100d) / 100d;
				   balance = Math.round((balance - principal) * 100d) / 100d;
			   }
			   if( balance < 0d) balance = 0d;
		   }
	   } 
	   catch (NumberFormatException e) {
		   mSrk.getSysLogger().warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));
	   }
	   catch (Exception e) {
		   mSrk.getSysLogger().warning(this.getClass(), errMsg + (e.getMessage() != null ? "\n" + e.getMessage() : ""));
	   }

	   mBalanceAtEndOfTerm = balance;
	   return balance;
   }

   /**
    * Returns parts of estimated closing date in "MM/dd/yyyy" format
    *
    * @param pField int
    * @return String
    */
   protected String parseEstimatedClosingDate(int pField) {
   	String val = null;
   	String date = formatDate(exEstimatedClosingDate());
   	switch( pField ) {
   		case Calendar.DAY_OF_MONTH:	{ val = date.substring(4,5); break; }
   		case Calendar.MONTH:				{ val = date.substring(1,2); break; }
   		case Calendar.YEAR:				{ val = date.substring(7,10); break; }
   	}
   	return val;
   }

   /**
    * Returns estimated closing date
    *
    * @return Date
    */
   protected Date exEstimatedClosingDate() {
   	Date val = null;
   	try {
   		val = mDeal.getEstimatedClosingDate();
   	}
   	catch (Exception e){
   		String msg = "DisclosureDataProvider failed while extracting estimated closing date." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);		   
   	}
   	return val;
   }

   /**
    * Returns a name of a broker's firm that was source of the deal
    *
    * @return String
    */
   protected String exSourceFirmName() {
	   String val = null;
	   try {
		   SourceFirmProfile sfp;
		   sfp = new SourceFirmProfile(mSrk, mDeal.getSourceFirmProfileId());
		   val = sfp.getSourceFirmName();
	   }
	   catch (Exception e) {
		   String msg = "DisclosureDataProvider failed while extracting source firm name." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);		   
	   }
	   return val;
   }

   /**
    * Persists a date in a format that will be known to the class that would transform this 
    * xml into a hashMap to be used by client class. That format is defined with static field
    * DisclosureDataProvider.DDP_DATE_FORMAT All the xml tags with "Date" in the name will be 
    * treated as a date type during creation of hashMap. 
    * 
    * @return date from UU fields as String
    */
   protected Date exBkrSignatureDate() {  
	   Date sDate = null;
	   try {
		String sDay = exQuestion("13UU_DAY");
		   if(!pIsEmpty(sDay)) {
			   String sMnt = exQuestion("13UU_MNT");
			   if(!pIsEmpty(sMnt)) {
				   String sYear = exQuestion("13UU_YEAR");
				   if( !pIsEmpty(sYear)) {
					   DateFormat df = new SimpleDateFormat( DDP_DATE_FORMAT );
					   
					   int iMonth = Integer.parseInt(sMnt);
					   sDate = df.parse(sDay + "-" + DateUtilCommon.months[iMonth-1] + "-" + sYear);	//bruce 		
				   }
			   }
		   }
	   }
	   catch (ParseException e) {
		   String msg = "DisclosureDataProvider failed while extracting broker signature date." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);
	   }
	   catch( Exception e ) {
		   String msg = "DisclosureDataProvider failed while extracting broker signature date." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);
	   }
	   
	   return sDate;
   }
  
  
   /**
    * Returns an addres of a lender's contact formated as:
    *    addressLine1 + " " + addressLine2 + ", " + city + ", " + province
    *
    * @return String
    */
   protected String exLenderAddress() {
	   String val = null;
	   LenderProfile lp;
	   try{
		   lp = new LenderProfile(mSrk, mDeal.getLenderProfileId());
		   val = pFormatAddress( lp.getContact().getAddr());
	   }
	   catch (Exception e){
		   String msg = "DisclosureDataProvider failed while extracting lender's address." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);		   
	   }
	   return val;
   }

   /**
    * Returns contact's first name, middle initial (if any) and last name
    *
    * @return String
    */
   protected String exSourceOfBusinessContactName() {
	   String val = null;
	   try{
		   SourceOfBusinessProfile sobp = mDeal.getSourceOfBusinessProfile();
		   if (sobp.getContact() != null) {
			   Contact c = sobp.getContact();
			   if (!pIsEmpty(c.getContactFirstName()) && !pIsEmpty(c.getContactLastName())) {
				   val = c.getContactFirstName() + mFormat.space();
				   if (!pIsEmpty(c.getContactMiddleInitial())) {
					   val += c.getContactMiddleInitial() + mFormat.space();
				   }
				   val += c.getContactLastName();
			   }
		   }
	   }
	   catch (Exception e){
		   String msg = "DisclosureDataProvider failed while extracting business contact name." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);		   
	   }
	   return val;
   }

   /**
	 * The method is use to return the list of Co-Borrowers for the deal
    *
	 * @return String[]
    */
	//	 TAR # 1612
	protected String[] exCoBorAddress() {
		ArrayList val = new ArrayList();
		String[] coBorrowerNames_list = null;
	   try {
			if (borrowersList != null) {
				Iterator borrowerIt = borrowersList.iterator();
				while (borrowerIt.hasNext()) {
					Borrower borrower = (Borrower) borrowerIt.next();
					if (!borrower.isPrimaryBorrower()
						&& borrower.getBorrowerTypeId() == Mc.BT_BORROWER) {
						Collection borrowerAddresses = borrower
							.getBorrowerAddresses();

						if (borrowerAddresses != null) {
							val.add(
								exCurrentAddress(borrowerAddresses));
						}
					}
				}
			}
			coBorrowerNames_list = new String[val.size()];
			for (int loopcntr = 0; loopcntr < val.size(); loopcntr++) {
				coBorrowerNames_list[loopcntr] = (String) val.get(loopcntr);
			}
	   }
	   catch (Exception e) {
			String msg = "Disc Data Provider failed while extracting Borrower name6"
				+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
	   }
		return coBorrowerNames_list;
   }

   /**
	 * The method is use to return the Current Address of the Borrower 
    *
	 * @param Collection - Collection of Borrower Address.
	 * @return String - Borrower Address
    */
	
	//	 TAR # 1612
	private String exCurrentAddress(Collection borrowerAddresses) {
		String formattedAddress = "";
	   try {
			if (borrowerAddresses != null) {
				Iterator borrowerAddressesIt = borrowerAddresses.iterator();
				while (borrowerAddressesIt.hasNext()) {
					BorrowerAddress badd = (BorrowerAddress)
						borrowerAddressesIt
						.next();
					// Checks If Borrower Address Type is 'Current'
					if (badd.getBorrowerAddressTypeId() ==
						Mc.BT_ADDR_TYPE_CURRENT) {
						Addr add = badd.getAddr();
						formattedAddress = pFormatAddress(add);
						break;
					}
				}
			   }
		   }
		catch (FinderException fe) {
			String msg =
				"Disclosure Data Provider failed while formatting current address"
				+ (fe.getMessage() != null ? "\n" + fe.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
	   }
		catch (RemoteException re) {
			String msg =
				"Disclosure Data Provider failed while formatting current address"
				+ (re.getMessage() != null ? "\n" + re.getMessage() : "");
			srk.getSysLogger().warning(this.getClass(), msg);
	   }
		return formattedAddress;
   }

    // Start : Updated method so that it returns the address in the format which
    // consistent with primary property address
    /**
     * pFormatAddress: This method formats a given <code>Addr</code> in the following format: UnitNumber + " - " +
     * StreetNumber + " " + StreetName + " " + StreetType + " " + StreetDirection + " " + City + ", " +
     * Province + " " + PostalFSA + " " + PostalLDU
     * 
     * @param addr - Addr object that needs to be formatted
     * 
     * @return String - Returns the formatted address in the format mentioned above
     */
    private String pFormatAddress(Addr addr) {
        String val = "";
        try {
            if (addr != null) {
                if (!pIsEmpty(addr.getUnitNumber())) {
                    val = addr.getUnitNumber() + " - ";
                }

                if (!pIsEmpty(addr.getStreetNumber())) {
                    val += addr.getStreetNumber() + mFormat.space();
                }

                if (!pIsEmpty(addr.getStreetName())) {
                    val += addr.getStreetName() + mFormat.space();
                }

                if (addr.getStreetTypeId() != 0) {
                    val += BXResources.getPickListDescription(mSrk.getExpressState().getDealInstitutionId(), "StreetType", addr.getStreetTypeId(), mLangId)
                        + mFormat.space();
                }

                if (addr.getStreetDirectionId() != 0) {
                    val += pReadBXResources("StreetDirection", Integer.toString(addr.getStreetDirectionId()));
                }

                if (!val.equals("")) {
                    val += ", ";
                }
                if (!pIsEmpty(addr.getCity())) {
                    val += addr.getCity() + ", ";
                }

                if (addr.getProvinceId() != 0) {
                    Province prov = new Province(mSrk, addr.getProvinceId());
                    if (!pIsEmpty(prov.getProvinceName())) {
                        val += prov.getProvinceName() + mFormat.space();
                    }
                }

                if (!pIsEmpty(addr.getPostalFSA()) & !pIsEmpty(addr.getPostalLDU())) {
                    val += addr.getPostalFSA() + mFormat.space() + addr.getPostalLDU();
                }
            }
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while formatting an address."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            mSrk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }

    // End : Updated method so that it returns the address in the format which
    // consistent with primary property address
   
   /**
    * If MIStatusId is APPROVAL RECEIVED or APPROVED, sums up insurer fee from DealFee table, otherwise
    * uses default insurer's fee amount from Fee table.
    *  
    * WARNING: if deal has associated insurance fees from different insurers (CMHC, GE, AIG UG), they will 
    * all be summed up.
    *  
    * @param feeTypeId
    * @return
    */
   protected double exMIFees( int mortgageInsurerId ) {
   	double fee = 0d;
   	int miStatusId = mDeal.getMIStatusId();
   	try
   	{
   		if( miStatusId == Mc.MI_STATUS_APPROVAL_RECEIVED || miStatusId == Mc.MI_STATUS_APPROVED ) {
   			fee = exSelectedFeesTotalAmt(pOTHER3_FEES);
   		}
   		else {
   			switch (mortgageInsurerId) {
   				case Mc.MI_INSURER_CMHC: {
   					fee = exDefaultFeeAmount(Mc.FEE_TYPE_CMHC_FEE);
   					break;
   				}
   				case Mc.MI_INSURER_GE: {
   					fee = exDefaultFeeAmount(Mc.FEE_TYPE_GE_CAPITAL_FEE);
   					break;
   				}
   				case Mc.MI_INSURER_AIG_UG: {
   					fee = exDefaultFeeAmount(Mc.FEE_TYPE_AIG_UG_FEE);
   					break;
   				}
   			}
   		}
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while formatting exMIFees." + ((e.getMessage() != null) ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return fee;
   }   

   /**
    * Returns date in the Express format for a lnaguage
    *
    * @param date Date
    * @return String
    */
   protected String formatDate(Date date) {
	   String ret = null;
	   try {
		   ret = (new SimpleDateFormat(DDP_DATE_FORMAT)).format(date);
	   }
	   catch (Exception e) {
		   String msg = "DisclosureDataProvider failed while formatting a date." + ((e.getMessage() != null) ? "\n" + e.getMessage() : "");
		   mSrk.getSysLogger().warning(this.getClass(), msg);
	   }
	   return ret;
   }

   /**
    * Returns true if string is null or ""
    *
    * @param str String
    * @return boolean
    */
   private boolean pIsEmpty(String str) {
	   return (str == null || str.trim().length() == 0);
   }
  
   /**
    * 
    * @param table
    * @param value
    * @return
    */
   private String pReadBXResources(String table, String value) {
	   String desc = null;
	   try {
          desc = BXResources.getPickListDescription(mSrk.getExpressState().getDealInstitutionId(), table, value, mLangId);
//          desc = BXResources.getPickListDescription(table, value, mLangId);
	   }
	   catch(Exception exc) {
          mSrk.getSysLogger().error("Missing resource bundle: table = " + table + "; value = " + value + "; lang = " + mLangId);
          desc = PicklistData.getDescription(table, value);
	   }
	   return desc;		
   }
  
   /**
    * Returns number of payments per year based on paymentFrequencyId and number of days in year used
    * (365.25 or 365)
    * 
    * @param pPaymentFrequency
    * @param daysInYear
    * @return
    */
   private double pCalcNumOfPaymentsPerYear(int pPaymentFrequency, double daysInYear) {
   	double paymentsPerYear;
   	switch (pPaymentFrequency) {
   		case Mc.PAY_FREQ_SEMIMONTHLY:				{ paymentsPerYear = 24d; break; }
   		case Mc.PAY_FREQ_BIWEEKLY:					{ paymentsPerYear = daysInYear/14d; break; }
   		case Mc.PAY_FREQ_WEEKLY:					{ paymentsPerYear = daysInYear/7d; break; }
   		case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:	{ paymentsPerYear = daysInYear/14d; break; }
   		case Mc.PAY_FREQ_ACCELERATED_WEEKLY:	{ paymentsPerYear = daysInYear/7d; break; }
   		default: paymentsPerYear = 12d;
   	}
   	return paymentsPerYear;
   }
   
   /**
    * Checks the current deal's fees against defined feeIDs (different for each province) and
    * sums those that have been used. This method is used in Total Cost Of Borrowing calculation
    *
    * @param  Property primaryProperty	- province of property defines the list of applicable fees
    * @return double
    * @throws ExtractException
    */
   private double pExTotalAllFeesAmt(Property primaryProperty ) throws ExtractException {
	   double val=0;
	   try{
		   double feeTotal = 0d;
		   
		   switch(primaryProperty.getProvinceId()) {
			   case Mc.PROVINCE_BRITISH_COLUMBIA: { 
				   int[] feeList = {Mc.FEE_TYPE_BUYDOWN_FEE, Mc.FEE_TYPE_BROKER_FEE}; 
				   feeTotal = exSelectedFeesTotalAmt( feeList );
				   break;
			   }
			   case Mc.PROVINCE_ALBERTA:
			   case Mc.PROVINCE_PRINCE_EDWARD_ISLAND: {
				   int[] feeList = { Mc.FEE_TYPE_BUYDOWN_FEE,  Mc.FEE_TYPE_BROKER_FEE,
						   				Mc.FEE_TYPE_CMHC_PREMIUM, Mc.FEE_TYPE_GE_CAPITAL_PREMIUM };
				   feeTotal = exSelectedFeesTotalAmt( feeList );
				   break;
			   }
			   default: {
				   feeTotal += exMIFees(mDeal.getMortgageInsurerId());
				   feeTotal += mDeal.getMIPremiumAmount();
				   feeTotal += mDeal.getMIPremiumPST();
				   
				   int[] feeList = { Mc.FEE_TYPE_APPRAISAL_FEE,      Mc.FEE_TYPE_APPLICATION_FEE,
						   				Mc.FEE_TYPE_BRIDGE_FEE,         Mc.FEE_TYPE_APP_FEE_NOT_CAPITALIZED,
						   				Mc.FEE_TYPE_BROKER_FEE,         Mc.FEE_TYPE_APPRAISAL_FEE_ADDITIONAL,
						   				Mc.FEE_TYPE_BUYDOWN_FEE,        Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE,
						   				Mc.FEE_TYPE_STANDBY_FEE,        Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE,
						   				Mc.FEE_TYPE_TITLE_INSURANCE_FEE
				   };
				   feeTotal += exSelectedFeesTotalAmt( feeList );
				   break;
			   }
		   }
		   
		   val = feeTotal;
	   }
	   catch(Exception e){
		   String msg = "DisclosureDataProvider failed while totaling fee amounts." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		   throw new ExtractException(msg);
	   }
	   return val;
   }
   
   /**
    * Returns First payment issue date as string
    *
    * @return String
    */
    protected Date exFirstPaymentDate() {
        Date val = null;
   	try {
            val = mDeal.getFirstPaymentDate();
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting payment frequency - Date  ."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }

   /**
    * Retrieves the Perdium Intrest amount on the deal as a string
    *
    * @return The completed total loan amount XML tag.
    */
    protected double exPerdiumAmount() {
        double val = 0d;
   	try {
            val = mDeal.getPerdiemInterestAmount();
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting Perdium Intrest amount."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
   
   /**
    * Returns lien position description of a mortgage
    *
    * @return String
    */
   protected String exIadNumOfDays() {
   	String val = null;
   	try {
   		val = String.valueOf(mDeal.getIADNumberOfDays());
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting lien position description." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
    // TAR # 1612 - Modified the return type from String to Date
   /**
     * Returns maturity date as Date
    *
     * @return Date
    */
    protected Date exMaturityDate() {
        Date val = null;
   	try {
            val = mDeal.getMaturityDate();
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting maturity date   ."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
   
   /**
	 * Returns Prepayment Amount PrePaymentOptionsId ! = 3 then Yes else No
	 * 
	 * @return String
	 */
	protected String exPrepaymentAmount() {
		String val = null;
		try {
			if (mDeal.getPrePaymentOptionsId() != 3)
				val = "Yes";
			else
				val = "No";
		} catch (Exception e) {
			String msg = "DisclosureDataProvider failed while extracting Prepayment Amount."
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			mSrk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}
   
   /**
	 * Returns a string containing "Yes" if Tax Payor Id is 0 else returns a
	 * string containing "No"
	 * 
	 * @return String : containing Yes or No
	 */
	protected String exPaymentIncludeTax() {
		String val = null;
		try {
			if (mDeal.getTaxPayorId() == 0) {
				val = "Yes";
			} else {
				val = "No";
			}
		} catch (Exception e) {
			String msg = "DisclosureDataProvider failed while extracting Payment Includes property Tax."
					+ (e.getMessage() != null ? "\n" + e.getMessage() : "");
			mSrk.getSysLogger().warning(this.getClass(), msg);
		}
		return val;
	}
   
   /**
	 * Retrieves the P AND I Payment amount on the deal as a string
	 * 
	 * @return The completed total loan amount XML tag.
	 */
    protected double exPandIPayment() {
        double val = 0d;
        try {

            val = mDeal.getPandiPaymentAmount();
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting P AND I Payment amount."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
            mSrk.getSysLogger().warning(this.getClass(), msg);
        }
        return val;
    }
   
   /**
    * Retrieves the InterimInterestAmount on the deal as a string
    *
    * @return The completed total loan amount XML tag.
    */
    protected double exInterimInterestAmount() {
        double val = 0d;
   	try {
            val = mDeal.getInterimInterestAmount();
        } catch (Exception e) {
            String msg = "DisclosureDataProvider failed while extracting InterimInterestAmount ."
                + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
	
   /**
    * Returns date as string
    *
    * @return String
    */
   protected String getDay(String date) {
   	String val = null;	   
   	try {
            if (date != null) {    
            	val = date.substring(4,date.indexOf(","));
   		}
   	}
   	catch (Exception e) {
   		String msg = "Date." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	return val;
   }
	  
   /**
    * Returns date as string
    *
    * @return String
    */
   protected String getMonth(String date) {
   	String val = null;

   	try {
   		if(date != null)
   			val = (date.toString()).substring(0,3);
   	}
   	catch (Exception e) {
   		String msg = "Month." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	System.out.println("Date in getDay line number 1783 Month"+ val);
   	return val;
   }
	  
   /**
    * Returns date as string
    *
    * @return String
    */
   protected String getYear(String date) {
   	String val = null;
   	try {
   		if(date != null)
   			val = (date.toString()).substring(7,date.length());
   	}
   	catch (Exception e) {
   		String msg = "Year." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	System.out.println("Date in getDay line number 1804 year"+ val);
   	return val;
   }
	  
   /**
    * Returns date as string
    *
    * @return String
    */
   protected String getYr(String date) {
   	String val = null;
   	try {
   		if(date != null) {
   			val = getYear(date).substring(2);
   			if(val.length()>2) val = val.substring(1);
   		}
   	}
   	catch (Exception e) {
   		String msg = "Yr." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);
   	}
   	System.out.println("Date in getDay line number 1826 YR"+ val);
   	return val;
   }
   
   	  //TAR : 1612 - Added method to get the Current Time in HH:MM:ss format
	  /**
	   * Returns Current time as string
	   *
	   * @return String
	   */
	  protected String getTime() {
	    String val = null;
	    Calendar cal = new GregorianCalendar(TimeZone.getDefault());
	    try {
	        val = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) +":"+ cal.get(Calendar.SECOND);
	    }
	    catch (Exception e) {
	      String msg = "Time  ." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
		  mSrk.getSysLogger().warning(this.getClass(), msg);
	    }
        return val;
    }
   
   /**
    * Returns LenderName
    * 
    * @return String
    */
   protected String exLenderFullName() {
   	String val = null;
   	try{
   		val = (new LenderProfile(mSrk, mDeal.getLenderProfileId())).getLenderName();
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting lender's Full name." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	
   	}
   	return val;
   } 
	   
	   
   /**
    * exPaymentFreqWeekly
    * Returns 1 if the payment frequency type is Weekly
    * else returns 0
    * 
    * @return String
    */
   protected String exPaymentFreqWeekly() {
   	String val = null;
   	try{
   		if(mDeal.getPaymentFrequencyId()== Mc.PAY_FREQ_WEEKLY || mDeal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_WEEKLY)
   			val = CHECKED_CHKBOX;
   		else
   			val = UNCHECKED_CHKBOX;
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting Weekly Payment Frequency." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	
   	}
   	return val;
   } 
	   
   /**
    * exPaymentFreqWeekly
    * Returns 1 if the payment frequency type is Bi-Weekly
    * else returns 0
    * 
    * @return String
    */
   protected String exPaymentFreqBiWeekly() {
   	String val = null;
   	try{
   		if(mDeal.getPaymentFrequencyId()== Mc.PAY_FREQ_BIWEEKLY || mDeal.getPaymentFrequencyId() == Mc.PAY_FREQ_ACCELERATED_BIWEEKLY)
   			val = CHECKED_CHKBOX;
   		else
   			val = UNCHECKED_CHKBOX;
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting Bi-Weekly Payment Frequency." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	
   	}
   	return val;
   } 
	   
	   
   /**
    * exPaymentFreqWeekly
    * Returns 1 if the payment frequency type is Monthly
    * else returns 0
    * 
    * @return String
    */
   protected String exPaymentFreqMonthly() {
   	String val = null;
   	try{
   		if(mDeal.getPaymentFrequencyId()== Mc.PAY_FREQ_MONTHLY)
   			val = CHECKED_CHKBOX;
   		else
   			val = UNCHECKED_CHKBOX;
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting Monthly Payment Frequency." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	
   	}
   	return val;
   }
   
   /**
    * exPaymentFreqText
    * Returns text for the payment frequency type
    * 
    * @return String
    */
   protected String exPaymentFreqText() {
   	String val = null;
   	try{
   		switch(mDeal.getPaymentFrequencyId()){
   			case Mc.PAY_FREQ_WEEKLY:
                return "Weekly"; // TAR : 1612 - Modified the text from Monthly to Weekly
   			case Mc.PAY_FREQ_BIWEEKLY:
   				return "Bi-Weekly";
   			case Mc.PAY_FREQ_MONTHLY:
   				return "Monthly";
   			case Mc.PAY_FREQ_SEMIMONTHLY:
   				return "Semi-Monthly";
            case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:
            	return "Accelerated Biweekly"; // TAR : 1612 - Added Check for Accelerated Biweekly
            case Mc.PAY_FREQ_ACCELERATED_WEEKLY:
            	return "Accelerated Weekly"; // TAR : 1612 - Added Check for Accelerated weekly
   		}
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting Payment Frequency Description." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	
   	}
   	return val;
   } 
	   
   /**
    * exPaymentFreqWeekly
    * Returns 1 if the payment frequency type is SemiMonthly
    * else returns 0
    * 
    * @return String
    */
   protected String exPaymentFreqSemiMonthly() {
   	String val = null;
   	try{
   		if(mDeal.getPaymentFrequencyId()== Mc.PAY_FREQ_SEMIMONTHLY)
   			val = CHECKED_CHKBOX;
   		else
   			val = UNCHECKED_CHKBOX;
   	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting SemiMonthly Payment Frequency." + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	
   	}
   	return val;
   } 
	   
   /**
    * exgetResponce
    * Returns 1 if the responce is Y
    * else returns 0
    * 
    * @return String
    */
   protected String exGetResponce(String responce) {
   	String val = null;
   	try{
   		if(responce == "Y")
    			val = CHECKED_CHKBOX;
    		else
    			val = UNCHECKED_CHKBOX;
    	}
   	catch (Exception e) {
   		String msg = "DisclosureDataProvider failed while extracting exGetResponce" + (e.getMessage() != null ? "\n" + e.getMessage() : "");
   		mSrk.getSysLogger().warning(this.getClass(), msg);    	
   	}
   	return val;
   } 
	   
	   
   /**
     * exTotalCostOfBorrowing: Protected method that returns the total cost of borrowing stored in the deal
     * 
     * @return double - Returns the total cost of borrowing for the given deal
     */
    protected double exTotalCostOfBorrowing() {
        double val = 0d;
        val = mDeal.getTotalCostOfBorrowing();
        return val;
    }	
 }
