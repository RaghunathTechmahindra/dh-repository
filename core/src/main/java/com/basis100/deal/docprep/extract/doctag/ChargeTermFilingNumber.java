package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.entity.*;
import MosSystem.Mc;

public class ChargeTermFilingNumber extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());
    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    Deal deal = (Deal)de;
    StringBuffer errBuf = new StringBuffer("");
    try
    {
      int mid = deal.getMIIndicatorId();
      int lender = deal.getLenderProfileId();
      Property prim = new Property(srk,null);
      prim.findByPrimaryProperty(deal
          .getDealId(), deal.getCopyId(), deal.getInstitutionProfileId());
      int prov = prim.getProvinceId();

      ChargeTerm ct = new ChargeTerm(srk);
      // Only BC, MANITOBA and ONTARIO
      switch(prov){
        case Dc.PROVINCE_BRITISH_COLUMBIA :{
           if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
                && deal.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"CMHC");
                val =  ct.getChargeTermFillingNumber();
              } catch (FinderException fexc1){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("CMHC");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc1.getMessage());
              }
           }
           else
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"Conventional");
                val =  ct.getChargeTermFillingNumber();
              } catch(FinderException fexc2){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("Conventional");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc2.getMessage());
              }
           }
           break;
        } // end case british columbia
        case Dc.PROVINCE_MANITOBA :{
           if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
                && deal.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"CMHC");
                val =  ct.getChargeTermFillingNumber();
              } catch (FinderException fexc3){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("CMHC");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc3.getMessage());
              }
           }
           else
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"Conventional");
                val =  ct.getChargeTermFillingNumber();
              } catch(FinderException fexc4){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("Conventional");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc4.getMessage());
              }
           }
           break;
        } // end case manitoba
        case Dc.PROVINCE_ONTARIO :{
           if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
                && deal.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"CMHC");
                val =  ct.getChargeTermFillingNumber();
              } catch (FinderException fexc5){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("CMHC");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc5.getMessage());
              }
           }
           else
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"Conventional");
                val =  ct.getChargeTermFillingNumber();
              } catch(FinderException fexc6){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("Conventional");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc6.getMessage());
              }
           }
           break;
        } // end case ontario
        case Dc.PROVINCE_ALBERTA :{
           if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
                && deal.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"CMHC");
                val =  ct.getChargeTermFillingNumber();
              } catch (FinderException fexc7){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("CMHC");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc7.getMessage());
              }
           }
           else
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"Conventional");
                val =  ct.getChargeTermFillingNumber();
              } catch(FinderException fexc8){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("Conventional");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc8.getMessage());
              }
           }
           break;
        } // end case alberta
        case Dc.PROVINCE_NEW_BRUNSWICK :{
           if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
                && deal.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"CMHC");
                val =  ct.getChargeTermFillingNumber();
              } catch (FinderException fexc8){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("CMHC");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc8.getMessage());
              }
           }
           else
           {
              try{
                ct.findFirstByLenderProvinceAndQualifier(lender,prov,"Conventional");
                val =  ct.getChargeTermFillingNumber();
              } catch(FinderException fexc9){
                errBuf = new StringBuffer("DOCPREP:");
                errBuf.append("DOCPREP ").append(this.getClass().getName()).append(" ");
                errBuf.append("LENDER =").append(lender).append(" ");
                errBuf.append("PROVINCE =").append(prov).append(" ");
                errBuf.append("Conventional");
                srk.getSysLogger().error(errBuf.toString());
                srk.getSysLogger().error(fexc9.getMessage());
              }
           }
           break;
        } // end case new brunswick

      } // end switch(prov)

      //val = "ALERT ISSUE UNDER CONTROL: Temporary Place Holder for Filing Number.";
      //fml.addValue(val, fml.TERM);
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }
    if(val != null){
      fml.addValue(val, fml.STRING);
    }
    return fml;
  }
}
