package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class VIPText extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = null;
    FMLQueryResult fml = new FMLQueryResult();
    
    Deal deal = (Deal)de;

    try
    {
 
      ConditionHandler ch = new ConditionHandler(srk);

      String prodName = "";
      int id = deal.getMtgProdId();
      MtgProd mp = new MtgProd(srk,null);
      mp = mp.findByPrimaryKey(new MtgProdPK(id));

      if(mp != null) prodName = mp.getMPShortName();

      if(prodName.equalsIgnoreCase("VIP"))
      {
         val = ch.getVariableVerbiage(deal,Dc.VARIABLE_PAYMENTS,lang);
      }

      fml.addValue(val, fml.STRING);
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
