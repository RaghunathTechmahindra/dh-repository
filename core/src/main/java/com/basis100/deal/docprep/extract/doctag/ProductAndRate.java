package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.deal.calc.*;
import MosSystem.*;
import com.basis100.deal.docprep.*;
import com.basis100.entity.*;

public class ProductAndRate extends DocumentTagExtractor
{
    protected SessionResourceKit m_srk;
    protected Deal m_deal;

    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        m_srk = srk;


        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();
        int clsid = de.getClassId();

        try
        {
            if(clsid == ClassId.DEAL)
            {
                m_deal = (Deal)de;

                MtgProd prod = m_deal.getMtgProd();

                if (prod == null)
                   return null;

                String desc = prod.getMtgProdName();
                Rates rates = getRates(desc);

                double r1 = rates.rate3Yr;
                double r2 = rates.rate5Yr;

                String rate1 = DocPrepLanguage.getInstance().getFormattedInterestRate(r1, lang);
                String rate2 = DocPrepLanguage.getInstance().getFormattedInterestRate(r2, lang);

                if (desc.equalsIgnoreCase("3 year FirstSolutions \"A\"") ||
                    desc.equalsIgnoreCase("5 year FirstSolutions \"A\""))
                {

                    fml.addString("�3 year FirstSolutions \"A\"" + format.space() + rate1 +
                                  "�5 year FirstSolutions \"A\"" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstSolutions \"B\"") ||
                         desc.equalsIgnoreCase("5 year FirstSolutions \"B\""))
                {
                    fml.addString("�3 year FirstSolutions \"B\"" + format.space() + rate1 +
                                  "�5 year FirstSolutions \"B\"" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstSolutions \"C\"") ||
                         desc.equalsIgnoreCase("5 year FirstSolutions \"C\""))
                {
                    fml.addString("�3 year FirstSolutions \"C\"" + format.space() + rate1 +
                                  "�5 year FirstSolutions \"C\"" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstSolutions \"A\" NF") ||
                         desc.equalsIgnoreCase("5 year FirstSolutions \"A\" NF"))
                {
                    fml.addString("�3 year FirstSolutions \"A\" NF" + format.space() + rate1 +
                                  "�5 year FirstSolutions \"A\" NF" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstSolutions \"B\" NF") ||
                         desc.equalsIgnoreCase("5 year FirstSolutions \"B\" NF"))
                {
                    fml.addString("�3 year FirstSolutions \"B\" NF" + format.space() + rate1 +
                                  "�5 year FirstSolutions \"B\" NF" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstSolutions \"C\" NF") ||
                         desc.equalsIgnoreCase("5 year FirstSolutions \"C\" NF"))
                {
                    fml.addString("�3 year FirstSolutions \"C\" NF" + format.space() + rate1 +
                                  "�5 year FirstSolutions \"C\" NF" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstEquity 90") ||
                         desc.equalsIgnoreCase("5 year FirstEquity 90"))
                {
                    fml.addString("�3 year FirstEquity 90" + format.space() + rate1 +
                                  "�5 year FirstEquity 90" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstEquity 90B") ||
                         desc.equalsIgnoreCase("5 year FirstEquity 90B"))
                {
                    fml.addString("�3 year FirstEquity 90B" + format.space() + rate1 +
                                  "�5 year FirstEquity 90B" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstEquity 100") ||
                         desc.equalsIgnoreCase("5 year FirstEquity 100"))
                {
                    fml.addString("�3 year FirstEquity 100" + format.space() + rate1 +
                                  "�5 year FirstEquity 100" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstPurchase 100") ||
                         desc.equalsIgnoreCase("5 year FirstPurchase 100"))
                {
                    fml.addString("�3 year FirstPurchase 100" + format.space() + rate1 +
                                  "�5 year FirstPurchase 100" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstEquity 90 NF") ||
                         desc.equalsIgnoreCase("5 year FirstEquity 90 NF"))
                {
                    fml.addString("�3 year FirstEquity 90 NF" + format.space() + rate1 +
                                  "�5 year FirstEquity 90 NF" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstEquity 90B NF") ||
                         desc.equalsIgnoreCase("5 year FirstEquity 90B NF"))
                {
                    fml.addString("�3 year FirstEquity 90B NF" + format.space() + rate1 +
                                  "�5 year FirstEquity 90B NF" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstEquity 100 NF") ||
                         desc.equalsIgnoreCase("5 year FirstEquity 100 NF"))
                {
                    fml.addString("�3 year FirstEquity 100 NF" + format.space() + rate1 +
                                  "�5 year FirstEquity 100 NF" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("3 year FirstPurchase 100 NF") ||
                         desc.equalsIgnoreCase("5 year FirstPurchase 100 NF"))
                {
                    fml.addString("�3 year FirstPurchase 100 NF" + format.space() + rate1 +
                                  "�5 year FirstPurchase 100 NF" + format.space() + rate2, null);
                }
                else if (desc.equalsIgnoreCase("5 year i100"))
                {
                    fml.addString("�5 year i100" + format.space() + rate2, null);
                }
            }
        }
        catch (Exception e)
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

            srk.getSysLogger().error(msg);

            if (e instanceof com.basis100.entity.FinderException)
               fml.addString("", null);
            else
               throw new ExtractException(msg);
        }

        return fml;
    }

    /**
     * Gets the 3 year and 5 year rates for the deal.  A test is done against
     * the text of the description to see if we have the 3 or 5 year rate.  The
     * corresponding rate is looked up and used for the other rate.
     *
     * @param desc the description of the used rate.
     * @return a populated <code>Rates</code> object, containing the 3 and 5
     * year rates.
     */
    protected Rates getRates(String desc)
    {
        Rates rates = new Rates();
        String temp = desc.toLowerCase();

        try
        {
            PricingProfile pp = new PricingProfile(m_srk);
            PricingRateInventory pri1 = new PricingRateInventory(m_srk, m_deal.getPricingProfileId());

            pp = pp.findByPricingRateInventory(pri1.getPricingRateInventoryID());

            int index = -1;

            try
            {
                index = Integer.parseInt(pp.getIndexName());
            }
            catch (NumberFormatException nfe)
            {
                return rates;
            }

            PricingRateInventory pri2 = new PricingRateInventory(m_srk);
            List list = pri2.findByPricingProfileId(index);

            if (list.isEmpty())
               return rates;
            else
            {
                Iterator i = list.iterator();

                while (i.hasNext())
                {
                    pri2 = (PricingRateInventory)i.next();

                    if (pri2.getIndexExpiryDate() == null)
                       break;

                    //  This is unexpected, so we need to ensure that no rates
                    //  are returned.  A valid rate will have no expiry date.
                    if (!i.hasNext())
                       return rates;
                }
            }

            if (temp.startsWith("3 year"))
            {
                rates.rate3Yr = pri1.getInternalRatePercentage();
                rates.rate5Yr = pri2.getInternalRatePercentage();
            }
            else if (temp.startsWith("5 year"))
            {
                rates.rate3Yr = pri2.getInternalRatePercentage();
                rates.rate5Yr = pri1.getInternalRatePercentage();
            }
            else    //  Don't know what this is, so return empty Rates object
                return rates;
        }
        catch (Exception e)
        {
            String msg = e.getMessage();

            if(msg == null) msg = "Unknown Error";

            msg = "DOCPREP ERROR: Error while trying to process rates: " +
                this.getClass().getName() + " msg: " + msg;

            m_srk.getSysLogger().error(msg);
        }

        return rates;
    }

    /**
     * Inner class used as a struct to hold the 3 and 5 year rates.
     */
    protected class Rates
    {
        double rate3Yr;
        double rate5Yr;
    }
}
