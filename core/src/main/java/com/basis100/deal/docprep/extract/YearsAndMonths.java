package com.basis100.deal.docprep.extract;

import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import java.util.*;
import com.basis100.deal.docprep.*;

public class YearsAndMonths
{
    private int termValue;

    public YearsAndMonths(int term)
    {
      termValue = term;
    }

    public YearsAndMonths(String term) throws NumberFormatException
    {
      termValue = Integer.parseInt(term);
    }

    public String getValue(int lang)
    {
      return DocPrepLanguage.getYearsAndMonths(termValue,lang);
    }
}
