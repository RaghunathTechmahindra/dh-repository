package com.basis100.deal.docprep.extract.data;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

/**
 * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
  - Many changes to become the Xprs standard
 *    by making this class inherit from DealExtractor/XSLExtractor
 *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
 *    is made available so that all templates: GENX type and the newer ones
 *    alike can share the same xml
 */

import java.util.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import org.w3c.dom.*;
import com.basis100.resources.*;
import com.basis100.xml.*;
import MosSystem.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.picklist.*;

public class XceedWireTransferII extends XceedExtractor
{

  protected double totalFees = 0d;
  protected double totalRefundableFees = 0d;
//  public XceedWireTransferII()
//  {
//  }

  //#DG238
  /**
   * For compatibility after making this class inherit from DealExtractor/XSLExtractor
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception {
    buildXMLDataDoc();
  }

    /**
     * The main exposed method that is called when a client desires the given document
     * type to be created.  Since the same logic is executed by each child class, the
     * logic was placed here.  This method calls buildXMLDataDoc(), which is
     * abstract and must be overloaded by the child class to handle how the document
     * is actually created.
     *
     */
  public String extractXMLData(DealEntity de,
                           int pLang,
                           DocumentFormat pFormat,
                           SessionResourceKit pSrk) throws Exception
  {
      deal = (Deal) de;
      lang = pLang;
      format = pFormat;
      srk = pSrk;

      buildXMLDataDoc();
      XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

      if (lang == Mc.LANGUAGE_PREFERENCE_FRENCH)
         xmlwr.setEncoding("iso-8859-1");

      return xmlwr.printString(doc);
  }

  protected void buildXMLDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();

    Element root = doc.createElement("Document");
    root.setAttribute("type", "1");
    root.setAttribute("lang", "1");
    doc.appendChild(root);
    //==========================================================================
    // Language stuff
    //==========================================================================
    addTo(root, getLanguageTag());

    addTo(root, extractAdjustedEstClosingDate());

    addTo(root, extractFees());
    addTo(root, extractNetWire());
    addTo(root, extractSolicitorSection() );
  }

  protected Node extractFees()
  {
    Node elemNode = null;
    Node itemNode = null;
    try
    {
      Collection fees = deal.getDealFees();

      if (fees.isEmpty())
      {
        return null;
      }

      elemNode = doc.createElement("Fees");

      Iterator it = fees.iterator();
      DealFee df;
      double amt;
      int status;
      int feeid;
      String type;

      while (it.hasNext())
      {
        df     = (DealFee) it.next();
        amt    = df.getFeeAmount();
        status = df.getFeeStatusId();
        feeid  = df.getFeeId();

        // change for refundable fee
        if(df.getFee() != null && df.getFee().getRefundable() != null && df.getFee().getRefundable().equalsIgnoreCase("y")){
          itemNode = doc.createElement("refundableFee");

          type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType",
                                                    df.getFee().getFeeTypeId(),
                                                    lang);

          addTo(itemNode, "Verb", type);
          addTo(itemNode, "Amount", formatCurrency(amt));

          addTo(elemNode, itemNode);
          totalRefundableFees += amt;
          continue;
        }



        if (!df.getFee().getPayableIndicator() && (amt != 0d) &&
              (status != Mc.FEE_STATUS_RECIEVED) &&
              (status != Mc.FEE_STATUS_WAIVED) &&
              (feeid != Mc.FEE_TYPE_CMHC_FEE) &&
              (feeid != Mc.FEE_TYPE_APPRAISAL_FEE_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_CMHC_FEE_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_CMHC_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_FEE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM) &&
              (feeid != Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE))
        {
          itemNode = doc.createElement("Fee");

          //type = PicklistData.getDescription("FeeType", df.getFee().getFeeTypeId());
          //ALERT:BX:ZR Release2.1.docprep
          type = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FeeType", df.getFee().getFeeTypeId(), lang);

          addTo(itemNode, "Verb", type);
          addTo(itemNode, "Amount", DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang));
          //addTo(itemNode, "Amount", formatCurrency(amt));

          addTo(elemNode, itemNode);

          totalFees += amt;
        }
      }
    }
    catch (Exception exc)
    {
    }

    return elemNode;
  }


  protected Node extractNetWire()
  {
    String val = null;

    try
    {

      double amt = deal.getTotalLoanAmount() + totalRefundableFees - totalFees -
                   deal.getInterimInterestAmount();
      // change for refundable fees
      //double amt = deal.getTotalLoanAmount() - totalFees -
      //             deal.getInterimInterestAmount();
      val = DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang);
      //val = formatCurrency(amt);
    }
    catch (Exception e)
    {
    }

    return getTag("NetWire", val);
  }

  protected Node extractAdjustedEstClosingDate()
  {
      String val = null;

      try
      {
        Date lDate = deal.getEstimatedClosingDate();
        if(lDate == null){
          val = "";
        } else {
          Calendar cal = Calendar.getInstance();
          cal.setTime(lDate);
          if(cal.DAY_OF_WEEK == Calendar.SATURDAY){
            cal.add(Calendar.DATE, -1);
          }else if (cal.DAY_OF_WEEK == Calendar.SUNDAY){
            cal.add(Calendar.DATE, -2);
          }else if(cal.DAY_OF_WEEK == Calendar.MONDAY){
            cal.add(Calendar.DATE, -3);
          }
          Date valDate = cal.getTime();
          try{
             val = DocPrepLanguage.getInstance().getFormattedDate(valDate, lang);
          }catch(Exception exc){
             val = "NULL";
          }
        }
      }
      catch (Exception e){
        return getTag("AdjustedEstClosingDate", "ERROR");
      }

      return getTag("AdjustedEstClosingDate", val);
  }


 /**
   * Builds a language-specific tag used by the XSL when formatting.
   *
   * @return The completed language tag, or null if the class-level
   *         variable <code>lang</code> is unrecognized.
   */
  protected Node getLanguageTag()
  {
      Element elem = null;

      switch (lang)
      {
          case Mc.LANGUAGE_PREFERENCE_ENGLISH:
               elem = doc.createElement("LanguageEnglish");
               break;

          case Mc.LANGUAGE_PREFERENCE_FRENCH:
               elem = doc.createElement("LanguageFrench");
               break;

          case Mc.LANGUAGE_PREFERENCE_SPANISH:
               elem = doc.createElement("LanguageSpanish");
               break;

          default:
               break;
      }

      if (elem != null)
         elem.appendChild(doc.createTextNode("Y"));

      return elem;
  }

}
