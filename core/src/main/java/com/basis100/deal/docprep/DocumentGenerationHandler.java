package com.basis100.deal.docprep;

/**
 * 24/Oct/2007 DVG #DG650 DEJ18897: Desjardin DocPrep Queue taking too long 
 * 24/Aug/2007 DVG #DG622 FXP18288: Firstline - Express to Target Download  
 * 24/May/2007 DVG #DG604 Billing 2007 - send 1 more field to datx faxing service
 * 04/May/2006 DVG #DG416 #1965  Xeed Solicitor Package Middle Name initial is not shown
  - before sending to ldd a final transformation to align upload to the schema.
 * 17/Feb/2006 DVG #DG394 #2728  3.1 Outbound Faxing - Mortgage Number Field
 * 17/Feb/2006 Sasa Added code for handling creation of Disclosure Document
 * 28/Nov/2005 DVG #DG366 added return email to faxing
 * 11/Nov/2005 DVG #DG360 #2408  Review and/or correct "System.getProperty" calls globally
 * 25/Aug/2005 DVG #DG302 #1863  GE Money - new mossys properties and change in DOCPREP
 * 23/Aug/2005 DVG #DG296 #1940  Blank commitment
  - incorrect suffix renaming of file sent to secure url
 * 02/Aug/2005 DVG #DG276 #1863  GE Money - new mossys properties and change in DOCPREP
 * 06/Jul/2005 DVG #DG248 #1740  FXLINKIII pending message support
 * 04/Jul/2005 DVG #DG242 link 2 to Funding (Profile) and
 *   new link to Closing services (FCT)
 * 02/Jun/2005 DVG #DG220 #1487  GE Money AU request - http post not timing out
 *              rearranged web service setups
 * 30/May/2005 DVG #DG216 #1462  GE-Production - AU request - retrieve specific copy id
 * 19/Apr/2005 DVG #DG190 GE Money AU DATX Equifax upload
* 14/Apr/2005 DVG #DG186 new fax interface with datx
* 03/Mar/2005 DVG #DG164 #1054  Prospera - CR001
    new web service created from their wsdl file and called from:
     DocumentGenerationHandler.java
 * 02/Mar/2005 DVG #DG154 datx french connection
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Collection;
import javax.mail.internet.MimeBodyPart;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;

import MosSystem.Mc;

import com.basis100.deal.docprep.ExpressDocprepHelper;
import com.basis100.deal.docprep.deliver.DocPrepDeliveryException;
import com.basis100.deal.docprep.deliver.MailDeliver;
import com.basis100.deal.docprep.extract.CCAPSBuilder;
import com.basis100.deal.docprep.extract.ExtractDom;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.Extractor;
import com.basis100.deal.docprep.extract.FMLExtractor;
import com.basis100.deal.docprep.extract.UploadExtractor;
import com.basis100.deal.docprep.gui.DPAlertManager;
import com.basis100.deal.docprep.rtf.RTFProducer;
import com.basis100.deal.docprep.xsl.XSLDataFactory;
import com.basis100.deal.docprep.xsl.XSLMerger;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.DocumentQueue;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.pk.DocumentQueuePK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.FinderException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.mail.MailException;
import com.basis100.mail.SimpleMailMessage;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.file.IOUtil;
import com.basis100.util.security.PGPCoder;
import com.filogix.common.document.Document;
import com.filogix.datx.messagebroker.exception.CommunicationException;
import com.filogix.datx.messagebroker.exception.InvalidValidationException;
import com.filogix.datx.messagebroker.fax.ContactInfo;
import com.filogix.datx.messagebroker.fax.DocumentInfo;
import com.filogix.datx.messagebroker.fax.FaxMessageSender02;
import com.filogix.datx.messagebroker.fax.FaxRequestInfo;
import com.filogix.datx.messagebroker.fax.FaxResponseInfo;
import com.filogix.datx.messagebroker.lddclosing.LDDClosingMessageSender;
import com.filogix.datx.messagebroker.lddclosing.LDDRequestBean;
import com.filogix.datx.messagebroker.messageproducer.MessageProducerMessageSender;
import com.filogix.datx.messagebroker.messageproducer.MessageProducerRequestBean;
import com.filogix.datx.messagebroker.servicing.mcap.ServicingMessageSender;
import com.filogix.datx.messagebroker.servicing.mcap.ServicingRequestBean;
import com.filogix.externallinks.gemoney.LoanSubmitterLocator;
import com.filogix.externallinks.gemoney.LoanSubmitterSoapStub;
import com.filogix.externallinks.prospera.MortgageIntegrationLocator;
import com.filogix.externallinks.prospera.MortgageIntegrationSoap;
import com.filogix.externallinks.services.datx.DatxResponseParser;
import com.filogix.util.Xc;

//#DG190 alternative way of connecting to ge, not working anyways and will not ocmpile w jdk1.3
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.HttpsURLConnection;

// PROGRAMMERS NOTE: this class has changed for multiple unresolved exception fix
 /**
  * The DocumentGenerationHandler handles the generation of a specific document     <br>
  * instance.    <br>
  *
 * @version 1.1 <br>
 *          Date: 08/04/2006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Changes made to bypass deal creation for Failure Mails <br> -
 *          modified method initialize to check for document type id and <br>
 *          by pass deal creation if type is Failure mail <br> - Added a new
 *          case for failure mail handling
  */
public class DocumentGenerationHandler extends Thread implements Xc
{
  public static boolean debug = false;
  private SysLogger logger;

  //private int handlerId;
  private DocumentProfile profile; //read only
  private DocumentQueue queueRequest;
  private Deal deal;
  //private String message = "Unknown Error";
  private SessionResourceKit srk;
  private String resourceRoot;
  private String resourcePath;
  private String docName;
  private String version;
  private int docType;
  private Vector listeners;
  private HandlerWorkerHolder workerHolder;
  private ActionEvent statusEvent;
  //#DG622 private StringWriter swr;
  private String archPath;
  private boolean xslRoute;
  private boolean mapRoute;

  //private static String PROTUS_STATUS = "Protus-Fax executed successfully"; // DatX response to successful faxing
  private static String CONVERSATION_KEY = "<CONVERSATIONKEY>";
  private static String CONVERSATION_KEY_END = "</CONVERSATIONKEY>";
  private static int[] notEncryptDocList = null;  //#DG302 list of doc not to be encrypted

  private static Collection<InstitutionProfile> institutions = null;
  static Map <Integer, SMIMEEnvelopedGenerator>  mimeGenerMap = null;
  static Map <Integer, MortgageIntegrationSoap> sunapticConMap = null;
  static Map <Integer, LoanSubmitterSoapStub> gemoneyConMap = null;
  //* #DG164 send to sunaptic - initialize
  static SMIMEEnvelopedGenerator mimeGener = null; 
  static MortgageIntegrationSoap sunapticCon = null;
  private static LoanSubmitterSoapStub gemoneyCon = null; //#DG190
  static final int MaxHttpTimeout = 60*1000;  //#DG220 max http timeout in msecs

  static {
    //#DG220 rearranged
    try {
        initUploadProspera();
        initUploadGEMoney();
        buildEncryptDocList();   //#DG302
    }
    catch (Exception ex) {
        final String errTrace = StringUtil.stack2string(ex);
        //throw new DocPrepException("Initializing UploadService " +StringUtil.stack2string(ex));
        new DPAlertManager().sendEmailAlert(errTrace, "Initializing DocumentGenerationHandler"); //#DG302
        System.err.println("Initializing DocumentGenerationHandler " +errTrace);
    }
  }

  /**
   * retrieved certificate(s), initialize smime generator and the web service
   * @throws Exception
   */
  static synchronized private void initUploadProspera() throws Exception {
	  
      if ( institutions == null ) {
          loadInstitutions();
      }
         
    SysLogger logger = ResourceManager.getSysLogger("WTstatic");
    PropertiesCache pc = PropertiesCache.getInstance();
    String sUploadUrl = pc.getUnchangedProperty(-1, COM_FILOGIX_DOCPREP_PROSPERA_URL, null);
    logger.trace("Prospera link in use is set to " + sUploadUrl);
   if (sUploadUrl == null || sunapticConMap != null)  // feature is off or already initialized
      return;
    Iterator instIter = institutions.iterator();
    while (instIter.hasNext()) {
        InstitutionProfile inst = (InstitutionProfile)instIter.next();
        Integer instId = new Integer(inst.getInstitutionProfileId());
    
    String sSMimekeyPath = pc.getProperty(instId, COM_FILOGIX_DOCPREP_SMIME_KEYS);
    logger.trace("UseSMime in use is set to " + sSMimekeyPath);
    if (sSMimekeyPath != null){
      logger.debug("retrieving smime certificates");
      try {
        Security.addProvider(new BouncyCastleProvider());

        // get the certificate from file
        CertificateFactory x509Factory = CertificateFactory.getInstance("X.509", "BC");
        //
        // create the generator for creating an smime/encrypted message
        //
         mimeGener = new SMIMEEnvelopedGenerator();
        File[] akeyFiles = new File(sSMimekeyPath).listFiles();
        for (int i = 0; i < akeyFiles.length; i++) {
          FileInputStream keyFileReader = new FileInputStream(akeyFiles[i]);
          while (keyFileReader.available() > 0) {
            Certificate oCert = x509Factory.generateCertificate(keyFileReader);
            mimeGener.addKeyTransRecipient( (X509Certificate) oCert);
          }
        }
        logger.trace("--------------------- End of loading SMime keys --------------------- ");
        mimeGenerMap.put(instId, mimeGener);
      }
      catch (Exception exc) {
        exc.printStackTrace();
        logger.error("error reading from smime key " + StringUtil.stack2string(exc));  //#DG186
      }
    }
    logger.debug("create the service");
    MortgageIntegrationLocator loMtgLoc = new MortgageIntegrationLocator();
    // if not set let it break!
    //          sUploadUrl = loMtgLoc.getMortgageIntegrationSoapAddress();
    sunapticConMap.put(instId, loMtgLoc.getMortgageIntegrationSoap(new URL(sUploadUrl)));    
    }
    logger.trace("end init Upload service");
  }
  //* #DG164 end

   //#DG190
  /**
   * initialize the web service
   * @throws Exception
   */
  static synchronized private void initUploadGEMoney() throws Exception {
	  
      if ( institutions == null ) {
          loadInstitutions();
      }  
    SysLogger logger = ResourceManager.getSysLogger("WTstatic");
    PropertiesCache pc = PropertiesCache.getInstance();
    
    Iterator instIter = institutions.iterator();
    while (instIter.hasNext()) {
    	InstitutionProfile inst = (InstitutionProfile)instIter.next();
    	Integer instId = new Integer(inst.getInstitutionProfileId());
    	   String sUploadUrl = pc.getUnchangedProperty(instId, COM_FILOGIX_GEMONEY_URL, null);
    logger.trace("GEMoney link in use is set to " + sUploadUrl);
    if (sUploadUrl == null || gemoneyConMap != null)  // feature is off or already initialized
      return;

    logger.debug("create the service");
    LoanSubmitterLocator servLoc = new LoanSubmitterLocator();
    // if not set let it break!
    //          sUploadUrl = loMtgLoc.getMortgageIntegrationSoapAddress();
    URL geUrl = new URL(sUploadUrl);
   
    gemoneyCon = (LoanSubmitterSoapStub)servLoc.getLoanSubmitterSoap(geUrl);
    gemoneyCon.setUsername(pc.getUnchangedProperty(-1, COM_FILOGIX_GEMONEY_URL_USERNAME, null));
    gemoneyCon.setPassword(pc.getUnchangedProperty(-1, COM_FILOGIX_GEMONEY_URL_PASSWORD, null));
    gemoneyCon.setTimeout(MaxHttpTimeout);  //#DG220
    gemoneyConMap.put(instId, gemoneyCon);
  // Install the custom authenticator
    Authenticator.setDefault(new Authenticator() {
        // This method is called when a password-protected URL is accessed
        protected PasswordAuthentication getPasswordAuthentication() {
          //if (geUrl.getHost() == getRequestingHost())
            return new PasswordAuthentication(gemoneyCon.getUsername(), gemoneyCon.getPassword().toCharArray());
          //else
            //return null;
        }
    }); 
    
    /*/ FOR TESTING ONLY:
    //Create a trust manager that does not validate certificate chains
    TrustManager[] trustAllCerts = new TrustManager[]{
        new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
            }
        }
    };
    // Install the all-trusting trust manager
    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    // FOR TESTING ONLY end */
    logger.trace("end init Upload service"); 
  }}
  //* #DG190 end

  //#DG302 build list of docs not to be encrypted with pgp
  static synchronized private void buildEncryptDocList() throws Exception {
	  
      if ( institutions == null ) {
          loadInstitutions();
      } 
     PropertiesCache pc = PropertiesCache.getInstance();
    String clist = pc.getProperty(-1, COM_FILOGIX_DOCPREP_ENCRYPT_NOT_DOCS);

     if (clist == null)
       return;
     StringTokenizer st = new StringTokenizer(clist, ",");
     int i = st.countTokens();
     if (i <= 0)
       return;
     notEncryptDocList = new int[i];
     while (st.hasMoreTokens())
       notEncryptDocList[--i] = Integer.parseInt(st.nextToken());
   } 
              
   //* #DG302 end
  
  private static void loadInstitutions()throws Exception { 
      
      SessionResourceKit srk = new SessionResourceKit("System");
      InstitutionProfile inst = new InstitutionProfile(srk);
      institutions = inst.getAllInstitutions();
      
  }

 /**
  * Construct a new fully initialized DocumentGenerationHandler instance.
  * @param pHolder HandlerWorkerHolder
  * @param pListener ActionListener
  * @throws DocPrepException
  */
 public DocumentGenerationHandler( HandlerWorkerHolder pHolder, ActionListener pListener)throws DocPrepException
  {

    //#DG622 identify thread name properly
    super("WT"+pHolder.getWorkerNumberId()+'.'+pHolder.getDocumentQueueId());		
	 
     if (institutions == null) {
         try {
         initUploadProspera();
         initUploadGEMoney();
         buildEncryptDocList();   //#DG302
     }
     catch (Exception ex) {
         //throw new DocPrepException("Initializing UploadService " +StringUtil.stack2string(ex));
         new DPAlertManager().sendEmailAlert(StringUtil.stack2string(ex), "Initializing DocumentGenerationHandler"); //#DG302
         System.err.println("Initializing DocumentGenerationHandler " +StringUtil.stack2string(ex));
     }         
   }

    listeners = new Vector();
    addActionListener(pListener);
    workerHolder = pHolder;
    resourceRoot = DocumentGenerationManager.getInstance().getResourcesRoot();
    initialize( pHolder.getDocumentQueueId(), pHolder.getInstitutionProfileId() );                               // docType will be defined inside initialize
    statusEvent = new ActionEvent(workerHolder,1,"Handler Initialized");
    notifyListeners(statusEvent);
    xslRoute = routeToXSL();
    mapRoute = routeToMapping();

    File archDir = DocumentGenerationManager.getInstance().getArchiveDir();
    if (archDir != null) {
      archPath = archDir.getAbsolutePath() + "/";
    }
    archDir = null;
  }

  private void addActionListener(ActionListener obj){
    listeners.addElement(obj);
  }

  private void notifyListeners(ActionEvent ev){
    Iterator it = listeners.iterator();
    while( it.hasNext() ){
      ((ActionListener) it.next()).actionPerformed(ev);
    }
  }

  private void printMsg(String msg, boolean error){
    if (error){
      System.err.println(msg);
      logger.error(msg);
    } else {
      System.out.println(msg);
      logger.trace(msg);
    }

  }
/**
  * Initialize this handler instance with the data related to the specific document
  * request<br/>
  * @param pQueueId int
  * @throws DocPrepException
  * @version 1.2  <br>
  * Date: 08/04/2006 <br>
  * Author: NBC/PP Implementation Team <br>
  * Change: modified method initialize to check for document type id and <br>
  *         by pass deal creation if type is Failure mail <br>
  */
 //private void initialize(DocumentQueue request) throws DocPrepException
  private void initialize(int pQueueId, int institutionProfileId) throws DocPrepException
  {
    DocumentQueue lRequest;

    try
    {
      this.srk = new SessionResourceKit(String.valueOf( workerHolder.getRequestorUserId() ));
      // added for ML
      
      /* Serghei:: October 07, 2008
       * Paradigm. We have to set the userProfileId in the srk.ExpressState object, because this one
       * is used in the GENXConOutstExtractor.extractCurentUser method. Now the SRK constructor, sets
       * it's own userProfileId but not the ExpressState's user id. 
       */
      srk.getExpressState().setUserIds(workerHolder.getRequestorUserId(), institutionProfileId);
      srk.getExpressState().setDealInstitutionId(institutionProfileId);
      
      logger = ResourceManager.getSysLogger(getName());
      logger.trace("Initializing DocumentGenerationHandler...");

      lRequest = new DocumentQueue(srk);
      lRequest = lRequest.findByPrimaryKey(new DocumentQueuePK(pQueueId));
      System.out.println("===========================> Request Id = " + pQueueId + " found.Thread="+getName());
      this.profile = lRequest.buildDocumentProfile();
      if(profile == null){
       workerHolder.setInException(true);
       workerHolder.appendErrorMessage("Document Profile could not be found.");
       throw new DocPrepException("DocumentProfile not found.");
      }

      System.out.println("DOCPREP: Document type = " + profile.getDocumentTypeId());
      //    ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
      // failure emails will not be associated with deal information
      if (profile.getDocumentTypeId() != Dc.DOCUMENT_GENERAL_TYPE_FAILURE_EMAIL){
      deal = determineAndCreateSourceDeal(lRequest,profile);
      

      if(Dc.DOCUMENT_GENERAL_TYPE_MEMO != lRequest.getDocumentTypeId() && deal == null)
      {
         workerHolder.setInException(true);
         workerHolder.appendErrorMessage("Failed to determine source Deal.");
         throw new DocPrepException("Failed to determine source Deal.");
      }
      }
      //    ***** Change by NBC Impl. Team - Version 1.1 - End *****//

      this.queueRequest = lRequest;
      this.resourcePath = getMetaDataPath(resourceRoot);
      logger.trace("Initializing DocumentGenerationHandler...done");
    }
    catch(Exception e)
    {
    	//#DG622 rewritten
      workerHolder.setInException(true);
      workerHolder.appendErrorMessage(StringUtil.stack2string(e));

      String msg = "DOCPREP: Document Request invalid: " + e;

      System.err.println(msg);
      logger.error(msg);
      throw new DocPrepException(msg);
    }
  }
  /**
   *  <B>MI:</B>
   *  <OL>
   *  <LI/> If request.scenarioNumber = R or is null find the RecommendedScenario
   *  <LI/> else find the non-transactional deal copy that corresponds with request.scenarioNumber
   *  </OL>
   *
   *  <B>All other Documents:</B>
   *  Find recommended scenario -
   *  if does not exist send mail message.
   *
   * @param request DocumentQueue
   * @param profile DocumentProfile
   * @return Deal
   * @throws Exception
   */
  private Deal determineAndCreateSourceDeal( DocumentQueue request, DocumentProfile profile)throws Exception
  {
      Deal theDeal = new Deal(srk, null);
      int id     = request.getDealId();
      int copyId = request.getDealCopyId();
      version = profile.getDocumentVersion();
      docName = profile.getDocumentFullName();
      docType = profile.getDocumentTypeId();

      String scenario = request.getScenarioNumber();
      boolean mitype = false;
      logger.trace("DOCPREP: Getting source deal for document named: " + docName);

      //#DG216 #1462  GE-Production - AU request - retrieve specific copy id
      if(docType == Dc.DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD)
        return theDeal.findByNonTransactional(id);

      // 1) process MI
      if(docType == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED || docType == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED)
      {
        mitype = true;
        try
        {
          if(scenario != null && !scenario.equals("R"))
          {
            int scenarioNumber = Integer.parseInt(scenario);
            theDeal = theDeal.findByNonTransactionalScenario(scenarioNumber, id);
            return theDeal;
          }
        }
        catch(FinderException fe)
        {
          String ermsg = "DOCPREP: failed to find Deal " + id + " corresponding to NonTransactional Scenario #" + scenario + " Searching for RecommendedScenario:"+fe;
          logger.error(ermsg );
          ;// fall through
        }
      }

      // 2) other
      try
      {
        theDeal = theDeal.findByNonTransactionalRecommendedScenario(id);
        return theDeal;
      }
      catch(FinderException fe)
      {
      	//#DG622 rewritten
        StringBuffer feMsg = new StringBuffer("DOCPREP: ");
          feMsg.append("Unable to create Document ( ").append(docName);
        if (mitype)
        	feMsg.append(" - MI communication type ");
          feMsg.append(" ) for dealId = " ).append(id);
        feMsg.append(" \nA non Transactional Recommended Scenario could not be found.");
          feMsg.append("(Provided copyId = ").append(copyId).append(" )");
        feMsg.append(" \nException msg = '").append(fe).append("'\n");
        final String body = feMsg.toString();
        try
        {
          SimpleMailMessage mail = new SimpleMailMessage();
          mail.setSubject("DocPrep:NonTransactionalRecommendedScenario not found"); //#DG622          
          mail.setText(body);
          mail.makeDefault("error");
          mail.send(null);
        }
        catch(MailException me)
        {
          logger.error("DOCPREP: Mail error - " + me);
          logger.error("DOCPREP: Unable to send Error Mail with message: " + body);
        }
      }
     return null;
  }


  //  -------------------------------------------------------------------
  // this is the entry point to request processing
  //  -------------------------------------------------------------------
 /**
  * @version 1.1  <br>
  * Date: 08/04/2006 <br>
  * Author: NBC/PP Implementation Team <br>
  * Change:  <br>
  * Added a new case for failure mail handling. <br>
  */
  public void run()
  {
    String msg;
    try
    {
      synchronized (queueRequest)                // while handler thread in this block other threads cannot update the Request object.
      {
        logger.info("handler thread started documentQueueId=" + workerHolder.getDocumentQueueId()); 
		
		String data = null;
        Map arData = null;
        int docType = queueRequest.getDocumentTypeId();

        logThisRequest();

        notifyListeners(new ActionEvent(workerHolder,1,"Handler Started"));
        srk.beginTransaction();

        long n = System.currentTimeMillis();

        // ------------------------ 1) Extract data
        switch ( docType ) {
          // ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
          // This document type requires no attachment, only text mail will be sent
          case Dc.DOCUMENT_GENERAL_TYPE_FAILURE_EMAIL: {
            deliverMemo();
            printMsg("DOCPREP: Failure Mail Sent", false);
            break;
          }
          // ***** Change by NBC Impl. Team - Version 1.1 - End *****//
          case Dc.DOCUMENT_GENERAL_TYPE_MEMO: {  //  This document type requires no attachment, only to send email text to given user (SCR #408)
            deliverMemo();
            printMsg("DOCPREP: memo sent", false);
            break;
          }
          case Dc.DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD:{                                      // BMO to CCAPS 1.2
            arData = extractCCAPS();
            printMsg("DOCPREP: completed CCAPS extraction for Deal: " +  this.getDealId(), false);
            break;
          }
          case Dc.DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD:  //#DG190
          case Dc.DOCUMENT_GENERAL_TYPE_PROSPERA_UPLOAD:{  //#DG164
            data = extractOneDoc();
            archiveData(data);    //#DG622
            printMsg("DOCPREP: completed UPLOAD extraction for Deal: " +  this.getDealId(), false);
            break;
          }
          default: {
            data = extractData();
            printMsg("DOCPREP: completed extraction for Deal: " +  this.getDealId(), false);
            // ------------------------ 2) Archive
            this.archiveData(data);                                                         // as opposed to DocumentGenerationManager.archive()
            printMsg("DOCPREP: completed archiving for Deal: " + this.getDealId(), false);
            break;
          }
        }

        // ------------------------ 3) Deliver
        switch (docType) {
          case Dc.DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD:{                                      // BMO to CCAPS 1.2
            if (arData != null){
              deliverData(arData);
            }
            else {
              printMsg("DOCPREP: data == null. CCAPS not delivered", true);
            }
            break;
          }
          case Dc.DOCUMENT_GENERAL_TYPE_FUNDING_UPLOAD:{   // link to MCAP
            if (data != null){
                deliverFundingUpload(data);
            }
            else {
              printMsg("DOCPREP: data == null. FUNDING not delivered", true);
            }
            break;
          }
          case Dc.DOCUMENT_GENERAL_TYPE_CLOSING_UPLOAD:{ //#DG242
            sendDatXUploadMessage(data, COM_FILOGIX_DATX_CLOSING_RECEIVINGPARTYID);
            break;
          }
          case Dc.DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD: { //#DG190
            data = finalTransform(data);	//#DG622
            sendGeMoneyUpload(data);
            break;
          }
          case Dc.DOCUMENT_GENERAL_TYPE_PROSPERA_UPLOAD:{  //#DG164
            data = finalTransform(data);	//#DG622
            sendSunapticUpload(data);
            break;
          }
          default: {
            if (data != null){
              deliverData(data);
            }
            else {
              printMsg("DOCPREP: data == null. Not delivered", true);
            }
            break;
          }
        }

        // ------------------------ 4) post process (MI)
        postProcess();

        srk.commitTransaction();

        logger.trace("DOCPREP: Committed and Sent.");
        logger.debug("DOCPREP: Generation time (millisec) : " + (System.currentTimeMillis() - n));
        notifyListeners(new ActionEvent(workerHolder,1,"SUCCESS-Finished"));
		logger.info("handler thread successfully finished. documentQueueId=" + workerHolder.getDocumentQueueId()); 
      }//end sync
    }
    // DatX error handling, Catherine, 11-Apr-05
    catch (DatXException dexc)
    {
		logger.error(StringUtil.stack2string(dexc)); 
    	//#DG622 rewritten
      workerHolder.setInException(true);
      workerHolder.appendErrorMessage("FXP error in DatX");
      workerHolder.appendErrorMessage(StringUtil.stack2string(dexc));
      notifyListeners(new ActionEvent(workerHolder,1,"Handler ERROR in DatX"));
      workerHolder.clearErrorMessage();
      workerHolder.setInException(false);
      try
      {
         srk.rollbackTransaction();
      }catch(Exception ex){;}
    }
	catch(Throwable t) //changed Exception to Throwable to catch Errors too, Mar 16 2011 
	{ 
	  String trace = StringUtil.stack2string(t); 
    	//#DG622 rewritten
      workerHolder.setInException(true);
      workerHolder.appendErrorMessage(trace);
      notifyListeners(new ActionEvent(workerHolder,1,"Handler ERROR"));

      msg = "DOCPREP: Failed to generate Document due to error: [ " + t + " ]";
      System.err.println(msg);
      logger.error(msg);
	  
	  logger.error(trace); 

      try
      {
         srk.rollbackTransaction();
      }catch(Exception ex){;}

    }
    finally
    {
      logger.trace("DOCPREP: Freeing SessionResourceKit Resources: " +  srk.getIdentity());
      this.queueRequest = null;
      this.profile = null;
      this.deal = null;
      srk.freeResources();
      msg = null;
      srk = null;
	  logger.info("handler thread finished. documentQueueId=" + workerHolder.getDocumentQueueId()); 
    }
  }

  //#DG164
  /**
   * transform and deliver data through upload web service
   * @param data String the data
   * @throws Exception
   */
  private String finalTransform(String data) throws Exception {
    logger.info("Process upload final xsl transform");

    //#DG622 already saved //archiveData(data, "A.xml");
    //printMsg("DOCPREP: completed archiving A for Deal: " + getDealId(), false);

    String styleSheetName = profile.getDocumentTemplatePath();
    return xmlTransform(data, styleSheetName);      //#DG416
  }

  //#DG416  make it available for general transforms
  /**
   * transform input xml string according to given xsl
   * @param data String
   * @param styleSheetName String
   * @return String
   * @throws Exception
   */
  private String xmlTransform(String data, String styleSheetName) throws Exception {
    logger.trace("Started creating document based on : " + styleSheetName);
    
    //fixed to handle classpath docgen/template 
    InputStream is = ExpressDocprepHelper.getTemplateAsStream(styleSheetName);

    StreamSource xmlSource = new StreamSource(new StringReader(data));
    StreamSource xslSource = new StreamSource(is);
    logger.trace("TRANSFORMATION STARTED");
    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer = tFactory.newTransformer(xslSource);
    StringWriter swr = new StringWriter();
    logger.trace("Ready to call transform method.");
    transformer.transform(xmlSource, new StreamResult(swr));
    logger.trace("TRANSFORMATION ENDED");
    data = swr.toString().trim();

    // save final
    archiveData(data, "B.xml");
    printMsg("DOCPREP: completed archiving B for Deal: " + this.getDealId(), false);
    return data;
  }

  private Map extractCCAPS() throws DocPrepException{
    CCAPSBuilder extractor = null;
    ExtractDom extractionInfo = new ExtractDom(new File(resourcePath));

    extractor = new CCAPSBuilder(srk, extractionInfo, this);
    if(extractor != null)
    {
      try
      {
        return extractor.buildApps();
      }
      catch(Exception exc)
      {
        exc.printStackTrace();
      	//#DG622 rewritten
        workerHolder.setInException(true);
        workerHolder.appendErrorMessage(StringUtil.stack2string(exc));
        throw new DocPrepException("Failed to extract data: " + exc);
      }
      finally {
        extractionInfo = null;
        extractor = null;
      }
    }
    else {
     throw new DocPrepException("Failed to extract data: couldn't create Extractor Instance");
    }
  }

  private void deliverData(Map aData) throws Exception {
    String oneDoc = null;

    File locationDir = null;
    String filename = null;

    // BMO to CCAPS 1.2 ----------
    if ( version != null && version.equalsIgnoreCase(VERSION_1_CCAPS))
    {
      locationDir = DocumentGenerationManager.getInstance().getOutboundDirectoryFor(VERSION_1_CCAPS);
    }
    // BMO to CCAPS 1.2 end ----------

    for (int i=0; i < aData.size(); i++){

      int iSeq = ((Integer)(aData.keySet().toArray()[i])).intValue();

      oneDoc = (String)(aData.values().toArray()[i]);

      String location = null;

      if(locationDir != null){
        location = locationDir.getAbsolutePath();
      }

      filename = buildFileName();

      if(( location != null ) && (location.endsWith("\\") || location.endsWith("/")))
       filename = location + filename;
      else if(location != null)
       filename = location + "/" + filename;

      filename = filename.replace(("?").charAt(0), ("" + iSeq).charAt(0));

      logger.trace("DOCPREP: Writing file : " + filename);
      System.out.println("Writing file: " + filename);

      OutputStreamWriter str = new OutputStreamWriter(new FileOutputStream(filename));
      str.write(oneDoc);
      str.flush();
      str.close();

    }
  }

  private void deliverData(String data) throws Exception {
    {
      if( mapRoute )
      {
        logger.info("Document generation handler - before routeToMapping()");       // logger.info("Document generation handler - branching point 1");
        writeToFile(data);
      }
      else if( xslRoute )
      {
        logger.info("Document generation handler - before processXSLRoute()");      // logger.info("Document generation handler - branching point 2");
        processXSLRoute(data);
      }
      else
      {
        logger.info("Document generation handler - before processOldRTFRoute()");   // logger.info("Document generation handler - branching point 3");
        processOldRTFRoute(data);
      }
    }
  }

  private void logThisRequest()
  {
    StringBuffer msgBuf = new StringBuffer("\n=========================================================\n");
    msgBuf.append("DOCPREP INITIALIZE REQUEST :").append(queueRequest.getDocumentQueueId() ).append("\n");
    msgBuf.append("Deal Id:").append("(").append(queueRequest.getDealId()).append(")\n");
    msgBuf.append("Copy Id:").append("(").append(queueRequest.getDealCopyId()).append(")\n");
    msgBuf.append("Requestor user id:").append("(").append(queueRequest.getRequestorUserId()).append(")\n");
    msgBuf.append("Document type:").append("(").append(queueRequest.getDocumentTypeId()).append(")\n");
    msgBuf.append("Document format:").append("(").append(queueRequest.getDocumentFormat()).append(")\n");
    msgBuf.append("Document version:").append("(").append(queueRequest.getDocumentVersion()).append(")\n");
    msgBuf.append("E mail address:").append("(").append(queueRequest.getEmailAddress()).append(")\n");
    msgBuf.append("Fax numbers:").append("(").append(queueRequest.getFaxNumbers()).append(")\n");
    msgBuf.append("Language:").append("(").append(queueRequest.getLanguagePreferenceId()).append(")\n");
    msgBuf.append("Lender profile:").append("(").append(queueRequest.getLenderProfileId()).append(")\n");
    //#DG622//msgBuf.append("Worker thread id:").append("(").append( workerHolder.getWorkerNumberId() ).append(")\n");
    msgBuf.append("Worker thread id:").append("(").append( getName() ).append(")\n");
    msgBuf.append("=========================================================");
    logger.debug( msgBuf.toString() );
  }

  //#DG164 allow a little more flexibility in file naming
  /**
   * convenience method for <i>archiveData</i>
   * @param data String data to be written in archive
   */
  private void archiveData(String data) {
    archiveData(data, "xml");
  }
  /**
   * archive this data using dealid,doc name and given suffix in archive folder
   * @param data String
   * @param sufix String
   */
  private void archiveData(String data, String sufix) {
    String msg = null;

    if (this.archPath == null) {
      archPath = null;
      msg = "WARNING: invalid archive path; documents will not be archived";
      printMsg(msg, false);
    } else {
      // Catherine 16-Dec-04: do not archive if 'processOldRTFRoute' because mailRTFDocument will archive it anyways

      if (mapRoute || xslRoute )
        {
            DocumentGenerationManager dgm = DocumentGenerationManager.getInstance();
            dgm.archiveData(data, this.deal.getDealId(), this.deal.getInstitutionProfileId(), docName, sufix);
        }
    }
  }

  private String extractData() throws DocPrepException {
    String str = null;

    /*#DG622 even data from extractor of type '*.upload' may be transformed.
     * so, if it is a class then use it 
    if( xslRoute )     {
      xslRoute = true;
      logger.debug("DOCPREP: Route to XSL - before extracting data.");
      str = extractForXSL();
      logger.debug("DOCPREP: Route to XSL - after extracting data.");
    }
    else {
      str = extractOneDoc();
    }*/
    try {
      Class.forName(profile.getDocumentSchemaPath());
      str = extractForXSL();
    }
    catch (ClassNotFoundException e) {
      str = extractOneDoc();
    }
    return str;
  }

  /**
   *  perform any logic that should take place after the document has been generated.
   */
  private void postProcess() throws Exception
  {
     logger.debug("DOCPREP: Start postProcess.");
     notifyListeners(new ActionEvent(workerHolder,1,"POST_PROCESS_STARTED"));
     if(docType == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED ||
         docType == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED)
     {
        MIProcessHandler mi;
        try
        {
          mi = MIProcessHandler.getInstance();
          mi.updateOnSubmission(getDeal(),srk);
        }
        catch(Exception e)
        {
          // bug fix: we have to retry and get new copy because user
          // on the screen probably screwed it up there.
          logger.debug("DOCPREP: PostProcess - retry - started.");
          try
          {
            int lId = getDeal().getDealId();
            Deal lDeal = new Deal(srk,null);
            lDeal = lDeal.findByNonTransactionalRecommendedScenario(lId);
            mi = MIProcessHandler.getInstance();
            mi.updateOnSubmission(lDeal, srk);
          }
          catch(FinderException fe)
          {
              String msg = "MI process failure - Reason: " + fe;		//#DG622
              workerHolder.setInException(true);
              workerHolder.appendErrorMessage("Error in post process.");
              workerHolder.appendErrorMessage(msg);
              notifyListeners(new ActionEvent(workerHolder,1,"Handler ERROR"));
              throw new Exception(msg);
          }
          logger.debug("DOCPREP: PostProcess - retry - ended.");
        }
     }

     logger.trace("DOCPREP: End postProcess.");
     queueRequest.ejbRemove();
     notifyListeners(new ActionEvent(workerHolder,1,"Handler POST_PROCESS_ENDED"));
  }

  public static String toUTF8String(String str)
  {
  try {
          byte [] bArr = str.getBytes("UTF-8");
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(bArr));
        BufferedReader reader = new BufferedReader(isr);
        str = reader.readLine();

        } catch (Exception ex)
        {
            ex.printStackTrace();
            return "toUTF8String: couldn't transform string";
        }
        return str;
  }

  private void processXSLRoute(String pData) throws DocPrepException, Exception
  {
    sendLdd(pData);		//#DG650 

    logger.info("Process xsl route - point 1");
    // =========================================================================
    // prepare xsl merger
    // =========================================================================
    // Rel 3.1 - Sasha, added constructor receives all the needed parameters
    //#DG650 XSLMerger xslMerger = new XSLMerger(this.deal.getDealId(), this.deal.getInstitutionProfileId(), this.srk.getSysLogger(), this.profile);
    XSLMerger xslMerger = new XSLMerger(deal, srk, this.profile);
    String format = profile.getDocumentFormat();

    int type = XSLMerger.getTypeFromFormat(format);
    Vector fVect = null;
    switch (type){
      case  XSLMerger.TYPE_PDF : {
        logger.info("Process xsl route - before create PDF documents.");
        fVect = xslMerger.createPDFDocumentsFromString(pData);
        break;
      }
      case XSLMerger.TYPE_CSV : { //2006-6-14 DSun: for CSV file
          logger.info("Process xsl route - before create CSV documents.");
          fVect = xslMerger.createCSVDocuments(pData);
        break;
      }
      case XSLMerger.TYPE_RTF : {
        logger.info("Process xsl route - before create RTF documents.");
        fVect = xslMerger.createRTFDocuments(pData);
        break;
      }
      default : {
        throw new DocPrepException("Could not identify type for xsl merging: " + type);
      }
    }
    if( fVect == null ){
      // TODO see whether we should throw exception at this point
      return;
    }
    // =========================================================================
    // now go and deliver documents
    // =========================================================================
    deliverDocuments(fVect, type);
  }

	/** #DG650 simplify
   *
	 * @param pData
   * @throws DocPrepException
   */
	private void sendLdd(String pData) throws DocPrepException {
		if( !isSendLDDClosing() )	{
			logger.info("sendToLDD = false. Do not send to LDD");
	return;    
  }
    try{
      logger.info("Trying to send to LDD");

      sendLDDClosingMessage(pData);
    }
    // Catherine, Dec 2004
    // do not interrupt the process of document generation if LDD has not been sent
    catch (DatXException dexc)
    {
    	//#DG622 rewritten
      workerHolder.setInException(true);
      workerHolder.appendErrorMessage("Error in LDD closing");
      workerHolder.appendErrorMessage(StringUtil.stack2string(dexc));
      notifyListeners(new ActionEvent(workerHolder,1,"Handler ERROR in LDD"));
      workerHolder.clearErrorMessage();
      workerHolder.setInException(false);
    }
    catch(Exception exc){
    	//#DG622 rewritten
      notifyListeners(new ActionEvent(workerHolder,1,"Handler ERROR in LDD"));
      throw new DocPrepException("DOCPREP : Failed to send LDD Closing message: " + exc);
    }
  }


  private void processOldRTFRoute(String pData) throws DocPrepException, Exception
  {
      RTFProducer rtfProducer =  new RTFProducer(this);
      rtfProducer.setDealNumber( this.deal.getDealId() );
      logger.debug("DOCPREP: RTFProducer created.");

      StringBuffer buf = rtfProducer.createDocument(pData);
      logger.debug("DOCPREP: RTF buffer created.");

      if(buf != null)
      {
        logger.debug("DOCPREP: Attempt to mail document...");
        mailRTFDocument(buf.toString());
      }
      else
      {
        workerHolder.setInException(true);
        workerHolder.appendErrorMessage("DOCPREP-No RTF buffer produced. [4]");
        notifyListeners(new ActionEvent(workerHolder,1,"Handler ERROR - no RTF buffer"));
        throw new DocPrepException("DOCPREP: No RTF buffer produced.");
      }
  }


  /**
   * Rel 3.1 - Sasha
   * 
   * Extracts data from DB using extractor class defined in the DocumentProfile.DocumentSchemaPath field.
   * Former extractForXSL is moved into XSLDataFactory class, to allow data extraction to be performed
   * independently of DocumentGenerationHandler. We are now able to call this method from any place in the 
   * code through XSLDataFactory.ExtractDataForXsl. Coupled with new PDFproducer class, we can now extract 
   * data, generate PDF and send it to the front end without ever instantiating DocumentGenerationHandler 
   * class.
   * 
   * Note: PDFProducer class is protected and can be called only through XSLMerger class.
   * 
   * @return
   * @throws DocPrepException
   */
  private String extractForXSL() throws DocPrepException {
      
    logger.debug("DOCPREP: java class - before extracting data.");
      XSLDataFactory xslDataFactory = new XSLDataFactory(this.getDeal(), profile, srk);
      String extrStr = null;
      try {
          extrStr = xslDataFactory.ExtractDataForXsl();
      }
	  catch( Exception exc ) {
	      exc.printStackTrace();
		  logger.error(StringUtil.stack2string(exc)); 
      	//#DG622 rewritten
          workerHolder.setInException(true);
        workerHolder.appendErrorMessage(StringUtil.stack2string(exc));
          
	      throw new DocPrepException( "DocumentGenerationHandler.ExtractForXSL: " + exc);		  
      }
    logger.debug("DOCPREP: java class - after extracting data.");
      return extrStr;
  }


  /**
  * Determines the generation approach and obtains ancillary files.  <br>
  * Initiates thread  <br>
  */
  private String extractOneDoc()throws DocPrepException
  {
    Extractor extractor = null;
    ExtractDom extractionInfo = new ExtractDom(new File(resourcePath));

    if( mapRoute )
      extractor = new UploadExtractor(srk, extractionInfo);
    else
      extractor = new FMLExtractor(srk, extractionInfo);

    if(extractor != null)
    {
      try
      {
         System.out.println("resource Path : " + resourcePath);
         //manager.debug("DOCPREP: Start data extraction for document.");
         String rv = extractor.extractXmlString(this);
         return rv;
      }
      catch(ExtractException exc)
      {
      	//#DG622 rewritten
        workerHolder.setInException(true);
        workerHolder.appendErrorMessage(StringUtil.stack2string(exc));
        throw new DocPrepException("Failed to extract data: " + exc);
      }
    }
    else
     throw new DocPrepException("Failed to extract data: couldn't create Extractor Instance");
  }

  /**
   *  Write the supplied String to a file based on specified naming conventions.
   *
   * @param xmlData String
   * @throws Exception
   */
  //#DG622 rewritten 
  private synchronized void writeToFile(String xmlData)throws Exception
  {
    String outboundTag = null;

    try
    {
      final boolean isFXLinkTyp = isFXLinkType( deal.getSystemTypeId());
			if( version != null && version.equals(VERSION_1_APPROVAL))
      {
      	outboundTag = isFXLinkTyp ? OUT_TAG_FILOGIXLINK: OUT_TAG_APPROVAL;
      }
      else if( version != null && version.equals(Dc.VERSION_1_DENIAL))
      {
      	outboundTag = isFXLinkTyp ? OUT_TAG_FILOGIXLINK: OUT_TAG_DENIAL;
        }
      else if( version != null && version.equals(VERSION_1_UPLOAD))
      {
      	outboundTag = OUT_TAG_UPLOAD;
      }
      else if( version != null && version.equalsIgnoreCase(VERSION_1_EZENET))
      {
      	outboundTag = OUT_TAG_EZENET;
      }
      else if( version != null && version.equalsIgnoreCase(VERSION_1_PENDING_MESSAGE))
      {
      	outboundTag = isFXLinkTyp ? OUT_TAG_FILOGIXLINK: OUT_TAG_PENDINGMESSAGE;
      }
      else if( version != null && version.equalsIgnoreCase(VERSION_1_TDCT_CAPLINK) )
      {
      	outboundTag = OUT_TAG_CAPLINK;
      }
      else if( profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED ||
              profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED )
      {
        int mitype = this.deal.getMortgageInsurerId();

        if(mitype == MI_INSURER_GE)
        {
        	outboundTag = OUT_TAG_GE;
        }
        else if(mitype == MI_INSURER_CMHC)
        {
        	outboundTag = OUT_TAG_CMHC;
        }
        else
        {
          logger.error("DocPrep: MIRequest or Cancellation: Invalid Insurer Type:" + mitype);
          //send a mail message
          return;
        }
      }

      writeData(outboundTag, buildFileName(), xmlData);	//#DG622
    }
    catch(Exception dpe)
    {
      StringBuffer msg = new StringBuffer("Handler.writeToFile: ");
      msg.append(StringUtil.stack2string(dpe));
      logger.error(msg.toString());
      throw dpe;	//#DG622
    }
    }

  //#DG622
	/**
	 * @param outboundTag the tag used to get the output path 
	 * @param fileName TODO
	 * @param xmlData the xml data to be written to a file
	 * @throws Exception
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void writeData(String outboundTag, String fileName, String xmlData) 
			throws Exception, FileNotFoundException, IOException {
    DocumentGenerationManager dgm = DocumentGenerationManager.getInstance();  
    
    File outFile = dgm.getOutboundDirectoryFor(outboundTag);
    outFile = new File(outFile, fileName);

		String fullFilename = outFile.getAbsolutePath();
		logger.trace("DOCPREP: Writing file : " + fullFilename);
		System.out.println("Writing file: " + fullFilename);

		OutputStreamWriter str = new OutputStreamWriter(new FileOutputStream(outFile));
		str.write(xmlData);
		str.close();
  }

  // ---------- CR, 19-Jul-04, PGP for DJ  ---------
  private Vector pgpEncryptDocuments(Vector pVect) throws DocPrepException {
    InputStream[] isKeys = workerHolder.getPGPKeys();
    Vector pgpVect = null;
    try {
      pgpVect = PGPCoder.PGPencrypt(pVect, isKeys, archPath);
    } catch (Error er){
      logger.error("Error in PGPCoder: " + er);		//#DG622
      // er.printStackTrace();
      throw new DocPrepException("Error in PGPencrypt");
    } catch (Exception e){
        logger.error(e);	//#DG622
    }
    return pgpVect;
  }

  /**
   * $$$ new functionality
   */
  private void deliverNormalEmail(Vector pVect, String to) throws DocPrepException, MailException
  {
    //DocumentGenerationManager dgm = DocumentGenerationManager.getInstance();
    //#DG360 SimpleMailMessage message = new SimpleMailMessage();
    String subject = (getEmailSubject() != null) ? getEmailSubject() : " ";

    //  Catherine, #1053 start --------------------------------
    String text;
    text = queueRequest.getEmailText();
    if ((text == null) || (text.trim().length() == 0)  ) {
      text = (getEmailFullName() != null) ? getEmailFullName() : " ";
    }
    //  Catherine, #1053 end   --------------------------------

    try
    {
      logger.debug("DOCPREP: Delivery address: " + to);

      SimpleMailMessage message = createMailMsg(to);    //#DG360

      if (text.indexOf('�') > -1){
        StringTokenizer st = new StringTokenizer(text, "�");
        while (st.hasMoreTokens())
        {
          message.appendTextLine(st.nextToken());
        }
      } else {
        message.appendText(text);
      }
      message.setSubject(subject);

      MailDeliver.send(message, pVect, docName);

      DocumentGenerationManager.debug("DOCPREP: DEBUG: Mailed Document for: " + this);

    }
    //  SCR #458
    //  Need to handle if the document doesn't reach the intended user.
    //  First we try to send the error message(s) to the
    //  current user and if that fails, then send to BXsupport
    //  (defined in application.xml)
    catch(Exception de)
    {
      //mailDeliveryError();
       workerHolder.setInException(true);
       workerHolder.appendErrorMessage("E mail could not be delivered.");
       throw new DocPrepException("E mail could not be delivered." + de);	//#DG622
    }
  }

  private void deliverDocumentsByFax(Vector pVect, int pType, String pFaxNumbers)throws Exception
  {
      try {
        logger.info("faxing process - entered.");
        //#DG186 FaxMessageSender requestSender = new FaxMessageSender();
        FaxMessageSender02 requestSender = new FaxMessageSender02();
        //#DG186 FaxRequestBean faxRequestBean = new FaxRequestBean();
        FaxRequestInfo  faxRequestBean = new FaxRequestInfo ();

        String lDatexUrl = PropertiesCache.getInstance().getUnchangedProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCPREP_FAX_DATX_URL, null);
        String lReceivingPartyId = PropertiesCache.getInstance().getUnchangedProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCPREP_FAX_RECEIVING_PARTY_ID, null);
        String lRequestingPartyId = PropertiesCache.getInstance().getUnchangedProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCPREP_FAX_REQUESTING_PARTY_ID, null);
        String lAccountIdentifier = PropertiesCache.getInstance().getUnchangedProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCPREP_FAX_ACCOUNT_IDENTIFIER, null);
        String lAccountPassword = PropertiesCache.getInstance().getUnchangedProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCPREP_FAX_ACCOUNT_PASSWORD, null);
        String billingAccount = PropertiesCache.getInstance().getUnchangedProperty(deal.getInstitutionProfileId(),COM_FILOGIX_DOCPREP_FAX_BILLING_ACCOUNT,null);	//#DG604

        if(lDatexUrl == null){throw new Exception("Faxing error - property  not set : datx url " );}
        if(lReceivingPartyId == null){throw new Exception("Faxing error - property  not set : Receiving PartyId " );}
        if(lRequestingPartyId == null){throw new Exception("Faxing error - property  not set : Requesting Party Id " );}
        if(lAccountIdentifier == null){throw new Exception("Faxing error - property  not set : Account Identifier " );}
        if(lAccountPassword == null){throw new Exception("Faxing error - property  not set : Account Password " );}
        if(billingAccount == null){throw new Exception("Faxing error - property  not set : billing Account " );}	//#DG604

        faxRequestBean.setReceivingPartyId(lReceivingPartyId);
        faxRequestBean.setRequestingPartyId(lRequestingPartyId);
        faxRequestBean.setAccountIdentifier(lAccountIdentifier);
        faxRequestBean.setAccountPassword(lAccountPassword);

        String to = pFaxNumbers ;
        //#DG186 ArrayList array = new ArrayList() ;
        ContactInfo contact;
        StringTokenizer tokenizer = new StringTokenizer(to,",") ;
        while(tokenizer.hasMoreTokens()){
          String oneTok = tokenizer.nextToken();
          String lFaxPrefix = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DOCPREP_FAX_PREFIX);
          if( lFaxPrefix != null && !lFaxPrefix.equals("")){
            oneTok = lFaxPrefix + oneTok;
          }
          /*#DG186
          FaxTo faxTo = new FaxTo();
          faxTo.setName("***");
          faxTo.setPhone(oneTok);
          this.printMsg("fax number: " + oneTok, false);
          array.add(faxTo);*/
          contact = new ContactInfo();
          contact.setName("***");
          contact.setFaxNo(oneTok);
          faxRequestBean.addRecipient(contact);
        }
        //#DG186 faxRequestBean.setFaxTo(array);
        contact = new ContactInfo();
        contact.setName("DocPrep");

        //#DG366 added return email to faxing
        BranchProfile b = new BranchProfile(srk, deal.getBranchProfileId());
        String email = b.getFaxNotificationEmail();
        if (b.getFaxNotificationPreferenceId() != 3 && email != null && !email.trim().equals("")) {
          contact.setEmailAddress(email);
          contact.setNotificationPreference(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "FAXNOTIFICATIONPREFERENCE", b.getFaxNotificationPreferenceId(), 0));
        }
        contact.setCompanyName(billingAccount);	//#DG604
        faxRequestBean.setSender(contact);
        faxRequestBean.setDealID(String.valueOf(deal.getDealId()));        //#DG394 added dealid to faxing

        byte[] faxDocument;

        Iterator it = pVect.iterator();
        while( it.hasNext() )
        {
          File lFile = (File) it.next();
          FileInputStream fis = new FileInputStream(lFile);
          faxDocument = new byte[(int)lFile.length()];
          fis.read(faxDocument);
          fis.close();
          //faxRequestBean.setFaxFile(faxDocument);  // for one document

          printMsg("faxing document " + lFile.getAbsolutePath(), false);

          DocumentInfo documentInfo;  //#DG186
          switch(pType){
            case XSLMerger.TYPE_PDF : {
              //#DG186 faxRequestBean.addFaxFile(faxDocument,IFax.TYPE_PDF,1);
              documentInfo = new DocumentInfo(DocumentInfo.PDF_DOC_TYPE, faxDocument);
              break;
            }
            case XSLMerger.TYPE_RTF : {
              //#DG186 faxRequestBean.addFaxFile(faxDocument,IFax.TYPE_RTF,1);
              documentInfo = new DocumentInfo(DocumentInfo.RTF_DOC_TYPE, faxDocument);
              break;
            }
            default :{
              //#DG186 faxRequestBean.addFaxFile(faxDocument,IFax.TYPE_TXT,1);
              documentInfo = new DocumentInfo(DocumentInfo.TXT_DOC_TYPE, faxDocument);
              break;
            }
          }
          faxRequestBean.addDocument(documentInfo);  //#DG186
        }

        // this is hard coded for now before we provide the real number of pages
        // faxRequestBean.setPageCount(1);
        String requestXML = requestSender.createRequest(faxRequestBean);
        //logger.trace("deliverDocumentsByFax.DatX request = " + requestXML);

        logger.trace("deliverDocumentsByFax: DatX URL = " + lDatexUrl);       //#DG394
        String respXML = requestSender.execute(requestXML, lDatexUrl);
        logger.trace("deliverDocumentsByFax.DatX response = " + respXML);

      // 2) check the response
      /*#DG186
      if ((respXML != null) || (respXML.length() > 0)) {
        String key = getDatXResponseKey(respXML);
        String status = null;
        if (respXML.indexOf(PROTUS_STATUS) != 0){
          status = "Faxing executed successfully. ";
        } else {
          status = "Failed";
        }
        StringBuffer msg = new StringBuffer(status).append("Conversation key = ").append(key);
        printMsg(msg.toString(), false);
      }*/

      FaxResponseInfo faxResponseInfo = requestSender.parseResponse(respXML);
      System.out.println("faxResponseInfo: " + faxResponseInfo);
      if (! faxResponseInfo.isSuccess()) {
//          throw new Exception("datx returned failure: "+faxResponseInfo.getDatxTxnID()+','
//                              +faxResponseInfo.getExtTxnID()+','+faxResponseInfo.getStatus()+','
//                              +faxResponseInfo.getMessage());
        throw new Exception("datx returned failure: "+faxResponseInfo);
      }

    } catch(Exception exc){
      logger.error("DOCPREP - fax exception. could not fax: " + StringUtil.stack2string(exc));  //#DG186
      throw new Exception("DOCPREP - fax exception. could not fax ");
    }
    logger.info("Faxing process - finished.");
  }

  private String getDatXResponseKey(String resp){
    String lKey = null;
    int ind = resp.indexOf(CONVERSATION_KEY);

    if ( ind != 0){
      int ind2 = resp.indexOf(CONVERSATION_KEY_END);
      lKey = resp.substring( ind + CONVERSATION_KEY.length(), ind2);
    }
    return lKey;
  }

  /**
   * $$$ new functionality
   */
  private void deliverDocuments(Vector pVect, int pType) throws Exception
  {
    logger.info("Deliver documents entered.");
    // step 1. deliver documents by fax
    String lFaxingEnabled = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DOCPREP_ENABLE_FAXING,"N");
    if(lFaxingEnabled.equalsIgnoreCase("y")){
      if( isFaxingRequired() ){
        logger.info("Deliver documents part 1: deliver documents fy fax " );
        deliverDocumentsByFax(pVect, pType, getFaxNumbers() );
      }
    }

    // ---------- CR, 19-Jul-04, PGP for DJ  ---------
    PropertiesCache pc = PropertiesCache.getInstance();
    String sGMB =  pc.getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCPREP_FORCE_EMAIL_TO_ADDRESS);
    String sUsePGP = pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DOCPREP_PGP_IN_USE, "N");
    String encryptEmail = pc.getProperty(deal.getInstitutionProfileId(), COM_BASIS100_ENCRYPT_EMAIL,"N");
    String sRecip = getEmailAddress(); // recipient, other than gmb (from the documentqueue)
    String sURL = pc.getProperty(deal.getInstitutionProfileId(), COM_BASIS100_ENCRYPT_EMAIL_URL);

    // these booleans are here to make the logic below easier to read
    boolean isGMB = (sGMB != null);                  // GMB = general mail box
    boolean isPGP = sUsePGP.equalsIgnoreCase("y");
    boolean isRcp = ( sRecip != null);
    boolean isEncryptEmail = encryptEmail.equalsIgnoreCase("y");   //#DG276
    boolean isSecurURL = isEncryptEmail && (sURL != null);

    Vector pgpDocs = null;
    if ( isGMB ){
      // if we have to force a copy of each email to the gmb
      if ( isPGP ){
        sendPgpDocs(pVect, sGMB, pgpDocs);
      }
      else { // not isPGP
        // no PGP encryption for email going to gmb
        if ( isSecurURL ){
          logger.info("Deliver documents part 3. Deliver secure URL to gmb: " + sGMB);
          //#DG296 deliverSecureURL(pVect, docType, sGMB);
          deliverSecureURL(pVect, pType, sGMB);
        }
        else { // not isURL
          logger.info("Deliver documents part 4. Deliver normal email to gmb: " + sGMB);
          deliverNormalEmail(pVect, sGMB);
        }
      }
    }
    if ( isRcp ) {
      if ( isSecurURL ){
        logger.info("Deliver documents part 5. Deliver secure URL to recipient " + sRecip);
        //#DG296 deliverSecureURL(pVect, docType, sRecip);
        deliverSecureURL(pVect, pType, sRecip);
      }
      else if (isEncryptEmail && isPGP && encryptDoc()) { //#DG276 #DG302
        sendPgpDocs(pVect, sRecip, pgpDocs);
      }
      else {   // not isURL
        logger.info("Deliver documents part 6. Deliver normal email to recipient " + sRecip);
        deliverNormalEmail(pVect, sRecip);
      }
    }
   // ---------- CR, 19-Jul-04, PGP for DJ end ---------

    logger.info("Deliver documents finished.");
  }

  //#DG276 now regular email users can receive pgp'ed docs too
  /**
   * Send pgp'ed <code>pgpDocs</code> to email destination <code>dest</code>
   * @param pVect Vector input docs
   * @param dest String
   * @param pgpDocs Vector
   * @throws MailException
   * @throws DocPrepException
   */
  private void sendPgpDocs(Vector pVect, String dest, Vector pgpDocs) throws
      MailException, DocPrepException {
    logger.info("Deliver documents part 2: PGP encryption to gmb: " + dest);
    if( pgpDocs == null) {
      pgpDocs = pgpEncryptDocuments(pVect);
      logger.info("PGP encryption done");
    }
    if (pgpDocs != null){
      deliverNormalEmail(pgpDocs, dest);
    } else {
      throw new DocPrepException("Deliver documents part 2: PGP documents vector is null");
    }
  }

  //#DG302 is the current document to be encrypted?
  private boolean encryptDoc() {
    logger.debug("encryptDoc A");
    if (notEncryptDocList == null)
      return true;
    for (int i = 0; i < notEncryptDocList.length; i++) {
      if(docType == notEncryptDocList[i])
        return false;
    }
    return true;
  }


  private void mailRTFDocument(String rtfData)throws Exception
  {
    File lOutputFile;
    FileWriter fwr;
    Vector v;

    try {

      String rtfExt = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
          COM_BASIS100_ALTERNATE_RTF_EXTENSION, "rtf");

      if (this.archPath == null) {
        lOutputFile = new File( generateRTFFileName() + "." + rtfExt);
        logger.trace("Invalid archive folder, using DocPrep current folder to create temporary file(s)..." );
      }
      else {
        lOutputFile = new File(archPath + generateRTFFileName() + "." + rtfExt);
      }

      fwr = new FileWriter(lOutputFile);
      fwr.write(rtfData);
      fwr.flush();
      fwr.close();

      v = new Vector();
      v.addElement(lOutputFile);

      String sTo = generateEmailAddresses(); // CR, PGP
      deliverNormalEmail(v, sTo);
    }
    finally {
      lOutputFile = null;
      fwr = null;
      v = null;
    }

  }

/*
  private void mailDocument(String ext, File file)throws Exception
  {
    mailDocument(null, ext, file);
  }
*/
  private String generateEncryptedURL(String pFileName){
    String rv = PropertiesCache.getInstance().getUnchangedProperty(deal.getInstitutionProfileId(), COM_BASIS100_ENCRYPT_EMAIL_URL, null);
    rv = rv + pFileName;
    return rv;
  }

  /**
   * Helper method that creates file name for old way of production of RTF documents.
   */
  private String generateRTFFileName(){
    String lName = profile.getDocumentFullName();
    lName = lName + "_";
    Date cd = new Date();
    String datestr = TypeConverter.isoDateFrom(cd);
    lName += datestr;
    lName += "_";
    lName += deal.getDealId();

    return lName;
  }

  /**
   * Helper method which generates encrypted file name.
   */
  private String generateEncryptedFileName(int pType){
    String rv = "";
    Date lDate = new Date();
    long lTime = lDate.getTime();

    Random rndm = new Random(lTime);
    for(int i=1; i<=5; i++){
      rv = rv + rndm.nextInt(99) + "_";
    }
    logger.info("generateEncryptedFileName(pType)="+pType);//#DG296 debug

    switch(pType){
      case XSLMerger.TYPE_PDF : {
        rv = rv + "_" + lTime + ".pdf" ;
        break;
      }
      case XSLMerger.TYPE_RTF : {
        rv = rv + "_" + lTime + ".rtf" ;
        break;
      }
      default : {
        rv = rv + "_" + lTime + ".txt" ;
        break;
      }
    }

    return rv;
  }

  private void writeFileToEncryptedFolder(File pFile, String pFileName) throws Exception
  {
    DocumentGenerationManager manager = DocumentGenerationManager.getInstance();
    File locationDir = null;
    locationDir = manager.getOutboundDirectoryFor("encryptedfilesfolder");

    String location = null;

    if(locationDir != null)
      location = locationDir.getAbsolutePath();

    if(( location != null ) && (location.endsWith("\\") || location.endsWith("/")))
     pFileName = location + pFileName;
    else if(location != null)
     pFileName = location + "/" + pFileName;

    File lOutFile = new File(pFileName);
    IOUtil.moveFile(pFile,lOutFile, false );
  }

  private String generateEmailAddresses(){
    StringBuffer rb = new StringBuffer("");

    if( getEmailAddress() != null ){
      rb.append(getEmailAddress());
    }
    final String forceEmail = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),COM_BASIS100_DOCPREP_FORCE_EMAIL_TO_ADDRESS);
    if( rb.toString().equals("") ){
      if(forceEmail != null){
        rb.append(forceEmail);
      }
    }else{
      if(forceEmail != null){
        rb.append(",");
        rb.append(forceEmail);
      }
    }
    return rb.toString();
  }

  /**
   *  $$$ new functionality
   */
  private void deliverSecureURL( Vector pVect, int pType, String to) throws Exception
  {
    logger.info("Deliver secure URL - started.");
    DocumentGenerationManager dgm = DocumentGenerationManager.getInstance();
    //#DG360 SimpleMailMessage message = new SimpleMailMessage();

    // store file to folder which has to accept files for encryption
    // these files will be picked up by frost or something
    StringBuffer eMailBodyBuf = new StringBuffer("Please, follow these links :  \n");
    Iterator it = pVect.iterator();
    logger.info("Deliver secure URL - writing files to encrypted folder.");
    while( it.hasNext() ){
      String lFileName = generateEncryptedFileName(pType);
      eMailBodyBuf.append( generateEncryptedURL( lFileName ) ).append("\n");
      File lFile = (File) it.next();
      logger.info("Deliver secure URL - writing a file to encrypted folder:"+lFileName+":"+profile.getDocumentFormat()); //#DG296 debug
      writeFileToEncryptedFolder( lFile, lFileName );
    }
    String subject = (getEmailSubject() != null) ? getEmailSubject() : " ";
    // String to = generateEmailAddresses(); // CR, PGP
    try
    {
      logger.debug("DOCPREP: ENCRYPTED - Delivery address: " + to);

      SimpleMailMessage message = createMailMsg(to);    //#DG360

      message.appendText( eMailBodyBuf.toString() );
      message.setSubject(subject);

      MailDeliver.send(message);

      dgm.debug("DOCPREP: DEBUG: Mailed message for: " + this);

    }
    catch(Exception de)
    {
      final String msg = getClass().getName() + ":\n" + de;
      //#DG360 System.out.println(" This exception must be taken care of: " + de.getMessage());
      System.err.println(msg);
      logger.error(msg);
        }
        }

  /**
   * Helper method. returns true if e-mailing has been required.
   */
  private boolean isEMailRequired(){
    String lMailAddr = getEmailAddress();
    String lForceEMailTo = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_DOCPREP_FORCE_EMAIL_TO_ADDRESS);
    if( lMailAddr != null ){return true;}
    if( lForceEMailTo != null ){return true;}
    return false;
  }


  /**
   * Helper method. returns true if the faxing has been required.
   */
  private boolean isFaxingRequired(){
    String lFNum = getFaxNumbers();
    if( lFNum != null  &&  lFNum.trim().length() > 0 ){
      return true;
    }
    return false;
  }

  private String extractEcniLenderId(){
    String retVal = "UNKNOWN";
    try{
      InstitutionProfile ip = new InstitutionProfile(this.srk);
      ip = ip.findByFirst();
      retVal = ip.getECNILenderId();

      if(retVal != null){
        return retVal.trim();
      }else{
        return "NULL";
      }
    }catch(Exception exc){
      return "ERROR";
    }
  }

  private String extractLenderName()
  {
    String retVal = "UNKNOWN";
    try{
      InstitutionProfile ip = new InstitutionProfile(this.srk);
      ip = ip.findByFirst();
      retVal = ip.getMortyLenderId();

      if(retVal != null){
        return retVal.trim().substring(0,3);
      }else{
        return "NULL";
      }
    }catch(Exception exc){
      return "ERROR";
    }
  }

  // BMO to CCAPS
  private String buildFileName(int appNum)
  {
      String name = "DocGenerated_file.xml";                                   // default file name
      if( version != null &&
         (docType == Dc.DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD)){
     }
      return name;
  }

  // Naming method for archiving file.
  private String buildFileName() throws Exception {
      String name = "DocGenerated_file.xml";                                   // default file name
      Date d = new Date();

      String date = TypeConverter.isoDateFrom(d);
      String dealstr = String.valueOf(this.deal.getDealId());
     if( version != null &&
        (version.equals(Dc.VERSION_1_APPROVAL) ||
         version.equals(Dc.VERSION_1_DENIAL)))
      {
        int systype = this.deal.getSystemTypeId();
        String prefix = this.getFileNamePrefix(deal.getStatusId());
        //name = prefix + "000" + filenum + ".000";
        if(systype == Mc.SYSTEM_TYPE_MORTY){
          int filenum = this.getNextSystemTypeNumber(systype);
          name = prefix + this.extractLenderName().trim() + filenum + ".000";
        }else{
          name = buildFxlinkName(name, prefix);  //#DG248
        }
      }
      else if(  version != null && version.equals(Dc.VERSION_1_UPLOAD) )
      {
        // -- # 371: upload file name for Cervus, CatR, 08-Jun-04
        String nc = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DOCPREP_UPLOADFILENAMINGCONVENTION,Dc.UPLOAD_FILE_NAMING_MOS);
        if (nc.toUpperCase().equals(Dc.UPLOAD_FILE_NAMING_CV))
        {
          //--- Catherine, #724
          date = TypeConverter.isoDateFrom24Hrs(d);
          //--- Catherine, #724 end
          name = "Cervus_Helix_" + date + "_" + dealstr + ".000";
        }
        else {
          //name = "MARS_" + date + "_" + dealstr + ".000";
          // change made based on Paul Lewis's request
          name = "BAS_" + date + "_" + dealstr + ".000";
        }
      } //if(  version != null && version.equals(Dc.VERSION_1_UPLOAD) )
      else if(  profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED ||
              profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED )
      {
        int mitype = this.deal.getMortgageInsurerId();
        if(mitype == Mc.MI_INSURER_GE)
        {
          name = "MI_GE" + date + "_" + dealstr + ".ge";
        }
        else if(mitype == Mc.MI_INSURER_CMHC)
        {
          name = "MI_CMHC" + date + "_" + dealstr + ".cmhc";
        }
        else
        {
          name = "MI_NO_INSURER" + date + "_" + dealstr + ".xml";
        }
      }
      else if(profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_EZENET_BASE)
      {
         String underscoredDate = TypeConverter.formattedDate(d,"yyyy_MM_dd_hhmmss");
         name = "Deal_" + dealstr.trim() + "_" + underscoredDate.trim() + ".xml" ;
      }
      else if(profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE)
      {
        if( isFXLinkType( deal.getSystemTypeId() ) ){  //#DG248
          name = buildFxlinkName(name, getFileNamePrefix(deal.getStatusId()));
        }
        else {
          String underscoredDate = TypeConverter.formattedDate(d,"yyyy_MM_dd_hhmmss");
          name = "PMESS_" + dealstr.trim() + "_" + underscoredDate.trim() + ".xml" ;
        }
      }
      else if(profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_TDCT_CAPLINK)
      {
         String underscoredDate = TypeConverter.formattedDate(d,"yyyy_MM_dd_hhmmss");
         name = "TDCT_CL_" + deal.getBranchProfileId() + "_" + dealstr.trim() + "_" + underscoredDate.trim() + ".xml" ;
      }
      else if(profile.getDocumentTypeId() == Dc.DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD)
      {
         String underscoredDate = TypeConverter.formattedDate(d,"yyyy_MM_dd_hhmmss");
         name = "CCAPS" + "_" + dealstr.trim() + "_?_"+ underscoredDate.trim() + ".xml" ;
      }

    logger.trace("DOCPREP: Output file name created: " + name);
    return name;
  }

  //#DG248
  /**
   * Build a name suitable for fxlink
   * @param name String
   * @param prefix String
   * @return String
   */
  private String buildFxlinkName(String name, String prefix) {
    // FXLINK:PHASEII modification
    // --------- this is to fix empty sourceapplicatonid, 14-jan-05
    if (deal.getSourceApplicationId() != null){
      Date d = new Date();
      //name = prefix + "_" + this.extractLenderName().trim() + "_" + StringUtil.stripWhiteSpace(deal.getSourceApplicationId()) + "_" + d.getTime() + ".xml";
      name = prefix + "_" + this.extractEcniLenderId().trim() + "_" + StringUtil.stripWhiteSpace(deal.getSourceApplicationId()) + "_" + d.getTime() + ".xml";
    }
    return name;
  }

  private boolean routeToXSL(){
    String templateName = profile.getDocumentTemplatePath();
    if(templateName == null){
      return false;
    }
    templateName = templateName.trim().toLowerCase();

    return (templateName.endsWith(".xsl"));
  }

  private boolean isFXLinkType(int pType) throws Exception
  {
    String fxlinkTypes = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_FXLINK_TYPES);
    // if the property does not exist return false
    if(fxlinkTypes == null){
      return false;
    }
    try
    {
      StringTokenizer st = new StringTokenizer(fxlinkTypes, ",");
      String token;

      while (st.hasMoreTokens())
      {
        token = st.nextToken();

        if( Integer.parseInt( token.trim() ) == pType){
          return true;
        }
      }
      return false;
    }
    catch (Exception e)
    {
      throw new Exception("ERROR - wrong property value(s) for property: com.basis100.fxlink.types "+e);	//#DG622
    }

  }

  // to FROST
  private boolean routeToMapping()
  {
    if( docType == Dc.DOCUMENT_GENERAL_TYPE_DEAL_UPLOAD ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_DEAL_APPROVAL ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_DEAL_DENIAL ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED||
        docType == Dc.DOCUMENT_GENERAL_TYPE_EZENET_BASE ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_TDCT_CAPLINK ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_FUNDING_UPLOAD ||
        docType == Dc.DOCUMENT_GENERAL_TYPE_PROSPERA_UPLOAD ||  //#DG164
        docType == Dc.DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD ||  //#DG190
        docType == Dc.DOCUMENT_GENERAL_TYPE_CLOSING_UPLOAD ||  //#DG242
        docType == Dc.DOCUMENT_GENERAL_TYPE_CCAPS_UPLOAD)                             // BMO to CCAPS 1.2

        return true;

    return false;
  }

  /**
   *  get the next sequence number for document names.
   *  ALERT: this should be done with db sequences - post R1
   */
  public int getNextSystemTypeNumber(int systype)
  {
    String sql = "Select NEXTAVAILABLENUMBER from systemtype where systemtypeid = " + systype;
    JdbcExecutor jExec = srk.getJdbcExecutor();
    int id = -1;

    try
    {
      int key = jExec.execute(sql);

      for (; jExec.next(key); )
      {
        id = jExec.getInt(key,1);
        break; // one record only;
      }

      jExec.closeData(key);

    }
    catch(Exception e)
    {
      String msg = "DOCPREP: Failed to obtain nextavailable number for Document name: " + e;	//#DG622
      DocumentGenerationManager.debug(msg);
      logger.error(msg);
    }

   int nextid = id + 1;

   //if(nextid > 999) nextid = 0;
   // programmer's note: this change took place because TD has larger volume of files and it
   // happened that we lost some files because they were overwritten. this will cure the problem
   // for a little while, but we do not have more room than four digit number. Four spaces are
   // already occupied and we must follow 8+3 file naming convention.
   // NOTE posted by Zivko on Oct 2, 2003
   if(nextid > 9999) nextid = 0;

   sql = "Update systemtype set NEXTAVAILABLENUMBER = " + nextid + " where systemtypeId = " + systype;

    try
    {
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      String msg = "DOCPREP: Failed to set nextavailable number for Document name: " + e;	//#DG622
      printMsg(msg, true);
    }

    return id;
  }

  /**
   *  specified prefix (for mapping identification) - should be parameterized in future.
   */
  public String getFileNamePrefix(int status)
  {
    switch(status)
    {
      case Mc.DEAL_APPROVED:
      case Mc.DEAL_COMMIT_OFFERED:
      case Mc.DEAL_OFFER_ACCEPTED:
      case Mc.DEAL_CONDITIONS_OUT:
      case Mc.DEAL_CONDITIONS_SATISFIED:
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT:
      case Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED:
      case Mc.DEAL_POST_DATED_FUNDING:
      case Mc.DEAL_FUNDED:
      case Mc.DEAL_TO_SERVICING:
       return "a";

      case Mc.DEAL_PRE_APPROVAL_OFFERED:
       return "r";

      case Mc.DEAL_DENIED:
      case Mc.DEAL_COLLAPSED:
       return "d";

      default:
       return "p";
    }
  }

  private String getMetaDataPath(String rootDir)
  {
      String path = profile.getDocumentSchemaPath().toLowerCase();
      String location = "";
	  if (path!=null && !path.startsWith("com.basis100.deal.docprep.extract.")) {
		  location = ExpressDocprepHelper.getMdfmlFolder() + path;
	  }
	  return location;
  }

  private String getMetaData()
  {
    return profile.getDocumentSchemaPath().toLowerCase();
  }

  /**
  *  gets  the source Deal
  */
  public Deal getDeal()
  {
    return this.deal;
  }

   /**
  *  gets the dealId that identifies the source Deal
  */
  public int getDealId()
  {
    return this.deal.getDealId();
  }

   /**
  *  gets the copyId of the source Deal
  */
  public int getDealCopyId()
  {
    return this.deal.getCopyId();
  }
  /**
  *  gets the email destination for this request
  */
  public String getEmailAddress()
  {
    return queueRequest.getEmailAddress();
  }

  public String getFaxNumbers(){
    return queueRequest.getFaxNumbers();
  }

  public int getLanguagePreferenceId(){
    return this.queueRequest.getLanguagePreferenceId();
  }

  /**
  *  gets the email FullName for this request
  */
  public String getEmailFullName()
  {
    return queueRequest.getEmailFullName();
  }

   /**
  *  gets the email subject for this request
  *  zivko:marker
  */
  public String getEmailSubject()
  {
    String lVal = queueRequest.getEmailSubject();
    if(lVal == null){ return ""; }
    int lPos = lVal.indexOf("|");
    if(lPos == -1){ return lVal; }
    String lPart = lVal.substring(0,lPos);
    lPos++;
    String rPart = lVal.substring(lPos);
    lPart = StringUtil.zeroLeftPad(lPart,6);

    return lPart + " " + rPart;
  }

   /**
  *  gets the email body for this request
  */
  public String getEmailText()
  {
    return queueRequest.getEmailText();
  }

    /**
  *  gets the email FullName for this request
  */
  public String getDocumentFullName()
  {
    return profile.getDocumentFullName();
  }

     /**
  *  gets the email Description for this request
  */
  public String getDocumentDescription()
  {
    return profile.getDocumentDescription();
  }

    /**
  *  gets the email profile for this request
  */
  public DocumentProfile getDocumentProfile()
  {
    return profile;
  }

  public SessionResourceKit getSessionResourceKit()
  {
    return srk;
  }

  /**
   * Deliver memo
   */
  private void deliverMemo()
  {
    //DocumentGenerationManager dgm = DocumentGenerationManager.getInstance();

    try
    {
      String subject = (getEmailSubject() != null) ? getEmailSubject() : "";
      String text = (getEmailText() != null) ? getEmailText() : "";
      String to = getEmailAddress();

      logger.debug("DOCPREP: Delivery address: " + to);

      SimpleMailMessage message = createMailMsg(to);    //#DG360

      message.setText(text);
      message.setSubject(subject);

      logger.info("Sending memo:" +
         "\nTo: " + to +
         "\nSubject: " + subject +
         "\nText: " + text);

      message.send(null);

      logger.info("Memo sent.");

      DocumentGenerationManager.debug("DOCPREP: DEBUG: Mailed Document for: " + this);

    }
    catch(Exception de)
    {
        logger.error(getClass().getName() + ":\n" + de);
    }
  }

  //#DG360
  /**
   * prepares the mail message setting the from and to fields
   * @param to String
   * @return SimpleMailMessage
   * @throws Exception
   */
  private SimpleMailMessage createMailMsg(String to) throws Exception {
    if (to == null)
      throw new DocPrepDeliveryException("DOCPREP:ERROR: No delivery address found.");

    SimpleMailMessage message = new SimpleMailMessage();
    DocumentGenerationManager.debug("DOCPREP: DEBUG: Mailing Document using: " + message + " with " + this);

    try
    {
      message.makeDefault("docprep");
    }
    catch(MailException e)   //no docprep entry in application.xml - mail module
    {
      logger.info("DGH@createMailMsg(): No message entry for 'docprep' found in application.xml mail module:"+e);
      //String defEmail = System.getProperty("com.basis100.sysmail.from", "bxsupport@filogix.com");
      String defEmail = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_BASIS100_SYSMAIL_FROM, "notification@filogix.com");
      message.setFrom(defEmail);
      logger.info("Using " + defEmail + " as a from email");
    }
    message.clearTo();
    message.appendTo(to);
    return message;
  }

  private synchronized void sendLDDClosingMessage(String msg) throws Exception
  {
    logger.info("DOCPREP : method sendLDDClosingMessage entered.");
    try {
      LDDRequestBean lddRequestBean = new LDDRequestBean();
      final PropertiesCache pc = PropertiesCache.getInstance();
      //#DG154 lddRequestBean.setLddDocument(msg);
      lddRequestBean.setReceivingPartyId(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_RECEIVINGPARTYID));
      lddRequestBean.setRequestingPartyId(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_REQUESTINGPARTYID));
      lddRequestBean.setAccountIdentifier(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTID));
      lddRequestBean.setAccountPassword(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTPASSWORD));

      msg = xmlTransform(msg, "LDDUpload.xsl");      //#DG416
      lddRequestBean.setDocument(new com.filogix.common.document.Document(Document.TEXT_XML_CONTENT_TYPE, Document.UTF8_ENCODING, msg.getBytes())); //#DG154

      logger.info("DOCPREP : LDD : filogix.datx.url coming from property file is : " + pc.getUnchangedProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_URL, null));
      LDDClosingMessageSender requestSender = new LDDClosingMessageSender();
      String requestXML = requestSender.createRequest(lddRequestBean);
      String responseXML = requestSender.executeAsynch(requestXML, pc.getUnchangedProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_URL, null));
      // ----------- #786 - Xceed - LDD Link
      logger.debug("DatX responseXML = " + responseXML);
      if (responseXML.indexOf("Invalid XML sent") != -1){
        throw new InvalidValidationException(responseXML);
      }
      // ----------- #786 - Xceed - LDD Link
      logger.info("DOCPREP : LDD : item sent.");

    //#DG622 rewritten, exception handling
    } catch (Exception ex) {
      logger.error("DOCPREP: Failed to send LDD Closing message: " + ex);
      throw new DatXException("DOCPREP: Failed to send LDD Closing message: " +  ex);
    }
    logger.info("DOCPREP : method sendLDDClosingMessage finished.");
  }

  private boolean isSendLDDClosing() {
    boolean lResult = false;
    String lPropVal = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_ENABLED,"N");

    if( ! lPropVal.equalsIgnoreCase("y")){return lResult;}

    // 29 Dec 2004, Catherine, PVCS #809:
    // Doc Prep should only be sending deals to DatX (then to LDD) when the property's province is not Quebec. They don't use LDD for Quebec applications.
    if (isLDDValidProperty()){
      switch (profile.getDocumentTypeId()) {
        case Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE: {      lResult = true;     }
        case Dc.DOCUMENT_GENERAL_TYPE_SOLICITOR_PACKAGE_PDF: {  lResult = true;     }
      }
    }
    return lResult;
  }

  private boolean isLDDValidProperty(){
    boolean result = true;
    // primary property
    Property pProperty = null;

    try {
      pProperty = new Property(srk, null);
      pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), 
                      deal.getCopyId(), deal.getInstitutionProfileId());
      if (pProperty.getProvinceId() == 11 ){ result = false; }
    }
    catch (Exception fe) {
      logger.error(fe.getClass() + ": " + fe.getMessage());
    }
    return result;
  }

  // link to MCAP ---------------
  private void deliverFundingUpload(String xmlData) throws Exception {
    try {
			if (version.equalsIgnoreCase(VERSION_FUNDING2)) {  //#DG242
        sendDatXUploadMessage(xmlData, COM_FILOGIX_DATX_FUNDING_RECEIVINGPARTYID);
        return;
      }
      //#DG622
      else if (version.equalsIgnoreCase(VERSION_FUNDING_ALT)) {  
      	final String fileName = version+'-'
      		+TypeConverter.isoDateFrom24Hrs(new Date())+".xml";
      	writeData(OUT_TAG_UPLOAD, fileName, finalTransform(xmlData));
        return;
      }
      //#DG622 end
      // 1) login set up
      ServicingRequestBean requestBean = new ServicingRequestBean();
      final PropertiesCache pc = PropertiesCache.getInstance();
      requestBean.setRequestingPartyId(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_REQUESTINGPARTYID));
      requestBean.setReceivingPartyId(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_MCAP_RECEIVINGPARTYID));
      requestBean.setAccountIdentifier(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTID));
      requestBean.setAccountPassword(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTPASSWORD));

      // 2) data set up
      // String xml = "<Root><Child>Test message</Child></Root>"; // test

      //#DG622 OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("file"));

      //#DG154 requestBean.setMessage(dataXML.getBytes("UTF-8"));
      requestBean.setDocument(new com.filogix.common.document.Document(Document.TEXT_XML_CONTENT_TYPE, Document.UTF8_ENCODING, xmlData.getBytes())); //#DG154

      ServicingMessageSender requestSender = new ServicingMessageSender();

      String requestXML = requestSender.createRequest(requestBean);
      // logger.trace("FUNDING REQUEST: " + requestXML);  // avoid logging the request because it is very big

      // 3) send request
      String datXURL = pc.getUnchangedProperty(deal.getInstitutionProfileId(),COM_FILOGIX_DATX_URL, null);
      logger.trace("sendToDatxInvoker: DatX URL = " + datXURL);
      printMsg("DOCPREP: Sending FUNDING upload to DatX", false);
      String responseXML = requestSender.execute(requestXML, pc.getUnchangedProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_URL, null));
      logger.trace("FUNDING RESPONSE: " + responseXML);

      // 4) analize response
      if (responseXML.indexOf("Invalid XML sent") != -1){
        throw new InvalidValidationException(responseXML);
      }
      DatxResponseParser drh = new DatxResponseParser();
      drh.read(responseXML);

      if (drh.isFailed()){
        throw new DatXException("DocPrep failed to send FUNDING upload to DatX:\n"
            + "    REFERENCE_KEY = " +  drh.getReferenceKey()
            + "; ERROR_CODE = " + drh.getCode() + "; DESCRIPTION = " + drh.getDescription() + "\n");
      }
      if (drh.isAcknowlegement() || drh.isCompleted()){
        printMsg("DOCPREP: FUNDING upload successfully sent to DatX, CONVERSATION_KEY = " + drh.getConversationKey(), false);
      }
    //#DG622 rewritten
    } catch (CommunicationException ce) {
      // The application can perform a retry if required or send e-mail to support
      printMsg("DocPrep to DatX: Connection Exception Occured.\n" + ce, true);
    } catch (DatXException ex) {
      throw ex;
    } catch (Exception ex) {
      throw new DocPrepException("DocPrep failed to send FUNDING upload: " + ex);	
    }
  }
  // link to MCAP end ---------------

  //* #DG164 send to sunaptic

   /**
    * encode and send xml data to sunaptic, it's sync cause mimeGener and
    *   web service connection are static and shared
    * @param msg String the data to send
    * @throws Exception
    */
   private synchronized void sendSunapticUpload(String msg) throws Exception
  {
  //if (sunapticCon == null)  //#DG220
  //    return;
    
    logger.info("DOCPREP : method sendUploadMessage entered.");
    try {
      if (mimeGener != null){
        MimeBodyPart mimeBody = new MimeBodyPart();
        mimeBody.setText(msg);
         mimeBody = mimeGener.generate(mimeBody, SMIMEEnvelopedGenerator.RC2_CBC, "BC");
        ByteArrayOutputStream lomsgstrm = new ByteArrayOutputStream();
        mimeBody.writeTo(lomsgstrm);
        //logger.info("DOCPREP : method sendUploadMessage mimed content:"+lomsgstrm.toString());
        msg = lomsgstrm.toString();
      }
      else{ 
    	  return;
      }
        sunapticCon.providerMortgage(msg);
    
      logger.info("DOCPREP : sendSunapticUpload : item sent.");
    } catch (Exception ex) {
      logger.error("DOCPREP: Failed to sendSunapticUpload: " + ex);
      throw ex;
    }
    logger.info("DOCPREP : method sendSunapticUpload finished.");
  }
  //#DG164 end

  //* #DG190 send to gemoney

   /**
    * encode and send xml data to sunaptic, it's sync cause mimeGener and
    *   web service connection are static and shared
    * @param xmlData String the data to send
    * @throws Exception
    */
  private synchronized void sendGeMoneyUpload(String xmlData) throws Exception
  {
    if (gemoneyCon == null) {
      return;
    }
    logger.trace("DOCPREP : method sendGeMoneyUpload entered.");
    try {
      logger.debug("DOCPREP : sendGeMoneyUpload : item:" +gemoneyCon.submitApplication(xmlData));

      /*/URLConnection conn;
      HttpURLConnection conn;
      String s = PropertiesCache.getInstance().getUnchangedProperty("com.filogix.GEMoney.url", null);
//      s += "?FilogixFeed="+msg;
      URL url = new URL(s);

      conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
      //conn.setRequestProperty("Host",url.getHost());
      //conn.setRequestProperty("Content-Length", String.valueOf(inpFile.length()));

      // Send the request to the server
      // below is where the connection actually happens
      OutputStream outstr = conn.getOutputStream();
      BufferedWriter outwr = new BufferedWriter(new OutputStreamWriter(outstr));
//      outwr.write("FilogixFeed=");
      outwr.write("strFilogixXml=");
      outwr.write(msg);
      outwr.close();
      // read answer
      InputStream intstr = conn.getInputStream();
      BufferedReader rd = new BufferedReader(new InputStreamReader(intstr));
      int c;
      StringBuffer sb = new StringBuffer();
      while ( (c = rd.read()) != -1)
        sb.append(c);
      rd.close();

      if (conn.getResponseCode() != HttpURLConnection.HTTP_ACCEPTED) {
        throw new Exception(sb.toString());
      }
      //*/
      logger.trace("DOCPREP : sendGeMoneyUpload : item sent.");
    } catch (Exception ex) {
      logger.error("DOCPREP: Failed to sendGeMoneyUpload: " + ex);
      throw ex;
    }
    logger.trace("DOCPREP : method sendGeMoneyUpload finished.");
  }
  //#DG190 end

  //#DG242
  /**
   * Newer link to datx upload services. It can potentialy be used for all types
   *  of datx uploads.
   * @param dataXML String
   * @param recvPartyProp String the name of the property containing the receiving party id
   * @throws Exception
   */
  private void sendDatXUploadMessage(String dataXML, String recvPartyProp) throws Exception {
    // 1) login set up
    MessageProducerRequestBean requestBean = new MessageProducerRequestBean();
    PropertiesCache pc = PropertiesCache.getInstance();
    requestBean.setReceivingPartyId(pc.getProperty(deal.getInstitutionProfileId(), recvPartyProp));
    requestBean.setRequestingPartyId(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_REQUESTINGPARTYID));
    requestBean.setAccountIdentifier(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTID));
    requestBean.setAccountPassword(pc.getProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTPASSWORD));

    // 2) data set up
    //#DG622 OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("file"));
    //#DG730 final String enc = Document.UTF8_ENCODING;
    final String enc = ENCODING_ISO_8859_1;	 
		requestBean.setDocument(new com.filogix.common.document.Document(Document.TEXT_XML_CONTENT_TYPE, enc, dataXML.getBytes(enc)));
    MessageProducerMessageSender requestSender = new MessageProducerMessageSender();
    String requestXML = requestSender.createRequest(requestBean);

    // 3) send request
    String datXURL = pc.getUnchangedProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_URL, null);
    logger.trace("sendToDatxInvoker: DatX URL = " + datXURL);
    printMsg("DOCPREP: Sending general upload to DatX", false);
    String responseXML = requestSender.execute(requestXML, pc.getUnchangedProperty(deal.getInstitutionProfileId(), COM_FILOGIX_DATX_URL, null));
    logger.trace("GENERAL DATX RESPONSE: " + responseXML);

    // 4) analize response
    if (responseXML.indexOf("Invalid XML sent") != -1){
      throw new InvalidValidationException(responseXML);
    }
    DatxResponseParser drh = new DatxResponseParser();
    drh.read(responseXML);

    if (drh.isFailed()){
      throw new DatXException("DocPrep failed to send general upload to DatX:\n"
          + "    REFERENCE_KEY = " +  drh.getReferenceKey()
          + "; ERROR_CODE = " + drh.getCode() + "; DESCRIPTION = " + drh.getDescription() + "\n");
    }
    if (drh.isAcknowlegement() || drh.isCompleted()){
      printMsg("DOCPREP: general upload successfully sent to DatX, CONVERSATION_KEY = " + drh.getConversationKey(), false);
    }
  }
  //#DG242 end
}
