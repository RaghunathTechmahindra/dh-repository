package com.basis100.deal.docprep.extract.data;

/**
 * 28/Aug/2008 DVG #DG746 FXP21967: LS-MLMQA - "Odd" characters in Express Commitment Letter 
 * 08/Dec/06 CR20 - Catherine, 3.2 - CR20, use parsed address if present
 * 07/Nov/2005 DVG #DG354 Decline letter project - new
 *  - why is this class abstract? let it provide some basic functionaly instead
  * 24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package
   - Many changes to become the Xprs standard
  *    by making XSLExtractor inherit from DealExtractor/XSLExtractor
  *    the method: createSubTree(doc,"Deal", srk, deal, true, true, lang, true);
  *    is made available so that all templates: GENX type and the newer ones
  *    alike can share the same xml
 * 01/Jun/2005 DVG #DG218 #1488  DJ - CR120 - Disability percentage coverage required
 * 28/Mar/2005 DVG #DG168 #1075  DJ - document change - CR120  / translate the entry
 */

import java.lang.reflect.*;
import java.util.*;

import java.awt.event.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.*;
import MosSystem.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import com.basis100.xml.*;
import com.filogix.util.Xc;

public class DealExtractor implements java.awt.event.ActionListener, Xc //#DG354 no more abstract
{
	public static boolean debug = false;
  //  public SysLogger logger = null;
  public Log logger = LogFactory.getLog(getClass());	//#DG746        

  protected Vector actionListeners=new Vector();
  protected Deal deal;
  protected int lang;
  protected DocumentFormat format;
  protected SessionResourceKit srk;
  protected Document doc;

  //#DG354 public abstract String extractXMLData(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk) throws Exception;
  //#DG354 protected abstract void buildDataDoc() throws Exception;

  protected Element dealAdditionalElement = null;
  protected Element dealElement = null;
  protected Element specElem = null;

  protected RatioFieldList ratioFieldList ;
  protected boolean primaryBorrAddressExtracted ;

  //#DG354 no more abstract
  /**
   * extractXMLData
   *
   * @param de DealEntity
   * @param pLang int
   * @param pFormat DocumentFormat
   * @param pSrk SessionResourceKit
   * @return String
   * @throws Exception
   */
  public String extractXMLData(DealEntity de, int pLang, DocumentFormat pFormat,
                               SessionResourceKit pSrk) throws Exception
  {
    addActionListener(this);
    ratioFieldList = DocumentGenerationManager.getInstance().getRatioFieldList();

    deal = (Deal) de;
    lang = pLang;
    format = pFormat;
    srk = pSrk;

    buildDataDoc();

    XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

//    if (pLang == Mc.LANGUAGE_PREFERENCE_FRENCH)
       xmlwr.setEncoding(ENCODING_ISO_8859_1);

    return xmlwr.printString(doc);
  }

  //#DG354 no more abstract
  /**
   * builds the basic data node
   * @throws Exception
   */
  protected void buildDataDoc() throws Exception
  {
    doc = XmlToolKit.getInstance().newDocument();
    Element root = doc.createElement("Document");
    root.setAttribute("type","1");
    root.setAttribute("lang","" + lang);

    Element dealNode = createSubTree(doc,"Deal", srk, deal, true, true, lang, true);

    root.appendChild( dealNode ) ;
    root.appendChild( buildGeneralTags()  );

    doc.appendChild(root);
  }

  protected void buildDealTags() throws Exception
  {
    String val;
    // set initial values
    primaryBorrAddressExtracted = false;
    // create element nodes first
    dealElement = doc.createElement("Deal");
    dealAdditionalElement = doc.createElement("DealAdditional");

    addTo(dealElement, "dealId", ""+deal.getDealId());
    addTo(dealElement, "copyId", ""+deal.getCopyId());
    addTo(dealElement, "dealTypeId", ""+deal.getDealTypeId());
    addTo(dealElement, "underwriterUserId", "" + deal.getUnderwriterUserId());
    addTo(dealElement, extractUnderwriterData() );
    addTo(dealElement, "totalPurchasePrice", formatCurrency(deal.getTotalPurchasePrice()) );
    addTo(dealElement, "downPaymentAmount", formatCurrency(deal.getDownPaymentAmount()) );
    addTo(dealElement, "totalLoanAmount", formatCurrency(deal.getTotalLoanAmount()) );

    addTo(dealElement, "netLoanAmount", formatCurrency(deal.getNetLoanAmount()) );

    addTo(dealElement, "netInterestRate", formatInterestRate(deal.getNetInterestRate()) );
    //addTo(dealElement, "systemTypeId", ""+45);   // for test purposes only
    addTo(dealElement, "systemTypeId", ""+deal.getSystemTypeId());
    addTo(dealElement, "interestTypeId", ""+deal.getInterestTypeId());
    final int dealInstitutionId = srk.getExpressState().getDealInstitutionId();
		val = BXResources.getPickListDescription(dealInstitutionId, PKL_INTEREST_TYPE, deal.getInterestTypeId(),lang);
    addTo(dealElement , "interestType", val );


    addTo(dealElement, "discount", formatInterestRate(deal.getDiscount()) );
    addTo(dealElement, "premium", formatInterestRate(deal.getPremium() ) );
    addTo(dealElement, "PandiPaymentAmount", formatCurrency(deal.getPandiPaymentAmount()) );
    addTo(dealElement, "paymentFrequencyId", ""+deal.getPaymentFrequencyId());
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_PAYMENT_FREQUENCY, deal.getPaymentFrequencyId(),lang);
    addTo(dealElement , "paymentFrequency", val );

    addTo(dealElement, "mortgageInsurerId", ""+deal.getMortgageInsurerId());
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_MORTGAGEINSURANCECARRIER, deal.getMortgageInsurerId(),lang);
    addTo(dealElement , "mortgageInsurer", val );
    addTo(dealElement, "MITypeId", ""+ deal.getMITypeId());
    addTo(dealElement, "MIIndicatorId", String.valueOf(deal.getMIIndicatorId()));		//#DG678

    addTo(dealElement, "actualPaymentTerm", ""+deal.getActualPaymentTerm());
    addTo(dealElement, "amortizationTerm", ""+deal.getAmortizationTerm());
    addTo(dealElement, "prePaymentOptionsId", ""+deal.getPrePaymentOptionsId());
    addTo(dealElement, "privilegePaymentId", ""+deal.getPrivilegePaymentId());
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_PRIVILEGE_PAYMENT, deal.getPrivilegePaymentId(),lang);
    addTo(dealElement , "privilegePaymentDescription", val );

    addTo(dealElement, "lienPositionId", ""+deal.getLienPositionId() );
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_LIEN_POSITION, deal.getLienPositionId(),lang);
    addTo(dealElement , "lienPosition", val );

    addTo(dealElement,"combinedGDS",  formatInterestRate(deal.getCombinedGDS() ) );
    addTo(dealElement,"combinedTDS",  formatInterestRate(deal.getCombinedTDS() ) );

    addTo(dealElement, "MIStatusId", ""+deal.getMIStatusId() );

    if(deal.getProgressAdvance() != null){
      addTo(dealElement, "progressAdvance", deal.getProgressAdvance().toUpperCase().trim() );
    }

    if(deal.getMIPolicyNumber() != null){
      addTo(dealElement, "MIPolicyNumber", deal.getMIPolicyNumber() );
    }
    if(deal.getMIPolicyNumCMHC() != null){
      addTo(dealElement, "MIPolicyNumCMHC", deal.getMIPolicyNumCMHC() );
    }
    if(deal.getMIPolicyNumGE() != null){
      addTo(dealElement, "MIPolicyNumGE", deal.getMIPolicyNumGE() );
    }
    addTo(dealElement, "MIPremiumAmount", formatCurrency(deal.getMIPremiumAmount()) );

    if( deal.getMIUpfront() != null ){
      addTo(dealElement, "MIUpfront", deal.getMIUpfront().toUpperCase().trim() );
    }

    addTo(dealElement, "MIPremiumPST", formatCurrency(deal.getMIPremiumPST()) );
    if( deal.getEstimatedClosingDate() != null ){
      addTo(  dealElement,  "estimatedClosingDate" , formatDate(  deal.getEstimatedClosingDate()  ) );
    }

    addTo(dealElement, "mortgageInsuranceResponse", deal.getMortgageInsuranceResponse() );

    if( deal.getApplicationDate() != null){
      addTo(  dealElement,  "applicationDate" , formatDate(  deal.getApplicationDate()  ) );
    }

    if( deal.getFirstPaymentDateMonthly() != null ){
      addTo(  dealElement,  "firstPaymentDateMonthly" , formatDate(  deal.getFirstPaymentDateMonthly()  ) );
    }

    addTo(dealElement, "dealPurposeId", ""+deal.getDealPurposeId() );
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_DEAL_PURPOSE, deal.getDealPurposeId(),lang);
    addTo(dealElement , "dealPurpose", val );

    // -- DJ_CR_010, DJ_CR203.1 start ----------------
    addTo(dealElement , "commisionCode", deal.getCommisionCode() );
    addTo(dealElement , "multiProject", deal.getMultiProject() );
    addTo(dealElement , "proprietairePlus", deal.getProprietairePlus() );
    addTo(dealElement , "proprietairePlusLOC", "" + formatCurrency(deal.getProprietairePlusLOC()));
    addTo(dealElement , "pmntPlusLifeDisability", formatCurrency(deal.getPmntPlusLifeDisability()));
    addTo(dealElement , "interestRateIncrement", formatInterestRate(deal.getInterestRateIncrement()));
    addTo(dealElement , "equivalentInterestRate", formatInterestRate(deal.getEquivalentInterestRate()));
    addTo(dealElement , "pricingProfileId", "" + deal.getPricingProfileId());
    // -- DJ_CR_010,  DJ_CR203.1 end ----------------
    addTo(dealElement,  "bestRate", "" + formatInterestRate(extractBestRate()));
    addTo(dealElement,  "refiImprovementAmount", formatCurrency(deal.getRefiImprovementAmount()));

    addTo(dealElement, "interestRateIncLifeDisability", "" + formatInterestRate(deal.getInterestRateIncLifeDisability()));

    // -- DJ #499 fields - Catherine  --
    addTo(dealElement , "addCreditCostInsIncurred", "" + formatCurrency(deal.getAddCreditCostInsIncurred()));
    addTo(dealElement , "totalCreditCost", "" + formatCurrency(deal.getTotalCreditCost()));
    addTo(dealElement , "intWithoutInsCreditCost", "" + formatCurrency(deal.getIntWithoutInsCreditCost()));
    // -- DJ #499 fields - Catherine end --
    //======================================== ActiveX Ver 1.2 End ==========================================
    // Catherine, #567 - DJ
    addTo(dealElement, "combinedTotalAnnualHeatingExp", "" + formatCurrency(deal.getCombinedTotalAnnualHeatingExp()));
    addTo(dealElement, "combinedTotalAnnualTaxExp", "" + formatCurrency(deal.getCombinedTotalAnnualTaxExp()));
    addTo(dealElement, "combinedTotalCondoFeeExp", "" + formatCurrency(deal.getCombinedTotalCondoFeeExp()));

    extractBorrowerSection();
    extractPropertySection() ;

    addTo(dealElement, extractSourceOfBusinessProfile() );
    addTo(dealElement, extractSourceFirmProfile() );
    addTo(dealElement, extractBranchAddressComplete() );
    addTo(dealElement, extractDealNotes() );
    extractDocumentTrackingRecords();

    extractCreditBureauReport();

    addTo( dealAdditionalElement, extractPartyType("solicitorData",Mc.PARTY_TYPE_SOLICITOR ));
    addTo( dealAdditionalElement, extractPartyType("appraiserData",Mc.PARTY_TYPE_APPRAISER ));
    addTo( dealAdditionalElement, extractPartyType("originationBranch",Mc.PARTY_TYPE_ORIGINATION_BRANCH ));
    addTo( dealAdditionalElement, extractPartyType("referralSource",Mc.PARTY_TYPE_REFERRAL_SOURCE ));

    extractEscrowPayments();
    addTo(dealElement, extractMtgProd() );
  }

  //#DG238 minor change to allow it to be overriden by XSLExtractor descendants
  //protected void extractCreditBureauReport() throws Exception
  protected Node extractCreditBureauReport() throws Exception
  {
    CreditBureauReport cbr = new CreditBureauReport(srk);
    Collection cbrColl = cbr.findByDealid(deal.getDealId());
    Iterator it = cbrColl.iterator();
    Node elemNode = null;
    elemNode = doc.createElement("creditReport");

    while( it.hasNext() ){
      CreditBureauReport oneReport = (CreditBureauReport)it.next();
      Element cbrNode = doc.createElement("CreditBureauReport");
      addTo(cbrNode,"creditBureauReportId",""+oneReport.getCreditBureauReportId());

      // --- Catherine, #569, DJ
      if (!isEmpty(oneReport.getCreditReport()))
      {
          elemNode = convertCRToLines(elemNode, makeSafeXMLString(oneReport.getCreditReport()));
      }

      cbrNode.appendChild(elemNode);
      // ---
      addTo(dealElement, cbrNode);
    }
    return elemNode;  //#DG238
  }

  protected double extractBestRate(){
    double val = 0.0;
    try
    {
        PricingRateInventory pri = new PricingRateInventory(srk);
        pri = pri.findByPrimaryKey( new PricingRateInventoryPK (deal.getPricingProfileId()));

        if(pri != null )
        {
          val = pri.getBestRate();
        }
    }
    catch (Exception e){ 
    	logger.error("extractBestRate:" + e);		//#DG746 
    }

    return val;
  }

  protected Node extractDealNotes() throws Exception
  {
    Element dealNotesNode = doc.createElement("DealNotes");
    Collection lNotesCol = deal.getDealNotes();
    Iterator it = lNotesCol.iterator();
    while(it.hasNext()){
      DealNotes oneNote = (DealNotes)it.next();
      Element oneNoteNode = doc.createElement("DealNote");
      addTo(oneNoteNode,"dealId","" + oneNote.getDealId());
      addTo(oneNoteNode,"dealNotesId","" + oneNote.getDealNotesId());
      addTo(oneNoteNode,"dealNotesCategoryId","" + oneNote.getDealNotesCategoryId());
      addTo(oneNoteNode,"dealNotesUserId","" + oneNote.getDealNotesUserId() );
      addTo(oneNoteNode,"dealNotesText", oneNote.getDealNotesText() );
      addTo(oneNoteNode,"dealNotesDate", formatDate(  oneNote.getDealNotesDate()  ));
      addTo(dealNotesNode, oneNoteNode);
    }
    return dealNotesNode;
  }


  protected Node extractMtgProd() throws Exception
  {
    Element mtgProdNode = doc.createElement("MtgProd");
    MtgProd mtgProd = deal.getMtgProd();
    addTo(mtgProdNode,"mtgProdId","" + mtgProd.getMtgProdId());
    //#DG168 addTo(mtgProdNode,"mtgProdName","" + mtgProd.getMtgProdName());
    addTo(mtgProdNode,"mtgProdName",
          BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_MTGPROD, mtgProd.getMtgProdId(),lang)); //#DG168

    addTo(mtgProdNode,"interestTypeId","" + mtgProd.getInterestTypeId());
    return mtgProdNode;
  }

/*
  protected Node extractPrimaryPropertyData(Property pProp){
    Element onePropNode = doc.createElement("primaryProperty");
    if(pProp.isPrimaryProperty()){
      addTo(onePropNode , "primaryPropertyFlag", "Y" );
    }else{
      addTo(onePropNode , "primaryPropertyFlag", "N" );
    }
    addTo(onePropNode , "propertyId",""+ pProp.getPropertyId() );
    addTo(onePropNode , "propertyStreetNumber", pProp.getPropertyStreetNumber() );
    addTo(onePropNode , "propertyStreetName", pProp.getPropertyStreetName() );
    addTo(onePropNode , "propertyAddressLine2", pProp.getPropertyAddressLine2() );
    addTo(onePropNode , "propertyCity", pProp.getPropertyCity() );
    addTo(onePropNode , "propertyPostalFSA", pProp.getPropertyPostalFSA() );
    addTo(onePropNode , "propertyPostalLDU", pProp.getPropertyPostalLDU() );
    String val = BXResources.getPickListDescription("Province",pProp.getProvinceId(),lang);
    addTo(onePropNode , "propertyProvince", val );
    addTo(onePropNode,"unitNumber" ,pProp.getUnitNumber());
    val = BXResources.getPickListDescription("StreetType",pProp.getStreetTypeId(),lang);
    addTo(onePropNode , "propertyStreetType", val );
    val = BXResources.getPickListDescription("StreetDirection",pProp.getStreetDirectionId(),lang);
    addTo(onePropNode , "propertyStreetDirection", val );
    return onePropNode;
  }
*/

  protected Node extractPropertyData(Property pProp) throws Exception
  {
    Element onePropNode = doc.createElement("Property");
    String val;
    if(pProp.isPrimaryProperty()){
      addTo(onePropNode , "primaryPropertyFlag", "Y" );
    }else{
      addTo(onePropNode , "primaryPropertyFlag", "N" );
    }
    addTo(onePropNode , "propertyId", ""+pProp.getPropertyId() );
    addTo(onePropNode , "propertyStreetNumber", pProp.getPropertyStreetNumber() );
    addTo(onePropNode , "propertyStreetName", pProp.getPropertyStreetName() );
    //addTo(onePropNode , "streetTypeId", pProp.getStreetTypeId() );
    //val = BXResources.getPickListDescription("StreetType",pProp.getStreetTypeId(),lang);
    //addTo(onePropNode , "streetType", val );

    addTo(onePropNode , "propertyAddressLine2", pProp.getPropertyAddressLine2() );
    addTo(onePropNode , "propertyPostalFSA", pProp.getPropertyPostalFSA() );
    addTo(onePropNode , "propertyPostalLDU", pProp.getPropertyPostalLDU() );
    addTo(onePropNode , "propertyCity", pProp.getPropertyCity() );
    addTo(onePropNode , "provinceId", ""+pProp.getProvinceId() );
    final int dealInstitutionId = srk.getExpressState().getDealInstitutionId();
		val = BXResources.getPickListDescription(dealInstitutionId, PKL_PROVINCE, pProp.getProvinceId(),lang);
    addTo(onePropNode , "province", val );
    addTo(onePropNode,"unitNumber" ,pProp.getUnitNumber());

    addTo(onePropNode , "newConstructionId",  ""+pProp.getNewConstructionId() );
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_NEW_CONSTRUCTION,pProp.getNewConstructionId(),lang);
    addTo(onePropNode , "newConstruction", val );

    addTo(onePropNode,"dwellingTypeId","" + pProp.getDwellingTypeId());
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_DWELLING_TYPE, pProp.getDwellingTypeId(),lang);
    addTo(onePropNode , "dwellingType", val );

    addTo(onePropNode , "propertyTypeId",  ""+pProp.getPropertyTypeId() );
    addTo(onePropNode,"streetTypeId" ,""+pProp.getStreetTypeId());
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_STREET_TYPE, pProp.getStreetTypeId(),lang);
    addTo(onePropNode , "streetType", val );

    addTo(onePropNode,"streetDirectionId" ,""+pProp.getStreetDirectionId());
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_STREET_DIRECTION, pProp.getStreetDirectionId(),lang);
    addTo(onePropNode , "streetDirection", val );
    addTo(onePropNode , "waterTypeId", "" + pProp.getWaterTypeId());

    addTo(onePropNode , "legalLine1", pProp.getLegalLine1());
    addTo(onePropNode , "legalLine2", pProp.getLegalLine2());
    addTo(onePropNode , "legalLine3", pProp.getLegalLine3());

    AppraisalOrder appraisalOrder = new AppraisalOrder(srk,null);
    PropertyPK propPK = new PropertyPK( pProp.getPropertyId(), pProp.getCopyId());
    appraisalOrder = appraisalOrder.findByProperty(propPK);
    if(appraisalOrder != null){
      addTo( onePropNode, extractApraisalOrder(appraisalOrder) );
    }
    return onePropNode;
  }
  protected Node extractApraisalOrder(AppraisalOrder pOrder) throws Exception
  {
    Node aoNode = doc.createElement("AppraisalOrder");

    addTo(aoNode,"appraisalOrderId", "" + pOrder.getAppraisalOrderId());
    addTo(aoNode,"copyId", "" + pOrder.getCopyId());
    addTo(aoNode,"propertyId", "" + pOrder.getPropertyId());
    addTo(aoNode,"useBorrowerInfo", "" + pOrder.getUseBorrowerInfo());
    addTo(aoNode,"propertyOwnerName", pOrder.getPropertyOwnerName());
    addTo(aoNode,"contactName", pOrder.getContactName());

/* Tracker #1328 - DJ: CR060, phone number formating
    addTo(aoNode,"contactWorkPhoneNum", StringUtil.getAsFormattedPhoneNumber(pOrder.getContactWorkPhoneNum(),null));
    addTo(aoNode,"contactWorkPhoneNumExt", pOrder.getContactWorkPhoneNumExt());
    addTo(aoNode,"contactCellPhoneNum", StringUtil.getAsFormattedPhoneNumber(pOrder.getContactCellPhoneNum(),null));
    addTo(aoNode,"contactHomePhoneNum", StringUtil.getAsFormattedPhoneNumber(pOrder.getContactHomePhoneNum(),null));
*/
    addTo(aoNode, "contactWorkPhoneNum", pFormatPhone(pOrder.getContactWorkPhoneNum()));
    addTo(aoNode, "contactWorkPhoneNumExt", pOrder.getContactWorkPhoneNumExt());
    addTo(aoNode, "contactCellPhoneNum", pFormatPhone(pOrder.getContactCellPhoneNum()));
    addTo(aoNode, "contactHomePhoneNum", pFormatPhone(pOrder.getContactHomePhoneNum()));
/* Tracker #1328 - DJ: CR060, phone number formating */

    addTo(aoNode,"appraisalStatusId","" + pOrder.getAppraisalStatusId());
    // #601
    if (pOrder.getCommentsForAppraiser() != null){
      addTo(aoNode, "commentsForAppraiser", "" + pOrder.getCommentsForAppraiser()); // CR: Mar 11, 04
    }

    return aoNode;
  }

/*
Tracker #1328 - DJ: CR060, phone number formating
   Avoids throwing an exception for phone numbers that cannot be properly formatted
   Applies to an AppraisalOrder Contact node.
*/
  private String pFormatPhone( String sPhoneNum ) throws Exception
  {
    try {
      return StringUtil.getAsFormattedPhoneNumber(sPhoneNum, null);
    }
    catch (Exception e) {
      return sPhoneNum;
    }
  }

  protected void extractEscrowPayments() throws Exception
  {
    double escrowPaymentPropTaxTotal = 0d;
    Collection col = null;
    try{
       col =  deal.getEscrowPayments();
    }catch(Exception exc){
      return;
    }
    if(col == null || col.isEmpty()){
      return;
    }
    Iterator it = col.iterator();
    while( it.hasNext() ){
      EscrowPayment  ep = (EscrowPayment) it.next();
      Node escrowNode = doc.createElement("escrowPayment");
      addTo(escrowNode,"escrowPaymentId", "" + ep.getEscrowPaymentId());
      addTo(escrowNode,"escrowTypeId", "" + ep.getEscrowTypeId());
      addTo(escrowNode,"escrowPaymentAmount",  formatCurrency( ep.getEscrowPaymentAmount() ) );
      addTo( dealElement, escrowNode );
      if(ep.getEscrowTypeId() == 0){
        escrowPaymentPropTaxTotal += ep.getEscrowPaymentAmount();
      }
    }
    if(escrowPaymentPropTaxTotal != 0d){
      addTo(dealAdditionalElement,"propertyTaxEscrow",formatCurrency(escrowPaymentPropTaxTotal));
    }
  }

  protected void extractPropertySection() throws Exception
  {
    Collection coll = deal.getProperties();
    Iterator it = coll.iterator();
    int lCount = 0;
    while( it.hasNext() ){
      Property prop = (Property)it.next();
      lCount++;
      if( prop.isPrimaryProperty() ){
        addTo(dealAdditionalElement, extractPropertyData(prop));
      }
      addTo(dealElement, extractPropertyData(prop));
    }
    addTo(dealAdditionalElement,"totalNumberOfProperties",""+lCount);

  }

  //========================================================
  // we have to do this because our system allows more than
  // on current address per borrower
  //========================================================
  protected int extractMaxMonthsOnCurrentEmpl(Borrower pBor){
    int lMax = 0;
    try{

        Collection lCol = pBor.getEmploymentHistories();
        Iterator it = lCol.iterator();
        EmploymentHistory lHist;
        while( it.hasNext() ){
          lHist = (EmploymentHistory)it.next();
          if(  lHist.getEmploymentHistoryStatusId() == Mc.EMPLOYMENT_HISTORY_STATUS_CURRENT ){
            if(lHist.getMonthsOfService() > lMax){
              lMax = lHist.getMonthsOfService();
            }
          }

        }
    }catch(Exception exc){
      // do nothing here
    }
    return lMax;
  }


  //========================================================
  // we have to do this because our system allows more than
  // on current address per borrower
  //========================================================
  protected int extractMaxMonthsOnCurrentAddr(Borrower pBor){
    int lMax = 0;
    try{

        Collection pCol = pBor.getBorrowerAddresses();
        Iterator it = pCol.iterator();
        BorrowerAddress bAdr;
        while( it.hasNext() ){
          bAdr = (BorrowerAddress)it.next();
          if(  bAdr.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT ){
            if(bAdr.getMonthsAtAddress() > lMax){
              lMax = bAdr.getMonthsAtAddress();
            }
          }

        }
    }catch(Exception exc){
      // do nothing here
    }
    return lMax;
  }

  protected void extractDocumentTrackingRecords() throws Exception
  {
    Collection tracks = deal.getDocumentTracks();
    Iterator it = tracks.iterator();
    while(it.hasNext()){
      DocumentTracking oneTrack = (DocumentTracking) it.next();
      addTo(dealElement,extractDocumentTracking(oneTrack));
    }
  }

  protected Node extractDocumentTracking(DocumentTracking pDt) throws Exception
  {
    Node conditionNode = doc.createElement("Condition");
    Node dtNode = doc.createElement("DocumentTracking");
    ConditionParser p    = new ConditionParser();
    Condition cond = new Condition(srk, pDt.getConditionId());
    if( cond != null){
      addTo(conditionNode,"conditionId","" + cond.getConditionId() );
      addTo(conditionNode,"conditionTypeId","" + cond.getConditionTypeId() );
      addTo(conditionNode,"conditionDesc", cond.getConditionDesc() );
      addTo(conditionNode,"conditionResponsibilityRoleId", "" + cond.getConditionResponsibilityRoleId() );
    }

    addTo(dtNode,"documentTrackingId", "" + pDt.getDocumentTrackingId());
    addTo(dtNode,"documentTrackingSourceId", "" + pDt.getDocumentTrackingSourceId());
    addTo(dtNode,"documentStatusId", "" + pDt.getDocumentStatusId());
    addTo(dtNode,"documentResponsibilityId", "" + pDt.getDocumentResponsibilityId());
    addTo(dtNode,"documentResponsibilityRoleId", "" + pDt.getDocumentResponsibilityRoleId());

    String conditionFullText = pDt.getDocumentTextByLanguage(lang);
    conditionFullText = p.parseText(conditionFullText, lang, deal, srk);
    addTo(dtNode,"text", "" + conditionFullText);

    addTo(dtNode,conditionNode);
    return dtNode;
  }

  protected void extractBorrowerSection() throws Exception
  {
    Collection coll = deal.getBorrowers();
    Iterator it = coll.iterator();
    int lCount = 0;
    while( it.hasNext() ){
      Borrower borr = (Borrower)it.next();
      if(borr.isGuarantor() ) { continue; }
      lCount++;
      if( borr.isPrimaryBorrower() ){
        addTo(dealAdditionalElement, extractBorrowerData(borr));
        addTo(dealAdditionalElement, "primBorMonthsAtCurrentAddress", "" +extractMaxMonthsOnCurrentAddr(borr) );
      }
      notifyListeners(borr,"process_net_worth");
      notifyListeners(borr,"process_assets");
      notifyListeners(borr,"process_liabilities");
      addTo(dealElement, extractBorrowerData(borr));
    }
    addTo(dealAdditionalElement,"totalNumberOfBorrowers",""+lCount);
  }

  protected void appendPrimBorrAddress(BorrowerAddress pBorAddr){
      Element oneAddrNode = doc.createElement("AddressOfPrimaryBorrower");
      addTo(oneAddrNode,"borrowerAddressId",""+pBorAddr.getBorrowerAddressId() );
      addTo(oneAddrNode,"borrowerAddressTypeId",""+pBorAddr.getBorrowerAddressTypeId() );
      try{
        Addr lAddr = pBorAddr.getAddr();
        addTo(oneAddrNode,extractAddressData(lAddr  ));
      }catch(Exception exc){
        addTo(oneAddrNode,extractAddressData( null ));
      }
      addTo(dealAdditionalElement,oneAddrNode);
  }

  protected Node extractBorrowerData(Borrower pBor){
    Element oneBorrNode = doc.createElement("Borrower");
    addTo(oneBorrNode , "borrowerId", ""+pBor.getBorrowerId() );
    addTo(oneBorrNode , "primaryBorrowerFlag", pBor.getPrimaryBorrowerFlag() );
    addTo(oneBorrNode , "borrowerFirstName", pBor.getBorrowerFirstName() );
    addTo(oneBorrNode , "borrowerLastName", pBor.getBorrowerLastName() );
    addTo(oneBorrNode , "borrowerMiddleInitial", pBor.getBorrowerMiddleInitial() );
    addTo(oneBorrNode , "borrowerFaxNumber", pBor.getBorrowerFaxNumber() );

    addTo(  oneBorrNode, "totalIncomeAmount", formatCurrency(pBor.getTotalIncomeAmount()) );
    addTo(  oneBorrNode,  "borrowerBirthDate" , formatDate(  pBor.getBorrowerBirthDate()  ) );

    int dealInstitutionId = srk.getExpressState().getDealInstitutionId();
		String val = BXResources.getPickListDescription(dealInstitutionId, PKL_SALUTATION, pBor.getSalutationId(),lang);
    addTo(oneBorrNode , "salutation", val );

    if( pBor.getSocialInsuranceNumber() != null){
      addTo(oneBorrNode , "socialInsuranceNumber", StringUtil.formatSIN(pBor.getSocialInsuranceNumber(), '-'));
    }

    if(pBor.getBorrowerHomePhoneNumber() != null){
      try{
        addTo(oneBorrNode , "borrowerHomePhoneNumber", StringUtil.getAsFormattedPhoneNumber( pBor.getBorrowerHomePhoneNumber(), null ) );
      } catch(Exception exc){
        addTo(oneBorrNode , "borrowerHomePhoneNumber",  pBor.getBorrowerHomePhoneNumber() );
      }
    }

    if(pBor.getBorrowerWorkPhoneNumber() != null){
      try{
        addTo(oneBorrNode , "borrowerWorkPhoneNumber", StringUtil.getAsFormattedPhoneNumber(pBor.getBorrowerWorkPhoneNumber(), null) );
      }catch(Exception exc){
        addTo(oneBorrNode , "borrowerWorkPhoneNumber", pBor.getBorrowerWorkPhoneNumber() );
      }
    }

    addTo(oneBorrNode,"GDS",  formatRatio( pBor.getGDS() )  );
    addTo(oneBorrNode,"TDS",  formatRatio( pBor.getTDS() )  );
    if(pBor.getBorrowerWorkPhoneExtension() != null){
      addTo(oneBorrNode, "borrowerWorkPhoneExtension",  pBor.getBorrowerWorkPhoneExtension() );
    }

    addTo(oneBorrNode , "salutationId","" + pBor.getSalutationId() );

    val = BXResources.getPickListDescription(dealInstitutionId, PKL_SALUTATION, pBor.getSalutationId(),lang);
    addTo(oneBorrNode , "salutation", val );

    addTo(oneBorrNode , "numberOfDependents","" + pBor.getNumberOfDependents() );

    addTo(oneBorrNode , "maritalStatusId","" + pBor.getMaritalStatusId() );

    val = BXResources.getPickListDescription(dealInstitutionId, PKL_MARITAL_STATUS, pBor.getMaritalStatusId(),lang);
    addTo(oneBorrNode , "maritalStatus", val );

    /*
    if( pBor.getBorrowerGender() != null ){
      val = BXResources.getPickListDescription("Gender", pBor.getBorrowerGender(), lang);
      addTo(oneBorrNode , "borrowerGender", val );
    }
    */
    addTo(oneBorrNode , "borrowerGenderId","" + pBor.getBorrowerGenderId() );

    //val = BXResources.getPickListDescription("Gender", pBor.getBorrowerGender(), lang);
    val = BXResources.getPickListDescription(dealInstitutionId, PKL_LANGUAGE_PREFERENCE, pBor.getLanguagePreferenceId(), lang);
    addTo(oneBorrNode , "languagePreference", val );
    addTo(oneBorrNode , "languagePreferenceId", ""+pBor.getLanguagePreferenceId() );
    addTo(oneBorrNode, "borrowerTypeId",""+pBor.getBorrowerTypeId());
    addTo(oneBorrNode, "disabilityStatusId",""+pBor.getDisabilityStatusId());
    addTo(oneBorrNode, "disPercentCoverageReq",""+formatRatio( pBor.getDisPercentCoverageReq()));  //#DG218
    addTo(oneBorrNode, "lifeStatusId",""+pBor.getLifeStatusId());
    addTo(oneBorrNode,"lifePercentCoverageReq","" + formatRatio( pBor.getLifePercentCoverageReq()));
    extractBorrowerLiabilities(pBor, oneBorrNode);
    extractBorrowerAssets(pBor,oneBorrNode);
    extractEmploymentHistories(pBor, oneBorrNode);
    extractIncomes( pBor, oneBorrNode );

    try{
        Collection pCol = pBor.getBorrowerAddresses();
        Iterator it = pCol.iterator();
        BorrowerAddress bAdr;
        while( it.hasNext() ){
          bAdr = (BorrowerAddress)it.next();
          Node lAddrNode = extractBorrowerAddress(bAdr);
          if( primaryBorrAddressExtracted == false && pBor.isPrimaryBorrower() && bAdr.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT ){
            primaryBorrAddressExtracted = true;
            addTo(dealAdditionalElement,lAddrNode);
          }
        }
        //======================================================================
        it = pCol.iterator();
        while( it.hasNext() ){
          bAdr = (BorrowerAddress) it.next();
          if(bAdr.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT ){
            addTo(oneBorrNode,extractBorrowerAddress(bAdr));
          }
        }
        //======================================================================
        it = pCol.iterator();
        while( it.hasNext() ){
          bAdr = (BorrowerAddress) it.next();
          if(bAdr.getBorrowerAddressTypeId() != Mc.BT_ADDR_TYPE_CURRENT ){
            addTo(oneBorrNode,extractBorrowerAddress(bAdr));
          }
        }
    }catch(Exception exc){
      // do nothing here
    }
    return oneBorrNode;
  }

  protected Node extractIncome(Income pIncome){
    Node rv = doc.createElement("Income");
    if(pIncome == null){
      return rv;
    }
    addTo(rv,"incomeId", ""+pIncome.getIncomeId());
    addTo(rv,"incomeAmount", formatCurrency(pIncome.getAnnualIncomeAmount()));
    addTo(rv,"monthlyIncomeAmount", formatCurrency(pIncome.getMonthlyIncomeAmount()));
    addTo(rv,"incomeTypeId", "" + pIncome.getIncomeTypeId());
    addTo(rv,"incomeDescription", "" + pIncome.getIncomeDescription());

    return rv;
  }

  protected  void extractIncomes(Borrower pBor, Node pNode){

    try{
      Collection lIncomeCol = pBor.getIncomes();
      Iterator lIncomeColIt = lIncomeCol.iterator();
      while(lIncomeColIt.hasNext()){
        Income lIncome = (Income) lIncomeColIt.next();
        //String lVal = PicklistData.getMatchingColumnValue("INCOMETYPE",lIncome.getIncomeTypeId(),"EMPLOYMENTRELATED");
        //if( lVal != null && lVal.equalsIgnoreCase("y") ){
        //  continue;
        //}
        addTo(pNode,extractIncome(lIncome));
      }
    }catch(Exception lHistExc){
      //do nothing here
    }
  }


  protected  void extractEmploymentHistories(Borrower pBor, Node pNode){
    Element oneHistNode ;
    try{
      Collection lHistCol = pBor.getEmploymentHistories();
      Iterator lHistColIt = lHistCol.iterator();
      while(lHistColIt.hasNext()){
        oneHistNode = doc.createElement("EmploymentHistory");
        EmploymentHistory lHist = (EmploymentHistory) lHistColIt.next();
        addTo(oneHistNode, "employmentHistoryId", "" + lHist.getEmploymentHistoryId() );
        addTo(oneHistNode, "employmentHistoryTypeId", "" + lHist.getEmploymentHistoryTypeId() );
        addTo(oneHistNode, "employmentHistoryStatusId", "" + lHist.getEmploymentHistoryStatusId() );
        addTo(oneHistNode, "borrowerId", "" + lHist.getBorrowerId() );
        addTo(oneHistNode, "monthsOfService", "" + lHist.getMonthsOfService() );
        int id = lHist.getJobTitleId();
        addTo(oneHistNode, "jobTitleId", "" + id );
        String val = " ";
        if (id==0) {
          val = lHist.getJobTitle();
        }
        else {
          val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_JOB_TITLE, id,lang);
        }
        addTo(oneHistNode, "jobTitle", val );
        addTo(oneHistNode, "employerName", lHist.getEmployerName() );
        addTo(oneHistNode, extractContactData( lHist.getContact()));
        addTo(oneHistNode, extractIncome(lHist.getIncome()));
        addTo(pNode,oneHistNode);
      }
    }catch(Exception lHistExc){
      //do nothing here
    }
  }

  protected  void extractBorrowerAssets(Borrower pBor, Node pNode){
    Element oneAssetNode ;
    try{
      Collection lAssetCol = pBor.getAssets();
      Iterator lAssetColIt = lAssetCol.iterator();
      while(lAssetColIt.hasNext()){
        oneAssetNode = doc.createElement("Asset");
        Asset lAsset = (Asset) lAssetColIt.next();
        addTo(oneAssetNode, "assetId", "" + lAsset.getAssetId() );
        addTo(oneAssetNode, "assetDescription", lAsset.getAssetDescription() );
        addTo(oneAssetNode, "borrowerId", "" + lAsset.getBorrowerId() );
        addTo(oneAssetNode, "assetValue", "" + formatCurrency(lAsset.getAssetValue() ) );
        addTo(oneAssetNode, "assetTypeId", "" + lAsset.getAssetTypeId() );
        addTo(oneAssetNode, "copyId", "" + lAsset.getCopyId() );
        addTo(pNode,oneAssetNode);
      }
    }catch(Exception lAssetExc){
      //do nothing here
    }
  }

  protected  void extractBorrowerLiabilities(Borrower pBor, Node pNode){
    Element oneLiabNode ;
    try{
      Collection lLiabCol = pBor.getLiabilities();
      Iterator lLiabColIt = lLiabCol.iterator();
      while(lLiabColIt.hasNext()){
        oneLiabNode = doc.createElement("Liability");
        Liability lLiab = (Liability) lLiabColIt.next();
        addTo(oneLiabNode, "liabilityId", "" + lLiab.getLiabilityId() );
        addTo(oneLiabNode, "liabilityTypeId", "" + lLiab.getLiabilityTypeId() );
        addTo(oneLiabNode, "borrowerId", "" + lLiab.getBorrowerId() );
        addTo(oneLiabNode, "liabilityDescription", "" + lLiab.getLiabilityDescription() );
        addTo(oneLiabNode, "liabilityAmount", formatCurrency(lLiab.getLiabilityAmount()) );
        addTo(oneLiabNode, "liabilityMonthlyPayment", formatCurrency(lLiab.getLiabilityMonthlyPayment() ) );
        addTo(pNode,oneLiabNode);
      }
    }catch(Exception lLiabExc){
      //do nothing here
    }
  }

  protected Node extractTimeAtAddress(int pMonths){
    Node rv = doc.createElement("timeAtAddress");
    return rv;
  }

  protected Node extractBorrowerAddress( BorrowerAddress pBorrowerAddress )
  {
    Element oneBorrAddrNode = doc.createElement("BorrowerAddress");
    addTo(oneBorrAddrNode,"borrowerAddressId", ""+pBorrowerAddress.getBorrowerAddressId() );
    addTo(oneBorrAddrNode,"borrowerAddressTypeId",""+pBorrowerAddress.getBorrowerAddressTypeId() );
    addTo(oneBorrAddrNode,"residentialStatusId",""+pBorrowerAddress.getResidentialStatusId() );
    addTo(oneBorrAddrNode,"monthsAtAddress",""+pBorrowerAddress.getMonthsAtAddress() );

    try{
      addTo(oneBorrAddrNode,extractAddressData( pBorrowerAddress.getAddr() ));
    } catch(Exception exc){
      // do nothing here, just return the borrower address node as is
    }

    return oneBorrAddrNode;
  }

  protected Node extractSourceFirmProfile() throws Exception
  {
    Element sfpNode = doc.createElement("SourceFirmProfile");
    SourceFirmProfile sfp = deal.getSourceFirmProfile();
    if(sfp == null){
      return sfpNode;
    }
    addTo(sfpNode,"sourceFirmProfileId",""+sfp.getSourceFirmProfileId());
    if(sfp.getSourceFirmName() != null){
      addTo(sfpNode,"sourceFirmName",sfp.getSourceFirmName());
    }
    addTo(sfpNode,extractContactData(sfp.getContact()));
    return sfpNode;
  }

  protected Node extractSourceOfBusinessProfile() throws Exception
  {
    Element sobNode = doc.createElement("SourceOfBusinessProfile");
    SourceOfBusinessProfile sob = deal.getSourceOfBusinessProfile();
    if(sob == null){
      return sobNode;
    }
    addTo(sobNode,"sourceOfBusinessProfileId",""+sob.getSourceOfBusinessProfileId());
    addTo(sobNode,"sourceOfBusinessCategoryId",String.valueOf(sob.getSourceOfBusinessCategoryId()));		
    if( sob.getAlternativeId() != null ){
      addTo(sobNode,"alternativeId","" + sob.getAlternativeId());
    }
    addTo(sobNode,extractContactData(sob.getContact()));
    return sobNode;
  }

  protected Node extractBranchAddressComplete()
  {
      Element branchNode = doc.createElement("branchProfile");
      //String val = null;

      try
      {
          BranchProfile b = new BranchProfile(srk, deal.getBranchProfileId());
          addTo( branchNode,"branchProfileId","" + b.getBranchProfileId() );
          addTo( branchNode,"branchName",b.getBranchName() );
          if(b.getBPBusinessId() != null){
            addTo( branchNode,"BPBusinessId",b.getBPBusinessId() );
          }
          if( b == null ){ return branchNode; }
          Contact c = b.getContact();
          if( c == null ){return branchNode;}
          addTo(branchNode,extractContactData(c));
      }
      catch (Exception e){
        return branchNode;
      }
      return branchNode;
  }

  protected Node buildGeneralTags(){
    Element generalElement = doc.createElement("General");

    addTo( generalElement, getLanguageTag() );
    addTo( generalElement, "CurrentDate", formatDate(new Date()) );
    addTo( generalElement, "CurrentDateTime", formatDateTime(new Date()) );
    // CR20, begin ---------
    addTo( generalElement, "primaryBorrowerAddress", getPrimaryBorrowerAddrss());
    // CR20, end --------- 
    
    return generalElement;

  }

  // CR20, begin ---------
  protected String getPrimaryBorrowerAddrss(){
	String result = null;
	
    Collection coll;
	try {
		coll = deal.getBorrowers();
	    Iterator it = coll.iterator();
	    while( it.hasNext() ){
	      Borrower borr = (Borrower)it.next();
	      if(! borr.isPrimaryBorrower() ) { continue; }
	
	      Collection pCol = borr.getBorrowerAddresses();
	      Iterator it2 = pCol.iterator();
	      BorrowerAddress bAdr;
	      while( it2.hasNext() ){
	        bAdr = (BorrowerAddress)it2.next();
	        if(  bAdr.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT ){
	            Addr lAddr = bAdr.getAddr();
	
	            if (isNotEmpty(lAddr.getStreetName()) || isNotEmpty(lAddr.getStreetNumber())) {
	            	srk.getSysLogger().debug("DealExtractor.getPrimaryBorrowerAddress(): Parsed address is not empty");
	                result = lAddr.getConcatenatedAddressLine();
	             } else {
	            	srk.getSysLogger().debug("DealExtractor.getPrimaryBorrowerAddress(): Parsed address is blank");
	             }
	          }
	        }
	      }   
    
	} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);		//#DG746 
	}
    return result;
    }  
        // CR20, end --------- 
  
  
  protected Node extractPartyType(String pRootNodeName,int pPartyType){
    Element rootNodeElem = doc.createElement( pRootNodeName );
    Node partyProfileNode = doc.createElement( "partyProfile" );

    Collection col = null;
    try{
      PartyProfile pp = new PartyProfile(srk);
      col = pp.findByDealAndType(new DealPK(deal.getDealId(), 
                                 deal.getCopyId()), pPartyType);
    }catch(Exception exc){
      rootNodeElem.appendChild(partyProfileNode);
      return rootNodeElem;
    }
    if( col== null || col.isEmpty() ){
      rootNodeElem.appendChild(partyProfileNode);
      return rootNodeElem;
    }
    Iterator it = col.iterator();
    while( it.hasNext() ){
      PartyProfile profile = (PartyProfile) it.next();
      addTo(partyProfileNode,"partyProfileId",""+profile.getPartyProfileId());
      addTo(partyProfileNode,"partyCompanyName",profile.getPartyCompanyName());
      addTo(partyProfileNode,"partyName",profile.getPartyName());
      addTo(partyProfileNode,"ptPBusinessId",profile.getPtPBusinessId());
      addTo(partyProfileNode,"ptPShortName",profile.getPtPShortName());
      Contact lContact = null;
      try{
        lContact = profile.getContact();
        if( lContact != null ){
          addTo(partyProfileNode, extractContactData(lContact));
        }
      }catch(Exception exc){
        // do nothing here - we must continue
      }
      addTo(rootNodeElem,partyProfileNode);
    }
    return rootNodeElem;
  }


  protected Node extractUnderwriterData() throws Exception
  {
    Element uwNode = doc.createElement("underwriter");

    UserProfile up = new UserProfile(srk);
    // added instituionProfileId from ExpressState
    up = up.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(), 
                                                   deal.getInstitutionProfileId()));
    addTo( uwNode, "userTypeId", "" + up.getUserTypeId() );
    addTo( uwNode, "userType", BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_USER_TYPE, up.getUserTypeId(),lang));

    if(up == null){ return uwNode;  }

    Contact c = up.getContact();
    if(c == null){
      return uwNode;
    }

    addTo(uwNode,extractContactData(c));

    return uwNode;
  }

  protected Node getLanguageTag()
  {
      Element elem = null;

      elem = doc.createElement("LanguageUsed");
      switch (lang)
      {
          case Mc.LANGUAGE_PREFERENCE_ENGLISH:{
            elem.appendChild(doc.createTextNode("English"));
            break;
          }
          case Mc.LANGUAGE_PREFERENCE_FRENCH:{
            elem.appendChild(doc.createTextNode("French"));
            break;
          }
          case Mc.LANGUAGE_PREFERENCE_SPANISH:{
            elem.appendChild(doc.createTextNode("English"));
            break;
          }
          default:{
            elem.appendChild(doc.createTextNode("English"));
          }
      }

      return elem;
  }
    /**
     * Replaces various characters that would break an XML string with their
     * escaped counterparts.  These characters include &, < and >.
     *
     * @param pStr the string to replace any illegal characters in.
     *
     * @return the XML-safe string.
     */
    public static String makeSafeXMLString(String pStr)
    {
      /*  #DG746 harmonize code
        
        if (pStr == null)
           return pStr;
        int ind;
        StringBuffer retBuf = new StringBuffer("");
        ind = pStr.indexOf("&");
        if (ind == -1)
           ind = pStr.indexOf("<");
        if (ind == -1)
           ind = pStr.indexOf(">");
        while( ind != -1 )        {
            retBuf.append(pStr.substring(0,ind));
            if (pStr.charAt(ind) == '&')
               retBuf.append( "&amp;" );
            else if (pStr.charAt(ind) == '<')
               retBuf.append( "&lt;" );
            else if (pStr.charAt(ind) == '>')
               retBuf.append( "&gt;" );
            pStr = pStr.substring(ind + 1);
            ind = pStr.indexOf("&");
            if (ind == -1)
               ind = pStr.indexOf("<");
            if (ind == -1)
               ind = pStr.indexOf(">");
        }
        retBuf.append(pStr); */        
      	return StringUtil.makeSafeXMLString(pStr);        
    }
    
    /**
     * Utility method that adds <code>ins</code> to <code>inl</code>
     * only if both are not <code>null</code>.
     *
     * @param inl the list to add the string to.
     * @param ins the string to add to the list.
     *
     * @return whether the addition was successful.
     */
    protected boolean addTo(List<String> inl, String ins)
    {
        if (!isEmpty(ins) && inl != null)
        {
           inl.add(ins);
           return true;
        }

        return false;
    }

    /**
     * Utility method that adds <code>childNode</code> to <code>parentNode</code>
     * only if both are not <code>null</code>.
     *
     * @param parentNode the parent node to add the child to.
     * @param childNode the child node to be added.
     *
     * @return whether the addition was successful.
     */
    protected boolean addTo(Node parentNode, Node childNode)
    {
        if(childNode == null){ return false; }
        try
        {
            parentNode.appendChild(childNode);
            return true;
        }
        catch (Exception e) {}

        return false;
    }

    /**
     * Utility method that adds a new <code>Node</code> with a
     * name of <code>tagName</code> and data of <code>data</code>.
     *
     * @param parentNode the parent node to add the new Node to.
     * @param tagName the name of the child node to be added.
     * @param data the child node text to be added.
     *
     * @return whether the addition was successful.
     */
    protected boolean addTo(Node parentNode, String tagName, String data)
    {
        try
        {
            if (parentNode != null && !isEmpty(tagName) && !isEmpty(data))
            {
                data = data.trim();

                parentNode.appendChild(getTag(tagName, data));
                return true;
            }
        }
        catch (Exception e) {}

        return false;
    }

    /**
     * Utility method that returns a new <code>Node</code> with data of <code>data</code>
     * and a name of <code>tagName</code>.
     *
     * @param tagName the name of the child node to be added.
     * @param data the child node text to be added.
     *
     * @return the new <code>Node</code> or <code>null</code> if an error is encountered.
     */
    protected Node getTag(String tagName, String data)
    {
        //  DEBUG
        //  This ensures that SOMETHING gets put in the field.  This is useful
        //  for debugging XML data source.
        //if (data == null || data.trim().length() == 0)
           //data = "ABC123";

        try
        {
            if (!isEmpty(tagName) && !isEmpty(data))
            {
                data = data.trim();

                Node elemNode = doc.createElement(tagName);
                elemNode.appendChild(doc.createTextNode(data));

                return elemNode;
            }
        }
        catch (Exception e) {
        	logger.debug(e.getStackTrace());
        	logger.debug("failed to getTag: tagName ==> " + tagName + " data ==> " + data );
        }

        return null;
    }

    protected boolean isEmpty(String str)
    {
        return (str == null || str.trim().length() == 0);
    }

    protected boolean isNotEmpty(String pStr){
      if( pStr == null ){return false;}
      if( pStr.trim().length() == 0){return false;}
      return true;
    }
    /**
     * Utility method that formats <code>date</code> using the current doc language.
     * settings.
     *
     * @param date the date to format into the relevant date style
     *
     * @return The formatted date.
     */
    protected String formatDate(Date date)
    {
        try
        {
            return DocPrepLanguage.getInstance().getFormattedDate(date, lang);
        }
        catch (Exception e) {}

        return null;
    }
    /**
     * Utility method that formats <code>amt</code> using the current doc language
     * settings.
     *
     * @param amt number to format into the relevant currency style
     *
     * @return The formatted number.
     */
    protected String formatCurrency(double amt)
    {
        return DocPrepLanguage.getInstance().getFormattedCurrency(amt, lang);
    }

    /**
     * Utility method that formats <code>rate</code> using the current doc language.
     * settings.
     *
     * @param rate number to format into the relevant rate style
     *
     * @return The formatted number.
     */
    protected String formatInterestRate(double rate)
    {
        return DocPrepLanguage.getInstance().getFormattedInterestRate(rate, lang);
    }

    /**
     * Utility method that formats <code>rate</code> using the current doc language.
     * settings.
     *
     * @param rate number to format into the relevant rate style
     *
     * @return The formatted number.
     */
    protected String formatRatio(double rate)
    {
        return DocPrepLanguage.getInstance().getFormattedRatio(rate, lang);
    }

  protected Node extractContactData(Contact pContact)throws Exception
  {
    Element contactNode = doc.createElement("Contact");
    if(pContact == null){
      return contactNode;
    }
    Addr addr = pContact.getAddr();
    if(addr == null){
      return contactNode;
    }
    addTo(contactNode,extractAddressData(addr));
    String str = null;
    try{
      str = StringUtil.getAsFormattedPhoneNumber(pContact.getContactFaxNumber(), null);
    }catch(Exception exc){
      str = null;
    }
    if(str == null){
      addTo(contactNode,"contactFaxNumber", "" );
    }else{
      addTo(contactNode,"contactFaxNumber", str );
    }

    try {
      str = pContact.getContactEmailAddress();
    }catch (Exception exc){
      str = null;
    }
    if (str==null){
      addTo(contactNode, "contactEmailAddress","");
    } else {
      addTo(contactNode, "contactEmailAddress", str);
    }

    try{
      str = pContact.getContactPhoneNumberExtension();
    }catch(Exception exc){
      str = null;
    }
    if(str == null){
      addTo(contactNode,"contactPhoneNumberExtension", "" );
    }else{
      addTo(contactNode,"contactPhoneNumberExtension", str );
    }

    try{
      str = StringUtil.getAsFormattedPhoneNumber(pContact.getContactPhoneNumber(), null);
    }catch(Exception exc){
      str = null;
    }
    if(str == null){
      addTo(contactNode,"contactPhoneNumber", "" );
    }else{
      addTo(contactNode,"contactPhoneNumber", str );
    }

    addTo(contactNode,"contactFirstName", pContact.getContactFirstName() );
    addTo(contactNode,"contactMiddleInitial", pContact.getContactMiddleInitial() );
    addTo(contactNode,"contactLastName", pContact.getContactLastName() );
    addTo(contactNode,"salutationId", "" + pContact.getSalutationId() );

    String val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_SALUTATION, pContact.getSalutationId(),lang);
    addTo(contactNode,"salutation", val );

    return contactNode;
  }

  protected Node extractAddressData(Addr pAddr){
    Element addrNode = doc.createElement("Address");
    if ( pAddr != null )
    {
        String val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), PKL_PROVINCE, pAddr.getProvinceId(),lang);
        addTo(addrNode,"province",val);
        
        if(isNotEmpty(pAddr.getAddressLine1())){
          addTo(addrNode,"addressLine1",pAddr.getAddressLine1());
        }
        if(isNotEmpty(pAddr.getAddressLine2())){
          addTo(addrNode,"addressLine2",pAddr.getAddressLine2());
        }
        if(isNotEmpty(pAddr.getCity())){
          addTo(addrNode,"city",pAddr.getCity());
        }
        if(isNotEmpty(pAddr.getPostalFSA())){
          addTo(addrNode,"postalFSA",pAddr.getPostalFSA());
        }
        if(isNotEmpty(pAddr.getPostalFSA())){
          addTo(addrNode,"postalLDU",pAddr.getPostalLDU());
        }
    }
    return addrNode;
  }

  protected String convertMonthsToYearsAndMonths(int pNum){
    String sYrs = BXResources.getGenericMsg(YEARS_SHORT, lang);
    String sMnt = BXResources.getGenericMsg(MONTHS_SHORT, lang);

    double lNum;
    lNum = pNum / 12.0;

    int lInt = (new Double(lNum)).intValue();
    if( (lNum - lInt ) == 0.0 ){
      return "" + lInt + " " + sYrs + " 0 " + sMnt;
    }

    return "" + lInt + " " + sYrs + " " + ( pNum - (lInt * 12) ) + " " + sMnt;
  }


  protected void addActionListener(java.awt.event.ActionListener pListener){
    actionListeners.addElement(pListener);
  }
  protected void notifyListeners(Object pObj, String pCommand){
    Iterator it = actionListeners.iterator();
    while(it.hasNext()){
      ((ActionListener)it.next()).actionPerformed(new java.awt.event.ActionEvent(pObj,88,pCommand));
    }
  }

  /**
   * Utility method that formats <code>date</code> using the current doc language.
   * settings.
   *
   * @param date the date to format into the relevant date style
   *
   * @return The formatted date.
   */
  protected String formatDateTime(Date date)
  {
      try
      {
          return DocPrepLanguage.getInstance().getFormattedDateTime(date, lang);
      }
      catch (Exception e) {}

      return null;
  }

  protected Element createSubTree(Document owner, String elementName,
                                  SessionResourceKit srk, DealEntity ctx,
                                  boolean picklistDesc,boolean recurse,
                                  int pLang, boolean pAllowEmpty)
  {
    Element top = null;

    try
    {
      if(ctx == null || owner == null) return null;

      if( ctx instanceof com.basis100.deal.entity.Property ){
        notifyListeners(ctx,"primary_property_event");
      }
      if(elementName == null || elementName.trim().length() == 0)
      {
        elementName = ctx.getEntityTableName();
      }

      top = owner.createElement(elementName);

      List children = null;
      if(recurse)
      {
        children = extractChildren(ctx);
      }

      Class ctxClass = ctx.getClass();
      Field[] fls = ctxClass.getDeclaredFields();
      int len = fls.length;
      String value = null;
      String fname = null;
      Element current = null;
      Object obj;

      for(int i = 0; i < len; i++)
      {
    	//FXP27162
    	//If the field has not a 'protected' modifier, then ignore the field.
    	if(!Modifier.isProtected(fls[i].getModifiers())) continue;
    	//End of FXP27162

        fname = fls[i].getName();
        if( !pAllowEmpty ){continue;}
        obj = ctx.getFieldValue( fls[i].getName() );

        if(obj instanceof Double){
          if( ratioFieldList.isRatioField(ctxClass.getName() ,  fls[i].getName() ) ){
            value = formatInterestRate( ((Double)obj).doubleValue() );
          }else{
            value = formatCurrency( ((Double)obj).doubleValue());
          }
        }else if( obj instanceof java.sql.Date ){
          value = formatDate((java.sql.Date)obj);
        }else if( obj instanceof java.util.Date ){
          value = formatDate((java.util.Date)obj);
        }else{
          value = ctx.getStringValue(fname);
        }
        if( value == null){ value = ""; }

        //boolean isPicklistType = false;
        if(picklistDesc)
        {
          if(fname.toUpperCase().endsWith("ID"))
          {

            String table = fname.substring(0,fname.length() - 2);
            String desc = null;
            // FXLINK:PHASEII: modification request is to show id fields in the map
            if(PicklistData.containsTable(table)){
              current = createElement(owner, fname, value);
              top.appendChild(current);

              try{
                desc = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), table,value, pLang);
              } catch(Exception exc){
                srk.getSysLogger().error("Missing resource bundle: table = " + table + "value = " + value + "lang = " + pLang);
                desc = PicklistData.getDescription(table,value);
              }

            }else{
              desc = PicklistData.getDescription(table,value);
            }

            if(desc != null)
            {
              if(desc.trim().length() == 0)
               continue;

              value = desc;
              fname = table;
            }
          }
        }
        current = createElement(owner, fname, value);
        if(current != null)
         top.appendChild(current);
      }  // end for

      //==========================================
      // now deal with children elements
      //==========================================
      DealEntity appEnt = null;
      if(children != null && !children.isEmpty())
      {
        Iterator chit  =  children.iterator();
        while(chit.hasNext())
        {
          appEnt = (DealEntity)chit.next();
          Element stree = createSubTree(owner, null, srk,appEnt,picklistDesc,recurse, pLang, pAllowEmpty);
          if(stree != null)
           top.appendChild(stree);
        }
      }

    }
    catch(Exception e)
    {
      e.printStackTrace();
      srk.getSysLogger().error("ElementBuilder : an error has occurred during element creation");
      srk.getSysLogger().error("ElementBuilder : error msg (null if unknown):" + e);		//#DG746
    }

    return top;
  }

  private Element createElement(Document owner, String name, String data)
  {
    Element ret = createElement(owner, name);

    if(data == null || ret == null) return ret;

    DomUtil.appendString(ret, data);

    return ret;
  }

  private Element createElement(Document owner, String name)
  {
    if(owner == null || name == null) return null;

    return owner.createElement(name);
  }

  private List extractChildren(DealEntity pEnt) throws Exception
  {
    Contact lContact = null;
    Income lIncome = null;
    Addr lAddr = null;

    List rv = new ArrayList();
    if(pEnt instanceof com.basis100.deal.entity.Deal){
      Deal d = (Deal)pEnt;
      rv.addAll( d.getProperties() );
      rv.addAll( d.getBorrowers() );
      rv.addAll( d.getDocumentTracks());
      rv.addAll( d.getEscrowPayments());
      rv.addAll( d.getDownPaymentSources());
      // deal fees will be extracted later ------------ CR
      // rv.addAll( d.getDealFees());
      rv.addAll( d.getPartyProfiles() );
      rv.addAll( d.getCreditBureauReports() );

      SourceFirmProfile sfp = d.getSourceFirmProfile();
      if( sfp != null ){
        rv.add( sfp );
      }
      SourceOfBusinessProfile sbp = d.getSourceOfBusinessProfile();
      if( sbp != null ){
        rv.add( sbp );
      }
      rv.addAll( d.getDealNotes() );

      BranchProfile branchProfile = new BranchProfile(srk, d.getBranchProfileId());
      if( branchProfile != null ){ rv.add(branchProfile); }

      // CR, 25-jun-04 -- preventing duplicate UserProfile nodes for Cervus docprep xml
      int aid = deal.getAdministratorId();
      int uid = deal.getUnderwriterUserId();
      int fid = deal.getFunderProfileId();
      
      // added instituionProfileId for UserProfileBeanPk for ML
      int iid = deal.getInstitutionProfileId();

      UserProfile adminProfile = new UserProfile(srk);
      adminProfile = adminProfile.findByPrimaryKey(new UserProfileBeanPK(aid, iid));
      if(adminProfile != null){
        rv.add(adminProfile);
      }

      UserProfile lUnderwriterProfile = new UserProfile(srk);
      lUnderwriterProfile = lUnderwriterProfile
                     .findByPrimaryKey(new UserProfileBeanPK(uid, iid));
      if((lUnderwriterProfile != null)){
        rv.add(lUnderwriterProfile);
      }

      if (fid != uid){
        UserProfile lFunderProfile = new UserProfile(srk);
        // lFunderProfile = lUnderwriterProfile.findByPrimaryKey(new UserProfileBeanPK(fid));
        lFunderProfile = lFunderProfile
                       .findByPrimaryKey(new UserProfileBeanPK(fid, iid));
        if(lFunderProfile != null){
          rv.add(lFunderProfile);
        }
      }

      DealFee dealFee = new DealFee(srk,null);
      Collection col = dealFee.findByDeal(new DealPK(deal.getDealId(), deal.getCopyId()));
      rv.addAll(col);

      LenderProfile lp = deal.getLenderProfile();
      if(lp != null){
        rv.add(lp);
      }
      MtgProd mtgProd = deal.getMtgProd();
      if(mtgProd != null){
        rv.add(mtgProd);
      }
    }else if(pEnt instanceof com.basis100.deal.entity.Borrower){
      Borrower b = (Borrower)pEnt;
      rv.addAll( b.getBorrowerAddresses());
      rv.addAll( b.getIncomes());
      rv.addAll( b.getAssets());
      rv.addAll( b.getLiabilities());
      rv.addAll( b.getCreditReferences());
      rv.addAll( b.getEmploymentHistories());
      rv.addAll( b.getLifeDisabilityPremiums());
    }else if( pEnt instanceof com.basis100.deal.entity.Property ){
      Property p = (Property)pEnt;
      rv.addAll( p.getPropertyExpenses());

      AppraisalOrder theAO = p.getAppraisalOrder();
      if(theAO != null){ rv.add(theAO); }
    }else if( pEnt instanceof com.basis100.deal.entity.PartyProfile ){
      lContact = ((PartyProfile)pEnt).getContact() ;
      if(lContact != null){ rv.add( lContact ); }
    }else if( pEnt instanceof com.basis100.deal.entity.Contact ){
      lAddr = ((Contact)pEnt).getAddr();
      if(lAddr != null){ rv.add(  lAddr);      }
    }else if( pEnt instanceof com.basis100.deal.entity.EmploymentHistory ){
      lContact = ((EmploymentHistory)pEnt).getContact();
      if(lContact != null ){rv.add( lContact );}
      lIncome = ((EmploymentHistory)pEnt).getIncome();
      if(lIncome != null){ rv.add( lIncome );}
    }else if( pEnt instanceof com.basis100.deal.entity.BorrowerAddress ){
      lAddr = ((BorrowerAddress)pEnt).getAddr();
      if( lAddr != null ){ rv.add( lAddr );}
    }else if( pEnt instanceof com.basis100.deal.entity.BranchProfile ){
      lContact = ((BranchProfile)pEnt).getContact();
      if(lContact != null){rv.add( lContact );}
    }else if( pEnt instanceof com.basis100.deal.entity.SourceFirmProfile ){
      lContact = ((SourceFirmProfile)pEnt).getContact();
      if(lContact != null){rv.add( lContact );}
    }else if( pEnt instanceof com.basis100.deal.entity.SourceOfBusinessProfile ){
      lContact = ((SourceOfBusinessProfile)pEnt).getContact();
      if(lContact != null){rv.add( lContact );}
    }else if( pEnt instanceof com.basis100.deal.entity.UserProfile ){
      lContact = ((UserProfile)pEnt).getContact();
      if(lContact != null){rv.add( lContact );}
    }else if( pEnt instanceof com.basis100.deal.entity.DealFee ){
      Fee fee = ((DealFee)pEnt).getFee();
      if(fee != null){rv.add(fee);}
    }
    return rv;
  }

  protected Node convertCRToLines(Node root, String pStr)
  {
  	//#DG746 return convertCRToLines(doc, root, pStr, "�\n");
  	// added the equivalent of paragraph char when converted
  	return convertCRToLines(doc, root, pStr, "\u0153\u00b6\n");
  }

  protected static Node convertCRToLines(Document pDoc, Node root, String pStr, String returnChar)
  {
      if (root == null || pDoc == null || pStr == null)
         return root;

      StringTokenizer st = new StringTokenizer(pStr, returnChar);
      Node child;

      while (st.hasMoreTokens())
      {
          child = pDoc.createElement("Line");
          child.appendChild(pDoc.createTextNode(st.nextToken().trim()));
          root.appendChild(child);
      }

      return root;
  }

  public void actionPerformed(ActionEvent e) {
  }
}

// =============================================================================
// list of created nodes
// =============================================================================
