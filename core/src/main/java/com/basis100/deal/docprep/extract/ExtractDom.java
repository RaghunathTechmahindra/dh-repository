package com.basis100.deal.docprep.extract;

import org.w3c.dom.*;
import MosSystem.Mc;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.deal.docprep.*;
import com.basis100.xml.*;
import com.basis100.deal.entity.DocumentProfile;

import java.io.*;
import java.util.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;

public class ExtractDom
{
  private File description;
  private Document doc;

  private String codePackage;

  public ExtractDom(File desc) throws DocPrepException
  {
     description = desc;
     init();
  }
  
  public void init()throws DocPrepException
  {
    if(description == null || !description.exists())
    {
      String msg = "Schema File not found: " + description.getAbsolutePath();
      throw new DocPrepException(msg);
    }

    try
    {
      DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();
      doc = parser.parse(description);
    }
    catch(Exception e)
    {
      String msg = "DocProp: XML File Parse Error - File: " + description.getName();

      if(e.getMessage() != null)
       msg += " Reason: " + e.getMessage();

      throw new DocPrepException(msg);
    }

  }

  public Document getDocument()
  {
    return this.doc;
  }

  public String getCodePackage()
  {
    if(this.codePackage != null)
     return codePackage;

    Node cp = DomUtil.getFirstNamedDescendant(doc, "CodePackage");

    if(cp == null) return null;

    codePackage = cp.getFirstChild().getNodeValue();

    if(codePackage != null && codePackage.endsWith("."))
     codePackage = codePackage.substring(0,codePackage.length()-1);

    return codePackage;

  }

  public String getPreferredLangAttrib()
  {
    Node n = doc.getDocumentElement();
    return DomUtil.getAttributeValue(n,"lang");
  }

}
