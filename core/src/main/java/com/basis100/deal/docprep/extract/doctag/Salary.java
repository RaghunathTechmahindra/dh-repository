package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class Salary  extends DocumentTagExtractor
{
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());

        FMLQueryResult fml = new FMLQueryResult();

        //int clsid = de.getClassId();

        try
        {
            /*if (clsid == ClassId.BORROWER)
            {
                Borrower borrower = (Borrower)de;
                EmploymentHistory eh = new EmploymentHistory(srk);
                eh = eh.findByCurrentEmployment(new BorrowerPK(borrower.getBorrowerId(),
                                                               borrower.getCopyId()));
                double amt = eh.getIncome().getIncomeAmount();
                fml.addCurrency(amt);
            }else if(clsid == ClassId.EMPLOYMENTHISTORY){
                EmploymentHistory employmentHistory = (EmploymentHistory) de;
                double amount = employmentHistory.getIncome().getIncomeAmount();
                fml.addCurrency(amount);
            }*/
        	
			Borrower borrower;
			EmploymentHistory eh;
			double amount = 0;
			if (de instanceof Deal) {
				Deal deal = (Deal) de;
				borrower = new Borrower(srk, null);
				borrower.setSilentMode(true);
				borrower.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
				de = borrower;
			}
			if (de instanceof Borrower) {
				borrower = (Borrower) de;
				eh = new EmploymentHistory(srk);
				eh.setSilentMode(true);

				try {
					Collection ehs = eh.findByCurrentEmployments((BorrowerPK) borrower.getPk());
					for (Iterator iterator = ehs.iterator(); iterator.hasNext();) {
						eh = (EmploymentHistory) iterator.next();
						amount += eh.getIncome().getIncomeAmount();
					}
					if(amount > 0)
						fml.addCurrency(amount);
					else
						srk.getSysLogger().trace("useless employment/income amount found:" + amount);
				} 
				catch (Exception e) {
					srk.getSysLogger().trace("no current employment found:" + e);
				}
			} 
			else if (de instanceof EmploymentHistory) {
				eh = (EmploymentHistory) de;

				amount = eh.getIncome().getIncomeAmount();
				fml.addCurrency(amount);
			} 
			else
				srk.getSysLogger().warning("tag not applicable to context:" + de.getClass());
        }
        catch (Exception e)
        {
            srk.getSysLogger().error(e.getMessage());
            srk.getSysLogger().error(e);
            throw new ExtractException("Unable to get DocumentTracking for : "  + this.getClass().getName());
        }

        return fml;
    }

}