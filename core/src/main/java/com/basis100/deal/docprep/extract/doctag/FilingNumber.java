package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.calc.*;
import MosSystem.Mc;

public class FilingNumber extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
        Deal d = (Deal)de;
        Property prim = new Property(srk,null);
        prim.findByPrimaryProperty(d.getDealId(),
            d.getCopyId(), d.getInstitutionProfileId());

        int prov = prim.getProvinceId();

        int mid = d.getMIIndicatorId();

        int lender = d.getLenderProfileId();

        ChargeTerm ct = new ChargeTerm(srk);

         if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
              && d.getMortgageInsurerId() == Mc.MI_INSURER_CMHC)
         {
            ct.findFirstByLenderProvinceAndQualifier(lender,prov,"CMHC");
            fml.addValue(ct.getChargeTermFillingNumber(), fml.STRING);
         }
         else
         {
            ct.findFirstByLenderProvinceAndQualifier(lender,prov,"Conventional");
            fml.addValue(ct.getChargeTermFillingNumber(), fml.STRING);
         }

      }
    }
    catch (FinderException fe)
    {
       return fml;
    }
    catch (Exception e)
    {
       String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }
}
