package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;
import com.basis100.deal.calc.*;
import com.basis100.resources.*;
import java.util.*;
import com.basis100.entity.*;


public class ContactPhone extends DocumentTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.CONTACT)
      {
        Contact c = (Contact)de;
        String phone =
                StringUtil.getAsFormattedPhoneNumber(
                                  c.getContactPhoneNumber(),
                                        c.getContactPhoneNumberExtension());

        fml.addValue(phone, fml.STRING);

      }
      else if(clsid == ClassId.PARTYPROFILE)
      {
        PartyProfile pp = (PartyProfile)de;
        int contid = pp.getContactId();

        Contact c = new Contact(srk);
        c = c.findByPrimaryKey(new ContactPK(contid, 1));

        return this.extract(c, lang, format, srk);

      }
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(e);
      throw new ExtractException("ERROR: Unable to extract Contact first or last name");
    }

    return fml;
  }
}
