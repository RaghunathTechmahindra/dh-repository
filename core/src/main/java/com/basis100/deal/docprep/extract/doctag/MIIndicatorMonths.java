package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;

import MosSystem.Mc;


public class MIIndicatorMonths extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    debug("entityCtx -> " + de.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    EmploymentHistory eh = (EmploymentHistory)de;

    int copyId = eh.getCopyId();

    try
    {
      Deal deal = new Deal(srk, null, (new Borrower(srk, null, eh.getBorrowerId(), copyId)).getDealId(), copyId);

       int mid = deal.getMIIndicatorId();

       if(mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED)
       {
          fml.addValue("3", fml.STRING);
       }
       else
       {
          fml.addValue("2", fml.STRING);
       }
     return fml;
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }


  }
}
