package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.calc.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.deal.conditions.*;
import MosSystem.Mc;

public class LoanAmountVerb extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    FMLQueryResult fml = new FMLQueryResult();

    String val = "";
    int clsid = de.getClassId();

    try
    {
      if(clsid == ClassId.DEAL)
      {
         Deal deal = (Deal)de;
         int id = deal.getDealPurposeId();
         int mindid = deal.getMIIndicatorId();

         if( id == Mc.DEAL_PURPOSE_PORTABLE  || id == Mc.DEAL_PURPOSE_PORTABLE_DECREASE)
         {
            ConditionHandler ch = new ConditionHandler(srk);
            val = ch.getVariableVerbiage(deal, Dc.PROJECTED_BALANCE_VERB,lang);
         }
//==============================================================================
// PROGRAMMERS NOTE:  The following section of code has been removed because
// the specification has changed.  Look also at LoanAmount class.
//==============================================================================
/*
         else if( id == Mc.DEAL_PURPOSE_PORTABLE_INCREASE || id == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
         {
            ;//do nothing
         }
*/
         else
         {
           if((mindid == Mc.MII_REQUIRED_STD_GUIDELINES || mindid == Mc.MII_UW_REQUIRED)
                && deal.getMIUpfront().equals("N"))
           {
              ConditionHandler ch = new ConditionHandler(srk);
              val = ch.getVariableVerbiage(deal,Dc.BASE_LOAN_AMOUNT,lang);
           }
           else
           {
             ConditionHandler ch = new ConditionHandler(srk);
             val = ch.getVariableVerbiage(deal,Dc.LOAN_AMOUNT,lang);
           }
         }
       }
       else if(clsid == ClassId.BRIDGE)
       {
        
       }

      fml.addValue(val,fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch (Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg = "DOCPREP ERROR: Unable to extract tag data: " + this.getClass().getName() + " msg: " + msg;

      srk.getSysLogger().error(msg);
      throw new ExtractException(msg);
    }

    return fml;
  }



}
