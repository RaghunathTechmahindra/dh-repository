package com.basis100.deal.docprep.extract.doctag;

/**
* 03/Mar/2005 DVG #DG158 #922  New tags required,
         2 new classes created as new conditions inheriting from the original ones
 */

import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.docprep.extract.ExtractException;

/**
 * <p>Title: FXpressJ2EE</p>
 * <p>Description: FXpress NetDynamics project converted to the J2EE realm with the JATO as an iPlanet Application Framework.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Filogix</p>
 * @since 03/Mar/2005
 * @author Denis V. Gadelha
 * @version 1.0
 */

public class DebtsPriorToAdvanceNRent extends DebtsPriorToAdvance {
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)
      throws ExtractException {
    String lsv = format.getVersion();
    format.setVersion("@@NORENT@@");  // just a fancy unique id
    FMLQueryResult lo = null;
    try {
      lo = super.extract(de, lang, format, srk);
    }
    finally {
      format.setVersion(lsv);
    }
    return lo;
  }
}
