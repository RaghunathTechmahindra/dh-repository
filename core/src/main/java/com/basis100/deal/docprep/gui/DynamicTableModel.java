package com.basis100.deal.docprep.gui;


import javax.swing.table.AbstractTableModel;
import java.util.*;

public class DynamicTableModel extends AbstractTableModel
{
  Vector colNames = new Vector();
  Vector colVect;
  Vector vectorTableData;
  private boolean DEBUG = false;

  public DynamicTableModel(Vector colVect, Vector dataVect)
  {
     this.vectorTableData = dataVect;
     this.colNames = colVect;
  }

   public int getColumnCount() {
    return colNames.size();
   }

   public int getRowCount() {
     return vectorTableData.size();
   }

   public String getColumnName(int col) {
     // System.out.println("Column Name " + col + " = " + (String) colNames.elementAt(col)); // debug
     return (String) colNames.elementAt(col);
   }

   public Object getValueAt(int row, int col) {
     return ((Vector)((Vector)vectorTableData.elementAt(row))).elementAt(col);
   }

   /*
   * JTable uses this method to determine the default renderer/
   * editor for each cell.  If we didn't implement this method,
   * then the last column would contain text ("true"/"false"),
   * rather than a check box.
   */
   public Class getColumnClass(int c) {
      return getValueAt(0, c).getClass();
   }

   /*
   * Don't need to implement this method unless your table's
   * editable.
   */
   public boolean isCellEditable(int row, int col) {
      //Note that the data/cell address is constant,
      //no matter where the cell appears onscreen.
      return false;
      /*
      if (col < 1) {
          return false;
      } else {
          return true;
      }
      */
   }

   public void setValueAt(Object value, int row, int col) {
     Object lObj = ((Vector)vectorTableData.elementAt(0)).elementAt(col);
     if(lObj instanceof Integer && !(value instanceof Integer)){
      ((Vector)vectorTableData.elementAt(row)).setElementAt(new Integer(   value.toString()  ),col);
     } else{
      ((Vector)vectorTableData.elementAt(row)).setElementAt(value,col);
     }
     this.fireTableCellUpdated(row,col);
   }

   public void addRow(Vector someVector){
     vectorTableData.addElement(someVector);
     if(vectorTableData.isEmpty()){
       System.out.println("adding row -1");
       fireTableRowsInserted(0,0);
     } else{
       System.out.println("adding row -2");
       fireTableRowsInserted(vectorTableData.size()-1,vectorTableData.size()-1);
      }
   }

   public void removeRow(int row){
     if(  row < 0 || row > (vectorTableData.size()-1) ){
       return;
     }
     vectorTableData.removeElementAt(row);
     fireTableRowsDeleted(row,row);
   }

   public void storeNewData(Vector dataV){
     vectorTableData = new Vector();
     for(int i=0; i<dataV.size(); i++){
       vectorTableData.addElement((Vector) dataV.elementAt(i));
     }
     fireTableDataChanged();
   }

   public void clearData(){
     vectorTableData = new Vector();
     fireTableDataChanged();
  }
}

