package com.basis100.deal.docprep.extract;

import com.basis100.deal.xml.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.util.*;

public class DocumentContext
{
  private DocumentProfile profile;
  private Deal deal;
  private DocumentFormat format;

  public DocumentContext(DocumentProfile profile, Deal deal)throws UnsupportedFormatException
  {
    this.profile = profile;
    this.deal = deal;

    if(profile != null)   //temp measure for use in tests
      this.format = new DocumentFormat(profile);
  }

  public DocumentProfile getDocumentProfile()
  {
    return profile;
  }

  public Deal getDeal()
  {
    return this.deal;
  }

  public DocumentFormat getFormat()
  {
    return this.format;
  }
}