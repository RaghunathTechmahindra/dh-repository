package com.basis100.deal.docprep.rtf;

import java.util.*;

public class ConditionalActionList {
  private Hashtable varList;

  public ConditionalActionList() {
    varList = new Hashtable();
  }

  public void addNewAction(ConditionalAction actionToAdd){
    if(varList.containsKey(actionToAdd.getVarName())){
      ((Vector)varList.get(actionToAdd.getVarName())).addElement(actionToAdd);
    } else {
      Vector actVect = new Vector();
      actVect.addElement(actionToAdd);
      varList.put(actionToAdd.getVarName(), actVect);
    }
  }

  public Vector getAllActions(String pVarName){
    String tmpVar = pVarName.trim().toLowerCase();
    if(varList.containsKey(tmpVar)){
      return (Vector)varList.get(tmpVar);
    }
    return null;
  }

  public boolean isActionConditional(String pVarName){
    if(varList.containsKey(pVarName.trim().toLowerCase())){
      return true;
    }
    return false;
  }

} 