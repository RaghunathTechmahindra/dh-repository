package com.basis100.deal.docprep.gui;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Iterator;
import javax.swing.JToolBar;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;
import javax.swing.border.EtchedBorder;
import com.basis100.resources.PropertiesCache;
import com.basis100.deal.docprep.gui.DynamicTableModel;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.GridLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.Box;

public class PropertiesPanel extends JPanel implements ActionListener, ItemListener {
  private JTable table;
  private JToolBar toolBar;
  private JCheckBox sortChkB;
  private JButton refreshButton;
  private DynamicTableModel dtm;
  private JScrollPane scrollPane;
  private Vector vData;
  private Vector vCols;
  private boolean SORTED = true;

  public PropertiesPanel(Dimension dm) {
    this();
    table.setPreferredScrollableViewportSize(dm);
  }

  public PropertiesPanel(){
    // 1) initialize panel
    super();
    setBackground(Color.green);
    setLayout(new BorderLayout());

    // 2) create toolbar
    JToolBar toolBar = createToolBar();
    add(toolBar, BorderLayout.NORTH);

    // 3) initialize table model with empty vectors
    vCols = new Vector();
    vCols.addElement(new String("Property"));
    vCols.addElement(new String("Value"));

    vData = new Vector();

    dtm = new DynamicTableModel(vCols, vData);
    table = new JTable(dtm);
    Dimension rv = this.getSize();

    table.setPreferredScrollableViewportSize(new Dimension(rv.width, rv.height));
    table.setBackground(Color.white);
    scrollPane = new JScrollPane(table);
    add(scrollPane, BorderLayout.CENTER);
  }

  public void actionPerformed(ActionEvent ae) {
    Object obj = ae.getSource();
    if (obj == refreshButton){
      refreshData();
    }
  }

  public boolean refreshData(){
    try {
      dtm.clearData();
      dtm.storeNewData(getData());
    }
    catch (Exception e){
      return false;
    }
    return true;
  };

  private Vector getData() throws Exception {

    this.vData.clear();
    Properties pr = PropertiesCache.getInstance().getLoadedProperties().properties;

    if (this.SORTED) {
      TreeMap tm = new TreeMap(pr);
      for (Iterator i=tm.keySet().iterator(); i.hasNext(); ){
        String element = i.next().toString();

        Vector vRow = new Vector();
        vRow.addElement(new String(element));
        vRow.addElement(PropertiesCache.getInstance().getProperty(-1, element));

        this.vData.addElement(vRow);
      }
    } else {
      for (Enumeration el = pr.propertyNames(); el.hasMoreElements(); ) {
        String element = el.nextElement().toString();

        Vector vRow = new Vector();
        vRow.addElement(new String(element));
        vRow.addElement(PropertiesCache.getInstance().getProperty(-1, element));

        this.vData.addElement(vRow);
      }
    }
    return this.vData;
  }

  //  --------------------------- GUI components ---------------------------
  private JToolBar createToolBar(){

     JToolBar tlb = new JToolBar();
     tlb.setFloatable(false);
     Dimension rv = this.getSize();

     tlb.setPreferredSize(new Dimension(rv.width, 35));
     tlb.setBorder(BorderFactory.createLineBorder(Color.black));
     tlb.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
     tlb.addSeparator();
     addButtonsTo(tlb);

     return tlb;
   }

  private void addButtonsTo(JToolBar ToolBar){
    refreshButton = new JButton("Refresh Data");
    refreshButton.setSize(23, 23);
    refreshButton.setPreferredSize(new Dimension(23, 23));
    refreshButton.setMinimumSize(new Dimension(23, 23));
    refreshButton.addActionListener(this);
    ToolBar.add(refreshButton);
    ToolBar.addSeparator();

    sortChkB = new JCheckBox("Sorted");
    // sortChkB.setMnemonic(KeyEvent.VK_C); // 1.4
    sortChkB.setSelected(true);
    sortChkB.addItemListener(this);
    ToolBar.add(sortChkB);
  }

  public void itemStateChanged(ItemEvent e) {
    if (e.getStateChange() == ItemEvent.DESELECTED)
    {
       this.SORTED = false;
    } else {
      this.SORTED = true;
    }
  }

}