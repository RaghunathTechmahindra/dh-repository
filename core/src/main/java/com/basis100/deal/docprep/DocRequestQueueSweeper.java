package com.basis100.deal.docprep;

/**
 * 11/Nov/2005 DVG #DG360 #2408  Review and/or correct "System.getProperty" calls globally
 */

import com.basis100.mail.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;

import java.util.*;
import java.net.*;

public class DocRequestQueueSweeper implements Runnable
{
  private Thread sweeper;

  private boolean stopped = true;

  private boolean errorstate = false;

  private boolean handledUncleared = false;

  private SessionResourceKit srk;

  private SysLogger log;

  private int offsetInterval = 10;

  private int requeue = 1;

  private Object lock = new Object();

  public DocRequestQueueSweeper(int offset)
  {
    sweeper = new Thread(this);

    sweeper.setName("DocRequestQueueSweeper");

    offsetInterval =DocumentGenerationManager.getInstance().getStaleTimeOut();

    srk = new SessionResourceKit("Sweeper");

    log = srk.getSysLogger();

    log.setJdbcTraceOn(false);

    try
    {
      InetAddress ipaddr = InetAddress.getLocalHost();

      String installId = "DocPrep @ - " + ipaddr.toString();

      System.getProperties().put("com.basis100.install.id", installId);
    }
    catch(UnknownHostException  uh)
    {
      log.info("DOCPREP: SWEEPER: Couldn't get localhost InetAddress: " + uh.getMessage());
      System.getProperties().put("com.basis100.installation.id", "Unknown Installation Location");
    }
    catch(Exception  se)//probably a security exeception
    {
      log.info("DOCPREP: SWEEPER: Couldn't get localhost InetAddress: " + se.getMessage());
      System.getProperties().put("com.basis100.installation.id", "UnIdentified Installation Location");
    }
  }


  public void run()
  {
     while(true)
     {
       try
       {
         synchronized ( lock )
         {
           if(stopped && errorstate)
           {
             log.error("DOCPREP: Queue sweeper stopping due to error state.");
             break;
           }
           else if(stopped)
           {
             log.info("DOCPREP: Queue sweeper stopping due to stop request.");
           }

           sweepQueue();

           lock.wait(offsetInterval * 6000);

           lock.notify(); // Notify threads waiting on lock condition (shouldn't be any)
         }
       }
       catch(Exception e)
       {
         stopped = true;
         errorstate = true;
         continue;
       }
     }

  }

  public void startSweeper()
  {
    if(stopped)
    {
      stopped = false;
      log.info("DOCPREP: starting queue sweeper.");
      System.out.println("DOCPREP: starting queue sweeper.");
      sweeper.start();
    }
  }

  public void stop()
  {
    if(!stopped)
     stopped = true;
     System.out.println("DOCPREP: stopping queue sweeper.");
     log.info("DOCPREP: stopping queue sweeper.");
  }

  private void sweepQueue() throws Exception
  {

    DocumentQueue queue = new DocumentQueue(srk);

    try
    {
      srk.beginTransaction();

      List stranded = (List)queue.findByOrphanedRequest(offsetInterval, requeue);



      if(stranded != null && !stranded.isEmpty())
      System.out.println("DOCPREP: Sweeper: found " + stranded.size() + " stranded in Document Queue.");
    //  else
    //  System.out.println("DOCPREP: Sweeper: clear");

      ListIterator it = stranded.listIterator();

      while(it.hasNext())
      {
        queue = (DocumentQueue)it.next();

        DocumentProfile profile = null;

        profile = queue.buildDocumentProfile();

        int status = queue.getRequestStatusId();

        boolean sendmail = false;

        if(profile != null)
        {
          if(status == Dc.DOCUMENT_REQUEST_STATUS_NEW )
          {
              queue.setRequestStatusId(Dc.DOCUMENT_REQUEST_STATUS_RETRIED);

              int id = queue.getDocumentQueueId();

              queue.setDocumentQueueId((id * -1));

              queue.setTimeRequested(new Date());
          }
          else if(status == Dc.DOCUMENT_REQUEST_STATUS_RETRIED )
          {
             sendmail = true;

             queue.setRequestStatusId(Dc.DOCUMENT_REQUEST_STATUS_FAILED);
          }

        }
        else  //if there is no profile just set the request to 'failed'
        {
          queue.setRequestStatusId(Dc.DOCUMENT_REQUEST_STATUS_FAILED);
        }

        queue.ejbStore();

        if(sendmail)
         sendMailAndLogStranded(queue, profile);
      }

      srk.commitTransaction();

    }
    catch(Exception ex)
    {
      srk.rollbackTransaction();

      log.error("DOCPREP: Failed to sweep the queue.");
      throw new Exception(ex.getMessage());
    }
       //once a day check for requests with status = failed that have been around
       //greater than 24 hours (could be parameterized later)
    try
    {
      Calendar now = Calendar.getInstance();
      int nowhour = now.HOUR_OF_DAY;

      if(nowhour == 12 && !handledUncleared)
      {
        int hours = 24;
        List stranded = (List)queue.findByUnclearedUnhandledOrphans(hours, Dc.DOCUMENT_REQUEST_STATUS_FAILED);

        if(stranded != null && !stranded.isEmpty())
         handleUncleared(stranded);

        handledUncleared = true;
      }
      else
      {
        if(handledUncleared)
         handledUncleared = false;
      }
    }
    catch(Exception ex2)
    {
      log.error("DOCPREP: Failed to clear old requests.");
    }

  }

  private void sendMailAndLogStranded(DocumentQueue que, DocumentProfile profile)
  {

    String msgname = "";

    try
    {
      int id = que.getRequestorUserId();
      //int queueId = que.getDocumentQueueId();

      UserProfile user = new UserProfile(srk);

      user.findByPrimaryKey(new UserProfileBeanPK(id, srk.getExpressState().getDealInstitutionId()));

      String name = "";

      StringBuffer msg =
         new StringBuffer( "An error occurred while attempting to create and send a document ");
         msg.append("\nfor BasisExpress DealId: ");
         msg.append(que.getDealId());
         msg.append("\nThe request was made at: " + que.getTimeRequested());


         msg.append("\n\n\t__________________________________________\n\n");


         if(profile != null)
         {
            name = profile.getDocumentFullName();

            if(name != null)
             msg.append("\nDocument Name: ").append(name);

            msg.append("\nThe document request has been retried and failed.");
         }
         else
         {
            msg.append("\nThe document requested does not have a valid profile and ");
            msg.append("\nhas not been requeued.");
         }

      StringBuffer msgDetail = new StringBuffer(msg.toString());
      //#DG360 String location = System.getProperty("com.basis100.install.id");
      String location = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.install.id");
      msgDetail.append("\nThe error occurred on DocPrep installation : " + location + "\n");

      String fullMessage = msgDetail.toString();
      String msgSubject = "Warning - Unfulfilled Document Request.";

      log.info(fullMessage);

      SimpleMailMessage userMsg = new SimpleMailMessage();
      SimpleMailMessage adminMsg  = new SimpleMailMessage();

      userMsg.setSubject(msgSubject);
      adminMsg.setSubject(msgSubject);

      userMsg.setText(msg.toString());
      adminMsg.setText(fullMessage);

      ProfileMailAddresser add = new ProfileMailAddresser(srk);
      add.addressMessage(userMsg, user);

      adminMsg.makeDefault("warning");

      adminMsg.setFrom("BasisExpress.Documents@basis100.com");
      userMsg.setFrom("BasisExpress.Documents@basis100.com");

      try
      {
        adminMsg.send(null);
      }
      catch(Exception e2)
      {
         msgname = " Docprep Administrator ";
         String  err = "DOCPREP - DocRequestQueueSweeper - Failed to send one or more email notifications to" + msgname + "due to error: " + e2.getMessage();
         System.out.println(err);
      }

    }
    catch(Exception e)
    {
      String er = e.getMessage();

      if(er == null) er = e.getClass().getName();

      er = "DOCPREP - DocRequestQueueSweeper - Failed to send one or more email notifications to" + msgname + "due to error: " + er;

      log.error(er);
      System.out.println(er);
      return;
    }

  }


  private void handleUncleared(List l)
  {
    StringBuffer msg = new StringBuffer("The following unsent and uncleared Document Requests ");
    msg.append("\nwill be disabled.\n\n");

    boolean disabled = false;
    boolean mailed = false;

    try
    {
      try
      {
        Iterator it = l.iterator();
        int number = 1;

        DocumentQueue queue = new DocumentQueue(srk);
        srk.beginTransaction();

        while(it.hasNext())
        {
          queue = (DocumentQueue)it.next();
          queue.setRequestStatusId(Dc.DOCUMENT_REQUEST_STATUS_DISABLED);

          DocumentProfile profile = null;

          try
          {
            profile = queue.buildDocumentProfile();
          }
          catch(Exception e)
          {
            //do nothing - this may fail
          }

          String docname = "Unknown Name";

          if(profile != null)
           docname = profile.getDocumentDescription();

          Date d = queue.getTimeRequested();
          int dealid =  queue.getDealId();
          String dest = queue.getEmailAddress();


          msg.append(number).append(") ").append("Document Name: ").append(docname);
          msg.append("\n\tRequested at : ").append(d);
          msg.append("\n\tFor DealId   : ").append(dealid);
          msg.append("\n\tFor Recipient: ").append(dest).append("\n\n");
          number++;
        }

        disabled = true;

      }
      catch(Exception e)
      {
        String er = e.getMessage();

        if(er == null) er = e.getClass().getName();

        er = "DOCPREP - DocRequestQueueSweeper - Failed to disable uncleared record or send one email notification due to error: " + er;

        log.error(er);
      }

      try
      {
        SimpleMailMessage mailmsg = new SimpleMailMessage();
        mailmsg.makeDefault("warning");
        mailmsg.setText(msg.toString());
        mailmsg.setFrom("QueueSweeper@basis100.com");

        mailmsg.send(null);

        mailed = true;

      }
      catch(Exception me)
      {
        String er = me.getMessage();

        if(er == null) er = me.getClass().getName();

        er = "DOCPREP - DocRequestQueueSweeper - Failed to disable uncleared record or send one email notification due to error: " + er;

        log.error(er);
      }


      if(disabled && mailed)
       srk.commitTransaction();
      else
       srk.rollbackTransaction();

    }
    catch(Exception e)
    {
      String er = e.getMessage();

      if(er == null) er = e.getClass().getName();

      er = "DOCPREP - DocRequestQueueSweeper - Failed to delete uncleared record or send one email notification due to error: " + er;

      log.error(er);
      System.out.println(er);

      log.info(msg.toString());
      System.out.println(msg.toString());
    }
  }

}
