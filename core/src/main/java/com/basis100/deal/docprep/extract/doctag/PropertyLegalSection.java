package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;


public class PropertyLegalSection extends SectionTagExtractor
{
  public FMLQueryResult extract(DealEntity deal, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    PropertyAddressSection s = new PropertyAddressSection();
    return s.extract(deal, lang, format, srk);
  }
}
