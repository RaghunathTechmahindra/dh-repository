package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.util.*;
/**
 * Title: ComponentConditions
 * <p>
 * Description: This class is used to extract the component details as conditions.
 * @author MCM Impl Team.
 * @version 1.0 13-Aug-2008 XS_16.5 Initial version
 * @version 1.1 06-Oct-2008 Bug Fix :FXP22542 current.getSectionCode change to current.loadSectionCode().
 * 
 */
public class ComponentConditions extends DocumentTagExtractor
{

    /**
     * Description: This mthod returns the component details as conditions
     * @author MCM Impl Team.
     * @version 1.0 13-Aug-2008 XS_16.5 Initial version
     * 
     */
    public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
    {
        debug("Tag -> " + this.getClass().getName());
        System.out.println("Tag name---"+this.getClass().getName());

        String val = "";
        Deal deal = (Deal)de;

        FMLQueryResult fml = new FMLQueryResult();
        try
        {
            StringBuffer buf = new StringBuffer();
            int index = 0;
            ConditionHandler handler = new ConditionHandler(srk);
            // gets the Document tracking entries with tags already parsed
            List text = (List)handler.retrieveCommitmentLetterDocTracking(deal,lang);
            ListIterator li = text.listIterator();
            DocumentTracking current = null;
            String hasCondToExclude = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.filogix.docprep.doctags.hide.conditions");
            while(li.hasNext())
            {
                current = (DocumentTracking)li.next();
                if(hasCondToExclude != null  ){
                    if( StringUtil.isInTheList(hasCondToExclude, ""+current.getConditionId() ) ){
                        continue;
                    }
                }
                //Including only component related conditions if the deal product type is component elligible.
                //@version 1.2 06-Oct-2008 Bug Fix :FXP22542 current.getSectionCode change to current.loadSectionCode().
                if(("MCM").equals(current.loadSectionCode()) && deal.getMtgProd().getComponentEligibleFlag().equals("Y"))
                {
                    buf.append(++index).append(".").append(format.space());
                    buf.append(current.getDocumentTextByLanguage(lang)).append(format.space());
                    buf.append( format.carriageReturn() );
                    buf.append( format.carriageReturn() );
                }
            }

            val += buf.toString();

            if(format.getType().startsWith("rtf")){
                val = this.convertCRtoRTF(val, format);
            }

            fml.addValue(val, fml.STRING);
        }
        catch(NullPointerException npe)
        {
            return fml;
        }
        catch(FinderException fe)
        {
            return fml;
        }
        catch (Exception e)
        {
            srk.getSysLogger().error(e.getMessage());
            srk.getSysLogger().error(e);
            throw new ExtractException("Unable to get CommitmentConditions ");
        }


        return fml;
    }
    /**
     * Description: This mthod returns the string which is in rtf format
     * @author MCM Impl Team.
     * @version 1.0 13-Aug-2008 XS_16.5 Initial version
     * 
     */
    private String convertCRtoRTF(String pStr, DocumentFormat format){
        if(! format.getType().startsWith("rtf") ){ return pStr; }
        int ind;
        StringBuffer retBuf = new StringBuffer("");
        while( (ind = pStr.indexOf("�")) != -1 ){
            retBuf.append(pStr.substring(0,ind));
            retBuf.append( format.carriageReturn() );
            pStr = pStr.substring(ind + 1);
        }
        retBuf.append(pStr);
        return retBuf.toString() ;
    }

}
