package com.basis100.deal.docprep;

import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.docprep.deliver.*;
import java.io.*;
import java.util.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.deal.docrequest.*;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import com.basis100.xml.*;
public class RequestTest
{
   public static void main(String args[]) throws Exception
   {

     // ResourceManager.init("\\mos\\admin\\","MOS_D");
     // ResourceManager.init("\\mos\\admin\\","MOS_Q");
     // ResourceManager.init("\\mos\\admin\\","MOS_PRODUCTION_TD");
     // ResourceManager.init("\\mos\\admin\\","MOS_E");
      ResourceManager.init("\\mos\\admin\\","MOS_DJ_E2E");
      //ResourceManager.init("\\mos\\admin\\","MOS_BM_E2E");
      //ResourceManager.init("\\mos\\admin\\","MOS_TD_E2E");
     PropertiesCache.addPropertiesFile( "\\mos\\admin\\mossys.properties");
     SessionResourceKit srk  = new SessionResourceKit("3790");

     TestDocGen t = new TestDocGen();

//t.testSasasEngine();
//     t.testReportCreator();



//    t.testDocumentGeneration(srk);



     //t.testPendingMessage(srk,null);
 //    t.testConditionsOutstanding(srk,null);
    //  t.testMailFromSysLogger(srk);
   //  t.testMessageModule(srk);
//t.testXSLOutstandingCondMerge();
//t.testConditionParser(srk,null);
      //t.testRTFCreator();
      // t.testPagelessBRExecutor(srk);

  //t.testConditionBusinessRuleExecutor(srk);
   //t.testBCBusinessRuleExecutor(srk);

      //t.extractLogData();
      //t.testConditions(srk,null);
//==============================================================================
// desjardain
//==============================================================================
//t.testXSLDealExtractor("com.basis100.deal.docprep.extract.data.DJConventioanlApprovalExtr",srk,null);

//============================================================================================
// DESJARDAINS
//============================================================================================
//t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJConventioanlApprovalExtr",srk,null);
//t.testXSLDJMergerFromFile();
// t.testMergeTwoFiles();

//t.examineFile();

//==============================================================================
// desjardin tests
//==============================================================================
//t.yetAnotherFOPTest();
// t.anotherFOTest();
//t.testFOAsString();
//t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR053_f.xsl","f:\\temporary\\djtests\\DJ_CR053_f.pdf");
/*
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR119_f.xsl","f:\\temporary\\djtests\\DJ_CR119_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR120_f.xsl","f:\\temporary\\djtests\\DJ_CR120_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR053_f.xsl","f:\\temporary\\djtests\\DJ_CR053_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR057_f.xsl","f:\\temporary\\djtests\\DJ_CR057_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR055_f.xsl","f:\\temporary\\djtests\\DJ_CR055_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR118_f.xsl","f:\\temporary\\djtests\\DJ_CR118_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR059_f.xsl","f:\\temporary\\djtests\\DJ_CR059_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR060_f.xsl","f:\\temporary\\djtests\\DJ_CR060_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR061_f.xsl","f:\\temporary\\djtests\\DJ_CR061_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR047_f.xsl","f:\\temporary\\djtests\\DJ_CR047_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR040_f.xsl","f:\\temporary\\djtests\\DJ_CR040_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR039_f.xsl","f:\\temporary\\djtests\\DJ_CR039_f.pdf");
t.testXSLDJMerger("com.basis100.deal.docprep.extract.data.DJSpecialExtractor",srk,null, "d:\\mos\\admin\\docgen\\templates\\DJ_CR050_f.xsl","f:\\temporary\\djtests\\DJ_CR050_f.pdf");
*/

//     t.testXSLDJMergerFromFile();
//t.testDropChar();

//t.testApplicationLenderHandler(srk);
//t.testXSLExtractor("com.basis100.deal.docprep.extract.data.XceedMCAPDataInputSheet", srk,null);
//t.testXSLExtractor("com.basis100.deal.docprep.extract.data.GENXCommitmentExtractor", srk,null);
//t.testXSLExtractor("com.basis100.deal.docprep.extract.data.XceedCommitmentExtractor", srk,null);
// t.testXSLExtractor("com.basis100.deal.docprep.extract.data.CTCCDisburLtrExtractor", srk,null);

// t.testXSLExtractor("com.basis100.deal.docprep.extract.data.GENXCondOutstExtractor", srk,null);

//t.testXSLExtractor("com.basis100.deal.docprep.extract.data.XceedSolicitorsPackageExtractor", srk,null);

// t.testXSLExtractor("com.basis100.deal.docprep.extract.data.XceedWireTransfer", srk,null);
// t.testXSLExtractor("com.basis100.deal.docprep.extract.data.XceedMCAPDataInputSheet", srk,null);




//t.testXSLExtractor("com.basis100.deal.docprep.extract.data.XceedWireTransfer", srk,null);
// t.testXSLExtractor("com.basis100.deal.docprep.extract.data.XceedWireTransferII", srk,null);

//t.testXSLMerger("com.basis100.deal.docprep.extract.data.GENXCondOutstExtractor",  srk, null);
//t.findMatchingBracket();


      //t.testXMLParser();
//      t.testDisbursment(srk,null);
//      t.testConditionHandler(srk);
    //  t.testPreApp(t.setup(), null);
 //    t.testSolicitors(t.setup(),null);
      //t.testEzenet(srk,null);
      //t.testNASAppraisal(t.setup(),null);
      //t.testFBCAppraisal(t.setup(),null);
      //t.testEquiFax(t.setup(),null);
      // t.testPreApp(t.setup(),null);
//t.testCommit(t.setup(),null);
//t.testFOProduction();
//t.testInteger();
      // t.testDenial(t.setup(),null);
      // t.testMICancel(t.setup(),null);
//      t.testMIRequest(t.setup(), null);
//t.testApproval(t.setup(),null);
//t.testTDCTCapLink(t.setup(),null);
t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.PropertyEstimatedAppraisalValue", srk,null);
      //t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.CommitmentConditions", srk,null);
     // t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.LoanAmount", srk,null);
//     t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.LenderName", srk,null);
      //t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.BranchCurrentTime", srk,null);
      //t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.LiabilitiesSection", srk,null);
      // t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.Debts", srk,null);
      //t.testOneConditionGen("com.basis100.deal.conditions.sysgen.DebtsPaidPriorToAdvanceCondition", srk,null);
      //t.testOneConditionGen("com.basis100.deal.conditions.sysgen.FunderConditionSet", srk,null);
      //t.testOneConditionGen("com.basis100.deal.conditions.sysgen.WorkVisaCondition", srk,null);
      //t.testOneConditionGen("com.basis100.deal.conditions.sysgen.OccupancyCertificateCondition", srk,null);
      //t.testConditionHandler(srk);

//t.testBrokerApprovalConditionHandler(srk);
      //t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.BrokerConditions", srk,null);
      //t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.PrePayment", srk,null);
//t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.CommitmentBrokerConditions", srk,null);
      // t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.BusinessRule", srk,null);
      //t.testOneExtractor("com.basis100.deal.docprep.extract.doctag.CommitmentConditions", srk,null);
//t.testDocumentRequest(srk);

//      t.testUpload(t.setup(),null);

//t.testSummary(t.setup(), null);
//t.testDealSummaryLite(t.setup(),null);
      //t.testConditionHandler(t.setup(),null);
      //t.testOneConditionGen("com.basis100.deal.conditions.sysgen.SolicitorPaymentOfDebtsCondition", srk,null);
    /*
    try{
      Document retVal = null;
      File file = new File("d:\\temporary\\try005.xml");
      DocumentBuilder docBuilder = XmlToolKit.getInstance().xercesDocumentBuilder();

      retVal = docBuilder.parse( file );
      retVal.getDocumentElement().normalize();
      System.out.println("finished");
    } catch (Exception exc){
      System.out.println("it is exception");
    }
    */
  }


}