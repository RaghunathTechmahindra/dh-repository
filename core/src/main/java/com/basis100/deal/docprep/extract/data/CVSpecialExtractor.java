package com.basis100.deal.docprep.extract.data;

import java.util.*;

import java.awt.event.*;

import org.w3c.dom.*;
import MosSystem.*;
import com.basis100.deal.conditions.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.picklist.*;
import com.basis100.resources.*;
import com.basis100.xml.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class CVSpecialExtractor extends DealExtractor implements java.awt.event.ActionListener
{
  public CVSpecialExtractor() {
  }

  protected void buildDataDoc() throws Exception
  {

    doc = XmlToolKit.getInstance().newDocument();
    Element root = doc.createElement("Document");
    root.setAttribute("type","1");
    root.setAttribute("lang","" + lang);

    specElem = doc.createElement("specialRequirementTags");

    Element dealNode = createSubTree(doc,"Deal", srk, deal, true, true, lang, true);

    root.appendChild( dealNode ) ;
    root.appendChild( buildGeneralTags()  );
    root.appendChild( buildSpecificTags() );

    doc.appendChild(root);
  }

  /**
   * extractXMLData
   *
   * @param de DealEntity
   * @param pLang int
   * @param pFormat DocumentFormat
   * @param pSrk SessionResourceKit
   * @return String
   * @throws Exception
   */
  public String extractXMLData(DealEntity de, int pLang, DocumentFormat pFormat,
                               SessionResourceKit pSrk) throws Exception
  {
    addActionListener(this);
    ratioFieldList = DocumentGenerationManager.getInstance().getRatioFieldList();

    deal = (Deal) de;
    lang = pLang;
    format = pFormat;
    srk = pSrk;

    buildDataDoc();

    XmlWriter xmlwr = XmlToolKit.getInstance().getXmlWriter();

//    if (pLang == Mc.LANGUAGE_PREFERENCE_FRENCH)
       xmlwr.setEncoding("iso-8859-1");

    return xmlwr.printString(doc);
  }

  //===========================================================================
  // extraction of specific tags
  //===========================================================================
  protected Node buildSpecificTags() throws Exception
  {
    addTo(specElem, "isAmended", (deal.haveInstructionsBeenProduced() ? "Y" : "N"));   // #727
    addTo(specElem, extractGuarantorClause());
    addTo(specElem, extractLoanAmountMinusFees());
    addTo(specElem, extractInterestRate());
    addTo(specElem, extractPremium());
    addTo(specElem, extractPaymentTerm());
    addTo(specElem, extractTaxPortion());
    addTo(specElem, extractTotalAmount());
    addTo(specElem, extractClosingText());
    addTo(specElem, extractConditions());
    addTo(specElem, extractVoidChq());
    addTo(specElem, extractTitleInsurance());
    addTo(specElem, extractFireInsurance());
    addTo(specElem, extractTaxesPaid());
    addTo(specElem, extractRateGuarantee());
    addTo(specElem, extractPrivilegePayment());
    addTo(specElem, extractInterestCalc());
    addTo(specElem, extractClosing());
    addTo(specElem, extractAcceptance());
    addTo(specElem, extractSignature());
    addTo(specElem, extractBorrNamesAsLine());
    addTo(specElem, extractCurrentUser());
    addTo(specElem, extractCareOf());
    addTo(specElem, extractDeductibleFees()); // CR 24-JUN-04
    addTo(specElem, extractAmortizationInYrsMnth()); // CR 30-JUN-04
    return specElem;
  }

  protected Node extractAmortizationInYrsMnth(){
    String val = null;

    try {
      val = ("" + convertMonthsToYearsAndMonths(deal.getAmortizationTerm()));
    } catch (Exception e) { }
    return getTag("AmortizationInYrsMnth", val);
  }

  protected Node extractCareOf()
  {
      String val = null;

      try
      {
          val = (deal.getSystemTypeId() == Mc.SYSTEM_TYPE_CCAPS) ?
                "FROM:" : "C/O:";
      }
      catch (Exception e){}

      return getTag("CareOf", val);
  }

  protected Node extractLandOfficeClause(Property prop)
  {
      String val = null;

      try
      {
          int id = prop.getProvinceId();

          if (id == Mc.PROVINCE_ALBERTA ||
              id == Mc.PROVINCE_BRITISH_COLUMBIA ||
              id == Mc.PROVINCE_MANITOBA  ||
              id == Mc.PROVINCE_SASKATCHEWAN)
          {
              val = "Land Titles Office";
          }
          else
          {
              val = "Land Registry Office";
          }
      }
      catch (Exception e) {}

      return getTag("LandOfficeClause", val);
  }

  protected Node extractCurrentUser() throws Exception {
    UserProfile lUserProfile = new UserProfile(srk);
//    lUserProfile = lUserProfile.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId()));
    // added instituionProfileId from ExpressState
    lUserProfile = lUserProfile
                         .findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(), 
                                                                 srk.getExpressState().getDealInstitutionId()));

    if(lUserProfile != null){
      return createSubTree(doc, "currentUser", srk,  lUserProfile, true,true, lang, true);
    }
    return null;
  }

  protected Node extractBorrNamesAsLine() throws Exception {
    Node borrAndGuarNamesNode = doc.createElement("borrowerAndGuarantorNames");
    Node borrowerNamesNode = doc.createElement("borrowerNames");
    Node guarantorNamesNode = doc.createElement("guarantorNames");

    ArrayList bList = (ArrayList) deal.getBorrowers();
    Iterator it = bList.iterator();
    String borrowerNames = "";
    String guarantorNames = "";
    String primaryBorrowerName = "";
    boolean bStart = true;
    boolean gStart = true;
    while( it.hasNext() ){
      Borrower b = (Borrower) it.next();
      if( b.isPrimaryBorrower() ){
        primaryBorrowerName = primaryBorrowerName + b.getBorrowerFirstName() + " " + b.getBorrowerLastName();
      }else if(b.isGuarantor()){
        if(gStart){
          gStart = false;
          guarantorNames = guarantorNames + b.getBorrowerFirstName() + " " + b.getBorrowerLastName();
        }else{
          guarantorNames = guarantorNames + ", " + b.getBorrowerFirstName() + " " + b.getBorrowerLastName();
        }
      }else{
        if(bStart){
          bStart = false;
          borrowerNames = borrowerNames + b.getBorrowerFirstName() + " " + b.getBorrowerLastName();
        }else{
          borrowerNames = borrowerNames + ", " + b.getBorrowerFirstName() + " " + b.getBorrowerLastName();
        }
      }
    }// end while
    if( !borrowerNames.equals("") ){
      primaryBorrowerName = primaryBorrowerName + ", " + borrowerNames;
    }
    addTo(borrowerNamesNode,"names",primaryBorrowerName);
    addTo(guarantorNamesNode,"names",guarantorNames);
    addTo(borrAndGuarNamesNode,borrowerNamesNode);
    addTo(borrAndGuarNamesNode,guarantorNamesNode);
    return borrAndGuarNamesNode;
  }

  protected Node extractSignature()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);
          val = ch.getVariableVerbiage(deal, Dc.SIGNATURE, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("Signature"), val));
  }

  protected Node extractAcceptance()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);
          val = ch.getElectronicVerbiage(deal, Dc.ACCEPTANCE_RETURN_STATEMENT, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("Acceptance"), val));
  }


  protected Node extractClosing()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);

          val = ch.getVariableVerbiage(deal, Dc.CLOSING_TEXT_ENDING, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("Closing"), val));
  }

  protected Node extractInterestCalc()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);
          //  TODO:  Need a constant here, instead of 377.
          val = ch.getVariableVerbiage(deal, 377, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("InterestCalc"), val));
  }


  protected Node extractPrivilegePayment()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);

          val = ch.getVariableVerbiage(deal, Dc.PRIVILEGE, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("PrivilegePayment"), val));
  }

  protected Node extractRateGuarantee()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);

          val = ch.getVariableVerbiage(deal, Dc.RATE_GUARANTEE, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("RateGuarantee"), val));
  }

  protected Node extractTaxesPaid()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);
          val = ch.getVariableVerbiage(deal, Dc.TAXES_PAID, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("TaxesPaid"), val));
  }

  protected Node extractFireInsurance()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);
          //  TODO:  Need a constant here instead of 406.
          val = ch.getVariableVerbiage(deal, 406, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("FireInsurance"), val));
  }

  protected Node extractTitleInsurance()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);
          val = ch.getVariableVerbiage(deal, Dc.TRANSFER_SURVEY, lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("TitleInsurance"), val));
  }

  protected Node extractVoidChq()
  {
    String val = null;

    try
    {
      //--> In order to fix the bug that DocPrep creating the un-necessary PACAgreementCondition when
      //--> Generating the Instruct Solicitor document.
      //--> By Billy 17Aug2004
      /*
      ConditionHandler ch = new ConditionHandler(srk);
      ConditionParser parser = new ConditionParser();
      Condition c = new Condition(srk, Dc.PAC_AGREEMENT_ASSUMPTIONS);
      DocumentTracking dt = parser.createSystemGeneratedDocTracking(c, deal, deal, srk);
      */
      //--> Check if Any PACAgreementCondition documentTracking record
      DocumentTracking dt = new DocumentTracking(srk);
      Collection dts = dt.findByDealAndConditionId((DealPK)(deal.getPk()), Dc.PAC_AGREEMENT_ASSUMPTIONS);

      Iterator it = dts.iterator();

      if(it.hasNext())
      {
        dt = (DocumentTracking)it.next();
        val = dt.getDocumentTextByLanguage(lang);
      }
      else
      {
        val = "nil";
      }
    }
    catch(Exception e)
    {
      srk.getSysLogger().error("Exception @CVSpecialExtractor.extractVoidChq : " + e);
    }

    return getTag("VoidChq", val);
  }

  protected Node extractConditions()
  {
      Node elemNode = null;

      try
      {
          // ConditionHandler ch = new ConditionHandler(srk);
          List dts = (List)this.retrieveCommitmentLetterDocTracking(deal, lang); // as opposed to ch

          if (dts.isEmpty())
             return null;

          boolean bShowRole =
              PropertiesCache.getInstance().
                  getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.conditions.show.responsibilityrole.tag", "Y").
                      equalsIgnoreCase("Y");

          Iterator i = dts.iterator();

          String role;
          //ConditionParser parser = new ConditionParser();
          //Condition cond;
          DocumentTracking current;
          StringBuffer buf;
          ConditionParser p = new ConditionParser();

          while (i.hasNext())
          {
             buf = new StringBuffer();
             current = (DocumentTracking)i.next();

             //  No longer need the index of the item, since the XSL will
             //  take care of it for us, which allows for better formatting
             //buf.append(++index).append(".").append(format.space());

             buf.append(current.getDocumentTextByLanguage(lang)).append(format.space());

             if(bShowRole)
             {
               role = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ConditionResponsibilityRole", current.getDocumentResponsibilityRoleId() , lang);
               //ALERT:BX:ZR Release2.1.docprep
               //role = PicklistData.getDescription("ConditionResponsibilityRole",
               //          current.getDocumentResponsibilityRoleId());
               //role = DocPrepLanguage.getInstance().getTerm(role, lang);
               buf.append('(').append(role).append(')');
             }

              if (elemNode == null)
                 elemNode = doc.createElement("Conditions");

                 //  JS - changed to make the XML data not RTF-specific.  This
                 //  new way adheres to other XML generation techniques,
                 //  where the lines previously divided by '\par'
                 //  are now divided into child nodes.  This allows the
                 //  XSL to format to the respective document type's syntax.
//                addTo(elemNode, "Condition", convertCRtoRTF(buf.toString(), format));

                 elemNode.appendChild(
                    convertCRToLines(doc.createElement("Condition"),
                    p.parseText(buf.toString(),lang,deal,srk)));
          }
      }
      catch (Exception e){}

      return elemNode;
  }


  protected Node extractClosingText()
  {
      String val = null;

      try
      {
          ConditionHandler ch = new ConditionHandler(srk);
          val = ch.getVariableVerbiage(deal,Dc.CLOSING_TEXT,lang);
      }
      catch (Exception e){}

      return (val == null ? null :
              convertCRToLines(doc.createElement("ClosingText"), val));
  }

  protected Node extractTotalAmount()
  {
      String val = null;

      try
      {
          int id = deal.getDealPurposeId();
          int miIndID = deal.getMIIndicatorId();
          double amt;

          if( id == Mc.DEAL_PURPOSE_PORTABLE_INCREASE ||
              id == Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT)
          {
              amt = deal.getTotalLoanAmount();
          }
          else if((miIndID == Mc.MII_REQUIRED_STD_GUIDELINES ||
                   miIndID == Mc.MII_UW_REQUIRED) &&
                   deal.getMIUpfront().equals("N"))
          {
              amt = deal.getTotalLoanAmount();
          }
          else
              amt = deal.getNetLoanAmount();

          val = formatCurrency(amt);
      }
      catch (Exception e){}

      return getTag("TotalAmount", val);
  }

  // get all fees except 6, 12, 14, 16, 18, 20 (as per DerekM)
  // CR 24-JUN-04
  // CR 4-Oct-04, changed again to a previous version


  protected Node extractDeductibleFees()
  {
      Node dealFeeNode = doc.createElement("deductibleFees");
      List col1 = new ArrayList();
      String val = null;

      try
      {
          List fees = (List)deal.getDealFees();

          Iterator i = fees.iterator();

          while (i.hasNext())
          {
              DealFee df = (DealFee)i.next();
              int ftype = df.getFee().getFeeTypeId();

              if ((ftype == Mc.FEE_TYPE_APPRAISAL_FEE) ||                       // Catherine Rutgaizer, 17-May-05, #1402
                  (ftype == Mc.FEE_TYPE_CMHC_FEE_PAYABLE) ||
                  (ftype == Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE) ||
                  (ftype == Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE) ||
                  (ftype == Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE) ||
                  (ftype == Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE) ||
                  (ftype == Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE))
              {
              } else {
                col1.add(df);
              }
          }
      }
      catch (Exception e){}

      Iterator it = col1.iterator();
      Element stree;
      DealFee dFee = null;
      double total = 0;

      while (it.hasNext()){
        dFee = (DealFee)it.next();
        stree = createSubTree(doc, "DealFee", srk, dFee, true, true, lang, true);
        dealFeeNode.appendChild(stree);
        total += dFee.getFeeAmount();
      }
      total += deal.getInterimInterestAmount(); // add interest adjustment here!!
      val = formatCurrency(total);

      dealFeeNode.appendChild(getTag("totalDeductionsAmount", val));

      val = formatCurrency(deal.getTotalLoanAmount() - total);
      dealFeeNode.appendChild(getTag("netAdvance", val));
      return dealFeeNode;
  }

  // CR 25-JUN-04
  protected Node extractExecution(Property prop)
  {
      String val = null;

      try
      {
          int id = prop.getProvinceId();

          ConditionHandler ch = new ConditionHandler(srk);

          val = (id == Mc.PROVINCE_ALBERTA) ?
              ch.getVariableVerbiage(deal, Dc.ALBERTA_EXECUTION, lang) :
              ch.getVariableVerbiage(deal, Dc.EXECUTION, lang);
      }
      catch (Exception e) {}

      return getTag("Execution", val);
  }

  protected Node extractTaxPortion()
  {
      String val = null;

      try
      {
          EscrowPayment ep = new EscrowPayment(srk, null);

          List list = (List)ep.findByDeal((DealPK)deal.getPk());

          if (!list.isEmpty())
          {
              Iterator i = list.iterator();

              while (i.hasNext())
              {
                  ep = (EscrowPayment)i.next();

                  if (ep.getEscrowTypeId() == 0)
                  {
                      val = formatCurrency(ep.getEscrowPaymentAmount());
                      break;
                  }
              }
          }
      }
      catch (Exception e){}

      return getTag("TaxPortion", val);
  }

  protected Node extractPaymentTerm()
  {
      Node retNode = doc.createElement("PaymentTerm");
      Node textNode;

      String retValStr = null;
      String ptDescTranslated = null;
      int apt = deal.getActualPaymentTerm();

      retValStr = DocPrepLanguage.getInstance().getYearsAndMonths(apt,lang);
      retValStr += format.space();

      try{
        int ptid = deal.getPaymentTermId();
        int paymentTermTypeId = findPaymentTermTypeId(srk,ptid);
        if(paymentTermTypeId == -1){
          ptDescTranslated = "";
        }else{
          //ptDesc = PicklistData.getDescription("PaymentTermType",paymentTermTypeId);
          //ALERT:BX:ZR Release2.1.docprep
          ptDescTranslated = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "PaymentTermType", paymentTermTypeId, lang);
          //ptDescTranslated = DocPrepLanguage.getInstance().getTerm(ptDesc,lang);
          if(ptDescTranslated == null){
            ptDescTranslated = "";
          }
        }
      }catch(Exception exc){
        return null;
      }
      retValStr += ptDescTranslated;
      textNode = doc.createTextNode(retValStr);
      retNode.appendChild(textNode);
      return retNode;
  }

  protected int findPaymentTermTypeId( SessionResourceKit srk, int parId )
  {
    JdbcExecutor jExec = srk.getJdbcExecutor();
    String sql01 = "Select * from paymentterm where paymenttermid = " + parId;

    int retVal = -1;
    boolean gotRecord = false;
    try{
      int key = jExec.execute(sql01);

      for (; jExec.next(key); )
      {
        retVal = jExec.getInt(key,"pttypeid");
        gotRecord = true;
        break; // only one record, please.
      }
      jExec.closeData(key);
    } catch (Exception exc){
      String message = exc.getMessage();
      message += "\n Method: findPaymentTermTypeId Class: XSLExtractor paymenttermid: ";
      message += "" + parId + "\n";
      message += "SQL statement used: " + sql01;
      srk.getSysLogger().error("DOCPREP: XSLExtractor: findPaymentTermTypeId:" + parId + " .");
      return -1;
    }

    return (gotRecord) ? retVal : -1;
  }

  protected Node extractPremium()
  {
      String val = null;

      try
      {
          double amt = 0.0;
          int mid = deal.getMIIndicatorId();

          if((mid == Mc.MII_REQUIRED_STD_GUIDELINES || mid == Mc.MII_UW_REQUIRED) &&
              deal.getMIUpfront().equals("N"))
          {
              amt = deal.getMIPremiumAmount();
          }
          val = formatCurrency(amt);
      }
      catch (Exception e){}

      return getTag("Premium", val);
  }

  protected Node extractInterestRate()
  {
      String val = null;

      try
      {
          PricingProfile pp = new PricingProfile(srk);
          pp = pp.findByPricingRateInventory( deal.getPricingProfileId());
          val = null;

          if(pp != null && pp.getRateCode().equalsIgnoreCase("UNCNF"))
          {
              ConditionHandler ch = new ConditionHandler(srk);
              val = ch.getVariableVerbiage(deal, Dc.UNCONFIRMED_RATE, lang);
          }
          else
              val = formatInterestRate(deal.getNetInterestRate());
      }
      catch (Exception e){}

      return getTag("InterestRate", val);
  }

  protected Node extractLoanAmountMinusFees()
  {
      String val = null;

      try
      {
          double amt = deal.getNetLoanAmount();

          List dealFees = (List)deal.getDealFees();

          Iterator i = dealFees.iterator();
          DealFee df;
          Fee fee;

          while (i.hasNext())
          {
              df = (DealFee)i.next();
              fee = df.getFee();

              if (fee.getFeeTypeId() == Dc.FEE_TYPE_APPLICATION_FEE)
                 amt -= df.getFeeAmount();
          }
          val = formatCurrency(amt);
      }
      catch (Exception e){}

      return getTag("LoanAmountMinusFees", val);
  }

  protected Node extractGuarantorClause()
  {
      String gc = null;

      try
      {
          Vector propVect = (Vector) deal.getProperties();
          Iterator it = propVect.iterator();
          if( propVect.isEmpty() ){
            return getTag("GuarantorClause", "");
          }
          while( it.hasNext() ){
            Property oneProp = (Property)it.next();
            if( ! oneProp.isPrimaryProperty() ){
              continue;
            }
            if( oneProp.getProvinceId() ==  Mc.PROVINCE_BRITISH_COLUMBIA ){
              gc = (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? "Covenantor" :"Covenantant" );
            }else {
              gc = (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH ? "Guarantor" : "Garant");
            }
            break;
          }
      }
      catch (Exception e){}
      if(gc == null){
        return getTag("GuarantorClause", "");
      }
      return getTag("GuarantorClause", gc);
  }

  protected Node extractLoanAmountPreFee()
  {
      String val = null;

      try
      {
          double amt = deal.getNetLoanAmount();

          List list = (List)deal.getDealFees();

          Iterator i = list.iterator();
          DealFee df;
          Fee fee;

          while (i.hasNext())
          {
              df = (DealFee)i.next();
              fee = df.getFee();

              if (fee.getFeeTypeId() == Mc.FEE_TYPE_APPLICATION_FEE)
                 amt -= df.getFeeAmount();
          }

          val = formatCurrency(amt);
      }
      catch (Exception e){}

      return getTag("LoanAmountPreFee", val);
  }

  /**
   * actionPerformed
   *
   * @param actionEvent ActionEvent
   */
  public void actionPerformed(ActionEvent actionEvent) {
    Object obj = actionEvent.getSource();
    if(obj instanceof com.basis100.deal.entity.Property){
      if( ((Property)obj).isPrimaryProperty()  ){
        addTo(specElem,  extractLandOfficeClause((Property)obj) ) ;
        addTo(specElem,  extractExecution((Property)obj) ) ; // CR 25-JUN-04
      }
    }
  }


  public Collection retrieveCommitmentLetterDocTracking(Deal deal,int languagePreference)throws Exception
  {
    DocumentTracking dt = new DocumentTracking(srk);

    Collection entries = dt.findForCommitmentLetter((DealPK)deal.getPk(), true);
    ArrayList textout = new ArrayList();

    if(!entries.isEmpty())
    {
      Iterator it = entries.iterator();

      ConditionParser p = new ConditionParser();

      while(it.hasNext())
      {
        dt = (DocumentTracking)it.next();

        String text = dt.getDocumentTextByLanguage(languagePreference);

        if(text == null)continue;

        text = p.parseText(text,languagePreference,deal,srk);

        dt.setDocumentTextForLanguage(languagePreference,text);

        textout.add(dt);
      }
    }

    return textout;
  }
}
