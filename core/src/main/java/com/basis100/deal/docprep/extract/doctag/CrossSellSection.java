package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;



public class CrossSellSection extends ReplaceableSectionTagExtractor
{
  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    Deal deal = (Deal)de;
    FMLQueryResult fml = new FMLQueryResult();

    try
    {
      
      int id  =  deal.getLenderProfileId();
      LenderProfile lp = new LenderProfile(srk);
      lp.findByPrimaryKey(new LenderProfilePK(id));

      if(lp.getLPShortName().equalsIgnoreCase("Clarica")
          && lang == DocPrepLanguage.ENGLISH)
      {
        fml.setAction(this.REPLACE_ONLY);
        fml.addValue(deal);
      }
    }
    catch(NullPointerException npe)
    {
       return new FMLQueryResult();
    }
    catch(FinderException fe)
    {
       return new FMLQueryResult();
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(e);
      throw new ExtractException();
    }

    return new FMLQueryResult();




  }
}
