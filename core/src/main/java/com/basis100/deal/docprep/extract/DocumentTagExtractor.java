package com.basis100.deal.docprep.extract;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.log.*;
import com.basis100.resources.*;

import java.util.*;

public abstract class DocumentTagExtractor
{

   public static boolean debug = false;

   private SysLogger logger;

   public String debug(String in)
   {
     if(debug)
       System.out.println(in);

     if(logger == null)
     {
       logger = ResourceManager.getSysLogger("TagExtractor");
     }

     logger.debug(in);

     return in;
   }

   public abstract FMLQueryResult extract(DealEntity deal, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException;
   
}
