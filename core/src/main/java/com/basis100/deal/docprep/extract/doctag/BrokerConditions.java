package com.basis100.deal.docprep.extract.doctag;

import com.basis100.deal.docprep.extract.*;
import com.basis100.deal.docprep.*;

import com.basis100.deal.conditions.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.resources.*;
import com.basis100.entity.*;
import java.util.*;
import com.basis100.picklist.*;
import com.basis100.deal.conditions.*;

public class BrokerConditions extends DocumentTagExtractor
{

  public FMLQueryResult extract(DealEntity de, int lang, DocumentFormat format, SessionResourceKit srk)throws ExtractException
  {
    debug("Tag -> " + this.getClass().getName());

    String val = "";
    Deal deal = (Deal)de;

    FMLQueryResult fml = new FMLQueryResult();

    BrokerApprovalConditionHandler bach = new BrokerApprovalConditionHandler(srk);
    Map condMap = bach.extractBrokerApprovalConditionNodes(deal,format);

    if( condMap == null ){
      return new FMLQueryResult();
    }
    int index = 0;
    StringBuffer outBuf = new StringBuffer("");
    try
    {
         Set keys = condMap.keySet();

         Iterator it = keys.iterator();
         Integer currKey = null;
         String text = null;
         Integer lConditionKey;
         ConditionContainer conditionContainer;
         ConditionParser p;
         String outText;
         while(it.hasNext())
         {
            currKey = (Integer)it.next();
            conditionContainer = (ConditionContainer)condMap.get(currKey) ;
            lConditionKey = conditionContainer.getCondIdCode() ;
            text = conditionContainer.getCondText() ;

            p = new ConditionParser();
            outText = p.parseText(text,lang,deal,srk);

            outBuf.append(++index).append(".").append(format.space()) ;
            outBuf.append(outText) ;
            if(format.getType().startsWith("rtf")){
             outBuf.append(format.carriageReturn()).append(format.carriageReturn()) ;
            }
         }
         String wrkStr = outBuf.toString();
         //val = outBuf.toString();
         if(format.getType().startsWith("rtf")){
           val = this.convertCRtoRTF(wrkStr, format);
         }
         //System.out.println(val);
         fml.addValue(val, fml.STRING);
    }
    catch(NullPointerException npe)
    {
       return fml;
    }
    catch (Exception e)
    {
      srk.getSysLogger().error(e.getMessage());
      srk.getSysLogger().error(e);
      throw new ExtractException("Unable to get CommitmentConditions ");
    }
    return fml;
  }

  private String convertCRtoRTF(String pStr, DocumentFormat format){
    if(! format.getType().startsWith("rtf") ){ return pStr; }
    int ind;
    StringBuffer retBuf = new StringBuffer("");
    while( (ind = pStr.indexOf("�")) != -1 ){
      retBuf.append(pStr.substring(0,ind));
      retBuf.append( format.carriageReturn() );
      pStr = pStr.substring(ind + 1);
    }
    retBuf.append(pStr);
    return retBuf.toString() ;
  }

}
/*
       StringBuffer buf = new StringBuffer();

       int index = 0;

       ConditionHandler handler = new ConditionHandler(srk);

       // gets the Document tracking entries with tags already parsed
       List text = (List)handler.retrieveCommitmentLetterDocTracking(deal,lang);

       ListIterator li = text.listIterator();

       DocumentTracking current = null;
       String role = "Unknown Responsibility Role";

       while(li.hasNext())
       {
         current = (DocumentTracking)li.next();

         buf.append(++index).append(".").append(format.space());

         buf.append(current.getDocumentTextByLanguage(lang)).append(format.space());

         role = PicklistData.getDescription(
            "ConditionResponsibilityRole",current.getDocumentResponsibilityRoleId());

         role = DocPrepLanguage.getInstance().getTerm(role, lang);

         buf.append('(').append(role).append(')').append(format.carriageReturn());
       }

      val += buf.toString();

      fml.addValue(val, fml.STRING);

*/

/*
    try
    {

      if(resMap != null)
      {
         Set keys = resMap.keySet();

         Iterator it = keys.iterator();
         Integer currKey = null;
         String text = null;
         Integer lConditionKey;
         ConditionContainer conditionContainer;
         while(it.hasNext())
         {
           currKey = (Integer)it.next();
           //text = (String)resMap.get(currKey);
           conditionContainer = (ConditionContainer)resMap.get(currKey);
           lConditionKey = conditionContainer.getCondIdCode();
           text = conditionContainer.getCondText();

           //Node approval =  createApprovalNode(currKey,deal,text,dom,format);
           Node approval =  createApprovalNode(lConditionKey,deal,text,dom,format);

           if(approval != null)
              root.appendChild(approval);
           else
           {
             String msg = "Approval Conditions: Unable to create Approval Node for Condition: " +
                          currKey + " LanguagePreferenceId: " + lang;
             logger.info(msg);
           }
         }
      }
    }
    catch(Exception e)
    {
      logger.error("Approval Conditions: Error while building nodes.");
      logger.error(e);
    }

*/