/*
 * Created on 21-Jan-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.deal.util;

/**
 * @author crutgaizer
 *
 */
public class StreetAddress
{
  private String unit;
  private String streetNum;
  private String streetDir;
  private String streetName;
  private String streetType;

  /**
   * @param streetType The streetType to set.
   */
  public void setStreetType(String streetType)
  {
    this.streetType = streetType;
  }

  /**
   * @return Returns the streetType.
   */
  public String getStreetType()
  {
    return streetType;
  }

  /**
   * @param streetName The streetName to set.
   */
  public void setStreetName(String streetName)
  {
    this.streetName = streetName;
  }

  /**
   * @return Returns the streetName.
   */
  public String getStreetName()
  {
    return streetName;
  }

  /**
   * @param streetDir The streetDir to set.
   */
  public void setStreetDir(String streetDir)
  {
    this.streetDir = streetDir;
  }

  /**
   * @return Returns the streetDir.
   */
  public String getStreetDir()
  {
    return streetDir;
  }

  /**
   * @param streetNum The streetNum to set.
   */
  public void setStreetNum(String streetNum)
  {
    this.streetNum = streetNum;
  }

  /**
   * @return Returns the streetNum.
   */
  public String getStreetNum()
  {
    return streetNum;
  }

  /**
   * @param unit The unit to set.
   */
  public void setUnit(String unit)
  {
    this.unit = unit;
  }

  /**
   * @return Returns the unit.
   */
  public String getUnit()
  {
    return unit;
  }
  
  public String toString(){
    return this.toString(0);
  }
  
  // returns street address based on English formatting 
  public String toString(int lang){
    StringBuffer stb = new StringBuffer();
    if (unit != null){  stb.append(unit);  }
    if (streetNum != null){ 
      if (unit != null) {stb.append("-");} 
      stb.append(streetNum + " ");  
    }
    
    switch (lang){
      case 0: {
        stb.append(streetName + " ");  
        if (streetType != null){  stb.append(streetType + " ");  }
        break;
        }
      case 1: {
        if (streetType != null){  stb.append(streetType + " ");  }
        stb.append(streetName + " ");
        break;
        }
    }
    if (streetDir != null){  stb.append(streetDir);  }

    return stb.toString();
  }
  
  public void printDetails(){
    printDetails(0);
  }
  
  public void printDetails(int lang){
      System.out.println("Unit = " + this.getUnit());
      System.out.println("Number = " + this.getStreetNum());
      System.out.println("Name = " + this.getStreetName());
      System.out.println("Type = " + this.getStreetType());
      System.out.println("Direction = "+ this.getStreetDir());
      System.out.println("Street address  = " + this.toString(lang));
  }
}