package com.basis100.deal.util;

import java.util.*;

public class Format
{
  private Format()  {}

  /**
   * Prints <code>number</code> as a float at the requested width and precision.
   *
   * @param w the desired width
   * @param d the desired precision
   * @param number the number to format
   *
   * @return the formatted string
   */
  protected static String printFloat(int w, int d, Double number)
  {
    String s;
    int p;
    s = "" + adjustPrecision(number.doubleValue(), d);

    if (s.charAt(0) == '.')
    {
      s = "0"+s;
    }
    else if (s.indexOf("-.") != -1)
    {
      s = "-0"+ s.substring(1, s.length());
    }

    if (s.indexOf(".") == -1)
      s += ".";

    p = s.indexOf(".");

    while (s.length()-1-p < d)
    {
      s += "0";
    }

    while (s.length() < w)
    {
      s = " " + s;
    }

    return s;
  }

  /**
   * Prints <code>number</code> as a float.
   *
   * @param w the desired width
   * @param d the desired precision
   * @param number the number to format
   *
   * @return the formatted string
   */
  protected static String printNumberG(int w, int d, Double number)
  {
    String s;
    int ww, temp;
    double abs = Math.abs(number.doubleValue());

    if (number.doubleValue() == 0)
    {
      temp = 1;
    }
    else if (abs >= 1)
    {
      temp = d-(""+(int)Math.floor(abs)).length();
    }
    else
    {
      temp = d-1;

      while (abs < 1)
      {
        temp++;
        abs *= 10;
      }
    }
    s = "" + adjustPrecision(number.doubleValue(), temp);

    if (s.charAt(0) == '.')
    {
      s = "0" + s;
    }
    else if (s.indexOf("-.") != -1)
    {
      s = "-0" + s.substring(1, s.length());
    }

    if (s.indexOf(".") == -1)
      s += ".";

    ww = s.length();
    ww -= (number.doubleValue() < 0 ? 1 : 0)+(Math.abs(number.doubleValue()) < 1 ? 1 : 0)+1;

    if (ww < d)
      for (int i=0; i < d-ww; i++)
        s += "0";

    ww = s.length();

    if (ww < w)
      for (int i=0; i < w-ww; i++)
        s = " " + s;

    return s;
  }

  /**
   * Prints <code>number</code> as an exponent.
   *
   * @param w
   * @param d the precision
   * @param number
   *
   * @return the formatted number
   */
  protected static String printExponent(int w, int d, Double number)
  {
    int exponent;
    double abs = Math.abs(number.doubleValue());
    Double temp;

    w -= 4;
    exponent = 0;

    while (abs < 1)
    {
      exponent--;
      abs *= 10;
    }

    while (abs >= 10)
    {
      exponent++;
      abs /= 10;
    }

    temp = new Double((number.doubleValue() < 0 ? -1 : 1)*abs);
    return printFloat(w, d-1, temp)+"e"+(exponent < 0 ? "-" : "+")+rec(exponent);
  }

  /**
   * Pads <code>expo</code> with leading zeroes if it is less than 100.
   *
   * @param expo the number to pad
   *
   * @return the formatted string
   */
  protected static String rec(int expo)
  {
    expo = Math.abs(expo);
    //return (expo < 10) ? "0"+expo : ""+expo;

    String str = "" + expo;

    while (str.length() < 3)
    {
      str = "0" + str;
    }

    return str;
  }

  /**
   * Adjusts <code>d</code> to the requested <code>precision</code>.
   * For example, adjustPrecision(1.2345, 2) would make 1.23.
   *
   * @param d the number to adjust
   * @param precision the number of decimal places requested
   * @return the adjusted number
   */
  public static double adjustPrecision(double d, int precision)
  {
    return Math.round(d*Math.pow(10, precision))/Math.pow(10, precision);
  }

  /**
   * This is a conversion of the printf Javascript code that is in
   * GenericCommonFunctions.txt, optimized where possible.
   * <BR>
   * <PRE>
   * %b print an int argument in binary
   * %d	print an int argument in decimal
   * %l	print a long int argument in decimal (in C's printf, this is %ld)
   * %c	print a character
   * %s	print a string
   * %f	print a float or double argument
   * %e	same as %f, but use exponential notation
   * %g	use %e or %f, whichever is better
   * %o	print an int argument in octal (base 8)
   * %x	print an int argument in hexadecimal (base 16)
   * %%	print a single %
   * </PRE>
   * Example:<BR>
   * Consider the following code:<BR><BR>
   * <ol>
   * <li>String[] user = {"Adam", "Eve"};
   * <li>int[] age = {24, 20};
   * <li>Format.printf("%s & %s are together %d years old.\n",
   * <li>&nbsp;&nbsp;new Parameters(user[0]).add(user[1]).add(age[0]+age[1]));
   * </ol>
   * <BR>
   * This code will produce the output:<BR>
   * <BR>
   * Adam & Eve are together 44 years old.<BR>
   *
   * @param str the string to format
   * @param p the parameters to apply to the string
   *
   * @return the formatted string
   */
  public static String printf(String str, Parameters p) throws FormatException
  {
    char c, c2, c3;
    int pointer = 0, w = 0, d = 0;
    boolean period;
    String line = "";
    Vector vArgs = p.toVector();

    for (int i = 0; i < str.length(); i++)
    {
      c = str.charAt(i);

      if (c == '%')
      {
        i++;
        c2 = str.charAt(i);

        if (c2 == '%')
        {
          line += c2;
          continue;
        }
        else if (c2 == '-')
        {
          c2 = str.charAt(++i);
        }

        if (isDigit(c2))
        {
          period = false;
          w = 0;
          d = 0;
          String width = "", precision = "";

          while ((c3 = str.charAt(i)) != 's' && c3 != 'b' &&
                  c3 != 'f' && c3 != 'g' && c3 != 'i' &&
                  c3 != 'd' && c3 != 'e' && c3 != 'c' &&
                  c3 != 'o' && c3 != 'x' && c3 != 'l')
          {
            if (c3 == '.')
            {
              period = true;
            }
            else if (!isDigit(c3))
            {
              line += "{non digit after % ("+c3+")}";
              return line;
            }
            else if (!period)
            {
              width += c3;
            }
            else if (period)
            {
              precision += c3;
            }
            i++;
          }

          try
          {
            w = Integer.parseInt(width);
          }
          catch (NumberFormatException nfe) {}

          try
          {
            d = Integer.parseInt(precision);
          }
          catch (NumberFormatException nfe) {}

          if (c3 == 's')
          {
            Object o = vArgs.get(pointer++);

            if (!(o instanceof String))
               throw new FormatException(getErrorText(c3, new String(), o));

            line += ((String)o).substring(0, w);
          }
          //  Here we need to choose between e and f, whichever is more compact for the
          //  the given value and precision...
          else if (c3 == 'g')
          {
            Object o = vArgs.get(pointer++);

            if (!(o instanceof Double))
               throw new FormatException(getErrorText(c3, new Double(0.00), o));

            Double number = (Double)o;
            double abs = Math.abs(number.doubleValue());

            if (number.doubleValue() == 0 || (0.001 <= abs && abs < Math.pow(10, d)))
            {
              line += printNumberG(w, d, number);
            }
            else
            {
              line += printExponent(w, d, number);
            }
          }
          else if (c3 == 'f')
          {
            Object o = vArgs.get(pointer++);

            if (!(o instanceof Double))
               throw new FormatException(getErrorText(c3, new Double(0.00), o));

            line += adjustPrecision (((Double)o).doubleValue(), d);
          }
          else if (c3 == 'e') //  exponent
          {
            Object o = vArgs.get(pointer++);

            if (!(o instanceof Double))
               throw new FormatException(getErrorText(c3, new Double(0.00), o));

            line += printExponent(w, d, (Double)o);
          }
          else if (c3 == 'c' || c3 == 'o' || c3 == 'x' ||
                   c3 == 'l' || c3 == 'i' || c3 == 'd' || c3 == 'b')
          {
            line += getValue(c3, vArgs.get(pointer++));
          }
        }
        if (c2 == 's' || c2 == 'f' || c2 == 'g' || c2 == 'i' ||
            c2 == 'd' || c2 == 'e' || c2 == 'c' || c2 == 'o' ||
            c2 == 'x' || c2 == 'l' || c2 == 'b')
        {
          line += getValue(c2, vArgs.get(pointer++));
        }
      }
      else
      {
        line += c;
      }
    }
    return line;
  }

  /**
   * This version of <code>isDigit</code> is different than Character.isDigit
   * in the sense that this version includes the decimal as a digit.
   *
   * @param c the character to check.
   *
   * @return whether <code>c</code> is a digit or not.
   */
  protected static boolean isDigit(char c)
  {
    return "0123456789.".indexOf(c) != -1;
  }

  /**
   * Returns an error message indicating the expected and actual objects
   * for <code>type</code>.
   *
   * @param type the character code for which there is an error
   * @param expected the expected object for character code <code>type</code>
   * @param actual the actual object for character code <code>type</code>
   *
   * @return the formatted error text
   */
  protected static String getErrorText(char type, Object expected, Object actual)
  {
    return "Expected object type for %" + type + " is {" + expected.getClass().getName() +
           "}, got {" + actual.getClass().getName() + "}.";
  }

  /**
   * Gets the value contained within <code>o</code> given the type expected
   * as a result of <code>type</code>.  If <code>o</code> isn't the expected type,
   * a <code>FormatException</code> is thrown.  Otherwise, the value is
   * returned as a <code>String</code>.  See the definition of
   * <code>printf()</code> for valid types.
   *
   * @param type the type of data that should be contained within <code>o</code>
   * @param o the <code>Object</code> holding the data
   *
   * @return the data contained within <code>o</code>
   */
  protected static String getValue(char type, Object o) throws FormatException
  {
    switch (type)
    {
      case 'b':
        if (!(o instanceof Integer))
          throw new FormatException(getErrorText(type, new Integer(0), o));
        else
          return Integer.toBinaryString(((Integer)o).intValue());

      case 'd':
      case 'i':
      {
        if (!(o instanceof Integer))
          throw new FormatException(getErrorText(type, new Integer(0), o));
        else
          return ((Integer)o).toString();
      }

      case 'l':
      {
        if (!(o instanceof Long))
          throw new FormatException(getErrorText(type, new Long(0), o));
        else
          return ((Long)o).toString();
      }

      case 'c':
      {
        if (!(o instanceof Character))
          throw new FormatException(getErrorText(type, new Character('a'), o));
        else
          return ((Character)o).toString();
      }

      case 's':
      {
        if (!(o instanceof String))
          throw new FormatException(getErrorText(type, new String(), o));
        else
          return (String)o;
      }

      case 'f':
      case 'e':
      {
        if (!(o instanceof Double))
          throw new FormatException(getErrorText(type, new Double(0.00), o));
        else
          return ((Double)o).toString();
      }

      case 'o':
      {
        if (!(o instanceof Integer))
          throw new FormatException(getErrorText(type, new Integer(0), o));
        else
          return Integer.toString(((Integer)o).intValue(), 8);
      }

      case 'x':
      {
        if (!(o instanceof Integer))
          throw new FormatException(getErrorText(type, new Integer(0), o));
        else
          return Integer.toString(((Integer)o).intValue(), 16);
      }

      default: throw new FormatException("UNKNOWN TYPE");
    }
  }

}