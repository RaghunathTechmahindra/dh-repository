package com.basis100.deal.util;

public class FormatException extends Exception
{
  public FormatException(String s)
  {
    super(s);
  }
}