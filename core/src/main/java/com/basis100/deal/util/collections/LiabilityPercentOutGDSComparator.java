package com.basis100.deal.util.collections;

import java.util.*;
import com.basis100.deal.entity.*;
import MosSystem.Mc;

/**
 * A comparison function, which imposes a <i>total ordering</i> on some
 * collection of objects.  Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 *  Note: this comparator imposes orderings that are inconsistent with equals.
 */

public class LiabilityPercentOutGDSComparator implements Comparator
{

   /**
   * Compares its two arguments for order.  Sorts collections by
   * PercentOutGDS.
   *
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   * @throws ClassCastException if the arguments' are not of type DealEntity.
   *
   */
  public int compare(Object o1, Object o2)
  {
     Liability l1 = (Liability)o1;
     Liability l2 = (Liability)o2;

     if (l1.getPercentOutGDS() < l2.getPercentOutGDS())
       return 1;
     else if (l1.getPercentOutGDS() > l2.getPercentOutGDS())
       return -1;
     else
       return 0;
  }
}
