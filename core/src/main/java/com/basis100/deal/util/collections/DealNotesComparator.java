package com.basis100.deal.util.collections;

import java.util.Comparator;
import com.basis100.deal.entity.DealNotes;
import com.basis100.picklist.PicklistData;
import MosSystem.Mc;
import java.util.*;

/**
 * A comparison function, which sorts based on date.
 * Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 */
public class DealNotesComparator implements Comparator
{
    /**
    * Compares its two arguments for order.
    *
    * @return a negative integer, zero, or a positive integer as the
    * 	       first argument is less than, equal to, or greater than the
    *	       second.
    * @throws ClassCastException if either of the arguments is not of type
    * <code>DealNotes</code>.
    *
    */
    public int compare(Object o1, Object o2)
    {
        DealNotes dn1 = (DealNotes)o1;
        DealNotes dn2 = (DealNotes)o2;
        Date date1 = dn1.getDealNotesDate();
        Date date2 = dn2.getDealNotesDate();

        if (date1 == null)
        {
            return (date2 == null) ? 0 : 1;
        }
        else if (date2 == null)
        {
            return (date1 == null) ? 0 : -1;
        }
        else
            return dn1.getDealNotesDate().compareTo(dn2.getDealNotesDate());
    }
}