/*
 * Created on 21-Jan-2005
 *
 * Address line parser 
 */
package com.basis100.deal.util;

import java.util.Collection;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * @author crutgaizer
 * This parser is used to pares "addressLine" fields (Borrower etc) 
 * returns instance of StreetAddress class 
 */
public class AddressLineParser {
	private StreetAddress strAddr;
	protected SessionResourceKit _srk;

	public AddressLineParser() {
	}
	
	
	public AddressLineParser(SessionResourceKit srk) {
	    this._srk = srk;
    }
    

	public StreetAddress getAddress() {
		return strAddr;
	}

	public void parse(String addrLine){
	  parse(addrLine, 0);
	}
	
	public void parse(String addrLine, int lang) {
		String tempAddr = new String(addrLine.trim());
		String temp;
		Collection res;

		int idx;

		strAddr = new StreetAddress();

		// 1) unit
		idx = tempAddr.indexOf("-");
		if (idx > 0) {
			strAddr.setUnit(tempAddr.substring(0, idx));
			tempAddr = tempAddr.substring(idx + 1);
		}

		// 2) number
		idx = tempAddr.indexOf(" ");
		if (idx > 0) {
			strAddr.setStreetNum(tempAddr.substring(0, idx));
			tempAddr = tempAddr.substring(idx + 1);
		}

		// 3) direction
		idx = tempAddr.lastIndexOf(" ");
		if (idx > 0) {
			temp = tempAddr.substring(idx + 1);

			res = BXResources.getPickListDescriptions(_srk.getExpressState().getDealInstitutionId(),"STREETDIRECTION", lang);
			
			for (Iterator it = res.iterator(); it.hasNext();) {
				if (temp.equalsIgnoreCase((String) it.next())) {
					strAddr.setStreetDir(temp);
					tempAddr = tempAddr.substring(0, idx);
					break;
				}
			}
		}

		// 4) type
		if (lang == Mc.LANGUAGE_PREFERENCE_ENGLISH) {
			idx = tempAddr.lastIndexOf(" ");
			if (idx > 0) {
				temp = tempAddr.substring(idx + 1);

				res = BXResources.getPickListDescriptions(_srk.getExpressState().getDealInstitutionId(),"STREETTYPE", lang);
				for (Iterator it = res.iterator(); it.hasNext();) {
					if (temp.equalsIgnoreCase((String) it.next()))
					{
						strAddr.setStreetType(temp);
						tempAddr = tempAddr.substring(0, idx);
						break;
					}
				}
			}
		} else {
			idx = tempAddr.indexOf(" ");
			if (idx > 0) {
				temp = tempAddr.substring(0, idx);

				res = BXResources.getPickListDescriptions(_srk.getExpressState().getDealInstitutionId(),"STREETTYPE", lang);
				for (Iterator it = res.iterator(); it.hasNext();) {
					if (temp.equalsIgnoreCase((String) it.next())){
						strAddr.setStreetType(temp);
						tempAddr = tempAddr.substring(idx + 1);
						break;
					}
				}	
			}
		}

		// 5) street name
		strAddr.setStreetName(tempAddr.trim());
	} 

}