package com.basis100.deal.util;

import java.util.*;
import java.text.*;

import com.basis100.resources.*;
import com.basis100.log.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.filogix.express.core.ExpressRuntimeException;

import config.*;


/**
 * A class representing a Business Calendar specific for the MOS.
 *
 * Initialization:
 * ---------------
 *
 * A one-time initialization is performed upon the first call to the constructor. This initialization
 * obtains the business day definition and holiday information. Initialization also determines the
 * time zone of the business calander. The time zone should match the time zone in which the
 * database is running which is considered the system timezone - all date/timestamp information
 * in the database is stored in the system timezone. Two versions of the constructor are
 * provided, one of which allows specification of the system timezone (integer id value - see
 * TimeZoneEntry entity). EST timezone (id=5) is the default used when time zone is not provided
 * (constructor without), or when the provided value does not match the primary key of a TimeZoneEntry
 * entiry.
 *
 * There is no provision for reloading business day and holidays information, or for changing the
 * system timezone.
 *
 *
 * Important Methods:
 *
 * DATE CALCULATIONS
 * -----------------
 *
 * . public Date calcBusDateo(Date baseDate, TimeZone callerTimeZone, int interval)
 *
 *   This method initializes the calendar to the date provided and adds an time interval (minutes) to
 *   arrive at an adjusted date. The adjusted date is returned as the result. The adjustment time
 *   interval may be negative to calculate a date in the past. The time zone passed is the time zone of
 *   the caller (e.g. in an application sense corresponds to the locale of the end user). The date
 *   passed is expressed in the systems' time zone.  The result is also a date expressed in
 *   the systems' time zone.
 *
 *   All date calculations take into account the MOS business day definition (see entity BusinessDay)
 *   and holidays (full and partial holidays - see entity Holidays). That is, date calculations are
 *   performed in accordance to the business calender.  This differs from normal calendar
 *   calculations in the following ways (following not an exhaustive list of rules but rather illustrative):
 *
 *     1) Initial date must fall on business hours (normal business hours or on the business hours
 *        defined for a partial holiday). The initial date is adjusted to the beginning (defined by
 *        time at start of business day - not calander day) of the next valid business day (normal or
 *        partial holiday) when it falls on a non-business day or is past the end of the current
 *        business day.  The initial date is adjusted to the beginning (as defined above) of the
 *        current business day if it falls on business day but with a time (e.g. 3:15am) before the
 *        start of the business day.
 *
 *     2) The adjustment interval is treated as business units (e.g. business hours and minutes). That
 *        is, date is advanced (or set back) based upon the hours and minutes in the business day (or
 *        partial holiday), not calander intervals. For example:
 *
 *              business day     ->  9:00am - 5:00pm
 *              date passed      ->  (a normal business day) at 4:00pm
 *              interval         ->  +2 Hours (120 minutes)
 *              caller time zone -> same as system time zone
 *
 *              calculated date        -> (same business day)  at 6:00pm (one hour beyond end of bus. day)
 *              adjusted for overflow  -> (next business day) at 10:00am
 *
 *
 *     3) If the (caller) time zone does not match the system's time zone the business day definition
 *        (and each holiday definition) is adusted to match the locale of the (user) time zone provided.
 *        For example, if the time zone provided is one hour behind the system time zone, and the
 *        business day is defined as 9:00am - 5:00pm, then the definition is modified to 8:00am - 4:00pm;
 *        e.g. 8:00am in the callers' time zone is 9:00am in the systems' time zone.
 *
 *   Various (overloaded) versions of calcBusDate() are provided. They differ in the manner in which
 *   callers' time zone, and the initial date/time initial date are specified - for example:
 *
 *        public Date calcBusDate(int year, int month, int day, int hour, int minute, int second,
 *                                int mosTimeZoneId, int interval)
 *
 *        where: mosTimeZoneId    - Mos Time Zone Entry Id (see below)
 *               year, month ...  - specification of the initial date as year, month, date, hour,
 *                                  minute and second.
 *
 *  Mos Time Zone Entry Ids
 *  -----------------------
 *
 *  A Mos Time Zone Entry Id is resolved by accessing a record in the TimeZoneEntry table. This is Mos
 *  picklist table with an additional column to hold the Java standard time zone Ids. Table format:
 *
 *    TimeZoneEntryId         - System assigned (picklist) values (see MOS Data Dictionary)
 *    TZEDescription          - Description
 *    TZESysId                - System/Java Time Zone Ids
 *
 * DATE REPRESENTATION
 * -------------------
 *
 *  . public String tzDisplay(Date dt, TimeZone callerTimeZone, String tfMaskOut)
 *  . public String tzDisplay(String dateStr, TimeZone callerTimeZone, String tfMaskIn, String tfMaskOut)
 *
 *  . ++ Many other overloaded version that take fewer arguments.
 *
 *  The basic idea behind the tzDisplay() methods is to accept a date expressed in the system (canendar's) time zone
 *  and return an string representation of the date expressed in the caller's time zone - simply put, the date
 *  provided is 'converted' to the callers' time zone. Conversion is guided by comparing appropriate TimeZone objects,
 *  one reflecting the system (see Initialization above) and another representing the caller; by this mechanism
 *  details such as Daylight Saving Time (DST) and other attributes are fully accounted for.
 *
 *  The caller's time zone may be provided directly (via a TimeZone object) or specified indirectly via a 'time zone
 *  id'. A time zone id is an integer value that must match one of the values in the TIMEZONEENTRYID column
 *  of the TIMEZONEENTRY, that is, the id must point to a record in the TIMEZONEENTRY table. Information in the
 *  matching record is used to to create an appropriate time zone object.
 *
 *  The date to be converted may be provided directly (via a Date object) or may be provided in a string representation.
 *  A SimpleDateFormat compatible pattern mask is required to interpret a date representation which is either provided
 *  directly (argument) or assumed to be the canendars' default input pattern mask. Of course the pattern mask (default or provided)
 *  must match the date representation provided. Pattern masks are disscussed separately below.
 *
 *  A pattern mask is also required for the resulting date representation, the output mask. Most version accept the
 *  output mask as a argument. In versions that do no accept the output mask the input mask (default or specified after
 *  time portion, if any) is used as the output mask.
 *
 *  The following verison is worth noting as no tine zone manipulation occurs (caller time zone assumed to match the
 *  system time zone) so instead it performs a pure representation translation service:
 *
 *      public String tzDisplay(String dateStr, String tfMaskIn, String tfMaskOut)
 *
 *      e.g., tzDisplay("24/02/2000 16:07:15", "dd/MM/yyyy", "yyyy-MM-dd hh:mm a")
 *
 *            Result: "2000-02-24 04:07 AM"
 *
 *  Date Representation Pattern Masks
 *  ---------------------------------
 *
 *  The syntax for time format masks matches the syntax used in java.text.SimpleDateFormat (see API
 *  documentation). Usage of the time zone formatter symbol (e.g. z, zzz, etc.) should not be specified in an
 *  output mask. This is because a SimpleDateFormat object is used to construct the final output representation and
 *  hence the time zone information would always match the default (system) time sone - it seems a SimpleDateFormat
 *  object always assumes the default time zone that it obtains from a calendar object it constructs internally!.
 *
 *  The following method can be used to obtain time zone information that can be used to augment the representation
 *  returned by tzDisplay.
 *
 *    public String getTimeZoneId(int callerTimeZoneId)
 *
 *    Returns contents of the TZESYSID column of the matching TIMEZONEENTRY table record; record where value
 *    matched value in IMEZONEENTRYID column. Note the is not necessarilly equivalent to what is returned via
 *    a call to the getId() method of the corresponding TimeZone object.
 *
 *    Example:  tzDisplay("Jul-05-2001 13:36:01", 9) + " " + getTimeZoneId(9);
 *
 *  The default pattern mask is "dd/MM/yyyy HH:mm:ss" but may be changed via setTfMask(). Actually the default may
 *  be specified without time formatting (wiothout the "HH:mm:ss") portion. In this case the time portion is
 *  automatically appended whenever the representaion provided is longer than the default mask. Example:
 *
 *               setTfMask("MMM-dd-YYYY");
 *
 *               tzDisplay("Jul-05-2001 13:36:01", 5);
 *
 *               Pattern mask used = "MMM-dd-YYYY HH:mm:ss"
 *
 *  WARNING - USAGE NOTES
 *  ---------------------
 *
 *  Caller must ensure thread safe behavior. This is easilly accomplished by ensuring
 *  that each caller instantiates a separate instance. A given caller may re-use an
 *  instance provided, again, usage is thread safe; e.g. the caller should not re-use
 *  the instance in multiple threads.
 *
 *  The decision to not support thread safe operation was based upon performance requirements, not
 *  technical limitations. For example, method synchronization could have been used for thread safe
 *  behavior. However, it was felt that such measures would have a overly negative effect on
 *  performance.
 *
 **/


public class BusinessCalendar extends GregorianCalendar
{

  SessionResourceKit srk;
    SysLogger logger;

  //following normally false - set to rtrue mainly for testing purposes
  boolean ignoreBeforeOrAfterBusinessHoursDetection = false;

  public void setIgnoreBeforeOrAfterBusinessHoursDetection(boolean b)
  {
    ignoreBeforeOrAfterBusinessHoursDetection = b;
  }

  //business day qualifiers (e.g. current time relative to dusiness day)
  static final int NOT_BUSINESS_DAY       = -1;
  static final int BEFORE_WORKING_HOURS   = 0;
  static final int DURING_WORKING_HOURS   = 1;
  static final int AFTER_WORKING_HOURS    = 2;

  //holiday qualifiers (e.g. current time relative to a holiday)
  static final int NOT_HOLIDAY                            = 0;
  static final int FULL_DAY_HOLIDAY                       = 1;
  static final int HALF_DAY_HOLIDAY_BEFORE_WORKING_HOURS  = 2;
  static final int HALF_DAY_HOLIDAY_DURING_WORKING_HOURS  = 3;
  static final int HALF_DAY_HOLIDAY_AFTER_WORKING_HOURS   = 4;

  // objects used for optimization  ...
  private static boolean initDone = false;
  private static Hashtable timeZoneAdjustedBusinessDays = new Hashtable();
  private static BusinessDay businessDay;

  private static TimeZone tz = TimeZone.getTimeZone("EST");  // system time zone

  private static Hashtable holidayTable;
  private static Hashtable timeZoneTable;
  private static Hashtable dateFormatters = new Hashtable();


  private String defaultTfMask       = "dd/MM/yyyy";
  private String tfOutputMask        = defaultTfMask;

  /** time and date variables. */
  private int currentTime;

  // Constructor:
  //
  // Upon first

  public BusinessCalendar(SessionResourceKit srk)
  {
    super();

    this.srk = srk;

    if (srk != null)
          logger = srk.getSysLogger();

    init(-1);

    setTimeZone(tz);
  }


  public BusinessCalendar(SessionResourceKit srk, int timeZoneEntryId)
  {
    super();

    this.srk = srk;

    if (srk != null)
          logger = srk.getSysLogger();

    init(timeZoneEntryId);

    setTimeZone(tz);
  }


  /**
    *   One time read of DB related business calander entities and setup of local (optimization)
    *   tables.
    *
    *   @return void
  **/

  public void init(int timeZoneEntryId)
  {
    if (initDone)
      return;

    try
    {
      Holidays h = new Holidays(srk);
      logger.debug("Getting all holidays ...");

      h.findByHolidayId();

      holidayTable = Holidays.getHolidayTable();
      logger.debug("Holidays amount: " + holidayTable.size());

      businessDay = new BusinessDay(srk);
      businessDay.calcStartEndTimes();

      TimeZoneEntry tze = new TimeZoneEntry(srk);

      tze.findByAll();

      timeZoneTable = tze.getTzTable();

      initDone = true;

      // set system time zone
      if (timeZoneEntryId != -1)
      {
          tze = (TimeZoneEntry)timeZoneTable.get(new Integer(timeZoneEntryId));

          if (tze == null)
            logError("Initialization - invalid time zone id (" + timeZoneEntryId + ") specified - ignored");
          else
          {
              try
              {
                  TimeZone sysTz = tze.getCustTimeZone();
                  tz = sysTz;
              }
              catch (Exception ts_exception)
              {
                  logError("Initialization - problem setting system time zone, specified id = " + timeZoneEntryId + ", lang.id = " + tze.getTzeSysId() + " - ignored");
                  logError(ts_exception);
              }
          }
      }
    }
    catch (Exception e)
    {
        logError("Initialization - unexpected exception during one-time initialization - operation may be compromised");
        logError(e);
    }

    try
    {
      logInfo("Initialization - system time in use = " + tz.getDisplayName());
      logInfo("Initialization - " + businessDay.toString());
    }
    catch (Exception e2)
    {
      ;
    }
  }

  private void logInfo(String msg)
  {
      if (logger == null)
        return;

      logger.info("BusinessCalander: " + msg);
  }

  private void logWarning(String msg)
  {
      if (logger == null)
        return;

      logger.warning("BusinessCalander: " + msg);
  }

  private void logError(String msg)
  {
      if (logger == null)
        return;

      logger.error("BusinessCalander: " + msg);
  }

  private void logError(Exception e)
  {
      if (logger == null)
        return;

      logger.error(e);
  }



/**
  * Get the default time format mask (e.g. used by some tzDisplay() calls)
  *
  **/

  public String getDefaultTfMask()
  {
    return defaultTfMask;
  }

/**
  * Get duration of business day in minutes
  *
  **/
  public int getBusinessDayDurationInMinutes()
  {
    return businessDay.getEndTime() - businessDay.getStartTime();
  }

/**
  * Set the default time format mask (e.g. used by some tzDisplay() calls)
  *
  **/

  public void setTfMask(String tfMask)
  {
    tfOutputMask = tfMask;
  }


/**
  * Returns the calculated date in EST format, based on the current date
  * and time interval.  Takes into account working business hours,
  * full-day and half-day holidays.
  *
  * @param     baseDate        Current date based on which the calculation will be performed
  * @param     callerTimeZone  Timezone corresponding to location of caller
  * @param     interval        Time interval in minutes
  *
  * @return    Date of interest
  */
    public Date calcBusDate(Date baseDate, int callerTimeZoneId, int interval)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));
	// SEAN issue #494 set province id to -1, means it will go through the old process.
	// Typically for calculating CommitmentExpiryDate and CommitmentReturnDate.
    return calcBusDate(baseDate, TimeZone.getTimeZone(tze.getTzeSysId()), interval, -1);
	// SEAN issue #494 END
  }
    // SEAN issue #494 added the provinceid as the prarm.
    //public Date calcBusDate(Date baseDate, int callerTimeZoneId, int interval)
  public Date calcBusDate(Date baseDate, int callerTimeZoneId, int interval, int provinceId)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));
    //return calcBusDate(baseDate, TimeZone.getTimeZone(tze.getTzeSysId()), interval)
    return calcBusDate(baseDate, TimeZone.getTimeZone(tze.getTzeSysId()), interval,
                       provinceId);
  }
    // SEAN issue #494 END

  // this version assumes caller time zone matches the system time zone
  public Date calcBusDate(Date baseDate, int interval)
  {
      // SEAN issue #494 temp solution, nobody invoke this method.
    return calcBusDate(baseDate, tz, interval, 0);
    // SEAN issue #494 END
  }

    // SEAN issue #494 added the provinceid as the param.
    //public Date calcBusDate(Date baseDate, TimeZone callerTimeZone, int interval)
  public Date calcBusDate(Date baseDate, TimeZone callerTimeZone, int interval,
                          int provinceId)
      // SEAN issue #494 END
  {
    setTime(baseDate);

    int offsetInMinutes = getTimeZoneOffset(callerTimeZone);

    // Bug fix -- Should set the BusDay to the equivalent time of in system time zone
    //  e.g. if System.StartTime = 9am (EST), and the TimeZone offset = -1hr (CST)
    //     ==> The BusDay.StartTime = 10am(EST) == 9am(CST)
    //    -- By BILLY 13Dec2001
    BusinessDay callerBusDay = adjustBusinessDayHours(-offsetInMinutes);

    // SEAN issue #494 add the province id parameter.
    quickCalc(interval, callerBusDay, offsetInMinutes, provinceId);
    // SEAN issue #494 END

    return getTime();
  }

  /**
     * Returns the calculated date in EST format, based on the current date
     * and time interval.  Takes into account working business hours,
     * full-day and half-day holidays.
     *
     * @param     year            Current year.
     * @param     month           Current month.
     * @param     day             Current day.
     * @param     hour            Current hour.
     * @minute    minute          Current minute.
     * @second    second          Current second.
     * @param     callerTimeZone  Timezone corresponding to location of caller
     * @param     interval        Time interval in minutes
     * @return    Date of interest
  */

  public Date calcBusDate(int year, int month, int day, int hour, int minute, int second, int callerTimeZoneId, int interval)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));
    return calcBusDate(year, month, day, hour, minute, second, TimeZone.getTimeZone(tze.getTzeSysId()), interval);
  }

   public Date calcBusDate(int year, int month, int day, int hour, int minute, int second, TimeZone callerTimeZone, int interval)
   {
    set(year, month, day, hour, minute, second);

    int offsetInMinutes = getTimeZoneOffset(callerTimeZone);

    BusinessDay callerBusDay = adjustBusinessDayHours(offsetInMinutes);

    // SEAN issue #494 temp solution , nobody invoke this method.
    quickCalc(interval, callerBusDay, offsetInMinutes, -1);
    // SEAN issue #494 END

    return getTime();
  }


  /**
     * Returns the calculated date in EST format, based on the current date
     * and time interval.  Takes into account working business hours,
     * full-day and half-day holidays.
     *
     * @param     year      Current year.
     * @param     month     Current month.
     * @param     day       Current day.
     * @param     hour      Current hour.
     * @minute    minute    Current minute.
     * @param     timeZone  Timezone in which the calculation is present
     * @param     interval  Time interval in minutes
     * @return    Date of interest
  */

  public Date calcBusDate(int year, int month, int day, int hour, int minute, int timeZoneId, int interval)
  {
    return calcBusDate(year, month, day, hour, minute, 0, timeZoneId, interval);
  }

  public Date calcBusDate(int year, int month, int day, int hour, int minute, TimeZone timeZone, int interval)
  {
    return calcBusDate(year, month, day, hour, minute, 0, timeZone, interval);
  }


  /*
   * Given a date check to ensure it is in the future relative to the current system date.
   *
   * Returns the date passed if in the future, or null otherwise.
   *
   **/

  public Date futureDate(Date dt)
  {
      if (dt == null)
          return null;

      if (dt.compareTo(new Date()) > 0)
          return dt;

      return null;
  }

  /*
   * Given a date and a interval of time (minutes) return the next future date determined by
   * adding the interval to the date provided as necessary. If the date provided is already in
   * future it is returned immediately.
   *
   * IMPORTANT
   *
   * Date calculation are preformed based on this business calendar not simply on a calendar
   * basis (see documentation at top of class for details)
   *
   * Call is expensive when:
   *   . date passed is in well in past
   *   . interval is short
   *        example: date is two months out of date andf internval is 1H
   *
   **/

  public Date nextFutureDate(Date dt, int callerTimeZoneId, int interval)
  {
      if (dt == null)
          return null;

      if (futureDate(dt) != null)
          return dt;  // already in future

      // if invalid interval specified use 1 hour
      if (interval <= 0)
          interval = 60;

      Date now = new Date();

      for (;;)
      {
        dt = calcBusDate(dt, callerTimeZoneId, interval);

        if (dt.compareTo(now) > 0)
          break;
      }

      return dt;
  }

  /*
   * Given a date and a interval of time (minutes) return the 'back' date defined as:
   *
   *            back date = date - interval
   *
   * If the back date is before the current system time null is returned.
   *
   * IMPORTANT
   *
   * The date passed is assumed to be in the system's time zone.
   *
   * Date calculations are preformed based on this business calendar not simply on a calendar
   * basis (see documentation at top of class for details)
   *
   **/

  public Date backDate(Date dt, int callerTimeZoneId, int interval)
  {
      if (futureDate(dt) == null)
        return null;

      // if invalid interval specified use 1 hour
      if (interval <= 0)
          interval = 60;

      dt = calcBusDate(dt, callerTimeZoneId, -interval);

      return futureDate(dt);
  }



/**
  *  tzDisplay() methods - see class documentation.
  *
  **/

  public String tzDisplay(String dateStr, TimeZone callerTimeZone)
  {
    return tzDisplay(dateStr, callerTimeZone, null, null);
  }

  public String tzDisplay(String dateStr, int callerTimeZoneId)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));

    return tzDisplay(dateStr, tze.getCustTimeZone(), null, null);
  }

  public String tzDisplay(String dateStr, TimeZone callerTimeZone, String tfMaskIn)
  {
    return tzDisplay(dateStr, callerTimeZone, tfMaskIn, null);
  }

  public String tzDisplay(String dateStr, int callerTimeZoneId, String tfMaskIn)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));

    return tzDisplay(dateStr, tze.getCustTimeZone(), tfMaskIn, null);
  }

  public String tzDisplay(String dateStr, int callerTimeZoneId, String tfMaskIn, String tfMaskOut)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));

    return tzDisplay(dateStr, tze.getCustTimeZone(), tfMaskIn, tfMaskOut);
  }

  public String tzDisplay(String dateStr, String tfMaskOut)
  {
    return tzDisplay(dateStr, tz, null, tfMaskOut);
  }

  // straight format conversion - no time zone adjustments
  public String tzDisplay(String dateStr, String tfMaskIn, String tfMaskOut)
  {
    return tzDisplay(dateStr, tz, tfMaskIn, tfMaskOut);
  }

  // ALL ABOVE ARRIVE HERE!

  public String tzDisplay(String dateStr, TimeZone callerTimeZone, String tfMaskIn, String tfMaskOut)
  {
    if(tfMaskIn == null)
      tfMaskIn = getDefaultTfMask();

    Date dt = dtStrToDate(dateStr, tfMaskIn);

    if (tfMaskOut == null)
      tfMaskOut = getOutputTfMask(); // was set to match mask in via dtStrToDate()

    return tzDisplay(dt, callerTimeZone, tfMaskOut);
  }


  public String tzDisplay(Date dt, String tfMaskOut)
  {
    return tzDisplay(dt, tz, tfMaskOut);
  }

  public String tzDisplay(Date dt, TimeZone callerTimeZone)
  {
    return tzDisplay(dt, callerTimeZone, null);
  }

  public String tzDisplay(Date dt, int callerTimeZoneId)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));

    return tzDisplay(dt, tze.getCustTimeZone(), null);
  }

  public String tzDisplay(Date dt, int callerTimeZoneId, String tfMaskOut)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));

    return  tzDisplay(dt, tze.getCustTimeZone(), tfMaskOut);
  }

  // ALL ABOVE ARRIVE HERE

  public String tzDisplay(Date dt, TimeZone callerTimeZone, String tfMaskOut)
  {
    // specifc output mask specified - use instead of input/default
    if(tfMaskOut != null)
      setOutputTfMask(tfMaskOut);
    else
      setOutputTfMask(getDefaultTfMask());

    setTime(dt);

    int offset = getTimeZoneOffset(callerTimeZone);

    if (offset != 0)
      add(MINUTE, offset);

    dt = getTime();

    SimpleDateFormat df = getDateFormatter(getOutputTfMask());

    return df.format(dt);
  }

  public String getTimeZoneId(int callerTimeZoneId)
  {
    TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));

    //return tze.getTzeSysId();
    return tze.getTzeCustomId();
  }


/**
  * Gets day number.  The day number is calculated based on the
  * increment of 400 for each year, starting on January 1, 1970
  * and the day of the current year
  * @param     date
  * @return int dayNumber
  **/
   public int getDayNumber (java.util.Date theDate)
   {
    Calendar theDateInCal = Calendar.getInstance();
    theDateInCal.setTime(theDate);
    int dayNumber = (theDateInCal.get(theDateInCal.YEAR) - 1970)*400 + theDateInCal.get(theDateInCal.DAY_OF_YEAR);

    return dayNumber;
  }

/**
  *  Sets the business week starting and ending days of the week and time.
  *
  *  @param    startDay  Start day of the week, example: MONDAY
  *  @param    endDay    End day of the week, example: FRIDAY
  *  @param    startHour
  *  @param    startMinute
  *  @param    endHour
  *  @param    endMinute
  *
  *  @return a void
  **/

  public void setBusinessWeek(String startDay, String endDay, int startHour, int startMinute, int endHour, int endMinute)
  {
    businessDay.setEndHour(endHour);
    businessDay.setEndMinute(endMinute);
    businessDay.setStartHour(startHour);
    businessDay.setStartMinute(startMinute);
    businessDay.setWeekEnd(endDay);
    businessDay.setWeekStart(startDay);
    try
    {
      businessDay.ejbStore();
    }
      catch (Exception e)
    {
    }
  }


/**
  *  Sets holiday's Id, name, date, starting and ending hours
  *  statuary holiday, recurring and span weekend values.
  *
  *  @param    holidayId
  *  @param    holidayName
  *  @param    holidayDate
  *  @param    startHour
  *  @param    startMinute
  *  @param    endHour
  *  @param    endMinute
  *  @param    statuaryHoliday
  *  @param    recurring
  *  @param    spanWeekend
  *
  *  @return a void
  **/

  public void setHoliday (int holidayId, String holidayName, Date holidayDate, int startHour, int startMinute, int endHour, int endMinute, String statuaryHoliday,String recurring, String spanWeekend) throws Exception
  {
    Holidays h = (Holidays)holidayTable.get(new Integer(holidayId));

    try
    {
      if (h == null)
      {
        // not an existing holiday .. create new
        h = new Holidays(srk);
        h = h.create(holidayId);
        holidayTable.put(new Integer(getDayNumber(holidayDate)), h);
      }

      h.setHolidayId(holidayId);
      h.setHolidayName(holidayName);
      h.setHolidayDate(holidayDate);
      h.setStartHour(startHour);
      h.setStartMinute(startMinute);
      h.setEndHour(endHour);
      h.setEndMinute(endMinute);
      h.setStatuaryHoliday(statuaryHoliday);
      h.setRecurring(recurring);
      h.setSpanWeekend(spanWeekend);

      h.ejbStore();
    }
    catch (Exception e) {
      throw e;
    }
  }


/**
  *  Removes a holiday from the database based on the holidayName.
  *
  *  @param    holidayName
  *  @return a void
  **/

  public void removeHoliday (String holidayName) throws Exception
  {
    Holidays h = new Holidays(srk);

    try
    {

      h = h.findByName(holidayName);

      if (h == null)
        return; // no such

      int id = h.getHolidayId();

      h.remove();

      holidayTable.remove(new Integer(this.getDayNumber(h.getHolidayDate())));
    }
    catch (Exception e)
    {
      throw e;
    }
  }


/**
  *  Removes a holiday from the database based on the holidayId.
  *
  *  @param    holidayId
  *  @return a void
  **/

  public void removeHoliday(int holidayId) throws Exception
  {
    Holidays h = new Holidays(srk);

    try
    {
      h = h.findByPrimaryKey(new HolidaysPK(holidayId));

      if (h == null)
        return; // no such

      int id = h.getHolidayId();

      h.remove();

      holidayTable.remove(new Integer(getDayNumber(h.getHolidayDate())));
    }
    catch (Exception e)
    {
      throw e;
    }
  }

/**
  *  Returns the current calendar.
  *
  *  @return a Calendar
  **/

  public Calendar getCalendar()
  {
    return getInstance();
  }


/**
  *    ---------------- IMPLEMENTATION ---------------
  *
  **/


/**
  *
  * Return relative time zone offset between the systems' time zone (time zone associate with
  * this instance) and another time zone, specifically:
  *
  *              (other time zone offset to GMT) - (systems' time zone offset to GMT)
  *
  * Date must be set before the call. The calculation takes in account the usage
  * (or non-usage as the aase may be) of DST in the time zones compared.
  *
  * @Return Relative time zone offset in minutes (minutes other time zone is ahead of system time zone [e.g.
  *         60 minutes = 1 hour ahead], result will be negative if the other time zone is behind the
  *         system time zone).
  *
  **/

  private int getTimeZoneOffset(TimeZone tzOther)
  {
/* ALERT -- following sometimes throws exception (likely due to last parm in getOffset() call
            [whatever the the HELL is it!] but alt. soln. looks better anyway.

    int sysOffset   =   tz.getOffset(this.get(ERA), this.get(YEAR), this.get(MONTH), this.get(DAY_OF_MONTH), this.get(DAY_OF_WEEK), 0);
    int otherOffset =   tzOther.getOffset(this.get(ERA), this.get(YEAR), this.get(MONTH), this.get(DAY_OF_MONTH), this.get(DAY_OF_WEEK), 0);
*/

    int sysOffset = get(ZONE_OFFSET) + get(DST_OFFSET);

    Calendar calOther = Calendar.getInstance();
    calOther.setTimeZone(tzOther);
    calOther.setTime(getTime());

    int otherOffset = calOther.get(ZONE_OFFSET) + calOther.get(DST_OFFSET);

    return (otherOffset - sysOffset)/60000;
  }

/**
  * Returns a time zone adjusted business day object. This is best understook by a
  * simplified example:
  *
  *   Let business day be defined as 9:00am - 5:00pm; by definition the business day is relative to
  *   the systems' time zone (e.g. EST).
  *
  *   Now suppose the user is located in a time zone that is one hour ahead the systems' time zone.
  *
  *   In that case the users' business day, expressed in the systems' time zone is:
  *
  *           10:00am - 6:00pm
  *
  *           i.e.: 10:00am in the user time zone is equivalent to 9:00am in the systems' time zone
  *                 because the users' location is one hour ahead.
  *
  *  WARNING:
  *
  *  While method does allow for rollover (or rollback) of hour of day (start and end) it does not
  *  accomondate date (day of week) adjustments. It is assumed that the business day adjustments for
  *  all time zones covered would not cause a date rollover
  *
  * @param
  * @return
  **/

  private BusinessDay adjustBusinessDayHours(int userTimeZoneOffsetInMinutes)
  {
    if (userTimeZoneOffsetInMinutes == 0)
      return businessDay;

    // check for adjusted business day already exists
    BusinessDay adjDay = (BusinessDay)timeZoneAdjustedBusinessDays.get(new Integer(userTimeZoneOffsetInMinutes));

    if (adjDay != null)
      return (BusinessDay)adjDay;

    // create time zone adjusted business day objects
    adjDay = businessDay.privateCopy();

    // whole hours adjustment
    int hours = userTimeZoneOffsetInMinutes / 60;  // whole hours (+/-)

    if (!adjDay.addToStartAndEndHour(hours)) {	
    	String message = "Invalid BusinessDay (HOUR 0-23 only): " + adjDay;
    	logger.error(message);
    	logger.error("Business hour start/end must be within 0 and 23.");
    	logger.error("Express does not support BusinessDay with time zones that cause date rollover/rollback.");
    	throw new ExpressRuntimeException(message);
    }

    // adds minutes, adjusts hours if necessary
    int minutes = userTimeZoneOffsetInMinutes - (hours * 60);

    if (minutes != 0)
    {
      if (adjDay.addToStartMinute(minutes) == false)
      {
        // resulting minutes outside range - adjust start hour
        int resMin = adjDay.getStartMinute();

        if (resMin >= 60)
        {
          adjDay.addToStartHour(1);
          adjDay.addToStartMinute(-60);
        }
        else
        {
          adjDay.addToStartHour(-1);
          adjDay.addToStartMinute(60);
        }
      }

      if (adjDay.addToEndMinute(minutes) == false)
      {
        // resulting minutes outside range - adjust start hour
        int resMin = adjDay.getEndMinute();

        if (resMin >= 60)
        {
          adjDay.addToEndHour(1);
          adjDay.addToEndMinute(-60);
        }
        else
        {
          adjDay.addToEndHour(-1);
          adjDay.addToEndMinute(60);
        }
      }
    }

    adjDay.calcStartEndTimes();

    // add new to queue (for subsequent optimized lookup)
    timeZoneAdjustedBusinessDays.put(new Integer(userTimeZoneOffsetInMinutes), adjDay);

    return adjDay;
  }

/**
  * Returns a time zone adjusted holiday object. Functionality similar to adjustBusinessDayHours().
  *
  *  WARNING:
  *
  *  While method does allow for rollover (or rollback) of hour of day (start and end) it does not
  *  accomondate date (day of week) adjustments. It is assumed that the business day adjustments for
  *  all time zones covered would not cause a date rollover
  *
  * @param
  * @return
  **/
  private Holidays adjustHolidayHours(Holidays holiday, int userTimeZoneOffsetInMinutes)
  {
    if (userTimeZoneOffsetInMinutes == 0)
      return holiday;

    // create time zone adjusted business day objects
    Holidays adjDay = holiday.privateCopy();

    // whole hours adjustment
    int hours = userTimeZoneOffsetInMinutes / 60;  // whole hours (+/-)

    adjDay.addToStartAndEndHour(hours);

    // adds minutes, adjusts hours if necessary
    int minutes = userTimeZoneOffsetInMinutes - (hours * 60);

    if (minutes != 0)
    {
      if (adjDay.addToStartMinute(minutes) == false)
      {
        // resulting minutes outside range - adjust start hour
        int resMin = adjDay.getStartMinute();

        if (resMin >= 60)
        {
          adjDay.addToStartHour(1);
          adjDay.addToStartMinute(-60);
        }
        else
        {
          adjDay.addToStartHour(-1);
          adjDay.addToStartMinute(60);
        }
      }

      if (adjDay.addToEndMinute(minutes) == false)
      {
        // resulting minutes outside range - adjust start hour
        int resMin = adjDay.getEndMinute();

        if (resMin >= 60)
        {
          adjDay.addToEndHour(1);
          adjDay.addToEndMinute(-60);
        }
        else
        {
          adjDay.addToEndHour(-1);
          adjDay.addToEndMinute(60);
        }
      }
    }

    adjDay.calcStartEndTimes();

    return adjDay;
  }

/**
  * Checks for whether a given date is a business day, a holiday,
  * a half-holiday.  Also calculates the spill over time if a job
  * can not be completed within given day's business hours.  Sets the
  * calendar to the proper date.
  *
  * @param     interval  Time interval in minutes
  * @return
  */
    // SEAN issue #494 change to add province ID parameter.
    //private int quickCalc (int interval, BusinessDay userBusDay, int userTimeZoneOffsetInMinutes)
    //{
    private int quickCalc (int interval, BusinessDay userBusDay,
                           int userTimeZoneOffsetInMinutes, int provinceId) {
        // issue #494 END
    /** next day available time in minutes */
    int remainingInterval, busDayQual, hldyQual;

    currentTime = get(HOUR_OF_DAY) * 60 + get(MINUTE);

    HolidayHolder hh = new HolidayHolder();

    busDayQual = isBusinessDay(userBusDay);
    // SEAN issue #494 changed to use the method with province id param.
    //hldyQual   = isHoliday(userTimeZoneOffsetInMinutes, hh);
    hldyQual   = isHoliday(userTimeZoneOffsetInMinutes, provinceId, hh);
    // SEAN issue #494 END

    if (busDayQual == NOT_BUSINESS_DAY || hldyQual == FULL_DAY_HOLIDAY)
    {
      // move to the next business day at first business hour
      //System.out.println("not a business day or a full-day holiday move to the next business day at first business hour");

      if (interval < 0 ) {
        previousBusinessDayEndTime(userBusDay);
      }
      else {
        nextBusinessDay(userBusDay);
      }

      // SEAN issue #494 add the province id parameter.
      return quickCalc(interval, userBusDay, userTimeZoneOffsetInMinutes, provinceId);
      // SEAN issue #494 END

    }

    if (hldyQual != NOT_HOLIDAY)
    {
      // partial holiday ...

      Holidays h = hh.h;  // get the holiday object 'returned' via the isHoliday() call

      if (hldyQual == HALF_DAY_HOLIDAY_BEFORE_WORKING_HOURS)
      {
        //System.out.println("holiday before hours");
        if (interval < 0) {
          previousBusinessDayEndTime(userBusDay);
        }
        else {
          sameHolidayDay(h);
        }
        // SEAN issue #494 add the province id parameter.
        return quickCalc(interval, userBusDay, userTimeZoneOffsetInMinutes, provinceId);
        // SEAN issue #494 END
      }

      if (hldyQual == HALF_DAY_HOLIDAY_AFTER_WORKING_HOURS)
      {
        //System.out.println("holiday after working hours");
        if (interval < 0) {
          sameHolidayDayEndTime(h);
        }
        else {
          nextBusinessDay(userBusDay);
        }
        // SEAN issue #494 add the province id parameter.
        return quickCalc(interval, userBusDay, userTimeZoneOffsetInMinutes, provinceId);
        // SEAN issue #494 END
      }

      // (must be) during business hours (of the holiday) ... (determine remaining time to end of day,
      // or for interval < 0 time to the beginning of the holiday)

      // System.out.println("partial holiday  - within working hours");
      if (interval < 0 ) {
        remainingInterval = interval + (currentTime - h.getStartTime());
      }
      else {
        remainingInterval = interval - (h.getEndTime() - currentTime);
      }
      // System.out.println("Remaining Interval = " + remainingInterval);

    }
    else
    {
      // business day ...

      if (busDayQual == BEFORE_WORKING_HOURS)
      {
        // System.out.println("business day before opening hours move to the beginning of the business day or end of business day if interval is less than 0");
        if (interval < 0) {
          previousBusinessDayEndTime(userBusDay);
        }
        else {
          sameBusinessDay(userBusDay);
        }
        // SEAN issue #494 add the province id parameter.
        return quickCalc(interval, userBusDay, userTimeZoneOffsetInMinutes, provinceId);
        // SEAN  issue #494 END
      }

      if (busDayQual == AFTER_WORKING_HOURS)
      {
        // System.out.println("holiday after working hours");
        if (interval < 0) {
          sameBusinessDayEndTime(userBusDay);
        }
        else {
          nextBusinessDay(userBusDay);
        }

        // SEAN issue #494 add the province id parameter.
        return quickCalc(interval, userBusDay, userTimeZoneOffsetInMinutes, provinceId);
        // SEAN issue #494 END
      }

      // (must be) during business hours (normal business day) ... (determine remaining time to end of day
      // or in case of interval < 0 remaining time to the beginning of the day

      // System.out.println("business day - within working hours");

      if (interval < 0) {
        remainingInterval = interval + (currentTime - userBusDay.getStartTime());
      }
      else {
        remainingInterval = interval - (userBusDay.getEndTime() - currentTime);
      }
    }

    if (interval > 0 && remainingInterval > 0)
    {
      // move to beginning of following day and apply remaining inverval

      // System.out.println("nextDay hours to the next business day");
      nextBusinessDay(userBusDay);
      // SEAN issue #494 add the province id parameter.
      return quickCalc(remainingInterval, userBusDay, userTimeZoneOffsetInMinutes, provinceId);
      // SEAN issue #494 END
    }
    else if (interval < 0 && remainingInterval < 0)
    {
      //move to end of the previous day and apply remaining interval
      previousBusinessDayEndTime(userBusDay);
      // SEAN issue #494 add the province id parameter.
      return quickCalc(remainingInterval, userBusDay, userTimeZoneOffsetInMinutes, provinceId);
      // SEAN issue #494 END
    }

    // after interval applied we remain in calander day (so we are done!)
    add(MINUTE,interval);

    return 0;  // only to allow cleaner recursive calls (i.e. want method to return value!)
  }

/**
  *  Checks whether give date is within business week
  *  and within business hours.
  *  Returns NOT_BUSINESS_DAY if not a business day
  *  Returns BEFORE_WORKING_HOURS if business day before working hours
  *  Returns AFTER_WORKING_HOURS if business day during working hours
  *  Returns DURING_WORKING_HOURS if business day after working hours
  *
  *  @return   int
  */

  private int isBusinessDay(BusinessDay dDay)
  {
      // check whether within business week

      int dayNumber = get(DAY_OF_WEEK);

      if ((dayNumber >= dDay.getWeekStartDayNum()) && (dayNumber <= dDay.getWeekEndDayNum()))
      {
        if (ignoreBeforeOrAfterBusinessHoursDetection == true)
          return DURING_WORKING_HOURS;
        else if (currentTime < dDay.getStartTime())
          return BEFORE_WORKING_HOURS;
        else if (currentTime > dDay.getEndTime())
          return AFTER_WORKING_HOURS;
        else
          return DURING_WORKING_HOURS;
      }
      else
       {
        // not a business day
        //System.out.println("BusinessCalendar, isBusinessDay, Not a business day");
        return NOT_BUSINESS_DAY;
      }
  }


/**
  *  Checks whether a given day is a holiday.
  *  Returns int based on what kind of a 'Holiday Day'
  *  it is.
  *  Returns NOT_HOLIDAY if not a holiday
  *  Returns FULL_DAY_HOLIDAY if a full-day holiday
  *  Returns HALF_DAY_HOLIDAY_BEFORE_WORKING_HOURS if a half-day holiay before working hours
  *  Returns HALF_DAY_HOLIDAY_DURING_WORKING_HOURS if a half-day holiday during working hours
  *  Returns HALF_DAY_HOLIDAY_AFTER_WORKING_HOURS if a half-day holiday after working hours
  *
  *  @return  int
  */
  private int isHoliday(int userTimeZoneOffsetInMinutes, HolidayHolder hh)
  {

    Holidays h = (Holidays)holidayTable.get(new Integer(getDayNumber(getTime())));

    if (h == null)
    {
      return NOT_HOLIDAY;
    }

    /**the Holidays object from the hashtable*/

    if (h.getStatuaryHoliday().toUpperCase().startsWith("Y"))
    {
      return FULL_DAY_HOLIDAY;
    }

    if (ignoreBeforeOrAfterBusinessHoursDetection == true)
      return HALF_DAY_HOLIDAY_DURING_WORKING_HOURS;

    h = adjustHolidayHours(h, userTimeZoneOffsetInMinutes);

    hh.h = h; // caller will need to access

    if (currentTime < h.getStartTime())
    {
      return HALF_DAY_HOLIDAY_BEFORE_WORKING_HOURS;
    }
    else if (currentTime > h.getEndTime())
    {
      /**return 4 if if is a half-day holiday after hours*/
      return HALF_DAY_HOLIDAY_AFTER_WORKING_HOURS;
    }

    return HALF_DAY_HOLIDAY_DURING_WORKING_HOURS;
  }

    // SEAN issue #494 added the province id parameter to check for each specific
    // province.  if provinceId = -1 or < 0, invoke the legacy method.
    protected int isHoliday(int userTimeZoneOffsetInMinutes, int provinceId,
                          HolidayHolder hh) {

		// for back compatable reason.
		if (provinceId < 0) {
			return isHoliday(userTimeZoneOffsetInMinutes, hh);
		}

        Holidays h = (Holidays)holidayTable.get(new Integer(getDayNumber(getTime())));

        if (h == null) {
            logger.debug("Totally not a holiday!");
            return NOT_HOLIDAY;
        }

        if ((h.getProvinceId() > 0) && (h.getProvinceId() != provinceId)) {
            logger.info("Not this province's holiday");
            return NOT_HOLIDAY;
        }

        logger.info("OK... It is a holiday. Let's check the the holiday type.");

        hh.h = h; // caller will need to access

        if (h.getStatuaryHoliday().toUpperCase().startsWith("Y")) {
          return FULL_DAY_HOLIDAY;
        }

        if (ignoreBeforeOrAfterBusinessHoursDetection == true)
          return HALF_DAY_HOLIDAY_DURING_WORKING_HOURS;

        h = adjustHolidayHours(h, userTimeZoneOffsetInMinutes);

        if (currentTime < h.getStartTime()) {
          return HALF_DAY_HOLIDAY_BEFORE_WORKING_HOURS;
        } else if (currentTime > h.getEndTime()) {
          /**return 4 if if is a half-day holiday after hours*/
          return HALF_DAY_HOLIDAY_AFTER_WORKING_HOURS;
        }

        return HALF_DAY_HOLIDAY_DURING_WORKING_HOURS;
    }
    // SEAN issue #494 END

/**
  * Helper class to allow isHoliday() to return a holoday object
  *
  **/
  class HolidayHolder
  {
    Holidays h = null;
  }

/**
  *  Forwards the calendar to the next day.  Sets the hour at the
  *  business day starting time.
  *
  *  @return
  */

  private void nextBusinessDay(BusinessDay db)
  {
    add(DAY_OF_MONTH, 1);
    set(HOUR_OF_DAY, db.getStartHour());
    set(MINUTE, db.getStartMinute());
    set(SECOND, 0);
    set(MILLISECOND, 0);
  }

  /** Revision March 14, 2000
    * Added method
  **/

  /**
  *  Forwards the calendar to the next day.  Sets the hour at the
  *  business day starting time.
  *
  *  @return
  */

  private void previousBusinessDayEndTime(BusinessDay db)
  {
    add(DAY_OF_MONTH, - 1);
    set(HOUR_OF_DAY, db.getEndHour());
    set(MINUTE, db.getEndMinute());
    set(SECOND, 0);
    set(MILLISECOND, 0);
  }

/**
  *  Forwards the calendar's time to the business day's starting hour
  *  and minutes
  *
  *  @return
  **/

  public void sameBusinessDay(BusinessDay db)
  {
    set(get(YEAR), get(MONTH), get(DATE), db.getStartHour(), db.getStartMinute());
    set(SECOND, 0);
    set(MILLISECOND, 0);
  }


  /**
  *  Forwards the calendar's time to the business day's ending hour
  *  and minutes
  *
  *  @return
  **/

  public void sameBusinessDayEndTime(BusinessDay db)
  {
    set(get(YEAR), get(MONTH), get(DATE), db.getEndHour(), db.getEndMinute());
    set(SECOND, 0);
    set(MILLISECOND, 0);
  }


/**
  *  Forwards the calendar's time to the current holiday starting hour
  *  and minutes
  *
  *  @return
  **/

  private void sameHolidayDay(Holidays h)
  {
    set(get(YEAR),get(MONTH), get(DATE), h.getStartHour(), h.getStartMinute());
    set(SECOND, 0);
    set(MILLISECOND, 0);
  }

  /**
  *  Forwards the calendar's time to the current holiday ending hour
  *  and minutes
  *
  *  @return
  **/

  private void sameHolidayDayEndTime(Holidays h)
  {
    set(get(YEAR),get(MONTH), get(DATE), h.getEndHour(), h.getEndMinute());
    set(SECOND, 0);
    set(MILLISECOND, 0);
  }

/**
  * Utility to return Date object for date text representation in a specified mask - mask manipulation
  * possible via call to getTfMaskToUse().
  *
  * NOTE
  * ----
  *
  * Calls setOutputTfMask() to set the (possibly manipulated) time format mask as the output mask (e.g.
  * to be used by tzDisplay()) - See documentation at top regarding thread safe requirements for this
  * class!
  *
  * WARNING
  * -------
  *
  * If a failiure is encountered (e.g. nonesense time format mask, representation to mask mismatch)
  * returns dat object for current time.
  **/

  private Date dtStrToDate(String dateStr, String tfMask)
  {
    Date dt = null;
    try
    {
      tfMask = getTfMaskToUse(dateStr, tfMask);

      // set resultinmg mask as output mask
      setOutputTfMask(tfMask);

      SimpleDateFormat df = getDateFormatter(tfMask);

      dt = df.parse(dateStr, new ParsePosition(0));
    }
    catch (Exception e) {;}

    if (dt != null)
      return dt;

    // formatter had a problem - replace it with a new one
    dateFormatters.remove(tfMask);
    ///// getDateFormatter(tfMask);

    // error handling - switch to default time format mask, return date obj for current system time

    setOutputTfMask(getDefaultTfMask());
    return new Date();
  }

  private SimpleDateFormat getDateFormatter(String mask)
  {
    // check for existing date formatter
    SimpleDateFormat df = (SimpleDateFormat)dateFormatters.get(mask);

    if (df == null)
    {
      // make one and save for re-use
      df = new SimpleDateFormat(mask);

      dateFormatters.put(mask, df);
    }

    return df;
  }

/**
  * Utility to chech time format mask against length of date representation - if latter is longer adds
  * " HH:mm:ss" to time format string (if no ':' in original
  *
  **/

  private String getTfMaskToUse(String dateStr, String tfMask)
  {
    if (dateStr.length() <= tfMask.length())
      return tfMask;

    if (tfMask.indexOf(':', 0) == -1)
      return tfMask + " HH:mm:ss";

    return tfMask;
  }

/**
  * set the output time format mask to be used in tzDisplay() calls (where specific output mask
  * not specified).
  *
  * WARNING
  * -------
  *
  * The implementation is intentionaly non-thread safe - see documentation at top for usage notes
  *
  **/

  private void setOutputTfMask(String tfMask)
  {
    tfOutputMask = tfMask;
  }

/**
  * get the default time format mask used by tzDisplay()
  *
  **/

  private String getOutputTfMask()
  {
    return tfOutputMask;
  }

}
