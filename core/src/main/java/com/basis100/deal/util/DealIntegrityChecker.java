package com.basis100.deal.util;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.FinderException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

/**
 * @since 3.3
 */
public class DealIntegrityChecker
{
  private int     dealId      = 0;
  private int     copyId      = 0;
  private int     numCopies   = 0;
  private int     lim         = 0;
  private int     key         = 0;
  private int     lastCopyId  = 0;
  private String  copyType    = "T";

    /**
     * constructor.
     */
    public DealIntegrityChecker() {
    }

  /**
   * Perform integrity checks and corrections.
   *
   * PROGRAMMER NOTE
   *
   * Note the order of the checks/corrections should not be changed as it is known that some dependencies exists -
   * this also implies that care must be taken before eliminating a method.
   *
   **/

  public void performIntegriryChecks(SessionResourceKit srk, SysLogger logger, boolean echoToConsole)
  {
      boolean auditOn = true;

      try
      {
      // ensure no auditing of cleanup operations

      auditOn = srk.isAuditOn();

      srk.setAuditOn(false);

      ensureOneGoldCopy(srk, logger, echoToConsole);

      ensureGoldCopyExists(srk, logger, echoToConsole);

      ensureGoldCopyCorrect(srk, logger, echoToConsole);

      ensureMasterDealCorrect(srk, logger, echoToConsole);

      deleteTransactionalCopies(srk, logger, echoToConsole);
      
      // Catherine for AVM ------------- begin -------------
      if (PropertiesCache.getInstance().getInstanceProperty("com.deal.util.integritychecker.delete.orphan.responses", "N").equals("Y")) 
      {
        deleteorphanResponses(srk, logger, echoToConsole);
      }
      // Catherine for AVM ------------- ends -------------
      
      srk.setAuditOn(auditOn);
      }
      catch (Exception e)
      {
            srk.cleanTransaction();

            srk.setAuditOn(auditOn);

            logger.error("Exception @DealIntegrityChecker.performIntegrityChecks: - ignored");
            logger.error(e);
      }
  }

  // Catherine for AVM ------------- begin -------------
  /*
   * We need this method because the referential link between the Request and Response is broken 
   * (although Response has a requestId attribute, that   
   * to ensure there is no orphan responses left in the database  
   */
  public void deleteorphanResponses(SessionResourceKit srk, SysLogger logger, boolean echoToConsole) {

      log(logger, echoToConsole, "DIC.deleteorphanResponses(..), before clear vpd...");
      srk.getExpressState().cleanDealIds();
    String sql = null;
    JdbcExecutor jExec = srk.getJdbcExecutor();

    log(logger, echoToConsole, "DealIntegrityChecker.deleteorphanResponses()");

    String[][] orphans = null;

    sql = "SELECT RESPONSEID FROM RESPONSE WHERE REQUESTID NOT IN (SELECT REQUESTID FROM REQUEST)";
    sql = "SELECT resp.RESPONSEID, resp.institutionprofileid FROM RESPONSE resp " +
            "WHERE REQUESTID NOT IN " +
            "(SELECT req.REQUESTID FROM REQUEST req where req.institutionprofileid = resp.institutionprofileid) " +
            "order by resp.institutionprofileid";

    try {
      jExec.execute(sql);
      key = jExec.execute(sql);

      orphans = jExec.getData(key);

      jExec.closeData(key);

      lim = orphans.length;

      if (lim > 0)
        log(logger, echoToConsole, "DealIntegrityChecker.deleteorphanResponses(): Number of orphan responses found = " + lim);
      
      int lastInstProfId = -1;
      
      // orphan records
      for (int i = 0; i < lim; ++i) {
        int respId = Integer.parseInt(orphans[i][0].trim());
        int instProfId      = Integer.parseInt(orphans[i][1].trim());

        if(instProfId != lastInstProfId){
            log(logger, echoToConsole, "DIC.deleteorphanResponses(..), before set vpd...");
            srk.getExpressState().setDealIds(0, instProfId, 0);
            lastInstProfId = instProfId;
        }
        
        try {
          srk.beginTransaction();

          Response resp = new Response(srk);
          resp = resp.findByPrimaryKey(new ResponsePK(respId));

          resp.ejbRemove();

          srk.commitTransaction();
          log(logger, echoToConsole, "DealIntegrityChecker.deleteorphanResponses(): Orphan record deleted "  + respId) ;

        } catch (Exception e) {
          srk.cleanTransaction();

          log(logger, echoToConsole, "Exception @DealIntegrityChecker.deleteorphanResponses(): Orphan record deleted "  + respId);
          logger.error(e);
        }
      }
    } catch (Exception e) {
      srk.cleanTransaction();

      log(logger, echoToConsole,
          "Exception @DealIntegrityChecker.deleteTransactionalCopies: Obtaining stranded transactional copies");
      logger.error(e);
    }
  }


  /**
   * Ensure each deal has just one gold copy. If for a given deal this rule is
   * violated then the true gold copy is assumed to be the one with the highest
   * copy id and the other are converted to locked scenarios.
   * 
   */
  public void ensureOneGoldCopy(SessionResourceKit srk, SysLogger logger, boolean echoToConsole)
  {
      log(logger, echoToConsole, "DIC.ensureOneGoldCopy(..), before clear vpd...");
      srk.getExpressState().cleanDealIds();
      String sql = null;
      JdbcExecutor jExec = srk.getJdbcExecutor();

      log(logger, echoToConsole, "DealIntegrityChecker.ensureOneGoldCopy starts...");

      String [][] multigolds = null;

      try
      {
        sql = "select min(dealid), max(copyid), count(dealid) from deal where copytype = 'G' group by dealid having count(dealid) > 1";
        sql = "select dealid, max(copyid), count(dealid), institutionprofileid from deal " +
                "where copytype = 'G' " +
                "group by institutionprofileid,dealid " +
                "having count(dealid) > 1 " +
                "order by institutionprofileid";

        key = jExec.execute(sql);
        multigolds = jExec.getData(key);

        jExec.closeData(key);

        lim = multigolds.length;
        
        int lastInstProfId = -1;

        for (int i = 0; i < lim; ++i)
        {
            dealId      = Integer.parseInt(multigolds[i][0].trim());
            copyId      = Integer.parseInt(multigolds[i][1].trim());
            int instProfId      = Integer.parseInt(multigolds[i][3].trim());
            
            if(instProfId != lastInstProfId){
                log(logger, echoToConsole, "DIC.ensureOneGoldCopy(..), before set vpd...");
                srk.getExpressState().setDealIds(dealId, instProfId, copyId);
                lastInstProfId = instProfId;
            }

            sql = "select copyid, institutionProfileId from deal " +
                     "where dealid = " + dealId + " and copyid <> " + copyId;
            
            key = jExec.execute(sql);

            String [][] extras = jExec.getData(key);

            jExec.closeData(key);

            int lim2 = extras.length;

            for (int i2 = 0; i2 < lim2; ++ i2)
            {
                try
                {
                    srk.beginTransaction();

                    copyId = Integer.parseInt(extras[i2][0].trim());

                    Deal deal = new Deal(srk, null);

                    deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));

                    // get next scenario number
                    MasterDeal md = new MasterDeal(deal.getSessionResourceKit(), null);

                    md = md.findByPrimaryKey(new MasterDealPK(deal.getDealId()));
                    int nextScenarionNumber = md.nextScenarioNumber();

                    // change duplicate gold to a scenario - a locked one
                    deal.setCopyType("S");
                    deal.setScenarioNumber(nextScenarionNumber);
                    deal.setScenarioLocked("Y");

                    deal.ejbStore();

                    srk.commitTransaction();

                    log(logger, echoToConsole, "DealIntegrityChecker.ensureOneGoldCopy: Deal with multiple gold copies :: dealid=" + dealId + ", CID=" + copyId + ":: copy changed to locked scenario");
                }
                catch (Exception e2)
                {
                    srk.cleanTransaction();

                    log(logger, echoToConsole, "Exception @DealIntegrityChecker.ensureOneGoldCopy - fixing deal with multiple gold copies :: dealid=" + dealId + ", copyId=" + copyId);
                    logger.error(e2);
                }
            }
        }
      }
      catch (Exception e3)
      {
        srk.cleanTransaction();

        log(logger, echoToConsole, "Exception @DealIntegrityChecker.ensureOneGoldCopy: fixing deal with multiple gold copies :: dealid=" + dealId);
        logger.error(e3);
        return;
      }

  }

  /**
   * Ensure each deal has a gold copy. If for a given deal this rule is violated then the scenario with the
   * highest copy id is converted to the gold copy (if one).
   **/
  public void ensureGoldCopyExists(SessionResourceKit srk, SysLogger logger, boolean echoToConsole)
  {
      log(logger, echoToConsole, "DIC.ensureGoldCopyExists(..), before clear vpd...");
      srk.getExpressState().cleanDealIds();
      String sql = null;
      JdbcExecutor jExec = srk.getJdbcExecutor();

      log(logger, echoToConsole, "DealIntegrityChecker.ensureGoldCopyExists starts.....");

      String [][] nogolds = null;

      try
      {
        sql = "select distinct dealid, copyid, copyType from deal where copytype <> 'G' and dealid not in (select dealid from deal where copytype = 'G') order by dealid, copyid desc";
        sql = "select distinct d1.dealid, d1.copyid, d1.copyType, d1.institutionProfileId " +
                "from deal d1 " +
                "where d1.dealid not in " +
                    "(select d2.dealid from deal d2 where d2.copytype = 'G' and d1.institutionProfileId = d2.institutionProfileId) " +
                "order by d1.institutionProfileId, d1.dealid, d1.copyid desc";
        
        key = jExec.execute(sql);

        nogolds = jExec.getData(key);

        jExec.closeData(key);

        lim = nogolds.length;

        int lastDealId = -1;
        
        int lastInstProfId = -1;
        
        for (int i = 0; i < lim; ++i)
        {
            dealId      = Integer.parseInt(nogolds[i][0].trim());
            copyId      = Integer.parseInt(nogolds[i][1].trim());
            copyType    = nogolds[i][2];
            int instProfId      = Integer.parseInt(nogolds[i][3].trim());
            
            if (copyType.equals("T"))
              continue;

            if (dealId == lastDealId)
              continue;

            try
            {
                if(instProfId != lastInstProfId){
                    log(logger, echoToConsole, "DIC.ensureGoldCopyExists(..), before set vpd...");
                    srk.getExpressState().setDealIds(dealId, instProfId, copyId);
                    lastInstProfId = instProfId;
                }
                
                srk.beginTransaction();

                Deal deal = new Deal(srk, null);

                deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
                deal.setCopyType("G");
                deal.ejbStore();

                MasterDeal md = new MasterDeal(deal.getSessionResourceKit(), null);
                
                //This is added to correct the G copy id. Remember to remove this....
                md.setGoldSanityCheckEnabled(false);
                
                md = md.findByPrimaryKey(new MasterDealPK(deal.getDealId()));
                md.setGoldCopyId(copyId);
                md.ejbStore();

                lastDealId = dealId;

                srk.commitTransaction();

                log(logger, echoToConsole, "DealIntegrityChecker.ensureGoldCopyExists: Deal without gold copy :: dealid=" + dealId + ", (now) gold CID=" + copyId);
            }
            catch (Exception eeeee)
            {
                srk.cleanTransaction();

                log(logger, echoToConsole, "Exception @DealIntegrityChecker.ensureGoldCopyExists: setting gold copy type :: dealid=" + dealId + ", (now) gold CID=" + copyId + ", message = " + eeeee.getMessage());
                logger.error(eeeee);
            }
        }
      }
      catch (Exception eeee)
      {
        srk.cleanTransaction();

        log(logger, echoToConsole, "Exception @DealIntegrityChecker.ensureGoldCopyExists: setting gold copy type :: dealid=" + dealId + ", copyId=" + copyId + ", message = " + eeee.getMessage());
        logger.error(eeee);
        return;
      }
  }


  /**
   * Ensure for each deal that the gold copy is properly recorded in the master deal record. If for a given deal
   * this rule is violated then the master records is corrected.
   *
   **/

  public void ensureGoldCopyCorrect(SessionResourceKit srk, SysLogger logger, boolean echoToConsole)
  {
      log(logger, echoToConsole, "DIC.ensureGoldCopyCorrect(..), before clear vpd...");
      srk.getExpressState().cleanDealIds();
      String sql = null;
      JdbcExecutor jExec = srk.getJdbcExecutor();

      log(logger, echoToConsole, "DealIntegrityChecker.ensureGoldCopyCorrect");

      String [][] golds = null;

      try
      {
        // work only on the infected Deals for performance enhancement
        sql = "select d.dealid, d.copyid from deal d, masterdeal m "
                    + "where d.copytype = 'G' and "
                    + "d.dealid = m.dealid and " + "m.goldcopyid <> d.copyid "
                    + "order by dealid";
        
        //TODO: order by d.institutionprofileid, d.dealid
        sql = "select d.dealid, d.copyid, d.institutionprofileid from deal d, masterdeal m " +
                "where d.copytype = 'G' and d.dealid = m.dealid and d.institutionprofileid=m.institutionprofileid and m.goldcopyid <> d.copyid " +
                "order by d.institutionprofileid, d.dealid";
        
        key = jExec.execute(sql);

        golds = jExec.getData(key);

        jExec.closeData(key);

        lim = golds.length;

        int lastInstProfId = -1;
        
        for (int i = 0; i < lim; ++i)
        {
            dealId      = Integer.parseInt(golds[i][0].trim());
            copyId      = Integer.parseInt(golds[i][1].trim());
            int instProfId  = Integer.parseInt(golds[i][2].trim());

            if(instProfId != lastInstProfId){
                log(logger, echoToConsole, "DIC.ensureGoldCopyCorrect(..), before set vpd...");
                srk.getExpressState().setDealIds(dealId, instProfId, copyId);
                lastInstProfId = instProfId;
            }
            
            MasterDeal md = new MasterDeal(srk, null);
            md.setGoldSanityCheckEnabled(false);

            try
            {
              md = md.findByPrimaryKey(new MasterDealPK(dealId));
              if (md.getGoldCopyId() != copyId)
              {
                srk.beginTransaction();

                md.setGoldCopyId(copyId);
                md.ejbStore();

                srk.commitTransaction();

                log(logger, echoToConsole, "DealIntegrityChecker.ensureGoldCopyCorrect: MasterDeal record - incorrect goldId :: dealid=" + dealId + ", gold CID=" + copyId);
              }
            }
            catch (Exception ee)
            {
              srk.cleanTransaction();

              log(logger, echoToConsole, "Exception @DealIntegrityChecker.ensureGoldCopyCorrect: correcting gold copy id :: dealid=" + dealId + ", gold CID=" + copyId);
              logger.error(ee);
            }
        }
      }
      catch (Exception eee)
      {
        srk.cleanTransaction();

        log(logger, echoToConsole, "Exception @DealIntegrityChecker.ensureGoldCopyCorrect: checking correct gold CIDs");
        logger.error(eee);
        return;
      }
  }

  /**
   * Ensure for each deal that the attributes in the master deal records, namely number of copies and last copy id
   * are correct. Master deal records corrected as necessary.
   * 
   * @param srk Session Resource Kit.
   * @param logger logger object.
   * @param echoToConsole echo to console(true/false)
   */
  public void ensureMasterDealCorrect(SessionResourceKit srk, SysLogger logger, boolean echoToConsole)
  {
      log(logger, echoToConsole, "DIC.ensureMasterDealCorrect(..), before clear vpd...");
      srk.getExpressState().cleanDealIds();
      String sql = null;
      JdbcExecutor jExec = srk.getJdbcExecutor();

      log(logger, echoToConsole, "DealIntegrityChecker.ensureMasterDealCorrect");

      String [][] numCopiesData = null;

      try
      {
      	// Select incorrect MasterDeal.lastDealID/NumberOfCopies record.
        sql = "SELECT   md.institutionprofileid inst, md.dealid," +
        		"         md.numberofcopies AS logicalcount, d.cnt AS realcount," +
        		"         md.lastcopyid AS logicalcopyid, d.cpid AS realcopyid" +
        		"    FROM masterdeal md" +
        		"         JOIN" +
        		"         (SELECT   institutionprofileid, dealid, COUNT (*) cnt," +
        		"                   MAX (copyid) cpid" +
        		"              FROM deal" +
        		"          GROUP BY institutionprofileid, dealid) d" +
        		"         ON (    md.institutionprofileid = d.institutionprofileid" +
        		"             AND md.dealid = d.dealid" +
        		"             AND (md.numberofcopies != d.cnt OR md.lastcopyid < d.cpid)" +
        		"            ) " +
        		"ORDER BY inst";

        key = jExec.execute(sql);

        numCopiesData = jExec.getData(key);

        jExec.closeData(key);

        lim = numCopiesData.length;

        int lastInstProfId = -1;

        srk.beginTransaction();
        for (int i = 0; i < lim; ++i)
        {
			int institutionProfileID  = Integer.parseInt(numCopiesData[i][0].trim());
			dealId                       = Integer.parseInt(numCopiesData[i][1].trim());
			int logicalCount          = Integer.parseInt(numCopiesData[i][2].trim());
			int realCount             = Integer.parseInt(numCopiesData[i][3].trim());
			int logicalCopyID         = Integer.parseInt(numCopiesData[i][4].trim());
			int realCopyID            = Integer.parseInt(numCopiesData[i][5].trim());
			
            if(institutionProfileID != lastInstProfId){
                log(logger, echoToConsole, "DIC.ensureMasterDealCorrect(..), before set vpd... institutionProfileID:" + institutionProfileID);
                srk.getExpressState().setDealIds(dealId, institutionProfileID, realCount);
                lastInstProfId = institutionProfileID;
            }
            
            MasterDeal md = new MasterDeal(srk, null);
            md.setGoldSanityCheckEnabled(false);

            try
            {
              md = md.findByPrimaryKey(new MasterDealPK(dealId));
            }
            catch (FinderException fe)
            {
              // This exception never happen, because of it's constraint by FK.
              log(logger, echoToConsole, "DealIntegrityChecker.ensureMasterDealCorrect: Deal without MasterDeal Record: dealId =  " + dealId + ", MasterDeal record created");
            }


			// Correct Number of copies.
			if(logicalCount != realCount){
			    md.setNumberOfCopies(realCount);
			}

			// Correct Last copy ID.
	        if(logicalCopyID < realCopyID){
                md.setLastCopyId(realCopyID);
            }
            md.ejbStore();

            log(logger, echoToConsole, "DealIntegrityChecker.ensureMasterDealCorrect: MasterDeal record - incorrect # copies and/or last CID :: " +
            		"dealId = " + dealId + " numCopies = " + realCount + " (was " + logicalCount + "), lastCopyId = " + realCopyID + " (was " + logicalCopyID + ")");

            // Commit for redolog buffer overflow.
            if(i % 1000 == 0 && i > 0){
            	log(logger, echoToConsole, "DealIntegrityChecker.ensureMasterDealCorrect: MasterDeal record - commit (" +i+ "/" +lim+ ")");
            	srk.commitTransaction();
            	srk.beginTransaction();
            }
        }
        log(logger, echoToConsole, "DealIntegrityChecker.ensureMasterDealCorrect: MasterDeal record - done.("+lim+" records)");
        srk.commitTransaction();
      }
      catch (Exception e)
      {
        srk.cleanTransaction();

        log(logger, echoToConsole, "Exception @DealIntegrityChecker.ensureMasterDealCorrect: setting # copies and/or last CID :: dealid=" + dealId );
        logger.error(e);
        logger.error(StringUtil.stack2string(e));
        return;
      }
  }

  /**
   * Delete (stranded) transactional copies.
   **/
  public void deleteTransactionalCopies(SessionResourceKit srk, SysLogger logger, boolean echoToConsole)
  {
      log(logger, echoToConsole, "DIC.deleteTransactionalCopies(..), before clear vpd...");
      srk.getExpressState().cleanDealIds();
      String sql = null;
      JdbcExecutor jExec = srk.getJdbcExecutor();

      log(logger, echoToConsole, "DealIntegrityChecker.deleteTransactionalCopies");

      String [][] txCopies      = null;


      try
      {
        // Modified to delete rows in a loop, otherwise, the statement will be timeout
        // delete stranded audit records
        //--> The following is commented out because the performance of deleting the records taking too long
        //--> over 1 mins to remove 1000 records and it will always caused timeout.  For the time being we have to remove
        //--> it manually until Scott can figure out a better way !!
        //--> By Billy 12May2004
        /*
        sql = "delete from dealChangeHistory where AID <> 0 AND ROWNUM <= 1000";

        srk.beginTransaction();
        int affectedRows = jExec.executeUpdate(sql);
        logger.debug("@deleteTransactionalCopies :: affectedRows = " + affectedRows);
        srk.commitTransaction();

        while(affectedRows >= 1000)
        {
          srk.beginTransaction();
          affectedRows = jExec.executeUpdate(sql);
          logger.debug("@deleteTransactionalCopies :: affectedRows = " + affectedRows);
          srk.commitTransaction();
        }
        //================================================================================
        */

        // query for transactional copies
        sql = "select dealId, copyId, institutionprofileid from deal where copyType = 'T' order by institutionprofileid, dealId, copyId";

        key = jExec.execute(sql);

        txCopies = jExec.getData(key);

        jExec.closeData(key);

        lim = txCopies.length;

        if (lim > 0)
            log(logger, echoToConsole, "DealIntegrityChecker.deleteTransactionalCopies: Number of Tx Copies Located = " + lim);

        int lastInstProfId = -1;
        
        // delete transactional copies
        for (int i = 0; i < lim; ++i)
        {
          try
          {

              // remove data from database.
              srk.beginTransaction();

                dealId = Integer.parseInt(txCopies[i][0].trim());
                copyId = Integer.parseInt(txCopies[i][1].trim());
                int instProfId = Integer.parseInt(txCopies[i][2].trim());

                if(instProfId != lastInstProfId){
                    log(logger, echoToConsole, "DIC.deleteTransactionalCopies(..), before set vpd...");
                    srk.getExpressState().setDealIds(dealId, instProfId, copyId);
                    lastInstProfId = instProfId;
                }
                
                MasterDeal md = new MasterDeal(srk, null);
                md = md.findByPrimaryKey(new MasterDealPK(dealId));

                md.deleteCopy(copyId);
                md.ejbStore();

                srk.commitTransaction();

                log(logger, echoToConsole, "DealIntegrityChecker.deleteTransactionalCopies: Transactional copy deleted, dealId=" + dealId + ", copyId=" + copyId);
            }
            catch (Exception e)
            {
                srk.cleanTransaction();

                log(logger, echoToConsole, "Exception @DealIntegrityChecker.deleteTransactionalCopies: Deleting transaction copy, dealId=" + dealId + ", copyId=" + copyId);
                logger.error(e);
            }
        }
      }
      catch (Exception e)
      {
        srk.cleanTransaction();

        log(logger, echoToConsole, "Exception @DealIntegrityChecker.deleteTransactionalCopies: Obtaining stranded transactional copies");
        logger.error(e);
      }
   }

  //vpd: not modified yet...
   public void cleanAllDeals(SessionResourceKit srk, SysLogger logger)
   {
      int dealId    = 0;
      int copyId    = 0;
      int lim       = 0;
      int key       = 0;

      String sql = null;
      JdbcExecutor jExec = srk.getJdbcExecutor();

      log(logger, true, "\n-----> Delete common tables (work queue - document queue");

      try
      {
        srk.beginTransaction();
        sql = "delete from assignedtasksworkqueue";
        jExec.executeUpdate(sql);
        srk.commitTransaction();

        srk.beginTransaction();
        sql = "delete from document";
        jExec.executeUpdate(sql);
        srk.commitTransaction();
      }
      catch (Exception e)
      {
        String msg = null;
        log(logger, true, msg = "Exception @cleanAllDeals - deleting common tables");
        logger.error(msg);
        log(logger, true, msg = sql);
        logger.error(msg);

        logger.error(e);
        return;
      }

      /////////////////////////////////////////////////////////////////////////////////////////////

      log(logger, true, "\n-----> Delete All Deals\n");

      String [][] txCopies      = null;

      sql = "select dealId, copyId from deal order by dealId, copyId";

      try
      {
        key = jExec.execute(sql);

        txCopies = jExec.getData(key);

        jExec.closeData(key);

        lim = txCopies.length;

        if (lim > 0)
        {
            log(logger, true, "\n **** DELETE ALL DEAL (ALL COPIES = " + lim + ")\n");
        }

        for (int i = 0; i < lim; ++i)
        {
            dealId = Integer.parseInt(txCopies[i][0].trim());
            copyId = Integer.parseInt(txCopies[i][1].trim());

            MasterDeal md = new MasterDeal(srk, null);
            md = md.findByPrimaryKey(new MasterDealPK(dealId));

            md.deleteCopy(copyId);
            md.ejbStore();
        }
      }
      catch (Exception e)
      {
        srk.cleanTransaction();

        log(logger, true, "Exception @cleanAllDeals :: message = " + e.getMessage());
        log(logger, true, "" + e);

        logger.error(e);
      }
   }

   private void log(SysLogger logger, boolean echoToConsole, String msg)
   {
      if (msg.startsWith("Exception"))
        logger.error(msg);
      else
        logger.info(msg);

      if (echoToConsole)
          System.out.println(msg + "\n");
   }

}









