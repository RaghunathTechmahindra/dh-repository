package com.basis100.deal.util.collections;

import java.util.*;
import com.basis100.deal.entity.*;
import MosSystem.Mc;

/**
 * A comparison function, which imposes a <i>total ordering</i> on some
 * collection of objects.  Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 *  Note: this comparator imposes orderings that are inconsistent with equals.
 */

public class PrimaryPropertyComparator implements Comparator
{

   /**
   *
   * Places  Propertys with primary borrower flag == Y  first in List.
   *
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   * @throws ClassCastException if the arguments' are not of type DealEntity.
   *
   */
  public int compare(Object o1, Object o2)
  {
     Property b1 = (Property)o1;
     Property b2 = (Property)o2;

     if(b1.isPrimaryProperty() && !b2.isPrimaryProperty())
     {
       return -1;
     }
     else if(!b1.isPrimaryProperty() && b2.isPrimaryProperty())
     {
       return 1;
     }
     else
     {
       return 0;
     }
  }

}
