package com.basis100.deal.util;

import java.util.Vector;

/**
 * Class used to hold parameters for methods that are intended to have
 * a variable number of parameters.
 */
public class Parameters
{
    protected Vector vParms = new Vector();

    public Parameters(byte byte0)
    {
        add(byte0);
    }

    public Parameters(char c)
    {
        add(c);
    }

    public Parameters(double d)
    {
        add(d);
    }

    public Parameters(float f)
    {
        add(f);
    }

    public Parameters(int i)
    {
        add(i);
    }

    public Parameters(long l)
    {
        add(l);
    }

    public Parameters(Object obj)
    {
        add(obj);
    }

    public Parameters(String s)
    {
        add(s);
    }

    public Parameters(Vector vector)
    {
        vParms = vector;
    }

    public Parameters(short word0)
    {
        add(word0);
    }

    public Parameters(boolean flag)
    {
        add(flag);
    }

    public Parameters add(byte byte0)
    {
        vParms.addElement(new Byte(byte0));
        return this;
    }

    public Parameters add(char c)
    {
        vParms.addElement(new Character(c));
        return this;
    }

    public Parameters add(double d)
    {
        vParms.addElement(new Double(d));
        return this;
    }

    public Parameters add(float f)
    {
        vParms.addElement(new Float(f));
        return this;
    }

    public Parameters add(int i)
    {
        vParms.addElement(new Integer(i));
        return this;
    }

    public Parameters add(long l)
    {
        vParms.addElement(new Long(l));
        return this;
    }

    public Parameters add(Object obj)
    {
        vParms.addElement(obj);
        return this;
    }

    public Parameters add(String s)
    {
        vParms.addElement(s);
        return this;
    }

    public Parameters add(short word0)
    {
        vParms.addElement(new Short(word0));
        return this;
    }

    public Parameters add(boolean flag)
    {
        vParms.addElement(new Boolean(flag));
        return this;
    }

    public Vector toVector()
    {
        return vParms;
    }
}