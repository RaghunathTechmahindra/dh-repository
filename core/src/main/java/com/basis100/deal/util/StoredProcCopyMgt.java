package com.basis100.deal.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealCopyException;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

import edu.emory.mathcs.backport.java.util.Arrays;

public class StoredProcCopyMgt {

	private final static Logger logger 
	= LoggerFactory.getLogger(StoredProcCopyMgt.class);

	private final static String CMD_CREATE_COPY = "{? = call copy_management.create_copy(?, ?, ?, ?)}";
	private final static String CMD_DUPLICATE_DEALTREE = "{? = call copy_management.duplicate_dealtree(?, ?, ?, ?, ?)}";
	private final static String CMD_DELETE_COPY = "{? = call copy_management.delete_copy(?, ?, ?)}";

	/**
	 * create new deal copy.
	 * new copy is created within the same dealId specified by srcMD.
	 * note that, base deal(srcMD.dealid + copyid) have to exist,
	 * and MasterDeal table won't be changed.
	 * this method executes PL/SQL function: call copy_management.create_copy
	 * 
	 * @param srk
	 * @param srcMD
	 * @param srcCopyId
	 * @param destCopyId
	 * @return
	 * @throws DealCopyException
	 */
	public static int createCopy(SessionResourceKit srk, MasterDeal srcMD,
			int srcCopyId, int destCopyId) throws DealCopyException {

		validateSrk(srk);
		validateMasterDeal(srcMD);
		if(srcCopyId <= 0) new IllegalArgumentException("invalid srcCopyId");
		if(destCopyId <= 0) new IllegalArgumentException("invalid destCopyId");

		String msg = new StringBuilder().append("StoredProcCopyMgt.createCopy dealid=")
		.append( srcMD.getDealId()).append(",src copyid=").append(srcCopyId) 
		.append(",dest copyid=").append(destCopyId).append( ",institution=")
		.append( srk.getExpressState().getDealInstitutionId()).toString();

		logger.info("before " + msg);

		int affectedTables = 0;
		Connection con = srk.getConnection();
		CallableStatement stat = null;
		try {
			try{
				stat = con.prepareCall(CMD_CREATE_COPY);
				stat.registerOutParameter(1, Types.INTEGER);
				stat.setInt(2, srcMD.getDealId());
				stat.setInt(3, srcCopyId);
				stat.setInt(4, destCopyId);
				stat.setInt(5, srk.getExpressState().getDealInstitutionId());
				stat.execute();
				affectedTables = stat.getInt(1);
			}finally{
				if (stat != null) stat.close();
			}
			logger.info("after " + msg + " : affected tables=" + affectedTables);
		} catch (SQLException e) {
			throwException(e, msg);
		}
		return affectedTables;
	}

	/**
	 * create new deal copy using different dealid + copyid.
	 * new copy is created in different deal tree (dealid) specified by destMD.
	 * note that, base dealid(srcMD.dealid + srcCopyid) have to exist,
	 * but for destination, only masterDeal is required.
	 * both MasterDeal table srcMD and destMD won't be changed.
	 * special logic will also be applied. 
	 * this method executes PL/SQL function: call copy_management.duplicate_dealtree
	 * 
	 * @param srk
	 * @param srcMD
	 * @param srcCopyId
	 * @param destMD
	 * @param destCopyId
	 * @return
	 * @throws DealCopyException
	 */
	public static int duplicateDealTree(SessionResourceKit srk, MasterDeal srcMD,
			int srcCopyId, MasterDeal destMD, int destCopyId)
	throws DealCopyException {

		validateSrk(srk);
		validateMasterDeal(srcMD);
		validateMasterDeal(destMD);
		if(srcCopyId <= 0) new IllegalArgumentException("invalid srcCopyId");
		if(destCopyId <= 0) new IllegalArgumentException("invalid destCopyId");

		String msg = new StringBuilder().append(
		"StoredProcCopyMgt.duplicateDealTree - src dealid=") .append(srcMD.getDealId())
		.append(",src copyid=").append(srcCopyId) .append(",dest dealid=").append( destMD.getDealId()) 
		.append(",dest copyid=").append( destCopyId).append(",institution=") 
		.append( srk.getExpressState().getDealInstitutionId()).toString(); 
		logger.info("before " + msg);

		int affectedTables = 0;
		Connection con = srk.getConnection();
		CallableStatement stat = null;
		try {
			try{
				stat = con.prepareCall(CMD_DUPLICATE_DEALTREE);
				stat.registerOutParameter(1, Types.INTEGER);
				stat.setInt(2, srcMD.getDealId());
				stat.setInt(3, srcCopyId);
				stat.setInt(4, destMD.getDealId());
				stat.setInt(5, destCopyId);
				stat.setInt(6, srk.getExpressState().getDealInstitutionId());
				stat.execute();
				affectedTables = stat.getInt(1);
			}finally{
				if (stat != null) stat.close();
			}
			logger.info("after " + msg + " : affected tables=" + affectedTables);
		} catch (SQLException e) {
			throwException(e, msg);
		}

		// special logic
		// http://ops.filogix.com/fxpwiki/index.php/Mossys_properties#Mossys_Properties_Master_List
		// the following logic was ported from DealEntity#doCopy, Catherine,
		// 16Feb05, BMO to CCAPS #972
		try {
			if ("Y".equalsIgnoreCase(PropertiesCache.getInstance().getProperty(
					srk.getExpressState().getDealInstitutionId(),
					"com.filogix.deal.copy.lender.notes", "N"))) {

				Deal destGold = new Deal(srk, null, destMD.getDealId(), destMD .getGoldCopyId());
				Deal destLast = new Deal(srk, null, destMD.getDealId(), destCopyId);
				if (destGold != null) {
					destLast.setServicingMortgageNumber(destGold .getServicingMortgageNumber()); 
					destLast.ejbStore();
				}
			}
		} catch (Exception e) {
			throwException(e, "special logic in " + msg);
		}

		return affectedTables;
	}

	/**
	 * delete copy
	 * note that, base dealid(md.dealid + copyid) have to exist, 
	 * and this method won't delete MasterDeal table.
	 * this method executes PL/SQL function: call copy_management.delete_copy
	 * 
	 * @param srk
	 * @param md
	 * @param copyId
	 * @return
	 * @throws DealCopyException
	 */
	public static int deleteCopy(SessionResourceKit srk, MasterDeal md, int copyId)
	throws DealCopyException {

		validateSrk(srk);
		validateMasterDeal(md);
		if(copyId <= 0) new IllegalArgumentException("invalid copyid");

		String msg = new StringBuilder().append( "StoredProcCopyMgt.deleteCopy - dealid=")
		.append( md.getDealId()) .append(",copyid=").append(copyId).append(",institution=") 
		.append( srk.getExpressState().getDealInstitutionId()).toString();

		int affectedTables = 0;
		Connection con = srk.getConnection();
		CallableStatement stat = null;
		try {
			try{
				stat = con.prepareCall(CMD_DELETE_COPY);
				stat.registerOutParameter(1, Types.INTEGER);
				stat.setInt(2, md.getDealId());
				stat.setInt(3, copyId);
				stat.setInt(4, srk.getExpressState().getDealInstitutionId());
				stat.execute();
				affectedTables = stat.getInt(1);
			}finally{
				if (stat != null)stat.close();
			}
			logger.info("after " + msg + " : affected tables=" + affectedTables);
		} catch (SQLException e) {
			throwException(e, msg);
		}
		return affectedTables;
	}

	private static void throwException(Exception e, String cmd) throws DealCopyException {

		String msg = new StringBuilder().append("faild to execute: ").append(cmd).toString();
		logger.error(msg, e);
		throw new DealCopyException(msg, e);
	}

	private static void validateSrk(SessionResourceKit srk){
		if(srk == null) 
			throw new IllegalArgumentException("srk object is required; it must not null");
		if(srk.getExpressState().getDealInstitutionId() < 0)
			throw new IllegalArgumentException("Deal Institution is not specified");
	}
	private static void validateMasterDeal(MasterDeal md){
		if(md == null) 
			throw new IllegalArgumentException("MasterDeal object is required; it must not null");
		if(md.getDealId() <= 0) 
			throw new IllegalArgumentException("MasterDeal object is invalid; dealid has to be specified");
	}

}
