package com.basis100.deal.util;

import java.util.*;
import java.io.*;
import com.basis100.resources.*;

// Name: DispalyFieldPicklist
// Given a AttributeName, return the DisplayFieldName
// populated from folder   /mos/admin/dbprops/*.*
// Structure:
//        AttributeName ==> DisplayFieldName
//        return "" if nothing found
//

public class DisplayFieldPicklist {

  private static Properties displayFields = new Properties()  ;
  private static String pathName = "\\mos\\admin\\dbprops\\" ;
  private static boolean loaded = false ;

  static
  { init(); }

  private static void init()
  {
       try
       {
       pathName = PropertiesCache.getInstance().getInstanceProperty("com.basis100.picklist.displayfieldpicklist");
       File f = new File( pathName ) ;
       String[]  filenames = f.list ();

       for ( int i=0; i <=  filenames.length -1 ; i ++ )    // populate with all the files under the given folder
       {
          FileInputStream in = new FileInputStream( pathName + "\\" + filenames[i] ) ;
          if ( in != null )
            displayFields.load ( in );
          in.close ();
       }
       } catch ( Throwable t )
       {
          loaded=false;   
       }

       loaded = true;

  } // ----------------------  End of init() -------------------------


  public static String getDisplayField ( String input)
  {
      if(!loaded) return "";
      // return the displayfield , if not found , return empty string
      return displayFields.getProperty ( input , "" ) ;
  }

  public void setPathName( String pname )
  {
      pathName = pname;
  }

}