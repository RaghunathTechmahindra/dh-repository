package com.basis100.deal.util.collections;

import java.util.*;
import com.basis100.deal.entity.*;

 /**
 * A comparison function, which imposes a <i>total ordering</i> on some
 * collection of objects.  Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 *  Note: this comparator imposes orderings that are inconsistent with equals.
 */
public class DealApplicationDateComparator implements Comparator
{
  /*
  * @return a negative integer, zero, or a positive integer respectively as the first
  *      argument's applicationDate is before, equal to, or after the second
  * @throws ClassCastException if the arguments are not of type Deal.
  */
   public int compare(Object a, Object b)
   {
     Deal adeal = (Deal)a;
     Deal bdeal = (Deal)b;

     Date adate = adeal.getApplicationDate();
     Date bdate = bdeal.getApplicationDate();

     if(adate == null && bdate == null)
     {
       EntityIdComparator eic = new EntityIdComparator();
       return eic.compare(adeal,bdeal);
     }

     if(adate == null && bdate != null) return -1;

     if(adate != null && bdate == null) return 1;

     if(adate.after(bdate))
     {
       return -1;
     }
     if(adate.equals(bdate))
     {
       return 0;
     }
     if(bdate.after(adate))
     {
       return 1;
     }

     return 0;

  }
}

