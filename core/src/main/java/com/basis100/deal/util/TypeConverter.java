package com.basis100.deal.util;

/**
 * 14/Oct/2008 DVG #DG762 LEN323830: Jennifer Jarek Express is slow       
 * 03/Jun/2008 DVG #DG688 LEN217161: Chantal Blanchard MI application fee from AIG does not display in SOLPAK
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 * 30/Oct/2006 DVG #DG534 CTFS - [QUAR]CTFS Change Requests
 */

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.DealCalc;
import com.basis100.resources.PropertiesCache;

/**
 *  The TypeConverter class provides "safe" conversions of inbound and
 *  outbound types.  Methods catch exceptions and return benign values consistent
 *  with expected return types as follows: <br>
 *  <ul> <li> if the method takes a descendant of object and returns a non-String
 *   descendant of object - return null  </li>
 *  <li> if the method converts a literal type to a String - return an empty String</li>
 *  <li> if the method converts a String to a literal type - return a benign literal value</li>
 *  </ul> see individual methods
 *
 */
public class TypeConverter
{

	private final static Logger logger 
		= LoggerFactory.getLogger(TypeConverter.class);

  /**
   * parses a date in the Inbound Application Data Standard Format
   * - ddmmyyyy - (or dd/mm/yyyy)  and converts it  to a java.sql.Date object
   * @return a Date - If an exception is thrown - return null
   */
  public static Date dateFrom(String _input)
  {

     if(_input == null || _input.length() == 0) return null;

     String input = _input.toUpperCase();

      if(input.indexOf("T") != -1){
        input = input.replace('T',' ');
      }

     String[] supportedFormats = {"ddMMyyyy", "dd/MM/yyyy", "yyyy-MM-dd HH:mm:ss" };

     SimpleDateFormat currentFormat = null;

     //String buf = "";
     java.util.Date d = null;
     
     try
     {

         for(int i = 0; i < supportedFormats.length; i++)
         {
            currentFormat = new SimpleDateFormat(supportedFormats[i]);

            if(supportedFormats[i].length() == input.length())
            {
               

               try
              {
                  d = currentFormat.parse(input);
                  //============================================================
                  // Wired thing happens when we supply input value of 00/00/0000.
                  // Simple date format parses it as 30 november of -1898 and the
                  // value that goes to the database is 30-Nov-0002. This causes
                  // troubles during ingestion, so we decided to return null if
                  // the year is  less than 1800.
                  //============================================================
                  Calendar cal = new GregorianCalendar();
                  cal.setTime(d);
                  if( cal.get( Calendar.YEAR) < 1800){
                    return null;
                  }
               }
               catch(ParseException pe)
               {
                 continue;
               }
               return d;
            }

         }
         //FXP26826, 4.2GR, Oct 22, 2009 -- start
         //if you come here, 'input' didn't match existing pattern
         //now, we try W3CDTS format
         d = dateFromW3CDTF(_input);
         if(d != null) return d;
         //FXP26826, 4.2GR, Oct 22, 2009 -- end

     }
     catch(Exception e)
     {
    	 //String msg = "Date format error" + input;
    	 return null;
     }
     
     logger.error("@dateFrom parameter has unexpected pattern. faild to parse input=" + input);
     return null;
  }

  //FXP26826, 4.2GR, Oct 22, 2009, Adding ability of parsing W3CDTF expression of String. -- start  

  //YYYY (eg 1997)
  private static final String W3CDTF_REGEX_PATTERN_1 = "^\\d{4}$";
  //YYYY-MM (eg 1997-07)
  private static final String W3CDTF_REGEX_PATTERN_2 = "^\\d{4}-\\d{2}$";
  //YYYY-MM-DD (eg 1997-07-16)
  private static final String W3CDTF_REGEX_PATTERN_3 = "^\\d{4}-\\d{2}-\\d{2}$";
  //YYYY-MM-DDThh:mmTZD      (eg 1997-07-16T19:20+01:00)
  private static final String W3CDTF_REGEX_PATTERN_4 = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}[-+]\\d{2}:\\d{2}$";
  //YYYY-MM-DDThh:mm:ssTZD   (eg 1997-07-16T19:20:30+01:00)
  private static final String W3CDTF_REGEX_PATTERN_5 = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[-+]\\d{2}:\\d{2}$";
  //YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)
  private static final String W3CDTF_REGEX_PATTERN_6 = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{2}[-+]\\d{2}:\\d{2}$";
  //YYYY-MM-DDThh:mm:ssTZD   (eg 1997-07-16T19:20:30+0100)
  private static final String W3CDTF_REGEX_PATTERN_5_J = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[-+]\\d{4}$";

  private static Date dateFromW3CDTF(String input)
  {
	  if(StringUtils.isEmpty(input)) return null;
	  try{
		  //mostly, W3CDTF_REGEX_PATTERN_5 will be used by FCX schema
		  if(input.matches(W3CDTF_REGEX_PATTERN_5))
		  {
			  //YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
			  // change time zone part from "-04:00" to "-0400"
			  // so that SimpleDateFormat can recognise the time zone with "Z"
			  int tz = input.length() - 5;
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			  return sdf.parse(input.substring(0, tz) + input.substring(tz).replace(":", ""));

		  } else if(input.matches(W3CDTF_REGEX_PATTERN_1)){
			  //YYYY (eg 1997)
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			  return sdf.parse(input);
		  } else if(input.matches(W3CDTF_REGEX_PATTERN_2)){
			  //YYYY-MM (eg 1997-07)
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			  return sdf.parse(input);
		  } else if(input.matches(W3CDTF_REGEX_PATTERN_3)){
			  //YYYY-MM-DD (eg 1997-07-16)
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			  return sdf.parse(input);
		  } else if(input.matches(W3CDTF_REGEX_PATTERN_4)) {
			  //YYYY-MM-DDThh:mmTZD      (eg 1997-07-16T19:20+01:00)
			  // change time zone part from "-04:00" to "-0400"
			  // so that SimpleDateFormat can recognise the time zone with "Z"
			  int tz = input.length() - 5;
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
			  return sdf.parse(input.substring(0, tz) + input.substring(tz).replace(":", ""));
		  } else if(input.matches(W3CDTF_REGEX_PATTERN_6)) {
			  //YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)
			  // change time zone part from "-04:00" to "-0400"
			  // so that SimpleDateFormat can recognise the time zone with "Z"
			  int tz = input.length() - 5;
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSZ");
			  return sdf.parse(input.substring(0, tz) + input.substring(tz).replace(":", ""));
		  } else if(input.matches(W3CDTF_REGEX_PATTERN_5_J)) {
			  //YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+0100)
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			  return sdf.parse(input);
		  } else {
			  logger.warn("@dateFromW3CDTF parameter has unexpected pattern. input=" + input);
		  }

	  } catch (ParseException e) {
		  logger.error("@dateFromW3CDTF faild to parse string", e);
	  }
	  return null;
  }

  //FXP26826, 4.2GR, Oct 22, 2009, Adding ability of parsing W3CDTF expression of String. -- end
  
/*   #DG534 not used, or usable ...
  public static String formattedDateFrom(java.util.Date date, String format )
  {
     if(date == null ) return null;


     SimpleDateFormat outFormat = new SimpleDateFormat("format");


     return outFormat.format(date);
  }
*/

  public static String formattedDate(java.sql.Date sqlDate, String format)
  {
     if(sqlDate == null ) return null;


     SimpleDateFormat outFormat = new SimpleDateFormat(format);


     return outFormat.format(sqlDate);

  }

  public static String formattedDate(java.util.Date date, String format)
  {
     if(date == null ) return null;


     SimpleDateFormat outFormat = new SimpleDateFormat(format);


     return outFormat.format(date);

  }

  public static String isoDateFrom(java.sql.Date sqlDate)
  {
     if(sqlDate == null ) return null;


     SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddhhmmss");


     return outFormat.format(sqlDate);
  }


  public static String isoDateFrom(java.util.Date date)
  {
     if(date == null ) return null;


     SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddhhmmss");


     return outFormat.format(date);
  }

  // Catherine #724
  public static String isoDateFrom24Hrs(java.util.Date date)
  {
     if(date == null ) return null;


     SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHmmss");


     return outFormat.format(date);
  }

  /**
   *  @return true if the String s contains the char c
   */
  public static boolean containsChar(String s, char c)
  {
    char[] cArray = new char[s.length()];
    s.getChars(0,s.length(),cArray,0);

    for(int i = 0; i < s.length(); i++)
    {
      if(cArray[i] == c) return true;
    }

    return false;
  }
  /**
   *  @return true if the String s contains any of the constituent characters of chars
   *
   */
  public static boolean containsChar(String s, String chars)
  {
    char[] sArray = new char[s.length()];
    s.getChars(0,s.length(),sArray,0);

    char[] testChars = new char[chars.length()];
    chars.getChars(0,chars.length(),testChars,0);

    for(int i = 0; i < s.length(); i++)
    {
      for(int j = 0; j < chars.length(); j++)
      {
         if(sArray[i] == testChars[j]) return true;
      }
    }

    return false;
  }

  /**
   *  <pre>
   *  Converts a small subset of String formats to a int number of months using
   *  the following criterea:
   *  if the first  characters are separated from the following characters by '/' the
   *  String is assumed to be in the format years/months.  If the length = 4 then the string
   *  is treated as yymm format and if the length is less than 4 it is assumed to already be
   *  converted to months.
   *  ie: 04/05 = 53 months
   *      123   = 123 months
   *      2/7   = 31 months
   *
   *  </pre>
   *  @return the number of months or -1 if an exception is thrown
   */
  public static int convertToMonths(String yymm)
  {
     //empty case
     if(yymm == null )  return 0;
     yymm = yymm.trim();
     if(yymm.equals("") )  return 0;

     int numYears   = 0;
     int numMonths  = 0;
     String years  = "0";
     String months = "0";
     //boolean monthOverrun = false;

     //format 1 == yy/mm
     if(containsChar(yymm,'/'))
     {
         int sep = yymm.indexOf('/');
         years  = yymm.substring(0,sep);
         months = yymm.substring(sep + 1,yymm.length());
         // note in this format accept any month value since intent is clear
     }


     if(yymm.length() == 4) //assume yymm format
     {
         years  = yymm.substring(0,2);
         months = yymm.substring(2,yymm.length());
     }

     if(yymm.length() < 4 ) //assume already in months
     {
        months = yymm;
     }


     try
     {
        numYears  = Integer.parseInt(years);
     }
     catch(NumberFormatException nfe)
     {
       return -1;
     }
     try
     {
        numMonths  = Integer.parseInt(months);
     }
     catch(NumberFormatException nfe)
     {
       return -1;
     }

     numMonths = (numYears * 12) + numMonths;
     return numMonths;
  }
  /**
   *
   * Safely converts a String to a double.
   * @return a double or if an illegal (throws NumberFormatException)
   * or null value is recieved return 0.0
   */
  public static double doubleTypeFrom(String d)
  {

      if(d == null || d.trim().length() == 0)
         return 0.0;

      d = d.trim();
      
      try
      {
        return Double.parseDouble(d);
      }
      catch(NumberFormatException nfe)
      {
    	logger.error("@doubleTypeFrom faild to parse. parameter = " + d, nfe);
        return 0.0;
      }
  }
   /**
   *
   * Safely converts a String to a double.
   * @return a double or if an illegal (throws NumberFormatException)
   * or null value is recieved return the value stipulated in parameter error.
   */
  public static double doubleTypeFrom(String d, double error)
  {
      if(d == null || d.trim().length() == 0)
         return error;

      d = d.trim();
      
      try
      {
        return Double.parseDouble(d);
      }
      catch(NumberFormatException nfe)
      {
    	logger.error("@doubleTypeFrom faild to parse. parameter = " + d, nfe);
        return error;
      }
  }

    /**
   *
   * Safely converts a String to a float.
   * @return a float or if an illegal (throws NumberFormatException)
   * or null value is recieved return 0.0
   */
  public static float floatTypeFrom(String d)
  {

      if(d == null || d.trim().length() == 0)
         return 0.0f;

      d = d.trim();
      
      try
      {
        return Float.parseFloat(d);
      }
      catch(NumberFormatException nfe)
      {
    	logger.error("@floatTypeFrom faild to parse. parameter = " + d, nfe);
        return 0.0f;
      }
  }
   /**
   *
   * Safely converts a String to a float.
   * @return a float or if an illegal (throws NumberFormatException)
   * or null value is recieved return the value stipulated in parameter error.
   */
  public static float floatTypeFrom(String d, float error)
  {
      if(d == null || d.trim().length() == 0)
         return error;
      
      d = d.trim();
      
      try
      {
        return Float.parseFloat(d);
      }
      catch(NumberFormatException nfe)
      {
    	logger.error("@floatTypeFrom faild to parse. parameter = " + d, nfe);
        return error;
      }
  }
  /**
   * <pre>
   * Safely converts a String to an int.
   * In other words, if an illegal - throws NumberFormatException -
   * or empty value is recieved no exception is thrown.
   * Instead zero value is returned.
   *
   * @returns the int value of the String or 0 if none can be found.
   * </pre>
   */
  public static int intTypeFrom(String d)
  {

     if(d == null || d.equals(""))
        return 0;
     
     d = d.trim();
     
     try
      {
        return Integer.parseInt(d)  ;
      }
      catch(NumberFormatException nfe)
      {
    	logger.error("@intTypeFrom faild to parse. parameter = " + d, nfe);
        return 0;
      }
  }

  // #DG534 //#DG688 rewritten
  /**
   * <pre>
   * Safely converts a String to an int array.
   * In other words, if an illegal - throws NumberFormatException -
   * or empty value is recieved no exception is thrown.
   * Instead zero value is returned.
   *
   * @returns the int value of the String or 0 if none can be found.
   * </pre>
   */
  public static int[] intArrayTypeFrom(String d) {

    int[] intList = new int[0];

    if(d == null || d.equals(""))
      return intList;

    //try {
    //  ArrayList list     = new ArrayList();
    //  StringTokenizer st = new StringTokenizer(d, ",");
    //  int i = st.countTokens();
    //  if (i <= 0)
    //    return intList;
    //  intList = new int[i];
    //  while (st.hasMoreTokens())
    //    intList[--i] = Integer.parseInt(st.nextToken());
    //}
    //catch(NumberFormatException nfe)
    //{
    //}
    ArrayList list     = new ArrayList();
    StringTokenizer st = new StringTokenizer(d, ",");
    while (st.hasMoreTokens())
      list.add(new Integer(st.nextToken()));      
    int i = list.size();
    intList = new int[i];
    for (int j = i-1; j >= 0; j--) 
    	intList[j] = ((Integer)list.get(j)).intValue();
    return intList;
  }

  // #DG688 convenience method
  /**
   * retrieves int array from mossys property
   *
   * @param mosysProName mosys property name
   * @param is default value of above property
   * @return array of integer with the values
   */
  public static int[] intArrayTypeFromProp(String mosysProName, int[] defVal) {
    String csvList = PropertiesCache.getInstance().getProperty(-1, mosysProName);
    return csvList == null? defVal: intArrayTypeFrom(csvList);
  }
  
   /**
   * <pre>
   * Safely converts a String to an int.
   * In other words, if an illegal - throws NumberFormatException -
   * or empty value is recieved no exception is thrown.
   * Instead the value stipulated in the second parameter is returned.
   * </pre>
   * @returns the int value of the String or value stipulated in error
   * if none can be found.
   *
   */
  public static int intTypeFrom(String d, int error)
  {

     if(d == null || d.trim().length() == 0)
        return error;
     
     d = d.trim();
     
     try
      {
        return Integer.parseInt(d)  ;
      }
      catch(NumberFormatException nfe)
      {
    	logger.error("@intTypeFrom faild to parse. parameter = " + d, nfe);
        return error;
      }
  }
   /**
   * Safely converts a String to an short.
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead a zero is returned.
   *
   * @returns the short value of the String or 0 if none can be found.
   */
  public static short shortTypeFrom(String d)
  {

     if(d == null || d.trim().length() == 0)
         return (short)0;
     
     d = d.trim();
     
     try
      {
        return (short)Integer.parseInt(d)  ;
      }
      catch(NumberFormatException nfe)
      {
    	 logger.error("@shortTypeFrom faild to parse. parameter = " + d, nfe);
         return (short)0;
      }
  }

  /**
   * Safely converts a String to an short.
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead a the second parameter is returned.
   *
   * @returns the short value of the String or parameter errorVal if none can be found.
   */
  public static short shortTypeFrom(String d, short errorVal)
  {

     if(d == null || d.trim().length() == 0)
         return errorVal;
     
     d = d.trim();
      
     try
      {
        return (short)Integer.parseInt(d)  ;
      }
      catch(NumberFormatException nfe)
      {
    	 logger.error("@shortTypeFrom faild to parse. parameter = " + d, nfe);
         return errorVal;
      }
  }
   /**
   * Safely converts a String to an long.
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead a zero is returned.
   *
   * @returns the long value of the String or 0 if none can be found.
   */
  public static long longTypeFrom(String d)
  {

      if(d == null || d.trim().length() == 0)
        return (long)0;
      
      d = d.trim();
      
      try
      {
//        return (long)Integer.parseInt(d) ;
        return Long.parseLong(d);
      }
      catch(NumberFormatException nfe)
      {
    	 logger.error("@longTypeFrom faild to parse. parameter = " + d, nfe);
         return (long)0;
      }
  }

  /**
   * Safely converts a String to an long.
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead a zero is returned.
   *
   * @returns the long value of the String or 0 if none can be found.
   */
  public static long longTypeFrom(String d, long errorVal)
  {

      if(d == null || d.trim().length() == 0)
        return errorVal;

      d = d.trim();
      
      try
      {
        return Long.parseLong(d) ;
      }
      catch(NumberFormatException nfe)
      {
    	 logger.error("@longTypeFrom faild to parse. parameter = " + d, nfe);
         return errorVal;
      }
  }
   /**
   * Safely converts a String to an char.
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead a blank char is returned.
   *
   * @return the char value of the String or a blank if none can be found.
   */
  public static char charTypeFrom(String d)
  {

      if(d == null)
      {
        return ' ';
      }

      d = d.trim();

      if( d.length() < 1 )
       return ' ';

      try
      {
        return d.charAt(0);
      }
      catch(IndexOutOfBoundsException iob)
      {
        return ' ';
      }
  }

  /**
   * Safely converts a String to an char by returning the first character of the
   * String d as a char.
   * @return a char - if an illegal or empty value is recieved no exception is thrown.
   * Instead errorVal char is returned.
   *
   */
  public static char charTypeFrom(String d, char errorVal)
  {

      if(d == null)
      {
        return errorVal;
      }

      d = d.trim();

      if( d.length() < 1 )
       return errorVal;

      try
      {
        return d.charAt(0);
      }
      catch(IndexOutOfBoundsException iob)
      {
        return errorVal;
      }
  }




   /**
   * Safely converts a the value of int to a String;
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead an empty String is returned.
   *
   * @returns the String value of the parameter or "" if none can be determined
   */
  public static String stringTypeFrom(int in)
  {
    String retval = "";
    try
    {
      return String.valueOf(in);
    }
    catch(Exception e)
    {
      return retval;
    }

  }

   /**
   * Safely converts a the value of in to a String;
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead an empty String is returned.
   *
   * @returns the String value of the parameter or "" if none can be determined
   */
  public static String stringTypeFrom(char in)
  {
    String retval = "";
    try
    {
        retval = String.valueOf(in);
    }
    catch(Exception e)
    {
      return retval;
    }

     return retval;
  }
   /**
   * Safely converts a the value of in to a String;
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead an empty String is returned.
   *
   * @returns the String value of the parameter or "" if none can be determined
   */
  public static String stringTypeFrom(short in)
  {
    String retval = "";

    try
    {
      return String.valueOf(in);
    }
    catch(Exception e)
    {
      return retval;
    }
  }
   /**
   * Safely converts a the value of in to a String;
   * In other words, if an illegal or empty value is recieved no exception is thrown.
   * Instead an empty String is returned.
   *
   * @returns the String value of the parameter or "" if none can be determined
   */
  public static String stringTypeFrom(long in)
  {
    String retval = "";
    try
    {
      return String.valueOf(in);
    }
    catch(Exception e)
    {
      return retval;
    }

  }

  public static String stringTypeFrom(double in)
  {
    String retval = "";

    try
    {
    	//Date:2009/02/02 Ticket:FXP24246
    	//Java automatic converts the double value into exponential value if the value > 9999999.99
    	//Mecartor doesn't accept exponential value, therefore, we need to convert 
    	//the double value to big decimal and return its String representation without exponent field
    	if (in > DealCalc.T9D2)
    	{
    		BigDecimal bgIn = new BigDecimal(in);
    		String bgInString = bgIn.toPlainString();
    		int decimalIndex = bgInString.indexOf(".");
    		if(decimalIndex > 0)
    		{
    			return bgInString.substring(0, decimalIndex + 3);
    		}
    		else
    		{
    			return bgInString;
    		}
    	}
    	else
    	{
    		return String.valueOf(in);
    	}
    	
    }
    catch(Exception e)
    {
      return retval;
    }

  }

   /**
   * 'Safely' converts a the value of <in> to a String in the default date format
   * for MOS Oracle: YYYY-MM-DD HH24:MI:SS.
   *
   * Details:
   *            If <in> == null returns null
   *            If an conversion exception occurs returns null (the 'safely' part)
   *
   * @returns the String value of the parameter or null
   */
   public static String stringTypeFrom(java.util.Date d)
   {
     if (d == null)
        return null;

     try
     {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return  df.format(d);
     }
     catch(Exception e) {
    	 logger.error("@stringTypeFrom faild to format. parameter = " + d, e);
     }

     return null;
   }



   /**
   * Safely handles inputs.
   * If an null value is recieved no exception is thrown and
   * empty String is returned. If the input value is not null
   * it is returned without leading or trailing whitespace.
   *
   * @returns the String value of the parameter or "" if null
   */
  public static String stringTypeFrom(String in)
  {
    if(in == null) return "";
    return in;
  }

   /**
   * Safely converts a the value of in to a String of the form
   * dictated by the second and third parameters;
   * If an null value is recieved no exception is thrown and
   * empty String is returned.
   *
   * @returns the String value of the parameter or "" if null
   * 
   * @deprecated tricky: this method return NO if boolean is true
   */
  @Deprecated
  public static String stringFromBoolean(boolean val, String no, String yes)
  {
    if(val) return no;

    return yes;
  }

  /** #DG638
   * Safely converts a the value of in to a String 
   * If an null value is recieved no exception is thrown and
   * null String is returned.
   * 
   * @throws SQLException 
   * @throws IOException 
   *
   * @returns the String value of the parameter 
   */
  public static String stringFromClob(Clob cloba) throws SQLException, IOException
  {
  	if (cloba == null)		
  		return null;
  	
		int i = (int)cloba.length();
		StringBuffer sb = new StringBuffer(i);
		Reader reader = cloba.getCharacterStream();
		char ac[] = new char[i];
		while(true) {
			int j = reader.read(ac);
			if( j <= -1) 
				break;
			sb.append(ac, 0, j);
		}
		reader.close();
		return sb.toString();
  }
  // merged LS ticket for 4.2GR
  // #DG762 convenience method
  /**
   * converts to string from int array 
   *
   * @param array of integer with the values
   * @return string of comma separated (no space) integers
   */
  public static String stringFromIntArrayType(int[] intAr) {

    if (intAr == null) return null; // added to avoid NullPointerException

	  StringBuffer sb = new StringBuffer();
	  int ind;
	  for (ind = 0; ind < intAr.length; ind++)
		  sb.append(intAr[ind]).append(',');
	  ind = sb.length();
	  if(ind > 0)		// remove leftover
		  sb.setLength(ind-1);
    return sb.toString();
  }

  // #DGx temp implementation of Class.getSimpleName() which is available from jvm 1.5 on
  /**
   * converts to string from last name of the class 
   *
   * @param class to convert
   * @return the last part of the class name 
   */
  public static String stringFromClassSimple(Class classa) {

    if (classa == null) return null;  // added to avoid NullPointerException

  		String namea = classa.getName();
  		if(namea == null || namea.length() <= 0)
  			return "";
  		int i = namea.lastIndexOf(".");
  		return i <= 0? namea: namea.substring(i);
  }
  
  /**
   *  @return a boolean value based on the parameter false format. If the value is
   *  equal to the false format return false otherwise return true.
   *
   */
  public static boolean booleanFrom(String value, String falseFormat)
  {
     if(value == null) return false;
     if(falseFormat == null) falseFormat = "N";

     if(value.equals(falseFormat)) return false;

     return true;
  }

  public static boolean booleanFrom(String value)
  {
     if(value == null) return false;

     if(value.equalsIgnoreCase("Y")   ||
        value.equalsIgnoreCase("yes") ||
        value.equalsIgnoreCase("true"))
     {
       return true;
     }

     return false;
  }



  /**
   *  For use readying input for the database...
   *  Converts an array of true/false formats (ignore case) to "Y" or "N"
   *  <pre>
   *   Y, yes, true, oui, ok are returned as Y
   *   N, no, false, non  are returned as N
   *   null or other values are returned as an empty String
   *  </pre>
   *
   */
  public static String getFlagFormat(String in)
  {
    if(in == null) return "";
    if(in.equalsIgnoreCase("Y")   ||
       in.equalsIgnoreCase("yes") ||
       in.equalsIgnoreCase("true")||
       in.equalsIgnoreCase("oui") ||
       in.equalsIgnoreCase("ok")   ) { return "Y"; }

    if(in.equalsIgnoreCase("N")   ||
       in.equalsIgnoreCase("no") ||
       in.equalsIgnoreCase("false")||
       in.equalsIgnoreCase("non")   ) { return "N"; }

    return "";
  }
}
