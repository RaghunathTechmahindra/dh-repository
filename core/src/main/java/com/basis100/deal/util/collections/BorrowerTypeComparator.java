package com.basis100.deal.util.collections;

import java.util.*;
import com.basis100.deal.entity.*;
import MosSystem.Mc;

/**
 * A comparison function, which imposes a <i>total ordering</i> on some
 * collection of objects.  Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 *  Note: this comparator imposes orderings that are inconsistent with equals.
 */

public class BorrowerTypeComparator implements Comparator
{

   /**
   * Compares its two arguments for order.   Mc.BT_BORROWER is evaluated as
   * less than other BorrowerTypes
   * Sorts collections with BorrowerType = "Borrower" first and all others follow.
   *
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   * @throws ClassCastException if the arguments' are not of type DealEntity.
   *
   */
  public int compare(Object o1, Object o2)
  {
     Borrower b1 = (Borrower)o1;
     Borrower b2 = (Borrower)o2;

     int type1 = b1.getBorrowerTypeId();
     int type2 = b2.getBorrowerTypeId();

     if(type1 == Mc.BT_BORROWER)
     {
       return -1;
     }
     else if(type2 == Mc.BT_BORROWER)
     {
       return 1;

     }
     else
     {
       return 0;
     }
  }



}
