package com.basis100.deal.util.collections;

import java.util.*;
import com.basis100.deal.entity.*;

/**
 * A comparison function, which imposes a <i>total ordering</i> on some
 * collection of objects.  Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 *  Note: this comparator imposes orderings that are inconsistent with equals.
 */
public class EntityIdComparator implements Comparator
{

  /**
   * Compares its two arguments for order.  Returns a negative integer,
   * zero, or a positive integer as the first argument primary key id is
   * less than, equal to, or greater than the second.<p>
   *
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   * @throws ClassCastException if the arguments' are not of type DealEntity.
   */
   public int compare(Object a, Object b)
   {
     DealEntity ae = (DealEntity)a;
     DealEntity be = (DealEntity)b;

     if(ae.getPk().getId() < be.getPk().getId())
     {
       return 1;
     }
     if(ae.getPk().getId() ==  be.getPk().getId())
     {
       return 0;
     }
     if(ae.getPk().getId() > be.getPk().getId())
     {
       return -1;
     }

     return 0;

   }
}
