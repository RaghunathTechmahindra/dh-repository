package com.basis100.deal.util;

/**
 *  Y2K Exception Class
 *
 * @author  Bartek Jasionowski
 * @version 
 * @see     
 */
public final class Y2KException extends Exception {

  public Y2KException() {
    super();
  }

  public Y2KException(String message)
    {
        super(message);
    }
}

