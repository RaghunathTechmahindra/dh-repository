package com.basis100.deal.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

public class InstitutionProfileUtils {

    public static Collection<InstitutionProfile> getAllInstitutionProfiles(
        SessionResourceKit srk) throws RemoteException, FinderException {

        InstitutionProfile ip = new InstitutionProfile(srk);
        Collection<InstitutionProfile> list = ip.getAllInstitutions();
        return list;
    }

    /**
     * create Map that contains key:institutionProfileId and Value:String value
     * that was found in sysProperty table
     * 
     * @param srk
     * @param sysPropertyName
     * @param defaultValue
     * @return map<key:(Integer)institutionProfileid, value:(String)value found
     *         in sysProperty with param:sysPropertyName
     * @throws RemoteException
     * @throws FinderException
     */
    public static Map<Integer, String> createMapAllInstitutionAndSysPropertyValueString(
        SessionResourceKit srk, String sysPropertyName, String defaultValue)
        throws RemoteException, FinderException {

        Collection<InstitutionProfile> ipList = getAllInstitutionProfiles(srk);
        PropertiesCache prop = PropertiesCache.getInstance();
        Map<Integer, String> ipValueMap = new HashMap<Integer, String>();

        for (InstitutionProfile ip : ipList) {
            String value = prop.getProperty(
                ip.getInstitutionProfileId(), sysPropertyName, defaultValue);
            ipValueMap.put(ip.getInstitutionProfileId(), value);
        }

        return ipValueMap;
    }

    /**
     * create Map that contains key:institutionProfileId and Value:Integer value
     * that was found in sysProperty table
     * 
     * @param srk
     * @param sysPropertyName
     * @param defaultValue
     * @return map<key:(Integer)institutionProfileid, value:(Integer)value found
     *         in sysProperty with param:sysPropertyName
     * @throws RemoteException
     * @throws FinderException
     */
    public static Map<Integer, Integer> createMapAllInstitutionAndSysPropertyValueInt(
        SessionResourceKit srk, String sysPropertyName, int defaultValue)
        throws RemoteException, FinderException {

        Map<Integer, String> map =
            createMapAllInstitutionAndSysPropertyValueString(
                srk, sysPropertyName, String.valueOf(defaultValue));

        Map<Integer, Integer> intMap = new HashMap<Integer, Integer>();
        for (Integer key : map.keySet()) {
            String value = map.get(key);
            intMap.put(key, Integer.parseInt(value));
        }
        return intMap;
    }

}
