package com.basis100.deal.util.collections;

import java.util.Comparator;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.picklist.PicklistData;
import MosSystem.Mc;

public class ConditionDescriptionComparator implements Comparator
{

  public int compare(Object o1, Object o2)
  {
      DocumentTracking dt1 = (DocumentTracking)o1;
      DocumentTracking dt2 = (DocumentTracking)o2;

      //  Didn't encounter the Requested status, so let's sort alphabetically
      String cdc1;
      String cdc2;
      try{
        cdc1 = dt1.getSectionDescription();
        cdc2 = dt2.getSectionDescription();
      } catch(Exception exc){
        return 0;
      }
      return (cdc1 == null || cdc2 == null) ? 0 : cdc1.compareTo(cdc2);
  }

}