package com.basis100.deal.util.collections;

import java.util.Comparator;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.picklist.PicklistData;
import MosSystem.Mc;

/**
 * A comparison function, which sorts based on the DocumentTracking status.
 * The desired order is Requested Status first and alphabetical after that.
 * Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 */
public class DocumentStatusComparator implements Comparator
{
    /**
    * Compares its two arguments for order.   Document Tracking status
    * Mc.DOC_STATUS_REQUESTED is evaluated as less than other statuses.
    * Sorts collections with status = "Requested" first and all others follow,
    * alphabetically.
    *
    * @return a negative integer, zero, or a positive integer as the
    * 	       first argument is less than, equal to, or greater than the
    *	       second.
    * @throws ClassCastException if either of the arguments is not of type DocumentTracking.
    *
    */
    public int compare(Object o1, Object o2)
    {
        DocumentTracking dt1 = (DocumentTracking)o1;
        DocumentTracking dt2 = (DocumentTracking)o2;

        //  Requested status supercedes the alphabetic sort
        if (dt1.getDocumentStatusId() == Mc.DOC_STATUS_REQUESTED)
            return -1;
        else if (dt2.getDocumentStatusId() == Mc.DOC_STATUS_REQUESTED)
            return 1;

        //  Didn't encounter the Requested status, so let's sort alphabetically
        String status1 = PicklistData.getDescription("DocumentStatus",
                             dt1.getDocumentStatusId());

        String status2 = PicklistData.getDescription("DocumentStatus",
                             dt2.getDocumentStatusId());

        return (status1 == null || status2 == null) ? 0 : status1.compareTo(status2);
    }
}