package com.basis100.deal.util;

/**
 * 08.Apr.2009 DVG #DG838 NBC357552: 	Susie Zanon Backslash followed by numbers can break deal notes window
 * 19/Mar/2007 DVG #DG588 FXP16317: MX_3.1_Production_ Deal Notes problem on Source Resubmission 
 * 18/Oct/2006 DVG #DG526 #4916  DJ 3.1 - Apostrophes appear as "accent graves" when entered in a note
 */

import com.basis100.deal.entity.Addr;
import java.io.*;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class StringUtil
{
 /**
  *  Uitility for constructing where clause expression that features a comparison test on a string field
  *  (e.g. VARCHAR). For example consider the following expression:
  *
  *       "select * from mytable where MYSTRFIELD = 'abc'"
  *
  *  We are concerned with the expression forllowing the where clause which compares the field/column against the
  *  literal 'abc'. This expression would be returned by the following call:
  *
  *       formFieldMatchExpression("MYSTRFIELD", "abc", false; // based on ex. = "MYSTRFIELD = 'abc'"
  *
  *  However, when the value to compare the field with is null the following query is more appropriate:
  *
  *       "select * from mytable where MYSTRFIELD is NULL"
  *
  *
  *  @param matchCase - Select case sensitivity criteria for comparison
  *
  **/


  public static String formFieldMatchExpression(String fldName, String matchValue, boolean matchCase)
  {
    if (matchValue == null)
      return (" " + fldName + " IS NULL ");

    // case sensitive expression
    matchValue = matchValue.trim();

    if (matchCase == true)
      return (" " + fldName + "  =  '" + StringUtil.makeQuoteSafe(matchValue) + "' ");

    return (" upper(" + fldName + ")  =  \'" + StringUtil.makeQuoteSafe(matchValue.toUpperCase()) + "\' ");
  }


 /**
  *  Safely concatenates the parameter Strings. If the inbound (existing) String is null
  *  the result includes the added String.  If the addable String is null the existing
  *  String will be found in the return buffer. If both parameters
  *  are null or empty an empty buffer is returned.
  *
  *  @param in -  the existing String (right operand)
  *  @param add -  the String to add (left operand)
  *  @return A String buffer with the concatenated input values
  *
  */
  public static StringBuffer concat(String in, String add)
  {
     StringBuffer out = new StringBuffer();

     if(in != null)
     {
       out.append(in);
     }

     if(add == null || add.length() == 0)
     {
        return out;
     }

     out.append(add);
     return out;
  }

  public static StringBuffer concat(StringBuffer in, String add)
  {
     if(in == null)
     {
       in = new StringBuffer();
     }

     if(add == null || add.length() == 0)
     {
        return in;
     }

     return in.append(add);
  }

  public static String zeroLeftPad(int intNum, int maxSize){
    String longZero = "0000000000";
    while(longZero.length() <= maxSize){
      longZero += longZero;
    }
    longZero = longZero + intNum;
    return longZero.substring(longZero.length() - maxSize - 1);
  }

  public static String zeroLeftPad(String pVal, int maxSize){
    String longZero = "0000000000";
    while(longZero.length() <= maxSize){
      longZero += longZero;
    }
    longZero = longZero + pVal;
    return longZero.substring(longZero.length() - maxSize - 1);
  }

   public static String trimString(String s)
   {
      if (s != null)
        return s.trim();
      else
        return s;
   }

   public static String stripWhiteSpace(String in)
   {
      int len = in.length();
      char[] inchars = in.toCharArray();
      char[] newchars = new char[len];
      int count = 0;

      for(int i = 0; i < len; i++)
      {
        if(!Character.isWhitespace(inchars[i]))
        {
          newchars[count] = inchars[i];
          count++;
        }
      }

     return (new String(newchars)).trim();
   }

   public static String setSystemPathSeparator(String in)
   {
      if(in == null) return in;

      int len = in.length();
      char[] inchars = in.toCharArray();
      char[] newchars = new char[len];
      int count = 0;

      for(int i = 0; i < len; i++)
      {
        if(inchars[i] == '\\' || inchars[i] == '/')
        {
          newchars[count] = System.getProperty("file.separator").charAt(0);
        }
        else
        {
          newchars[count] = inchars[count];
        }
        count++;
      }

     return (new String(newchars)).trim();
   }






 /**
  * Home Phone numbers will be entered in three separate fields, area code,
  * exchange, route. [Nnn] [nnn] [nnnn] (stored as nnnnnnnnnn) In display
  * only fields, the phone number should be concatenated together with
  * punctuation (nnn) nnn-nnnn.  The fields will be stored in the database
  * concatenated but are to be displayed on entry screens as parsed into three
  * fields.  The user must be enforced in entering 10 digits.
  *
  *	Work/Contact Phone numbers will be entered in four separate fields, area code,
  * exchange, route, extension, if applicable. [Nnn] [nnn] [nnnn] x [nnnnnn]
  * (stored as nnnnnnnnnn and nnnnnn in a separate field)  In display only fields
  * the phone number should be concatenated together with punctuation (nnn) nnn-nnnn x
  * nnnnnn.  The fields will be stored in the database concatenated but are to be displayed
  * on entry screens as parsed into four fields.  The user must be enforced in entering 10
  * digits in the work phone/contact number, but the extension is optional.
  *
  * This method removes characters from the given phone number.
  *
  *
  * @return a String formatted as indicated above
  * @throws Exception - exception if an invalid number of digits are provided.
  */
  public static String getAsFormattedPhoneNumber(String number, String ext) throws Exception
  {

    String[] numArray = getPhoneNumberAsArray(number);

    StringBuffer digits = new StringBuffer();

    if(numArray[0].length() == 3)
    {
      digits.append("(").append(numArray[0]).append(") ");
    }

    digits.append(numArray[1]).append("-").append(numArray[2]);

    if(ext != null && ext.length() > 0)
    {
      digits.append(" x ").append(ext.trim());
    }

    return digits.toString();
  }

  /**
  *  Spec: The fields will be stored in the database concatenated but are to be displayed
  *  on entry screens as parsed into four fields.
  *  This method returns an Array containing first 3 fields of phone number. If
  *  no area code is provided the array contains an empty string at index = 0;
  *  @throws exception if an invalid number of digits are provided
  */
  public static String[] getPhoneNumberAsArray(String number) throws Exception
  {

    StringBuffer digits = getDigitsAsBuffer(number);

    if(digits == null) return null;

    String[] out = new String[3];

    int len = digits.length();

    if(len == 7)
    {
      out[0] = "";
      out[1] = (digits.substring(0,3)).toString();
      out[2] = (digits.substring(3)).toString();
    }
    else if(len == 10)
    {
      out[0] = (digits.substring(0,3)).toString();;
      out[1] = (digits.substring(3,6)).toString();
      out[2] = (digits.substring(6,10)).toString();
    }
    else
    {
      throw new Exception("Invalid Length for phone number format");
    }

    return out;
  }

  private static StringBuffer getDigitsAsBuffer(String number)
  {
    if(number == null) return null;   // short circuit

    int len = number.length();
    //check content

    char[] str = number.toCharArray();
    StringBuffer digits = new StringBuffer();

    for(int i = 0; i < len; i++)
    {
      if(Character.isDigit(str[i]))
        digits.append(str[i]);
    }

    return digits;
  }

  public static String getPostalCode(Addr addr)
  {
    if(addr == null) return "";

    String fsa = addr.getPostalFSA();
    String ldu = addr.getPostalLDU();

    if(fsa != null && ldu != null)
     return fsa + " " + ldu;

    return "";

  }

  /**
   *  makes a String with single quotes safe for sql statements by doubling up on
   *  the single quote.<br> i.e. e'xample becomes e''xample
   */
  //--> Changed to use StringBuffer fro Optimization by Billy 08Sept2003
  /*
  public static String makeQuoteSafe(String in)
  {
     if(in == null || in.length() == 0) return in;

     char quote = '\'';

     char[] inbuf = in.toCharArray();
     int len = inbuf.length;
     char[] outbuf = new char[len + len];
     int count = 0;

     for(int i = 0; i < len; i++)
     {
        outbuf[count++] = inbuf[i];
        if(inbuf[i] == quote)
        {
          outbuf[count++]= quote;
        }

     }

     String out = new String(outbuf,0,count);
     return out;
  }
  */
  public static String makeQuoteSafe(String in)
  {
     if(in == null || in.length() == 0) return in;
     char quote = '\'';

     StringBuffer outbuf = new StringBuffer();

     char c;
     for(int i = 0; i < in.length(); i++)
     {
        c = in.charAt(i);
        outbuf.append(c);
        if(c == quote)
        {
          outbuf.append(quote);
        }
     }

     return outbuf.toString();
  }
  
  public static String makeBackSlashSafe(String in){
	  String backSlashFind = "\\\\";
	  String replace = "&#92;";
	  String output ;
	  if(in==null){
		  output  = in ;
	  }else{
	  Pattern pattern = Pattern.compile(backSlashFind);
	  Matcher matcher = pattern.matcher(in);
		  output = matcher.replaceAll(replace);
	  }
	  return output;
  }
  //==================================================

  public static String extractTrailingNumbers(String pStr){
    if(pStr == null ){return null;}
    pStr = pStr.trim();
    if(pStr.length() == 0){return null;}
    char[] ch = pStr.toCharArray();
    StringBuffer sbNumbers = new StringBuffer("");
    for(int i = ch.length - 1; i >= 0; i--){
      if( Character.isDigit(ch[i]) ){
        sbNumbers.insert(0,ch[i]);
      }else{
        break;
      }
    }
    return sbNumbers.toString();
  }

  // New method to convert the StackTrace meaasge to String for Debug purpose
  //  -- Billy 07May2002
  static public String stack2string(Throwable t)
  {
    try
    {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      t.printStackTrace(pw);
      return "MOS Stack Trace >> ---------  Begin ------------ \r\n" + sw.toString() + "-------  End ----------<<\r\n";
    }
    catch(Exception e2)
    {
      return "bad stack2string : Return getMessage() instead :: " + t;
    }
  }


	/** #DG638
	 * 
	 * get only a segment of current stack items.
	 * This is useful to trace some portion of the code without cluttering the log file 
	 * typical calls may be like:
	 * 		"Exception DocumentTracking@findBySqlColl:"+StringUtil.stack2string(e, 0, 1,"")+':'+e	  
	 * or  
	 * 		"tracing:"+StringUtil.stack2string(null, 4, 5, "")
	 * or  
	 * 		"tracing:"+StringUtil.stack2string(null, 4, 6, null)
	 *  
	 * The stack trace returns something like this. 
	 * 
	"stackTrace"= StackTraceElement[5]  (id=19)	
	[0]= StackTraceElement  (id=27)	
		declaringClass= "java.lang.Thread"	
		fileName= "Thread.java"	
		lineNumber= -2	
		methodName= "dumpThreads"	
	[1]= StackTraceElement  (id=22)	
		declaringClass= "java.lang.Thread"	
		fileName= "Thread.java"	
		lineNumber= 1383	
		methodName= "getStackTrace"	
	[2]= StackTraceElement  (id=25)	
		declaringClass= "com.basis100.log.SysLogger"	
		fileName= "SysLogger.java"	
		lineNumber= 190	
		methodName= "getCallee"	
	[3]= StackTraceElement  (id=26)	
		declaringClass= "com.basis100.log.SysLogger"	
		fileName= "SysLogger.java"	
		lineNumber= 182	
		methodName= "error"	
	[4]= StackTraceElement  (id=28)	
		declaringClass= "com.basis100.deal.docprep.RequestTest"	
		fileName= "RequestTest.java"	
		lineNumber= 165	
		methodName= "main"
	 * 
   * @param      beginIndex   the beginning index, inclusive.
   * @param      endIndex     the ending index, exclusive.
   * @param      separat	string to separate entries
   * 
	 * @return the string stack trace for the selected segment
	 */
  static public String stack2string(Exception e, int beginIndex, int endIndex, String separat)	{
  	// absolutely no exception is to be thrown from here!!
  	try {
  		if(separat == null)
  			separat = System.getProperty("line.separator");
			StackTraceElement[] stackTrace = e == null? 
					Thread.currentThread().getStackTrace(): 
					e.getStackTrace();
	
			StringBuffer sb = new StringBuffer(99);
			for (int i = beginIndex; i < stackTrace.length && i < endIndex; i++) 
				sb.append(stackTrace[i]).append(separat);
			return sb.toString();			
		}
		catch (Exception exc) {
			return "benign: stack2string:"+exc;
		}
	}

  /**
   * Formats a Social Insurance Number in the format '999 999 999'.  Passed
   * strings that are <code>null</code> or aren't 9 characters in length are
   * returned unprocessed.
   *
   * @param SIN the social insurance number to format
   * @return the formatted SIN
   *
   */
    public static String formatSIN(String SIN)
    {
        //  Not going to bother trying to format SINs that are null or the
        //  incorrect number of digits.
        if (SIN == null || SIN.trim().length() != 9)
           return SIN;

        return SIN.substring(0, 3) + " " + SIN.substring(3, 6) + " " + SIN.substring(6, 9);
    }

     // CR: 5-MAR-04
      /**
       * Formats a Social Insurance Number in the format '999-999-999' using the separator character provided.
       *  Passed strings that are <code>null</code> or aren't 9 characters in length are
       * returned unprocessed.
       *
       * @param SIN the social insurance number to format
       * @param Separator the separator character
       * @return the formatted SIN
       *
       */
    public static String formatSIN(String SIN, char Separator)
    {
        //  Not going to bother trying to format SINs that are null or the
        //  incorrect number of digits.
        if (SIN == null || SIN.trim().length() != 9)
           return SIN;

        return SIN.substring(0, 3) + Separator + SIN.substring(3, 6) + Separator + SIN.substring(6, 9);
    }


    public static String makeSafeXMLString(String pStr)
    {
        if (pStr == null)
           return pStr;

        int ind;
        StringBuffer retBuf = new StringBuffer("");

        ind = pStr.indexOf("&");

        if (ind == -1)
           ind = pStr.indexOf("<");

        if (ind == -1)
           ind = pStr.indexOf(">");

        while( ind != -1 )
        {
            retBuf.append(pStr.substring(0,ind));

            if (pStr.charAt(ind) == '&')
               retBuf.append( "&amp;" );
            else if (pStr.charAt(ind) == '<')
               retBuf.append( "&lt;" );
            else if (pStr.charAt(ind) == '>')
               retBuf.append( "&gt;" );

            pStr = pStr.substring(ind + 1);

            ind = pStr.indexOf("&");

            if (ind == -1)
               ind = pStr.indexOf("<");

            if (ind == -1)
               ind = pStr.indexOf(">");
        }

        retBuf.append(pStr);

        return retBuf.toString();
    }

    public static boolean isInTheList(String pList, String pSearchValue){
      if( pList == null || pSearchValue == null ){return false;}

      StringTokenizer st = new StringTokenizer(pList, ",");
      while( st.hasMoreTokens() ){
        String oneTok = st.nextToken();
        if(oneTok.trim().equals(pSearchValue.trim() )){
          return true;
        }
      }
      return false;
    }

    // #DG526
    /**  moved and renamed from MosWebHandlerCommon.convertToHtmString2
     *
     * @param input String
     * @return String
     */
    public static String convertToHtmString(String input) {

      if(input == null)
        return " ";
    	//#DG588 rewritten to replace \n\r together or separate
      //Pattern pat = Pattern.compile("(\\?|\\r)|(')|(\")|(  )|([\\x00-\\x1F\\x7F-\\x9F])"); //, Pattern.MULTILINE);
      //Pattern pat = Pattern.compile("(\\?|\\n\\r|\\n|\\r)|(')|(\")|(  )|([\\x00-\\x1F\\x7F-\\x9F])");
      Pattern pat = Pattern.compile("(\\n\\r|\\n|\\r)|(')|(\")|(  )|([\\x00-\\x1F\\x7F-\\x9F])|(\\?)");
      StringBuffer sb = new StringBuffer();
      Matcher matcha = pat.matcher(input);
      while (matcha.find()) {
        if (matcha.group(1) != null)
          matcha.appendReplacement(sb, "<br>");
        else if (matcha.group(2) != null)
          matcha.appendReplacement(sb, "&#39;");
        else if (matcha.group(3) != null)
          matcha.appendReplacement(sb, "&quot;");
        else if (matcha.group(4) != null)
          matcha.appendReplacement(sb, " &nbsp;");
        else if (matcha.group(5) != null)
          matcha.appendReplacement(sb, "");
        else if (matcha.group(6) != null)
          matcha.appendReplacement(sb, "&#63;");
      }
      matcha.appendTail(sb);

      return sb.toString();

      // this is an alternative way, maybe looks simpler but scans the input multiple times
      // whereas above implementation scans only once

//      input = input.replaceAll("\\?|\\r","<br>");
//      input = input.replaceAll("  "," &nbsp;");
//      input = input.replaceAll("[\\x00-\\x1F\\x7F-\\x9F]","");
//      return input;

      // this is the original code that was replacing quotes

//
//        StringBuffer retStr = new StringBuffer("");
//        int x = 0;
//        boolean lastIsSpace = false;
//        for(x = 0; x < input.length(); x++)        {
//            char c = input.charAt(x);
//
//            // Convert Return Chars
//            if ((c == '?') ||(c == 10))
//            {
//                retStr.append("<br>");
//                lastIsSpace = false;
//            }
//            else if ((c == '\'') ||(c == '\"'))
//            {
//                retStr.append('`');
//                lastIsSpace = false;
//            }
//            else if ((c == ' '))
//            {
//                if (lastIsSpace == false)
//                {
//                    retStr.append(c);
//                    lastIsSpace = true;
//                }
//                else
//                {
//                    retStr.append("&nbsp;");
//                    lastIsSpace = false;
//                }
//            }
//            else if (! Character.isISOControl(c))
//            {
//                retStr.append(c);
//                lastIsSpace = false;
//            }
//        }
//        return retStr.toString();
    }

  	/** #DG738
  	 * Compares <code>str1</code> to <code>str2</code>, taking into account
  	 * possible <code>null</code> values being passed in.
  	 *
  	 * @param str1 The first <code>String</code> to compare
  	 * @param str2 The second <code>String</code> to compare
  	 *
  	 * @return whether the two strings were equal
  	 */
  	public static boolean isEqual(String stra, String strb)
  	{
  		return stra == null? 
  			strb == null:
  			stra.equals(strb);
  	}
  	
  	/** #DG738
  	 * convenience method to test if a string is 'empty', ie., null or only 'spaces'
  	 * @param sVal input string
  	 * @return true if string is 'empty'
  	 */
    public static boolean isEmpty(String sVal) {
      if(sVal == null) 
      	return true;
      return sVal.trim().length() <= 0;
    }  

  	/** #DG738
  	 * convenience method to assure input string is not null and trimmed
  	 * @param sVal input string
  	 * @return normalized string
  	 */
    public static String normalize(String sVal) {
      if(sVal == null) 
      	return "";
      return sVal.trim();
    }   
    
  	/** #DG838
  	 * trim the size of string 
  	 * 
  	 * 	to be used when logging very long strings to avoid cluttering log file
  	 * 
  	 * @param sVal input string
  	 * @param max string size
  	 * 
  	 * @return trimmed string with trailing ...
  	 */
    public static String trimToSize(String sVal, int maxSize) {
      if(sVal == null || sVal.length() <= maxSize) 
      	return sVal;
      return sVal.substring(0, maxSize);
    }    
}
