package com.basis100.deal.util.collections;

import java.util.*;
import com.basis100.deal.entity.*;
import MosSystem.Mc;

/**
 * A comparison function, which imposes a <i>total ordering</i> on some
 * collection of objects.  Comparators can be passed to a sort method (such as
 * <tt>Collections.sort</tt>) to allow precise control over the sort order.
 * Comparators can also be used to control the order of certain data
 * structures (such as <tt>TreeSet</tt> or <tt>TreeMap</tt>).<p>
 *
 *  Note: this comparator imposes orderings that are inconsistent with equals.
 */


public class DJBorrowerOrderComparator implements Comparator
{

   /**
   *
   * Places  Borrowers with primary borrower flag == Y  first in List.
   * then borrowers ordered by borrowerId, guarantors ordered by borrowerId,
   * coventors ordered by borrowerId, etc..
   *
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   * @throws ClassCastException if the arguments' are not of type DealEntity.
   *
   */
  public int compare(Object o1, Object o2)
  {
     Borrower b1 = (Borrower)o1;
     Borrower b2 = (Borrower)o2;

     if (b1.getBorrowerTypeId() < b2.getBorrowerTypeId())
     {
       return -1;
     }
     else if (b1.getBorrowerTypeId() > b2.getBorrowerTypeId())
       {
         return 1;
       }
       else {
         if (b1.getBorrowerId() < b2.getBorrowerId()){
           return -1;
         } else if (b1.getBorrowerId() > b2.getBorrowerId())
            {
              return 1;
            }
            else {
              return 0;
            }
       }
  }

}
