package com.basis100.deal.util;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.services.util.XMLUtil;

/**
 * Utility class that generates DealFee for all copies in a deal
 * 
 * @since 5.0
 * @author Hiro
 * @version 1.0 Nov 14, 2011
 */
public class DealFeeCreator {

    private final static Logger logger = LoggerFactory.getLogger(DealFeeCreator.class);
    private final static String insertSQL = "INSERT INTO DEALFEE ( DEALFEEID, FEEID, DEALID, "
        + "COPYID, FEEAMOUNT, FEEPAYMENTMETHODID, FEESTATUSID, FEEPAYMENTDATE, PAYORPROFILEID, INSTITUTIONPROFILEID ) "
        + "VALUES ( DEALFEESEQ.NEXTVAL , ?, ?, ?, ?, ?, ?, ?, ?, ? )";

    private List<Entry> entryList;
    private Map<Integer, Fee> feeMapByFeeType = new HashMap<Integer, Fee>();

    public DealFeeCreator() {
        entryList = new ArrayList<DealFeeCreator.Entry>();
    }

    public void add(int feeTypeId, BigDecimal amount) {
        double value = XMLUtil.bigDecimal2Double(amount, 2);
        entryList.add(new Entry(feeTypeId, value));
    }

    public void add(int feeTypeId, double amount) {
        entryList.add(new Entry(feeTypeId, amount));
    }

    public void batchCreateDealFees(SessionResourceKit srk, Deal deal) throws Exception {

        if (entryList.isEmpty())
            return;

        PreparedStatement pstat = null;
        try {
            pstat = srk.getJdbcExecutor().getPreparedStatement(insertSQL);

            for (Deal aCopyOfDeal : deal.findAllCopies()) {
                for (Entry ent : entryList) {

                    // don't create record if amount = $0.0
                    if (ent.getAmount() <= 0.00d)
                        continue;

                    Fee fee = null;

                    try {
                        fee = findFeeByType(srk, ent.getFeeTypeId());
                    } catch (Exception e) {
                        logger.error("FeeType for id: " + ent.getFeeTypeId() + " not found.", e);
                        continue;
                    }

                    addInsertDealFeeToBatch(pstat, fee, aCopyOfDeal, ent.getAmount(), false);

                    if (fee.getOffSetingFeeTypeId() > 0) {
                        Fee offsetFee = findFeeByType(srk, fee.getOffSetingFeeTypeId());
                        addInsertDealFeeToBatch(pstat, offsetFee, aCopyOfDeal, ent.getAmount(), true);
                    }
                }
            }

            // execute batch
            int[] ret = pstat.executeBatch();
            if (logger.isDebugEnabled() && ret != null) {
                logger.debug("## batch inserted " + ret.length + " records ##");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if (pstat != null)
                pstat.close();
        }
    }

    private Fee findFeeByType(SessionResourceKit srk, int feeTypeId) throws RemoteException, FinderException {

        Fee fee = feeMapByFeeType.get(feeTypeId);
        if (fee == null) {
            fee = new Fee(srk, null);
            fee = fee.findFirstByType(feeTypeId);
            feeMapByFeeType.put(feeTypeId, fee);
        }

        return fee;
    }

    private static void addInsertDealFeeToBatch(PreparedStatement pstat, Fee fee, Deal deal, double feeAmount, boolean offset) throws SQLException {

        if (logger.isDebugEnabled())
            logger.debug("Creating DealFee record for dealid=" + deal.getDealId() + " copyid=" + deal.getCopyId() + " FeeId=" + fee.getFeeId()
                + " amount=" + feeAmount + " offset=" + String.valueOf(offset));

        pstat.setInt(1, fee.getFeeId());// 1.FEEID
        pstat.setInt(2, deal.getDealId());// 2.DEALID
        pstat.setInt(3, deal.getCopyId());// 3.COPYID
        pstat.setDouble(4, feeAmount);// 4.FEEAMOUNT
        pstat.setInt(5, fee.getDefaultPaymentMethodId());// 5.FEEPAYMENTMETHODID
        pstat.setInt(6, Mc.FEE_STATUS_REQUIRED);// 6.FEESTATUSID
        if (offset) {
            // not sure if this is correct, this is the same logic as in
            // MIFeeBuilder and MIDealFeeBuilder
            pstat.setNull(7, java.sql.Types.NULL);// 7.FEEPAYMENTDATE
            pstat.setInt(8, Mc.FEE_PAYOR_TYPE_NONE);// 8.PAYORPROFILEID
        } else {
            pstat.setDate(7, createPaymentDate(deal, fee));// 7.FEEPAYMENTDATE
            pstat.setInt(8, createPayorProfileId(deal, fee));// 8.PAYORPROFILEID
        }
        pstat.setInt(9, fee.getInstProfileId());// 9.INSTITUTIONPROFILEID

        // pool SQL for later execution
        pstat.addBatch();

    }

    private static int createPayorProfileId(Deal deal, Fee fee) {

        int payorType = fee.getFeePayorTypeId();

        switch (payorType) {
        case Mc.FEE_PAYOR_TYPE_SOURCE_FIRM:
            return deal.getSourceFirmProfileId();
        case Mc.FEE_PAYOR_TYPE_SOURCE_OF_BUSINESS:
            return deal.getSourceOfBusinessProfileId();
        case Mc.FEE_PAYOR_TYPE_SOLICITOR:
            try {
                PartyProfile pp = new PartyProfile(deal.getSessionResourceKit());
                pp.setSilentMode(true);
                pp = pp.findByDealSolicitor(deal.getDealId());
                if (pp != null)
                    return pp.getPartyProfileId();
            } catch (Exception e) {
                logger.error("unable to determine payor for type: Solicitor", e);
            }
            break;
        case Mc.FEE_PAYOR_TYPE_APPRAISER:
            try {
                PartyProfile pp = new PartyProfile(deal.getSessionResourceKit());
                Collection<PartyProfile> c = pp.findByDealAndType((DealPK) deal.getPk(), Mc.PARTY_TYPE_APPRAISER);
                if (c.isEmpty() == false) {
                    pp = (PartyProfile) CollectionUtils.get(c, 0);
                    return pp.getPartyProfileId();
                }
            } catch (Exception e) {
                logger.error("INGESTION: CMHC REPLY: unable to determine payor for type: Appraiser");
            }
            break;
        case Mc.FEE_PAYOR_TYPE_MORTGAGE_INSURER:
            return deal.getMortgageInsurerId();
        default:
            logger.error("unable to determine payor for type");
        }

        return Mc.FEE_PAYOR_TYPE_NONE;
    }

    private static java.sql.Date createPaymentDate(Deal deal, Fee fee) {

        int dateType = fee.getDefaultFeePaymentDateTypeId();
        Date feePaymentDate = null;

        switch (dateType) {
        case Mc.FEE_PAYMENT_DATE_TYPE_CURRENT_DATE:
            feePaymentDate = new Date();
            break;
        case Mc.FEE_PAYMENT_DATE_TYPE_OFFER_RETURN_DATE:
            feePaymentDate = deal.getReturnDate();
            break;
        case Mc.FEE_PAYMENT_DATE_TYPE_CURRENT_DATE_PLUS_10_DAYS:
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 10);
            feePaymentDate = c.getTime();
            break;
        case Mc.FEE_PAYMENT_DATE_TYPE_FUNDS_ADVANCED_DATE:
            feePaymentDate = deal.getEstimatedClosingDate();
            break;
        default:
            feePaymentDate = new Date();
            logger.error("unkown defaultFeePaymentDateTypeId = " + dateType);
        }

        return date2date(feePaymentDate);

    }

    private static java.sql.Date date2date(java.util.Date juDate) {
        /*
         * To conform with the definition of SQL DATE, the millisecond values
         * wrapped by a java.sql.Date instance must be 'normalized' by setting
         * the hours, minutes, seconds, and milliseconds to zero in the
         * particular time zone with which the instance is associated.
         */
        Calendar cal = Calendar.getInstance();
        cal.setTime(juDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new java.sql.Date(cal.getTimeInMillis());
    }

    private class Entry {

        private int feeTypeId;
        private double amount;

        private Entry(int feeTypeId, double amount) {
            this.feeTypeId = feeTypeId;
            this.amount = amount;
        }

        public int getFeeTypeId() {
            return feeTypeId;
        }

        public double getAmount() {
            return amount;
        }
    }

}
