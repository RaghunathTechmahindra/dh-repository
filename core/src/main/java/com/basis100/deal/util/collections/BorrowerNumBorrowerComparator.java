/*
 * Created on 21-Jan-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.deal.util.collections;

import java.util.Comparator;

import com.basis100.deal.entity.Borrower;

/**
 * @author crutgaizer
 *
 * sorts borrowers in the order: Primary Borrower, non-primary, co-applicant 
 * then by borrower number
 * this is based on the assumption that primaryBorrowerFlag is a char(1) and possible values are:
 * {"Y", "N", "C"}
 * 
 */
public class BorrowerNumBorrowerComparator implements Comparator
{
  public int compare(Object o1, Object o2) 
  {
   Borrower b1 = (Borrower)o1;
   Borrower b2 = (Borrower)o2;
   
   if ((b1.getPrimaryBorrowerFlag().length() > 1) || (b2.getPrimaryBorrowerFlag().length() > 1)){     
     return -2;  // error
   }
   
   int i1 = (int)b1.getPrimaryBorrowerFlag().charAt(0);     
   int i2 = (int)b2.getPrimaryBorrowerFlag().charAt(0);     
   
   if (i2 < i1)         // "Y" > "N" > "C"
   {
     return -1;
   }
   else if (i2 > i1)
     {
       return 1;
     }
     else if (b1.getBorrowerNumber() < b2.getBorrowerNumber())
     	{
         return -1;
     	} 
     	else if (b1.getBorrowerNumber() > b2.getBorrowerNumber())
        {
          return 1;
        }
        else 
        {
          return 0;
        }
  }
}
