package com.basis100.deal.util;

public class MathUtil
{

  /**
   *  rounds a double value to the indicated number of decimal places. If the number of
   *  decimal places is negative the rounding occurs to the left of the decimal place.
   *  If the number of decimal places is positive rounding occurs to the right of the decimal place.
   *  Rounding occurs towards positive infinity. If the indicated decimal value is
   *  greater than the available decimal places no rounding occurs and the additional spaces are
   *  occupied by zeros.
   *  @param dp number of decimal places to round
   *  @param num the number to round
   */
  public static String roundToString(double num, int dp)
  {

    String zeros = "000000000000000000000000000000000000000000000000000";
    double exp = Math.pow(10, dp);
    boolean negative = false;

    if(num < 0)
    {
      num = num * -1;
      negative = true;
    }

    // test length of dp applicable section of num
    // in the first statment dp is positive but greater than available spaces
    // in the second dp is zero. Return as a non floatin point number
    // in the third dp is negative but greater than the available decimal places
    // return 0

    String test = null;

    if(dp > 0)
    {
      test = String.valueOf(num);
      int index = test.indexOf('.');
      if(!(index == -1))
      {
        test = test.substring(index + 1, test.length());

      }
      if(test.length() <= dp)
      {
        int numzero = dp - test.length();
        if(numzero >= 0)
        {
          return(negative ? "-" : "") + String.valueOf(num) + zeros.substring(0, numzero);
        }
      }

    }
    else if(dp == Math.abs(0))
    {
      return(negative ? "-" : "") + String.valueOf((long)num);
    }
    else
    {
      test = String.valueOf((long)num);
      if(Math.abs(dp) >= test.length())
      {
        return "0";
      }
    }

    num += .5 / exp;
    String truncatedNum = String.valueOf((long)(num * exp));
    int truncatedLen = truncatedNum.length();

    if(dp < 0)
    {
      dp = Math.abs(dp);
      return truncatedNum + zeros.substring(0, dp);
    }
    else
    {
      int end = truncatedLen - dp;
      if(end > 0)
      {
        return(negative ? "-" : "") + truncatedNum.substring(0, end) + "." + truncatedNum.substring(end);
      }
      else
      {
        return(negative ? "-" : "") + "0." + zeros.substring(0, end) + truncatedNum;
      }
    }

  }

  public static String roundToString(long num, int dp)
  {
    return roundToString((double)num, dp);
  }

  public static String roundToCurrencyString(double value)
  {
    return roundToString(value, 2);
  }

  public static double round(double val, int dp)
  {
    dp = (int)Math.pow(10, dp);
    val = val * dp;
    val = Math.round(val);
    val = val / dp;

    return val;
  }

  /**
   *  returns the lesser of two values if they are not zero.
   *  zero values are eliminated from the comparison. If both values are zero
   *  return zero;
   */
  public static double minNotZero(double x, double y)
  {
    if(x == 0 && y == 0)
    {
      return 0;
    }
    if(x == 0)
    {
      return y;
    }
    if(y == 0)
    {
      return x;
    }
    return Math.min(x, y);
  }

  /**
   *  returns the lesser of two values if they are not zero.
   *  zero values are eliminated from the comparison. If both values are zero
   *  return zero;
   */
  public static double minNotZero(int x, int y)
  {
    if(x == 0 && y == 0)
    {
      return 0;
    }
    if(x == 0)
    {
      return y;
    }
    if(y == 0)
    {
      return x;
    }
    return Math.min(x, y);
  }

  /**
   *  returns a random integer between the min and max value
   *  Created by : Billy 05May2004
   */
  public static int getRandomInt(int min, int max)
  {
    Double theD = new Double((Math.random()*(max - min))+0.5d);
    //return new Double(Math.random()*(max - min)).intValue() + min;
    return theD.intValue() + min;
  }

}
