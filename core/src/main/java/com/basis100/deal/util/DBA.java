package com.basis100.deal.util;


import java.util.Date;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.EntityBuilder;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;


/**
  * DBA - Database Assistant
  *
  * Commonly used database entity query functions
  *
  **/


public class DBA
{

  public static Deal getDeal(SessionResourceKit srk, int dealId, int copyId) throws Exception
  {
      return getDeal(srk, dealId, copyId, null);
  }

  public static Deal getDeal(SessionResourceKit srk, int dealId, int copyId, CalcMonitor dcm) throws Exception
  {
        Deal deal = null;

      try
      {
          deal = new Deal(srk, null);
          deal.findByPrimaryKey(new DealPK(dealId, copyId));

          if (dcm != null)
            deal.setCalcMonitor(dcm);
      }
      catch (Exception e)
      {
          SysLogger logger = srk.getSysLogger();
          logger.error("DBA :: getDeal :: error encountered locating deal Id = " + dealId + " (copyId = " + copyId + ")");
          logger.error("DBA :: getDeal :: error = " + e.getMessage());
          throw e;
      }

      return deal;
  }

  /** recordDealStatus
  *
  * Record new deal status code on the deal pointed to by the page. Actions:
  *
  *   . set the status code.
  *
  *     In addition to deal (copy) provided the method propogates the status to all unlocked scenarios. Status
  *     setting for gold copy and provided copy (if not gold) is captured in transaction history (is audited).
  *     Status setting for all other unlocked scenarios is not recorded in transaction history.
  *
  *   . if status code is one of the following the user is also recorded as the current
  *     approver of the deal:
  *
  *        DEAL_APPROVAL_RECOMMENDED
  *        DEAL_EXTERNAL_APPROVAL_REVIEW
  *        DEAL_APPROVED
  *        // these get set secondary to DEAL_APPROVAL_RECOMMENDED so shouldn't
  *        // be encountered - but in case this changes ...
  *        DEAL_HIGHER_APPROVAL_REQUESTED
  *        DEAL_JOINT_APPROVAL_REQUESTED
  *
  *   . if the copy pointed to by the page is not the gold copy then the status is set and
  *     [optionally] approver recorded for the gold copy as well (gold copy stored)
  *
  *   . deal resolution condition code is set for the deal if provided (arg <drc> != -1).
  *     When the satus is being set to denied the code automatically set the resolution
  *     to one of:
  *
  *         DEAL_RES_CONDITION_DEAL_UNDERWRITER_DENIED // when assigned underwriter denies deal
  *         DEAL_RES_CONDITION_HIGHER_APPROVER_DENIED  // when anyone else denies deal
  *
  * Result - deal entity (updated but not stored - caller must call deal.ejbStore())
  *
  **/

  public static Deal recordDealStatus(SessionResourceKit srk, int dealId, int copyId, int ds, int drc) throws Exception
  {
      return recordDealStatus(srk, null, dealId, copyId, ds, drc);
  }

  // used when caller can provide deal entity
  public static Deal recordDealStatus(SessionResourceKit srk, Deal deal, int ds, int drc, int dummy) throws Exception
  {
      int dealId = 0;
      int copyId = 0;

      if (deal != null)
      {
        dealId = deal.getDealId();
        copyId = deal.getCopyId();
      }
      return recordDealStatus(srk, deal, dealId, copyId, ds, drc);
  }

  private static Deal recordDealStatus(SessionResourceKit srk, Deal deal, int dealId, int copyId, int ds, int drc) throws Exception
  {
      if (deal == null)
        deal = DBA.getDeal(srk, dealId, copyId);

      // set status
      Date dateStatus = new Date();
      deal.setStatusId(ds);
      deal.setStatusDate(dateStatus);

      // set resolution condition (automatically for denial else if provided)
      if (ds == Mc.DEAL_DENIED && drc == -1)
      {
          if (deal.getUnderwriterUserId() == srk.getExpressState().getUserProfileId())
              drc = Mc.DEAL_RES_CONDITION_DEAL_UNDERWRITER_DENIED;
          else
              drc = Mc.DEAL_RES_CONDITION_HIGHER_APPROVER_DENIED;
      }

      if (drc != -1)
        deal.setResolutionConditionId(drc);

      boolean recordAsApprover = false;

      //  record approver if necessary
      if (ds == Mc.DEAL_APPROVAL_RECOMMENDED ||
          ds == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW ||
          ds == Mc.DEAL_APPROVED ||
          ds == Mc.DEAL_HIGHER_APPROVAL_REQUESTED ||
          ds == Mc.DEAL_JOINT_APPROVAL_REQUESTED)
      {
          recordAsApprover = true;
          deal.recordApproval(srk.getExpressState().getUserProfileId());
      }

      // do the same thing on current gold copy (if not manipulating the gold copy)
      Deal goldDeal = null;
      if (!(deal.getCopyType().equals("G")))
      {
          MasterDeal md = new MasterDeal(srk, null);
          md = md.findByPrimaryKey(new MasterDealPK(dealId));
          goldDeal = new Deal(srk, null);
          goldDeal = goldDeal.findByPrimaryKey(new DealPK(dealId, md.getGoldCopyId()));

          goldDeal.setStatusId(ds);
          goldDeal.setStatusDate(dateStatus);

          if (drc != -1)
            goldDeal.setResolutionConditionId(drc);

          if (recordAsApprover)
              goldDeal.recordApproval(srk.getExpressState().getUserProfileId());

          goldDeal.ejbStore();
      }

      // propogate change (silently - non-audit) to all other non-locked scenarios
      //
      // Note: It is not necessary to avoid hitting the provided scenario since update here does not
      //       prevent proper auditing of change via entity update (e.g. when deal.ebjStore() called)

      String sql = "UPDATE Deal set StatusId  = " + ds;

      if (drc != -1)
            sql += ", ResolutionConditionId = " + drc;

      sql += ", StatusDate = " + deal.sqlStringFrom(dateStatus) +
            " where DealId = " + deal.getDealId();

      JdbcExecutor jExec = srk.getJdbcExecutor();

      jExec.executeUpdate(sql);
      
      //4.4 above SQL will cause data mismatch between db and cache
      //so removing all deal entity objects from cache
      ThreadLocalEntityCache.removeFromCache(Deal.class);

      return deal;
  }

  // used after commitment offer

  public static Deal recordDealStatus(SessionResourceKit srk, Deal deal, int ds) throws Exception
  {
      // set status
      Date dateStatus = new Date();
      deal.setStatusId(ds);
      deal.setStatusDate(dateStatus);

      // do the same thing on current gold copy (if not manipulating the gold copy)
      //
      //  Notes:
      //
      //  . After commitment we expect only the gold copy to be manioulated but this may change

      if (!(deal.getCopyType().equals("G")))
      {
          int dealId = deal.getDealId();

          MasterDeal md = new MasterDeal(srk, null);
          md = md.findByPrimaryKey(new MasterDealPK(dealId));
          Deal goldDeal = new Deal(srk, null);
          goldDeal = goldDeal.findByPrimaryKey(new DealPK(dealId, md.getGoldCopyId()));

          goldDeal.setStatusId(ds);
          goldDeal.setStatusDate(dateStatus);
          goldDeal.ejbStore();
     }

      return deal;
  }

  /** recordDealStatus
  *
  * Record new deal status code on the deal pointed to by the page. Note, this version is called
  * only when escalating approval.
  *
  Actions:
  *
  *   . set the status code and record next approver (if provided) as current approver for the deal.
  *
  *     In addition to deal (copy) provided the method propogates the status (and other changes) to all unlocked
  *     scenarios including the gold. Updates for gold copy and provided copy (if not gold) are captured in
  *     transaction history (are audited). Updates for all other unlocked scenarios are not recorded in
  *     transaction history.
  *
  * Result - deal entity (updated but not stored - caller must call deal.ejbStore())
  *
  **/

  //

  public static Deal recordDealStatus(SessionResourceKit srk, Deal deal, int ds, int nextApprover) throws Exception
  {
      // set status
      Date dateStatus = new Date();
      deal.setStatusId(ds);
      deal.setStatusDate(dateStatus);

      boolean jointApprover = ds == Mc.DEAL_JOINT_APPROVAL_REQUESTED;

      // if precaution only - should never happen ... but
      if (nextApprover != -1)
        deal.assignCurrentApprover(nextApprover, jointApprover);

      // do the same thing on current gold copy (if not manipulating the gold copy)
      if (!(deal.getCopyType().equals("G")))
      {
          int dealId = deal.getDealId();

          MasterDeal md = new MasterDeal(srk, null);
          md = md.findByPrimaryKey(new MasterDealPK(dealId));
          Deal goldDeal = new Deal(srk, null);
          goldDeal = goldDeal.findByPrimaryKey(new DealPK(dealId, md.getGoldCopyId()));

          goldDeal.setStatusId(ds);
          goldDeal.setStatusDate(dateStatus);

          // if precaution only - should never happen ... but
          if (nextApprover != -1)
            goldDeal.assignCurrentApprover(nextApprover, jointApprover);

          goldDeal.ejbStore();
     }

     // propogate change (silently - non-audit) to all other non-locked scenarios
     //
     // Note: It is not necessary to avoid hitting the provided scenario since update here does not
     //       prevent proper auditing of change via entity update (e.g. when deal.ebjStore() called)

     String sql = "UPDATE Deal set" +
                  "  StatusId  = " +                ds +
                  ", StatusDate = " +               deal.sqlStringFrom(dateStatus) +

                  // approval history details ...

                  ", UnderwriterUserId = " +        deal.getUnderwriterUserId() +
                  ", SecondApproverId = " +         deal.getSecondApproverId() +
                  ", ThirdApproverId = " +          deal.getThirdApproverId() +
                  ", FourthApproverId = " +         deal.getFourthApproverId() +
                  ", FifthApproverId = " +          deal.getFifthApproverId() +
                  ", JointApproverId = " +          deal.getJointApproverId() +
                  ", FinalApproverId = " +          deal.getFinalApproverId() +

                  " where DealId = " + deal.getDealId() +
                  " AND ScenarioLocked = 'N'";

      JdbcExecutor jExec = srk.getJdbcExecutor();

      jExec.executeUpdate(sql);

      //4.4 above SQL will cause data mismatch between db and cache
      //so removing all deal entity objects from cache
      ThreadLocalEntityCache.removeFromCache(Deal.class);
      
      return deal;
  }

  /**
  *
  *  createEntity methods - access to EntityBuilder
  *
  **/

    public static DealEntity createEntity(SessionResourceKit srk, String entityName, CalcMonitor dcm) throws Exception
    {
    return createEntity(srk, entityName, null, dcm);
    }


    public static DealEntity createEntity(SessionResourceKit srk, String entityName,
                                                                    DealEntity container, CalcMonitor dcm) throws Exception
    {
        return createEntity(srk, entityName, container, dcm, true);
    }

    public static DealEntity createEntity(SessionResourceKit srk, String entityName,
                                                                    DealEntity container, CalcMonitor dcm,
                                                                    boolean calledInTransaction) throws Exception
    {
        DealEntity de             = null;

        try
        {
            if (calledInTransaction == false)
                srk.beginTransaction();

          de = EntityBuilder.createEntity(entityName,
                                                                           srk,
                                                                           dcm,
                                                                           container);

            if (calledInTransaction == false)
              srk.commitTransaction();
        }
    catch (Exception ex)
        {
            if (calledInTransaction == false)
                srk.cleanTransaction();

      SysLogger logger = srk.getSysLogger();

            logger.error("Error @DBA.createEntity: entity name = <" + entityName + ">, called in tx = <" + calledInTransaction + ">");
            // Catherine, 16-May-05 -------- begin ---------
            // logger.error(ex);
            logger.error(StringUtil.stack2string(ex));
            // Catherine, 16-May-05 -------- end ---------

      throw new Exception("Entity creation exception.");
        }

        return de;
    }

  //
  // Routines to get SQL ready values (e.g. for usage in UPDATE, INSERT statements, etc.)
  //
  // Example: setting value of a VARCHAR2 column:
  //
  //              String usevalue = sqlStringFrom(myvalue)
  //
  //          While it may appear that such a call is superflous the method attends to details such as
  //          embedded "'" characters.  In addition the string returned is encloded in "'" delimiters and
  //          ready for use in a sql statement.
  //
  //          There are overloaded version of sqlStringFrom() for typical Java/primitive types - e.g. Date.

  public static String sqlStringFrom(String in, int maxLen)
  {
    if(in == null)
      return "null";

    if (in.length() > maxLen)
      in = in.substring(0, maxLen);

    in = StringUtil.makeQuoteSafe(in);

    return "\'" + in + "\'";
  }


  public static String sqlStringFrom(String in)
  {
    if(in == null)
      return "null";

    in = StringUtil.makeQuoteSafe(in);

    return "\'" + in + "\'";
  }

   public static String sqlStringFrom(int in)
   {
      return String.valueOf(in);
   }

    public static String sqlStringFrom(long l)
   {
      return String.valueOf(l);
   }

   public static String sqlStringFrom(short in)
   {
      return String.valueOf((int)in);
   }

   public static String sqlStringFrom(char c)
   {
     return "\'" + String.valueOf(c) + "\'";
   }

   public static String sqlStringFrom(double d)
   {
      return String.valueOf(d);
   }

   public static String sqlStringFrom(float f)
   {
      return String.valueOf(f);
   }

   public static String sqlStringFrom(boolean b)
   {
     String value = "N";

     if(b)value = "Y";

     return "\'" + value + "\'";
   }

   /**
    * Default date format for MOS Oracle: YYYY-MM-DD HH24:MI:SS
    *
    * Note: This method assumes that date fields in the database are nullable. Hence
    *       the string "null" is returned if the date proided is null.
    */
   public static String sqlStringFrom(java.util.Date d)
   {
      String strRep = TypeConverter.stringTypeFrom(d);

      if(strRep == null)
        return "null";
      else
        return "\'" + strRep + "\'";
   }


}


