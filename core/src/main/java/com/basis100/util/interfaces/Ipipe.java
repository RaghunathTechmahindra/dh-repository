package com.basis100.util.interfaces;

import java.awt.*;

public interface Ipipe
{
    public void pipe(Object o) throws Exception;
}