package com.basis100.util.thread;

/*
 * A busy flag object can be used as a monitor to control access to a resource.
 *
 * Methods:
 *
 * . public BusyFlag(Object resource)
 *   public BusyFlag()
 *
 *   Constructors. The first version allows specification of an resource object - see setResource()
 *
 * . public synchronized void getBusyFlag()
 *
 *   This method associates the calling thread with the calling thread. If another thread is
 *   currently associated with the busy flag the method will block, and the method will remain
 *   blocked until the busy flag can be uniquely associated calling thread (i.e. until
 *   the 'other' thread relases the busy flag via a call to freeBusyFlag()). Upon return we can say
 *   that the busy flag has been 'locked' by the calling thread.
 *
 * . public synchronized void freeBusyFlag()
 *
 *   This method disassociates the calling thread from the busy flag object. This method must be
 *   called by a thread that has called getBusyFlag() to 'unlock' the busy flag. Until this call
 *   is make other threads that are attempting to 'lock' the flag (via calls to getBusyFlag())
 *   will remain blocked.
 *
 * . public void setResource(Object resource)
 *
 *   This method assigns an object to the busy flag. Presumably the object provided is a resource
 *   object that the busy flag is providing guarded access rights to.
 *
 * . public void getResource(Object resource)
 *
 *   Return the object associated with the busy flag at construction (or via a call to
 *   setresource()).
 *
 *
 * Example:
 *
 * Let UnsafePrintResource be a third party print resource that is known to be not thread
 * safe. As UnsafePrintResource is from a third party it is not possible to modify it for
 * thread safe operation. Now if we wish to use a UnsafePrintResource object in a multi-thread
 * implementation (e.g. multiple active threads wish to use the print object) we must devise
 * a method to ensure safe operation .... BusyFlag to the rescue!
 *
 * Example:
 *
 *  // create monitor that uses a BusyFlag to control access to a UnsafePrintResource
 *
 *  class UnsafePrintResourceMonitor
 *  {
 *     static BusyFlag  monitorBusyFlag;
 *
 *     // singleton!
 *     private UnsafePrintResourceMonitor()
 *     {
 *        // create the print resource object to be controlled
 *        UnsafePrintResource upr = new UnsafePrintResource();
 *
 *        // set up the busy flag to provide access control
 *        monitorBusyFlag = new BusyFlag(upr);
 *     }
 *
 *     // get exclusive access to print resource
 *     public static UnsafePrintResource getPrintResource()
 *     {
 *        monitorBusyFlag.getBusyFlag(); // blocks if anyone else using the print resource
 *
 *        // got control of busy flag so have exclusive usage of print resource
 *        return (UnsafePrintResource)monitorBusyFlag.getResource();
 *     )
 *
 *     // release usage of print resource
 *     public static void releasePrintResource()
 *     {
 *        monitorBusyFlag.freeBusyFlag();
 *     }
 *
 *  }
 *
 *
 **/

public class BusyFlag
{
    protected Thread busyflag = null;
    protected int busycount = 0;
    protected Object resource;

    public BusyFlag()
    {
    }

    public BusyFlag(Object resource)
    {
      setResource(resource);
    }

    // --- //

    public synchronized void getBusyFlag()
    {
        while (tryGetBusyFlag() == false)
        {
            try
            {
                wait();
            } catch (Exception e) {}
        }
    }

    public synchronized void freeBusyFlag()
    {
        if ( getBusyFlagOwner() == Thread.currentThread())
        {
            busycount--;
            if (busycount == 0 )
            {
                busyflag = null;
                notify();
            }
        }
    }

    public void setResource(Object resource)
    {
        this.resource = resource;
    }

    public Object getResource()
    {
        return resource;
    }

    public boolean isBusy()
    {
        return busyflag != null;
    }

    //
    // Less frequently used methods ...
    //

    public synchronized boolean tryGetBusyFlag()
    {
        if ( busyflag == null )
        {
            busyflag = Thread.currentThread();
            busycount = 1;
            return true;
        }
        if ( busyflag == Thread.currentThread())
        {
            busycount++;
            return true;
        }

        return false;
    }


    public synchronized Thread getBusyFlagOwner()
    {
        return busyflag;
    }
}