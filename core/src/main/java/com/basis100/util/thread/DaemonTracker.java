package com.basis100.util.thread;

import java.lang.*;

/*
 * Utility to allow a Java VM to maintain a number of daemon threads. Normally a VM will
 * shut down daemon threads if there are no user threads running. This would be a problem
 * if the 'no user thread' condition was the norm - e.g. VM starts and a number of daemon
 * threads launched with the user thread (deamon launcher) then terminating.
 *
 * This class avoid the problem mentioned above by ensuring that a user thread is running as
 * long as at least one daemon thread is running; the user thread is launched internally. This is
 * accomplished by adhering to the following protocol:
 *
 *   . a single DaemonTracker object is used to track all daemon threads started
 *   . when a daemon thread is launched the method daemonStarted() is called
 *   . when a daemon thread is terminated the method daemonTerminarted() is called
 *
 * When the last daemon is terminated the internal user thread will also terminate. This will
 * the VM to cleanup in a normal fasion.
 *
 * WARNING
 * -------
 *
 * The scheme depends upon detection of the termination of each deamon thread being tracked and
 * care must be taken to ensure termination detection regardless of the cause (e.g. mormal
 * termination and trermination due to error). If abnormal termination of a daemon thread is
 * not detected the user thread run internally will never end (i.e. daemonTerminated() not
 * called for the error daemon and hence the tracker will believe that at least one deamon
 * is active even if all other termainate mormally.
 *
 **/

public class DaemonTracker implements java.lang.Runnable
{
    private int daemonCount = 0;

    public synchronized void daemonStarted()
    {
        daemonCount++;
        if (daemonCount == 1)
        {
            Thread t = new Thread(this);
            t.setDaemon(false);
            t.start();
        }
    }

    public synchronized void daemonTerminated()
    {
        --daemonCount;
        if (daemonCount == 0)
        {
            notify();
        }
    }


    public void run()
    {
        synchronized (this)
        {
            while ( daemonCount != 0 )
            {
                try
                {
                    wait();
                }
                catch (InterruptedException ex) {};
            }
        }
    }
}