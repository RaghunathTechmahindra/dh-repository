package com.basis100.util.thread;

import java.lang.*;
import java.util.*;

/*
 * This class maintains a list of scheduled jobs. Jobs are launched on a scheduled basis and can
 * be repeated (relaunched) periodically. The caller may specify a limit on the number of time a job
 * should run (e.g. 10 times) or indicate that the job should repeat indefinitely. The following
 * method is used to schedule a job:
 *
 *   public void executeAtAndRepeat(Runnable job, Date when, long repeat, int count)
 *
 *   where: <job> is the runnable object to execute - the job is 'run' by calling its run() method
 *
 *          <when> is a timestamp that indicates the date and time the job should run (if the
 *          timestamp is in the past the job will be eligible to run immediately).
 *
 *          <repeat> indicated the periodic frequency for repeating the job. This is may be a specific
 *          time interval (in milliseconds) or one of the following:
 *
 *                    MONTHLY, YEARLY, HOURLY, DAILY, WEEKLY, WEEK_DAILY
 *
 *                    WEEK_DAILY --> means daily but not on weekends
 *
 *          <count> is the maximum number of time to repeat a job as discussed above. The following
 *          constants (with obvious meanings) are also supported:
 *
 *                    ONCE, FOREVER
 *
 *
 * Various overloaded versions of executeAtAndRepeat() are provided for convenience; also see closely
 * related executeInAndRepeat() methods - these generally dicate the initial run time at some interval
 * of time in the future (e.g. run in ten minutes). Finally there is execute(Runnable job) that runs
 * the specified job immediately (as soon as possible) one time only.
 *
 * Daily Jobs With a Day Schedule
 * ------------------------------
 *
 * There is a special version of executeAtAndRepeat() that takes an additional argument, a
 * DaySchedule object. This version is valid only if the <repeat> value is DAILY or WEEK_DAILY.
 * Basically the day schedule object dictates valid run periods during the day (active periods in the
 * day schedule) and periods during which the job cannot run (inactive periods in the day schedule).
 *
 * Typically a daily job to be run according to a day schedule would run continuously during the active
 * periods of the day. For example, job runs from start of day until 4:00am (first active period), and
 * from 9:00pm until the end of the day (second active period); each calendar day is treated
 * separately without melding of active period at end of one day into active period at beginning
 * of next day!
 *
 *   Aside
 *
 *   The job scheduler does not provide any mechanism to stop a job once it has been launched. In theory
 *   a job run by the scheduler would perfom some task and then terminate. If the job is repeatable
 *   (e.g. runs every 4 hours) then it is assumed that an given invocation would end before the
 *   same job was scheduled to run again! - the scheduler performs no checks and hence could potentially
 *   launch multiple instances of the same job.
 *
 * Thus for daily jobs with day schedules, where a given job invocation is intended to run continuously
 * for the active portion of the day it was launched for, additional support is required to stop the job
 * at the end of the active period. This support is provided as follows:
 *
 * . The scheduler checks to see if the job to be run (the runnable object <job> passed in the
 *   call to executeAtAndRepeat()) implements the StopOnTimeTrigger interface. If the job does then:
 *
 *   . the scheduler creates a TimeTrigger object that is scheduled to 'go off' when the job
 *     should stop (i.e. at the end of the active period the job is launched for).
 *
 *   . the time trigger is then passed to the job via the setTimeTrigger() method (this is a required
 *     method for any object that implements the StopOnTimeTrigger interface).
 *
 * The job itself is responsible for stopping on the time trigger; see class TimeTrigger for an
 * illustrative code sample of a thread that stops on a time trigger.
 *
 * Usage of Pre-allocated Thread Pool to Run Jobs
 * ----------------------------------------------
 *
 * The class supports usage of a pre-allocated pool of threads to run jobs. The size of the pool (number
 * of threads) is specified at construction; see class ThreadPool for details. If zero is specified a
 * thread pool is not used, rather a new thread is allocated to run each job launched.
 *
 * Scheduler Administration
 * ------------------------
 * The scheduler begins operation immediately upon instantiation; meaning no special actions are
 * required after instantiation to permit scheduling and running of jobs. The scheduler can be stopped
 * by calling shutdown(). After the call the scheduler will not launch any new jobs - see method for
 * particulars regarding jobs currently running.
 *
 * WARNING
 * -------
 * Currently no check is performed to validate usage of a day schedule (i.e. no check to ensure
 * repeat interval is one of DAILY or WEEK_DAILY).
 *
 **/


public class JobScheduler implements java.lang.Runnable
{
    public final static int  ONCE    =  1;
    public final static int  FOREVER = -1;

    // periodic scheduling constants (for jobs that repeat)
    public final static long MONTHLY    = -1;
    public final static long YEARLY     = -2;
    public final static long WEEK_DAILY = -3;

    public final static long HOURLY = (long)60*60*1000;
    public final static long DAILY = 24 * HOURLY;
    public final static long WEEKLY = 7 * DAILY;

    // Define the Max Wait time :
    // Problem if wait() for too long i.e. the wait time is not accurate.
    // e.g. In order to better off the situation we use a shorter Wait time (e.g. 1 hour)
    //  -- By BILLY 21May2002
    public final static long MAXWAITTIME = (long)60*60*1000;  // 1 Hour in Mill Secs
    //===================================================================================

    private ThreadPool tp;
    private DaemonTracker dTracker = new DaemonTracker();
    private Vector jobs = new Vector(100);

    private Vector timeTriggers = new Vector();

    private boolean stop = false;

    public JobScheduler(int poolsize)
    {
        tp = (poolsize > 0) ? new ThreadPool(poolsize) : null;
        Thread js = new Thread(this);
        js.setDaemon(true);
        js.start();
    }

    // helper class to store job specifics

    private class JobNode
    {
        public Runnable job;
        public Date executeAt;
        public DaySchedule dSched;
        public long interval;
        public int count;
    }

/*
 * Service thread. Basically this thread runs scheduled jobs.
 *
 **/

    public void run()
    {

        while (stop == false)
        {
            long waitTime = runJobs();

            synchronized (this)
            {
                try
                {
                    wait(waitTime);
                }
                catch (Exception e) {};
            }
        }
    }

/*
 * Stop the scheduler. Causes the scheduler to stop launching jobs. No attempt is make to
 * stop jobs currently running with the exception of jobs that should stop on time triggers, which
 * are signaled to stop by stopping the timers.
 *
 * If the scheduler used a thread pool it is shut down as well. However, jobs curently in the
 * pools' queue are run before the pool shuts down. The method blocks until the thread pool
 * has shut down.
 *
 * WARNING
 *
 * If the thread pool is servicing a job that runs continuously (i.e. the job run() method never
 * returns control to the service thread) this method would block forever.
 *
 */

    public void shutdown()
    {
        stop = true;

        // stop timers for jobs running on timers
        stopTimeTriggers();

        // notify the service thread to stop
        synchronized (this)
        {
            notify();
        }

        // wait until thread pool (if one) is no longer running any job
        if (tp != null)
        {
            try
            {
                tp.waitForAll(true);
            }
            catch (InterruptedException e) {;}
        }
    }

    public void execute(Runnable job)
    {
        executeIn(job, (long)0);
    }

    public void executeIn(Runnable job, long millis)
    {
        executeInAndRepeat(job, millis, 1000, ONCE);
    }

    public void executeInAndRepeat(Runnable job, long millis, long repeat, int count)
    {
        Date when = new Date(System.currentTimeMillis() + millis);
        executeAtAndRepeat(job, when, repeat, count);
    }

    public void executeAt(Runnable job, Date when)
    {
        executeAtAndRepeat(job, when, 1000, ONCE);
    }

    public void executeAtAndRepeat(Runnable job, Date when, long repeat)
    {
        executeAtAndRepeat(job, when, repeat, FOREVER);
    }

    public void executeAtAndRepeat(Runnable job, Date when, long repeat, int count)
    {
        executeAtAndRepeat(job, when, repeat, count, null);
    }

    public void executeAtAndRepeat(Runnable job, Date when, long repeat, int count, DaySchedule dSched)
    {
        JobNode tn = new JobNode();
        tn.job = job;
        tn.executeAt = when;
        tn.dSched = dSched;
        tn.interval = repeat;
        tn.count = count;
        addJob(tn);
    }


    public void cancel(Runnable job)
    {
        deleteJob(job);
    }

/*
 * Launch all jobs that are eligible to run (e.g. those whose exacute at timrestamp is not in the
 * future).
 *
 * Return a time interval (milliseconds) that corresponds to the timestamp in the future at which
 * the next eligible run job should be launched.
 *
 **/

    private synchronized long runJobs()
    {
        long minDiff = Long.MAX_VALUE;
        long diff = 0L;
        long now = System.currentTimeMillis();

        cleanTimeTriggers();

        int lim = jobs.size();

        for (int i=0; i < lim;)
        {
            if (stop == true)
                break;

            JobNode tn = (JobNode) jobs.elementAt(i);
            if (tn.executeAt.getTime() <= now)
            {
                // add job to thread pool if one, else allocate thread to run the job
                if (tp != null)
                {
                    tp.addRequest(tn.job);
                }
                else
                {
                    Thread jt = new Thread(tn.job);
                    jt.setDaemon(false);
                    jt.start();
                }

                setJobStopTrigger(tn);

                // update next run date - but eliminate job if not to run again
                if (updateJobNode(tn) == null)
                {
                    jobs.removeElementAt(i);
                    dTracker.daemonTerminated();
                    lim--;
                    continue;
                }
            }

            // revise next run time (including job just run scheduled to repeat)
            diff = tn.executeAt.getTime() - now;
            minDiff = Math.min(diff, minDiff);

            i++;
        }

        // Check if next wait time > Max -- By BILLY 21May2002
        if(minDiff > MAXWAITTIME)
          minDiff = MAXWAITTIME;

        return minDiff;
    }

/*
 * Called after a job is launched to determine if a stop timer should be created and passed to the
 * job - this applies for daily job with a day schedule (see class documentation).
 *
 **/

    private void setJobStopTrigger(JobNode job)
    {
        if (!(job.job instanceof StopOnTimeTrigger))
            return;

        if (job.dSched == null)
            return;

        // determine stop date
        Date stopDate = null;

        try
        {
            stopDate = job.dSched.getNextStateTransition(job.executeAt);

            // have a stop time of day - ensure on same date at start time

            Calendar cal = Calendar.getInstance();
            cal.setTime(job.executeAt);

            Calendar stopCal = Calendar.getInstance();
            stopCal.setTime(stopDate);

            cal.set(Calendar.HOUR_OF_DAY, stopCal.get(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE,      stopCal.get(Calendar.MINUTE));
            cal.set(Calendar.SECOND,      stopCal.get(Calendar.SECOND));
            cal.set(Calendar.MILLISECOND, 0);

            stopDate = cal.getTime();
        }
        catch (Exception e)
        {
            // active till end of day - so stop time is end of day

            Calendar cal = Calendar.getInstance();
            cal.setTime(job.executeAt);

            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE,      59);
            cal.set(Calendar.SECOND,      59);
            cal.set(Calendar.MILLISECOND, 0);

            stopDate = cal.getTime();
        }

        // create the time trigger
        TimeTrigger tt = new TimeTrigger(stopDate, false);

        // communicate to job
        StopOnTimeTrigger tJob = (StopOnTimeTrigger)job.job;
        tJob.setTimeTrigger(tt);

        // start the trigger
        tt.startTimer();

        // track timers (see shutdown())
        timeTriggers.addElement(tt);
    }


/*
 * Maintenance facility to discard any time trigger objects that have 'gone off'.
 *
 **/

    private void  cleanTimeTriggers()
    {
        int lim = timeTriggers.size();

        if (lim == 0)
          return;

        Vector tickingTimers = new Vector(lim);

        for (int i = 0; i < lim; ++i)
        {
            if (((TimeTrigger)timeTriggers.elementAt(i)).isTicking())
                tickingTimers.addElement(timeTriggers.elementAt(i));
        }

        timeTriggers = tickingTimers;
    }

/*
 * Facility to proactively stop time trigger objects being tracked.
 *
 **/

    private void  stopTimeTriggers()
    {
        int lim = timeTriggers.size();

        if (lim == 0)
          return;

        for (int i = 0; i < lim; ++i)
        {
            ((TimeTrigger)timeTriggers.elementAt(i)).stopTimer();
        }

        timeTriggers = new Vector(); // only interested in running timers
    }

    private JobNode updateJobNode(JobNode tn)
    {

        // ensure job eligible to be re-run
        tn.count = (tn.count == FOREVER) ? FOREVER : tn.count - 1;
        if (tn.count == 0)
            return null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(tn.executeAt);

        if (tn.interval == MONTHLY)
        {
            cal.add(Calendar.MONTH, 1);
            tn.executeAt = cal.getTime();
        }
        else if ( tn.interval == YEARLY)
        {
            cal.add(Calendar.YEAR, 1);
            tn.executeAt = cal.getTime();
        }
        else if (tn.interval == WEEK_DAILY || tn.interval == DAILY)
        {
            if (tn.dSched != null)
                setJobStartForDaySchedJob(tn, false);  // special handling for daily with day schedule
            else
                advanceDate(tn);
        }
        else
        {
            tn.executeAt = new Date(tn.executeAt.getTime() + tn.interval);
        }

        return tn;
    }

    private void setJobStartForDaySchedJob(JobNode job, boolean initialRun)
    {
        if (initialRun == true)
        {
            // for starters if the desired run time is in the past use current time
            if (job.executeAt == null || job.executeAt.getTime() < System.currentTimeMillis())
                job.executeAt = new Date();

            // check start time during an active period - if so start immediately
            if (job.dSched.stateAt(job.executeAt) == job.dSched.ACTIVE)
              return;
        }

        Date nextDate    = null;
        boolean followingDay = false;

        // determine next run time (special case: initial run and current time is during inactive period)
        try
        {
            nextDate = job.dSched.getFollowingActiveTime(job.executeAt);
        }
        catch (Exception e)
        {
            // no more active periods in day .. so will run at first active period in a future day
            followingDay = true;
            nextDate = job.dSched.getFirstActiveTime();
        }

        job.executeAt = nextDate;

        // check for run time of day set but date advance required
        if (followingDay == true)
            advanceDate(job);
    }

    private void advanceDate(JobNode job)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(job.executeAt);

        for (;;)
        {
            cal.add(Calendar.DATE, 1);
            if (job.interval == DAILY)
                break;

            int day = cal.get(Calendar.DAY_OF_WEEK);
            if (day == Calendar.SATURDAY || day == Calendar.SUNDAY)
                continue;

            break;
        }

        job.executeAt = cal.getTime();
    }


    private synchronized void addJob(JobNode job)
    {
        // ensure valid job start time for a dail job with a day schedule
        if (job.dSched != null)
            setJobStartForDaySchedJob(job, true);

        dTracker.daemonStarted();
        jobs.addElement(job);
        notify();
    }

    private synchronized void deleteJob(Runnable job)
    {
        for (int i=0; i<jobs.size(); i++)
        {
            if ( ((JobNode)jobs.elementAt(i)).job == job)
            {
                jobs.removeElementAt(i);
                dTracker.daemonTerminated();
                break;
            }
        }
    }

}



