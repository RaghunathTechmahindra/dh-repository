package com.basis100.util.thread;

/*
 * Implement a pool of threads to service run requests. The pool is constructed with a
 * desired number of service threads. Each thread in the pool simply services run requests
 * registered by calls to addRequest() and addRequestAndWait() (the latter for synchronous
 * requests - e.g. caller blocks until request serviced).
 *
 * Pool shutdown is accomplished by calling waitForAll(true) which causes all service threads to
 * complete (terminate) after all outstanding run requests have been serviced.
 *
 **/

import java.util.*;

public class ThreadPool
{
    // threads to service runable requests queue (number of thread set via constructor

    ThreadPoolThread poolThreads[];

    // LIFO (last in first out) runable requests queue. Object in queue are instances of
    // ThreadPoolRequest (wrapper for runnable object with option lock/monitor object)

    int nObjects = 0;    // number of requests (including requests current being serviced/run)
    Vector objects;

    // --- //

    boolean terminated = false;

    // pool busy flag :: this semaphore is used to ensure thread safe operation - for example, to ensure
    // that only one service thread can access the request queue at a time, etc.
    BusyFlag cvFlag;

    // service thread wait monitor - this object provides a convienient 'waiting' location for
    // service threads - when a request arrives the minitor is notified - this provides notification
    // to service threads of requests pending. The wait monitor is based on the pools' busy flag. This
    // means that when a service thread become active (due to a wait location notification) that
    // same thread is guaranteed to hold a exclusive 'lock' on the common semaphore (pools' busy flag).

    CondVar cvAvailable;

    // queue empty wait monitor - this object provides a convienient 'waiting' location for a caller
    // requiring notification that there are no outstanding requests (no requests pending and no
    // requests currently being services. Like <cvAvailable> this monitor is based on the pools' busy
    // flag.
    //
    // The monitor is used by the public waitForAll(true/false) method; e.g. waitForAll(true) is
    // called to shutdown the pool after all requests are completed.

    CondVar cvEmpty;

    public ThreadPool(int n)
    {
        cvFlag = new BusyFlag();

        cvAvailable = new CondVar(cvFlag);

        cvEmpty     = new CondVar(cvFlag);

        objects = new Vector();

        poolThreads = new ThreadPoolThread[n];

        for (int i=0; i<n; i++)
        {
            poolThreads[i] = new ThreadPoolThread(this, i);

            poolThreads[i].start();
        }
    }

/*
 * Add a run request - the method will return immediately before the request is serviced - this
 * allows the caller to implement a asychronous run request.
 *
 **/

    public void addRequest(Runnable target)
    {
        add(target, null);
    }

/*
 * Add a run request - the method will block until the request has been serviced - this allows the
 * caller to implement a synchronous run request.
 *
 * WARNING
 *
 * This call assumes that the target object (run by a pool thread) does not run continuously (e.g.
 * that it performs some function and then stops). If the target runs continuously (its run() method
 * never returns control) this method would never return control to the caller.
 *
 **/

    public void addRequestAndWait(Runnable target) throws InterruptedException
    {
        Object notifyAfterDoneObject = new Object();

        synchronized(notifyAfterDoneObject)
        {
            add(target, notifyAfterDoneObject);
            notifyAfterDoneObject.wait();
        }
    }

/*
 *
 * Pools maintenance facility to wait until all run requests have been services. Upon being
 * of no further work to perform if <terminate> = true then all service thread are terminated;
 * otherwise no specific action is performed upon being notified.
 *
 */

    public void waitForAll(boolean terminate) throws InterruptedException
    {
        // stop taking new requests if pool to be stopped.
        if (terminate == true)
            terminated = true;

        try
        {
            // wait until all current requests serviced
            cvFlag.getBusyFlag();
            while ( nObjects != 0)
            {
                cvEmpty.cvWait();
            }

            if (terminate)
            {
                // shutdown service threads
                for ( int i=0; i<poolThreads.length; i++)
                {
                    poolThreads[i].shouldrun = false;
                }
                cvAvailable.cvBroadcast();
            }
        }
        finally
        {
            cvFlag.freeBusyFlag();
        }
    }


    public void waitForAll() throws InterruptedException
    {
        waitForAll(false);
    }

/*
 * Add a runnable object to the thread pool's reqwuest queue. Notify service thread (one of pools'
 * threads) to wake to service the request.
 *
 **/

    private void add(Runnable target, Object notifyAfterDoneObject)
    {
        try
        {
            cvFlag.getBusyFlag();
            if (terminated)
            {
                throw new IllegalStateException("Thread pool has shut down");
            }
            objects.addElement( new ThreadPoolRequest(target, notifyAfterDoneObject));
            nObjects++;
            cvAvailable.cvSignal();
        }
        finally
        {
            cvFlag.freeBusyFlag();
        }
    }

    //
    // Helper inner class: Wrapper for runnable object and lock object that. An object of this type
    // represents a runnable request on the thread pool's queue.
    //

    class ThreadPoolRequest
    {
        Runnable target;
        Object notifyAfterDoneObject;

        ThreadPoolRequest(Runnable t, Object l)
        {
            target = t;
            notifyAfterDoneObject = l;
        }
    }

    //
    // Thread object that contains a flag to indicate if the thread should run (<shouldrun>).
    // Object of this type are the individiual threads maintained by the thread pool. Each such
    // thread services run requests apprearing the thread pool request queue, namely:
    //
    //    . retrieve request object (wrapper for Runnable object and optional 'notify after done' object)
    //      from pool's request queue
    //    . check 'should run' member - return immediately (stopping thread) if set to false
    //    . service the request (call Runnable objects' run() method)
    //    . if the request object specified a 'notify after done' object place a notify() on it
    //

    class ThreadPoolThread extends Thread
    {
        ThreadPool parent;
        boolean shouldrun = true;

        ThreadPoolThread(ThreadPool parent, int i)
        {
            super("ThreadPoolThread "+i);
            this.parent = parent;
        }

        public void run()
        {
            ThreadPoolRequest obj = null;
            while (shouldrun)
            {
                try
                {
                    // get exclusive lock on pool's busy flag
                    parent.cvFlag.getBusyFlag();

                    while (obj == null && shouldrun)
                    {
                        // read from pool's LIFO pool of runnable requests
                        try
                        {
                            obj = (ThreadPoolRequest)parent.objects.elementAt(0);
                            parent.objects.removeElementAt(0);
                        }
                        catch (ArrayIndexOutOfBoundsException aioble)
                        {
                            obj = null;
                        }
                        catch (ClassCastException cce)
                        {
                            System.err.println("Unexpected data");
                            obj = null;
                        }

                        // if no request object to service just hybernate (until notified)
                        if ( obj == null )
                        {
                            try
                            {
                                parent.cvAvailable.cvWait();
                            }
                            catch (InterruptedException ie)
                            {
                                return;
                            }
                        }
                    }
                }
                finally
                {
                    parent.cvFlag.freeBusyFlag();
                }

                if (!shouldrun)
                {
                    return;
                }

                // call run() directly to run the request in 'this' thread
                obj.target.run();


                // count of jobs in queue reduced (for job just ended). This was not done
                // before running the job since the count is number of requests INCLUDING those
                // currently being serviced.
                try
                {
                    parent.cvFlag.getBusyFlag();
                    nObjects--;
                    if (nObjects == 0)
                    {
                        parent.cvEmpty.cvSignal();
                    }
                }
                finally
                {
                    parent.cvFlag.freeBusyFlag();
                }

                if (obj.notifyAfterDoneObject != null)
                {
                    synchronized(obj.notifyAfterDoneObject)
                    {
                        obj.notifyAfterDoneObject.notify();
                    }
                }
                obj = null;
            }
        }
    }

}