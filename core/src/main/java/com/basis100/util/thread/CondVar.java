package com.basis100.util.thread;

/*
 * Utility class to aid a group of threads that must share a thread unsafe resource. Specifically,
 * access to the shared resource is controlled by a semaphore (instance of BusyFlag) - the object
 * may be provided upon construction or, if not, created automatically as an embedded object.
 *
 * The cvTimeWait() method is called by the current thread that has a 'lock' on the semaphore/
 * monitor prior to it (the current thread) hybernating; the method cvWait() is similar but
 * causes an indefinite wait until notified. During the wait the semaphore is relaese.  The service
 * ensure that the thread re-obtains the lock once becoming (again) active.
 *
 * Hence a group of thread coorperates over access to the shared resource by:
 *
 *  . all referencing the same ConVar object, or
 *  . all referencing the same BusyFlag object
 *
 * In the former calls to cvTimeWait() without specifing a BusyFlag object are made (and hence
 * the thread share the object created by the CondVar object. In the latter case calls to
 * cvTimeWait() specifing the common BusyFlag object are made.
 *
 * ---
 *
 * The class also provides cvSignal() and cvBroadcast() methods that allow the current thread to
 * notify waiting thread(s) [on this objects monitor] before hybernating (calling cvTimeWait() or
 * cvWait().
 *
 **/

public class CondVar
{
    private BusyFlag SyncVar;

    public CondVar()
    {
        this(new BusyFlag());
    }

    public CondVar(BusyFlag sv)
    {
        SyncVar = sv;
    }

/*
 *
 * Called to implement a thread wait where the caller is the 'owner' of (thread associated with)
 * the busy flag (semaphore) passed. The wait may be indefinite until external notification
 * or finite (<millis> != 0). During the wait the busy flag is relaesed, and after the wait the
 * busy flag is re-aquired.
 *
 * This service could be used by a group of threads that share a resource that cannot be
 * used asychronously (that is a resource that is not thread safe). A single busy flag object
 * could be allocated to act as an exclusive usage monitor for the shared resource.
 * When the current thread that has access to the shared resource wishes to hybernate
 * it can call this service to release the usage monitor. After the thread again becomes active
 * the service will restablish the thread's exclusive access (lock) on the monitor. Hence upon
 * return the calling thread again has exclusive usage of the busy flag/monitor.
 *
 **/

    public void cvTimeWait(BusyFlag sv, int millis) throws InterruptedException
    {
        int i = 0;
        InterruptedException errex = null;

        synchronized (this)
        {
            if ( sv.getBusyFlagOwner() != Thread.currentThread())
            {
                throw new IllegalMonitorStateException ("current thread not owner");
            }

            // release monitor

            while ( sv.getBusyFlagOwner() == Thread.currentThread())
            {
                i++;
                sv.freeBusyFlag();
            }

            // sleep : for time interval or indefinitely

            try
            {
                if ( millis == 0 )
                    wait();
                else
                    wait(millis);
            }
            catch ( InterruptedException iex)  {  errex = iex; }
        }

        // active! - re-aquire monitor

        for ( ; i>0; i--)
        {
            sv.getBusyFlag();
        }

        if ( errex != null) throw errex;

        return;
    }

    // wait until notified using this objects' busy flag flag (monitor)

    public void cvWait() throws InterruptedException
    {
        cvTimeWait(SyncVar,0);
    }

    // wait until notified using the specified busy flag flag (monitor)

    public void cvWait(BusyFlag sv) throws InterruptedException
    {
        cvTimeWait(sv,0);
    }

    // wait for time interval using this object embedded busy flag (monitor)

    public void cvTimeWait(int millis) throws InterruptedException
    {
        cvTimeWait(SyncVar, millis);
    }


/*
 * Notify a waiting thread; a thread waiting on this objects' monitor - i.e. a thread waiting as a
 * result of calling this objects' cvTimeWait() method). Calling thread must be the 'owner' of
 * (thread associated with) the busy flag (semaphore) passed.
 *
 */

    public synchronized void cvSignal(BusyFlag sv)
    {
        if (sv.getBusyFlagOwner() != Thread.currentThread())
        {
            throw new IllegalMonitorStateException("Current thread not owner");
        }
        notify();
    }

    // notify a waiting thread - caller must be 'owner' of this objects' busy flag
    public void cvSignal()
    {
        cvSignal(SyncVar);
    }


/*
 * Notify all waiting threads; threads waiting on this objects' monitor - i.e. threads waiting as a
 * result of calling this objects' cvTimeWait() method). Calling thread must be the 'owner' of
 * (thread associated with) the busy flag (semaphore) passed.
 *
 */

    public synchronized void cvBroadcast(BusyFlag sv)
    {
        if (sv.getBusyFlagOwner() != Thread.currentThread())
        {
            throw new IllegalMonitorStateException("Current thread not owner");
        }
        notifyAll();
    }

    // notify all waiting threads - caller must be 'owner' of this objects' busy flag

    public void cvBroadcast()
    {
        cvBroadcast(SyncVar);
    }

}