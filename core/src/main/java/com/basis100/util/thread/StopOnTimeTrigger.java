package com.basis100.util.thread;

public interface StopOnTimeTrigger
{
    public void setTimeTrigger(TimeTrigger timer);
    public TimeTrigger getTimeTrigger();

    // following methods match TimeTrigger methods - is is expected that implementing object would
    // save the TimeTrigger object (set via setTimeTrigger()) and delegate to same named methods.

    // particulatilly useful when the TimeTrigger object's internal clock is not started automatically
    // on construction - see class TimeTrigger for details.
    public void startTimer();

    public void stopTimer();
}
