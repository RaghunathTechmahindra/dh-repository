package com.basis100.util.thread;

import java.util.*;

/* A object of this class allows a day (24 hour period from 0:00 - 24:00) to be divided into active and
 * inactive periods. It is constructed with an array of values holding time of day values, each of
 * which corresponds to a transition point from active to inactive state or visa versa; the time of
 * day values must be provided in ascending order. The constructor also accepts an initial activity
 * state flag that activity state at the beginning of the day (i.e. at 0:00am).
 *
 * Example:
 *
 *      INACTIVE, 4 11 16 (constructor parameters - the time of day values would be passed in an array)
 *
 *      day modelled as:   INACTIVE from 0:00am up to but not including 4:00am
 *                         ACTIVE   from 4:00am up to but not including 11:00am
 *                         INACTIVE from 11:00am up to but not including 4:00pm (1600 hours)
 *                         ACTIVE   from 4:00pm till end of day (up to but not including 0:00am)
 *
 * Notes:
 *
 * 1) Start of day is specified as 0 Hours, 0 Minutes, 0 Seconds. However, internally this will
 *    be converted to 0 hours, 1 minutes, 0 seconds.
 *
 * 2) End of day is specified as 24 hours, 0 Minutes, 0 seconds. However, internall this will
 *    be converted to 23 hours, 59 minutes, 59 seconds. Actually any time of day specification with
 *    hour 24 is converted as specified.
 *
 * PROGRAMMER NOTE:
 *
 * These manipulation of start and end times of day are required to ensure a significant differential
 * between end of one day and start of following. This is because a daily job must be stopped no later than at
 * the end of the day, and can start no sooner than the beginning of the day. Hence if a job were active
 * until the end of the day, and active again at the start of the following day, we could not ensure that the
 * the stop (current) job occured and completed before the launch of the (new) job - the artifical differential
 * (one minute) provides the required delay between stop and start.
 *
 * This really is a limitation of the current implementation since a better solution would allow for
 * continuation of current job when it is active to the end of the day and again active as of the beginning
 * of the following - i.e. don't stop and start, rather remain active (running). This may be considered in a
 * future release.
 *
 *
 **/

public class DaySchedule
{
    public static final boolean INACTIVE  = false;
    public static final boolean ACTIVE    = true;

    private int [] transPoints;  // transition points:: each time of day in seconds where 0:00am = 0 seconds

    // to hold breakout of each transition time (hour / minute / second)
    private int hours[];
    private int minutes[];
    private int seconds[];

    private boolean [] dayStates;
    private boolean finalState;

    private int numberOfTransitions;

    public DaySchedule(boolean initialState, int [] hourPoints, int [] minInHourPoints)
    {
        numberOfTransitions = hourPoints.length;

        transPoints = new int[numberOfTransitions];
        hours       = new int[numberOfTransitions];
        minutes     = new int[numberOfTransitions];
        seconds     = new int[numberOfTransitions];

        dayStates   = new boolean[numberOfTransitions];

        // transition points
        for (int i = 0; i < numberOfTransitions; ++i)
        {
            hours[i] = hourPoints[i];

            if (minInHourPoints != null)
            {
                minutes[i] = minInHourPoints[i];
            }

            seconds[i]  = 0;

            // implement manipulation mentioned above
            if (hours[i] >= 24)
            {
               hours[i]   = 23;
               minutes[i] = 59;
               seconds[i] = 59;
            }

            if (hours[i] == 0 && minutes[i] == 0)
            {
                minutes[i] = 1;
                seconds[i] = 0;
            }

            transPoints[i] = (hours[i] * 60 * 60) + (minutes[i] * 60);
        }

        // states :: alter starting with initial state
        for (int i = 0; i < numberOfTransitions; ++i)
        {
            dayStates[i] = initialState;

            initialState = (initialState == true) ? false : true; // toggle
        }

        finalState = initialState;
    }


    public DaySchedule(boolean initialState, int [] hourPoints)
    {
        this(initialState, hourPoints, null);
    }

/*
 *  Returns the activity state corresponding to the time of day determined from the date passed.
 *  The date passed need not be fall on the current calendar day. Rather, the time of day information
 *  is extracted and compared with the day modelled by this object.
 *
 *  Example: Date passed = Jan 21 1964 16:33:34
 *
 *  Result:  ACTIVE (true) if day modeled with active period at 4:33:34 pm, otherwise INACTIVE
 *
 **/

    public boolean stateAt(Date dt)
    {
        Calendar cal = Calendar.getInstance();

        cal.setTime(dt);

        int todSec = (cal.get(Calendar.HOUR_OF_DAY)  * 60 * 60) + (cal.get(Calendar.MINUTE) * 60) +
                      cal.get(Calendar.SECOND);

        for (int i = 0; i < numberOfTransitions; ++i)
        {
            if (todSec < transPoints[i])
                return dayStates[i];
        }

        return finalState;
    }


/*
 *  Returns the date corresponding to the beginning of the first active period for the day
 *  modelled. The date returned falls on the current calendar day. If the initial activity state
 *  is ACTIVE the date returned will correspond to the beginning of the day (0:00 am) - actually
 *  manipulated to 1 minute beyond start of dfay as described above.
 *
 **/

    public Date getFirstActiveTime()
    {
        Calendar cal = Calendar.getInstance();

        cal.setTime(new Date());

        if (dayStates[0] == ACTIVE)
        {
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 1);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
        }
        else
        {
            cal.set(Calendar.HOUR_OF_DAY, hours[0]);
            cal.set(Calendar.MINUTE, minutes[0]);
            cal.set(Calendar.SECOND, seconds[0]);
            cal.set(Calendar.MILLISECOND, 0);
        }

        return cal.getTime();
    }

/*
 *  Returns the date corresponding to beginning of the next active period for the day relative to the
 *  time of day derived from the date passed. The date passed need not fall on the current calendar day -
 *  rather, the time of day information is extracted.
 *
 *  The result is a date object set to the beginning of the next ACTIVE period. The date returned
 *  ALWAYS falls on the current calendar day.
 *
 *  The method will throw a Exception if there is not following active period relative to the
 *  time of day passed (derived).
 *
 **/

    public Date getFollowingActiveTime(Date relDate) throws Exception
    {
        int numberOfTransitionsToActive = 1; // assuming time of day (from relDate) date in an inactive
                                             // period

        if (stateAt(relDate) == ACTIVE)
        {
            // time of day in active period - hence two transitions to following active:
            //     - next transition is from ACTIVE to INACTIVE
            //     - following that its INACTIVE to ACTIVE == following active
            numberOfTransitionsToActive++;
        }

        try
        {
            relDate = getNextStateTransition(relDate);

            if (numberOfTransitionsToActive == 2)
                relDate = getNextStateTransition(relDate);

            return relDate;
        }
        catch (Exception e)  { ; }

        throw new Exception("No following active period");
    }


/*
 *  Returns the next transition point (time at which state changes from INACTIVE to ACTIVE or visa versa)
 *  relative to the time of day determined from the date passed. The date passed need not fall on the
 *  current calendar day. Rather, the time of day information is extracted and compared with the day
 *  modelled by this object.
 *
 *  The result is a date object set to the time of day of the next activity transition point. The date
 *  returned ALWAYS falls on the current calendar day.
 *
 *  The method will throw a Exception if the time of day passed (derived) is beyond [or at] the last
 *  state transition in the day.
 *
 **/

    public Date getNextStateTransition(Date dt) throws Exception
    {
        Calendar cal = Calendar.getInstance();

        cal.setTime(dt);

        int todSec = (cal.get(Calendar.HOUR_OF_DAY)  * 60 * 60) + (cal.get(Calendar.MINUTE) * 60) +
                      cal.get(Calendar.SECOND);

        for (int i = 0; i < numberOfTransitions; ++i)
        {
            if (todSec < transPoints[i])
            {
                // sepecial case - transition point is exactly end of day - considered no furth transaition
                if (hours[i] == 24)
                  break;

                cal.setTime(new Date());

                cal.set(Calendar.HOUR_OF_DAY, hours[i]);
                cal.set(Calendar.MINUTE, minutes[i]);
                cal.set(Calendar.SECOND, seconds[i]);
                cal.set(Calendar.MILLISECOND, 0);

                return cal.getTime();
            }
        }

        throw new Exception("No further activity transitions");
    }


/*
 * Utility to generate activity transition time points based upon a periodic number of minutes
 * (from the start of a day).
 *
 * Result: if <hours> == true  - hour of day points corresponding to periodic transition
 *                    == false - minute of hour points corresponding to periodic transition
 *
 *
 * Example:
 *
 * Let periodic transition occur every 2 1/2 hours (every 150 minutes).
 *
 * The activity transition points are then:
 *
 *   2:30 am, 5:00am, 7:30am, 10:00am, 12:30 pm, 3:00 pm, 5:30 pm, 8:00 pm, 10:30 pm
 *
 * Hence:  DaySchedule(150, true) returns:
 *
 *   2        5       7       10       12        15       17        20       22
 *
 *         DaySchedule(150, false) returns:
 *
 *  30        0       30       0       30        0        30         0       30
 *
 **/

    public static int[] generateScheduleTransition(int minutes, boolean hours)
    {
        int minutesInDay = 24 * 60;

        int transitions = minutesInDay / minutes;

        int [] transPoints = new int[transitions];

        for (int i = 0; i < transitions; ++i)
        {
            transPoints[i] = minutes * (i + 1);

            if (hours == true)
                transPoints[i] /= 60;
            else
                transPoints[i] %= 60;
        }

        return transPoints;
    }
}
