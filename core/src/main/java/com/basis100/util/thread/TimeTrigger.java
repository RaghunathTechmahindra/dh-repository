package com.basis100.util.thread;

import java.util.*;

/*
 * Utility class that emulates a timer (e.g. stop watch functionality). The timer is considered to
 * be 'ticking' from the time it is instantiated until the time it 'goes off'. The following methods
 * are available to test the state of the timer:
 *
 *   isTicking() - true, while timer is 'ticking', false, after timer 'goes off'
 *   isOff()     - false, while timer is 'ticking', 'true', after timer 'goes off'
 *
 * Basically the caller chooses the method that fits his/her preferred metaphore.
 *
 * The 'off' time is specified at construction as a Date object (presumable set for some future point
 * in time); an alternate constructor allows the 'off' time to be determined as the current time plus
 * a time interval in milliseconds; allows construction of a timer set to 'go off' in 10 minutes for
 * example.
 *
 * An instance uses a internal clock (thread) to detect when the timer 'goes off'. However the
 * constructor parameter <startNow> controls when the internal clock should start. The possible values
 * are:
 *        true  - start internal clock immediately
 *        false - internal clock not started until method startTimer() called.
 *
 * Remember, the timer is considered to be 'ticking' immediately after construction - this is true
 * even if the internal clock is not started at construction; starting the clock explicitly is supported
 * to avoid the overhead of starting the clock thread in cases where, for whatever reasons, a timer is
 * instantiated but subsequently determined to be unnecessary.
 *
 * The method waitTillOff() blocks until the timer 'goes off' - i.e. used by a caller to block until
 * the timer 'goes off'.
 *
 * The basic idea behind this class is to control some process that is dependent on some future
 * timestamp. For example, support we have a thread that must stop at 2:00pm. This could be
 * coded as follows:
 *
 *       // run loop
 *       Date twoOclock = {date object for 2:00pm today};
 *       long two       = twoOclock.getTime();
 *       for (;;)
 *       {
 *          Date now = new Date();
 *          if (now.getTime() >= two)
 *              break;
 *          Thread.sleep(30);
 *       }
 *
 *       ... process code ...
 *
 * This can be simplified via the usage of a TimeTrigger object as follows:
 *
 *       // run loop
 *       Date twoOclock     = {date object for 2:00pm today};
 *
 *       TimeTrigger ttTwo = new TimeTrigger(twoOclock, true);
 *       ttTwo.waitTillOff();
 *
 *       ... process code ...
 *
 *
 **/

public class TimeTrigger extends Thread
{
    private boolean ticking = true;
    private boolean started = false;

    private long delayMillis;

    private Date offAt;

    public TimeTrigger(long delayMillis, boolean startNow)
    {
        this.delayMillis = delayMillis;

        if (startNow == true)
        {
            start();
            started = true;
            return;
        }

        // determine the date at which the timer 'goes off'
        offAt = new Date(System.currentTimeMillis() + delayMillis);
    }

    public TimeTrigger(Date offAt, boolean startNow)
    {
        this.offAt = offAt;

        if (startNow == true)
          startTimer();
    }


    public void startTimer()
    {
        if (started == true)
            return;

        long delay =  offAt.getTime() - System.currentTimeMillis();

        // date passed already in past (or is now ... whatever)
        if (delay <= 0)
        {
          stopTimer();
          return;
        }

        this.delayMillis  = delay;

        start();
        started = true;
    }



    public boolean isTicking()
    {
        return ticking;
    }


    public boolean isOff()
    {
        return (!isTicking());
    }

    public synchronized void waitTillOff()
    {
        if (isOff() == true)
          return;

        synchronized (this)
        {
            try
            {
                wait();
                stopTimer();
            }
            catch (InterruptedException e)
            {
                ;
            }
        }

        return;
    }


    public void run()
    {
        synchronized (this)
        {
            try
            {
                wait(delayMillis);
                stopTimer();
            }
            catch (InterruptedException e)
            {
                ;
            }

            return;
        }
    }

    public void stopTimer()
    {
        ticking = false;

        synchronized (this)
        {
            notifyAll();
        }
    }

}
