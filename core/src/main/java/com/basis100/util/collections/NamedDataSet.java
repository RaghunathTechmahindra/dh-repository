// NamedDataSet:
//
// This class is a container for a set of data records organized in manner similar to
// a (simple) table in a relational database. The contained itself is assigned a 'name'.
//
// An example will clarify the storage structure:
//
// Data Set Name: "Expenses"
//
//  Records:
//                                     Column #
//                           0             1          2
//                           ----------------------------                Subordinate
//     Record/Row #        "CATEGORY"    "QUANTITY"   "AMOUNT"         Named Data Set(s)
//      ----------      --------------------------------------
//          0            "Breakfest"       "1"       "5.67"
//          1              "Folio"         "1"       "234.56"          "FOLIOIDETAILS"
//          2              "Taxi"          "3"       "89.00"
//
// The example contains three records (numbered 0, 1, and 2). The records are organized
// in columns with key values "CATEGORY", "QUANTITY" and "AMOUNT" respectively (numbered
// 0, 1, and 2 respectively).
//
// Individual record data elements can be accessed in two ways:
//
// . by intersection row and column number (i.e. value at row 2, columns 1 is "3")
// . by row number and column key value (i.e. value at row 0, column key "AMOUNT" is "5.67")
//
// Any record in the set may also contain any number of subordinate named data sets.
// In the example above,  row 1 contains a subordinate data set named "FOLIOIDETAILS".
// The following is a example of what the "FOLIOIDETAILS" might contain:
//
//  Records:
//                                  Column #
//                             0                1
//                             ---------------------
//     Record/Row #        "SUBCATEGORY"     "AMOUNT"
//      ----------        ------------------------------
//          0               "Lodging"        "200.00"
//          1               "Lunch"           "34.56"
//
//  A subordinate data set is a true named dataset in every sense. Consequently any
//  record a subordinate data set may also contain subordinate data set(s).
//
//  Hence this class allows for the storage a of collection date sets organized in a
//  heirarchy where the structure from level to level may be disparate.
//
// Column Numbering
// ----------------
//
// A new column is dynamically added each time a data vale is stored specifying a new
// column key value (see setValueAt()). Column numbers follow the order in which column
// key values are introduced. While record data would normally be set by referencing a
// column key to determine the row, a version of setValueAt() takes the column number.
// The caller of this method must be certain of the order keys were introduced - column
// select via a column key specification is safer.
//
// Record Data
// ------------
//
// The data for records can be objects of any type (not limited to String as in the example
// above). It is likely that for a given column all records will hold objects of
// the same type (or null) - however, this is not enforced.
//
// Column Key Comparisons
// ----------------------
//
// While it is expected that column keys would be String objects this is not enforced -
// a column key may be any arbitary object. Column keys are internally stored in a
// Hashtable and lookup is done via the Hashtable get() method - see Programmer Note
// below. This information is provided to allow the programmer to consider the
// ramifications of using non-String objects as keys.
//
// PROGRAMMER NOTE
// ---------------
//
// A named data set object internally uses a KeyedItemsTable object to store data.
// Most methods delegate their work to the internal KeyedItemsTable object.
//
// When studying the KeyedItemsTable class you will note that semantics of
// the storage mechanism is actually the inverse of the logical repesentation
// described above. For example a new record is added a the end of a named data set
// (the number of rows increases by one). However, when a item is added to
// KeyedItemsTable object the number of columns increases by one. This is largely an
// issue of nomenclature.
//
// Interface
// ---------
//
// Construction:
//
//  . public NamedDataSet()
//  . public NamedDataSet(String name)
//
//  Create an empty named data set. The default version creates an object with name
//  "DEFAULT" - there is no facility to change the name after construction.
//
// Methods:
//
//  . public String getName()
//
//    Return the name of the data set.
//
//  . public int getRowCount()
//
//    Return the number of records in the table.
//
//  . public int getColumnCount()
//
//    Return the number of columns (cloumn keys) in the table
//
//  . public Enumeration getKeys()
//
//    Returns an enumeration of column keys in the table. The order of the elements in the
//    enumeration matches the order in which keys were introduced - here we rely
//    upon the functionality provided by the KeyedItemsTable delegate object used!
//
//  . addRecord()
//
//    Add a new record to the end of the data set. All data elements in the record are
//    set to null.
//
//  . insertRecord(int insertIndex)
//
//    Insert a new record at the specified index. All data elements in the record are
//    set to null.
//
//  . deleteRecord(int deleteIndex)
//
//    Delete the record at the specified index.
//
//  . setValueAt(Object value, Object columnKey)
//  . setValueAt(Object value, int columnNum)
//  . setValueAt(Object value, int recordNum, Object columnKey)
//  . setValueAt(Object value, int recordNum, int columnNum)
//
//    Store a value in the table.
//
//    If the record number is not specified the value is stored in the last record
//    added to the table.
//
//    If a column key is specified and the column key and does not match an existing
//    key a new column is created. The initial value stored (all records) in this (new)
//    column is null.
//
//    If a record number or/or column number specified is invalid an IllegalArgumentException
//    exception is thrown for invalid value(s).
//
//  . public Object getValueAt(int recordNum, Object columnKey) throws IllegalArgumentException
//  . public Object getValueAt(int recordNum, int columnNum) throws IllegalArgumentException
//
//    Retrieve a value from the table.
//
//    If the record number or column number is invalid an IllegalArgumentException
//    exception is thrown for invalid value(s).
//
//    If a column key is not found in the table an IllegalArgumentException
//    exception is thrown.
//
//  . public Vector getRecord(int recordNum)
//
//    Returns the specified record as a vector containing the values in the record.
//
//  . addColumn(Object columnKey)
//
//    Add a new column with associated column key to table. The initial value stored
//    (all records) in this (new) column is null.
//
//    Note, if the columnKey already exists in the table this call performs no action.
//
//  . public int getColumnIndex(Object columnKey)
//
//    Return the column number (index) matching the specified column key. Returns -1
//    if the specified key does not match an existing column key.
//
//  . public NamedDataSet addSubDataSet(String name)
//  . public NamedDataSet addSubDataSet(int recordNum, String name)
//
//    Creates (and returns) a subordinate named data set associated with a record. The
//    new set is constructed with the name provided and is empty - the caller must
//    populate using addRecord(), setValueAt(), etc.
//
//    If the record number is not specified the new set is associated with the last record
//    added to the table.
//
//    If a record number specified is invalid an IllegalArgumentException exception is
//    thrown.
//
//  . public NamedDataSet getSubDataSet(int recordNum, String name)
//
//    Returns the subordinate named data set with the name specified associated with the
//    specified record. Returns null if no such subordinate data set was added.
//
//    If a record number specified is invalid an IllegalArgumentException exception is
//    thrown.
//
//  . public boolean isSubDataSet(int recordNum, String name)
//
//    Returns true if a subordinate named data set with the name specified was
//    added to the specified record, else false.
//
//    If a record number specified is invalid an IllegalArgumentException exception is
//    thrown.
//
//  . public Enumeration getAllSubs(int recordNum)
//
//    Returns an Enumeration of all subordinate data sets added to the specified
//    record. The order of the data set in the enumeration matches the order in which they
//    were added (via addSubDataSet()) - if a particular data set name is reused the order
//    matches the original usage.
//
//    The enumeration will be empty (not null) if the record has no associated subordinates.
//
//    If the record number specified is invalid an IllegalArgumentException exception is
//    thrown.

package com.basis100.util.collections;

import java.util.*;

public class NamedDataSet implements java.io.Serializable
{
    private String name = "DEFAULT";

    private int rowCount = 0;
    private KeyedItemsTable kit = new KeyedItemsTable();

    public NamedDataSet()
    {
        name = "DEFAULT";
    }

    public NamedDataSet(String name)
    {
        this.name = name;
    }

    // --- //

    // store and retrieve value methods

    public String getName()
    {
        return name;
    }

    public int getRowCount()
    {
        return rowCount;
    }

    public int getColumnCount()
    {
        return kit.getNumberOfKeys();
    }

    public Enumeration getKeys()
    {
        return kit.getKeys();
    }

    // --- //

    // add record methods

    public void addRecord()
    {
        rowCount++;
        kit.addItem();
    }

    public void insertRecord(int insertIndex)
    {
        rowCount++;
        kit.insertItem(insertIndex);
    }

    public void deleteRecord(int deleteIndex)
    {
        rowCount--;
        kit.deleteItem(deleteIndex);
    }

    // --- //

    // store and retrieve value methods

    public void setValueAt(Object value, Object columnKey) throws IllegalArgumentException
    {
        kit.setValue(columnKey, value);
    }

    public void setValueAt(Object value, int columnNum) throws IllegalArgumentException
    {
        kit.setValue(columnNum, value);
    }

    public void setValueAt(Object value, int recordNum, Object columnKey) throws IllegalArgumentException
    {
        kit.setValue(recordNum, columnKey, value);
    }

    public void setValueAt(Object value, int recordNum, int columnNum) throws IllegalArgumentException
    {
        kit.setValue(recordNum, columnNum, value);
    }

    public Object getValueAt(int recordNum, Object columnKey) throws IllegalArgumentException
    {
        return kit.getValue(recordNum, columnKey);
    }

    public Object getValueAt(int recordNum, int columnNum) throws IllegalArgumentException
    {
        return kit.getValue(recordNum, columnNum);
    }

    public Vector getRecord(int recordNum)
    {
        return kit.getItem(recordNum);
    }

    public void addColumn(Object columnKey)
    {
        int r = kit.addNewKey(columnKey);

        if (r == -1)
        {
            throw new IllegalArgumentException("Column key not unique = " + columnKey);
        }
    }

    public int getColumnIndex(Object columnKey)
    {
        return kit.getKeyIndex(columnKey);

    }
    // --- //

    // subordinate named data sets

    public NamedDataSet addSubDataSet(String name) throws IllegalArgumentException
    {
        return addSubDataSet(rowCount - 1, name);
    }

    public NamedDataSet addSubDataSet(int recordNum, String name) throws IllegalArgumentException
    {
        NamedDataSet sub = new NamedDataSet(name);

        kit.put(recordNum, name, sub);

        return sub;
    }

    public NamedDataSet getSubDataSet(int recordNum, String name) throws IllegalArgumentException
    {
        return (NamedDataSet)kit.get(recordNum, name);
    }

    public boolean isSubDataSet(int recordNum, String name) throws IllegalArgumentException
    {
        return (kit.get(recordNum, name) != null);
    }

    public Enumeration getAllSubs(int recordNum)
    {
        return (kit.subElements(recordNum));
    }
}
