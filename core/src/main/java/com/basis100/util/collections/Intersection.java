package com.basis100.util.collections;

import java.util.*;


public class Intersection
{

   public static Collection intersect(Collection c1, Collection c2)
   {
     List temp = new ArrayList(2);
     temp.add(c1);
     temp.add(c2);
     return intersectAll(temp);
   }

   public static Collection intersect(Collection c1, Collection c2, Collection c3)
   {
     List temp = new ArrayList(2);
     temp.add(c1);
     temp.add(c2);
     temp.add(c3);
     return intersectAll(temp);
   }

   public static Collection intersect(Collection ofCollections)
   {
      return intersectAll(ofCollections);
   }

   private static Collection intersectAll(Collection c)
   {
      HashSet intersection = new HashSet();
      Collection shortest = null;
      Collection current = null;
      Iterator it = c.iterator();
      int count = 0;

      while(it.hasNext())
      {
         current = (Collection)it.next();

         if(count == 0) shortest = current;

         count++;

         if(current.size() < shortest.size())
         shortest = current;
      }

      Iterator iter = shortest.iterator();

       while(iter.hasNext())
       {
            Object value = iter.next();

            Iterator itt = c.iterator();

            while(itt.hasNext())
            {
               current = (Collection)itt.next();

               if(current.contains(value))
                 intersection.add(value);

            }
       }

       return intersection;
   }

}