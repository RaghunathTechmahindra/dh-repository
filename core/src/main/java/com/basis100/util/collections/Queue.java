
package com.basis100.util.collections;
import java.util.*;

public class Queue
{

  private LinkedList internal;

  public Queue()
  {
     internal = new LinkedList();
  }

  public synchronized void put(Object data)
  {
     internal.addFirst(data);
  }

  public synchronized Object get()
  {
    return internal.removeLast();
  }

  public boolean isEmpty()
  {
    return internal.isEmpty();
  }

  public int size()
  {
    return internal.size();
  }

  public ListIterator iterator()
  {
     return internal.listIterator();
  }

 }
