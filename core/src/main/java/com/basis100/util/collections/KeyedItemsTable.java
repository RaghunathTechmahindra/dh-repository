// KeyedItemsTable:
//
// This class is a logical extension of a Hashtable. While a Hashtable supports multiple
// key-value pairs it is limited to a single value for each key. Suppose we used a
// a Hashtable to hold the attributes for some composite object using the keys as meta-data
// (describing or naming the individual attributes). Consider Fig. 1 which uses a
// Hashtable to describe a composite 'expense item'
//
//      Fig 1. Hashtable as expense item (composite object)
//
//      key             value
//      ----------      -----------
//      "CATEGORY"      "Breakfest"
//      "QUANTITY"      "1"
//      "AMOUNT"        "5.67"
//
// Since Hashtable stores only one value for a given key we are forced to extend the model
// if we wish to use the keys as meta-data for multiple items (i.e. multiple expense
// items). KeyedItemsTable provides this extension (and more).
//
// A KeyedItemsTable logically organizes the data for multiple items in a multi-column
// table with one item per column. The table also features a 'key' column that
// that describes the content of each row in the table. The key column is treated
// as meta-data. Fig. 2 demonstrates the use of a KeyedItemsTable object to hold
// the atributes of multiple expense items.
//
//      Fig 2. KeyedItemsTable as multiple expense item (composite bjects)
//
//       Key            Item 1         Item 2    Item 3
//       ----------     ------         ------    ------
//       "CATEGORY"     "Breakfest"    "Folio"   "Taxi"
//       "QUANTITY"     "1"            "1"       "5"
//       "AMOUNT"       "5.67"         "234.56"  "89.00"
//
// Value in table can be accessed by specifying item and row indicies - both start
// from value 0 (i.e. first item is item 0 - not 1). The key column, as meta-data, is
// ignored in such accessess - i.e. in Fig. 2 the value at item number 1 and row number
// 2 is "234.56" (e.g. it would be "5.67" if the keys column were not ignored).
//
// Row Numbering
// -------------
//
// As should in Fig. 2 there are as many rows as there are keys in the table. A row is
// dynamically added each time a new key (see Key Comparisons) is encountered while
// storing item data (see setValue()). Row numbers follow the order in which keys are
// introduced (the first row is row 0, the next row 1, and so on). While item data
// would normally be set by referencing a key to determine the row, a version of
// setValue() takes the row number. The caller of this method must be certain of the
// order keys were introduced - row select via a key specification is safer.
//
// Item Data
// ---------
//
// The data for items can be objects of any type (not limited to String as Fig 1). It is
// expected that for a given row all items will hold objects of the same type (or null).
// However. this is not enforced.
//
// Key Comparisons
// ---------------
//
// While it is expected that key would be String object this is not enforced - a key may
// be any arbitary object. Keys are internally stored in a Hashtable and lookup is done via
// the Hashtable get() method. This imformation is provided to allow the programmer to
// consider the ramifications of using not String objects as keys.
//
// Keyed Sub-Item Data
// -------------------
//
// KeyedItemsTable also provides the facilities to attach keyed data objects to an
// individual item in the table. This is done by using a Hashtable to hold
// arbitary key-value pairs associated with a specified item - see put() and get()
// methods.
//
// The key and data objects provided in a call to put() are arbitary objects. For an example
// of the usage of keyed sub-item data see class see class NamedDataSet class (which
// delegates most of its work to a KeyedItemsTable object!). NamedDataSet uses
// this facility to support its 'subordinate named data sets' scheme.
//
// ------------------------------------------------------------------------------------
//
// Interface:
//
// Constructor(s):
//
// . public KeyedItemsTable()
//
//   Creates an empty (no items) table
//
// Methods:
//
// . public int getNumberOfKeys()
//
//   Return number of keys (rows) in the table.
//
// . public int getNumberOfItems()
//
//   Return number of items in the table
//
// . public Enumeration getKeys()
//
//   Returns an enumeration of keys in the table.  The order of the elements in the 
//   enumeration matches the order in which keys were introduced.
//
// . addItem()
//
//   Add a new item to the table. The new item is populated with nulls.
//
// . insertItem(int insertAtIndex)
//
//   Insert an item into the table. Existing items starting with the item at the specified
//   index are shifted right one position. All valies in the new item are set to null. If
//   the specified index is equal to the current number of items the item is added as the last item.
//
// . deleteItem(int deleteAtIndex)
//
//   Delete an item from the table. Items to the right of the deleted atem are shifted
//   to the left one position.
//
// . public void setValue(Object rowKey, Object value) throws IllegalArgumentException
// . public void setValue(int rowNum, Object value) throws IllegalArgumentException
// . public void setValue(int itemNum, Object rowKey, Object value) throws IllegalArgumentException
// . public void setValue(int itemNum, int rowNum, Object value) throws IllegalArgumentException
//
//   Store the value in the table.
//
//   If the item number is not specified the last values is stored in the last item
//   added to the table.
//
//   If a row key is specified and the row key and does not match an existing
//   key a new row is created. The initial value stored (all items) in this (new)
//   row is null.
//
//   If an item number and/or row number is specified an IllegalArgumentException
//   exception is thrown for invalid value(s).
//
// . public Object getValue(int itemNum, Object rowKey) throws IllegalArgumentException
// . public Object getValue(int itemNum, int rowNum) throws IllegalArgumentException
//
//   Retrieve a value from the table.
//
//   If the item number or row number is invalid an IllegalArgumentException
//   exception is thrown for invalid value(s).
//
//   If a row key is not found null is returned.
//
// . public Vector getItem(int itemNum)
//
//   Return the item specified by item number. The item is a vector containing the 
//   values in the item (all rows).
//
// . public int addNewKey(Object key)
//
//   Add a new row with specified key. The initial value stored (all items) in this (new)
//   row is null.
//
//   If the row key matches an existing row a new row is not added and the method
//   returns -1.
//
// . public int getKeyIndex(Object key)
//
//   Return the row number (key index) that matches the specified key.
//
//   If no key match found return -1.
//
// . public void put(Object key, Object value) throws IllegalArgumentException
// . public void put(int itemNum, Object key, Object value) throws IllegalArgumentException
//
//   Place the key-value pair into a item's Keyed Sub-Item Data Hashtable.
//
//   If the item is not provided the key-value pair is placed with the last item
//   added to the table.
//
//   If the item number is invalid an IllegalArgumentException exception is thrown.
//
// . public Object get(int itemNum, Object key) throws IllegalArgumentException
//
//   Returns the value stored with the specified key in the Keyed Sub-Item Data Hashtable
//   associated with the specified item. Null is returned if the key is not found.
//
//   If the item number is invalid an IllegalArgumentException exception is thrown.
//
// . public Enumeration subElements(int itemNum)
//
//   Returns an Enumeration of all values added to an item Keyed Sub-Item Data Hashtable.
//   The order of elements in the Enumeration matches the order in which the values 
//   were added. The enumeration is empty if no values were added.
//
//   If the item number is invalid an IllegalArgumentException exception is thrown.


package com.basis100.util.collections;

import java.util.*;

class KeyedItemsTable implements java.io.Serializable
{
    int numberOfKeys  = 0;
    int numberOfItems = 0;

    Hashtable keyData         = new Hashtable();
    Vector    orderedKeyData  = new Vector();  // to support a ordered (as introduced) Enumeration

    // holds the values for an item - a logical 'column'
    Vector  itemsData;     
    
    // elements hold a hash tables for each item with subordinate key-value pairs (see put())
    Vector  subHashTables;    
    
    // elements hold vectors to maintain the put order of keys for items with sub
    // key-value pairs - see subElements()
    Vector  subHashTablesOrderedKeys;

    public KeyedItemsTable()
    {
        itemsData                = new Vector();
        subHashTables            = new Vector();
        subHashTablesOrderedKeys = new Vector();
    }

    public int getNumberOfKeys()
    {
        return numberOfKeys;
    }

    public int getNumberOfItems()
    {
        return numberOfItems;
    }

    public Enumeration getKeys()
    {
        return new OrderedKeysEnumeration(orderedKeyData);
    }

    public void addItem()
    {
        insertItem(numberOfItems);
    }

    public void insertItem(int insertAtIndex)
    {
        Vector item = new Vector(numberOfKeys);
        item.setSize(numberOfKeys);

        itemsData.insertElementAt(item, insertAtIndex);
        
        numberOfItems++;

        // set sub items to null
        subHashTables.insertElementAt(null, insertAtIndex);
        subHashTablesOrderedKeys.insertElementAt(null, insertAtIndex);
    }
    
    public void deleteItem(int deleteAtIndex)
    {
        itemsData.removeElementAt(deleteAtIndex);
        
        numberOfItems--;

        subHashTables.removeElementAt(deleteAtIndex);
        subHashTablesOrderedKeys.removeElementAt(deleteAtIndex);
    }

    public void setValue(Object rowKey, Object value) throws IllegalArgumentException
    {
        setValue(numberOfItems-1, rowKey, value);
        return;
    }

    public void setValue(int rowNum, Object value) throws IllegalArgumentException
    {
        setValue(numberOfItems-1, rowNum, value);
        return;
    }

    public void setValue(int itemNum, Object rowKey, Object value) throws IllegalArgumentException
    {
        if (itemNum < 0)
            throw new IllegalArgumentException("Invalid item = " + itemNum);

        if (itemNum >= numberOfItems)
            throw new IllegalArgumentException("Invalid item = " + itemNum + ", Max = " + (numberOfItems-1));

        // find row number
        int rowNum      = 0;
        Integer iRowNum = (Integer)keyData.get(rowKey);

        if (iRowNum == null)
            rowNum = addNewKey(rowKey);
        else
            rowNum = iRowNum.intValue();

        // store data value
        ((Vector)itemsData.elementAt(itemNum)).setElementAt(value, rowNum);
    }


    public void setValue(int itemNum, int rowNum, Object value) throws IllegalArgumentException
    {
        if (rowNum >= numberOfKeys)
            throw new IllegalArgumentException("Invalid row = " + rowNum + ", Max = " + numberOfKeys);

        if (itemNum < 0)
            throw new IllegalArgumentException("Invalid item = " + itemNum);

        if (itemNum >= numberOfItems)
            throw new IllegalArgumentException("Invalid item = " + itemNum + ", Max = " + (numberOfItems-1));

        // store data value
        ((Vector)itemsData.elementAt(itemNum)).setElementAt(value, rowNum);
    }


    public Object getValue(int itemNum, int rowNum) throws IllegalArgumentException
    {
        if (rowNum >= numberOfKeys)
            throw new IllegalArgumentException("Invalid row = " + rowNum + ", Max = " + numberOfKeys);

        if (itemNum < 0)
            throw new IllegalArgumentException("Invalid item = " + itemNum);

        if (itemNum >= numberOfItems)
            throw new IllegalArgumentException("Invalid item = " + itemNum + ", Max = " + (numberOfItems-1));

        return((Vector)itemsData.elementAt(itemNum)).elementAt(rowNum);
    }

    public Object getValue(int itemNum, Object rowKey) throws IllegalArgumentException
    {
        Integer rowNum = (Integer) keyData.get(rowKey);

        if (rowNum == null)
            return null;

        return getValue(itemNum, rowNum.intValue());
    }
    
    public Vector getItem(int itemNum)
    {
        return (Vector)itemsData.elementAt(itemNum);
    }
    
    public int addNewKey(Object key)
    {
        Integer iRowNum = (Integer)keyData.get(key);

        if (iRowNum != null)
            return -1;
        
        keyData.put(key, new Integer(numberOfKeys));
        orderedKeyData.addElement(key);

        // extend all items for new key/rowNum
        for (int i = 0; i < numberOfItems; ++i)
            ((Vector)itemsData.elementAt(i)).addElement(null);

        int r = numberOfKeys;
        numberOfKeys++;

        return r;
    }
    
    public int getKeyIndex(Object key)
    {
        Integer iRowNum = (Integer)keyData.get(key);

        if (iRowNum != null)
            return -1;
            
        return iRowNum.intValue();
    }

    //
    // Sub-Item Data
    //

    public void put(Object key, Object value) throws IllegalArgumentException
    {
        put(numberOfItems-1, key, value);
    }

    public void put(int itemNum, Object key, Object value) throws IllegalArgumentException
    {
        if (itemNum < 0)
            throw new IllegalArgumentException("Invalid item = " + itemNum);

        if (itemNum >= numberOfItems)
            throw new IllegalArgumentException("Invalid item = " + itemNum + ", Max = " + (numberOfItems-1));

        Hashtable subHashTab = (Hashtable)subHashTables.elementAt(itemNum);
        
        Vector orderedSubKeys = null;
        
        if (subHashTab == null)
        {
            // create table
            subHashTab = new Hashtable();
            subHashTables.setElementAt(subHashTab, itemNum);
            
            orderedSubKeys = new Vector();
            subHashTablesOrderedKeys.setElementAt(orderedSubKeys, itemNum);
        }
        else
        {
            orderedSubKeys = (Vector)subHashTablesOrderedKeys.elementAt(itemNum);
        }

        subHashTab.put(key, value);
        
        // add to ordered sub-keys for item (if not previously added - possible restatement)
        if (!orderedSubKeys.contains(key))
            orderedSubKeys.addElement(key);
    }

    public Object get(int itemNum, Object key) throws IllegalArgumentException
    {
        if (itemNum < 0)
            throw new IllegalArgumentException("Invalid item = " + itemNum);

        if (itemNum >= numberOfItems)
            throw new IllegalArgumentException("Invalid item = " + itemNum + ", Max = " + (numberOfItems-1));

        Hashtable h = (Hashtable)subHashTables.elementAt(itemNum);

        if (h == null)
            return null;
            
        return h.get(key);
    }

    public synchronized Enumeration subElements(int itemNum)
    {
        if (itemNum < 0)
            throw new IllegalArgumentException("Invalid item = " + itemNum);

        if (itemNum >= numberOfItems)
            throw new IllegalArgumentException("Invalid item = " + itemNum + ", Max = " + (numberOfItems-1));

        Hashtable h = (Hashtable)subHashTables.elementAt(itemNum);
        
        if (h == null)
            return (new Vector()).elements();  // empty enumeration!
            
        // create ordered enumeration of sub elements
        Vector orderedKeys = (Vector)subHashTablesOrderedKeys.elementAt(itemNum);
        
        int numKeys = orderedKeys.size();
        
        Vector orderedValues = new Vector();
        
        for (int i = 0; i < numKeys; ++i)
        {
            orderedValues.addElement(h.get(orderedKeys.elementAt(i)));
        }
        
        // return a ordered enumeration
        return new OrderedKeysEnumeration(orderedValues);
    }
    
    // --- //
    

    // Inner class to support an enumeration of keys that are ordered according to key
    // introduction. Also used to support ordered subordinates value enumerations.

    class OrderedKeysEnumeration implements Enumeration
    {
        private Vector v = null;

        private int currentElement;
        private int size;

        OrderedKeysEnumeration(Vector orderedVector)
        {
            if (orderedVector == null || orderedVector.size() == 0)
                return;

            v = (Vector)orderedVector.clone();
            size = v.size();
            currentElement = 0;
        }

        public boolean hasMoreElements()
        {
            if (v == null)
                return false;

            return true;
        }

        public Object nextElement() throws NoSuchElementException
        {
            if (v == null)
                throw (new NoSuchElementException());

            Object r = v.elementAt(currentElement);

            currentElement++;

            if (currentElement == size)
                v = null;

            return r;
        }
    }
}

