package com.basis100.util.message;

/**
 * Title:        Your Product Name
 * Description:  Your description
 * Copyright:    Copyright (c) 1999
 * Company:
 * @author
 * @version
 */

public class ClientMessage
{

  private int messageId;
  private int languagePreferenceId;
  private String messageText;

  public ClientMessage()
  {
    messageId = -1;
    languagePreferenceId = -1;
    messageText = "";
  }

  public ClientMessage(int id, int lang, String text)
  {
    messageId = id;
    messageText = text;
    languagePreferenceId = lang;
  }

  public void setLanguagePreferenceId(int idToSet)
  {
    languagePreferenceId = idToSet;
  }

  public int getLanguagePreferenceId()
  {
    return languagePreferenceId;
  }

  public void setMessageId(int idToSet)
  {
    messageId = idToSet;
  }

  public int getMessageId()
  {
    return messageId;
  }

  public void setMessageText(String textToSet)
  {
    messageText = textToSet;
  }

  public String getMessageText()
  {
    return messageText;
  }

}