package com.basis100.util.message;

/**
 * Title:        Express
 * Description:  ClientMessageHandler handle db stored client message using EHCache
 * Copyright:    Copyright (c) 2007
 * Company:     Filogix Partnership Ltd.
 * @author      Midori
 * @version     update for 3.3:
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.cache.IMessageCache;
import com.filogix.express.core.ExpressCoreContext;

public class ClientMessageHandler {
    public static final String CLIENT_MASSAGE_CACHE_KEY = "clientMessageCache";
    protected static IMessageCache clientMessageCache = null;
    private static ClientMessageHandler INSTANCE = null;

  private Logger logger;
  private SessionResourceKit srk;

  public static synchronized ClientMessageHandler getInstance() throws Exception
  {
    if(INSTANCE == null)
    {
        INSTANCE = new ClientMessageHandler();                
    }

    return INSTANCE;
  }

  private ClientMessageHandler() throws Exception
  {
      // The logger
      logger = 
          LoggerFactory.getLogger(ClientMessageHandler.class);
      clientMessageCache = (IMessageCache) ExpressCoreContext
          .getService(CLIENT_MASSAGE_CACHE_KEY);
      clientMessageCache.load();
  }
  
//  private void updateCache(ClientMessage message){
//    if( messageCache.size() >= this.MAX_CACHE_SIZE ){
//      messageCache.remove(0);
//    }
//    messageCache.add(message);
//  }

  public ClientMessage findMessage(SessionResourceKit srk, int id, int lang) 
      throws Exception
  {
      logger.trace("findMessage:" + srk.getExpressState().getDealInstitutionId() 
                   + "#" + id + "#" + lang);
      
      return clientMessageCache.findMessage(srk.getExpressState().getDealInstitutionId(), 
                                            id, lang);
      
  }
}



