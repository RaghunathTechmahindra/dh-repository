package com.basis100.util.file;

import java.util.*;
import java.io.*;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;

public class IOUtil
{

   /**
    *  copies stream in to stream out
    *  @return true if the copy procedure does not throw an IOException
    */

   public static boolean streamCopy(InputStream in , OutputStream out)
   {
     int index = -2;

       try
       {
         while((index = in.read()) != -1)
         {
            out.write(index);
         }

         out.flush();
         in.close();
         out.close();
       }
       catch(IOException ioe)
       {
          try{ in.close(); out.close();} catch(Exception e){};
          return false;
       }

       return true;

   }

  /**
    *  <pre>
    *  Moves a files contents or copies contents to a new location
    *  @param src the file to move.
    *  @param dest the location to move the file to
    *  @param delete the souce file after copying if deleteSrc is set to true;
    *  @return true if the copy procedure does not throw an IOException
    *  Warning: this method does not maintain file meta information.
    *  </pre>
    */
  public static boolean moveFile(File src, File dest, boolean deleteSrc)
  {
     FileInputStream fin = null;
     FileOutputStream fout = null;

     try
     {
         fin = new FileInputStream(src);
         fout = new FileOutputStream(dest);
         
         if(!streamCopy(fin,fout)) throw new IOException("Failed to Copy " + src.getAbsoluteFile());

         if(deleteSrc)
           if(!src.delete()) throw new IOException("Failed To Delete " + src.getAbsoluteFile());

     }
     catch(IOException io)
     {
         io.printStackTrace();
         SysLog.error(io);
         return false;
     }
     
     return true;
  }
  /**
   * <pre>
   * Mimics moving and renaming a group of files.
   * Copies a vector of file objects to a new destination. The files in the vector are deleted from
   * the old location.
   *
   * @param  fileVector - the files to be moved
   * @param  destinationDirectory - the destination
   * @param  ext - the new extension for the moved files
   * @return a Vector containing references to the newly named and moved files
   * </pre>
   */
  public static Vector copyAndDelete(Vector fileVector, File destinationDirectory, String ext) throws IOException
  {
      String dest = destinationDirectory.getAbsolutePath();
      Enumeration e = fileVector.elements();
      Vector movedFiles = new Vector(fileVector.size());
      File outFile = null;

      while(e.hasMoreElements())
      {
        File currentFile = (File)e.nextElement();
        if(ext != null)
        {
           String newName = changeExtension(currentFile.getName(),ext);
           outFile = new File(dest + "\\" + newName);
        }
        else
        {
            outFile = new File(dest + "\\" + currentFile.getName());
        }

        movedFiles.add(outFile);

        if(!moveFile(currentFile, outFile, true))throw new IOException();
      }

     return movedFiles;
  }

  /**
   * <pre>
   * Mimics moving and renaming a file.
   * Copies a vector of file objects to a new destination. The files in the vector are deleted from
   * the old location.
   *
   * @param  in - the file to be moved
   * @param  destinationDirectory - the destination
   * @param  ext - the new extension for the moved file
   * </pre>
   */
  public static void copyAndDelete(File in, File destinationDirectory, String newExtension) throws IOException
  {
      String dest = destinationDirectory.getCanonicalPath();
      File outFile = null;

      if(newExtension != null)
      {
          String newName = changeExtension(in.getName(),newExtension);
          outFile = new File(dest + "\\" + newName);
      }
      else
      {
          outFile = new File(dest + "\\" + in.getName());
      }
      
      if(!moveFile(in, outFile, true))throw new IOException();
  }

  public static File changeExtension(File f, String newExtension) throws IOException
  {
     String file = f.getAbsolutePath();
     return new File(changeExtension(file,newExtension));
  }
  /**
   *  creates a new filename with the new extension.
   */
  public static String changeExtension(String filename, String newExtension) throws IOException
  {

     int index = filename.lastIndexOf('.');

     newExtension = newExtension.trim();

     if(!newExtension.startsWith("."))
      newExtension = "." + newExtension;

     if(index > 0) filename = filename.substring(0,index) + newExtension;
     else filename = filename + newExtension;

     return filename;
  }

  public static String[] appendTo(String[] existingArray, String[] anotherArray)
  {
     int diff = 0;
     if(existingArray != null) diff = existingArray.length ;

     int len = diff + anotherArray.length;

     String[] out = new String[len];

     for(int i = 0; i < diff; i++)
     {
       out[i] = existingArray[i];
     }

     for(int j = diff; j < len; j++)
     {
       out[j] = anotherArray[j - diff];
     }


     return out;

  }


  public static FileOutputStream constructSafeFOS(File dir, String name, String ext) throws IOException
  {
    if(dir == null || !dir.exists() || !dir.canWrite() || !dir.isDirectory())
     dir = new File("./");

    String dirstr = dir.getAbsolutePath() + "/";

    if(name == null)
     name = "tempfile";
    else
    {
      int ind = name.lastIndexOf(".");

      if(ind != -1)
       name = name.substring(0,ind);
    }

    if(ext == null)
     ext = "";
    else
    {
      ext = ext.trim();

      if(ext.startsWith("."))
      {
        ext = ext.substring(1);
      }
      
      ext = "." + ext;
    }

    File write = new File(dirstr + name + ext);

    FileOutputStream str = new FileOutputStream(write);

    return str;
  }



} 


