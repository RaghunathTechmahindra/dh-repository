package com.basis100.util.file;

import java.util.*;
import com.basis100.util.collections.*;
import java.io.*;

/**
 *   An extension of IniData that provides easy access
 *   to collections of sections and section data.
 *   Sections are arranged with a header label followed by
 *   a key = value set
 *
 *   The section order is maintained by using <code>SectionData</code> Objects
 *   with an index value. Order of keys is maintained as well.
 */
public class IniDataView extends IniData
{

  /**
   *  Vector of section names 
   */
  private Vector    sectionNames;
  /**
   *  Vector of SectionData objects
   */
  private Vector    sectionData;


  public IniDataView()
  {
    this.sectionNames = new Vector();
    this.sectionData = new Vector();
  }

  /**
   *  Load and read the ini file
   *  @return true if file is successfully loaded else return false
   */
  public boolean loadIniFile(String fileName)
  {
        super.loadIniFile(fileName);
        // open the file
        FileReader in  = null;
        BufferedReader iniFile = null;

        try
        {
          in = new FileReader(fileName);
          iniFile = new BufferedReader(in);
        }
        catch (Exception e)
        {
            try {iniFile.close(); in.close();} catch (Exception ee) {;}

            return false;
        }

        // read all sections and store keyword/value pairs for each

        boolean problemEncountered = false;
        String line = "";
        String sectionName = "";
        String keyword = "";
        String value = "";
        int index = 0;
        SectionData data = null;

        while(true)
        {
            try
            {
                line = iniFile.readLine();
                if (line == null)
                    break;

                if (line.trim().equals(""))
                    continue;
                if (line.trim().startsWith("/"))
                    continue;
                if (line.trim().startsWith("#"))
                    continue;
                if (line.trim().startsWith("*"))
                    continue;

                // new header section
                if (line.startsWith("[") )
                {
                    sectionName = readSectionName(line);

                    if (sectionName == null)
                    {
                        problemEncountered = true;
                        break;
                    }

                    // obtain/create section data
                    sectionNames.addElement(sectionName);
                    data = new SectionData(sectionName, index++);
                    sectionData.addElement(data);


                    continue;
                }

                // get keyword/value pair for the current section
                keyword = (line.substring(0,line.indexOf(keywordValueSeparator))).trim();
                value = (line.substring(line.indexOf(keywordValueSeparator)+1)).trim();
                data.add(keyword,value);
            }
            catch (EOFException e)
            {
                break;
            }
            catch (IOException e)
            {
                problemEncountered = true;
                break;
            }

        }

        // close file
        try {iniFile.close(); in.close();} catch (Exception ee) {;}

        if (problemEncountered)
            return false;

        return true;
    }

   /**
    * reads the section name from a String
    * that is assumed to hold a valid section name tag (starts with
    * ']') from an ini file.
    *
    * @return  section name or  null - if the tag is invalid
    */

    protected String readSectionName(String sectionNameLine)
    {
        int endNdx = sectionNameLine.indexOf("]");

        if (endNdx == -1)
            return null;

        String snm = sectionNameLine.substring(1, sectionNameLine.indexOf("]"));

        snm = snm.trim();

        if (snm.length() == 0)
            return null;

        return snm;
    }

    /**
     *  gets the value, within a section, that is associated with the given key
     *  @return null if the section or key are not found
     */
    public String getValue(String section, String key)
    {
        return getValue(section, key, null);
    }

    /**
     *  gets the value, within a section, that is associated with the given key
     *  @return defaultValue if the section or key are not found
     */
    public String getValue(String section, String key, String defaultValue)
    {
        try
        {
          SectionData data = this.getSection(section);
          return data.getValueFor(key);
        }
        catch(Exception e)
        {
          return defaultValue;
        }
    }

     /**
     *  gets the value, within a section, that is associated with the given key
     *  @return defaultValue if the section or key are not found
     */
    public String getValueIgnoreCase(String section, String key, String defaultValue)
    {
        try
        {
          SectionData data = this.getSectionIgnoreCase(section);
          return data.getValueForKeyIgnoreCase(key);
        }
        catch(Exception e)
        {
          return defaultValue;
        }

    }


    /**
     *  gets the index of a section within this ini file.
     *  @return the index or -1 if the section is not found.
     */
    public int getSectionIndex(String section, boolean ignoreCase)
    {
      try
      {
        if(ignoreCase)
        {
          return this.getSectionIgnoreCase(section).getSectionIndex();
        }
        else
        {
          return sectionNames.indexOf(section);
        }
      }
      catch(Exception e)
      {
        return -1;
      }

    }

    /**
     *  gets a Vector containing String representations of all the section names
     *  in this ini file.
     */
    public Vector getSectionNames()
    {
       return sectionNames;
    }

    /**
     *  gets the named section as a <code>SectionData</code> object
     *  @return SectionData or null if non matching name is found
     */
    public SectionData getSection(String name)
    {
      int index = sectionNames.indexOf(name);

      if(index == -1) return null;

      return (SectionData)sectionData.get(index);
    }

    public SectionData getSectionAt(int index)
    {
      if(index > sectionNames.size() || index < 0) return null;

      try
      {
         return (SectionData)sectionData.get(index);
      }
      catch(Exception e)
      {
         return null;
      }
    }

     /**
     *  gets the first section equal to name ignore case
     *  @return null if not found
     */
    public SectionData getSectionIgnoreCase(String name)
    {
      name = name.toUpperCase();
      int len = sectionData.size();

      for(int i = 0; i < len; i++)
      {
        SectionData current = (SectionData)sectionData.elementAt(i);
        if(current != null && current.getSectionName().equalsIgnoreCase(name))
        return current;
      }

      return null;
    }

    public boolean containsSection(String name)
    {
       return sectionNames.contains(name);
    }

    public boolean containsSectionIgnoreCase(String name)
    {
      Enumeration e = sectionNames.elements();

      while(e.hasMoreElements())
      {
        String current = (String)e.nextElement();
        if(current.equalsIgnoreCase(name)) return true;
      }
      
      return false;
    }

    /**
     *  Prints the contents of this IniDataView to the parameter PrintWriter
     *  destination.
     *  @param PrintWriter for printing.
     *
     */
    public void printTo(PrintWriter writer) throws IOException
    {
      Enumeration e = sectionData.elements();

      while(e.hasMoreElements())
      {
        SectionData d = (SectionData)e.nextElement();
        writer.write(d.toString());
      }

      writer.flush();
    }

   /**
   *  Prints the contents of this IniDataView to the parameter File
   *  destination.
   *  @param File - to print to
   *
   */
    public void printTo(File file) throws IOException
    {
       printTo(new PrintWriter(new FileWriter(file)));
    }

   /**
   *  Prints the contents of this IniDataView to the parameter File
   *  destination.
   *  @param String filename - to print to
   *
   */
    public void printTo(String file) throws IOException
    {
       printTo(new PrintWriter(new FileWriter(file)));
    }


  /**
   *  Prints the contents of this IniDataView to the print stream
   *  destination. This is a convienience for displaying this object's contents
   *  to the System console.
   *  @param String filename - to print to
   *
   */
    public void printTo(PrintStream ps) throws IOException
    {
       printTo(new PrintWriter(ps));
    }



    /**
     *  gets a Vector containing String representations of all the section keys
     *  in this ini file.
     */
    public Vector getSectionKeys(String section)
    {
      SectionData s = getSectionIgnoreCase(section);
      return s.getKeys();
    }

   /**
    *  gets the key corresponding to the given value in the named section
    *  within this ini file.
    * @return a String representation of the key or null if none is found
    */
    public String getKeyFor(String section, int value)
    {
        String valueString = null;

        try
        {
            valueString = String.valueOf(value);
        }
        catch(Exception e)
        {
          e.printStackTrace();
          return null;
        }

       return getKeyFor(section,valueString);
    }



    /**
    *  gets the key corresponding to the given value in the named section
    *  within this ini file.
    * @return a String representation of the key or null if none is found
    */
    public String getKeyFor(String section, String value)
    {
        if(section == null || value == null) return null;

        int index = sectionNames.indexOf(section);

        if(index == -1) return null;

        SectionData data = null;

        try
        {
           data = (SectionData)sectionData.get(index);
           return data.getKeyFor(value);
        }
        catch(Exception e)
        {
          e.printStackTrace();
          return null;
        }

    }

   /**
    *  gets the key corresponding to the given value ignoring case in the named section
    *  within this ini file.
    *  @return a String representation of the key or null if none is found
    */
    public String getKeyForValueIgnoreCase(String section, String value)
    {
        if(section == null || value == null) return null;

        int index = getSectionIndex(section,true);

        if(index == -1) return null;

        SectionData data = null;

        try
        {
           data = (SectionData)sectionData.get(index);
           return data.getKeyFor(value);
        }
        catch(Exception e)
        {
          e.printStackTrace();
          return null;
        }

    }
}

