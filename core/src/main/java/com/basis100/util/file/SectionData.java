package com.basis100.util.file;

import java.util.*;
import com.basis100.util.collections.*;
import java.io.*;

/**
 *  Represents a Section of the ini file.
 */
public class SectionData
{

   Vector keys;
   Vector values;
   String sectionName;
   int sectionIndex;


   public SectionData(String name, int index)
   {
     keys = new Vector();
     values = new Vector();
     this.sectionName = name;
     this.sectionIndex = index;
   }

   public void add(String key, String value)
   {
     this.keys.addElement(key);
     this.values.addElement(value);
   }

   public Vector getKeys(){ return keys;}

   public Vector getValues(){ return values;}

   public String getKeyFor(String value)
   {
      int index = values.indexOf(value);
      if(index == -1) return null;

      return this.getKeyAt(index);

   }

    public String getKeyForValueIgnoreCase(String value)
   {
      int len = values.size();

      for(int i = 0; i < len; i++)
      {
        String k = (String)values.elementAt(i);

        if(value.equalsIgnoreCase(k)) return getValueAt(i);
      }

     return null;
   }

   public String getValueFor(String key)
   {
      int index = keys.indexOf(key);
      if(index == -1) return null;

      return this.getValueAt(index);
   }

   public String getValueForKeyIgnoreCase(String key)
   {
      int len = keys.size();

      for(int i = 0; i < len; i++)
      {
        String k = (String)keys.elementAt(i);

        if(key.equalsIgnoreCase(k)) return getValueAt(i);
      }

     return null;
   }

   public String getValueAt(int index)
   {
      String test = (String) values.elementAt(index);
      return (String)values.elementAt(index);
   }

   public String getKeyAt(int index)
   {
      return  (String)keys.elementAt(index);
   }

   public boolean containsKey(String value)
   {
      return keys.contains(value);
   }

   public boolean containsValue(String value)
   {
      return values.contains(value);
   }

   public String getSectionName()
   {
     return this.sectionName;
   }

   public int getSectionIndex()
   {
     return this.sectionIndex;
   }

   public int size()
   {
      return keys.size();

   }


   /**
    *  presents a String view of the section as
    * <pre>
    * [sectionname]
    * key=val
    * key=val
    * </pre><br>
    * ie. no spaces in key value presentation
    */
   public String toString()
   {
     String newline = System.getProperty("line.separator") ;
     StringBuffer buffer = new StringBuffer("[" + getSectionName() + "]" + newline);
     Enumeration e = keys.elements();
     String currentKey = null;
     String currentVal = null;
     int count = 0;

     while(e.hasMoreElements())
     {
       currentKey = (String)e.nextElement();

       if(count < values.size())
        currentVal = (String)values.elementAt(count++);

       if(currentVal == null)currentVal = "";

       buffer.append(currentKey + "=" + currentVal);
       buffer.append(newline);

     }
         
     buffer.append(newline);
     return new String(buffer);
   }
}
