// IniData:
//
// Instances of this class are used to read the contents of an ini file (actually a single
// instance may contain the information from multiple ini files - see loadIniFile() for
// details.
//
// Methods are provided to:
//
//   . obtain keyword values (i.e. from a given section)
//   . add/update section keyword/value pairs.
//   . generate a new (or overwrite an existing) ini file based upon the current
//     information contained.

package com.basis100.util.file;

import java.util.*;
import java.io.*;

import com.basis100.util.collections.*;

public class IniData
{
    // iniData:
    //
    // Named data set used to hold ini data. Essentially the set contain one (empty)
    // record with ini sections (the key-value pairs) represented as subordinate named
    // (named after section name) data sets.
    //
    //      "DEFAULT" - one empty record -  sub data set for first section
    //                                   -  sub data set for second section
    //                                   -  sub data set for third section
    //                                   -   ...
    //                                   .
    //                                   .
    //                                   .
    //
    // This organization is preferable to creating one record for each section and
    // attaching the corresponding subordinate data set to them - why? because
    // we can obtain an enumeration of all section data sets simply by retrieving the
    // enumeration of sub data sets of the first record. Weak argument? oh well that's
    // the way its done!
    //
    // The section sub data sets are organized as follows:
    //
    //    . one record to hold key-value pairs: Example:
    //
    //      [SECTEX]            Data Set: Name = SECTEX
    //      key1 = 1
    //      key2 = 2                        Columns(keys):  "key1"   "key2"   "key3"
    //      key3 = three           Record 0 Values:           "1"      "2"    "three"



    private NamedDataSet iniData;

    private boolean isEof;
    private Hashtable valueStorage;

    protected char keywordValueSeparator = '=';

    // Constructors:

    // version allowing specification of keyword value separator (default is '=')
    public IniData(char keywordValueSeparator)
    {
        this();

        this.keywordValueSeparator = keywordValueSeparator;
    }

    public IniData()
    {
        iniData = new NamedDataSet();

        iniData.addRecord(); // our 'root' empty record
    }

    // loadIniFile:
    //
    // Given a ini file name load all sections and contained keyword/value pairs.
    //
    // Notes:
    //
    // While normally called just once for a given ini file this method may be called
    // several times (presumably with a different ini file per invocation). Hence a
    // single instance of this class may hold the information contained in multiple ini
    // files. If two (or more) files contain the same named section:
    //
    //       . all unique keyword/value pairs will be stored
    //       . if a keyword is common the value obtained from the last file accessed
    //         will be stored.
    //
    // Result: true  - file
    //         false - problem encountered - file not found or IO exception encountered
    //


    public boolean loadIniFile(String fileName)
    {
        // open the file
        FileInputStream    fInp    = null;
        DataInputStream    iniFile = null;

        try
        {
            fInp    = new FileInputStream(fileName);
            iniFile = new DataInputStream(fInp);
        }
        catch (Exception e)
        {
            try {iniFile.close(); fInp.close();} catch (Exception ee) {;}

            return false;
        }

        // read all sections and store keyword/value pairs for each

        boolean problemEncountered = false;

        NamedDataSet sectionData = null;

        String line = "";
        String sectionName = "";
        String keyword = "";
        String value = "";

        while(true)
        {
            try
            {
                line = iniFile.readLine();
                if (line == null)
                    break;

                if (line.trim().equals(""))
                    continue;
                if (line.trim().startsWith("/"))
                    continue;
                if (line.trim().startsWith("#"))
                    continue;
                if (line.trim().startsWith("*"))
                    continue;

                // new header section
                if (line.startsWith("[") )
                {
                    sectionName = getSectionName(line);

                    if (sectionName == null)
                    {
                        problemEncountered = true;
                        break;
                    }

                    // obtain/create section data

                    sectionData = iniData.getSubDataSet(0, sectionName);

                    if (sectionData == null)
                    {
                        sectionData = iniData.addSubDataSet(0, sectionName);

                        sectionData.addRecord(); // record to hold section keyword/value pairs
                    }

                    continue;
                }

                // get keyword/value pair for the current section
                keyword = (line.substring(0,line.indexOf(keywordValueSeparator))).trim().toUpperCase();
                value = (line.substring(line.indexOf(keywordValueSeparator)+1)).trim();

                sectionData.setValueAt(value, 0, keyword);
            }
            catch (EOFException e)
            {
                break;
            }
            catch (IOException e)
            {
                problemEncountered = true;
                break;
            }

        }

        // close file
        try {iniFile.close(); fInp.close();} catch (Exception ee) {;}

        if (problemEncountered)
            return false;

        return true;
    }


    // dumpIniFile:
    //
    // Save data to ini file - all sections and keyword value pairs are written.
    //
    //
    // Result: true  - file written successfully
    //         false - problem encountered - file not found or IO exception encountered
    //                 In this case no assumptions can be made (i.e. the file may have
    //                 been partially written, or may not even have been created at all)

    // version that prints values only (i.e. when used as a diagnostic tool - we want
    // print in order that values were set!! - don't ask!!)

    private boolean noKeys = false;

    public boolean dumpIniFile(String fileName, boolean noKeys)
    {
        this.noKeys = noKeys;

        boolean b =  dumpIniFile(fileName);

        this.noKeys = false;

        return b;
    }

    public boolean dumpIniFile(String fileName)
    {
        // open the file

        FileOutputStream  fOut    = null;
        DataOutputStream  iniFile = null;

        try
        {
            fOut   = new FileOutputStream(fileName);
            iniFile = new DataOutputStream(fOut);
        }
        catch (IOException e)
        {
            return false;
        }

        // Get enumeration of all section data sets
        Enumeration sections = iniData.getAllSubs(0);
        if (sections == null)
        {
            // no data!
            try {iniFile.close(); fOut.close();} catch (Exception e) {;}
            return false;
        }

        boolean problemEncountered = false;

        for (; (sections.hasMoreElements()) && (problemEncountered == false);)
        {
            if (writeLine(iniFile, "") == false)
            {
                problemEncountered = true;
                break;
            }

            NamedDataSet sectionData = (NamedDataSet)sections.nextElement();

            if (writeLine(iniFile, "[" + sectionData.getName() + "]") == false)
            {
                problemEncountered = true;
                break;
            }

            if (noKeys)
            {
                // values only in order they were set
                int lim = sectionData.getColumnCount();

                for (int i = 0; i < lim; ++i)
                {
                    String str = (String)sectionData.getValueAt(0, i);

                    if (writeLine(iniFile, str) == false)
                    {
                        problemEncountered = true;
                        break;
                    }
                }
            }
            else
            {
                Enumeration eKeys = sectionData.getKeys();
                for (; eKeys.hasMoreElements();)
                {
                    String key   = (String)eKeys.nextElement();

                    String value = (String)sectionData.getValueAt(0, key);

                    if (value == null)
                        continue;

                    String valueSeparator = " " + keywordValueSeparator + " ";

                    if (writeLine(iniFile, key + valueSeparator + value) == false)
                    {
                        problemEncountered = true;
                        break;
                    }
                }
            }

            if (writeLine(iniFile, "") == false)
            {
                problemEncountered = true;
                break;
            }
        }

        // close file
        try {iniFile.close(); fOut.close();} catch (Exception e) {;}

        return problemEncountered;
    }

    private boolean writeLine(DataOutputStream iniFile, String str)
    {
        try
        {
            iniFile.writeBytes(str + "\r\n");
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    // getSectionName:
    //
    // Given a line (string) that is assumed to hold a valid section name tag (starts with
    // ']') from an ini file, return:
    //
    //       . section name converted to upper case
    //       . null - if the tag is invalid

    protected String getSectionName(String sectionNameLine)
    {
        int endNdx = sectionNameLine.indexOf("]");

        if (endNdx == -1)
            return null;

        String snm = sectionNameLine.substring(1, sectionNameLine.indexOf("]"));

        snm = snm.trim().toUpperCase();

        if (snm.length() == 0)
            return null;

        return snm;
    }

    // getValue:
    //
    // Obtain the value for a keyword in a specified section. If key not found, or if the
    // stored value is null, return the default value provided in the call (which may also
    // be null)

    // getValue:
    //
    // Obtain the value for a keyword in a specified section. If key not found returns null.
    // If stored value is null this is returned.

    public String getValue(String section, String keyword)
    {
        return getValue(section, keyword, null);
    }

    // version that returns a default valuse (passed as parameter) if otherwise null would
    // be returned - see above

    public String getValue(String section, String keyword, String defaultValue)
    {
        NamedDataSet sectionData = iniData.getSubDataSet(0, section.trim().toUpperCase());
        if (sectionData == null)
            return defaultValue;

        String value = (String)sectionData.getValueAt(0, keyword.trim().toUpperCase());

        if (value == null)
            return defaultValue;

        return value;
    }

    // setValue:
    //
    // Set a value in the ini file (data). The section and keyword are specifed.
    //
    // If the section does not exist a new one is created and the keyword and value are
    // stored in the new section.
    //
    // If the section exists the value replaces the existing value for the specified keyword,
    // or if the keyword does not already exist the new keyword and associated value are
    // added to the section.

    public void setValue(String section, String keyword, String value)
       {
        section = section.trim().toUpperCase();
        keyword = keyword.trim().toUpperCase();

        NamedDataSet sectionData = iniData.getSubDataSet(0, section);
        if (sectionData == null)
        {
            // section doesn't exist - create new section to hold key/value
            sectionData = (NamedDataSet)iniData.addSubDataSet(0, section);
            sectionData.addRecord();
        }

        // save/update value (if keyword exists value replaces current value)
        sectionData.setValueAt(value, 0, keyword);
    }
}
