package com.basis100.util.file;
 

import java.util.Properties;
import java.io.*;

 /**
  * The purpose of the properties reader is to replace the java.util.Properties.load()
  * capability with a more configurable reader. In particular one where the separator and 
  * comment and line continuation chars can be set by the programmer.
  * 
  * In addition comments can be added to line ends using the comment character
  * i.e. key=value #this is a comment
  *      anotherKey=another\
  *      Value
  *      
  * 
  * To use the reader pass the constructor a file name as a String or file object and
  * and call properties.get(key , value);
  * or use the default constructor and set the reader properties
  *
  * Alternatively construct a PropertiesReader and set keyValueSeparator and commentChar
  *
  * @version 1.0 1/06/99
  * @author  Brad Hadfield
  */
public class PropertiesReader 
{
   private static char commentChar = '#';
   private static char keyValueSeparator = '=';
   private static String lineContinuation = "\\&";
   public  Properties properties;
   private String file = "";
   private static String fileLocation;
   
   public PropertiesReader()
   {
        this.properties = new Properties();
   }

   public PropertiesReader( String file )throws IOException
   {
     properties = new Properties();
     read(file);   
   }
   
    public PropertiesReader( File file )throws IOException
   {
     properties = new Properties();
     read(file);   
   }
   
   public PropertiesReader( File file, String separator)throws IOException
   {
     setKeyValueSeparator(separator);
     properties = new Properties();
     read(file);   
   }
   
   
    public PropertiesReader( File file, String separator, String comment)throws IOException
   {
     setKeyValueSeparator(separator);
     setCommentChar(comment);
     properties = new Properties();
     read(file);   
   } 
   
   
   public Properties read( String fileName ) throws IOException
   {
     return read(new File(fileName));
    
   }
   
   
   public Properties read(File file )throws IOException
   {
       
       if(this.properties == null)
       {
          this.properties = new Properties();
       }

       if(!file.exists())
       {
          String msg = "Error: No Properties File found: ";
          if(file != null) msg+= file.getAbsolutePath();
          throw new IOException(msg);
       }


       this.fileLocation = file.getCanonicalPath();

       BufferedReader reader = new BufferedReader( new FileReader(file));
       boolean next = true;
       String line = null;
       String holder = "";
       
       while(next)
       {
            line = reader.readLine();
            int lineEnd = 0;

            
            if(line == null)
            {
                next = false;
                break;
            }
            
            line = line.trim();

            if(line.length() == 0 || line.charAt(0) == commentChar)
            {
                next = true;
                continue;
            }
            else if(( lineEnd = line.indexOf(commentChar)) != -1 )
            {
               line = line.substring(0,lineEnd);
            }
            else if(line.endsWith(lineContinuation))
            {
                while( line != null && line.endsWith(lineContinuation))
                {
                    holder += line.substring(0,line.length()-1);
                    line = reader.readLine();
                }
               
                if( line == null)
                {
                  next = false;   
                  line = holder;
                }
                else
                {
                    holder += line;
                    line = holder;
                    holder = "";
                    next = true;
                }
                
            }
           
           String key = parseKeyFromLine(line);
           if(key.equals(""))
             System.out.print("@");
           String value = parseValueFromLine(line);
            
           if(key != null && value != null)
           {           
              this.properties.put(key,value);
           }
           
       }
      return this.properties;
   }

   

   public void setCommentChar( String characterString )
   {
     this.commentChar = characterString.charAt(0);
   }
  
   
   public void setCommentChar( char character )
   {
     this.commentChar = character;
   }
   
   public void setKeyValueSeparator( String characterString )
   {
     this.keyValueSeparator = characterString.charAt(0);
   }
   
   public void setKeyValueSeparator(char character)
   {
     this.keyValueSeparator = character;
   }
   
   public void setLineContuationChar( String characterString )
   {
      this.lineContinuation = characterString.trim();
    
   }
   
    public void setLineContuationChar( char character )
   {
      this.lineContinuation = String.valueOf(character);
   }
   
   public String parseKeyFromLine( String line )
   {
    try
     {
        int index = line.indexOf(keyValueSeparator);

        if(index == -1) index = line.length();

         if(index > 0)
         {
            line = line.substring(0,index);
            return line.trim();
         }
         else
         {
            return null;
         }
      }
      catch(Exception e)
      {
         return null;
      }
   }
   
   
   private String parseValueFromLine( String line )
   {
      try
      {
        int index = line.indexOf(keyValueSeparator);
        
          if(index > 0)
          {
             line = line.substring(index + 1,line.length());
             return line.trim();
          }
          else
          {
            return "";
          }
      }
      catch(Exception e)
      {
         return "";
      }
     
   }

   public String getProperty(String name)
   {
     return this.properties.getProperty(name);
   }

   public String getProperty(String name, String defaultValue)
   {
     return this.properties.getProperty(name,defaultValue);
   }

   public static String getProperty(File file,String name) throws IOException
   {
       PropertiesReader reader = new PropertiesReader(file);
       return reader.properties.getProperty(name);
   }

   public static String getProperty(File file,String name,String sep) throws IOException
   {
       PropertiesReader reader = new PropertiesReader(file,sep);
       return reader.properties.getProperty(name);
   }

   public static Properties getProperties(File name)throws IOException
   {
     PropertiesReader reader = new PropertiesReader(name);
     return reader.properties;
   }

   public static  String getFileLocation()
   {
      return fileLocation;
   }
}
