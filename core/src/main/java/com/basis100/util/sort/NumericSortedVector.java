package com.basis100.util.sort;

import java.util.Vector;

import com.basis100.util.interfaces.*;

// NumericSortedVector:
//
// Class for creating a sortable vector.
//
// Usage: Objects added to the vector must implement the InumericSortable interface;
//        this interface requires the object to implement the sortvalue() method that
//        returns the objects sort value (a long). After objects are added sort() may
//        be called to order the elements in asscending order according to their sort
//        values - elements ARE NOT sorted automatically when added to the vector.
//
//        Example:
//
//        class SortableInteger extends Integer implements InumericSortable
//            {
//            public SortableInteger(int value) { super(value); }
//
//            // interface method
//            public long sortvalue() { return longValue(); }
//            }
//
//        ... usage fragment
//
//        NumericSortedVector nsv = new NumericSortedVector();
//
//        SortableInteger one   = new SortableInteger(1);
//        SortableInteger two   = new SortableInteger(2);
//        SortableInteger three = new SortableInteger(3);
//
//        nsv.addElement(three);
//        nsv.addElement(two);
//        nsv.addElement(one);
//
//        // elements in vector are now: three, two, one
//
//        nsv.sort();
//
//        // elements in vector are now: one, two, three

public class NumericSortedVector extends Vector
{
	public void sort()
	{
		// Bubble sort elements by sort value ascending

		int num = size();
		InumericSortable current;
		InumericSortable compare;

		int lastSwap = num;
		int sortBound = num;

		while ( lastSwap != 0 )
		{
			lastSwap = 0;
			for (int j = 0; j <  sortBound-1; j++)
			{
				current = (InumericSortable) elementData[j];
				compare = (InumericSortable) elementData[j+1];

				if (current.sortvalue()  > compare.sortvalue())
				{
					elementData[j] = compare;
					elementData[j+1] = current;

					lastSwap  = j+1;
				}
			}
			sortBound = lastSwap;
		}
	}
}
