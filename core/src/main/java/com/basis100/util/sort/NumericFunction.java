/* NM - July 16
*  A class to define general numeric funcitions
*
*
*
******************************************************************/

package com.basis100.util.sort;

import com.basis100.util.interfaces.*;


public class NumericFunction extends java.lang.Object
{

        // Number sorting methods

    // sortNumberOrderIndicies:
    //
    // Given an array of numbers return an array of indicies that represent how the elements
    // of the array must be re-arranged to be placed in ascending order.

    // ... version that takes as input an array of strings where each string holds the
    // character representation of a number

    public static int[] sortNumberOrderIndicies(String[]strNums)
    {
        int lim = strNums.length;
        long []lnums = new long[strNums.length];

        for (int i = 0; i < lim; ++i)
            lnums[i] = NumberRep(strNums[i]);

        return sortNumberOrderIndicies(lnums);
    }

    public static int[] sortNumberOrderIndicies(long[]numbers)
    {
        int numbersLen = numbers.length;

        // handle special case - dates has 0 or 1 elements
        if (numbersLen <= 1)
            return new int[numbersLen];

        // create a numerical sortable vector and add sortable objects that corresponds to the
        // array in our array = the objects in the sortable array correspond one-to-one with our
        // doubles

        NumericSortedVector nsv = new NumericSortedVector();

        for (int i = 0; i < numbersLen; ++i)
            nsv.addElement(new NumberAndNdx(numbers[i], i));

        // sort it

        nsv.sort();

        // create results array

        int []ndxs = new int[numbersLen];

        // get sort cross indicies
        for (int i = 0; i < numbersLen; ++i)
            ndxs[i] = ((NumberAndNdx)nsv.elementAt(i)).assoc;

        return ndxs;
    }


    private static long NumberRep(String str)
    {
        try
        {
            long l = Double.valueOf(str).longValue();
            return l;
        }
        catch (Exception e) {;}

        return 0;
    }


}


class NumberAndNdx implements InumericSortable
    {
          public long numberRep;                // Number representation of string
          public int  assoc;                    // associated data

          public NumberAndNdx(long numberRep, int assoc)
          {
              this.numberRep = numberRep;
              this.assoc  = assoc;
          }

          public long sortvalue()
          {
              return numberRep;
          }
    }
