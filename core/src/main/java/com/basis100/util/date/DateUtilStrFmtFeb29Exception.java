// DateUtilStrFmtFeb29Exception: Subclass of DateUtilStrFmtException

package com.basis100.util.date;

public class DateUtilStrFmtFeb29Exception extends DateUtilStrFmtException
{
    public DateUtilStrFmtFeb29Exception()
    {
        super(BAD_FEB29);
    }

    public DateUtilStrFmtFeb29Exception(String message)
    {
        super(message, BAD_FEB29);
    }
}


