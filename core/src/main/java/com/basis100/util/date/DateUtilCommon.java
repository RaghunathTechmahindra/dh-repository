package com.basis100.util.date;

import com.basis100.picklist.BXResources;

public class DateUtilCommon
{
    public final static String[] months = {"Jan","Feb","Mar","Apr","May","Jun",
                                            "Jul","Aug","Sep","Oct","Nov","Dec"};

    public final static String[] uMonths = {"JAN","FEB","MAR","APR","MAY","JUN",
                                            "JUL","AUG","SEP","OCT","NOV","DEC"};

    public final static String[] days = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

	  public static int monthDays[] = {31,28,31,30,31,30,31,31,30,31,30,31};

    public static final long   ONE_DAY = 86400000;
    public static final double ONE_DAY_AS_DOUBLE = 86400000;

    public static String[] multilingMonths = new String[12];




}