// DateUtilStrFmtYrException: Subclass of DateUtilStrFmtException

package com.basis100.util.date;

public class DateUtilStrFmtYrException extends DateUtilStrFmtException
{
    public DateUtilStrFmtYrException()
    {
        super(BAD_YR_PART);
    }

    public DateUtilStrFmtYrException(String message)
    {
        super(message, BAD_YR_PART);
    }
}



