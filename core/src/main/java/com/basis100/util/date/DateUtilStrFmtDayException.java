// DateUtilStrFmtDayException: Subclass of DateUtilStrFmtException

package com.basis100.util.date;

public class DateUtilStrFmtDayException extends DateUtilStrFmtException
{
    public DateUtilStrFmtDayException()
    {
        super(BAD_DAY_PART);
    }

    public DateUtilStrFmtDayException(String message)
    {
        super(message, BAD_DAY_PART);
    }
}

