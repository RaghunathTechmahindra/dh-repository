// DateUtilStrFmtMthException: Subclass of DateUtilStrFmtException

package com.basis100.util.date;

public class DateUtilStrFmtMthException extends DateUtilStrFmtException
{
    public DateUtilStrFmtMthException()
    {
        super(BAD_MTH_PART);
    }

    public DateUtilStrFmtMthException(String message)
    {
        super(message, BAD_MTH_PART);
    }
}



