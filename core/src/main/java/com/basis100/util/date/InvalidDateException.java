package com.basis100.util.date;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class InvalidDateException extends Exception
{

  public InvalidDateException()
  {
    super();
  }

  public InvalidDateException(String pMsg)
  {
    super(pMsg);
  }

}