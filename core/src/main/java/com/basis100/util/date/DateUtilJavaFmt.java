// class DateUtilJavaFmt
//
// Date utilities class. Services provided operate upon dates represented as
// java.util.date objects.
//
// Methods:
//
//    public int dateDiff(Date d1, Date d2)
//
//    public Date dateAdd(Date d1, int numOfDay)
//
//    public Date dateSubtract(Date d1, int numOfDay)
//

package com.basis100.util.date;

import java.util.*;

public class DateUtilJavaFmt extends DateUtilCommon


{

//            int y = dateDiff(new java.util.Date(1998, , 6, 0, 0, 0),
//                             new java.util.Date(1998, 3, 30, 0, 0, 0));

    public int dateDiff(Date d1, Date d2)
    {
        long diff = 0;
        double timeDiff = 0;

        //double time1 = d1.getTime();

        timeDiff = d1.getTime() - d2.getTime();
        timeDiff = Math.abs(timeDiff);

        double flooredDiff = Math.floor(timeDiff / ONE_DAY_AS_DOUBLE);
        double modDiff = (timeDiff / ONE_DAY_AS_DOUBLE) - flooredDiff;

        diff = (long)flooredDiff;
        if (modDiff > 0.50)
        {
            diff = diff + 1; // Fix rounding problem varying from CPU to CPU
        }

        int idiff = (int)diff;

        // validate - fix if wrong
        while (true)
        {
            Date d = dateAdd(d2, idiff);

            if (d.getYear() < d1.getYear())
            {
                idiff++;
                continue;
            }

            if (d.getMonth() < d1.getMonth())
            {
                idiff++;
                continue;
            }

            if (d.getDate() < d1.getDate())
            {
                idiff++;
                continue;
            }

            break;
        }

        return idiff;
    }

    /**
     *  Add certain number of days onto the date provided
     */
    public Date dateAdd(Date d1, int numOfDay)
    {
        Date dt = new Date();

        dt.setDate(d1.getDate() + numOfDay);
        dt.setMonth(d1.getMonth());
        dt.setYear(d1.getYear());

        dt = new Date(dt.getTime());

        return dt;
    }

    /**
     *  Subtract certain number of days from the date provided
     */

    public Date dateSubtract(Date d1, int numOfDay)
    {
        Date dt = new Date();

        dt.setDate(d1.getDate() - numOfDay);
        dt.setMonth(d1.getMonth());
        dt.setYear(d1.getYear());

        dt = new Date(dt.getTime());

        return dt;
    }

    public String dateExtractDay(Date d1)
    {
        return days[d1.getDay()];
    }

    public String dateExtractMonth(Date d1)
    {
        return months[d1.getMonth()];
    }


}