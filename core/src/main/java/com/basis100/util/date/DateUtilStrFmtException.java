// DateUtilStrFmtException:
//
// Exception thrown by date validation functions in class NdateFunction.
//
// Also see sub-classes: DateUtilStrFmtDayException, DateUtilStrFmtMthException,
//                       DateUtilStrFmtYrException,  DateUtilStrFmtFeb29Exception

package com.basis100.util.date;

public class DateUtilStrFmtException extends Exception
{
    // problem codes

    public static final int BAD_DAY_PART = 0;
    public static final int BAD_MTH_PART = 1;
    public static final int BAD_YR_PART  = 2;
    public static final int BAD_FEB29    = 3;

    public int problem;

    // array index: if exception thrown while validating an array of dates
    // this variable holds the index of the element that was bad

    public int arrayNdx = -1;

    // Constructors:

    public DateUtilStrFmtException()
    {
        super();
    }

    public DateUtilStrFmtException(String message)
    {
        super(message);
    }
    public DateUtilStrFmtException(int problem)
    {
        super();
        this.problem = problem;
    }
    public DateUtilStrFmtException(String message, int problem)
    {
        super(message);
        this.problem = problem;
    }
}

