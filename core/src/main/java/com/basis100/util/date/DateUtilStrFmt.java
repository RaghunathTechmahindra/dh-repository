// class DateUtilStrFmt
//
// Date utilities class. Services provided operate upon dates represented as
// strings (java.lang.String).
//
// Methods:
//
//    public static String dateDD_Mon_YYtoDD_Mon_YYYY(String dtStr)
//
//    public String dateYYYY_MM_DDtoDD_MMM_YYYY(String dtStr)
//
//    public String dateYYYY_MM_DDtoDD_MMM_YY(String dtStr)
//
//    public String dateDD_MMM_YYYYtoYYYY_MM_DD(String dtStr)
//
//    public String dateDD_MMM_YYtoYYYY_MM_DD(String dtStr)
//
//    public static String dateDD_Mon_YYYYtoDD_Mon_YY(String dtStr)
//
//    public String dateDD_MMM_YY_HH_MM_SStoYYYY_MM_DD_HH_mm_SS(String dtStr)
//
//    public String dateDD_MMM_YY_HH_MM_SStoMMM_DD_YYYY_HH_mm_SS(String dtStr)
//
//    public boolean valDate(String dtStr) throws DateUtilStrFmtException
//
//    public boolean compareDates(String a, String b)
//
//    public boolean dateBetween(String da, String db, String dc)
//
//    public Date getDateObject(String dtstr)
//
//	  public Timestamp parseStringToTimeStamp(String timeStampString, String format)
//
//    public String TimestampToString(Timestamp ts, String format)
//
//    public int[] sortDatesOrderIndicies(String[]dates)
//
//    public String[] sortDates(String[]dates)
//

package com.basis100.util.date;

import java.util.*;
import java.sql.*;
import java.text.*;

import com.basis100.util.sort.*;
import com.basis100.util.interfaces.*;

public class DateUtilStrFmt extends DateUtilCommon
{
    // supported string formats:
    //
    //      Notes: variable names match formats where "_" represents a
    //             separator character. Examples:
    //
    //             "10 Dec 97"             --> format DD_mmm_YY
    //             "1997-01-24 10:20:45.6" --> format YYYY_MM_DD_HH_MM_SS
    //
    //                   N.B.: tenths (or thousandth) of seconds ignored in
    //                         format YYYY_MM_DD_HH_MM_SS

    public static final int DD_mmm_YY           = 0; // Navigater standard display format
    public static final int YYYY_MM_DD_HH_MM_SS = 1; // generic database date format
    public static final int DD_mmm_YY_HH_MM_SS  = 3; // alternate ull timestamp
    public static final int DD_mmm_YYYY         = 4; //Navigater standard display format from 15 Jan 1990
    //  .

    //  .
    //  .
    //  .

    // selected format (see constructors)

    private int dateFormat = DD_mmm_YY;

    // exceptions thrown in date validation methods

    private boolean throwValExceptions = false;



    // --------------------------------------------------------------------------------------

    // Constructor: default format, exceptions not thrown in validation methods

    public DateUtilStrFmt()
    {
    }

    // Constructor: specify date format, exceptions not thrown in validation methods

    public DateUtilStrFmt(int dateFormat)
    {
        this.dateFormat = dateFormat;
    }

    // Constructor: specify date format, specify exceptions thrown by validation methods

    public DateUtilStrFmt(int dateFormat, boolean throwValExceptions)
    {
        this.dateFormat         = dateFormat;
        this.throwValExceptions = throwValExceptions;
    }

    public static String dateDD_Mon_YYtoDD_Mon_YYYY(String dtStr)
    {
        String newDate = new String(dtStr);

        // Return if already in DD Mon YYYY format
        if (dtStr.length() > 9)
            return newDate;

        try {
            int YYdateYear = Integer.parseInt(dtStr.substring(dtStr.length()-2,dtStr.length()));
            if (YYdateYear >= 50)
            {   // Treat that as 19XX
                newDate = dtStr.substring(0,dtStr.length()-2) + String.valueOf(1900 + YYdateYear);
            } else {
                // Treat that as 20XX
                newDate = dtStr.substring(0,dtStr.length()-2) + String.valueOf(2000 + YYdateYear);
            }
        } catch (Exception err) {
            // Bad thing happened
        }
        return newDate;
    }

    public String dateYYYY_MM_DDtoDD_MMM_YYYY(String dtStr)
    {
        String pad = "00000";

        int savedDateFormat = dateFormat;
        dateFormat = YYYY_MM_DD_HH_MM_SS;

        int YMDHMS[] = getYMDHMS(dtStr);

        dateFormat = savedDateFormat;

        int len;

        String yr = String.valueOf(YMDHMS[0]);
        len = yr.length();

        if (len < 2)
            yr = pad.substring(0, 2-len) + yr;

        String day = String.valueOf(YMDHMS[2]);
        len = day.length();

        if (len < 2)
            day = pad.substring(0, 2-len) + day;

        return day + " " + ((YMDHMS[1] < 1 || YMDHMS[1] > 12) ? "???" : months[YMDHMS[1]-1]) + " " + yr;

    }

    public String dateYYYY_MM_DDtoDD_MMM_YY(String dtStr)
    {
        String pad = "00000";

        int savedDateFormat = dateFormat;
        dateFormat = YYYY_MM_DD_HH_MM_SS;

        int YMDHMS[] = getYMDHMS(dtStr);

        dateFormat = savedDateFormat;

        // drop century

    	if (YMDHMS[0] >= 2000)
    	    YMDHMS[0] -= 2000;

    	if (YMDHMS[0] >= 1900)
    	    YMDHMS[0] -= 1900;


        int len;

        String yr = String.valueOf(YMDHMS[0]);
        len = yr.length();

        if (len < 2)
            yr = pad.substring(0, 2-len) + yr;

        String day = String.valueOf(YMDHMS[2]);
        len = day.length();

        if (len < 2)
            day = pad.substring(0, 2-len) + day;

        return day + " " + ((YMDHMS[1] < 1 || YMDHMS[1] > 12) ? "???" : months[YMDHMS[1]-1]) + " " + yr;
    }

    public String dateDD_MMM_YYYYtoYYYY_MM_DD(String dtStr)
    {
        String pad = "00000";
        int len;

        int savedDateFormat = dateFormat;
        dateFormat = DD_mmm_YYYY;

        int YMDHMS[] = getYMDHMS(dtStr);

        dateFormat = savedDateFormat;

        // determine century :: years 0-50 assumed to be in 21th century,
        //                      51-99 in 20th


        String yr = dtStr.substring(7);
        len = yr.length();


        String mth = String.valueOf(YMDHMS[1]);
        len = mth.length();

        if (len < 2)
            mth = pad.substring(0, 2-len) + mth;

        String day = String.valueOf(YMDHMS[2]);
        len = day.length();

        if (len < 2)
            day = pad.substring(0, 2-len) + day;

        return yr + "-" + mth + "-" + day;
    }

    public String dateDD_MMM_YYtoYYYY_MM_DD(String dtStr)
    {
        String pad = "00000";

        int savedDateFormat = dateFormat;
        dateFormat = DD_mmm_YY;

        int YMDHMS[] = getYMDHMS(dtStr);

        dateFormat = savedDateFormat;

        // determine century :: years 0-50 assumed to be in 21th century,
        //                      51-99 in 20th

    	if (YMDHMS[0] >= 0 && YMDHMS[0] <= 50)
    	    YMDHMS[0] += 2000;
        else if (YMDHMS[0] >= 51 && YMDHMS[0] <= 99)
            YMDHMS[0] += 1900;

        int len;

        String yr = String.valueOf(YMDHMS[0]);
        len = yr.length();

        if (len < 4)
            yr = pad.substring(0, 4-len) + yr;

        String mth = String.valueOf(YMDHMS[1]);
        len = mth.length();

        if (len < 2)
            mth = pad.substring(0, 2-len) + mth;

        String day = String.valueOf(YMDHMS[2]);
        len = day.length();

        if (len < 2)
            day = pad.substring(0, 2-len) + day;

        return yr + "-" + mth + "-" + day;
    }

    public static String dateDD_Mon_YYYYtoDD_Mon_YY(String dtStr)
    {
        String newDate = new String(dtStr);

        // Return if already in DD Mon YY format
        if (dtStr.length() <= 9)
            return newDate;

        try {
            newDate = dtStr.substring(0,dtStr.length()-4) + dtStr.substring(dtStr.length()-2,dtStr.length());
        } catch (Exception err) {
            // Bad thing happened
        }
        return newDate;
    }


    public String dateDD_MMM_YY_HH_MM_SStoYYYY_MM_DD_HH_mm_SS(String dtStr)
    {
        String pad = "00000";

        int savedDateFormat = dateFormat;
        dateFormat = DD_mmm_YY_HH_MM_SS;

        int YMDHMS[] = getYMDHMS(dtStr);

        dateFormat = savedDateFormat;

        // determine century :: years 0-89 assumed to be in 21th century,
        //                      90-99 in 20th

    	if (YMDHMS[0] >= 0 && YMDHMS[0] <= 89)
    	    YMDHMS[0] += 2000;
        else if (YMDHMS[0] >= 90 && YMDHMS[0] <= 99)
            YMDHMS[0] += 1900;

        int len;

        String yr = String.valueOf(YMDHMS[0]);
        len = yr.length();

        if (len < 4)
            yr = pad.substring(0, 4-len) + yr;

        String mth = String.valueOf(YMDHMS[1]);
        len = mth.length();

        if (len < 2)
            mth = pad.substring(0, 2-len) + mth;

        String day = String.valueOf(YMDHMS[2]);
        len = day.length();

        if (len < 2)
            day = pad.substring(0, 2-len) + day;

        String hour =  String.valueOf(YMDHMS[3]);
        len = hour.length();

        if (len < 2)
            hour = pad.substring(0, 2-len) + hour;

        String min =  String.valueOf(YMDHMS[4]);
        len = min.length();

        if (len < 2)
            min = pad.substring(0, 2-len) + min;

        String sec =  String.valueOf(YMDHMS[5]);
        len = sec.length();

        if (len < 2)
            sec = pad.substring(0, 2-len) + sec;


        return yr + " " + mth + " " + day + " " + hour + ":" + min + ":" + sec;
    }

    public String dateDD_MMM_YY_HH_MM_SStoMMM_DD_YYYY_HH_mm_SS(String dtStr)
    {
        String pad = "00000";

        int savedDateFormat = dateFormat;
        dateFormat = DD_mmm_YY_HH_MM_SS;

        int YMDHMS[] = getYMDHMS(dtStr);

        dateFormat = savedDateFormat;

        // determine century :: years 0-89 assumed to be in 21th century,
        //                      90-99 in 20th

    	if (YMDHMS[0] >= 0 && YMDHMS[0] <= 89)
    	    YMDHMS[0] += 2000;
        else if (YMDHMS[0] >= 90 && YMDHMS[0] <= 99)
            YMDHMS[0] += 1900;

        int len;

        String yr = String.valueOf(YMDHMS[0]);
        len = yr.length();

        if (len < 4)
            yr = pad.substring(0, 4-len) + yr;

        String mth = (YMDHMS[1] < 1 || YMDHMS[1] > 12) ? "???" : months[YMDHMS[1]-1];

        String day = String.valueOf(YMDHMS[2]);
        len = day.length();

        if (len < 2)
            day = pad.substring(0, 2-len) + day;

        String hour =  String.valueOf(YMDHMS[3]);
        len = hour.length();

        if (len < 2)
            hour = pad.substring(0, 2-len) + hour;

        String min =  String.valueOf(YMDHMS[4]);
        len = min.length();

        if (len < 2)
            min = pad.substring(0, 2-len) + min;

        String sec =  String.valueOf(YMDHMS[5]);
        len = sec.length();

        if (len < 2)
            sec = pad.substring(0, 2-len) + sec;

        return mth + " " + day + " " + yr + " " + hour + ":" + min + ":" + sec;
    }

    public boolean valDate(String dtStr) throws DateUtilStrFmtException
    {
        int [] YMDHMS = getYMDHMS(dtStr);

        if (dateFormat == YYYY_MM_DD_HH_MM_SS || dateFormat == DD_mmm_YYYY)
        {
            // formats with century included in date
    		if (YMDHMS[0] < 0 || YMDHMS[0] > 2099)
		    {
                if (throwValExceptions)
                    throw new DateUtilStrFmtYrException();
                else
                    return false;
            }
        }
        else
        {
            // formats without century included in date
    		if (YMDHMS[0] < 0 || YMDHMS[0] > 99)
    		{
                if (throwValExceptions)
                    throw new DateUtilStrFmtYrException();
                else
                    return false;
            }
        }

		if (YMDHMS[1] < 0 || YMDHMS[1] > 12)
	    {
            if (throwValExceptions)
                throw new DateUtilStrFmtMthException();
            else
                return false;
        }

        if ((YMDHMS[0] % 4) == 0 && YMDHMS[1] == 2)    // Leap Year, Feb has 29 days
        {
            if (YMDHMS[2] < 0 || YMDHMS[2] > 29)   // Feb cannot more than 29 days
            {
                if (throwValExceptions)
                    throw new DateUtilStrFmtFeb29Exception();
                else
                    return false;
            }
        }
        else
        {
    		if (YMDHMS[2] < 0 || YMDHMS[2] > monthDays[YMDHMS[1]-1])
		    {
                if (throwValExceptions)
                    throw new DateUtilStrFmtDayException();
                else
                    return false;
            }
        }

	    return true;
    }

    // compareDates: true if date A > date B
    //
    //  Notes:  dates assumed to be valid (for object's current date format)


    public boolean compareDates(String a, String b)
    {
        return (numRep(a) > numRep(b));
    }

    // dateBetween:
    //
    // Returns true if da <= db <= dc (i.e. date b falls between dates a and b inclusive).
    //
    //  Notes: . each date assumed to be valid (for object's current date format)
    //         . da is assumed to be <= dc

    public boolean dateBetween(String da, String db, String dc)
    {
        long ndb = numRep(db);

        return (numRep(da) <= ndb && ndb <= numRep(dc));
    }

    // getDateObject
    // Given a date in string representation (object's cureent date format) returns
    //  a standard date object.
    public java.util.Date getDateObject(String dtstr)
    {
        int[] ymdhms = getYMDHMS(dtstr);

        if ( ymdhms[0] > 1900 )
           ymdhms[0] -= 1900;

        return new java.util.Date(ymdhms[0], ymdhms[1], ymdhms[2]);
    }

    /**
	Parse a string into a TimeStamp
	A valid format is:
	"dd MMM yyyy"
	*/

	public Timestamp parseStringToTimeStamp(String timeStampString, String format)
	{
    	Timestamp rv;
        java.util.Date tempDate = null;
        DateFormat df = new SimpleDateFormat(format);
        ParsePosition pos = new ParsePosition(0);
        tempDate = df.parse(timeStampString, pos);
        rv = new java.sql.Timestamp(tempDate.getTime());
        return rv;
    }

    /**
    Return a Timestamp as a string.
    A vaild format is:
    "dd MMM. yyyy"
    */

    public String TimestampToString(Timestamp ts, String format)
    {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(ts);
    }


    // Date sorting methods

    // sortDatesOrderIndicies:
    //
    // Given an array of dates return an array of indicies that represent how the elements of the
    // dates array must be re-arranged to be placed in ascending order.
    //
    // Example: dates[]  == 10 Feb 98, 09 Feb 98, 08 Feb 98
    //
    //          result[] ==     2          1          0
    //
    //          so if a new dates array were created and populated with elements of the original using
    //          the result as a pick order the new array would hold the original dates in ascending
    //          order.
    //
    //
    //  Notes:  each date string (in array) assumed to be valid (for object's current date format)

    public int[] sortDatesOrderIndicies(String[]dates)
    {
        int datesLen = dates.length;

        // handle special case - dates has 0 or 1 elements
        if (datesLen <= 1)
            return new int[datesLen];

        // create a numerical sortable vector and add sortable objects that corresponds to the
        // dates in our array = the objects in the sortable array correspond one-to-one with our
        // dates - each entry holds corresponding date's numeric representation and index in
        // <dates[]>.

        NumericSortedVector nsv = new NumericSortedVector();

        for (int i = 0; i < datesLen; ++i)
            nsv.addElement(new NumDateAndNdx(numRep(dates[i]), i));

        // sort it

        nsv.sort();

        // create results array

        int [] ndxs = new int[datesLen];

        for (int i = 0; i < datesLen; ++i)
            ndxs[i] = ((NumDateAndNdx)nsv.elementAt(i)).assoc;

        return ndxs;
    }


    // sortDates: Given an array of dates return an array of same in sorted (ascending) order
    //
    //  Notes:  each date string (in array) assumed to be valid (for object's current date format)

/*

    public String[] sortDates(String[]dates)
    {
        int datesLen = dates.length;

        // handle special case - dates has 0 or 1 elements
        if (datesLen <= 1)
            {
                String [] r = new String[datesLen];
                if (datesLen == 0)
                    return r;

                r[0] = new String(dates[0]);
                return r;
            }

        // create a numerical sortable vector and add sortable objects that corresponds to the
        // dates in our array = the objects in the sortable array correspond one-to-one with our
        // dates - each entry holds corresponding date's numeric representation and index in
        // <dates[]>.

        NumericSortedVector nsv = new NumericSortedVector();

        for (int i = 0; i < datesLen; ++i)
            nsv.addElement(new NumDateAndNdx(numRep(dates[i]), i));

        // sort it

        nsv.sort();

        // create results array and move dates into it in the sorted order: objecs in nsv
        // are now sorted - since each object also holds it's original (corresponding) index
        // in <dates> nsv basically now holds a sorted cross-index list

        String [] sd = new String[datesLen];

        for (int i = 0; i < datesLen; ++i)
            sd[i] = new String(dates[((NumDateAndNdx)nsv.elementAt(i)).assoc]);

        return sd;
    }
*/

    //
    // Implementation:
    //

    // numRep - return numerical representation of a date
    //
    //   Notes:  date string assumed to be valid (for object's current date format)
    //           years 0-89 assumed to be in 21th century, 90-99 in 20th

    private long numRep(String dtStr)
    {
        int YMDHMS[] = getYMDHMS(dtStr);

        return numRep(YMDHMS);
    }

        // ... version given date already broken into MM, DD, YY
    private long numRep(int YMDHMS[])
    {
    	if (YMDHMS[0] >= 0 && YMDHMS[0] <= 89)
    	    YMDHMS[0] += 2000;
        else if (YMDHMS[0] >= 90 && YMDHMS[0] <= 99)
            YMDHMS[0] += 1900;

		return
		(YMDHMS[0] *  1000000000000L +     // year  * 100E6
		 YMDHMS[1] *      100000000L +     // month * 100E4
		 YMDHMS[2] *        1000000L +     // day   * 100E3
		 YMDHMS[3] *          10000L +     // hour  * 100E2
		 YMDHMS[4] *            100L +     // min   * 100E1
		 YMDHMS[5]);                       // sec   * 100E0

    }



    // getYMDHMS:
    //
    // Return an integer array holding the day, month, year, hour, minute and second extracted
    // from a date string. Date format is as specified in the object and the date string is expected to conform to
    // this format.
    //
    // Notes:
    //
    // If the date format does not support time coding (or if time coding is missing
    // from a representation - i.e. "1998-01-15" for format YYYY_MM_DD_HH_MM_SS)
    // the time elements are set to 0.
    //
    // If a problem is encountered parsing the string the resulting array will hold
    // a -1/-1's for the part/parts that could not be extracted as expected.
    // (i.e. "1o Sep 98" will result in -1 9 98 0 0 0 when the expected format
    // is DD_mmm_YY).

	private int[] getYMDHMS(String dtStr)
	{
        int YMDHMS[];

        switch (dateFormat)
            {
            case DD_mmm_YY:
            case DD_mmm_YYYY :
                YMDHMS = getYMDHMSFromDD_mmm_YY(dtStr);
                break;

            case YYYY_MM_DD_HH_MM_SS:
                YMDHMS = getYMDHMSFromYYYY_MM_DD_HH_MM_SS(dtStr);
                break;

            case DD_mmm_YY_HH_MM_SS:
                YMDHMS = getYMDHMSFromDD_MMM_YY_HH_MM_SS(dtStr);
                break;

            default:
                YMDHMS = new int[6];
                for (int i = 0; i < 6; ++ i)
                    YMDHMS[i] = -1;         // i.e. all 0's for unknown date format
            }

        return YMDHMS;
    }

    // getYMDHMSFromDD_mmm_YY
    //
    // Note: fuction also allows for four digit dates
    private int [] getYMDHMSFromDD_mmm_YY(String dtStr)
    {
        int YMDHMS[] = new int[6];

        dtStr = dtStr.trim();

        int firstSpace = dtStr.indexOf(' ');
        int lastSpace  = dtStr.lastIndexOf(' ');

        try
            {

            String yr = dtStr.substring(lastSpace+1).trim();
    	    YMDHMS[0] = Integer.parseInt(yr.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[0] = -1;
            }


        try {
            String mth = dtStr.substring(firstSpace+1, lastSpace).trim().toUpperCase();

            for (int i = 0; i < 12; ++i)
                {
                    if (mth.equals(uMonths[i]))
                        {
                        YMDHMS[1] = i+1;
                        break;
                        }
                }

            if (YMDHMS[1] == 0)
                YMDHMS[1] = -1;
            }
        catch (StringIndexOutOfBoundsException e)
            {
    	    YMDHMS[1] = -1;
            }

    	try	{
            String day = dtStr.substring(0, firstSpace).trim();

    	    YMDHMS[2] = Integer.parseInt(day.trim());
    	    }
    	catch(NumberFormatException e)
    	    {
    	    YMDHMS[2] = -1;
    	    }

        return YMDHMS;
    }

    private int [] getYMDHMSFromDD_MMM_YY_HH_MM_SS(String dtStr)
    {
        int YMDHMS[] = new int[6];

        dtStr = dtStr.trim();

        // ensure day represented by two digits
        if (dtStr.indexOf(' ') == 1)
            dtStr = "0" + dtStr;

        // ensure year represented by two digits
        if (dtStr.substring(7).indexOf(' ') == 1)
        {
            dtStr = dtStr.substring(0, 7) + "0" + dtStr.substring(7);
        }



        // allow input to be DD$MMM$YY or DD$MMM$YYYY

        if (dtStr.length() == (new String("DD$MMM$YY")).length() ||
            dtStr.length() == (new String("DD$MMM$YYYY")).length())
            dtStr += " 00:00:00";

        int yrLen = dtStr.substring(7).indexOf(' ');

        dtStr += " "; // don't remove!

        try
            {
            String yr = dtStr.substring(7, 7+yrLen).trim();
    	    YMDHMS[0] = Integer.parseInt(yr.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[0] = -1;
            }

        try {
            String mth = dtStr.substring(3, 6).trim();
            for (int i = 0; i < 12; ++i)
                {
                    if (mth.equals(uMonths[i]))
                        {
                        YMDHMS[1] = i+1;
                        break;
                        }
                }

            if (YMDHMS[1] == 0)
                YMDHMS[1] = -1;
            }
        catch (NumberFormatException e)
            {
    	    YMDHMS[1] = -1;
            }

    	try	{
            String day = dtStr.substring(0, 2).trim();

    	    YMDHMS[2] = Integer.parseInt(day.trim());
    	    }
    	catch(NumberFormatException e)
    	    {
    	    YMDHMS[2] = -1;
    	    }

    	int yrOff = (yrLen == 4) ? 2 : 0;
        try
            {
            String hr = dtStr.substring(10+yrOff, 12+yrOff).trim();
    	    YMDHMS[3] = Integer.parseInt(hr.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[3] = 0;
            }

        try
            {
            String min = dtStr.substring(13+yrOff, 15+yrOff).trim();
    	    YMDHMS[4] = Integer.parseInt(min.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[4] = 0;
            }

        try
            {
            String sec = dtStr.substring(16+yrOff, 18+yrOff).trim();
    	    YMDHMS[5] = Integer.parseInt(sec.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[5] = 0;
            }

        return YMDHMS;

    }
    private int [] getYMDHMSFromYYYY_MM_DD_HH_MM_SS(String dtStr)
    {
        int YMDHMS[] = new int[6];

        // allow input to be YYYY$MM$DD

        if (dtStr.length() == (new String("YYYY$MM$DD")).length())
            dtStr += " 00:00:00";

        dtStr += " "; // don't remove!

        try
            {
            String yr = dtStr.substring(0, 4).trim();
    	    YMDHMS[0] = Integer.parseInt(yr.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[0] = -1;
            }

        try {
            String mth = dtStr.substring(5, 7).trim();
    	    YMDHMS[1] = Integer.parseInt(mth.trim());

            if (YMDHMS[1] == 0)
                YMDHMS[1] = -1;
            }
        catch (NumberFormatException e)
            {
    	    YMDHMS[1] = -1;
            }

    	try	{
            String day = dtStr.substring(8, 10).trim();

    	    YMDHMS[2] = Integer.parseInt(day.trim());
    	    }
    	catch(NumberFormatException e)
    	    {
    	    YMDHMS[2] = -1;
    	    }

        try
            {
            String hr = dtStr.substring(11, 13).trim();
    	    YMDHMS[3] = Integer.parseInt(hr.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[3] = 0;
            }

        try
            {
            String min = dtStr.substring(14, 16).trim();
    	    YMDHMS[4] = Integer.parseInt(min.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[4] = 0;
            }

        try
            {
            String sec = dtStr.substring(17, 19).trim();
    	    YMDHMS[5] = Integer.parseInt(sec.trim());
            }
        catch (Exception e)
            {
    	    YMDHMS[5] = 0;
            }

        return YMDHMS;
    }



    // Inner classes

    // support classeses for sortDates()

    class NumDateAndNdx implements InumericSortable
    {
        public long numRep;             // numerical representation of date
        public int  assoc;              // associated data

        public NumDateAndNdx(long numRep, int assoc)
        {
            this.numRep = numRep;
            this.assoc  = assoc;
        }

        public long sortvalue()
        {
            return numRep;
        }
    }
}

