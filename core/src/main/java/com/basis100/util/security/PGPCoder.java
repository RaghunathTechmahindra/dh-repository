package com.basis100.util.security;

/**
* <p>Title: PGPCoder </p>
* <p>Description: wrapper to code a stream using pgp.</p>
* <p>Copyright: Copyright (c) 2004</p>
* <p>Company: FI</p>
* @author DVG 25/May/2004
* @version 1.0
*
* @version 1.2
* 24/May/2005 DVG #DG212 #1429  DJ Prod-Document for deal id 66 stuck up in Document Queue
   Just added synchronization to avoid conflicts when reading the inputStream pub.keys
* 23/AUG/2004-DVG #DG50 add output path
* 13/AUG/2004-DVG #DG20 reset key inputstream allowing it to be reused
*
*/

import java.io.*;
import java.security.*;
import java.util.*;


import org.bouncycastle.bcpg.*;
import org.bouncycastle.jce.provider.*;
import org.bouncycastle.openpgp.*;

public class PGPCoder {

  /**
   * bouncy castle provider already registered with Security?
   */
  private static boolean ibBCprovider = false;

  /**
   * A simple routine that opens a key ring file and loads the first available key suitable for
   * encryption.
   *
   * @param in input stream
   * @return key as PGPPublicKey
   * @throws IOException
   * @throws PGPException
   */
  private static PGPPublicKey readPublicKey(
      InputStream	poin)
      throws IOException, PGPException
  {
    poin = PGPUtil.getDecoderStream(poin);

    PGPPublicKeyRingCollection		pgpPub = new PGPPublicKeyRingCollection(poin);

    //
    // we just loop through the collection till we find a key suitable for encryption, in the real
    // world you would probably want to be a bit smarter about this.
    //
    PGPPublicKey	key = null;

    //
    // iterate through the key rings.
    //
    Iterator rIt = pgpPub.getKeyRings();

    while (key == null && rIt.hasNext())
    {
      PGPPublicKeyRing	kRing = (PGPPublicKeyRing)rIt.next();
      Iterator						kIt = kRing.getPublicKeys();

      while (key == null && kIt.hasNext())
      {
        PGPPublicKey	k = (PGPPublicKey)kIt.next();

        if (k.isEncryptionKey())
        {
          key = k;
        }
      }
    }

    if (key == null)
    {
      throw new IllegalArgumentException("Can't find encryption key in key ring.");
    }

    return key;
  }

  /**
   * Load a secret key ring collection from keyIn and find the secret key corresponding to
   * keyID if it exists.
   *
   * @param keyIn input stream representing a key ring collection.
   * @param keyID keyID we want.
   * @param pass passphrase to decrypt secret key with.
   * @return key as PGPPrivateKe
   * @throws IOException
   * @throws PGPException
   * @throws NoSuchProviderException
   */
//  private static PGPPrivateKey findSecretKey(
//      InputStream keyIn,
//      long keyID,
//      char[] pass)
//      throws IOException, PGPException, NoSuchProviderException
//  {
//    PGPSecretKeyRingCollection	pgpSec = new PGPSecretKeyRingCollection(
//        PGPUtil.getDecoderStream(keyIn));
//
//    PGPSecretKey	pgpSecKey = pgpSec.getSecretKey(keyID);
//
//    if (pgpSecKey == null)
//    {
//      throw new IllegalArgumentException("secret key for message not found.");
//    }
//
//    return pgpSecKey.extractPrivateKey(pass, "BC");
//  }

  /**
   * decrypt the passed in message stream
   * @param poInStr input data stream
   * @param poKeyInStr input key stream
   * @param paPasswd the key password
   * @throws Exception
   */

  private static void decryptStr(
      InputStream poInStr,
      InputStream poKeyInStr,
      char[] paPasswd)
      throws Exception
  {
    if (!ibBCprovider){
      Security.addProvider(new BouncyCastleProvider());
      ibBCprovider = true;
    }
    poInStr = PGPUtil.getDecoderStream(poInStr);

    try
    {
      PGPObjectFactory lopgpF = new PGPObjectFactory(poInStr);
      PGPEncryptedDataList loEncdl;
      PGPPublicKeyEncryptedData	pbe = null;
      PGPPrivateKey lopk = null;

      Object lo = lopgpF.nextObject();
      //
      // the first object might be a PGP marker packet.
      //
      if (lo instanceof PGPEncryptedDataList)
      {
        loEncdl = (PGPEncryptedDataList)lo;
      }
      else
      {
        loEncdl = (PGPEncryptedDataList)lopgpF.nextObject();
      }

      PGPSecretKeyRingCollection	pgpSec = new PGPSecretKeyRingCollection(
          PGPUtil.getDecoderStream(poKeyInStr));
      found:{
        for (int lij = 0; lij < loEncdl.size(); lij++) {
          pbe = (PGPPublicKeyEncryptedData)loEncdl.get(lij);
          //lopk = findSecretKey(poKeyInStr, pbe.getKeyID(), paPasswd);

          PGPSecretKey pgpSecKey = pgpSec.getSecretKey(pbe.getKeyID());
          if (pgpSecKey != null)        {
            lopk = pgpSecKey.extractPrivateKey(paPasswd, "BC");
            break found;
          }
        }
        throw new IllegalArgumentException("secret key for message not found.");
      }

      //InputStream clear = pbe.getDataStream(findSecretKey(poKeyInStr, pbe.getKeyID(), paPasswd), "BC");
      InputStream clear = pbe.getDataStream(lopk, "BC");

      PGPObjectFactory plainFact = new PGPObjectFactory(clear);

      PGPCompressedData	cData = (PGPCompressedData)plainFact.nextObject();

      PGPObjectFactory pgpFact = new PGPObjectFactory(cData.getDataStream());

      Object message = pgpFact.nextObject();

      if (message instanceof PGPLiteralData)
      {
        PGPLiteralData ld = (PGPLiteralData)message;

        FileOutputStream fOut = new FileOutputStream(ld.getFileName());

        InputStream unc = ld.getInputStream();
        int ch;

        while ((ch = unc.read()) >= 0)
        {
          fOut.write(ch);
        }
      }
      else if (message instanceof PGPOnePassSignatureList)
      {
        throw new PGPException("encrypted message contains a signed message - not literal data.");
      }
      else
      {
        throw new PGPException("message is not a simple encrypted file - type unknown.");
      }

      if (pbe.isIntegrityProtected())
      {
        if (!pbe.verify())
        {
          System.err.println("message failed integrity check");
        }
        else
        {
          System.err.println("message integrity check passed");
        }
      }
      else
      {
        System.err.println("no message integrity check");
      }
    }
    catch (PGPException e)
    {
      System.err.println(e);
      if (e.getUnderlyingException() != null)
      {
        e.getUnderlyingException().printStackTrace();
      }
    }
  }

  /**
   * encrypt the input file into the output stream
   * @param poInFil input data File
   * @param poStrOut output encoded stream
   * @param poEncKey the encryption public key
   * @param pbArmor asc2 format?
   * @param pbwithIntegrityCheck use integrity check?
   * @throws IOException
   * @throws NoSuchProviderException
   */
  private static void encryptStr(
      File poInFil,
      OutputStream	poStrOut,
//      InputStream poStrIn,
      PGPPublicKey[]	poEncKey,
      boolean			pbArmor,
      boolean			pbwithIntegrityCheck)
      throws IOException, NoSuchProviderException
  {
    if (pbArmor)
    {
      poStrOut = new ArmoredOutputStream(poStrOut);
    }

    try
    {
      ByteArrayOutputStream bOut = new ByteArrayOutputStream();

      PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(
          PGPCompressedData.ZIP);

      PGPUtil.writeFileToLiteralData(comData.open(bOut), PGPLiteralData.BINARY, poInFil);

      comData.close();

      PGPEncryptedDataGenerator	cPk = new PGPEncryptedDataGenerator(PGPEncryptedData.CAST5, pbwithIntegrityCheck, new SecureRandom(), "BC");

      for (int li = 0; li < poEncKey.length; li++) {
        cPk.addMethod(poEncKey[li]);
      }

      byte[] bytes = bOut.toByteArray();

      OutputStream cOut = cPk.open(poStrOut, bytes.length);

      cOut.write(bytes);

      cPk.close();

      poStrOut.close();
    }
    catch (PGPException e)
    {
      System.err.println(e);
      if (e.getUnderlyingException() != null)
      {
        e.getUnderlyingException().printStackTrace();
      }
    }
  }

  /**
   * encrypt vector of files using pgp, defaults to ascii w/o integrity check
   * @param poInFiles vector of input files
   * @param poPubKey public key stream
   * @return vector of encrypted output files
   * @throws Exception
   */
  public static Vector PGPencrypt(
      Vector poInFiles, InputStream[] poPubKey)
      throws Exception
  {
    return PGPencrypt(poInFiles, poPubKey, true, false, null); //#DG50
  }

  //#DG50
  /**
   * encrypt vector of files using pgp, defaults to ascii w/o integrity check
   * @param poInFiles vector of input files
   * @param poPubKey public key stream
   * @param psOutPath String contains the common output path folder
   * @throws Exception
   * @return Vector of encrypted output files
   */
  public static Vector PGPencrypt(
      Vector poInFiles, InputStream[] poPubKey, String psOutPath)
      throws Exception
  {
    return PGPencrypt(poInFiles, poPubKey, true, false, psOutPath);
  }

  /**
   * encrypt vector of files using pgp, defaults to ascii w/o integrity check
   * @param poInFiles vector of input files
   * @param psPubKey public key string
   * @return vector of encrypted output files
   * @throws Exception
   */
  public static Vector PGPencrypt(
      Vector poInFiles, String[] psPubKey)
      throws Exception
  {
    ByteArrayInputStream[] lokeyIn = new ByteArrayInputStream[psPubKey.length];
    for (int li = 0; li < lokeyIn.length; li++) {
      lokeyIn[li] = new ByteArrayInputStream(psPubKey[li].getBytes());
    }
    return PGPencrypt(poInFiles, lokeyIn, true, false, ".");
  }

  /**
   * encrypt vector of files using pgp
   * @param poInFiles vector of input files
   * @param poPubKey public key stream
   * @param pbAsc produces an ascii files (as opposed to binary)
   * @param pbIntegChk include integrity check
   * @return vector of encrypted output files
   * @throws Exception
   */
    public static synchronized Vector PGPencrypt(   //#DG212 added sync
        Vector poInFiles, InputStream[] poPubKey,
        boolean pbAsc, boolean pbIntegChk, String psOutPath) //#DG50
        throws Exception
    {
    if (!ibBCprovider){
      //addBCprovider();
      //if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null)
      Security.addProvider(new BouncyCastleProvider());
      ibBCprovider = true;
    }

    PGPPublicKey[] loEncKey = new PGPPublicKey[poPubKey.length];
    for (int li = 0; li < loEncKey.length; li++) {
      poPubKey[li].reset(); //#DG20 reset key inputstream allowing it to be reused
      loEncKey[li] = readPublicKey(poPubKey[li]);
      poPubKey[li].reset(); //#DG20 reset key inputstream allowing it to be reused
    }

    Vector loOutFiles = new Vector(poInFiles.size());
    Iterator loItOutFil = poInFiles.iterator();
    while(loItOutFil.hasNext()){
      File loIn = (File)loItOutFil.next();
      FileInputStream loStrIn = new FileInputStream(loIn);

      if (psOutPath == null) //#DG50
        psOutPath = loIn.getParent()+File.separatorChar;
      //#DG50 File loOut = new File(loIn.getName()+ ".pgp");
      File loOut = new File(psOutPath+loIn.getName()+ ".pgp"); //#DG50
      FileOutputStream	loStrOut = new FileOutputStream(loOut);

      encryptStr(loIn, loStrOut, loEncKey, pbAsc, pbIntegChk);
      loOutFiles.add(loOut);
    }
    return loOutFiles;
  }

}
