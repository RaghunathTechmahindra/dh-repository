package com.basis100.entity;

public final class RemoveException extends Exception {

	private static final long serialVersionUID = 1L;

	public RemoveException() {
    }

	public RemoveException(String message) {
        super(message);
    }

	public RemoveException(Throwable cause) {
		super(cause);
	}

	public RemoveException(String message, Throwable cause) {
		super(message, cause);
	}

}