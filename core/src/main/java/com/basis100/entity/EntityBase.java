// EntityBase
//
// Base class for MOS entity classes.
//
// The purpose of this base class is to provide a holder for the session resources
// object proided in entity constructors.
//
// In addition, the class provides certain facilities for allowing entities to be
// codes as quasi-enterprise beans, For example, the enterprise beans interface EntityContext is
// implemented as an inner class. This allows coding of the entity beans methods
// setEntityContext() and unsetEntityContext() enven though the entities at not actually
// deployed as true enterprise beans.
//

package com.basis100.entity;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.util.DBA;
import com.basis100.deal.util.DisplayFieldPicklist;
import com.basis100.jdbcservices.JdbcConnection;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

public class EntityBase implements java.io.Serializable
{
    private static final long serialVersionUID = 1L;
    public EntityContext  entityContext ;
    public TransactionHistory transactionHistory ;

    // resources kit provided by caller in (sub-class) constructors
    public SessionResourceKit srk;

    protected final static Log logger = LogFactory.getLog(EntityBase.class);
//    public SysLogger logger;
    public JdbcExecutor jExec;

    // Typically used in context of finder calls when absence of record would be permissible (not an error).
    // When set in such a case the finder could avoid generating log messages.
    protected boolean silentMode = false;

    //4.4 Transaction History
    private static boolean useOldDCHTable = true;      

   static
   {
	   try
	   {
		   //4.4 Transaction History
			JdbcExecutor jExec = JdbcConnection.getInstance(null).getJdbcExecutor();
	
			int key = jExec.execute("select column_name from user_tab_cols where table_name ='DEALCHANGEHISTORY' and column_name = 'DISPLAYFIELDNAME'"); 
			
			if (jExec.next(key) )
				useOldDCHTable = false; // if it returns a row, we want to use new table, so set this false
			else
				useOldDCHTable = true; // if that column is not in table, use old table.
	
			jExec.closeData(key);
			//4.4 Transaction History
	   }
	   catch (Exception e)
	   {
		   //maybe do something meaninful here....
		   useOldDCHTable = true;
	   }
    }
    
    public EntityBase()
    {
      entityContext = new EntityContext();    // Initital the entityContext
      transactionHistory = new TransactionHistory() ; // Initital the transactionHistory
    }

    public EntityBase(SessionResourceKit srk)
    {
      setSessionResourceKit(srk);
      entityContext = new EntityContext();    // Initital the entityContext
      transactionHistory = new TransactionHistory() ; // Initital the transactionHistory
    }

    public SessionResourceKit getSessionResourceKit()
    {
      return srk;
    }


    public void setSessionResourceKit(SessionResourceKit srk) {
        this.srk = srk;
        // code put in for CIBC so this method doesnt hurl on trying to set to
        // null
        if (null == srk) {
            jExec = null;
        } else {
            jExec = srk.getJdbcExecutor();
        }
    }

    public void setSilentMode(boolean b)
    {
      silentMode = b;
    }
    public boolean getSilentMode()
    {
      return silentMode;
    }

    //
    // dummy out Enterprise Beans context class (for now) as an inner class ???
    //

    public class EntityContext
    {
      private int applicationId ;
      private String contextTextSource="" ;
      private String contextText ="";
      private String scenarioContext ="";

      public void setApplicationId( int aId)  {   applicationId = aId ;   }
      public void setContextText( String ct)  {   contextText = ct;     }
      public void setContextSource( String cs) {  contextTextSource = cs;    }
      public void setScenarioContext( String sc )  {scenarioContext = sc;   }

      public int getApplicationId ()
      {
        return applicationId;
      }

      public String getContextText()
      {
        return contextText ;
      }

      public String getContextSource()
      {
        return contextTextSource ;
      }

      public String getScenarioContext()
      {
        return  scenarioContext  ;
      }
    }

    //
    //
    //
    public class TransactionHistory
    {
      //private int dealChangeHistoryId ;         // unique key

      private int auditId  ;                    // Audit Id   srk.getAuditId()
      private String userProfileId ;

      private int applicationId ;               // deal number
      private java.util.Date transactionDate ;  // current date at record creation
      private int historyFieldNameId ;          // reference to history Field Table
      private String contextTextSource ;
      private String contextText ;

      private String scenarioContext ;

      private int currentValue;                 // Current Field value== new value
      private String currentValueText ;         // Current value in text
      private int previousValue ;               // PreviousValue == Old value
      private String previousValueText ;        // previous value in text
//4.4 Transaction History
      private String entityName;				// entity to be written to
      private String attributeName;				// field within said entity
      private String displayFieldName;			// displayfield name of above

      public void setGenericInfo()              // this section of info. are generic for a transaction history
      {
        auditId = srk.getAuditId ()  ;
        userProfileId = "" + srk.getExpressState().getUserProfileId();

        // this part of info are from EntityContext
        applicationId = entityContext.getApplicationId () ;
        scenarioContext = entityContext.getScenarioContext ();
        contextText = entityContext.getContextText ();
        contextTextSource = entityContext.getContextSource ();
        transactionDate  = java.util.Calendar.getInstance ().getTime ();
      }

      public void setHistoryFieldNameId( int hfnID )      {  historyFieldNameId = hfnID;    }

      public void setCurrentValue ( int cValue )              { currentValue = cValue ; }
      public void setCurrentValueText ( String cValue )       { currentValueText = cValue ; }
      public void setPreviousValue ( int pValue )             { previousValue = pValue ; }
      public void setPreviousValueText ( String pValue )      { previousValueText = pValue ; }
//4.4 Transaction History
      public void setEntityName ( String name )      { entityName = name ; }
      public void setAttributeName ( String attribute )      { attributeName = attribute ; }
      public void setDisplayFieldName ( String displayField )      { displayFieldName = displayField ; }
      
      public void store( )  throws Exception
      {
        StringBuffer sb = new StringBuffer();

        if (useOldDCHTable)
        {
            // displayField: ( EntityName, AttributeName, DisplayFieldName ) ==> return displayFieldId
			int historyFieldNameId = updateDisplayFieldTable(entityName, attributeName, displayFieldName);	
				
        sb.append(
          "Insert into DealChangeHistory " +
          " (AID, DealChangeHistoryId, DCHUSERPROFILEID , ApplicationId, TransactionDate,HistoryFileNameId, ContextText," +
          " contextTextSource, CurrentValue , CurrentValueText , " +
          " PreviousValue, PreviousValueText, ScenarioContext, INSTITUTIONPROFILEID) " +
          " Values ( "
        );
	
        sb.append(auditId);                                             sb.append(", ");
        sb.append("DealChangeHistorySeq.nextval, ");
        sb.append(userProfileId);                                       sb.append(", ");
        sb.append(applicationId);                                       sb.append(", ");
        sb.append(DBA.sqlStringFrom(transactionDate));                  sb.append(", ");
        sb.append(historyFieldNameId);                                  sb.append(", ");
        sb.append(DBA.sqlStringFrom(contextText, 35));                  sb.append(", ");
        sb.append(DBA.sqlStringFrom(contextTextSource, 35));            sb.append(", ");
        sb.append(currentValue);                                        sb.append(", ");
        sb.append(DBA.sqlStringFrom(currentValueText, 255));            sb.append(", ");
        sb.append(previousValue);                                       sb.append(", ");
        sb.append(DBA.sqlStringFrom(previousValueText, 255));           sb.append(", ");
        sb.append(DBA.sqlStringFrom(scenarioContext, 255));             sb.append(", ");
        sb.append(srk.getExpressState().getDealInstitutionId());        sb.append(")");
        }
        else
        {
        sb.append(
          "Insert into DealChangeHistory " +
          " (AID, DealChangeHistoryId, DCHUSERPROFILEID , ApplicationId, TransactionDate,HistoryFileNameId, ContextText," +
          " contextTextSource, CurrentValue , CurrentValueText , " +
          " PreviousValue, PreviousValueText, ScenarioContext, INSTITUTIONPROFILEID, " +
          " entityName, attributeName, displayFieldName " +
          ") Values ( "
        );

        sb.append(auditId);                                             sb.append(", ");
        sb.append("DealChangeHistorySeq.nextval, ");
        sb.append(userProfileId);                                       sb.append(", ");
        sb.append(applicationId);                                       sb.append(", ");
        sb.append(DBA.sqlStringFrom(transactionDate));                  sb.append(", ");
        sb.append("0, ");
        sb.append(DBA.sqlStringFrom(contextText, 35));                  sb.append(", ");
        sb.append(DBA.sqlStringFrom(contextTextSource, 35));            sb.append(", ");
        sb.append(currentValue);                                        sb.append(", ");
        sb.append(DBA.sqlStringFrom(currentValueText, 255));            sb.append(", ");
        sb.append(previousValue);                                       sb.append(", ");
        sb.append(DBA.sqlStringFrom(previousValueText, 255));           sb.append(", ");
        sb.append(DBA.sqlStringFrom(scenarioContext, 255));             sb.append(", ");
        sb.append(srk.getExpressState().getDealInstitutionId());        sb.append(", ");
        sb.append(DBA.sqlStringFrom(entityName, 35));					sb.append(", ");
        sb.append(DBA.sqlStringFrom(attributeName, 35));				sb.append(", ");
        sb.append(DBA.sqlStringFrom(displayFieldName, 255));			sb.append(")");
        }

        try
        {
          int key = jExec.executeUpdate(sb.toString());
        }
        catch (Exception e)
        {
            logger.error("Exception @EntityBase.TransactionHistory.store: Inserting audit record");
            logger.error(e);
        }
      }
    //*************************************************************************************************
    //******** Beginning of Low Level Aduit************************************************************
      //////////////////////////////////////////////////////////////////////////////////////////////////
      // Inert a ChangeMapvalue into the DisplayFieldTable
      // return DisplayFieldTableId
      //moved from DealEntity class for 4.4T    
     private int updateDisplayFieldTable(String entityName, String attributeName, String displayFieldName) throws Exception
      { // insert the displayfield to the DisplayField table
          ///////////////////// Get the Sequence ID First ////////////////////////////////
          int displayFieldId= -1  ;
          try
          {
              String sql = "Select DisplayFieldseq.nextval from dual";
              int key = jExec.execute(sql);
              for (; jExec.next(key);  )
              {   displayFieldId  = jExec.getInt(key,1);  // can only be one record
    }
              jExec.closeData(key);
              if( displayFieldId == -1 ) throw new Exception();
          } catch ( Exception e )
          {
               CreateException ce = new CreateException("DealEntity.DisplayField exception getting DispalyFieldId from sequence");
               logger.error(ce.getMessage());
               logger.error(e);
               throw ce;
          }///////////////// End of getting the sequance Id for DisplayField ////////////////////
          try
          {
              String sql = "Insert into DisplayField ( DisplayFieldId, EntityName, AttributeName, DisplayFieldName, INSTITUTIONPROFILEID) Values( " ;
              sql +=  displayFieldId + "," ;
              sql +=  "'" + entityName + "'," ;
              sql +=  "'" + attributeName + "'," ;
              sql +=  "'" + DisplayFieldPicklist.getDisplayField ( displayFieldName) + "'," ;
              sql +=  "" + srk.getExpressState().getDealInstitutionId()  + " ) " ;
              int key = jExec.execute(sql);
              jExec.closeData(key);
          } catch ( Exception e )
          {
               CreateException ce = new CreateException("DealEntity.DisplayField exception Insertting to DisplayField");
               logger.error(ce.getMessage());
               logger.error(e);
               throw ce;
          }
        return displayFieldId;
     } // ----------  End of updateDisplayFieldTable()   -------------------------
    }
}


