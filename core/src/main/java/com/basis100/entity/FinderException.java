package com.basis100.entity;

/**
 * 11/Feb/2008 DVG #DG698 Merix Multi-Lender Lender Assignment Project
 */

public final class FinderException extends Exception
{
    public FinderException()
    {
        super();
    }

    public FinderException(String message)
    {
        super(message);
    }
    
    //#DG698 complete the family of constructors
    public FinderException(String message, Throwable cause)
    {
    	super(message, cause);
    }
}