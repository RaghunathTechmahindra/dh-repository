package com.basis100.entity;

public final class RemoteException extends Exception
{
    public RemoteException()
    {
        super();
    }

    public RemoteException(String message)
    {
        super(message);
    }

}