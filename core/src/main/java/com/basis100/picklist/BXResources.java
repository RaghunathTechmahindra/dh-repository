package com.basis100.picklist;

/**
 * Title:       ResourceBundleUtil
 * Description: Provides Method to access the Resource Bundle properties
 * Copyright:   Copyright Basis100(c) 2002
 * Company:     Basis100
 * @author      Billy Lam
 * @version 1.0 Created on 30Oct2002
 *
 * 26.Jun.2009 DVG #DG862 LEN380950: 	Ruth Kiaupa DE-INIT business rule issue
 * 04/Oct/2007 DVG #DG634 FXP18630: DupeCheck - BDN Express - some changes won't be able to show up in Deal Compare Notes 
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 * 22/Jan/2007 DVG #DG572 #5573  java.lang.NullPointerException==>Fall Back to ?null?  
 * 23/SEP/2004- DVG #DG80 address scrub - added new method to bxresources to
        retrieve an id given a description
 */

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Formattable;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;

public class BXResources implements Xc
{
	
    public static final String LANGUAGEPREFERENCEID = "LANGUAGEPREFERENCEID";
    // Definitations
  // Locale Language Code
  public static final String ENGLISH_CODE = "en";
  public static final String FRENCH_CODE = "fr";
  public static final String JSP_EXTENTION = ".jsp";
  public static final String UNDERSCORE = "_";

  // Class Path for Picklist Properties
  public static final String PICKLIST_PATH = "BXResources.PickList.";

  // SEAN Routing Order Changes (May 09, 2005): path to sys config directory.
  public static final String SYSCONFIG_PATH = "BXResources.SysConfig.";
  private static final String VALUE_DESC_SEPARATOR = "|";
  // SEAN Routing Order Changes END

  // Path and Filename of System Message Resource Properties
  public static final String SYS_MSG_PATH = "BXResources.SystemMessage";
  // Path and Filename of Generic Translation Message Resource Properties
  public static final String BXGENERIC_PATH = "BXResources.BXGeneric";
  // Path and Filename of DB Field Description Resource Properties
  public static final String BXDBFIELD_PATH = "BXResources.DBFieldDesc";
  // Path and Filename of docprep / ingestion resource properties
  public static final String DOCPREP_INGESTION_PATH = "BXResources.docprep_ingestion";
  // Path to rich client message.
  public static final String RICH_CLIENT_MSG_PATH = "BXResources.RichClientMessage";
//  public static final String FRENCH_CODE = Locale.FRENCH.getLanguage();

  //#DG600 language preference items
    public static final String LOCALEVARIANT = "LOCALEVARIANT";
    public static final String LOCALECOUNTRY = "LOCALECOUNTRY";
    public static final String LOCALELANGUAGE = "LOCALELANGUAGE";
    //#DG600 end
  
  // System Logger for reporting error and debug
    //private static SysLogger logger = ResourceManager.getSysLogger("BXRESOURCES");
    static Log logger = LogFactory.getLog(BXResources.class);      
  
    private static Entry<Integer, Locale>[] locales = buildLocales();   //#DG600
public static final String INSTITUTION_SEPARATOR = ".";

    // the default institution id.
    private static int _defaultInstitutionId = -1;
    
    //map with institutionProfileId and name
    private static HashMap<Integer, String> _institutions = null;

    public BXResources()
  {
  }

  //===================================================================================
  // Methods for Getting PickList Data from Resource Bundle Properties files
  //
  // Assumptions :
  // 1) PickList ResourceBundle fielname format : TABLENAME_xx.properties (where xx is
  //    the language code e.g. "en" for English, "fr" for French
  //
  // 2) PickList Properties file format : 'ID = SortOrder|Description'
  //  Note : The first one (lowest sort order #) will be the default
  //  If Sort order not specified will be sorted by ID
  //====================================================================================
 
  //#DG600
  /**
  //===================================================================================
  // Method to get a Description of a particular PickList ID
  //
  // Inputs:
* @param tableName : String Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
* @param id String : The ID of the PickList entry in string format e.g. "1", "2", ...
* @param languageId int : Language ID e.g. 0 for English, 1 for Franch
  //
* @return String Output : The description of the PickList Entry
  //
  //===================================================================================
   *
   */
  public static String getPickListDescription(int institutionProfileId, String tableName, String id, int languageId)
  {
    return getPickListDescription(institutionProfileId, tableName, id, getLocale(languageId));  //#DG600
  }
  
  
  public static String getPickListDescription(int institutionProfileId,String tableName, int id, Locale locala)
  {
    return getPickListDescription(institutionProfileId,tableName, String.valueOf(id), locala); //#DG600
  }

  /**
  //===================================================================================
  // Method to get a Description of a particular PickList ID
  //
  // Inputs:
   * @param tableName : String Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
   * @param id String : The ID of the PickList entry in string format e.g. "1", "2", ...
   * @param languageId int : Language ID e.g. 0 for English, 1 for Franch
  //
   * @return String Output : The description of the PickList Entry
  //
  //===================================================================================
   *
   */
  public static String getPickListDescription(int institutionProfileId,String tableName, String id, Locale locala)
  {
    try
    {
        if (institutionProfileId < 0)
        {
            institutionProfileId = getDefaultInstitutionId();
        }
        ResourceBundle pRsBundle = ResourceBundle.getBundle(PICKLIST_PATH+tableName.toUpperCase(),locala);
        String theOrgVal = null;
      String newId = institutionProfileId + INSTITUTION_SEPARATOR + id;
      
      //Check if the properties have been institutionalized. Else use the default key
      if (isKeyPresent(pRsBundle.getKeys(), newId)){
          theOrgVal = pRsBundle.getString(newId);
      }
      else
      {
          try {
            theOrgVal = pRsBundle.getString(id);
        } catch (NullPointerException e) {   // id is null, get default
            theOrgVal = getDefaultValue(tableName, locala);
        }  
      }
        
      /* #DG572 rewritten to handle null keys, returning the default, ie. lowest sort order key 
      String theOrgVal = pRsBundle.getString(id);
      StringTokenizer rep = new StringTokenizer(theOrgVal, "|");
      try
      {
        //Check if Sort Order exist
        if(rep.countTokens() > 1)
          rep.nextToken().trim(); // Get Sort Order

        return rep.nextToken().trim(); // Get Description
      }
      catch(Exception ex)
      {
        // Fallback to return the original string
        logger.warning("@BXResources.getPickListDescription :: SortOrder not set ==> Fallback to return the original value");
        return theOrgVal;
      } */
      
        
            
        int cur = theOrgVal.indexOf(VALUE_DESC_SEPARATOR); 
        if (cur < 0) 
            logger.warn("@BXResources.getPickListDescription :: SortOrder not set ==> Fallback to return the original value");
        else
            theOrgVal = theOrgVal.substring(cur+1);  // skip sort order
        theOrgVal = theOrgVal.trim();
        return theOrgVal;
    }
    catch(Exception e)
    {
        final String msg = "Exception @BXResources.getPickListDescription : tableName="+tableName
        	+ ";instid="+institutionProfileId+
                            " : ID=" + id + " : languageId=" + locala + " :: " + e+
                            "==>Fall Back to " + "?" + id + "?";
            // Got an Exception : Most likely resource properties file not found
        //e.printStackTrace();  avoid cluttering server log file!
        //System.err.println(msg);
        logger.warn(msg);
        // Fallback to return a null string
        return "?" + id + "?";
    }
  }
/*
   * FXP19908 - FIX BEGIN
   */
  /**
   * Method to get a Description of a particular PickList ID
   * 
   * @param institutionProfileId
   * @param tableName :
   *            String Name of the PickList Table e.g. ACCESSTYPE (non case
   *            sensitive)
   * @param id
   *            String : The ID of the PickList entry in string format e.g.
   *            "1", "2", ...
   * @param languageId
   *            int : Language ID e.g. 0 for English, 1 for French //
   * @return Integer Output : The sort order of the PickList Entry
   */
  public static Integer getPickListSortOrder(int institutionProfileId,String tableName, String id, int languageId)
  {
      try
      {
          if (institutionProfileId < 0)
          {
              institutionProfileId = getDefaultInstitutionId();
          }
          String languageCode = getLocaleCode(languageId);
          
          ResourceBundle pRsBundle = ResourceBundle.getBundle(PICKLIST_PATH+tableName.toUpperCase(),
                  new Locale(languageCode, ""));
          String theOrgVal = null;
          String newId = institutionProfileId + INSTITUTION_SEPARATOR + id;
          
          //Check if the properties have been institutionalized. Else use the default key
          if (isKeyPresent(pRsBundle.getKeys(), newId)){
              theOrgVal = pRsBundle.getString(newId);
          }
          else
          {
              theOrgVal = pRsBundle.getString(id);
          }
          
          
          StringTokenizer rep = new StringTokenizer(theOrgVal, "|");
          try
          {
              //Check if Sort Order exist
              if(rep.countTokens() > 1)
                  return new Integer(rep.nextToken().trim()); // Get Sort Order
              
              return new Integer("0"); // Get Description
          }
          catch(Exception ex)
          {
              // Fallback to return the original string
              logger.warn("@BXResources.getPickListDescription :: SortOrder not set ==> Fallback to return the original value");
              return new Integer("0");
          }
      }
      catch(Exception e)
      {
//      e.printStackTrace();
          // Got an Exception : Most likely resource properties file not found
          logger.warn("Exception @BXResources.getPickListDescription : tableName="+tableName
               	+ ";instid="+institutionProfileId+
                  " : ID=" + id + " : languageId=" + languageId + " :: " + e +
                  "==>Fall Back to " + "?" + id + "?");
          return new Integer("0");
      }
  }
 /*
   * FXP19908 - FIX END
   */
/**
   * Method to get a Description of a particular PickList ID
   * 
   * @param institutionProfileId
   * @param tableName
   *            String : Name of the PickList Table e.g. ACCESSTYPE (non case
   *            sensitive)
   * @param id
   *            int : The ID of the PickList entry in int format
   * @param languageId
   *            int : Language ID e.g. 0 for English, 1 for French 
   * @return String Output : The description of the PickList Entry
   */
  public static String getPickListDescription(int institutionProfileId,String tableName, int id, int languageId)
  {
      return getPickListDescription(institutionProfileId, tableName, id+"", languageId);
  }
  
  /**
  //===================================================================================
  // Method to get a Description of a particular PickList ID
  //
  // Inputs:
  * @param tableName String : Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
  * @param id int : The ID of the PickList entry in int format
  * @param languageId int : Language ID e.g. 0 for English, 1 for Franch
  //
* @return String Output : The description of the PickList Entry
  //===================================================================================
*/
  	
  //
  /* 23/SEP/2004- DVG #DG80 address scrub - added new method to bxresources to
          retrieve an id given a description
   */
  /**
   * Return the (first) entry id corresponding to given description
   *
   * @param pstableName String Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
   * @param psDescr String the description of the entry
   * @param pilanguageId int Language ID e.g. 0 for English, 1 for Franch
   * @return String the entry id
   */
  //#DG600 convenience method
  
  public static String getPickListDescriptionId(int institutionProfileId, String pstableName, String psDescr,
          int languageId) {
        return getPickListDescriptionId(institutionProfileId,pstableName, psDescr, getLocale(languageId)); //#DG600
      }
  /**
   * Return the (first) entry id corresponding to given description
   *
   * @param institutionProfileId
   * @param table String Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
   * @param description String the description of the entry
   * @param languageId int Language ID e.g. 0 for English, 1 for French
   * @return String the entry id
   */
  public static String getPickListDescriptionId(int institutionProfileId, String table, String description, Locale locala) {
    try {
        if (institutionProfileId < 0)
        {
            institutionProfileId = getDefaultInstitutionId();
        }
        ResourceBundle bundle = ResourceBundle.getBundle(PICKLIST_PATH + table.toUpperCase(),
                locala);

        Enumeration<String> all = bundle.getKeys();
        //Filter based on institutionProfileId
        List<String> filteredKeys = getKeysForInstitution(all, institutionProfileId);
        String lstheKey = "";
        String lstheOrgVal = "";
        
        for (int i=0; i < filteredKeys.size(); i++){
          lstheKey = filteredKeys.get(i);
          lstheOrgVal = bundle.getString(lstheKey);

          StringTokenizer tokenizer = new StringTokenizer(lstheOrgVal, "|");
          //Check if Sort Order exist
          if(tokenizer.countTokens() > 1)
              tokenizer.nextToken(); // skip Sort Order

          if (description.equalsIgnoreCase(tokenizer.nextToken().trim())) // compare Description
          {
              StringTokenizer keytokenizer = new StringTokenizer(lstheKey, INSTITUTION_SEPARATOR);
              if(keytokenizer.countTokens() > 1){
                  String keyMinusInst = keytokenizer.nextToken();
                  if (keytokenizer.hasMoreTokens())
                  {
                      keyMinusInst = keytokenizer.nextToken();
                  }
                  return keyMinusInst;
              }
              else
              {
                  return lstheKey;
              }
          }
        }
        throw new Exception('"'+description+"\" not found");
    }
    catch (Exception e) {
        // Got an Exception : Most likely resource properties file not found
        logger.error("Exception @BXResources.getPickListDescriptionId : tableName=" 
      		  +table + ";instid="+institutionProfileId+
                     " : languageId=" + locala + " :: " + e);
        // Fallback to return a null string
        return null;
      }
  }
 
  /**
  //===================================================================================
  // Method to get all Values and Descriptions of a particular PickList Table in sorted
  //  by the SortOrder specified in the Properties file
  //
  // Inputs:
* @param tableName String : Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
* @param languageId int : Language ID e.g. 0 for English, 1 for Franch
  //
* @return Collection Output : Collection of String[2] objects ==>  String[0] = Value; String[1] = Desc
  //         : Returns NULL if any problem
  //
  //===================================================================================
*/
  //#DG600
  
  public static Collection<String[]> getPickListValuesAndDesc(int institutionProfileId, String tableName, int languageId)
  {
    return getPickListValuesAndDesc(institutionProfileId,tableName, getLocale(languageId));  
  }
  

  /**
   * Method to get all Values and Descriptions of a particular PickList
   * Table in sorted by the SortOrder specified in the Properties file 
   * 
   * @param institutionProfileId
   * @param tableName
   *            String : Name of the PickList Table e.g. ACCESSTYPE (non case
   *            sensitive)
   * @param languageId
   *            int : Language ID e.g. 0 for English, 1 for French //
   * @return Collection Output : Collection of String[2] objects ==> String[0] =
   *         Value; String[1] = Desc // : Returns NULL if any problem
   */
  public static Collection<String[]> getPickListValuesAndDesc(int institutionProfileId,String tableName, Locale locala)
  {
      try
      {
          if (institutionProfileId < 0)
          {
              institutionProfileId = getDefaultInstitutionId();
          }
          ResourceBundle pRsBundle = ResourceBundle.getBundle(PICKLIST_PATH+tableName.toUpperCase(),
                  locala);

          Enumeration<String> all = pRsBundle.getKeys();
          
          //Filter based on institutionProfileId          
          List<String> filteredKeys = getKeysForInstitution(all, institutionProfileId);

          TreeMap<Integer, String[]> sorted = new TreeMap<Integer,String[]>();
          String theKey = "";
          String theOrgVal = "";
          String sortOrderStr = "";

          for (int i=0; i < filteredKeys.size(); i++){
              String[] theVal = new String[2];
              Integer sortOrder = null;
              theKey = filteredKeys.get(i);
              theOrgVal = pRsBundle.getString(theKey);

              StringTokenizer keytokenizer = new StringTokenizer(theKey, INSTITUTION_SEPARATOR);
              if(keytokenizer.countTokens() > 1){
                  theKey = keytokenizer.nextToken();
                  if (keytokenizer.hasMoreTokens())
                  {
                      theKey = keytokenizer.nextToken();
                  }
              } 

              StringTokenizer rep = new StringTokenizer(theOrgVal, "|");
              try
              {
                  sortOrderStr = rep.nextToken().trim(); // Get Sort Order
                  sortOrder = new Integer(sortOrderStr);
                  theVal[0] = theKey;
                  theVal[1] = rep.nextToken().trim(); // Get Value
              }
              catch(Exception ex)
              {
                  // Fallback to sort by Key
                  sortOrder = new Integer(theKey);
                  theVal[0] = theKey;
                  theVal[1] = theOrgVal;
              }
              while(sorted.containsKey(sortOrder)) sortOrder = new Integer(sortOrder.intValue()+1);
              sorted.put(sortOrder, theVal);
          }

          return sorted.values();

      }
      catch(Exception e)
      {
        // Got an Exception : Most likely resource properties file not found
        logger.error("Exception @BXResources.getPickListValuesAndDesc : tableName="+tableName
             	+ ";instid="+institutionProfileId+
              " : language=" + locala + " :: " + e);
        // Fallback to return a null string
        return null;
      }
  }
  /**  //===================================================================================
  // Method to get all Values of a particular PickList Table in sorted
  //  by the SortOrder specified in the Properties file
  //
  // Inputs:
* @param tableName String : Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
* @param languageId int : Language ID e.g. 0 for English, 1 for Franch
  //
* @return Collection Output : Collection of String objects ==>  with Values stored
  //         : Returns NULL if any problem
  //
  //===================================================================================
*/
  //#DG600

  public static Collection<String> getPickListValues(int institutionProfileId,String tableName, int languageId)
  {
    return getPickListValues(institutionProfileId,tableName, getLocale(languageId)); 
  }
  
  
  /**
   * Method to get all Values of a particular PickList Table in sorted // by
   * the SortOrder specified in the Properties file //
   * 
   * @param institutionProfileId
   * @param tableName
   *            String : Name of the PickList Table e.g. ACCESSTYPE (non case
   *            sensitive)
   * @param languageId
   *            int : Language ID e.g. 0 for English, 1 for French //
   * @return Collection Output : Collection of String objects ==> with Values
   *         stored // : Returns NULL if any problem
   */
  public static Collection<String> getPickListValues(int institutionProfileId,String tableName, Locale locala)
  {
    try
    {
        if (institutionProfileId < 0)
        {
            institutionProfileId = getDefaultInstitutionId();
        }
      ResourceBundle pRsBundle = ResourceBundle.getBundle(PICKLIST_PATH+tableName.toUpperCase(),
          locala);

      Enumeration<String> all = pRsBundle.getKeys();
      
      //Filter based on institutionProfileId
      List<String> filteredKeys = getKeysForInstitution(all, institutionProfileId);
      
      TreeMap<Integer,String> sorted = new TreeMap<Integer,String>();
      String theKey = "";
      String theOrgVal = "";
      String sortOrderStr = "";

      for (int i=0; i < filteredKeys.size(); i++){
        String theVal = "";
        Integer sortOrder = null;
        theKey = filteredKeys.get(i);
        theOrgVal = pRsBundle.getString(theKey);

        StringTokenizer rep = new StringTokenizer(theOrgVal, "|");
        StringTokenizer keytokenizer = new StringTokenizer(theKey, INSTITUTION_SEPARATOR);
        if(keytokenizer.countTokens() > 1){
            theKey = keytokenizer.nextToken();
            if (keytokenizer.hasMoreTokens())
            {
                theKey = keytokenizer.nextToken();
            }
            
        } 
        try
        {
          sortOrderStr = rep.nextToken().trim(); // Get Sort Order
          sortOrder = new Integer(sortOrderStr);
          theVal = theKey;
        }
        catch(Exception ex)
        {
          // Fallback to sort by Key
          sortOrder = new Integer(theKey);
          theVal = theKey;
        }
        while(sorted.containsKey(sortOrder)) sortOrder = new Integer(sortOrder.intValue()+1);
        sorted.put(sortOrder, theVal);
      }

      return sorted.values();

    }
    catch(Exception e)
    {
      // Got an Exception : Most likely resource properties file not found
      logger.error("Exception @BXResources.getPickListValues : tableName="+tableName
           	+ ";instid="+institutionProfileId+
            " : language=" + locala + " :: " + e);
      // Fallback to return a null string
      return null;
    }
  }

  /**
  //===================================================================================
  // Method to get all Descriptions of a particular PickList Table in sorted
  //  by the SortOrder specified in the Properties file
  //
  // Inputs:
* @param tableName String : Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
* @param languageId int : Language ID e.g. 0 for English, 1 for Franch
  //
* @return Collection Output : Collection of String objects ==>  with Descriptions stored
  //         : Returns NULL if any problem
  //
  //===================================================================================
*/
  //#DG600
  
  public static Collection<String> getPickListDescriptions(int institutionProfileId,String tableName, int languageId)
  {
    return getPickListDescriptions(institutionProfileId,tableName, getLocale(languageId));   
  }
  
  /**
  //===================================================================================
  // Method to get all Descriptions of a particular PickList Table in sorted
  //  by the SortOrder specified in the Properties file
  //
  // Inputs:
   * @param institutionProfileId
* @param tableName String : Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
* @param languageId int : Language ID e.g. 0 for English, 1 for French
  //
* @return Collection Output : Collection of String objects ==>  with Descriptions stored
  //         : Returns NULL if any problem
  //
  //===================================================================================
*/
  public static Collection<String> getPickListDescriptions(int institutionProfileId, String tableName, Locale locala)
  {
      try
      {
          if (institutionProfileId < 0)
          {
              institutionProfileId = getDefaultInstitutionId();
          }
          ResourceBundle pRsBundle = ResourceBundle.getBundle(PICKLIST_PATH+tableName.toUpperCase(),
                  locala);

          Enumeration<String> all = pRsBundle.getKeys();
          
          //Filter based on institutionProfileId
          List<String> filteredKeys = getKeysForInstitution(all, institutionProfileId);
          TreeMap<Integer, String> sorted = new TreeMap<Integer, String>();
          String theKey = "";
          String theOrgVal = "";
          String sortOrderStr = "";

          for (int i=0; i < filteredKeys.size(); i++)
          {
              String theVal = "";
              Integer sortOrder = null;
              theKey = filteredKeys.get(i);
              theOrgVal = pRsBundle.getString(theKey);

              StringTokenizer rep = new StringTokenizer(theOrgVal, "|");
              try
              {
                  sortOrderStr = rep.nextToken().trim(); // Get Sort Order
                  sortOrder = new Integer(sortOrderStr);
                  theVal = rep.nextToken().trim();
              }
              catch(Exception ex)
              {
                  // Fallback to sort by Key
                  StringTokenizer keytokenizer = new StringTokenizer(theKey, INSTITUTION_SEPARATOR);
                  if(keytokenizer.countTokens() > 1){
                      theKey = keytokenizer.nextToken();
                      if (keytokenizer.hasMoreTokens())
                      {
                          theKey = keytokenizer.nextToken();
                      }
                  }
                  sortOrder = new Integer(theKey);
                  theVal = theOrgVal;
              }
              while(sorted.containsKey(sortOrder)) sortOrder = new Integer(sortOrder.intValue()+1);
              sorted.put(sortOrder, theVal);
          }

          return sorted.values();

      }
      catch(Exception e)
      {
        // Got an Exception : Most likely resource properties file not found
        logger.error("Exception @BXResources.getPickListDescriptions : tableName="+tableName+
             	 ";instid="+institutionProfileId+
              " : language=" + locala + " :: " + e);
        // Fallback to return a null string
        return null;
      }
  }

  /**
  //===================================================================================
  // Method to get size a particular PickList Table
  //
  // Inputs:
* @param tableName String : Name of the PickList Table e.g. ACCESSTYPE (non case sensitive)
* @param languageId int : Language ID e.g. 0 for English, 1 for Franch
  //
* @return int Output : int that count all entries in the table
  //         : Returns NULL if any problem
  //
  //===================================================================================
*/
  //#DG600
    
  public static int getPickListTableSize(int institutionProfileId, String tableName, int languageId)
  {  
      return getPickListTableSize(institutionProfileId,tableName, getLocale(languageId));  
  }
  

  
  /**
   * Method to get size a particular PickList Table
   * 
   * @param institutionProfileId
   * @param tableName
   *            String : Name of the PickList Table e.g. ACCESSTYPE (non case
   *            sensitive)
   * @param languageId
   *            int : Language ID e.g. 0 for English, 1 for French 
   * @return int Output : int that count all entries in the table. 
   *            Returns NULL if any problem
   */
  public static int getPickListTableSize(int institutionProfileId, String tableName, Locale locala)
  {
    int count = 0;

    try
    {
        if (institutionProfileId < 0)
        {
            institutionProfileId = getDefaultInstitutionId();
        }
      ResourceBundle pRsBundle = ResourceBundle.getBundle(PICKLIST_PATH+tableName.toUpperCase(),
          locala);

      Enumeration<String> all = pRsBundle.getKeys();
      //Filter based on institutionProfileId
      List<String> filteredKeys = getKeysForInstitution(all, institutionProfileId);

      count = filteredKeys.size();

      return count;

    }
    catch(Exception e)
    {
      // Got an Exception : Most likely resource properties file not found
      logger.error("Exception @BXResources.getPickListTabelSize : tableName="+tableName+
         	 ";instid="+institutionProfileId+
            " : languageId=" + locala + " :: " + e);
      // Fallback to return a null string
      return count; // default 0.
    }
  }

  //==================== End PickList methods ========================================================

  //========================================================================================
  // Methods for Getting System Messages from SystemMessage Resource Bundle Properties file
  //========================================================================================
  /**
   * @param key String
   * @param languageId int
   * @return String
   */
  public static String getSysMsg(String key, int languageId)
  {
    return getSysMsg(key, getLocale(languageId));   //#DG600
  }

  /** #DG600
   * 
   * @param keya the key to retrieve message for
   * @param locala the locale to be used
   * @return
   */
    public static String getSysMsg(String keya, Locale locala) {
    try
    {
      ResourceBundle pRsBundle = ResourceBundle.getBundle(SYS_MSG_PATH, locala);
      return pRsBundle.getString(keya);
    }
    catch(Exception e)
    {
      // Got an Exception : Most likely resource properties file not found
      logger.error("Exception @BXResources.getSysMsg :: " + e+ " : languageId=" + locala ); 
      // Fallback to return thew key string
      return keya;
    }
    }
  
    // 4.3GR, Update Total Loan Amount -- start
    /**
     * returns message with parameters applied.
     * 
     * @see http://java.sun.com/j2se/1.5.0/docs/api/java/text/MessageFormat.html
     */
    public static String getSysMsg(String key, int languageId, Object... args) {
	
	/*
	  This method utilizes java.text.MessageFormat.
	  Single quote is used as an escaping character in this class, however 
	  since Express uses a lot of it especially in French resource file, 
	  there is a demand displaying single quote as they are written in resource file. 
	  Therefore, before feeding resource to MessageFormat class, this method 
	  will first mask single quotes with control character NUL, and then 
	  use [ ~ ] as a substitution of single quote.
	*/
        String msg = getSysMsg(key, languageId);
        if(args == null || args.length == 0 || msg == null) return msg;
	
	msg = msg.replace("'", "\u0000"); //control char NUL
	msg = msg.replace("~", "'");
	
	try {
            MessageFormat mf = new MessageFormat(msg, getLocale(languageId));
            msg = mf.format(args);
	    msg = msg.replace("'", "~");
	    msg = msg.replace("\u0000", "'");
	    return msg;
        } catch (IllegalArgumentException e) {
            logger.error(" : key=" + key + ", + languageId=" + languageId, e);
            return msg;
        }
    }

    //4.3GR, Update Total Loan Amount -- end
    
  //==================== End System Message methods ==============================================

  /**
   * returns the i18n message for rich client.
   */
    //#DG600
  public static String getRichClientMsg(String key, int languageId) {

    return getRichClientMsg(key, getLocale(languageId));    
  }
  public static String getRichClientMsg(String key, Locale locala) {

      try {
          ResourceBundle rb =
              ResourceBundle.getBundle(RICH_CLIENT_MSG_PATH, locala);   //#DG600
          return rb.getString(key);
      } catch (Exception e) {
          logger.error(e+ " : languageId=" + locala );
          return getSysMsg(key, locala);
      }
  }

  /**
   * returns the i18n message for rich client.
   */
  public static String getRichClientMsg(String key, int languageId,
                                        Object[] params) {

      String msg = getRichClientMsg(key, languageId);
      try {
          return MessageFormat.format(msg, params);
      } catch (IllegalArgumentException e) {
          logger.error(e.toString()+ " : languageId=" + languageId );
          return msg;
      }
  }

  //========================================================================================
  // Methods get the translated string from Generic Translation Resource Bundle Properties file
  //========================================================================================
  //#DG600 rewritten
  /**
   * @param key String
   * @param languageId int
   * @return String
   */
  public static String getGenericMsg(String key, int languageId)
  {
    return getGenericMsg(key, getLocale(languageId));   //#DG600
  }

  //#DG600
  /**
   * @param key String
   * @param languageId Locale
   * @return String
   */
  public static String getGenericMsg(String key, Locale locala)
  {
    try
    {
        ResourceBundle pRsBundle = ResourceBundle.getBundle(BXGENERIC_PATH,locala);         
        return pRsBundle.getString(key);
        
    }
    catch(Exception e)
    {
        // Got an Exception : Most likely resource properties file not found
        logger.error("Exception @BXResources.getGenericMsg :: " + e.toString()+ " : languageId=" + locala ); 
        // Fallback to return a null string
        return key;
    }
  }
  
  //========================================================================================
  // Methods get the translated of the DataField Descriptions
  //========================================================================================
  /**
   * @param key String
   * @param languageId int
   * @return String
   */
  //#DG600
  public static String getDBFieldDescription(String key, int languageId)
  {
    return getDBFieldDescription(key, getLocale(languageId));   
  }
  public static String getDBFieldDescription(String key, Locale locala)
  {
    try
    {
      ResourceBundle pRsBundle = ResourceBundle.getBundle(BXDBFIELD_PATH, 
            locala);    //#DG600

      return pRsBundle.getString(key);

    }
    catch(Exception e)
    {
      // Got an Exception : Most likely resource properties file not found
      logger.error("Exception @BXResources.getDBFieldDescription :: " + e.toString()+ " : languageId=" + locala ); 
      // Fallback to return a null string
      return key;
    }
  }

  /**
   *
   * @param languageId int
   * @return String
   */
  public static String getLocaleCode(int languageId)
  {
	  //#DG862
	  Locale locala = getLocale(languageId);
      return locala.getLanguage();
   }

   /**
   //// This method massages the defauld JATO URL based on the
  //// user language preference.
*
* @param defaultURL String
* @param languageId int
* @return String
*/
  public static String getBXUrl(String defaultURL, int languageId)
  {
      //String languageCode = Locale.ENGLISH.getLanguage();  //#DG862 Default to English
      String BXUrl = "";

    try
    {
      if(languageId == Mc.LANGUAGE_PREFERENCE_FRENCH)
      {
        if(defaultURL != null && !defaultURL.trim().equals("") && !defaultURL.trim().equals("/"))
        {
           String temp = defaultURL.substring(0, defaultURL.length() - 4);
           ////logger.debug("BXResources@getBXUrl::BaseUrl: " + temp);
           BXUrl = temp + UNDERSCORE + Locale.FRENCH.getLanguage() + JSP_EXTENTION;	//#DG862
        }
      }
      //--> Bug fixed by Billy 14Jan2004
      //--> Should default to English if the language not defined
      //else if(languageId == Mc.LANGUAGE_PREFERENCE_ENGLISH)
      //=========================================================
      else
      {
        BXUrl = defaultURL; // do nothing, default system language.
      }

      return BXUrl;
    }
    catch(Exception e)
    {
      // Got an Exception : For some reasons the defaultURL had not been initialized.
      logger.error("Exception @BXResources.getSysMsg :: " + e.toString()+ " : languageId=" + languageId ); 
      // Fallback to return the non-initialize string and manipulate with it in the ViewBean.
      return defaultURL;
    }
  }


  //==================== End Generic Message methods ==============================================

  //====================== For DocPrep and Ingestion ==============================================
  //#DG600 rewritten
  /**
     * @param key String
     * @param languageId int
     * @return String
     */
    public static String getDocPrepIngMsg(String key, int languageId) {
        return getDocPrepIngMsg(key, getLocale(languageId));
    }

    // ====================================================================================

    /**
     * 
     * @param key
     * @param locala
     * @return
     */
    public static String getDocPrepIngMsg(String key, Locale locala) {
        try {
            ResourceBundle pRsBundle = ResourceBundle.getBundle(
                    DOCPREP_INGESTION_PATH, locala);

            return pRsBundle.getString(key);
        } catch (Exception e) {
            // Got an Exception : Most likely resource properties file not found
            logger.error("Exception @BXResources.getDocPrepIngMsg :: " + e+ " : languageId=" + locala );  
            // Fallback to return a null string
            return key;
        }
    }
  //#DG600 end 

    /**
     * <p>
     * returns routingOrder within the target institutionProfile.
     * this method uses BXResources.SysConfig.ROUTINGORDER.properties.
     * the format of the properties is the following.
     * <br>
     * A.B = C | DDDDD
     * 
     * (A: institutionProfileId)
     * (B: order of the method, this is different from other properites)
     * (C: routing method id)
     * (D: description. this is only for human)
     * 
     * Feb 25, 2009: FXP24419, institutionalise 
     *  + minor bug fix (old ver. didn't actually care 'B' column) 
     * 
     * </p>
     * @param institutionProfileId : institutionProfileid
     * @param orderType : target order property file
     * @return list of routing method ids with correct order
     * 
     * @since 4.1GR
     * @author hiro
     */
    public static ArrayList<Integer> getSysConfigOrderList(
    		int institutionProfileId, String orderType) {

    	if (institutionProfileId < 0) {
    		institutionProfileId = getDefaultInstitutionId();
    	}

    	String prefix = institutionProfileId + INSTITUTION_SEPARATOR;
    	ResourceBundle rb = ResourceBundle.getBundle(SYSCONFIG_PATH + orderType);
    	Enumeration<String> enumList = rb.getKeys();
    	
    	//Format: [A.B = C | DDDDDD], 
    	// collect data 'A' which is the same as institutionProfileId.
    	ArrayList<String> orderedKeyList = new ArrayList<String>();
    	while (enumList.hasMoreElements()) {
    		String key = enumList.nextElement().toString();
    		if (key == null || key.length() < 1) continue;
    		if (key.startsWith(prefix) == false) continue;
    		if (key.trim().length() <= prefix.length()) continue;
    		//now, the key matches and valid
    		orderedKeyList.add(key.trim());
    	}
    	
    	//when no data found
    	if (orderedKeyList.isEmpty()){
    		MissingResourceException ex = new MissingResourceException(
    		(new StringBuilder())
    		.append("Can't find any entry that stats with '").append(prefix)
    		.append("' in ").append(SYSCONFIG_PATH + orderType)
    		.toString(), BXResources.class.getName(), prefix);
    		logger.error(ex);
    		throw ex;
    	}
    	
    	//Format: [A.B = C | DDDDDD], sorts data using 'B' as Integer
    	Collections.sort(orderedKeyList, new Comparator<String>(){ 
    		public int compare(String o1, String o2) {
    		Integer i1 = new Integer(o1.substring(
    				o1.indexOf(INSTITUTION_SEPARATOR) + 1).trim());
    		Integer i2 = new Integer(o2.substring(
    				o2.indexOf(INSTITUTION_SEPARATOR) + 1).trim());
    		return i1.compareTo(i2);
    	}});

    	//Format: [A.B = C | DDDDDD], collects 'C' with correct order
    	ArrayList<Integer> orderList = new ArrayList<Integer>();
    	for(Iterator<String> i = orderedKeyList.iterator();i.hasNext();){
    		String value = rb.getString(i.next());
    		int sepPosition = value.indexOf(VALUE_DESC_SEPARATOR);
    		if (sepPosition > 0) {
    			// expecting the value is like "12 | Source Firm to Group"
    			// get only number from the string
    			orderList.add(new Integer(value.substring(0, sepPosition).trim()));
    		} else {
    			// expecting the value is just like "2"
    			orderList.add(new Integer(value.trim()));
    		}
    	}
    	return orderList;
    }

    // SEAN Routing Order Changes Split system config properties files from
    // picklist package to sysconfig package.
    
    /**
     * returns the value for the specified key from the config file indicated by
     * the given type.
     * 
     * <p>FXP24419, 4.1GR, Feb 24, 2009 - adding capability of handling institution.</p>
     */
    public static String getSysConfigValue(int institutionProfileId,
			String type, String key, String defaultValue) {

		if (institutionProfileId < 0) {
			institutionProfileId = getDefaultInstitutionId();
		}

		String value = defaultValue;
		String instPlusKey = institutionProfileId + INSTITUTION_SEPARATOR + key;

		try {
			ResourceBundle rb = ResourceBundle.getBundle(SYSCONFIG_PATH + type);
			value = rb.getString(instPlusKey);
		} catch (MissingResourceException e) {
			logger.error("the key: " + instPlusKey + " doesn't exist in "
					+ SYSCONFIG_PATH + type);
			logger.error(e);
			throw e;
		}
		if(value == null)
			value = defaultValue;
		return value;
	}
    
    // SEAN Routing Order Changes END

    // #DG600
  // #DG572 return default value for this table
    public static String getDefaultValue(String tableName, int languageId) throws StringIndexOutOfBoundsException {
        return getDefaultValue(tableName, getLocale(languageId));   
    }

    public static final Entry<Integer, Locale>[] getLocales() {
        return locales;
    }
    
    /** 
     * 
     * @param languageId xpres language id
     * @return Locale return the Locale for the given xpres language id
     */
    public static Locale getLocale(int languageId) {
		//String loCode = getLocaleCode(languageId);		
		Entry<Integer, Locale>[] locales = BXResources.getLocales();
		for (Entry<Integer, Locale> intLoc : locales)
			if (intLoc.getKey() == languageId)
				return intLoc.getValue();

		logger.error(StringUtil.stack2string(new Exception("getLocale: unknown language id:" + languageId)));
		return Locale.ENGLISH; // should never happen - last resort default
    }

    /**
     * 
     * @param locala Locale to retrieve xpres language id for
     * @return xpres language id 
     * @throws IndexOutOfBoundsException if there's no language id for this locale
     */
    public static int getLangId(Locale locala) {
        List<String> langIds = PicklistData.getValueList(PKL_LANGUAGE_PREFERENCE, LANGUAGEPREFERENCEID);
        for (String aLang : langIds) {
            int languageId = Integer.parseInt(aLang);
            Locale localb = getLocale(languageId);          
            if(locala == localb)
                return languageId;
        }
        logger.warn("PickList missing data locale: "+locala);
        return Mc.LANGUAGE_UNKNOW;
    }
    
    /**
     *  
     * @param tableName resources table name
     * @param locala locale of the chosen language
     * @return the value of the key with lowest sort order
     */
    public static String getDefaultValue(String tableName, Locale locala) {
        int lowestKey = Integer.MAX_VALUE;
        String id = "0";
        ResourceBundle pRsBundle = ResourceBundle.getBundle(PICKLIST_PATH+tableName.toUpperCase(),locala);
    for (Enumeration e = pRsBundle.getKeys(); e.hasMoreElements(); ) {
            String keya = e.nextElement().toString();           
        String theVal = pRsBundle.getString(keya);
        int cur = theVal.indexOf('|'); 
        if (cur < 0) 
            throw new StringIndexOutOfBoundsException(cur);//"@BXResources.getPickListDescription :: SortOrder not set ==> Fallback to return the original value");
        theVal = theVal.substring(0, cur).trim();
            cur = Integer.parseInt(theVal);
        if (lowestKey > cur) {
            lowestKey = cur;
            id = keya;
        }
    }
        return pRsBundle.getString(id);
    }

    
     //#DG600 create a bunch of helper formattable internal classes to facilitate localization
     /*
      * since languageId can be negative (unknown language) then using it to
      * index an array will not do. Instead let's use the more generic Entry
      * structure. 
      */        
     private static Entry<Integer, Locale>[] buildLocales (){
         final String[][] langIds = PicklistData.getTableValues(PKL_LANGUAGE_PREFERENCE);       
         class entryMap implements Entry<Integer, Locale> {
             int keysa;
             Locale valuesa;
             public entryMap(int languageId, Locale localb) {
                 keysa = languageId;
                 valuesa = localb;
             }
             public Integer getKey() {
                 return keysa;
             }
             public Locale getValue() {
                 return valuesa;
             }
             public Locale setValue(Locale value) {
                 return valuesa = value;
             }
         }
         List<entryMap> entries = new ArrayList<entryMap>();

         for (String[] cols : langIds) {
             int languageId = Integer.parseInt(cols[0]);
             if(languageId == LANGUAGE_UNKNOW)
                 continue;
             //Locale localb = getLocale(languageId);
             String localeLang = cols[2];
             String localeCountry = cols[3];
             if (localeCountry == null || "".equals(localeCountry.trim())) {
                 // 4.3GR, Update Total Loan Amount
                 // default: Canada
                 localeCountry = "CA";
             }
             String localeVar = cols[4];
             if(localeVar == null)
                 localeVar = "";
             Locale localb = new Locale(localeLang, localeCountry, localeVar);
             entries.add(new entryMap(languageId,localb));
         }

         return entries.toArray(new entryMap[0]);
     }

      /*      
        static DocPrepLanguage dpl = DocPrepLanguage.getInstance();
        public static int getLangId(Locale locala) {
            int langId = Mc.LANGUAGE_PREFERENCE_ENGLISH;
            if (locala == Locale.FRENCH)
                langId = Mc.LANGUAGE_PREFERENCE_FRENCH;
            //else if (locala == Locale.SPANISH)
                //langId = Mc.LANGUAGE_PREFERENCE_SPANISH;
            return langId;
      }*/
     
      //create a bunch of helper formattable internal classes to facilitate localization
    
        /**  
         * create a Formattable that outputs the list of items localizing each
         * using given resources file 
         * 
         * @param keya localization key
         * @param pars list of items
         * @return the Formattable that converts the items
         */
         public static Formattable newResFormata(final String resFileName, final String keya, final Object ... pars) {
             return new Formattable() {
                 public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                     ResourceBundle pRsBundle = ResourceBundle.getBundle(resFileName, fmt.locale());        
                     final String fmtTemplate = pRsBundle.getString(keya);
                     fmt.format(fmtTemplate, pars);
                 }
                    //@Override #DG634 just for logging purpose ...
                    public String toString() {
                        return "[newResFormata:resFileName:"+resFileName+";keya="+keya+";pars="+pars+']';
                    }
             };
         } 

        /**
         * create a Formattable that outputs the list of items localizing each one
         * using the DOCPREP_INGESTION_PATH resources file as the default 
         * - convenience method
         * 
         * @param keya localization key
         * @param pars list of items
         * @return the Formattable that converts the items
         */
        public static Formattable newFormata(final String keya, final Object ... pars) {
            return new Formattable() {
                public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                    Formattable formata = newDPIGMsg(keya, pars);
                    formata.formatTo(fmt, flagas, width, precision);
                }
                //@Override #DG634 just for logging purpose ...
                public String toString() {
                    return "[newFormata:keya="+keya+";pars="+pars+']';
                }
            };
        }

        /**
         * create a Formattable that outputs the list of items localizing each one
         * using the DOCPREP_INGESTION_PATH resources file  
         * - convenience method
         * 
         * @param keya localization key
         * @param pars list of items
         * @return the Formattable that converts the items
         */
        public static Formattable newDPIGMsg(final String keya, final Object ... pars) {
        return new Formattable() {
            public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                    final String fmtTemplate = getDocPrepIngMsg(keya, fmt.locale());        
                    fmt.format(fmtTemplate, pars);
            }
                //@Override #DG634 just for logging purpose ...
                public String toString() {
                    return "[newDPIGMsg:keya="+keya+";pars="+pars+']';
                }
        };
      }
    
      /**
       * create a Formattable that outputs the item localized 
       * using the BXGENERIC_PATH resources file
       * - convenience method
       * 
       * @param keya localization key
       * @param pars list of items
       * @return the Formattable that converts the items
       */
        public static Formattable newGenMsg(final String keya, final Object ... pars) {
            return new Formattable() {
                public void formatTo(Formatter fmt, int flagas, int width, int precision) {                 
                    final String fmtTemplate = getGenericMsg(keya, fmt.locale());       
                    fmt.format(fmtTemplate, pars);
                }
                //@Override #DG634 just for logging purpose ...
                public String toString() {
                    return "[newGenMsg:keya="+keya+";pars="+pars+']';
                }
            };
        }
    
    /**
     * create a Formattable that outputs the picklist item
     * 
     * @param table picklist table name
     * @param id picklist table name key id
     * @return the Formattable that converts the item
     */
    public static Formattable newPkList(final int institutionId, final String table, final String id) {
        return new Formattable() {
            public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                try {
                    fmt.out().append(getPickListDescription(institutionId, table,  id, fmt.locale()));
                } 
                catch (IOException e) {
                    logger.error("newPkList:"+getClass(), e);	//#DG862
                }               
            }
            //@Override #DG634 just for logging purpose ...
            public String toString() {
                return "[newPkList:table="+table+";id="+id+']';
            }
        };
    }

    /**
     * create a Formattable that outputs the picklist item
     * - convenience method
     * 
     * @param table picklist table name
     * @param id picklist table name key id
     * @return the Formattable that converts the item
     */
    public static Formattable newPkList(final int institutionId, final String table, final int id) {
        return new Formattable() {
            public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                Formattable formata = newPkList(institutionId, table, Integer.toString(id));
                formata.formatTo(fmt, flagas, width, precision);
            }
            //@Override #DG634 just for logging purpose ...
            public String toString() {
                return "[newPkList:table="+table+";id="+id+']';
            }
        };
    }
    
    /**
     * create a Formattable that outputs literals items that don't require 
     * localization and are output as is
     * @param objs list of items to output
     * @return the Formattable that converts the items
     */
    public static Formattable newStrFormata(final Object ...objs) {
        return new Formattable() {
            public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                for (Object obja : objs) {
                    fmt.format("%s", obja);
                }
            }
            //@Override #DG634 just for logging purpose ...
            public String toString() {
                return "[newStrFormata:objs="+objs+']';
            }
        };
    }

    /**
     * create a Formattable that outputs term as years and months
     * @param keya localization key
     * @param term the input term
     * @return the Formattable that converts the term
     */
    public static Formattable newYearsMonths(final String keya, final int term) {
        return new Formattable() {
            public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                // 'YEAR_MONTH_FORMAT' can be used as the default for 'keya'
                String dtForm = BXResources.getDocPrepIngMsg(keya,fmt.locale());
            int yeara = term/12;
            int montha = term - (yeara * 12);
                fmt.format(dtForm, yeara, montha);
            }
            //@Override #DG634 just for logging purpose ...
            public String toString() {
                return "[newYearsMonths:keya:"+keya+";term="+term+']';
            }
        };
    }
        
    /**
     * create a Formattable that outputs term as years and months using default format
     * - convenience method
     * 
     * @param term the input term
     * @return the Formattable that converts the term
     */
    public static Formattable newYearsMonths(final int term) {
        return new Formattable() {
            public void formatTo(Formatter fmt, int flagas, int width, int precision) {
                Formattable formata = newYearsMonths(YEAR_MONTH_FORMAT, term);
                formata.formatTo(fmt, flagas, width, precision);
            }
            //@Override #DG634 just for logging purpose ...
            public String toString() {
                return "[newYearsMonths:key:"+YEAR_MONTH_FORMAT+";term="+term+']';
            }
        };
    }
    
    //#DG600 end 
    /**
     * method to check if the key is present in the Enumeration
     * @param enumResource
     * @param key
     */
    public static boolean isKeyPresent(Enumeration<String> enumResource, String key)
    {
        String keyValue;
        while(enumResource.hasMoreElements())
        {
            keyValue = enumResource.nextElement().toString();
            if (keyValue.equals(key))
                return true;
        }
                
        return false;
    }
    
    /**
     * Filters keys enumeration based on the institution profile Id. 
     * @param enumList
     * @param institutionProfileId
     * @return keys for institution or keys with no institution if none 
     * can be found for institution
     */
    public static List<String> getKeysForInstitution(Enumeration<String> enumList, int institutionProfileId)
    {
        if (institutionProfileId < 0)
        {
            institutionProfileId = getDefaultInstitutionId();
        }
        List<String> keysForInst = new ArrayList<String>();
        List<String> keysWithoutInst = new ArrayList<String>();
        String keyValue;
        while(enumList.hasMoreElements())
        {
            keyValue = enumList.nextElement().toString();
            
            //Check if intituionalized entry is present
            if (keyValue.indexOf(institutionProfileId + INSTITUTION_SEPARATOR) >= 0
                            && keyValue.trim().length() > (institutionProfileId + INSTITUTION_SEPARATOR).length()){
                
                keysForInst.add(keyValue);
            }        
            else if (keyValue.indexOf(INSTITUTION_SEPARATOR) < 0){
                //No: Add it to a list with default entries without institions if
                //there is no institutionProfileId
                keysWithoutInst.add(keyValue);
            }
        }
        
        if (keysForInst.size() == 0)
        {
            //return list of keys without institution
            return keysWithoutInst;
        }
        
        return keysForInst;
    }

    /**
     * 
     * @return lowest intitutionProfileId
     */
    public static int getDefaultInstitutionId()
    {
        if (_defaultInstitutionId < 0) {

            try {
                SessionResourceKit srk = new SessionResourceKit("Cache", true);
                InstitutionProfile insProfile = new InstitutionProfile(srk);
                _defaultInstitutionId = insProfile.findLowestId();
                logger.info("Set default institution profile id to: " +
                            _defaultInstitutionId);
                srk.freeResources();
            } catch(Exception e) {
                // if anything wrong, we go with 0.
                _defaultInstitutionId = 0;
            }
        }

        return _defaultInstitutionId;
    }
    
    /**
     * get a map of all institutionIds and names
     * @return hashmap of or all institution names with id as the key
     */
    public static HashMap<Integer, String> getAllInstitutions()
    {
        if (_institutions == null)
        {
            SessionResourceKit srk = new SessionResourceKit("Cache", true);
            try {
                InstitutionProfile insProfile = new InstitutionProfile(srk);
                _institutions = insProfile.getInstitutionNames();
                logger.info("got all institutions: " +
                        _institutions);
            } catch(Exception e) {
                // if anything wrong, we go with null.
                _institutions = null;
            } finally {
                srk.freeResources();
            }
        }
        return _institutions;
    }
    
    /**
     * get the name of institution based on the id. Reads from the static map
     * @param institutionProfileId
     * @return institution name
     */
    public static String getInstitutionName(int institutionProfileId)
    {
        String institutionName = "";
        HashMap<Integer, String> instMap = getAllInstitutions();
        if (instMap != null)
        {
            institutionName = instMap.get(new Integer(institutionProfileId));
        }
        
        return institutionName;        
    }

 
}
