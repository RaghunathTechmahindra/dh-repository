package com.basis100.picklist;

/**
 * 22.Apr.2010 DVG #DG928 LEN449378: Paradigm Express - Business rule DEA-888 firing on deals
 * 26.Jun.2009 DVG #DG862 LEN380950: 	Ruth Kiaupa DE-INIT business rule issue
 * 04/Apr/2008 DVG #DG712 FXP21127: td prod - interest compound different between deal and mtg prod tables
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 * 01/Nov/2006 DVG #DG536 IBC#1511  Intermittent Deal Ingestion Trouble
 */

import java.io.*;
import java.sql.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.*;

import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.*;
import com.filogix.util.Xc;

/**
 *  Provides an interface to the picklist data held in the picklist.ini file.
 *  The picklist.ini is arranged such that section = table followed by
 *  one or more  description = id  key,value pairs. <B>For example:</B>
 *  <pre>
 * [ASSETTYPE]
 * PENSION = 1
 * SAVINGS = 2
 * GIFT = 3
 * </pre>
 *
 */

public class PicklistData implements Xc
{
	//private final static String tablesFile = "picklisttables.txt";
	//private final static String PickListXmlFile = "picklistData.xml";
	private static String tablesFile = "com/basis100/picklist/picklistTables.txt";

	static Log loger = LogFactory.getLog(PicklistData.class);
	static boolean synchMode4Rebuild = true; // #DG928 revamp synching

  	// list of original table names in upper case tables in order
	//		as tables get actually loaded they get added to this list 
	//			with inst.id suffixed with it
  	//		eg. ...,CREDITREFTYPE, CROSSSELLPROFILE, CROSSSELLPROFILE.0, CROSSSELLPROFILE.1, ...
	private static String[] tableNames;		//#DG928   	
  	private static ArrayList<String> tableNamesTmp = null;	//#DG928 temp list -same as above -will be kept as array not list 		  	
  	private static Map<String, String> 
  		tableIdDescrMap, //maps a table_id key to description value
  		tableDescrIdMap; //maps a table_description to id value
  	private static Map<String, Node> 
  		tableDataMap,  //holds table node with table name as key
  		tableRowViewMap,  //holds rowView node for table name key
  		tableColumnViewMap;  //holds columnView node for table name key
  	//========================================================

	public static String SCHEMA = "MOS";

	// data types (we map JDBC types to these internal types)
	private static final int STRING = 1;
	private static final int INTEGER = 2;
	private static final int DECIMAL = 3;
	private static final int TIMESTAMP = 4;
	private static final int OTHER = 5;

	public static final String INSTITUITIONPROFID = "INSTITUTIONPROFILEID";
	private static final String VALUE_ATR = "value";
	private static final String ROW_NODE = "Row";
	private static final String DESCRIPTIONA = "DESCRIPTION";
	private static final String PICKLIST_TABLE_VAL = "picklistTable";
	private static final String TYPE_ATR = "type";
	private static final String PKFIELD_ATR = "pkfield";
	private static final String DESCRIPTION_ATR = "description";
	private static final String ID_ATR = "id";
	private static final String ROWS_ATR = "rows";
	private static final String COLUMNS_ATR = "columns";
	private static final String DESC_FIELD_ATR = "descField";
	private static final String DATA_TYPE_ATR = "dataType";
	private static final String JDBC_TYPE_ATR = "jdbcType";
	private static final String INDEX_ATR = "index";
	private static final String ROW_VIEW_NODE = "RowView";
	private static final String COLUMN_VIEW_NODE = "ColumnView";
	private static final String PICKLIST = "Picklist";

	// static private SessionResourceKit srk;
	private static Document doc;  	
     
	static	{
		final PropertiesCache pc = PropertiesCache.getInstance();
		//String loadSource = pc.getInstanceProperty(COM_BASIS100_PICKLIST_SOURCE, "DB");
		//String resourcPath = pc.getInstanceProperty(COM_BASIS100_RESOURCE_INIT);

		//loger.debug("Picklist: initializing from " + loadSource);
		//String xmlFile = resourcPath + PickListXmlFile;

		//String pickListTablesTxt = resourcPath + tablesFile;
		// load list of original tables names into array
		tableNamesTmp = new ArrayList<String>(200);
		//readTableList(pickListTablesTxt);
		readTableList(tablesFile);
		tableNames = new String[tableNamesTmp.size()];
		tableNamesTmp.toArray(tableNames);
	}
	// after it became static there's no more need for this method
	//	kept just so as not 2 change its many calls 
	public static void init() {
	}
  
	// no more need for the 'forceInit' parameter
	//	kept just so as not 2 change its single call from workflow 
 	public static void init(boolean forceInit) {
 	}
 	
 	//now it can update a single table as needed
 	// synchronized: it works the same but let's just make explicit  
 	//"If we declare a static method as synchronized, then the lock is obtained on the corresponding Class object."
 	private static void loadIt(String tableName) {
 		synchMode4Rebuild = true;
		synchronized (PicklistData.class) { 		
	 		synchMode4Rebuild = true;
	 		loadItSynch(tableName);
	 		synchMode4Rebuild = false;
		}
 	}
 	private static void loadItSynch(String tableName) {
  	
		loger.trace("table re/load start="+tableName);
		String statMsg = "start";
		SessionResourceKit srk = new SessionResourceKit("PkList");
		
		try {
			statMsg = "Failed to get Picklist builder.";
			initVars();			
			
			cacheFromDb(srk, tableName);

			loger.trace("table load finished="+tableName);

			//#DG928 optimize search -keep as array
			int i = tableNamesTmp.size();
			if(i > tableNames.length)	{
			  	tableNames = new String[tableNamesTmp.size()];
			  	tableNamesTmp.toArray(tableNames);
			  	
				/*String resourcPath = PropertiesCache.getInstance().getInstanceProperty(COM_BASIS100_RESOURCE_INIT);
				statMsg = "Picklist:  Failed at properties get.";
				String xmlFile = resourcPath + PickListXmlFile;
				PrintWriter wr = new PrintWriter(new FileOutputStream(xmlFile));
				XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();
				writer.print(wr,doc);
				wr.close();*/
			}
		  	//tableNamesTmp.clear();		//minimize its memory 
		  	//tableDataMap = Collections.unmodifiableMap(tableDataMap);
			//tableDataMapSiz = tableDataMap.size();
			//tableIdDescrMap = Collections.unmodifiableMap(tableIdDescrMap);
			//tableIdDescrMapSiz = tableIdDescrMap.size();
			//tableDescrIdMap = Collections.unmodifiableMap(tableDescrIdMap);
			//tableDescrIdMapSiz = tableDescrIdMap.size();
			//tableRowViewMap = Collections.unmodifiableMap(tableRowViewMap);
			//tableRowViewMapSiz = tableRowViewMap.size();
			//tableColumnViewMap = Collections.unmodifiableMap(tableColumnViewMap);
			//tableColumnViewMapSiz = tableColumnViewMap.size();
			/*loger.debug("tableDataMap size= "+ tableDataMap.entrySet().size());
			loger.debug("tableIdDescriptionMap size= "+ tableIdDescrMap.entrySet().size());
			loger.debug("tableDescriptionIdMap size= "+ tableDescrIdMap.entrySet().size());
			loger.debug("tableRowViewMap size= "+ tableRowViewMap.entrySet().size());
			loger.debug("tableColumnViewMap size= "+ tableColumnViewMap.entrySet().size());*/
		}
		catch (Exception e) {
			loger.error("Failed to loadTable PicklistData: " + statMsg+':'+e);
		}

		if(srk != null)
			srk.freeResources();
	}
 	
   static void initVars()	throws Exception    {
   
		if(doc == null)
			doc = XmlToolKit.getInstance().newDocument();
		
		if(tableDataMap == null)
			tableDataMap = new PiklMap<String, Node>(150);				
		if(tableIdDescrMap == null)
			tableIdDescrMap = new PiklMap<String, String>(3000);
		if(tableDescrIdMap == null)
			tableDescrIdMap = new PiklMap<String, String>(2000);
		if(tableRowViewMap == null)
			tableRowViewMap = new PiklMap<String, Node>(200);
		if(tableColumnViewMap == null)
			tableColumnViewMap = new PiklMap<String, Node>(200);
		
   //=======================================================
		//#DG712 if(dbMeta == null)
		//dbMeta = srk.getConnection().getMetaData();			
   } 

   //#DG928 load original table list from file and sort it
   static private void readTableList(String txtFileName)
   {
     try
     {
        //BufferedReader reader = new BufferedReader(new FileReader(txtFileName));
     	 // changed for 4.2GR
     	 // changed to get picklist table from classpath.
     	 // since this is static method, use SystemClassLoader
         BufferedReader reader = new BufferedReader(new InputStreamReader(
                     PicklistData.class.getClassLoader().getResourceAsStream(
                             txtFileName)));
        String tableName = null;
        loger.debug("read tables file." + txtFileName);
        while((tableName = reader.readLine())!= null)
        {
       	 //#DG712
          if(tableName.length() <= 0)
         	 continue;
          tableName = tableName.toUpperCase();
          addTable(tableName);
        }
     }
     catch(Exception e)
     {
       loger.error("readTableList: Error reading table list:"+e);
     }
   }

	/** add table to lists of tables if not already there
	 * 		- it's an ordered list!
	 * 
	 * @param tableName
	 */
	static private void addTable(String tableName) {
		//tableName = tableName.toUpperCase();
		int i;
		for (i = 0; i < tableNamesTmp.size(); i++) {

			String aTableName = tableNamesTmp.get(i);	  		 
			int res = aTableName.compareTo(tableName);
			if(res == 0)	// match
				return;	// tableName;
			if(res > 0)	// insert
	  			break;
		 }		
		tableNamesTmp.add(i, tableName);
		//return tableName;
	}

   static private Document cacheFromDb(SessionResourceKit srk, String tableName) 
   	throws Exception
   {
   	// moved to static
//   	if(tableNamesTmp == null)	{		//likely this will only b true the very first time
//   		tableNamesTmp = new ArrayList<String>(200);
//   		readTableList(pickListTablesTxt);	//#DG928 original list
//   	}
//   	if(tableNamesOri == null)			//likely this will only b true the very first time
//   		readTableList(pickListTablesTxt);	//#DG928 original list

     Element topNode = (Element) DomUtil.getFirstNamedChild(doc, PICKLIST);
     if(topNode == null)	{
     	topNode = doc.createElement(PICKLIST);
     	doc.appendChild(topNode);
     }
     
     //#DG712 update all
     if(tableName == null)	{      	
   	  for (String aTableName : tableNames) {	// this is an upper case list!
   		  try
   		  {
   			  buildTableNode(srk, topNode, aTableName);          
   		  }
   		  catch(Exception e)
   		  {
   			  loger.error("Exception @cacheFromDb: tablename: " + aTableName+':'+e);
   		  }
   	  }
     }
     else	{      
   	  try	{
   		  buildTableNode(srk, topNode, tableName);          
   	  }
   	  catch(Exception e)      {
   		  loger.error("Exception @cacheFromDb: tablename: " + tableName+':'+e);
   	  }
     }
     return doc;
   }

   /**
    *  builds a 'Table' node in the picklist DOM. A table node consists of a
    *  root table element with name = table name and attibutes indicating the
    *  name of the description column and the id column. A third attribute indicates the
    *  table type - always 'picklist'
    *  The table node has two main child branches - rowview and columnview.
    *
    *  The construction of the table node proceeds as follows:<br>
    *  - Use the supplied table parameter to create the table element. <br>
    *  - Create the child branch roots (col and row views)<br>
    *  - Query DB meta data for the PK column<br>
    *  - Query ResultSet and DB meta data for the PK column name
   * @param srk 
    */
   static private void buildTableNode(SessionResourceKit srk, Element topNode, String tableNameIn )throws Exception
   {
 		
   	//#DG712 Element tel = doc.createElement(table);
   	final String tableNameUp = tableNameIn.toUpperCase();
   	
  	 	/* #DG928 table names have no suffix 
      int i = tableNameUp.indexOf(".");
      boolean nameHasInstId = i > 0; //TODO: it should be i > 0 - done! 
      final String tableName = nameHasInstId? tableNameUp.substring(0, i): tableNameUp;*/
      
      StringBuffer sb = new StringBuffer(32);
      sb.setLength(0);
      sb.append("select * from ").append(tableNameUp);
   	String sql = sb.toString();
   	sb.append(" order by INSTITUTIONPROFILEID");
   	String sqlInstId = sb.toString();

   	boolean hasInstId;
   	ResultSet rs;
      Statement statm = srk.getConnection().createStatement();
      try {
         loger.debug("sql: " + sqlInstId);     
      	rs = statm.executeQuery(sqlInstId);  //get the table contents
         hasInstId = true;
      } 
      catch (SQLException e) {
      	final int erCode = e.getErrorCode();
			if(erCode == 942)	{		//java.sql.SQLException: ORA-00942: table or view does not exist
		      // if table does not even exist, not bother
            loger.debug("@buildTableNode: "+e);
      		return;
      	}
      	if(erCode == 904)	{		//java.sql.SQLException: ORA-00904: "INSTITUTIONPROFILEID": invalid identifier
            /* #DG928 this should have been caught in containsTable method 
            if(nameHasInstId)	{
	          	 // this is for sure a rebuild since the original list does NOT contain inst.id in the name
	          	 //		and more: if the rebuild request is for a table with inst.id but this db does not ACTUALLY
	          	 //		has this col. just return!
	              loger.warn("@buildTableNode: not bother rebuilding cuz db. table does NOT have inst.id");
	              return;
            }*/
      		hasInstId = false;
      		// query again - must succeed
            loger.debug("sql: " + sql);     
         	rs = statm.executeQuery(sql);  //get the table contents
      	}
      	else
      		throw e;
      }
      
      //#DG862 skip instid
     //String pkfield = getPrimaryKeyName(tableName);
     String pkfield = null;
     DatabaseMetaData dbMeta = srk.getConnection().getMetaData();			
     ResultSet rsPrim = dbMeta.getPrimaryKeys("", null , tableNameUp);
     while(rsPrim.next())
     {
     	  pkfield = rsPrim.getString("COLUMN_NAME");
        if(pkfield == null || pkfield.length() <= 0)
        	  continue;
        if(pkfield.equals(INSTITUITIONPROFID))	{
        	  pkfield = null;
        }
        else	{
        	  break;
        }
     }
     rsPrim.close();

     int rowcount = 1, curInstId = -1, instId = -2;
     Element tableNode = null;
     String tableNameSf = tableNameUp;	// name with suffix if applicable 

     try
     {
       boolean descnameset = false;
       Element columnView = null;
       Element rowView = null;

       List<Element> tempColumnHolder = new ArrayList<Element>(7);

       ResultSetMetaData rsmd = rs.getMetaData();
       int cols = rsmd.getColumnCount();  //get the number of columns
       String colCnt;
       if(hasInstId)	{
          colCnt = String.valueOf(cols -1);
       }
       else	{
          colCnt = String.valueOf(cols);
          addTable(tableNameUp);       	  
       }
       
       for (; rs.next(); )
       {         
         //prepare nodes
         if(hasInstId)	{
        	 	instId = rs.getInt(INSTITUITIONPROFID);
            tableNameSf = tableNameUp+"."+instId;
            addTable(tableNameSf);		       	  
         }          
     	 	if(instId != curInstId)	{         
     	 		tableNode =  (Element) DomUtil.getFirstNamedChild(topNode, tableNameSf);
     	 		if(tableNode == null)	{
     	 			tableNode = doc.createElement(tableNameSf);
     	 			topNode.appendChild(tableNode);
     	 		}	

     	 		columnView = doc.createElement(COLUMN_VIEW_NODE);
     	 		tableColumnViewMap.put(tableNameSf, columnView);
     	 		// if already there replace ...
     	 		Element aColNode =  (Element) DomUtil.getFirstNamedChild(tableNode, COLUMN_VIEW_NODE);
     	 		if(aColNode != null) // replace it
     	 			tableNode.removeChild(aColNode);
     	 		tableNode.appendChild(columnView);
     	 		columnView.setAttribute(COLUMNS_ATR , colCnt);
        	
     	 		rowView = doc.createElement(ROW_VIEW_NODE);
     	 		tableRowViewMap.put(tableNameSf, rowView);
     	 		// if already there replace ...
     	 		Element aRowNode =  (Element) DomUtil.getFirstNamedChild(tableNode, ROW_VIEW_NODE);
     	 		if(aRowNode != null)	// replace it
     	 			tableNode.removeChild(aRowNode);
     	 		tableNode.appendChild(rowView);
          
     	 		if(pkfield != null && pkfield.length() > 0)
     	 			tableNode.setAttribute(PKFIELD_ATR, pkfield);          
     	 		tableNode.setAttribute(TYPE_ATR, PICKLIST_TABLE_VAL); 
   
     	 		rowcount = 1;
     	 		tempColumnHolder.clear();
     	 		curInstId = instId;
     	 	}

         Element row         = doc.createElement(ROW_NODE); //create a row element for the row view
         int jdbcType        = 0;
         int ourType         = 0;

         String name         = null;
         //String key          = null;
         String val          = null;
         String id           = null;
         String description  = null;

         boolean ispk        = false;
         boolean isDesc      = false;
         //boolean pkset       = false;
         //boolean descset     = false;

         //do the row
         Element column = null;
         /* #DGx 28/may/2010
          * for latest code inst. id col. is not the last one causing
          	   indexoutofbounds exceptions to happen here:
          	   	(Element)tempColumnHolder.get(i - 1);
          	   	
          	   this line retrieves the column element offset by 1.
          	   When inst.id column is skipped there is a need to offset by 2 instead 

          * 	this bug fixed by Dave B. in other lines by  adding a col. 
          * 		to tempColumnHolder for instId even if this element is never used 
          */
         short offSetColElem = 1;
         for(int i = 1; i <= cols; i++)   //begin appending rows
         {
           //get column info
           name      = rsmd.getColumnName(i);
           if(hasInstId && INSTITUITIONPROFID.equals(name))	{
         	  offSetColElem = 2;
         	  continue; 
           }
           
           jdbcType  = rsmd.getColumnType(i);
           ourType   = mapJdbcTypeToInternalType(jdbcType);
            if(rowcount == 1)
            {
               if(name == null) break;
               column = doc.createElement(name);
               column.setAttribute(INDEX_ATR, String.valueOf(i));
               column.setAttribute(JDBC_TYPE_ATR, String.valueOf(jdbcType));
               column.setAttribute(DATA_TYPE_ATR, String.valueOf(ourType));
               columnView.appendChild(column);

               tempColumnHolder.add(column);
            }

           if( name.toUpperCase().endsWith(DESCRIPTIONA)
              || name.toUpperCase().endsWith("NAME") ||  name.toUpperCase().endsWith("DESC"))
                 isDesc = true;

           ispk = name.equalsIgnoreCase(pkfield);

           if(isDesc)   //set the description field attribute in the table element
           {
              if(!descnameset)
              {
             	 tableNode.setAttribute(DESC_FIELD_ATR,name);
                //descset = true;
              }
           }

           val = rs.getString(i); //get the data and create a row in this column view

           if(val == null) val = "";

           Element colval = doc.createElement(ROW_NODE);     //put the column value in an element
           colval.setAttribute(INDEX_ATR, String.valueOf(rowcount));
           colval.setAttribute(VALUE_ATR, val);

           // get the column element from the zero based list and append this column to it
           Element colElement = (Element)tempColumnHolder.get(i - offSetColElem);
           colElement.appendChild(colval);

           if(val != null)
           { //for each column create a row child element s.t we have rowview.row.col
             Element col = doc.createElement(name);
             col.setAttribute(VALUE_ATR, val);
             if(isDesc)
             {
               description = val;
             }
             else if(ispk)
             {
               id = val;
             }
             row.appendChild(col);
           }
           isDesc = false;
           ispk = false;
         }

         if(id != null)
         {
           if(description == null || description.trim().length() == 0)
            description = " ";

           row.setAttribute(ID_ATR,id);
           row.setAttribute(DESCRIPTION_ATR,description);
           tableDescrIdMap.put(tableNameSf +"."+ description.toUpperCase(), id);	//#DG862
           tableIdDescrMap.put(tableNameSf +"."+ id.toUpperCase(), description);
         }

         rowView.appendChild(row);
         rowView.setAttribute(ROWS_ATR, String.valueOf(rowcount));
         rowcount++;		//#DG928 minor 
       }//end rs

       statm.close();
     }
     catch(Exception e)
     {
      loger.error(e);
      statm.close();
     }
     tableDataMap.put(tableNameSf, tableNode);
   }

  /**	
   *  Obtains a Picklist id value corresponding to a String value
   *  key as defined in picklistvalues.ini
   *  @param  table:  table/section name
   *  @param  input:  the id value or the corresponding key;
   *  @return the id value or -1 if an id cannot be parsed or  the id cannot be found
   */
 	public static int getPicklistIdValue(int instId, String tableName, String descr)  {

 		int val = -1;
 		
 		for (int i = 1; ; i++)	{ 
 			err:	{
	 			try {
	 				//val = findId(instId, tableName, descr);
	 				String ida = findIdAsString(instId, tableName, descr);
	 		 		val = Integer.parseInt(ida);	 				
	 			}
	 			catch (Exception e) {
	 				loger.warn("(may just be 1st load):"+e);	//#DG928
	 				//break err;
	 			}
	 			if(val < 0 && containsTableI(instId, tableName))
	 				break err;
	 			break;
	 		}
	 		if(i >= 2)	{
	 			loger.debug("Missing item at tableName="+tableName+" : ID=" + descr);
	 			break;
	 		}
	 		//rebuild and try again
	 		//loger.warn("reLoading table="+tableName);
	 		loadIt(tableName);
 		}
 		return val;
 	} 	
 	
 	public static int getPicklistIdValue(String tableName, String descr)  {
		return getPicklistIdValue(-1, tableName, descr);
  } 	
  
   /**
   *  Obtains a Picklist id value corresponding to a String value
   *  key as defined in picklistvalues.ini
   *  @param  table:  table/section name
   *  @param  input:  the id value or the corresponding key;
   *  @return the id value as a String  or null if an id  cannot be found
   */
   public static String getIdAsString(int instId, String tableName, String descr)  {
	  String val = null;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findIdAsString(instId, tableName, descr);
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : ID=" + descr);
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading only table="+tableName);	    
		  loadIt(tableName);
	  }
	  return val;
  }
  public static String getIdAsString(String tableName, String descr)  {
		return getIdAsString(-1, tableName, descr);
  }
  
   /**
   *  Obtains a Picklist description for an int value.
   *  @param  table:  case insensitive table/section name
   *  @param  id:  the picklist id value ( corresponding key to description value );
   *  @return String representation of the description corresponding to the input value
   *  or null if none is found
   */
  public static String getDescription(int instId, String tableName, int id)  {
	  
	  if(tableName == null)	//#DG928a
		  return null;
 	 
	  String val = null;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findDescription(instId, tableName, Integer.toString(id));
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : ID=" + id);
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);	    
		  loadIt(tableName);
	  }
	  return val;
  }	
  public static String getDescription(String tableName, int id)  {
		return getDescription(-1, tableName, id);
  }	
 	
   /**
   *  Obtains a Picklist description for an int value.
   *  @param  table:  table/section name
   *  @param  id:  the picklist id value ( corresponding key to description value );
   *  @param  safe: boolean true if an empty String should be returned when the corresponding description is not found.
   *  if false return null if no description is found
   *  @return String representation of the description corresponding to the input value
   */
	public static String getDescription(int instId, String tableName, String id, boolean safe)	{
		String desc = getDescription(instId, tableName, id);		
		if(desc == null && safe)
			return "";
		return desc;
	}
	public static String getDescription(String tableName, String id, boolean safe)	{
		return getDescription(-1, tableName, id, safe);
  }

   /**
   *  Obtains a Picklist description for an String representation of an id value.
   *  @param  table:  table/section name
   *  @param  id:  the picklist id value ( corresponding key to description value );
   *  @return String representation of the description corresponding to the input value or
   *  null if none is found or the id cannot be parsed
   */
	public static String getDescription(int instId, String tableName, String id)  {
		
		if(tableName == null || id == null)	//#DG928a
			return null;
		
		String val = null;
	  
		for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findDescription(instId, tableName, id);
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : ID=" + id);
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);	    
		  loadIt(tableName);
	  }
	  return val;
  }
  public static String getDescription(String tableName, String id)  {
		return getDescription(-1, tableName, id);
  }
  
   /**
   *  Obtains a Picklist id for an String representation of a field value.
   *  @param  table:  table name
   *  @param  column:  the field/column name;
   *  @return id as int or -1 if the value cannot be found
   */
  public static int getIdForColumnValue(int instId, String tableName, String colName, String vala)  {
	  int val = -1;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findIdForColumnValue(instId, tableName, colName, vala);
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val < 0 && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName + " : col=" + colName+" : ID=" + vala);
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);	    
		  loadIt(tableName);
	  }
	  return val;
  }	
  public static int getIdForColumnValue(String tableName, String col, String vala)  {
		return getIdForColumnValue(-1, tableName, col, vala);
  }	
  
  public static String getMatchingColumnValue(int instId, String tableName, int id, String col)  {
	  String val = null;	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findMatchingColumnValue(instId, tableName, id, col);
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //loger.warn("(may just be 1st load):"+e, e);	//#DG928 where exactly comes the null pointer except?
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : ID=" + id + " : col=" + col);
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);
		  loadIt(tableName);
	  }
	  return val;
  }	
  public static String getMatchingColumnValue(String tableName, int id, String col)  {
		return getMatchingColumnValue(-1, tableName, id, col);
  }
  
  /* #DG928 not used
  public static boolean containsTable(int instId, String tableName)  {	  
	   return instId >= 0?
			  containsTableI(tableName+"."+instId):	// must match exactly
			  containsTable(tableName);	// try with or w/o
  }
   // for internal use - must match exactly (inst.id)
   private static boolean containsTableI(String tableName)  {
 	  for (int i = 1; ; i++)	{ 
 		  err:	{
	 		  try {
	 	   	  tableName = tableName.toUpperCase();
		 		  for (String aTableName : tableNamesOri) {		 			  
	 					int res = aTableName.compareTo(tableName);
	 					if(res == 0)	// match
	 						return true;
	 					if(res > 0)	// got past
	 			  			break;
		 		  }
	 		  }
	 		  catch (Exception e) {
	 			  loger.warn("(may just be 1st load):"+e);	//#DG928
	 			  //break err;
	 		  }
	 		  /* tableNames is not a HashMap anyways so should never disappear!
	  		     if(val == null)
	  					break err;* /
	 		  break;
	 	  }
	 	  if(i >= 2)	{
	 		  loger.debug("Missing table="+tableName);
	 		  break;
	 	  }
	 	  //rebuild and try again
	 	  //loger.warn("reLoading table list for="+tableName);
	 	  init(tableName);
 	  }
 	  return false;	  
   }    */  
  
  public static boolean valueEqualsId(String descr, String tableName, int id)
  {
	  return id == getPicklistIdValue(tableName, descr);
  }  
  /*#DG862	if this call ever needs instId it must just be inlined with the contents instead
  public static boolean valueEqualsId(int instId, String descr, String table, int id)
  {
     return id == getPicklistIdValue(instId, table, descr);
  }*/

 /** Return comma delimited (CD) list (in string) of values in column honoring the
  *  column datatype (e.g. non-numeric column types are quoted in the string formed).
  *
  * E.g. Column values are 0 1 and 2 (INTEGER)
  *
  *      Result: "0, 1, 2"
  *
  *      Column values are "this" "that" "the other" (STRING)
  *
  *     Result: "'this', 'that', 'the other'"
  *
  **/
  public static String getColumnValuesCDV(int instId, String tableName, String col)  {
	  String val = null;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findColumnValuesCDV(instId, tableName, col);				
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : col=" + col);
			  val = "'0', '0'";	//#DG928a keep original behaviour		
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);
		  loadIt(tableName);
	  }
	  return val;    
  }
  public static String getColumnValuesCDV(String tableName, String col)  {
		return getColumnValuesCDV(-1, tableName, col);
  }
  
 /**
  * Return comma delimited (CD) list (in string) of values in column and
  * treat all values as STRING data type regardless of actual column data type.
  *
  * E.g. Column values are 0 1 and 2 (INTEGER)
  *
  *      Result: "'0', '1', '2'"
  *
  **/
  public static String getColumnValuesCDS(int instId, String tableName, String col)  {
	  String val = null;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findColumnValuesCDS(instId, tableName, col);		
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : col=" + col);
			  val = "'0', '0'";	//#DG928a empty string "" would be better but let's keep original behaviour		
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);
		  loadIt(tableName);
	  }
	  return val;
  }
  public static String getColumnValuesCDS(String tableName, String col)  {
	  return getColumnValuesCDS(-1, tableName, col);
  }
  
  /**
  * Return comma delimited (CD) list (in string) of values in column and
  * treat all values as STRING data type if column data type = DECIMAL.
  *
  * E.g. Column values are 0 1 and 2 (DECIMAL)
  *
  *      Result: "'0.0', '1.0', '2.0'"
  *
  **/
  public static String getColumnValuesCDDS(int instId, String tableName, String col)  {
	  String val = null;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findColumnValuesCDDS(instId, tableName, col);		
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : col=" + col);
			  val = "'0', '0'";	//#DG928a empty string "" would be better but let's keep original behaviour		
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);
		  loadIt(tableName);
	  }
	  return val;
  }
  public static String getColumnValuesCDDS(String tableName, String col)  {
		return getColumnValuesCDDS(-1, tableName, col);
  }
  
  public static List<String> getValueList(int instId, String tableName, String col)	{
	  List<String> val = null;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findValueList(instId, tableName, col);		
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if((val == null || val.isEmpty()) && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : col=" + col);
			  val = new ArrayList<String>(0);	//#DG928a keep original behaviour 		  
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);
		  loadIt(tableName);
	  }
	  return val;
  }
  public static List<String> getValueList(String tableName, String col)	{	
		return getValueList(-1, tableName, col);
  }
  
  public static String getValuesCDS(int instId, String tableName, String colName,
		  String op, double compareValue,
		  String trueValue, String falseValue)  {
	  String val = null;	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {			  
				  val = findValuesCDS(instId, tableName, colName, op, compareValue, trueValue, falseValue);
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if(val == null && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing item at tableName="+tableName+" : col=" + colName);
			  val = "'0', '0'";	//#DG928a empty string "" would be better but let's keep original behaviour		
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);
		  loadIt(tableName);
	  }
	  return val;
  }	
  public static String getValuesCDS(String tableName, String colName,
		  String op, double compareValue,
		  String trueValue, String falseValue)  {
		return getValuesCDS(-1, tableName, colName, op, compareValue, trueValue, falseValue);
  }	
  
  public static String[][] getTableValues(int instId, String tableName)	{        
	  String[][] val = null;
	  
	  for (int i = 1; ; i++)	{ 
		  err:	{
			  try {
				  val = findTableValues(instId, tableName);	  		
			  }
			  catch (Exception e) {
				  loger.warn("(may just be 1st load):"+e);	//#DG928
				  //break err;
			  }
			  if((val == null || val.length <= 0) && containsTableI(instId, tableName))
				  break err;
			  break;
		  }
		  if(i >= 2)	{
			  loger.debug("Missing values at tableName="+tableName);
			  //if missing it's bad: returns null //val = new String[0][0];
			  break;
		  }
		  //rebuild and try again
		  //loger.warn("reLoading table="+tableName);
		  loadIt(tableName);
	  }
	  return val;
  }
  public static String[][] getTableValues(String tableName)	{        
		return getTableValues(-1, tableName);        
	}

  /* failure is not to happen here - tableNames is not a HashMap anyways so should never disappear!
  public static boolean containsTable(int instId, String tableName)
  {
	  boolean val = false;	  
	  for (int i = 1; ; i++)	{
		  err:	{
		  try {
			  val = containsTableI(instId, tableName);
		  }
		  catch (Exception e) {
			  loger.warn("(may just be 1st load):"+e);	//#DG928
			  //break err;
		  }
		  /* only used in docprep and ingestion - tableNames is not a HashMap anyways so should never disappear!
 		     if(val == null)
 					break err;* /
		  break;
	  }
	  if(i >= 2)	{
		  loger.debug("Missing table="+tableName);
		  break;
	  }
	  //rebuild and try again
	  //loger.warn("reLoading table list for="+tableName);
	  loadIt(tableName);
	  }
	  return val;	  
  }*/
  public static boolean containsTable(int instId, String tableName)
  {
	  return containsTableI(instId, tableName);	  
  }
  public static boolean containsTable(String tableName)	{
	  return containsTable(-1, tableName);
  }  
  
	  //*************************************
	  // 	internal implementations 
	  //*************************************
  
	/* #DG928a moved up
	static private int findId(int instId, String tableName, String descr) {
		try {
			String ida = findIdAsString(instId, tableName, descr);
			return Integer.parseInt(ida);
		} 
		catch (Exception e) {
			return -1;
		}
	}*/

  static private String findIdAsString(int instId, String tableName, String descr)    {
		if(tableName == null || descr == null)
   		 return null;
   	 
	  	 final String tableNameUp = tableName.toUpperCase();
		 final String descrUp = descr.toUpperCase();

 		 String ida;
 		 if(instId >= 0)	{
 			 ida = tableDescrIdMap.get(tableNameUp+"."+instId + "."+descrUp); 			 
 	 		 if(ida == null)	// fall back and try w/o instId
 	 			 loger.warn("likely access with inst.id but db. table does not have it:"+tableNameUp);//+"::"+StringUtil.stack2string(null, 4, 6, "::"));
 	 		 else
 	 	 		 return ida;
 		 }
 		 ida = tableDescrIdMap.get(tableNameUp + "."+descrUp);
   	 return ida;
    }

    static private String findDescription(int instId, String tableName, String id)    {
   	 /* #DG928a moved up
   	 if(tableName == null || id == null)
   		 return null;*/
   	 
   	 final String tableNameUp = tableName.toUpperCase();
   	 
 		 String descr;
 		 if(instId >= 0)	{   	 
 			 descr =tableIdDescrMap.get(tableNameUp+"."+instId +"."+ id);
 	 		 if(descr == null)	// fall back and try w/o instId
 	 			 loger.warn("likely access with inst.id but db. table does not have it:"+tableNameUp);//+"::"+StringUtil.stack2string(null, 4, 6, "::"));
 	 		 else
 	 	 		 return descr;
 		 }
 		 descr = tableIdDescrMap.get(tableNameUp+"."+ id);
   	 return descr;
    }

    /**
     *  Obtains a Picklist id for an String representation of a field value.
     *  @param  table:  table name
     *  @param  column:  the field/column name;
     *  @return id as int or -1 if the value cannot be found
     *
     *  <b>Note:</b> for efficiency depends on the records being in ascending id order. <br/>Really just
     *  determined the index of the value in the xml file and decrements by one.
     */
    static private int findIdForColumnValue(int instId, String tableName, String colName, String vala)    {
   	 
   	 final String tableNameUp = tableName.toUpperCase();
   	 final String colNameUp = colName.toUpperCase();
   	 //get the column element for the table
   	 Element columnView = getColumnElement(instId, tableNameUp, colNameUp);
   	  		 
   	 if(columnView == null) return -1;
   	 
   	 //search the values in the column
   	 Element row = (Element)DomUtil.getFirstChildWithAttrValue(columnView, ROW_NODE, VALUE_ATR, vala.toUpperCase());		//#DG862
   	 
   	 if(row == null) return -1;
   	 
   	 //return the value attribute for the field
   	 String index = row.getAttribute(INDEX_ATR);   	 
   	 int id = -1;   	 
   	 try
   	 {
   		 id = Integer.parseInt(index);
   	 }
   	 catch(NumberFormatException e)
   	 {
   		 loger.debug("findIdForColumnValue::" + tableNameUp+"::"+colNameUp+"::"+vala+"::"+e);
   		 return id;
   	 }   	 
   	 return id - 1;
    }
    
    static private List<String> findValueList(int instId, String tableName, String colName)    {
   	 final String tableNameUp = tableName.toUpperCase();
   	 final String colNameUp = colName.toUpperCase();
   	 Element columnView = getColumnElement(instId, tableNameUp, colNameUp);
   	 
   	 if(columnView == null) //#DG928a
   		 throw new NullPointerException("columnView null in findValueList");		

   	 List<Element> rows = DomUtil.getNamedChildren(columnView, ROW_NODE );
   	 List<String> result = new ArrayList<String>(rows.size());
   	 
   	 ListIterator<Element> li = rows.listIterator();
   	 
   	 Element currow = null;
   	 String rowval = null;
   	 while(li.hasNext())
   	 {
   		 currow = (Element)li.next();
   		 rowval = currow.getAttribute(VALUE_ATR);
   		 
   		 if(rowval != null) 
   			 result.add(rowval);
   	 }
   	 return result;
    }

    // return value from named column that corresponds to the id value provided.
    //
    static private String findMatchingColumnValue(int instId, String tableName, int id, String colName)    {
   	 final String tableNameUp = tableName.toUpperCase();
   	 //get the row element for the table
   	 Element rowView = getRowElementForId(instId, tableNameUp, id);   	 
   	 if(rowView  == null) return null;
   	 
   	 //get the field element in the row
   	 final String colNameUp = colName.toUpperCase();
   	 Element column = (Element)DomUtil.getFirstNamedChild(rowView , colNameUp);   	 
   	 if(column == null) return null;
   	 
   	 //return the value attribute for the field
   	 return column.getAttribute(VALUE_ATR);
    }

    //#DG600
    /**
     *  return the string array of the values of this row
     * @param tableName the table
     * @param rowId the row id for the table
     * @return array of values of the chosen row
     * #DG928 not used /
    static public String[] getRowValues(int instId, String tableName, int rowId)    {
   	 
   	 final String tableNameUp = tableName.toUpperCase();   	 
   	 //get the row element for the table
   	 Element rowView = getRowElementForId(instId, tableNameUp, rowId);
   	 /*	#DG712 let go for efficiency, will be caught later anyways
      if(rowa == null) 
      	return null;* /   	 
   	 List<Element> cols = DomUtil.getChildElements(rowView);
   	 String resulta[] = new String[cols.size()];
   	 int i = 0;
   	 for (Element col : cols) {
   		 resulta[i++] = col.getAttribute(VALUE_ATR);
   	 }  	
   	 return resulta;
    }*/
    
    // retrive array of string values arranged by rows
    // the name is different from the other method which is static 
    static private String[][] findTableValues(int instId, String tableName)    {
   	 final String tableNameUp = tableName.toUpperCase();
       Element rowView = getRowViewElement(instId, tableNameUp);      
   	 
       if(rowView == null) //#DG928a
   		 throw new NullPointerException("rowView null in findTableValues");
       
   	 List<Element> rows = DomUtil.getNamedChildren(rowView, ROW_NODE);  	
   	 String rowsa[][] = new String[rows.size()][];
   	 int i = 0;
   	 for (Element rowa : rows) {
   		 List<Element> cols = DomUtil.getChildElements(rowa);
   		 int j =cols.size();
   		 if(j <= 0)	//#DG712 if missing, refuse it
   			 return null;
   		 String colsa[] = new String[j];
   		 j = 0;
   		 for (Element col : cols) {
   			 colsa[j++] = col.getAttribute(VALUE_ATR);
   		 }  	
   		 rowsa[i++] = colsa;
   	 }  	
   	 return rowsa;
    }

    // return comma delimited (CD) list (in string) of values in column.
    //
    // E.g. Column values are 0 1 and 2 (INTEGER)
    //
    //      Result: "0, 1, 2"
    //
    //      Column values are "this" "that" "the other"
    //
    //      Result: "'this', 'that', 'the other'"

    // the name is different from the other method which is static 
    static private String findColumnValuesCDV(int instId, String tableName, String colName)    {
   	 final String tableNameUp = tableName.toUpperCase();
   	 final String colNameUp = colName.toUpperCase();
   	 List<String> values = findValueList(instId, tableNameUp, colNameUp);
   	 if (values.size() == 0) values.add("0");
   	 if (values.size() == 1) values.add("0");
   	 
   	 int colDataType = getColumnDataType(instId, tableNameUp, colNameUp);
   	 
   	 boolean stringType = colDataType == STRING 
   	 	|| colDataType == TIMESTAMP 
   	 	|| colDataType == OTHER;   	 
   	 
   	 StringBuffer ret = getValuesString(values, stringType, false);
   	 
   	 return ret.toString();
    }

    // return comma delimited (CD) list (in string) of values in column and
    // treat all values as string regardless of column data type.
    //
    // E.g. Column values are 0 1 and 2 (INTEGER)
    //
    //      Result: "'0', '1', '2'"

    // the name is different from the other method which is static 
    static private String findColumnValuesCDS(int instId, String tableName, String colName)    {
   	 List<String> values = findValueList(instId, tableName, colName);	//#DG928a
   	 
   	 if (values.size() == 0) values.add("0");
   	 if (values.size() == 1) values.add("0");
   	 
   	 StringBuffer ret = getValuesString(values, true, false);
   	 
   	 return ret.toString();
    }

    // return comma delimited (CD) list (in string) of values in column and
    // treat all values as string regardless of if column data type is DECIMAL.
    //
    // E.g. Column values are 0 1 and 2 (DECIMAL)
    //
    //      Result: "'0.0', '1.0', '2.0'"
    
    // the name is different from the other method which is static 
    static private String findColumnValuesCDDS(int instId, String tableName, String colName)    {
   	 final String tableNameUp = tableName.toUpperCase();
   	 final String colNameUp = colName.toUpperCase();
   	 List<String> values = findValueList(instId, tableNameUp, colNameUp);	//#DG928a
   	 
   	 if (values.size() == 0) values.add("0");
   	 if (values.size() == 1) values.add("0");
   	 
   	 StringBuffer ret;
   	 
   	 if(getColumnDataType(instId, tableNameUp, colNameUp) != DECIMAL)
   		 ret = getValuesString(values, true, false);
   	 else
   		 ret = getValuesString(values, true, true);
   	 
   	 return ret.toString();
    }

    static private int getColumnDataType(int instId, String tableNameUp, String colNameUp)    {
   	 Element columnView = getColumnElement(instId, tableNameUp, colNameUp);   	 
   	 try
   	 {
   		 String val = columnView.getAttribute(DATA_TYPE_ATR);   		 
   		 return Integer.parseInt(val);
   	 }
   	 catch(Exception e)
   	 {
   		 return STRING;
   	 }
    }

    // the name is different from the other method which is static 
    static private String  findValuesCDS(int instId, String tableName, String colName,
   		 String op, double compareValue,
   		 String trueValue, String falseValue)
    {
   	 List<String> lst = findValueList(instId, tableName, colName);   	 
   	 List<String> newLst = new ArrayList<String>(2);
   	 
   	 Iterator<String> iter = lst.iterator();
   	 
   	 while (iter.hasNext())
   	 {
   		 double d = 0.0;
   		 
   		 try {d = Double.parseDouble((String)iter.next());} catch (Exception e) {d = 0.0;}
   		 
   		 if (op.equals("=="))
   			 newLst.add((d == compareValue)? trueValue : falseValue);
   		 
   		 else if (op.equals(">"))
   			 newLst.add((d > compareValue)? trueValue : falseValue);
   		 
   		 else if (op.equals(">="))
   			 newLst.add((d >= compareValue)? trueValue : falseValue);
   		 
   		 else if (op.equals("<"))
   			 newLst.add((d < compareValue)? trueValue : falseValue);
   		 
   		 else if (op.equals("<="))
   			 newLst.add((d <= compareValue)? trueValue : falseValue);
   		 
   		 else
   			 newLst.add((d == compareValue)? trueValue : falseValue);
   	 }
   	 
   	 if (newLst.size() == 0) newLst.add("0");
   	 if (newLst.size() == 1) newLst.add("0");
   	 
   	 StringBuffer ret = getValuesString(newLst, true, false);
   	 
   	 return ret.toString();
    }

    /*#DG928 not used ...
    static private int getColumnNumberOfRows(int instId, String tableNameUp)    {
       Element rowView;
       if(instId >= 0)	{   	 
       	rowView = (Element)tableRowViewMap.get(tableNameUp+"."+instId);
 			if(rowView == null)	{	// fall back and try w/o instId
 	 			 loger.warn("likely access with inst.id but db. table does not have it:"+tableNameUp);//+"::"+StringUtil.stack2string(null, 4, 6, "::"));
 	 			 rowView = (Element)tableRowViewMap.get(tableNameUp);
 			}
 		 }
 		 else
 			 rowView = (Element)tableRowViewMap.get(tableNameUp);   	 
   	 
   	 String val = rowView.getAttribute(ROWS_ATR);   	 
   	 try
   	 {
   		 return Integer.parseInt(val);
   	 }
   	 catch(Exception e)
   	 {
   		 return -1;
   	 }
    }
    static private boolean hasColumn(int instId, String tableName, String colName)    {
   	 final String tableNameUp = tableName.toUpperCase();
   	 final String colNameUp = colName.toUpperCase();
   	 
   	 Element columnView = getColumnElement(instId, tableNameUp, colNameUp);
   	 return columnView != null;
    }*/
    
    // performs ordered sequential search for table name W/O inst id suffix
    //		as loaded from the original list from disk file
	static private boolean containsTableI(int instId, String tableName) 
	{
		/*tableNamesLoaded - all entries are non null and in uppercase, ordered, no repetition allowed,
		 * 		 
		 * 	this table will contain the original list of loadable tables
		 * 	(w/o instId sufix) and will also contain the tables names that 
		 * 		were so far loaded with instId sufix.
		*/
		tableName = tableName.toUpperCase();
		String tableNameDot = tableName+".";
		boolean found = false;; 
		boolean foundWInstid = false;
		for (String aTableName : tableNames) {
			// if(aTableName.equals(tableName) ||
			// aTableName.startsWith(tableNameDot))
			int res = aTableName.compareTo(tableName);
			if (res == 0)	{ // match
				found = true;
				continue;
			}
			if (res > 0) 	{// got past
				foundWInstid = aTableName.startsWith(tableNameDot);
				break;
			}
		}

		if(!found)	// not found - false for sure
			return false;
		// found, let's chek instId presence
		if(instId >= 0)	{	
			if(foundWInstid)		// good, it was requested with instId and it's in the list as such
				return true;
			//not ideal, requested w instId, but not found with instId
			loger.warn("table requested with instId but it's in the db. WITHOUT instId:"+tableName);
			return true;
		}
		if(foundWInstid)	// no good, it was requested w/o instId but it's in the list w instId
			loger.error("table requested w/o instId but it's in the db. WITH instId:"+tableName);
		else
			return true;	// good, it was requested w/o instId and it's in the list as such 		
		return false;		
	}
    
    /**
     *  gets the named column element for the given table
     *  input params are in UPPERCASE
     */
    static private Element getColumnElement(int instId, String tableNameUp, String colNameUp)
    {
      Element columnView;
      if(instId >= 0)	{   	 
      	columnView = (Element)tableColumnViewMap.get(tableNameUp+"."+instId);		
      	if(columnView == null)	{	// fall back and try w/o instId
      		loger.warn("likely access with inst.id but db. table does not have it:"+tableNameUp);//+"::"+StringUtil.stack2string(null, 4, 6, "::"));
      		columnView = (Element)tableColumnViewMap.get(tableNameUp);
      	}
      }
      else
      	columnView = (Element)tableColumnViewMap.get(tableNameUp);      
      
      Element column = (Element)DomUtil.getFirstNamedChild(columnView, colNameUp);
      return column;
    }


     /**
     *  gets the named column element for the given table
     */
    static private Element getRowElementForId(int instId, String tableNameUp, int ida)
    {
      Element rowView = getRowViewElement(instId, tableNameUp);      
      return (Element)DomUtil.getFirstChildWithAttrValue(rowView, ROW_NODE,ID_ATR, String.valueOf(ida));
    }

    static private Element getRowViewElement(int instId, String tableNameUp)
    {
      Element rowView;
      if(instId >= 0)	{   	 
      	rowView = (Element)tableRowViewMap.get(tableNameUp+"."+instId);
			if(rowView == null)	{	// fall back and try w/o instId
	 			 loger.warn("likely access with inst.id but db. table does not have it:"+tableNameUp);//+"::"+StringUtil.stack2string(null, 4, 6, "::"));
	 			 rowView = (Element)tableRowViewMap.get(tableNameUp);
			}
		 }
		 else
			 rowView = (Element)tableRowViewMap.get(tableNameUp);            
      return rowView;
    }

    /* #DG928 not used
    static private StringBuffer getValuesString(List<String> in, boolean forceStringType)    {
        return(getValuesString(in, forceStringType, false));
    }*/

    // Modified by BILLY to handle decimal values -- 15Jan2001
    static private StringBuffer getValuesString(List<String> in, boolean forceStringType, boolean isDecimal)
    {
      if(in == null) return null;

      StringBuffer buf = new StringBuffer();
      if(in.isEmpty()) return buf;

      Iterator<String> it = in.iterator();
      String current = null;
      //int len = in.size();
      int count = 0;
      while(it.hasNext())
      {
        current = (String)it.next();
        if(isDecimal)
        {
          // append '.0' if decimal point not find
          if(current.indexOf(".") == -1)
            current = current + ".0";
        }
        if(current != null)
        {
          if (!forceStringType)
            buf.append(current);
          else
          {
            String quote = "'";

            // check for embedded 's - if found quote string with "
            if (current.indexOf(quote) != -1)
              quote = "\"";

            buf.append(quote + cleanForScript(current, quote) + quote);
          }
        }
        count++;
        if(count != in.size())
         buf.append(", ");
      }
      return buf;
    }

    static public String cleanForScript(String s, String quoteChar)
    {
      if (s.indexOf(quoteChar) == -1)
        return s;

      byte quote = (quoteChar.getBytes())[0];
      StringBuffer sb = new StringBuffer();
      byte[] bytes = s.getBytes();
      int len = bytes.length;

      for (int i = 0; i < len; ++i)
      {
        if (bytes[i] < 32)
          continue;

        if (bytes[i] == quote)
          continue;

        sb.append((char)bytes[i]);
      }
      return sb.toString();
    }

    static private int mapJdbcTypeToInternalType(int jdbcType)
    {
      if (jdbcType == Types.NUMERIC ||
          jdbcType == Types.DECIMAL ||
          jdbcType == Types.FLOAT ||
          jdbcType == Types.REAL ||
          jdbcType == Types.DOUBLE)
      {
          return DECIMAL;
      }
      else if (jdbcType == Types.BIT ||
          jdbcType == Types.TINYINT ||
          jdbcType == Types.SMALLINT ||
          jdbcType == Types.INTEGER ||
          jdbcType == Types.BIGINT)
      {
          return INTEGER;
      }
      else if (jdbcType == Types.DATE ||
          jdbcType == Types.TIME ||
          jdbcType == Types.TIMESTAMP ||
          jdbcType == 11 ||
          jdbcType == 9)
      {
          return TIMESTAMP;
      }
      else if (jdbcType == Types.CHAR ||
          jdbcType == Types.VARCHAR ||
          jdbcType == Types.LONGVARCHAR)
      {
          return STRING;
      }
      else if (jdbcType == Types.BINARY)
      {
          // binary types!
          return OTHER;
      }

      return OTHER;
    }
//////////////////////////////////////////////////////////////////////////////  /
////////////////////////////////////////////////////////////////////////////  //
////////////////////////////////////////////////////////////////////////////  //


    /**
     *  Obtains a represenation of a Picklist table suitable for use with Java Script (or other similar
     *  scripting languages).
     *
     *  The result is an list with one element for each column in the picklist table. Eack column
     *  element is a string array (String[]) containing the following:
     *
     *    [0] = {column name} (uppercase)
     *    [1] = {data type}
     *          data type = "STRING" || "INTEGER" || "DECIMAL" || "TIMESTAMP" || "OTHER"
     *    [2] = {number of rows} (string represenation of int value)
     *    [3] = {comma delimited list of (row) values in the column}
     *
     *    The comma delimited list of row values provides the 'script representation'. For example in
     *    JavaScript arrays of values can assigned a variables using such lists, e.g.:
     *
     *          incomeTypes   = new Array('Forced Labor', 'Chain Gang', 'Salary');
     *          incomeTypeIds = new Array(0, 1, 2);
     *
     * Example: getScriptRep("IncomeType")
     *
     *      --> assume table contains columns IncomeTypeId, and ITDESCRIPTION (only) and that
     *          there are three rows.
     *
     *      [0][0] = "INCOMETYPEID"
     *      [0][1] = "INTEGER"
     *      [0][2] = "3"
     *      [0][3] = "0, 1, 2"
     *
     *      [0][0] = "ITDESCRIPTION"
     *      [0][1] = "STRING"
     *      [0][2] = "3"
     *      [0][3] = "'Forced Labor', 'Chain Gang', 'Salary'"
     *
     **/

    static public List<String[]> getScriptRep(String table)
    {

      if (table.equals(PKL_INCOME_TYPE))
        return incomeTypeScriptRep();

      if (table.equals(PKL_ASSET_TYPE))
        return assetTypeScriptRep();

      if (table.equals(PKL_LIABILITY_TYPE))
        return liabilityTypeScriptRep();

      return new ArrayList<String[]>();
    }

    static String[] incomeTypeId =  { "INCOMETYPEID", "DECIMAL", "12", "0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11"  };
    static String[] itDescription =  { "ITDESCRIPTION", "STRING", "12", "'Salary', 'Hourly', 'Hourly + Commissions', 'Commissions', 'Alimony', 'Child Support', 'Self-Employed', 'Pension', 'Interest Income', 'Rental Income (- From Expense)', 'Rental Income (+ To Income)', 'Other'"  };
    static String[] employmentRelated =  { "EMPLOYMENTRELATED", "STRING", "12", "'Y', 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N'"  };
    static String[] incomeGdsInclusion =  { "GDSINCLUSION", "DECIMAL", "12", "100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 75, 100"  };
    static String[] incomeTdsInclusion =  { "TDSINCLUSION", "DECIMAL", "12", "100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 75, 100" };

    static private List<String[]> incomeTypeScriptRep()
    {
        List<String[]> r = new ArrayList<String[]>(5);
        r.add(incomeTypeId);
        r.add(itDescription);
        r.add(employmentRelated);
        r.add(incomeGdsInclusion);
        r.add(incomeTdsInclusion);

        return r;
    }

    final static String[] ATId =  { "ASSETTYPEID", "DECIMAL", "14", "0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13"  };
    final static String[] ATDesc =  { "ASSETTYPEDESCRIPTION", "STRING", "14", "'Savings', 'RRSP', 'Gift', 'Residence', 'Vehicle', 'Property', 'Stocks/Bonds/Mutual', 'Rental Property', 'Other', 'Down Payment', 'Holding', 'Household Goods', 'Life Insurance', 'Deposit on Purchase'"  };
    final static String[] ATnet =  { "NETWORTHINCLUSION", "DECIMAL", "14", "100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 0, 100, 100"  };

    static private List<String[]> assetTypeScriptRep()
    {
        List<String[]> r = new ArrayList<String[]>(3);
        r.add(ATId);
        r.add(ATDesc);
        r.add(ATnet);

        return r;
    }

    final static String[] LTId =  { "LIABILITYTYPEID", "DECIMAL", "20", "0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19"  };
    final static String[] LTDesc =  { "LDESCRIPTION", "STRING", "20", "'Mortgage','Equity Mortgage', 'Credit Card', 'Personal Loan', 'Auto Loan', 'Alimony', 'Child Support', 'Student Loan', 'Wage Garnishment', 'Other', 'Unsecured Line of Credit', 'Income Tax', 'Mortgage on a Rental Property', 'Secured Line of Credit', 'Closing Costs', 'Lease', 'Rent', 'Estimated Municipal Taxes', 'Estimated Condo Fees', 'Estimated Heating Expenses'"  };
    final static String[] LTGdsInclusion =  { "GDSINCLUSION", "DECIMAL", "20", "100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 75, 100"  };
    final static String[] LTTdsInclusion =  { "TDSINCLUSION", "DECIMAL", "20", "100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 75, 100" };
    final static String[] LTpayQual =  { "LIABILITYPAYMENTQUALIFIER", "STRING", "20", "'F', 'F', 'V', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'V', 'F', 'F', 'V', 'F', 'F', 'F', 'F', 'F','F'"  };
    final static String[] LTpropEst =  { "PROPERTYEXPENSEESTIMATE", "STRING", "20", "'N', 'N', 'N', 'N', 'N', 'N', 'N','N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'Y', 'Y'"  };

    static private List<String[]> liabilityTypeScriptRep()
    {
        List<String[]> r = new ArrayList<String[]>(6);
        r.add(LTId);
        r.add(LTDesc);
        r.add(LTGdsInclusion);
        r.add(LTTdsInclusion);
        r.add(LTpayQual);
        r.add(LTpropEst);

        return r;
    }

  	/* not used
  	private String valueListForScript(List values, String dataType)
  	{
      boolean treatAsText = dataType.equals("STRING") ||
                            dataType.equals("OTHER")  ||
                            dataType.equals("TIMESTAMP");
      // avoid lists with just one element - e.g. if used in JS statement such as
      // values = new Array({substitute a list}) then a list with only one element
      // may be interpreted as array size setting vs. array population (with one value only).
      // so ... ensure list holds at least two elements
      if (values == null) values = new ArrayList();
      if (values.size() == 0) values.add("0");
      if (values.size() == 1) values.add("0");
      StringBuffer sb = new StringBuffer();
      int lim         = values.size();
      String val      = null;
      for (int i = 0; i < lim; ++i)      {
          val = (String)values.get(i);
          if (treatAsText == false)
            sb.append(val);
          else          {
            String quote = "'";
            // check for embedded 's - if found quote string with "
            if (val.indexOf(quote) != -1)
              quote = "\"";
            sb.append(quote + cleanForScript(val, quote) + quote);
          }
          if (i != lim -1)
            sb.append(",");
      }
      return sb.toString();
    }*/

    /** #DG928 revamp synching
     * 
     * make sure all access is suspended until rebuild is over
     */
    static class PiklMap<K,V> extends HashMap<K,V>
    {
		private static final long serialVersionUID = 1L;
		public PiklMap(int i) {
			super(i);
		}   
		
	   public int size()	{
	   	if(synchMode4Rebuild)
				synchronized (PicklistData.class) {				
					return super.size();
				}
	   	else
	   		return super.size();
	   }
		public V get(Object key) {
	   	if(synchMode4Rebuild)
				synchronized (PicklistData.class) {				
					return super.get(key);
				}
	   	else
	   		return super.get(key);
	   }		
	   public V put(K key, V value) {
			if (synchMode4Rebuild)
				synchronized (PicklistData.class) {
					return super.put(key, value);
				}
			else
				return super.put(key, value);
		}		
    }

    //   THOSE METHODS ARE FOR TESTING ONLY !!
    public static String[] getTableNamesTest()	{ return tableNames; }
    public static Element getRowTest(int instId, String tableNameUp)	{ 
   	 return getRowViewElement(instId, tableNameUp); 
    }
    public static void setTableIdDescrMap(Map<String, String> m)	{ tableIdDescrMap = m; }
    public static void setTableDescrIdMap(Map<String, String> m)	{ tableDescrIdMap = m; }
    public static void setTableColumnViewMap(Map<String, Node> m)	{ tableColumnViewMap = m; }
    public static void setTableRowViewMap(Map<String, Node> m)	{ tableRowViewMap = m; }
    
    public static void main(String[] args)throws Exception
    {/*
      System.out.println("Init start...");
      PropertiesCache.addPropertiesFile("c:\\mos\\admin\\mossys.properties");
      ResourceManager.init("c:\\mos\\admin\\","MOS_D");
      SessionResourceKit srk = new SessionResourceKit("0");
      System.out.println("Init end\n");

      PicklistData.init();
      double mos = 126;
      int mosint = (int)(mos/12);
      String mosstr = String.valueOf(mosint);
       System.out.println("years: " + mosstr);
      if(mos < 1 && mos > 0)
       mosstr = "0.50";
      int val = PicklistData.getIdForColumnValue("PaymentTerm", "ptValue", mosstr);
      System.out.println("Got value: " + val);
      String desc = PicklistData.getDescription("PaymentTerm", val);
      System.out.println("Description: " + desc);
      */

      //Test for PropertiesBundle
      System.out.println("Start ...");
      ResourceBundle pRsBundle = ResourceBundle.getBundle("TestPicklist", new Locale("en", ""));
  /*
      System.out.println("Key1 = " + pRsBundle.getString("key1"));
      System.out.println("Key2 = " + pRsBundle.getString("key2"));
      pRsBundle = ResourceBundle.getBundle("TestPicklist", new Locale("fr", ""));
      System.out.println("Key1 = " + pRsBundle.getString("key1"));
      System.out.println("Key2 = " + pRsBundle.getString("key2"));
      pRsBundle = ResourceBundle.getBundle("TestPicklist");
  */
      //System.out.println("Key1 = " + pRsBundle.getString("key1"));
      //System.out.println("Key2 = " + pRsBundle.getString("key2"));
      Enumeration all = pRsBundle.getKeys();
      TreeMap sorted = new TreeMap();
      String theKey = "";
      String theOrgVal = "";
      String sortOrderStr = "";

      System.out.println("Unsorted ...");
      while(all.hasMoreElements())      {
        String[] theVal = new String[2];
        Integer sortOrder = null;
        theKey = all.nextElement().toString();
        theOrgVal = pRsBundle.getString(theKey);
        System.out.println("The key = " + theKey + " :: Val = " + theOrgVal);

        StringTokenizer rep = new StringTokenizer(theOrgVal, "|");
        try
        {
          sortOrderStr = rep.nextToken().trim(); // Get Sort Order
          sortOrder = new Integer(sortOrderStr);
          theVal[0] = theKey;
          theVal[1] = rep.nextToken().trim(); // Get Value
        }
        catch(Exception ex)
        {
  System.err.println(PicklistData.class+":Fallback to sort by Key : " + theOrgVal);
          // Fallback to sort by Key
          sortOrder = new Integer(theKey);
          theVal[0] = theKey;
          theVal[1] = theOrgVal;
        }
        while(sorted.containsKey(sortOrder)) sortOrder = new Integer(sortOrder.intValue()+1);
        sorted.put(sortOrder, theVal);
      }

      System.out.println("Sorted ...");
      Iterator theList = sorted.values().iterator();
      String[] theVal = new String[2];
      while(theList.hasNext())
      {
        theVal = (String[])(theList.next());
        System.out.println("The key = " + theVal[0] + " :: Val = " + theVal[1]);
      }

      System.out.println("Test for System Msg ...");
      pRsBundle = ResourceBundle.getBundle("BXResource.SystemMessage", new Locale("en", ""));

      System.out.println("DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS = " + pRsBundle.getString("DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS"));
      System.out.println("PAGE_ACCESS_DENIED = " + pRsBundle.getString("PAGE_ACCESS_DENIED"));
      System.out.println("CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED = " + pRsBundle.getString("CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED"));

      pRsBundle = ResourceBundle.getBundle("BXResource.SystemMessage", new Locale("fr", ""));

      System.out.println("DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS = " + pRsBundle.getString("DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS"));
      System.out.println("PAGE_ACCESS_DENIED = " + pRsBundle.getString("PAGE_ACCESS_DENIED"));
      System.out.println("CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED = " + pRsBundle.getString("CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED"));

      System.out.println("End ...");
    }
  }  