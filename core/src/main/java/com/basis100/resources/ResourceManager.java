package com.basis100.resources;

// ResourceManager:
//
// This class manages resources used by MOS user session.
//
// . Jdbc Connection Pool - only one (named) connection pool is managed
// . MOS system logger
//
// Initializing - System Startup
// -----------------------------
//
// The resource manager is a singleton - initiailzation is via the init() method which is
// called at system startup:
//
//      public static void init(String configurationFilesPath, String connectionPoolName)
//      public static void init(String configurationFilesPath)
//      public static void init()
//
//      where:  <configurationFilesPath> is the path to resource configuration files (e.g.
//              connection pool ini file, system logger properties file, etc.)
//
//              <connectionPoolName> is the name of the connection pool to instantiate.
//
//  If <configurationFilesPath> is not specified the configuration file path
//  defaults to:
//
//    "\\moscore\\admin\\" // i.e. \moscore\admin\
//
//  If <connectionPoolName> is not specified the connection pool name defaults to "MOS".
//
// Access to Resources
// -------------------
//
// Logger object (see SysLog.java):
//
//      public static Syslogger getSysLogger(String loginId)
//
// JDBC Connection (see JdbcConnection.java)
//
//      public static JdbcConnection getJdbcConnection()
//
// Freeing Resources - Shutdown
// ----------------------------
//  public void shutdown()

import com.basis100.jdbcservices.ConnectionRequestFailedException;
import com.basis100.jdbcservices.JdbcConnection;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.filogix.express.state.ExpressState;

/**
 * @since 3.3
 */
public class ResourceManager {
	private static boolean initDone = false;
	private static String CONFIGURATION_FILES_PATH = "";
	private static String CONNECTION_POOL_NAME = "";

	// Constructor: private - class inmplements singleton
	private ResourceManager() {
	}

	/**
	 * @deprecated
	 */
	public static JdbcConnection getJdbcConnection(ExpressState expressState)
	throws Exception {

		if (initDone == false)
			throw (new ConnectionRequestFailedException(
					"Request for connection failed, Reason: Resource Manager not initiailized"));

		//4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- start
		//return new JdbcConnection(expressState);
		return JdbcConnection.getInstance(expressState);
		//4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- end
		// return JdbcConnectionPool.getConnection(CONNECTION_POOL_NAME,
		// timeoutCallback);
	}

	// bruce: no need for this method, as in all cases true.
	public static void setCloseJdbcConnectionOnRelease(JdbcConnection jCon,
			boolean closeOnRelease) {
		// JdbcConnectionPool.setCloseOnRelease(CONNECTION_POOL_NAME, jCon, closeOnRelease);
	}

	public static void setIndefiniteUseJdbcConnection(JdbcConnection jCon) {
		// JdbcConnectionPool.setIndefiniteUse(CONNECTION_POOL_NAME, jCon);
	}

	public static SysLogger getSysLogger(String loginId) {
		return SysLog.getSysLogger(loginId);
	}

	public static boolean isInitialized() {
		return initDone;
	}

	//used internally only
	public static void init() {

		if (initDone)
			return;

		initDone = true;
	}

	// used internally only
	public static void init(String confFilesPath) {
		if (initDone)
			return;

		if (!(confFilesPath.endsWith("\\") || confFilesPath.endsWith("/"))) {
			if (confFilesPath.indexOf("\\") != -1)
				confFilesPath += "\\";
			else
				confFilesPath += "/";
		}

		CONFIGURATION_FILES_PATH = confFilesPath;

		init();
	}

	// Initialization
	public static void init(String confFilesPath, String connectionPoolName) {
		if (initDone)
			return;

		CONNECTION_POOL_NAME = connectionPoolName;
		init(confFilesPath);
	}

	/**
	 * Methods to release specific resources
	 *
	 * @deprecated
	 */
	public static void releaseJdbcConnection(JdbcConnection jCon)
	throws Exception {
		if (initDone == false)
			throw (new IllegalStateException(
					"Resource.Manager.releaseJdbcConnection: Illegal call - Resource Manager not initiailized"));

		// JdbcConnectionPool.releaseConnection(CONNECTION_POOL_NAME, jCon);
		// bruce: just return it to the pool
		jCon.closeConnection();
	}

	public static void shutdown() {
		initDone = false;

		//      JdbcConnectionPool.shutdown();
		SysLog.shutdown();
	}

	public static String getConfigurationFilePath() {
		return CONFIGURATION_FILES_PATH;
	}

}
