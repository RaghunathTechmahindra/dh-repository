package com.basis100.resources;

import java.sql.Connection;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLogger;
import com.filogix.express.state.ExpressState;

/**
 *
 *
 *
 */
public class SessionResourceWrapper extends SessionResourceKit {
    private boolean txStartedHere = false;
	private SessionResourceKit srk;

	public SessionResourceWrapper(SessionResourceKit srk) {
		//super(srk.getIdentity());
		this.srk = srk;
	}

	public void beginTransaction() throws JdbcTransactionException {
		if(!isInTransaction()) {
			txStartedHere = true;
		    srk.beginTransaction();
		}
	}

	public void cleanTransaction() {
		srk.cleanTransaction();
	}

	public void commitTransaction() throws JdbcTransactionException {
		if (!txStartedHere) return;
		
		if(isInTransaction())
		    srk.commitTransaction();
	}

	public void freeResources() {
		srk.freeResources();
	}

	public int getAuditId() {
		return srk.getAuditId();
	}

	public Connection getConnection() {
		return srk.getConnection();
	}

	public boolean getDiagnosticViaClient() {
		return srk.getDiagnosticViaClient();
	}

	public String getIdentity() {
		return srk.getIdentity();
	}

	public JdbcExecutor getJdbcExecutor() {
		return srk.getJdbcExecutor();
	}

	public boolean getModified() {
		return srk.getModified();
	}

	public SysLogger getSysLogger() {
		return srk.getSysLogger();
	}

	public int getUserProfileId() {
		return srk.getUserProfileId();
	}

	public boolean isAuditOn() {
		return srk.isAuditOn();
	}

	public boolean isInTransaction() throws JdbcTransactionException {
		return srk.isInTransaction();
	}

	public int nextAuditId() throws Exception {
		return srk.nextAuditId();
	}

	public void restoreAuditOn() {
		srk.restoreAuditOn();
	}

	public void rollbackTransaction() throws JdbcTransactionException {
		srk.rollbackTransaction();
	}

	public void setAuditId(int id) {
		srk.setAuditId(id);
	}

	public void setAuditOn(boolean b) {
		super.setAuditOn(b);
	}

	public void setCloseJdbcConnectionOnRelease(boolean closeOnRelease) {
		srk.setCloseJdbcConnectionOnRelease(closeOnRelease);
	}

	public void setDiagnosticViaClient(boolean b) {
		super.setDiagnosticViaClient(b);
	}

	public void setIndefiniteUseJdbcConnection() {
		srk.setIndefiniteUseJdbcConnection();
	}

	public void setInterimAuditOn(boolean b) {
		srk.setInterimAuditOn(b);
	}

	public void setModified(boolean b) {
		srk.setModified(b);
	}

	public void setSysLogger(SysLogger logger) {
		srk.setSysLogger(logger);
	}
	
	public void setLanguageId(int languageId) {
		srk.setLanguageId(languageId);
	}
	
	public int getLanguageId() {
		return srk.getLanguageId();
	}
	
	public int getDealInstitutionProfileId()
	{
	    return srk.getExpressState().getDealInstitutionId();
	}
	
	public void setDealInstitutionProfileId(int dealInstitutionProfileId)
	{
	    srk.getExpressState().setDealInstitutionId(dealInstitutionProfileId);
	}
	
	public ExpressState getExpressState()
	{
	    return srk.getExpressState();
	}

}
