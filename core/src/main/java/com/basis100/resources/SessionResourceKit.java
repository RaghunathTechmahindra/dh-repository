package com.basis100.resources;


import java.io.Serializable;
import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.jdbcservices.JdbcConnection;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.express.state.ExpressState;
import com.filogix.express.state.ExpressStateFactory;

/**
 * 
 * @Since 3.3
 */
public class SessionResourceKit implements Serializable
{
	//4.4, logging first access
    static {
    	DBConnectionSourceLogger.logInitialAccess();
    }
    
    // The logger
    private final static Log _log =
        LogFactory.getLog(SessionResourceKit.class);
    // the express state, we will maintain the VPD state in the express state
    private ExpressState _expressState;

  String  identity;
  int     userProfileId;
  
  String    vpdStmt;
    
  int     auditId           = -1;
  boolean modified          = false;

  boolean auditOn           = true;
  boolean auditOnWas        = true;

  boolean diagnosticViaClient = false;

    private SysLogger logger    = null;
    private JdbcConnection jCon = null;
  private  int languageId = 0;

    //No Args Constructor
    public SessionResourceKit() {

        _expressState = createVoidState();
    }

    /**
     * create a session resource kit for the provided express state.
     *
     * @param state the provided express state.
     * @since 3.3
     */
    public SessionResourceKit(ExpressState state) {

        this();
        if (null != state)
            _expressState = state;
        else
            _expressState = createVoidState();
    }

    /**
     * @since 3.3
     */
    public SessionResourceKit(String identity) {

        this(null, identity);
    }

    /**
     * Special constructor to be used by classes that need SRK before cache is built
     * @param identity
     * @param special
     * @since 3.3
     */
    public SessionResourceKit(String identity, boolean special) {
        this();
        this.identity = identity;

        // R1 - identity == userProfileId
        try
        {
            userProfileId = (new Integer(identity)).intValue();
        }
        catch (Exception e)
        {
          // looks like identity NOT a user profile id - this is typically because of system
          // usage - e.g. identity is "System" or some process identity, e.g. "DocPrep".
          // For these cases we set the profile id as 0.

            userProfileId = 0;
        }

            // create system log access object
            logger = ResourceManager.getSysLogger(identity);

        // set auditOn control values as per system properties
        try
        {
            setAuditOn(true); // default

            setDiagnosticViaClient(false);  // default
        }
        catch (Exception e)
        {
            setAuditOn(true);
            setDiagnosticViaClient(false);
        }
    }

    
    /**
     * @since 3.3
     */
    public SessionResourceKit(ExpressState state, String identity)
    {
        this(state);
    this.identity = identity;

    // R1 - identity == userProfileId
    try
    {
        userProfileId = (new Integer(identity)).intValue();
    }
    catch (Exception e)
    {
      // looks like identity NOT a user profile id - this is typically because of system
      // usage - e.g. identity is "System" or some process identity, e.g. "DocPrep".
      // For these cases we set the profile id as 0.

        userProfileId = 0;
    }

        // create system log access object
        logger = ResourceManager.getSysLogger(identity);

    // set auditOn control values as per system properties
    try
    {
        String YN = PropertiesCache.getInstance().getInstanceProperty("com.basis100.resource.audit.disabled");
//        String YN = PropertiesCache.getInstance().getProperty("com.basis100.resource.audit.disabled");

        setAuditOn(true); // default

        if (YN == null || (YN != null && YN.equals("Y")))
            setAuditOn(false);

        YN = PropertiesCache.getInstance().getInstanceProperty("com.basis100.diagnosticviaclient");
//        YN = PropertiesCache.getInstance().getProperty("com.basis100.diagnosticviaclient");

        setDiagnosticViaClient(false);  // default

        if (YN != null && YN.equals("Y"))
            setDiagnosticViaClient(true);
    }
    catch (Exception e)
    {
        setAuditOn(true);
        setDiagnosticViaClient(false);
    }
    }

    /**
     * create a void express state.
     *
     * @since 3.3
     */
    private ExpressState createVoidState() {

        // create a ExpressState with void VPD status.
        ExpressStateFactory factory = (ExpressStateFactory) ExpressCoreContext.
            getService(ExpressCoreContext.EXPRESS_STATE_FACTORY_KEY);
        return factory.getExpressState();
    }

    /**
     * returns the express state binding to this session resource kit.
     * @since 3.3
     */
    public ExpressState getExpressState() {

        return _expressState;
    }

  //
  // Methods to allow fine grained control of audit records production (application level
  // for when it is know that some activity should not produce an audit trail - not
  // intended for abuse!
  //

  public void setInterimAuditOn(boolean b)
  {
      auditOn = b;
  }

  public void restoreAuditOn()
  {
      auditOn = auditOnWas;
  }

  // setters & getters

  public String getIdentity()
  {
    return identity;
  }

  /**
   * @deprecated
   */
  public int getUserProfileId()
  {
      return userProfileId;
  }
  public int getAuditId()
  {
    return auditId;
  }

  public void setAuditId(int id)
  {
    auditId = id;
  }

  public boolean isAuditOn()
  {
    return auditOn;
  }

  public void setAuditOn(boolean b)
  {
    auditOn = b;
    auditOnWas = b;
  }

  public boolean getDiagnosticViaClient()
  {
    return diagnosticViaClient;
  }

  public void setDiagnosticViaClient(boolean b)
  {
    diagnosticViaClient = b;
  }

  public boolean getModified()
  {
    return modified;
  }

  public void setModified(boolean b)
  {
    modified = b;
  }


    /**
    * @return logger object - service guaranteed not to fail.
  */
  public SysLogger getSysLogger()
    {
    if (logger != null)
          return logger;

    //--> Modified  for Debug and problem investigation
    //--> by Billy 08April2003
    //return ResourceManager.getSysLogger("{System}");
    if(identity != null && identity.length()>0)
    {
      logger = SysLog.getSysLogger(identity);
    }
    else
    {
      logger = SysLog.getSysLogger("{System}");
    }
    logger.warning("@SRK.getSysLogger :: logger was null and it's fixed !!");
    return logger;
    }


  /**
  * Method to set and override the default SysLogger
  */
  public void setSysLogger(SysLogger logger)
  {
    this.logger = logger;
  }

  /**
  *  Determine if the executor in use (obtain one if one not in use) is currently conducting
  *  a transaction.
  */

    public boolean isInTransaction() throws JdbcTransactionException
  {
     JdbcExecutor jExec = getJdbcExecutor();

     if(jExec == null)
       throw new JdbcTransactionException("Could not report tx state - Reason:Failed to get JdbcExecutor");

     return jExec.isInTransaction();
  }


  /**
  *  Begin a Transaction using this SessionResourceKit's JdbcExector object. If
  *  no exector is available attempts to get a new one.
  *  @throws JdbcTransactionException if a exector cannot be obtained or if an
  *  illegal transaction state is encountered.
  */
  public void beginTransaction() throws JdbcTransactionException
  {
     JdbcExecutor jExec = getJdbcExecutor();

     if(jExec != null)
     {
       jExec.begin();
     }
     else
     {
       throw new JdbcTransactionException("Could not begin transaction - Reason:Failed to get JdbcExecutor");
     }

     setModified(false);
  }


  /**
  *  Commit the transaction using this SessionResourceKit's JdbcExector object. If
  *  no exector is available attempts to get a new one.
  *  @throws JdbcTransactionException if a exector cannot be obtained or if an
  *  illegal transaction state is encountered.
  */
  public void commitTransaction() throws JdbcTransactionException
  {
     JdbcExecutor jExec = getJdbcExecutor();

     if(jExec != null)
     {
       jExec.commit();
     }
     else
     {
       throw new JdbcTransactionException("Commit Failed - Failed to get JdbcExecutor");
     }
  }

  /**
  *  Rollback the Transaction using this SessionResourceKit's JdbcExector object. If
  *  no exector is available attempts to get a new one.
  *  @throws JdbcTransactionException if a exector cannot be obtained or if an
  *  illegal transaction state is encountered.
  */
  public void rollbackTransaction() throws JdbcTransactionException
  {
     JdbcExecutor jExec = getJdbcExecutor();

     if(jExec != null)
     {
       jExec.rollback();
     }
     else
     {
       throw new JdbcTransactionException("Rollback Failed - Reason: no JdbcExecutor (=null)");
     }
  }

  /**
  *  Rollback the Transaction (if one) using this SessionResourceKit's JdbcExector object.
  *  This method is the preferred way to cleanup after an aborted transaction as it
  *  does not throw exceptions itself.
  */
  public void cleanTransaction()
  {
     try
     {
     if (this.isInTransaction() == true)
        rollbackTransaction();
     }
     catch (Exception e) {;}
  }


    /**
     * Return the Jdbc resources to ResourceManager connection pool. We don't free the
     * logger resource - beyond the object itself there are no resources being held.
     */
    public void freeResources() {

        if (jCon == null)
            return;

        try {
            if (jCon.isConnected() == true && jCon.isExecutor() == true && isInTransaction()) {
                SysLogger logger = getSysLogger();
                // SEAN Ticket #1734 Dec. 07, 2005: change from error to warning
                logger.warning("********** JDBC Executor returned to pool while still in transaction - transaction rolled back!");
                // SEAN Ticket #1734 END
                try {
                    rollbackTransaction();
                } catch (Exception e) {;}
            }

            jCon.closeConnection();
            jCon = null;
        } catch(Exception e) {
            getSysLogger().error("Error releasing connection @srk.freeResources()");
            getSysLogger().error(e);
        }
    }

  /**
    * Gets a Connection object.  If null is returned this is an indication of
     * failure (i.e. technical problem, or connection not available from pool -
     * all pool connections in use).
    *
    * @return java.sql.Conection object, or null
    *
  */
    public Connection getConnection()
    {
        if (jCon != null) {
            return jCon.getCon();
        }

        // otherwise we get connection from the resource manager's connection pool
        jCon = getJdbcConnection();

        if (jCon == null)
        {
            return null;
        }

        return jCon.getCon();
    }

    /**
     * <P> getJdbcConnection </p>
     * 
     * @version 1.1 
     * Nov 20, 2008: changed to private method.
     * 
     * @return new JdbcConnection object
     * @author MCM team
     */
    //public JdbcConnection getJdbcConnection()
    private JdbcConnection getJdbcConnection()
    {
        JdbcConnection jCon = null;

        try
        {
            //4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- start
            //jCon = new JdbcConnection(_expressState);
            jCon = JdbcConnection.getInstance(_expressState);
            //4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- end
        }
        catch (Exception e)
        {
            getSysLogger().error(e);
            return null;
        }

        return jCon;
    }

    /**
     * Gets a JDBC executor (see JdbcExecutor.java for details).
     * 
     * @return executor object or null. If null is return this is an indication
     *         of failure (i.e. technical problem, or connection not available
     *         from pool - all pool connections in use).
     */
    public JdbcExecutor getJdbcExecutor()
    {
        try {
            if (jCon != null) {
                return jCon.getJdbcExecutor(this);
            }

            jCon = getJdbcConnection();

            if (jCon == null) {
                return null;
            }
            return jCon.getJdbcExecutor(this);
        } catch (Exception e) {
            getSysLogger().error("SRK :: failed to obtain executor");
            getSysLogger().error(e);
            return null;
        }
    }

  /**
   * Specifiy disposition of JDBC connection upon return to the connection pool.
   * 
   * Value: true - connection closed when returned to pool false - connection
   * not closed when returned to pool.
   * 
   */

    public void setCloseJdbcConnectionOnRelease(boolean closeOnRelease)
    {
    if (jCon == null)
      return;

    ResourceManager.setCloseJdbcConnectionOnRelease(jCon, closeOnRelease);
    }

  /**
    * Specifiy that JDBC connection managed will be used for indefinite period of time -
  * not subject to usage timeout.
  *
  */

    public void setIndefiniteUseJdbcConnection()
    {
    if (jCon == null)
      return;

    ResourceManager.setIndefiniteUseJdbcConnection(jCon);
    }



  /**
    * Gets next available audit id from DB sequence.
  *
  */

  public int nextAuditId() throws Exception
  {
      int nextId = -1;

      JdbcExecutor jExec = getJdbcExecutor();

      String sql = "Select auditSeq.nextval from dual";

      int key = jExec.execute(sql);

      for (; jExec.next(key);  )
      {
         nextId = jExec.getInt(key,1);

         break;  // can only be one record ...  but ...
      }

      jExec.closeData(key);

      return nextId;
  }

  public int getLanguageId()
  {
         return languageId;
  }

  public void setLanguageId(int languageId)
  {
         this.languageId = languageId;
  }

  public String toString(){
      String str = "";

      str = "srk.toString: identity = " + identity + ", userProfileId = " + userProfileId;
      
      return str;
  }
  
  public String getActualVPDStateForDebug(){
      getConnection();
      if (jCon == null)
          return "jCon is null";
      return jCon.getActualVPDStateForDebug();
  }
  
}
