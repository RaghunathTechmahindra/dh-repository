package com.basis100.resources;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.util.StringUtil;
import com.basis100.jdbcservices.JdbcConnection;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.file.PropertiesReader;
import com.filogix.express.cache.IPropertiesCache;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.express.core.ExpressRuntimeException;

/**
 * 
 * PropertiesCache
 * 
 * @version 1.0 Oct 2, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class PropertiesCache {
    
    //the logger
    private final static Log _log = LogFactory
            .getLog(PropertiesCache.class);

    public static final String       SYSTEM_PROPERTY_CACHE_KEY      = "systemPropertyCache";

    private static IPropertiesCache  sysProp;

    protected static PropertiesCache instance = new PropertiesCache();

    protected SessionResourceKit     _srk;

    private static boolean useOldDCHTable = true;
    
    private static int defaultInstId = 0;
    
    /**
     * get an instance of the singleton
     * @return
     */
    public static synchronized PropertiesCache getInstance() {
        if( instance == null) throw new ExpressRuntimeException("instance is null");
        return instance;
    }

    /**
     * protected constructor 
     */
    protected PropertiesCache() {
        init();
    }

    /**
     * Initialize the common variables
     */
    public void init() {
        _srk = new SessionResourceKit("Cache", true);
        sysProp = (IPropertiesCache) ExpressCoreContext
                .getService(SYSTEM_PROPERTY_CACHE_KEY);
        
        String sql = "select min(institutionprofileid) from institutionprofile"
        			+ " where profilestatusid = 0";
        try
        {
        	//JdbcExecutor jExec = _srk.getJdbcExecutor();
        	JdbcExecutor jExec = JdbcConnection.getInstance(null).getJdbcExecutor();
        	int key = jExec.execute(sql);
        	if (jExec.next(key))
        		defaultInstId = jExec.getInt(key, 1);
        	else
        	{
        		defaultInstId = 0;
        		_log.error("Properties Cache could not retrieve lowest institution profile id");
        	}
        }
        catch (Exception e)
        {
        	defaultInstId = 0;
        	_log.error("Properties Cache could not retrieve lowest institution profile id, is database available?", e);
        }
        
    }

    /**;
     * return the instance property.
     */
    public String getInstanceProperty(String name) {
        String property = sysProp.getProperty(name);
        _log.debug("PropertiesCache getting Instance Property name: " + name
                + " value: " + property);
        return property;
    }

    /**
     * return the instance property.
     */
    public String getInstanceProperty(String name, String defaultVal) {

        String value = getInstanceProperty(name);
        if (value == null) {
            value = defaultVal;
        }
        _log.debug("PropertiesCache getting Instance Property with (defaultVal) name:  "
                + name + " value: " + value);
        return value;
    }

    /**
     * 
     * @param name
     * @return
     * @deprecated
     */
/*    public String getProperty(String name) {

        return getInstanceProperty(name);
    }*/
    
    /**
     * 
     * @param name
     * @param defaultVal
     * @return
     * @deprecated
     */
/*    public String getUnchangedProperty(String name, String defaultVal) {

        return getInstanceProperty(name, defaultVal);
    }
*/
    /**
     * 
     * @param name
     * @param defaultVal
     * @return
     * @deprecated
     */
/*    public String getProperty(String name, String defaultVal) {

        return getInstanceProperty(name, defaultVal);
    }
*/
    /**
     * get the property from the cache based on intitution ID and name
     * @param institutionId
     * @param name
     * @return the value from the cache
     */
    public String getProperty(int institutionId, String name) {
        
        if (institutionId == -1)
        	institutionId = defaultInstId;
        
    	String value = StringUtil.setSystemPathSeparator(sysProp.getProperty(
                institutionId, name));
        _log.debug("PropertiesCache getting Property with name:" + name
                + " value: " + value + " institutionid: " + institutionId);
        return value;
    }

    /**
     * get the property from the cache based on intitution ID and name
     * @param institutionId
     * @param name
     * @param defaultVal
     * @return value from the cache. Default value if the value in cache is null
     */
    public String getProperty(int institutionId, String name, String defaultVal) {

        if (institutionId == -1)
        	institutionId = defaultInstId;
        
    	String val = getProperty(institutionId, name);
        String property = (val == null) ? defaultVal : val;
        _log.debug("PropertiesCache getting Property with name:" + name
                + " value: " + property + " institutionid: " + institutionId);
        return property;
    }

    /**
     * get the property from the cache based on intitution ID and name
     * @param institutionId
     * @param name
     * @param defaultVal
     * @return value from the cache. Default value if the value in cache is null
     */
    public String getUnchangedProperty(int institutionId, String name,
            String defaultVal) {

        if (institutionId == -1)
        	institutionId = defaultInstId;
        
    	String val = sysProp.getProperty(institutionId, name.trim());
        String property = (val == null) ? defaultVal : val;
        _log.debug("PropertiesCache getting UnchangedProperty with name:"
                + name + " value: " + property + " institutionid: "
                + institutionId);
        return property;
    }

    /**
     * 
     * @param file
     * @throws IOException
     * @deprecated
     */
    public static void addPropertiesFile(String file) throws IOException {
        // Do nothing. We don't need it anymore
    }

    /**
     * 
     * @return
     * @deprecated
     */
    public PropertiesReader getLoadedProperties() {
        // TODO:PropertiesPanel needs to be changed to get properties the new way
        return new PropertiesReader();
    }

    /**
     * 
     * @return
     * @deprecated
     */
    public PropertiesReader getProperties() {
        // TODO:PropertiesPanel needs to be changed to get properties the new way
        return new PropertiesReader();
    }
    
    /**
     * Clear the cache
     */
    public void flush() {
        sysProp.flush();
    }
    
    public static void setUseOldDCHTable(boolean value)
    {
    	useOldDCHTable = value;
    }
    
    public static boolean getUseOldDCHTable()
    {
    	return useOldDCHTable;
    }

}
