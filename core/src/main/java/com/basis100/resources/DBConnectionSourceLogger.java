package com.basis100.resources;

import java.io.File;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.core.ExpressCoreContext;

/**
 * <em>DBConnectionSourceLogger</em>
 * <p>
 * This class logs host machine�s info into DBCONNECTIONSOURCE table in order to
 * identify which program is connecting to the same database. This class is
 * intended to be used just once per start up.
 * </p>
 * 
 * @since 4.4
 * @version 1.0 Sep 15, 2010, Hiro, initial version
 * 
 */
public class DBConnectionSourceLogger {

	public static void logInitialAccess() {
		
		Log log = LogFactory.getLog(DBConnectionSourceLogger.class);
		try {
			String path = new File(".").getAbsolutePath();
			InetAddress inetAddr = InetAddress.getLocalHost();
			String ip = inetAddr.getHostAddress();
			String name = inetAddr.getHostName();
			
			//fit to the max size in db fields
			path = StringUtils.right(path, 500);	
			name = StringUtils.left(name, 100);

			DataSource dataSource = (DataSource)ExpressCoreContext
				.getService(ExpressCoreContext.EXPRESS_DATASOURCE_KEY);
			Connection con = dataSource.getConnection();
			try{
				//use auto-commit, whatever happens no roll back is required
				con.setAutoCommit(true); 
				storeData(con, ip, path, name, log);
			}finally{
				con.close();
			}

		} catch (Exception e) {
			//this is just a nice to have utility, no error should be thrown outside.
			log.error("Failed to update/insert DBConnectionSource record", e);
		}
	}

	private static void storeData(Connection con, String ip, String path, String name, Log log) throws SQLException{

		String updSql = "UPDATE DBCONNECTIONSOURCE set RECENTTIMESTAMP = sysdate, " +
		" count = count + 1 where ip = '" + ip + "' and  path = '" + path + "'";
		Statement stat = con.createStatement();
		try{
			int count = stat.executeUpdate(updSql);
			log.info("DBConnectionSourceLogger storeData sql = " + updSql);
			//when no data is updated
			if(count <= 0){
				String insSql = "INSERT INTO DBCONNECTIONSOURCE " +
				"(ID, IP, NAME, PATH, INITIALTIMESTAMP, RECENTTIMESTAMP, COUNT) " +
				"VALUES (DBCONNECTIONSOURCE_seq.nextval, '" 
				+ ip + "', '" + name + "', '" + path + "',  sysdate, sysdate, 1 )"; 
				
				stat.executeUpdate(insSql);
				log.info("DBConnectionSourceLogger storeData sql = " + insSql);
			}
		}finally{
			stat.close();
		}
	}

	public static void main(String[] args){
		DBConnectionSourceLogger.logInitialAccess();
	}

}
