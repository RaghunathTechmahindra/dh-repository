package com.basis100.mail;

public class MailException extends Exception
{
  private Throwable rootException;

 /**
  * Constructs a <code>DeliveryException</code> with no specified detail message.
  */

  public MailException()
  {
	  super();
  }

  /**
  * Constructs a <code>DeliveryException</code> with the specified message.
  * @param   msg  the related message.
  */
  public MailException(String msg)
  {
    super(msg);
  }

  /**
  * Constructs a <code>DeliveryException</code> with the specified message.
  * @param   msg  the related message.
  * @param   ex   the root Exception that led to the throwing of this instance
  */
  public MailException(String msg, Throwable ex)
  {
    //This RootException is not needed because JDK introduced
    //'cause' in 1.4. RootException can be removed if desired.
    super(msg, ex);
    setRootException(ex);
  }


  public void setRootException(Throwable r)
  {
    this.rootException = r;
  }


  public Throwable getRootException(){return this.rootException;}
} 
