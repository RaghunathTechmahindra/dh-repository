package com.basis100.mail;

public class DeliveryException extends MailException
{
  private Throwable rootException;

 /**
  * Constructs a <code>DeliveryException</code> with no specified detail message.
  */

  public DeliveryException()
  {
	  super();
  }

  /**
  * Constructs a <code>DeliveryException</code> with the specified message.
  * @param   msg  the related message.
  */
  public DeliveryException(String msg)
  {
    super(msg);
  }

  /**
  * Constructs a <code>DeliveryException</code> with the specified message.
  * @param   msg  the related message.
  * @param   ex   the root Exception that led to the throwing of this instance
  */
  public DeliveryException(String msg, Throwable ex)
  {
    super(msg);
    setRootException(ex);
  }


  public void setRootException(Throwable r)
  {
    this.rootException = r;
  }


  public Throwable getRootException(){return this.rootException;}
} 