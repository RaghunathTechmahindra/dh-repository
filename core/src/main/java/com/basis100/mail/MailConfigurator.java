package com.basis100.mail;

/**
 * 11/Nov/2005 DVG #DG360 #2408  Review and/or correct "System.getProperty" calls globally
 */

import java.io.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;

import com.basis100.resources.*;
import com.basis100.xml.*;

/**
 * The MailConfigurator is responsible for holding mail configuration attributes. The default mail <br/>configurator reads the Mail attributes from the application.xml file.
 * <p/>
 * The application.xml file location is determined using the resource.init location supplied in the <br/>mossys.properties file.
 * @author Brad Hadfield
 * @see SimpleMailMessage
 */
public class MailConfigurator
{
  public static boolean init = false;
  public static boolean debug = false;

  private static Map defaultMessages;

  private static Map addressSets;

  private static Properties mailProperties;

  private static Session session = null;

  private MailConfigurator(){;}

  public static void initializeMailSystem()throws MailException
  {
    if(init) return;

    mailProperties = new Properties();

    defaultMessages = new HashMap();
    addressSets = new HashMap();

    try
    {

      DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();
            
      Document config = parser.parse(ClassLoader.getSystemResourceAsStream("application.xml")); 

      Element mail = DomUtil.getFirstNamedDescendant(config, "mail");

      if(mail == null) throw new MailException("No Mail configuration parameters");

      String verbose = DomUtil.getChildValue(mail, "verbose");
     // System.setProperty("javax.activation.debug",verbose);

      if(verbose.equalsIgnoreCase("true"))
       debug = true;

      String transport = DomUtil.getChildValue(mail, "transport");

      Element hostElement = (Element)DomUtil.getFirstNamedChild(mail, "host");

      if(hostElement == null)
       throw new MailException("No Mail Host stipulated!");

      String host = DomUtil.getElementText(hostElement);

      if(hostElement == null)
       throw new MailException("No Mail Host stipulated!");

      String port = hostElement.getAttribute("port");

      if(port != null && port.trim().length() != 0)
       mailProperties.setProperty("mail.smtp.port", port);
      else
       mailProperties.setProperty("mail.smtp.port", "25");

      //since only smtp is supported presently
      if(transport == null || transport.equals("smtp"))
      {
        System.setProperty("mail.smtp.class","com.sun.mail.smtp.SMTPTransport");
        mailProperties.setProperty("mail.smtp.class","com.sun.mail.smtp.SMTPTransport");
        mailProperties.setProperty("mail.smtp.host", host);
        mailProperties.setProperty("mail.transport.protocol", transport);
      }
      else
       throw new MailException("Unsupported Transport Method stipulated: " + transport);

      Element fromElement = (Element)DomUtil.getFirstNamedChild(mail, "mailfrom-default");


      String from  = DomUtil.getElementText(fromElement);
      String to = DomUtil.getChildValue(mail, "mailto-default");

      try
      {
        InternetAddress.parse(from);
      }
      catch(Exception e)
      {
        init = false;
        throw new MailException( "Illegal default source address stipulated: " + from, e);
      }

      try
      {
        InternetAddress.parse(to);
      }
      catch(Exception e)
      {
        init = false;
        throw new MailException( "Illegal default destination address stipulated: " + to, e);
      }

      mailProperties.setProperty("com.basis100.sysmail.from", from);
      mailProperties.setProperty("mail.smtp.from", from);
      mailProperties.setProperty("com.basis100.sysmail.to", to);

      String user     = fromElement.getAttribute("user");
      String password = fromElement.getAttribute("password");

      if(user != null && user.trim().length() != 0)
       mailProperties.setProperty("mail.smtp.user", user);

      if(password != null && password.trim().length() != 0)
       mailProperties.setProperty("com.basis100.sysmail.password", password);

      mailProperties.setProperty("com.basis100.sysmail.init", "true");

      List messages = DomUtil.getNamedChildren(mail, "message");

      Iterator it = messages.iterator();

      while(it.hasNext())
      {
         Element current = (Element)it.next();

         String id = current.getAttribute("id");

         defaultMessages.put(id, current);
      }

      List addresses = DomUtil.getNamedChildren(mail, "address-list");

      Iterator ait = addresses.iterator();
      List addr = null;

      while(ait.hasNext())
      {
         Element current = (Element)ait.next();

         String id = current.getAttribute("id");

         addr = (List)DomUtil.getNamedChildren(current, "address");

         Iterator addit = addr.iterator();
         Element currentAddrElem = null;

         Set addrStrings = new HashSet();

         while(addit.hasNext())
         {
           currentAddrElem = (Element)addit.next();
           addrStrings.add(DomUtil.getElementText(currentAddrElem));
         }

         addressSets.put(id, addrStrings);
      }

    }
    catch(Exception e)
    {
       init = false;
       throw new MailException("Configurator Failure - Reason: " + e.getMessage(), e);
    }

    init = true;
  }

  /**
   *  Sets the address and subject components to the provided SimpleMailMessage as
   *  defined in the xml configuration file.
   */

  public static SimpleMailMessage addressDefaultMessage(String id, SimpleMailMessage msg) throws MailException
  {
    Element e = (Element)defaultMessages.get(id);

    if(e == null)
     throw new MailException("No such default type configured: " + id);

    String sub = e.getAttribute("subject");
    String from = e.getAttribute("mailfrom");
    String to   = e.getAttribute("mailto");
    String text = DomUtil.getNodeText(e) + " ";

    //Xceed Delta 360: Unable to use attribute 'subject' of tag 'message' in application.xml
    //                 because of the following line.
    //if(sub != null) sub = id;

    msg.setSubject(sub);

    //#DG360 rewritten
    if(from == null)
      from = PropertiesCache.getInstance().getProperty(-1, "com.basis100.sysmail.from");
    msg.setFrom(from);

    if(to != null)
    {
      String cis = to.toLowerCase();

      if(cis.startsWith("list:"))
      {
         to = to.substring(5);

         Set addr = (Set)addressSets.get(to);

         if(addr == null || addr.isEmpty())
         {
            //#DG360 msg.appendTo( System.getProperty("com.basis100.sysmail.to"));
            msg.appendTo( PropertiesCache.getInstance().getProperty(-1, "com.basis100.sysmail.to"));
         }
         else
         {
           msg.appendTo(addr);
         }

      }
      else
      {
        msg.appendTo(to);
      }
    }
    else
     msg.appendTo( mailProperties.getProperty("com.basis100.sysmail.to"));

    if(text == null)
      text = "\nBasisExpress Default Message: " + id;

      msg.appendText("\n" + text);

    return msg;
  }


  public static Set getAddressSet(String id) throws MailException
  {
    return (Set)addressSets.get(id);
  }

  public static void setNewHost(String host)
  {
    mailProperties.setProperty("mail.smtp.host", host);
  }

  public static Properties getProperties()
  {
    return mailProperties;
  }

}
