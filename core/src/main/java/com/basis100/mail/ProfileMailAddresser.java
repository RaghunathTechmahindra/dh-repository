package com.basis100.mail;

import java.util.*;
import java.io.*;

import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.deal.entity.*;

import com.basis100.mail.SimpleMailMessage;

/**
 * Convienience class for addressing SimpleMailMessages to Profile.contacts
 */
public class ProfileMailAddresser
{
  private SessionResourceKit srk = null;
  private SysLogger logger = null;

  public ProfileMailAddresser(SessionResourceKit kit)
  {
    srk = kit;
    logger = srk.getSysLogger();
  }

  /**
   * Addresses the supplied mail message with the contactEmailAddress for the given Contact. The Profile may be of type (or combination of types) of User, Party, Branch, Region, Institution, SourceFirm or SourceOfBusiness.
   * @param msg - the message to be addressed
   * @param profiles - the profile to obtain the address from
   * @see SimpleMailMessage
   * @exception MailException   
   *
   */
  public void addressMessage(SimpleMailMessage msg, Collection profiles)
                                  throws MailException
  {

    if(profiles == null || profiles.isEmpty())
     throw new MailException("No recipient profiles recieved in DefaultMail");

    if(msg == null)
      throw new MailException("Undefined message type requested");

    Contact contact;

    Iterator upit = profiles.iterator();

    while(upit.hasNext())
    {
      try
      {
         contact = getProfileContact((DealEntity)upit.next());

         String mailto = contact.getContactEmailAddress();
         mailto = mailto.trim();
         msg.appendTo(mailto);
      }
      catch(Exception e)//catch nulls and unfound - considered benign;
      {
        logger.info("Benign Error in DefaultMail no Contact or Contact.email found in one or more supplied profiles");
        continue;
      }
    }
  }

   /**
   * Addresses the supplied mail message with the contactEmailAddress for the given Contact. The Profile may be of type (or combination of types) of User, Party, Branch, Region, Institution, SourceFirm or SourceOfBusiness.
   * @param msg - the message to be addressed
   * @param profiles - the profile to obtain the address from
   * @see SimpleMailMessage
   * @exception MailException   
   *
   */
  public void addressMessage(SimpleMailMessage msg, DealEntity profile)
                                  throws MailException
  {

    if(profile == null)
      throw new MailException("No recipient profiles recieved in DefaultMail");

    if(msg == null)
      throw new MailException("Undefined message type requested");

    try
    {
       Contact contact = getProfileContact(profile);

       String mailto = contact.getContactEmailAddress();
       mailto = mailto.trim();
       msg.appendTo(mailto);
    }
    catch(Exception e)
    {
      String m = "Benign Error in DefaultMail no Contact or Contact.email found in supplied profile";
      throw new MailException(m);
    }

  }



  private Contact getProfileContact(DealEntity de) throws Exception
  {
     String name = de.getEntityTableName();

     if(name.equals("GroupProfile"))
      return ((GroupProfile)de).getContact();
     else if(name.equals("UserProfile"))
      return ((UserProfile)de).getContact();
     else if(name.equals("BranchProfile"))
      return ((BranchProfile)de).getContact();
     else if(name.equals("PartyProfile"))
      return ((PartyProfile)de).getContact();
     else if(name.equals("LenderProfile"))
      return ((LenderProfile)de).getContact();
     else if(name.equals("InstitutionProfile"))
      return ((InstitutionProfile)de).getContact();
     else if(name.equals("SourceFirmProfile"))
      return ((SourceFirmProfile)de).getContact();
     else if(name.equals("SourceOfBusinessProfile"))
      return ((SourceOfBusinessProfile)de).getContact();
     else
      return null;

  }

}