package com.basis100.mail;

import java.util.*;
import java.io.*;

import javax.mail.*;
import javax.mail.event.*;
import javax.mail.internet.*;
import javax.activation.*;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import com.basis100.xml.*;
import com.basis100.log.*;
import com.basis100.resources.*;



/**
 *   This class wraps and condenses attributes of the javamail api that <br>
 *   allow the simple the creation, manipulation and sending of text messages with or without attachments. <br>
 *   The use of this class requires mail.jar and activation.jar. <P>
 *
 *   To use the mailing facility construct a message and address it using one of the append <br>
 *   functions. <br>    Append text and files as desired and send the message. <br/>
 *   <p>
 *  This class relies on the the existence of a fully initialized MailConfigurator. If
 *  The System property "com.basis100.sysmail.init" does not have the value 'true' then<br/>
 *  a the MailConfigurator will initialize the mail system.
 *
 */
public class SimpleMailMessage
{
    protected InternetAddress[] recipients = null;
    protected Set mailbcc   = null;
    protected Set mailcc    = null;
    protected Set mailto   = null;
    protected String mailsubject = null;
    protected String mailfrom  = null;
    protected StringBuffer bodypart;

    protected Message message;
    protected Session session;

    protected List parts;
    protected String url   = null;

    public SysLogger maillogger;

    protected static final String mailer = "msgsend";

    /**
     * Constructs a SimpleMailMessage. Checks to see if the mail system is initialized. If not then asks MailConfigurator to attempt configuration. If the config fails MailException is thrown.
     */
    public SimpleMailMessage() throws MailException
    {

      maillogger = ResourceManager.getSysLogger("Mail");

      MailConfigurator.initializeMailSystem();

      session = Session.getDefaultInstance(MailConfigurator.getProperties(), null);

      session.setDebug(MailConfigurator.debug);

      parts = new ArrayList();

      message = new MimeMessage(session);
    }

  /**
   * <pre>
   *  Set address components for this SimpleMailMessage instance.
   *  @param subject: the subject string
   *  @param from:    the mail address of the sender
   *  @param to:      mail address String(s) describing the recipients of this message.
   *  @param cc:      mail address String(s) describing cc recipients of this message.
   *  @param bcc      mail address String(s) describing bcc recipients of this message.
   *  </pre>
   */
  private void address() throws DeliveryException
  {

     if(!isAddressSet())
     {
       String missing = null;

       if(mailto == null)
        missing = "No destination address found";

       if(mailfrom == null)
        if(missing == null) missing = "No source address found";
        else missing += " and no source address found";


       throw new DeliveryException("Can't send an Unaddressed Mail Message -" + missing);
     }

     try
     {
        InternetAddress[] toaddr = null;
        InternetAddress[] ccaddr = null;
        InternetAddress[] bcaddr = null;
        int tolen = 0;

        message.setFrom(new InternetAddress(mailfrom));

        String addrTo = buildAddressString(mailto);

        toaddr = InternetAddress.parse(addrTo, false);
        tolen = toaddr.length;

        message.setRecipients(Message.RecipientType.TO,toaddr);
        if(addrTo != null ){
          log("Setting address - " + addrTo);
        } else {
          log("Setting address - addrTo is null" );
        }

        if(isSubjectSet())
         message.setSubject(mailsubject);
        else
         message.setSubject("");

        if (mailcc != null && !mailcc.isEmpty())
        {
            String addrCC = buildAddressString(mailcc);
            ccaddr = InternetAddress.parse(addrCC, false);
            message.setRecipients(Message.RecipientType.CC,	ccaddr);
        }
        if (mailbcc != null)
        {
            String addrBCC = buildAddressString(mailcc);
            bcaddr = InternetAddress.parse(addrBCC, false);
            message.setRecipients(Message.RecipientType.BCC, bcaddr);
        }

       // Address[] add = this.message.getAllRecipients();

      //  log("Message addressed to " + add.length + " recipients");


     }
     catch(AddressException ae)
     {
       String msg = "Error handling internet address while addressing mail message to: " + mailto;

       if(ae.getMessage() != null)
         msg += ae.getMessage();

       DeliveryException de = new DeliveryException(msg);
       de.setRootException(ae);

       maillogger.error("MAIL: " + de.getMessage());
       maillogger.error(de);
       maillogger.error(ae);

       throw de;

     }
     catch(MessagingException me)
     {
       String mess = "Messaging Exception while addressing mail message.";

       if(me.getMessage() != null)
         mess += "Exception Message " + me.getMessage();

       DeliveryException de = new DeliveryException(mess);
       de.setRootException(me);

       maillogger.error("MAIL: " + de.getMessage());
       maillogger.error(de);
       maillogger.error(me);

       throw de;
     }

  }

  private String buildAddressString(Set addresses)
  {

     StringBuffer addrBuff = new StringBuffer();

     if(addresses != null && !addresses.isEmpty())
     {
       Iterator li = addresses.iterator();
       int count = 0;
       int last = addresses.size();

       while(li.hasNext())
       {
         String current = (String)li.next();
         addrBuff.append(current);
         count++;

         if(count != last)
          addrBuff.append(",");
       }

     }

     return addrBuff.toString();
  }



 /**
  *  Attach the named file to this message.
  * @param filename: the file path and name
  * @throws DeliveryException: if an error occurs during file attachment
  */
  public void attachFile(String filename) throws DeliveryException
  {
    try
    {
       MimeBodyPart part = new MimeBodyPart();

       // attach the file to the message
       FileDataSource fds = new FileDataSource(filename);
       part.setDataHandler(new DataHandler(fds));
       part.setFileName(fds.getName());
       parts.add(part);
       log("Attaching file: " + filename);
    }
    catch(MessagingException me)
    {
       String msg = "Messaging Exception while attaching file: " + filename;
       DeliveryException de = new DeliveryException(msg);
       de.setRootException(me);

       maillogger.error("MAIL: " + de.getMessage());
       maillogger.error(de);
       maillogger.error(me);

       throw de;
    }
  }

 /**
  *  Attach a file to this message.
  * @param filename: the file to attach
  */
  public void attachFile(File file) throws DeliveryException
  {
     attachFile(file.getAbsolutePath());
  }


 /**
  *  Append some text to the body of this message. Text is buffered
  *  and inserted when send() is called.
  *  @param the text to append.
  *
  */
  public void appendText(String s)
  {
     if(bodypart == null) bodypart = new StringBuffer(s);
     else
      bodypart.append(s).append(' ');
  }

  // Catherine, #1053 start ---------------------------------------------------------------------------------
  /**
   *  Append some text as a new line to the body of this message. 
   *  Text is buffered and inserted when send() is called.
   *  @param the text to append.
   *
   */
   public void appendTextLine(String s)
   {
      if(bodypart == null) bodypart = new StringBuffer(s);
      else
       bodypart.append(s).append('\n');
   }
   // Catherine, #1053 end ---------------------------------------------------------------------------------

  public void setText(String s)
  {
    bodypart = null;
    bodypart = new StringBuffer(s);
  }

  /**
  *  Append some text to the main body of this message. Text is buffered
  *  and inserted when send() is called.
  *  @param the text to append.
  *
  */
  public void attachTextPart(String s)throws MessagingException
  {
    MimeBodyPart textPart = new MimeBodyPart();
    textPart.setText(s);

    parts.add(textPart);
  }

  public void setSubject(String sub)
  {
    if(sub == null)sub = "";

    mailsubject = sub;
  }

  public boolean setFrom(String fr)
  {
    if(fr == null)
      return false;

    fr = fr.trim();

    if(fr.indexOf('@') == -1)
      return false;

    mailfrom = fr;

    return true;

  }

  public void clearTo()
  {
    mailto = new HashSet();
  }

  public void clearCC()
  {
    mailcc = new HashSet();
  }

  public void clearBCC()
  {
    mailbcc = new HashSet();
  }

  public void clearRecipients()
  {
     clearTo();
     clearCC();
     clearBCC();
  }


  public boolean appendTo(String addr)
  {
     if(addr == null)
      return false;

     addr = addr.trim();

     if(addr.indexOf('@') == -1)
      return false;

     if(mailto == null)
       mailto = new HashSet();

     mailto.add(addr);

     return true;
  }

  public boolean appendTo(Set addr)
  {
    if(addr == null || addr.isEmpty()) return false;
      boolean all = true;

    try
    {
      Iterator it = addr.iterator();

      while(it.hasNext())
      {
        String current = (String)it.next();

        boolean result = appendTo(current);

        if(result == false)
          all = false;
      }
    }
    catch(ClassCastException cex)
    {
       maillogger.error("MAIL: Entry in address list is not a java.lang.String." );
       maillogger.error(cex);

       return false;
    }

    return all;
  }

  public boolean appendConfigAddressList(String id) throws MailException
  {
    return appendTo(MailConfigurator.getAddressSet(id));
  }

  public boolean appendCC(String recp)
  {
     if(recp == null)
      return false;

     recp = recp.trim();

     if(recp.indexOf('@') == -1)
      return false;

     if(mailcc == null)
       mailcc = new HashSet();

     mailcc.add(recp);

     return true;
  }

 public boolean appendBCC(String recp)
 {
     if(recp == null)
      return false;

     recp = recp.trim();

     if(recp.indexOf('@') == -1)
      return false;

     if(mailbcc == null)
       mailbcc = new HashSet();

     mailbcc.add(recp);


     return true;
 }

  /**
  *  Assembles and sends this message. The system propety describing the SMTP
  *  host must be set - "mail.smtp.host" . This can be accomplished using the
  *  MessageMailer to send this message.
  *
  *  @throws DeliverException if an error occurs assembling or sending this message.
  *  @see com.basis100.mail.MailManager
  */
  public void send(Object eventListener)throws DeliveryException
  {
     try
     {
      Transport transport = session.getTransport();

      address();

      message.setHeader("X-Mailer", mailer);
      message.setSentDate(new Date());

      Multipart mp = new MimeMultipart();
      MimeBodyPart textPart = new MimeBodyPart();

      if(isBodySet())
       textPart.setText(bodypart.toString());

      mp.addBodyPart(textPart);

      ListIterator li = parts.listIterator();

      while(li.hasNext())
      {
         mp.addBodyPart((MimeBodyPart)li.next());
      }

      message.setContent(mp);

      if(eventListener != null)
      {
        if(eventListener instanceof ConnectionListener)
         transport.addConnectionListener((ConnectionListener)eventListener);

        if(eventListener instanceof TransportListener)
         transport.addTransportListener((TransportListener)eventListener);
      }

      // connect the transport
      if(!transport.isConnected())
      {
        transport.connect();
      }

      message.saveChanges();

      if(!transport.isConnected())
       throw new DeliveryException("Failed to connect to email Transport." + transport.getURLName());

      // send the message
      try
      {
         transport.sendMessage(message, message.getAllRecipients());
      }
      catch(SendFailedException sfe)
      {
        log("Mail sent to valid addresses:" + sfe.getValidSentAddresses());
        log("Mail not sent to valid addresses: " + sfe.getValidUnsentAddresses());
        log("Mail not sent - invalid addresses: " + sfe.getInvalidAddresses());

        //  SCR #458 - Added to be able to handle this exception higher up,
        //  by sending an error message to relevant parties.
        throw sfe;
      }

      // give the EventQueue enough time to fire its events
      try {Thread.sleep(5);}catch(InterruptedException e) {}

      transport.close();

    }
    catch(MessagingException e)
    {
       String msg = "Error sending mail message. ";

       if(e.getMessage() != null) msg += "Exception Message: " + e.getMessage();

       DeliveryException de = new DeliveryException(msg);
       de.setRootException(e);

       maillogger.error("MAIL: " + de.getMessage());
       maillogger.error(de);
       maillogger.error(e);

       throw de;
    }
  }


 /**
  * Indicates whether the address for this message has be set.
  * @return false if an address(..) method has not be successfully called on
  * this instance - else return true
  */
  public boolean isAddressSet()
  {
    return (mailto != null) && (mailfrom != null);
  }

  /**
  * Indicates whether the address for this message has be set.
  * @return false if an address(..) method has not be successfully called on
  * this instance - else return true
  */
  public boolean isSubjectSet()
  {
    return (mailsubject != null);
  }

  /**
  * Indicates whether the address for this message has be set.
  * @return false if an address(..) method has not be successfully called on
  * this instance - else return true
  */
  public boolean isBodySet()
  {
    return (bodypart != null);
  }

  public String getSessionProperties()
  {
    Properties p = session.getProperties();

    StringWriter sw = new StringWriter();

    p.list(new PrintWriter(sw));

    return sw.toString();
  }

  public void makeDefault(String type) throws MailException
  {
     MailConfigurator.addressDefaultMessage(type, this);
  }

  private void log(String message)
  {
    if(MailConfigurator.debug)
    {
      message = "MAIL: " + message;
      maillogger.debug(message);
      System.out.println(message);
    }
  }

  public String getTo()
  {
     String str = "";

     if (mailto != null)
     {
        Iterator i = mailto.iterator();

        String address;

        while (i.hasNext())
        {
            address = (String)i.next();

            str += address;

            if (i.hasNext())
               str += ";";
        }
     }
     return str;
  }

  public Set getCC() { return mailcc; }
  public Set getBCC() { return mailbcc; }

  public String getFrom() { return mailfrom; }

  public String getSubject() { return mailsubject; }
}