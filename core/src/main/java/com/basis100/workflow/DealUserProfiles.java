package com.basis100.workflow;

// DealUserProfiles:
//
// Convienence class for holding the profiles of the users assigned to a deal. These include:
//
//   . administrator assigned to the deal
//   . underwriter assigned to the deal]
//   . deal current approver
//   . funder assigned to the deal (added by BILLY 14Jan2002
//

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

public class DealUserProfiles
{
	Deal deal;
	SessionResourceKit srk;

  private int underwriterId   = 0;
  private int administratorId = 0;
  //Added Funder -- By BILLY 14Jan2002
  private int funderId        = 0;

  //--> Ticket#135 : Ensure to hunt for Admin and Funder with the SOB/Firm group
  //--> Added SorceGroupId to store the group id from SOB / Firm to Group relationship
  //--> By Billy 27Nov2003
  private int SourceGroupId   = -1;
  //============================================================================

	private UserProfile underwriterProfile = null;
	private UserProfile administratorProfile = null;
  //Added Funder -- By BILLY 14Jan2002
  private UserProfile funderProfile = null;
	private UserProfile currentApproverProfile = null;

	public DealUserProfiles(Deal deal, SessionResourceKit srk)
	{
		this.deal   = deal;
		this.srk    = srk;

    // check for pre-assignments - load profiles
    administratorId = deal.getAdministratorId();

    if (!(administratorId == 0 || administratorId == -1))
      setAdministratorProfile(getAdministratorProfile());

    underwriterId = deal.getUnderwriterUserId();

    if (!(underwriterId == 0 || underwriterId == -1))
    {
      setUnderwriterProfile(getUnderwriterProfile());

      // no sense asking for 'current' if assigned underwriter not found!
      if (underwriterProfile != null)
          getCurrentApproverProfile();
    }

    //Added Funder -- By BILLY 14Jan2002
    funderId = deal.getFunderProfileId();
    if (!(funderId == 0 || funderId == -1))
      setFunderProfile(getFunderProfile());

	}

	public UserProfile getAdministratorProfile()
	{
		if (administratorProfile != null)
			return administratorProfile;

    if (administratorId == 0 || administratorId == -1)
        return null;

		SysLogger logger = srk.getSysLogger();

		try
		{
			administratorProfile = new UserProfile(srk);
			administratorProfile = administratorProfile
            .findByPrimaryKey(new UserProfileBeanPK(deal.getAdministratorId(), 
                                                    deal.getInstitutionProfileId()));
		}
		catch (Exception e)
		{
			administratorProfile = null;
			logger.error("DealUserProfiles::getAdministratorProfile: Error locating profile");
			return null;
		}

		return administratorProfile;
	}

	public UserProfile getUnderwriterProfile()
	{
		if (underwriterProfile != null)
			return underwriterProfile;

    if (underwriterId == 0 || underwriterId == -1)
        return null;

		SysLogger logger = srk.getSysLogger();

		try
		{
			underwriterProfile = new UserProfile(srk);
			underwriterProfile = underwriterProfile
                  .findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(),
                                                          deal.getInstitutionProfileId()));
		}
		catch (Exception e)
		{
			underwriterProfile = null;
			logger.error("DealUserProfiles::getUnderwriterProfile: Error locating profile");
			return null;
		}

		return underwriterProfile;
	}

  // the last approver (higher/second, or joint approver) assigned to the deal
	public UserProfile getCurrentApproverProfile()
	{
		if (currentApproverProfile != null)
			return currentApproverProfile;

		SysLogger logger = srk.getSysLogger();

		try
		{
			currentApproverProfile = new UserProfile(srk);
			currentApproverProfile = currentApproverProfile
               .findByPrimaryKey(new UserProfileBeanPK(deal.currentApprovedUserId(),
                                                       deal.getInstitutionProfileId()));
		}
		catch (Exception e)
		{
			currentApproverProfile = null;
			logger.error("DealUserProfiles::getCurrentApproverProfile: Error locating profile");
			return null;
		}

		return currentApproverProfile;
	}

  //Added Funder -- By BILLY 14Jan2002
  public UserProfile getFunderProfile()
	{
		if (funderProfile != null)
			return funderProfile;

    if (funderId == 0 || funderId == -1)
        return null;

		SysLogger logger = srk.getSysLogger();

		try
		{
			funderProfile = new UserProfile(srk);
			funderProfile = funderProfile
               .findByPrimaryKey(new UserProfileBeanPK(deal.getFunderProfileId(),
                                                       deal.getInstitutionProfileId()));
		}
		catch (Exception e)
		{
			funderProfile = null;
			logger.error("DealUserProfiles::getFunderProfile: Error locating profile");
			return null;
		}

		return funderProfile;
	}

	public void setAdministratorProfile(UserProfile p)
	{
		administratorProfile = p;

    if (administratorProfile == null)
      administratorId = -1;
    else
      administratorId = administratorProfile.getUserProfileId();
	}
	public void setUnderwriterProfile(UserProfile p)
	{
		underwriterProfile = p;

    if (underwriterProfile == null)
      underwriterId = -1;
    else
      underwriterId = underwriterProfile.getUserProfileId();
	}

  public void setCurrentApproverProfile(UserProfile p)
  {
    currentApproverProfile = p;
  }

    public void setCurrentApproverProfile(int userProfileId) throws Exception
  {
		//SysLogger logger = srk.getSysLogger();

    currentApproverProfile = new UserProfile(srk);
    currentApproverProfile = currentApproverProfile
                 .findByPrimaryKey(new UserProfileBeanPK(userProfileId, 
                                                         srk.getExpressState().getDealInstitutionId()));
  }

  //Added Funder -- By BILLY 14Jan2002
  public void setFunderProfile(UserProfile p)
	{
		funderProfile = p;

    if (funderProfile == null)
      funderId = -1;
    else
      funderId = funderProfile.getUserProfileId();
	}

  //--> Ticket#135 : Ensure to hunt for Admin and Funder with the SOB/Firm group
  //--> Added groupId
  //--> By Billy 27Nov2003
  public void setSourceGroupId(int theGroupId)
	{
		if (theGroupId > 0)
      SourceGroupId = theGroupId;
    else
      SourceGroupId = -1;
	}

  public int getSourceGroupId()
	{
		return SourceGroupId;
	}
  //============================================================================

/**
  *
  *
  public void getTimeZoneId(int userProfileId)
  {
  }

  **/


  public void traceShow(SysLogger logger)
  {
    logger.trace("DealUserProfiles :: " + userDetail("     Underwriter", underwriterId, underwriterProfile));
    logger.trace("DealUserProfiles :: " + userDetail("   Administrator", administratorId, administratorProfile));
    logger.trace("DealUserProfiles :: " + userDetail("Current Approver", 0, currentApproverProfile));
  }

  private String userDetail(String label, int userId, UserProfile up)
  {
    if (up == null)
    {
      return label + ":: Id = " + userId + ", Profile information not available (null)";
    }

    return label + ":: Id = " + ((userId == 0) ? up.getUserProfileId() : userId) +
                   ", login = " + up.getUserLogin() +
                   ", group = " + up.getGroupProfileId() +
                   ", type = " + up.getUserTypeId() +
                   ", profileStatus = " + up.getProfileStatusId() +
                   ", 2nd/joint/partner/stand-in = " +
                    up.getSecondApproverUserId()    + "/" +
                    up.getJointApproverUserId()     + "/" +
                    up.getPartnerUserId()           + "/" +
                    up.getStandInUserId();
  }

}
