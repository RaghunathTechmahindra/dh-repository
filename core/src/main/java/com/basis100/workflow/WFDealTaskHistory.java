package com.basis100.workflow;

import java.util.*;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;

import MosSystem.Sc;

class WFDealTaskHistory
{
	private Vector assignedTaskList  = new Vector();

	private int currentWorkflowId    = -1;
	private int currentWorkflowStage =  0;
	private int currentWorkflowPass  =  0;



	public WFDealTaskHistory()
	{
		; // usage for initial task generation (to determine if task(s) generated
	}
	public WFDealTaskHistory(SessionResourceKit srk, int dealId) throws Exception
	{
		AssignedTask worker = new AssignedTask(srk);

    // finds all tasks for deal ordered by stage (earliest stages first)
		assignedTaskList = worker.findByDeal(dealId);

    AssignedTask mostRecent = null;

    // determine current workflow stage:
    //
    //  . lowest stage number with an open (or re-route) task
    //
    // ----->>>> WAS
    //  . if no such, then consider current stage to be the stage of the most recently (in time)
    //    generated task
    //
    // ----->>>> NOW
    //  . if no such, then consider current stage to be the highest stage number for which a task
    //    was generated within the highest <pass> value encountered
    //
    //  . asychronous tasks (tasks in stage 0) ignored during determinination of current stage
    //

		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			AssignedTask aTask = (AssignedTask)assignedTaskList.elementAt(i);

      // check asyc task
      if (aTask.getWorkflowStage() == 0)
        continue;

      int atStatus = aTask.getTaskStatusId();

			if (atStatus == Sc.TASK_OPEN || atStatus == Sc.TASK_REROUTE)
			{
				currentWorkflowId    = aTask.getWorkflowId();
				currentWorkflowStage = aTask.getWorkflowStage();
				currentWorkflowPass  = aTask.getPass();
				break;
			}

			// until open task located track most recently generated (as described above)
      if (mostRecent == null)
      {
        mostRecent = aTask;
        continue;
      }


      // ----->>> WAS
      /*
      if (aTask.getStartTimestamp().compareTo(mostRecent.getStartTimestamp()) > 0)
        mostRecent = aTask;
      */

      // ----->>> NOW
      if (aTask.getPass() > mostRecent.getPass())
      {
        mostRecent = aTask;
        continue;
      }

      if (aTask.getWorkflowStage() > mostRecent.getWorkflowStage())
      {
        mostRecent = aTask;
        continue;
      }

		}

    // if no open tasks found base info on most recent task generated
    if (currentWorkflowId == -1 && mostRecent != null )
    {
				currentWorkflowId    = mostRecent.getWorkflowId();
				currentWorkflowStage = mostRecent.getWorkflowStage();
				currentWorkflowPass  = mostRecent.getPass();
    }
	}

	public void addAssignedTasks(Vector tasks)
	{
		if (tasks == null)
			return;

    int lim             = tasks.size();

    for (int i = 0; i < lim; ++ i)
    {
        assignedTaskList.addElement((AssignedTask)tasks.elementAt(i));
    }
	}

	public int getAssignedTaskUser(int taskId, int stageNum, int pass, boolean openStatus)
	{
		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			AssignedTask aTask = (AssignedTask)assignedTaskList.elementAt(i);

			int atStatus = aTask.getTaskStatusId();

			if (aTask.getWorkflowId()    == currentWorkflowId && aTask.getTaskId() == taskId &&
          aTask.getWorkflowStage() == stageNum          && aTask.getPass() == pass)
			{
				boolean open = atStatus == Sc.TASK_OPEN || atStatus == Sc.TASK_REROUTE;

				if (openStatus == true && open == false)
					continue;

				return aTask.getUserProfileId();
			}
		}

		return 0;
	}

	public Vector getAllOpenTasks()
	{
		AssignedTask aTask = null;
		Vector oTasks = new Vector();

		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			aTask = (AssignedTask)assignedTaskList.elementAt(i);

			int atStatus = aTask.getTaskStatusId();

			if (atStatus == Sc.TASK_OPEN || atStatus == Sc.TASK_REROUTE)
				oTasks.addElement(aTask);
		}

    return oTasks;
	}

	public AssignedTask [] getAllOpenTasksArray()
	{
		Vector oTasks = getAllOpenTasks();

		int size = oTasks.size();

		AssignedTask [] tasks = new AssignedTask[size];

		for (int i = 0; i < size; ++i)
		{
			tasks[i] = (AssignedTask)oTasks.elementAt(i);
		}

		return tasks;
	}

	public Vector getOpenTasksForPage(SessionResourceKit srk, int pageId) throws RemoteException, FinderException
	{
		AssignedTask aTask = null;
		Vector oTasks = new Vector();

		// find open tasks associated with the page

		WorkflowTaskPage worker = new WorkflowTaskPage(srk);

		Vector tpsVec = worker.findByPage(pageId, currentWorkflowId); // used as list of task (ids)

		for (int i = 0; i < tpsVec.size(); ++i)
		{
			int taskId = ((WorkflowTaskPage)tpsVec.elementAt(i)).getTaskId();

			aTask = getAssignedTask(taskId, -1,  true);

			if (aTask != null)
				oTasks.addElement(aTask);
		}

		return oTasks;
	}

	public int getWorkflowId()
	{
		return currentWorkflowId;
	}

	public int getWorkflowTransitionStage()
	{
		return currentWorkflowStage + 1;
	}

	public boolean haveOpenTasks()
	{
		AssignedTask aTask = null;

		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			aTask = (AssignedTask)assignedTaskList.elementAt(i);

			int atStatus = aTask.getTaskStatusId();

			if (atStatus == Sc.TASK_OPEN || atStatus == Sc.TASK_REROUTE)
				return true;
		}

		return false;
	}

	public boolean isTaskAlreadyAssigned(int taskId, int workflowStage)
	{
		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			AssignedTask aTask = (AssignedTask)assignedTaskList.elementAt(i);

			if (aTask.getWorkflowId()    == currentWorkflowId && aTask.getTaskId() == taskId &&
          aTask.getWorkflowStage() == workflowStage     && aTask.getPass()   == currentWorkflowPass)
				return true;
		}

		return false;
	}

  // PROGRAMMER NOTE:
  //
  // In future the attribute Task.ExternalRecordId may need to be considered when testing for 'openness'.
  // This is because we may have multiple tasks with same id/stage/etc that are differientated by
  // external record id only.
  //
  // At present this does not occur and perhaps this is a good thing. Should we however decide to change
  // this the open test will require revisiting.

	public boolean isTaskOpen(int taskId, int workflowStage)
	{
		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			AssignedTask aTask = (AssignedTask)assignedTaskList.elementAt(i);

			if (aTask.getTaskId()         == taskId               &&
          aTask.getWorkflowStage()  == workflowStage        &&
				  aTask.getPass()           == currentWorkflowPass  &&
			    (aTask.getTaskStatusId() == Sc.TASK_OPEN || aTask.getTaskStatusId() == Sc.TASK_REROUTE))

				return true;
		}

		return false;
	}


  // note: non-critical task(s) open do not prevent workflow stage transition. Also asynchronous tasks
  // are ignored when determining if a workflow transition has occured (hence by definition async. tasks
  // are non-critical)
	public boolean isWorkflowTransition()
	{
		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			AssignedTask aTask = (AssignedTask)assignedTaskList.elementAt(i);

      //  async. task
      if (aTask.getWorkflowStage() == 0)
        continue;

      // non-critical task
      if (aTask.getCriticalFlag().equals("N"))
        continue;

			int atStatus = aTask.getTaskStatusId();

			if (aTask.getWorkflowStage() <= currentWorkflowStage &&
				  (atStatus == Sc.TASK_OPEN || atStatus == Sc.TASK_REROUTE))
				return false;
		}
		return true;
	}


	//
	// Private
	//

  // ALERT :: any call to this ??? - looks suspect!!!

	private void addOtherAssignedTasksWithSameBase(Vector aTasks, int taskId, int basedOnTaskId, int userId)
	{
		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			AssignedTask aTask = (AssignedTask)assignedTaskList.elementAt(i);

			if (aTask.getTaskId() == taskId || aTask.getUserProfileId() != userId ||
				aTask.getBasedOnTaskId() != basedOnTaskId)
				continue;

			int atStatus = aTask.getTaskStatusId();
			boolean open = atStatus == Sc.TASK_OPEN || atStatus == Sc.TASK_REROUTE;

			if (!open)
				return;

			aTasks.addElement(aTask);
		}
	}

	private AssignedTask getAssignedTask(int taskId, int userId,  boolean openStatus)
	{
		for (int i = 0; i < assignedTaskList.size(); ++i)
		{
			AssignedTask aTask = (AssignedTask)assignedTaskList.elementAt(i);

			int atStatus = aTask.getTaskStatusId();

			if (aTask.getTaskId() == taskId)
			{
        if (aTask.getWorkflowId() != currentWorkflowId)
          continue;

				if (userId != -1 && aTask.getUserProfileId() != userId)
					continue;

				boolean open = atStatus == Sc.TASK_OPEN || atStatus == Sc.TASK_REROUTE;

				if (openStatus == true && open == true)
					return aTask;

				if (openStatus == false && open == false)
					return aTask;
			}
		}

		return null;
	}
}
