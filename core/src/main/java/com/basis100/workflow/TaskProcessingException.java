package com.basis100.workflow;

/**
* If failure happens during getting connection by exceeding the no of
* connections then instance of
* this exception class should be thrown
* @see JdbcConnectionPool
*/

public final class TaskProcessingException extends Exception
{
	public TaskProcessingException()
	{
		super();
	}
	public TaskProcessingException(String message)
	{
		super(message);
	}
}
