package com.basis100.workflow.pk;

public class WorkflowBeanPK
{
	String wfName;

	public WorkflowBeanPK(String wfName)
	{
		this.wfName = wfName;
	}
	public String getWfName()
	{
		return wfName;
	}
	public void setWfName(String wfName)
	{
		this.wfName = wfName;
	}
	public String toString()
	{
		return "WorkflowBeanPK( wfName=" + wfName + ")";
	}
}
