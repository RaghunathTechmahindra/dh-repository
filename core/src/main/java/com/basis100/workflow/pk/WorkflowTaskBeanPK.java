package com.basis100.workflow.pk;

public class WorkflowTaskBeanPK
{
	int workflowId;
	int taskId;

	public WorkflowTaskBeanPK(int workflowId, int taskId)
	{
		this.workflowId = workflowId;
		this.taskId     = taskId;
	}
	public boolean equals(Object other)
	{
		if (!(this.getClass().isInstance(other)))
			return false;

		return toString().equals(other.toString());
	}
	public int getTaskId()
	{
		return taskId;
	}
	public int getWorkflowId()
	{
		return workflowId;
	}
	public void setTaskId(int taskId)
	{
		this.taskId = taskId;
	}
	public void setWorkflowId(int workflowId)
	{
		this.workflowId = workflowId;
	}
	public String toString()
	{
		return "TaskBeanPK(workflowId=" + workflowId + ", taskId=" + taskId + ")";
	}
}
