package com.basis100.workflow.pk;

public class PageAccessBeanPK implements java.io.Serializable
{
	int pageId;

	int userTypeId;
	int userProfileId;
	String userLogin;

	public PageAccessBeanPK(int userTypeId, int userProfileId)
	{
		this.userTypeId = userTypeId;
    this.userProfileId = userProfileId;
	}

      // added for FXP21790
      // when user login, this method should be used from SignonHandler.finishLogin
	public PageAccessBeanPK(int userTypeId, String userLogin) {
        this.userTypeId = userTypeId;
        this.userLogin = userLogin;
    }
	public int getUserProfileId()
	{
		return userProfileId;
	}
	public int getUserTypeId()
	{
		return userTypeId;
	}
	public void setUserProfileId(int userProfileId)
	{
		this.userProfileId = userProfileId;
	}
	public void setUserTypeId(int userTypeId)
	{
		this.userTypeId = userTypeId;
	}
	public String toString()
	{
		return "PageAccessBeanPK(userTypeId=" + userTypeId + ", userProfileId=" + userProfileId + ")";
	}
	
    // fixed for FXP21790
    public String getUserLogin() {
        return userLogin;
    }
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
}
