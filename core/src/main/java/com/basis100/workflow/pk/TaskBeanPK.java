package com.basis100.workflow.pk;

public class TaskBeanPK
{
	int taskId;

	public TaskBeanPK(int taskId)
	{
		this.taskId     = taskId;
	}
	public boolean equals(Object other)
	{
		if (!(this.getClass().isInstance(other)))
			return false;

		return toString().equals(other.toString());
	}
	public int getTaskId()
	{
		return taskId;
	}
	public void setTaskId(int taskId)
	{
		this.taskId = taskId;
	}
	public String toString()
	{
		return "TaskBeanPK(taskId=" + taskId + ")";
	}
}
