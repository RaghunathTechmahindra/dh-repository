package com.basis100.workflow.pk;

import java.io.Serializable;

/**
 *
 * <p><h2>Title</h2>com.basis100.deal.entity.WorkflowProcessAssocPK</p>
 * <p><h2>Description</h2>Primary Key for WorkflowTaskProcessAssoc</p>
 * <p><h3>Copyright</h3>Copyright (c) 2004</p>
 * <p><h3>Company</h3><a href="http://www.filogix.com">FiLogix Inc.</a></p>
 * @author Neil Kong
 * @version 1.0
 */
public class WorkflowTaskProcessAssocPK
	implements Serializable
{

	private int taskId;
	private int orderNo;

	/**
	 * defult constructor
	 */
	public WorkflowTaskProcessAssocPK(int taskId, int orderNo)
	{
		this.taskId = taskId;
		this.orderNo = orderNo;
	}

	/**
	 *
	 * @return int
	 */
	public int getTaskId()
	{
		return taskId;
	}

	/**
	 *
	 * @return int
	 */
	public int getOrderNo()
	{
		return orderNo;
	}

	/**
	 *
	 * @return String
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer("TaskId = ");
		sb.append(taskId);
		sb.append(", OrderNo = ");
		sb.append(orderNo);
		return sb.toString();
	}

	/**
	 *
	 * @param keyObj Object
	 * @return boolean
	 */
	public boolean equals(Object keyObj)
	{
		if(this == keyObj)
		{
			return true;
		}
		else if(keyObj == null || this.getClass() != keyObj.getClass())
		{
			return false;
		}
		else
		{
			WorkflowTaskProcessAssocPK wtpak = (WorkflowTaskProcessAssocPK)keyObj;
			if(wtpak.taskId == this.taskId && wtpak.orderNo == this.orderNo)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	/**
	 *
	 * @return int
	 */
	public int hashCode()
	{
		return(taskId + orderNo);
	}
}
