package com.basis100.workflow.pk;

public class WorkflowTaskPageBeanPK
{
	int workflowTaskPageId;

	public WorkflowTaskPageBeanPK(int workflowTaskPageId)
	{
		this.workflowTaskPageId = workflowTaskPageId;
	}
	public int getWorkflowTaskPageId()
	{
		return workflowTaskPageId;
	}
	public void setWorkflowTaskPageId(int workflowTaskPageId)
	{
		this.workflowTaskPageId = workflowTaskPageId;
	}
	public String toString()
	{
		return "WorkflowTaskPageBeanPK(WorkflowTaskPageId=" + workflowTaskPageId + ")";
	}
}
