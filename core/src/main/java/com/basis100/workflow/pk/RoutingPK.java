package com.basis100.workflow.pk;

public class RoutingPK
{
	int routingId;

	public RoutingPK(int routingId)
	{
		this.routingId = routingId;
	}
	public int getRoutingId()
	{
		return routingId;
	}
	public void setRoutingId(int routingId)
	{
		this.routingId = routingId;
	}
	public String toString()
	{
		return "RoutingPK(routingId=" + routingId + ")";
	}
}
