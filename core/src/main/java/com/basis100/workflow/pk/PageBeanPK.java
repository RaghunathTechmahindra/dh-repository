package com.basis100.workflow.pk;

public class PageBeanPK
{
	int pageId;

	public PageBeanPK(int pageId)
	{
		this.pageId = pageId;
	}
	public int getPageId()
	{
		return pageId;
	}
	public void setPageId(int pageId)
	{
		this.pageId = pageId;
	}
	public String toString()
	{
		return "PageBeanPK(pageId=" + pageId + ")";
	}
}
