package com.basis100.workflow.pk;

public class AssignedTaskBeanPK
{
	int assignedTasksWorkQueueId;

	public AssignedTaskBeanPK(int assignedTasksWorkQueueId)
	{
		this.assignedTasksWorkQueueId = assignedTasksWorkQueueId;
	}
	public int getAssignedTasksWorkQueueId()
	{
		return assignedTasksWorkQueueId;
	}
	public void setAssignedTasksWorkQueueId(int assignedTasksWorkQueueId)
	{
		this.assignedTasksWorkQueueId = assignedTasksWorkQueueId;
	}
	public String toString()
	{
		return "AssignedTaskBeanPK(assignedTasksWorkQueueId=" 
			+ assignedTasksWorkQueueId +  ")";
	}

}
