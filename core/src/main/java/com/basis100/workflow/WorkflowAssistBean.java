package com.basis100.workflow;

import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.BREngine.BRUtil;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.ConditionStatusUpdateAssoc;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.DBA;
import com.basis100.entity.FinderException;
// SEAN Routing Order Changes (May 05, 2005):
import com.basis100.entity.RemoteException;
// SEAN Routing Order Changes END
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.entity.Routing;
import com.basis100.workflow.entity.Workflow;
import com.basis100.workflow.entity.WorkflowTask;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.util.Xc;

public class WorkflowAssistBean
{
	private final static Logger logger = 
        LoggerFactory.getLogger(WorkflowAssistBean.class);
	
    static UserProfileQuery uq = new UserProfileQuery();

	// SEAN Routing Order Changes (May 06, 2005): the constant for routing types
	// defined in file ROUTINGORDER.properties
	// Source of business to group
	public static final int ROUTE_METHOD_SOB2GROUP = 1;
	// Source Firm to Group.
	public static final int ROUTE_METHOD_SF2GROUP = 2;
	// Routing table
	public static final int ROUTE_METHOD_TABLE = 3;
	// Source of business to user.
	public static final int ROUTE_METHOD_SOB2USER = 4;
	// Source Firm to User.
	public static final int ROUTE_METHOD_SF2USER = 5;
	// workflow to user.
	public static final int ROUTE_METHOD_WORKFLOW2USER = 6;
	// fallback mapping.
	public static final int ROUTE_METHOD_FALLBACK = 7;
	// SEAN Routing Order Change END
	
	
    public WorkflowAssistBean()
    {
    }
    
    /**
     * <pre>
     *             Select a workflow for the deal passed and perform initial task generation activity.
     *            
     *             Selection of workflow and assignment of users (underwriter and administrator) is documented in the
     *             MOS Design.
     *            
     *             This method also detected pre-assignment of user(s) via the deal attributes
     *             &lt;underwriterUserId&gt; and &lt;administratorId&gt;; pre-assignment detected via id value &gt; 0 (i.e. a
     *             valid user profile Id).
     *            
     *             If an underwriter has been assigned but an admistrator has not then the system selects the
     *             administrator as:
     *            
     *                . the partner of the selected underwriter if an active administrator), else
     *                . as per administrator selection algorithm (documented)
     *            
     *             If an administrator has been assigned but an underwriter has not the the system selects the
     *             underwriter as:
     *            
     *                . the partner of the selected administrator if an active underwriter, else
     *                . underwriter select via qualifying underwriters from group (branch, if necessary) matching
     *                  the administrator; this is similar to the documented procedure for locating a administrator
     *                  after an underwriter assignment.
     *            
     *             @return
     *             true  - workflow selected, users assigned, and at least one task generated.
     *             false - some error condition such as: workflow could not be selected, user(s) could not be
     *                     assigned, or no tasks were generated.
     *            
     *             
     *   <p>Express 4.2GR DUS Note: 
     *   this class was modified fro 4.2GR Deal Event Status Update
     *   Before a method returns true as final decision, then store request trigger to ESBOutbound Queue.
     *   If a method purely call another method, that creates tasks, then not store into the Queue.
     *           
     * </pre>
     */
    public boolean generateInitialTasks(int dealId, int copyId, 
            SessionResourceKit srk)
    {
        //
        // select a workflow:
        Deal deal = null;
        try
        {
            deal = DBA.getDeal(srk, dealId, copyId);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:generateInitialTasks: Error locating deal Id = "
                            + dealId + ", copy Id = " + copyId);
            return false;
        }
        return generateInitialTasks(deal, srk);
    }

    public boolean generateInitialTasks(Deal deal, SessionResourceKit srk)
    {
        // select workflow for the deal
        //
        WorkflowDetails wfInfo = selectWorkflow(deal, srk);
        if (wfInfo.getWorkflowId() == -1) { return false; }
        // assigned users ... (DealUserProfiles constructor loads profile for
        // pre-assignments)
        DealUserProfiles dealUserProfiles = new DealUserProfiles(deal, srk);
        // assign an underwriter
        if (assignUnderwriter(deal, dealUserProfiles, wfInfo, srk) == false) { return false; }
        // assign an administrator
        if (assignAdmin(deal, dealUserProfiles, srk) == false)
        {
            // fall back (unlikely) - assign underwriter!
            dealUserProfiles.setAdministratorProfile(dealUserProfiles
                    .getUnderwriterProfile());
        }
        // Added assign an funder -- By BILLY 14Jan2002
        if (assignFunder(deal, dealUserProfiles, srk) == false)
        {
            // fall back (unlikely) - assign admin!
            dealUserProfiles.setFunderProfile(dealUserProfiles
                    .getAdministratorProfile());
        }
        //
        // assign initial tasks ..
        //
        // start a transaction ... (from this point its all or nothing)
        WFDealTaskHistory taskHist = new WFDealTaskHistory();
        boolean tasksGenerated = false;
        boolean calledInTransaction = false;
        try
        {
            calledInTransaction = srk.isInTransaction();
            if (calledInTransaction == false)
            {
                srk.beginTransaction();
            }
            deal.setUnderwriterUserId(dealUserProfiles.getUnderwriterProfile()
                    .getUserProfileId());
            deal.setAdministratorId(dealUserProfiles.getAdministratorProfile()
                    .getUserProfileId());
            deal.setFunderProfileId(dealUserProfiles.getFunderProfile()
                    .getUserProfileId());
            // set branch and group corresponding to underwriter
            deal.setGroupProfileId(dealUserProfiles.getUnderwriterProfile()
                    .getGroupProfileId());
            if (deal.getBranchProfileId() < 1)
            {
                JdbcExecutor jExec = srk.getJdbcExecutor();
                int key = jExec
                        .execute("Select BRANCHPROFILEID from groupprofile where groupprofileid = "
                                + deal.getGroupProfileId());
                while (jExec.next(key))
                {
                    deal.setBranchProfileId(jExec.getInt(key, 1));
                    break;
                }
                jExec.closeData(key);
            }
            deal.ejbStore();
            int stageNum = 0; // start fro the async stage.
            generateTasks(deal, wfInfo.getWorkflowId(), stageNum, null,
                    dealUserProfiles, srk, false, taskHist);
            // check for tasks generated
            tasksGenerated = taskHist.haveOpenTasks();
            
        }
        catch (Exception e)
        {
            if (calledInTransaction == false)
            {
                srk.cleanTransaction();
            }
            logger
                    .error("WFE::WorkflowAssistBean:generateInitialTasks: Error generating initital tasks, deal Id="
                            + deal.getDealId());
            return false;
        }
        if (tasksGenerated == false)
        {
            if (calledInTransaction == false)
            {
                srk.cleanTransaction();
            }
            logger
                    .error("WFE::WorkflowAssistBean:generateInitialTasks: Error generating initital tasks");
            logger
                    .error("WFE::WorkflowAssistBean:generateInitialTasks: No tasks generated, deal Id = "
                            + deal.getDealId());
            return false;
        } 
        try
        {
            if (calledInTransaction == false)
            {
                srk.commitTransaction();
            }
        }
        catch (Exception e)
        {
            if (calledInTransaction == false)
            {
                srk.cleanTransaction();
            }
            logger
                    .error("WFE::WorkflowAssistBean:generateInitialTasks: Error commitiing generating initital tasks, deal Id="
                            + deal.getDealId());
            return false;
        }
        return true;
    }

    /**
     * Called after an new underwriter has been assigned to a deal. Attempts to
     * assign a new administrator to the deal. Basically the mechanism is
     * identical to administrator assignment for an newly introduced deal (via
     * ingestion) following underwriter assignment. However, if a new
     * administrator cannot be determined the the fallback position is: .
     * current administrator retained (if one) . underwriter assigned as
     * administrator
     * 
     * Note, this fallback position differs from the fallback position used
     * during initial task generation as in this latter case there will not be
     * an administrator present.
     * 
     * PROGRAMMER NOTES ----------------
     * 
     * Before calling the (new) underwriter must have been set into the deal
     * provided; i.e. via deal.setUnderwriterUserId().
     * 
     * Method assumed to be called in calles transaction context (does not
     * establish own transaction).
     * 
     * Result - true = a new adminuistrator was assigned to the deal false =
     * current administrator was unchanged
     */
    public boolean assignAdminForNewUnderwriter(Deal deal,
            SessionResourceKit srk)
    {
        // save current administrator assignment
        int currAdministratorId = deal.getAdministratorId();
        deal.setAdministratorId(0);
        DealUserProfiles dealUserProfiles = new DealUserProfiles(deal, srk);
        // assign an administrator
        if (assignAdmin(deal, dealUserProfiles, srk) == false)
        {
            // fallback
            if (currAdministratorId > 0)
            {
                // restore current administrator
                deal.setAdministratorId(currAdministratorId);
                return false;
            }
            deal.setAdministratorId(deal.getUnderwriterUserId());
            return true;
        }
        // administator located - chech if different than current
        int newAdminId = dealUserProfiles.getAdministratorProfile()
                .getUserProfileId();
        // Bug Fix -- Should keep the currentAdminId = newAdminId -- By BILLY
        // 11Dec2001
        if (newAdminId == currAdministratorId)
        {
            deal.setAdministratorId(currAdministratorId);
            return false;
        }
        deal.setAdministratorId(newAdminId);
        return true;
    }

    // New Method to handle Funder Assignment -- By Billy 14Jan2002
    public boolean assignFunderForNewUnderwriter(Deal deal,
            SessionResourceKit srk)
    {
        // save current administrator assignment
        int currFunderId = deal.getFunderProfileId();
        deal.setFunderProfileId(0);
        DealUserProfiles dealUserProfiles = new DealUserProfiles(deal, srk);
        // assign an Funder
        if (assignFunder(deal, dealUserProfiles, srk) == false)
        {
            // fallback
            if (currFunderId > 0)
            {
                // restore current funder
                deal.setFunderProfileId(currFunderId);
                return false;
            }
            // Set it to the underwiter -- fallback
            deal.setFunderProfileId(deal.getUnderwriterUserId());
            return true;
        }
        // funder located
        int newFunderId = dealUserProfiles.getFunderProfile()
                .getUserProfileId();
        deal.setFunderProfileId(newFunderId);
        return true;
    }

    public AssignedTask generateTasks(Deal deal, int workflowId, int stageNum,
            AssignedTask linkableTask, DealUserProfiles dealUserProfiles,
            SessionResourceKit srk, boolean asTransaction,
            WFDealTaskHistory taskHistory) throws TaskProcessingException
    {
        Workflow workflow = null;
        //  
        try
        {
            workflow = new Workflow(srk);
            workflow = workflow.findById(workflowId);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:generateTasks: Error locating workflow Id = "
                            + workflowId);
            throw new TaskProcessingException("");
        }
        return generateTasks(deal, workflow, stageNum, linkableTask,
                dealUserProfiles, srk, asTransaction, taskHistory);
    }

    public AssignedTask generateTasks(Deal deal, Workflow workflow,
            int stageNum, AssignedTask linkableTask,
            DealUserProfiles dealUserProfiles, SessionResourceKit srk,
            boolean asTransaction, WFDealTaskHistory taskHistory)
            throws TaskProcessingException
    {
        boolean followUpOpen;
        int followUpStageNum;
        WorkflowTask followUpWt = null;
        Vector tasks = null;
        // check if at end of workflow
        if (stageNum > workflow.getNumberOfStages()) { return null; }
        //  
        // determine the link task characteristics (if detecting)
        AssignedTask linkTask = null;
        int linkTaskUserId = -1;
        int linkTaskId = -1;
        if (linkableTask != null)
        {
            linkTaskUserId = linkableTask.getUserProfileId();
            WorkflowTask wt = workflow.getLinkTask(linkableTask);
            if (wt != null)
            {
                linkTaskId = wt.getTaskId();
            }
        }
        boolean tasksGenerated = false;
        try
        {
            if (asTransaction)
            {
                srk.beginTransaction();
                // Use the same BusinessRule Engine for performance improvment
                //  -- By BILLY 29April2002
            }
            BusinessRule br = new BusinessRule(
                    srk, -1, -1, "na", "");
            br.setDealId(deal.getDealId(), deal.getCopyId());
            //=================================================================
            
            // #2242: Catherine, 23-Nov-05 --------- start ---------  
            br.setLenderName();
            BRUtil.debug("WorkflowAssistBean: set LenderName done");
            // #2242: Catherine, 23-Nov-05 --------- end ---------  

            for (; tasksGenerated == false; stageNum++)
            {
                // check if at end of workflow
                if (stageNum > workflow.getNumberOfStages()) { return null; }
                WorkflowTask[] wts = workflow.getStageTasks(stageNum);
                int len = wts.length;
                for (int i = 0; i < len; ++i)
                {
                    WorkflowTask wt = wts[i];
                    // check task open (e.g. generated early prior to workflow
                    // transition as follow-up task)
                    if (taskHistory.isTaskOpen(wt.getTaskId(), stageNum) == true)
                    {
                        // open - consider tasks generated :: unless Async stage
                        // (0)
                        if (stageNum != 0)
                        {
                            tasksGenerated = true;
                        }
                        continue;
                    }
                    // if task has follow-up considered generation candidate
                    // only if follow-up not open
                    //  (recursive to end of follow-up chain)
                    followUpOpen = false;
                    followUpStageNum = stageNum;
                    followUpWt = wt;
                    while ((followUpWt = workflow.getFollowUpTask(followUpWt)) != null)
                    {
                        followUpStageNum++;
                        if (taskHistory.isTaskOpen(followUpWt.getTaskId(),
                                followUpStageNum) == false)
                        {
                            // test
                            followUpOpen = true;
                            break;
                        }
                    }
                    if (followUpOpen == true)
                    {
                        continue;
                    }
                    // test generation
                    // Use the same BusinessRule Engine for performance
                    // improvment
                    //  -- By BILLY 29April2002
                    tasks = generateTask(deal, wt, dealUserProfiles, srk, br);
                    //============================================================
                    if (tasks != null)
                    {
                        // task(s) generated ...
                        taskHistory.addAssignedTasks(tasks);
                        // check for link task (must be to same user as task
                        // completed - passed, see above)
                        if (linkTaskId != -1 && linkTask == null
                                && tasks.size() == 1)
                        {
                            AssignedTask aTask = (AssignedTask) tasks
                                    .elementAt(0);
                            if (aTask.getTaskId() == linkTaskId
                                    && aTask.getUserProfileId() == linkTaskUserId)
                            {
                                linkTask = aTask;
                            }
                        }
                        // set task generate flag :: unless Async stage (0)
                        if (stageNum != 0)
                        {
                            tasksGenerated = true; // at lease one task
                            // generated
                        }
                        continue;
                    }
                    // task generation not required - but if has follow-up
                    // attempt to generate the follow-up
                    //  (recursive to end of follow-up chain)
                    followUpOpen = false;
                    followUpStageNum = stageNum;
                    followUpWt = wt;
                    while ((followUpWt = workflow.getFollowUpTask(followUpWt)) != null)
                    {
                        followUpStageNum++;
                        if (taskHistory.isTaskOpen(followUpWt.getTaskId(),
                                followUpStageNum) == true)
                        {
                            continue;
                        }
                        // test generation
                        // Use the same BusinessRule Engine for performance
                        // improvment
                        //  -- By BILLY 29April2002
                        tasks = generateTask(deal, followUpWt,
                                dealUserProfiles, srk, br);
                        //============================================================
                        if (tasks != null)
                        {
                            taskHistory.addAssignedTasks(tasks);
                            break;
                        }
                    }
                }
            }
            // added for 4.2GR DSU STARTS
            String supportDSU = PropertiesCache.getInstance().getProperty(
                    deal.getInstitutionProfileId(), Xc.STATUSUPDATE_DEALANDEVENT_SUPPORT);
            if ("Y".equals(supportDSU) || "y".equals(supportDSU)) {
                if (tasksGenerated && dealUserProfiles.getUnderwriterProfile()!= null) {
                    dealStatusUpdateRequest(deal, 
                            dealUserProfiles.getUnderwriterProfile().getUserProfileId());
                }
            }
            // added for 4.2GR DSU ENDS
        }
        catch (Exception e)
        {
            if (asTransaction)
            {
                srk.cleanTransaction();
            }
            logger
                    .error("WFE::WorkflowAssistBean:generateTasks: Error generating tasks");
            if (!(e instanceof TaskProcessingException))
            {
                ;
            }
            logger.error("WFE::WorkflowAssistBean:generateTasks: err = "
                    + e.getMessage());
            throw new TaskProcessingException("");
        }
        if (asTransaction)
        {
            try
            {
                srk.commitTransaction();
            }
            catch (Exception e)
            {
                srk.cleanTransaction();
                logger
                        .error("WFE::WorkflowAssistBean:generateTasks: Error committing generating tasks");
                throw new TaskProcessingException("");
            }
        }
        return linkTask;
    }

    public void generateRegressionTasks(Deal deal, Workflow workflow,
            int stageNum, int backToStageNum,
            DealUserProfiles dealUserProfiles, SessionResourceKit srk,
            boolean asTransaction, WFDealTaskHistory taskHistory)
            throws TaskProcessingException
    {
        //  
        boolean tasksGenerated = false;
        try
        {
            if (asTransaction)
            {
                srk.beginTransaction();
            }
            boolean taskGenerated = false;
            
            // Use the same BusinessRule Engine for performance improvment
            //  -- By BILLY 29April2002
            BusinessRule br = new BusinessRule(
                    srk, -1, -1, "na", "");
            br.setDealId(deal.getDealId(), deal.getCopyId());
            //=================================================================
            
            // #2242: Catherine, 23-Nov-05 --------- start ---------  
            br.setLenderName();
            BRUtil.debug("WorkflowAssistBean: set LenderName done");
            // #2242: Catherine, 23-Nov-05 --------- end ---------  

            for (; stageNum >= backToStageNum; --stageNum)
            {
                if (taskGenerated == true)
                {
                    break;
                }
                WorkflowTask[] wts = workflow.getStageTasks(stageNum);
                int len = wts.length;
                for (int i = 0; i < len; ++i)
                {
                    WorkflowTask wt = wts[i];
                    // during regression generation a repeat task is allowed -
                    // but not if task currently open!
                    if (taskHistory.isTaskOpen(wt.getTaskId(), stageNum) == true)
                    {
                        continue;
                    }
                    // if task has follow-up considered generation candidate
                    // only if follow-up not open
                    //  (recursive to end of follow-up chain)
                    boolean followUpOpen = false;
                    int followUpStageNum = stageNum;
                    WorkflowTask followUpWt = wt;
                    while ((followUpWt = workflow.getFollowUpTask(followUpWt)) != null)
                    {
                        followUpStageNum++;
                        if (taskHistory.isTaskOpen(followUpWt.getTaskId(),
                                followUpStageNum) == true)
                        {
                            followUpOpen = true;
                            break;
                        }
                    }
                    if (followUpOpen == true)
                    {
                        continue;
                    }
                    // evaluate for generation
                    // Use the same BusinessRule Engine for performance
                    // improvment
                    //  -- By BILLY 29April2002
                    Vector tasks = generateTask(deal, wt, dealUserProfiles,
                            srk, br);
                    //================================================================
                    if (tasks != null)
                    {
                        taskGenerated = true;
                        taskHistory.addAssignedTasks(tasks); // add to task
                        // history
                    }
                }
            }
            // added for 4.2GR DSU STARTS
            String supportDSU = PropertiesCache.getInstance().getProperty(
                    deal.getInstitutionProfileId(), Xc.STATUSUPDATE_DEALANDEVENT_SUPPORT);
            if ("Y".equals(supportDSU) || "y".equals(supportDSU)) {
                if (tasksGenerated && dealUserProfiles.getUnderwriterProfile()!= null) {
                    dealStatusUpdateRequest(deal, dealUserProfiles.getUnderwriterProfile().getUserProfileId());
                }
            }
            // added for 4.2GR DSU ENDS
        }
        catch (Exception e)
        {
            if (asTransaction)
            {
                srk.cleanTransaction();
            }
            logger
                    .error("WFE::WorkflowAssistBean:generateRegressionTasks: Error evaluating regression tasks");
            //logger.error(e);
            if (!(e instanceof TaskProcessingException))
            {
                ;
            }
            logger
                    .error("WFE::WorkflowAssistBean:generateRegressionTasks: err = "
                            + e.getMessage());
            throw new TaskProcessingException("");
        }
        if (asTransaction)
        {
            try
            {
                srk.commitTransaction();
            }
            catch (Exception e)
            {
                srk.cleanTransaction();
                logger
                        .error("WFE::WorkflowAssistBean:generateRegressionTasks: Error committing generating tasks");
                throw new TaskProcessingException("");
            }
        }
    }

    public Vector generateTask(Deal deal, WorkflowTask wTask,
            DealUserProfiles dealUserProfiles, SessionResourceKit srk,
            BusinessRule br) throws TaskProcessingException
    {
        Vector tasks = new Vector();
        //  
        srk.setCloseJdbcConnectionOnRelease(true);
        // retrieve task generation business rule
        boolean taskGenerated = true;
        RuleResult rr = null;
        int numTypes = 0;
        int types[] = null;
        // defaults for generic treatment when no records id produced
        int numExternalRecordIds = 1;
        int[] externalRecordIds = new int[1];
        externalRecordIds[0] = -1;
        try
        {
            // Use the same BusinessRule Engine for performance improvment
            //  -- By BILLY 29April2002
            // Assume the br input already called setDealId
            br.reQuery(wTask.getWorkflowId(), wTask.getTaskId(), "na",
                    Sc.TASK_BR_NAME_TASK_GENERATE, wTask.getInstitutionProfileId());
            // BusinessRule br = new
            // BusinessRule((srk.getJdbcExecutor()).getCon(),
            //                     wTask.getWorkflowId(), wTask.getTaskId(),
            //                                 "na", Sc.TASK_BR_NAME_TASK_GENERATE);
            //br.setDealId(deal.getDealId(), deal.getCopyId());
            taskGenerated = true; // if no task genertation rule generate task
            // unconditionally
            while (br.next())
            {
                // if have at lease one expression then require true evaluation
                // to generate
                taskGenerated = false;
                rr = br.evaluate();
                if (rr.ruleStatus == true)
                {
                    taskGenerated = true;
                    break;
                }
            }
            // check to see if engine has populated engine global variable
            // <recordIds>
            //
            //       exp --> "sizeof($recordIds := fetchRows(...) > 0)"
            /*
             * ALERT ALERT ALERT Object recordIds = br.getGlobal("$recordsIds"); //
             * array of rows if (recordIds != null) { numExternalRecordIds =
             * Array.getLength(recordsIds); externalRecordIds = new
             * int[numExternalRecordIds];
             * 
             * for (int i = 0; i < numExternalRecordIds; ++i) { Object row =
             * br.getElrement(recordIds, i); externalRecordIds[i] =
             * ((Integer)br.getElememt(row, 0)).intValue(); } }
             */
            br.close();
            if (taskGenerated == false) { return null; }
            // store user types from rule result set (if no result operands
            // assume
            // Mc.USER_DEAL_ADMINISTRATOR, Mc.USER_DEAL_UNDERWRITER)
            numTypes = (rr == null) ? 2 : rr.opCount;
            types = new int[numTypes];
            for (int i = 0; i < numTypes; ++i)
            {
                if (rr == null)
                {
                    if (i == 0)
                    {
                        types[i] = Mc.USER_DEAL_ADMINISTRATOR;
                    }
                    else
                    {
                        types[i] = Mc.USER_DEAL_UNDERWRITER;
                    }
                    continue;
                }
                //Added to handle assign directly to specified UserId e.g. the
                // Ruleesult will return "U1234" means
                //  assign it to UserId 1234 -- Modified by BILLY 14Feb2002
                try
                {
                    types[i] = rr.getInt(i + 1);
                }
                catch (Exception e1)
                {
                    // Try to get the String and Check if it started by U
                    String theResult = rr.getString(i + 1);
                    if (theResult.startsWith("U") || theResult.startsWith("u"))
                    {
                        try
                        {
                            //Set to negetive value to indicate that is for
                            // particular UseId
                            types[i] = -1
                                    * Integer.parseInt((theResult.substring(1,
                                            theResult.length())).trim());
                            //logger.debug("BILLY ===> Assign WF to Specified
                            // UserId " + types[i]);
                        }
                        catch (Exception e2)
                        {
                            types[i] = Mc.USER_DEAL_UNDERWRITER; // FallBack
                            logger
                                    .warn("WFE::WorkflowAssistBean:generateTask: Problem when try to assign for Specified User :: RuleResult returned ==> "
                                            + theResult
                                            + " Have fallback to Deal.Underwriter !!");
                        }
                    }
                    else
                    {
                        // Wrong Resultformat set in Business Ruke
                        types[i] = Mc.USER_DEAL_UNDERWRITER; // FallBack
                        logger
                                .warn("WFE::WorkflowAssistBean:generateTask: Invalide RuleResult format :: RuleResult returned ==> "
                                        + theResult
                                        + " Have fallback to Deal.Underwriter !!");
                    }
                }
            }
            rr = null;
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:generateTask: Business Rule Engine exception generating initital tasks, Business Rule="
                            + Sc.TASK_BR_NAME_TASK_GENERATE
                            + ", deal Id="
                            + deal.getDealId()
                            + ", task Id = "
                            + wTask.getTaskId()
                            + ", workflow Id = "
                            + wTask.getWorkflowId());
            logger
                    .error("WFE::WorkflowAssistBean:generateTask: Business Rule Engine exception = "
                            + e.getMessage());
            throw new TaskProcessingException("");
        }
        int userProfileId = -1;
        UserProfile oUser = null;
        
        // WF Regression Issue wrong asignee - OCT 1, 2008 STARTS ---
      AssignedTask oldTask = null;
      if ("N".equalsIgnoreCase(wTask.getBypassWFRegression())) {
          oldTask = new AssignedTask(srk);
          // this task should have userId * -1 in PASS;
          logger.debug("generateTask()@WorkflowAssitBean Thread ID = " + Thread.currentThread().getId() );
          logger.debug("generateTask()@WorkflowAssitBean Thread Name = " + Thread.currentThread().getName() );
          logger.debug("**** GET ORIGINAL TASK STARTED at:" + System.currentTimeMillis());
          oldTask = oldTask.findRegressedOriginalTask(wTask, deal.getDealId());
          logger.debug("**** GET ORIGINAL TASK END at:" + System.currentTimeMillis());
          if (oldTask != null) {
              int originalUserId = oldTask.getPass() * -1;
              try {
                  oUser = new UserProfile(srk);
                  oUser.setSilentMode(true);
                  oUser.findByPrimaryKey(new UserProfileBeanPK(originalUserId, deal.getInstitutionProfileId()));
                  if (oUser != null && oUser.getProfileStatusId() != Sc.PROFILE_STATUS_ONLEAVE) {
                      userProfileId = oUser.getUserProfileId();
                      oldTask.setPass(0);
//                    srk.beginTransaction();
                      oldTask.ejbStore();
                      logger.debug("==WorkflowAssistBean.generateTask() taskid=" + oldTask.getTaskId() + " : reset pass as 0" );                        
//                    srk.commitTransaction();
                   }
              } catch (Exception e) {
                  logger.debug("==WorkflowAssistBean.generateTask(): find regressed Task's user was not found");                        
              }
          }
      }
        // WF Regression Issue wrong asignee - OCT 1, 2008 ENDS ---

        
        if (dealUserProfiles == null)
        {
            dealUserProfiles = new DealUserProfiles(deal, srk);
        }
        if (userProfileId == -1) {
        for (int i = 0; i < numTypes; ++i)
        {
            userProfileId = getUserIdForTask(deal, types[i], srk,
                        dealUserProfiles, oUser);
            if (userProfileId != -1)
            {
                break;
            }
        }
        }
        // handle exceptions ...
        if (userProfileId == -1)
        {
            logger
                    .warn("WorkflowAssistBean::generateTask: Warning: No user for required task (will assigning to underwriter): "
                            + "deal Id = "
                            + deal.getDealId()
                            + ", task Id = "
                            + wTask.getTaskId()
                            + ", workflow Id = "
                            + wTask.getWorkflowId());
            // send task to assigned underwriter (if possible)
            UserProfile underwriter = dealUserProfiles.getUnderwriterProfile();
            if (underwriter != null)
            {
                userProfileId = underwriter.getUserProfileId();
            }
            if (userProfileId == -1)
            {
                logger
                        .error("WFE::WorkflowAssistBean:generateTask: task cannot be assigned to underwriter (CANNOT DETERMINE)");
                throw new TaskProcessingException("");
            }
        }
        // create and assigned task(s) - potentially multiple if record ids
        // retrieved
        for (int i = 0; i < numExternalRecordIds; ++i)
        {
            AssignedTask aTask = new AssignedTask(srk);
            try
            {
                aTask.create(wTask, deal.getInstitutionProfileId(), deal.getDealId(), deal.getApplicationId(),
                        userProfileId, -1, 0, new Date(), 
                        deal.getEstimatedClosingDate(),
                        externalRecordIds[i]);
                tasks.add(aTask);
                //-- ========== SCR#537 begins ========== --//
                //-- by Neil on Jan/27/2005
                logger.debug("==>#537by Neil: " + "@WorkflowAssistBean.generateTask()");

                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(userProfileId, deal.getInstitutionProfileId()));
                logger.debug("==>#537by Neil: UserProfileId = " + userProfileId);
                logger.debug("==>#537by Neil: Task Alert = " + up.getTaskAlert());
                if (up.getTaskAlert().trim().equalsIgnoreCase("Y"))
                {
                Contact contact = new Contact(srk);
                contact.findByUserProfileId(userProfileId);
                    int languageId = 0; // default English
                    languageId = contact.getLanguagePreferenceId();

                    String emailSubj = BXResources.getGenericMsg("NEW_TASK_ALERT_EMAIL_SUBJECT", languageId);
                    logger.debug("==>#537by Neil: Email Subject = " + emailSubj);
                    String emailText = BXResources.getGenericMsg("NEW_TASK_ALERT_EMAIL_MESSAGE", languageId);
                    logger.debug("==>#537by Neil: Email = " + emailText);

                    // sending alert message
                    DocumentRequest.requestAlertEmail(srk, deal, contact.getContactEmailAddress(), emailSubj, emailText);
                }

                //String emailSubj = "Subject: "
                //  + "A new task has been assigned to you in Express. "
                //  + "Please sign into the application to review.";
                //String emailText = null;
                //Contact contact = new Contact(srk);
                //contact.findByUserProfileId(userProfileId);
                //DocumentRequest.requestAlertEmail(srk, deal, 
                //  contact.getContactEmailAddress(), emailSubj, emailText);
                //-- ========== SCR#537 ends ========== --//
            }
            catch (Exception e)
            {
                logger
                        .error("WFE::WorkflowAssistBean:generateTask: Error creating required assigned task: "
                                + "deal Id = "
                                + deal.getDealId()
                                + ", task Id = "
                                + wTask.getTaskId()
                                + ", workflow Id = " + wTask.getWorkflowId());
                //logger.error(e);
                throw new TaskProcessingException("");
            }
        }
        return tasks;
    }

    //
    // Private methods:
    //
    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            Helper class to contain multi-value attributes of workflow selection
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * 
     * 
     */
    class WorkflowDetails
    {
        int workflowId = -1;
        boolean allowSOBMapping = true;
        boolean approvalAuthorityQualification = false;

        WorkflowDetails()
        {
        }

        int getWorkflowId()
        {
            return workflowId;
        }

        void setWorkflowId(int workflowId)
        {
            this.workflowId = workflowId;
        }

        boolean getAllowSOBMapping()
        {
            return allowSOBMapping;
        }

        void setAllowSOBMapping(boolean allowSOBMapping)
        {
            this.allowSOBMapping = allowSOBMapping;
        }

        boolean getApprovalAuthorityQualification()
        {
            return approvalAuthorityQualification;
        }

        void setApprovalAuthorityQualification(
                boolean approvalAuthorityQualification)
        {
            this.approvalAuthorityQualification = approvalAuthorityQualification;
        }
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *             Select a workflow for the deal passed. Execute the Workflow Selection Business Rule and stores
     *             selected workflow details in a WorkflowDetails object.
     *            
     *             @return
     *             Workflow details object (class WorkflowDetails). If a workflow can not be determined the &lt;workflowId&gt;
     *             attribute will be -1.
     *            
     *             
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    // SEAN Routing Order Changes [May 04, 2005]: For unit test, I changed the
    // modifier from private to protected.
    protected WorkflowDetails selectWorkflow(Deal deal, SessionResourceKit srk)
    {
        WorkflowDetails wfInfo = new WorkflowDetails();
        //  
        srk.setCloseJdbcConnectionOnRelease(true);
        try
        {
            BusinessRule br = new BusinessRule(
                    srk, -1, -1, "na",
                    Sc.WORFLOW_SELECT_BR_NAME);
            br.setDealId(deal.getDealId(), deal.getCopyId());
            
            // #2242: Catherine, 23-Nov-05 --------- start ---------  
            br.setLenderName();
            BRUtil.debug("WorkflowAssistBean: set LenderName done");
            // #2242: Catherine, 23-Nov-05 --------- end ---------  

            RuleResult rr = null;
            while (br.next())
            {
                rr = br.evaluate();
                if (rr.ruleStatus == true)
                {
                    // first operand is the workflow Id
                    if (rr.opCount <= 0)
                    {
                        continue; // really an error! (no workflow id associated
                        // with exp)
                    }
                    wfInfo.setWorkflowId(rr.getInt(1));
                    if (rr.opCount > 1)
                    {
                        if (rr.getInt(2) != 1)
                        {
                            wfInfo.setAllowSOBMapping(false);
                        }
                        if (rr.opCount > 2)
                        {
                            if (rr.getInt(3) == 1)
                            {
                                wfInfo.setApprovalAuthorityQualification(true);
                            }
                        }
                    }
                    break;
                }
            }
            br.close();
        }
        catch (Exception e)
        {
            wfInfo.setWorkflowId(-1);
            logger
                    .error("WFE::WorkflowAssistBean:selectWorkflow: Business Rule Engine exception generating initital tasks, Business Rule="
                            + Sc.WORFLOW_SELECT_BR_NAME
                            + ", deal Id="
                            + deal.getDealId());
            logger
                    .error("WFE::WorkflowAssistBean:selectWorkflow: Business Rule Engine exception = "
                            + e.getMessage());
        }
        return wfInfo;
    }

	// SEAN Routing Order Changes (May 06, 2005): Go through the order defined
	// in the properties file: ROUTINGORDER.properties to find available
	// underwriters.
	/**
	 * 
	 */
	protected boolean assignUnderwriter(Deal deal,
										 DealUserProfiles dealUserProfiles,
										 WorkflowDetails wfInfo,
										 SessionResourceKit srk) {
        //
		UserProfile uw = null;
        // check for underwriter pre-assigned
        if (dealUserProfiles.getUnderwriterProfile() != null) { return true; }
        // check for admin assigned - if so attempt to assign admin's partner
        UserProfile admin = dealUserProfiles.getAdministratorProfile();
        if (admin != null)
        {
            // assign partner if active underwriter (active stand-in recursive
            // performed)
            dealUserProfiles.setUnderwriterProfile(uq.getUserProfile(admin
                    .getPartnerUserId(), deal.getInstitutionProfileId(), true, true,
                    UserProfileQuery.UNDERWRITER_USER_TYPE, srk));
            // check for underwriter now assigned
            if (dealUserProfiles.getUnderwriterProfile() != null) { return true; }
            // perform underwriter assignment - load balanced group (branch if
            // necessary) underwriters
            Vector uws = uq.getUnderwriters(admin.getGroupProfileId(), -1,
                    true, srk, deal.getInstitutionProfileId());
            uw = getLeastLoadedProfile(uws, srk, deal.getInstitutionProfileId() );
            if (uw != null)
            {
                dealUserProfiles.setUnderwriterProfile(uw);
                return true;
            }
        }
        // perform underwriter assignment as per design - utilizing
        // associations, etc.
        Vector underwriterIds = null;
		//FXP24419, Feb 25 2009 - start
        int institutionId = srk.getExpressState().getDealInstitutionId();
		List<Integer> routingOrder = BXResources.getSysConfigOrderList(institutionId, "ROUTINGORDER");
		for (Iterator<Integer> i = routingOrder.iterator(); i.hasNext(); ) {
		//FXP24419, Feb 25 2009 - end
			// get the value.
			Integer value = (Integer) i.next();
			try {
    			underwriterIds = findUnderWriters(deal, dealUserProfiles, wfInfo,
    											  srk, value.intValue());
    			if ((underwriterIds != null) && (underwriterIds.size() > 0)) {
    				// we found something.
    				break;
    			}
			} catch (Exception e) {
			    logger.error("assignUnderwriter@WorkFlowAssistBean: no underwriter exist - routing " + value);
			}
		}

        // qualify underwriters by approval authority (if necessary)
        if (wfInfo.getApprovalAuthorityQualification() == true)
        {
            Vector qualifiedUnderwriters = qualifyByApprovalAuthority(
                    underwriterIds, deal, srk);
            // ensure at least one underwriter
            if (qualifiedUnderwriters == null
                    || qualifiedUnderwriters.size() == 0) { return false; }
            // perform workload balancing if more than one underwriter - passing
            // list of profiles
            uw = getLeastLoadedProfile(qualifiedUnderwriters,  srk, deal.getInstitutionProfileId());
        }
        else
        {
            // perform workload balancing if more than one underwriter
            uw = getLeastLoadedProfile(underwriterIds, deal.getInstitutionProfileId(), srk, true);
        }
        if (uw == null) { return false; }
        dealUserProfiles.setUnderwriterProfile(uw);
        return true;
	}

	/**
	 * find out the under writers based on the given routing method.
	 */
	protected Vector findUnderWriters(Deal deal, DealUserProfiles dup,
									  WorkflowDetails wfInfo,
									  SessionResourceKit srk,
									  int routingMethod) {

		Vector ids = null;
		switch (routingMethod) {
		case ROUTE_METHOD_SOB2GROUP:
			// Source of Business to Group
			if (deal.getSourceOfBusinessProfileId() > 0) {
				ids =
					performSOBGroupMapping(deal.getSourceOfBusinessProfileId(),
                                           deal.getInstitutionProfileId(),
										   srk, dup);
			}
			break;

		case ROUTE_METHOD_SF2GROUP:
			// Source Firm to Group
			if (deal.getSourceFirmProfileId() > 0) {
				ids = performFirmGroupMapping(deal.getSourceFirmProfileId(),
                                              deal.getInstitutionProfileId(),
											  srk, dup);
			}
			break;

		case ROUTE_METHOD_TABLE:
			// Table routing.
			ids = performTableMapping(deal, srk);
			break;

		case ROUTE_METHOD_SOB2USER:
			// Source of Business to User
			if (wfInfo.getAllowSOBMapping() == true &&
				deal.getSourceOfBusinessProfileId() != 0) {
				ids =
					performSOBUserMapping(deal.getSourceOfBusinessProfileId(),
										  srk);
				ids = ensureActiveProfiles(ids, deal.getInstitutionProfileId(), srk, true);
			}
			break;

		case ROUTE_METHOD_SF2USER:
			// Source Firm to User.
			if (wfInfo.getAllowSOBMapping() == true &&
				deal.getSourceFirmProfileId() != 0) {
				ids = performFirmUserMapping(deal.getSourceFirmProfileId(),
											 srk);
				ids = ensureActiveProfiles(ids, deal.getInstitutionProfileId(), srk, true);
			}
			break;

		case ROUTE_METHOD_WORKFLOW2USER:
			// Workflow to user.
			ids = performWorkflowMapping(wfInfo.getWorkflowId(), deal, srk);
			ids = ensureActiveProfiles(ids, deal.getInstitutionProfileId(), srk, true);
			break;

		case ROUTE_METHOD_FALLBACK:
			// Fallback mapping.
			ids = performFallbackMapping(deal, srk);
			ids = ensureActiveProfiles(ids, deal.getInstitutionProfileId(), srk, true);
			break;

		default:
			break;
		}

		return ids;
	}
	// SEAN Routing Order Changes END

    protected boolean assignUnderwriter0(Deal deal,
            DealUserProfiles dealUserProfiles, WorkflowDetails wfInfo,
            SessionResourceKit srk)
    {
        UserProfile uw = null;
        // check for underwriter pre-assigned
        if (dealUserProfiles.getUnderwriterProfile() != null) { return true; }
        // check for admin assigned - if so attempt to assign admin's partner
        UserProfile admin = dealUserProfiles.getAdministratorProfile();
        if (admin != null)
        {
            // assign partner if active underwriter (active stand-in recursive
            // performed)
            dealUserProfiles.setUnderwriterProfile(uq.getUserProfile(admin
                    .getPartnerUserId(), deal.getInstitutionProfileId(), true, true,
                    UserProfileQuery.UNDERWRITER_USER_TYPE, srk));
            // check for underwriter now assigned
            if (dealUserProfiles.getUnderwriterProfile() != null) { return true; }
            // perform underwriter assignment - load balanced group (branch if
            // necessary) underwriters
            Vector uws = uq.getUnderwriters(admin.getGroupProfileId(), -1,
                    true, srk, deal.getInstitutionProfileId());
            uw = getLeastLoadedProfile(uws, srk, deal.getInstitutionProfileId());
            if (uw != null)
            {
                dealUserProfiles.setUnderwriterProfile(uw);
                return true;
            }
        }
        // perform underwriter assignment as per design - utilizing
        // associations, etc.
        Vector underwriters = null;
        //=================================================
        //--> CRF#37 :: Source/Firm to Group routing for TD
        //=================================================
        //--> Perform SOB to Group routing and then Frim to group routing first
        //--> By Billy 07Aug2003
        if ((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                "com.basis100.workflow.enablesobgrouprouting", "N"))
                .equals("Y")
                && deal.getSourceOfBusinessProfileId() > 0)
        {
            //--> Ticket#135 : Ensure to hunt for Admin and Funder with the
            // SOB/Firm group
            //--> The assigned GroupId from SOB to Group relationship will be
            // stored in the dealUserProfiles
            //--> By Billy 27Nov2003
            underwriters = performSOBGroupMapping(deal.getSourceOfBusinessProfileId(), 
                                                  deal.getInstitutionProfileId(), 
                                                  srk, dealUserProfiles);
            //===============================================================================================
        }
        if ((underwriters == null || underwriters.size() == 0)
                && (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                        "com.basis100.workflow.enablefirmgrouprouting", "N"))
                        .equals("Y") && deal.getSourceFirmProfileId() > 0)
        {
            //--> Ticket#135 : Ensure to hunt for Admin and Funder with the
            // SOB/Firm group
            //--> The assigned GroupId from Frim to Group relationship will be
            // stored in the dealUserProfiles
            //--> By Billy 27Nov2003
            underwriters = performFirmGroupMapping(deal.getSourceFirmProfileId(), 
                                                   deal.getInstitutionProfileId(), 
                                                   srk, dealUserProfiles);
            //===============================================================================================
        }
        //===============================================
        //Added Table Routing -- By BILLY 17Jan2002
        //Check if Table Routing is enabled
        if ((underwriters == null || underwriters.size() == 0)
                && (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                        "com.basis100.workflow.enabletablerouting", "Y"))
                        .equals("Y"))
        {
            underwriters = performTableMapping(deal, srk);
        }
        if (underwriters == null || underwriters.size() == 0)
        {
            if (wfInfo.getAllowSOBMapping() == true
                    && deal.getSourceOfBusinessProfileId() != 0)
            {
                underwriters = performSOBMapping(wfInfo.getWorkflowId(), deal
                        .getSourceOfBusinessProfileId(), srk);
                underwriters = ensureActiveProfiles(underwriters, deal.getInstitutionProfileId(), srk, true);
            }
        }
        if (underwriters == null || underwriters.size() == 0)
        {
            underwriters = performWorkflowMapping(wfInfo.getWorkflowId(), deal,
                    srk);
            underwriters = ensureActiveProfiles(underwriters, deal.getInstitutionProfileId(), srk, true);
        }
        if (underwriters == null || underwriters.size() == 0)
        {
            underwriters = performFallbackMapping(deal, srk);
            Vector activeUws = ensureActiveProfiles(underwriters, deal.getInstitutionProfileId(), srk, true);
            if (activeUws != null && activeUws.size() > 0)
            {
                underwriters = activeUws;
            }
        }
        // ensure at least one underwriter selected
        if (underwriters == null || underwriters.size() == 0) { return false; }
        // qualify underwriters by approval authority (if necessary)
        if (wfInfo.getApprovalAuthorityQualification() == true)
        {
            Vector qualifiedUnderwriters = qualifyByApprovalAuthority(
                    underwriters, deal, srk);
            // ensure at least one underwriter
            if (qualifiedUnderwriters == null
                    || qualifiedUnderwriters.size() == 0) { return false; }
            // perform workload balancing if more than one underwriter - passing
            // list of profiles
            uw = getLeastLoadedProfile(qualifiedUnderwriters,  srk, deal.getInstitutionProfileId());
        }
        else
        {
            // perform workload balancing if more than one underwriter
            uw = getLeastLoadedProfile(underwriters, deal.getInstitutionProfileId(), srk, true);
        }
        if (uw == null) { return false; }
        dealUserProfiles.setUnderwriterProfile(uw);
        return true;
    }

    //// private boolean assignAdmin(Deal deal, DealUserProfiles
    // dealUserProfiles, WorkflowDetails wfInfo, SessionResourceKit srk)
    private boolean assignAdmin(Deal deal, DealUserProfiles dealUserProfiles,
            SessionResourceKit srk)
    {
        UserProfile admin = null;
        // check for admin pre-assigned
        if (dealUserProfiles.getAdministratorProfile() != null) { return true; }
        // attempt to assign underwriter's partner
        UserProfile uw = dealUserProfiles.getUnderwriterProfile();
        if (uw != null)
        {
            // assign partner if active underwriter
            dealUserProfiles.setAdministratorProfile(uq.getUserProfile(uw
                    .getPartnerUserId(), deal.getInstitutionProfileId(), true, true,
                    UserProfileQuery.ADMIN_USER_TYPE, srk));
            // check admin now assigned
            if (dealUserProfiles.getAdministratorProfile() != null) { return true; }
        }
        // perform admin assignment - load balanced group (branch if necessary)
        // admins
        //--> Ticket#135 : Ensure to hunt for Admin and Funder with the
        // SOB/Firm group
        //--> Check to use the Source Group if it's available
        //--> By Billy 27Nov2003
        int groupId = dealUserProfiles.getSourceGroupId();
        if (groupId <= 0)
        {
            groupId = uw.getGroupProfileId();
        }
        Vector admins = uq.getAdministrators(groupId, -1, true, srk, deal.getInstitutionProfileId());
        //============================================================================
        admin = getLeastLoadedProfile(admins, srk, deal.getInstitutionProfileId());
        if (admin != null)
        {
            dealUserProfiles.setAdministratorProfile(admin);
            return true;
        }
        return false;
    }

    // New Method to handle assignment of Funder of the deal -- By Billy
    // 14Jan2002
    private boolean assignFunder(Deal deal, DealUserProfiles dealUserProfiles,
            SessionResourceKit srk)
    {
        UserProfile funder = null;
        // check for funder pre-assigned
        if (dealUserProfiles.getFunderProfile() != null) { return true; }
        UserProfile uw = dealUserProfiles.getUnderwriterProfile();
        // perform funder assignment - load balanced group (branch if necessary)
        // funders
        //--> Ticket#135 : Ensure to hunt for Admin and Funder with the
        // SOB/Firm group
        //--> Check to use the Source Group if it's available
        //--> By Billy 27Nov2003
        int groupId = dealUserProfiles.getSourceGroupId();
        if (groupId <= 0)
        {
            groupId = uw.getGroupProfileId();
        }
        Vector funders = uq.getFunders(groupId, true, srk, deal.getInstitutionProfileId());
        //==============================================================================
        funder = getLeastLoadedProfile(funders,  srk,  deal.getInstitutionProfileId());
        if (funder != null)
        {
            dealUserProfiles.setFunderProfile(funder);
            return true;
        }
        return false;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Given a list of user profile ids return same containing subset which have active profiles. If all
     *               users eliminated return null.
     *            
     *               &lt;allowStandIn&gt; - When &lt;true&gt; an inactive user may be replaced by that user's stand-in (via the recursive
     *                                location method, e.g.
     *            
     *                                       User A (from list), Status = Inactive, Stand-In = User B
     *                                       User B, Status = Inactive, Stand-In = User C
     *                                       User C, Status = Active
     *            
     *                                       User C replace User A in result list
     *            
     *                                 Note, stand-in determination will not occur for [a user with one of] the following
     *                                 profile status codes:
     *            
     *                                        PROFILE_STATUS_SUSPENDED, PROFILE_STATUS_DISALLOWED
     *            
     *               @return
     *               List of user profile ids or null (see above)
     *            
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private Vector ensureActiveProfiles(Vector userIds, int institutionId, SessionResourceKit srk,
            boolean allowStandIn)
    {
        if (userIds == null || userIds.size() == 0) { return null; }

    	int iterations = (int) (userIds.size() / 1000);
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        Vector activeUserIds = new Vector();

    	for (int i = 0; i <= iterations; i++) //check users in increments of 1000
    	{
	        String sql = "Select profilestatusid, userprofileid from userprofile where userprofileid IN ("
	        	    + formCommaDelimitedList(userIds,1000*i, 1000)
	                + ") AND "
	                + "(profilestatusid = "
	                + Sc.PROFILE_STATUS_ACTIVE
	                + " OR profilestatusid = "
	                + Sc.PROFILE_STATUS_DEACTIVE
	                + " OR profilestatusid = " + Sc.PROFILE_STATUS_CAUTION + ")";
	        try
	        {
	            int key = jExec.execute(sql);
	            while (jExec.next(key))
	            {
	                int userProfileStatusId = jExec.getInt(key, 1);
	                int userId = jExec.getInt(key, 2);
	                if (userProfileStatusId != Sc.PROFILE_STATUS_ACTIVE)
	                {
	                    if (allowStandIn == false)
	                    {
	                        continue;
	                    }
	                    // attempt to use stand-in
	                    UserProfile up = uq.getUserProfile(userId, institutionId, true, true,
	                            UserProfileQuery.UNDERWRITER_USER_TYPE, srk);
	                    if (up == null
	                            || up.getProfileStatusId() != Sc.PROFILE_STATUS_ACTIVE)
	                    {
	                        continue;
	                    }
	                    // have an active stand-in
	                    userId = up.getUserProfileId();
	                }
	                activeUserIds.add(new Integer(userId));
	            }
	            jExec.closeData(key);
	        }
	        catch (Exception e)
	        {
	            logger
	                    .error("WFE::WorkflowAssistBean:ensureActiveProfiles: Error qualifying by active profile - assume all inactive");
	            logger.error("WFE::WorkflowAssistBean:ensureActiveProfiles: sql = "
	                    + sql);
	            logger.error("WFE::WorkflowAssistBean:ensureActiveProfiles: err = "
	                    + e.getMessage());
	            return null;
	        }
    	}
        if (activeUserIds.size() == 0) { return null; }
        return activeUserIds;
    }

    // Same as the above but with inputing a list of UserProfiles Objects
    // instead of UserIds
    //    -- By BILLY 17Jan2002
    private Vector ensureActiveProfiles2(Vector userProfiles,  
            SessionResourceKit srk, boolean allowStandIn, int institutionId)
    {
        if (userProfiles == null || userProfiles.size() == 0) { return null; }
        
        Vector activeUserIds = new Vector();
        int len = userProfiles.size();
        UserProfile currUser = null;
        try
        {
            for (int i = 0; i < len; i++)
            {
                currUser = (UserProfile) userProfiles.elementAt(i);
                int userProfileStatusId = currUser.getProfileStatusId();
                int userId = currUser.getUserProfileId();
                if (userProfileStatusId != Sc.PROFILE_STATUS_ACTIVE)
                {
                    if (allowStandIn == false)
                    {
                        continue;
                    }
                    // attempt to use stand-in
                    UserProfile up = uq.getUserProfile(userId, institutionId, true, true,
                            UserProfileQuery.UNDERWRITER_USER_TYPE, srk);
                    if (up == null
                            || up.getProfileStatusId() != Sc.PROFILE_STATUS_ACTIVE)
                    {
                        continue;
                    }
                    // have an active stand-in
                    userId = up.getUserProfileId();
                }
                activeUserIds.add(new Integer(userId));
            }
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:ensureActiveProfiles2: Error qualifying by active profile - assume all inactive");
            logger.error("WFE::WorkflowAssistBean:ensureActiveProfiles: err = "
                    + e.getMessage());
            return null;
        }
        if (activeUserIds.size() == 0) { return null; }
        return activeUserIds;
    }

    // Overload of previous declaration.  Allows us to only form a list
    // based on a pre-determined subset of the vector.
    // For FXP23637
    private String formCommaDelimitedList(Vector values, int start_index, int how_many)
    {
    	//limit how_many to 1000 due to Oracle's list processing limitation: ORA-01795
        if (values == null || values.size() == 0 || start_index < 0 || start_index > values.size() || how_many > 1000) { return "-1"; }
        StringBuffer sb = new StringBuffer();

        int lim = start_index + how_many;
        if (lim > values.size())
        	lim = values.size();

        for (int i = start_index; i < lim; ++i)
        {
            Object val = values.elementAt(i);
            if (val instanceof String)
            {
                sb.append("\"" + (String) val + "\"");
            }
            else
            {
                sb.append("" + ((Integer) val).intValue());
            }
            if (i != lim - 1)
            {
                sb.append(",");
            }
        }
        return sb.toString();
    }
    
    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Called during underwriter assignment to a deal. This method operates on the following
     *               associative relationships in the MOS schema:
     *            
     *                . Source of Business (broker) to User(s) Association
     *                . Source Firm to User(s) Association (used if former fails to locate at leat 1 underwriter).
     *            
     *               @return
     *               List of underwriters located via the associations discussed above. null is returned if no
     *               undedrwriter is located.
     *            
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    // SEAN Routing Order Changes [May 03, 2005]: for testing I changed the
    // modifer from private to protected.
    protected Vector performSOBMapping(int workflowId, int sobId,
            SessionResourceKit srk)
    {
        Vector underwriters = new Vector();
        String sql = "Select userProfileId from SOURCETOUSERASSOC where sourceOfBusinessProfileId = "
                + sobId;
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                int userProfileId = jExec.getInt(key, 1);
                underwriters.addElement(new Integer(userProfileId));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performSOBMapping: Error on query Source Of Business to Underwriter association");
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: sql = "
                    + sql);
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: err = "
                    + e.getMessage());
        }
        if (underwriters.size() > 0) { return underwriters; }
        // no underwriters located - move to source firm mapping ...
        //---------------- FXLink Phase II ------------------//
        //--> Modified to use SOB.sourceFirmProfileId instead of
        // SOURCETOFIRMASSOC table
        //--> By Billy 17Nov2003
        //sql = "Select sourceFirmProfileId from SOURCETOFIRMASSOC where
        // sourceOfBusinessProfileId = " + sobId;
        sql = "Select sourceFirmProfileId from sourceOfBusinessProfile where sourceOfBusinessProfileId = "
                + sobId;
        //=====================================================
        int sourceFirmId = -1;
        try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                sourceFirmId = jExec.getInt(key, 1);
                break;
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performSOBMapping: Error locating Source Firm for Source of Business Id = "
                            + sobId);
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: sql = "
                    + sql);
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: err = "
                    + e.getMessage());
            return null;
        }
        if (sourceFirmId == -1)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performSOBMapping: Error locating Source Firm for Source of Business Id = "
                            + sobId);
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: sql = "
                    + sql);
            logger
                    .error("WFE::WorkflowAssistBean:performSOBMapping: err = No mapping to Source Firm for Source of Business defined.");
            return null;
        }
        // check source firm to underwriter mapping
        sql = "Select userProfileId from SOURCEFIRMTOUSERASSOC where sourceFirmProfileId = "
                + sourceFirmId;
        try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                underwriters.addElement(new Integer(jExec.getInt(key, 1)));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performSOBMapping: Error on query Source Firm to Underwriter association");
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: sql = "
                    + sql);
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: err = "
                    + e.getMessage());
            return null;
        }
        return underwriters;
    }

	// SEAN Routing Order Changes (May 05, 2005): Split the method
	// performSOBMapping.
	/**
	 * Accouting to the Routing Order Changes spec, we need split the method
	 * performSOBMapping to two spearate method: SOB to user routing and
	 * SourceFirm to user mapping.  In method performSOBMapping, the param
	 * workflowId has never been used, so don't keep this param in the two new
	 * methods.
	 * <p>
	 * Mapping SOB to user.
	 */
	protected Vector performSOBUserMapping(int sobId, SessionResourceKit srk) {

        Vector underwriters = new Vector();
        String sql = "Select userProfileId from SOURCETOUSERASSOC where sourceOfBusinessProfileId = "
                + sobId;
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                int userProfileId = jExec.getInt(key, 1);
                underwriters.addElement(new Integer(userProfileId));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performSOBMapping: Error on query Source Of Business to Underwriter association");
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: sql = "
                    + sql);
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: err = "
                    + e.getMessage());
        }

		return underwriters;
	}

	/**
	 * Mapping Source Firm to User.
	 */
	protected Vector performFirmUserMapping(int sfId, SessionResourceKit srk) {

		Vector underwriters = new Vector();
		String sql = "Select userProfileId from SOURCEFIRMTOUSERASSOC where sourceFirmProfileId = "
                + sfId;
		
        JdbcExecutor jExec = srk.getJdbcExecutor();
		try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                underwriters.addElement(new Integer(jExec.getInt(key, 1)));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performSOBMapping: Error on query Source Firm to Underwriter association");
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: sql = "
                    + sql);
            logger.error("WFE::WorkflowAssistBean:performSOBMapping: err = "
                    + e.getMessage());
            return null;
        }

        return underwriters;
	}
	// SEAN Routing Order Changes END

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Called during underwriter assignment to a deal. This method operates on the following
     *               associative relationships in the MOS schema:
     *            
     *                . Workflow to Group
     *                . Workflow to Branch (used if former fails to locate a group).
     *            
     *                Note:
     *            
     *                Branch and group qualified by origination province using branch to province association
     *                table.
     *            
     *               Groups or branches located via the mappings (if any) are qualified against the
     *               province of origination of the deal - See Consolidate Design for determination of origination
     *               province of the deal.
     *            
     *               Should all groups/branches be eliminated during
     *               qualification the method will use the unqualified list of groups/branches.
     *            
     *               @return
     *               List of underwriters (from groups/branches [qualified or not as per above]) located
     *               via the workflow mappings (associations discussed above). null is returned if no
     *               workflow to group/branch mapping are located in the workflow-group/branch association
     *               tables.
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    // SEAN Routing Order Changes [May 03, 2005]: for testing I changed the
    // modifer from private to protected.
    protected Vector performWorkflowMapping(int workflowId, Deal deal,
            SessionResourceKit srk)
    {
        boolean isGroups = false;
        boolean isBranches = false;
        Vector grpsOrBranches = new Vector();
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        String sql = null;
        int key = 0;
        // first check for group mapping
        try
        {
            sql = "Select groupProfileId from WORKFLOWTOGROUPASSOC where workflowId = "
                    + workflowId;
            key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                isGroups = true;
                grpsOrBranches.addElement(new Integer(jExec.getInt(key, 1)));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            try
            {
                jExec.closeData(key);
            }
            catch (Exception ee)
            {
                ;
            }
            isGroups = false;
            logger
                    .error("WFE::WorkflowAssistBean:performWorkflowMapping: Error on query Workflow to Groups association (ignoring)");
            logger
                    .error("WFE::WorkflowAssistBean:performWorkflowMapping: sql = "
                            + sql);
            logger
                    .error("WFE::WorkflowAssistBean:performWorkflowMapping: err = "
                            + e.getMessage());
        }
        if (isGroups == false)
        {
            grpsOrBranches = new Vector();
            sql = "Select branchProfileId from WORKFLOWTOBRANCHASSOC where workflowId = "
                    + workflowId;
            try
            {
                key = jExec.execute(sql);
                for (; jExec.next(key);)
                {
                    isBranches = true;
                    grpsOrBranches
                            .addElement(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                isBranches = false;
                logger
                        .error("WFE::WorkflowAssistBean:performWorkflowMapping: Error on query Workflow to Branches association (ignoring, using all branches)");
                logger
                        .error("WFE::WorkflowAssistBean:performWorkflowMapping: sql = "
                                + sql);
                logger
                        .error("WFE::WorkflowAssistBean:performWorkflowMapping: err = "
                                + e.getMessage());
            }
        }
        // ensure something found
        if (isGroups == false && isBranches == false) { return null; }
        // get deal origination province
        int originationProv = getOriginationProvince(deal, srk);
        Vector groups = groupsOptFromBranchesQualifyGeographically(isGroups,
                grpsOrBranches, originationProv, srk);
        return underwritersFromGroups(groups, srk);
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Called during underwriter assignment to a deal as a last measure. A call to this
     *               method indicates that all other measures have failed to locate an underwriter.
     *            
     *               Specifically, the method accesses all branch profiles, eliminates those that do not
     *               match the province of origination (determined from the deal) and returns a list of
     *               underwriters from the qualified branches. If all branches are eliminated during
     *               province of origination, the the method returns a list of all underwriters across
     *               all branches unqualified.
     *            
     *               @return
     *               List (vector) of underwriters or null if no underwriters located.
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    // SEAN Routing Order Changes [May 03, 2005]: for testing I changed the
    // modifer from private to protected.
    protected Vector performFallbackMapping(Deal deal, SessionResourceKit srk)
    {
        Vector branches = new Vector();
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        String sql = null;
        sql = "Select branchProfileId from BRANCHPROFILE";
        try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                branches.addElement(new Integer(jExec.getInt(key, 1)));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            branches = null;
            logger
                    .error("WFE::WorkflowAssistBean:performFallbackMapping: Error on query all branches");
            logger
                    .error("WFE::WorkflowAssistBean:performFallbackMapping: sql = "
                            + sql);
            logger
                    .error("WFE::WorkflowAssistBean:performFallbackMapping: err = "
                            + e.getMessage());
        }
        if (branches == null || branches.size() == 0) { return null; }
        int originationProvince = getOriginationProvince(deal, srk);
        Vector groups = groupsOptFromBranchesQualifyGeographically(false,
                branches, originationProvince, srk);
        // if no groups because no matching prov. then fall back to all groups
        // from branches
        if ((groups == null || groups.size() == 0) && originationProvince != -1)
        {
            groups = groupsOptFromBranchesQualifyGeographically(false,
                    branches, -1, srk);
            // underwriters from groups
        }
        if (groups != null && groups.size() > 0) { return (underwritersFromGroups(
                groups, srk)); }
        return null;
    }

    /*
     * public int performFallbackMappingTest(int originationProvince,
     * SessionResourceKit srk) { Vector branches = new Vector();
     * 
     * 
     * 
     * JdbcExecutor jExec = srk.getJdbcExecutor();
     * 
     * String sql = null;
     * 
     * sql = "Select branchProfileId from BRANCHPROFILE"; try { int key =
     * jExec.execute(sql);
     * 
     * for (; jExec.next(key); ) { branches.addElement(new
     * Integer(jExec.getInt(key, 1))); }
     * 
     * jExec.closeData(key); } catch (Exception e) { branches = null;
     * logger.error("WFE::WorkflowAssistBean:performFallbackMapping: Error on
     * query all branches");
     * logger.error("WFE::WorkflowAssistBean:performFallbackMapping: sql = " +
     * sql); logger.error("WFE::WorkflowAssistBean:performFallbackMapping: err = " +
     * e.getMessage()); }
     * 
     * if (branches == null || branches.size() == 0) return 0;
     * 
     * Vector groups = groupsOptFromBranchesQualifyGeographically(false,
     * branches, originationProvince, srk);
     * 
     * return 0; }
     */
    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Given a deal entity determine origination province (id) as:
     *            
     *               �  The province of the principal property in the Deal, if one, or
     *               �  The province of the current address of the principal borrower, if one, or
     *               �  The province of the current address of the first borrower, if one, or
     *               �  The province of the current address of the SourceOfBusiness, if one, or
     *               .  Ontario.
     *            
     *               @return
     *               Province Id or -1 if could not be determined (in latter case error messages are delivered
     *               to the system log file).
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private int getOriginationProvince(Deal deal, SessionResourceKit srk)
    {
        int ONTARIO = 9;
        
        //JdbcExecutor jExec = srk.getJdbcExecutor();
        int originationProvince = -1;
        // first by principal property
        try
        {
            Property prop = new Property(srk, null);
            prop.setSilentMode(true);
            prop = prop.findByPrimaryProperty(deal.getDealId(), deal
                    .getCopyId(), deal.getInstitutionProfileId());
            originationProvince = prop.getProvinceId();
        }
        catch (Exception e)
        {
            originationProvince = -1;
            logger
                    .trace("WFE::WorkflowAssistBean:getOriginationProvince: Could not locate primary property on deal (ignoring)");
            logger
                    .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                            + e.getMessage());
        }
        if (originationProvince != -1) { return originationProvince; }
        int borrowerId = -1;
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            borrowerId = borrower.getBorrowerId();
        }
        catch (Exception e)
        {
            borrowerId = -1;
            logger
                    .trace("WFE::WorkflowAssistBean:getOriginationProvince: Could not locate address of primary borrower (ignoring)");
            logger
                    .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                            + e.getMessage());
        }
        if (borrowerId == -1)
        {
            try
            {
                Borrower borrower = new Borrower(srk, null);
                borrower = borrower.findByFirstBorrower(deal.getDealId(), deal
                        .getCopyId());
                borrowerId = borrower.getBorrowerId();
            }
            catch (Exception e)
            {
                borrowerId = -1;
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: Could not locate address of first borrower (ignoring)");
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                + e.getMessage());
            }
        }
        if (borrowerId > -1)
        {
            // Borrrower Found
            int addrId = -1;
            try
            {
                BorrowerAddress bAddr = new BorrowerAddress(srk);
                bAddr = bAddr
                        .findByCurrentAddress(borrowerId, deal.getCopyId());
                addrId = bAddr.getAddrId();
            }
            catch (Exception e)
            {
                addrId = -1;
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: Error locating address of borrower (ignoring)");
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                + e.getMessage());
            }
            if (addrId > -1)
            {
                try
                {
                    Addr addr = new Addr(srk);
                    addr = addr.findByPrimaryKey(new AddrPK(addrId, deal
                            .getCopyId()));
                    originationProvince = addr.getProvinceId();
                }
                catch (Exception e)
                {
                    originationProvince = -1;
                    logger
                            .trace("WFE::WorkflowAssistBean:getOriginationProvince: Error locating address of borrower(2) (ignoring)");
                    logger
                            .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                    + e.getMessage());
                }
            }
        }
        // If still fail look for the address of the SOB -- By Billy14Jan2002
        if (originationProvince == -1)
        {
            try
            {
                originationProvince = deal.getSourceOfBusinessProfile()
                        .getContact().getAddr().getProvinceId();
            }
            catch (Exception e)
            {
                originationProvince = -1;
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: Error locating address of SourceOfBusiness (ignoring)");
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                + e.getMessage());
            }
        }
        if (originationProvince == -1) { return ONTARIO; }
        return originationProvince;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Called during user assignment to a deal. Locates list of underwriters in a list of group.
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private Vector underwritersFromGroups(Vector groups, SessionResourceKit srk)
    {
        if (groups == null || groups.size() == 0) { return null; }
        Vector underwriters = new Vector();
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        String sql = null;
        int key = 0;
        int len = groups.size();
        for (int i = 0; i < len; ++i)
        {
            int groupId = ((Integer) groups.elementAt(i)).intValue();
            try
            {
                sql = "Select userProfileId from USERPROFILE where groupProfileId = "
                        + groupId
                        + " AND userTypeId IN("
                        + Mc.USER_TYPE_JR_UNDERWRITER
                        + ", "
                        + Mc.USER_TYPE_UNDERWRITER
                        + ", "
                        + Mc.USER_TYPE_SR_UNDERWRITER + ")";
                key = jExec.execute(sql);
                for (; jExec.next(key);)
                {
                    underwriters.addElement(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .error("WFE::WorkflowAssistBean:underwritersFromGroups: Error locating underwriters for group Id = "
                                + groupId + "(ignoring group)");
                logger
                        .error("WFE::WorkflowAssistBean:underwritersFromGroups: sql = "
                                + sql);
                logger
                        .error("WFE::WorkflowAssistBean:underwritersFromGroups: err = "
                                + e.getMessage());
                return null;
            }
        }
        return underwriters;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Return vector of underwriters (user profiles of underwriters) that:
     *            
     *                . have sufficient approval authority (for LOB) - but see &lt;approvalAuthorityQualification&gt;
     *            
     *               &lt;underwriters&gt; contains Ids of underwriter to qualify.
     *               &lt;approvalAuthorityQualification&gt; - true:  approval authority qualification is performed
     *                                                 false:  approval authority qualification not performed
     *            
     *               @result
     *               List of underwriters (user profiles of underwriters). null is returned if no underwriter is located.
     *            
     *               Notes:
     *            
     *               If none of the underwriters have sufficient approval authority (and AA screening in effect) we return a
     *               list of the most senior types (see code for more detail) without approval authority qualification.
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private Vector qualifyByApprovalAuthority(Vector underwriters, Deal deal,
            SessionResourceKit srk)
    {
        if (underwriters == null || (underwriters.size() == 0)) { return null; }
        //
        double totalLoanAmount = deal.getTotalLoanAmount();
        int lineOfBusinessId = deal.getLineOfBusinessId();
        int len = underwriters.size();
        Vector qualifiedUnderwriters = new Vector();
        Vector mostSeniors = new Vector();
        int mostSeniorType = Mc.USER_TYPE_JR_UNDERWRITER;
        for (int i = 0; i < len; ++i)
        {
            int underwriterId = ((Integer) underwriters.elementAt(i))
                    .intValue();
            // get profile for underwriter (or stand-in recursive). require:
            // active, underwriter
            UserProfile userProfile = uq.getUserProfile(underwriterId, 
                                                        deal.getInstitutionProfileId(), 
                                                        true, true, 1, srk);
            if (userProfile == null)
            {
                continue;
            }
            // add to list of most senior (as necessary)
            int uType = userProfile.getUserTypeId();
            if (mostSeniorType == Mc.USER_TYPE_JR_UNDERWRITER
                    && (uType == Mc.USER_TYPE_UNDERWRITER || uType >= Mc.USER_TYPE_SR_UNDERWRITER))
            {
                mostSeniorType = uType;
                mostSeniors = new Vector();
            }
            else if (mostSeniorType == Mc.USER_TYPE_UNDERWRITER
                    && uType >= Mc.USER_TYPE_SR_UNDERWRITER)
            {
                mostSeniorType = Mc.USER_TYPE_SR_UNDERWRITER;
                mostSeniors = new Vector();
            }
            if (uType >= mostSeniorType)
            {
                mostSeniors.addElement(userProfile);
            }
            if (true == userProfile.hasApprovalAuthorityForLOB(totalLoanAmount,
                    lineOfBusinessId))
            {
                qualifiedUnderwriters.addElement(userProfile);
            }
        }
        if (qualifiedUnderwriters != null && qualifiedUnderwriters.size() > 0) { return qualifiedUnderwriters; }
        // return most seniors (if any)
        if (mostSeniors == null || (mostSeniors.size() == 0)) { return null; }
        return mostSeniors;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *              Perform workload balancing (algorithm documented in MOS Design) across a list of users (vector of
     *              user Ids provided.
     *            
     *              Two versions provided, one takes a lost of user profiles, the other a list of user profile ids. Dummy
     *              argument used to differentiate between versions
     *            
     *              @return
     *              User profile of least loaded user - optimization: returns immediately first encountered user
     *              without an workload (no work currently assigned to).
     *              
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    public UserProfile getLeastLoadedProfile(Vector userProfiles,  
            SessionResourceKit srk, int institutionId)
    {
        if (userProfiles == null) { return null; }
        int lim = userProfiles.size();
        if (lim == 0) { return null; }
        if (lim == 1) { return (UserProfile) userProfiles.elementAt(0); }
        UserProfile up = null;
        Vector userIds = new Vector();
        for (int i = 0; i < lim; ++i)
        {
            up = (UserProfile) userProfiles.elementAt(i);
            userIds.add(new Integer(up.getUserProfileId()));
        }
        int userId = leastLoaded(userIds, institutionId, srk);
        if (userId == -1) { return null; }
        // locate and return profile for returned id
        for (int i = 0; i < lim; ++i)
        {
            up = (UserProfile) userProfiles.elementAt(i);
            if (up.getUserProfileId() == userId) { return up; }
        }
        // should never happen ... but ...
        return null;
    }

    private UserProfile getLeastLoadedProfile(Vector userIds, int institutionId, 
            SessionResourceKit srk, boolean dummy)
    {
        int userId = leastLoaded(userIds, institutionId, srk);
        if (userId == -1) { return null; }
        UserProfile up = null;
        try
        {
            up = new UserProfile(srk);
            up = up.findByPrimaryKey(new UserProfileBeanPK(userId, institutionId));
        }
        catch (Exception e)
        {
            
            logger
                    .error("WFE::WorkflowAssistBean:getLeastLoadedProfile: Error obtaining profile for user Id = "
                            + userId + ", return null");
            return null;
        }
        return up;
    }

    private int leastLoaded(Vector userIds, int institutionid, SessionResourceKit srk)
    {
        if (userIds == null) { return -1; }
        if (userIds.size() == 1) { return ((Integer) userIds.elementAt(0))
                .intValue(); }
        JdbcExecutor jExec = srk.getJdbcExecutor();
        String sql = null;
        int len = userIds.size();
        double load = -1.0;
        int selectedUserProfileId = 0;
        //--Merge--EncriptedPassword--//
        // Shuffle the list to make sure not always assign the task to the same
        // people
        // -- By Billy 29Oct2002
        Collections.shuffle(userIds);
        //============================================================================
        String userProfileIds = "";
        int userProfileId;
        for (int j = 0; j < len; ++j)
        {
        	userProfileIds += (((String) userIds.elementAt(j).toString()) +",");
        }
        // prepare the userprofile ids for using in 'IN' Clause
        if(userProfileIds.length() != 0)
        	userProfileIds = userProfileIds.substring(0, userProfileIds.length() -1);
        
        try
        {
            // Modified to weight the loading by Load Factor -- By BILLY
            // 18Feb2002
            sql = "Select SUM(t.loadFactor), a.userprofileid from ASSIGNEDTASKSWORKQUEUE a, TASK t where a.userprofileId in ("
                    + userProfileIds
                    + " ) AND "
                    + "(a.taskStatusId = "
                    + Sc.TASK_OPEN
                    + " OR a.taskStatusId = "
                    + Sc.TASK_REROUTE + ")" + " AND a.taskId = t.taskId group by userprofileid";
            
            int key = jExec.execute(sql);
            float userLoad = 0;
          
            for (int i = 0; jExec.next(key); ++i)
            {
                //userLoad += (11 - jExec.getInt(key, 1))/10.0;
                userLoad = jExec.getInt(key, 1);
                userProfileId = jExec.getInt(key, 2);
                
                logger.debug("BILLY ===> Userid : " + userProfileId
                        + " Total Load factor: " + userLoad);
                if (userLoad == 0.0) { 
                	return userProfileId; 
                }
                
                if (userLoad < load || i == 0)
                {
                    load = userLoad;
                    selectedUserProfileId = userProfileId;
                }
            }
            
            jExec.closeData(key);    
        }
        catch (Exception e)
        {
            logger.error("WFE::WorkflowAssistBean:leastLoaded: Error on workload query for user list = "
                            + userProfileIds);
            logger.error("WFE::WorkflowAssistBean:leastLoaded: sql = "
                    + sql);
            logger.error("WFE::WorkflowAssistBean:leastLoaded: err = "
                    + e);
        }
    
        return selectedUserProfileId;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *             Called during task generation activity. This important method determines the user to receive
     *             a task based upon the user type passed.
     *            
     *             Preference is given to users [already] assigned to the deal. Specifically:
     *            
     *              . if the requested type is an administrator type the deal administrator is selected if that
     *                users' type is at least as high as the requested type; e.g. if requested type is administrator
     *                the deal administrator is suitable if he/she has type administator or sr. administrator.
     *            
     *              . if the requested type is a type that can exercise underwriter decisioning (junior underwriter and
     *                and all higer types) the deal underwriter is selected if that users' type is at least as high
     *                as the requested type.
     *            
     *             If deal users do not qualify the method then attempts to find a user from the assigned underwriters' group
     *             (first) then branch. The user must have the requrested type (with application of 'minimum' type within the
     *             'pure' administrator and underwriter types) and be active. This approach may yield multiple qualified users.
     *             In this case workload balancing (as documented in MOS Design) is applied to select a single user.
     *            
     *             Finally, if the requested type is a quasi-type (e.g. deal administrator) the user implied by
     *             the type is selected; see MOS Design for description of quasi-types used in Task Generation
     *             Business Rule. Request of a quasi-type overrides the behavior described above.
     *            
     *             Additiona : 14Feb2002 by BILLY
     *             If the input type is a negetive number e.g. -1234 that means to assign to UserId = 1234 if active, otherwise,
     *             look for his/her stand-in and so on.
     *            
     *             
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    public int getUserIdForTask(Deal deal, int type, SessionResourceKit srk,
            DealUserProfiles dealUserProfiles, UserProfile originalUser)
    {
        UserProfile underwriter = null;
        UserProfile admin = null;
        int userId = -1;
        // Added to check if assign to UserId directly -- By BILLY 14Feb2002
        if (type < 0)
        {
            UserProfile userToAssign = uq.getUserProfile(-1 * type, deal.getInstitutionProfileId(), true, true,
                    UserProfileQuery.ANY_USER_TYPE, srk);
            if (userToAssign != null)
            {
                userId = userToAssign.getUserProfileId();
                return userId;
            }
            //else fall back to Hunt by the Group and Branch
        }
        
        if (originalUser != null ) 
            return findActiveUserForTask(originalUser.getUserProfileId(), deal.getInstitutionProfileId(), srk);  
            
        //====================================================================
        // first check for quasi-types
        // Added FUNDER Quasi-type -- By BILLY 14Jan2002
        if (type == Mc.USER_DEAL_ADMINISTRATOR
                || type == Mc.USER_DEAL_UNDERWRITER
                || type == Mc.USER_SECOND_APPROVER
                || type == Mc.USER_JOINT_APPROVER
                || type == Mc.USER_DEAL_CURERENT_APPROVER
                || type == Mc.USER_DEAL_FUNDER)
        {
            switch (type)
            {
            case Mc.USER_DEAL_ADMINISTRATOR:
                userId = deal.getAdministratorId();
                break;
            case Mc.USER_DEAL_UNDERWRITER:
                userId = deal.getUnderwriterUserId();
                break;
            case Mc.USER_SECOND_APPROVER:
                underwriter = dealUserProfiles.getUnderwriterProfile();
                userId = underwriter.getSecondApproverUserId();
                break;
            case Mc.USER_JOINT_APPROVER:
                underwriter = dealUserProfiles.getUnderwriterProfile();
                userId = underwriter.getJointApproverUserId();
                break;
            case Mc.USER_DEAL_CURERENT_APPROVER:
                userId = deal.currentApprovedUserId();
                break;
            case Mc.USER_DEAL_FUNDER:
                underwriter = dealUserProfiles.getUnderwriterProfile();
                userId = deal.getFunderProfileId();
                break;
            }
            if (userId > 0) { return findActiveUserForTask(userId, deal.getInstitutionProfileId(), srk); }
        }
        // next, preference to [already] assigned deal user ....
        // check for deal administrator qualifies (type requested must be an
        // administrator type)
        if (type == Mc.USER_TYPE_JR_ADMIN || type == Mc.USER_TYPE_ADMIN
                || type == Mc.USER_TYPE_SR_ADMIN)
        {
            admin = dealUserProfiles.getAdministratorProfile();
            if (admin != null && admin.getUserTypeId() >= type) { return findActiveUserForTask(
                    admin.getUserProfileId(), deal.getInstitutionProfileId(), srk); }
        }
        // check for deal underwriter qualifies (type requested must be type
        // with approval authority - all uw types and
        // all higher excepting system administrators)
        if (type >= Mc.USER_TYPE_JR_UNDERWRITER
                && type < Mc.USER_TYPE_SYS_ADMINISTRATOR)
        {
            underwriter = dealUserProfiles.getUnderwriterProfile();
            if (underwriter != null && underwriter.getUserTypeId() >= type) { return findActiveUserForTask(
                    underwriter.getUserProfileId(), deal.getInstitutionProfileId(), srk); }
        }
        // no user from deal qualifies ... locate from uw's group/branch ...
        underwriter = dealUserProfiles.getUnderwriterProfile();
        if (underwriter == null) { return -1; // precaution ... should never
                                              // happen ... but ...
        }
        Vector users = uq.getUsers(underwriter.getGroupProfileId(), type, true,
                srk, deal.getInstitutionProfileId());
        if (users == null || users.size() == 0) { return -1; }
        // load balance where multiple active users located
        UserProfile user = getLeastLoadedProfile(users, srk, deal.getInstitutionProfileId());
        if (user == null) { return -1; }
        return user.getUserProfileId();
    }

    // getProvincesForBranch
    //
    // Get the (routing) provinces for a branch (id). The association branch to
    // province is a many-to-many -
    // hence a given branch may accept deals originating from more than one
    // province (0..n).
    //
    // Args: method - 1 : locate (routing) provinces from branch to province
    // association table
    //                0 : locate province from the branch's address (i.e. only one province
    // possible in this case,
    //                    the province in which the branch is geographically located.)
    //
    //       Note: For the purposes of efficiency the routing table should contain for
    // each branch a record that
    //             associates the branch with it's geographical province. This will ensure
    // that a least one province
    //             will be located with method == 1, hence avoiding the need to search using
    // method == 0; the latter
    //             requires 3 separate query statements!.
    //
    // Notes: If method 1 is specified but fails method 0 is used.
    //
    //        Returns: vector containing province ids that branch accepts deals from,
    // or null if failure occurs.
    //
    private Vector getProvincesForBranch(int branchId, int method,
            JdbcExecutor jExec)
    {
        String sql = null;
        int key = 0;
        Vector provs = new Vector();
        if (method == 1)
        {
            try
            {
                // via branch to province association
                sql = "select PROVINCEID from PROVINCEBRANCHROUTING where BRANCHPROFILEID = "
                        + branchId;
                key = jExec.execute(sql);
                while (jExec.next(key))
                {
                    provs.add(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
                if (provs.size() > 0) { return provs; }
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .trace("WFE::WorkflowAssistBean:getProvinceForBranch: Error locating province id (from branch-prov association): branchId = "
                                + branchId
                                + ", (ignoring - get province from branch address)");
                logger
                        .trace("WFE::WorkflowAssistBean:getProvinceForBranch: sql = "
                                + sql);
                logger
                        .trace("WFE::WorkflowAssistBean:getProvinceForBranch: err = "
                                + e.getMessage());
                provs = new Vector();
            }
        }
        // via address
        try
        {
            // via branch address
            sql = "select contactId from BRANCHPROFILE where branchProfileId = "
                    + branchId;
            key = jExec.execute(sql);
            jExec.next(key);
            int contactId = jExec.getInt(key, 1);
            jExec.closeData(key);
            sql = "select addrId from CONTACT where contactId = " + contactId;
            key = jExec.execute(sql);
            jExec.next(key);
            int addrId = jExec.getInt(key, 1);
            jExec.closeData(key);
            sql = "select provinceId from ADDR where addrId = " + addrId;
            key = jExec.execute(sql);
            jExec.next(key);
            provs.add(new Integer(jExec.getInt(key, 1)));
            jExec.closeData(key);
            return provs;
        }
        catch (Exception e)
        {
            try
            {
                jExec.closeData(key);
            }
            catch (Exception ee)
            {
                ;
            }
            logger
                    .trace("WFE::WorkflowAssistBean:getProvinceForBranch: Error locating province id (from branch address): branchId = : branchId = "
                            + branchId);
            logger.trace("WFE::WorkflowAssistBean:getProvinceForBranch: sql = "
                    + sql);
            logger.trace("WFE::WorkflowAssistBean:getProvinceForBranch: err = "
                    + e.getMessage());
        }
        return null;
    }

    // groupsOptFromBranchesQualifyGeographically
    //
    // Return a list of groups, optionally from a list of branches, qualified by
    // province (if province
    // not -1 - but this should never happen)
    private Vector groupsOptFromBranchesQualifyGeographically(boolean isGroups,
            Vector list, int provinceId, SessionResourceKit srk)
    {
        String sql = null;
        Vector qualifiedGroups = new Vector();
        Vector qualifiedBranches = new Vector();
        Integer minusOne = new Integer(-1);
        int key = 0;
        Vector groups = list;
        Vector branches = list;
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        // if groups provided form a list of the corresponding branches
        if (isGroups == true)
        {
            int lim = groups.size();
            for (int i = 0; i < lim; ++i)
            {
                branches.setElementAt(minusOne, i);
            }
            try
            {
                sql = "Select GROUPPROFILEID, BRANCHPROFILEID from GROUPPROFILE where GROUPPROFILEID in ("
                        + formCommaDelimitedList(groups) + ")";
                key = jExec.execute(sql);
                while (jExec.next(key))
                {
                    int gId = jExec.getInt(key, 1);
                    int bId = jExec.getInt(key, 2);
                    populateCrossMatch(gId, bId, groups, branches, lim);
                }
                jExec.closeData(key);
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: Error locating branchs (ids) for groups");
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: sql = "
                                + sql);
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: err = "
                                + e.getMessage());
                return null;
            }
        }
        int len = branches.size();
        boolean checkEliminationByProvince = provinceId != -1;
        for (int i = 0; i < len; ++i)
        {
            int branchId = ((Integer) branches.elementAt(i)).intValue();
            if (branchId == -1)
            {
                continue;
            }
            if (checkEliminationByProvince)
            {
                Vector provs = getProvincesForBranch(branchId, 1, jExec);
                if (provinceInList(provs, provinceId) == false)
                {
                    continue;
                }
            }
            if (isGroups)
            {
                qualifiedGroups.add(groups.elementAt(i));
            }
            else
            {
                qualifiedBranches.add(new Integer(branchId));
            }
        }
        if (isGroups) { return qualifiedGroups; }
        // get all the groups in the qualified branches
        len = qualifiedBranches.size();
        int branchId = 0;
        for (int i = 0; i < len; ++i)
        {
            // get groups in the branch
            try
            {
                branchId = ((Integer) qualifiedBranches.elementAt(i))
                        .intValue();
                if (branchId == -1)
                {
                    continue;
                }
                sql = "Select groupProfileId from GROUPPROFILE where branchProfileId = "
                        + branchId;
                key = jExec.execute(sql);
                for (; jExec.next(key);)
                {
                    qualifiedGroups
                            .addElement(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: Error locating groups for branch Id = "
                                + branchId + "(ignoring branch)");
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: sql = "
                                + sql);
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: err = "
                                + e.getMessage());
                return null;
            }
        }
        return qualifiedGroups;
    }

    private boolean provinceInList(Vector provs, int provId)
    {
        if (provs == null) { return false; }
        int lim = provs.size();
        for (int i = 0; i < lim; ++i)
        {
            if (provId == ((Integer) provs.elementAt(i)).intValue()) { return true; }
        }
        return false;
    }

    private void populateCrossMatch(int matchId, int crossRefId, Vector main,
            Vector cross, int lim)
    {
        Integer matchVal = new Integer(matchId);
        Integer crossVal = new Integer(crossRefId);
        for (int i = 0; i < lim; ++i)
        {
            if (matchVal.equals((Integer) main.elementAt(i)))
            {
                cross.setElementAt(crossVal, i);
            }
        }
    }

    // Given a list of values return same in a comma delimited list. E.g.
    //
    //     IN == 1234 789 IN == "THIS" "THAT"
    //     OUT == "1234, 789" OUT == "\"THIS\", "\THAT\""
    //
    //     --> not that where the input is Strings each element in the
    //         result is encased in double quoted.
    //
    // Returns "-1" if list null or empty - e.g. if used in a SQL IN clause we
    // have "IN(-1)" - hence
    // guaranteed not users selected.
    private String formCommaDelimitedList(Vector values)
    {
        if (values == null || values.size() == 0) { return "-1"; }
        StringBuffer sb = new StringBuffer();
        int lim = values.size();
        for (int i = 0; i < lim; ++i)
        {
            Object val = values.elementAt(i);
            if (val instanceof String)
            {
                sb.append("\"" + (String) val + "\"");
            }
            else
            {
                sb.append("" + ((Integer) val).intValue());
            }
            if (i != lim - 1)
            {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    // New Method for Routing Table Mapping -- By BILLY 16Jan2002
    //    Details please refer to Ticket # 234
    // SEAN Routing Order Changes [May 03, 2005]: for testing I changed the
    // modifer from private to protected.
    protected Vector performTableMapping(Deal deal, SessionResourceKit srk)
    {
        
        Vector theRoutingList = null;
        // Get sorted Routing Records (Sorted by RouteOrder)
        try
        {
            Routing ro = new Routing(srk);
            theRoutingList = ro.getOrderedList();
            if (theRoutingList == null || theRoutingList.size() == 0)
            {
                logger
                        .warn("WFE::WorkflowAssistBean:performTableMapping: No Routing Entries found!!");
                return null;
            }
        }
        catch (FinderException fe)
        {
            logger
                    .warn("WFE::WorkflowAssistBean:performTableMapping: No Routing Entries found!! -- "
                            + fe.getMessage());
            return null;
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performTableMapping: get Routing List exception -- "
                            + e.getMessage());
            return null;
        }
        //Go through the list one by one and try mapping.
        int len = theRoutingList.size();
        Routing currRo = null;
        boolean matched = false;
        Vector underwriterIds = new Vector();
        for (int i = 0; i < len; ++i)
        {
            currRo = (Routing) theRoutingList.elementAt(i);
            matched = false;
            //logger.debug("BILLY ===> Current Routing Try Data: RouteType = "
            // + currRo.getRouteTypeId() +
            //        " RouteKey = " + currRo.getRouteKey() + " MatchType = " +
            // currRo.getMatchTypeId() +
            //        " MatchValue = " + currRo.getMatchValue());
            switch (currRo.getRouteTypeId())
            {
            case Mc.ROUTE_TYPE_SOURCE:
                // check if SourceOfBusinessId matched
                if (deal.getSourceOfBusinessProfileId() == getIntValue(currRo
                        .getRouteKey()))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- SOB Id Matched - "
                                    + deal.getSourceOfBusinessProfileId());
                    matched = true;
                }
                break;
            case Mc.ROUTE_TYPE_SOURCEFIRM:
                // check if SourceFirmId matched
                if (deal.getSourceFirmProfileId() == getIntValue(currRo
                        .getRouteKey()))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- SFirm Id Matched - "
                                    + deal.getSourceFirmProfileId());
                    matched = true;
                }
                break;
            case Mc.ROUTE_TYPE_POSTALCODE:
            {
                // check if PostalCode matched
                String tmpPostalCode = trimAllSpaces(
                        getRoutePostalCode(deal, srk)).toUpperCase();
                String theRouteKey = trimAllSpaces(currRo.getRouteKey())
                        .toUpperCase();
                if (tmpPostalCode.startsWith(theRouteKey))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- PostalCode Matched - "
                                    + tmpPostalCode);
                    matched = true;
                }
                break;
            }
            case Mc.ROUTE_TYPE_AREACODE:
            {
                // check if AresCode matched
                String tmpAreaCode = trimAllSpaces(getRouteAreaCode(deal, srk));
                String theRouteKey = trimAllSpaces(currRo.getRouteKey())
                        .substring(0, 3);
                if (tmpAreaCode.equals(theRouteKey))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- AreaCode Matched - "
                                    + tmpAreaCode);
                    matched = true;
                }
                break;
            }
            case Mc.ROUTE_TYPE_BORROWERLASTNAME:
            {
                // check if BorrowerLastName matched
                String tmpLastName = getRouteBLastName(deal, srk).toUpperCase();
                String theRouteKey = currRo.getRouteKey().toUpperCase();
                if (tmpLastName.startsWith(theRouteKey))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- Borrower's LastName Matched - "
                                    + tmpLastName);
                    matched = true;
                }
                break;
            }
            case Mc.ROUTE_TYPE_WORKFLOW:
            {
                // perform Workflow matching
                // NOT SUPPORTED YET !!
                break;
            }
            case Mc.ROUTE_TYPE_PROVINCE:
            {
                // check if Province matched
                int theProvince = getRouteProvince(deal, srk);
                if (theProvince != -1
                        && theProvince == getIntValue(currRo.getRouteKey()))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- Province Matched - "
                                    + theProvince);
                    matched = true;
                }
                break;
            }
            // Added new RouteType (SOBRegion) -- By BILLY 17May2002
            case Mc.ROUTE_TYPE_SOBREGION:
            {
                try
                {
                    // check if SourceOfBusiness.SOBRegionId matched
                    int theSOBRegionId = deal.getSourceOfBusinessProfile()
                            .getSOBRegionId();
                    //--Merge--//
                    //Modified to route even if SOBRegionId = 0
                    //  - By Billy 25June2002
                    if (theSOBRegionId >= 0
                            && theSOBRegionId == getIntValue(currRo
                                    .getRouteKey()))
                    {
                        //Matched
                        logger
                                .debug("WFE::WorkflowAssistBean:performTableMapping -- SOBRegion Matched - "
                                        + theSOBRegionId);
                        matched = true;
                    }
                }
                catch (Exception e)
                {
                    break;
                }
                break;
            }
            // Added new RouteType (SpecialFeature) -- By BILLY 17May2002
            case Mc.ROUTE_TYPE_SPECIALFEATURE:
            {
                // check if SPECIALFEATURE matched
                if (deal.getSpecialFeatureId() == getIntValue(currRo
                        .getRouteKey()))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- SpecialFeature Id Matched - "
                                    + deal.getSpecialFeatureId());
                    matched = true;
                }
                break;
            }
            // Added new RouteType (SourceFirm + Province) -- By BILLY
            // 03July2003
            case Mc.ROUTE_TYPE_SOURCEFIRM_PROVINCE:
            {
                // check if SourceFirmId and Province code matched
                // RouteKey format == SFId, ProvId
                Vector theRouteKeys = currRo.getRouteKeys();
                //logger.debug("SourceFirmId from RouteKeys = " +
                // theRouteKeys.get(0) + " :: ProvinceId = " +
                // theRouteKeys.get(1));
                int theProvince = getRouteProvince(deal, srk);
                if ((deal.getSourceFirmProfileId() == getIntValue(theRouteKeys
                        .get(0).toString().trim()))
                        && (theProvince != -1)
                        && (theProvince == getIntValue(theRouteKeys.get(1)
                                .toString().trim())))
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- SFirm Id - "
                                    + deal.getSourceFirmProfileId()
                                    + " And Province Matched - " + theProvince);
                    matched = true;
                }
                break;
            }
            //--> Added new RouteType (Self-Employed) :: Ticket# 328
            //--> By BILLY 21April2004
            case Mc.ROUTE_TYPE_SELFEMPLOYED:
            {
                // check if any borrower's current Employment is Self-Empolyed
                // RouteKey format == SFId, ProvId
                boolean theRouteKey = (currRo.getRouteKey().equals("N") ? false
                        : true);
                logger.debug("Self-employed from RouteKey = " + theRouteKey);
                boolean isSelfEmployed = getIsSelfEmployed(deal, srk);
                if (isSelfEmployed == theRouteKey)
                {
                    //Matched
                    logger
                            .debug("WFE::WorkflowAssistBean:performTableMapping -- SelfEmployed - Matched - "
                                    + isSelfEmployed);
                    matched = true;
                }
                break;
            }
            // SEAN Routing Order Changes (May 05, 2005): handle the two new
            // routing types.
            case Mc.ROUTE_TYPE_OCCUPANCYTYPE:
            {
                try {
                    Collection props = (new Property(srk, null)).
                        findByDealIdAndPrimaryFlag(deal.getDealId(), 'Y');
                    if (props.size() > 0) {
                        // get the first the one.
						Iterator p = props.iterator();
                        Property primary = (Property) p.next();
                        if (primary.getOccupancyTypeId() ==
                            getIntValue(currRo.getRouteKey())) {

                            matched = true;
                        }
                    }
                } catch (Exception e) {
                    // error ...
                }
                break;
            }
            case Mc.ROUTE_TYPE_SOBSTATUS:
            {
				try {
					int statusId =
						deal.getSourceOfBusinessProfile().getProfileStatusId();
					if (statusId == getIntValue(currRo.getRouteKey())) {

						matched = true;
					}
				} catch (FinderException fe) {
					// error...
				} catch (RemoteException re) {
					// error ...
				}
                break;
            }
            case Mc.ROUTE_TYPE_NEWCONSTRUCTIONID:
            {
            	//findByPrimaryProperty
            	try {
            		Property property = new Property(srk, null)
                      .findByPrimaryProperty(deal.getDealId(), deal
                          .getCopyId(), deal.getInstitutionProfileId());
            		if(property != null) {
            			if(property.getNewConstructionId() == 
            						getIntValue(currRo.getRouteKey()))
            			    matched = true;
            		}
            		
            	}
            	catch (FinderException fe) {
            		// error ...
            	}
            	catch(RemoteException re) {
            		// error ...
            	}
            	break;
            }
            // SEAN Routing Order Changes END.
            default:
                // problem !!
                break;
            }
            if (matched == true)
            {
                //Perform UW hunting based on Matched Routing Record
                underwriterIds = routingHuntForUsers(currRo, deal, srk);
            }
            if (underwriterIds != null && underwriterIds.size() > 0)
            {
                break; // we found it !!
            }
        } // For
        return underwriterIds;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Given a deal entity determine origination PostalCode) as:
     *            
     *               �  The PostalCode of the primary property in the Deal, if one, or
     *               �  The PostalCode of the primary borrower in the Deal
     *            
     *               @return
     *               PostalCode or &quot;&quot; if could not be determined (in latter case error messages are delivered
     *               to the system log file).
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private String getRoutePostalCode(Deal deal, SessionResourceKit srk)
    {
        
        String thePostalCode = "";
        // first by primary property
        try
        {
            Property prop = new Property(srk, null);
            prop.setSilentMode(true);
            prop = prop.findByPrimaryProperty(deal.getDealId(), deal
                    .getCopyId(), deal.getInstitutionProfileId());
            thePostalCode = prop.getPropertyPostalFSA()
                    + prop.getPropertyPostalLDU();
        }
        catch (Exception e)
        {
            thePostalCode = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRoutePostalCode: Could not locate primary property on deal (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRoutePostalCode: err = "
                    + e.getMessage());
        }
        if (!thePostalCode.trim().equals("")) { return thePostalCode; }
        // Secondly by primary Borrower
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            BorrowerAddress bAddr = new BorrowerAddress(srk);
            bAddr = bAddr.findByCurrentAddress(borrower.getBorrowerId(), deal
                    .getCopyId());
            thePostalCode = bAddr.getAddr().getPostalFSA()
                    + bAddr.getAddr().getPostalLDU();
        }
        catch (Exception e)
        {
            thePostalCode = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRoutePostalCode: Could not locate address of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRoutePostalCode: err = "
                    + e.getMessage());
        }
        return thePostalCode;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Given a deal entity determine origination AreaCode as:
     *            
     *               �  The AreaCode of the primary borrower in the Deal
     *            
     *               @return
     *               AreaCode or &quot;&quot; if could not be determined (in latter case error messages are delivered
     *               to the system log file).
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private String getRouteAreaCode(Deal deal, SessionResourceKit srk)
    {
        
        String theAreaCode = "";
        // By primary Borrower only for the time being
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            theAreaCode = borrower.getBorrowerHomePhoneNumber().substring(0, 3);
        }
        catch (Exception e)
        {
            theAreaCode = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteAreaCode: Could not locate areacode of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteAreaCode: err = "
                    + e.getMessage());
        }
        return theAreaCode;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Given a deal entity determine origination BorrowerLastName as:
     *            
     *               �  The BorrowerLastName of the primary borrower in the Deal
     *            
     *               @return
     *               BorrowerLastName or &quot;&quot; if could not be determined (in latter case error messages are delivered
     *               to the system log file).
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private String getRouteBLastName(Deal deal, SessionResourceKit srk)
    {
        
        String theBLastName = "";
        // By primary Borrower only for the time being
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            theBLastName = borrower.getBorrowerLastName();
        }
        catch (Exception e)
        {
            theBLastName = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteBLastName: Could not locate LastName of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteBLastName: err = "
                    + e.getMessage());
        }
        return theBLastName;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Given a deal entity determine origination province (id) as:
     *            
     *               �  The province of the principal property in the Deal, if one, or
     *               �  The province of the current address of the principal borrower
     *            
     *               @return
     *               Province Id or -1 if could not be determined (in latter case error messages are delivered
     *               to the system log file).
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private int getRouteProvince(Deal deal, SessionResourceKit srk)
    {
        
        //JdbcExecutor jExec = srk.getJdbcExecutor();
        int theProvince = -1;
        // first by principal property
        try
        {
            Property prop = new Property(srk, null);
            prop.setSilentMode(true);
            prop = prop.findByPrimaryProperty(deal.getDealId(), deal
                    .getCopyId(), deal.getInstitutionProfileId());
            theProvince = prop.getProvinceId();
        }
        catch (Exception e)
        {
            theProvince = -1;
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteProvince: Could not locate primary property on deal (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteProvince: err = "
                    + e.getMessage());
        }
        if (theProvince != -1) { return theProvince; }
        // Then try get it from Primary Borrower
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            BorrowerAddress bAddr = new BorrowerAddress(srk);
            bAddr = bAddr.findByCurrentAddress(borrower.getBorrowerId(), deal
                    .getCopyId());
            theProvince = bAddr.getAddr().getProvinceId();
        }
        catch (Exception e)
        {
            theProvince = -1;
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteProvince: Could not locate province of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteProvince: err = "
                    + e.getMessage());
        }
        return theProvince;
    }

    //--> Added new RouteType (Self-Employed) :: Ticket# 328
    //--> By BILLY 21April2004
    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Given a deal entity determine if any borrower's current Employment is Self-Empolyed as:
     *            
     *               �  IncomeType = 6 (Self-Empolyed) or
     *               �  OccupationID = 6 (Self-Empolyed)
     *            
     *               @return
     *               True : Self-Employed
     *               False : not Self-Employed
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    private boolean getIsSelfEmployed(Deal deal, SessionResourceKit srk)
    {
        
        //JdbcExecutor jExec = srk.getJdbcExecutor();
        boolean isSelfEmployed = false;
        // first by principal property
        try
        {
            Collection eHistories = new EmploymentHistory(srk, null)
                    .findAllCurrent(new DealPK(deal.getDealId(), deal.getCopyId()));
            for (Iterator iEH = eHistories.iterator(); iEH.hasNext();)
            {
                EmploymentHistory oneEHistory = (EmploymentHistory) iEH.next();
                //logger.debug("Billy ===> oneEHistory.getOccupationId() = " +
                // oneEHistory.getOccupationId());
                //logger.debug("Billy ===>
                // oneEHistory.getIncome().getIncomeTypeId() = " +
                // oneEHistory.getIncome().getIncomeTypeId());
                if (oneEHistory != null
                        && (oneEHistory.getOccupationId() == Sc.OCCUPATION_SELFEMPLOYED || (oneEHistory
                                .getIncome() != null && oneEHistory.getIncome()
                                .getIncomeTypeId() == Sc.INCOME_SELF_EMP)))
                {
                    //logger.debug("Billy ===> getIsSelfEmployed = true");
                    isSelfEmployed = true;
                    break;
                }
            }//for
        }
        catch (Exception e)
        {
            isSelfEmployed = false;
            logger.warn("WFE::WorkflowAssistBean:getIsSelfEmployed: err = "
                    + e.getMessage());
        }
        //logger.debug("Billy ===> getIsSelfEmployed return = " +
        // isSelfEmployed);
        return isSelfEmployed;
    }

    protected int getIntValue(String sNum)
    {
        if (sNum == null) { return 0; }
        sNum = sNum.trim();
        if (sNum.length() == 0) { return 0; }
        try
        {
            // handle decimal (e.g. float type) representation
            int ndx = sNum.indexOf(".");
            if (ndx != -1)
            {
                sNum = sNum.substring(0, ndx);
            }
            return Integer.parseInt(sNum.trim());
        }
        catch (Exception e)
        {
            ;
        }
        return 0;
    }

    protected String trimAllSpaces(String input)
    {
        String tmp = "";
        int x = 0;
        for (x = 0; x < input.length(); x++)
        {
            char c = input.charAt(x);
            if ((c != ' '))
            {
                tmp += c;
            }
        }
        return tmp;
    }

    private Vector routingHuntForUsers(Routing ro, Deal deal,
            SessionResourceKit srk)
    {
        Vector underwriterIds = new Vector();
        
        switch (ro.getMatchTypeId())
        {
        case Mc.ROUTE_MATCH_TYPE_USER:
        {
            UserProfile theUser = uq.getUserProfile(ro.getMatchValue(), deal.getInstitutionProfileId(), true,
                    true, UserProfileQuery.UNDERWRITER_USER_TYPE, srk);
            if (theUser != null && theUser.getUserProfileId() > 0)
            {
                underwriterIds.addElement(new Integer(theUser
                        .getUserProfileId()));
                logger
                        .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform User matching : MatchValue = "
                                + ro.getMatchValue()
                                + " Map to user = "
                                + theUser.getUserProfileId());
            }
            else
            {
                logger
                        .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform User matching : MatchValue = "
                                + ro.getMatchValue()
                                + " No Active User found !!");
            }
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_GROUP:
        {
            Vector allUserProfilesInGroup = uq.getGroupUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup,
                                                        srk, true, deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Group matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Group " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_BRANCH:
        {
            Vector allUserProfilesInGroup = uq.getBranchUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup, 
                                                        srk, true, deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Branch matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Branch " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_REGION:
        {
            Vector allUserProfilesInGroup = uq.getRegionUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup,
                                                        srk, true,
                                                        deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Region matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Region " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_INSTITUTION:
        {
            Vector allUserProfilesInGroup = uq.getInstitutionUnderwriters(ro
                    .getMatchValue(), -1, false, srk);
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup, 
                                                        srk, true,
                                                        deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Institution matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Institution " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_LENDER:
        {
            Vector allUserProfilesInGroup = uq.getLenderUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup, 
                                                        srk, true,
                                                        deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Lender matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Lender " + ro.getMatchValue());
            break;
        }
        default:
            // Problem
            break;
        }
        return underwriterIds;
    }

    //==========================================================================
    // Method to hunt for a Active user for task
    //  - Only perform hunting if User status = "On Leave"
    //  - if selected user id in-active then hunt by the Stand-in
    //  - if all stand-in in-active then hunt by group and branch
    //  - return -1 if No user found
    //==========================================================================
    private int findActiveUserForTask(int selectedUserId, int institutionId, SessionResourceKit srk)
    {
        
        UserProfile selectedUser = null;
        try
        {
            selectedUser = new UserProfile(srk)
                    .findByPrimaryKey(new UserProfileBeanPK(selectedUserId, institutionId));
        }
        catch (Exception e)
        {
            logger
                    .error("UserProfile Not found @findActiveUser -- Return null !!");
            return -1;
        }
        // Return the current UserID if User Status <> OnLeave
        if (selectedUser.getProfileStatusId() != Sc.PROFILE_STATUS_ONLEAVE) { return selectedUserId; }
        int iUserType = uq.getInternalUserTypeId(selectedUser.getUserTypeId());
        UserProfile userToAssign = uq.getUserProfile(selectedUser
                .getUserProfileId(), institutionId, true, true, iUserType, srk);
        if (userToAssign == null)
        {
            //No active stand-in user found.
            // Then try get user from Group or Branch
            Vector matchedUsers = null;
            if (iUserType == UserProfileQuery.ADMIN_USER_TYPE)
            {
                // perform admin assignment - load balanced group (branch if
                // necessary)
                matchedUsers = uq.getAdministrators(selectedUser
                        .getGroupProfileId(), -1, true, srk, institutionId);
                userToAssign = getLeastLoadedProfile(matchedUsers, srk, institutionId);
            }
            else if (iUserType == UserProfileQuery.FUNDER_USER_TYPE)
            {
                // perform funder assignment - load balanced group (branch if
                // necessary)
                matchedUsers = uq.getFunders(selectedUser.getGroupProfileId(),
                        true, srk, institutionId);
                userToAssign = getLeastLoadedProfile(matchedUsers, srk, institutionId );
            }
            else
            {
                // perform underwriter assignment - load balanced group (branch
                // if necessary) underwriters
                matchedUsers = uq.getUnderwriters(selectedUser
                        .getGroupProfileId(), -1, true, srk, institutionId);
                userToAssign = getLeastLoadedProfile(matchedUsers, srk, institutionId);
            }
        }
        if (userToAssign != null)
        {
            return userToAssign.getUserProfileId();
        }
        else
        {
            return -1;
        }
    }

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> New workflow routing -- By Billy 07Aug2003
    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Called during underwriter assignment to a deal. This method operates on the following
     *               associative relationships in the MOS schema:
     *            
     *                . Source of Business (broker) to Group(s) Association
     *            
     *               @return
     *               List of underwriters located via the associations discussed above. null is returned if no
     *               undedrwriter is located.
     *            
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    //--> Ticket#135 : Ensure to hunt for Admin and Funder with the SOB/Firm
    // group
    //--> The assigned GroupId from SOB to Group relationship will be stored in
    // the dealUserProfiles
    //--> By Billy 27Nov2003
    // SEAN Routing Order Changes [May 03, 2005]: For Unit test, I changed the
    // modified from private to protected.
    protected Vector performSOBGroupMapping(int sobId, int institutionid, SessionResourceKit srk,
            DealUserProfiles dup)
    {
        Vector underwriters = new Vector();
        String sql = "Select groupProfileId from SourceToGroupAssoc where sourceOfBusinessProfileId = "
                + sobId;
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        int groupProfileId = -1;
        try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                groupProfileId = jExec.getInt(key, 1);
				// SEAN Ticket #1709 July 05, 2005: added condition statement to
				// avoid NullPointerException.
				Vector activeUWs =
					ensureActiveProfiles2(uq.getGroupUnderwriters(groupProfileId,
																  -1, false, srk, institutionid),
										  srk, true, institutionid);
				if (activeUWs != null) {
					underwriters.addAll(activeUWs);
				}
				// SEAN Ticket #1709 END
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performSOBGroupMapping: Error on query Source Of Business to Group association");
            logger
                    .error("WFE::WorkflowAssistBean:performSOBGroupMapping: sql = "
                            + sql);
            logger
                    .error("WFE::WorkflowAssistBean:performSOBGroupMapping: err = "
                            + e);
        }
        //--> Ticket#135 : Ensure to hunt for Admin and Funder with the
        // SOB/Firm group
        //--> Check to store Group id if target underwriters found.
        //--> By Billy 27Nov2003
        if (underwriters != null && underwriters.size() > 0)
        {
            dup.setSourceGroupId(groupProfileId);
        }
        else
        {
            // Just in case
            dup.setSourceGroupId(-1);
        }
        //============================================================================
        return underwriters;
    }

    /**
     * <pre>
     * 
     *  
     *   
     *    
     *     
     *      
     *       
     *        
     *         
     *          
     *           
     *            
     *               Called during underwriter assignment to a deal. This method operates on the following
     *               associative relationships in the MOS schema:
     *            
     *                . Source Firm (broker) to Group(s) Association
     *            
     *               @return
     *               List of underwriters located via the associations discussed above. null is returned if no
     *               undedrwriter is located.
     *            
     *               
     *            
     *           
     *          
     *         
     *        
     *       
     *      
     *     
     *    
     *   
     *  
     * </pre>
     */
    //--> Ticket#135 : Ensure to hunt for Admin and Funder with the SOB/Firm
    // group
    //--> The assigned GroupId from Frim to Group relationship will be stored
    // in the dealUserProfiles
    //--> By Billy 27Nov2003
    // SEAN Routing Order Changes [May 03, 2005]: for testing I changed the
    // modifer from private to protected.
    protected Vector performFirmGroupMapping(int sfId, int institutionId, SessionResourceKit srk,
            DealUserProfiles dup)
    {
        Vector underwriters = new Vector();
        String sql = "Select groupProfileId from SourceFirmToGroupAssoc where sourceFirmProfileId = "
                + sfId;
        
        JdbcExecutor jExec = srk.getJdbcExecutor();
        int groupProfileId = -1;
        try
        {
            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                groupProfileId = jExec.getInt(key, 1);
                Vector allUserProfilesInGroup = uq.getGroupUnderwriters(
                        groupProfileId, -1, false, srk, institutionId);
                underwriters.addAll(this.ensureActiveProfiles2(
                        allUserProfilesInGroup, srk, true, institutionId));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:performFirmGroupMapping: Error on query Source Firm to Group association");
            logger
                    .error("WFE::WorkflowAssistBean:performFirmGroupMapping: sql = "
                            + sql);
            logger
                    .error("WFE::WorkflowAssistBean:performFirmGroupMapping: err = "
                            + e);
        }
        //--> Ticket#135 : Ensure to hunt for Admin and Funder with the
        // SOB/Firm group
        //--> Check to store Group id if target underwriters found.
        //--> By Billy 27Nov2003
        if (underwriters != null && underwriters.size() > 0)
        {
            dup.setSourceGroupId(groupProfileId);
        }
        else
        {
            // Just in case
            dup.setSourceGroupId(-1);
        }
        //============================================================================
        return underwriters;
    }
    
    // since 4.2 DSU STARTS
    public void dealStatusUpdateRequest(Deal deal, int userId) throws Exception{
        
    	// For Manual Deal originated from Deal Entry screen will have channel Media as 'M' 
    	// Block updating the Deal event status and Condition update status for deals created for express 
    	// FXP32829 - Dec 06 2011
    	if(deal.getChannelMedia() != null && deal.getChannelMedia().trim().equalsIgnoreCase(Xc.CHANNEL_MEDIA_M))
    		return;
    	
        // check the systemTypeId for the deal is supported DealEventStatus update
        //FXP25721 - July 22
        try {
            DealEventStatusUpdateAssoc desua = new DealEventStatusUpdateAssoc(deal.getSessionResourceKit());
            desua.setSilentMode(true);
            Collection<DealEventStatusUpdateAssoc> c = desua.findBySystemType(deal.getSystemTypeId());
            if (c==null || c.size()==0) {
                return;
            }
        } catch (Exception rd) {
            logger.info("Couldn't get DealEventStatusUpdateAssoc for systemtypeid=" + deal.getSystemTypeId());
            return;
        }

        ESBOutboundQueue queue = new ESBOutboundQueue(deal.getSessionResourceKit());
        ESBOutboundQueuePK pk = queue.createPrimaryKey();
        queue.create(pk, ServiceConst.SERVICE_TYPE_DEALEVENT_STATUS_UPDATE, deal.getDealId(), 
                  deal.getSystemTypeId(), userId);
    }
    // since 4.2 DSU ENDS

}
