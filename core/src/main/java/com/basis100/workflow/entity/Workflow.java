package com.basis100.workflow.entity;


import java.util.Vector;

import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.TaskBeanPK;
import com.basis100.workflow.pk.WorkflowBeanPK;

/*
 * Workflow:
 *
 * An instance of WorkflowBean is used to represent a workflow in the MOS system. A work flow
 * is identified by a unique name and consists of multiple stages. Each stage is a logical
 * container for one or more task defintions - see MOS - Page/Workflow Design Specifications
 * for a more complete definition of a workflow.
 *
 * Notes:
 *
 * On ejbCreate a workflow record with zero stages is created (table workflow). The workflow
 * Id generated for this record is obtained as the next available value of sequence
 * WORKFLOWSEQ. This sequence must exist in the database, e.g.:
 *
 *                  create sequence WORKFLOWSEQ;
 *
 * Workflow stages are populated with tasks (i.e. task definitions that correspond to records
 * in table task - TaskBean entities).
 *
 *
 * Methods:
 *
 * There are methods available during the creation and maintenance of a workflow definition.
 * For example, there are methods to add/remove a stage from the workflow defineition, and to
 * add/remove a task to/from a stage.
 *
 * Another set of methods are provided to query the workflow definition. For example there is a
 * method to return an array of tasks defined for a particular stage in the workflow definition.
 *
 * --> Workflow create/maintenance methods: <--
 *
 * . addStage()
 *
 *   Add a new stage to the workflow definition. The new stage becomes the last stage in the
 *   workflow defininition.
 *
 * . addStageTask(int stageNum, int taskID)
 *   addStageTask(int stageNum, Task dTask)
 *
 *   Add a task to a specified stage in the workflow definition. The task may be identified by
 *   its unique task ID (corresponding to a row in the task table), or may be represented by
 *   a Task object (an entity corresponding to a row in the task table).
 *
 * . removeStage(int stageNum)
 *
 *   Remove a stage from the workflow definition.
 *
 * . removeStageTask(int stageNum, int taskID)
 * . removeStageTask(int stageNum, Task dTask)
 *
 *   Remove a task from a specified stage in the workflow definition. The task may be
 *   identified by its unique task ID (corresponding to a row in the task table), or may be
 *   represented by a Task object (an entity corresponding to a row in the task table).
 *
 *
 * --> Workflow query methods: <--
 *
 * . WorkflowTask [] getStageTasks(int stageNum)
 *
 *   Return an array of WorkflowTask entities that correspond to the tasks defined in the
 *   specified stage of the workflow.
 *
 * . WorkflowTask getFollowUpTask(AssignedTask aTask)
 *
 *   Locate and return the follow-up task for the specific assigned task. The follow-up task
 *   must exist in the immediately following stage in the workflow. Return null if no
 *   qualifying follow-up task.
 *
 * . WorkflowTask getLoopBackTask(AssignedTask aTask, boolean toOrigin)
 *
 *   Find the task definition that has the specified task (as defined in the specified
 *   assigned task) as its follow-up task. The task must be defined in the stage immediately
 *   before the stage of the assigned task.
 *
 *   If <toOrigin> is true a check is made to see if the located task is itself the follow-up
 *   for task in the a immediately previous stage return the previous task definition, and
 *   so on (back to the follow-up origin as it were).
 *
 **/


public class Workflow extends EntityBase
{
    
	EntityContext ctx;

	private int    workflowId;
	private String wfName;
	private int    numberOfStages = 0;
	private int    institutionProfileId;
	
	// stageTasks:
	//
	// Internal table for holding workflow stage information - i.e. the workflow tasks
	// defined for each stage in the workflow. This is vector where the first element holds
	// stage zero (the async. stage) information, the second element holds stage one information,
  // and so on.
	//
	// The value for each element is also a vector that holds the tasks (WorkflowTask entities)
	// included in the workflow for the stage in question.

	private Vector stageTasks = new Vector();

	//
	// Constructor: Accepts session resources object (access to system logger and Jdbc
	//              connection resources.
	//

	public Workflow(SessionResourceKit srk)
	{
		super(srk);
	}
	//
	// Other Methods:
	//

	// Workflow create/maintenance methods:

	public void addStage()
	{
		stageTasks.addElement(new Vector());
		numberOfStages++;
	}
	public void addStageTask(int stageNum, int taskId) throws RemoteException, IllegalArgumentException
	{
		// get the task definition
		Task dTask = null;
		try
		{
			dTask = new Task(srk);

			dTask.findByPrimaryKey(new TaskBeanPK(taskId));
		}
		catch (FinderException e)
		{
			throw new IllegalArgumentException("Workflow::addStageTask() - Invalid task Id - Could not find task definition, task Id = " + taskId);
		}

		addStageTask(stageNum, dTask);
	}
	public void addStageTask(int stageNum, Task dTask) throws IllegalArgumentException, RemoteException
	{
		if (stageNum < 0 || stageNum > numberOfStages)
			throw new IllegalArgumentException("Workflow::addStageTask() - Invalid stage number (" + stageNum + "), number of stages = " + numberOfStages);

		// check to ensure task not already defined in workflow
		int newTaskId = dTask.getTaskId();

		for (int i = 0; i < numberOfStages; ++i)
		{
			Vector tasks = (Vector)stageTasks.elementAt(i);

			int numTasks = tasks.size();

			for (int j = 0; j < numTasks; ++j)
			{
				if (((WorkflowTask)tasks.elementAt(j)).getTaskId() == newTaskId)
				{
					throw new IllegalArgumentException("Workflow::addStageTask() - TaskId " +
					newTaskId + " (" + dTask.getTaskName() + ") already defined in " +
					"workflow at stage = " + (i));
				}
			}
		}

		// create the new workflow task
		WorkflowTask wt = new WorkflowTask(srk);

		try
		{
			wt.create(workflowId, stageNum, dTask);
		}
		catch (CreateException e)
		{
			throw new IllegalArgumentException("Workflow::addStageTask() - TaskId=" +
			newTaskId + ", stage=" + (stageNum) + ": unexpected problem creating workflow task");
		}

		// add new task
		((Vector)stageTasks.elementAt(stageNum)).addElement(wt);
	}
	private Workflow commonFind(String sql, WorkflowBeanPK pk) throws RemoteException, FinderException
	{
		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;

				workflowId     = jExec.getInt(key, "WORKFLOWID");
				wfName         = jExec.getString(key, "WFNAME");
				numberOfStages = jExec.getInt(key, "NUMBEROFSTAGES");
				institutionProfileId = jExec.getInt(key, "INSTITUTIONPROFILEID");
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = null;
				if (pk != null)
					msg =  "Workflow Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
				else
					msg =  "Workflow Entity: @findById(), Id=" + workflowId + ", entity not found";

				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			String msg = null;
			if (pk != null)
				msg =  "Workflow Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
			else
				msg =  "Workflow Entity: @findById(), Id=" + workflowId + ", entity not found";

			FinderException fe = new FinderException(msg);

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		// get the work flow tasks for all stages
		for (int i = 0; i <= numberOfStages; ++i)
		{
			try
			{
				WorkflowTask wt = new WorkflowTask(srk);
				stageTasks.addElement(wt.findByWorkflowStage(workflowId, i));
			}
			catch (FinderException e)
			{
        // stage 0 can be empty (e.g. no async. tasks)
        if (i == 0)
        {
            stageTasks.addElement(new Vector());
            continue;
        }

				String msg = null;
				if (pk != null)
					msg =  "Workflow Entity: @findByPrimaryKey(), key=" + pk + ", task entitities (workflowId=" + workflowId + ", stage=" + (i+1) + ") not found";
				else
					msg =  "Workflow Entity: @findById(), Id=" + workflowId + ", task entitities (workflowId=" + workflowId + ", stage=" + (i+1) + ") not found";

				FinderException fe = new FinderException(msg);
				logger.error(fe.getMessage());
				throw fe;
			}
		}

		return this;
	}

/*
 * ******** WARNING *************
 * This method is never called since WrokFlow table is static
 * if it needs to be called, InstitiutionProfileId must be added somehow
 * Sep 25, 2007 - Midori
 * 
 * Remote-Home inerface methods:
 *
 * The following methods have counterparts in Enterprise Bean classes - in an
 * Enterprise Entity Bean each methods name would be prefixed with "ejb"
 *
 **/


	public Workflow create(String wfName) throws RemoteException, CreateException
	{
		boolean gotRecord = false;

		this.wfName         = wfName;
		this.numberOfStages = 0;

    // there is always at least one stage (stage = 0 is the asynchronous stage which may be empty)

		//String sql = "insert into workflow set workflowId = WORKFLOWSEQ.NEXTVAL, " +
		//			 "wfName = '" + wfName + "', numberOfStages = 1, institutionProfileId = " 
        //             + srk.getExpressState().getDealInstitutionId();

		String wfIdSql = "select workflowId from workflow where wfName = '" + wfName + "'";

		try
		{
			int key = jExec.execute(wfIdSql);

			for (; jExec.next(key); )
			{
				gotRecord = true;

				workflowId = jExec.getInt(key, 1);
				break; // can only be one record
			}
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException("Workflow Entity :: create() exception");

			logger.error(ce.getMessage() + " :: exception follows");
			logger.error(e.getMessage());
			logger.error(e);
			throw ce;
		}

		if (gotRecord == false)
		{
			CreateException ce = new CreateException("Workflow Entity - create() exception:: did not obtain workflow ID");

			logger.error(ce.getMessage());
			throw ce;
		}

		return this;
	}
	public void ebjLoad() throws RemoteException
	{
		// implementation not required for now!
	}
	public void ebjPassivate () throws RemoteException {;}
	public void ejbActivate() throws RemoteException{;}

/*
 * ejbStore:
 *
 * Note: Addition/remove of stage and/or stage tasks causes creation/removal of
 *       subordinate workflow task entities - hence, store needs only concern itself
 *       with the main (workflow table) record.
 *
 **/

	public void ejbStore() throws RemoteException
	{
		// technique:: remove existing entity and save entirely new representation (for now!)

		String sql = null;
		try
		{
			// (re) insert the workflow record

			sql = "update workflow set " +
				   "wfName = '" + wfName + "', numberOfStages = " + numberOfStages +
				   " where workflowId = " + workflowId;

			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoteException re = new RemoteException("Workflow Entity - ejbStore() exception");

			logger.error(re.getMessage());
			logger.error("sql: " + sql);
			logger.error(e);

			throw re;
		}

	}
	public Workflow findById(int workflowId) throws RemoteException, FinderException
	{
		String sql = "Select * from workflow where workflowId = " + workflowId;

		return commonFind(sql, null);
	}

/*
 * finders:
 *
 **/

	public Workflow findByPrimaryKey(WorkflowBeanPK pk) throws RemoteException, FinderException
	{
		String sql = "Select * from workflow where wfName = '" + pk.getWfName() + "'";

		return commonFind(sql, pk);
	}

/*
 * getters - setters
 *
 */

  public int getWorkflowId()
  {
	  return  workflowId;
  }

  public String getWfName()
  {
    return wfName;
  }

  public int getNumberOfStages()
  {
  return numberOfStages;
  }

  public int getInstitutionProfileId() {
      return institutionProfileId;
  }
  
/*
 * Public methods
 *
 */

	public WorkflowTask getFollowUpTask(AssignedTask aTask)
	{
    // by definition async. task does not have a followup
    if (aTask.getWorkflowStage() == 0)
      return null;

		int followUpTaskId = aTask.getFollowUpTaskId();

		if (followUpTaskId == -1)
			return null;

		// check for follow up defined in next stage of the workflow
    //
    //  note: in following we have stageNum <= 0 not stageNum < 0 because by definition
    //        tasks in the async. stage (stage 0) do not feature follow-ups

		int stageNum = aTask.getWorkflowStage();
		if (stageNum <= 0 || stageNum > numberOfStages)
			return null;

		Vector v = (Vector)stageTasks.elementAt(stageNum);  // stages stored 0 origin == next stage
		int len = v.size();

		for (int i = 0; i < len; ++i)
		{
			if (((WorkflowTask)v.elementAt(i)).getTaskId() == followUpTaskId)
				return (WorkflowTask)v.elementAt(i);
		}

		return null;
	}

	public WorkflowTask getFollowUpTask(WorkflowTask wTask)
	{
    // by definition async. task does not have a followup
    if (wTask.getWorkflowStage() == 0)
      return null;

		int followUpTaskId = wTask.getFollowUpTaskId();

		if (followUpTaskId == -1)
			return null;

		// check for follow up defined in next stage of the workflow
    //
    //  note: in following we have stageNum <= 0 not stageNum < 0 because by definition
    //        tasks in the async. stage (stage 0) do not feature follow-ups


		int stageNum = wTask.getWorkflowStage();
		if (stageNum <= 0 || stageNum > numberOfStages)
			return null;

		Vector v = (Vector)stageTasks.elementAt(stageNum);  // stages stored 0 origin == next stage
		int len = v.size();

		for (int i = 0; i < len; ++i)
		{
			if (((WorkflowTask)v.elementAt(i)).getTaskId() == followUpTaskId)
				return (WorkflowTask)v.elementAt(i);
		}

		return null;
	}

	public WorkflowTask getLinkTask(AssignedTask aTask)
	{
		int linkTaskId   = aTask.getLinkTaskId();

		if (linkTaskId == -1)
		  return null;

		int stageNum = aTask.getWorkflowStage();

    //
    //  note: in following we have stageNum <= 0 not stageNum < 0 because by definition
    //        tasks in the async. stage (stage 0) do not feature link tasks


		if (stageNum <= 0 || stageNum > numberOfStages)
			return null;

		// check for link task in the next stage

		Vector v = (Vector)stageTasks.elementAt(stageNum);  // stages stored 0 origin == next stage
		int len = v.size();

		for (int i = 0; i < len; ++i)
		{
			if (((WorkflowTask)v.elementAt(i)).getTaskId() == linkTaskId)
				return (WorkflowTask)v.elementAt(i);
		}

		return null;
	}

	public WorkflowTask getLoopBackTask(AssignedTask aTask, boolean toOrigin)
	{
		WorkflowTask loopBackTask = null;
		int taskId                = aTask.getTaskId();

		boolean originalTaskLocated = false;

    // loop over stages going from last to first (i.e. reverse order) to:
    //   - Part I, locate original task passed in, then,
    //   - Part II, locate the loopback task (possibly going back to origin of follow-up chain)

		for (int i = numberOfStages; i >= 0; i--)
		{
			Vector v = (Vector)stageTasks.elementAt(i);
			int size = v.size();

			boolean found = false;

      // loop over tasks in stage
			for (int j = 0; j < size; ++j)
			{
				WorkflowTask dTask = (WorkflowTask)v.elementAt(j);

				if (originalTaskLocated == false)
				{
					// Part I - locate the original task ...
					if (dTask.getTaskId() == taskId)
					{
            found               = true;
						originalTaskLocated = true;
						break;
					}
				}
				else
				{
					// Part II - find the loop back task (possibly back to origin of follow-up chain) ...
					if (dTask.getFollowUpTaskId() == taskId)
					{
            if (toOrigin == false)
              return dTask;

						loopBackTask = dTask;

            // to origin - set to find preceeding loopback task (if one)
						found  = true;
						taskId = dTask.getTaskId();

            break;
					}
				}
			}

			if (originalTaskLocated == true && found == false)
				break;  // because a loopback task must be in the immediately preceeding stage!
		}

		return loopBackTask;
	}

/*
 * Workflow query methods:
 *
 **/

	public WorkflowTask[] getStageTasks(int stageNum) throws IllegalArgumentException
	{
		if (stageNum < 0 || stageNum > numberOfStages)
			throw new IllegalArgumentException("Workflow::getStageTasks() - Invalid stage number (" + stageNum + "), number of stages = " + numberOfStages);

		Vector v = (Vector)stageTasks.elementAt(stageNum);
		int size = v.size();

		WorkflowTask [] tasks = new WorkflowTask[size];

		for (int i = 0; i < size; ++i)
		{
			tasks[i] = (WorkflowTask)v.elementAt(i);
		}

		return tasks;
	}
	public void remove() throws RemoteException, RemoveException
	{
		String sql = "Delete from workflow where workflowId = " + workflowId;
		try
		{
			jExec.executeUpdate(sql);
			sql = null;

			// loop over stages removing workflowTask entities
			for (int i = 0; i <= numberOfStages; ++i)
			{
				Vector tasks = (Vector)stageTasks.elementAt(i);

				int numTasks = tasks.size();

				for (int j = 0; j < numTasks; ++j)
				{
					((WorkflowTask)tasks.elementAt(j)).remove();
				}
			}
		}
		catch (Exception e)
		{
			RemoveException re = new RemoveException("Workflow Entity - remove() exception");

			logger.error(re.getMessage());
			if (sql != null)
			{
				// if null must have been a exception from subordinate entity remove
				logger.error("remove sql: " + sql);
				logger.error(e);
			}

			throw re;
		}

		stageTasks = new Vector();
	}
	public void removeStage(int stageNum) throws IllegalArgumentException, RemoteException
	{
    // note: cannot remove stage 0 (async stage) but can remove tasks it.
		if (stageNum < 0 || stageNum > numberOfStages)
			throw new IllegalArgumentException("Workflow::removeStage() - Invalid stage number (" + stageNum + "), number of stages = " + numberOfStages);

		// remove the wokflow stage entiries for the stage being removed

		Vector tasks = (Vector)stageTasks.elementAt(stageNum);
		int size = tasks.size();

		for (int j = 0; j < size; ++j)
		{
			try
			{
				((WorkflowTask)tasks.elementAt(j)).remove();
			}
			catch (RemoveException e)
			{
				throw new IllegalArgumentException("Workflow::removeStage() - Unexpected problem removing stage=" + stageNum);
			}
		}

    if (stageNum == 0)
    {
      stageTasks.setElementAt(new Vector(), 0);
    }
    else
    {
      stageTasks.removeElementAt(stageNum);
      numberOfStages--;
    }
	}
	public void removeStageTask(int stageNum, int taskId) throws IllegalArgumentException, RemoteException
	{
		if (stageNum < 0 || stageNum > numberOfStages)
			throw new IllegalArgumentException("Workflow::removeStageTask() - Invalid stage number (" + stageNum + "), number of stages = " + numberOfStages);

		Vector tasks = (Vector)stageTasks.elementAt(stageNum);
		int numTasks = tasks.size();

		for (int i = 0; i < numTasks; ++i)
		{
			WorkflowTask wt = (WorkflowTask)tasks.elementAt(i);
			if (wt.getTaskId() == taskId)
			{
				try
				{
					wt.remove();
          tasks.removeElementAt(i);
				}
				catch (RemoveException e)
				{
					throw new IllegalArgumentException("Workflow::removeStageTask() - Unexpected problem removing stage=" + stageNum + ", task=" + wt.getTaskName());
				}
				return;
			}
		}

		throw new IllegalArgumentException("Workflow::removeStageTask() - Invalid taskId = " + taskId + " - task not contained in workflow stage = " + stageNum);
	}
	public void removeStageTask(int stageNum, Task dTask) throws IllegalArgumentException, RemoteException
	{
		removeStageTask(stageNum, dTask.getTaskId());
	}
/*
 *
 * Methods implemented in Enterprise Bean Instances that have no remote object
 * counterparts - i.e. EJB container callback methods, and context related methods.
 *
 **/

	public void setEntityContext(EntityContext ctx)
	{
		this.ctx = ctx;
	}
	public void unsetEntityContext()
	{
		ctx = null;
	}


}
