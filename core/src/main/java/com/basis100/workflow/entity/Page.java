package com.basis100.workflow.entity;

// PageBean:
//
// Enterprise bean entity representing a record MOS datbase table 'page'.
//

import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.PageBeanPK;

public class Page extends EntityBase
{
    // The logger
    private final static Logger _log = LoggerFactory .getLogger(Page.class);
    
	EntityContext ctx;

	// properties

	private int    pageId;
	private String pageName;
	private String pageLabel;
	private boolean editable;
	private boolean editViaTxCopy;
	private boolean dealLockOnEdit;
	private boolean allowEditWhenLocked;
	private boolean dealPage;
	private boolean includeInGoto;
	private int institutionProfileId;
	private boolean pageLockOnEdit;


	//
	// Constructor: Accepts session resources object (access to system logger and Jdbc
	//              connection resources.
	//

	public Page(SessionResourceKit srk)
	{
		super(srk);
	}
	//
	// Remote-Home inerface methods:
	//
	// The following methods have counterparts in Enterprise Bean classes - in an
	// Enterprise Entity Bean each methods name would be prefixed with "ejb"
	//

	public Page create(int pageId, String pageName, String pageLabel,
	        boolean editable, boolean editViaTxCopy, boolean dealLockOnEdit,
	        boolean allowEditWhenLocked, boolean dealPage, 
	        boolean includeInGoto, int institutionProfileId, boolean pageLockOnEdit) throws RemoteException, CreateException {

	    //FIXME : usage of institutionId needs reconsideration

	    StringBuffer sql = new StringBuffer();
	    StringBuffer values = new StringBuffer();

	    sql.append("Insert into PAGE (");

	    sql.append("PAGEID,"); 
	    values.append(pageId).append(",");

	    sql.append("PAGENAME,"); 
	    values.append("'").append(pageName).append("',");

	    sql.append("PAGELABEL,"); 
	    values.append("'").append(pageLabel).append("',");

	    sql.append("EDITABLE,"); 
	    values.append(quotedBoolStr(editable)).append(", ");

	    sql.append("EDITVIATXCOPY,"); 
	    values.append(quotedBoolStr(editViaTxCopy)).append(", ");

	    sql.append("DEALLOCKONEDIT,"); 
	    values.append(quotedBoolStr(dealLockOnEdit)).append(", ");

	    sql.append("ALLOWEDITWHENLOCKED,"); 
	    values.append(quotedBoolStr(allowEditWhenLocked)).append(", ");

	    sql.append("DEALPAGE,"); 
	    values.append(quotedBoolStr(dealPage)).append(", ");

	    sql.append("INCLUDEINGOTO,"); 
	    values.append(quotedBoolStr(includeInGoto)).append(", ");

	    sql.append("INSTITUTIONPROFILEID,"); 
	    values.append(institutionProfileId).append(", ");
	    
	    sql.append("PAGELOCKONEDIT"); 
	    values.append(quotedBoolStr(pageLockOnEdit));
	    
	    sql.append(") Values (");
	    sql.append(values);
	    sql.append(")");

	    _log.debug("create sql = " + sql.toString());

	    try
	    {
	        jExec.executeUpdate(sql.toString());
	    }
	    catch (Exception e)
	    {
	        CreateException ce = new CreateException("Page Entity - create() exception");

	        _log.error(ce.getMessage(),e);
	        _log.error("create sql: " + sql.toString());

	        throw ce;
	    }

	    setPageId(pageId);
	    setPageName(pageName);
	    setPageLabel(pageLabel);
	    setEditable(editable);
	    setEditViaTxCopy(editViaTxCopy);
	    setDealLockOnEdit(dealLockOnEdit);
	    setAllowEditWhenLocked(allowEditWhenLocked);
	    setDealPage(dealPage);
	    setIncludeInGoto(includeInGoto);
	    setInstitutionProfileId(institutionProfileId);
	    setPageLockOnEdit(pageLockOnEdit);
	    return this;
	}

	private static String quotedBoolStr(boolean b){
	    return (b) ? "'Y'" : "'N'";
	}
	
	public void ebjLoad() throws RemoteException
	{
		// implementation not required for now!
	}
	public void ebjPassivate () throws RemoteException {;}
	public void ejbActivate() throws RemoteException{;}
	
	public void ejbStore() throws RemoteException
	{
	    String sql = "Update PAGE set " +
	    "pageName = '" + getPageName() + "', " + 
	    "pageLabel = '" + getPageLabel() + "', " +
	    "editable = " + quotedBoolStr(isEditable()) + ", " + 
	    "editViaTxCopy = " + quotedBoolStr(getEditViaTxCopy()) + ", " + 
	    "dealLockOnEdit = " + quotedBoolStr(getDealLockOnEdit()) + ", " + 
	    "allowEditWhenLocked = " + quotedBoolStr(getAllowEditWhenLocked()) + ", " + 
	    "dealPage = " + quotedBoolStr(getDealPage()) + ", " + 
	    "IncludeInGoto = " + quotedBoolStr(getIncludeInGoto()) +  ", " + 
	    "pageLockOnEdit = " + quotedBoolStr(getPageLockOnEdit()) +  
	    " where pageID = " + getPageId();

	    try
	    {
	        jExec.executeUpdate(sql);
	    }
	    catch (Exception e)
	    {
	        RemoteException re = new RemoteException("Page Entity - ejbStore() exception");

	        logger.error(re.getMessage());
	        logger.error("ejbStore sql: " + sql);
	        logger.error(e);

	        throw re;
	    }
	}
	
	public Page findByName(String name) throws RemoteException, FinderException
	{
		String sql = "Select * from PAGE where pageName = '" + name + "'";

		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Page Entity: @findByName(), name=" + name + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}

		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Page Entity - findByName() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return this;
	}

	// finders:

	public Page findByPrimaryKey(PageBeanPK pk) throws RemoteException, FinderException
	{
		String sql = "Select * from PAGE where pageId = " + pk.getPageId();

		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Page Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Page Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return this;
	}

	public Vector findByAll() throws RemoteException, FinderException
	{
		String sql = "Select * from PAGE";

		boolean gotRecord = false;

    Vector v = new Vector();

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;

        Page pg = new Page(srk);

				pg.setPropertiesFromQueryResult(key);

        v.add(pg);
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Page Entity: @findByAll(), no entities found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Page Entity: @findByAll() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return v;
	}

	// setters/getters

	public int getPageId()
	{
		return pageId;
	}
	public String getPageLabel()
	{
		return pageLabel;
	}
	public String getPageName()
	{
		return pageName;
	}

 	public boolean getEditable()
	{
		return editable;
	}

	public boolean isEditable()
	{
		return getEditable();
	}

	public boolean getEditViaTxCopy()
	{
		return editViaTxCopy;
	}

	public boolean getDealLockOnEdit()
	{
		return dealLockOnEdit;
	}

	public boolean getAllowEditWhenLocked()
	{
		return allowEditWhenLocked;
	}

	public boolean getDealPage()
	{
		return dealPage;
	}
		
	public boolean getPageLockOnEdit()
	{
		return pageLockOnEdit;
	}
	
	public void remove() throws RemoteException, RemoveException
	{
		String sql = "Delete from PAGE where pageID = " + getPageId();
		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoveException re = new RemoveException("Page Entity - remove() exception");

			logger.error(re.getMessage());
			logger.error("remove sql: " + sql);
			logger.error(e);

			throw re;
		}
	}
	//
	// Methods implemented in Enterprise Bean Instances that have no remote object
	// counterparts - i.e. EJB container callback methods, and context related methods.
	//

	public void setEntityContext(EntityContext ctx)
	{
		this.ctx = ctx;
	}
	public void setPageId(int pageId)
	{
		this.pageId = pageId;
	}
	public void setPageLabel(String pageLabel)
	{
		this.pageLabel = pageLabel;
	}
	public void setPageName(String pageName)
	{
		this.pageName = pageName;
	}
	public void setEditable(boolean b)
	{
		editable = b;
	}
	public void setEditViaTxCopy(boolean b)
	{
		editViaTxCopy = b;
	}
	public void setDealLockOnEdit(boolean b)
	{
		dealLockOnEdit = b;
	}

	public void setAllowEditWhenLocked(boolean b)
	{
		allowEditWhenLocked = b;
	}

	public void setDealPage(boolean b)
	{
		dealPage = b;
	}
	
    public boolean getIncludeInGoto() {
        return includeInGoto;
    }

    public void setIncludeInGoto(boolean includeInGoto) {
        this.includeInGoto = includeInGoto;
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }

    public void setPageLockOnEdit(boolean b)
	{
		pageLockOnEdit = b;
	}
	//
	// Private
	//

	private void setPropertiesFromQueryResult(int key) throws Exception {
	    
        setPageId(jExec.getInt(key, "PAGEID"));
        setPageName(jExec.getString(key, "PAGENAME"));
        setPageLabel(jExec.getString(key, "PAGELABEL"));
        setEditable(jExec.getString(key, "EDITABLE").equals("Y"));
        setEditViaTxCopy(jExec.getString(key, "EDITVIATXCOPY").equals("Y"));
        setDealLockOnEdit(jExec.getString(key, "DEALLOCKONEDIT").equals("Y"));
        setAllowEditWhenLocked(jExec.getString(key, "ALLOWEDITWHENLOCKED").equals("Y"));
        setDealPage(jExec.getString(key, "DEALPAGE").equals("Y"));
        setIncludeInGoto(jExec.getString(key, "INCLUDEINGOTO").equals("Y"));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
        setPageLockOnEdit(jExec.getString(key, "PAGELOCKONEDIT").equals("Y"));
    }

	public void unsetEntityContext()
	{
		ctx = null;
	}


}
