package com.basis100.workflow.entity;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Sc;

import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.BusinessCalendar;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.AssignedTaskBeanPK;

/*
 *
 * AssignedTaskBean:
 *
 * Enterprise bean entity representing a record MOS datbase table 'assignedTasksWorkQueue'.
 *
 * Notes:
 *
 * On ejbCreate the value of property <assignedTasksWorkQueueId> is obtained as the next
 * available value of sequence WORKQUEUESEQ. This sequence must exist in the database, e.g.:
 *
 *                  create sequence ASSIGNEDTASKSWORKQUEUESEQ;
 *
 * At present the attribute <pass> is unconmditionally set to 0 - e.g. when creating an entity, on
 * update to database. This attribute was originally thought to be necessary for task regeneration (i.e.
 * creating a task that had previousloy been spawned and completed). It appears <pass> is not necessary
 * for this and hence only complicates matter.
 *
 **/

public class AssignedTask
  extends EntityBase
{

  EntityContext ctx;

  private final static Logger logger = 
      LoggerFactory.getLogger(AssignedTask.class);
  
  public static final String updateUP = "Update  ASSIGNEDTASKSWORKQUEUE set userProfileId = ? where assignedTasksWorkQueueId = ?";
  
  // Task visibility constants
  public static final int NEVER_VISIBLE = -1;
  public static final int HIDDEN = 0;
  public static final int VISIBLE = 1;
  public static final int TICKLE = 2;
  public static final int INPROGRESS = -2;

  // Time event types
  public static final int NONE = 0;
  public static final int REMINDER = 1;
  public static final int ESCALATION = 2;
  public static final int EXPIRY = 3;
  public static final int REISSUE = 4;

  //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
  //--> By Billy 06Oct2004
  public static final int RETRY = 5;

  //================================================================================

  // Sentinel On constants
  public static final int SENTINEL_ON_ESCALATION = 1;
  public static final int SENTINEL_ON_EXPIRY = 2;
  public static final int SENTINEL_ON_REISSUE = 3;

  // properties

  private int assignedTasksWorkQueueId;
  private int dealId;
  private String applicationId;
  private int userProfileId;
  private int timeZoneEntryId;
    // SEAN issue #494 the province id for checking the holiday associated with
    // province.
    private int _userProvinceId;
    // END issue #494
  private int workflowId;
  private int workflowStage;
  private int pass;
  private String acknowledged;
  private int taskStatusId;
  private int timeMilestoneId;
  private int reissueCount;
  private Date startTimestamp;
  private Date reminderTimestamp;
  private Date escalateTimestamp;
  private Date dueTimestamp;
  private Date endTimestamp;
  private Date nextTimeEventTimestamp;
  private int nextTimeEventType;
  private int crossRefQueueId;
  private int taskId;
  private String taskLabel;
  private int priorityId;
  private String criticalFlag;
  private int visibleFlag;
  private String sentinelFlag;
  private int sentinelOn;
  private int basedOnTaskId;
  private int followUpTaskId;
  private int linkTaskId;
  private long reminderTime;
  private long warningTime;
  private long reissueAlarmDuration;
  private int sentinelTo;
  private Date reissueTimestamp;
  private int externalRecordId;

  //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
  //--> By Billy 04Oct2004
  // New Fields to support AutoTask Retry
  private int autoTaskRetryTime;
  private int retryCounter;

  //================================================================================

  // --- for private usage only --- //
  private BusinessCalendar bCal = null;

  private int institutionProfileId;
  /*
   * Constructor: Accepts session resources object (access to system logger and Jdbc
   *              connection resources.
   *
   **/

  public AssignedTask(SessionResourceKit srk)
  {
    super(srk);
  }

  /*
   *
   * Remote-Home inerface methods:
   *
   * The following methods have counterparts in Enterprise Bean classes - in an
   * Enterprise Entity Bean each methods name would be prefixed with "ejb"
   *
   **/
  public AssignedTask create(WorkflowTask wtTask, int institutionId, 
    int dealId, String applicationId, int userProfileId, int timeZoneEntryId, int pass,
    String acknowledged, int taskStatusId, int timeMilestoneId,
    Date startTimestamp, Date reminderTimestamp, Date escalateTimestamp, Date dueTimestamp,
    int externalRecordId) throws RemoteException, CreateException
  {
//    pass = 0; // unconditional!

    // defaults on creation;
    setReissueCount(0);
    setCrossRefQueueId(0);

    commonSets(wtTask, institutionId, dealId, applicationId, userProfileId, timeZoneEntryId, pass);

    setAcknowledged(acknowledged);
    setTaskStatusId(taskStatusId);
    setTimeMilestoneId(timeMilestoneId);

    setStartTimestamp(startTimestamp);
    setReminderTimestamp(reminderTimestamp);
    setEscalateTimestamp(escalateTimestamp);
    setDueTimestamp(dueTimestamp);
    setEndTimestamp(dueTimestamp);
    setExternalRecordId(externalRecordId);

    // set next time event and type - updates time status (e.g. escalated on creation) if necessary
    setNextTimeEventAndType();

    // check time status elevated (ensure created in unacknowledged state if so)
    if(this.getTimeMilestoneId() != Sc.TASK_TIME_NORMAL)
    {
      this.setAcknowledged("N");
    }

    commonCreate();

    return this;
  }

  /**
   * Version that performs internal date calculations, i.e.
   *
   *   <startTimestamp> - if null used current time
   *   <dueTimestamp>   - if null due determined based on start and task duration
   *
   *   <reminderTimestamp> - set to first reminder event timestamp as appropriate; e.g. if
   *                         supported by task and would occur before escalation/expiry, etc.
   *
   *   <escalateTimestamp> - set automatically based on task attributes (e.g. supported by task.
   *
   * Other attrbutes are also set to default values, e.g. task status = OPEN, etc.
   *
   **/

  public AssignedTask create(WorkflowTask wtTask, int institutionId, 
    int dealId, String applicationId, int userProfileId, int timeZoneEntryId, int pass,
    Date startTimestamp, Date externalReferenceTimestamp, int externalRecordId) throws RemoteException, CreateException
  {
//    pass = 0; // unconditional!

    // defaults on creation;
    setReissueCount(0);
    setCrossRefQueueId(0);

    commonSets(wtTask, institutionId, dealId, applicationId, userProfileId, timeZoneEntryId, pass);

    setInitialAcknowledgedState();

    setTaskStatusId(Sc.TASK_OPEN);
    setTimeMilestoneId(Sc.TASK_TIME_NORMAL);
    setExternalRecordId(externalRecordId);

    getBusinessCalendar();

    // start date - now if null
    if(startTimestamp == null)
    {
      startTimestamp = new Date();

    }
    setStartTimestamp(startTimestamp);

    // due date:
    //
    // . If task duration is positive then is:  start + duration (seconds)
    // . If task duration is 0 then is: external refernece date
    // . If task duration is negative then is:   external reference date - duration (absolute value, seconds)
    //
    // If null is provided for the external reference date then the due date calculated as:
    //
    //      start + absolute value(duration)

    Date dueTimestamp = null;

    if(wtTask.getDuration() <= 0 && externalReferenceTimestamp != null)
    {
        // SEAN issue #494 we need pass the province to the method calcBusDate
        //dueTimestamp = bCal.calcBusDate(externalReferenceTimestamp, getTimeZoneEntryId(), intervalMinutes(wtTask.getDuration()));
        dueTimestamp = bCal.calcBusDate(externalReferenceTimestamp,
                                        getTimeZoneEntryId(),
                                        intervalMinutes(wtTask.getDuration()),
                                        getUserProvinceId());
        // END issue #494
    }
    else
    {
      // ensure positive duration
      int minutes = intervalMinutes(wtTask.getDuration());
      if(minutes < 0)
      {
        minutes = -minutes;

      }
      // SEAN issue #494 pass the province id the method: calcBusDate
      dueTimestamp = bCal.calcBusDate(startTimestamp, getTimeZoneEntryId(),
                                      minutes, getUserProvinceId());
      // END issue #494
    }

    setDueTimestamp(dueTimestamp);
    setEndTimestamp(dueTimestamp);

    setReminderTimestamp(null);
    setEscalateTimestamp(null);
    //--> Bug fix : to use the reissueTimestamp for better performance
    //--> By Billy 18Feb2005
    setReissueTimestamp(null);
    //===============================================================

    // escalation: due - warning duration (if not 0)
    if(getWarningTime() > 0)
    {
        // SEAN issue #494 pass the province id.
        //setEscalateTimestamp(bCal.calcBusDate(dueTimestamp, getTimeZoneEntryId(), -(intervalMinutes(getWarningTime()))));
        setEscalateTimestamp(bCal.calcBusDate(dueTimestamp,
                                              getTimeZoneEntryId(),
                                              -(intervalMinutes(getWarningTime())),
                                              getUserProvinceId()));
        // END issue #494

      // reminder: first reminder timestamp in future
    }
    if(getReminderTime() > 0)
    {
      setReminderTimestamp(bCal.nextFutureDate(startTimestamp, getTimeZoneEntryId(), intervalMinutes(getReminderTime())));

      // set next time event and type - updates time status (e.g. escalated on creation) if necessary
    }
    setNextTimeEventAndType();

    // check time status elevated (ensure created in unacknowledged state if so)
    if(this.getTimeMilestoneId() != Sc.TASK_TIME_NORMAL)
    {
      this.setAcknowledged("N");
    }

    commonCreate();

    return this;
  }

  /**
   * Version that create sentinel task for the assigned task passed.
   *
   **/

  public AssignedTask create(AssignedTask aTask, int userProfileId) throws RemoteException, CreateException
  {
    // modify sentinel specific attributes

    setDealId(aTask.getDealId());
    setTaskId(aTask.getTaskId() + 50000);
    setWorkflowId(aTask.getWorkflowId());
    setPass(aTask.getPass());
    setUserProfileId(userProfileId);
    setWorkflowStage(aTask.getWorkflowStage());
    setAcknowledged("N");
    setReissueCount(0);
    setStartTimestamp(new Date());
    setApplicationId(aTask.getApplicationId());
    setBasedOnTaskId( -1);
    setFollowUpTaskId( -1);
    setLinkTaskId( -1);
    setDueTimestamp(getStartTimestamp());
    setEscalateTimestamp(null);
    setTimeMilestoneId(Sc.TASK_TIME_NORMAL);
    setTaskStatusId(Sc.TASK_OPEN);
    setPriorityId(Sc.TASK_PRIORITY_HIGH);
    setTaskLabel(trimToLength("*Sentinel* " + aTask.getTaskLabel(), 35));
    setReissueAlarmDuration(0);
    setEndTimestamp(getStartTimestamp());
    setReminderTime(0);
    setCriticalFlag("N");
    setSentinelFlag("N");
    setSentinelOn(0);
    setVisibleFlag(aTask.getVisibleFlag());
    setReminderTimestamp(null);
    setNextTimeEventTimestamp(null);
    setNextTimeEventType(0);
    setWarningTime(0);
    setCrossRefQueueId(aTask.getAssignedTasksWorkQueueId());
    setTimeZoneEntryId(aTask.getTimeZoneEntryId());
    setSentinelTo(0);
    setExternalRecordId( -1);
    //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
    //--> By Billy 18Oct2004
    // New Fields to support AutoTask Retry
    // Will never retry on sentinel tasks
    setAutoTaskRetryTime(0);
    setRetryCounter(0);
    setReissueTimestamp(null);

    setInstitutionProfileId(aTask.getInstitutionProfileId());
    
    commonCreate();

    return this;
  }

  private String trimToLength(String str, int len)
  {
    if(str.length() > len)
    {
      str = str.substring(0, len);

    }
    return str;
  }

  private void commonCreate() throws CreateException
  {
    String sql = "Insert into AssignedTasksWorkQueue(" +
      "assignedTasksWorkQueueId, dealId, applicationId, userProfileId, timeZoneEntryId, " +
      "workflowId, workflowStage, pass, " +
      "acknowledged, taskStatusId, timeMilestoneId, reissueCount, " +
      "startTimestamp, reminderTimestamp, escalateTimestamp, dueTimestamp, endTimestamp, " +
      "nextTimeEventTimestamp, nextTimeEventType, crossRefQueueId, " +
      "taskId, taskLabel, priorityId, criticalFlag, visibleFlag, sentinelFlag, sentinelOn, " +
      "basedOnTaskId, followUpTaskId, linkTaskId, " +
      "reminderTime, warningTime, reissueAlarmDuration, sentinelTo, externalRecordId, " +
      //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
      //--> By Billy 18Oct2004
      // New Fields to support AutoTask Retry
      "autoTaskRetryTime, retryCounter, " +
      //--> Bug fix : to use the reissueTimestamp for better performance
      //--> By Billy 18Feb2005
      "reissueTimestamp, " +
      //================================================================
      "institutionProfileId " + 
      ") Values (" +

      "ASSIGNEDTASKSWORKQUEUESEQ.NEXTVAL" +
      ", " + getDealId() + ", '" + getApplicationId() + "', " + getUserProfileId() + ", " + getTimeZoneEntryId() +
      ", " + getWorkflowId() + ", " + getWorkflowStage() + ", " + getPass() +
      ", '" + getAcknowledged() + "', " + getTaskStatusId() + ", " + getTimeMilestoneId() + ", " + getReissueCount() +
      ", " + sqlDateStr(getStartTimestamp()) + ", " + sqlDateStr(getReminderTimestamp()) +
      ", " + sqlDateStr(getEscalateTimestamp()) + ", " + sqlDateStr(getDueTimestamp()) +
      ", " + sqlDateStr(getDueTimestamp()) + ", " + sqlDateStr(getNextTimeEventTimestamp()) + // end == due on creation
      ", " + getNextTimeEventType() + ", " + getCrossRefQueueId() + ", " +

      getTaskId() + ", '" +
      getTaskLabel() + "', " +
      getPriorityId() + ", '" +
      getCriticalFlag() + "', " +
      getVisibleFlag() + ", '" +
      getSentinelFlag() + "', " +
      getSentinelOn() + ", " +
      getBasedOnTaskId() + ", " +
      getFollowUpTaskId() + ", " +
      getLinkTaskId() + ", " +
      getReminderTime() + ", " +
      getWarningTime() + ", " +
      getReissueAlarmDuration() + ", " +
      getSentinelTo() + ", " +
      getExternalRecordId() + ", " +
      //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
      //--> By Billy 18Oct2004
      // New Fields to support AutoTask Retry
      getAutoTaskRetryTime() + ", " +
      getRetryCounter() + ", " +
      //--> Bug fix : to use the reissueTimestamp for better performance
      //--> By Billy 18Feb2005
      sqlDateStr(getReissueTimestamp()) + ", " +
      getInstitutionProfileId() + ")";

      //=================================================================

    //boolean gotRecord = false;

    try
    {
      jExec.executeUpdate(sql);

      sql = "select assignedTasksWorkQueueId from assignedTasksWorkQueue where dealId = " +
        dealId + " AND taskId = " + getTaskId() + " AND userProfileId = " +
        userProfileId + " AND pass = " + pass;

      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        //gotRecord = true;

        setAssignedTasksWorkQueueId(jExec.getInt(key, 1));
        break; // can only be one record
      }

      jExec.closeData(key);
    }
    catch(Exception e)
    {
      CreateException ce = new CreateException("AssignedTask Entity - create() exception");

      logger.error(ce.getMessage());
      logger.error("create sql: " + sql);
      //logger.error(e);

      throw ce;
    }
  }

  private void commonSets(WorkflowTask wtTask, int institutionId,
    int dealId, String applicationId, int userProfileId, int timeZoneEntryId, int pass)
  {

    // from parameters passed
    setDealId(dealId);
    setApplicationId(applicationId);
    setUserProfileId(userProfileId);
    // SEAN issue #494 get the province associated with the user profile id. set
    // to the province id.
    setUserProvinceIdByUserProfileId(userProfileId, institutionId);
    // END issue #494
    setPass(pass);

    // time zone : if -1 passed get it from the database
    if(timeZoneEntryId == -1)
    {
      timeZoneEntryId = getTimeZone(userProfileId);

    }
    setTimeZoneEntryId(timeZoneEntryId);

    // values from task passed
    setWorkflowId(wtTask.getWorkflowId());
    setWorkflowStage(wtTask.getWorkflowStage());

    setTaskId(wtTask.getTaskId());
    setTaskLabel(wtTask.getTaskLabel());
    setPriorityId(wtTask.getPriorityId());
    setCriticalFlag(wtTask.getCriticalFlag());
    setVisibleFlag(wtTask.getVisibleFlag());
    setSentinelFlag(wtTask.getSentinelFlag());
    setSentinelOn(wtTask.getSentinelOn());
    setBasedOnTaskId(wtTask.getBasedOnTaskId());
    setFollowUpTaskId(wtTask.getFollowUpTaskId());
    setLinkTaskId(wtTask.getLinkTaskId());
    setReminderTime(wtTask.getReminderTime());
    setWarningTime(wtTask.getWarningTime());
    setReissueAlarmDuration(wtTask.getReissueAlarmDuration());
    setSentinelTo(wtTask.getSentinelTo());
    //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
    //--> By Billy 18Oct2004
    // New Fields to support AutoTask Retry
    setAutoTaskRetryTime(wtTask.getAutoTaskRetryTime());
    setRetryCounter(0);
    setInstitutionProfileId(wtTask.getInstitutionProfileId());
    //================================================================================

  }

    // SEAN issue #494 set the user province id based on the user profile id.
    // This method will be invoked in two places: one in the create method,
    // another is in the reset method.
    private void setUserProvinceIdByUserProfileId(int userProfileId, int institutionId) {
        try {
            UserProfile userProfileDAO = new UserProfile(srk);
            UserProfile theProfile = userProfileDAO.
                findByPrimaryKey(new UserProfileBeanPK(userProfileId, institutionId));
            setUserProvinceId(theProfile.getContact().getAddr().getProvinceId());
        } catch (RemoteException re) {
            logger.error("Remote Exception: " + re.getMessage());
        } catch (FinderException fe) {
            logger.error("Finder Exception: " + fe.getMessage());
        }
    }
    // END issue #494

  /*
   * Called at task creation (and also when task re-routed) to set acknowledged state. Basically,
   * if the task priority is higher than "MEDIUM" the acknowledged attribute is set to "N", otherwise
   * to "Y".
   *
   **/

  private void setInitialAcknowledgedState()
  {
    String acknowledged = (getPriorityId() <= Sc.TASK_PRIORITY_MEDIUM) ? "N" : "Y";

    setAcknowledged(acknowledged);
  }

  public void ebjLoad() throws RemoteException
  {
    // implementation not required for now!
  }

  public void ebjPassivate() throws RemoteException
  {
    ;
  }

  public void ejbActivate() throws RemoteException
  {
    ;
  }

  
  
  public void ejbStore() throws RemoteException
  {
    // Following intentionally limited to attribute that can change during the
    // lifecycle of the entity

    String sql = "Update  ASSIGNEDTASKSWORKQUEUE set " +

      "userProfileId = " + getUserProfileId() + ", " +

      "acknowledged = '" + getAcknowledged() + "', " +
      "reissueCount = " + getReissueCount() + ", " +

      "startTimestamp = " + sqlDateStr(getStartTimestamp()) + ", " +
      "reminderTimestamp = " + sqlDateStr(getReminderTimestamp()) + ", " +
      "escalateTimestamp = " + sqlDateStr(getEscalateTimestamp()) + ", " +
      "dueTimestamp = " + sqlDateStr(getDueTimestamp()) + ", " +
      "endTimestamp = " + sqlDateStr(getEndTimestamp()) + ", " +
      "nextTimeEventTimestamp = " + sqlDateStr(getNextTimeEventTimestamp()) + ", " +
      "nextTimeEventType = " + getNextTimeEventType() + ", " +
      "taskStatusId = " + getTaskStatusId() + ", " +
      "timeMilestoneId = " + getTimeMilestoneId() + ", " +
      "priorityId = " + getPriorityId() + ", " +
      "visibleFlag = " + getVisibleFlag() + ", " +
      "crossRefQueueId = " + getCrossRefQueueId() + ", " +
      "reminderTime = " + getReminderTime() + ", " +
      "warningTime = " + getWarningTime() + ", " +
      "reissueAlarmDuration = " + getReissueAlarmDuration() + ", " +
      //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
      //--> By Billy 19Oct2004
      // New Fields to support AutoTask Retry
      "autoTaskRetryTime = " + getAutoTaskRetryTime() + ", " +
      "retryCounter = " + getRetryCounter() + ", " +
      //================================================================================
      //--> Bug fix : to use the reissueTimestamp for better performance
      //--> By Billy 18Feb2005
      "reissueTimestamp = " + sqlDateStr(getReissueTimestamp()) + ", " +
      //================================================================================
      "pass = " + getPass() +

      " Where assignedTasksWorkQueueId = " + getAssignedTasksWorkQueueId();
    try
    {
    	logger.debug("ejbStore@AssignedTask Tread ID = " + Thread.currentThread().getId());
    	logger.debug("ejbStore@AssignedTask Tread Name = " + Thread.currentThread().getName());
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      RemoteException re = new RemoteException("AssignedTask Entity - ejbStore() exception");

      logger.error(re.getMessage());
      logger.error("ejbStore sql: " + sql);
      //logger.error(e);

      throw re;
    }
  }

  /*
  *
  * finders:
  *
  **/

 /*
  * findByDeal:
  *
  * This finder return a collection of assigned tasks. The assigned tasks are all those
  * associated with the specified deal (currently in the work queue). The assigned tasks
  * are ordered by workflow stage
  *
  **/

 public Vector<AssignedTask> findByDealbyStatus(int dealId, int taskStatusId) throws RemoteException, FinderException
 {
   Vector<AssignedTask> v = new Vector<AssignedTask>();

   String sql = "Select * from ASSIGNEDTASKSWORKQUEUE where dealId = " + dealId + " and taskstatusid = " + taskStatusId +
   		" Order by workflowStage, startTimestamp";

   try
   {
     int key = jExec.execute(sql);

     for(; jExec.next(key); )
     {
       AssignedTask aTask = new AssignedTask(srk);

       aTask.setPropertiesFromQueryResult(key);

       v.addElement(aTask);
     }

     jExec.closeData(key);
   }
   catch(Exception e)
   {
     FinderException fe = new FinderException("AssignedTask Entity - findByDealbyStatus() exception");

     logger.error(fe.getMessage());
     logger.error("finder sql: " + sql);
     //logger.error(e);

     throw fe;
   }

   return v;
 }
 
  /*
   *
   * finders:
   *
   **/

  /*
   * findByDeal:
   *
   * This finder return a collection of assigned tasks. The assigned tasks are all those
   * associated with the specified deal (currently in the work queue). The assigned tasks
   * are ordered by workflow stage
   *
   **/

  public Vector findByDeal(int dealId) throws RemoteException, FinderException
  {
    Vector v = new Vector();

    String sql = "Select * from ASSIGNEDTASKSWORKQUEUE where dealId = " + dealId + " Order by workflowStage, startTimestamp";

    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        AssignedTask aTask = new AssignedTask(srk);

        aTask.setPropertiesFromQueryResult(key);

        v.addElement(aTask);
      }

      jExec.closeData(key);
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException("AssignedTask Entity - findByDeal() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      //logger.error(e);

      throw fe;
    }

    return v;
  }

  public AssignedTask findNextAutoTask(int i_intUserProfileId, int i_intRetryCounter) throws RemoteException, FinderException
  {

    String strSQL = "SELECT * FROM ASSIGNEDTASKSWORKQUEUE WHERE ENDTIMESTAMP IN (SELECT MIN(ENDTIMESTAMP)FROM ASSIGNEDTASKSWORKQUEUE WHERE USERPROFILEID = " +
       i_intUserProfileId + " AND TASKSTATUSID = 1 AND VISIBLEFLAG = 1 AND RETRYCOUNTER < " + i_intRetryCounter + ")" + " AND USERPROFILEID = " +
       i_intUserProfileId + " AND TASKSTATUSID = 1 AND VISIBLEFLAG = 1 AND RETRYCOUNTER < " + i_intRetryCounter ;

    boolean gotRecord = false;

    try
    {

      int key = jExec.execute(strSQL);
      for(; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
        String msg = "No Auto Task found !!";
        logger.warn(msg);
       return null;
      }
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException("AssignedTask Entity - findNextAutoTask() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + strSQL);
      //logger.error(e);

     return null;
    }

    return this;

  }

  public AssignedTask findByPrimaryKey(AssignedTaskBeanPK pk) throws RemoteException, FinderException
  {
    String sql = "Select  * from ASSIGNEDTASKSWORKQUEUE where assignedTasksWorkQueueId = " + pk.getAssignedTasksWorkQueueId();

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
        String msg = "AssignedTask Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException("AssignedTask Entity - findByPrimaryKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      //logger.error(e);

      throw fe;
    }

    return this;
  }

  /*
   * Populate a task as a copy of another - this may be stretching the 'findBy' paradigm but does provide
   * a reliable (admittedly very inefficient!) method to get a copy.
   *
   * Reliability is the key here since this technique ensure that we get true copy even should the
   * entity attributes change (via call to setPropertiesFromQueryResult()).
   *
   **/

  public AssignedTask findByCreateCopy(AssignedTask aOrigTask) throws RemoteException, FinderException
  {
    String sql = "Select  * from ASSIGNEDTASKSWORKQUEUE where assignedTasksWorkQueueId = " +
      aOrigTask.getAssignedTasksWorkQueueId();

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
        String msg = "AssignedTask Entity: @findByCreateCopy(), key=" + aOrigTask.getAssignedTasksWorkQueueId() +
          ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException("AssignedTask Entity - findByCreateCopy() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      //logger.error(e);

      throw fe;
    }

    return this;
  }

  // WF Regression Issue wrong asignee - OCT 1, 2008 STARTS ---
  public AssignedTask findRegressedOriginalTask(WorkflowTask newWFTask, int dealId) {
      
      String sql = "Select  * from ASSIGNEDTASKSWORKQUEUE where " +
          "dealid = " + dealId + " and taskId = " + newWFTask.getTaskId() + 
          " and workflowId = " + newWFTask.getWorkflowId() + " and pass < 0";
      
    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
          String msg = "AssignedTask Entity: @findRegressedOriginalTask()_1, dealId =" + dealId  +
          ", old task not found";
          logger.debug(msg);
          return null;
      } else {
          String msg = "AssignedTask Entity: @findRegressedOriginalTask()_2, dealId =" + dealId  +
          ", Found Old Task ID= : " + this.getAssignedTasksWorkQueueId();
          logger.debug(msg);
      }
    }
    catch(Exception e)
    {
        String msg = "AssignedTask Entity: @findRegressedOriginalTask()_2, dealId =" + dealId  +
        ", old task not found";
        logger.debug(msg);
        return null;
    }

    return this;

  }

  
  public void remove() throws RemoteException, RemoveException
  {
    String sql = "Delete from ASSIGNEDTASKSWORKQUEUE where assignedTasksWorkQueueId = " + getAssignedTasksWorkQueueId();
    try
    {
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      RemoveException re = new RemoveException("AssignedTask Entity - remove() exception");

      logger.error(re.getMessage());
      logger.error("remove sql: " + sql);
      //logger.error(e);

      throw re;
    }
  }

  public void traceShow(SysLogger logger)
  {
    String str = "assignedTasksWorkQueueId = " + assignedTasksWorkQueueId + ", " +
      "dealId = " + getDealId() + ", " +
      "taskId = " + getTaskId() + "(" + getTaskLabel() + ")" + ", " +
      "userProfileId = " + getUserProfileId() + ", " +
      "workflowId/workflowStage/pass = " + getWorkflowId() + "/" + getWorkflowStage() + "/" + getPass() + ", " +
      "priority/acknowledged/critical = " + getPriorityId() + "/" + getAcknowledged() + "/" + getCriticalFlag() + ", " +
      "taskStatusId/timeMilestoneId/reissueCount = " + getTaskStatusId() + "/" + getTimeMilestoneId() + "/" + getReissueCount() +
      ", " +
      "follow-up/link = " + getFollowUpTaskId() + "/" + getLinkTaskId();

    logger.trace("AssignedTask :: " + str);
  }

  /**
   * Typically called when a task is being extended - e.g. the due timestamp is adjusted to a future
   * date. May also be called to assign the task to a different user (for a re-route) - this is natural
   * since a re-routed task is typically extended.
   *
   * Arguments
   *
   * <newUserProfileId> - A valid user profile id or -1. If a user profile id is provided this indicates
   *                      a task re-route and the task status is changed accordingly.
   *
   *                      The acknowledged state is revised when the tash is re-routed. The logic matches
   *                      the rule used at task creation (e.g. in constructors that do not specify the
   *                      acknowledged state) - current set to unacknowledged if priority is HIGH or
   *                      greater.
   *
   * <newDueDate>       - If not null is the new due date for the task - the date provided would presumably
   *                      be a future date but no validation is perfomed to ensure this.
   *
   * <newDuration>      - Only interrogated when <newDueDate> is null. Indicate a new duration (time in
   *                      seconds from current time till due). If 0 the duration is the original duration
   *                      (i.e. duration when task was created). Finally, -1 indicates no extension.
   *
   *                       Business calendar is used to calculate due date when duration (special or
   *                       original) used - i.e. calculation not simply calendar based.
   *
   * The following chart summaries task extension for values of <newDueDate> and <newDuration>:
   *
   *  <newDueDate>   <newDuration>  Task Extended?   New Due Date
   *  ------------   -------------  --------------   -------------
   *   {provided}        n/a            YES          provided date
   *      null           >0             YES          current date +  <newDuration>
   *      null            0             YES          current date + original duration
   *      null           -1              NO          n/a
   *
   *
   *  <reminderTime>, <warningTime>, and <reissueAlarmDuration>
   *
   *     These arguments allow same named properties to be set. To indicate no change in existing
   *     values code -1; note value 0 is valid - i.e. if <warningTime> is specified as 0 the task would
   *     not escalate even if in its original state it did.
   *
   *     The values of these arguments are only examined if the task is being extended, otherwise
   *     they are ignored.
   *
   * When a task is extended the other date/time related field are automatically recalculated, that is,
   * the escalation and/or reminder date/times as appropriate.
   *
   * ASSUMPTIONS
   *
   * When a task is being re-routed the application (caller) is entirely responsible for ensuring that
   * the indicated (new) user is suitable to accept the task.
   *
   **/

  public void reset(int newUserProfileId, int institutionId, Date newDueDate, long newDuration,
    long reminderTime, long warningTime, long reissueAlarmDuration)
  {
    if(newUserProfileId != -1)
    {
      //  re-route
      setUserProfileId(newUserProfileId);
      // SEAN issue #494 reset the province id based on the user profile id.
      setUserProvinceIdByUserProfileId(newUserProfileId, institutionId);
      // END issue #494
      setInitialAcknowledgedState();
      setTaskStatusId(Sc.TASK_REROUTE);
    }

    // check for extension ...
    if(newDueDate == null && newDuration == -1)
    {
      return;
    }

    // being extended ... pick up timing redefinitions (if any)
    if(reminderTime != -1)
    {
      setReminderTime(reminderTime);

    }
    if(warningTime != -1)
    {
      setWarningTime(warningTime);

    }
    if(reissueAlarmDuration != -1)
    {
      setReissueAlarmDuration(reissueAlarmDuration);

      // revise dates
    }
    setExtensionDueDate(newDueDate, newDuration);

    setReminderTimestamp(null);
    setEscalateTimestamp(null);
    //--> Bug fix : to use the reissueTimestamp for better performance
    //--> By Billy 18Feb2005
    setReissueTimestamp(null);
    //================================================================

    // Bug fixed -- Without this will cause NullsePointer Ecxeption
    getBusinessCalendar();

    // escalation: due - warning duration (if not 0)
    if(getWarningTime() > 0)
    {
        // SEAN issue #494 pass province id to the method calcBusDate.
        //setEscalateTimestamp(bCal.calcBusDate(getDueTimestamp(), getTimeZoneEntryId(), -(intervalMinutes(getWarningTime()))));
        setEscalateTimestamp(bCal.calcBusDate(getDueTimestamp(),
                                              getTimeZoneEntryId(),
                                              -(intervalMinutes(getWarningTime())),
                                              getUserProvinceId()));
        // END issue #494
      // reminder: first reminder timestamp in future
    }
    if(getReminderTime() > 0)
    {
      setReminderTimestamp(bCal.nextFutureDate(startTimestamp, getTimeZoneEntryId(), intervalMinutes(getReminderTime())));

      // set next time event and type - updates time status (e.g. escalated on creation) if necessary)
    }
    setTimeMilestoneId(Sc.TASK_TIME_NORMAL);
    setNextTimeEventAndType();

    // check time status elevated (ensure created in unacknowledged state if so)
    if(this.getTimeMilestoneId() != Sc.TASK_TIME_NORMAL)
    {
      setAcknowledged("N");
    }
  }

  private void setExtensionDueDate(Date newDueDate, long newDuration)
  {
    getBusinessCalendar();

    if(newDueDate != null)
    {
      // Bug fix : need also to check if the new DueDate is a valid Working Day -- By BILLY 11Jan2002
      //setDueTimestamp(newDueDate);
      logger.debug("BILLY ===> the newDueDate = " + newDueDate + " :: getTimeZoneEntryId() = " + getTimeZoneEntryId());
      // SEAN issue #494 pass the province id to the method: calcBusDate
      setDueTimestamp(bCal.calcBusDate(newDueDate, getTimeZoneEntryId(), 0,
                                       getUserProvinceId()));
      // END issue #494
      logger.debug("BILLY ===> the DueTimestamp = " + getDueTimestamp());
    }
    else
    {
      // determine duration as original duration (if necessary)

      // ALERT we should have the original duration since the calculation below will not
      //       necessarilly yield original duration (due to usage of business calander).
      if(newDuration == 0)
      {
        newDuration = (getEndTimestamp().getTime() - getStartTimestamp().getTime()) / 1000; // secs

      }
      // SEAN issue #494 pass the province id to the method: calcBusDate
      setDueTimestamp(bCal.calcBusDate(new Date(), getTimeZoneEntryId(),
                                       intervalMinutes(newDuration),
                                       getUserProvinceId()));
      // END issue #494
    }

    // end timestamp matches due until task completed
    setEndTimestamp(getDueTimestamp());
  }

  /*
   * Called (by external daemon) when a time threshold has been passed - e.g. when the external daemon
   * detects that the current time is beyond the value set in <nextTimeEventTimestamp>.
   *
   * Result: true  - if entity has been updated (any attribute!)
   *         false - if no update activity required - if call is a no-op
   *
   **/

  public boolean handleTimeEvent()
  {
    traceLog("HandleTineEvent(IN )", true);

    // check for task completed  - if so place into resolution state (no futher time events ....
    int val = getTaskStatusId();
    if(val == Sc.TASK_COMPLETE || val == Sc.TASK_CANCELLED)
    {
      if(getVisibleFlag() != NEVER_VISIBLE)
      {
        setVisibleFlag(VISIBLE);
      }
      setAcknowledged("Y");
      setNextTimeEventTimestamp(null);
      setNextTimeEventType(NONE);

      traceLog("HandleTineEvent(OUT)", true);
      return true;
    }

    // set task to 'unacknowledged'
    setAcknowledged("N");

    if(getNextTimeEventType() == REISSUE)
    {
      setReissueCount(getReissueCount() + 1);

      // on reminder event change visibility to 'tickle' if visibility is 'hidden'
    }
    if((getNextTimeEventType() == REMINDER) && (getVisibleFlag() == HIDDEN))
    {
      setVisibleFlag(VISIBLE);

      // setup for 'next' time event.
    }
    //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
    //--> By Billy 19Oct2004
    // Set the task visible for the AME to handle it
    if((getNextTimeEventType() == RETRY) && (getVisibleFlag() == HIDDEN))
    {
      setVisibleFlag(VISIBLE);
    }
    //================================================================================

    setNextTimeEventAndType();

    traceLog("HandleTineEvent(OUT)", true);
    return true;
  }

  /**
   * Called to determine if a sentinel task should be generated - normally called immediately
   * after a 'time' event (see handleTimeEvent()).
   *
   * Programmer Note:
   *
   * While a sentinel may already exist at the time the call is made this has no bearing on the result.
   * The id of an existing sentinel can be obtained via a call to getCrossRefQueueId() - if the result is
   * not 0 the result would be the id of the existing sentinel.
   *
   **/

  public boolean sentinelRequired()
  {
    // if sentinel already exists then obviously necessary
    if(this.getCrossRefQueueId() != 0)
    {
      return true;
    }

    // if never generated then obviously not
    if(!sentinelFlag.equals("Y"))
    {
      return false;
    }

    // remainder simply qualify start on ...
    int tmId = getTimeMilestoneId();

    // never start until at least escalation
    if(tmId == Sc.TASK_TIME_NORMAL)
    {
      return false;
    }

    // check starts beyond escalation
    if(tmId == Sc.TASK_TIME_ESCALATED && getSentinelOn() != SENTINEL_ON_ESCALATION)
    {
      return false;
    }

    if(tmId == Sc.TASK_TIME_EXPIRED)
    {
      // check starts beyond expiry
      if(getReissueCount() == 0 && getSentinelOn() != SENTINEL_ON_EXPIRY)
      {
        return false;
      }
    }

    // sentinel necessary!
    return true;
  }

  /**
   * Called to indicate a task in 'tickle' visible state has been 'selected' by the user. If the task has
   * not escalated, expired or reached completion state the visibility state is returned to 'hidden'.
   *
   * Result: true  - entity has been updated
   *         false - entity has not been updated
   **/

  public boolean tickle()
  {
    traceLog("TICKLE  (IN)", false);

    if(getVisibleFlag() != TICKLE)
    {
      traceLog("TICKLE (OUT)", false);
      return false;
    }

    // visibility back to 'hidden' (but to 'visible' if escalated, expired or reached completion state)

    if(getTimeMilestoneId() != Sc.TASK_TIME_NORMAL)
    {
      this.setVisibleFlag(this.VISIBLE);
    }
    else
    {
      this.setVisibleFlag(this.HIDDEN);
    }

    traceLog("TICKLE (OUT)", false);

    return true;
  }

  public String taskIdentification()
  {
    return

      "Task id=" + getTaskId() + " (" + getTaskLabel() + "), deal = " + getDealId() + ", user = " + getUserProfileId();
  }

  /*
   * setters/getters
   *
   **/

  public int getAssignedTasksWorkQueueId()
  {
    return assignedTasksWorkQueueId;
  }

  public int getBasedOnTaskId()
  {
    return basedOnTaskId;
  }

  public int getDealId()
  {
    return dealId;
  }

  public Date getDueTimestamp()
  {
    return dueTimestamp;
  }

  public Date getEndTimestamp()
  {
    return endTimestamp;
  }

  public Date getEscalateTimestamp()
  {
    return escalateTimestamp;
  }

  public int getFollowUpTaskId()
  {
    return followUpTaskId;
  }

  public int getLinkTaskId()
  {
    return linkTaskId;
  }

  public int getPass()
  {
    return pass;
  }

  public int getPriorityId()
  {
    return priorityId;
  }

  public long getReissueAlarmDuration()
  {
    return reissueAlarmDuration;
  }

  public int getReissueCount()
  {
    return reissueCount;
  }

  public Date getStartTimestamp()
  {
    return startTimestamp;
  }

  public int getTaskId()
  {
    return taskId;
  }

  public String getTaskLabel()
  {
    return taskLabel;
  }

  public int getTaskStatusId()
  {
    return taskStatusId;
  }

  public int getTimeMilestoneId()
  {
    return timeMilestoneId;
  }

  public int getUserProfileId()
  {
    return userProfileId;
  }

    // SEAN issue #494 getter for province.
    public int getUserProvinceId() {
        return _userProvinceId;
    }
    // END issue #494

  public int getTimeZoneEntryId()
  {
    return timeZoneEntryId;
  }

  public int getWorkflowId()
  {
    return workflowId;
  }

  public int getWorkflowStage()
  {
    return workflowStage;
  }

  public String getAcknowledged()
  {
    return acknowledged;
  }

  public String getApplicationId()
  {
    return applicationId;
  }

  public long getReminderTime()
  {
    return reminderTime;
  }

  public String getCriticalFlag()
  {
    return criticalFlag;
  }

  public String getSentinelFlag()
  {
    return sentinelFlag;
  }

  public int getSentinelOn()
  {
    return sentinelOn;
  }

  public int getSentinelTo()
  {
    return sentinelTo;
  }

  public int getVisibleFlag()
  {
    return visibleFlag;
  }

  public Date getReminderTimestamp()
  {
    return reminderTimestamp;
  }

  public Date getNextTimeEventTimestamp()
  {
    return nextTimeEventTimestamp;
  }

  public int getNextTimeEventType()
  {
    return nextTimeEventType;
  }

  public long getWarningTime()
  {
    return warningTime;
  }

  public int getCrossRefQueueId()
  {
    return crossRefQueueId;
  }

  public int getExternalRecordId()
  {
    return externalRecordId;
  }

  public int getAutoTaskRetryTime()
  {
    return autoTaskRetryTime;
  }

  public int getRetryCounter()
  {
    return retryCounter;
  }

  public Date getReissueTimestamp()
  {
    return reissueTimestamp;
  }

  // --- //

  public void setAcknowledged(String acknowledged)
  {
    this.acknowledged = acknowledged;
  }

  public void setApplicationId(String applicationId)
  {
    this.applicationId = applicationId;
  }

  public void setAssignedTasksWorkQueueId(int assignedTasksWorkQueueId)
  {
    this.assignedTasksWorkQueueId = assignedTasksWorkQueueId;
  }

  public void setBasedOnTaskId(int basedOnTaskId)
  {
    this.basedOnTaskId = basedOnTaskId;
  }

  public void setDealId(int dealId)
  {
    this.dealId = dealId;
  }

  public void setDueTimestamp(Date dueTimestamp)
  {
    this.dueTimestamp = dueTimestamp;
  }

  public void setEndTimestamp(Date endTimestamp)
  {
    this.endTimestamp = endTimestamp;
  }

  public void setEscalateTimestamp(Date escalateTimestamp)
  {
    this.escalateTimestamp = escalateTimestamp;
  }

  public void setFollowUpTaskId(int followUpTaskId)
  {
    this.followUpTaskId = followUpTaskId;
  }

  public void setLinkTaskId(int linkTaskId)
  {
    this.linkTaskId = linkTaskId;
  }

  public void setPass(int pass)
  {
    this.pass = pass;
  }

  public void setPriorityId(int priorityId)
  {
    this.priorityId = priorityId;
  }

  public void setReissueAlarmDuration(long reissueAlarmDuration)
  {
    this.reissueAlarmDuration = reissueAlarmDuration;
  }

  public void setReissueCount(int reissueCount)
  {
    this.reissueCount = reissueCount;
  }

  public void setStartTimestamp(Date startTimestamp)
  {
    this.startTimestamp = startTimestamp;
  }

  public void setTaskId(int taskId)
  {
    this.taskId = taskId;
  }

  public void setTaskLabel(String taskLabel)
  {
    this.taskLabel = taskLabel;
  }

  public void setTaskStatusId(int taskStatusId)
  {
    this.taskStatusId = taskStatusId;
  }

  public void setTimeMilestoneId(int TMId)
  {
    timeMilestoneId = TMId;
  }

  public void setUserProfileId(int userProfileId)
  {
    this.userProfileId = userProfileId;
  }

    // SEAN issue #494 the setter for province id.
    public void setUserProvinceId(int provinceId) {
        _userProvinceId = provinceId;
    }
    // END issue #494

  public void setTimeZoneEntryId(int timeZoneEntryId)
  {
    this.timeZoneEntryId = timeZoneEntryId;
  }

  public void setWorkflowId(int workflowId)
  {
    this.workflowId = workflowId;
  }

  public void setWorkflowStage(int workflowStage)
  {
    this.workflowStage = workflowStage;
  }

  public void setReminderTime(long reminderTime)
  {
    this.reminderTime = reminderTime;
  }

  public void setCriticalFlag(String criticalFlag)
  {
    this.criticalFlag = criticalFlag;
  }

  public void setSentinelFlag(String sentinelFlag)
  {
    this.sentinelFlag = sentinelFlag;
  }

  public void setSentinelOn(int sentinelOn)
  {
    this.sentinelOn = sentinelOn;
  }

  public void setSentinelTo(int sentinelTo)
  {
    this.sentinelTo = sentinelTo;
  }

  public void setVisibleFlag(int visibleFlag)
  {
    this.visibleFlag = visibleFlag;
  }

  public void setReminderTimestamp(Date reminderTimestamp)
  {
    this.reminderTimestamp = reminderTimestamp;
  }

  public void setNextTimeEventTimestamp(Date nextTimeEventTimestamp)
  {
    this.nextTimeEventTimestamp = nextTimeEventTimestamp;
  }

  public void setNextTimeEventType(int nextTimeEventType)
  {
    this.nextTimeEventType = nextTimeEventType;
  }

  public void setWarningTime(long warningTime)
  {
    this.warningTime = warningTime;
  }

  public void setCrossRefQueueId(int crossRefQueueId)
  {
    this.crossRefQueueId = crossRefQueueId;
  }

  public void setExternalRecordId(int recId)
  {
    externalRecordId = recId;
  }

  public void setAutoTaskRetryTime(int autoTaskRetryTime)
  {
    this.autoTaskRetryTime = autoTaskRetryTime;
  }

  public void setRetryCounter(int retryCounter)
  {
    this.retryCounter = retryCounter;
  }

  public void setReissueTimestamp(Date reissueTimestamp)
  {
    this.reissueTimestamp = reissueTimestamp;
  }

  /*
   *
   * Methods implemented in Enterprise Bean Instances that have no remote object
   * counterparts - i.e. EJB container callback methods, and context related methods.
   *
   **/

  public void setEntityContext(EntityContext ctx)
  {
    this.ctx = ctx;
  }

  //
  // Private
  //

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
    setDealId(jExec.getInt(key, "dealId"));
    setAssignedTasksWorkQueueId(jExec.getInt(key, "assignedTasksWorkQueueId"));
    setTaskId(jExec.getInt(key, "taskId"));
    setWorkflowId(jExec.getInt(key, "workflowid"));
    setPass(jExec.getInt(key, "pass"));
    setUserProfileId(jExec.getInt(key, "userProfileId"));
    setWorkflowStage(jExec.getInt(key, "workflowStage"));
    setAcknowledged(jExec.getString(key, "acknowledged"));
    setReissueCount(jExec.getInt(key, "reissueCount"));
    setStartTimestamp(jExec.getDate(key, "startTimestamp"));
    setApplicationId(jExec.getString(key, "applicationId"));
    setBasedOnTaskId(jExec.getInt(key, "basedOnTaskId"));
    setFollowUpTaskId(jExec.getInt(key, "followUpTaskId"));
    setLinkTaskId(jExec.getInt(key, "linkTaskId"));
    setDueTimestamp(jExec.getDate(key, "dueTimestamp"));
    setEscalateTimestamp(jExec.getDate(key, "escalateTimestamp"));
    setTimeMilestoneId(jExec.getInt(key, "timeMilestoneId"));
    setTaskStatusId(jExec.getInt(key, "taskStatusId"));
    setPriorityId(jExec.getInt(key, "priorityId"));
    setTaskLabel(jExec.getString(key, "taskLabel"));
    setReissueAlarmDuration(jExec.getInt(key, "reissueAlarmDuration"));
    setEndTimestamp(jExec.getDate(key, "endTimestamp"));
    setReminderTime(jExec.getInt(key, "reminderTime"));
    setCriticalFlag(jExec.getString(key, "criticalFlag"));
    setSentinelFlag(jExec.getString(key, "sentinelFlag"));
    setSentinelOn(jExec.getInt(key, "sentinelOn"));
    setVisibleFlag(jExec.getInt(key, "visibleFlag"));
    setReminderTimestamp(jExec.getDate(key, "reminderTimestamp"));
    setNextTimeEventTimestamp(jExec.getDate(key, "nextTimeEventTimestamp"));
    setNextTimeEventType(jExec.getInt(key, "nextTimeEventType"));
    setWarningTime(jExec.getInt(key, "warningTime"));
    setCrossRefQueueId(jExec.getInt(key, "crossRefQueueId"));
    setTimeZoneEntryId(jExec.getInt(key, "timeZoneEntryId"));
    setSentinelTo(jExec.getInt(key, "sentinelTo"));
    setExternalRecordId(jExec.getInt(key, "externalRecordId"));
    //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
    //--> By Billy 04Oct2004
    // New Fields to support AutoTask Retry
    setAutoTaskRetryTime(jExec.getInt(key, "autoTaskRetryTime"));
    setRetryCounter(jExec.getInt(key, "retryCounter"));
    //================================================================================
    //--> Bug fix : to use the reissueTimestamp for better performance
    //--> By Billy 18Feb2005
    setReissueTimestamp(jExec.getDate(key, "reissueTimestamp"));
    //================================================================================
    setInstitutionProfileId(jExec.getInt(key, "institutionProfileId"));
  }

  public void unsetEntityContext()
  {
    ctx = null;
  }

  /**
   * Based upon current attributes of the entity determines the next time related event
   * to occur and its type. For example, call may determine that next time event is expiry.
   *
   * In addition the method will:
   *
   *   . ensure time milestone status is set appropriately (e.g.escalation or expiry).
   *
   *   . automatically determine the timestamp for the next reminder or reissue alarm
   *     event.
   *
   * Since the call can update entity attributes an update of the entity would typically
   * be performed after the call. Attributes potentially updated during the call:
   *
   *   nextTimeEventTimestamp, nextTimeEventType, reminderTimestamp, timeMilestoneId
   *
   *   .... and others (see changeTimeMilestoneId() for attributes updated indirectly)
   *
   * PROGRAMMER NOTE
   *
   * When determining the 'next' reminder event the method obtains the timestamp of the
   * 'last' reminder event from the value of <reminderTimestamp>. This attribute will
   * be updated with the 'next' reminder timestamp (but set to null if there are to be
   * no future reminder events - see code below).
   *
   * When determining the next reissue alarm event the method does not have a means
   * of determining the 'last' reissue event - i.e. there is no <reissueTimestamp> attribute
   * that tracks the last reissue event. Consequently, the method determines the next reissue
   * event by calculating forward from the due timestamp. This is done via a
   * BusinessCalander object. It is mentioned here since the calculation can be fairly
   * heavy depending upon current date and length of the reissue alarm duration; in general
   * worst as time since expiry increases when reissue duration is short.
   *
   **/

  private void setNextTimeEventAndType()
  {
    getBusinessCalendar();

    Date dt = null;
    Date compareTo = null;

    // check for reminder event
    int interval = intervalMinutes(getReminderTime());

    if(interval > 0)
    {
      dt = bCal.nextFutureDate(getReminderTimestamp(), getTimeZoneEntryId(), interval);
      if(dt != null)
      {
        // next time event is a reminder if before escalation/expiry
        compareTo = getEscalateTimestamp();
        if(compareTo == null)
        {
          compareTo = getDueTimestamp();

        }
        if(dt.compareTo(compareTo) < 0)
        {
          setReminderTimestamp(dt);
          setNextTimeEventTimestamp(dt);
          setNextTimeEventType(REMINDER);
          return;
        }
      }
    }
    setReminderTimestamp(null); // won't be any future reminders

    // check for escalation event (ensuring escalation time hasn't already happened)
    dt = bCal.futureDate(getEscalateTimestamp());
    if(dt != null)
    {
      setNextTimeEventTimestamp(dt);
      setNextTimeEventType(ESCALATION);
      return;
    }

    // ensure time milestone set to escalated (overriden to expired if necessary below)
    if(getEscalateTimestamp() != null)
    {
      changeTimeMilestoneId(Sc.TASK_TIME_ESCALATED);

      // check for expiry event (ensuring expiry hasn't already happened)
    }
    dt = bCal.futureDate(getDueTimestamp());
    if(dt != null)
    {
      setNextTimeEventTimestamp(dt);
      setNextTimeEventType(EXPIRY);
      return;
    }

    // ensure time milestone set to expired
    changeTimeMilestoneId(Sc.TASK_TIME_EXPIRED);

    // check for a reissue alarm event
    interval = intervalMinutes(getReissueAlarmDuration());
    if(interval <= 0)
    {
      // task does not reissue (non-completed) warnings
      setNextTimeEventTimestamp(null);
      setNextTimeEventType(NONE);
      return;
    }

    // determine timestamp of next reissue
    //--> Bug fix : to use the reissueTimestamp for better performance
    //--> By Billy 18Feb2005
    if(reissueTimestamp == null)
    {
      dt = bCal.nextFutureDate(getDueTimestamp(), getTimeZoneEntryId(), interval);
    }
    else
    {
      dt = bCal.nextFutureDate(reissueTimestamp, getTimeZoneEntryId(), interval);
    }
    setReissueTimestamp(dt);
    //===================================================================
    setNextTimeEventTimestamp(dt);
    setNextTimeEventType(REISSUE);
  }

  /*
   * Internal call to set state of entity due to escalation or expiry (i.e. that is time milestone
   * status moving to escalation or expiry). Namely:
   *
   *  . Upon task escalation task priority is elevated one level, e.g. Medium to High, Low to Medium, etc.
   *
   *    If the priority at time of escalation is Very High the priority does not change.
   *
   *  . Upon task expiry task priority is changed to High subject to the following.
   *
   *      If at time of expiry the priority is High it is changed to Very High.
   *      If a time of expiry task priority is Very High the priority does not change.
   *
   * Logic to update task visibility for change in time milestone status also housed here, namely:
   *
   *  . A hidden task becomes visible upon the task escalation (or upon expiry if the task does not
   *    escalate). This is true regardless of current state (Hidden or Tickle) at the time of escalation
   *    (or expiry).
   *
   **/

  private void changeTimeMilestoneId(int TMId)
  {
    // detect no-op calls - the time milestone never 'retreats' (e.g. goes from escalated to normal) -
    // however certain internal processing is made easier by not checking for current status, hence
    // 'set back' calls are allowed but ignored!

    if(TMId <= timeMilestoneId)
    {
      return;
    }

    setTimeMilestoneId(TMId);

    // escallation or expiry - ensure visible (if visible status is not never visible)
    if(getVisibleFlag() != NEVER_VISIBLE)
    {
      setVisibleFlag(VISIBLE);

    }
    int priorityId = getPriorityId();

    if(timeMilestoneId == Sc.TASK_TIME_ESCALATED)
    {
      // increase priority one level (if not already very high)
      priorityId--;
      if(priorityId != 0)
      {
        setPriorityId(priorityId);

      }
      return;
    }

    if(timeMilestoneId == Sc.TASK_TIME_EXPIRED)
    {
      if(priorityId == Sc.TASK_PRIORITY_VERY_HIGH)
      {
        return;
      }

      if(priorityId == Sc.TASK_PRIORITY_HIGH)
      {
        setPriorityId(Sc.TASK_PRIORITY_VERY_HIGH);
        return;
      }

      setPriorityId(Sc.TASK_PRIORITY_HIGH);

      return;
    }
  }

  private void traceLog(String msg, boolean timeEvent)
  {
    String prefix = "{WFTASK: id = " + getAssignedTasksWorkQueueId() + ", deal = " + getDealId() + "} - " + msg + " :: ";

    String detail =
      "taskStatusId = " + getTaskStatusId() + ", " +
      "timeMilestoneId = " + getTimeMilestoneId() + ", " +
      "priorityId = " + getPriorityId() + ", " +
      "visibleFlag = " + getVisibleFlag();

    logger.trace(prefix + detail);

    if(timeEvent)
    {
      detail = "nextEvent = " + eventSymbol() + ", " +
        "nextEvent@ = " + sqlDateStr(getNextTimeEventTimestamp()) + ", " +
        "reminder@ = " + sqlDateStr(getReminderTimestamp()) + ", " +
        "escalate@ = " + sqlDateStr(getEscalateTimestamp()) + ", " +
        "due@ = " + sqlDateStr(getDueTimestamp());
      logger.trace(prefix + detail);
    }
  }

  private String eventSymbol()
  {
    switch(getNextTimeEventType())
    {
      case NONE:
        return "NONE";

      case REMINDER:
        return "REMINDER";

      case ESCALATION:
        return "ESCALATION";

      case EXPIRY:
        return "EXPIRY";

      case REISSUE:
        return "REISSUE";
    }

    return "???";
  }

  private BusinessCalendar getBusinessCalendar()
  {
    if(bCal != null)
    {
      return bCal;
    }

    bCal = new BusinessCalendar(srk);
    return bCal;
  }

  /**
   * Conver seconds to minutes (strictly integer division)
   *
   **/

  private int intervalMinutes(long seconds)
  {
    //--> Bug fix : min is 1 min if it's <> 0
    if(seconds > 0 && seconds < 60)
    {
      return 1;
    }
    else
    {
      return(int)(seconds / 60L);
    }
  }

  private int getTimeZone(int userProfileId)
  {
    int userTimeZone = 0;

    try
    {
      String sql = "Select a.TIMEZONEENTRYID from BRANCHPROFILE a, GROUPPROFILE b, USERPROFILE c WHERE " +
        " c.USERPROFILEID = " + userProfileId +
        " AND c.GROUPPROFILEID = b.GROUPPROFILEID AND b.BRANCHPROFILEID = a.BRANCHPROFILEID";

      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        userTimeZone = jExec.getInt(key, 1);
        break;
      }

      jExec.closeData(key);
    }
    catch(Exception e)
    {
      userTimeZone = 5;
    }

    return userTimeZone;
  }

  /**
   * Default date format for MOS Oracle: YYYY-MM-DD HH24:MI:SS
   *
   */
  public String sqlDateStr(Date d)
  {
    return DBA.sqlStringFrom(d);
  }

  //--> Cervus II : Modifications to handle AutoTask Retry (AME spec. section 4.2.2)
  //--> By Billy 04Oct2004

  /**
   * To set this Assigned Task for Retry state.
   *
   * Set the following if Task Status = OPEN or RE-ROUTE:
   * 1) Increment RetryCounter if incrRetryCount = true
   * 2) Set Next Event = "RETRY"
   * 3) Set Next Event Time = Current Time + AutoTaskRetryTime (in Sec.)
   * 4) Set visible flag = HIDDEN
   *
   * Note : This will override all other events (e.g. Escalaction or Expire).
   *
   * @param incrRetryCount boolean
   * - true : increment the RetryCounter (retry controlled internally)
   * - false : do not increment the RetryCounter (retry controlled by external, e.g. Helix)
   */
  public void setForRetry(boolean incrRetryCount)
  {
    // Check if the Task Status == (OPEN or RE-ROUTE) ==> otherwise nothing to do
    if(getTaskStatusId() == Sc.TASK_OPEN || getTaskStatusId() == Sc.TASK_REROUTE)
    {
      //check if increment retry counter
      if(incrRetryCount)
      {
        setRetryCounter(getRetryCounter() + 1);
        // check if current Retry >= MAXRETRY
        int maxRetry = TypeConverter.intTypeFrom(
          (PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.workflow.maxautotaskretry", "5")), 0);
        if(getRetryCounter() >= maxRetry)
        {
          // Set for expired all retry and escalated...
          setRetryExpired();
        }
        else
        {
          // Set for Retry
          int retryTime = intervalMinutes(getAutoTaskRetryTime());
          if(retryTime > 0)
          {
            // Set the next time event to RETRY
            setNextTimeEventType(RETRY);

            // Set Next event TimeStamp to CurrentTime + RetryTime
            Calendar retryDate = Calendar.getInstance();
            retryDate.add(Calendar.MINUTE, retryTime);
            setNextTimeEventTimestamp(retryDate.getTime());

            // Set the task to be Hidden
            setVisibleFlag(HIDDEN);

            return;
          }
        }
            }
        } else if (getTaskStatusId() == Sc.TASK_COMPLETE
                && getVisibleFlag() == INPROGRESS)
        {
	   setVisibleFlag(VISIBLE);
	}
    }
  

  /**
   * To set this Assigned Task to the Max Retrys and forced the system alert to be generated.
   * This is used when Retry is controlled by external system e.g. Helix.
   *
   * Set the following if Task Status = OPEN or RE-ROUTE:
   * 1) Set RetryCounter = MAXRETRY
   * 2) Call setNextTimeEventAndType
   *
   */
  public void setRetryExpired()
  {
    // Check if the Task Status == (OPEN or RE-ROUTE) ==> otherwise nothing to do
    if(getTaskStatusId() == Sc.TASK_OPEN || getTaskStatusId() == Sc.TASK_REROUTE)
    {
      // Set Retry counter to MAXRETRY
      int maxRetry = TypeConverter.intTypeFrom(
        (PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),"com.basis100.workflow.maxautotaskretry", "5")), 0);
      setRetryCounter(maxRetry);

      // Set the task to be visible
      setVisibleFlag(VISIBLE);

      // Call setNextTimeEventAndType to set the next event as usual
      setNextTimeEventAndType();
    }else if (getTaskStatusId() == Sc.TASK_COMPLETE && getVisibleFlag() == INPROGRESS ){
       setVisibleFlag(VISIBLE);
    }

  }



  //--> Billy ==> Ruth : Should return the number of records affected
  //--> 22Nov2004
  public int resetOutstandingTasks(int userProfileId) throws Exception
  {
    String strSQL = "UPDATE ASSIGNEDTASKSWORKQUEUE set visibleflag = " + this.VISIBLE +
      " WHERE USERPROFILEID = " + userProfileId + " AND visibleflag = " + this.INPROGRESS;
    int affected = 0;
    try
    {

      affected = jExec.executeUpdate(strSQL);

    }
    catch(Exception e)
    {
      Exception fe = new Exception("AssignedTask Entity - resetOutstandingTasks() exception");

      logger.error(fe.getMessage());
      logger.error("update sql: " + strSQL);
      //logger.error(e);

      throw fe;
    }

    return affected;

  }

  /**
   * @return the institutionId
   */
  public int getInstitutionProfileId() {
    return institutionProfileId;
  }

  /**
   * @param institutionId the institutionId to set
   */
  public void setInstitutionProfileId(int institutionId) {
    this.institutionProfileId = institutionId;
  }

}
