package com.basis100.workflow.entity;

//PageAccess:

//A page access entity hold a list of access permissions for MOS pages. The primary key of an
//entity is composed of the following fields:

//userTypeId, userProfileId

//If userProfileId is 0 then the entity contains the default page access information by
//user type - a default page access entity

//If userProfileId is not 0 then the entity contains page access information for the
//specified user - a user page access entity.

//Page access information consists (logically) a list with the following columns:

//pageId, access permission

//Page Id corresponds to the entries in the page table.
//Access permission (ids) correspond to the records in table accessType - these are
//currently defined as (Sc symbolic constants):

//PAGE_ACCESS_UNASSIGNED
//PAGE_ACCESS_DISALLOWED
//PAGE_ACCESS_VIEW_ONLY
//PAGE_ACCESS_EDIT
//PAGE_ACCESS_SUSPENDED
//PAGE_ACCESS_EDIT_ON_DECISION_MOD

//A default page access entity is expected to contain access information for all pages
//defined (in the MOS system) - the access list corresponds to records form table
//userTypePageAccess

//A user page access entity may:

//. not exist (for a specific MOS user)
//. exist, but contains fewer entries that the number of defined pages.

//Basically, a user page access entity holds exceptional user page access information
//(for a specifcic MOS user). If no exception access has been stipulated for a user then
//no user page access entity will exist for that user.

//Programmer Notes
//----------------

//Internally the access list is maintained in a hashtable - page Id is the key for table
//entries. Each list entry is composed of the following (including the key):

//Page Id,  Access Permission, Status

//Status is defined as follows:

//ORIGINAL - no change since instance instantiated
//UPDATE   - access permission changed (see setAccessPermission())
//DELETE   - access permission removed (see deleteAccessPermission())
//NEW      - access permission is a new entry (see setAccessPermission())

//Methods:

//. public int getAccessPermission(int pageId)

//Return the access permission for the specified page id.

//. public void setAccessPermission(int pageId)

//Sets the access permission for the specified page id; entry added to access list
//if page id no found, otherwise access list entry updated.

//. public void removeAccessPermission(int pageId)

//Remove entry from access list with the specific page Id.

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import MosSystem.Sc;

import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.PageAccessBeanPK;

public class PageAccess extends EntityBase
{
    EntityContext ctx;

    private int userTypeId;
    private int userProfileId;
    private int institutionProfileId;

    // internal
    private static final int ORIGINAL = 0;
    private static final int UPDATE   = 1;
    private static final int DELETE   = 2;
    private static final int NEW      = 4;

    private Hashtable<String, Vector<Integer>> accessList = new Hashtable<String, Vector<Integer>>();

    //
    // Remote-Home inerface methods:
    //
    // The following methods have counterparts in Enterprise Bean classes - in an
    // Enterprise Entity Bean each methods name would be prefixed with "ejb"
    //

    // create:
    //
    // Creation occurs under following circumstances:
    //
    //  userTypeId   - is a valid MOS user type
    //  userProfile  - is 0, or is a valid MOS user Id and that the userTypeId is correct for
    //                 the specified user.
    //
    // Upon creation no database representation is created - this is deferred until the filst call
    // to ejbStore.

    private boolean dbRepresentationExists = false;

    //
    // Constructor: Accepts session resources object (access to system logger and Jdbc
    //              connection resources.
    //

    public PageAccess(SessionResourceKit srk)
    {
        super(srk);
    }

    /**
     * @deprecated
     */
    public PageAccess create(int userTypeId, int userProfileId, int institutionId) 
        throws RemoteException, CreateException
    {

        this.userTypeId    = userTypeId;
        this.userProfileId = userProfileId;
        this.institutionProfileId = institutionId;

        // ensure that the entity does not exist

        PageAccess testPgAccess = new PageAccess(srk);

        boolean exists = false;
        PageAccessBeanPK pk = new PageAccessBeanPK(userTypeId, userProfileId);

        try
        {
            testPgAccess = testPgAccess.findByPrimaryKey(pk);
            exists = true;
        }
        catch (FinderException fe) {;}
        catch (Exception e)
        {
            CreateException ce = new CreateException("PageAccess Entity - create() exception");

            logger.error(ce.getMessage() + " :: exception follows");
            logger.error(e.getMessage());
            logger.error(e);
            throw ce;
        }


        if (exists)
        {
            CreateException ce = new CreateException("PageAccess Entity - create() exception :: Entity Exists pk = " + pk);

            logger.error(ce.getMessage());
            throw ce;
        }
        // don't create db representation until store.
        return this;
    }

//    /**
//     * @deprecated
//     */
//    public void ebjLoad() throws RemoteException
//    {
//        // implementation not required for now!
//    }
//    /**
//     * @deprecated
//     */
//    public void ebjPassivate () throws RemoteException {;}
//    /**
//     * @deprecated
//     */
//    public void ejbActivate() throws RemoteException{;}
    /**
     * @deprecated
     */
    public void ejbStore() throws RemoteException
    {
        String table        = null;
        String idFld        = null;
        int    idVal;

        // assume userTypePageAccess
        table = "userTypePageAccess";
        idFld = "userTypeId";
        idVal = userTypeId;

        if (userProfileId != 0)
        {
            table = "userPageAccessException";
            idFld = "userProfileId";
            idVal = userProfileId;
        }

        String iSql = "Insert into " + table + " (" + idFld + ", pageID, accessTypeId, institutionProfileId) Values (";
        String uSql = "Update " + table + " set accessTypeId = ";
        String dSql = "Delete from " + table + " where " + idFld + " = ";

        Enumeration<String> e = accessList.keys();

        for (; e.hasMoreElements() ;)
        {
            String key = e.nextElement();

            Vector<Integer> v = accessList.get(key);

            int status = v.elementAt(1).intValue();

            if (status == ORIGINAL)
                continue;

            int pageId           = getPageIdFromKey(key);
            int accessPermission = ((Integer)v.elementAt(0)).intValue();

            String sql = null;
            if (status == UPDATE)
            {
                sql = uSql + accessPermission + " WHERE pageID = " + pageId + " AND " + idFld + " = " + idVal;
            }
            else if (status == DELETE)
            {
                sql = dSql + idVal + " AND pageID = " + pageId;
            }
            else // new
            {
                sql = iSql + idVal + ", " + pageId + ", " + accessPermission + ", " + institutionProfileId + ")";
            }

            try
            {
                jExec.executeUpdate(sql);
            }
            catch (Exception ex)
            {
                RemoteException re = new RemoteException("PageAccess Entity - ejbStore() exception");

                logger.error(re.getMessage());
                logger.error("ejbStore sql: " + sql);
                logger.error(ex);

                throw re;
            }
        }

        // one more time - removing deleted entries and setting other to original

        e = accessList.keys();

        for (; e.hasMoreElements() ;)
        {
            String pID = e.nextElement();

            Vector<Integer> v = accessList.get(pID);

            Integer iS  = v.elementAt(1);

            int status = iS.intValue();

            if (status == DELETE)
            {
                accessList.remove(pID);
                continue;
            }

            // not original - then updated or new - now saved - change to original
            if (status != ORIGINAL)
            {
                Integer iAP = (Integer)v.elementAt(0);

                accessList.remove(pID);
                Vector<Integer> nv = new Vector<Integer>(2);
                nv.addElement(iAP);
                nv.addElement(iS);

                accessList.put(pID, nv);
            }
        }

        dbRepresentationExists = true;
    }

    // finders:

    public PageAccess findByPrimaryKey(PageAccessBeanPK pk) throws RemoteException, FinderException
    {
        String sql = null;


        this.userTypeId    = pk.getUserTypeId();
        this.userProfileId = pk.getUserProfileId();

        // fixed for FXP21790
        if (pk.getUserLogin() != null && pk.getUserLogin().trim().length()>0) {
            sql = "Select upae.* from userPageAccessException upae, userprofile up "
                + "where up.userlogin = '" + pk.getUserLogin() + "' and upae.userprofileid=up.userprofileId"; 
        } else if (userProfileId < 1){
            sql = "Select * from userTypePageAccess where userTypeID = " + pk.getUserTypeId();
        } else {
            // this section should not used when user loing after MCM
            sql = "Select * from userPageAccessException where userProfileId = " + pk.getUserProfileId();
        }
        
        boolean gotRecord = false;

        try
        {
            int key = jExec.execute(sql);

            for (; jExec.next(key); )
            {
                gotRecord = true;
                addAccessListEntry(key, ORIGINAL);
            }

            jExec.closeData(key);

            if (gotRecord == false)
            {
                String msg = "PageAccess Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";

                if (getSilentMode() == false)
                    logger.error(msg);

                throw new FinderException(msg);
            }
        }
        catch (Exception e)
        {
            if (gotRecord == false && getSilentMode() == true)
                throw (FinderException)e;

            FinderException fe = new FinderException("PageAccess Entity - findByPrimaryKey() exception");

            logger.error(fe.getMessage());
            logger.error("finder sql: " + sql);
            logger.error(e);

            throw fe;
        }

        dbRepresentationExists = true;
        return this;
    }

    //
    // Methods
    //
    /**
     * @deprecated
     */
    public int getAccessPermission(int pageId, int institutionId)
    {
        String key = makeKey(pageId, institutionId);
        Vector<Integer> v = accessList.get(key);

        if (v == null)
            return Sc.PAGE_ACCESS_UNASSIGNED;  // access not represented

        if (((Integer)v.elementAt(0)).intValue() == DELETE)
            return Sc.PAGE_ACCESS_UNASSIGNED;  // deleted entry equivalent to not specified

        return ((Integer)v.elementAt(0)).intValue();
    }

    // This methods applies the entries in a specified page access object as updates to this
    // object. Specifically:
    //
    //     . for common pageId the access type in the override object is adopted
    //     . pageId in the override object that are not present in this object are
    //       added


    public void applyAsOverrides(PageAccess overrides) {
        if (overrides == null)
            return;

        Hashtable<String, Vector<Integer>> al = overrides.accessList;

        Enumeration<String> e = al.keys();

        for (; e.hasMoreElements();) {
            String key = e.nextElement();
            Vector<Integer> v = al.get(key);
            
            int[] keys = PageAccess.decomposeKey(key); 
            
            setAccessPermission(keys[0],keys[1], v.elementAt(0).intValue());
        }
    }

    // Returns a hash table with page id as key and access type as value
    public Hashtable<String, Integer> getAccessTable() {
        Enumeration<String> e = accessList.keys();

        Hashtable<String, Integer> simpleAL = new Hashtable<String, Integer>();

        for (; e.hasMoreElements();) {
            String key = e.nextElement();

            Vector<Integer> v = accessList.get(key);

            simpleAL.put(key, v.elementAt(0));
        }

        return simpleAL;
    }

    /**
     * @deprecated
     */
    public void remove() throws RemoteException, RemoveException
    {
        if (dbRepresentationExists == false)
            return; // never actually existed

        String sql = null;

        if (userProfileId == 0)
        {
            sql = "Delete from userTypePageAccess where userTypeID = " + userTypeId;
        }
        else
        {
            sql = "Delete from userPageAccessException where userProfileID = " + userProfileId;
        }

        try
        {
            jExec.executeUpdate(sql);
        }
        catch (Exception e)
        {
            RemoveException re = new RemoveException("PageAccess Entity - remove() exception");

            logger.error(re.getMessage());
            logger.error("remove sql: " + sql);
            logger.error(e);

            throw re;
        }
    }

    /**
     * @deprecated
     */
    public void removeAccessPermission(int pageId, int institutionId)
    {
        //Integer tabKey = new Integer(pageId);
        String key = makeKey(pageId, institutionId);

        Vector<Integer> v = accessList.get(key);

        if (v == null)
            return; // not present - ignore

        // remove and add back with remove status

        int status = ((Integer)v.elementAt(1)).intValue();

        accessList.remove(key);

        if (status == NEW)
            return;   // never savwed to database

        Vector<Integer> nv = new Vector<Integer>(2);
        nv.addElement(new Integer(1));
        nv.addElement(new Integer(DELETE));

        accessList.put(key, nv);
    }


    public void setAccessPermission(int pageId, int institutionId, int accessPermission)
    {

        String tabKey = makeKey(pageId, institutionId);
        Vector<Integer> v = accessList.get(tabKey);

        int status = NEW; // assume new

        if (v != null)
        {
            // exists - remove and add new entry as update
            accessList.remove(tabKey);
            status = UPDATE;
        }

        Vector<Integer> nv = new Vector<Integer>(2);
        nv.addElement(new Integer(accessPermission));
        nv.addElement(new Integer(status));

        accessList.put(tabKey, nv);
    }
    //
    // Methods implemented in Enterprise Bean Instances that have no remote object
    // counterparts - i.e. EJB container callback methods, and context related methods.
    //

    /**
     * @deprecated
     */
    public void setEntityContext(EntityContext ctx)
    {
        this.ctx = ctx;
    }
    /**
     * @deprecated
     */
    public void unsetEntityContext()
    {
        ctx = null;
    }

    //
    // Private
    //

    private void addAccessListEntry(int key, int status) throws Exception
    {
        int pageId = jExec.getInt(key, "PAGEID");
        int institutionId = jExec.getInt(key, "INSTITUTIONPROFILEID");
        String mapKey = makeKey(pageId, institutionId);

        Integer accessTypeId = new Integer(jExec.getInt(key, "ACCESSTYPEID"));
        Vector<Integer> v = new Vector<Integer>(2);
        v.addElement(accessTypeId);
        v.addElement(new Integer(status));

        accessList.put(mapKey, v);
    }

    private static final String SEPARATOR = "#";
    public static String makeKey(int pageid, int institutionProfileId){

        StringBuffer sb = new StringBuffer();
        sb.append(String.valueOf(pageid));
        sb.append(SEPARATOR);
        sb.append(String.valueOf(institutionProfileId));
        return sb.toString();
    }
    
    public static int getPageIdFromKey(String tableKey){
        return decomposeKey(tableKey)[0];
    }
    
    public static int getInstitutionIdFromKey(String tableKey){
        return decomposeKey(tableKey)[1];
    }
    
    public static int[] decomposeKey(String tableKey) {
        int[] result = new int[2];

        StringTokenizer st = new StringTokenizer(tableKey, SEPARATOR);

        if (st.hasMoreTokens())
            result[0] = TypeConverter.intTypeFrom(st.nextToken(), 0);
        if (st.hasMoreTokens())
            result[1] = TypeConverter.intTypeFrom(st.nextToken(), 0);

        return result;
    }

}
