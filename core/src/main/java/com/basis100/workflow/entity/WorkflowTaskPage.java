package com.basis100.workflow.entity;

// WorkflowTaskPageBean:
//
// Enterprise bean entity representing a record MOS datbase table 'page'.
//

import java.util.Vector;

import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.WorkflowTaskPageBeanPK;

public class WorkflowTaskPage extends EntityBase
{
	EntityContext ctx;

	// properties

	private int    workflowTaskPageId;
	private int    workflowId;
	private int    pageId;
	private int    taskId;
	private int    taskSequence;
	private int    institutionProfileId;
	
	//
	// Constructor: Accepts session resources object (access to system logger and Jdbc
	//              connection resources.
	//

	public WorkflowTaskPage(SessionResourceKit srk)
	{
		super(srk);
	}
	//
	// Remote-Home inerface methods:
	//
	// The following methods have counterparts in Enterprise Bean classes - in an
	// Enterprise Entity Bean each methods name would be prefixed with "ejb"
	//

	public WorkflowTaskPage create(int workflowId, int pageId, int taskId, int taskSequence) throws RemoteException, CreateException
	{
	  //FIXME : the usage of institutionId needs reconsideration
		WorkflowTaskPageBeanPK pk = createPrimaryKey();

		String sql = "Insert into WORKFLOWTASKPAGE (workflowTaskPageId, workflowId, pageId, taskId, taskSequence) Values (" +
					  pk.getWorkflowTaskPageId() + ", " + workflowId + ", " + pageId + ", " + taskId + ", " + taskSequence + ")";

		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException("WorkflowTaskPage Entity - create() exception");

			logger.error(ce.getMessage());
			logger.error("create sql: " + sql);
			logger.error(e);

			throw ce;
		}

		setWorkflowTaskPageId(workflowTaskPageId);
		setWorkflowId(workflowId);
		setPageId(pageId);
		setTaskId(taskId);
		setTaskSequence(taskSequence);


		return this;
	}
	public WorkflowTaskPageBeanPK createPrimaryKey()throws CreateException
	{
		String sql = "Select WORKFLOWTASKPAGESEQ.NEXTVAL from dual";
		int id = -1;

		try
		{
		   int key = jExec.execute(sql);

		   for (; jExec.next(key);  )
		   {
			   id = jExec.getInt(key,1);  // can only be one record
		   }
		   if( id == -1 ) throw new Exception();
		}
		catch (Exception e)
		{
			e.printStackTrace();

			CreateException ce = new CreateException("WorkflowTaskPage Entity create() exception getting userProfileId from sequence");
			logger.error(ce.getMessage());
			logger.error(e);
			throw ce;
		}

		return new WorkflowTaskPageBeanPK(id);
	}
	public void ebjLoad() throws RemoteException
	{
		// implementation not required for now!
	}
	public void ebjPassivate () throws RemoteException {;}
	public void ejbActivate() throws RemoteException{;}
	public void ejbStore() throws RemoteException
	{
		String sql = "Update WORKFLOWTASKPAGE set taskSequence = " + getTaskSequence() + " where workflowTaskPageId = " + getWorkflowTaskPageId();

		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoteException re = new RemoteException("WorkflowTaskPage Entity - ejbStore() exception");

			logger.error(re.getMessage());
			logger.error("ejbStore sql: " + sql);
			logger.error(e);

			throw re;
		}
	}
	// findByPage:
	//
	// This finder return a collection of page-workflow task association beans. The beans are
	// all those with matching page Id and workflow - i.e. used to locate all workflow tasks
	 // associated with a given interface page. The beans are ordered by task sequence.

	public Vector findByPage(int pageId, int workflowId) throws RemoteException, FinderException
	{
		Vector tps = new Vector();

		String sql = "Select * from WORKFLOWTASKPAGE " +
					 "where pageId = " + pageId + " AND workflowId = " + workflowId + " Order by taskSequence";

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				WorkflowTaskPage tp = new WorkflowTaskPage(srk);

				tp.setPropertiesFromQueryResult(key);

				tps.addElement(tp);
			}

			jExec.closeData(key);
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("WorkflowTaskPage Entity - findByPage() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return tps;
	}
	// finders:

	public WorkflowTaskPage findByPrimaryKey(WorkflowTaskPageBeanPK pk) throws RemoteException, FinderException
	{
		String sql = "Select * from WORKFLOWTASKPAGE where workflowTaskPageId = " + pk.getWorkflowTaskPageId();

		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "WorkflowTaskPage Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("WorkflowTaskPage Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return this;
	}
	// findByTask:
	//
	// This finder return a collection of page-workflow task association beans. The beans are
	// all those with matching task Id and workflow Id - i.e. used to locate all pages
	// associated with a given workflow task. The beans are ordered by task sequence.

	public Vector findByTask(int taskId, int workflowId) throws RemoteException, FinderException
	{
		Vector tps = new Vector();

		String sql = "Select * from WORKFLOWTASKPAGE " +
					 "where taskId = " + taskId + " AND workflowId = " + workflowId + " Order by taskSequence";

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				WorkflowTaskPage tp = new WorkflowTaskPage(srk);

				tp.setPropertiesFromQueryResult(key);

				tps.addElement(tp);
			}

			jExec.closeData(key);
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("WorkflowTaskPage Entity - findByTask() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return tps;
	}
	public int getPageId()
	{
		return pageId;
	}
	public int getTaskId()
	{
		return taskId;
	}
	public int getTaskSequence()
	{
		return taskSequence;
	}
	public int getWorkflowId()
	{
		return workflowId;
	}
	// setters/getters

	public int getWorkflowTaskPageId()
	{
		return workflowTaskPageId;
	}
	public void remove() throws RemoteException, RemoveException
	{
		String sql = "Delete from WORKFLOWTASKPAGE where workflowTaskPageId = " + getWorkflowTaskPageId();
		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoveException re = new RemoveException("WorkflowTaskPage Entity - remove() exception");

			logger.error(re.getMessage());
			logger.error("remove sql: " + sql);
			logger.error(e);

			throw re;
		}
	}
	//
	// Methods implemented in Enterprise Bean Instances that have no remote object
	// counterparts - i.e. EJB container callback methods, and context related methods.
	//

	public void setEntityContext(EntityContext ctx)
	{
		this.ctx = ctx;
	}
	public void setPageId(int pageId)
	{
		this.pageId = pageId;
	}
	//
	// Private
	//

	private void setPropertiesFromQueryResult(int key) throws Exception {

        setWorkflowTaskPageId(jExec.getInt(key, "WORKFLOWTASKPAGEID"));
        setWorkflowId(jExec.getInt(key, "WORKFLOWID"));
        setPageId(jExec.getInt(key, "PAGEID"));
        setTaskId(jExec.getInt(key, "TASKID"));
        setTaskSequence(jExec.getInt(key, "TASKSEQUENCE"));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }
	
	public void setTaskId(int taskId)
	{
		this.taskId = taskId;
	}
	public void setTaskSequence(int taskSequence)
	{
		this.taskSequence = taskSequence;
	}
	public void setWorkflowId(int workflowId)
	{
		this.workflowId = workflowId;
	}
	public void setWorkflowTaskPageId(int workflowTaskPageId)
	{
		this.workflowTaskPageId = workflowTaskPageId;
	}
	public void unsetEntityContext()
	{
		ctx = null;
	}

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }
}
