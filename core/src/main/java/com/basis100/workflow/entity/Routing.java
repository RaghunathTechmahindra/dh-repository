//
// Routing Entity : Representing a Routing Record from MOS.Routing Table
// Version : Initial version by BILLY 16Jan2002
//

package com.basis100.workflow.entity;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.RoutingPK;

public class Routing extends EntityBase
{
	EntityContext ctx;

	// properties

	private int     routingId;
	private int     routeTypeId;
	private String  routeKey;
	private int     matchTypeId;
	private int     matchValue;
    private int     institutionProfileId;


	//
	// Constructor: Accepts session resources object (access to system logger and Jdbc
	//              connection resources.
	//

	public Routing(SessionResourceKit srk)
	{
		super(srk);
	}
	//
	// Remote-Home inerface methods:
	//
	// The following methods have counterparts in Enterprise Bean classes - in an
	// Enterprise Entity Bean each methods name would be prefixed with "ejb"
	//

	public Routing create(int routingId, int routeTypeId, String routeKey, int matchTypeId, int matchValue)throws RemoteException, CreateException
	{
	  //FIXME : usage of institutionId needs reconsideration
	    
    String sql = "Insert into ROUTING (routingId, routeTypeId, routeKey, matchTypeId, matchValue) " +
                 "Values (" + routingId + ", '" + routeTypeId + "', '" + routeKey + "', " + matchTypeId + "', " + matchValue + ")";

		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException("Routing Entity - create() exception");

			logger.error(ce.getMessage());
			logger.error("create sql: " + sql);
			logger.error(e);

			throw ce;
		}

		setRoutingId(routingId);
		setRouteTypeId(routeTypeId);
		setRouteKey(routeKey);
    setMatchTypeId(matchTypeId);
    setMatchValue(matchValue);

		return this;
	}
	public void ebjLoad() throws RemoteException
	{
		// implementation not required for now!
	}
	public void ebjPassivate () throws RemoteException {;}
	public void ejbActivate() throws RemoteException{;}
	public void ejbStore() throws RemoteException
	{
		String sql = "Update ROUTING set " +
                  "routeTypeId = '" + getRouteTypeId() + ", " +
                  "routeKey = '" + getRouteKey() + ", " +
                  "matchTypeId = '" + getMatchTypeId() + ", " +
                  "matchValue = '" + getMatchValue() +

					        " where routingId = " + getRoutingId();

		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoteException re = new RemoteException("Routing Entity - ejbStore() exception");

			logger.error(re.getMessage());
			logger.error("ejbStore sql: " + sql);
			logger.error(e);

			throw re;
		}
	}

  public void remove() throws RemoteException, RemoveException
	{
		String sql = "Delete from ROUTING where routingId = " + getRoutingId();
		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoveException re = new RemoveException("Routing Entity - remove() exception");

			logger.error(re.getMessage());
			logger.error("remove sql: " + sql);
			logger.error(e);

			throw re;
		}
	}

	// finders:
  public Routing findByPrimaryKey(int routingId) throws RemoteException, FinderException
	{
    RoutingPK pk = new RoutingPK(routingId);
    return this.findByPrimaryKey(pk);
  }

  public Routing findByPrimaryKey(RoutingPK pk) throws RemoteException, FinderException
	{
		String sql = "Select * from ROUTING where routingId = " + pk.getRoutingId();

		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Routing Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Routing Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return this;
	}

  // Return all Routing records not in any particular order
	public Vector findByAll() throws RemoteException, FinderException
	{
		String sql = "Select * from ROUTING";
		boolean gotRecord = false;
    Vector v = new Vector();

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
        Routing ro = new Routing(srk);
				ro.setPropertiesFromQueryResult(key);
        v.add(ro);
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Routing Entity: @findByAll(), no entities found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Routing Entity: @findByAll() exception");
			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}
		return v;
	}

  // Get the list of Routing records ordered by the Routing Order
	public Vector getOrderedList() throws RemoteException, FinderException
	{
		String sql = "Select r.* from ROUTING r, RouteOrder o " +
          "where r.routetypeid = o.routetypeid " + "order by o.routeorderid ASC, r.routingid ASC";

		boolean gotRecord = false;
    Vector v = new Vector();

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
        Routing ro = new Routing(srk);
				ro.setPropertiesFromQueryResult(key);
        v.add(ro);
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Routing Entity: @getOrderedList(), no entities found";
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Routing Entity: @getOrderedList() exception");
			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}
		return v;
	}

  // Get the list of Routing records based on the RouteType
	public Vector findByRouteType(int routeTypeId) throws RemoteException, FinderException
	{
		String sql = "Select * from ROUTING " +
          "where routetypeid = " + routeTypeId;

		boolean gotRecord = false;
    Vector v = new Vector();

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
        Routing ro = new Routing(srk);
				ro.setPropertiesFromQueryResult(key);
        v.add(ro);
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Routing Entity: @findByRouteType(), no entities found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Routing Entity: @findByRouteType() exception");
			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}
		return v;
	}

	// setters/getters
  public int getRoutingId(){ return routingId; }
  public int getRouteTypeId(){ return routeTypeId; }
  public String getRouteKey(){ return routeKey; }
  //--> New Method support multiple RouteKeys
  //--> by Billy 03July2003
  public Vector getRouteKeys()
  {
    Vector results = new Vector();
    StringTokenizer rep = new StringTokenizer(routeKey, ",");
    try
    {
      while(rep.hasMoreTokens())
      {
        results.add(rep.nextToken());
      }
    }
    catch(NoSuchElementException ex)
    {
      ;
    }
    return results;
  }
  //===========================================
  public int getMatchTypeId(){ return matchTypeId; }
  public int getMatchValue(){ return matchValue; }
  public int getInstitutionProfileId() { return institutionProfileId; }
  
  public void setEntityContext(EntityContext ctx){ this.ctx = ctx; }
  public void setRoutingId(int routingId)	{	this.routingId = routingId; }
  public void setRouteTypeId(int routeTypeId)	{	this.routeTypeId = routeTypeId; }
  public void setRouteKey(String routeKey)	{	this.routeKey = routeKey; }
  public void setMatchTypeId(int matchTypeId)	{	this.matchTypeId = matchTypeId; }
  public void setMatchValue(int matchValue)	{	this.matchValue = matchValue; }
  public void setInstitutionProfileId(int institutionProfileId) 
  { this.institutionProfileId = institutionProfileId; }

	//
	// Private
	//
	private void setPropertiesFromQueryResult(int key) throws Exception {
        setRoutingId(jExec.getInt(key, "ROUTINGID"));
        setRouteTypeId(jExec.getInt(key, "ROUTETYPEID"));
        setRouteKey(jExec.getString(key, "ROUTEKEY"));
        setMatchTypeId(jExec.getInt(key, "MATCHTYPEID"));
        setMatchValue(jExec.getInt(key, "MATCHVALUE"));
        setInstitutionProfileId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

	public void unsetEntityContext()
	{
		ctx = null;
	}

	




}
