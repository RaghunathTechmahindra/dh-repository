package com.basis100.workflow.entity;

import java.util.ArrayList;
import java.util.Collection;

import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.WorkflowTaskProcessAssocPK;

/**
 * <p><h2>Title</h2>com.basis100.deal.entity.WorkflowTaskProcessAssoc</p>
 * <p><h2>Description</h2>A Workflow Process is a Java process the AME executes when handling an Auto-Task.<br>
 * The relationship between Workflow Tasks and Workflow Process is descripted in WorkflowTaskProcessAssoc table.<br>
 * This table is mapped to class WorkflowTaskProcessAssoc.  It's READ-ONLY.<br>
 * All associations should be stored in the database table by Administrator in advance.</p>
 * <p><h3>Copyright</h3>Copyright (c) 2004</p>
 * <p><h3>Company</h3><a href="http://www.filogix.com">FiLogix Inc.</a></p>
 * @author Neil Kong
 * @version 1.0
 */
public class WorkflowTaskProcessAssoc
    extends EntityBase
{

    private int taskId;
    private int orderNo;
    private String processName;
    private int institutionProfileId;
    private WorkflowTaskProcessAssocPK pk;

    /**
     * default constructor
     * @param srk SessionResourceKit
     * @throws com.basis100.entity.RemoteException
     */
    public WorkflowTaskProcessAssoc(SessionResourceKit srk) throws RemoteException
    {
        super(srk);
    }

    /**
     *
     * @param srk SessionResourceKit
     * @param taskId int
     * @param orderNo int
     * @throws com.basis100.entity.RemoteException
     * @throws com.basis100.entity.FinderException
     */
    public WorkflowTaskProcessAssoc(SessionResourceKit srk, int taskId, int orderNo) throws RemoteException,
        FinderException
    {
        super(srk);
        pk = new WorkflowTaskProcessAssocPK(taskId, orderNo);
        findByPrimaryKey(pk);
    }

    /**
     *
     * @param taskId int
     * @param orderNo int
     * @param processName String
     * @param pk WorkflowTaskProcessAssocPK
     */
    private WorkflowTaskProcessAssoc(int taskId, int orderNo, String processName, int institutionId, WorkflowTaskProcessAssocPK pk)
    {
        this.taskId = taskId;
        this.orderNo = orderNo;
        this.processName = processName;
        this.institutionProfileId = institutionId;
        this.pk = pk;
    }

    /**
     *
     * @param pk WorkflowTaskProcessAssocPK
     * @throws FinderException
     * @return WorkflowTaskProcessAssoc
     */
    public WorkflowTaskProcessAssoc findByPrimaryKey(WorkflowTaskProcessAssocPK pk) throws FinderException
    {
        this.taskId = pk.getTaskId();
        this.orderNo = pk.getOrderNo();
        StringBuffer sqlBuff = new StringBuffer("SELECT * FROM WORKFLOWTASKPROCESSASSOC WHERE TASKID = ");
        sqlBuff.append(taskId);
        sqlBuff.append(" AND ORDERNO = ");
        sqlBuff.append(orderNo);
        sqlBuff.append(" ORDER BY ORDERNO");
        String sql = sqlBuff.toString();

        try
        {
            int key = jExec.execute(sql);
            if(jExec.next(key))
            {
                this.processName = jExec.getString(key, "PROCESSNAME");
                this.institutionProfileId = jExec.getInt(key, "INSTITUTIONPROFILEID");
            }
            else
            {
                String msg = "@WorkflowTaskProcessAssoc::findByPrimaryKey(), taskId = " + taskId + " orderNo = " +
                    orderNo + ", Entity not found!";
                logger.error(msg);
                throw new FinderException(msg);
            }
            jExec.closeData(key);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            FinderException fe = new FinderException("@WorkflowTaskProcessAssoc::findByPrimaryKey(): exception");
            logger.error(fe.getMessage());
            logger.error("@WorkflowTaskProcessAssoc::findByPrimaryKey(): Finder SQL: " + sql);
            logger.error(e);
            throw fe;
        }
        this.pk = pk;
        return this;
    }

    /**
     *
     * @param taskId int
     * @throws FinderException
     * @return ArrayList
     */
    public Collection<WorkflowTaskProcessAssoc> findByTaskId(int taskId) throws FinderException
    {
        StringBuffer sqlBuff = new StringBuffer("SELECT * FROM WORKFLOWTASKPROCESSASSOC WHERE TASKID = ");
        sqlBuff.append(taskId);
        sqlBuff.append(" ORDER BY ORDERNO");
        String sql = sqlBuff.toString();

        Collection<WorkflowTaskProcessAssoc> recordsFound = new ArrayList<WorkflowTaskProcessAssoc>();
        WorkflowTaskProcessAssoc wtpa = null;

        try
        {
            int key = jExec.execute(sql);
            while(jExec.next(key))
            {
                int orderNo = jExec.getInt(key, "ORDERNO");
                String processName = jExec.getString(key, "PROCESSNAME");
                int institutionId = jExec.getInt(key, "INSTITUTIONPROFILEID");
                WorkflowTaskProcessAssocPK pk = new WorkflowTaskProcessAssocPK(taskId, orderNo);
                wtpa = new WorkflowTaskProcessAssoc(taskId, orderNo, processName, institutionId, pk);
                recordsFound.add(wtpa);
            }

            if((recordsFound == null) || (recordsFound.size() == 0))
            {
                String msg = "@WorkflowTaskProcessAssoc::findByTaskId(), taskId = " + taskId + " orderNo = " + orderNo +
                    ", Entity not found!";
                logger.error(msg);
                throw new FinderException(msg);
            }
            jExec.closeData(key);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            FinderException fe = new FinderException("@WorkflowTaskProcessAssoc::findByTaskId(): exception");
            logger.error(fe.getMessage());
            logger.error("@WorkflowTaskProcessAssoc::findByTaskId(): Finder SQL: " + sql);
            logger.error(e);
            throw fe;
        }

        return recordsFound;
    }

    /**
     * Mohan added for testing purpose.
     */
    public WorkflowTaskProcessAssoc findByTaskName(String processName, int institutionProfileId) throws FinderException {
        this.processName = processName;
        this.institutionProfileId = institutionProfileId;
        StringBuffer sqlBuff = new StringBuffer("SELECT * FROM WORKFLOWTASKPROCESSASSOC WHERE PROCESSNAME = '");
        sqlBuff.append(this.processName);
        sqlBuff.append("' AND INSTITUTIONPROFILEID = ");
        sqlBuff.append(this.institutionProfileId);
        String sql = sqlBuff.toString();
        
        try {
            int key = jExec.execute(sql);
            if(jExec.next(key)) {
                this.taskId = jExec.getInt(key, "TASKID");
                this.orderNo = jExec.getInt(key, "ORDERNO");
            } else {
                String msg = "@WorkflowTaskProcessAssoc::findByPrimaryKey(), taskId = " + taskId + " orderNo = " +
                    orderNo + ", Entity not found!";
                logger.error(msg);
                throw new FinderException(msg);
            }
            jExec.closeData(key);
        } catch(Exception e) {
            e.printStackTrace();
            FinderException fe = new FinderException("@WorkflowTaskProcessAssoc::findByPrimaryKey(): exception");
            logger.error(fe.getMessage());
            logger.error("@WorkflowTaskProcessAssoc::findByPrimaryKey(): Finder SQL: " + sql);
            logger.error(e);
            throw fe;
        }
        return this;
    }

    /**
     *
     * @return int
     */
    public int getTaskId()
    {
        return taskId;
    }

    /**
     *
     * @return int
     */
    public int getOrderNo()
    {
        return orderNo;
    }

    /**
     *
     * @return String
     */
    public String getProcessName()
    {
        return processName;
    }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    
    /**
     *
     * @throws CloneNotSupportedException
     * @return WorkflowTaskProcessAssoc
     */
    public WorkflowTaskProcessAssoc deepCopy() throws CloneNotSupportedException
    {
        return(WorkflowTaskProcessAssoc)this.clone();
    }




}
