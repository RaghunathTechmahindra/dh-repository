package com.basis100.workflow.entity;


import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.TaskBeanPK;

/*
 * TaskBean:
 *
 * Enterprise bean entity representing a record MOS datbase table 'assignedTasksWorkQueue'.
 *
 **/

public class Task extends EntityBase
{
	EntityContext ctx;

	// properties

	private int taskId;
	private String taskName;
	private String taskLabel;
	private String baseTask;  // Y/N
	private int basedOnTaskId;
	private int followUpTaskId;
	private int linkTaskId;
	private int priorityId;
	private long duration;
	private long warningTime;
	private long reissueAlarmDuration;
  private long reminderTime;
  private String criticalFlag;
  private String sentinelFlag;
  private int sentinelOn;
  private int visibleFlag;
  private int sentinelTo;
  private int institutionId;
  

/*
 *
 * Constructor: Accepts session resources object (access to system logger and Jdbc
 *              connection resources.
 *
 **/

	public Task(SessionResourceKit srk)
	{
		super(srk);
	}
/*
 *
 * Remote-Home inerface methods:
 *
 * The following methods have counterparts in Enterprise Bean classes - in an
 * Enterprise Entity Bean each methods name would be prefixed with "ejb"
 *
 **/

	public Task create(int taskId, String taskName,  String taskLabel, String baseTask,
					   int basedOnTaskId, int followUpTaskId, int linkTaskId, int priorityId,
					   long duration, long warningTime, long reissueAlarmDuration, long reminderTime,
					   String criticalFlag, String sentinelFlag, int sentinelOn, int visibleFlag, int sentinelTo, int institutionId)
					   throws RemoteException, CreateException
	{
	    //FIXME : the usage of institutionId needs reconsideration
		String sql = "Insert into Task (" +
					  "taskId, taskName,  taskLabel, baseTask, basedOnTaskId, followUpTaskId, " +
					  "linkTaskId, priorityId, duration, warningTime, reissueAlarmDuration, " +
					  "reminderTime, criticalFlag, sentinelFlag, sentinelOn, " +
					  "visibleFlag, sentinelTo,institutionProfileId) " +
					  " Values (" +
					  taskId                +  ", '" +
					  taskName              + "', '" +
					  taskLabel             + "', '" +
					  baseTask              + "', " +
					  basedOnTaskId         +  ", " +
					  followUpTaskId        +  ", " +
					  linkTaskId            +  ", " +
					  priorityId            +  ", " +
					  duration              +  ", " +
					  warningTime           +  ", " +
					  reissueAlarmDuration  +  ", " +
					  reminderTime          +  ", '" +
					  criticalFlag          +  "', '" +
					  sentinelFlag          +  "', " +
					  sentinelOn            +  ", " +
					  visibleFlag           +  ", " +
                      sentinelTo            +  ", " +
                      institutionId +   ") ";

		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			CreateException ce = new CreateException("Task Entity - create() exception");

			logger.error(ce.getMessage());
			logger.error("create sql: " + sql);
			logger.error(e);

			throw ce;
		}

		setTaskId(taskId);
		setTaskName(taskName);
		setTaskLabel(taskLabel);
		setBaseTask(baseTask);
		setBasedOnTaskId(basedOnTaskId);
		setFollowUpTaskId(followUpTaskId);
		setLinkTaskId(linkTaskId);
		setPriorityId(priorityId);
		setDuration(duration);
		setWarningTime(warningTime);
		setReissueAlarmDuration(reissueAlarmDuration);
		setReminderTime(reminderTime);
		setCriticalFlag(criticalFlag);
		setSentinelFlag(sentinelFlag);
		setSentinelOn(sentinelOn);
		setVisibleFlag(visibleFlag);
		setSentinelTo(sentinelTo);
		setInstitutionId(institutionId);

		return this;
	}

	public void ebjLoad() throws RemoteException
	{
		// implementation not required for now!
	}



	public void ebjPassivate () throws RemoteException {;}
	public void ejbActivate() throws RemoteException{;}
	public void ejbStore() throws RemoteException
	{
		String sql = "Update TASK set "  +
					 "taskId    = "            + getTaskId() + ", " +
					 "taskName  = '"           + getTaskName() + "', " +
					 "taskLabel = '"           + getTaskLabel() + "', " +
					 "baseTask  = '"           + getBaseTask() + "', " +
					 "basedOnTaskId = "        + getBasedOnTaskId() + ", " +
					 "followUpTaskId = "       + getFollowUpTaskId() + ", " +
					 "linkTaskId = "           + getLinkTaskId() + ", " +
					 "priorityId = "             + getPriorityId() + ", " +
					 "duration = "             + getDuration() + ", " +
					 "warningTime = "          + getWarningTime() + ", " +
					 "reissueAlarmDuration = " + getReissueAlarmDuration() + ", " +
					 "reminderTime = "         + getReminderTime() +
					 "criticalFlag = '"        + getCriticalFlag() + "', " +
					 "sentinelFlag = '"        + getSentinelFlag() + "', " +
					 "sentinelOn = "           + getSentinelOn() +  ", " +
					 "visibleFlag = "          + getVisibleFlag() + ", " +
					 "sentinelTo = "           + getSentinelTo() +
					 " Where taskId = "        + getTaskId();

		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoteException re = new RemoteException("Task Entity - ejbStore() exception");

			logger.error(re.getMessage());
			logger.error("ejbStore sql: " + sql);
			logger.error(e);

			throw re;
		}
	}

	public void remove() throws RemoteException, RemoveException
	{
		String sql = "Delete from TASK where taskId = " + getTaskId();
		try
		{
			jExec.executeUpdate(sql);
		}
		catch (Exception e)
		{
			RemoveException re = new RemoveException("Task Entity - remove() exception");

			logger.error(re.getMessage());
			logger.error("remove sql: " + sql);
			logger.error(e);

			throw re;
		}
	}


/*
 * finders:
 *
 **/

	public Task findByPrimaryKey(TaskBeanPK pk) throws RemoteException, FinderException
	{
		String sql = "Select * from TASK where taskId = " + pk.getTaskId();

		boolean gotRecord = false;

		try
		{
			int key = jExec.execute(sql);

			for (; jExec.next(key); )
			{
				gotRecord = true;
				setPropertiesFromQueryResult(key);
				break; // can only be one record
			}

			jExec.closeData(key);

			if (gotRecord == false)
			{
				String msg = "Task Entity: @findByPrimaryKey(), key=" + pk + ", entity not found";
				logger.error(msg);
				throw new FinderException(msg);
			}
		}
		catch (Exception e)
		{
			FinderException fe = new FinderException("Task Entity - findByPrimaryKey() exception");

			logger.error(fe.getMessage());
			logger.error("finder sql: " + sql);
			logger.error(e);

			throw fe;
		}

		return this;
	}

/*
 *
 * Methods implemented in Enterprise Bean Instances that have no remote object
 * counterparts - i.e. EJB container callback methods, and context related methods.
 *
 **/

	public void setEntityContext(EntityContext ctx)
	{
		this.ctx = ctx;
	}

	public void unsetEntityContext()
	{
		ctx = null;
	}


/*
 * setters/getters
 *
 **/

	public int getTaskId()
	{
		return taskId;
	}
	public String getTaskLabel()
	{
		return taskLabel;
	}
	public String getTaskName()
	{
		return taskName;
	}
	public long getWarningTime()
	{
		return warningTime;
	}
	public int getBasedOnTaskId()
	{
		return basedOnTaskId;
	}
	public String getBaseTask()
	{
		return baseTask;
	}
	public long getDuration()
	{
		return duration;
	}
	public int getFollowUpTaskId()
	{
		return followUpTaskId;
	}
	public int getLinkTaskId()
	{
		return linkTaskId;
	}
	public int getPriorityId()
	{
		return priorityId;
	}
	public long getReissueAlarmDuration()
	{
		return reissueAlarmDuration;
	}
	public long getReminderTime()
	{
		return reminderTime;
	}
	public String getCriticalFlag()
	{
		return criticalFlag;
	}
	public String getSentinelFlag()
	{
		return sentinelFlag;
	}
	public int getSentinelOn()
	{
		return sentinelOn;
	}
	public int getVisibleFlag()
	{
		return visibleFlag;
	}
	public int getSentinelTo()
	{
		return sentinelTo;
	}


  // --- //

	public void setBasedOnTaskId(int basedOnTaskId)
	{
		this.basedOnTaskId = basedOnTaskId;
	}
	public void setBaseTask(String baseTask)
	{
		this.baseTask = baseTask;
	}
	public void setDuration(long duration)
	{
		this.duration = duration;
	}
	public void setFollowUpTaskId(int followUpTaskId)
	{
		this.followUpTaskId = followUpTaskId;
	}
	public void setLinkTaskId(int linkTaskId)
	{
		this.linkTaskId = linkTaskId;
	}
	public void setPriorityId(int priorityId)
	{
		this.priorityId = priorityId;
	}
	public void setReissueAlarmDuration(long reissueAlarmDuration)
	{
		this.reissueAlarmDuration = reissueAlarmDuration;
	}
	public void setTaskId(int taskId)
	{
		this.taskId = taskId;
	}
	public void setTaskLabel(String taskLabel)
	{
		this.taskLabel = taskLabel;
	}
	public void setTaskName(String taskName)
	{
		this.taskName = taskName;
	}
	public void setWarningTime(long warningTime)
	{
		this.warningTime = warningTime;
	}
	public void setReminderTime(long reminderTime)
	{
		this.reminderTime = reminderTime;
	}
	public void setCriticalFlag(String criticalFlag)
	{
		this.criticalFlag = criticalFlag;
	}
	public void setSentinelFlag(String sentinelFlag)
	{
		this.sentinelFlag = sentinelFlag;
	}
	public void setSentinelOn(int sentinelOn)
	{
		this.sentinelOn = sentinelOn;
	}
	public void setVisibleFlag(int visibleFlag)
	{
		this.visibleFlag = visibleFlag;
	}
	public void setSentinelTo(int sentinelTo)
	{
		this.sentinelTo = sentinelTo;
	}

	//
	// Private
	//

	private void setPropertiesFromQueryResult(int key) throws Exception {

        setTaskId(jExec.getInt(key, "TASKID"));
        setTaskName(jExec.getString(key, "TASKNAME"));
        setTaskLabel(jExec.getString(key, "TASKLABEL"));
        setBaseTask(jExec.getString(key, "BASETASK"));
        setBasedOnTaskId(jExec.getInt(key, "BASEDONTASKID"));
        setFollowUpTaskId(jExec.getInt(key, "FOLLOWUPTASKID"));
        setLinkTaskId(jExec.getInt(key, "LINKTASKID"));
        setPriorityId(jExec.getInt(key, "PRIORITYID"));
        setDuration(jExec.getInt(key, "DURATION"));
        setWarningTime(jExec.getInt(key, "WARNINGTIME"));
        setReissueAlarmDuration(jExec.getInt(key, "REISSUEALARMDURATION"));
        setReminderTime(jExec.getInt(key, "REMINDERTIME"));
        setCriticalFlag(jExec.getString(key, "CRITICALFLAG"));
        setSentinelFlag(jExec.getString(key, "SENTINELFLAG"));
        setSentinelOn(jExec.getInt(key, "SENTINELON"));
        setVisibleFlag(jExec.getInt(key, "VISIBLEFLAG"));
        setSentinelTo(jExec.getInt(key, "SENTINELTO"));
        setInstitutionId(jExec.getInt(key, "INSTITUTIONPROFILEID"));
    }

  /**
     * @return the institutionId
     */
  public int getInstitutionId() {
    return institutionId;
  }

  /**
     * @param institutionId
     *            the institutionId to set
     */
  public void setInstitutionId(int institutionId) {
    this.institutionId = institutionId;
  }
}
