package com.basis100.workflow.entity;

import java.util.Vector;

import com.basis100.entity.CreateException;
import com.basis100.entity.EntityBase;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.WorkflowTaskBeanPK;

/*
 * WorkflowTaskBean:
 *
 * Enterprise bean entity representing a record MOS datbase table 'workflowTask'. In one sense
 * a workflow task is a copy of a task definition (from table task) that has been associated
 * with a particular workflow. The properties of the task defineition 'int the workflow' can
 * be modified (customized) without affecting the task definition (static). However, the
 * following properties cannot be changed (there are not setter methods):
 *
 *    taskId, taskName (but taskLable can be changed), baseTask (a 'base task' cannot exist
 *    a workflow in any event!), basedOnTaskId
 *
 * Additionally, the workflowId cannot be changed (i.e. the association to a particular
 * workflow cannot be changed).
 *
 * Programmer Note:
 *
 * When a workflow task entity is created a recorded is inserted in the workflowTask table.
 * The unique field workflowTaskID is populated with the next value of sequence
 * WORKFLOWTASKSEQ. The sequence must exist in the database, e.g.:
 *
 *                  create sequence WORKFLOWTASKSEQ;
 **/

public class WorkflowTask
  extends EntityBase
{

  EntityContext ctx;

  // properties

  private int workflowTaskId;
  private int workflowId;
  private int workflowStage;

  private int taskId;
  private String taskName;
  private String taskLabel;
  private String baseTask;
  private int basedOnTaskId;
  private int followUpTaskId;
  private int linkTaskId;
  private int priorityId;
  private long duration;
  private long warningTime;
  private long reissueAlarmDuration;
  private long reminderTime;
  private String criticalFlag;
  private String sentinelFlag;
  private int sentinelOn;
  private int visibleFlag;
  private int sentinelTo;
  private int autoTaskRetryTime; //--> Cervus II : Section 4.2.1.  By Neil : 09/30/2004

  private int institutionProfileId;

  private String bypassWFRegression;

  /*
   * Constructor: Accepts session resources object (access to system logger and Jdbc
   *              connection resources.
   **/
  public WorkflowTask(SessionResourceKit srk)
  {
    super(srk);
  }

  /*
   * Remote-Home inerface methods:
   *
   * The following methods have counterparts in Enterprise Bean classes - in an
   * Enterprise Entity Bean each methods name would be prefixed with "ejb"
   *
   * It's NEVER been called.
   **/

  public WorkflowTask create(int workflowId, int workflowStage, Task dTask) throws
    RemoteException, CreateException
  {
    String sqlErr = null;

    /*******************************************************************************
       //--> Comment out by Neil : 09/30/2004

      String sql = "Insert into WorkflowTask(" +
       "workflowTaskId, workflowId, workflowStage" +
     "taskId, taskName,  taskLabel, baseTask, basedOnTaskId, followUpTaskId, " +
     "linkTaskId, priorityId, duration, warningTime, reissueAlarmDuration, " +
       "reminderTime, criticalFlag, sentinelFlag, sentinelOn, visibleFlag, sentinelTo) " +
       "Values (WORKFLOWTASKSEQ.NEXTVAL, " +
       workflowId + ",  " +
       workflowStage + ",  " +
       dTask.getTaskId() + ", '" +
       dTask.getTaskName() + "', '" +
       dTask.getTaskLabel() + "', '" +
       dTask.getBaseTask() + "', " +
       dTask.getBasedOnTaskId() + ", " +
       dTask.getFollowUpTaskId() + ", " +
       dTask.getLinkTaskId() + ", " +
       dTask.getPriorityId() + ", " +
       dTask.getDuration() + ", " +
       dTask.getWarningTime() + ", " +
       dTask.getReissueAlarmDuration() + ", " +
       dTask.getReminderTime() + ", '" +
       dTask.getCriticalFlag() + "', '" +
       dTask.getSentinelFlag() + "', " +
       dTask.getSentinelOn() + ", " +
       dTask.getVisibleFlag() + ", " +
       dTask.getSentinelTo() + ")";
     *******************************************************************************/

    //--> Cervus II : 4.2.1
    //--> By Neil : 09/30/2004
    //--> Because the method will never be invoked, it stays unchanged.
    //--> Just the same as the code commentted out above.
    StringBuffer sqlBuf = new StringBuffer("Insert into WorkflowTask(");
    sqlBuf.append("workflowTaskId, workflowId, workflowStage");
    sqlBuf.append(
      "taskId, taskName,  taskLabel, baseTask, basedOnTaskId, followUpTaskId, ");
    sqlBuf.append(
      "linkTaskId, priorityId, duration, warningTime, reissueAlarmDuration, ");
    sqlBuf.append("reminderTime, criticalFlag, sentinelFlag, sentinelOn, visibleFlag, sentinelTo, autoTaskRetryTime) ");
    sqlBuf.append("Values (WORKFLOWTASKSEQ.NEXTVAL, ");
    sqlBuf.append(workflowId);
    sqlBuf.append(", ");
    sqlBuf.append(workflowStage);
    sqlBuf.append(",  ");
    sqlBuf.append(dTask.getTaskId());
    sqlBuf.append(", '");
    sqlBuf.append(dTask.getTaskName());
    sqlBuf.append("', '");
    sqlBuf.append(dTask.getTaskLabel());
    sqlBuf.append("', '");
    sqlBuf.append(dTask.getBaseTask());
    sqlBuf.append("', ");
    sqlBuf.append(dTask.getBasedOnTaskId());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getFollowUpTaskId());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getLinkTaskId());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getPriorityId());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getDuration());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getWarningTime());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getReissueAlarmDuration());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getReminderTime());
    sqlBuf.append(", '");
    sqlBuf.append(dTask.getCriticalFlag());
    sqlBuf.append("', '");
    sqlBuf.append(dTask.getSentinelFlag());
    sqlBuf.append("', ");
    sqlBuf.append(dTask.getSentinelOn());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getVisibleFlag());
    sqlBuf.append(", ");
    sqlBuf.append(dTask.getSentinelTo());
//		sqlBuf.append(", ");
//		sqlBuf.append(// how to get autoTaskRetryTime);  //--?
    sqlBuf.append(")");
    String sql = sqlBuf.toString();
    logger.debug("=====> @WorkflowTask : create() --> sql = " + sql);

    ////////////////////////////////////////////////////////////////////////

    String wfIdSql =
      "select workflowTaskId from workflowTask where workflowId = " +
      workflowId + " AND taskId = " +
      dTask.getTaskId();

    boolean gotRecord = false;

    try
    {
      jExec.executeUpdate(sqlErr = sql);

      int key = jExec.execute(sqlErr = wfIdSql);

      for(; jExec.next(key); )
      {
        gotRecord = true;

        workflowTaskId = jExec.getInt(key, 1);
        break; // can only be one record
      }

      jExec.closeData(key);
    }
    catch(Exception e)
    {
      CreateException ce = new CreateException(
        "WorkflowTask Entity - create() exception");

      logger.error(ce.getMessage());
      logger.error("create sql: " + sqlErr);
      logger.error(e);

      throw ce;
    }

    if(gotRecord == false)
    {
      CreateException ce = new CreateException(
        "WorkflowTask Entity - create() exception:: did not obtain workflowTask ID");

      logger.error(ce.getMessage());
      throw ce;
    }

    this.workflowId = workflowId;
    this.workflowStage = workflowStage;

    this.taskId = dTask.getTaskId();
    this.taskName = dTask.getTaskName();
    this.taskLabel = dTask.getTaskLabel();
    this.baseTask = dTask.getBaseTask();
    this.basedOnTaskId = dTask.getBasedOnTaskId();
    this.followUpTaskId = dTask.getFollowUpTaskId();
    this.linkTaskId = dTask.getLinkTaskId();
    this.priorityId = dTask.getPriorityId();
    this.duration = dTask.getDuration();
    this.warningTime = dTask.getWarningTime();
    this.reissueAlarmDuration = dTask.getReissueAlarmDuration();
    this.reminderTime = dTask.getReminderTime();
    this.criticalFlag = dTask.getCriticalFlag();
    this.sentinelFlag = dTask.getSentinelFlag();
    this.sentinelOn = dTask.getSentinelOn();
    this.visibleFlag = dTask.getVisibleFlag();
    this.sentinelTo = dTask.getSentinelTo();

    return this;
  }

  /**
   *
   * @throws RemoteException
   * @throws RemoveException
   */
  public void remove() throws RemoteException, RemoveException
  {
    /***********************************************************************
       String sql = "Delete from WORKFLOWTASK where workflowTaskId = " +
     workflowTaskId;
     **********************************************************************/
    StringBuffer sqlBuff = new StringBuffer(
      "DELETE FROM WORKFLOWTASK WHERE WORKFLOWTASKID = ");
    sqlBuff.append(workflowTaskId);
    String sql = sqlBuff.toString();
    logger.debug("=====> @WorkflowTask : remove() : sql = " + sql);
    try
    {
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      RemoveException re = new RemoveException(
        "WorkflowTask Entity - remove() exception");

      logger.error(re.getMessage());
      logger.error("remove sql: " + sql);
      logger.error(e);

      throw re;
    }
  }

  public void ebjLoad() throws RemoteException
  {
    // implementation not required for now!
  }

  public void ebjPassivate() throws RemoteException
  {
    ;
  }

  public void ejbActivate() throws RemoteException
  {
    ;
  }

  /**
   *
   * @throws RemoteException
   */
  public void ejbStore() throws RemoteException
  {

    /*******************************************************************************
      String sql = "Update WORKFLOWTASK set " +
       "taskLabel = '" + getTaskLabel() + "', " +
       "followUpTaskId = " + getFollowUpTaskId() + ", " +
       "linkTaskId = " + getLinkTaskId() + ", " +
       "priorityId = " + getPriorityId() + ", " +
       "duration = " + getDuration() + ", " +
       "warningTime = " + getWarningTime() + ", " +
       "reissueAlarmDuration = " + getReissueAlarmDuration() + ", "
       "reminderTime = " + getReminderTime() + ", " +
       "criticalFlag = '" + getCriticalFlag() + "', " +
       "sentinelFlag = '" + getSentinelFlag() + "', " +
       "sentinelOn = " + getSentinelOn() + ", " +
       "visibleFlag = " + getVisibleFlag() + ", " +
       "sentinelTo = " + getSentinelTo() +

       " Where workflowTaskId = " + workflowTaskId;
     *******************************************************************************/

    //--> Cervus II : Section 4.2.1
    //--> By Neil : 09/30/2004
    //--> Added new field AUTOTASKRETRYTIME

    StringBuffer sqlBuff = new StringBuffer("Update WORKFLOWTASK set ");

    sqlBuff.append("taskLabel = '");
    sqlBuff.append(getTaskLabel());
    sqlBuff.append("', ");

    sqlBuff.append("followUpTaskId = ");
    sqlBuff.append(getFollowUpTaskId());
    sqlBuff.append(", ");

    sqlBuff.append("linkTaskId = ");
    sqlBuff.append(getLinkTaskId());
    sqlBuff.append(", ");

    sqlBuff.append("priorityId = ");
    sqlBuff.append(getPriorityId());
    sqlBuff.append(", ");

    sqlBuff.append("duration = ");
    sqlBuff.append(getDuration());
    sqlBuff.append(", ");

    sqlBuff.append("warningTime = ");
    sqlBuff.append(getWarningTime());
    sqlBuff.append(", ");

    sqlBuff.append("reissueAlarmDuration = ");
    sqlBuff.append(getReissueAlarmDuration());
    sqlBuff.append(", ");

    sqlBuff.append("reminderTime = ");
    sqlBuff.append(getReminderTime());
    sqlBuff.append(", ");

    sqlBuff.append("criticalFlag = '");
    sqlBuff.append(getCriticalFlag());
    sqlBuff.append("', ");

    sqlBuff.append("sentinelFlag = '");
    sqlBuff.append(getSentinelFlag());
    sqlBuff.append("', ");

    sqlBuff.append("sentinelOn = '");
    sqlBuff.append(getSentinelOn());
    sqlBuff.append("', ");

    sqlBuff.append("visibleFlag = '");
    sqlBuff.append(getVisibleFlag());
    sqlBuff.append("', ");

    sqlBuff.append("sentinelTo = '");
    sqlBuff.append(getSentinelTo());
    sqlBuff.append("', ");

    sqlBuff.append("autoTaskRetryTime = '");
    sqlBuff.append(getAutoTaskRetryTime());

    sqlBuff.append("' Where workflowTaskId = ");
    sqlBuff.append(workflowTaskId);

    String sql = sqlBuff.toString();

    logger.debug("=====> @WorkflowTask : ejbStore() : sql = " + sql);

    ////////////////////////////////////////////////////////////////////////////

    try
    {
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      RemoteException re = new RemoteException(
        "WorkflowTask Entity - ejbStore() exception");

      logger.error(re.getMessage());
      logger.error("ejbStore sql: " + sql);
      logger.error(e);

      throw re;
    }
  }

  /*
   * finders:
   *
   **/

  public WorkflowTask findByPrimaryKey(WorkflowTaskBeanPK pk) throws
    RemoteException, FinderException
  {

    /*******************************************************************************
       //--> Commentted out by Neil : 09/30/2004

      String sql = "Select " +
       "workflowTaskId, workflowId, workflowStage, " +
     "taskId, taskName,  taskLabel, baseTask, basedOnTaskId, followUpTaskId, " +
     "linkTaskId, priorityId, duration, warningTime, reissueAlarmDuration, " +
       "reminderTime, criticalFlag, sentinelFlag, sentinelOn, visibleFlag, sentinelTo " +
       "from WORKFLOWTASK where workflowId = " + pk.getWorkflowId() + " AND taskId = " + pk.getTaskId();
     *******************************************************************************/

    //--> Cervus II : 4.2.1
    //--> By Neil : 09/30/2004
    //--> Added new field autoTaskRetryTime
    StringBuffer sqlBuff = new StringBuffer(
      "SELECT * FROM WORKFLOWTASK WHERE WORKFLOWID = ");
    sqlBuff.append(pk.getWorkflowId());
    sqlBuff.append(" AND TASKID = ");
    sqlBuff.append(pk.getTaskId());

    String sql = sqlBuff.toString();

    logger.debug("=====> @WorkflowTask : findByPrimaryKey() : sql = " + sql);

    ////////////////////////////////////////////////////////////////////////////

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);
      for(; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
        String msg = "WorkflowTask Entity: @findByPrimaryKey(), key=" +
          pk + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException(
        "WorkflowTask Entity - findByPrimaryKey() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }

    logger.debug(
      "=====> @WorkflowTask : findByPrimaryKey() : workflowTask found is : ... ...");
    logger.debug("-----> autoTaskRetryTime\t= " + this.getAutoTaskRetryTime());
    logger.debug("-----> basedOnTaskId\t= " + this.getBasedOnTaskId());
    logger.debug("-----> baseTask\t= " + this.getBaseTask());
    logger.debug("-----> criticalFlag" + this.getCriticalFlag());
    logger.debug("-----> duration\t= " + this.getDuration());
    logger.debug("-----> followUpTaskId" + this.getFollowUpTaskId());
    logger.debug("-----> linkTaskId\t= " + this.getLinkTaskId());
    logger.debug("-----> priorityId\t= " + this.getPriorityId());
    logger.debug("-----> reissueAlarmDuration\t= " +
      this.getReissueAlarmDuration());
    logger.debug("-----> reminderTime\t= " + this.getReminderTime());
    logger.debug("-----> sentinelFlag\t= " + this.getSentinelFlag());
    logger.debug("-----> sentinelOn\t= " + this.getSentinelOn());
    logger.debug("-----> sentinelTo\t= " + this.getSentinelTo());
    logger.debug("-----> taskId\t= " + this.getTaskId());
    logger.debug("-----> taskLabel\t= " + this.getTaskLabel());
    logger.debug("-----> taskName\t= " + this.getTaskName());
    logger.debug("-----> visibleFlag\t= " + this.getVisibleFlag());
    logger.debug("-----> warningTime\t= " + this.getWarningTime());
    logger.debug("-----> workflowId\t= " + this.getWorkflowId());
    logger.debug("-----> workflowStage\t= " + this.getWorkflowStage());
    logger.debug("-----> workflowTaskId\t= " + this.getWorkflowTaskId());

    return this;
  }

  /**
   * findByWorkflowStage
   * @param workflowId int
   * @param stageNum int
   * @throws RemoteException
   * @throws FinderException
   * @return Vector
   */
  public Vector findByWorkflowStage(int workflowId, int stageNum) throws
    RemoteException, FinderException
  {
    /*******************************************************************************
      String sql = "Select " +
       "workflowTaskId, workflowId, workflowStage, " +
     "taskId, taskName,  taskLabel, baseTask, basedOnTaskId, followUpTaskId, " +
     "linkTaskId, priorityId, duration, warningTime, reissueAlarmDuration, " +
       "reminderTime, criticalFlag, sentinelFlag, sentinelOn, visibleFlag, sentinelTo " +
     "from WORKFLOWTASK where workflowId = " + workflowId + " " +
       "AND workflowStage = " + stageNum;
     *******************************************************************************/
    //--> Cervus II : 4.2.1
    //--> By Neil : 09/30/2004
    //--> Added new field autoTaskRetryTime
    StringBuffer sqlBuff = new StringBuffer(
      "SELECT * FROM WORKFLOWTASK WHERE WORKFLOWID = ");
    sqlBuff.append(workflowId);
    sqlBuff.append(" AND WORKFLOWSTAGE = ");
    sqlBuff.append(stageNum);

    String sql = sqlBuff.toString();

    logger.debug("=====> @WorkflowTask : findByWorkflowStage() : sql = " +
      sql);

    ////////////////////////////////////////////////////////////////////////////

    Vector tasks = new Vector();
    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        WorkflowTask wt = new WorkflowTask(srk);

        wt.setPropertiesFromQueryResult(key);

        tasks.addElement(wt);
      }

      jExec.closeData(key);
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException(
        "WorkflowTask Entity - findByWorkflowStage() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }

    return tasks;
  }

  ////////////////////////////////////////////////////////////////////////////
  //
  // getters/setters
  //
  ////////////////////////////////////////////////////////////////////////////

  public int getBasedOnTaskId()
  {
    return basedOnTaskId;
  }

  public String getBaseTask()
  {
    return baseTask;
  }

  public long getDuration()
  {
    return duration;
  }

  public int getFollowUpTaskId()
  {
    return followUpTaskId;
  }

  public int getLinkTaskId()
  {
    return linkTaskId;
  }

  public int getPriorityId()
  {
    return priorityId;
  }

  public long getReissueAlarmDuration()
  {
    return reissueAlarmDuration;
  }

  public int getTaskId()
  {
    return taskId;
  }

  public String getTaskLabel()
  {
    return taskLabel;
  }

  public String getTaskName()
  {
    return taskName;
  }

  public long getWarningTime()
  {
    return warningTime;
  }

  public int getWorkflowId()
  {
    return workflowId;
  }

  public int getWorkflowStage()
  {
    return workflowStage;
  }

  public int getWorkflowTaskId()
  {
    return workflowTaskId;
  }

  public long getReminderTime()
  {
    return reminderTime;
  }

  public String getCriticalFlag()
  {
    return criticalFlag;
  }

  public String getSentinelFlag()
  {
    return sentinelFlag;
  }

  public int getSentinelOn()
  {
    return sentinelOn;
  }

  public int getVisibleFlag()
  {
    return visibleFlag;
  }

  public int getSentinelTo()
  {
    return sentinelTo;
  }

  //--> Cervus II : 4.2.1
  //--> By Neil : 09/30/2004
  public int getAutoTaskRetryTime()
  {
    return autoTaskRetryTime;
  }

  public void setWorkflowStage(int workflowStage)
  {
    this.workflowStage = workflowStage;
  }

  public void setDuration(long duration)
  {
    this.duration = duration;
  }

  public void setFollowUpTaskId(int followUpTaskId)
  {
    this.followUpTaskId = followUpTaskId;
  }

  public void setLinkTaskId(int linkTaskId)
  {
    this.linkTaskId = linkTaskId;
  }

  public void setPriorityId(int priorityId)
  {
    this.priorityId = priorityId;
  }

  public void setReissueAlarmDuration(long reissueAlarmDuration)
  {
    this.reissueAlarmDuration = reissueAlarmDuration;
  }

  public void setTaskLabel(String taskLabel)
  {
    this.taskLabel = taskLabel;
  }

  public void setWarningTime(long warningTime)
  {
    this.warningTime = warningTime;
  }

  public void setReminderTime(long reminderTime)
  {
    this.reminderTime = reminderTime;
  }

  public void setCriticalFlag(String criticalFlag)
  {
    this.criticalFlag = criticalFlag;
  }

  public void setSentinelFlag(String sentinelFlag)
  {
    this.sentinelFlag = sentinelFlag;
  }

  public void setSentinelOn(int sentinelOn)
  {
    this.sentinelOn = sentinelOn;
  }

  public void setVisibleFlag(int visibleFlag)
  {
    this.visibleFlag = visibleFlag;
  }

  public void setSentinelTo(int sentinelTo)
  {
    this.sentinelTo = sentinelTo;
  }

  //--> Cervus II : 4.2.1
  //--> By Neil : 09/30/2004
  public void setAutoTaskRetryTime(int autoTaskRetryTime)
  {
    this.autoTaskRetryTime = autoTaskRetryTime;
  }

  /*
   *
   * Methods implemented in Enterprise Bean Instances that have no remote object
   * counterparts - i.e. EJB container callback methods, and context related methods.
   *
   **/

  public void setEntityContext(EntityContext ctx)
  {
    this.ctx = ctx;
  }

  /**
   * @return the institutionId
   */
  public int getInstitutionProfileId() {
    return institutionProfileId;
  }

  /**
   * @return the BypassWFRegression flag
   */
  public String getBypassWFRegression()
  {
    return bypassWFRegression;
  }

  ////////////////////////////////////////////////////////////////////////////
  //
  // Private
  //
  ////////////////////////////////////////////////////////////////////////////

  private void setPropertiesFromQueryResult(int key) throws Exception
  {
    /*******************************************************************************
      workflowTaskId = (jExec.getInt(key, 1));
      workflowId = (jExec.getInt(key, 2));
      workflowStage = (jExec.getInt(key, 3));
      taskId = (jExec.getInt(key, 4));
      taskName = (jExec.getString(key, 5));
      taskLabel = (jExec.getString(key, 6));
      baseTask = (jExec.getString(key, 7));
      basedOnTaskId = (jExec.getInt(key, 8));
      followUpTaskId = (jExec.getInt(key, 9));
      linkTaskId = (jExec.getInt(key, 10));
      priorityId = (jExec.getInt(key, 11));
      duration = (jExec.getInt(key, 12));
      warningTime = (jExec.getInt(key, 13));
      reissueAlarmDuration = (jExec.getInt(key, 14));
      reminderTime = (jExec.getInt(key, 15));
      criticalFlag = (jExec.getString(key, 16));
      sentinelFlag = (jExec.getString(key, 17));
      sentinelOn = (jExec.getInt(key, 18));
      visibleFlag = (jExec.getInt(key, 19));
      sentinelTo = (jExec.getInt(key, 20));
     *******************************************************************************/
    workflowTaskId = (jExec.getInt(key, "WORKFLOWTASKID"));
    workflowId = (jExec.getInt(key, "WORKFLOWID"));
    workflowStage = (jExec.getInt(key, "WORKFLOWSTAGE"));
    taskId = (jExec.getInt(key, "TASKID"));
    taskName = (jExec.getString(key, "TASKNAME"));
    taskLabel = (jExec.getString(key, "TASKLABEL"));
    baseTask = (jExec.getString(key, "BASETASK"));
    basedOnTaskId = (jExec.getInt(key, "BASEDONTASKID"));
    followUpTaskId = (jExec.getInt(key, "FOLLOWUPTASKID"));
    linkTaskId = (jExec.getInt(key, "LINKTASKID"));
    priorityId = (jExec.getInt(key, "PRIORITYID"));
    duration = (jExec.getInt(key, "DURATION"));
    warningTime = (jExec.getInt(key, "WARNINGTIME"));
    reissueAlarmDuration = (jExec.getInt(key, "REISSUEALARMDURATION"));
    reminderTime = (jExec.getInt(key, "REMINDERTIME"));
    criticalFlag = (jExec.getString(key, "CRITICALFLAG"));
    sentinelFlag = (jExec.getString(key, "SENTINELFLAG"));
    sentinelOn = (jExec.getInt(key, "SENTINELON"));
    visibleFlag = (jExec.getInt(key, "VISIBLEFLAG"));
    sentinelTo = (jExec.getInt(key, "SENTINELTO"));
    //--> Cervus II : Section 4.2.1
    //--> By Neil : 09/30/2004
    //--> Added new field AUTOTASKRETRYTIME
    autoTaskRetryTime = (jExec.getInt(key, "AUTOTASKRETRYTIME"));
    institutionProfileId = (jExec.getInt(key, "INSTITUTIONPROFILEID"));
    bypassWFRegression = (jExec.getString(key, "BYPASSWFREGRESSION"));
  }

  /**
   *
   */
  public void unsetEntityContext()
  {
    ctx = null;
  }
}
