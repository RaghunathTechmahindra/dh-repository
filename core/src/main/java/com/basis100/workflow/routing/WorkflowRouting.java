/*
 * @(#)WorkflowRouting.java    2005-5-20
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.ArrayList;

import com.basis100.log.SysLogger;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

//=================================================
//--> CRF#37 :: Source/Firm to Group routing for TD
//=================================================
//--> New workflow routing -- By Billy 07Aug2003
//--> Ticket#135 : Ensure to hunt for Admin and Funder with the SOB/Firm
// group
//--> The assigned GroupId from SOB to Group relationship will be stored in
// the dealUserProfiles
//--> By Billy 27Nov2003
/**
 * WorkflowRouting - 
 * Called during underwriter assignment to a deal. This method operates on the following
 * associative relationships in the MOS schema:
 * 
 *  . Source of Business (broker) to Group(s) Association
 *
 * @version   1.0 2005-5-20
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class WorkflowRouting extends AbstractWorkflowUDRouting {

    // The logger
    private final static String LOG_HEADER = "WFE::WorkflowRouting:";

    /**
     * Constructor function
     */
    public WorkflowRouting() {
    }

    /**
     * Perform the finding.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles dup,
                                 SessionResourceKit srk) {

        SysLogger logger = srk.getSysLogger();
        // select workflow.
        selectWorkflow(deal, srk);

        boolean isGroups = false;
        boolean isBranches = false;
        List grpsOrBranches = new ArrayList();
        JdbcExecutor jExec = srk.getJdbcExecutor();

        StringBuffer sql = new StringBuffer();
        int key = 0;
        // first check for group mapping
        try {

            sql.append("SELECT groupProfileId FROM WORKFLOWTOGROUPASSOC ").
                append("WHERE workflowId = ").
                append(getWorkflowId());
            logger.info(LOG_HEADER + "findUnderwriters: sql = " +
                        sql.toString());

            key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {

                isGroups = true;
                grpsOrBranches.add(new Integer(jExec.getInt(key, 1)));
            }
            jExec.closeData(key);
        } catch (Exception e) {

            try {
                jExec.closeData(key);
            } catch (Exception ee) {
                ;
            }

            isGroups = false;
            logger.error(LOG_HEADER + "findUnderwriters: " +
                         "Error on query Workflow to Groups association (ignoring)");
            logger.error(LOG_HEADER + "findUnderwriters: err = "
                         + e.getMessage());
        }

        if (isGroups == false) {

            grpsOrBranches = new ArrayList();
            sql = new StringBuffer();
            sql.append("SELECT branchProfileId FROM WORKFLOWTOBRANCHASSOC ").
                append("WHERE workflowId = ").
                append(getWorkflowId());
            logger.info(LOG_HEADER + "findUnderwriters: None Group sql = " +
                        sql.toString());
            try {

                key = jExec.execute(sql.toString());
                for (; jExec.next(key);) {

                    isBranches = true;
                    grpsOrBranches.add(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
            } catch (Exception e) {

                try {
                    jExec.closeData(key);
                } catch (Exception ee) {
                    ;
                }

                isBranches = false;
                logger.error(LOG_HEADER +
                             "findUnderwriters: Error on query Workflow to Branches association (ignoring, using all branches)");
                logger.error(LOG_HEADER + "findUnderwriters: err = "
                             + e.getMessage());
            }
        }

        // ensure something found
        if (isGroups == false && isBranches == false) { return null; }

        // get deal origination province
        int originationProv = getOriginationProvince(deal, srk);
        List groups = groupsOptFromBranchesQualifyGeographically(isGroups,
                grpsOrBranches, originationProv, srk);
        List ids = underwritersFromGroups(groups, srk);
        if (ids != null)
            logger.info(LOG_HEADER + "findUnderwriters: Found [" +
                        ids.size() + "] underwriters.");

        return ensureActiveProfiles(ids, srk, true, deal.getInstitutionProfileId());
    }
}
