/*
 * @(#)AbstractUnderwriterRouting.java    2005-5-19
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import MosSystem.Sc;
import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.pk.AddrPK;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.UserProfileQuery;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

/**
 * AbstractUnderwriterRouting is an abstract facility class for underwriter routing.
 *
 * @version   1.0 2005-5-19
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractUnderwriterRouting implements UnderwriterRouting {

    // The logger header
    private final static String LOG_HEADER = "WFE::AbstractUnderwriterRouting:";

    // the user profile uqery instance.
    protected UserProfileQuery _userProfileQuery = new UserProfileQuery();

    /**
     * Constructor function
     */
    public AbstractUnderwriterRouting() {
    }

    /**
     */
    protected List ensureActiveProfiles(List userIds, SessionResourceKit srk,
            boolean allowStandIn, int institutionId)
    {
        SysLogger logger = srk.getSysLogger();
        if (userIds == null || userIds.size() == 0) {
            return null;
        }
        
        String sql = "Select profilestatusid, userprofileid from userprofile where userprofileid IN ("
                + formCommaDelimitedList(userIds)
                + ") AND "
                + "(profilestatusid = "
                + Sc.PROFILE_STATUS_ACTIVE
                + " OR profilestatusid = "
                + Sc.PROFILE_STATUS_DEACTIVE
                + " OR profilestatusid = " + Sc.PROFILE_STATUS_CAUTION + ")";
        logger.info(LOG_HEADER + "ensureActiveProfiles: sql = " + sql);
        JdbcExecutor jExec = srk.getJdbcExecutor();
        List activeUserIds = new ArrayList();
        try
        {
            int key = jExec.execute(sql);
            while (jExec.next(key))
            {
                int userProfileStatusId = jExec.getInt(key, 1);
                int userId = jExec.getInt(key, 2);
                if (userProfileStatusId != Sc.PROFILE_STATUS_ACTIVE)
                {
                    if (allowStandIn == false)
                    {
                        continue;
                    }
                    // attempt to use stand-in
                    UserProfile up = _userProfileQuery.getUserProfile(userId, institutionId, true, true,
                            UserProfileQuery.UNDERWRITER_USER_TYPE, srk);
                    if (up == null
                            || up.getProfileStatusId() != Sc.PROFILE_STATUS_ACTIVE)
                    {
                        continue;
                    }
                    // have an active stand-in
                    userId = up.getUserProfileId();
                }
                activeUserIds.add(new Integer(userId));
            }
            jExec.closeData(key);
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::WorkflowAssistBean:ensureActiveProfiles: Error qualifying by active profile - assume all inactive");
            logger.error("WFE::WorkflowAssistBean:ensureActiveProfiles: err = "
                    + e.getMessage());
            return null;
        }
        if (activeUserIds.size() == 0) { return null; }
        return activeUserIds;
    }

    /**
     * check the user profile's activity.
     */
    protected List ensureActiveProfiles2(List userProfiles,
                                       SessionResourceKit srk,
                                       boolean allowStandIn, int institutionId) {

        if (userProfiles == null || userProfiles.size() == 0) {
            return null;
        }

        SysLogger logger = srk.getSysLogger();
        ArrayList activeUserIds = new ArrayList();
        UserProfile currUser = null;
        try
        {
            for (Iterator i = userProfiles.iterator(); i.hasNext(); )
            {
                currUser = (UserProfile) i.next();
                int userProfileStatusId = currUser.getProfileStatusId();
                int userId = currUser.getUserProfileId();
                if (userProfileStatusId != Sc.PROFILE_STATUS_ACTIVE)
                {
                    if (allowStandIn == false)
                    {
                        continue;
                    }
                    // attempt to use stand-in
                    UserProfile up =
                        _userProfileQuery.
                        getUserProfile(userId, institutionId, true, true,
                                       UserProfileQuery.UNDERWRITER_USER_TYPE,
                                       srk);
                    if (up == null
                            || up.getProfileStatusId() != Sc.PROFILE_STATUS_ACTIVE)
                    {
                        continue;
                    }
                    // have an active stand-in
                    userId = up.getUserProfileId();
                }
                activeUserIds.add(new Integer(userId));
            }
        }
        catch (Exception e)
        {
            logger
                    .error("WFE::AbstractUnderwriterRouting:ensureActiveProfiles2: Error qualifying by active profile - assume all inactive");
            logger.error("WFE::WorkflowAssistBean:ensureActiveProfiles: err = "
                    + e.getMessage());
            return null;
        }

        if (activeUserIds.size() == 0) { return null; }
        return activeUserIds;
    }

    /**
     * Given a list of values return same in a comma delimited list. E.g.
     * 
     *     IN == 1234 789 IN == "THIS" "THAT"
     *     OUT == "1234, 789" OUT == "\"THIS\", "\THAT\""
     * 
     *     --> not that where the input is Strings each element in the
     *         result is encased in double quoted.
     * 
     * Returns "-1" if list null or empty - e.g. if used in a SQL IN clause we
     * have "IN(-1)" - hence
     * guaranteed not users selected.
     */
    protected String formCommaDelimitedList(List values)
    {
        if (values == null || values.size() == 0) { return "-1"; }
        StringBuffer sb = new StringBuffer();
        //Iterator i = values.iterator();
        //while (i.hasNext()) {
        for (Iterator i = values.iterator(); i.hasNext(); ) {

            Object val = i.next();
            if (val instanceof String) {

                sb.append("\"" + (String) val + "\"");
            } else {
                sb.append("" + ((Integer) val).intValue());
            }

            if (i.hasNext()) sb.append(",");
        }
        return sb.toString();
    }

    /**
     */
    protected int getOriginationProvince(Deal deal, SessionResourceKit srk)
    {
        int ONTARIO = 9;
        SysLogger logger = srk.getSysLogger();
        //JdbcExecutor jExec = srk.getJdbcExecutor();
        int originationProvince = -1;
        // first by principal property
        try {
            Property prop = new Property(srk, null);
            prop.setSilentMode(true);
            prop = prop.findByPrimaryProperty(deal.getDealId(), deal
                    .getCopyId(), deal.getInstitutionProfileId());
            originationProvince = prop.getProvinceId();
        } catch (Exception e) {
            originationProvince = -1;
            logger.error(LOG_HEADER +
                         "getOriginationProvince: Could not locate primary property on deal (ignoring)");
            logger.error(LOG_HEADER + "getOriginationProvince: err = "
                         + e.getMessage());
        }
        if (originationProvince != -1) { return originationProvince; }
        int borrowerId = -1;
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            borrowerId = borrower.getBorrowerId();
        }
        catch (Exception e)
        {
            borrowerId = -1;
            logger
                    .trace("WFE::WorkflowAssistBean:getOriginationProvince: Could not locate address of primary borrower (ignoring)");
            logger
                    .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                            + e.getMessage());
        }
        if (borrowerId == -1)
        {
            try
            {
                Borrower borrower = new Borrower(srk, null);
                borrower = borrower.findByFirstBorrower(deal.getDealId(), deal
                        .getCopyId());
                borrowerId = borrower.getBorrowerId();
            }
            catch (Exception e)
            {
                borrowerId = -1;
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: Could not locate address of first borrower (ignoring)");
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                + e.getMessage());
            }
        }
        if (borrowerId > -1)
        {
            // Borrrower Found
            int addrId = -1;
            try
            {
                BorrowerAddress bAddr = new BorrowerAddress(srk);
                bAddr = bAddr
                        .findByCurrentAddress(borrowerId, deal.getCopyId());
                addrId = bAddr.getAddrId();
            }
            catch (Exception e)
            {
                addrId = -1;
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: Error locating address of borrower (ignoring)");
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                + e.getMessage());
            }
            if (addrId > -1)
            {
                try
                {
                    Addr addr = new Addr(srk);
                    addr = addr.findByPrimaryKey(new AddrPK(addrId, deal
                            .getCopyId()));
                    originationProvince = addr.getProvinceId();
                }
                catch (Exception e)
                {
                    originationProvince = -1;
                    logger
                            .trace("WFE::WorkflowAssistBean:getOriginationProvince: Error locating address of borrower(2) (ignoring)");
                    logger
                            .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                    + e.getMessage());
                }
            }
        }
        // If still fail look for the address of the SOB -- By Billy14Jan2002
        if (originationProvince == -1)
        {
            try
            {
                originationProvince = deal.getSourceOfBusinessProfile()
                        .getContact().getAddr().getProvinceId();
            }
            catch (Exception e)
            {
                originationProvince = -1;
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: Error locating address of SourceOfBusiness (ignoring)");
                logger
                        .trace("WFE::WorkflowAssistBean:getOriginationProvince: err = "
                                + e.getMessage());
            }
        }
        if (originationProvince == -1) { return ONTARIO; }

        return originationProvince;
    }

    /**
     * groupsOptFromBranchesQualifyGeographically
     * 
     * Return a list of groups, optionally from a list of branches, qualified by
     * province (if province
     * not -1 - but this should never happen)
     */
    protected List groupsOptFromBranchesQualifyGeographically(boolean isGroups,
            List list, int provinceId, SessionResourceKit srk)
    {
        String sql = null;
        List qualifiedGroups = new ArrayList();
        List qualifiedBranches = new ArrayList();
        Integer minusOne = new Integer(-1);
        int key = 0;
        List groups = list;
        List branches = list;
        SysLogger logger = srk.getSysLogger();
        JdbcExecutor jExec = srk.getJdbcExecutor();
        // if groups provided form a list of the corresponding branches
        if (isGroups == true)
        {
            int lim = groups.size();
            for (int i = 0; i < lim; ++i)
            {
                branches.set(i, minusOne);
            }
            try
            {
                sql = "Select GROUPPROFILEID, BRANCHPROFILEID from GROUPPROFILE where GROUPPROFILEID in ("
                        + formCommaDelimitedList(groups) + ")";
                key = jExec.execute(sql);
                while (jExec.next(key))
                {
                    int gId = jExec.getInt(key, 1);
                    int bId = jExec.getInt(key, 2);
                    populateCrossMatch(gId, bId, groups, branches, lim);
                }
                jExec.closeData(key);
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: Error locating branchs (ids) for groups");
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: sql = "
                                + sql);
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: err = "
                                + e.getMessage());
                return null;
            }
        }
        int len = branches.size();
        boolean checkEliminationByProvince = provinceId != -1;
        for (int i = 0; i < len; ++i)
        {
            int branchId = ((Integer) branches.get(i)).intValue();
            if (branchId == -1)
            {
                continue;
            }
            if (checkEliminationByProvince)
            {
                List provs = getProvincesForBranch(branchId, 1, logger, jExec);
                if (provinceInList(provs, provinceId) == false)
                {
                    continue;
                }
            }
            if (isGroups)
            {
                qualifiedGroups.add(groups.get(i));
            }
            else
            {
                qualifiedBranches.add(new Integer(branchId));
            }
        }
        if (isGroups) { return qualifiedGroups; }
        // get all the groups in the qualified branches
        len = qualifiedBranches.size();
        int branchId = 0;
        for (int i = 0; i < len; ++i)
        {
            // get groups in the branch
            try
            {
                branchId = ((Integer) qualifiedBranches.get(i))
                        .intValue();
                if (branchId == -1)
                {
                    continue;
                }
                sql = "Select groupProfileId from GROUPPROFILE where branchProfileId = "
                        + branchId;
                key = jExec.execute(sql);
                for (; jExec.next(key);)
                {
                    qualifiedGroups
                            .add(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: Error locating groups for branch Id = "
                                + branchId + "(ignoring branch)");
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: sql = "
                                + sql);
                logger
                        .trace("WFE::WorkflowAssistBean:groupsOptFromBranchesQualifyGeographically: err = "
                                + e.getMessage());
                return null;
            }
        }
        return qualifiedGroups;
    }

    /**
     *
     */
    protected List underwritersFromGroups(List groups, SessionResourceKit srk)
    {
        if (groups == null || groups.size() == 0) { return null; }
        List underwriters = new ArrayList();
        SysLogger logger = srk.getSysLogger();
        JdbcExecutor jExec = srk.getJdbcExecutor();
        String sql = null;
        int key = 0;
        int len = groups.size();
        for (int i = 0; i < len; ++i)
        {
            int groupId = ((Integer) groups.get(i)).intValue();
            try
            {
                sql = "Select userProfileId from USERPROFILE where groupProfileId = "
                        + groupId
                        + " AND userTypeId IN("
                        + Mc.USER_TYPE_JR_UNDERWRITER
                        + ", "
                        + Mc.USER_TYPE_UNDERWRITER
                        + ", "
                        + Mc.USER_TYPE_SR_UNDERWRITER + ")";
                key = jExec.execute(sql);
                for (; jExec.next(key);)
                {
                    underwriters.add(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .error("WFE::WorkflowAssistBean:underwritersFromGroups: Error locating underwriters for group Id = "
                                + groupId + "(ignoring group)");
                logger
                        .error("WFE::WorkflowAssistBean:underwritersFromGroups: sql = "
                                + sql);
                logger
                        .error("WFE::WorkflowAssistBean:underwritersFromGroups: err = "
                                + e.getMessage());
                return null;
            }
        }
        return underwriters;
    }

    /**
     *
     */
    private List getProvincesForBranch(int branchId, int method,
            SysLogger logger, JdbcExecutor jExec)
    {
        String sql = null;
        int key = 0;
        List provs = new ArrayList();
        if (method == 1)
        {
            try
            {
                // via branch to province association
                sql = "select PROVINCEID from PROVINCEBRANCHROUTING where BRANCHPROFILEID = "
                        + branchId;
                key = jExec.execute(sql);
                while (jExec.next(key))
                {
                    provs.add(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
                if (provs.size() > 0) { return provs; }
            }
            catch (Exception e)
            {
                try
                {
                    jExec.closeData(key);
                }
                catch (Exception ee)
                {
                    ;
                }
                logger
                        .trace("WFE::WorkflowAssistBean:getProvinceForBranch: Error locating province id (from branch-prov association): branchId = "
                                + branchId
                                + ", (ignoring - get province from branch address)");
                logger
                        .trace("WFE::WorkflowAssistBean:getProvinceForBranch: sql = "
                                + sql);
                logger
                        .trace("WFE::WorkflowAssistBean:getProvinceForBranch: err = "
                                + e.getMessage());
                provs = new ArrayList();
            }
        }
        // via address
        try
        {
            // via branch address
            sql = "select contactId from BRANCHPROFILE where branchProfileId = "
                    + branchId;
            key = jExec.execute(sql);
            jExec.next(key);
            int contactId = jExec.getInt(key, 1);
            jExec.closeData(key);
            sql = "select addrId from CONTACT where contactId = " + contactId;
            key = jExec.execute(sql);
            jExec.next(key);
            int addrId = jExec.getInt(key, 1);
            jExec.closeData(key);
            sql = "select provinceId from ADDR where addrId = " + addrId;
            key = jExec.execute(sql);
            jExec.next(key);
            provs.add(new Integer(jExec.getInt(key, 1)));
            jExec.closeData(key);
            return provs;
        }
        catch (Exception e)
        {
            try
            {
                jExec.closeData(key);
            }
            catch (Exception ee)
            {
                ;
            }
            logger
                    .trace("WFE::WorkflowAssistBean:getProvinceForBranch: Error locating province id (from branch address): branchId = : branchId = "
                            + branchId);
            logger.trace("WFE::WorkflowAssistBean:getProvinceForBranch: sql = "
                    + sql);
            logger.trace("WFE::WorkflowAssistBean:getProvinceForBranch: err = "
                    + e.getMessage());
        }
        return null;
    }

    private boolean provinceInList(List provs, int provId)
    {
        if (provs == null) { return false; }
        int lim = provs.size();
        for (int i = 0; i < lim; ++i)
        {
            if (provId == ((Integer) provs.get(i)).intValue()) { return true; }
        }
        return false;
    }

    private void populateCrossMatch(int matchId, int crossRefId, List main,
            List cross, int lim)
    {
        Integer matchVal = new Integer(matchId);
        Integer crossVal = new Integer(crossRefId);
        for (int i = 0; i < lim; ++i)
        {
            if (matchVal.equals((Integer) main.get(i)))
            {
                cross.set(i, crossVal);
            }
        }
    }
}
