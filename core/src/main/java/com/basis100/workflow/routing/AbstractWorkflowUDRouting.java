/*
 * @(#)AbstractWorkflowUDRouting.java    2005-5-20
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import MosSystem.Sc;

import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.Deal;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;

/**
 * AbstractWorkflowUDRouting is the abastract class for workflow involved
 * underwriter routing.
 *
 * @version   1.0 2005-5-20
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractWorkflowUDRouting
    extends AbstractUnderwriterRouting {

    // The logger header
    private final static String LOG_HEADER = "WFE::AbstractWorkflowUDRouting:";

    /**
     * Constructor function
     */
    public AbstractWorkflowUDRouting() {
    }

    /**
     * the selected workflow id.  The default is -1, which means there is no
     * workflow related.
     */
    private int _workflowId = -1;

    /**
     * Get the value of the selected workflow id.
     */
    protected int getWorkflowId() {
        return _workflowId;
    }

    /**
     * Set the value of the selected workflow id.
     */
    protected void setWorkflowId(int workflowId) {
        _workflowId = workflowId;
    }

    /**
     * indicate the selected the workflow allow SOB mapping or not.  The default
     * is true.
     */
    private boolean _isWFAllowSOBMapping = true;

    /**
     * Get the value of indicate the selected the workflow allow SOB mapping or not.
     */
    protected boolean getIsWFAllowSOBMapping() {
        return _isWFAllowSOBMapping;
    }

    /**
     * Set the value of indicate the selected the workflow allow SOB mapping or not.
     */
    protected void setIsWFAllowSOBMapping(boolean isWFAllowSOBMapping) {
        _isWFAllowSOBMapping = isWFAllowSOBMapping;
    }

    /**
     * The selected workflow is approval authority qualification or not?  The
     * default is false.
     */
    private boolean _isWFApprovalAQ = false;

    /**
     * Get the value of The selected workflow is approval authority qualification or not?
     */
    protected boolean getIsWFApprovalAQ() {
        return _isWFApprovalAQ;
    }

    /**
     * Set the value of The selected workflow is approval authority qualification or not?
     */
    protected void setIsWFApprovalAQ(boolean isWFApprovalAQ) {
        _isWFApprovalAQ = isWFApprovalAQ;
    }

    /**
     * 
     */
    protected void selectWorkflow(Deal deal, SessionResourceKit srk)
    {
        SysLogger logger = srk.getSysLogger();
        srk.setCloseJdbcConnectionOnRelease(true);
        try
        {
            // added for ML
            // set VPD from deal insttutionProfileId
            srk.getExpressState().setDealInstitutionId(deal.getInstitutionProfileId());
            
            BusinessRule br = new BusinessRule(
                    srk, -1, -1, "na",
                    Sc.WORFLOW_SELECT_BR_NAME);
            br.setDealId(deal.getDealId(), deal.getCopyId());
            RuleResult rr = null;
            while (br.next())
            {
                rr = br.evaluate();
                if (rr.ruleStatus == true)
                {
                    // first operand is the workflow Id
                    if (rr.opCount <= 0)
                    {
                        continue; // really an error! (no workflow id associated
                        // with exp)
                    }
                    setWorkflowId(rr.getInt(1));
                    if (rr.opCount > 1)
                    {
                        if (rr.getInt(2) != 1)
                        {
                            setIsWFAllowSOBMapping(false);
                        }
                        if (rr.opCount > 2)
                        {
                            if (rr.getInt(3) == 1)
                            {
                                setIsWFApprovalAQ(true);
                            }
                        }
                    }
                    break;
                }
            }
            br.close();
        }
        catch (Exception e)
        {
            setWorkflowId(-1);
            logger.error(LOG_HEADER
                         + "selectWorkflow: Business Rule Engine exception generating initital tasks, Business Rule="
                         + Sc.WORFLOW_SELECT_BR_NAME
                         + ", deal Id="
                         + deal.getDealId());
            logger.error(LOG_HEADER
                         + "selectWorkflow: Business Rule Engine exception = "
                         + e.getMessage());
        }
    }
}
