/*
 * @(#)SOB2GroupRouting.java    2005-5-19
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.Vector;

import com.basis100.log.SysLogger;
import com.basis100.deal.entity.Deal;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.resources.SessionResourceKit;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

/**
 * SOB2GroupRouting is going to find our the underwriters for a deal based on
 * the following relationships in the MOS schema:
 * Source of Business (broker) to Group(s) Association
 *
 * @version   1.0 2005-5-19
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class SOB2GroupRouting extends AbstractUnderwriterRouting {

    // the log header for syslog.
    private final static String LOG_HEADER = "WFE::SOB2GroupRouting:";

    /**
     * Constructor function
     */
    public SOB2GroupRouting() {
    }

    /**
     * Perform the finding.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles dup,
                                 SessionResourceKit srk) {

        SysLogger logger = srk.getSysLogger();
        int sobId = deal.getSourceOfBusinessProfileId();
        if (sobId <= 0) return null;

        List underwriters = new Vector();
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT groupProfileId FROM SourceToGroupAssoc ").
            append("WHERE sourceOfBusinessProfileId = ").
            append(sobId);
        logger.info(LOG_HEADER + "findUnderwriters: SQL = " +
                    sql.toString());
        JdbcExecutor jExec = srk.getJdbcExecutor();
        int groupProfileId = -1;
        try {

            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {

                groupProfileId = jExec.getInt(key, 1);
                Vector allUserProfilesInGroup =
                    _userProfileQuery.getGroupUnderwriters(groupProfileId, -1,
                                                           false, srk, deal.getInstitutionProfileId());
                underwriters.addAll(this.ensureActiveProfiles2(
                        allUserProfilesInGroup, srk, true, deal.getInstitutionProfileId()));
            }
            jExec.closeData(key);
        } catch (Exception e) {

            logger
                .error("WFE::WorkflowAssistBean:performSOBGroupMapping: Error on query Source Of Business to Group association");
            logger
                .error("WFE::WorkflowAssistBean:performSOBGroupMapping: err = "
                       + e);
        }

        if (underwriters != null && underwriters.size() > 0) {

            logger.info(LOG_HEADER + "findUnderwriters: found underwriter amount [" +
                        underwriters.size() + "]");
            dup.setSourceGroupId(groupProfileId);
        } else {
            // Just in case
            logger.info(LOG_HEADER + "findUnderwriters: found nothing!");
            dup.setSourceGroupId(-1);
        }

        return underwriters;
    }
}
