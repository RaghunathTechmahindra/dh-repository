/*
 * @(#)SOB2UserRouting.java    2005-5-20
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.ArrayList;

import com.basis100.log.SysLogger;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

/**
 * SOB2UserRouting - 
 *
 * @version   1.0 2005-5-20
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class SOB2UserRouting extends AbstractWorkflowUDRouting {

    // the header for sys logger.
    private final static String LOG_HEADER = "WFE::SOB2UserRouting:";

    /**
     * Constructor function
     */
    public SOB2UserRouting() {
    }

    /**
     * Perform the finding.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles dup,
                                 SessionResourceKit srk) {

        SysLogger logger = srk.getSysLogger();
        selectWorkflow(deal, srk);
        int sobId = deal.getSourceOfBusinessProfileId();

        List underwriterIds = new ArrayList();
        if (getIsWFAllowSOBMapping() && sobId != 0) {

            StringBuffer sql = new StringBuffer();
            sql.append("SELECT userProfileId FROM SOURCETOUSERASSOC ").
                append("WHERE sourceOfBusinessProfileId = ").
                append(sobId);
            logger.info(LOG_HEADER + "findUnderwriters: sql = " +
                        sql.toString());
            JdbcExecutor jExec = srk.getJdbcExecutor();
            try {

                int key = jExec.execute(sql.toString());
                for (; jExec.next(key);) {

                    int userProfileId = jExec.getInt(key, 1);
                    underwriterIds.add(new Integer(userProfileId));
                }
                jExec.closeData(key);
            } catch (Exception e) {
                logger
                    .error(LOG_HEADER + "findUnderwriters: Error on query Source Of Business to Underwriter association");
                logger.error(LOG_HEADER + "findUnderwriters: err = " + e.getMessage());
            }
        }

        if (underwriterIds != null)
            logger.info(LOG_HEADER + "findUnderwriters: found underwriter amount [" +
                        underwriterIds.size() + "]");

        return ensureActiveProfiles(underwriterIds, srk, true, deal.getInstitutionProfileId());
    }
}
