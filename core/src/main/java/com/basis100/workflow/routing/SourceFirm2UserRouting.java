/*
 * @(#)SourceFirm2UserRouting.java    2005-5-20
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.ArrayList;

import com.basis100.log.SysLogger;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

/**
 * SourceFirm2UserRouting - 
 *
 * @version   1.0 2005-5-20
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class SourceFirm2UserRouting extends AbstractWorkflowUDRouting {

    // The logger header
    private final static String LOG_HEADER = "WFE::SourceFirm2UserRouting:";

    /**
     * Constructor function
     */
    public SourceFirm2UserRouting() {
    }

    /**
     * Perform the finding.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles dup,
                                 SessionResourceKit srk) {

        SysLogger logger = srk.getSysLogger();
        // select the workflow for this deal.
        selectWorkflow(deal, srk);
        int sfId = deal.getSourceFirmProfileId();
        List underwriterIds = new ArrayList();

        if (getIsWFAllowSOBMapping() && sfId != 0) {

            StringBuffer sql = new StringBuffer();
            sql.append("SELECT userProfileId FROM SOURCEFIRMTOUSERASSOC ").
                append("WHERE sourceFirmProfileId = ").
                append(sfId);
            logger.info(LOG_HEADER + "findUnderwriters: sql = " +
                        sql.toString());

            JdbcExecutor jExec = srk.getJdbcExecutor();
            try {

                int key = jExec.execute(sql.toString());
                for (; jExec.next(key);) {

                    underwriterIds.add(new Integer(jExec.getInt(key, 1)));
                }
                jExec.closeData(key);
            } catch (Exception e) {
                logger
                    .error(LOG_HEADER + "findUnderwriters: Error on query Source Firm to Underwriter association");
                logger.error(LOG_HEADER + "findUnderwriters: err = "
                             + e.getMessage());
            }
        }

        if (underwriterIds != null)
            logger.info(LOG_HEADER + "findUnderwriters: found underwriter amount [" +
                        underwriterIds.size() + "]");

        return ensureActiveProfiles(underwriterIds, srk, true, deal.getInstitutionProfileId());
    }
}
