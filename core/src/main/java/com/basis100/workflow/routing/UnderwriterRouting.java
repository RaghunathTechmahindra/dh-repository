/*
 * @(#)UnderwriterRouting.java    2005-5-19
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;

import com.basis100.deal.entity.Deal;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.resources.SessionResourceKit;

/**
 * UnderwriterRouting defines the interface to assign the underwriter for the
 * given Deal.
 *
 * @version   1.0 2005-5-19
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface UnderwriterRouting {

    /**
     * find out the available underwrites for the given deal, and returns them
     * as a List.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles profils,
                                 SessionResourceKit srk);
}
