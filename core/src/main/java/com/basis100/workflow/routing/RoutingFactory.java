/*
 * @(#)RoutingFactory.java    2005-5-19
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

/**
 * RoutingFactory is the factory class to get an instance of routing concrete
 * class.
 *
 * @version   1.0 2005-5-19
 * @since     3.0
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class RoutingFactory {

    // The logger header
    private final static String LOG_HEADER = "WFE::RoutingFactory:";

    /**
     * returns an instance of underwriter routing class based on the given
     * concrete classname.
     *
     * @param className the concrete class name.
     * @return an instance of the concrete class name.
     */
    public static UnderwriterRouting getUnderwriterRouting(String className) {

        // TODO: do we need cache the instances of contrete classes?
        UnderwriterRouting routing = null;
        try {
            routing =
                (UnderwriterRouting) Class.forName(className).newInstance();
        } catch (ClassNotFoundException e) {
            //_log.error(e.getMessage(), e);
            // TODO: throw an eXpress exception
        } catch (InstantiationException e) {
            //_log.error(e.getMessage(), e);
            // TODO: throw an eXpress exception
        } catch (IllegalAccessException ie) {
            //_log.error(ie.getMessage(), ie);
            // TODO: throw an eXpress exception
        }

        return routing;
    }
}
