/*
 * @(#)TableRouting.java    2005-5-20
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.workflow.entity.Routing;
import com.basis100.workflow.UserProfileQuery;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

/**
 * TableRouting is trying to find out the available underwriters by using table
 * routing.
 * <p>
 * Details please refer to Ticket # 234
 *
 * @version   1.0 2005-5-20
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class TableRouting extends AbstractUnderwriterRouting {

    // The logger
    private final static String LOG_HEADER = "WFE::TableRouting:";

    /**
     * Constructor function
     */
    public TableRouting() {
    }

    /**
     * Perform the finding.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles dup,
                                 SessionResourceKit srk) {

        SysLogger logger = srk.getSysLogger();
        final String METHOD = "findUnderwriters: ";

        List theRoutingList = null;
        // Get sorted Routing Records (Sorted by RouteOrder)
        try {

            Routing ro = new Routing(srk);
            theRoutingList = ro.getOrderedList();
            if (theRoutingList == null || theRoutingList.size() == 0)
            {
                logger.warning(LOG_HEADER + METHOD +
                               "No Routing Entries found!!");
                return null;
            }
            logger.info(LOG_HEADER + METHOD +
                        "Found Routing [" + theRoutingList.size() +
                        "] Entries!");
        } catch (FinderException fe) {

            logger.error(LOG_HEADER + METHOD +
                         "No Routing Entries found!! -- " + fe.getMessage());
            return null;
        } catch (Exception e) {

            logger.error(LOG_HEADER + METHOD +
                         "get Routing List exception -- " + e.getMessage());
            return null;
        }

        //Go through the list one by one and try mapping.
        Routing currRo = null;
        boolean matched = false;
        List underwriterIds = new Vector();
        for(Iterator i = theRoutingList.iterator(); i.hasNext(); ) {

            currRo = (Routing) i.next();
            matched = false;
            // TODO: Consider to put them to method toString().
            logger.debug(LOG_HEADER + METHOD + "Current Routing Type [" +
                         "Routing Type=" + currRo.getRouteTypeId() +
                         " RouteKey=" + currRo.getRouteKey() +
                         " MatchType=" + currRo.getMatchTypeId() +
                         " MatchValue = " + currRo.getMatchValue() + "]");

            switch (currRo.getRouteTypeId()) {

            case Mc.ROUTE_TYPE_SOURCE:
                // check if SourceOfBusinessId matched
                if (deal.getSourceOfBusinessProfileId() ==
                    getIntValue(currRo.getRouteKey())) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- SOB Id Matched - "
                                 + deal.getSourceOfBusinessProfileId());
                    matched = true;
                }
                break;

            case Mc.ROUTE_TYPE_SOURCEFIRM:
                // check if SourceFirmId matched
                if (deal.getSourceFirmProfileId() ==
                    getIntValue(currRo.getRouteKey())) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- SFirm Id Matched - "
                                 + deal.getSourceFirmProfileId());
                    matched = true;
                }
                break;

            case Mc.ROUTE_TYPE_POSTALCODE:
            {
                // check if PostalCode matched
                String tmpPostalCode = trimAllSpaces(
                        getRoutePostalCode(deal, srk)).toUpperCase();
                String theRouteKey = trimAllSpaces(currRo.getRouteKey())
                        .toUpperCase();
                if (tmpPostalCode.startsWith(theRouteKey)) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- PostalCode Matched - "
                                 + tmpPostalCode);
                    matched = true;
                }
                break;
            }

            case Mc.ROUTE_TYPE_AREACODE:
            {
                // check if AresCode matched
                String tmpAreaCode = trimAllSpaces(getRouteAreaCode(deal, srk));
                String theRouteKey = trimAllSpaces(currRo.getRouteKey())
                    .substring(0, 3);
                if (tmpAreaCode.equals(theRouteKey)) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- AreaCode Matched - "
                                 + tmpAreaCode);
                    matched = true;
                }
                break;
            }

            case Mc.ROUTE_TYPE_BORROWERLASTNAME:
            {
                // check if BorrowerLastName matched
                String tmpLastName = getRouteBLastName(deal, srk).toUpperCase();
                String theRouteKey = currRo.getRouteKey().toUpperCase();
                if (tmpLastName.startsWith(theRouteKey)) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- Borrower's LastName Matched - "
                                 + tmpLastName);
                    matched = true;
                }
                break;
            }

            case Mc.ROUTE_TYPE_WORKFLOW:

                // perform Workflow matching
                // NOT SUPPORTED YET !!
                break;

            case Mc.ROUTE_TYPE_PROVINCE:
                {
                // check if Province matched
                int theProvince = getRouteProvince(deal, srk);
                if (theProvince != -1
                    && theProvince == getIntValue(currRo.getRouteKey())) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- Province Matched - "
                                 + theProvince);
                    matched = true;
                }
                break;
                }

            // Added new RouteType (SOBRegion) -- By BILLY 17May2002
            case Mc.ROUTE_TYPE_SOBREGION:

                try {
                    // check if SourceOfBusiness.SOBRegionId matched
                    int theSOBRegionId = deal.getSourceOfBusinessProfile()
                            .getSOBRegionId();
                    //--Merge--//
                    //Modified to route even if SOBRegionId = 0
                    //  - By Billy 25June2002
                    if (theSOBRegionId >= 0 &&
                        theSOBRegionId == getIntValue(currRo.getRouteKey())) {

                        //Matched
                        logger.debug(LOG_HEADER + METHOD + "-- SOBRegion Matched - "
                                     + theSOBRegionId);
                        matched = true;
                    }
                } catch (Exception e) {
                    // do nothing!
                    logger.debug(LOG_HEADER + METHOD + "-- SOBRegion error - " +
                                 e.getMessage());
                }

                break;

            // Added new RouteType (SpecialFeature) -- By BILLY 17May2002
            case Mc.ROUTE_TYPE_SPECIALFEATURE:

                // check if SPECIALFEATURE matched
                if (deal.getSpecialFeatureId() ==
                    getIntValue(currRo.getRouteKey())) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- SpecialFeature Id Matched - "
                                 + deal.getSpecialFeatureId());
                    matched = true;
                }
                break;

            // Added new RouteType (SourceFirm + Province) -- By BILLY
            // 03July2003
            case Mc.ROUTE_TYPE_SOURCEFIRM_PROVINCE:

                // check if SourceFirmId and Province code matched
                // RouteKey format == SFId, ProvId
                Vector theRouteKeys = currRo.getRouteKeys();
                int theProvince = getRouteProvince(deal, srk);
                if ((deal.getSourceFirmProfileId() ==
                     getIntValue(theRouteKeys.get(0).toString().trim()))
                    && (theProvince != -1)
                    && (theProvince ==
                        getIntValue(theRouteKeys.get(1).toString().trim()))) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- SFirm Id - "
                                 + deal.getSourceFirmProfileId()
                                 + " And Province Matched - " + theProvince);
                    matched = true;
                }
                break;

            //--> Added new RouteType (Self-Employed) :: Ticket# 328
            //--> By BILLY 21April2004
            case Mc.ROUTE_TYPE_SELFEMPLOYED:

                // check if any borrower's current Employment is Self-Empolyed
                // RouteKey format == SFId, ProvId
                boolean theRouteKey =
                    (currRo.getRouteKey().equals("N") ? false : true);
                logger.debug(LOG_HEADER + METHOD +
                             "Self-employed from RouteKey = " + theRouteKey);
                boolean isSelfEmployed = getIsSelfEmployed(deal, srk);
                if (isSelfEmployed == theRouteKey) {

                    //Matched
                    logger.debug(LOG_HEADER + METHOD + "-- SelfEmployed - Matched - "
                                 + isSelfEmployed);
                    matched = true;
                }
                break;

            // SEAN Routing Order Changes (May 05, 2005): handle the two new
            // routing types.
            case Mc.ROUTE_TYPE_OCCUPANCYTYPE:

                try {
                    Collection props = (new Property(srk, null)).
                        findByDealIdAndPrimaryFlag(deal.getDealId(), 'Y');
                    if (props.size() > 0) {
                        // get the first one.
                        Iterator p = props.iterator();
                        Property primary = (Property) p.next();
                        if (primary.getOccupancyTypeId() ==
                            getIntValue(currRo.getRouteKey())) {

                            logger.debug(LOG_HEADER + METHOD + "-- Occupancy type - Matched - "
                                         + primary.toString());
                            matched = true;
                        }
                    }
                } catch (Exception e) {
                    
                    // error ...
                    logger.error(LOG_HEADER + METHOD + "-- Occupancy type - Error - "
                                 + e.getMessage());
                }
                break;

            case Mc.ROUTE_TYPE_SOBSTATUS:

                try {
                    int statusId =
                        deal.getSourceOfBusinessProfile().getProfileStatusId();
                    if (statusId == getIntValue(currRo.getRouteKey())) {

                        logger.debug(LOG_HEADER + METHOD + "-- SOB Status - Matched - "
                                     + "SOB ID=" + deal.getSourceOfBusinessProfileId()
                                     + "Status ID=" + statusId);
                        matched = true;
                    }
                } catch (FinderException fe) {
                    // error...
                    logger.error(LOG_HEADER + METHOD + "-- SOB Status - error - "
                                 + fe.getMessage());
                } catch (RemoteException re) {
                    // error ...
                    logger.error(LOG_HEADER + METHOD + "-- SOB Status - error - "
                                 + re.getMessage());
                }
                break;

            // SEAN Routing Order Changes END.
            default:
                // problem !!
                break;
            }

            if (matched == true) {
                //Perform UW hunting based on Matched Routing Record
                underwriterIds = routingHuntForUsers(currRo, deal, srk);
            }

            if (underwriterIds != null && underwriterIds.size() > 0) {

                break; // we found it !!
            }
        } // For
        return underwriterIds;
    }

    /**
     */
    private List routingHuntForUsers(Routing ro, Deal deal,
            SessionResourceKit srk)
    {
        List underwriterIds = new Vector();
        SysLogger logger = srk.getSysLogger();
        switch (ro.getMatchTypeId())
        {
        case Mc.ROUTE_MATCH_TYPE_USER:
        {
            UserProfile theUser = _userProfileQuery.getUserProfile(ro.getMatchValue(), 
                                                                   deal.getInstitutionProfileId(), 
                                                                   true, true, 
                                                                   UserProfileQuery.UNDERWRITER_USER_TYPE, srk);
            if (theUser != null && theUser.getUserProfileId() > 0)
            {
                underwriterIds.add(new Integer(theUser
                        .getUserProfileId()));
                logger
                        .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform User matching : MatchValue = "
                                + ro.getMatchValue()
                                + " Map to user = "
                                + theUser.getUserProfileId());
            }
            else
            {
                logger
                        .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform User matching : MatchValue = "
                                + ro.getMatchValue()
                                + " No Active User found !!");
            }
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_GROUP:
        {
            Vector allUserProfilesInGroup = _userProfileQuery.getGroupUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup,
                    srk, true, deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Group matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Group " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_BRANCH:
        {
            Vector allUserProfilesInGroup = _userProfileQuery.getBranchUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup,
                    srk, true, deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Branch matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Branch " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_REGION:
        {
            Vector allUserProfilesInGroup = _userProfileQuery.getRegionUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup,
                    srk, true, deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Region matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Region " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_INSTITUTION:
        {
            Vector allUserProfilesInGroup = _userProfileQuery.getInstitutionUnderwriters(ro
                    .getMatchValue(), -1, false, srk);
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup,
                    srk, true, deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Institution matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Institution " + ro.getMatchValue());
            break;
        }
        case Mc.ROUTE_MATCH_TYPE_LENDER:
        {
            Vector allUserProfilesInGroup = _userProfileQuery.getLenderUnderwriters(ro
                    .getMatchValue(), -1, false, srk, deal.getInstitutionProfileId());
            underwriterIds = this.ensureActiveProfiles2(allUserProfilesInGroup,
                    srk, true, deal.getInstitutionProfileId());
            logger
                    .debug("WFE::WorkflowAssistBean:routingHuntForUsers -- Perform Lender matching : MatchValue = "
                            + ro.getMatchValue()
                            + " Map to users = ("
                            + formCommaDelimitedList(underwriterIds)
                            + ") in Lender " + ro.getMatchValue());
            break;
        }
        default:
            // Problem
            break;
        }
        return underwriterIds;
    }

    //--> Added new RouteType (Self-Employed) :: Ticket# 328
    //--> By BILLY 21April2004
    /**
     * Given a deal entity determine if any borrower's current Employment is Self-Empolyed as:
     * 
     * �  IncomeType = 6 (Self-Empolyed) or
     * �  OccupationID = 6 (Self-Empolyed)
     * 
     * @return
     * True : Self-Employed
     * False : not Self-Employed
     */
    private boolean getIsSelfEmployed(Deal deal, SessionResourceKit srk)
    {
        SysLogger logger = srk.getSysLogger();
        //JdbcExecutor jExec = srk.getJdbcExecutor();
        boolean isSelfEmployed = false;
        // first by principal property
        try
        {
            Collection eHistories = new EmploymentHistory(srk, null)
                    .findAllCurrent(new DealPK(deal.getDealId(), deal.getCopyId()));
            for (Iterator iEH = eHistories.iterator(); iEH.hasNext();)
            {
                EmploymentHistory oneEHistory = (EmploymentHistory) iEH.next();
                //logger.debug("Billy ===> oneEHistory.getOccupationId() = " +
                // oneEHistory.getOccupationId());
                //logger.debug("Billy ===>
                // oneEHistory.getIncome().getIncomeTypeId() = " +
                // oneEHistory.getIncome().getIncomeTypeId());
                if (oneEHistory != null
                        && (oneEHistory.getOccupationId() == Sc.OCCUPATION_SELFEMPLOYED || (oneEHistory
                                .getIncome() != null && oneEHistory.getIncome()
                                .getIncomeTypeId() == Sc.INCOME_SELF_EMP)))
                {
                    //logger.debug("Billy ===> getIsSelfEmployed = true");
                    isSelfEmployed = true;
                    break;
                }
            }//for
        }
        catch (Exception e)
        {
            isSelfEmployed = false;
            logger.warning("WFE::WorkflowAssistBean:getIsSelfEmployed: err = "
                    + e.getMessage());
        }
        //logger.debug("Billy ===> getIsSelfEmployed return = " +
        // isSelfEmployed);
        return isSelfEmployed;
    }

    /**
     * Given a deal entity determine origination BorrowerLastName as:
     * 
     * �  The BorrowerLastName of the primary borrower in the Deal
     * 
     * @return
     * BorrowerLastName or &quot;&quot; if could not be determined (in latter case error messages are delivered
     * to the system log file).
     */
    private String getRouteBLastName(Deal deal, SessionResourceKit srk)
    {
        SysLogger logger = srk.getSysLogger();
        String theBLastName = "";
        // By primary Borrower only for the time being
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            theBLastName = borrower.getBorrowerLastName();
        }
        catch (Exception e)
        {
            theBLastName = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteBLastName: Could not locate LastName of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteBLastName: err = "
                    + e.getMessage());
        }
        return theBLastName;
    }

    /**
     * Given a deal entity determine origination PostalCode) as:
     * 
     * �  The PostalCode of the primary property in the Deal, if one, or
     * �  The PostalCode of the primary borrower in the Deal
     * 
     * @return
     * PostalCode or &quot;&quot; if could not be determined (in latter case error messages are delivered
     * to the system log file).
     */
    private String getRoutePostalCode(Deal deal, SessionResourceKit srk)
    {
        SysLogger logger = srk.getSysLogger();
        String thePostalCode = "";
        // first by primary property
        try
        {
            Property prop = new Property(srk, null);
            prop.setSilentMode(true);
            prop = prop.findByPrimaryProperty(deal.getDealId(), deal
                    .getCopyId(), deal.getInstitutionProfileId());
            thePostalCode = prop.getPropertyPostalFSA()
                    + prop.getPropertyPostalLDU();
        }
        catch (Exception e)
        {
            thePostalCode = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRoutePostalCode: Could not locate primary property on deal (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRoutePostalCode: err = "
                    + e.getMessage());
        }
        if (!thePostalCode.trim().equals("")) { return thePostalCode; }
        // Secondly by primary Borrower
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            BorrowerAddress bAddr = new BorrowerAddress(srk);
            bAddr = bAddr.findByCurrentAddress(borrower.getBorrowerId(), deal
                    .getCopyId());
            thePostalCode = bAddr.getAddr().getPostalFSA()
                    + bAddr.getAddr().getPostalLDU();
        }
        catch (Exception e)
        {
            thePostalCode = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRoutePostalCode: Could not locate address of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRoutePostalCode: err = "
                    + e.getMessage());
        }
        return thePostalCode;
    }

    /**
     * Given a deal entity determine origination AreaCode as:
     * 
     * �  The AreaCode of the primary borrower in the Deal
     * 
     * @return
     * AreaCode or &quot;&quot; if could not be determined (in latter case error messages are delivered
     * to the system log file).
     */
    private String getRouteAreaCode(Deal deal, SessionResourceKit srk)
    {
        SysLogger logger = srk.getSysLogger();
        String theAreaCode = "";
        // By primary Borrower only for the time being
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            theAreaCode = borrower.getBorrowerHomePhoneNumber().substring(0, 3);
        }
        catch (Exception e)
        {
            theAreaCode = "";
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteAreaCode: Could not locate areacode of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteAreaCode: err = "
                    + e.getMessage());
        }
        return theAreaCode;
    }

    /**
     * Given a deal entity determine origination province (id) as:
     * 
     * �  The province of the principal property in the Deal, if one, or
     * �  The province of the current address of the principal borrower
     * 
     * @return
     * Province Id or -1 if could not be determined (in latter case error messages are delivered
     * to the system log file).
     */
    private int getRouteProvince(Deal deal, SessionResourceKit srk)
    {
        SysLogger logger = srk.getSysLogger();
        //JdbcExecutor jExec = srk.getJdbcExecutor();
        int theProvince = -1;
        // first by principal property
        try
        {
            Property prop = new Property(srk, null);
            prop.setSilentMode(true);
            prop = prop.findByPrimaryProperty(deal.getDealId(), deal
                    .getCopyId(), deal.getInstitutionProfileId());
            theProvince = prop.getProvinceId();
        }
        catch (Exception e)
        {
            theProvince = -1;
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteProvince: Could not locate primary property on deal (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteProvince: err = "
                    + e.getMessage());
        }
        if (theProvince != -1) { return theProvince; }
        // Then try get it from Primary Borrower
        try
        {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal
                    .getCopyId());
            BorrowerAddress bAddr = new BorrowerAddress(srk);
            bAddr = bAddr.findByCurrentAddress(borrower.getBorrowerId(), deal
                    .getCopyId());
            theProvince = bAddr.getAddr().getProvinceId();
        }
        catch (Exception e)
        {
            theProvince = -1;
            logger
                    .trace("WFE::WorkflowAssistBean:getRouteProvince: Could not locate province of primary borrower (ignoring)");
            logger.trace("WFE::WorkflowAssistBean:getRouteProvince: err = "
                    + e.getMessage());
        }
        return theProvince;
    }

    /**
     * Return the int value for the given string.
     */
    protected int getIntValue(String sNum)
    {
        if (sNum == null) { return 0; }
        sNum = sNum.trim();
        if (sNum.length() == 0) { return 0; }
        try
        {
            // handle decimal (e.g. float type) representation
            int ndx = sNum.indexOf(".");
            if (ndx != -1)
            {
                sNum = sNum.substring(0, ndx);
            }
            return Integer.parseInt(sNum.trim());
        }
        catch (Exception e)
        {
            ;
        }
        return 0;
    }

    /**
     */
    protected String trimAllSpaces(String input)
    {
        String tmp = "";
        int x = 0;
        for (x = 0; x < input.length(); x++)
        {
            char c = input.charAt(x);
            if ((c != ' '))
            {
                tmp += c;
            }
        }
        return tmp;
    }
}
