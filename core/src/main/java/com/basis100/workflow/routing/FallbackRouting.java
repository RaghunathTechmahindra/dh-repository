/*
 * @(#)FallbackRouting.java    2005-5-20
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.ArrayList;

import com.basis100.log.SysLogger;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

/**
 * FallbackRouting - 
 *
 * @version   1.0 2005-5-20
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class FallbackRouting extends AbstractUnderwriterRouting {

    // The logger
    private final static String LOG_HEADER = "WFE::FallbackRouting:";

    /**
     * Constructor function
     */
    public FallbackRouting() {
    }

    /**
     * Perform the finding.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles dup,
                                 SessionResourceKit srk) {

        List branches = new ArrayList();
        SysLogger logger = srk.getSysLogger();
        JdbcExecutor jExec = srk.getJdbcExecutor();
        String sql = null;
        sql = "Select branchProfileId from BRANCHPROFILE";
        logger.info(LOG_HEADER + "findUnderwriters: sql = " + sql);

        try {

            int key = jExec.execute(sql);
            for (; jExec.next(key);)
            {
                branches.add(new Integer(jExec.getInt(key, 1)));
            }
            jExec.closeData(key);
        } catch (Exception e) {

            branches = null;
            logger.error(LOG_HEADER +
                         "findUnderwriters: Error on query all branches");
            logger.error(LOG_HEADER + "findUnderwriters: err = "
                         + e.getMessage());
        }

        if (branches == null || branches.size() == 0) { return null; }
        int originationProvince = getOriginationProvince(deal, srk);
        List groups = groupsOptFromBranchesQualifyGeographically(false,
                branches, originationProvince, srk);
        // if no groups because no matching prov. then fall back to all groups
        // from branches
        if ((groups == null || groups.size() == 0) &&
            originationProvince != -1) {

            groups =
                groupsOptFromBranchesQualifyGeographically(false,
                                                           branches, -1, srk);
            // underwriters from groups
        }

        if (groups != null && groups.size() > 0) {

            List ids = underwritersFromGroups(groups, srk);
            if(ids != null) 
                logger.info(LOG_HEADER + "findUnderwriters: Found [" +
                            ids.size() + "] underwriters.");

            return ensureActiveProfiles(ids, srk, true, deal.getInstitutionProfileId());
        }

        return null;
    }
}
