/*
 * @(#)SourceFirm2GroupRouting.java    2005-5-20
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.Vector;

import com.basis100.deal.entity.Deal;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

//--> Ticket#135 : Ensure to hunt for Admin and Funder with the SOB/Firm
// group
//--> The assigned GroupId from Frim to Group relationship will be stored
// in the dealUserProfiles
//--> By Billy 27Nov2003
/**
 * SourceFirm2GroupRouting is trying to find the availabel underwriters based on
 * the association between SourceFirm and Gorup(s)
 *
 * Called during underwriter assignment to a deal. This method operates on the following
 * associative relationships in the MOS schema:
 * 
 *  . Source Firm (broker) to Group(s) Association
 * 
 * @version   1.0 2005-5-20
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class SourceFirm2GroupRouting extends AbstractUnderwriterRouting {

    // The logger
    private final static String LOG_HEADER = "WFE::SourceFirm2GroupRouting:";

    /**
     * Constructor function
     */
    public SourceFirm2GroupRouting() {
    }

    /**
     * perform the finding.
     */
    public List findUnderwriters(Deal deal, DealUserProfiles dup,
                                 SessionResourceKit srk) {

        SysLogger logger = srk.getSysLogger();
        int sfId = deal.getSourceFirmProfileId();
        if (sfId <= 0) return null;

        Vector underwriters = new Vector();
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT groupProfileId FROM SourceFirmToGroupAssoc ").
            append("WHERE sourceFirmProfileId = ").
            append(sfId);
        logger.info(LOG_HEADER + "findUnderwriters: sql = " +
                    sql.toString());

        JdbcExecutor jExec = srk.getJdbcExecutor();
        int groupProfileId = -1;
        try {
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {

                groupProfileId = jExec.getInt(key, 1);
                Vector allUserProfilesInGroup =
                    _userProfileQuery.
                    getGroupUnderwriters(groupProfileId, -1, false, srk, deal.getInstitutionProfileId());
                underwriters.addAll(this.ensureActiveProfiles2(
                        allUserProfilesInGroup, srk, true, deal.getInstitutionProfileId()));
            }
            jExec.closeData(key);
        } catch (Exception e) {

            logger
                .error("WFE::WorkflowAssistBean:performFirmGroupMapping: Error on query Source Firm to Group association");
            logger
                .error("WFE::WorkflowAssistBean:performFirmGroupMapping: err = "
                       + e);
        }

        if (underwriters != null && underwriters.size() > 0) {

            logger.info(LOG_HEADER + "findUnderwriters: found underwriter amount [" +
                        underwriters.size() + "]");
            dup.setSourceGroupId(groupProfileId);
        } else {
            // Just in case
            logger.info(LOG_HEADER + "findUnderwriters: found nothing!");
            dup.setSourceGroupId(-1);
        }

        return underwriters;
    }
}
