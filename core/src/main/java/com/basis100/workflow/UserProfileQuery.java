
package com.basis100.workflow;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

/**
 * This class contains the user profile query functions of the Workflow Engine.
 *
 * Method Summary:
 *
 * getUserProfile()
 *
 * Get the user profile record for a specified user - method includes logic to locate (recursively)
 * active 'stand-in'.
 *
 * get2ndApproverUserProfile()
 *
 * Get the user profile record for a specified (assumed to be) 2nd approver - method includes logic
 * to locate (recursively) an active 2nd approver from the logical chain of 2nd approver records
 * where inactive user(s) are encountered.
 *
 * getLast2ndApproverUserProfile()
 *
 * Get the'last' 2nd approver in the logical chain of 2nd approver records starting with a specified
 * 2nd approver id. Implementation contains special optimization for stopping search where a user
 * along the chain is known to refer (point to) an active joint approver - the assumption is that the
 * caller is interested in locating a profile that 'points to' an active joint approver.
 *
 * getJointApproverUserProfile()
 *
 * Get the user profile record for a specified (assumed to be) joint approver - method includes logic
 * to locate (recursively) an active joint approver from the logical chain of joint approver records
 * where inactive user(s) are encountered.
 *
 * --------------------------------------------------------------------------------------------------
 *
 * getGroupUsers(int groupId, int userType, boolean activeOnly ...)
 * getGroupUsers(Vector groups, int userType, boolean activeOnly ...)
 *
 * getBranchUsers(int groupId, int userType, boolean activeOnly ...)
 * getBranchUsers(int groupId, int userType, boolean activeOnly ...)
 *
 * getRegionUsers(int groupId, int userType, boolean activeOnly ...)
 * getRegionUsers(int groupId, int userType, boolean activeOnly ...)
 *
 *
 * Query methods to return list of (optionally) active users. The intrepretation of the
 * group id is as follows:
 * .  getGroupUsers:  Users selected from specified group only
 * .  getBranchUsers:  Users selected from all groups in the branch that contains the specified group.
 * .  getRegionUsers:  Users selected from all branches that contains the specified group.
 *
 * getAdministrators(int groupId ..., boolean activeOnly...)
 * getGroupAdministrators(int groupId ..., boolean activeOnly...)
 * getBranchAdministrators(int groupId..., boolean activeOnly...)
 * getRegionAdministrators(int groupId..., boolean activeOnly...)
 *
 * Query methods to return list of (optionally) active administrators. The intrepretation of the
 * group id is as follows:
 * . getAdministrators:       searchs the group and then if necessary the branch that containing the group
 * . getGroupAdministrators:  searchs the group only
 * . getBranchAdministrators: search the branch containing the group (including specified group) directly
 *                            without first checking the group
 * . getRegionAdministrators: search the region containing the group (including the specified group)
 *                            directly without first checking the group
 *
 *
 * getUnderwriters(int groupId ..., boolean activeOnly...)
 * getGroupUnderwriters(int groupId ..., boolean activeOnly...)
 * getBranchUnderwriters(int groyupid..., boolean activeOnly...)
 * getRegionAdministrators(int groupId..., boolean activeOnly...)
 *
 * Query methods to return list of (optionally) active underwriters. The intrepretation of the
 * group id is as follows:
 * . getUnderwriters:       searchs the group and then if necessary the branch that containing the group
 * . getGroupUnderwriters:  searchs the group only
 * . getBranchUnderwriters: search the branch containing the group (including specified group) directly
 *                          without first checking the group
 * . getRegionUnderwriters: search the region containing the group (including the specified group)
 *                          directly without first checking the group
 *
 * getUsers(int groupId ..., boolean activeOnly...)
 *
 * Query method to return list of (optionally) active user by type. In a manner similar to
 * getAdministrators() and getUnderwriters() the method takes a group Id and internally
 * searches the group and then if necessary the branch containing the group.
 *
 * ---------------------------------------------------------------------------------------------------
 *
 * getGroupsWhereManagerOrSupervisor(int userId, SessionResourceKit srk)
 *
 * Returns a list of all groups that belong to all regions and branches where the specified user is the
 * manager and all groups where the users is the supervisor - a given group is guaranteed to occur only
 * once in the result list.
 *
 * Return null if the user is not a manager (of any region or branch) nor a supervisor (of any group).
 *
 * getUsersInGroups(HashSet groups, int userType, boolean activeOnly, SessionResourceKit srk)
 *
 * Query method to return list of (optionally) active users belonging to one of the (list of) groups
 * provided. The result is a list of user Ids for the qualifying users.
 *
 **/

public class UserProfileQuery
{
  private final static Logger logger = 
        LoggerFactory.getLogger(UserProfileQuery.class);
	
  // Values for the <userTypeCheck> argument of getUserProfile()
  public static final int ANY_USER_TYPE = 0; // no user type checking
  public static final int UNDERWRITER_USER_TYPE = 1; // ensure underwriter user type
  public static final int ADMIN_USER_TYPE = 2; // ensure administrator user type
  public static final int FUNDER_USER_TYPE = 3; // ensure administrator user type

  // Recursion type for profile query methods
  public static final int STAND_IN = 0; // stand-in user recursion
  public static final int SECOND_APPROVER = 1; // 2nd/Higher approver user recursion
  public static final int LAST_SECOND_APPROVER = 2; // Last 2nd/Higher approver user recursion
  public static final int JOINT_USER = 3; // Joint approver user recursion

  public UserProfileQuery()
  {
  }

  /**
   *   <pre>
   *   Return the profile record for a given user id.
   *
   *   <active> - true:  ensure user is 'active'
   *              false: return profile even if user not 'active'
   *
   *   <recurseStandIn> - true:   recursively select 'stand-in' when original profile is not 'active'
   *                      false:  examine only profile of original user
   *
   *   <userTypeCheck> - see class constants ANY_USER_TYPE, UNDERWRITER_USER_TYPE, ADMIN_USER_TYPE
   *
   *   @result
   *   Profile (entity). null if profile cannot be determined.
   *
   * IMPORTANT
   *
   * When active recursion is selected the method will transverse the stand-in chain if necessary
   * to locate an active profile. However, if no active profile is located along the chain then the
   * inactive profile at the end of the chain is returned. Hence the caller should check the returned
   * profile to ensure an active profile.
   *
   *   </pre>
   */

  public UserProfile getUserProfile(int userId, int instituionId, boolean active, boolean recurseStandIn, int userTypeCheck,
    SessionResourceKit srk)
  {
    if(userId == 0 || userId == -1)
    {
      return null;
    }

    Vector checkedAlready = null;

    if(active == true && recurseStandIn == true)
    {
      checkedAlready = new Vector();

    }
    return getUserProfileInternal(userId, instituionId, checkedAlready, userTypeCheck, STAND_IN, srk);
  }

  /**
   *   <pre>
   *   Return the profile record for a given (2nd approver) user id; user passed is is expected to be
   *   used in 2nd (or Higher Authority) Approver role but this cannot be verified.
   *   The method ensure that the type of the selected user is a underwriter type.
   *
   *   <active> - true:  ensure user is 'active'
   *              false: return profile even if user not 'active'
   *
   *   <recurse> -  true:   recursively select '2nd approver' when original profile is not 'active'
   *               false:  examine only profile of original user
   *
   *   @result
   *   Profile (entity). null if profile cannot be determined.
   *
   * IMPORTANT
   *
   * When active recursion is selected the method will transverse the 2nd-approver chain if necessary
   * to locate an active profile. However, if no active profile is located along the chain then the
   * inactive profile at the end of the chain is returned. Hence the caller should check the returned
   * profile to ensure an active profile.
   *
   *   </pre>
   */

  public UserProfile get2ndApproverUserProfile(int userId, int institutionId, boolean active, boolean recurse, SessionResourceKit srk)
  {
    if(userId == 0 || userId == -1)
    {
      return null;
    }

    Vector checkedAlready = null;

    if(active == true && recurse == true)
    {
      checkedAlready = new Vector();

    }
    return getUserProfileInternal(userId, institutionId, checkedAlready, UNDERWRITER_USER_TYPE, SECOND_APPROVER, srk);
  }

  /**
   *   <pre>
   *   Return the profile record the 'last' 2nd approver starting with a given user id.
   *   The method ensure that the type of the selected user is a underwriter type.
   *
   *   In this call the user profile returned may be 'inactive'. This is because the caller likely
   *   interested in obtaining a joint approver from the profile returned. There is also some special
   *   optimizations related to the search in this case - see getUserProfileInternal for more detail.
   *
   *   @result
   *
   *   Profile (entity). null if profile cannot be determined.
   *   </pre>
   */

  public UserProfile getLast2ndApproverUserProfile(int userId, int institutionId, SessionResourceKit srk)
  {
    if(userId == 0 || userId == -1)
    {
      return null;
    }

    Vector checkedAlready = new Vector();

    return getUserProfileInternal(userId, institutionId, checkedAlready, UNDERWRITER_USER_TYPE, LAST_SECOND_APPROVER, srk);
  }

  /**
   *   <pre>
   *   Return the profile record for a given (joint approver) user id; user passed is is expected to be
   *   used in Joint Approver role but this cannot be verified. The method ensures that the type of the
   *   selected user is a underwriter type.
   *
   *   <active> - true:  ensure user is 'active'
   *              false: return profile even if user not 'active'
   *
   *   <recurse> -  true:  recursively select 'joint approver' when original profile is not 'active'
   *               false:  examine only profile of original user
   *
   *   @result
   *
   * IMPORTANT
   *
   * When active recursion is selected the method will transverse the joint-approver chain if necessary
   * to locate an active profile. However, if no active profile is located along the chain then the
   * inactive profile at the end of the chain is returned. Hence the caller should check the returned
   * profile to ensure an active profile.
   *
   *   Profile (entity). null if profile cannot be determined.
   *   </pre>
   */

  public UserProfile getJointApproverUserProfile(int userId, int institutionId, boolean active, boolean recurse, SessionResourceKit srk)
  {
    if(userId == 0 || userId == -1)
    {
      return null;
    }

    Vector checkedAlready = null;

    if(active == true && recurse == true)
    {
      checkedAlready = new Vector();

    }
    return getUserProfileInternal(userId, institutionId, checkedAlready, UNDERWRITER_USER_TYPE, JOINT_USER, srk);
  }

  /**
   * <pre>
   * Given a group (Id), user type id and flag for active profile return:
   *
   *  . list (of user profiles) of qualifying users in the group
   *
   *  . null, if no qualifying users located.
   *
   * Notes:
   *
   * . For an administrator or underwriter type the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   * </pre>
   **/

  public Vector getGroupUsersId(int groupId, int userType, boolean activeOnly, SessionResourceKit srk, int institutionId)
  {
    Vector users = null;

    try
    {
      UserProfile worker = new UserProfile(srk);

      users = worker.findByUserTypeInGroupId(groupId, userType, activeOnly, null,institutionId);
    }
    catch(Exception e)
    {
      users = null;
      logger.error("WFE::UserProfileQuery:getGroupUsers2: Error getting users in group Id = " + groupId);
    }

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }
  
  
  
  /**
   * <pre>
   * Given a group (Id), user type id and flag for active profile return:
   *
   *  . list (of user profiles) of qualifying users in the group
   *
   *  . null, if no qualifying users located.
   *
   * Notes:
   *
   * . For an administrator or underwriter type the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   * </pre>
   **/

  public Vector getGroupUsers(int groupId, int userType, boolean activeOnly, SessionResourceKit srk, int institutionId)
  {
    Vector users = null;

    //SysLogger logger = srk.getSysLogger();

    //JdbcExecutor jExec = srk.getJdbcExecutor();

    try
    {
      UserProfile worker = new UserProfile(srk);

      users = worker.findByUserTypeInGroup(groupId, userType, activeOnly, institutionId);
    }
    catch(Exception e)
    {
      users = null;
      logger.error("WFE::UserProfileQuery:getGroupUsers: Error getting users in group Id = " + groupId);
    }

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }
  
  

  /**
   * <pre>
   * Given a group (Id), user type id and flag for active profile return:
   *
   * . list of qualifying users from for the branch that contains the specified
   *   group (including those from the specified group), or,
   *
   * . null, if no qualifying users located.
   *
   * Notes:
   *
   * . For an administrator or underwriter type the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   * </pre>
   **/
  public Vector getBranchUsersByGroup(int groupId,int userType, boolean activeOnly, 
                                      SessionResourceKit srk,  int institutionId)
  {
    //SysLogger logger = srk.getSysLogger();

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String sql = null;

    // determine the branch
    int branchId = -1;
    try
    {
      sql = "Select branchProfileId from groupProfile where groupProfileId = " + groupId;

      int key = jExec.execute(sql);
      jExec.next(key);
      branchId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getBranchUsers: Error getting branch users: while locating branch for group Id = " +
        groupId);
      logger.error("WFE::UserProfileQuery:getBranchUsers: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getBranchUsers: err = " + e.getMessage());
      return null;
    }

    return getBranchUsers(branchId, userType, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Given a group (Id), user type id and flag for active profile return:
   *
   * . list of qualifying users from for the branch that contains the specified
   *   group (including those from the specified group), or,
   *
   * . null, if no qualifying users located.
   *   
   * Notes:
   *
   * . For an administrator or underwriter type the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   *   This method uses join one query to fetch group users
   * </pre>
   **/
  public Vector getBranchUsersByGroupId(int groupId,int userType, boolean activeOnly, 
                                      SessionResourceKit srk,  int institutionId)
  {
	  	Vector users = null;
	
	    try
	    {
	      UserProfile worker = new UserProfile(srk);

	      users = worker.findByUserTypeInGroupId(groupId, userType, activeOnly, null,institutionId);
	    }
	    catch(Exception e)
	    {
	      users = null;
	      logger.error("WFE::UserProfileQuery:getBranchUsersByGroupId: Error getting users in group Id = " + groupId);
	    }

	    if(users == null || users.size() == 0)
	    {
	      return null;
	    }

	    return users;
  }
  
  public Vector getBranchUsers(int branchId,  int userType, boolean activeOnly, 
                  SessionResourceKit srk, int institutionId)
  {
    //SysLogger logger = srk.getSysLogger();

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String sql = null;

    // determine all groups for branch
    Vector groups = new Vector();

    try
    {
      sql = "Select groupProfileId from groupProfile where branchProfileId = " + branchId 
          + " AND institutionProfileId = " + institutionId;

      int key = jExec.execute(sql);

      while(jExec.next(key))
      {
        groups.addElement(new Integer(jExec.getInt(key, 1)));
      }

      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getBranchUsers: Error getting groups for branch Id = " + branchId);
      logger.error("WFE::UserProfileQuery:getBranchUsers: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getBranchUsers: err = " + e.getMessage());
      return null;
    }

    // get all users for the groups
    Vector users = new Vector();

    int len = groups.size();

    for(int i = 0; i < len; ++i)
    {
      int grpId = ((Integer)groups.elementAt(i)).intValue();
      Vector userInGroup = getGroupUsers(grpId, userType, activeOnly, srk, institutionId);

      if(userInGroup != null)
      {
        int len2 = userInGroup.size();
        for(int j = 0; j < len2; ++j)
        {
          users.addElement(userInGroup.elementAt(j));
        }
      }
    }

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }

  /**
   * <pre>
   * Given a group (Id), user type id and flag for active profile return:
   *
   * . list of qualifying users from for the region that contains the specified
   *   group (including those from the specified group), or,
   *
   * . null, if no qualifying users located.
   *
   * Notes:
   *
   * . For an administrator or underwriter type the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   * </pre>
   **/
  public Vector getRegionUsersByGroup(int groupId, int userType, boolean activeOnly, 
                                      SessionResourceKit srk, int institutionId)
  {
    //SysLogger logger = srk.getSysLogger();

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String sql = null;

    // determine immediate branch to the group
    int branchId = -1;
    try
    {
      sql = "Select branchProfileId from groupProfile where groupProfileId = " + groupId;

      int key = jExec.execute(sql);
      jExec.next(key);
      branchId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getRegionUsers: Error getting branch users: while locating branch for group Id = " +
        groupId);
      logger.error("WFE::UserProfileQuery:getRegionUsers: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getRegionUsers: err = " + e.getMessage());
      return null;
    }

    // determine region to the branch
    int regionId = -1;
    try
    {
      sql = "Select regionProfileId from branchProfile where branchProfileId = " + branchId;

      int key = jExec.execute(sql);
      jExec.next(key);
      regionId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getRegionUsers: Error getting branch users: while locating region for branch Id = " +
        branchId);
      logger.error("WFE::UserProfileQuery:getRegionUsers: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getRegionUsers: err = " + e.getMessage());
      return null;
    }

    return getRegionUsers(regionId, userType, activeOnly, srk, institutionId);
  }

  public Vector getRegionUsers(int regionId, int userType, boolean activeOnly, SessionResourceKit srk, int institutionId)
  {
    SysLogger logger = srk.getSysLogger();

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String sql = null;

    // determine all branches for the region
    Vector branches = new Vector();

    try
    {
      sql = "Select branchProfileId from branchProfile where regionProfileId = " + regionId;

      int key = jExec.execute(sql);

      while(jExec.next(key))
      {
        branches.addElement(new Integer(jExec.getInt(key, 1)));
      }

      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getRegionUsers: Error getting branches for region Id = " + regionId);
      logger.error("WFE::UserProfileQuery:getRegionUsers: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getRegionUsers: err = " + e.getMessage());
      return null;
    }

    // determine all groups for the branches
    Vector groups = new Vector();

    for(int i = 0; i < branches.size(); ++i)
    {
      int branchid = -1;
      try
      {
        branchid = ((Integer)branches.elementAt(i)).intValue();
        sql = "Select groupProfileId from groupProfile where branchProfileId = " + branchid;
        int key = jExec.execute(sql);

        while(jExec.next(key))
        {
          groups.addElement(new Integer(jExec.getInt(key, 1)));
        }

        jExec.closeData(key);
      }
      catch(Exception e)
      {
        logger.error("WFE::UserProfileQuery:getRegionUsers: Error getting groups for branch Id = " + branchid);
        logger.error("WFE::UserProfileQuery:getRegionUsers: sql = " + sql);
        logger.error("WFE::UserProfileQuery:getRegionUsers: err = " + e.getMessage());
        return null;
      }
    }

    // get all users for the groups
    Vector users = new Vector();

    int len = groups.size();

    for(int i = 0; i < len; ++i)
    {
      int grpId = ((Integer)groups.elementAt(i)).intValue();
      Vector userInGroup = getGroupUsers(grpId, userType, activeOnly, srk, institutionId);

      if(userInGroup != null)
      {
        int len2 = userInGroup.size();
        for(int j = 0; j < len2; ++j)
        {
          users.addElement(userInGroup.elementAt(j));
        }
      }
    }

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }

  /**
   * <pre>
   * Given a group (Id), minimum administrator priviledge type (see Mc for administrator
   * user types, -1 = any administrator type) and flag for active profile return:
   *
   *  . list (of user profiles) of qualifying administrators in the group, or if
   *    none found,
   *
   *  . list of qualifying administrators for the branch that contains the specified
   *    group, or,
   *
   *  . null, if no qualifying administrators located.
   *
   * </pre>
   **/

  public Vector getAdministrators(int groupId, int adminType, boolean activeOnly, 
                                  SessionResourceKit srk, int institutionId)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getUsers(groupId, adminType, activeOnly, srk, institutionId);
  }

  // New Method to handle Funder User Type -- By BILLY 14Jan2002
  public Vector getFunders(int groupId, boolean activeOnly, SessionResourceKit srk, 
                              int institutionId)
  {
    return getUsers(groupId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Similar to getAdministrators() but limits search to group only; i.e. if no qualifying administrator
   * located in group then returns null without interrogating branch (group is located in).
   * </pre>
   **/

  public Vector getGroupAdministrators(int groupId, int adminType, boolean activeOnly, 
                                      SessionResourceKit srk, int institutionId)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getGroupUsers(groupId, adminType, activeOnly, srk, institutionId);
  }

  // New Method to handle Funder User Type -- By BILLY 14Jan2002
  public Vector getGroupFunders(int groupId, boolean activeOnly, SessionResourceKit srk, int institutionId)
  {
    return getGroupUsers(groupId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Similar to getAdministrators() but searchs at branch (group belongs to) level immediately  without
   * first checking group (only).
   * </pre>
   **/

  public Vector getBranchAdministratorsByGroup(int groupId, int institutionId, int adminType, boolean activeOnly, SessionResourceKit srk)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getBranchUsersByGroup(groupId, adminType, activeOnly, srk, institutionId);
  }

  public Vector getBranchAdministrators(int branchId, int adminType, boolean activeOnly, 
                  SessionResourceKit srk, int institutionId)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getBranchUsers(branchId, adminType, activeOnly, srk, institutionId);
  }

  // New Method to handle Funder User Type -- By BILLY 14Jan2002
  public Vector getBranchFundersByGroup(int groupId, boolean activeOnly, SessionResourceKit srk, int institutionId )
  {
    return getBranchUsersByGroup(groupId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  public Vector getBranchFunders(int branchId, boolean activeOnly, SessionResourceKit srk,  int institutionId )
  {
    return getBranchUsers(branchId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Similar to getAdministrators() but searchs at region (group belongs to) level immediately  without
   * first checking group (only).
   * </pre>
   **/

  public Vector getRegionAdministratorsByGroup(int groupId, int adminType, 
                                              boolean activeOnly, SessionResourceKit srk,
                                              int institutionId)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getRegionUsersByGroup(groupId, adminType, activeOnly, srk, institutionId);
  }

  public Vector getRegionAdministrators(int regionId, int adminType, boolean activeOnly,
                                          SessionResourceKit srk, int institutionId)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getRegionUsers(regionId, adminType, activeOnly, srk, institutionId);
  }

  // New Method to handle Funder User Type -- By BILLY 14Jan2002
  public Vector getRegionFundersByGroup(int groupId, int adminType, boolean activeOnly, 
                  SessionResourceKit srk, int institutionId)
  {
    return getRegionUsersByGroup(groupId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  public Vector getRegionFunders(int regionId, int adminType, boolean activeOnly, 
                  SessionResourceKit srk, int institutionId)
  {
    return getRegionUsers(regionId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Given a group (Id), minimum underwriter priviledge type (see Mc for user types, -1 = any underwriter)
   * return:
   *
   *  . list (of user profiles) of qualifying underwriters in the group, or if
   *    none found,
   *
   *  . list of qualifying underwriters for the branch that contains the specified group, or,
   *
   *  . null, if no qualifying underwriters located.
   *
   * </pre>
   **/

  public Vector getUnderwriters(int groupId, int underwriterType, boolean activeOnly, 
                                  SessionResourceKit srk, int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getUsers(groupId, underwriterType, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Similar to getUnderwriters() but limits search to group only; i.e. if no qualifying underwriter
   * located in group then returns null without interrogating branch (group is located in).
   * </pre>
   **/

  public Vector getGroupUnderwriters(int groupId, int underwriterType, 
                              boolean activeOnly, SessionResourceKit srk, int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getGroupUsers(groupId, underwriterType, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Similar to getUnderwriters() but searches at branch (group belongs to) level immediately  without
   * first checking group (only).
   * </pre>
   **/

  public Vector getBranchUnderwritersByGroup(int groupId, int underwriterType, boolean activeOnly, 
                                              SessionResourceKit srk, int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getBranchUsersByGroup(groupId, underwriterType, activeOnly, srk, institutionId);
  }

  public Vector getBranchUnderwriters(int branchId, int underwriterType, 
                                      boolean activeOnly, SessionResourceKit srk,
                                      int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getBranchUsers(branchId, underwriterType, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Similar to getUnderwriters() but searches at branch (group belongs to) level immediately  without
   * first checking group (only).
   * </pre>
   **/

  public Vector getRegionUnderwritersByGroup(int groupId, int underwriterType, 
                                              boolean activeOnly, SessionResourceKit srk,
                                              int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getRegionUsersByGroup(groupId, underwriterType, activeOnly, srk, institutionId);
  }

  public Vector getRegionUnderwriters(int regionId, int underwriterType, 
                                      boolean activeOnly, SessionResourceKit srk,
                                      int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getRegionUsers(regionId, underwriterType, activeOnly, srk, institutionId);
  }

  /**
   * <pre>
   * Given a group (Id), user type id and flag for active profile return:
   *
   *  . list (of user profiles) of qualifying users in the group, or if none found,
   *
   *  . list of qualifying users from the branch that contains the specified group, or if not found,
   *
   *  . list of qualifying users from the region that contains the specified branch, or if not found,
   *
   *  . null, if no qualifying users located.
   *
   * Notes:
   * . For an administrator or underwriter type the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   * </pre>
   **/

  public Vector getUsers(int groupId, int userType, boolean activeOnly, 
                          SessionResourceKit srk, int institutionId)
  {
    Vector users = getGroupUsers(groupId, userType, activeOnly, srk, institutionId);

    if(users == null || users.size() == 0)
    {
      
      //users = getBranchUsersByGroup(groupId, userType, activeOnly, srk, institutionId);
      users = getBranchUsersByGroupId(groupId, userType, activeOnly, srk, institutionId);
      
      // Modified to extend the up to Region level -- By BILLY 05Feb2002
    }
    if(users == null || users.size() == 0)
    {
      users = getRegionUsersByGroup(groupId, userType, activeOnly, srk, institutionId);
    }

    //--> Cervus II : Added new AUTO usertype (AME spec. section 3.2.1)
    //--> By Billy 28Sept2004
    // if the required UserType = Auto, here we look for any Active Auto User
    // in the system. As Auto user is allowed to set to GroupID = 0 (unassigned)
    if((users == null || users.size() == 0) && userType == Mc.USER_TYPE_AUTO)
    {
      users = getAnyUsers(userType, activeOnly, srk);
    }
    //=================================================================

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }

  /**
   * <pre>
   * Given a user Id return a list of all groups that belong to all regions and branches where the
   * specified user is the manager and all groups where the users is the supervisor - a given group is
   * guaranteed to occur only once in the result list.
   *
   * Return null if the user is not a manager (or supervisor), or if an exception; in latter case the
   * incident is logged to the file based system log file.
   *
   * Result: Hashtable where:
   *
   *         Key   = Group Id (Integer)
   *
   *         Value = Group Details = String[] where
   *
   *                 [0]  - region id  - region containing group (string rep.)
   *                 [1]  - region name
   *                 [2]  - region short name
   *                 [3]  - region business id
   *                 [4]  - region profile status
   *
   *                 [5]  - branch id - branch containing group (string rep.)
   *                 [6]  - branch name
   *                 [7]  - branch short name
   *                 [8]  - branch business id
   *                 [9]  - branch profile status
   *
   *                 [10] - group id (matches key) (string rep)
   *                 [11] - group name
   *                 [12] - group short name
   *                 [13] - group business id
   *                 [14] - group profile status
   *
   * </pre>
   **/

  public Hashtable getGroupsWhereManagerOrSupervisor(int userId, SessionResourceKit srk)
  {
    SysLogger logger = srk.getSysLogger();

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String sql = null;

    Hashtable groupsList = new Hashtable();

    Vector regions = new Vector();
    Vector regionDetails = new Vector();

    try
    {
      sql =
        "Select regionProfileId, regionName, rpShortName, rpBusinessId, profileStatusId from regionProfile where regionManagerUserId = " +
        userId;
      int key = jExec.execute(sql);
      while(jExec.next(key))
      {
        String[] regionDetail = new String[5];

        int regionId = jExec.getInt(key, 1);

        regionDetail[0] = "" + regionId;
        regionDetail[1] = jExec.getString(key, 2);
        regionDetail[2] = jExec.getString(key, 3);
        regionDetail[3] = jExec.getString(key, 4);
        regionDetail[4] = "" + jExec.getInt(key, 5);

        regions.addElement(new Integer(regionId));
        regionDetails.addElement(regionDetail);
      }
      jExec.closeData(key);

      for(int i = 0; i < regions.size(); ++i)
      {
        groupsInRegion(groupsList, ((Integer)regions.elementAt(i)).intValue(), (String[])regionDetails.elementAt(i), jExec);
      }
    }
    catch(Exception e)
    {
      logger.error(
        "WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: Error getting groups where user manager or supervisor: user Id  = " +
        userId);
      logger.error("WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: err = " + e.getMessage());
      return null;
    }

    // --- //

    Vector branchList = new Vector();
    Vector branchDetails = new Vector();

    try
    {
      sql = "Select branchProfileId, branchName, bpShortName, bpBusinessId, profileStatusId, regionProfileId from branchProfile where branchManagerUserId = " +
        userId;

      int key = jExec.execute(sql);
      while(jExec.next(key))
      {
        String[] branchDetail = new String[10];

        int branchId = jExec.getInt(key, 1);

        branchDetail[5] = "" + branchId;
        branchDetail[6] = jExec.getString(key, 2);
        branchDetail[7] = jExec.getString(key, 3);
        branchDetail[8] = jExec.getString(key, 4);
        branchDetail[9] = "" + jExec.getInt(key, 5);

        branchList.addElement(new Integer(branchId));
        branchDetails.addElement(branchDetail);

        // determine and set region details (if avialable - previous query)
        setRegionDetails(jExec.getInt(key, 6), branchDetail, regions, regionDetails);
      }
      jExec.closeData(key);

      for(int i = 0; i < branchList.size(); ++i)
      {
        groupsInBranch(groupsList, ((Integer)branchList.elementAt(i)).intValue(), (String[])branchDetails.elementAt(i), jExec);
      }
    }
    catch(Exception e)
    {
      logger.error(
        "WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: Error getting groups where user manager or supervisor: user Id  = " +
        userId);
      logger.error("WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: err = " + e.getMessage());
      return null;
    }

    // --- //

    try
    {
      sql = "Select groupProfileId, groupName, profileStatusId, branchProfileId from groupProfile where supervisorUserId = " +
        userId;

      int key = jExec.execute(sql);

      while(jExec.next(key))
      {
        int groupId = jExec.getInt(key, 1);
        Integer groupKey = new Integer(groupId);

        // check for group already present in groups list - if so it is assumed that the existing entry
        // contains more complete group detail

        if(groupsList.contains(groupKey) == false)
        {
          String[] groupDetail = new String[15];

          groupDetail[10] = "" + groupId;
          groupDetail[11] = jExec.getString(key, 2);
          groupDetail[12] = ""; // is no group short name
          groupDetail[13] = ""; // is no group business id
          groupDetail[14] = "" + jExec.getInt(key, 3);

          int branchId = jExec.getInt(key, 4);
          groupDetail[5] = "" + branchId;

          // determine and set branch/region details (if avialable - previous query)
          setBranchDetails(branchId, groupDetail, branchList, branchDetails);

          groupsList.put(new Integer(groupId), groupDetail);
        }
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error(
        "WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: Error getting groups where user manager or supervisor: user Id  = " +
        userId);
      logger.error("WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: err = " + e.getMessage());
      return null;
    }

    if(groupsList.size() == 0)
    {
      return null;
    }

    // fill out missing branch information as required

    try
    {
      Iterator iter = groupsList.values().iterator();

      while(iter.hasNext())
      {
        String[] groupDetail = (String[])iter.next();

        if(groupDetail[6] == null)
        {
          // missing branch information (since branch name is null)

          int branchId = (new Integer(groupDetail[5])).intValue();
          int key = jExec.execute("Select branchProfileId, branchName, bpShortName, bpBusinessId, profileStatusId, regionProfileId from branchProfile where branchProfileId = " +
            branchId);
          int regionId = 0;
          while(jExec.next(key))
          {
            groupDetail[6] = jExec.getString(key, 2);
            groupDetail[7] = jExec.getString(key, 3);
            groupDetail[8] = jExec.getString(key, 4);
            groupDetail[9] = "" + jExec.getInt(key, 5);

            regionId = jExec.getInt(key, 6);
            groupDetail[0] = "" + regionId;
            break;
          }
          jExec.closeData(key);

          // determine and set region details (if avialable - previous query)
          setRegionDetails(regionId, groupDetail, regions, regionDetails);
        }

        if(groupDetail[1] == null)
        {
          // missing region information (since region name is null)

          int regionId = (new Integer(groupDetail[0])).intValue();
          int key = jExec.execute(
            "Select regionProfileId, regionName, rpShortName, rpBusinessId, profileStatusId from regionProfile where regionProfileId = " +
            regionId);
          while(jExec.next(key))
          {
            groupDetail[1] = jExec.getString(key, 2);
            groupDetail[2] = jExec.getString(key, 3);
            groupDetail[3] = jExec.getString(key, 4);
            groupDetail[4] = "" + jExec.getInt(key, 5);
            break;
          }
          jExec.closeData(key);
        }
      }
    }
    catch(Exception ex)
    {
      logger.error("WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: Error getting groups where user manager or supervisor (2): user Id  = " +
        userId);
      logger.error("WFE::UserProfileQuery:getGroupsWhereManagerOrSupervisor: err = " + ex.getMessage());
      return null;
    }

    return groupsList;
  }

  /**
   * <pre>
   * Given a list of groups (Ids), user type id and flag for active profile return:
   *
   *  . list (of user UserProfile entities) of qualifying users in the groups
   *
   *  . null, if no qualifying users located.
   *
   * Notes:
   *
   * . For an administrator or underwriter types the type represents a minimum priviledge
   *   type; i.e. if admininistrator then senior administrator also included but junior
   *   administrator excluded included.
   *
   *   For all other types the specific type is included only.
   *
   *   If -1 is provided for the user type no user type screening is performed - i.e. all user types
   *   qualify.
   *
   * </pre>
   **/

  public Vector getUsersInGroups(Hashtable groups, int userType, boolean activeOnly, 
                              SessionResourceKit srk, int institutionId)
  {
    SysLogger logger = srk.getSysLogger();

    //JdbcExecutor jExec = srk.getJdbcExecutor();

    if(groups == null)
    {
      return null;
    }

    int groupId = 0;
    Vector userIds = new Vector();

    try
    {
      Iterator iter = groups.values().iterator();

      while(iter.hasNext() == true)
      {
        String[] groupDetail = (String[])iter.next();

        groupId = (new Integer(groupDetail[10])).intValue();

        UserProfile worker = new UserProfile(srk);

        Vector users = worker.findByUserTypeInGroup(groupId, userType, activeOnly, institutionId);

        for(int i = 0; i < users.size(); ++i)
        {
          UserProfile up = (UserProfile)users.elementAt(i);

          up.setRegionId((new Integer(groupDetail[0])).intValue());
          up.setRegionName(groupDetail[1]);
          up.setRegionShortName(groupDetail[2]);
          up.setRegionBusinessId(groupDetail[3]);
          up.setRegionProfileStatusId((new Integer(groupDetail[4])).intValue());

          up.setBranchId((new Integer(groupDetail[5])).intValue());
          up.setBranchName(groupDetail[6]);
          up.setBranchShortName(groupDetail[7]);
          up.setBranchBusinessId(groupDetail[8]);
          up.setBranchProfileStatusId((new Integer(groupDetail[9])).intValue());

          up.setGroupId(groupId);
          up.setGroupName(groupDetail[11]);
          up.setGroupShortName(groupDetail[12]);
          up.setGroupBusinessId(groupDetail[13]);
          up.setGroupProfileStatusId((new Integer(groupDetail[14])).intValue());

          userIds.add(up);
        }
      }
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getUsersInGroups: Error getting users in group Id = " + groupId);
      logger.error(e);
      return null;
    }

    if(userIds.size() == 0)
    {
      return null;
    }

    return userIds;
  }

//
// PRIVATE
//

  /**
   *  <pre>
   *  Common routine for implementation of:
   *
   *      getUserProfile(), get2ndApproverUserProfile(), getLast2ndApproverUserProfile(),
   *      getJointApproverUserProfile()
   *
   *  @args
   *  <checkedAlready> - if a vector (i.e. not null) this indicates recursive checking for
   *                     active user(1) (via stand-in as necessary) enabled. The vector contents are
   *                     use to hold user Ids already checked to ensure endless loop not encountered.
   *  <recurseType>    - use type selection when recursive call made, values:
   *                     STAND_IN             - Stand-in
   *                     SECOND_APPROVER      - 2nd/higher approver
   *                     LAST_SECOND_APPROVER - Last 2nd/Higher approver
   *                     JOINT_USER           - Joint approver
   *
   *  (1) - When the recurse type is Last 2nd Approver the method ignores the active/inactive attribute
   *        or the profile. In this mode it seeks to locate the user profile record that is at the end of
   *        a 2nd approver chain.
   *
   *        The end of a 2nd Approver chain is the last profile in a chain of 2nd approver links (links
   *        established between profile records by values in <secondApproverUserId> attribute, hence the
   *        'last' does not point to a 'next' 2nd approver).
   *
   *        The method 'may' return a profile that is NOT the end of the 2nd approver chain if the
   *        following conditions are detected:
   *
   *        . (non-last) user in the 2nd approver chain has a joint approver set, and
   *        . the joint apporover is active.
   *
   * IMPORTANT
   *
   * When active recursion is selected the method will transverse a chain (e.g. second approver chain -
   * depends recursive type provided) to locate an active profile. However, if no active profile is located
   * along the chain then the inactive profile at the end of the chain is returned. Hence the caller
   * should check the returned profile to ensure an active profile; e.g. the caller may prefer to
   * use the inactive profile at the start of the chain rather than the one at the end - or take some
   * other application specific action.
   *
   *  </pre>
   */

  public UserProfile getUserProfileInternal(int userId, int institutionId, Vector checkedAlready, int userTypeCheck, int recurseType,
    SessionResourceKit srk)
  {
    // SysLogger logger = srk.getSysLogger();

    boolean activeRecursion = checkedAlready != null;

    // avoid endless loop (recursive call for id already checked)
    if(activeRecursion == true)
    {
      if(checkedAlready.contains(new Integer(userId)) == true)
      {
        // cycled around to an aready checked user - break out else endless loop
        return null;
      }
    }

    UserProfile userProfile = null;
    try
    {
      userProfile = new UserProfile(srk);
      // added instituionProfileId from ExpressState
      //TODO: review it works 
      userProfile = userProfile
              .findByPrimaryKey(new UserProfileBeanPK(userId, institutionId));
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getUserProfile: Error locating user profile for user Id = " + userId);
      logger.error("WFE::UserProfileQuery:getUserProfile: err = " + e.getMessage());
      return null;
    }

    // user type checking (if necessary)
    if(userTypeCheck != ANY_USER_TYPE)
    {
      int uType = userProfile.getUserTypeId();

      if(userTypeCheck == UNDERWRITER_USER_TYPE)
      {
        // ensure underwriter type
        if(!(uType == Mc.USER_TYPE_JR_UNDERWRITER || uType == Mc.USER_TYPE_UNDERWRITER ||
          (uType >= Mc.USER_TYPE_SR_UNDERWRITER && uType < Mc.USER_TYPE_SYS_ADMINISTRATOR)))
        {
          logger.error("WFE::UserProfileQuery:getUserProfile: Error. User Id = " +
            userId + ", user type = " + uType + ": not an underwriter as expected.");

          return null;
        }
      }
      else if(userTypeCheck == this.ADMIN_USER_TYPE)
      {
        // ensure administrator type
        if(!(uType == Mc.USER_TYPE_JR_ADMIN || uType == Mc.USER_TYPE_ADMIN || uType == Mc.USER_TYPE_SR_ADMIN))
        {
          logger.error("WFE::UserProfileQuery:getUserProfile: Error. User Id = " +
            userId + ", user type = " + uType + ": not an administrator as expected.");
          return null;
        }
      }
      else if(userTypeCheck == FUNDER_USER_TYPE)
      {
        // ensure administrator type
        if(!(uType == Mc.USER_TYPE_FUNDER))
        {
          logger.error("WFE::UserProfileQuery:getUserProfile: Error. User Id = " +
            userId + ", user type = " + uType + ": not an Funder as expected.");
          return null;
        }
      }
    }

    // check special condition when 'last' 2nd approver requested - user along chain has active
    // joint approver set

    if(recurseType == LAST_SECOND_APPROVER)
    {
      int jointUserId = userProfile.getJointApproverUserId();
      if(!(jointUserId == 0 || jointUserId == -1))
      {
        try
        {
          // get the joint user profile record and ensure is active
          UserProfile jointUser = getUserProfile(jointUserId, institutionId, true, false, UNDERWRITER_USER_TYPE, srk);

          if(jointUser != null)
          {
            return userProfile; // special conditions met - no need to chack to end of chain
          }
        }
        catch(Exception e)
        {
          ;
        } // don't report as this is special handling allowed to fail
      }
    }

    if(activeRecursion != true)
    {
      return userProfile;
    }

    // ensure user is active (if possible)
    int nextUserId = -1;

    if(recurseType == LAST_SECOND_APPROVER || userProfile.getProfileStatusId() != Sc.PROFILE_STATUS_ACTIVE)
    {
      // user not active (or recursive to Last 2nd Approver) - recursively check for active user ...

      switch(recurseType)
      {
        case STAND_IN:

          // stand-in user
          nextUserId = userProfile.getStandInUserId();
          break;

        case SECOND_APPROVER:
        case LAST_SECOND_APPROVER:

          // 2nd/higher approver
          nextUserId = userProfile.getSecondApproverUserId();
          break;

        case JOINT_USER:

          // joint approver
          nextUserId = userProfile.getJointApproverUserId();
          break;

        default:
          logger.error(
            "WFE::UserProfileQuery:getUserProfile: Error. Recursive Type not stand-in, 2nd approver, or joint approver");
          return null;
      }
    }

    // Bug fixed -- if nextUser=0 stop looking --  By BILLY 19Feb2002
    if(nextUserId <= 0)
    {
      if(userProfile.getProfileStatusId() == Sc.PROFILE_STATUS_ACTIVE)
      {
        return userProfile;
      }
      else
      {
        return null;
      }
    }

    // have nect user ...

    // add current user to already checked list
    checkedAlready.addElement(new Integer(userId));

    // call recursive for next user
    return getUserProfileInternal(nextUserId, institutionId, checkedAlready, userTypeCheck, recurseType, srk);
  }

  private void groupsInRegion(Hashtable groupsList, int regionId, String[] regionDetail, JdbcExecutor jExec) throws Exception
  {
    Vector branchList = new Vector();
    Vector branchDetails = new Vector();

    int key = jExec.execute(
      "Select branchProfileId, branchName, bpShortName, bpBusinessId, profileStatusId from branchProfile where regionProfileId = " +
      regionId);
    while(jExec.next(key))
    {
      String[] branchDetail = new String[10];

      for(int i = 0; i < 5; ++i)
      {
        branchDetail[i] = regionDetail[i];

      }
      int branchId = jExec.getInt(key, 1);

      branchDetail[5] = "" + branchId;
      branchDetail[6] = jExec.getString(key, 2);
      branchDetail[7] = jExec.getString(key, 3);
      branchDetail[8] = jExec.getString(key, 4);
      branchDetail[9] = "" + jExec.getInt(key, 5);

      branchList.addElement(new Integer(branchId));
      branchDetails.addElement(branchDetail);
    }
    jExec.closeData(key);

    for(int i = 0; i < branchList.size(); ++i)
    {
      groupsInBranch(groupsList, ((Integer)branchList.elementAt(i)).intValue(), (String[])branchDetails.elementAt(i), jExec);
    }
  }

  private void groupsInBranch(Hashtable groupsList, int branchId, String[] branchDetail, JdbcExecutor jExec) throws Exception
  {
    int key = jExec.execute("Select groupProfileId, groupName, profileStatusId from groupProfile where branchProfileId = " +
      branchId);

    while(jExec.next(key))
    {
      int groupId = jExec.getInt(key, 1);
      Integer groupKey = new Integer(groupId);

      // check for group already present in groups list - if so it is assumed that the existing entry
      // contains more complete group detail

      if(groupsList.contains(groupKey) == false)
      {
        String[] groupDetail = new String[15];

        for(int i = 0; i < 10; ++i)
        {
          groupDetail[i] = branchDetail[i];

        }
        groupDetail[10] = "" + groupId;
        groupDetail[11] = jExec.getString(key, 2);
        groupDetail[12] = ""; // is no group short name
        groupDetail[13] = ""; // is no group business id
        groupDetail[14] = "" + jExec.getInt(key, 3);

        groupsList.put(new Integer(groupId), groupDetail);
      }
    }
    jExec.closeData(key);
  }

  private void setRegionDetails(int regionId, String[] branchDetail, Vector regions, Vector regionDetails)
  {
    int ndx = regions.indexOf(new Integer(regionId));

    if(ndx == -1)
    {
      return;
    }

    String[] regionDetail = (String[])regionDetails.elementAt(ndx);

    for(int i = 0; i < 5; ++i)
    {
      branchDetail[i] = regionDetail[i];
    }
  }

  private void setBranchDetails(int branchId, String[] groupDetail, Vector branches, Vector branchDetails)
  {
    int ndx = branches.indexOf(new Integer(branchId));

    if(ndx == -1)
    {
      return;
    }

    String[] branchDetail = (String[])branchDetails.elementAt(ndx);

    // actually takes region info as well since should have been set if available
    for(int i = 0; i < 10; ++i)
    {
      groupDetail[i] = branchDetail[i];
    }
  }

  // New method to check and return the UserGroupTypeId
  public int getInternalUserTypeId(int userId, int institutionId, SessionResourceKit srk)
  {
    // SysLogger logger = srk.getSysLogger();
    UserProfile userProfile = null;
    try
    {
      userProfile = new UserProfile(srk);
      userProfile = userProfile
                .findByPrimaryKey(new UserProfileBeanPK(userId, institutionId));
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getInternalUserTypeId: Error locating user profile for user Id = " + userId);
      logger.error("WFE::UserProfileQuery:getInternalUserTypeId: err = " + e.getMessage());
      return ANY_USER_TYPE;
    }

    int uType = userProfile.getUserTypeId();

    return(getInternalUserTypeId(uType));
  }

  public int getInternalUserTypeId(int uType)
  {
    if((uType == Mc.USER_TYPE_JR_UNDERWRITER || uType == Mc.USER_TYPE_UNDERWRITER ||
      (uType >= Mc.USER_TYPE_SR_UNDERWRITER && uType < Mc.USER_TYPE_SYS_ADMINISTRATOR)))
    {
      return UNDERWRITER_USER_TYPE;
    }

    if((uType == Mc.USER_TYPE_JR_ADMIN || uType == Mc.USER_TYPE_ADMIN || uType == Mc.USER_TYPE_SR_ADMIN))
    {
      return ADMIN_USER_TYPE;
    }

    // Added Funder user type -- By BILLY 15Jan2002
    if((uType == Mc.USER_TYPE_FUNDER))
    {
      return this.FUNDER_USER_TYPE;
    }

    return ANY_USER_TYPE;
  }

  // New Methods for getting UserProfile by Institution profile ID
  //  -- By Billy 21Jan2002
  public Vector getInstitutionAdministratorsByGroup(int groupId, int adminType, boolean activeOnly, SessionResourceKit srk)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getInstitutionUsersByGroup(groupId, adminType, activeOnly, srk);
  }

  public Vector getInstitutionAdministrators(int institutionId, int adminType, boolean activeOnly, SessionResourceKit srk)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getInstitutionUsers(institutionId, adminType, activeOnly, srk);
  }

  // New Method to handle Funder User Type -- By BILLY 14Jan2002
  public Vector getInstitutionFundersByGroup(int groupId, int adminType, boolean activeOnly, SessionResourceKit srk)
  {
    return getInstitutionUsersByGroup(groupId, Mc.USER_TYPE_FUNDER, activeOnly, srk);
  }

  public Vector getInstitutionFunders(int institutionId, int adminType, boolean activeOnly, SessionResourceKit srk)
  {
    return getInstitutionUsers(institutionId, Mc.USER_TYPE_FUNDER, activeOnly, srk);
  }

  public Vector getInstitutionUnderwritersByGroup(int groupId, int underwriterType, boolean activeOnly, SessionResourceKit srk)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getInstitutionUsersByGroup(groupId, underwriterType, activeOnly, srk);
  }

  public Vector getInstitutionUnderwriters(int institutionId, int underwriterType, boolean activeOnly, SessionResourceKit srk)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getInstitutionUsers(institutionId, underwriterType, activeOnly, srk);
  }

  public Vector getInstitutionUsersByGroup(int groupId, int userType, boolean activeOnly, SessionResourceKit srk)
  {
    SysLogger logger = srk.getSysLogger();

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String sql = null;

    // determine immediate branch to the group
    int branchId = -1;
    try
    {
      sql = "Select branchProfileId from groupProfile where groupProfileId = " + groupId;

      int key = jExec.execute(sql);
      jExec.next(key);
      branchId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error(
        "WFE::UserProfileQuery:getInstitutionUsersByGroup: Error getting users: while locating branch for group Id = " +
        groupId);
      logger.error("WFE::UserProfileQuery:getInstitutionUsersByGroup: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getInstitutionUsersByGroupnUsers: err = " + e.getMessage());
      return null;
    }

    // determine region to the branch
    int regionId = -1;
    try
    {
      sql = "Select regionProfileId from branchProfile where branchProfileId = " + branchId;

      int key = jExec.execute(sql);
      jExec.next(key);
      regionId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error(
        "WFE::UserProfileQuery:getInstitutionUsersByGroup: Error getting users: while locating region for branch Id = " +
        branchId);
      logger.error("WFE::UserProfileQuery:getInstitutionUsersByGroup: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getInstitutionUsersByGroup: err = " + e.getMessage());
      return null;
    }

    // determine Institution to the Region
    int institutionId = -1;
    try
    {
      sql = "Select institutionProfileId from regionProfile where regionprofileid = " + regionId;

      int key = jExec.execute(sql);
      jExec.next(key);
      regionId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error(
        "WFE::UserProfileQuery:getInstitutionUsersByGroup: Error getting users: while locating institutionProfile for region Id = " +
        branchId);
      logger.error("WFE::UserProfileQuery:getInstitutionUsersByGroup: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getInstitutionUsersByGroup: err = " + e.getMessage());
      return null;
    }

    return getInstitutionUsers(institutionId, userType, activeOnly, srk);
  }

  public Vector getInstitutionUsers(int institutionId, int userType, boolean activeOnly, SessionResourceKit srk)
  {
    // SysLogger logger = srk.getSysLogger();
    JdbcExecutor jExec = srk.getJdbcExecutor();
    String sql = null;

    // determine all regions for the institution
    Vector regions = new Vector();
    try
    {
      sql = "Select regionprofileid from regionProfile where  institutionProfileId = " + institutionId;
      int key = jExec.execute(sql);
      while(jExec.next(key))
      {
        regions.addElement(new Integer(jExec.getInt(key, 1)));
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getInstitutionUsers: Error getting regionprofileid for institutionProfile Id = " +
        institutionId);
      logger.error("WFE::UserProfileQuery:getInstitutionUsers: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getInstitutionUsers: err = " + e.getMessage());
      return null;
    }

    // determine all branches for the region
    Vector branches = new Vector();
    for(int i = 0; i < regions.size(); ++i)
    {
      int regionId = -1;
      try
      {
        regionId = ((Integer)regions.elementAt(i)).intValue();
        sql = "Select branchProfileId from branchProfile where regionProfileId = " + regionId;
        int key = jExec.execute(sql);
        while(jExec.next(key))
        {
          branches.addElement(new Integer(jExec.getInt(key, 1)));
        }
        jExec.closeData(key);
      }
      catch(Exception e)
      {
        logger.error("WFE::UserProfileQuery:getInstitutionUsers: Error getting branches for region Id = " + regionId);
        logger.error("WFE::UserProfileQuery:getInstitutionUsers: sql = " + sql);
        logger.error("WFE::UserProfileQuery:getInstitutionUsers: err = " + e.getMessage());
        return null;
      }
    }

    // determine all groups for the branches
    Vector groups = new Vector();
    for(int i = 0; i < branches.size(); ++i)
    {
      int branchid = -1;
      try
      {
        branchid = ((Integer)branches.elementAt(i)).intValue();
        sql = "Select groupProfileId from groupProfile where branchProfileId = " + branchid;
        int key = jExec.execute(sql);
        while(jExec.next(key))
        {
          groups.addElement(new Integer(jExec.getInt(key, 1)));
        }
        jExec.closeData(key);
      }
      catch(Exception e)
      {
        logger.error("WFE::UserProfileQuery:getInstitutionUsers: Error getting groups for branch Id = " + branchid);
        logger.error("WFE::UserProfileQuery:getInstitutionUsers: sql = " + sql);
        logger.error("WFE::UserProfileQuery:getInstitutionUsers: err = " + e.getMessage());
        return null;
      }
    }

    // get all users for the groups
    Vector users = new Vector();

    int len = groups.size();

    for(int i = 0; i < len; ++i)
    {
      int grpId = ((Integer)groups.elementAt(i)).intValue();
      Vector userInGroup = getGroupUsers(grpId, userType, activeOnly, srk, institutionId);

      if(userInGroup != null)
      {
        int len2 = userInGroup.size();
        for(int j = 0; j < len2; ++j)
        {
          users.addElement(userInGroup.elementAt(j));
        }
      }
    }

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }

  // New Methods for getting UserProfile by Lender profile ID
  //  -- By Billy 21Jan2002
  public Vector getLenderAdministratorsByGroup(int groupId, int adminType, boolean activeOnly, 
                                              SessionResourceKit srk, int institutionId)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getLenderUsersByGroup(groupId, adminType, activeOnly, srk, institutionId);
  }

  public Vector getLenderAdministrators(int lenderId, int adminType, boolean activeOnly, 
                                      SessionResourceKit srk, int institutionId)
  {
    if(adminType == -1)
    {
      adminType = Mc.USER_TYPE_JR_ADMIN; // any

    }
    return getLenderUsers(lenderId, adminType, activeOnly, srk, institutionId);
  }

  public Vector getLenderFundersByGroup(int groupId, int adminType, boolean activeOnly, 
                                          SessionResourceKit srk, int institutionId)
  {
    return getLenderUsersByGroup(groupId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  public Vector getLenderFunders(int lenderId, int adminType, boolean activeOnly, 
                                  SessionResourceKit srk, int institutionId)
  {
    return getLenderUsers(lenderId, Mc.USER_TYPE_FUNDER, activeOnly, srk, institutionId);
  }

  public Vector getLenderUnderwritersByGroup(int groupId, int underwriterType, boolean activeOnly, 
                                              SessionResourceKit srk, int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getLenderUsersByGroup(groupId, underwriterType, activeOnly, srk, institutionId);
  }

  public Vector getLenderUnderwriters(int lenderId, int underwriterType, boolean activeOnly, 
                                      SessionResourceKit srk, int institutionId)
  {
    if(underwriterType == -1)
    {
      underwriterType = Mc.USER_TYPE_JR_UNDERWRITER; // any

    }
    return getLenderUsers(lenderId, underwriterType, activeOnly, srk, institutionId);
  }

  public Vector getLenderUsersByGroup(int groupId, int userType, boolean activeOnly, 
                                      SessionResourceKit srk, int institutionId)
  {
    // SysLogger logger = srk.getSysLogger();

    JdbcExecutor jExec = srk.getJdbcExecutor();

    String sql = null;

    // determine immediate branch to the group
    int branchId = -1;
    try
    {
      sql = "Select branchProfileId from groupProfile where groupProfileId = " + groupId;

      int key = jExec.execute(sql);
      jExec.next(key);
      branchId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getLenderUsersByGroup: Error getting users: while locating branch for group Id = " +
        groupId);
      logger.error("WFE::UserProfileQuery:getLenderUsersByGroup: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getLenderUsersByGroup: err = " + e.getMessage());
      return null;
    }

    // determine lender to the branch
    int lenderId = -1;
    try
    {
      sql = "Select lenderprofileid from lendertobranchassoc where branchProfileId = " + branchId;

      int key = jExec.execute(sql);
      jExec.next(key);
      lenderId = jExec.getInt(key, 1);
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getLenderUsersByGroup: Error getting user: while locating lender for branch Id = " +
        branchId);
      logger.error("WFE::UserProfileQuery:getLenderUsersByGroup: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getLenderUsersByGroup: err = " + e.getMessage());
      return null;
    }

    return getLenderUsers(lenderId, userType, activeOnly, srk, institutionId);
  }

  public Vector getLenderUsers(int lenderId, int userType, boolean activeOnly, 
                              SessionResourceKit srk, int institutionId)
  {
    // SysLogger logger = srk.getSysLogger();
    JdbcExecutor jExec = srk.getJdbcExecutor();
    String sql = null;

    // determine all branch for the institution
    Vector branches = new Vector();
    try
    {
      sql = "Select branchProfileId from lendertobranchassoc where  lenderprofileid = " + lenderId;
      int key = jExec.execute(sql);
      while(jExec.next(key))
      {
        branches.addElement(new Integer(jExec.getInt(key, 1)));
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("WFE::UserProfileQuery:getLenderUsers: Error getting branchProfileId for lenderprofile Id = " + lenderId);
      logger.error("WFE::UserProfileQuery:getLenderUsers: sql = " + sql);
      logger.error("WFE::UserProfileQuery:getLenderUsers: err = " + e.getMessage());
      return null;
    }

    // determine all groups for the branches
    Vector groups = new Vector();
    for(int i = 0; i < branches.size(); ++i)
    {
      int branchid = -1;
      try
      {
        branchid = ((Integer)branches.elementAt(i)).intValue();
        sql = "Select groupProfileId from groupProfile where branchProfileId = " + branchid;
        int key = jExec.execute(sql);
        while(jExec.next(key))
        {
          groups.addElement(new Integer(jExec.getInt(key, 1)));
        }
        jExec.closeData(key);
      }
      catch(Exception e)
      {
        logger.error("WFE::UserProfileQuery:getLenderUsers: Error getting groups for branch Id = " + branchid);
        logger.error("WFE::UserProfileQuery:getLenderUsers: sql = " + sql);
        logger.error("WFE::UserProfileQuery:getLenderUsers: err = " + e.getMessage());
        return null;
      }
    }

    // get all users for the groups
    Vector users = new Vector();

    int len = groups.size();

    for(int i = 0; i < len; ++i)
    {
      int grpId = ((Integer)groups.elementAt(i)).intValue();
      Vector userInGroup = getGroupUsers(grpId, userType, activeOnly, srk, institutionId);

      if(userInGroup != null)
      {
        int len2 = userInGroup.size();
        for(int j = 0; j < len2; ++j)
        {
          users.addElement(userInGroup.elementAt(j));
        }
      }
    }

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }

  //--> Cervus II : Added new AUTO usertype (AME spec. section 3.2.1)
  //--> By Billy 28Sept2004
  /**
   * <pre>
   * Given a group (Id), minimum administrator priviledge type (see Mc for administrator
   * user types, -1 = any administrator type) and flag for active profile return:
   *
   *  . list (of user profiles) of qualifying administrators in the group, or if
   *    none found,
   *
   *  . list of qualifying administrators for the branch that contains the specified
   *    group, or,
   *
   *  . null, if no qualifying administrators located.
   *
   * </pre>
   **/
  public Vector getAutoUsers(int groupId, boolean activeOnly, 
                              SessionResourceKit srk, int institutionId)
  {
    return getUsers(groupId, Mc.USER_TYPE_AUTO, activeOnly, srk, institutionId);
  }

  /**
   * To get any user with the input UserType
   *
   * @param userType int
   * @param activeOnly boolean
   * @param srk SessionResourceKit
   * @return Vector
   */
  public Vector getAnyUsers(int userType, boolean activeOnly, SessionResourceKit srk)
  {
    Vector users = null;
    // SysLogger logger = srk.getSysLogger();
    //JdbcExecutor jExec = srk.getJdbcExecutor();

    try
    {
      UserProfile worker = new UserProfile(srk);
      users = worker.findByUserType(userType, activeOnly);
    }
    catch(Exception e)
    {
      users = null;
      logger.error("WFE::UserProfileQuery:getAnyUsers: Error getting users in with UserType = " + userType);
    }

    if(users == null || users.size() == 0)
    {
      return null;
    }

    return users;
  }

  //=======================================================================
}
