package com.basis100.workflow.notification;

import java.util.Date;
import MosSystem.Mc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
/**
 *  Handles storage of notification details in the WorkflowNotificationQueue table <br>
 *  Notification types are found in the NotificationType table and are represented by Mc constants.<br>
 */
public class WorkflowNotifier
{
   private SessionResourceKit srk;
   private SysLogger logger;


  public WorkflowNotifier(SessionResourceKit srk)
  {
     this.srk = srk;
     this.logger = srk.getSysLogger();
  }


  /**
   * Sends notification to the Workflow notification server
   *
   **/
  public void send(int dealId, int type) throws Exception
  {
     performSend(this.srk,dealId,type);
  }




  /**
 * Sends notification to the Workflow notification server and enters a DealNote
 * with category id = Document Preparation
 *
 **/
  public void send(int id, int type, String note) throws Exception
  {
     int catId = Mc.DEAL_NOTES_CATEGORY_DOCUMENT_PREPARATION;

     MasterDeal md = new MasterDeal(srk,null,id);
     int copy = md.getGoldCopyId();
     Deal deal = new Deal(srk,null,id,copy);

     DealNotes dn = new DealNotes(srk);
     dn.create((DealPK)deal.getPk(),id,catId,note);
     performSend(this.srk,id,type);
  }


/**
 * Sends notification to the Workflow notification server and enters a DealNote
 * with category id = Document Preparation
 *
 **/
  public static void send(SessionResourceKit srk, int id, int type, String note) throws Exception
  {
     int catId = Mc.DEAL_NOTES_CATEGORY_DOCUMENT_PREPARATION;

     MasterDeal md = new MasterDeal(srk,null,id);
     int copy = md.getGoldCopyId();
     Deal deal = new Deal(srk,null,id,copy);

     DealNotes dn = new DealNotes(srk);
     dn.create((DealPK)deal.getPk(),id,catId,note);
     (new WorkflowNotifier(srk)).send(id,type);
  }

   /**
   * Sends notification to the Workflow notification server
   *
   **/
  private void performSend(SessionResourceKit srk, int dealId, int type) throws Exception
  {
      logger.debug("Start Workflow Notification for DealId: " + dealId);

      /* DEALID
         NOTIFICATIONTIME
         DOCUMENTID
         WORKFLOWNOTIFICATIONTYPEID
         WORKFLOWNOTIFICATIONQUEUEID    */


      int queueIndex = this.getQueueIndex(srk);

      JdbcExecutor jExec = srk.getJdbcExecutor();
      SysLogger logger = srk.getSysLogger();

      Date d = new Date();

      StringBuffer sqlbuf = new StringBuffer("Insert into WorkflowNotificationQueue ");
      sqlbuf.append("values (").append(dealId).append(", ");
      sqlbuf.append("'").append(TypeConverter.stringTypeFrom(d)).append("'");
      sqlbuf.append(", ").append(0);
      sqlbuf.append(", ").append(type);
      sqlbuf.append(", ").append(queueIndex);
      sqlbuf.append(", ").append(srk.getExpressState().getDealInstitutionId()).append(")");
      try
      {
        jExec.executeUpdate(sqlbuf.toString());
      }
      catch (Exception e)
      {
        String msg = "Failed to notify workflow - Deal: " + dealId ;
        logger.error(msg);
        logger.error(e);
        throw new Exception(msg);
      }

  }

  private int getQueueIndex(SessionResourceKit srk) throws Exception
  {
       String sql = "Select WorkflowNotificationQueueSeq.nextval from dual";
       int id = -1;

       JdbcExecutor jExec = srk.getJdbcExecutor();

       int key = jExec.execute(sql);

       for (; jExec.next(key);  )
       {
           id = jExec.getInt(key,1);  // can only be one record
       }
       jExec.closeData(key);

       if(id < 0) throw new Exception("Failed to get sequence for WorkflowNotificationQueue");

       return id;
  }

}



