package com.basis100.workflow.notification;

public interface WorkflowNotificationListener
{
   public void workflowNotified(WFNotificationEvent evt);
}
