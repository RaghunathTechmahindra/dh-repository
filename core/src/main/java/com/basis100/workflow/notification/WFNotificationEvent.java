package com.basis100.workflow.notification;

public class WFNotificationEvent
{

  private int dealId;
  private int workflowNotificationType;
  private int institutionProfileId;


  public WFNotificationEvent(int id, int type, int docid, int iid)
  {
    this.dealId = id;
    this.workflowNotificationType = type;
    this.institutionProfileId = iid;
  }

  public int getDealId()
  {
    return dealId;
  }

  public int getWorkflowNotificationType()
  {
    return workflowNotificationType;
  }

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

  
}