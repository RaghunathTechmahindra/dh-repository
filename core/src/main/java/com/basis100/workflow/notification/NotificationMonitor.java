package com.basis100.workflow.notification;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;


/**
 *   <pre>
 *   Notifies registered Listeners that a deal has been ingested.
 *
 *   </pre>
 */
public class NotificationMonitor implements Runnable
{
  private Thread monitorThread = null;
  private SysLogger logger;
  private SessionResourceKit srk;
  private Set listeners;
  private int monitorInterval;

  private static NotificationMonitor instance = null;

  // diagnostic mode - when disabled low-level log messages (debug, trace, and jdbc trace) are supressed. Otherwise
  // production of these messages is controlled by SysLog global settings.

  private static boolean diagMode = false;

  //private boolean stopped = true;
  private boolean running = false;

  /**
   *  Monitors the WorkflowNotificationQueue table for new records. When a new record is
   *  recieved a WFNotificationEvent is fired to all registered listeners implementing
   *  interface WorkflowNotificationListener. The callback method: <br>
   *  <code>workflowNotified(WFNotificationEvent evt)</code>
   *  recieves the event message. Events of Interest can be trapped based on the
   *  value returned by <code>getWorkflowNotificationType()</code><br>
   *
   *  Available types =   <br>
   *   <br> WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL     	= 0;
	 * <br> WORKFLOW_NOTIFICATION_TYPE_MI_REPLY     = 1;
   * <br> WORKFLOW_NOTIFICATION_TYPE_DOCUMENT_DELIVERY_SUCCESS  = 2;
   * <br> WORKFLOW_NOTIFICATION_TYPE_DOCUMENT_DELIVERY_FAILED = 3;
   * <br> WORKFLOW_NOTIFICATION_TYPE_MI_REQUEST = 4;
   * <br> WORKFLOW_NOTIFICATION_TYPE_MI_CANCEL  = 5;
   *
   */

  private NotificationMonitor()
  {
  }

  public void init() {
	    try
		{
		    this.srk = new SessionResourceKit("NotificationMon");
		    this.logger = srk.getSysLogger();
		    this.listeners = new HashSet();
		    String delayString = PropertiesCache.getInstance().getProperty(-1, "com.basis100.notification.interval");

		    // disable most logging levels when not in diagnostic mode
		    if (logger != null && diagMode == false)
		        logger.setDebugTraceJdbcTrace(false, false, false);
		    try
		    {
		        monitorInterval = Integer.parseInt(delayString.trim());
		        logger.info("NotificationMonitor interval (sec): " +  monitorInterval);
		    }
		    catch(Exception e0)
		    {
		        monitorInterval = 20;
		        logger.info("NotificationMonitor interval not set using default (sec): " +  monitorInterval);
		    }
		 }
         catch(Exception e1)
         {
            SysLogger temp =  ResourceManager.getSysLogger("NotificationMon");
            String msg = "WORKFLOW NOTIFICATION: Failed to get instance. Unable to start.";
            temp.error(msg);
            temp.error(e1);
            temp.mail(msg);
         }
  }

  public static NotificationMonitor getInstance(boolean diagModeIn)
  {
    diagMode = diagModeIn;

    NotificationMonitor mm = getInstance();

    return mm;
  }

  public static NotificationMonitor getInstance()
  {
    if(instance == null)
      instance = new NotificationMonitor();

    return instance;
  }

  public void run()
  {
	  String delayString = PropertiesCache.getInstance().getProperty(-1, "com.basis100.notification.interval");
      monitorInterval = monitorInterval * 1000;
      int interval = monitorInterval;
      int errorCount = 0;

      synchronized (this)
      {

          while(true)
          {
          	logger.debug("Clement's test : NotificationMonitor is runing");
        	logger.debug("Clement's test : NotificationMonitor thread id = " + monitorThread.getId());  
            try
            {
              // if we have an event ensure handled (since all record of source of event removed)
              // in this method, srk.freeResources(); is called
              // evt has insitutionPRofileId for ML
              WFNotificationEvent evt = checkQueue(srk);

              if(evt != null)
              {
                  fireNotification(evt);
                  interval = 10;    //cant be zero else waits till notify
              }
              else
              {
                interval = monitorInterval;
              }

              if (!running){
            	logger.debug("Clement's test : NotificationMonitor stopped");  
                break;
              }
              //this.wait(interval);
              Thread.sleep(interval);
              
              if (!running){
            	logger.debug("Clement's test : NotificationMonitor stopped"); 
                break;
              }
            }
            catch(Exception e)
            {
              String msg = "Workflow Notification Monitor: Exception while monitoring queue.";
              logger.error(msg);
              logger.error(e);
              //--> For Debug by Billy 08April2003
              logger.error(com.basis100.deal.util.StringUtil.stack2string(e));
              //================================

              if (handleMonitorFailure(logger,e, ++errorCount) == true)
              {
                // stop
                //stopped = true;
              	running = false;
                break;
              }

              continue;
            }
          }

          monitorThread = null;

          //this.notifyAll();
      }

      if (running == true)
      {
      	running = false;
        logger.error("Workflow Notification Monitor: Stopped due to error.");
      }
  }


  private WFNotificationEvent checkQueue(SessionResourceKit srk) throws Exception
  {
      JdbcExecutor jExec = null;

      String sql = "select dealId, workflowNotificationTypeId, documentId, workflowNotificationQueueId, InstitutionProfileId " +
                    "from workflownotificationQueue where workflowNotificationQueueId = " +
                    "(select MIN(workflowNotificationQueueId) from workflownotificationqueue)";

      int dealId = -1;
      int type = -1;
      int doc = -1;
      int id = -1;
      int iid = -1;

      WFNotificationEvent evt = null;

      try
      {
          jExec = srk.getJdbcExecutor();

          int key = jExec.execute(sql);

          for (; jExec.next(key); )
          {
            dealId = jExec.getInt(key,"dealId");
            type = jExec.getInt(key,"workflowNotificationTypeId");
            doc = jExec.getInt(key,"documentId");
            id =  jExec.getInt(key,"workflowNotificationQueueId");
            iid = jExec.getInt(key,"InstitutionProfileId");
            evt = new WFNotificationEvent(dealId,type,doc, iid);

            break; // can only be one record at a time;
          }

          jExec.closeData(key);

          // remove entry from queue

          if (id > 0 && type >= 0 && evt != null)
          {
              String delEntrySql = "Delete from WorkflowNotificationQueue where workflowNotificationQueueId = " + id;

              srk.beginTransaction();

              jExec.executeUpdate(delEntrySql);

              srk.commitTransaction();
          }
          else
              evt = null;

          srk.freeResources();
      }
      catch (Exception e)
      {
        srk.cleanTransaction();
        srk.freeResources();

        throw e;
      }

      return evt;
  }


  public void startMonitor()
  {
    String on = PropertiesCache.getInstance().getInstanceProperty("com.basis100.notification.on", "Y");
    //logger.info("Clement's test : NOTIFICATION MONITOR running = " + running);
    
    if(running == false && on.equalsIgnoreCase("Y"))
    {
      running = true;

      logger.info("**** NOTIFICATION MONITOR STARTED - com.basis100.notification.on )");

      monitorThread = new Thread(this);
      monitorThread.start();

      return;
    }

    logger.info("**** NOTIFICATION MONITOR NOT STARTED - REQUIRES PROPERTY com.basis100.notification.on = Y");
  }

  public void stopMonitor()
  {
    // signal stop and wait for confirmation (e.g. don't want to kill an ingestion in progress)
    if(running = false){
        return;
    }
    running = false;
		
	//removed by Clement for Terracotta. Do not need to notifyAll
//    while(monitorThread != null)
//    {
//        synchronized (this)
//        {
//            try
//            {
//              this.notifyAll();
//              this.wait(60000);  // allow for long duration transaction (60 seconds - very conservative)
//            }
//            catch(Exception e)
//            {
//              logger.error("Workflow Notification Monitor: Exception waiting for stop confirmation");
//              logger.error(e);
//              return;
//            }

//            if (monitorThread != null)
//            {
//              logger.error("Workflow Notification Monitor: Failed to receive stop confirmation");
//              return;
//            }

//            logger.trace("Workflow Notification Monitor - stopped");
//            return;
//        }
//    }

    logger.trace("Workflow Notification Monitor - stopped (wasn't running!)");
  }


  // result: true - stop monitor, false - ignore failure (for now!)
  private boolean handleMonitorFailure(SysLogger log, Exception e, int count)
  {
    String msg = null;

    if(count > 5)
    {
      msg = "Workflow Notification Monitor: Multiple Unresolved Exceptions - stopping monitor";
      log.error(msg);
      log.mail(msg);
      return true;
    }

    if(monitorThread == null)
    {
      msg = "Workflow Notification Monitor: Null monitor thread encountered - stopping monitor";
      log.error(msg);
      log.mail(msg);
      return true;
    }

    return false;
  }

  public synchronized void addListener(WorkflowNotificationListener listener)
  {
    if(listener != null)
    {
      listeners.add(listener);
      logger.trace("Workflow Notification Monitor: Added WorkflowNotificationListener - " + listener );
      logger.trace("Workflow Notification Monitor: Listener list size - " + listeners.size() );
    }
  }

  public synchronized void removeListener(WorkflowNotificationListener listener)
  {
    if(listener != null)
    {
      listeners.remove(listener);
      logger.trace("Workflow Notification Monitor: Removing WorkflowNotificationListener - " + listener );
      logger.trace("Workflow Notification Monitor: Listener list size - " + listeners.size() );
    }
  }

  public synchronized void removeAllListeners()
  {
    if(listeners != null && !listeners.isEmpty())
    {
      listeners = null;
      listeners = new HashSet();
      logger.trace("Workflow Notification Monitor:  removeAllListeners ");
    }
  }

  //not synchronized because it is called from within a sync block
  public void fireNotification(WFNotificationEvent evt)
  {
    Iterator it = listeners.iterator();
     WorkflowNotificationListener current = null;

    while(it.hasNext())
    {
      current = (WorkflowNotificationListener)it.next();
      current.workflowNotified(evt);
      logger.trace("Workflow Notification Monitor:  notify: " + current);
    }

  }

  //Added for Terracotta
  public Set getListeners() {
	return listeners;
  }
}
