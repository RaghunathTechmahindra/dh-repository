
package com.basis100.workflow;

import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import MosSystem.Mc;
import MosSystem.Sc;
import java.io.Serializable;

import com.basis100.workflow.entity.*;

public interface WFEExternalTaskCompletion
{
	public String getName();
	public int getTaskCompletionCode(SessionResourceKit srk, AssignedTask aTask, Deal deal, DealUserProfiles dealUserProfiles) throws Exception;
	public int getRegressionBackToStage();
}


