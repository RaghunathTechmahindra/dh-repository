
package com.basis100.workflow.queuedaemon;

import java.util.*;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.util.thread.*;

import MosSystem.Sc;

/* SentinelGenerator
 *
 * Module to control generation/manipulation - see MOS Consolidated Design for details.
 *
 **/

public class SentinelGenerator
{

  /**
  * Constructor: Default
  *
  **/

  public SentinelGenerator()
  {
  }


/*
 * Called by the task maintenance after detecting a time event of a assigned task. The method
 * checks sentinel task requirements and, if necessary, creates or updates required sentinel
 * task.
 *
 * Programmer Note:
 *
 * The caller provides an session resources object for accessing the database. Is is assumed that
 * the caller has established an appropriate transaction context before placing the call - i.e. that
 * any database activity performed will either committed or rolled back by the caller.
 *
 **/

  static void handleSentinelActivity(AssignedTask aTask, SessionResourceKit srk) throws Exception
  {
    // determine if sentinel is required

    if (aTask.sentinelRequired() == false)
      return;

    AssignedTask aSenTask = null;
    SysLogger      logger = srk.getSysLogger();
    //JdbcExecutor   jExec  = srk.getJdbcExecutor();

    // require sentinel - determine if one exists already
    int senId = aTask.getCrossRefQueueId();

    if (senId > 0)
    {
        // appears to be one - find and update ...
        try
        {
          aSenTask = new AssignedTask(srk);

          aSenTask = aSenTask.findByPrimaryKey(new AssignedTaskBeanPK(senId));

          // found - update it (e.g. to ensure unaacknowledged)

          aSenTask.setTaskStatusId(Sc.TASK_OPEN);
          aSenTask.setAcknowledged("N");
          aSenTask.setStartTimestamp(new Date());
          aSenTask.setDueTimestamp(aSenTask.getStartTimestamp());
          aSenTask.setEndTimestamp(aSenTask.getStartTimestamp());

          aSenTask.ejbStore();

          return;
        }
        catch (Exception e)
        {
            // not found - unexpected but no problem, will create it ..
        }
    }


    // create the sentinel ...
    try
    {
      // first determine recipient ...

      MasterDeal md = new MasterDeal(srk, null);
      md = md.findByPrimaryKey(new MasterDealPK(aTask.getDealId()));

			Deal deal = new Deal(srk,null);
			deal = deal.findByPrimaryKey(new DealPK(aTask.getDealId(), 
                      md.getGoldCopyId()));

      DealUserProfiles dealUserProfiles = new DealUserProfiles(deal, srk);

      WorkflowAssistBean wab = new WorkflowAssistBean();

	    int userId = 0;

      userId = wab.getUserIdForTask(deal, aTask.getSentinelTo(), srk, dealUserProfiles, null);

      if (userId == -1)
      {
        logger.error("Could not locate user to receive sentinel for "
          + aTask.taskIdentification() + " :: FallBack to assign the Task to current UserId !!");
        //--> If Cannot locate sentinel user should fallback to current Userid
        //--> Fix by Billy 20Mar2003
        userId = aTask.getUserProfileId();
        //return;
      }

      // create the sentinel

      aSenTask = new AssignedTask(srk);
      aSenTask.create(aTask, userId);

      // link original task to the sentinel (reverse link done on sentinel creation!)

      aTask.setCrossRefQueueId(aSenTask.getAssignedTasksWorkQueueId());

      return;
    }
    catch (Exception e)
    {
        logger.error("Could not create sentinel for " + aTask.taskIdentification());
        logger.error(e);

        return;
    }
  }
}
