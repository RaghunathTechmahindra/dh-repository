package com.basis100.workflow.queuedaemon;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import MosSystem.Mc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.thread.JobScheduler;
import com.basis100.workflow.WFTaskExecServices;

/**
 * <i>PostDatedFundCheckDaemon</i>
 *
 * Purposes:
 *    Daemon process runs everyday (00:00) to update the deal.statuid to
 *
 * Technical Details :
 *
 * Only one such daemon is allowed to run at a time - to help ensure this the class is implemented as
 * a singleton (private constructor and public getInstance() method).
 *
 * The class is not a Thread but rather implements Runnable - this implies that daemon is launched
 * by some external means (e.g. thread allocated to run the daemon externally).
 *
 * The daemon assumes the presence of a properly instantiated ResourceManager and uses the utility
 * class SessionResourceKit to obtain resources (DB and system logger resources).
 *
 * @author Billy Lam (Basis100 Inc.)
 * @version 1.0 (initial version)
 *
 **/

public class PostDatedFundCheckDaemon implements Runnable
{
  private static PostDatedFundCheckDaemon instance;

  private SessionResourceKit srk;

    private SysLogger      logger = null;
    private JdbcExecutor   jExec  = null;

  // Constands to define the checking frequence
  public final static long REPEATFREQUENCE = JobScheduler.DAILY;
  //public final static long REPEATFREQUENCE = (long)5*60*1000; //set for 5 mins for testing
  public final static int REPEATCOUNT = JobScheduler.FOREVER;
  // The daily start time
  //    Note :  In the case that the DB server and the App Server may running on different machine,
  //            System Time may different.  Then we set the executing time to 00:15am just in case.
  public final static int STARTHOUR = 00;
  public final static int STARTMINUTE = 15;

    private static final String SELECT_STRING = "select dealId, copyId, " 
            + " institutionProfileid from Deal"
            + " where DEAL.STATUSID = "
            + Mc.DEAL_POST_DATED_FUNDING
            + " AND sysdate >= DEAL.ACTUALCLOSINGDATE";

  /**
   * Constructor: Default
   */
    private PostDatedFundCheckDaemon() {
    }

 /**
     * Method to return the static instance of PostDatedFundCheckDaemon. A new
     * instance will be created if not created yet.
     */
  public static PostDatedFundCheckDaemon getInstance()
  {
    if ( instance == null )
    {
      instance = new PostDatedFundCheckDaemon();
    }

    return instance;
  }


  /**
  * Main deamon method.  To be executed by the JobScheduler.
  *
  * Purpose :
  *   - Execute an UPDATE statement to change the deal.statusid to DEAL_FUNDED
  *     if (deal.statusid == DEAL_POST_DATED_FUNDING and deal.ActualClossingDate <= sysdate).
  **/
  public void run()
  {
     srk    =  new SessionResourceKit("PDFDAEMON");
     logger = srk.getSysLogger();

     jExec  = srk.getJdbcExecutor();

     jobMessage("Daily check.", false);

     if (jExec == null)
     {
        srk.freeResources();

        jobMessage("Failed to obtain DB connection resource", true);
        jobMessage("Finished", false);
        return;
     }

     try
     {
        // Set up the UPDATE statement and execut it
        srk.beginTransaction();

        // Catherine, XD and GE to MCAP start --------- NEW REQUIREMENT: call workflow here ---------
        ArrayList arPFD = getPostfundedDeals();
        logger.trace("--> Post Dated Funding Daemon: run(): " + arPFD.size() + " post-dated funded deals found");

        // Catherine, XD to MCAP end ------------------------------------------------------   
        logger.debug("--> Post Dated Funding Daemon: run(): Updating status to 21 on post-dated funded deals... ");
        int updateCount = jExec.executeUpdate(getSqlCheckAndUpdate());

        // Catherine, XD to MCAP start --------- NEW REQUIREMENT: call workflow here ---------
        if (!arPFD.isEmpty()) {
          logger.debug("--> Post Dated Funding Daemon: run(): Calling Workflow for post-dated funded deals... ");
          triggerWorkflowForPostfundedDeals(arPFD);
        }
        // Catherine, XD and GE to MCAP end ------------------------------------------------------   

        srk.commitTransaction();
        sendUpload();
        jobMessage("Number of Deal Records updated : " + updateCount , false);
     }
     catch (Exception ex)
     {
        srk.cleanTransaction();

        jobMessage("Unhandled exception encountered (ex=" + ex + ")", true);
     }

     srk.freeResources();

     jobMessage("Finished", false);
  }

  private static final String logMsgId = "Post Dated Funding Daemon: ";

  private void jobMessage(String message, boolean error)
  {
      if (error)
          logger.error(logMsgId + message);
      else
          logger.info(logMsgId + message);
  }

  private String getSqlCheckAndUpdate()
  {
    String sqlstr = "UPDATE deal" +
                    " SET DEAL.STATUSID = " + Mc.DEAL_FUNDED + "," +
                    " DEAL.STATUSDATE = sysdate" +
                    " where DEAL.STATUSID = " + Mc.DEAL_POST_DATED_FUNDING +
                    " AND sysdate >= DEAL.ACTUALCLOSINGDATE";

    return sqlstr;
  }

  /**
  * Method to setup and start the schdeule of the Daemon Job
  *
  **/
  public static void startPostDatedFundCheckDaemon(JobScheduler jobScheduler)
  {
      Date startDateTime = new Date();

      // Set the Start time (Hour and Minute) which defined in the Constands
      Calendar cal = Calendar.getInstance();
      cal.setTime(startDateTime);
      cal.set(Calendar.HOUR_OF_DAY, STARTHOUR);
      cal.set(Calendar.MINUTE, STARTMINUTE);
      cal.set(Calendar.SECOND, 0);
      startDateTime = cal.getTime();

      // Start the Schedule
      jobScheduler.executeAtAndRepeat(PostDatedFundCheckDaemon.getInstance(), startDateTime,
                                      REPEATFREQUENCE, REPEATCOUNT);
      // For testing purpose only
      //jobScheduler.executeAtAndRepeat(PostDatedFundCheckDaemon.getInstance(), new Date(),
      //                                REPEATFREQUENCE, REPEATCOUNT);
   }

    public void sendUpload()
    {
        try
        {
            int key = jExec.execute(SELECT_STRING);
            Deal deal;
            DealPK pk;
            int dealId, copyId, institutionId;
            CCM module = new CCM(srk);
            while (jExec.next(key))
            {
                dealId = jExec.getInt(key, "dealId");
                copyId = jExec.getInt(key, "copyId");
                institutionId = jExec.getInt(key, "institutionProfileId");

                pk = new DealPK(dealId, copyId);

                deal = new Deal();
                deal.findByPrimaryKey(pk);
                module.sendServicingUpload(srk, deal,
                        Mc.SERVICING_UPLOAD_TYPE_FUNDING, 0);
            }
            jExec.closeData(key);

        } catch (Exception e)
        {
            logger.error(StringUtil.stack2string(e));
        }
    }

    // --------------------------------------------- Catherine, XD and GE to MCAP start ---------
    // retrieve a list of deals to call workflow on
    private static final String SELECT_UPDATED =
                                 "SELECT dealid, copyid, institutionProfileId " +
                                 "FROM DEAL " +
                                 "WHERE " +
                                 "      STATUSID = " + Mc.DEAL_POST_DATED_FUNDING +
                                 "  AND sysdate >= ESTIMATEDCLOSINGDATE" +
                                 "  AND FUNDINGUPLOADDONE = 'N'" +
                                 "  order by institutionProfileId";


    private ArrayList getPostfundedDeals() {
        ArrayList arDeals = new ArrayList();

        logger.debug("PDFCDaemon.getPostfundedDeals(..), before clear vpd...");
        int lastInstProfId = -1;
        srk.getExpressState().cleanAllIds();

        int lCnt = 0;
        try {
            int key = jExec.execute(SELECT_UPDATED);
            Deal deal;
            DealPK pk;
            int dealId, copyId, institutionId;
            while (jExec.next(key)) {
                dealId = jExec.getInt(key, "dealId");
                copyId = jExec.getInt(key, "copyId");
                institutionId = jExec.getInt(key, "institutionProfileId");

                if (institutionId != lastInstProfId) {
                    logger.debug("PDFCDaemon.getPostfundedDeals(..), before set vpd...");
                    srk.getExpressState().setDealInstitutionId(institutionId);
                    lastInstProfId = institutionId;
                }

                pk = new DealPK(dealId, copyId);

                deal = new Deal(srk, null);
                try {
                    deal.findByPrimaryKey(pk);
                    arDeals.add(deal);

                    lCnt++;
                } catch (FinderException fe) {
                    logger
                            .debug("FinderException in Post Dated Funding Daemon getPostfundedDeals(): Deal not found! DealId = "
                                    + dealId
                                    + " copyId = "
                                    + copyId
                                    + ". Original exception:  "
                                    + fe.getMessage());
                }
            }
            jExec.closeData(key);
            return arDeals;
        } catch (Exception e) {
            logger.error("PostDatedFundCheckDaemon: cannot generate funding upload requests: "
                            + StringUtil.stack2string(e));
        }
        return arDeals;
    }
    
    private void triggerWorkflowForPostfundedDeals(ArrayList arPFD) {
      Iterator it = arPFD.iterator();
      
      // find userprofileId for Auto User
      // int autoUserProfileId = findAutoUser();
      
      int lastInstProfId = -1;
      
      while (it.hasNext()) {
        Deal d = (Deal)it.next();
        int dId = d.getDealId();
        int cId = d.getCopyId();
        int instProfId = d.getInstitutionProfileId();
        
        if(instProfId != lastInstProfId){
            logger.debug("PDFCDaemon.triggerWorkflowForPostfundedDeals(..), before set vpd...");
            srk.getExpressState().setDealInstitutionId(instProfId);
            lastInstProfId = instProfId;
        }
        
        logger.trace("=====>>> Post Dated Funding Daemon: triggerWorkflowForPostfunded(): calling Workflow for " + dId + " copy id = " + cId);
        
        try {
          triggerWorkflow(dId, cId, 210 /*autoUserProfileId*/);
        } catch (Exception e) {
          logger.trace("Post Dated Funding Daemon: triggerWorkflowForPostfunded(): ERROR calling Workflow for " 
              + dId + " copy id = " + cId + ": "+ e.getMessage());
        }
      }
    }
    
    protected void triggerWorkflow(int dealId, int copyId, int userProfileId) throws Exception
    {
    
    WFTaskExecServices tes = new WFTaskExecServices(srk, userProfileId, dealId, copyId, 99);
    
    try
    {
      tes.setAsyncStageTestGeneration(true);
      tes.REALtaskCompleted();
      return;
    }
    catch(Exception e)
    {
      String msg = "Exception @PostDatedFundCheckDaemon.triggerWorkflow(), type 2";
      srk.getSysLogger().error(msg);
      throw new Exception(msg);
    }
    catch(Error er)
    {
      String msg = "Exception @PostDatedFundCheckDaemon.triggerWorkflow(), type 2";
      srk.getSysLogger().error(er);
      throw new Exception(msg);
    }
  }
    // --------------------------------------------- Catherine, XD and GE to MCAP end ---------
    
}
