package com.basis100.workflow.queuedaemon;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.util.StringUtil;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.thread.StopOnTimeTrigger;
import com.basis100.util.thread.TimeTrigger;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.pk.AssignedTaskBeanPK;

import config.Config;

/* TaskMaintenanceDaemon
 *
 * Thread to check assigned task queue entities for time related events - e.g. check for
 * escalation, expiry, reissue alarm, etc - see documentation of method checkTaskstate() for
 * details.
 *
 * Technical Details
 *
 * Only one such daemon is allowed to run at a time - to help ensure this the class is implemented as
 * a singleton (private constructor and public getInstance() method).
 *
 * The class is not a Thread but rather implements Runnable - this implies that daemon is launched
 * by some external means (e.g. thread allocated to run the daemon externally).
 *
 * The daemon assumes the presence of a properly instantiated ResourceManager and uses the utility
 * class SessionResourceKit to obtain resources (DB and system logger resources).
 *
 **/

public class TaskMaintenanceDaemon
  implements Runnable, StopOnTimeTrigger
{
  private static TaskMaintenanceDaemon instance;

  private TimeTrigger stopTrigger;

  private SessionResourceKit srk;

  private SysLogger logger = null;
  private JdbcExecutor jExec = null;

  // diagnostic mode - when disabled low-level log messages (debug, trace, and jdbc trace) are supressed. Otherwise
  // production of these messages is controlled by SysLog global settings).

  private static boolean diagMode = false;

  private boolean stopped = true;

  // Define the maximum error counters -- Billy 05March2002
  private static int MAX_SQL_ERRORS = 10; // Max # of SQL errors before refreshing the connection
  private static int MAX_ERRORS_TOLERANCE = 10; // Max # Fatal Error before stopping the Daemon Process

  // keep failed task don't try to assigned again
  private static Map<Integer,AssignedTask> failedAsignedTask = new HashMap<Integer,AssignedTask>();
  /**
   * Constructor: Default
   *
   **/

  private TaskMaintenanceDaemon()
  {
  }

  public static TaskMaintenanceDaemon getInstance(boolean diagModeIn)
  {
    diagMode = diagModeIn;
    return getInstance();
  }

  public static TaskMaintenanceDaemon getInstance()
  {
    if(instance == null)
    {
      instance = new TaskMaintenanceDaemon();
    }

    return instance;
  }

  /**
   * Get the connection
   * Call @checkTaskstate to check and update the tasks in Db
   **/
  public void run()
  {
    stopped = false;

    srk = new SessionResourceKit("TMDAEMON");

    logger = srk.getSysLogger();

    if(diagMode == false)
    {
      logger.setDebugTraceJdbcTrace(false, false, false);

    }
    jExec = srk.getJdbcExecutor();

    if(jExec == null)
    {
      srk.freeResources();

      jobMessage("Failed to obtain DB connection resource", true);
      jobMessage("Stopped", false);
      stopTimer();
      return;
    }

    // using connection indefinitely
    srk.setIndefiniteUseJdbcConnection();

    jobMessage("Started", false);
    int SQLErrorCount = 0;
    int faultToleranceCount = 0;

    String tempInterval = PropertiesCache.getInstance().getInstanceProperty("com.basis100.taskmaintenencedaemon.interval", "30");

    long sweepIntervalInSec = new Long(tempInterval).longValue();

    if(sweepIntervalInSec < 5)
    {
      sweepIntervalInSec = 5;
    }
    jobMessage("Sweep interval = " + sweepIntervalInSec + " secs", false);
    long sweepIntervalInMiniSec = sweepIntervalInSec * 1000;

    for(; ; )
    {
      try
      {
        long startTime = System.currentTimeMillis();

        sweepTasks(jExec);
        if(checkForStop(false) == true)
        {
          break;
        }
        long sweepDuration = System.currentTimeMillis() - startTime;

        //--> Make Sweep Duration configurable
        //--> By Billy 30Jan2004
        if(sweepDuration < sweepIntervalInMiniSec) ///// 60000
        {
          // last sweep was performed in less than Sweep interval - delay for remainder of Sweep interval
          // before sweeping again
          synchronized(this)
          {
            //wait(sweepIntervalInMiniSec - sweepDuration); ///// 60000
        	Thread.sleep(sweepIntervalInMiniSec - sweepDuration);
            if(checkForStop(false) == true)
            {
              break;
            }
          }
        }
        //==============================================================================
      }
      catch(SQLException se)
      {
        //Modified to try to refresh the JDBC connection if too many sql error occured -- By BILLY 05March2002
        jobMessage("SQL error :: (" + se + ")" + " SQLErrorCount=" + SQLErrorCount, true);
        if(++SQLErrorCount >= MAX_SQL_ERRORS)
        {
          // Incerment fault tolerance counter and check if Max error meet ==> Stop Daemon
          if(++faultToleranceCount >= MAX_ERRORS_TOLERANCE)
          {
            jobMessage("Too many errors occured :: faultToleranceCount >= " + MAX_ERRORS_TOLERANCE +
              "(Max Fault Tolerance Counter) -- Stopping Daemon Process", true);
            break;
          }

          //Refresh the JDBC connection and reset the SQLError Counter
          jobMessage("Too many SQL Error occured -- try to refresh the JDBC connection. (faultToleranceCount=" +
            faultToleranceCount + ")", true);

          jExec.refreshConnection();

          // Reset SQL Error counter
          SQLErrorCount = 0;
        }
      }
      catch(Exception ex)
      {
        jobMessage("Unhandled exception encountered :: (ex=" + ex + ")", true);
        jobMessage("Exception :: " + StringUtil.stack2string(ex), true);
        // Incerment fault tolerance counter and check if Max error meet ==> Stop Daemon
        if(++faultToleranceCount >= MAX_ERRORS_TOLERANCE)
        {
          jobMessage("Too many errors occured :: faultToleranceCount >= " + MAX_ERRORS_TOLERANCE +
            "(Max Fault Tolerance Counter) -- Stopping Daemon Process", true);
          break;
        }
      }
    }

    // notify any waiting thread (e.g. signaled shutdown may be waiting confirmation)
    stopped = true;

    jobMessage("Stopped", false);

    srk.freeResources();

    stopTimer();
    //removed for Terracotta. Do not need to notifyAll
//    synchronized(this)
//    {
//      notifyAll();
//    }
  }

  /**
   * StopOnTimeTrigger interface methods.
   *
   **/

  public void setTimeTrigger(TimeTrigger timer)
  {
    stopTrigger = timer;
  }

  public TimeTrigger getTimeTrigger()
  {
    return stopTrigger;
  }

  public void startTimer()
  {
    if(stopTrigger == null)
    {
      return;
    }

    stopTrigger.startTimer();
  }

  public void stopTimer()
  {
    if(stopTrigger == null)
    {
      return;
    }

    stopTrigger.stopTimer();
  }

  // proactive call to stop the daemon - e.g. in response to system shutdown

  public void stopDaemon()
  {
    if(stopped == true)
    {
      return;
    }

    stopped = true;

    //removed by Clement for Terracotta. Do not need to notifyAll
//    synchronized(this)
//    {
//      try
//      {
//        notifyAll();
//        this.wait(20000); // wait to allow thread to stop - (very cvonservative duration - 20 seconds)
//      }
//      catch(Exception e)
//      {
//        ;
//      }
//    }
  }

  /**
   *
   * Scan the work queue to detect all assigned tasks that have crossed a time boundary - i.e.
   * detect time related events related to timeliness of the assigned tasks. Each assigned task
   * stores its own 'next time event timestamp'. Detection is simply the process of locating those
   * assigned tasks for which:
   *
   *     current time > next time event timestamp
   *
   * For each assigned task that has 'generated' a time event, the entity is loaded (found) and
   * the its handleTimeEvent() method is called; consequently the assigned task itself is
   * responsible for updating itself in response to the time event.
   *
   * PROGRAMMER NOTES
   *
   * Internally the method scans the work queue in blocks of deal id ranges from the top of the
   * queue on down; the range covers a maximum of 10 deal ids at a time. This is to limit the
   * number of assigned tasks entities isolated on any one query so as not to create a performance
   * bottle neck. Additionally the following internal procedures are followed to avoid hoging
   * the system:
   *
   *   . yields processor after each block query
   *
   *   . yield processor after each assigned task handle time event method call
   *
   * The yield are actually performed by the private chechForStop() method that is called at
   * the times indicated above. The methods main purpose is to detect a stop signal - class
   * implements StopOnTimeTrigger so a stop time trigger may have been set via interface method
   * setTimeTrigger(). If one has been set the job stops (e.g. run() method returns) when the
   * trigger 'goes off' (when isOn() method of the trigger object returns true) - see interface
   * StopOnTimeTrigger and class TimeTrigger for details.
   *
   **/
    private void sweepTasks(JdbcExecutor jExec) throws Exception {

        logger.debug("TMDaemon.sweepTasks(.), before clear vpd...");
        srk.getExpressState().cleanAllIds();

		//	#DG736 rewritten START
        int key = jExec.execute(getSqlForMinimumMaximumDealId());
        jExec.next(key);
        long firstDealId = jExec.getLong(key, 1);
        long lastDealId = jExec.getLong(key, 2);
        jExec.closeData(key);
		//	#DG736 rewritten END

        long startqryKey = firstDealId;
        long dealBlockSize = 2000;
        long endqryKey = firstDealId + dealBlockSize;
        String[][] data;

        boolean isVpdSet = false;
        
        do {
            if(isVpdSet){
                logger.debug("TMDaemon.sweepTasks(.), before clear vpd inside loop...");
                srk.getExpressState().cleanAllIds();
                isVpdSet = false;
            }
            
            AssignedTask aTask = null;
            AssignedTaskBeanPK taskPK = null;
            // get assined Task from ASSIGNEDTASKSWORKQUEUE table that has
            // dealId between startqryKey and endqryKey
            key = jExec.execute(getSqlForFetchTaskRecords(startqryKey, endqryKey));

            if (checkForStop(true) == true) {
                jExec.closeData(key);
                return;
            }

            data = jExec.getData(key);

            jExec.closeData(key);
            
            int lastInstProfId = -1;
            
            for (int i = 0; i < data.length; i++) {
                aTask = null;
                Integer taskId = new Integer(data[i][0]);
                Integer instProfId = new Integer(data[i][1]).intValue();

				//* #DG736 rewritten
				if (failedAsignedTask.containsKey(taskId))
					continue;

				if (instProfId != lastInstProfId) {
                    logger.debug("TMDaemon.sweepTasks(.), before set vpd...");
                    srk.getExpressState().setDealInstitutionId(instProfId);
                    lastInstProfId = instProfId;
                    isVpdSet = true;
                }

                taskPK = new AssignedTaskBeanPK(taskId.intValue());
                try {
                    aTask = new AssignedTask(srk);
                    if (!failedAsignedTask.containsKey(taskId)) {
                        aTask = aTask.findByPrimaryKey(taskPK);

                        boolean update = aTask.handleTimeEvent();

                        if (update == true) {
    						srk.cleanTransaction();		//#DG736 assure transaction is clean in case of previous errors/timeouts: 2008-07-08 12:55:25,857:ERROR[SysLog  ]:2008.07.08.12.55.25:857 E [TMDAEMON  ] java.sql.SQLException: ORA-00060: deadlock detected while waiting for resource
                            srk.beginTransaction();

                            SentinelGenerator
                                    .handleSentinelActivity(aTask, srk);

                            aTask.ejbStore();

                            srk.commitTransaction();
                        }
                        if (checkForStop(true) == true) {
                            return;
                        }
                    }
                } catch (Exception e) {
                    failedAsignedTask.put(taskId, aTask);
                    logger.error("Failed Assign Task: TaskId = " + taskId);
                    if (aTask != null)
                        logger.error("DealId = " + aTask.getDealId());
                    logger.error(StringUtil.stack2string(e));
					srk.cleanTransaction();		//#DG736 assure transaction is clean in case of previous errors/timeouts: 2008-07-08 12:55:25,857:ERROR[SysLog  ]:2008.07.08.12.55.25:857 E [TMDAEMON  ] java.sql.SQLException: ORA-00060: deadlock detected while waiting for resource
                    sendAlertEmail(taskPK, aTask, e);
                } finally{
                    srk.cleanTransaction();
                }
            }
            startqryKey = endqryKey + 1;
            endqryKey = startqryKey + dealBlockSize;
        } while (startqryKey <= lastDealId);
    }

  private void sendAlertEmail(AssignedTaskBeanPK taskPK, AssignedTask aTask, Exception e)
  {
      String poolname = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),"com.basis100.resource.connectionpoolname");

      try
      {
    	//#DG736 refactored
//        String subject = "Express TaskMaintenanceDaemon Error, %poolname environment".replaceFirst("%poolname", poolname);
//        String subject = "Express TaskMaintenanceDaemon Error";
        String subject = "Express TaskMaintenanceDaemon Error, " +poolname +" environment";
        String alertHeader = "**** PLEASE DO NOT REPLY FOR THIS MAIL **** \n\n";
//        String body = "Express TaskMaintenanceDaemon: error in assigning Task for AssignedTaskId = " 
//                                + taskPK.getAssignedTasksWorkQueueId();
//        if (aTask != null)
//            body = body + " dealId = " + aTask.getDealId(); 

        String body = "**** PLEASE DO NOT REPLY THIS MAIL **** \n\n"
        	+	"Express TaskMaintenanceDaemon: error in assigning Task for AssignedTaskId = "
        	+ taskPK.getAssignedTasksWorkQueueId() + aTask == null? "":" dealId = " + aTask.getDealId()+"\n\n"+e;

        int dealId = 0;
        if (aTask != null) dealId = aTask.getDealId();
        
        String emailTo = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),"com.filogix.docprep.alert.email.recipient");
        DocumentRequest.requestAlertEmail(srk, dealId, 0, emailTo, subject, alertHeader + body +"\n\n");
          logger.error("Email Alert was sent to :Express Support \n ");
          logger.error("******" + subject );
          logger.error(body);
      }
      catch(Exception ex)
      {
          logger.error("failed to send alert mail regarding TaskMaintenanceDaemon error - taskId = " + taskPK.getAssignedTasksWorkQueueId());
      }
  }

  private static final String logMsgId = "Task Maintenance Daemon: ";

  private void jobMessage(String message, boolean error)
  {
    if(error)
    {
      logger.error(logMsgId + message);
    }
    else
    {
      logger.info(logMsgId + message);
    }
  }

  private boolean checkForStop(boolean yieldIfNotStopping)
  {
    if(stopped == true)
    {
      return true;
    }

    if(stopTrigger != null)
    {
      if(stopTrigger.isOff() == true)
      {
        stopped = true;
        return true;
      }
    }

    if(yieldIfNotStopping)
    {
      // not going to stop - however we yield processor on each check to ensure this thread has
      // minimal inpact on user theads
        //#DG736 Thread.currentThread().yield();
        Thread.yield();
    }

    return false;
  }

  private String getSqlForFetchTaskRecords(long minkey, long maxkey)
  {
    String sqlstr = "SELECT ASSIGNEDTASKSWORKQUEUEID, INSTITUTIONPROFILEID from ASSIGNEDTASKSWORKQUEUE where " +
      "DEALID >= " + minkey + " AND DEALID <= " + maxkey + " AND sysdate > NEXTTIMEEVENTTIMESTAMP";

    return sqlstr;
  }

  private String getSqlForMinimumDealId()
  {
    String sqlstr = "SELECT MIN(dealid) FROM assignedTasksWorkQueue";

    return sqlstr;
  }

  private String getSqlForMaximumDealId()
  {
    String sqlstr = "SELECT MAX(dealid) FROM assignedTasksWorkQueue";

    return sqlstr;
  }

  private String getSqlForMinimumMaximumDealId()
  {
    String sqlstr = "SELECT MIN(dealid), MAX(dealid) FROM assignedTasksWorkQueue WHERE sysdate > NEXTTIMEEVENTTIMESTAMP";

    return sqlstr;
  }
  
  // For unit testing use  -- By BILLY 04April2003
  public static void main(String[] args)
  {

    System.out.println("=========== Begin TaskDaemon Test ==============");
    SessionResourceKit srk;
    SysLogger logger;

    SysLog.init(Config.SYS_PROPERTIES_LOCATION_DEV);
    logger = ResourceManager.getSysLogger("BILLY");
    srk = new SessionResourceKit("BILLY");

    try
    {

      PropertiesCache.addPropertiesFile(Config.SYS_PROPERTIES_LOCATION_DEV + "mossys.properties");

      ResourceManager.init(Config.SYS_PROPERTIES_LOCATION_DEV,
        PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),"com.basis100.resource.connectionpoolname"));

      //Start Daemon Process
      TaskMaintenanceDaemon.getInstance(true).run();

    }
    catch(Exception e)
    {
      System.out.println("Exception : " + e.getMessage());
      srk.cleanTransaction();
      srk.freeResources();
    }

    System.out.println("=========== Ended TaskDaemon Test==============");
  }

}
