package com.basis100.workflow;


import java.util.Collection;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.deal.util.DBA;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.util.Xc;

public class WFETrigger
{
  public WFETrigger()
  {
  }

  /** workflowTrigger
  *
  * Workflow activation trigger.
  *
  *   Type - 0 - task completion activity
  *          1 - workflow selection and initial task generation
  *          2 - as 1) but add unconditional asnc state (test) task generation (e.g. possible MI task(s))
  *
  **/

  public static void workflowTrigger(int type, SessionResourceKit srk, int userId, int dealId,
                                     int copyId, int userTypeId, WFEExternalTaskCompletion extObj) throws Exception
  {
      SysLogger logger = srk.getSysLogger();

      logger.trace("--T--> @WFETrigger.workflowTrigger: type=" + type);

      // workflow activities
      WFTaskExecServices tes = new WFTaskExecServices(srk, userId, dealId, copyId, userTypeId);

      if (type == 0 || type == 2)
      {
          try
          {

          if (type == 2) {
            tes.setAsyncStageTestGeneration(true);
          }
          
          if (extObj != null)
            tes.setExternalTaskCompletion(extObj);

          tes.REALtaskCompleted();

          // since 4.2 DSU STARTS
          Deal deal = DBA.getDeal(srk, dealId, copyId);
          String supportDSU = PropertiesCache.getInstance().getProperty(
                  deal.getInstitutionProfileId(), Xc.STATUSUPDATE_DEALANDEVENT_SUPPORT, "Y");
          if ("Y".equals(supportDSU) || "y".equals(supportDSU)) {
              
        	  // For Manual Deal originated from Deal Entry screen will have channel Media as 'M' 
	          // Block updating the Deal event status and Condition update status for deals created from express. 
	          // FXP32829 - Dec 06 2011
	          if(deal.getChannelMedia() != null && deal.getChannelMedia().trim().equalsIgnoreCase(Xc.CHANNEL_MEDIA_M))
	          	return;
	          	
              // check the systemTypeId for the deal is supported DealEventStatus update
              //FXP25721 - July 22
              try {
                  DealEventStatusUpdateAssoc desua = new DealEventStatusUpdateAssoc(deal.getSessionResourceKit());
                  desua.setSilentMode(true);
                  Collection<DealEventStatusUpdateAssoc> c = desua.findBySystemType(deal.getSystemTypeId());
                  if (c==null || c.size()==0) {
                      return;
                  }
              } catch (Exception rd) {
                  logger.info("Couldn't get DealEventStatusUpdateAssoc for systemtypeid=" + deal.getSystemTypeId());
                  return;
              }

              ESBOutboundQueue queue = new ESBOutboundQueue(deal.getSessionResourceKit());
              ESBOutboundQueuePK pk = queue.createPrimaryKey();
              queue.create(pk, ServiceConst.SERVICE_TYPE_DEALEVENT_STATUS_UPDATE, deal.getDealId(), 
                        deal.getSystemTypeId(), userId);
          }
          // since 4.2 DSU ENDS

          return;
          }
          catch (Exception e)
          {
              String msg = "Exception @WFETrigger.workflowTrigger(), type 0/2";
              srk.getSysLogger().error(msg);
              throw new Exception(msg);
          }
          catch (Error er)
          {
              String msg = "Exception (ERROR) @WFETrigger.workflowTrigger(), type 0/2";
              srk.getSysLogger().error(er);
              throw new Exception(msg);
          }

      }

      if (type == 1)
      {
          try
          {
              WorkflowAssistBean wab = new 	WorkflowAssistBean();

              Deal deal = DBA.getDeal(srk, dealId, copyId);

              wab.generateInitialTasks(deal, srk);

              return;
          }
          catch (Exception e)
          {
              String msg = "Exception @WFETrigger.workflowTrigger(), type 1";
              srk.getSysLogger().error(msg);
              throw new Exception(msg);
          }
          catch (Error er)
          {
              String msg = "Exception (ERROR) @WFETrigger.workflowTrigger(), type 1";
              srk.getSysLogger().error(er);
              throw new Exception(msg);
          }

      }

      throw new Exception("WFE@workflowTrigger: invalid activation type = " + type);
  }
  
}