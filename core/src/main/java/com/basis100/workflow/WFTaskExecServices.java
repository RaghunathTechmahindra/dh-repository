package com.basis100.workflow;


import java.sql.Time;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import MosSystem.Mc;
import MosSystem.Sc;


import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.entity.Page;
import com.basis100.workflow.entity.Workflow;
import com.basis100.workflow.entity.WorkflowTask;
import com.basis100.workflow.entity.WorkflowTaskPage;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.basis100.workflow.pk.WorkflowTaskBeanPK;
import com.basis100.deal.security.PDC;

import com.basis100.BREngine.BRUtil;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;

import com.filogix.express.legacy.mosapp.IPageHandlerCommon;
import com.filogix.express.legacy.mosapp.FleetPageHandlerCommon;
import com.filogix.express.legacy.mosapp.ISourceBusinessReviewHandler;
import com.filogix.express.legacy.mosapp.ISourceFirmReviewHandler;
import mosApp.MosSystem.PageEntry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WFTaskExecServices
{
    private static final Log _log = LogFactory.getLog(WFTaskExecServices.class);

    private IPageHandlerCommon phc;
    private SessionResourceKit srk;
    private SysLogger          logger;
    private JdbcExecutor       jExec;

    private PageEntry         pg;
    private WFDealTaskHistory taskHistory;
    private WorkflowAssistBean wfa = new WorkflowAssistBean(); // .ejbCreate()
    private Deal              deal     = null;
    private Workflow          workflow = null;
    private int               dealDenialCode;

    private int               regressionStartStage  = -1;
    private int               regressionBackToStage = 9999;

    //
    // Constructor:
    //

    public WFTaskExecServices(IPageHandlerCommon phc, PageEntry pg)
    {
        this.phc = phc;
        this.pg  = pg;
    }

    public WFTaskExecServices(SessionResourceKit srk, int userId, int dealId, int copyId, int userTypeId)
    {

        pg = new PageEntry();
        pg.setPageDealId(dealId);
        pg.setPageDealCID(copyId);
        pg.setPageMosUserId(userId);
        pg.setPageMosUserTypeId(userTypeId);

        phc = new FleetPageHandlerCommon();
        phc.setSessionResourceKit(srk);
    }


    // support for a single external task completion object currently supported - this
    // contradicts the design documentation but is sufficient for now.

    private WFEExternalTaskCompletion externalTaskCompletionObject = null;

    public void setExternalTaskCompletion(WFEExternalTaskCompletion obj)
    {
      externalTaskCompletionObject = obj;
    }

    // support for signaling uncomnditional test async stage (state 0) for task generation
    private boolean asyncStageTestGeneration = false;

    public void setAsyncStageTestGeneration(boolean b)
    {
        asyncStageTestGeneration = b;
    }

    public boolean REALtaskCompleted()
    {
      srk    = phc.getSessionResourceKit();
      logger = srk.getSysLogger();
      jExec  = srk.getJdbcExecutor();

      int dealId     = -1;
      int copyId     = -1;
      int userId     = -1;
      int userTypeId = -1;
      int workflowId = -1;

      boolean done             = false;
      boolean testTransition   = true;
      boolean failure          = false;
      boolean taskCompleted    = false;

      Exception eTransaction   = null;

      Vector openTasks                  = null;
      DealUserProfiles dealUserProfiles = null;

      int openTaskCount                 = 0;

      // setup ...
      try
      {
        dealId = pg.getPageDealId();
        copyId = pg.getPageDealCID();
        userId = pg.getPageMosUserId();
        userTypeId = pg.getPageMosUserTypeId();

        logger.trace("taskCompleted() :: dealId = " + dealId + ", userId = " + userId);

        // retrieve deal
        getDeal(dealId, copyId);

        // retrieve the task history instance
        taskHistory = new WFDealTaskHistory(phc.getSessionResourceKit(), dealId);
        workflowId  = taskHistory.getWorkflowId();

        logger.trace("taskCompleted() :: workflowId = " + workflowId);

        if (workflowId == -1)
          return true;       // no task history - hence no open tasks - nothing to do

        // retrieve list of open assigned tasks ...

        // all open tasks
        openTasks = taskHistory.getAllOpenTasks();
        openTaskCount = openTasks.size();

        logger.trace("taskCompleted() :: # open tasks  = " + openTaskCount);

        // setup deal user profiles helper object (retains unerwriter/administrator profiles
        // for deal as/when required - essentially caches these once obtained)
        dealUserProfiles = new DealUserProfiles(deal, srk);

        dealUserProfiles.traceShow(logger);
      }
      catch (Exception e)
      {
        logTaskCompletionFailure(null, e);
        failure = true;
        done    = true;
      }

      // Loop over open tasks (if any), marking each complete (if possible), and compiling generating
      // follow-up and loop-back tasks as we go. Other possibilities during task
      // completion include:
      //
      //  . workflow regression occurs: testing for task (re)generation of tasks
      //    in the current and possibly preceeding workflow stages, is applied (the tests for
      //    regeneration) after each currently open task is tested for completion. This allows us
      //    to determine the bounds of regression testing - stage at which to start regression
      //    testing and the last stage (going backwards) to consider.
      //
      //  . if asynchronous task (stage 0 task) completes we always test the async. stage for task
      //    generation in async stage. Note regression and loop back completion codes are not
      //    applicable for an async. task are treated as normal completion - if encountered the
      //    completion with be treated as a 'normal' completion (i.e. morphed to TASK_DONE).
      //
      //    Note, test async. stage for task generation may be unconditional - see setAsyncStageTestGeneration()
      //

      boolean regressionRequested = false;
      regressionStartStage  = -1;
      regressionBackToStage = 9999;


      // following used for 'link task' logic during workflow transition (below)
      AssignedTask lastCompletedTask = null;
      int          completedTaskCount = 0;

      logger.trace("taskCompleted() :: Check Open Tasks for completion");

      // Use the same BusinessRule Engine for performance improvment
      //  -- By BILLY 29April2002
      BusinessRule br = null;
      try{
        br = new BusinessRule(srk,
                             -1, -1, "na", "");
        br.setDealId(deal.getDealId(), deal.getCopyId());
        // #2242: Catherine, 23-Nov-05 --------- start ---------  
        br.setLenderName();
        BRUtil.debug("WFTaskExecServices: set LenderName done");
        // #2242: Catherine, 23-Nov-05 --------- end ---------  
      }
      catch (Exception e)
      {
          logger.error("taskCompleted:: Error encountered when getting BusinessRule Engine ::" + e.getMessage());
          return false;
      }
      //=================================================================


      for (int i = 0; i < openTaskCount && (!done); ++i)
      {
        eTransaction    = null;
        failure         = false;
        AssignedTask aTask = (AssignedTask)openTasks.elementAt(i);

        // skip sentinel task - killed automatically when parent task completes
        if (aTask.getTaskId() >= 50000)
          continue;

        boolean isAsyncTask = aTask.getWorkflowStage() == 0;

        int taskCompletionCode = -1;
        try
        {
            // Use the same BusinessRule Engine for performance improvment
            //  -- By BILLY 29April2002
            taskCompletionCode = testTaskCompletion(aTask, deal, dealUserProfiles, userTypeId,
                externalTaskCompletionObject, br);
            //================================================================================

            // ensure we avoid logic not applicable for async task (e.g. loop/link)
            if (isAsyncTask == true)
            {
              logger.trace("taskCompleted(ASYNC) :: Completion Code = " + taskCompletionCode);

              if (taskCompletionCode != Sc.TASK_NOT_DONE)
              {
                  asyncStageTestGeneration = true;

                  int completionStatus = Sc.TASK_COMPLETE;
                  if (taskCompletionCode == Sc.TASK_CANCEL)
                      completionStatus = Sc.TASK_CANCELLED;

                  // ensure we avoid link/follow-up behavior
                  updateAssignedTask(aTask, completionStatus, false);
              }

              continue;
            }
        }
        catch (Exception e)
        {
            logger.error("taskCompleted:: Error encountered testing task completion.");
            logTaskCompletionFailure(aTask, e);
            failure = true;
            break;
        }

        // not an async task ...

        // assume will complete but allow to roll back if doesn't (case TASK_NOT_DONE below)
        AssignedTask prevCompleted = lastCompletedTask;
        completedTaskCount++;
        lastCompletedTask = aTask;
        taskCompleted     = true;

        switch (taskCompletionCode)
        {
          case Sc.TASK_NOT_DONE:
            logger.trace("taskCompleted() :: Completion Code = TASK_NOT_DONE");

            taskCompleted = false;
            break;

          case Sc.TASK_DONE:
            logger.trace("taskCompleted() :: Completion Code = TASK_DONE");

            try
            {
              updateAssignedTask(aTask, Sc.TASK_COMPLETE, false);
              // Use the same BusinessRule Engine for performance improvment
              //  -- By BILLY 29April2002
              generateFollowUpTask(aTask, dealUserProfiles, taskHistory, br);
              //=================================================================
            }
            catch (Exception e)
            {
              failure      = true;
              done         = true;
              eTransaction = e;
            }

            break;

          case Sc.TASK_CANCEL:
            logger.trace("taskCompleted() :: Completion Code = TASK_CANCEL");

            try
            {
              updateAssignedTask(aTask, Sc.TASK_CANCELLED, false);
            }
            catch (Exception e)
            {
              failure      = true;
              done         = true;
              eTransaction = e;
            }

            break;


          case Sc.TASK_LOOP_BACK_PREDECESSOR:
          case Sc.TASK_LOOP_BACK_ORIGIN:

            logger.trace("taskCompleted() :: Completion Code = " + ((taskCompletionCode == Sc.TASK_LOOP_BACK_ORIGIN) ? "TASK_LOOP_BACK_ORIGIN": "TASK_LOOP_BACK_PREDECESSOR"));

            boolean toOrigin = taskCompletionCode == Sc.TASK_LOOP_BACK_ORIGIN;

            try
            {
              updateAssignedTask(aTask, Sc.TASK_COMPLETE, false);
              // Use the same BusinessRule Engine for performance improvment
              //  -- By BILLY 29April2002
              generateLoopBackTask(aTask, toOrigin, dealUserProfiles, taskHistory, br);
              //=================================================================
            }
            catch (Exception e)
            {
              failure      = true;
              done         = true;
              eTransaction = e;
            }

            break;

          case Sc.TASK_WORKFLOW_REGRESSION:
logger.trace("taskCompleted() :: Completion Code = TASK_WORKFLOW_REGRESSION");

            try
            {
              regressionRequested = true;
              updateAssignedTask(aTask, Sc.TASK_COMPLETE, false);
            }
            catch (Exception e)
            {
              failure      = true;
              done         = true;
              eTransaction = e;
            }

            break;

          default:
            // an error - unrecognized task completion code
            logger.error("taskCompleted: error encountered completing task, unknown task completion code = "  + taskCompletionCode);
            failure = true;
        }

        if (failure)
            logTaskCompletionFailure(aTask, eTransaction);

        // roll back completion tracking if task did not complete.
        if (taskCompleted == false || failure == true)
        {
          completedTaskCount--;
          lastCompletedTask = prevCompleted;
        }

      }


      // check for failure encountered
      if (failure)
        return false;

      // get workflow definition

      Workflow wf = null;
      try
      {
        wf = getWorkflow(workflowId);
      }
      catch (Exception e)
      {
          logger.error("taskCompleted(): Error obtaining workflow definition (entity)");
          logTaskCompletionFailure(null, e);

          return false;
      }

      // if regression required test for task (re)generation
      if (regressionRequested == true)
      {
        logger.trace("taskCompleted() :: regression called for --> wfa.generateRegressionTasks(): reg. start stage=" + regressionStartStage + ", reg. back-to stage=" + regressionBackToStage);

        try
        {
          wfa.generateRegressionTasks(getDeal(dealId, copyId), wf, regressionStartStage,
                                      regressionBackToStage, dealUserProfiles, srk, false, taskHistory);
        }
        catch (Exception e)
        {
          logger.error("taskCompleted:: Error encountered on regression testing.");
          logTaskCompletionFailure(null, e);

          return false;
        }
      }

      // check for a workflow transition ...
      //
      // The last clause in the test for transition is necessary because the history object
      // is unaware of the number of stages in the workflow and hence may report a false
      // workflow transition - i.e. to a stage beyond the end of the the actual workflow.
      // Hence we reference the workflow definition avoid attempting to generate tasks
      // beyond the end of the workflow.
      //
      // Notes:
      //
      // . Stage numbers are in index origin 0 (e.g. stage 0 is the async stage)

      AssignedTask linkTask = null;
      if ((testTransition == true) &&
          (taskHistory.isWorkflowTransition() == true) &&
          (taskHistory.getWorkflowTransitionStage() < wf.getNumberOfStages()))
      {
        logger.trace("taskCompleted() :: workflow transition --> wfa.generateTasks()");

        // check for link task? - only if a lone task completed to cause transition
        AssignedTask aTaskLinkable = null;

        if (completedTaskCount == 1 && lastCompletedTask != null && lastCompletedTask.getUserProfileId() == userId)
          aTaskLinkable = lastCompletedTask;

        // task generation
        try
        {
          linkTask = wfa.generateTasks(deal, wf, taskHistory.getWorkflowTransitionStage(),
                                       aTaskLinkable, dealUserProfiles, srk, false,
                                       taskHistory);
        }
        catch (Exception e)
        {
          logger.error("taskCompleted:: Error encountered on workflow transition.");
          logTaskCompletionFailure(null, e);

          return false;
        }
      }

      if (linkTask != null)
      {
        logger.trace("taskCompleted() :: have link task --> " + linkTask.getTaskId() + "(" + linkTask.getTaskLabel() + ")");

        try
        {
          setPageForAssignedTask(linkTask, userId, userTypeId);
        }
        catch (Exception e)
        {
          logger.error("taskCompleted::setPageForAssignedTask: Error. Navigating to link task page.");
          logTaskCompletionFailure(lastCompletedTask, null);
        }
      }

      // check for asynchronous tasks generation
      if (asyncStageTestGeneration == true)
      {
        try
        {
          wfa.generateTasks(deal, wf, 0, null, dealUserProfiles, srk, false, taskHistory);
        }
        catch (Exception e)
        {
          logger.error("taskCompleted:: Error encountered test async. task generation");
          logTaskCompletionFailure(null, e);

          return false;
        }
      }

      return true;
    }


    private void logTaskCompletionFailure(AssignedTask aTask, Exception e)
    {
        srk.cleanTransaction();

        String taskIdStr = "n/a";
        String dealIdStr = "?";

        if (aTask != null)
            taskIdStr = "" + aTask.getTaskId();

        if (deal != null)
            dealIdStr = "" + deal.getDealId();

        logger.error("taskCompleted: error encountered completing task Id=" + taskIdStr + " for deal Id=" + dealIdStr);
        if ((e != null) && (!(e instanceof TaskProcessingException)))
            logger.error("taskCompleted: exception = " + e.getMessage());
    }


    // testTaskCompletion:
    //
    // Given an assigned task determine if the task is 'completed' or done, and if so
    // the task completion code.
    //
    // Possible results (constants are Sc members):
    //
    // . TASK_NOT_DONE
    // . TASK_DONE
    // . TASK_CANCEL
    // . TASK_LOOP_BACK_PREDECESSOR
    // . TASK_LOOP_BACK_ORIGIN
    // . TASK_WORKFLOW_REGRESSION
    //
    // See Consolidated Design for documentation of the task result codes.
    //
    //
    // Programmer Notes:
    // ----------------
    //
    // Task completion is facilated via the business rules engine. Specifically, this is
    // done by retrieval and evaluation of the task's 'task completed' business rule; the
    // business rule search criteria is based upon the workflow Id and task Id attributes
    // of the assigned task, and rule name defined by constant TASK_BR_NAME_TEST_COMPLETION.
    //
    // If these is no business rule defined the assigned task is assumed to have no
    // completion criteria and is considered to have completed with completion code
    // TASK_DONE (see above).
    //
    // If a business rule is defined we may expect multiple expressions. The expressions are
    // evaluated until an expression returns TRUE. If no expression returns TRUE the task
    // is assumed to be 'incomplete' (i.e. TASK_NOT_DONE is returned).
    //
    // A TRUE result implies that the assigned task has completed. The rule result set
    // for the expression then contains:
    //
    //  Result Set Operand One: Task completion code (same as result codes shown above).
    //
    //          If the result operand is not present TASK_DONE will be assumed.
    //
    //  Result Set Operand Two:
    //
    //      If task completion code is TASK_WORKFLOW_REGRESSION, contains an indication of how far
    //      (backward) the engine will go (re) examining tasks for generation - see consolidated
    //      design document for a full explanation.
    //
    //      Otherwise, operand two is ignored (typicaly absent)
    //
    //  Notes:
    //
    //  . External decision completion code (TASK_EXTERNAL_DECISION) is detected and acted upon. Namely,
    //    the decision is delegated to a WFEExternalTaskCompletion object - if no object has been
    //    provided non-completion (TASK_NOT_DONE) is assumed.
    //

    private int testTaskCompletion(AssignedTask aTask, Deal deal, DealUserProfiles dealUserProfiles,
      int userTypeId, WFEExternalTaskCompletion externalTaskCompletionObject, BusinessRule br) throws Exception
    {
        srk.setCloseJdbcConnectionOnRelease(true);

        // retrieve task completion business rule expression(s)
        //BusinessRule br = new BusinessRule((srk.getJdbcExecutor()).getCon(),
        //                                    aTask.getWorkflowId(), aTask.getTaskId(),
        //                                   "na", Sc.TASK_BR_NAME_TEST_COMPLETION);

        // Use the same BusinessRule Engine for performance improvment
        //  -- By BILLY 29April2002
        br.reQuery(aTask.getWorkflowId(), aTask.getTaskId(),"na", Sc.TASK_BR_NAME_TEST_COMPLETION, aTask.getInstitutionProfileId());
        int copyId = deal.getCopyId();
        //br.setDealId(aTask.getDealId(), copyId);
        br.setUserTypeId(userTypeId);
        br.setRecordId(aTask.getExternalRecordId());

        boolean haveExpression = false;

        while(br.next())
        {
            haveExpression = true;

            RuleResult rr = br.evaluate();

            if (rr.ruleStatus == false)
              continue;

            // get completion code (assuming 'done' if absent)
            int resultQualifier = Sc.TASK_DONE;
            if (rr.opCount > 0)
                resultQualifier = rr.getInt(1);

            // handle regression (track regression bounds) and external completion
            if (resultQualifier == Sc.TASK_WORKFLOW_REGRESSION)
            {
                int backStages = 0;
                if (rr.opCount > 1)
                    backStages = rr.getInt(2);

                trackRegressionStages(aTask, backStages);
            }
            else if (resultQualifier == Sc.TASK_EXTERNAL_DECISION)
            {
                try
                {
                    logger.trace("testTaskCompletion() :: Completion Code = TASK_EXTERNAL_DECISION, user ext obj = " + externalTaskCompletionObject);

                    resultQualifier = Sc.TASK_NOT_DONE; // assumed if no external obj

                    if (externalTaskCompletionObject != null)
                    {
                      resultQualifier = externalTaskCompletionObject.getTaskCompletionCode(srk, aTask, deal, dealUserProfiles);

                      // refresh deal in case manipiulated by completion object
                      deal = null; getDeal(aTask.getDealId(), copyId);

                      // external completion may also call for regression
                      if (resultQualifier == Sc.TASK_WORKFLOW_REGRESSION)
                        trackRegressionStages(aTask, externalTaskCompletionObject.getRegressionBackToStage());
                  }
                }
                catch (Exception e)
                {
                    logger.error("taskCompleted:: Error encountered on call to external decision object.");
                    throw e;
                }
            }

            br.close();
            return resultQualifier;
        }

        br.close();

        if (haveExpression)
            return Sc.TASK_NOT_DONE;  // had business rule but no exp. returned true

        // was no task completion business rule
        return Sc.TASK_DONE;
    }

    // Whenever a workflow regression task completion code is encountered this function is called -
    // regression task completion condition may occur multiple times during task completion processing
    // in a single call.
    //
    // The method maintains the following:
    //
    //   regressionStartStage - first stage to re-examine for task generation - generally the
    //                          current stage but tasks in multiple stages may complete in a single
    //                          call - so this variable records the maximun stage number encountered
    //                          for tasks completing with regression.
    //
    //   regressionBackToStage - last stage to re-examine (going backwards from the current/max) stage
    //                          to be examinined for task re-generation.
    //

    private void trackRegressionStages(AssignedTask aTask, int howManyStagesBack)
    {
        // regrerssion is not applicable for asynchronous tasks
        if (aTask.getWorkflowStage() == 0)
          return;

        if (aTask.getWorkflowStage() > regressionStartStage)
        {
           regressionStartStage = aTask.getWorkflowStage();

           if (regressionStartStage < regressionBackToStage)
            regressionBackToStage = regressionStartStage; // i.e. first regression detection
        }

        int backToStage = 0;

        // check for -ve value - this indicates a stage number (the absolute value that is)
        // to proceed backwards towards

        if (howManyStagesBack < 0)
        {
          backToStage = -howManyStagesBack;

          if (backToStage > aTask.getWorkflowStage())
            return; // neaningless - back to stage > current stage, ignore
        }
        else
        {
          //  positive value - indicates haw many stages back relative to current stage

          backToStage = aTask.getWorkflowStage() - howManyStagesBack;

          // if negative assume back to first stage
          if (backToStage <= 0)
              backToStage = 1;
        }

        // if back further than current recorded back-to stage adopt new value
        if (backToStage < regressionBackToStage)
            regressionBackToStage = backToStage;
    }




    // updateAssignedTask:
    //
    // Update assigned task status. It is assumed that a 'completion' status is
    // being set (i.e. TASK_COMPLETE, TASK_CANCELLED). Hence the acknowledged
    // attibute is set (to "Y") as well since the task would not longer be a
    // candidate for a priority notification.
    //
    // If there is an outstanding sentinel for the task it is deleted.
    //

    private void updateAssignedTask(AssignedTask aTask, int status, boolean asTransaction) throws TaskProcessingException
    {
logger.trace("taskCompleted() :: updateAssignedTask() - update for completion: status = " + status);

        try
        {
            int sentinelTaskId = aTask.getCrossRefQueueId();

            if (asTransaction)
                srk.getJdbcExecutor().begin();

            aTask.setTaskStatusId(status);
            aTask.setEndTimestamp(new Date());
            aTask.setAcknowledged("Y");
            aTask.setNextTimeEventTimestamp(null);
            aTask.setNextTimeEventType(aTask.NONE);
            aTask.setCrossRefQueueId(0);

            aTask.ejbStore();

            if (sentinelTaskId > 0)
            {
logger.trace("taskCompleted() :: removing cross-ref (sentinel) task id = " + sentinelTaskId);

                // delete sentinel - but fail gracefully if can't be done
                try
                {
                    AssignedTask aSenTask = new AssignedTask(srk);
                    aSenTask.findByPrimaryKey(new AssignedTaskBeanPK(sentinelTaskId));
                    aSenTask.remove();
                }
                catch (Exception eIn)
                {
                    logger.error("Expected sentinel but problem removing - ref. " + aTask.taskIdentification());
                }
            }

            if (asTransaction)
                srk.getJdbcExecutor().commit();
        }
        catch (Exception e)
        {
            logger.error("taskCompleted::updateAssignedTask: Error encountered attempting to update assigned task task");
            logger.error("taskCompleted::updateAssignedTask: deal Id=" + aTask.getDealId() + ", task Id=" + aTask.getTaskId());
            logger.error("taskCompleted::updateAssignedTask: Error = " + e.getMessage());

            if (asTransaction)
                srk.cleanTransaction();

            throw new TaskProcessingException("");
        }
    }

    private void updateDealStatus(AssignedTask aTask, int status, boolean asTransaction) throws TaskProcessingException
    {
logger.trace("taskCompleted() :: updateDealStatus(): status = " + status);

        try
        {
            if (asTransaction)
                srk.getJdbcExecutor().begin();

            aTask.setTaskStatusId(status);
            aTask.setAcknowledged("Y");
            aTask.ejbStore();

            deal.setStatusId((short)status);
            deal.ejbStore();

            if (asTransaction)
                srk.getJdbcExecutor().commit();

        }
        catch (Exception e)
        {
            logger.error("taskCompleted::updateDealStatus: Error encountered attempting to update deal id=" + deal.getDealId() + " to status=" + status);
            logger.error("taskCompleted::updateDealStatus: related task Id=" + aTask.getTaskId());
            logger.error("taskCompleted::updateDealStatus: Error = " + e.getMessage());

            if (asTransaction)
                srk.cleanTransaction();

            throw new TaskProcessingException("");
        }
    }


    private void cancelOpenTasks() throws TaskProcessingException
    {
logger.trace("taskCompleted() :: cancelOpenTasks()");

        Vector tasks = taskHistory.getAllOpenTasks();

        int len = tasks.size();

        for (int i = 0; i < len; ++i)
        {
            try
            {
                updateAssignedTask((AssignedTask)tasks.elementAt(i), Sc.TASK_CANCELLED, false);
            }
            catch (Exception e)
            {
                logger.error("taskCompleted::updateDealStatus: Error encountered attempting to cancel open tasks");
                throw new TaskProcessingException("");
            }
        }

        return;
    }

    // Use the same BusinessRule Engine for performance improvment
    //  -- By BILLY 29April2002
    private void generateLoopBackTask(AssignedTask aTask, boolean toOrigin, DealUserProfiles dealUserProfiles,
        WFDealTaskHistory taskHistory, BusinessRule br) throws TaskProcessingException
    {
logger.trace("taskCompleted() :: generateLoopBackTask(), toOrigin = " + toOrigin);

        getWorkflow(aTask.getWorkflowId());

        WorkflowTask wt = workflow.getLoopBackTask(aTask, toOrigin);

        if (wt == null)
        {
logger.trace("taskCompleted() :: generateLoopBackTask(), no loop back task found");
            return; // absence of loop back task not considered error
        }


logger.trace("taskCompleted() :: generateLoopBackTask(), loopback task = " + wt.getTaskId() + "(" + wt.getTaskLabel() + ")");

        // check to see if the task is already open - not generated if open
        if (taskHistory.isTaskOpen(wt.getTaskId(), wt.getWorkflowStage()) == true)
        {
logger.trace("taskCompleted() :: generateLoopBackTask(), loopback task already assigned and open");
            return;
        }

        // Use the same BusinessRule Engine for performance improvment
        //  -- By BILLY 29April2002
        Vector tasks = wfa.generateTask(deal, wt, dealUserProfiles, srk, br);
        //===================================================================

        // failure to generate loopback task not considered an error

        if (tasks == null)
            return;

        taskHistory.addAssignedTasks(tasks);

logger.trace("taskCompleted() :: generateLoopBackTask(), generated");
    }

    // Use the same BusinessRule Engine for performance improvment
    //  -- By BILLY 29April2002
    private void generateFollowUpTask(AssignedTask aTask, DealUserProfiles dealUserProfiles,
      WFDealTaskHistory taskHistory, BusinessRule br) throws TaskProcessingException
    {
        if (aTask.getFollowUpTaskId() == -1)
            return;

logger.trace("taskCompleted() :: generateFollowUpTask(), follow-up task id = " + aTask.getFollowUpTaskId());

        getWorkflow(aTask.getWorkflowId());

        WorkflowTask wt = workflow.getFollowUpTask(aTask);

        if (wt == null)
        {
logger.trace("taskCompleted() :: generateFollowUpTask(), no follow up task to generate (this workflow)");

            return; // absence of follow-up task not considered error
        }

        // check to see if the task is already open - not generated if open
        if (taskHistory.isTaskOpen(wt.getTaskId(), wt.getWorkflowStage()) == true)
        {
logger.trace("taskCompleted() :: generateFollowUpTask(), follow-up task already assigned and open");
            return;
        }

        // Use the same BusinessRule Engine for performance improvment
        //  -- By BILLY 29April2002
        Vector tasks = wfa.generateTask(deal, wt, dealUserProfiles, srk, br);
        //====================================================================

        // failure to generate follow-up not considered an error

        if (tasks == null)
        {
logger.trace("taskCompleted() :: generateFollowUpTask(), follow-up task failed to generate - ignored");
            return;
        }

        taskHistory.addAssignedTasks(tasks);

logger.trace("taskCompleted() :: generateFollowUpTask(), follow-up task generated");
    }

    private Workflow getWorkflow(int workflowId) throws TaskProcessingException
    {
        if (workflow != null)
            return workflow;

        try
        {
            workflow = new Workflow(srk);
            workflow = workflow.findById(workflowId);
        }
        catch (Exception e)
        {
            logger.error("taskCompleted::getWorkflow: error encountered locating workflow Id=" + workflowId);
            logger.error("taskCompleted::getWorkflow: error = " + e.getMessage());
            throw new TaskProcessingException("");
        }

        return workflow;
    }

    private Deal getDeal(int dealId, int copyId) throws TaskProcessingException
    {
        if (deal != null)
            return deal;

        try
        {
            deal = new Deal(srk, null);
            deal.findByPrimaryKey(new DealPK(dealId, copyId));
        }
        catch (Exception e)
        {
            logger.error("taskCompleted::getDeal: error encountered locating deal Id=" + dealId);
            logger.error("taskCompleted::getDeal: error = " + e.getMessage());
            throw new TaskProcessingException("");
        }

        return deal;
    }



    /** extendOpenTasks:
    *
    * Called to extend open tasks outstanding fro the deal determined via the PageEntry object
    * provided.
    *
    * See AssignedTask.reset() for details.
    *
    **/

   public void extendOpenTasks(boolean i_blnNewTransaction, SessionResourceKit i_srk, int i_lngDealId, int i_intUserId, int i_institutionId, Date i_dateNewDueDate,long i_lngNewDuration, long i_lngReminderTime, long i_lngWarningTime,long i_lngReissueAlarmDuration)throws Exception{
     try
       {
           if (i_blnNewTransaction == true)
               i_srk.beginTransaction();


           AssignedTask aTask = new AssignedTask(i_srk);

           Vector tasks = aTask.findByDeal(i_lngDealId);

           int lim = tasks.size();

           for (int i = 0; i < lim; ++i)
           {
               aTask = (AssignedTask)tasks.elementAt(i);

               if (!(aTask.getTaskStatusId() == Sc.TASK_OPEN || aTask.getTaskStatusId() == Sc.TASK_REROUTE))
                   continue; // not open status

               aTask.reset(i_intUserId, i_institutionId, i_dateNewDueDate, i_lngNewDuration, i_lngReminderTime, i_lngWarningTime, i_lngReissueAlarmDuration);

               aTask.ejbStore();
           }

           if (i_blnNewTransaction == true)
             i_srk.commitTransaction();


       }
       catch (Exception e)
       {
           if (i_blnNewTransaction == true)
               i_srk.cleanTransaction();

           SysLogger logger = i_srk.getSysLogger();
           logger.error(e);

           throw e;
       }

   }
    public void extendOpenTasks(boolean newTransaction, SessionResourceKit srk, PageEntry pg, int newUserProfileId, int newInstitutionId,
                                Date newDueDate, long newDuration, long reminderTime, long warningTime, long reissueAlarmDuration)  throws Exception
    {
        try
        {
            if (newTransaction == true)
                srk.beginTransaction();

            AssignedTask aTask = new AssignedTask(srk);

            Vector tasks = aTask.findByDeal(pg.getPageDealId());

            int lim = tasks.size();

            for (int i = 0; i < lim; ++i)
            {
                aTask = (AssignedTask)tasks.elementAt(i);

                if (!(aTask.getTaskStatusId() == Sc.TASK_OPEN || aTask.getTaskStatusId() == Sc.TASK_REROUTE))
                    continue; // not open status

                aTask.reset(newUserProfileId, newInstitutionId, newDueDate, newDuration, reminderTime, warningTime, reissueAlarmDuration);

                aTask.ejbStore();
            }

            if (newTransaction == true)
              srk.commitTransaction();

            return;
        }
        catch (Exception e)
        {
            if (newTransaction == true)
                srk.cleanTransaction();

            SysLogger logger = srk.getSysLogger();

            logger.error("Problem encountered extending open tasks for dealId = " + pg.getPageDealId());
            logger.error(e);

            throw e;
        }
    }



    // setPageForAssignedTask:
    //
    // Called to navigate to the interface page associated with the assigned task.
    //
    // Result: n > 0:  the are n pages associated with the task (see below) - the first page is set automatically
    //                 as the 'next page' in the savedPages USO
    //         0:      no page was located to display (i.e. abnormal)
    //
    // An assigned task may be associated with one, or with multiple interface pages. This
    // is determined by entries in the workflowTaskPage table. If, for a given task
    // (task Id, i.e. task definition), there is just one MOS page the method will cause
    // navigation to that page.
    //
    // If a workflow task is associated with multiple pages (multiple records in the taskPage
    // table) then the taskSequence column (in the records) indicates the presentation
    // sequence for the pages. However, not all pages need necessarily be presented. The
    // decision is based upon the 'qualify page' business rules for the workflow task.
    //
    // Qualify Page Business Rules
    // ---------------------------
    //
    // A qualify page business rule is identified by the following attributes:
    //
    // workflow Id, task Id, and qualify page business rule name.
    //
    // The workflow and task attributes are derived from the assigned task. A qualify page
    // business rule name is formed as follows:
    //
    //        "QP_{page Id}"
    //
    // For example, if pages with ids 10 and 15 are associated with the task in question
    // the qualify page business names would be "QP_10" and "QP_15". Futher assume that
    // the workflow and task Ids (from the assigned task) are 34 and 23 respectively, then
    // we would be looking for business rules ifentidied as:
    //
    //    Workflow Id    Task Id    Event   Business Rule Name
    //         34           23       ""         "QP_10"
    //         34           23       ""         "QP_15"
    //
    // The qualify page business rule is evaluated to deternmine if the MOS page should
    // be presented (displayed) to the user. The business rule expression should evaluate to
    // TRUE or FALSE. Result is intrepreted as follows:
    //
    //     TRUE  - page (referenced by Id) should be presented to user
    //     FALSE - page does not need to be displayed to the user.
    //
    // When an expression return TRUE the rule result set for the expression then contains:
    //
    //  Result Set Operand One: Warning Text.
    //
    //          This should be a short string (i.e. single word, or short expression - not
    //          more than about 20 characters) that contains an indication of why the
    //          page is to be displayed (i.e. "Credit Problem", "Missing Data", etc.)
    //
    //          If this operand may be missing (implying no wwarning text).
    //
    //  Notes:
    //
    //  The absence of a qualify page business rule indiates that the page should be
    //  presented.
    //
    //  Multiple expressions are allowed for a given qualify page business rule. The
    //  expressions are evaluated one after the other untill one expression yield TRUE.
    //
    // MOS Page Entry Standards For Multi-Page Tasks
    // ---------------------------------------------
    //
    // The following page entries properties are used to represent multi-paged tasks:
    //
    //         Key                Value         Description
    //     ---------------    -------------     -----------
    //     taskNavigateMode     true/false      Page usage in context of selected task
    //     taskName               String        name of the task
    //     prevPageName           String        label identifying the prev page (null if none)
    //     prevPE                PageEntry      previous pages' page entry (null if none)
    //     nextPageName           String        label identifying the next page (null if none)
    //     nextPE                PageEntry      next pages' page entry (null if none)
    //

    public int setPageForAssignedTask(AssignedTask aTask, int userProfileId, int userTypeId) throws TaskProcessingException
    {
        srk    = phc.getSessionResourceKit();
        logger = srk.getSysLogger();
        jExec  = srk.getJdbcExecutor();

        /// logger.trace("--T--> @WFE.setPageForAssignedTask :: IN");

        Vector pgTask = null;

        WorkflowTaskPage tp = new WorkflowTaskPage(srk);
        Exception pageLinksException = null;
        try
        {
            /// logger.trace("--T--> @WFE.setPageForAssignedTask :: do tp.findByTask()");
            pgTask = tp.findByTask(aTask.getTaskId(), aTask.getWorkflowId());
        }
        catch (Exception e) { pgTask = null; pageLinksException = e; }

        // check abnormal case - no pagesd associated with task
        if (pgTask == null || pgTask.size() == 0)
        {
            logger.trace("Exception @WFE.setPageForAssignedTask: No page linkages (WorkflowTaskpage records) for task=" + aTask.getTaskLabel() + ", taskId=" + aTask.getTaskId() + ", WFId=" + aTask.getWorkflowId());
            if (pageLinksException != null)
                logger.error(pageLinksException);

            return 0;
        }

        // show vector (holds entries for pages to be displayed)
        Vector showPgTask = new Vector();

        int numPages = pgTask.size();

        if (numPages == 1)
        {
          showPgTask = pgTask;
        }
        else
        {
            /// logger.trace("--T--> @WFE.setPageForAssignedTask :: multiple pages :: pgTask.size() = " + pgTask.size());

            // multiple pages ... execute business rules to qualify pages
            boolean displayPage    = true;

            // for the deal point to the gold copy
            int copyId = 1;
            try
            {
            Deal deal = new Deal(srk, null);
            deal = deal.findByGoldCopy(new DealPK(aTask.getDealId(), -1));
            copyId = deal.getCopyId();
            } catch (Exception e) {;}

            for (int i = 0; i < numPages; ++i)
            {
                tp = (WorkflowTaskPage)pgTask.elementAt(i);

                logger.trace("--T--> @WFE.setPageForAssignedTask :: page[" + i + "]  - WorkflowTaskPage tp.getPageId() = " + tp.getPageId());

                try
                {
                  displayPage = qualifyPage( srk, aTask, tp, userTypeId, copyId);
                }
                catch (Exception e)
                {
                  logger.error("--T--> @WFE.setPageForAssignedTask :: page[" + i + "]  - Exception in qualify page business rule");
                  logger.error(e);

                  displayPage = true;
                }

                if (displayPage)
                    showPgTask.addElement(tp);
            }
        }

        numPages = showPgTask.size();

        if (numPages == 0)
        {
            // case when there is only one page (business rule not even checked for) as
            // well as precautionary code should the business rules have disqualified all pages

            numPages = 1;
            showPgTask.addElement(pgTask.lastElement());

            /// logger.trace("--T--> @WFE.setPageForAssignedTask :: all pages eliminated - using last page");
        }

        pgTask = showPgTask;

        // set up page entries
        Vector pageEntries = new Vector();

        /// logger.trace("--T--> @WFE.setPageForAssignedTask :: Setup page entries");

        Page pgDetails = null;

        for (int i = 0; i < numPages; ++i)
        {
            tp = (WorkflowTaskPage)pgTask.elementAt(i);

            try
            {
              pgDetails = (PDC.getInstance()).getPage(tp.getPageId());
            }
            catch (Exception e)
            {
                logger.error("Exception @WFTaskExecServices.setPageForAssignedTask :: Cannot locate Page details for task linkage page - skip page: pageId=" + tp.getPageId() + ", task=" + aTask.getTaskLabel());
                continue;
            }

            PageEntry pEntry = new PageEntry();

            pEntry.setTaskNavigateMode(true);
            pEntry.setTaskName(aTask.getTaskLabel());

            pEntry.setPageTaskId(aTask.getTaskId());
            pEntry.setPageTaskSequence(tp.getTaskSequence());

            pEntry.setPageMosUserId(userProfileId);
            pEntry.setPageMosUserTypeId(userTypeId);

            pEntry.setPageId(pgDetails.getPageId());
            pEntry.setPageName(pgDetails.getPageName());
            pEntry.setPageLabel(pgDetails.getPageLabel());
            pEntry.setDealPage(pgDetails.getDealPage());

            pEntry.setPageDealId(aTask.getDealId());
            pEntry.setPageDealCID(-1);
            pEntry.setDealInstitutionId(aTask.getInstitutionProfileId());
            pEntry.setPageDealApplicationId(aTask.getApplicationId());

            ////// pEntry.setPageDisplayMethod(Mc.DISPLAY_METHOD_LOAD);

            // Special handling for SOB page -- Billy 29Nov2001
            if(pgDetails.getPageId() == Sc.PGNM_SOB_REVIEW)
            {
              // Pass the deal.SourceOfBusinessId to display
              try {
                Deal deal = new Deal(srk, null);
                try{
                deal = deal.findByRecommendedScenario(new DealPK(aTask.getDealId(), -1), true);
                }
                catch(Exception e)
                {
                logger.warning("@ Setting up SOB task page -- No Recommended Copy found !! Used Gold Copy as fallback. Dealid = " + aTask.getDealId());
                deal = deal.findByGoldCopy(new DealPK(aTask.getDealId(), -1));
                }

                // In order to pass the Copyid correctly we need to set it to DealPage
                //    -- ie. in navigateToNextPage method it will set it to -1 if there is no Tx copy
                //            for non-Deal pages.
                pEntry.setDealPage(true);

                Hashtable subPst = pEntry.getPageStateTable();
                if(subPst == null)
                {
                  subPst = new Hashtable();
                  pEntry.setPageStateTable(subPst);
                }

               subPst.put(ISourceBusinessReviewHandler.SOB_ID_PASSED_PARM,
                        new String(""+deal.getSourceOfBusinessProfileId()));

                // Set pageCondition 3 to call workfloe trigger when submit
                pEntry.setPageCondition3(true);
              } catch (Exception e) {
                logger.error("Exception @WFE.setPageForAssignedTask when setting up Task SPB review page : " + e.getMessage());
              }
            }

            // Special handling for SourceFirm page -- Billy 10April2002
            if(pgDetails.getPageId() == Sc.PGNM_SFIRM_REVIEW)
            {
              // Pass the deal.SourceOfBusinessId to display
              try {
                Deal deal = new Deal(srk, null);
                try{
                deal = deal.findByRecommendedScenario(new DealPK(aTask.getDealId(),-1), true);
                }
                catch(Exception e)
                {
                logger.warning("@ Setting up SFirmB task page -- No Recommended Copy found !! Used Gold Copy as fallback. Dealid = " + aTask.getDealId());
                deal = deal.findByGoldCopy(new DealPK(aTask.getDealId(),-1));
                }

                // In order to pass the Copyid correctly we need to set it to DealPage
                //    -- ie. in navigateToNextPage method it will set it to -1 if there is no Tx copy
                //            for non-Deal pages.
                pEntry.setDealPage(true);

                Hashtable subPst = pEntry.getPageStateTable();

                if(subPst == null)
                {
                  subPst = new Hashtable();
                  pEntry.setPageStateTable(subPst);
                }

                subPst.put(ISourceFirmReviewHandler.SFIRM_ID_PASSED_PARM,
                        new String(""+deal.getSourceFirmProfileId()));

                // Set pageCondition 3 to call workfloe trigger when submit
                pEntry.setPageCondition3(true);
              } catch (Exception e) {
                logger.error("Exception @WFE.setPageForAssignedTask when setting up Task SFirm review page : " + e.getMessage());
              }
            }

            pageEntries.addElement(pEntry);
        }

        /// logger.trace("--T--> @WFE.setPageForAssignedTask :: Setup page links");

        // setup links between page entries (via page state table entries)
        linkTaskPages(pageEntries);

        // navigate to first page in sequence
        /// logger.trace("--T--> @WFE.setPageForAssignedTask :: Set first task page as next page");

        phc.getSavedPages().setNextPage((PageEntry) pageEntries.elementAt(0));

        return numPages;
    }

    public void linkTaskPages(Vector pageEntries)
    {
        int numPages  = pageEntries.size();
        int lastNdx   = numPages - 1;
        for (int i = 0; i < numPages; ++i)
        {
            PageEntry pe = (PageEntry)pageEntries.elementAt(i);

            // set up 'next page' variables
            if (i != lastNdx)
            {
                PageEntry nextPe = (PageEntry)pageEntries.elementAt(i+1);
                pe.setNextPageName(nextPe.getPageLabel());
                pe.setNextPE(nextPe);
            }
            else
            {
                pe.setNextPageName(null);
                pe.setNextPE(null);
            }

            // set up 'prev page' variables
            if (i != 0)
            {
                PageEntry prevPe = (PageEntry)pageEntries.elementAt(i-1);

                pe.setPrevPageName(prevPe.getPageLabel());
                pe.setPrevPE(prevPe);
            }
            else
            {
                pe.setPrevPageName(null);
                pe.setPrevPE(null);
            }
        }
    }

    private boolean qualifyPage(SessionResourceKit srk, AssignedTask aTask, WorkflowTaskPage tp, int userTypeId, int copyId) throws Exception
    {
        boolean displayPage = true;
        //boolean haveExpression = false;

        srk.setCloseJdbcConnectionOnRelease(true);

        try
        {
            // retrieve qualify page business rule expression(s)
            BusinessRule br = new BusinessRule(srk,
                                                aTask.getWorkflowId(), aTask.getTaskId(),
                                               "na", "QP_" + tp.getPageId());

logger.trace("setPageForAssignedTask :: Test Page Qualify Business Rule ::  wfid=" + aTask.getWorkflowId() + ", taskId=" + aTask.getTaskId()+ ", ruleName=<" + ("QP_" + tp.getPageId()) + ">");

logger.trace("setPageForAssignedTask :: Test Page Qualify Business Rule ::  br = " + br);

            // set deal for business rule engine
            br.setDealId(aTask.getDealId(), copyId);
            
            // #2242: Catherine, 23-Nov-05 --------- start ---------  
            br.setLenderName();
            BRUtil.debug("WFTaskExecServices: set LenderName done");
            // #2242: Catherine, 23-Nov-05 --------- end ---------  

            br.setUserTypeId(userTypeId);
            br.setRecordId(aTask.getExternalRecordId());



            //haveExpression = false;
            displayPage    = true;

            while(br.next())
            {

                displayPage = false;  // have at least one expression - require true
                                      // from expression for display

                RuleResult rr = br.evaluate();

                if (rr.ruleStatus == true)
                {
logger.trace("setPageForAssignedTask :: Expression = true");

                    displayPage = true;
                    break;
                }
logger.trace("setPageForAssignedTask :: Expression = false");

            }

            br.close();

        }
        catch (Exception e)
        {
            logger.error("setPageForAssignedTask: Error. Business Rule Engine exception encountered; Business Rule=" + ("QP_" + tp.getPageId()) + ", deal id=" + deal.getDealId());
            logger.error("setPageForAssignedTask: Error = " + e.getMessage());

            throw new TaskProcessingException("");
        }
        catch (Error er)
        {
            logger.error("setPageForAssignedTask: Error. Business Rule Engine exception encountered; Business Rule=" + ("QP_" + tp.getPageId()) + ", deal id=" + deal.getDealId());
            logger.error("setPageForAssignedTask: Error = " + er.getMessage());

            throw new TaskProcessingException("");
        }

        return displayPage;
    }

    public boolean cancelOpenClosingTasks()
    {
      srk    = phc.getSessionResourceKit();
      logger = srk.getSysLogger();
      jExec  = srk.getJdbcExecutor();

      int dealId     = -1;
      int copyId     = -1;
      int userId     = -1;
      int userTypeId = -1;
      int workflowId = -1;

      Vector openTasks                  = null;
      DealUserProfiles dealUserProfiles = null;

      int openTaskCount                 = 0;
      boolean done                      = false;
      boolean taskCancelled              = false;

      // setup ...
      try
      {
        dealId = pg.getPageDealId();
        copyId = pg.getPageDealCID();
        userId = pg.getPageMosUserId();
        userTypeId = pg.getPageMosUserTypeId();

        logger.trace("cancelOpenClosingTasks() :: dealId = " + dealId + ", userId = " + userId);

        // retrieve deal
        getDeal(dealId, copyId);

        // retrieve the task history instance
        taskHistory = new WFDealTaskHistory(phc.getSessionResourceKit(), dealId);
        workflowId  = taskHistory.getWorkflowId();

        logger.trace("cancelOpenClosingTasks() :: workflowId = " + workflowId);

        if (workflowId == -1)
          return true;       // no task history - hence no open tasks - nothing to do

        // retrieve list of open assigned tasks ...

        // all open tasks
        openTasks = taskHistory.getAllOpenTasks();
        openTaskCount = openTasks.size();

        logger.trace("cancelOpenClosingTasks() :: # open tasks  = " + openTaskCount);

        // setup deal user profiles helper object (retains unerwriter/administrator profiles
        // for deal as/when required - essentially caches these once obtained)
        dealUserProfiles = new DealUserProfiles(deal, srk);

        dealUserProfiles.traceShow(logger);
      }
      catch (Exception e)
      {
          logger.error("cancelOpenClosingTasks(): Error obtaining open assigned tasks");
          done    = true;
      }

      for (int i = 0; i < openTaskCount && (!done); ++i)
      {
        AssignedTask aTask = (AssignedTask)openTasks.elementAt(i);

        WorkflowTask wtTask = new WorkflowTask(srk);        
        workflowId = aTask.getWorkflowId();
        int taskId = aTask.getTaskId();

        try
        {
           wtTask = wtTask.findByPrimaryKey(new WorkflowTaskBeanPK(workflowId,taskId));
        }
        catch (Exception e)
        {
           logger.error("cancelOpenClosingTasks(): Error obtaining workflowTask definition (entity)");

           return false;
        }


        if (wtTask.getDuration() > 0 || ((wtTask.getBypassWFRegression()!= null) && (wtTask.getBypassWFRegression().equals("Y")))) continue;

        try
        {
            logger.debug("cancelOpenClosingTasks()@WFTaskExecService Thread ID = " + Thread.currentThread().getId() );
            logger.debug("cancelOpenClosingTasks()@WFTaskExecService Thread Name = " + Thread.currentThread().getName() );
        	
            // WF Regression Issue wrong assignee - OCT 1, 2008 STARTS ---
            // find current userprofileId
           int assigneeeId = aTask.getUserProfileId();
           // set pass current userprofileId * -1
           // this change made for Workflow Regression task disapear issue
           aTask.setPass(assigneeeId * -1);
           logger.debug("cancelOpenClosingTasks() taskid= " + aTask.getTaskId() + " : Set pass as " + aTask.getPass());
           // WF Regression Issue wrong assignee - OCT 1, 2008 ENDS ---
           logger.debug("**** @WFTaskExecService UPDATE TASK AS CANCEL STARTED at:" + System.currentTimeMillis());
           updateAssignedTask(aTask, Sc.TASK_CANCELLED, false);
           logger.debug("**** @WFTaskExecService UPDATE TASK AS CANCEL ENDED at:" + System.currentTimeMillis());
           taskCancelled = true;

        }
           catch (Exception e)
        {
           logger.error("cancelOpenClosingTasks(): Error set AssignedTask task status to Cancel");
           return false;
        }
      }
      
      if (taskCancelled) 
      {
        try
        {
	      // create history record for transaction
          DealHistory hist = new DealHistory(srk);
          hist.create(dealId, srk.getExpressState().getUserProfileId(), Mc.DEAL_TX_TYPE_EVENT, deal.getStatusId(), "Deal Mod: workflow regression due to change in closing date - Workflow", new java.util.Date());
        }
        catch (Exception e)
        {
          logger.error("cancelOpenClosingTasks(): Error create a history record of workflow regression");
          return false;
        }
      } 

      return true;
   }
}




