package com.basis100.log;

/**
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 */

import java.io.Serializable;

/*
 *  SysLogger:
 *
 *  Convienence class for access to system logger instance (see SysLog.java). It contains
 *  the following (wrapper) methods:
 *
 *    1. trace(String msg)
 *    2. debug(String msg)
 *    3. info(String msg)
 *    4. warning(String msg)
 *    5. error(String msg)
 *    6. error(Throwable e)
 *
 * The methods correspond one-to-one with static SysLog methods except that here a
 * login Id parameter is not required.
 *
 * An instance of the inner class is provided by the following SysLog method:
 *
 *    public static SysLogger getSysLogger(String loginId)
 *
 * The login Id is stored in the instance - it is provided in call to SysLog methods by the
 * wrapper methods.
 *
 * Extensions:
 *
 * Usage of a SysLogger instance also provides a mechanism for disabling debug and trace
 * logger messages. Generation of these message is normally controlled by the SysLog properties
 * file (see SysLog.java). The following are used:
 *
 *    . public setDebugOn(boolean b)
 *
 *      Enable of disable generation of debug records from this object. Default is true.
 *
 *      Important. This method cannot enable generation of debug messages if the SysLog (singleton
 *      logger instance) does not generate debug messages.
 *
 *    . public setTraceOn(boolean b)
 *
 *      Enable of disable generation of trace records from this object. Default is true.
 *
 *      Important. This method cannot enable generation of trace messages if the SysLog (singleton
 *      logger instance) does not generate trace messages.
 */

public class SysLogger implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// Keep the following for legacy code.
	boolean debugOn = true;
	boolean traceOn = true;
	boolean jdbcTraceOn = true;

	boolean calcTraceOn = true;

	boolean traceWas = true;
	boolean debugWas = true;
	boolean jdbcTraceWas = true;

	// allow setting of debug, trace, and jdbc trace logging in single call

	public void setDebugTraceJdbcTrace(boolean debugOn, boolean traceOn,
			boolean jdbcTraceOn) {
		setDebugOn(debugOn);
		setTraceOn(traceOn);
		setJdbcTraceOn(jdbcTraceOn);
	}

	public void setDebugTraceJdbcTraceRestore() {
		setDebugRestore();
		setTraceRestore();
		setJdbcTraceRestore();
	}

	public boolean isDebugOn() {
		return debugOn;
	}

	public void setDebugOn(boolean b) {
		debugWas = debugOn;
		debugOn = b;
	}

	public void setDebugRestore() {
		debugOn = debugWas;
	}

	public boolean isTraceOn() {
		return traceOn;
	}

	public void setTraceOn(boolean b) {
		traceWas = traceOn;
		traceOn = b;
	}

	public void setTraceRestore() {
		traceOn = traceWas;
	}

	public boolean isJdbcTraceOn() {
		return jdbcTraceOn;
	}

	public void setJdbcTraceOn(boolean b) {
		jdbcTraceWas = jdbcTraceOn;
		jdbcTraceOn = b;
	}

	public void setJdbcTraceRestore() {
		jdbcTraceOn = jdbcTraceWas;
	}

	public boolean isCalcTraceOn() {
		return calcTraceOn;
	}

	public void setCalcTraceOn(boolean b) {
		calcTraceOn = b;
	}

	// Reimplement the APIs.
	private String loginId;

	public SysLogger(String loginId) {
		this.loginId = loginId;
	}

	//#DG600 convenience methods
	public SysLogger(Object caller, String loginId) {
		_clazz = caller.getClass();
		this.loginId = loginId;
	}

	private Class _clazz;

	public void setClass(Class clazz) {
		_clazz = clazz;
	}

	//#DG600 convenience methods
	public void setClass(Object caller) {
		_clazz = caller.getClass();
	}

	public void debug(String msg) {
		if (debugOn == false)
			return;
		if (_clazz != null) {
			debug(_clazz, msg);
		}
		else {			
			SysLog.debug(msg, loginId);
		}
	}

	public void debug(Class clazz, String msg) {

		SysLog.debug(clazz, msg, loginId);
	}

	public void error(String msg) {

		if (_clazz != null) {
			error(_clazz, msg);
		}
		else {	
			SysLog.error(msg, loginId);
		}
	}

	public void error(Class clazz, String msg) {

		SysLog.error(clazz, msg, loginId);
	}

	public void error(Throwable e) {

		if (_clazz != null) {
			error(_clazz, e);
		}
		else {
			SysLog.error(e, loginId);
		}
	}

	public void error(Class clazz, Throwable e) {

		SysLog.error(clazz, e, loginId);
	}

	public void info(String msg) {

		if (_clazz != null)
			info(_clazz, msg);
		else
			SysLog.info(msg, loginId);
	}

	public void info(Class clazz, String msg) {

		SysLog.info(clazz, msg, loginId);
	}

	public void trace(String msg) {

		if (_clazz != null)
			trace(_clazz, msg);
		else
			SysLog.trace(msg, loginId);
	}

	public void trace(Class clazz, String msg) {

		SysLog.trace(clazz, msg, loginId);
	}

	public void warning(String msg) {

		if (_clazz != null)
			warning(_clazz, msg);
		else
			SysLog.warning(msg, loginId);
	}

	public void warning(Class clazz, String msg) {

		SysLog.warning(clazz, msg, loginId);
	}

	public void jdbcTrace(String msg) {

		if (_clazz != null)
			jdbcTrace(_clazz, msg);
		else
			SysLog.jdbcTrace(msg, loginId);
	}

	public void jdbcTrace(Class clazz, String msg) {

		SysLog.jdbcTrace(clazz, msg, loginId);
	}

	public void calcTrace(String msg) {

		if (_clazz != null)
			calcTrace(_clazz, msg);
		else
			SysLog.calcTrace(msg, loginId);
	}

	public void calcTrace(Class clazz, String msg) {

		SysLog.calcTrace(clazz, msg, loginId);
	}

	public void mail(String msg) {
		SysLog.mail(msg, loginId);
	}
}
