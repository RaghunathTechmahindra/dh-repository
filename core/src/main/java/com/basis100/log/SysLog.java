/*
 * @(#)SysLog.java    2005-7-8
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.log;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import config.Config;

/**
 * SysLog - Reimplemented the legacy Filogix eXpress syslog by using the
 * commons-logging abstract APIs.
 *
 * @version   1.0 2005-7-8
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class SysLog {

    // The logger
    private final static Log _log = LogFactory.getLog(SysLog.class);

    private static SimpleDateFormat _tsFmt =
		new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss:SSS");

    /**
     * Constructor function
     */
    private SysLog() {
    }

    /**
     * prepar the log message.
     */
    private static String formLogMessage(String type, String message, String loginId) {

        // we are going to use log4j, log4j will do this.
        //rollLogFile(true);

        loginId = " [" + (loginId + "             ").substring(0, 10) + "] ";
        return getTimeStamp(new Date()) + " " + type + loginId +  message;
    }

    /**
     * format the date!
     */
    protected static String getTimeStamp(Date d) {

        return _tsFmt.format(d);
    }

    // we are using log4j's config file to set up the following ....
    public static boolean isJdbcTraceOn() {

        return false;
    }

    public static boolean isCalcTraceOn() {

        return true;
    }

    private static boolean _mailOn = false;
    public static boolean isMailOn() {

        return _mailOn;
    }

    // the logging methods.

    /**
     * debug level.
     */
    public static void debug(String msg) {

        debug(msg, "{unknown}");
    }

    /**
     */
    public static void debug(String msg, String loginId) {

        if (_log.isDebugEnabled())
            _log.debug(formLogMessage("D", msg, loginId));
    }

    /**
     */
    public static void debug(Class clazz, String msg) {

        debug(clazz, msg, "{unknown}");
    }

    /**
     */
    public static void debug(Class clazz, String msg, String loginId) {

        Log log = LogFactory.getLog(clazz);
        if (log.isDebugEnabled())
            log.debug(formLogMessage("D", msg, loginId));
    }

    /**
     * trace level.
     */
    public static void trace(String msg) {

        trace(msg, "(unknown)");
    }

    /**
     */
    public static void trace(String msg, String loginId) {

        if(_log.isTraceEnabled())
            _log.trace(formLogMessage("T", msg, loginId));
    }

    /**
     */
    public static void trace(Class clazz, String msg) {

        trace(clazz, msg, "{unknown}");
    }

    /**
     */
    public static void trace(Class clazz, String msg, String loginId) {

        Log log = LogFactory.getLog(clazz);
        if (log.isTraceEnabled())
            log.trace(formLogMessage("D", msg, loginId));
    }

    /**
     * info level.
     */
    public static void info(String msg) {

        info(msg, "{unknown}");
    }

    /**
     */
    public static void info(String msg, String loginId) {

        _log.info(formLogMessage("I", msg, loginId));
    }

    /**
     */
    public static void info(Class clazz, String msg) {

        info(clazz, msg, "{unknown}");
    }

    /**
     */
    public static void info(Class clazz, String msg, String loginId) {

        Log log = LogFactory.getLog(clazz);
        log.info(formLogMessage("I", msg, loginId));
    }

    /**
     * warning level.
     */
    public static void warning(String msg) {

        warning(msg, "(unknown)");
    }

    /**
     */
    public static void warning(String msg, String loginId) {

        _log.warn(formLogMessage("W", msg, loginId));
    }

    /**
     */
    public static void warning(Class clazz, String msg) {

        warning(clazz, msg, "{unknown}");
    }

    /**
     */
    public static void warning(Class clazz, String msg, String loginId) {

        Log log = LogFactory.getLog(clazz);
        log.warn(formLogMessage("W", msg, loginId));
    }

    /**
     * error level.
     */
    public static void error(String msg) {

        error(msg, "{unknown}");
    }

    /**
     */
    public static void error(String msg, String loginId) {

        _log.error(formLogMessage("E", msg, loginId));
    }

    /**
     */
    public static void error(Class clazz, String msg) {

        error(clazz, msg, "{unknown}");
    }

    /**
     */
    public static void error(Class clazz, String msg, String loginId) {

        Log log = LogFactory.getLog(clazz);
        log.error(formLogMessage("E", msg, loginId));
    }

    /**
     */
    public static void error(Throwable th) {

        error(th, "{unknown}");
    }

    /**
     */
    public static void error(Throwable th, String loginId) {

        _log.error(formLogMessage("E", "" + th, loginId), th);
    }

    /**
     */
    public static void error(Class clazz, Throwable th) {

        error(clazz, th, "{unknown}");
    }

    /**
     */
    public static void error(Class clazz, Throwable th, String loginId) {

        Log log = LogFactory.getLog(clazz);
        log.error(formLogMessage("E", "" + th, loginId));
    }

    /**
     * jdbc trace.
     */
    public static void jdbcTrace(String msg) {

        jdbcTrace(msg, "{unknown}");
    }

    /**
     */
    public static void jdbcTrace(String msg, String loginId) {

        if(_log.isTraceEnabled())
            _log.trace(formLogMessage("J", msg, loginId));
    }

    /**
     */
    public static void jdbcTrace(Class clazz, String msg) {

        jdbcTrace(clazz, msg, "{unknown}");
    }

    /**
     */
    public static void jdbcTrace(Class clazz, String msg, String loginId) {

        Log log = LogFactory.getLog(clazz);
        if (log.isTraceEnabled())
            log.trace(formLogMessage("J", msg, loginId));
    }

    /**
     * calc trace
     */
    public static void calcTrace(String msg) {

        calcTrace(msg, "{unknown}");
    }

    /**
     */
    public static void calcTrace(String msg, String loginId) {

        if (_log.isTraceEnabled()) 
            _log.trace(formLogMessage("C", msg, loginId));
    }

    /**
     */
    public static void calcTrace(Class clazz, String msg) {

        calcTrace(clazz, msg, "{unknown}");
    }

    /**
     */
    public static void calcTrace(Class clazz, String msg, String loginId) {

        Log log = LogFactory.getLog(clazz);
        if (log.isTraceEnabled())
            log.trace(formLogMessage("C", msg, loginId));
    }

    /**
     * return an instace of SysLogger.
     */
    public static SysLogger getSysLogger(String loginId) {

        // record the loginId.
		// TODO: it is not a good solution.  we should cach the logger for each
		// loginid instead of creating one new instance every time!
        return new SysLogger(loginId);
    }

    /**
     * for mail.
     * DO NOTHING FOR NOW.
     */
    public static void mail(String msg, String loginId) {
        // DO NOTHING FOR NOW.
    }

    /**
     * init.  DO NOTING FOR NOW.
     */
    public static void init(String path) {
    }

    /**
     * shutdown.
     */
    public static void shutdown() {
    }
}
