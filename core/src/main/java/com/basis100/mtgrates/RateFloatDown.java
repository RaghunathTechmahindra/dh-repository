package com.basis100.mtgrates;
/**
 * <p>
 * Title: RateFloatDown
 * </p>
 * <p>
 * Description: This class will perform the Ratefloat down and returns the best RFD rate.
 * </p>
 * @author MCM Impl Team<br>
 * @version 1.0 XS_9.3 Modified the methods to change 
 * 			the rate float down functionality to Component level.
 */
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.date.DateUtilJavaFmt;

public class RateFloatDown {
    private Deal deal;
    private RateFloatDownSelector rfdSelector;
    private int langId;
    
    /**
     * Constructor to set the object's deal, languageId, and RateFloatDownSelector
     * 
     * @param deal
     * @param langId
     * @throws Exception
     */
    public RateFloatDown(Deal d, int lang) throws Exception
    {
        deal = d;
        langId = lang;
        try {
            int rfdtypeid = deal.getRateFloatDownTypeId();
            rfdSelector = RateFloatDownSelectorImpl.getRfdSelector(rfdtypeid);
        }
        catch(Exception e)
        {
            // Default as English system message
            ResourceBundle pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("en", ""));
            // if French
            if (langId != 0)
            {
                pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("fr", ""));
            }
            String errMsg = pRsBundle.getString("RATE_FLOAT_DOWN_UNEXPECTED_FAILURE");
            Exception fe = new Exception("Exception caught while performing Rate Float Down: " + errMsg);
            throw fe;
        }
    }
    
    /**
     * 
     * Description: Constructor to set the the object's languageId, and RateFloatDownSelector
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     *  
     * @param srk
     * @param dcm
     * @param component
     * @param commitmentIssueDate
     * @param langid
     * @throws Exception
     */
    public RateFloatDown(SessionResourceKit srk,CalcMonitor dcm,Component component,Date commitmentIssueDate,int langid) throws Exception
    {
        langId = langid;
        try {
        	MtgProd mtgProd=new MtgProd(srk,dcm,component.getMtgProdId());
            int rfdtypeid = mtgProd.getRateFloatDownTypeId();
            rfdSelector = RateFloatDownSelectorImpl.getRfdSelector(rfdtypeid);
        }
        catch(Exception e)
        {
            // Default as English system message
            ResourceBundle pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("en", ""));
            // if French
            if (langId != 0)
            {
                pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("fr", ""));
            }
            String errMsg = pRsBundle.getString("RATE_FLOAT_DOWN_UNEXPECTED_FAILURE");
            Exception fe = new Exception("Exception caught while performing Rate Float Down: " + errMsg);
            throw fe;
        }
    }
    /**
     * <p>
     * Description: Returns a String containing the pricingRateInventoryId and the
     * internalRatePercentage according to the Rate Float Down selection.
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * @param component
     * @param commitmetIssueDate
     * @return
     * @throws Exception
     */
     
    public PricingRateInventory performRateFloatDown(Component component,Date commitmetIssueDate) throws Exception
    {
        try {
            // get the associated deal and set up the needed attributes
            PricingRateInventory pri = rfdSelector.getPriOfBestRate(component,commitmetIssueDate);
            return pri;
        }
        catch(Exception e)
        {
            // Default as English system message
            ResourceBundle pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("en", ""));
            // if French
            if (langId != 0)
            {
                pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("fr", ""));
            }
            String errMsg = pRsBundle.getString("RATE_FLOAT_DOWN_UNEXPECTED_FAILURE");
            Exception fe = new Exception("Exception caught while performing Rate Float Down: " + errMsg);
            throw fe;
        }
    }
    /**
     * Returns a String containing the pricingRateInventoryId and the
     * internalRatePercentage according to the Rate Float Down selection
     * 
     * @return PricingRateInventory
     * @throws Exception
     */
    public PricingRateInventory performRateFloatDown() throws Exception
    {
        try {
            // get the associated deal and set up the needed attributes
            PricingRateInventory pri = rfdSelector.getPriOfBestRate(deal);
            return pri;
        }
        catch(Exception e)
        {
            // Default as English system message
            ResourceBundle pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("en", ""));
            // if French
            if (langId != 0)
            {
                pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("fr", ""));
            }
            String errMsg = pRsBundle.getString("RATE_FLOAT_DOWN_UNEXPECTED_FAILURE");
            Exception fe = new Exception("Exception caught while performing Rate Float Down: " + errMsg);
            throw fe;
        }
    }
    /**
     * <p>
     * Description: Returns a boolean depending on whether the value of the RFD rate is
     * lesser than the posted rate
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * @param rfdValue
     * @param component
     * @return boolean
     *          - true if RFD is lesser
     *          - false if RFD is greater than or equal to
     */
    public boolean isRfdRateBest(double rfdValue,Component component) throws Exception
    {
        try {
            boolean isRfdBest = rfdSelector.isRfdRateBest(component.getPostedRate(), rfdValue);
            return isRfdBest;
        }
        catch(Exception e)
        {
            Exception fe = new Exception("Exception caught while performing Rate Float Down: checkIfRfdIsGreaterThanPostedRate(double)");
            throw fe;
        }
    }
    /**
     * <p>
     * Description: Returns a boolean depending on whether the value of the RFD rate is
     * valid.
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * @param rfdValue
     * @param component
     * @return boolean
     *          - true if valid
     *          - false if invalid
     */
    public boolean isValidRate(double rfdValue,Component component) throws Exception
    {
        try {
            boolean isValidRfdRate= rfdSelector.isValidRate(component, rfdValue);
            return isValidRfdRate;
        }
        catch(Exception e)
        {
            // Default as English system message
            ResourceBundle pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("en", ""));
            // if French
            if (langId != 0)
            {
                pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("fr", ""));
            }
            String errMsg = pRsBundle.getString("RATE_FLOAT_DOWN_INVALID_VALUE");
            Exception fe = new Exception("Exception caught while performing Rate Float Down: " + errMsg);
            throw fe;
        }
    }
    
    /**
     * Returns a boolean depending on whether the value of the RFD rate is
     * valid
     *       
     * @param rfdValue
     * @return boolean
     *          - true if valid
     *          - false if invalid
     */
    public boolean isValidRate(double rfdValue) throws Exception
    {
        try {
            boolean isValidRfdRate= rfdSelector.isValidRate(deal, rfdValue);
            return isValidRfdRate;
        }
        catch(Exception e)
        {
            // Default as English system message
            ResourceBundle pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("en", ""));
            // if French
            if (langId != 0)
            {
                pRsBundle = ResourceBundle.getBundle("BXResources.SystemMessage", new Locale("fr", ""));
            }
            String errMsg = pRsBundle.getString("RATE_FLOAT_DOWN_INVALID_VALUE");
            Exception fe = new Exception("Exception caught while performing Rate Float Down: " + errMsg);
            throw fe;
        }
    }

    /**
     * Returns a boolean depending on whether the value of the RFD rate is
     * lesser than the posted rate
     *       
     * @param rfdValue
     * @return boolean
     *          - true if RFD is lesser
     *          - false if RFD is greater than or equal to
     */
    public boolean isRfdRateBest(double rfdValue) throws Exception
    {
        try {
            boolean isRfdBest = rfdSelector.isRfdRateBest(deal.getPostedRate(), rfdValue);
            return isRfdBest;
        }
        catch(Exception e)
        {
            Exception fe = new Exception("Exception caught while performing Rate Float Down: checkIfRfdIsGreaterThanPostedRate(double)");
            throw fe;
        }
    }
}
