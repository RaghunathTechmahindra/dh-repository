package com.basis100.mtgrates;

import com.basis100.deal.entity.*;
import com.basis100.resources.PropertiesCache;
import com.basis100.util.date.*;

import java.util.*;

/** 
 * <p>Title: RateFloatDownSelector</p> 
 * 
 * <p>Description: Super class for the Rate Float Down selectors.</p> 
 * 
 * <p>Copyright: Filogix Inc. (c) 2006</p> 
 * 
 * <p>Company: Filogix Inc.</p> 
 * 
 * @author NBC/PP Implementation Team 
 * @version 1.0 
 * 
 * @version 1.1 August 14, 2008 MCM Team (XS 9.3 Modified the methods to change 
 * 				the rate float down functionality for Components.)
 */ 

public class RateFloatDownSelector {

	/**
     * 
     * Description : Returns PricingRateInventory associated to the best rate
     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * @param component
     * @param commitmetIssueDate
     * @return
     * @throws Exception
     */

    public PricingRateInventory getPriOfBestRate(Component component,Date commitmetIssueDate) throws Exception
    {
        //Bugfix:FXP23287 Added commitmentIssue date     
        PricingRateInventory pri = selectBestRate(component,commitmetIssueDate);
        return pri;
    }
	
	/**
     * 
     * Returns PricingRateInventory associated to the best rate
     * 
     * @param deal
     * @return PricingRateInventory
     * @throws Exception
     */
    public PricingRateInventory getPriOfBestRate(Deal deal) throws Exception
    {
        PricingRateInventory pri = selectBestRate(deal);
        return pri;
    }

    /**
     * 
     * Description : Returns the start date used to calculate the Rate
     * Float Down value
     * 
     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * 
     * @param component
     * @param commitmentIssueDate
     * @return
     */
    protected Date getStartDate(Component component,Date commitmentIssueDate)
    {
        // default is the commitment date
        // when rgpDays > commitmentDays
        Date startDate = commitmentIssueDate;

        DateUtilJavaFmt dateUtil = new DateUtilJavaFmt();
        // number of days between commitment date and today's date
        Date today = new Date();
        int commitmentDays = dateUtil.dateDiff(today, commitmentIssueDate);
        int rgpDays = component.getComponentMortgage().getRateGuaranteePeriod();
        if (rgpDays <= commitmentDays)
        {
            String rgpStartCode = PropertiesCache.getInstance().getProperty(component.getInstitutionProfileId(),"com.basis100.mtgrates.rgpstartcode","B");
            if (rgpStartCode.equalsIgnoreCase("C")){
                // current date
                Calendar cal = Calendar.getInstance();
                java.sql.Date currentDate = new java.sql.Date(cal.getTime().getTime());
                startDate = currentDate;
            }
            else if (rgpStartCode.equalsIgnoreCase("E")){
                // commitment + rate guarantee period
                Calendar cal = Calendar.getInstance();
                cal.setTime(commitmentIssueDate);
                
                cal.add(Calendar.DATE,rgpDays);
                java.sql.Date expiryDate = new java.sql.Date(cal.getTime().getTime());

                startDate = expiryDate;
            }
            else if (rgpStartCode.equalsIgnoreCase("B")){
                // current date - rate guarantee period + 1
                Calendar cal = Calendar.getInstance();
                int rgpFactor = -1 * (rgpDays - 1);
                cal.add(Calendar.DATE,rgpFactor);
                java.sql.Date rateGuaranteeDate = new java.sql.Date(cal.getTime().getTime());

                startDate = rateGuaranteeDate;
            }
        }
        return startDate;
    }
    
    /**
     * 
     * Returns the start date used to calculate the Rate
     * Float Down value
     * 
     * @param deal
     * @return Date
     */
    
    protected Date getStartDate(Deal deal)
    {
        // default is the commitment date
        // when rgpDays > commitmentDays
        Date startDate = deal.getCommitmentIssueDate();

        DateUtilJavaFmt dateUtil = new DateUtilJavaFmt();
        // number of days between commitment date and today's date
        Date today = new Date();
        int commitmentDays = dateUtil.dateDiff(today, deal.getCommitmentIssueDate());
        int rgpDays = deal.getRateGuaranteePeriod();
        if (rgpDays <= commitmentDays)
        {
            String rgpStartCode = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.mtgrates.rgpstartcode","B");
            if (rgpStartCode.equalsIgnoreCase("C")){
                // current date
                Calendar cal = Calendar.getInstance();
                java.sql.Date currentDate = new java.sql.Date(cal.getTime().getTime());
                startDate = currentDate;
            }
            else if (rgpStartCode.equalsIgnoreCase("E")){
                // commitment + rate guarantee period
                Calendar cal = Calendar.getInstance();
                cal.setTime(deal.getCommitmentIssueDate());
                
                cal.add(Calendar.DATE,deal.getRateGuaranteePeriod());
                java.sql.Date expiryDate = new java.sql.Date(cal.getTime().getTime());

                startDate = expiryDate;
            }
            else if (rgpStartCode.equalsIgnoreCase("B")){
                // current date - rate guarantee period + 1
                Calendar cal = Calendar.getInstance();
                int rgpFactor = -1 * (deal.getRateGuaranteePeriod() - 1);
                cal.add(Calendar.DATE,rgpFactor);
                java.sql.Date rateGuaranteeDate = new java.sql.Date(cal.getTime().getTime());

                startDate = rateGuaranteeDate;
            }
        }
        return startDate;
    }

    /**
     * 
     * Description : Returns the PricingRateInventory associated to the deal. This should be
     * overidden by its subclasses to implement the best rate selection 

     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     *  
     * @param component
     * @return
     * @throws Exception
     * Bugfix:FXP23287 Added commitmentIssue date
     */
     
    protected PricingRateInventory selectBestRate(Component component,Date commitmetIssueDate) throws Exception
    {
        return component.getPricingRateInventory();
    }   
    
    /**
     * 
     * Returns the PricingRateInventory associated to the deal. This should be
     * overidden by its subclasses to implement the best rate selection 
     * 
     * @param deal
     * @return PricingRateInventory
     * @throws Exception
     */
    protected PricingRateInventory selectBestRate(Deal deal) throws Exception
    {
        return deal.getPricingRateInventory();
    }    

    /**
     * 
     * Description : Determines whether the value of the RFD rate is valid 

     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     *  
     * @param component
     * @param rfdRate
     * @return
     * @throws Exception
     */
    protected boolean isValidRate(Component component, double rfdRate) throws Exception
    {
        boolean validRate = false;
        // please modify NetRate.doCalc if netrate calculation is changed
        double rfdNetRate = rfdRate - component.getComponentMortgage().getDiscount() + component.getComponentMortgage().getPremium() - component.getComponentMortgage().getBuyDownRate();
        
        if ((rfdRate > 0) && (rfdNetRate > 0))
        {
            validRate = true;
        }
        
        return validRate;
    }    
    
    /**
     * 
     * Determines whether the value of the RFD rate is valid 
     * 
     * @param deal
     * @param rfdRate
     * @return boolean
     * @throws Exception
     */
    protected boolean isValidRate(Deal deal, double rfdRate) throws Exception
    {
        boolean validRate = false;
        // please modify NetRate.doCalc if netrate calculation is changed
        double rfdNetRate = rfdRate - deal.getDiscount() + deal.getPremium() - deal.getBuydownRate();
        
        if ((rfdRate > 0) && (rfdNetRate > 0))
        {
            validRate = true;
        }
        
        return validRate;
    }    
    
    /**
     * 
     * Determines whether the value of the RFD rate is less than the 
     * deal's current posted rate 
     * 
     * @param postedRate
     * @param rfdRate
     * @return boolean
     *          - true - less than posted rate
     *          - false - more than or equal to the posted rate
     * @throws Exception
     */
    protected boolean isRfdRateBest(double postedRate, double rfdRate) throws Exception
    {
        boolean isLess = false;
        
        if (rfdRate < postedRate)
        {
            isLess = true;
        }
        
        return isLess;
    }    


}
