package com.basis100.mtgrates;

import java.util.List;

import com.basis100.resources.SessionResourceKit;

/**
 * Title: IPricingLogicProduct
 * <p>
 * Description: Interface of the PricingLogic: product
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IPricingLogicProduct extends IPricingLogic {

    /**
     * returns product data
     * @param srk - sessionResourceKit
     * @param lenderProfileId - lender ProfileId
     * @param componentTypeId component type id, when it's "-1", 
     *  implementation class will assume it is for umbrella level's request
     * @param dealId 
     * @param dealCopyId
     * @return list of ProductInfo
     */
    public abstract List<ProductInfo> getLenderProducts(
            SessionResourceKit srk, int lenderProfileId,
            int componentTypeId, int dealId, int dealCopyId, boolean isUmbrella);

}