package com.basis100.mtgrates;

import java.util.List;

import com.basis100.resources.SessionResourceKit;

/**
 * Title: IPricingLogicRepaymentType
 * <p>
 * Description: Interface of the PricingLogic: RepaymentType
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IPricingLogicRepaymentType extends IPricingLogic{

    /**
     * returns re-payment type data
     * @param srk
     * @param productId
     * @return list of RepaymentTypeInfo
     */
    public abstract List<RepaymentTypeInfo> getRepaymentTypes(
            SessionResourceKit srk, int productId);

}