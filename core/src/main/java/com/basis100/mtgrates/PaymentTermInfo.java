package com.basis100.mtgrates;

/**
 * Title: PaymentTermInfo
 * <p>
 * Description: data bean implementation
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class PaymentTermInfo implements IPricingLogicDataBean {

    private int mtgProdId;
    private int paymentTermId;

    public int getMtgProdId() {
        return mtgProdId;
    }

    public void setMtgProdId(int mtgprodid) {
        this.mtgProdId = mtgprodid;
    }

    public int getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(int paymenttermid) {
        this.paymentTermId = paymenttermid;
    }
}
