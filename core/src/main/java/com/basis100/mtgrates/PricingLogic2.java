package com.basis100.mtgrates;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;

/**
 * Title: PricingLogic2
 * <p>
 * Description: implementation of each PricingLogic interfaces
 * @author MCM Impl Team.
 * @version 2.0 Jun 25, 2008, XS_2.29 - drastically changed. 
 * @version 2.1 Aug 05, 2008, XS_2.1 - changed criteria for Unbrella
 * @version 2.2 Sep 22, 2008, artf781198 - fixed unmatching SQL criteria.
 */

/*
 * MCM Impl Team. Change Log
 * 
 * XS_2.29 -- Jun 25, 2008
 * 
 * Legacy PricingLogic2 class is totally re-factored. 
 * This class now implements series of Pricing Logic interfaces.
 * Actually, this class doesn�t have to gather all pricing logic implementation.
 * however, I didn't break those implementation into small peaces 
 * because some legacy code executes this class directly, 
 * and keeping the same class name could be more clear for those legacy implementations.
 *     
 */

public class PricingLogic2 implements 
    IPricingLogicLenderProfile, IPricingLogicProduct, 
    IPricingLogicPaymentTerm, IPricingLogicProductRate, IPricingLogicRepaymentType {
    
    private static Log _log = LogFactory.getLog(PricingLogic2.class);

    /* (non-Javadoc)
     * @see com.basis100.mtgrates.IPricingLogicLenderProfile#getLenderProfiles(com.basis100.resources.SessionResourceKit)
     */
    public List<LenderInfo> getLenderProfiles(SessionResourceKit srk) {


        int institutionId = srk.getExpressState().getDealInstitutionId();
        List<String> pickList = PicklistData.getValueList(
                institutionId, Xc.PKL_LENDERPROFILE, "lenderProfileId");
        List<LenderInfo> result = new Vector<LenderInfo>();
        for(String lender : pickList){
            LenderInfo lp = new LenderInfo();
            lp.setLenderProfileId(Integer.parseInt(lender));
            result.add(lp);
        }
        Collections.sort(result);
        return result;
    }

    /* (non-Javadoc)
     * @see com.basis100.mtgrates.IPricingLogicProduct#getLenderProducts(com.basis100.resources.SessionResourceKit, int, java.util.Date, int)
     */
    public List<ProductInfo> getLenderProducts(SessionResourceKit srk,
            int lenderProfileId, int componentTypeId, int dealId, int dealCopyId, boolean isUmbrella) {

    	Date applicationDate = null;
        /***** GR 3.2.2 Firm-up PreApproval change starts *****/
        Date preApprovalDate = null;
        /***** GR 3.2.2 Firm-up PreApproval change ends *****/
        Deal deal = null;
        if (dealId >= 0) {
            try {
                deal = new Deal(srk, null).findByPrimaryKey(new DealPK(dealId,
                        dealCopyId));
                applicationDate = deal.getApplicationDate();
                
                /***** GR 3.2.2 Firm-up PreApproval change starts *****/
                preApprovalDate = getPreApprovalDate(deal, applicationDate, srk);
                /***** GR 3.2.2 Firm-up PreApproval change endd *****/

            } catch (Exception ex) {
                _log.warn(ex);
                _log.warn(StringUtil.stack2string(ex));
            }
        }
        if (applicationDate == null)
        	applicationDate = new Date();

        if (preApprovalDate == null)
        	preApprovalDate = new Date();

        List<ProductInfo> result = new Vector<ProductInfo>();
        try{
            
            Date conditionDate = getConditionDate(srk, preApprovalDate);

            String sql = "SELECT mtg.mtgProdId, mtg.repaymenttypeid, p.ratecode "
                + " FROM PRICINGPROFILE P, PRICINGRATEINVENTORY I, mtgprod mtg, mtgprodlenderinvestorassoc pass "
                + " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID "
                + " AND mtg.MtgProdId = pass.MtgProdId "
                + " AND mtg.pricingprofileid=i.pricingprofileid "
                + " AND mtg.pricingprofileid=p.pricingprofileid "
                + " AND (I.INDEXEXPIRYDATE >= "
                + sqlStringFromDate((java.util.Date) conditionDate)
                + " OR I.INDEXEXPIRYDATE IS NULL) AND I.INDEXEFFECTIVEDATE <= sysdate"
                + " AND (P.RATEDISABLEDDATE >= "
                + sqlStringFromDate((java.util.Date) applicationDate)
                + " OR P.RATEDISABLEDDATE IS NULL)"
                + " AND pass.lenderProfileId = "
                + lenderProfileId
                + " AND mtg.institutionProfileid = pass.institutionProfileid "
                + " AND mtg.institutionProfileid = i.institutionProfileid "
                + " AND mtg.institutionProfileid = p.institutionProfileid ";

            _log.debug("isUmbrella = " + isUmbrella);
            if (isUmbrella) {
				/***************MCM Impl team changes starts - XS_2.1 *******************/
                sql += "   AND (mtg.componenteligibleflag = 'Y' ";
                sql += "        OR ( mtg.componenteligibleflag = 'N' and mtg.producttypeid > 0 ))";
		       /***************MCM Impl team changes ends -  XS_2.1 *********************/
            } else {
                _log.debug("componentTypeId = " + componentTypeId);
                if(componentTypeId > 0) {
                    sql += "   AND mtg.componenttypeid != 0 ";
                    sql += "   AND mtg.componenttypeid  = " + componentTypeId;
                }else{
                    // get all component for add new function
                    sql += "   AND mtg.componenttypeid > 0 ";
                }
            }
            //sql += " group by mtg.MtgProdName, mtg.MtgProdId, pass.lenderProfileId, p.ratecode ";
            sql += " group by mtg.mtgProdId, mtg.repaymenttypeid, p.ratecode ";
            sql += " order by p.ratecode desc";

            _log.debug(">>> PricingLogic2.getLenderProducts sql = " + sql);

            JdbcExecutor jExec = srk.getJdbcExecutor();

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                ProductInfo product = new ProductInfo();
                product.setMtgProdId(jExec.getInt(key, 1));
                product.setRepaymenttypeid(jExec.getInt(key, 2));
                product.setRatecode(jExec.getString(key, 3));
                result.add(product);
            }
            jExec.closeData(key);

        }catch(Exception e){
            result = null;
            _log.error("Exception @PricingLogic2.getLenderProducts");
            _log.error(e.getMessage(), e);
            _log.error(StringUtil.stack2string(e));
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.basis100.mtgrates.IPricingLogicPaymentTerm#getProductPaymentTerms(com.basis100.resources.SessionResourceKit, int)
     */
    public List<PaymentTermInfo> getProductPaymentTerms(
            SessionResourceKit srk, int componentTypeId, boolean isUmbrella) {

        List<PaymentTermInfo> result = new Vector<PaymentTermInfo>();
        try {

            StringBuffer sql = new StringBuffer();
            sql.append("SELECT p.mtgprodid, t.paymenttermid ");
            sql.append("  FROM paymentterm t, mtgprod p");
            sql.append(" WHERE p.paymenttermid = t.paymenttermid ");
            sql.append("   AND p.institutionprofileid = t.institutionprofileid ");
            
            _log.debug("isUmbrella = " + isUmbrella);
            if (isUmbrella) {
			/***************MCM Impl team changes starts - XS_2.1 *******************/
                sql.append("   AND (p.componenteligibleflag = 'Y' ");
                sql.append("         OR ( p.componenteligibleflag = 'N' and p.producttypeid > 0 ))");
			/***************MCM Impl team changes starts - XS_2.1 *******************/                
            } else {
                sql.append("   AND p.componenttypeid != 0 ");
                sql.append("   AND p.componenttypeid  = ").append(componentTypeId);
            }
            JdbcExecutor jExec = srk.getJdbcExecutor();
            
            _log.debug(">>> PricingLogic2.getProductPaymentTerms sql = " + sql);


            int key = jExec.execute(sql.toString());

            for (; jExec.next(key);) {

                PaymentTermInfo pt = new PaymentTermInfo();
                pt.setMtgProdId(jExec.getInt(key, 1));
                pt.setPaymentTermId(jExec.getInt(key, 2));

                result.add(pt);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            result = null;
            _log.error("Exception @PricingLogic.setProductPaymentTerms");
            _log.error(e.getMessage(), e);
            _log.error(StringUtil.stack2string(e));
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.basis100.mtgrates.IPricingLogicRepaymentType#getRepaymentTypes(com.basis100.resources.SessionResourceKit, int)
     */
    public List<RepaymentTypeInfo> getRepaymentTypes(
            SessionResourceKit srk, int productId) {

        List<RepaymentTypeInfo> result = new Vector<RepaymentTypeInfo>();
        try {

            StringBuffer sql = new StringBuffer();

            sql.append("SELECT r.repaymenttypeid, nvl(m.mtgprodid,-1) ");
            sql.append("  FROM repaymenttype r ");
            sql.append("       LEFT OUTER JOIN");
            sql.append("       mtgprod m ");
            sql.append("       ON r.repaymenttypeid = m.repaymenttypeid ");
            sql.append("   AND m.mtgprodid = ").append(productId);
            sql.append(" ORDER BY r.repaymenttypeid ");
            
            JdbcExecutor jExec = srk.getJdbcExecutor();
            _log.debug(">>> PricingLogic2.getRepaymentTypes sql = " + sql);
            int key = jExec.execute(sql.toString());
            for (; jExec.next(key);) {

                RepaymentTypeInfo rt = new RepaymentTypeInfo();
                rt.setRepaymentTypeId(jExec.getInt(key, 1));
                rt.setMtgProdId(jExec.getInt(key, 2));

                result.add(rt);
            }

            jExec.closeData(key);
        } catch (Exception e) {
            result = null;
            _log.error("Exception @PricingLogic.getRepaymentTypes");
            _log.error(e.getMessage(), e);
            _log.error(StringUtil.stack2string(e));
        }
        return result;
    }

    /**
     * get list of RateInfo
     * @param srk
     * @param productId
     * @param applicationDate
     * @param rateDisabledDate
     * @return list of RateInfo, empty List when an exception happens
     */
    public Vector<RateInfo> getLenderProductRates(SessionResourceKit srk,
            int productId, Date applicationDate, String rateDisabledDate) {

        return getRatesForProductAndAppDate(productId, applicationDate, srk, null);
    }

    /*
     * (non-Javadoc)
     * @see com.basis100.mtgrates.IPricingLogicProductRate#getLenderProductRates(com.basis100.resources.SessionResourceKit, int, int, int)
     */
    public List<RateInfo> getLenderProductRates(SessionResourceKit srk,
            int productId, int dealId, int dealCopyId) {
        Deal deal = null;
        Date appDate = null;
        if (dealId >= 0) {
            try {
                deal = new Deal(srk, null).findByPrimaryKey(new DealPK(dealId,
                        dealCopyId));
                appDate = deal.getApplicationDate();
            } catch (Exception ex) {
                _log.error(ex);
                _log.error(StringUtil.stack2string(ex));
            }
        }
        if (appDate == null)
            appDate = new Date();

        return getRatesForProductAndAppDate(productId, appDate, srk,
                deal);
    }

    private Date getConditionDate(SessionResourceKit srk, Date applicationDate)
    throws Exception {
        JdbcExecutor jExec = srk.getJdbcExecutor();

        String sql = "SELECT REBACKDATELIMIT FROM INSTITUTIONPROFILE";

        int key = jExec.execute(sql);

        int reBackDateLimit = -999;

        if (jExec.next(key))
            reBackDateLimit = jExec.getInt(key, 1);

        jExec.closeData(key);

        if (reBackDateLimit == -999)
            throw new Exception("Error: No Institution");

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(applicationDate);
        calendar.add(Calendar.DATE, reBackDateLimit * (-1));
        Date conditionDate = calendar.getTime();

        return conditionDate;
    }

    private Vector<RateInfo> getRatesForProductAndAppDate(int productId,
            Date applicationDate, SessionResourceKit srk, Deal deal) {


        Vector<RateInfo> pricingRates = new Vector<RateInfo>();

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            /***** GR 3.2.2 Firm-up PreApproval change start *****/
            Date preApprovalDate = getPreApprovalDate(deal, applicationDate, srk);

            Date conditionDate = getConditionDate(srk, preApprovalDate);
            /***** GR 3.2.2 Firm-up PreApproval change end *****/
            int pricingStatusIdSearchCriteria = getProductStatusId(srk, productId);
            
            String dealCriteria = deal == null ? "" : " or I.PRICINGRATEINVENTORYID = " + deal.getPricingProfileId();
            
            String sql = "SELECT I.PRICINGRATEINVENTORYID, P.RATECODE, P.RATECODEDESCRIPTION,"
                + " I.INDEXEFFECTIVEDATE, I.INDEXEXPIRYDATE, I.INTERNALRATEPERCENTAGE,"
                + " I.MAXIMUMDISCOUNTALLOWED, I.INTERNALRATEPERCENTAGE - I.MAXIMUMDISCOUNTALLOWED"
                + " FROM  PRICINGPROFILE P, PRICINGRATEINVENTORY I"
                + " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID "
                + " AND P.INSTITUTIONPROFILEID = I.INSTITUTIONPROFILEID "
                + " AND P.PRICINGPROFILEID IN(SELECT PRICINGPROFILEID FROM MTGPROD WHERE MTGPRODID = "
                + productId
                + "  ) AND (I.INDEXEXPIRYDATE >= "
                + sqlStringFromDate((java.util.Date) conditionDate)
                + "  OR I.INDEXEXPIRYDATE IS NULL " + dealCriteria  + " ) "
                + " AND I.INDEXEFFECTIVEDATE <= sysdate"
                + " AND P.PRICINGSTATUSID = "
                + pricingStatusIdSearchCriteria
                + " AND (P.RATEDISABLEDDATE >= "
                + sqlStringFromDate((java.util.Date) applicationDate)
                + " OR P.RATEDISABLEDDATE IS NULL)"
                + " ORDER BY I.INDEXEFFECTIVEDATE DESC";

            _log.debug(">>> PricingLogic2.getRatesForProductAndAppDate sql = " + sql);

            int key = jExec.execute(sql);

            for (; jExec.next(key);) {
                int index = 0;

                RateInfo curRate = new RateInfo();

                curRate.setRateId(jExec.getInt(key, ++index));
                curRate.setRateCode(jExec.getString(key, ++index));
                curRate.setRateDescription(jExec.getString(key, ++index));
                curRate.setEffectiveDate(jExec.getDate(key, ++index));
                curRate.setExpiryDate(jExec.getDate(key, ++index));
                curRate.setPostedRate(jExec.getDouble(key, ++index));
                curRate.setMaxDiscount(jExec.getDouble(key, ++index));
                curRate.setBestRate(jExec.getDouble(key, ++index));

                pricingRates.add(curRate);
            }

            jExec.closeData(key);


        } catch (Exception e) {
            pricingRates = null;
            String msg = "Exception @PricingLogic.getValidRates";
            _log.error(msg);
            _log.error(e);
            _log.error(StringUtil.stack2string(e));
        }
        return pricingRates;
    }


    private int getProductStatusId(SessionResourceKit srk, int productId)
    throws Exception {
        JdbcExecutor jExec = srk.getJdbcExecutor();

        int prodStatusId = 0; // default is active.

        try {
            /***************MCM Impl team changes starts - artf781198*******************/
            /* Sep 22, 2008: artf781198: Before, productId(=MTGPROD.MTGPRODID) was applied to P.PRICINGPROFILEID in this SQL.
             * PRICINGPROFILEID and MTGPRODID often set the same value before, so no problem didn't show up unexpectedly.
             * however this is wrong. changing the sql to get PRICINGPROFILEID from corresponding MTGPROD table.
             */
            /* comment out by artf781198
            String sql = "SELECT P.PRICINGSTATUSID FROM  PRICINGPROFILE P, PRICINGRATEINVENTORY I "
                + "WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID "
                + " AND p.INSTITUTIONPROFILEID = I.INSTITUTIONPROFILEID " + // FXP19939
                " AND P.PRICINGPROFILEID = " + productId;
            */
            StringBuffer sql = new StringBuffer();
            sql.append("SELECT P.PRICINGSTATUSID ");
            sql.append("  FROM PRICINGPROFILE P "); 
            sql.append("  JOIN MTGPROD M ");
            sql.append("    ON P.INSTITUTIONPROFILEID = M.INSTITUTIONPROFILEID ");
            sql.append("   AND P.PRICINGPROFILEID     = M.PRICINGPROFILEID ");
            sql.append(" WHERE M.MTGPRODID            = ").append(productId);
            
            /***************MCM Impl team changes ends - artf781198*********************/
            
            _log.debug("> PricingLogic2.getProductStatusId sql = " + sql.toString());

            int key = jExec.execute(sql.toString());
            while (jExec.next(key)) {
                prodStatusId = jExec.getInt(key, 1); // should be only one
                // record
                break;
            }

            jExec.closeData(key);
        } catch (Exception e) {
            _log.error(e.getMessage());
            _log.error(StringUtil.stack2string(e));
            throw e;
        }

        return prodStatusId;
    }


    private String sqlStringFromDate(java.util.Date d) throws Exception {
        String dateString = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            dateString = " to_date('" + df.format(d) + "','yyyy-mm-dd') ";
        } catch (Exception e) {
            throw new Exception(
                    "Exception @PricingLogic.sqlStringFromDate: constructing sql string from date."
                    + e.getMessage());
        }

        return dateString;
    }

    /**
      * GR3.2.2 Firm Up PreApproval Functionality
      * 
      * @param applicationDate
      * @return
      * @throws Exception
      */
     public Date getPreApprovalDate(Deal deal, Date applicationDate, SessionResourceKit srk) throws Exception {

         boolean mosproperty = false;
         
         /***** FXP24050 fix start ******/
         if (deal == null) {
             int dealId = srk.getExpressState().getDealId();
             int copyId = srk.getExpressState().getDealCopyId();
             deal = new Deal(srk, null).findByPrimaryKey(new DealPK(dealId, copyId));
         }                 
         /***** FXP24050 fix end   ******/
         
         String property = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                 "deal.allow.preapproval.rate.on.firmup.preapproval", "N");
         if (property.equalsIgnoreCase("Y"))
             mosproperty = true;

         Date preApprovalDate = applicationDate;
         if (deal != null && deal.getPreApproval()!=null
         		&& deal.getPreApproval().equalsIgnoreCase("Y")
                 && deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL) {
             if (mosproperty) {
                 deal = deal.findLastPreApproval();
                 preApprovalDate = deal.getApplicationDate();
             }
         }
         return preApprovalDate;
     }

}
