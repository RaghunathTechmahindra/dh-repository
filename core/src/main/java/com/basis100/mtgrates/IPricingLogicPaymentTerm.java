package com.basis100.mtgrates;

import java.util.List;

import com.basis100.resources.SessionResourceKit;

/**
 * Title: IPricingLogicPaymentTerm
 * <p>
 * Description: Interface of the PricingLogic: PaymentTerm
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IPricingLogicPaymentTerm extends IPricingLogic {

    /**
     * return Payment Term data 
     * @param srk sessionResourceKit
     * @param componentTypeId : component type id, when it's "-1", 
     *  implementation class will assume it is for umbrella level's request
     * @return list of PaymentTermInfo
     */
    public abstract List<PaymentTermInfo> getProductPaymentTerms(
            SessionResourceKit srk, int componentTypeId, boolean isUmbrella);

}