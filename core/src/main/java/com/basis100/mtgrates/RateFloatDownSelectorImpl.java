package com.basis100.mtgrates;

import MosSystem.*;

public class RateFloatDownSelectorImpl {

    /**
     * 
     * Returns the rate float down selector depending on the
     * rateFloatDownType
     * 
     * @param rfdtype
     * @return RateFloatDownSelector
     */
    
    public static RateFloatDownSelector getRfdSelector(int rfdtype)
    {
        switch(rfdtype)
        {
            case Mc.RATE_FLOAT_DOWN_TYPE_UCURVE:
                return new UCurveSelector();
            case Mc.RATE_FLOAT_DOWN_TYPE_VCURVE:
                return new VCurveSelector();
        }
        // default
        return new DefaultRateFloatDownSelector();
        
    }
}
