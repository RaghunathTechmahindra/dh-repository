package com.basis100.mtgrates;

import java.util.List;

import com.basis100.resources.SessionResourceKit;

/**
 * Title: IPricingLogicProductRate
 * <p>
 * Description: Interface of the PricingLogic: rate
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IPricingLogicProductRate extends IPricingLogic {

    /**
     * returns Posted Interest Rate data
     * @param srk
     * @param productId
     * @param dealId
     * @param dealCopyId
     * @return list of RateInfo
     */
    public List<RateInfo> getLenderProductRates(SessionResourceKit srk,
            int productId, int dealId, int dealCopyId);
}