package com.basis100.mtgrates;
/**
 * Title: RepaymentTypeInfo
 * <p>
 * Description: data bean implimentation
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class RepaymentTypeInfo implements IPricingLogicDataBean {

    private int mtgProdId;
    private int repaymentTypeId;

    public int getMtgProdId() {
        return mtgProdId;
    }

    public void setMtgProdId(int mtgprodid) {
        this.mtgProdId = mtgprodid;
    }

    public int getRepaymentTypeId() {
        return repaymentTypeId;
    }

    public void setRepaymentTypeId(int repaymentTypeId) {
        this.repaymentTypeId = repaymentTypeId;
    }
}
