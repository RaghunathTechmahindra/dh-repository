package com.basis100.mtgrates;

import com.basis100.deal.entity.*;

import java.util.*;

/** 
 * <p>Title: UCurveSelector</p> 
 * 
 * <p>Description: Selection of the best rate according to the U-Curve
 *                  algorithm which selects the lowest rate between
 *                  the start date and the end date (inclusive).</p> 
 * 
 * <p>Copyright: Filogix Inc. (c) 2006</p> 
 * 
 * <p>Company: Filogix Inc.</p> 
 * 
 * @author NBC/PP Implementation Team 
 * @version 1.0 
 * 
 * @version 1.1 August 14, 2008 MCM Team (XS 9.3 Modified the methods to change 
 * 				the rate float down functionality for Components.)
 * @vesrion FXP2287 1.2 Added commitmentIssueDate for selectBestRate method
 * 
 */ 
public class UCurveSelector extends RateFloatDownSelector{

    /**
     * 
     * Returns the PricingRateInventory of the best rate
     * within the start date and the current date (inclusive).
     * 
     * @param deal
     * @return PricingRateInventory
     * @throws Exception
     */
    
    protected PricingRateInventory selectBestRate(Deal deal) throws Exception
    {

        PricingProfile pp = deal.getActualPricingProfile();
        // disabled product and disabled date after today's date 
        Date startDate = getStartDate(deal);
        Date disabledDate = pp.getRateDisabledDate();

        // throw exception if product is disabled but has no disabled date
        if ((pp.getPricingStatusId() == 1 && disabledDate == null) || (pp.getPricingStatusId() == 0 && disabledDate != null))
        {
            Exception ex = new Exception("Exception: UCurveSelector.selectBestRate(Deal deal)- disabled product configuration error");
            throw ex;
        }
        
        if ((pp.getPricingStatusId() == 1) && startDate.after(disabledDate))
        {
            return super.selectBestRate(deal);
        }
        else 
        if ((pp.getPricingStatusId() == 1) && (pp.getRateDisabledDate().before(new Date()))) 
        {
            return selectBestRateForDisabledProduct(deal, disabledDate);
        }
        else // active product
        {
            return deal.getPriByMinRate(startDate);
        }
    }
    /**
     * 
     * Description : Returns the PricingRateInventory of the best rate
     * within the start date and the current date (inclusive).
     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * @param component
     * @param commitmentIssueDate
     * @return
     * @throws Exception
     */
    
    protected PricingRateInventory selectBestRate(Component component,Date commitmentIssueDate) throws Exception
    {

        PricingProfile pp = component.getActualPricingProfile();
        // disabled product and disabled date after today's date 
        Date startDate = getStartDate(component,commitmentIssueDate);
        Date disabledDate = pp.getRateDisabledDate();

        // throw exception if product is disabled but has no disabled date
        if ((pp.getPricingStatusId() == 1 && disabledDate == null) || (pp.getPricingStatusId() == 0 && disabledDate != null))
        {
            Exception ex = new Exception("Exception: UCurveSelector.selectBestRate(Deal deal)- disabled product configuration error");
            throw ex;
        }
        
        if ((pp.getPricingStatusId() == 1) && startDate.after(disabledDate))
        {
            //FXP2287 1.2 Added commitmentIssueDate 
            return super.selectBestRate(component,commitmentIssueDate);
        }
        else 
        if ((pp.getPricingStatusId() == 1) && (pp.getRateDisabledDate().before(new Date()))) 
        {
            return selectBestRateForDisabledProduct(component, disabledDate);
        }
        else // active product
        {
            return component.getPriByMinRate(startDate);
        }
    }

    /**
     * 
     * Description : Returns the PricingRateInventory associated to the best rate for a disabled product. 
     * @version 1.0 August 14, 2008 XS_9.3 Initial Version 
     * @param component
     * @param disabledDate
     * @return
     * @throws Exception
     */
    protected PricingRateInventory selectBestRateForDisabledProduct(Component component, Date disabledDate) throws Exception
    {        
        
        return component.getPriByMinRate(getStartDate(component,disabledDate), disabledDate);
    }    
    
    /**
     * 
     * Returns the PricingRateInventory associated to the best rate for a disabled product. 
     * 
     * @param deal
     * @param disabledDate
     * @return PricingRateInventory
     * @throws Exception
     */
    protected PricingRateInventory selectBestRateForDisabledProduct(Deal deal, Date disabledDate) throws Exception
    {
        
        
        return deal.getPriByMinRate(getStartDate(deal), disabledDate);
    }    


}
