package com.basis100.mtgrates;

import java.util.Date;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PricingRateInventory;

/** 
 * <p>Title: DefaultRateFloatDownSelector</p> 
 * 
 * <p>Description: Selection of the best rate according to the default
 *                  algorithm which returns the rate currently associated 
 *                  with the deal.</p> 
 * 
 * <p>Copyright: Filogix Inc. (c) 2006</p> 
 * 
 * <p>Company: Filogix Inc.</p> 
 * 
 * @author NBC/PP Implementation Team 
 * @version 1.0 
 * @version 1.1 August 14, 2008 MCM Team (XS 9.3 Modified the methods to change 
 * 				the rate float down functionality for Components.)
 */ 

public class DefaultRateFloatDownSelector extends RateFloatDownSelector {

    /**
     * 
     * Returns the PricingRateInventory associated to the deal.
     * 
     * @param deal
     * @return PricingRateInventory
     * @throws Exception
     */

    protected PricingRateInventory selectBestRate(Deal deal) throws Exception
    {
        return super.selectBestRate(deal);
    } 
    
    /**
     * 
     * Description : Returns the PricingRateInventory associated to the component.
     * 
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * 
     * @param component
     * @return PricingRateInventory
     * @throws Exception
     */
    protected PricingRateInventory selectBestRate(Component component,Date commitmetIssueDate) throws Exception
    {
        return super.selectBestRate(component,commitmetIssueDate);
    }  
    
}
