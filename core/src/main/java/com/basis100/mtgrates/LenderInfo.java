package com.basis100.mtgrates;

/**
 * Title: LenderInfo
 * <p>
 * Description: data bean implementation
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class LenderInfo implements Comparable<LenderInfo>, IPricingLogicDataBean {

    private int lenderProfileId;

    public int getLenderProfileId() {
        return lenderProfileId;
    }

    public void setLenderProfileId(int lenderProfileId) {
        this.lenderProfileId = lenderProfileId;
    }

    public int compareTo(LenderInfo other) {
        return this.lenderProfileId - other.lenderProfileId;
    }

}
