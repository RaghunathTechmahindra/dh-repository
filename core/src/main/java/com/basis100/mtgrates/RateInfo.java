package com.basis100.mtgrates;

import java.io.Serializable;
import java.util.Date;


public class RateInfo implements Serializable, IPricingLogicDataBean
{
    private int     rateId =0;
    private String  rateCode = null;
    private String  rateDescription = null;
    private Date    effectiveDate = null;
    private Date    expiryDate = null;
    private double  postedRate = 0.0;
    private double  maxDiscount = 0.0;
    private double  bestRate = 0.0;

    //--CR_Ticket#478_AutoDiscount_process--05Aug2004--start--//
    private double  premium = 0.0;
    //--CR_Ticket#478_AutoDiscount_process--end--//

    //--Ticket#1773--18July2005--start--//
    private double primeBaseAdj = 0.0;
    private double primeIndexRate = 0.0;
    private int primeIndexId = 0;
    //--Ticket#1773--18July2005--end--//

    //--Ticket#1736--18July2005--end--//
    private double teaserDiscount = 0.0;
    private int teaserTerm = 0;
    //--Ticket#1736--18July2005--end--//

    // added for ML - Oct 26, 2007 Midori
    private int institutionProfileId;

    public RateInfo()
    {
    }

    public void setRateId(int rateId)
    {
        this.rateId = rateId;
    }

    public int getRateId()
    {
        return this.rateId;
    }

    public void setRateCode(String rateCode)
    {
        this.rateCode= rateCode;
    }

    public String getRateCode()
    {
        return this.rateCode;
    }

    public void setRateDescription(String rateDescription)
    {
        this.rateDescription= rateDescription;
    }

    public String getRateDescription()
    {
        return this.rateDescription;
    }

    public void setEffectiveDate(Date effectiveDate)
    {
        this.effectiveDate= effectiveDate;
    }

    public Date getEffectiveDate()
    {
        return this.effectiveDate;
    }

    public void setExpiryDate(Date expiryDate)
    {
        this.expiryDate= expiryDate;
    }

    public Date getExpiryDate()
    {
        return this.expiryDate;
    }

    public void setPostedRate(double postedRate )
    {
        this.postedRate = postedRate;
    }

    public double getPostedRate()
    {
        return this.postedRate;
    }

    public void setMaxDiscount(double maxDiscount )
    {
        this.maxDiscount = maxDiscount;
    }

    public double getMaxDiscount()
    {
        return this.maxDiscount;
    }

    public void setBestRate(double bestRate )
    {
        this.bestRate = bestRate;
    }

    public double getBestRate()
    {
        return this.bestRate;
    }

    //--CR_Ticket#478_AutoDiscount_process--05Aug2004--start--//
    public void setPremium(double premium )
    {
        this.premium = premium;
    }

    public double getPremium()
    {
        return this.premium;
    }
    //--CR_Ticket#478_AutoDiscount_process--end--//

    //--Ticket#1773--18July2005--start--//
    // All new deal fields: PRIMEBASEADJ, PRIMEINDEXID, PRIMEINDEXRATE
    // and all ones which weren't done before (TeaserDiscount, TeaserTerm) should be
    // added.
    public void setPrimeBaseAdj(double primeBaseAdj )
    {
        this.primeBaseAdj = primeBaseAdj;
    }

    public double getPrimeBaseAdj()
    {
        return this.primeBaseAdj;
    }

    public void setPrimeIndexRate(double primeIndexRate )
    {
        this.primeIndexRate = primeIndexRate;
    }

    public double getPrimeIndexRate()
    {
        return this.primeIndexRate;
    }

    public void setPrimeIndexId(int primeIndexId)
    {
        this.primeIndexId = primeIndexId;
    }

    public int getPrimeIndexId()
    {
        return this.primeIndexId;
    }
    //--Ticket#1773--18July2005--end--//

    //--Ticket#1736--18July2005--end--//
    public void setTeaserDiscount(double teaserDiscount )
    {
        this.teaserDiscount = teaserDiscount;
    }

    public double getTeaserDiscount()
    {
        return this.teaserDiscount;
    }

    public void setTeaserTerm(int teaserTerm)
    {
        this.teaserTerm = teaserTerm;
    }

    public int getTeaserTerm()
    {
        return this.teaserTerm;
    }
    //--Ticket#1736--18July2005--end--//

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }
}
