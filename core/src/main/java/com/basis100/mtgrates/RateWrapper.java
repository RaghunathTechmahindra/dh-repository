package com.basis100.mtgrates;

import java.util.*;
import java.text.*;


public class RateWrapper {
	     private String productName;
	     private int productId;
       private int lenderProfileId;
       private String rateCode;
       private int rateInventoryId;

   // "getters" methods.
    public String getProductName(){
        return productName;
    }
    public int getProductId(){
        return productId;
    }
    public int getLenderProfileId(){
        return lenderProfileId;
    }
    public String getRateCode(){
        return rateCode;
    }
    public int getRateInventoryId(){
        return rateInventoryId;
    }

   // "setters" methods.
    public void setProductName(String aProductName){
            productName = aProductName;
    }
    public void setProductId(int aProductId){
            productId = aProductId;
    }
    public void setLenderProfileId(int aLenderProfileId){
            lenderProfileId = aLenderProfileId;
    }
    public void setRateCode(String aRateCode){
            rateCode = aRateCode;
    }
    public void setRateInventoryId(int aRateInventoryId){
            rateInventoryId = aRateInventoryId;
    }

}