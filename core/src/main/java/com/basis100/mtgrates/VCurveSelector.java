package com.basis100.mtgrates;

import java.util.Date;

import com.basis100.deal.entity.*;

/** 
 * <p>Title: VCurveSelector</p> 
 * 
 * <p>Description: Selection of the best rate according to the V-Curve
 *                  algorithm which selects the lowest rate of either the
 *                  start date or the end date.</p> 
 * 
 * <p>Copyright: Filogix Inc. (c) 2006</p> 
 * 
 * <p>Company: Filogix Inc.</p> 
 * 
 * @author NBC/PP Implementation Team 
 * @version 1.0 
 * @version 1.1 August 14, 2008 MCM Team (XS 9.3 Modified the methods to change 
 * 				the rate float down functionality for Components.)
 * @vesrion FXP2287 1.2 Added commitmentIssueDate for selectBestRate method
 */ 
public class VCurveSelector extends RateFloatDownSelector{

    /**
     * 
     * Returns the PricingRateInventory of the best rate.
     * Rate of the start date OR rate of the current date/disabled date.
     * 
     * @param deal
     * @return PricingRateInventory
     * @throws Exception
     */
    
    protected PricingRateInventory selectBestRate(Deal deal) throws Exception
    {

        PricingProfile pp = deal.getActualPricingProfile();
        Date startDate = getStartDate(deal);
        Date disabledDate = pp.getRateDisabledDate();
      
        // throw exception if product is disabled but has no disabled date
        if ((pp.getPricingStatusId() == 1 && disabledDate == null) || (pp.getPricingStatusId() == 0 && disabledDate != null))
        {
            Exception ex = new Exception("Exception: VCurveSelector.selectBestRate(Deal deal)- disabled product configuration error");
            throw ex;
        }
        
        if ((pp.getPricingStatusId() == 1) && startDate.after(disabledDate))
        {
            return super.selectBestRate(deal);
        }
        // disabled product and disabled date after today's date 
        else 
        if ((pp.getPricingStatusId() == 1) && (pp.getRateDisabledDate().before(new Date()))) 
        {
            return selectBestRateForDisabledProduct(deal, disabledDate);
        }
        else // active product
        {
            return selectBestRateForActiveProduct(deal);
        }
    }

    /**
     * 
     * Description: Returns the PricingRateInventory of the best rate.
     * Rate of the start date OR rate of the current date/disabled date.
     * 
     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * @param component
     * @param commitmentIssueDate
     * @return
     * @throws Exception
     * 
     **/
 
    protected PricingRateInventory selectBestRate(Component component,Date commitmentIssueDate) throws Exception
    {

        PricingProfile pp = component.getActualPricingProfile();
        Date startDate = getStartDate(component,commitmentIssueDate);
        Date disabledDate = pp.getRateDisabledDate();
      
        // throw exception if product is disabled but has no disabled date
        if ((pp.getPricingStatusId() == 1 && disabledDate == null) || (pp.getPricingStatusId() == 0 && disabledDate != null))
        {
            Exception ex = new Exception("Exception: VCurveSelector.selectBestRate(Component component,Date commitmentIssueDate)- disabled product configuration error");
            throw ex;
        }
        
        if ((pp.getPricingStatusId() == 1) && startDate.after(disabledDate))
        {
            //FXP2287 1.2 Added commitmentIssueDate 
            return super.selectBestRate(component,commitmentIssueDate);
        }
        // disabled product and disabled date after today's date 
        else 
        if ((pp.getPricingStatusId() == 1) && (pp.getRateDisabledDate().before(new Date()))) 
        {
            return selectBestRateForDisabledProduct(component, disabledDate);
        }
        else // active product
        {
            return selectBestRateForActiveProduct(component,commitmentIssueDate);
        }
    }
    
    /**
     * 
     * Returns the PricingRateInventory of the best rate for active product.
     * Rate of the start date OR rate of the current date.
     * 
     * @param deal
     * @return PricingRateInventory
     * @throws Exception
     */
    
    protected PricingRateInventory selectBestRateForActiveProduct(Deal deal) throws Exception
    {
        double startRate = 0;
        PricingRateInventory priStart = null;
        // get PricingRateInventory of start date
        try {
            priStart = deal.getPriByDate(getStartDate(deal));
            startRate = priStart.getInternalRatePercentage();
        }
        catch(Exception e1)
        {
            startRate = 100;
        }
        
        // assumes that there should be a rate for today's date, so exception
        // will be thrown otherwise
        PricingRateInventory priEnd = deal.getPriByDate(new Date());
        double endRate = priEnd.getInternalRatePercentage();

        if ((startRate < endRate) && priStart != null)
        {
            return priStart;
        }
        else
        {
            return priEnd;            
        }
    }
    
    /**
     * 
     * Description: Returns the PricingRateInventory of the best rate for active product.
     * Rate of the start date OR rate of the current date.
     * 
     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * 
     * @param component
     * @param commitmentIssueDate
     * @return
     * @throws Exception
     */
  
    protected PricingRateInventory selectBestRateForActiveProduct(Component component,Date commitmentIssueDate) throws Exception
    {
        double startRate = 0;
        PricingRateInventory priStart = null;
        // get PricingRateInventory of start date
        try {
            priStart = component.getPriByDate(getStartDate(component,commitmentIssueDate));
            startRate = priStart.getInternalRatePercentage();
        }
        catch(Exception e1)
        {
            startRate = 100;
        }
        
        // assumes that there should be a rate for today's date, so exception
        // will be thrown otherwise
        PricingRateInventory priEnd = component.getPriByDate(new Date());
        double endRate = priEnd.getInternalRatePercentage();

        if ((startRate < endRate) && priStart != null)
        {
            return priStart;
        }
        else
        {
            return priEnd;            
        }
    }

    /**
     * 
     * Returns the PricingRateInventory of the best rate for the disabled product.
     * Rate of the start date OR rate of the disabled date.
     * 
     * @param deal
     * @param disabledDate
     * @return PricingRateInventory
     * @throws Exception
     */
    protected PricingRateInventory selectBestRateForDisabledProduct(Deal deal, Date disabledDate) throws Exception
    {
        double startRate = 0;
        PricingRateInventory priStart = null;
        PricingRateInventory priEnd = null;
        // get PricingRateInventory of start date
        try {
            priStart = deal.getPriByDate(getStartDate(deal));
            startRate = priStart.getInternalRatePercentage();
        }
        catch(Exception e1)
        {
            startRate = 100;
        }
        
        double endRate = 0;
        // end date will be the product's disabled date
        try {
            priEnd = deal.getPriByDate(disabledDate);
            endRate = priEnd.getInternalRatePercentage();
        }
        catch(Exception e1)
        {
            endRate = 100;
        }

        if ((startRate == 100) && (endRate == 100))
        {
            Exception ex = new Exception("Exception: VCurveSelector.selectBestRateForActiveProduct(Deal deal)- rates for start date and end date do not exist");
            throw ex;
        }
        
        if ((startRate < endRate) && priStart != null)
        {
            return priStart;
        }
        else
        {
            return priEnd;            
        }
    } 
    /**
     * 
     * Description : Returns the PricingRateInventory of the best rate for the disabled product.
     * Rate of the start date OR rate of the disabled date.
     * 
     * @version 1.0 August 14, 2008 XS_9.3 Initial Version
     * @param component
     * @param disabledDate
     * @return
     * @throws Exception
     **/

    protected PricingRateInventory selectBestRateForDisabledProduct(Component component, Date disabledDate) throws Exception
    {
        double startRate = 0;
        PricingRateInventory priStart = null;
        PricingRateInventory priEnd = null;
        // get PricingRateInventory of start date
        try {
            priStart = component.getPriByDate(getStartDate(component,disabledDate));
            startRate = priStart.getInternalRatePercentage();
        }
        catch(Exception e1)
        {
            startRate = 100;
        }
        
        double endRate = 0;
        // end date will be the product's disabled date
        try {
            priEnd = component.getPriByDate(disabledDate);
            endRate = priEnd.getInternalRatePercentage();
        }
        catch(Exception e1)
        {
            endRate = 100;
        }

        if ((startRate == 100) && (endRate == 100))
        {
            Exception ex = new Exception("Exception: VCurveSelector.selectBestRateForActiveProduct(Component component, Date disabledDate)- rates for start date and end date do not exist");
            throw ex;
        }
        
        if ((startRate < endRate) && priStart != null)
        {
            return priStart;
        }
        else
        {
            return priEnd;            
        }
    } 
}