package com.basis100.mtgrates;

import java.util.List;

import com.basis100.resources.SessionResourceKit;


/**
 * Title: IPricingLogicLenderProfile
 * <p>
 * Description: Interface of the PricingLogic: lenderProfile
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IPricingLogicLenderProfile extends IPricingLogic {

    /**
     * returns LenderProfile data
     * @param srk sessionResourceKit
     * @return list of LenderInfo.
     */
    public abstract List<LenderInfo> getLenderProfiles(SessionResourceKit srk);

}