package com.basis100.mtgrates;

/**
 * Title: IPricingLogicDataBean
 * <p>
 * Description: Interface of the data bean for pricinglogic
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IPricingLogicDataBean extends IPricingLogic {

}
