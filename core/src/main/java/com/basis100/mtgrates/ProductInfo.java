package com.basis100.mtgrates;
/**
 * Title: ProductInfo
 * <p>
 * Description: data bean implimentation
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class ProductInfo implements IPricingLogicDataBean {

    private int mtgProdId;
    private int repaymenttypeid;
    private String ratecode;

    public int getMtgProdId() {
        return mtgProdId;
    }

    public void setMtgProdId(int mtgProdId) {
        this.mtgProdId = mtgProdId;
    }

    public int getRepaymenttypeid() {
        return repaymenttypeid;
    }

    public void setRepaymenttypeid(int repaymenttypeid) {
        this.repaymenttypeid = repaymenttypeid;
    }

    public String getRatecode() {
        return ratecode;
    }

    public void setRatecode(String ratecode) {
        this.ratecode = ratecode;
    }

}
