package com.basis100.jdbcservices;

/** 
* If any failure happens during request for connection then instance of 
* this exception class should be thrown
* @see JdbcConnectionPool
*/


public final class ConnectionRequestFailedException extends Exception
{
	public ConnectionRequestFailedException() 
	{
		super();
	}
	public ConnectionRequestFailedException(String message)
	{
		super(message);
	}
}
