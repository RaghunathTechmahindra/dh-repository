package com.basis100.jdbcservices;

public interface JdbcConnectionLocator {
    public JdbcConnection getJdbcConnection();
}
