package com.basis100.jdbcservices.jdbcexecutor;

// Oracle7xExpressionGenerator:
//
// A class to produce Oracle7x specific expressions. See interface SqlExpressionGenerator
// for more details.

import com.basis100.util.date.*;

public class Oracle7xExpressionGenerator implements SqlExpressionGenerator
{
	public String applySpecialProcessing(String sql)
	{
		// no special processing required
		return sql;
	}
	public String getDateToString(Nds ds)
	{
		String exp = ds.getExpression();

		if (exp == null)
			return "NULL";

		return "to_char(" + exp + ", '" + ds.getMask() + "')";
	}
	public String getFieldName(Nfn fn)
	{
		return fn.getExpression();
	}
	public String getNumToString(Nnc nc)
	{
		String exp = nc.getExpression();

		if (exp == null)
			return "NULL";

		String mask = nc.getMask();

		if (mask.length() == 0)
			return "to_char(" + exp + ")";

		return "to_char(" + exp + ", '" + nc.getMask() + "')";
	}
	public String getStringToDate(Nsd sd)
	{

		String exp   = sd.getExpression();
		String mask  = sd.getMask();

		int addDays = sd.getAddDays();

		if (exp == null || exp.length() == 0)
			return "NULL";

		// Y2000
		if (mask.equals("DD-MON-YY") || mask.equals("DD MON YY"))
		{
			exp = DateUtilStrFmt.dateDD_Mon_YYtoDD_Mon_YYYY(exp);
			mask = "DD MON YYYY";
		}

		String sql = "to_date('" + exp + "', '" + mask + "')";

		if (addDays != sd.NO_ADD_DAYS)
			sql += " + " + addDays;

		return sql;
	}
	public String getSystemDate(Nsysdate sysdate)
	{
		String mask = sysdate.getMask();

		if (mask == null)
			return "sysdate";

		Nds ds = new Nds("sysdate", mask);
		return getDateToString(ds);
	}
	public String getToUppercase(Ntu tu)
	{
		String exp = tu.getExpression();

		if (exp == null)
			return "NULL";

		return "UPPER(" + exp + ")";
	}
}
