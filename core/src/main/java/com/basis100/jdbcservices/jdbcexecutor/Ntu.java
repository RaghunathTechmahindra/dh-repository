package com.basis100.jdbcservices.jdbcexecutor;

// Ntu:
//
// Non-DBMS specific statement support class used by class NSqlStatementBuild when building
// non-DBMS specific statements.
//
// This class holds the information required by an implementor of a NsqlExpressionGenerator
// to produce a string to uppercase conversion.
//
// Example:  let tu = new Ntu("name") be an object in a non-DBMS specific
//           statement (see class NSqlStatementBuild for details)
//
//           let Oracle7x be an object of a class that implements NsqlExpressionGenerator
//           for producing Oracle 7x expressions (see interface NsqlExpressionGenerator
//           for details:
//
//           then, in the Oracle7x specific statement tu could be represented as:
//
//                        "name.toUpperCase()"
//

public class Ntu extends Nxx
{
	String fldOrExpression;

	public Ntu(String fldOrExpression)
	{
		this.fldOrExpression = fldOrExpression;
	}
	public String getExpression()
	{
		return fldOrExpression;
	}
}
