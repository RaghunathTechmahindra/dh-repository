package com.basis100.jdbcservices.jdbcexecutor;

public final class JdbcTransactionException extends Exception
{
	public JdbcTransactionException()
	{
		super();
	}
	public JdbcTransactionException(String message)
	{
		super(message);
	}
}
