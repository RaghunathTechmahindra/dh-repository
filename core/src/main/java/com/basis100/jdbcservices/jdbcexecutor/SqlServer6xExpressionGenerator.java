package com.basis100.jdbcservices.jdbcexecutor;

// SqlServer6xExpressionGenerator:
//
// A class to produce SQL Server 6x expressions. See interface SqlExpressionGenerator
// for more details.

import com.basis100.util.date.*;

public class SqlServer6xExpressionGenerator implements SqlExpressionGenerator
{
	public String applySpecialProcessing(String sql)
	{
		// It seems most DBMS'a support '||' for catenation. SQL Server uses '+'.

		if (sql.indexOf("||") == -1)
			return sql;                // none!!

		return treat(sql);
	}
	public String getDateToString(Nds ds)
	{
		String exp  = ds.getExpression();

		if (exp == null)
			return "NULL";

		String mask = ds.getMask(); // expected to be a Oracle 7x date format

		if (mask.toUpperCase().equals("DD MON YY") ||
			mask.toUpperCase().equals("DD-MON-YY") )
		{
			return "CONVERT(VARCHAR, " + exp + ", 6)";
		}
		else if (mask.toUpperCase().equals("DD MON YYYY") ||
			mask.toUpperCase().equals("DD-MON-YYYY") )
		{
			return "CONVERT(VARCHAR, " + exp + ", 106)";
		}

		else if (mask.toUpperCase().equals("DD MON YY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD MON YY HH:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YY HH:MI:SS"))
		{
			return "CONVERT(VARCHAR, " + exp + ", 6) + ' ' + CONVERT(VARCHAR, " + exp + ", 8)";
		}
		else if (mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD MON YYYY HH:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH:MI:SS"))
		{
			return "CONVERT(VARCHAR(20), " + exp + ", 113)";
		}
		else if (mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS.SSS"))
		{
			return "CONVERT(VARCHAR(30), " + exp + ", 113)";
		}

		// else :: for now .....

		return "CONVERT(VARCHAR, " + exp + ")";
	}
	public String getFieldName(Nfn fn)
	{
		return fn.getExpression();
	}
	public String getNumToString(Nnc nc)
	{
		String exp  = nc.getExpression();

		if (exp == null)
			return "NULL";

		String mask = nc.getMask().trim();

		int len = mask.length();

		if (len == 0)
			return "CONVERT(VARCHAR, " +  exp + ")";

		int decNdx = mask.indexOf('.');

		if (decNdx != -1)
		{
			int digitsAfter = (len - (decNdx+1));
			len = 12 + digitsAfter + 1;           // trimming so prevent loss!

			return "LTRIM(STR(" + exp + ", " + len + ", " + digitsAfter + "))";
		}

		return "LTRIM(STR(" + exp + ", 12))";  // will be trimmed so we insure no loss (the 12)!
	}
	public String getStringToDate(Nsd sd)
	{
		String exp  = sd.getExpression();

		if (exp == null || exp.length() == 0)
			return "NULL";

		int addDays = sd.getAddDays();

		String mask = sd.getMask().toUpperCase(); // expected to be Oracle 7x date format

		String sql = null;

		if (mask.equals("DD-MON-YY")            || mask.equals("DD MON YY")            ||
			mask.equals("DD-MON-YYYY")          || mask.equals("DD MON YYYY")          ||
			mask.equals("DD MON YY HH24:MI:SS") || mask.equals("DD-MON-YY HH24:MI:SS") ||
			mask.equals("DD MON YY HH:MI:SS")   || mask.equals("DD-MON-YY HH:MI:SS"))
		{
			String dateStr = (new DateUtilStrFmt()).dateDD_MMM_YY_HH_MM_SStoMMM_DD_YYYY_HH_mm_SS(exp.toUpperCase()) ;
			sql = "CONVERT(DATETIME, '" + dateStr + "')";
		}
		else if (mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD MON YYYY HH:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH:MI:SS"))
		{
			return "'" + exp + "'";
		}
		else if (mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS.SSS"))
		{
			return "'" + exp + "'";
		}
		else
		{
			// for now try ...
			sql = "DATE('" + exp + "')";
		}

		if (addDays != sd.NO_ADD_DAYS)
		{
			sql = "DATEADD(day, " + addDays + "," + sql + ")";
		}

		return sql;
	}
	public String getSystemDate(Nsysdate sysdate)
	{
		String mask = sysdate.getMask();

		if (mask == null)
			return "getDate()";

		Nds ds = new Nds("getDate()", mask);
		return getDateToString(ds);
	}
	public String getToUppercase(Ntu tu)
	{
		String exp  = tu.getExpression();

		if (exp == null)
			return "NULL";

		return "UPPER(" + exp + ")";
	}
	private int replaceTag(String source, int searchAtNdx, StringBuffer dest, String tag, String replacement)
	{
		int ndx = source.indexOf(tag, searchAtNdx);

		if (ndx == -1)
			return -1;

		dest.append(source.substring(searchAtNdx, ndx));
		dest.append(replacement);

		return (ndx + tag.length());
	}
	// treat:
	//
	// Given a string following sequences if encountered:
	//
	// "||" with  "+"

	private String treat(String srce)
	{
		StringBuffer dest = new StringBuffer();

		int searchAtNdx = 0;

		int lim = srce.length();
		int ndx;

		for (; searchAtNdx < lim;)
		{
			if ((ndx = replaceTag(srce, searchAtNdx, dest, "||", "+")) != -1)
			{
				searchAtNdx = ndx;
				continue;
			}

			// no more -- copy remainder of original to buffer
			dest.append(srce.substring(searchAtNdx));
			break;
		}

		return dest.toString();
	}
}
