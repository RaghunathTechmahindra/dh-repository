package com.basis100.jdbcservices.jdbcexecutor;


// SqlExpressionGenerator:
//
// Interface specifying requirements for sql expression generators. A statement generator
// is responsible for producing DBMS specific expressions.
//
// Example:    let o7x be an Oracle7x expression generator
//             let nToS = new Nnc("amount", "999990.99");
//
//              then: o7x.getNumToString(nToNnc nToS) would likely return:
//
//                     "to_char('amount', '999990.99')"
//

public interface SqlExpressionGenerator
{
	// This method will be called immediately before a statement is executed. The statement
	// passed will already have DBMS specific expressions applied. This method may be
	// used to apply special processing required to prepare the statement for execution.

	public String applySpecialProcessing(String sql);
	public String getDateToString(Nds ds);
	public String getFieldName(Nfn fn);
	public String getNumToString(Nnc nc);
	public String getStringToDate(Nsd sd);
	public String getSystemDate(Nsysdate sysdate);
	public String getToUppercase(Ntu tu);
}
