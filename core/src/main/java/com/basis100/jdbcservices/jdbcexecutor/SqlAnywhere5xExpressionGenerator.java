package com.basis100.jdbcservices.jdbcexecutor;

// SqlAnywhere5xExpressionGenerator:
//
// A class to produce SQL Anywhere 5x expressions. See interface SqlExpressionGenerator
// for more details.

import com.basis100.util.date.*;

public class SqlAnywhere5xExpressionGenerator implements SqlExpressionGenerator
{
	public String applySpecialProcessing(String sql)
	{
		// no special processing required
		return sql;
	}
	public String getDateToString(Nds ds)
	{
		String exp  = ds.getExpression();

		if (exp == null)
			return "NULL";

		String mask = ds.getMask(); // expected to be a Oracle 7x date format

		mask = getEquivalentDateMask(mask);

		// else :: for now just try using existing mask

		return "DATEFORMAT(" + exp + ", '" + mask + "')";
	}
	// getEquivalentDateMask:
	//
	// Given a oracle date formatting return equivalent for SQL Anywhere

	private String getEquivalentDateMask(String mask)
	{
		mask = mask.toUpperCase();

		if (mask.equals("DD-MON-YY")   || mask.equals("DD MON YY"))
			mask = "DD Mmm YY";

		else if (mask.equals("DD-MON-YYYY") || mask.equals("DD MON YYYY"))
			mask = "DD Mmm YYYY";

		else if (mask.toUpperCase().equals("DD MON YY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD MON YY HH:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YY HH:MI:SS"))
			mask = "DD Mmm YY HH:NN:SS";

		else if (mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD MON YYYY HH:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH:MI:SS"))
			mask = "DD Mmm YYYY HH:NN:SS";
		else if (mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS.SSS"))
			mask = "DD Mmm YYYY HH:NN:SS.sss";


	   return mask; // i.e. if not a detected type just return mask passed in!
	}
	public String getFieldName(Nfn fn)
	{
		String oracleFieldName = fn.getExpression().toUpperCase();

		if (oracleFieldName.equals("REFERENCE"))
			return "reference1";

		// assume same field name
		return oracleFieldName;
	}
	public String getNumToString(Nnc nc)
	{
		String exp  = nc.getExpression();

		if (exp == null)
			return "NULL";

		String mask = nc.getMask().trim();


		int len = mask.length();

		if (len == 0)
			return "CONVERT(VARCHAR, " +  exp + ")";

		int decNdx = mask.indexOf('.');

		int precision = 0;
		int scale     = 0;

		if (decNdx == -1)
		{
			precision = len;
		}
		else
		{
			precision = len - 1;
			scale     = precision - decNdx;
		}

		return  "TRIM(CONVERT(VARCHAR(" + len + "), CAST(" + exp + " AS NUMERIC(" + precision + ", " + scale + "))))";
	}
	public String getStringToDate(Nsd sd)
	{
		String exp  = sd.getExpression();

		if (exp == null || exp.length() == 0)
			return "NULL";

		int addDays = sd.getAddDays();

		String mask = sd.getMask().toUpperCase(); // expected to be Oracle 7x date format

		String sql = null;

		if (mask.equals("DD-MON-YY")   || mask.equals("DD MON YY") ||
			mask.equals("DD-MON-YYYY") || mask.equals("DD MON YYYY"))
		{
			// accepts the YYYY formats because data function used allows for YYYY formats
			// as well
			String dateStr = (new DateUtilStrFmt()).dateDD_MMM_YYtoYYYY_MM_DD(exp.toUpperCase());
			sql = "CONVERT(DATETIME, '" + dateStr + "')";
		}
		else if (mask.equals("DD MON YY HH24:MI:SS") ||
				 mask.equals("DD-MON-YY HH24:MI:SS") ||
				 mask.equals("DD MON YY HH:MI:SS") ||
				 mask.equals("DD-MON-YY HH:MI:SS"))
		{
			String dateStr = (new DateUtilStrFmt()).dateDD_MMM_YY_HH_MM_SStoYYYY_MM_DD_HH_mm_SS(exp.toUpperCase());
			sql = "CONVERT(DATETIME, '" + dateStr + "')";
		}
		else if (mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD MON YYYY HH:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH24:MI:SS") ||
				 mask.toUpperCase().equals("DD-MON-YYYY HH:MI:SS") ||

				 mask.toUpperCase().equals("DD MON YYYY HH24:MI:SS.SSS"))
		{
			String dateStr = (new DateUtilStrFmt()).dateDD_MMM_YY_HH_MM_SStoYYYY_MM_DD_HH_mm_SS(exp.toUpperCase());
			sql = "CONVERT(DATETIME, '" + dateStr + "')";
		}

		else
		{
			// for now try ...
			sql = "DATE('" + exp + "')";
		}

		if (addDays != sd.NO_ADD_DAYS)
		{
			sql = "DATEADD(day, " + addDays + "," + sql + ")";
		}

		return sql;
	}
	public String getSystemDate(Nsysdate sysdate)
	{
		String mask = sysdate.getMask();

		if (mask == null)
			return "NOW(*)";

		Nds ds = new Nds("NOW(*)", mask);
		return getDateToString(ds);
	}
	public String getToUppercase(Ntu tu)
	{
		String exp  = tu.getExpression();

		if (exp == null)
			return "NULL";

		return "UCASE(" + exp + ")";
	}
}
