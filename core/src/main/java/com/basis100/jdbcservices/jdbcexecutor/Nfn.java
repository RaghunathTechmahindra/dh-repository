package com.basis100.jdbcservices.jdbcexecutor;

// Nfn:
//
// Non-DBMS specific statement support class used by class NSqlStatementBuild when building
// non-DBMS specific statements.
//
// The original design supported Oracle 7x. In some instances field names in Oracle 7x
// are reserved words in other DBMS. This class holds the original Oracle 7x field name.
//
// Example:  let fn = new Nfn("reference") be an object in a non-DBMS specific
//           statement (see class NSqlStatementBuild for details)
//
//           let SQLAnywhere5x be an object of a class that implements NsqlExpressionGenerator
//           for producing SQL Anywhere 5x expressions (see interface NsqlExpressionGenerator
//           for details:
//
//           then, in the SQL Anywhere 5x specific statement fn could be represented as:
//
//                        "reference1"
//
//           that is, we assume the original reference field in an Oracle 7x DB is renamed
//           to reference1 in a SQL Anywhere DB.
//

public class Nfn extends Nxx
{
	String fld;

	public Nfn(String fld)
	{
		this.fld = fld;
	}
	public String getExpression()
	{
		return fld;
	}
}
