package com.basis100.jdbcservices.jdbcexecutor;

// Nds:
//
// Non-DBMS specific statement support class used by class NSqlStatementBuilder when building
// non-DBMS specific statements.
//
// This class holds the information required by an implementor of a NsqlExpressionGenerator
// to produce a date to string conversion.
//
// Example:  let dc = new Nds("date", "DD Mon YY"); be an object in a non-DBMS specific
//           statement (see class NSqlStatementBuild for details)
//
//           let Oracle7x be an object of a class that implements NsqlExpressionGenerator
//           for producing Oracle 7x expressions (see interface NsqlExpressionGenerator
//           for details):
//
//           then, in the Oracle7x specific statement dc could be represented as:
//
//                        "to_char(date, 'DD Mon YY')"
//
//
// Notes:
//
// The <mask> parameter in constructor is the date format expected in Oracle 7x to_char()
// function.

public class Nds extends Nxx
{
	String fldOrExpression;
	String mask;

	public Nds(String fldOrExpression, String mask)
	{
		this.fldOrExpression = fldOrExpression;
		this.mask            = mask;
	}
	public String getExpression()
	{
		return fldOrExpression;
	}
	public String getMask()
	{
		return mask;
	}
}
