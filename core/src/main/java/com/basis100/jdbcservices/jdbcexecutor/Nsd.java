package com.basis100.jdbcservices.jdbcexecutor;

// Nsd:
//
// Non-DBMS specific statement support class used by class NSqlStatementBuild when building
// non-DBMS specific statements.
//
// This class holds the information required by an implementor of a NsqlExpressionGenerator
// to produce a string to date conversion.
//
// Example:  let sd = new Nsd("02 Aug 98", "DD-MON-YY"); be an object in a non-DBMS specific
//           statement (see class NSqlStatementBuild for details)
//
//           let Oracle7x be an object of a class that implements NsqlExpressionGenerator
//           for producing Oracle 7x expressions (see interface NsqlExpressionGenerator
//           for details):
//
//           then, in the Oracle7x specific statement sd could be represented as:
//
//                        "to_date('02 Aug 98', 'DD-MON-YY')"
//
//
// Notes:
//
// The <mask> parameter in constructor is the date format expected in Oracle 7x to_date()
// function.

public class Nsd extends Nxx
{
	String fldOrExpression;
	String mask;

	public static final int NO_ADD_DAYS = -45689;

	int addDays = NO_ADD_DAYS;

	public Nsd(String fldOrExpression, String mask)
	{
		this.fldOrExpression = fldOrExpression;
		this.mask            = mask;
	}
	public Nsd(String fldOrExpression, String mask, int addDays)
	{
		this.fldOrExpression = fldOrExpression;
		this.mask            = mask;
		this.addDays         = addDays;
	}
	public int getAddDays()
	{
		return addDays;
	}
	public String getExpression()
	{
		return fldOrExpression;
	}
	public String getMask()
	{
		return mask;
	}
}
