package com.basis100.jdbcservices.jdbcexecutor;

// Nnc:
//
// Non-DBMS specific statement support class used by class NSqlStatementBuild when building
// non-DBMS specific statements.
//
// This class holds the information required by an implementor of a NsqlExpressionGenerator
// to produce a numeric to string conversion.
//
// Example:  let nc = new Nnc("amount", "999990.99") be an object in a non-DBMS specific
//           statement (see class NSqlStatementBuild for details)
//
//           let Oracle7x be an object of a class that implements NsqlExpressionGenerator
//           for producing Oracle 7x expressions (see interface NsqlExpressionGenerator
//           for details:
//
//           then, in the Oracle7x specific statement nc could be represented as:
//
//                        "to_char(amount, '999990.99')"
//
// Notes:
//
// If <mask> (see constructor) is an empty string a simple numeric to character
// conversion is to be performed.

public class Nnc extends Nxx
{
	String fldOrExpression;
	String mask;

	public Nnc(String fldOrExpression, String mask)
	{
		this.fldOrExpression = fldOrExpression;
		this.mask            = mask;
	}
	public String getExpression()
	{
		return fldOrExpression;
	}
	public String getMask()
	{
		return mask;
	}
}
