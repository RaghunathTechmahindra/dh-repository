// DatabaseTableInfo:
//
// Instances of this class hold information about a database table. Specifically it holds
// a list of the column names and column data types.
//
// This information is used to generate select query statements and to form insert
// record statements, see:
//
//  getSelectSql -  Form a query that requests the information from all columns in a table.
//                  Where clause can be specified to limit the scope of the query.
//
//  getInsertSql -  Form a insert record (with values) statement.

package com.basis100.jdbcservices.jdbcexecutor;

import java.util.*;
import java.sql.*;
import java.io.*;

import com.basis100.log.*;
import com.basis100.util.interfaces.*;
import com.basis100.util.file.*;
import com.basis100.util.sort.*;

public class DatabaseTableInfo
{
    static boolean recordPrimaryKeys = true;

    private String tableName;
    private String tableOwner;

    SqlExpressionGenerator nseg;

    // data types (we map JDBC types to these internal types)

    private static final int STRING     = 1;
    private static final int INTEGER    = 2;
    private static final int DECIMAL    = 3;
    private static final int TIMESTAMP  = 4;
    private static final int OTHER      = 5;

    Vector columnNames             = new Vector();
    Vector columnDataTypes         = new Vector();
    Vector columnJdbcDataTypes     = new Vector();
    Vector columnSizes             = new Vector();
    Vector columnIds               = new Vector();

    Vector primaryKeyFlags         = null;

    int numberOfColumns;

    Object nonDBMSSelectStatement;
    Object nonDBMSInsertStatement;


    // Constructor:

    IniData tableInfoDumpIni = null;

    DatabaseTableInfo(JdbcExecutor jExec, Connection con, String tableName, String tableOwner,
                      boolean ordered, SqlExpressionGenerator nseg,
                      IniData tableInfoDumpIni, String tableInfoDumpFileName)
    {
        this.nseg             = nseg;

        if (nseg == null)
            nseg = new Oracle7xExpressionGenerator();

        this.tableInfoDumpIni = tableInfoDumpIni;
        this.tableOwner       = tableOwner;
        this.tableName        = tableName;

        ResultSet rs = null;
        DatabaseMetaData dbmd;
        try
        {
            /////////////////////////////////////////////////////////////
            //
            //  Column Type and Size
            //
            /////////////////////////////////////////////////////////////

/*

            //synchronized (Ncon.simultaneousExecutionToken)
            //{
            dbmd = con.getMetaData();

            rs = dbmd.getColumns(null, tableOwner, tableName, "%");
            //}

            while(rs.next())
            {
                if (false == mapTypeToInternalType(columnDataTypes, columnSizes, rs.getInt("DATA_TYPE"), rs.getInt("COLUMN_SIZE"), rs.getInt("DECIMAL_DIGITS")))
                    continue;  // data type ignored - ignore entire column

                columnJdbcDataTypes.addElement(new Integer(dataType));
            }

            rs.close();
*/

            int key     = jExec.execute("Select * from " + tableOwner + "." + tableName);

            ResultSetMetaData metaResult = jExec.getResultSetMetaData(key);

            int numCols = metaResult.getColumnCount();

            int colType = 0, colScale = 0, colPrecision = 0;

            for (int index = 1; index <= numCols; ++index)
            {
              colType       = metaResult.getColumnType(index);
              colPrecision  = metaResult.getPrecision(index);
              colScale      = metaResult.getScale(index);


              if (false == mapTypeToInternalType(columnDataTypes, columnSizes, colType, colPrecision, colScale))
                  continue;  // data type ignored - ignore entire column

              columnJdbcDataTypes.add(new Integer(colType));
            }

            jExec.closeData(key);

            /////////////////////////////////////////////////////////////
            //
            //  Column Names and Ids
            //
            /////////////////////////////////////////////////////////////

            String colNameSql =

            "SELECT T1.COLUMN_NAME, T1.COLUMN_ID FROM ALL_TAB_COLUMNS T1 WHERE " +
            "(T1.OWNER = '" + tableOwner.toUpperCase() + "') AND " +
            "(T1.TABLE_NAME = '" + tableName.toUpperCase() + "')" +
            "order by column_id";

            key = jExec.execute(colNameSql);

            while(jExec.next(key))
            {
                columnNames.addElement((jExec.getString(key, 1)).toUpperCase());
                columnIds.addElement(new Long(jExec.getInt(key, 2)));
            }

            jExec.closeData(key);

            /////////////////////////////////////////////////////////////
            //
            //  Primary Keys
            //
            /////////////////////////////////////////////////////////////


            if (recordPrimaryKeys)
              {
                primaryKeyFlags = new Vector();

                Boolean bfalse = new Boolean(false);
                Boolean btrue  = new Boolean(true);

                int len = columnNames.size();
                for (int ii = 0; ii < len; ++ii)
                {
                    primaryKeyFlags.addElement(bfalse);
                }

                //synchronized (Ncon.simultaneousExecutionToken)
                //{
                dbmd = con.getMetaData();

                rs = dbmd.getPrimaryKeys(null, tableOwner, tableName);
                //}

                while(rs.next())
                {
                    int keyColNdx = getColumnNdx(columnNames, rs.getString("COLUMN_NAME"));
                    if (keyColNdx != -1)
                      primaryKeyFlags.setElementAt(btrue, keyColNdx);
                }
                rs.close();
              }
        }
        catch (Exception e)
        {
            try {rs.close();} catch (Exception ee) {;}

            SysLog.error("NdatabaseTableInfo -> Exception: " + e.toString());
            SysLog.error(e);
        }

        numberOfColumns = columnNames.size();

        if (numberOfColumns <= 0)
            return;

        // sort column names (and corresponding data types) into ascending order
        if (ordered)
            orderColumns();
        else
            orderColumns(columnIds);

            

        if (tableInfoDumpIni != null)
        {
            for (int i = 0; i < numberOfColumns; ++i)
            {
                String colInfo = getColumnPrintString((String)columnNames.elementAt(i),
                              ((Integer)columnDataTypes.elementAt(i)).intValue(),
                              ((Integer)columnJdbcDataTypes.elementAt(i)).intValue());

                String cId = (99-i) + " ";
                tableInfoDumpIni.setValue(tableName, cId, colInfo);
            }
            
            tableInfoDumpIni.dumpIniFile(tableInfoDumpFileName);
        }

        //
        // form (partial, excluding FROM and WHERE clauses) select sql
        //

        SqlStatementBuilder sb = new SqlStatementBuilder();

        int lim = numberOfColumns - 1; // to iterate over all but last column

        sb.add("SELECT ");
        
        int i = 0;

        for (; i < lim; ++i)
        {
            addFieldToSelect(sb, (String)columnNames.elementAt(i),
                            ((Integer)columnDataTypes.elementAt(i)).intValue(), ", ");

        }

        // last column (possible bug - where type of last field is OTHER)
        addFieldToSelect(sb, (String)columnNames.elementAt(i),
                        ((Integer)columnDataTypes.elementAt(i)).intValue(), " ");

        nonDBMSSelectStatement = sb.getStatement();

        //
        // form (partial, missing INSERT INTO {table}, and values) inset sql : i.e.
        //

        sb = new SqlStatementBuilder();

        // field names (using trick - code above but with all columns represented as
        // string types - hence no conversion for data types just field names)

        for (i = 0; i < lim; ++i)
            addFieldToSelect(sb, (String)columnNames.elementAt(i), STRING, ", ");

        // last field
        addFieldToSelect(sb, (String)columnNames.elementAt(i), STRING, ") VALUES (");

        nonDBMSInsertStatement = sb.getStatement();
    }

    private boolean addFieldToSelect(SqlStatementBuilder sb, String columnName, int type, String appendStr)
    {
        if (type == STRING || type == INTEGER)
        {
            sb.add(columnName + appendStr);
            return true;
        }

        if (type == DECIMAL)
        {
            sb.add(new Nnc(columnName, "9999999990.9999999"));
            sb.add(appendStr);

            return true;
        }

        if (type == TIMESTAMP)
        {
            sb.add(new Nds(columnName, "DD MON YYYY HH24:MI:SS")); //it was "DD MON YY HH24:MI:SS"
            sb.add(appendStr);

            return true;
        }

        return false;
    }

    // Methods ...

    public String getTableName()
    {
        return tableName;
    }

    public String getTableOwner()
    {
        return tableOwner;
    }

    public int getNumberOfColumns()
    {
        return numberOfColumns;
    }

    // getJdbcType:
    //
    // Given a column name return the JDBC data type constant. Return 256 if column name is
    // invalid.

    public int getJdbcType(String columnName)
    {
        columnName = columnName.trim().toUpperCase();

        for (int i = 0; i < numberOfColumns; ++i)
        {
            if (columnNames.elementAt(i).equals(columnName))
                return ((Integer)columnJdbcDataTypes.elementAt(i)).intValue();
        }

        return 256;
    }

    // getSelectSql:
    //
    // Returns a select statement based upon the table information maintained (all
    // columns with conversions to string data). Optional where clause may be
    // specified.

    public String getSelectSql(String whereClause) throws Exception
    {
        return getSelectSql(null, whereClause);
    }

    // version that allowed alternate table owner to be specified.
    //
    // E.G.: Select against some other DB with an identically structured table, but the
    //       table is owner by a differ user

    public String getSelectSql(String owner, String whereClause) throws Exception
    {
        String sql = SqlStatementBuilder.getStatementForDBMS(nonDBMSSelectStatement, nseg);

        if (owner == null)
          sql += "FROM  " + tableName + " ";
        else
          sql += "FROM  " + owner + "." + tableName + " ";

/*
        if (owner == null)
            owner = tableOwner;

        // from ...
        sql += "FROM  " + owner + "." + tableName + " ";
*/

        if (whereClause != null)
            sql += whereClause;

        return sql;
    }

    // getUpdateSql
    //
    // Similar to getInsertSql (see below) - usage limited (e.g. to logic that tests
    // database triggers).
    //
    // PROGRAMMER NOTE
    //
    // A field may be excluded from the update statement by specifing a value that begins with
    // a "_" or with a "="; e.g. to eliminate the third field set data[2] = "_ignore".

    public String getUpdateSql(String owner, String [] data, String where) throws Exception
    {
        SqlStatementBuilder sb = new SqlStatementBuilder();

        if (owner == null)
            owner = tableOwner;

        sb.add("UPDATE " + owner + "." + tableName + " SET ");

        int lim = numberOfColumns;
        int i   = 0;

        boolean first = true;

        for (; i < lim; ++i)
        {
            if (data[i] == null || data[i].startsWith("_") || data[i].startsWith("="))
              continue;

            if (first == false)
              sb.add(", ");

            addFieldToSelect(sb, (String)columnNames.elementAt(i), STRING, " = ");
            addDataToInsert(sb, data[i],
                            ((Integer)columnDataTypes.elementAt(i)).intValue(),
                            ((Integer)columnSizes.elementAt(i)).intValue(),
                            "");

            first = false;
        }

        sb.add(where);

        return sb.getStatementForDBMS(sb.getStatement(), nseg);
    }

    public String getUpdateSql(String owner, Vector data, String where) throws Exception
    {
        SqlStatementBuilder sb = new SqlStatementBuilder();

        if (owner == null)
            owner = tableOwner;

        sb.add("UPDATE " + owner + "." + tableName + " SET ");

        int lim = numberOfColumns;
        int i   = 0;

        boolean first = true;

        for (; i < lim; ++i)
        {
            String datum = (String)data.elementAt(i);

            if (datum == null || datum.startsWith("_") || datum.startsWith("="))
              continue;

            if (first == false)
              sb.add(", ");

            addFieldToSelect(sb, (String)columnNames.elementAt(i), STRING, " = ");
            addDataToInsert(sb, datum,
                            ((Integer)columnDataTypes.elementAt(i)).intValue(),
                            ((Integer)columnSizes.elementAt(i)).intValue(),
                            "");

            first = false;
        }

        sb.add(where);

        return sb.getStatementForDBMS(sb.getStatement(), nseg);
    }


    // getPrimaryKeyWhereSql:
    //
    // PROGRAMMER NOTE
    //
    // When <allowIgnore> is true any (primary key) field that starts with a "_" or "=" character
    // is excluded from the where clause; e.g. to eliminate the third (primary key) field set
    // data[2] = "_ignore".
    //
    // If any (primary key) field that starts with a "_" or "=" character is encountered when <allowIgnore>
    // is false null is returned.
    //

    public String getPrimaryKeyWhereSql(Vector data, boolean allowIgnore) throws Exception
    {
        // ensure info on primary key fields
        if (primaryKeyFlags == null)
            return null;

        SqlStatementBuilder sb = new SqlStatementBuilder();

        sb.add(" WHERE ");

        int lim = numberOfColumns;
        int i = 0;

        boolean haveSome = false;

        for (; i < lim; ++i)
        {
            // skip if not primary key field
            if (((Boolean)primaryKeyFlags.elementAt(i)).booleanValue() == false)
                continue;

            String datum = (String)data.elementAt(i);

            if (allowIgnore == false && datum != null && (datum.startsWith("_") || datum.startsWith("=")))
                return null;

            if (datum == null || datum.startsWith("_") || datum.startsWith("="))
              continue;

            if (haveSome == true)
               sb.add(" AND ");

            addFieldToSelect(sb, (String)columnNames.elementAt(i), STRING, " = ");
            addDataToInsert(sb, datum,
                     ((Integer)columnDataTypes.elementAt(i)).intValue(),
                     ((Integer)columnSizes.elementAt(i)).intValue(),
                     "");

            haveSome = true;
        }

        if (haveSome == false)
            return null;

        return sb.getStatementForDBMS(sb.getStatement(), nseg);
    }


    // getDeleteSql
    //
    // Similar to getInsertSql (see below) - usage limited (e.g. to logic that tests
    // database triggers).

    public String getDeleteSql(String owner, String [] data, String where) throws Exception
    {
        SqlStatementBuilder sb = new SqlStatementBuilder();

        if (owner == null)
            owner = tableOwner;

        sb.add("DELETE FROM " + owner + "." + tableName + " ");

        if (where != null)
        {
            sb.add(where);
            return sb.getStatementForDBMS(sb.getStatement(), nseg);
        }

        // no where clause - construct one using all fields (if data provided)
        if (data == null)
            return sb.getStatementForDBMS(sb.getStatement(), nseg);

        // selective delete

        sb.add("WHERE ");
        int lim = numberOfColumns - 1; // iterate over all but last column
        int i   = 0;

        for (; i < lim; ++i)
        {
            addFieldToSelect(sb, (String)columnNames.elementAt(i), STRING, " = ");
            addDataToInsert(sb, data[i],
                            ((Integer)columnDataTypes.elementAt(i)).intValue(),
                            ((Integer)columnSizes.elementAt(i)).intValue(),
                            " AND ");

        }

        addFieldToSelect(sb, (String)columnNames.elementAt(i), STRING, " = ");
        addDataToInsert(sb, data[i],
                        ((Integer)columnDataTypes.elementAt(i)).intValue(),
                        ((Integer)columnSizes.elementAt(i)).intValue(),
                        " ");

        return sb.getStatementForDBMS(sb.getStatement(), nseg);
    }


    // getInsertSql:
    //
    // Returns an insert statement based upon the table information maintained.
    // The data for the insert is passed (as an array of strings).
    //
    // Notes:
    //
    // The order of the data must match the fields order as represented in a select
    // statment created by a call to getSelectSql(). !IMPT! Columns with data type
    // OTHER are not retrieved in statements created by getSelectSql() and hence there
    // must not appear in the data presented to this method.

    public String getInsertSql(Vector data) throws Exception
    {
        int lim = data.size();
        
        String [] dataArray = new String[lim];
        
        for (int i = 0; i < lim; ++i)
            dataArray[i] = (String)data.elementAt(i);
            
        return getInsertSql(dataArray);
    }

    public String getInsertSql(String [] data) throws Exception
    {
        return getInsertSql(null, data);
    }

    // version that allowed alternate table owner to be specified.
    //
    // E.G.: Insert to some other DB with an identically structured table, but the
    //       table is owner by a differ user

    public String getInsertSql(String owner, String [] data) throws Exception
    {
        String sql = SqlStatementBuilder.getStatementForDBMS(nonDBMSInsertStatement, nseg);

        if (owner == null)
            owner = tableOwner;

        sql ="INSERT INTO " + owner + "." + tableName + " (" + sql;

        // add data to statement

        SqlStatementBuilder sb = new SqlStatementBuilder();

        int lim = numberOfColumns - 1; // to iterate over all but last column

        // all but last data element

        int columnNameNdx = 0;
        int dataNdx       = 0;  // may differ from above if some columns have data type OTHER
        
        try
        {

        for (; columnNameNdx < lim; ++columnNameNdx)
        {
            int type = ((Integer)columnDataTypes.elementAt(columnNameNdx)).intValue();

            if (type == OTHER)
                continue;

            addDataToInsert(sb, data[dataNdx],
                            ((Integer)columnDataTypes.elementAt(columnNameNdx)).intValue(),
                            ((Integer)columnSizes.elementAt(columnNameNdx)).intValue(),
                            ", ");
            dataNdx++;
        }

        // last data element (!BUG! - where data type of last column is OTHER!)
        addDataToInsert(sb, data[dataNdx],
                        ((Integer)columnDataTypes.elementAt(columnNameNdx)).intValue(),
                        ((Integer)columnSizes.elementAt(columnNameNdx)).intValue(),                                                    
                        ") ");

        }
        catch (Exception e)
        {
            throw e;
        }

        Object dataPart = sb.getStatement();

        String sql2 = sb.getStatementForDBMS(dataPart, nseg);

        return sql + sql2;
    }

    private boolean addDataToInsert(SqlStatementBuilder sb, String data, int type, int size, String appendStr)
    {
        if (data == null)
        {
            sb.add("NULL" + appendStr);
            return true;
        }

        if (type == STRING)
        {
            if (size > 0)
            {
                // ensure input not too long
                if (data.length() > size)
                    data = data.substring(0, size);
            }

            sb.add("'" + singleQuotePadding(data) + "'" + appendStr);
            return true;
        }

        if (type == DECIMAL)
        {
            if (data.length() == 0)
                sb.add("0.0" + appendStr);
            else
                sb.add(data + appendStr);

            return true;
        }

        if (type == INTEGER)
        {
            if (data.length() == 0)
                sb.add("0" + appendStr);
            else
                sb.add(data + appendStr);

            return true;
        }

        if (type == TIMESTAMP)
        {
            if (data.equalsIgnoreCase("_sysdate"))
                sb.add(new Nsysdate());
            else
                sb.add(new Nsd(data, "DD MON YYYY HH24:MI:SS")); //it was "DD MON YY HH24:MI:SS"

            sb.add(appendStr);

            return true;
        }

        return false;
    }

    // getDummyData:
    //
    // Diagnostic routine that simply returns an array with an entry for each column in
    // the table.

    public String [] getDummyData(String employeeId, boolean update, boolean updatePrimary)
    {
        String [] dummyData = new String[numberOfColumns];

        updatePrimary = update && updatePrimary;

        int dNdx = 0;
        for (int i = 0; i < numberOfColumns; ++i)
        {
            if (((String)columnNames.elementAt(i)).equals("EMPLOYEE_ID"))
            {
                dummyData[dNdx] = employeeId;
                dNdx++;

                continue;
            }

            int type = ((Integer)columnDataTypes.elementAt(i)).intValue();

            if (type == OTHER)
                continue;

            if (type == STRING)
            {
                if (updatePrimary)
                    dummyData[dNdx] = "Y";
                else
                    dummyData[dNdx] = "X";
            }


            else if (type == DECIMAL)
            {
                if (updatePrimary)
                    dummyData[dNdx] = -(i+2) + ".0";
                else
                    dummyData[dNdx] = -(i+1) + ".0";
            }
            else if (type == INTEGER)
            {
                if (updatePrimary)
                    dummyData[dNdx] = -(i+2) + "";
                else
                    dummyData[dNdx] = -(i+1) + "";
            }
            else // if (type == TIMESTAMP)
                {
                    if (update)
                        dummyData[dNdx] = "01 JAN 1998 13:00:00";
                    else
                        dummyData[dNdx] = "01 JAN 1998 12:00:00";
                }

            dNdx++;
        }

       return dummyData;
    }

    public int columnNdx(String columnName)
    {
        int lim = columnNames.size();

        for (int i = 0; i < lim; ++i)
        {
            if (((String)columnNames.elementAt(i)).equals(columnName))
                return i;
        }

        return -1;
    }

    private String getJDBCTypeName(int jdbcType)
    {
        switch (jdbcType)
        {
            case Types.NUMERIC:
                return "NUMERIC";

            case Types.DECIMAL:
                return "DECIMAL";

            case Types.FLOAT:
                return "FLOAT";

            case Types.REAL:
                return "REAL";

            case Types.DOUBLE:
                return "DOUBLE";

            case Types.BIT:
                return "BIT";

            case Types.TINYINT:
                return "TINYINT";

            case Types.SMALLINT:
                return "SMALLINT";

            case Types.INTEGER:
                return "INTEGER";

            case Types.BIGINT:
                return "BIGINT";

            case Types.DATE:
                return "DATE";

            case Types.TIME:
                return "TIME";

            case Types.TIMESTAMP:
                return "TIMESTAMP";

            case Types.CHAR:
                return "CHAR";

            case Types.VARCHAR:
                return "VARCHAR";

            case Types.LONGVARCHAR:
                return "LONGVARCHAR";

            case 11:
                return "Date (***11***)";

            case 9:
                return "Date (***9***)";

            default:

        }

        return "*" + jdbcType + "*";
    }


    private String getTypeName(int type)
    {
        switch (type)
        {
            case STRING:
                return "STRING";

            case INTEGER:
                return "INTEGER";

            case DECIMAL:
                return "DECIMAL";

            case TIMESTAMP:
                return "TIMESTAMP";
        }

        return "*OTHER*";
    }

    private String getColumnPrintString(String tableName, int type, int jdbcType)
    {
        String pad = "                                                               ";

        String result = tableName;

        result = result + pad.substring(0, 30-result.length());

        result = result + getTypeName(type);

        result = result + pad.substring(0, 45-result.length());

        result = result + getJDBCTypeName(jdbcType);

        return result;
    }



    // orderColumns()
    //
    // Used to place columns in a predictable order - used when piping data between
    // differrent DBMSs so as to insure a predictable order (meta data call may return
    // differnt order for one JDBC driver vs. another)

    private void orderColumns()
    {
        int i;

        long []colNameHashCodes = new long[numberOfColumns];
        for (i = 0; i < numberOfColumns; ++i)
        {
            colNameHashCodes[i] = (long)((String)columnNames.elementAt(i)).hashCode();
        }

        int []crossNdxs = NumericFunction.sortNumberOrderIndicies(colNameHashCodes);

        Vector sortedColumnNames         = new Vector();
        Vector sortedColumnDataTypes     = new Vector();
        Vector sortedColumnJdbcDataTypes = new Vector();
        Vector sortedColumnSizes          = new Vector();

        for (i = 0; i < numberOfColumns; ++i)
        {
            sortedColumnNames.addElement(columnNames.elementAt(crossNdxs[i]));
            sortedColumnDataTypes.addElement(columnDataTypes.elementAt(crossNdxs[i]));
            sortedColumnJdbcDataTypes.addElement(columnJdbcDataTypes.elementAt(crossNdxs[i]));
            sortedColumnSizes.addElement(columnSizes.elementAt(crossNdxs[i]));

        }

        columnNames         = sortedColumnNames;
        columnDataTypes     = sortedColumnDataTypes;
        columnJdbcDataTypes = sortedColumnJdbcDataTypes;
        columnSizes         = sortedColumnSizes;

    }

    private void orderColumns(Vector columnIds)
    {
        int i;

        long []colIds = new long[numberOfColumns];
        for (i = 0; i < numberOfColumns; ++i)
        {
            colIds[i] = ((Long)columnIds.elementAt(i)).longValue();;
        }

        int []crossNdxs = NumericFunction.sortNumberOrderIndicies(colIds);

        Vector sortedColumnNames         = new Vector();
        Vector sortedColumnDataTypes     = new Vector();
        Vector sortedColumnJdbcDataTypes = new Vector();
        Vector sortedColumnSizes          = new Vector();

        for (i = 0; i < numberOfColumns; ++i)
        {
            sortedColumnNames.addElement(columnNames.elementAt(crossNdxs[i]));
            sortedColumnDataTypes.addElement(columnDataTypes.elementAt(crossNdxs[i]));
            sortedColumnJdbcDataTypes.addElement(columnJdbcDataTypes.elementAt(crossNdxs[i]));
            sortedColumnSizes.addElement(columnSizes.elementAt(crossNdxs[i]));
        }

        columnNames         = sortedColumnNames;
        columnDataTypes     = sortedColumnDataTypes;
        columnJdbcDataTypes = sortedColumnJdbcDataTypes;
        columnSizes         = sortedColumnSizes;
    }


    public static String singleQuotePadding(String s)
    {
        String newString = "";
        Character SINGLE_QUOTE = new Character('\'');

        if (s == null)
            return newString;

        for (int i=0; i<s.length(); i++)
        {
            if (SINGLE_QUOTE.charValue() == s.charAt(i))
            {
                newString = newString + s.charAt(i) + "'";
            } else {
                newString = newString + s.charAt(i);
            }
        }
        return newString;
    }

    private boolean  mapTypeToInternalType(Vector columnDataTypes, Vector columnSizes, int dataType, int dataPrecision, int dataScale)
    {
        // precision - for string type this is max num characters (we really don't care for other types)

        if (dataType == Types.NUMERIC ||
            dataType == Types.DECIMAL ||
            dataType == Types.FLOAT ||
            dataType == Types.REAL ||
            dataType == Types.DOUBLE)
        {
            columnSizes.add(new Integer(dataPrecision));

            if (dataScale == 0)
            {
              // no decimal digits - treat like integer
              columnDataTypes.add(new Integer(INTEGER));
              return true;
            }

            columnDataTypes.add(new Integer(DECIMAL));
            return true;
        }


        if (dataType == Types.BIT ||
            dataType == Types.TINYINT ||
            dataType == Types.SMALLINT ||
            dataType == Types.INTEGER ||
            dataType == Types.BIGINT)
        {
            columnSizes.add(new Integer(dataPrecision));

            columnDataTypes.add(new Integer(INTEGER));
            return true;
        }

        if (dataType == Types.DATE ||
            dataType == Types.TIME ||
            dataType == Types.TIMESTAMP ||
            dataType == 11 ||
            dataType == 9)
        {
            columnSizes.add(new Integer(dataPrecision));

            columnDataTypes.add(new Integer(TIMESTAMP));
            return true;
        }

        if (dataType == Types.CHAR ||
            dataType == Types.VARCHAR ||
            dataType == Types.LONGVARCHAR)
        {
            columnSizes.add(new Integer(dataPrecision));

            columnDataTypes.add(new Integer(STRING));
            return true;
        }

        if (dataType == Types.BINARY)
        {
            // Ignore binary types!
            //
            // One example is the encrypted password in the user data table
            // (where external authentication not used!) - we don't require this
            // in offline mode and the field is not present in the offline database.

            return false;
        }

        SysLog.debug(tableOwner + "." + tableName + ": column " + (String)columnNames.lastElement() + " has unknow JDC data type = " + dataType);

        columnSizes.add(new Integer(dataPrecision));

        columnDataTypes.add(new Integer(OTHER));

        return true;
    }

    private int getColumnNdx(Vector colNames, String colName)
    {
        int len = colNames.size();

        for (int i = 0; i < len; ++i)
        {
            if (colName.equals((String)colNames.elementAt(i)))
            {
                return i;
            }
        }

        return -1;
    }


}
