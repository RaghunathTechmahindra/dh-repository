package com.basis100.jdbcservices.jdbcexecutor;

/**
 * 26/Oct/2007 DVG #DG652 DES182595: Sebastien Lord DesJardins Express is down.(freezes)
 * 22/Apr/2008 ML FXP21161: deleting synchronized for jdbcExecutionToken , allowSimultaneousJdbcExecutions field, due to dead lockin
 */

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.util.StringUtil;
import com.basis100.jdbcservices.JdbcConnection;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.util.file.IniData;
import com.basis100.util.interfaces.Ipipe;

 /**
* <pre>
* <b>class: JdbcExecutor  </b>
*
* This class is used to execute JDBC Statements. In particular this class acts as a
* wrapper for a sub-set of the operations available from a JDBC connection object.
* The advantage provided by an object of this class (vs. using a JDBC connection
* object directly) include:
* <ol>
*  <li>  support for multiple databases systems (non-dbms support). The executor accepts
*   non-dbms specific statments created by (an instance of) class SqlStatementBuilder.
*   Internally it uses the a (provided - see constructor) expression generator (see class
*   SqlExpressionGenerator) to convert the non-dbms specific statement to a form
*   supported by the target database. Consequently, code can be written in a generic
*   form with the specifics of the actual dbms encapsulated in the provided expression
*   generator.  </li>
*
* <li>  transaction support (see startTransaction(), endTransaction()  </li>
*
*   Transaction demarcation support is modeled after the UserTransaction interface
*   in the Java Transaction API. Namely:
*
*   . begin()
*
*     Mark the begining of a transaction. Nested transactions are not supported.
*
*   . commit()
*
*     Commit the current transaction. This operation ends the current transaction.
*
*   . rollback()
*
*     Rollback the current transaction. This operation ends the current transaction.
*
*   . setRollbackOnly
*
*     Set the state of the current transaction such that the only possible outcome
*     is a rollback. Internally this method simply calls rollback().
*
* <li>  encapsulation of JDBC statements. Client user high level calls to execute statements
*   without the burden of creating JDBC statements (see execute() and executeUpdate()).  </li>
*
* <li>  encapsulation of JDBC result set handling with high level methods for retrieving
*   data same (see getData(), getVectorData()). Also wrapper methods are provided for low
*   level interface to data from result sets in a manner consistent with JDBC low level
*   calls (see getString(), getInt(), etc. </li>   </ol>
*
*
*
* Additionally this class supports a set of meta attributes that are required to
* interace sucessfully with certain database/access method (connection server)
* combinations. Included in these are:
*   <ul>
*   <li> boolean allowSimultaneousJdbcExecutions (default false)  </li>
*
*    Some DBMS's do not allow for simultaneous execution of statements (i.e. SQL Server
*    6x). This may be true even if different connections are used to contact the database.
*
*  <li> statementReuseAllowed (default true)    </li>
*
*    While reuse of a statement (for multiple statement executions) is the default
*    dehavior this may cause problems for certain connections.
*
*  <li> statementReuseMaximum (default 10)   </li>
*
*    Maximum statement reuse count (if statement reuse allowed).
*
*  <li> multipleOpenResultSetsAllowed (default true)  </li> </ul>
*
*    Some access types do not support multiple open result sets (even on the same
*    statement). If this is the case this propery must be set to false. This essentially
*    causes the executor to block an execution request until a reslt set associated
*    with a previous execution has been closed. This is the most restrictive constraint
*    to be used only if absolutely necessary due to performance implications
*    and the potential for deadlock should some thread neglect to close a result set
*    (see closeData()) after performing an execution.
*
*  <li safeDataMode (default false)  </li> </ul>
*
*    In safe (data) mode the executor will automatically close all open result sets before executing
*    a new query. In addition the sql statments associated with the open result sets are
*    reported. Safe mode is ignored if multipleOpenResultSetsAllowed is set to false.
*
*    !!!!!!!!!! Currently not implemented due to problem - problem requires more  !!!!!!!!!!
*    !!!!!!!!!! sophisticated solution than described above                       !!!!!!!!!!
*
*  <li statmentTimeOut (default 0sec)  </li> </ul>
*
*    To specify the timeout value of any sql statment in order to prevent dead lock.
*    Default  - 0 (unlimited)
*
*
* <b>Interface:</b>
*
* <dl><li>JdbcExecutor()
*
*
* <li> close()  </li>
*
*   Called when services of executor no longer required<li>
*
* <li> int execute()   </li>
*
*   Executes a SQL statement and returns a 'key' to data obtained (i<li>e<li> does not return
*   the result set directly)
*
* <li> int executeUpdate() </li>
*
*   Executes a INSERT, DELETE, or UPDATE sql statement and returns the number of rows
*   affected<li> There is no cleanup requirement following this call
*
* <li> String [][]getData(key) </li>
*
*   Returns the data obtained from the execute() invocation as a two dimensional array
*   of strings<li> The key returned by execute() is provided in the call<li>
*
* <li> getColumnCount(key)   getRowCount(key)    </li>
*
*   Get the number of columns and rows of data respectively returned in an invocation
*   of execute()<li> The key returned by execute() is provided in the call<li>
*
*  <li> next(key) </li>
*
*    This method provides access to the data returned in an invocation of execute() in
*    a sequential record manner<li> The key returned by execute() is provided in the call<li>
*    Basically it is a cover to JDBC's result set's next() method and advances the
*    data pointer to the next logical record in the result set<li>
*
*    The following methods provide access to column data in the current logical record
*    and are likewise covers to the corresponding JDBC methods:
*
*        getString(int key, int column);
*        getInt(int key, int column);
*        .... and so on
*
* <li> closeData(key)  </li>
* </dl>
*
*   A cleanup call the application should issue when no longer interested in the
*   the data obtained by execute()<li> The key returned by execute() is provided in the call<li>
*
*      Programmer Note: This call closes the result set obtained by an invocation of
*                       execute() When the connection is closed any result sets still
*                       open are automatically closed.
*

* <b>Constructor: </b>
*
* Interface:
*
* . initialize()
*
*   Provides required attributes
**/

public class JdbcExecutor
{
    static private final Log _log = LogFactory.getLog(JdbcExecutor.class);
    
    // For performance testing only -- By BILLY 19April2002
  //private boolean dbPerformanceCheck = false;
  //private long dbTime;
  //private long dbSqlCount;
  //=====================================================

  // JDBC Connection
  private JdbcConnection jCon;
    private Connection con;

    // DBMS specific expression generator
    private SqlExpressionGenerator nseg;

    // meta attributes
    //private boolean allowSimultaneousJdbcExecutions  = false; //ML FXP21161
    private boolean statementReuseAllowed            = true;
    private int     statementReuseMaximum            = 10;
    public boolean  multipleOpenResultSetsAllowed    = true;
  public boolean  safeDataMode                     = false;
  // Added statement Timeout parameter -- by BILLY 28Jan2002
  private int     queryTimeout                  = 0;  // default = unlimited

    // internal control
    // Current JDBC Statement in use
    private Statement stat;

    // current statement reuse count
    private int statementExecuteCount = 0;

    // table of statements - entry that corresponds to current statement in use
    private Vector sqlStatements = new Vector();
    private JdbcStatementEntry currentStatmentTableEntry;

    // table of open result sets/data obtained via invocations of execute().
    private Hashtable sqlResults = new Hashtable();
    private int nextResultSetKey = 0;


    // token is used as synchronization monitor if simultaneous execution disallowed.
    //private static Object jdbcExecutionToken = new Object(); //ML FXP21161

  //--> Added Timeout Sync Token to protect Thread out of sync when timeout
  //--> Test by Billy 26Feb2003
  public Object timeoutSyncToken = new Object();
  //=======================================================================

    // transaction support
    private boolean txStarted = false;

    // meta data access support
  Hashtable databaseTableInfoList = new Hashtable();
  String schemaOwner = null;

  private String id = "jExec";

  private SysLogger logger;

  // For testing Only -- By BILLY 19April2002
  //public void startDBPerformanceCheck() { this.dbPerformanceCheck = true; resetDBTime(0); resetDBCount(0);}
  //public long stopDBPerformanceCheck(){ this.dbPerformanceCheck = false; return getDBTime();}
  //public long getDBTime(){ return this.dbTime; }
  //public void resetDBTime(long value){ this.dbTime = 0; }
  //public long getDBCount(){ return this.dbSqlCount; }
  //public void resetDBCount(long value){ this.dbSqlCount = 0; }
  //=======================================================================================================


/*
 * Id set by caller for identification purposes (e.g. for a executor allocated in an executor pool
 * the id could be the executor number.
 *
 **/

  public void setId(String id)
  {
    this.id = id;
  }
  public String getId()
  {
    return id;
  }


 /*
 * Return the SysLogger instance used by this object (or null if none)
 */

 public void setLogger(SysLogger log)
 {
   this.logger = log;
 }

/*
 * Return the SysLogger instance used by this object (or null if none)
 */

 public SysLogger getLogger()
 {
    return logger;
 }

 //temp method
 public int getNumberOfOpenResultSets()
 {
   if(sqlResults == null) return 0;
   else
   return this.sqlResults.size();
 }

  //temp method
 public int getNumberOfOpenStatments()
 {
   if(sqlStatements == null) return 0;
   else
   return this.sqlStatements.size();
 }


    // Constructor(s)

    public JdbcExecutor()
    {
        logger = SysLog.getSysLogger("{JEXEC}");
    }

  public JdbcExecutor(JdbcConnection jCon, SqlExpressionGenerator nseg,
            /* boolean allowSimultaneousJdbcExecutions, //ML FXP21161 */
            boolean statementReuseAllowed,
            int     statementReuseMaximum,
            boolean multipleOpenResultSetsAllowed,
            boolean safeDataMode,
            String  schemaOwner,
            int     queryTimeout) throws Exception
  {
      logger = SysLog.getSysLogger("{JEXEC}");

      this.schemaOwner = schemaOwner;
      initialize(jCon, nseg, /* allowSimultaneousJdbcExecutions, //ML FXP21161*/
              statementReuseAllowed,
           statementReuseMaximum, multipleOpenResultSetsAllowed, safeDataMode, queryTimeout);
  }

    public JdbcExecutor(JdbcConnection jCon, SqlExpressionGenerator nseg,
                        boolean allowSimultaneousJdbcExecutions,
                        boolean statementReuseAllowed,
                        int     statementReuseMaximum,
                        boolean multipleOpenResultSetsAllowed,
            boolean safeDataMode,
            int     queryTimeout) throws Exception
    {
        logger = SysLog.getSysLogger("{JEXEC}");

          initialize(jCon, nseg, /* allowSimultaneousJdbcExecutions, //ML FXP21161 */
                statementReuseAllowed, statementReuseMaximum, multipleOpenResultSetsAllowed,
                safeDataMode, queryTimeout);
    }
    private void addToStatements(Statement stat)
    {
        currentStatmentTableEntry = new JdbcStatementEntry(this, stat, sqlStatements);
        sqlStatements.addElement(currentStatmentTableEntry);
    }

  //
  // Close Executor methods.
  //
    public void closeExecutor()
    {
      closeExecutorInternal(true);
    }

  /**
   *
   * Close executor temporarily - unlike closeExecutor() the executor may be used (in a normal fashion)
   * after this call. The method simply frees any resources (result sets and statements) currently held.
   *
   * IMPORTANT
   * ---------
   *
   * This method has no impact on the current transaction (if one) - that is, if there is an open transaction
   * it is not closed.
   *
   * Typically used before utilizing (directly) the underlying connection object associated with the
   * executor.
   *
   **/

  public void closeExecutorTemporary()
  {
      closeExecutorInternal(false);

      // ensure following members restored to original settings
      stat = null;
      statementExecuteCount = 0;
      sqlStatements = new Vector();
      currentStatmentTableEntry = null;
      sqlResults = new Hashtable();
      nextResultSetKey = 0;

      /*
      try
      {
      setAutoCommit(true);
      }
      catch (Exception e) {;}
      */
  }

    private void closeExecutorInternal(boolean closeTransaction)
    {
      if (closeTransaction == true)
      {
          // if transaction progress cancel it

          try
          {
            if (txStarted)
               rollback();
          }
          catch (Exception e) {;}
      }

      // ensure all result sets closed
      try
      {
        if (stat != null)
        {
            closeAllData();
        }
      }
      catch (Exception e) {;}

      // ensure all statements closed
      try
      {
        if (stat != null)
        {
            closeAllStatements();
        }
      }
      catch (Exception e) {;}
  }


    //
    // Transactions Support
    //
    // Notes:
    //
    // . When a transaction is started the auto commit mode is set to false. After a
    //   transaction ends (commit(), rollback(), setRollbackOnly()) the auto commit
    //   is set to true.
    //

    public void begin() throws IllegalStateException, JdbcTransactionException
    {
        if (txStarted == true)
            throw new IllegalStateException(
                logError("@begin(): transaction in progress, nested Tx not supported"));

        try
        {
            setAutoCommit(false);
            txStarted = true;

      logTrace("@begin(): transaction started");

        }
        catch (Exception e)
        {
            throw new JdbcTransactionException(
                logError("@begin(): problem encounted started transaction"));
        }

        return;
    }

  private boolean reportSqlOnResultSetClose = false;

  public void closeDataSafeMode() throws Exception
  {
    reportSqlOnResultSetClose = true;
    closeAllData();
    reportSqlOnResultSetClose = false;
  }

    private void closeAllData() throws Exception
    {
        if (sqlResults.size() <= 0)
            return;

        Enumeration entryKeys = sqlResults.keys();

        while (entryKeys.hasMoreElements() == true)
        {
        closeDataInternal(((Integer)entryKeys.nextElement()).intValue(), false);
        }

    sqlResults = new Hashtable();
    }

    private void closeAllStatements() throws Exception
    {
        if (sqlStatements.size() <= 0)
            return;

        Enumeration entrys = sqlStatements.elements();

        JdbcStatementEntry entry = null;

        while (entrys.hasMoreElements() == true)
        {
      entry = (JdbcStatementEntry)entrys.nextElement();
            try{
        entry.close(false);
      }
      catch(Exception e)
      {
        // Will get an unhandled Exception (Cancelled by User) when statement timeout (Ignored)
        logError("JdbcExecutor.closeAllStatements : Exception when closing statements (Ignored) : " + e.getMessage());
      }
        }
    sqlStatements = new Vector();
    currentStatmentTableEntry = null;
    }
    public void closeData(int key) throws Exception
    {
    closeDataInternal(key, true);
    }
    private void closeDataInternal(int key, boolean removeResult) throws Exception
    {
        Integer keyInt = new Integer(key);

        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(keyInt);

    if (entry == null)
    {
      logTrace("@closeData(): unknown key=" + key + ", assume closed already - ignored");
      return; // likely closed already!
    }

        entry.rs.close();

    if (reportSqlOnResultSetClose == true)
      logError("**** SAFEMODE: Open RS Closed: sql = " + entry.sql);

        // inform statement that resuls set closed and close the statement conditionally
    if(entry.statEntry != null)
    {
      entry.statEntry.resultSetClosed();

      entry.statEntry.closeIfNotInUseAndNoOpenResultSets();
    }

    if (removeResult == true)
          sqlResults.remove(keyInt);
    }


    public void commit() throws IllegalStateException, JdbcTransactionException
    {
        if (txStarted == false)
            throw new IllegalStateException(
                logError("commit(): no transaction in progress"));

        try
        {
            setCommit();
            setAutoCommit(true);
            txStarted = false;

      logTrace("@commit(): transaction commited");
        }
        catch (Exception e)
        {
            throw new JdbcTransactionException(
                logError("@commit(): problem encounted ending transaction"));
        }
    }
    public int execute(Object sql) throws Exception
    {
        if (multipleOpenResultSetsAllowed == true)
            return executeInternal(sql);

        // multiple open result sets not allowed - this requires a stricter
        return executeSyncInternal(sql);
    }
    // Execute a SQL statement
    public int execute(String sql) throws Exception
    {
        return execute((Object)sql);
    }
    private int executeInternal(Object sql) throws Exception
    {
        getStatement();

        String sqlStr = formSqlStatement(sql);

        logTrace(sqlStr);

        ResultSet rs = null;

    // For Performance Testing only -- By BILLY 19April2002
    //long tmpTime = 0;
    //if(this.dbPerformanceCheck)
    //  tmpTime = new java.util.Date().getTime();
    //=====================================================

        try
        {
            /* //ML FXP21161
            if (allowSimultaneousJdbcExecutions == true)
            {
            */
                rs = stat.executeQuery(sqlStr);
            /* //ML FXP21161
            }
            else
            {
                synchronized (jdbcExecutionToken)
                {
                    rs = stat.executeQuery(sqlStr);
                }
            } */
        }
        catch( Exception e )
        {
            try{
                logError(e);  
                logError("SQL = " + sqlStr);
                logError("VPD = " + jCon.getActualVPDStateForDebug());
                logError(StringUtil.stack2string(e));
            }catch(Throwable ignore){}
            
      // on exception assume worst, eg.
      //
      //  . that a result set was opened (so we attempt to close it)
      //  . that we should discontinue usage of the current statement

      try { rs.close(); } catch (Exception ec1) { ; }

      try
      {
        currentStatmentTableEntry.setNotInUse();
        currentStatmentTableEntry.close();
      }
      catch (Exception ec2) {;}

            logError("@executeInternal: exception encountered, sql=" +sqlStr
                +"::timeout(secs.):"+queryTimeout);	// #DG796
            logError(e);
            throw e;
        }

    statementExecuteCount++;

    currentStatmentTableEntry.resultSetOpened();

    // add entry to sqlResults table
    nextResultSetKey++;

    sqlResults.put(new Integer(nextResultSetKey), new JdbcResultSetEntry(rs, currentStatmentTableEntry, null, sqlStr));

    // Perform test only -- By BILLY 19April2002
    //if(this.dbPerformanceCheck)
    //{
    //  this.dbTime += new java.util.Date().getTime() - tmpTime;
    //  this.dbSqlCount ++;
    //}
    //========================================================

    // Debug by BILLY
    //logTrace("BILLY ==> SQL done !!!");

        return nextResultSetKey;
    }
    /**
    * <pre>
    * Internal <code>execute()</code> when multiple open result sets not allowed. This requires strong
    * synchronization and hence synchronization is on the entire method.
    *
    * The method waits until there are no open result sets on the current statement
    * before proceeding.
    * </pre>
    */

    private synchronized int executeSyncInternal(Object sql) throws Exception
    {
        while (currentStatmentTableEntry.isOpenResultSet() == true)
        {
            Thread.currentThread().sleep(200);
        }

        return executeInternal(sql);
    }
    
    public int executeUpdate(Object sql) throws Exception
    {
        //There is a strange problem -- May be the problem of the JDBC driver that
        // it keep looping infinitively if timeout problem occured.  Here is a temp
        // solution that to stop logging messages if it's getting into a loop.
        //  -- By BILLY 25April2002
        int loopCount = 0;
        //========================================================================

        getStatement();

        statementExecuteCount++;

        String sqlStr = formSqlStatement(sql);

        logTrace("@executeUpdate, sql = " + sqlStr);

        int updateCount = 0;
        //#DG652 StatementMonitor theMon = null;

        // For Performance Testing only -- By BILLY 19April2002
        //long tmpTime = 0;
        //if(this.dbPerformanceCheck)
        //  tmpTime = new java.util.Date().getTime();
        //====================================================

        try
        {
            /* //ML FXP21161
            if (allowSimultaneousJdbcExecutions == true)
            {
            */
                updateCount = stat.executeUpdate(sqlStr);
            /* //ML FXP21161
            }
            else
            {
                synchronized (jdbcExecutionToken)
                {
                    updateCount = stat.executeUpdate(sqlStr);
                }
            } */
        }
        catch( Exception e )
        {
            // on exception assume worst, eg.
            //
            //  . that we should discontinue usage of the current statement
            //There is a strange problem -- May be the problem of the JDBC driver that
            // it keep looping infinitively if timeout problem occured.  Here is a temp
            // solution that to stop logging messages if it's getting into a loop.
            //  -- By BILLY 25April2002

            //--> To sync the thread if timeout occured -- Otherwise may cause out of sync problem
            //--> Here should wait until the Timeout monitor finished it jobs !!
            //--> test by Billy 26Feb2003
            synchronized(timeoutSyncToken)
            {

                //ML, Apr 10, 2008: adding log trace.  
                try{
                    logError(e);  
                    logError("SQL = " + sqlStr);
                    logError("VPD = " + jCon.getActualVPDStateForDebug());
                    logError(StringUtil.stack2string(e));
                }catch(Throwable ignore){}
                
                if(loopCount < 10)
                {
                    currentStatmentTableEntry.setNotInUse();
                    // Commented it out for testing -- By BILLY 12April2002
                    currentStatmentTableEntry.close();

                    loopCount++;
                    if(loopCount >= 10)
                        logError("@executeUpdate: BILLYLOOKINGFORIT !!!");

                    logError("@executeUpdate: exception encountered:: LoopCount="
                            + loopCount + " , sql=" + sqlStr 
                            +"::timeout(secs.):"+queryTimeout);	// #DG796
                    logError(e);

                    throw e;
                }
            }
            //===================================================================================================
        }

        statementExecuteCount++;

        // Perform test only -- By BILLY 19April2002
        //if(this.dbPerformanceCheck)
        //{
        //  this.dbTime += new java.util.Date().getTime() - tmpTime;
        //  this.dbSqlCount ++;
        //}
        //========================================================

        // Debug by BILLY
        //logTrace("BILLY ==> SQL done !!!");

        return updateCount;
    }
    
    // Execute a SQL Update statement
    public int executeUpdate(String sql) throws Exception
    {
        return executeUpdate((Object)sql);
    }
    private String formSqlStatement(Object sql) throws Exception
    {
    if (sql instanceof String)
                return (String)sql;

        if (nseg == null)
        {
            logError("Invalid sql expression (non-DBMS specifc support not available)");

            throw new Exception("Invalid sql expression (String required)");
        }

        // we have a non-DBMS format object
        String sqlStr = SqlStatementBuilder.getStatementForDBMS(sql, nseg);

        return nseg.applySpecialProcessing(sqlStr);
    }

    public double getDouble(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getDouble(column);
    }
    public double getDouble(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getDouble(colName);
    }

    public Integer getInteger(int key, int column) throws Exception {

        JdbcResultSetEntry entry = (JdbcResultSetEntry) sqlResults.get(new Integer(key));

        return entry.getInteger(column);
    }
    public Integer getInteger(int key, String colName) throws Exception {

        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getInteger(colName);
    }
    public int getInt(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getInt(column);
    }
    public int getInt(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getInt(colName);
    }
    public long getLong(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getLong(column);
    }
    public long getLong(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getLong(colName);
    }
    public short getShort(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getShort(column);
    }
    public short getShort(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getShort(colName);
    }
    public java.sql.Date getSQLDate(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getSQLDate(column);
    }
    public java.sql.Date getSQLDate(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getSQLDate(colName);
    }
    public java.util.Date getDate(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

    return entry.getDate(column);
    }
    public java.util.Date getDate(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

    return entry.getDate(colName);
    }

    public InputStream getBinaryStream(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getBinaryStream(column);
    }
    public InputStream getBinaryStream(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getBinaryStream(colName);
    }
    // START EDD: AVM addition
    public Blob getBlob(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));
        
        return entry.getBlob(column);
    }
    public Blob getBlob(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));
        
        return entry.getBlob(colName);
    }
    // END EDD
    public boolean getBoolean(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getBoolean(column);
    }
    public boolean getBoolean(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getBoolean(colName);
    }

/**
 * Insert the method's description here.
 * Creation date: (26/01/00 1:25:55 PM)
 * @return java.sql.Connection
 */
  public Connection getCon()
  {
      return con;
  }

  // Catherine: AVM ----- begin -------
  public Clob getClob(int key, int column) throws Exception
  {
    JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));
    
    return entry.getClob(column);
  }
  public Clob getClob(int key, String colName) throws Exception
  {
    JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));
    
    return entry.getClob(colName);
  }
  // Catherine: AVM ----- end -------
  
    public int getColumnCount(int key) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

    return entry.getNumColumns();
    }
    public int getRowCount(int key) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

    return entry.getNumRows();
    }

    public String[][] getData(int key) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getData(true);
    }

    // version that allows translation array
    public String[][] getData(int key, int [] translationArray) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getData(translationArray);
    }
    // version that allows NULLs in table returned (normally nulls convert to empty string)
    public String[][] getData(int key, boolean allowNulls) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getData(allowNulls);
    }
    // version that additionally allows elimination of types data retrieve (default is to retrieve)
    public String[][] getData(int key, boolean allowNulls, boolean getTypedData) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getData(allowNulls, getTypedData);
    }

    public String getDate(String mask) throws Exception
    {
        SqlStatementBuilder sb = new SqlStatementBuilder();

        sb.add("Select distinct ");

        sb.add(new Nsysdate(mask));

        sb.add(" from navsys.category_detail_table"); // this table expected to exist!

        int dataKey = execute(sb.getStatement());

        String date = null;

        while(next(dataKey))
        {
            date = getString(dataKey, 1);
        }

        closeData(dataKey);

        return date;
    }

    // getStatement:
    //
    // Ensure current statment valid for use (in an execute(), executeUpdate(), etc.).
    // If not create a new statement.
    //
    // Programmer Note:
    //
    //   Conditions for creating a new statement:
    //
  //   . no current statement in use
  //
    //   . if current statement has at least one open result set a new statement is
    //     created (see documentation in class JdbcStatementEntry below for explanation).
    //
    //   . if too many statements have been issued against the current statement:
    //
    //       JDBC has a problem when re-using a statement to execute queries/updates. An
    //       exception will occur when the statement is re-used too many times.
    //
    // Result: implicit results are:
    //
    //           <stat>                      - statement to use
    //           <currentStatmentTableEntry> - corresponding entry in table of open statements

    private synchronized void getStatement() throws Exception
    {
    if (currentStatmentTableEntry == null || currentStatmentTableEntry.isInUse() == false)
    {
            // make a new statement
            newStatement();
      return;
    }

        if (statementReuseAllowed == false)
        {
            JdbcStatementEntry curr = currentStatmentTableEntry;

            if (curr != null)
            {
                // discontinue use of current statement and close if no open result sets
                // (if has open result sets will close when last of these closes)
                curr.setNotInUse();
                curr.close();

                ////  curr.closeIfNotInUseAndNoOpenResultSets();
            }

            // make a new statement
            newStatement();

            return;
        }

        if (currentStatmentTableEntry.isOpenResultSet())
        {
            // discontinue use of current statement but don't close - will close when
            // associated result set(s) close
            currentStatmentTableEntry.setNotInUse();

            // make a new statement
            newStatement();
        }
        else if (statementExecuteCount >= statementReuseMaximum)
        {
            JdbcStatementEntry curr = currentStatmentTableEntry;

            // make a new statement
            newStatement();

            // discontinue use of current statement and close if no open result sets
            // (if has open result sets will close when last of these closes)
            curr.setNotInUse();
            curr.closeIfNotInUseAndNoOpenResultSets();
        }

        // ... else current statement can be reused!

    }

  public ResultSetMetaData getResultSetMetaData(int key) throws Exception
  {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));
    return entry.getResultSetMetaData();
  }


    public String getString(int key, int column) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getString(column);
    }
    public String getString(int key, String colName) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getString(colName);
    }
    public Vector getVectorData(int key) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.getVectorData();
    }
    public void initialize(JdbcConnection jCon, SqlExpressionGenerator nseg,
                                /* boolean allowSimultaneousJdbcExecutions, //ML FXP21161 */
                                boolean statementReuseAllowed,
                                int     statementReuseMaximum,
                                boolean multipleOpenResultSetsAllowed,
                boolean safeDataMode,
                int     queryTimeout) throws Exception
    {
        this.jCon                            = jCon;
    this.con                             = jCon.getCon();
        this.nseg                            = nseg;
        //this.allowSimultaneousJdbcExecutions = allowSimultaneousJdbcExecutions; //ML FXP21161
        this.statementReuseAllowed           = statementReuseAllowed;
        this.statementReuseMaximum           = statementReuseMaximum;
        this.multipleOpenResultSetsAllowed   = multipleOpenResultSetsAllowed;
    this.safeDataMode                    = safeDataMode;
    this.queryTimeout                = queryTimeout;

        // set connection in auto-commit mode (true outside of transactions)
        setAutoCommit(true);
    }
    public boolean isInTransaction()
    {
        return txStarted;
    }
    private String logError(Exception e)
    {
        String msg = "[" + getId() + "] Exception: " + e;
        logger.error(msg);
        return msg;
    }
    private String logError(String message)
    {
        String msg = "[" + getId() + "] Error: " + message;
        logger.error(msg);
        return msg;
    }

    private String logTrace(String message)
    {
        String msg = "[" + getId() + "] " + message;
        // SEAN Jan 11, 2006: Migration to Log4j pass the class to syslogger.
        logger.jdbcTrace(JdbcExecutor.class, msg);
        _log.debug(msg);
        return msg;
    }
    private void newStatement() throws Exception
    {
        // create new statment
        stat = con.createStatement();
        stat.setQueryTimeout(queryTimeout); //#DG652

        statementExecuteCount = 0;

        addToStatements(stat);
    }
    public boolean next(int key) throws Exception
    {
        JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

        return entry.next();
    }
    // --- //

    public void rollback() throws IllegalStateException, JdbcTransactionException
    {
        if (txStarted == false)
            throw new IllegalStateException(
                logError("rollback(): no transaction in progress"));

        try
        {
            setRollback();
            setAutoCommit(true);
            txStarted = false;

      logTrace("@rollback(): transaction rolled back");
        }
        catch (Exception e)
        {
            throw new JdbcTransactionException(
                logError("@rollback(): problem encounted ending transaction"));
        }
    }
    //
    // Implementation:
    //

    private void setAutoCommit(boolean b) throws JdbcTransactionException
    {
        try
        {
            //ML FXP21161
            //synchronized (jdbcExecutionToken)
            //{
                if (b == true)
                {
                    con.setAutoCommit(true);
                }
                else
                {
                    con.setAutoCommit(false);
                }
            //}
        }
        catch (SQLException e)
        {
            logError("@setAutoCommit(" + b + "), " + e.toString());
            throw new JdbcTransactionException("error setting auto-commit mode");
        }
    }
    private void setCommit() throws Exception
    {
        try
        {
            //ML FXP21161
            //synchronized (jdbcExecutionToken)
            //{
                con.commit();
            //}
        }
        catch (Exception e)
        {
            logError("@setCommit(): " + e.toString());
            throw e;
        }
    }
    private void setRollback() throws Exception
    {
        try
        {
            //ML FXP21161
            //synchronized (jdbcExecutionToken)
            //{
                con.rollback();
            //}
        }
        catch (SQLException e)
        {
            logError("@setRollback(), " + e.toString());
            throw e;
        }
    }
    public void setRollBackOnly() throws IllegalStateException, JdbcTransactionException
    {
        try
        {
            rollback();
        }
        catch (IllegalStateException ei)
        {
            throw new IllegalStateException(logError("@setRollBackOnly(): call to rollback() failed"));
        }
        catch (JdbcTransactionException es)
        {
            throw new JdbcTransactionException(logError("@setRollBackOnly(): call to rollback() failed"));
        }

        return;
    }

    // getTableInfo:
    //
    // Retrieve/create the table information object for a specified table.
    //
    // Returns: table info object, or null if problem encountered (e.g. invalid table
    //          name, or technical problem, etc.)

    public DatabaseTableInfo getTableInfo(String tableName, IniData tableInfoDumpIni, String tableInfoDumpFileName)
    {
        return getTableInfo(tableName, schemaOwner, tableInfoDumpIni, tableInfoDumpFileName);
    }

    public DatabaseTableInfo getTableInfo(String tableName, String tableOwner, IniData tableInfoDumpIni, String tableInfoDumpFileName)
    {
        // get/create table info object for table

        tableName = tableName.trim();
        tableOwner = tableOwner.trim();

        DatabaseTableInfo tableInfo = (DatabaseTableInfo)databaseTableInfoList.get(tableName);

        if (tableInfo == null)
        {
            try
            {
                tableInfo = new DatabaseTableInfo(this, con, tableName, tableOwner, false, nseg, tableInfoDumpIni, tableInfoDumpFileName);

                databaseTableInfoList.put(tableName, tableInfo);
            }
            catch (Exception e)
            {
            	logger.error("ERROR: JdbcExecutor.getTableInfo(): could not obtain info for table <" + tableName + ">");
            	logger.error("ERROR: JdbcExecutor.getTableInfo(): error = " + e.toString());
            	logger.error(e);

                return null;
            }
        }

        if (tableInfo.getNumberOfColumns() <= 0)
        {
        	logger.error("ERROR: JdbcExecutor.getTableInfo(): could not obtain info for table <" + tableName + ">");
        	logger.error("ERROR: JdbcExecutor.getTableInfo(): number of columns returned = " + tableInfo.getNumberOfColumns());
            return null;
        }

        return tableInfo;
    }

    public String getLongAsString(int key, int column) throws Exception {
    	JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

		return entry.getLongAsString(column);
    }
    
    public String getLongAsString(int key, String colName) throws Exception {
    	JdbcResultSetEntry entry = (JdbcResultSetEntry)sqlResults.get(new Integer(key));

		return entry.getLongAsString(colName);
    }

  // ----------------------------------------------------------------------------------------------
    // Inner classes
  // ----------------------------------------------------------------------------------------------

    // JdbcStatementEntry
    //
    // Entry in table of open statements. An open statement may have multiple open result
    // sets. The statement should not be closed until all open result sets have been closed.
    //
    // PROGRAMMER NOTE:
    //
    //   Implied in the design of this class is the ability for a statement to have multiple
    //   open result sets (i.e. numOfOpenResultSets). However, it would appear that if a
    //   result set is opened after one execute command and another execute is issued
    //   the first result is closed (don't know if a execute time or when the result set
    //   for the second execute is requested but this academic). Perhaps multiple open result
    //   sets are allowed under certain conditions (i.e. execute a stored procedure that
    //   yields multiple result sets) but since the existing system does not exploit such
    //   constructs the actual implementation will insure that a new statement is used
    //   if the current statement has an open result set - see getStatement() above. This
    //   note is provided to explain the apparent contradiction in design and implementation.

    class JdbcStatementEntry
    {
        private JdbcExecutor je;
        private Statement stat;
        private Vector statementTable;

        // set to false when application no longer requires (i.e. indication that should
        // close when no more result sets)
        private boolean inUse = true;

        private int numOfOpenResultSets = 0;

        public JdbcStatementEntry(JdbcExecutor je, Statement stat, Vector statementTable)
        {
            this.je = je;
            this.stat = stat;
            this.statementTable = statementTable;
        }

        /**
        *
        * Close the statement and remove from connection's table of in use statements.
        */
    private boolean removeStatement = true;

        public void close()
        {
      try{
        setNotInUse();
        stat.close();

        if (removeStatement == true)
          statementTable.removeElement(this);
      }
      catch (Exception ec2) {
        logError("@Exception encountered when closing statement:"+ ec2.getMessage());
      }
        }
        /**
        * Version that conditionally controls removal of statement (from list) - see JdbcExecutor.closeAllStatements.
        */
    public void close(boolean removeStatement) throws Exception
    {
      this.removeStatement = removeStatement;
      close();
      this.removeStatement = true;
    }


        /**
        * Close the statement and remove from connection's table of in use statements
        * if statement no longer in use and has no open result sets.
        */

        public void closeIfNotInUseAndNoOpenResultSets() throws Exception
        {
            if (inUse || numOfOpenResultSets > 0)
                return;

            close();
        }

        public void setNotInUse()
        {
            inUse = false;
        }

    public boolean isInUse()
    {
        return inUse;
    }

        public void resultSetOpened()
        {
            numOfOpenResultSets++;
        }

        public void resultSetClosed()
        {
            numOfOpenResultSets--;
        }

        public boolean isOpenResultSet()
        {
            return (numOfOpenResultSets >= 1);
        }
    }

    // JdbcResultSetEntry
    //
    // Entry in table of open result sets. An open statement may have multiple open result
    // sets. The statement should not be closed until all open result sets have been closed.
  //

    class JdbcResultSetEntry
    {
    // row index - tracks row retrieval determined by calls to next() when operating on
    // prefetched data
    int currRowNdx = -1;

        Object translator; // TBD - translator object

        ResultSet rs;
        JdbcResultSetDataFetcher dataFetcher = null;
    boolean dataPreFetched = false;

        // entry in connection's table of open statements
        JdbcStatementEntry statEntry;

    String sql = null;

        int statementKey;

        JdbcResultSetEntry(ResultSet rs, JdbcStatementEntry statEntry, Object translator, String sql)
        {
            this.rs         = rs;
            this.statEntry  = statEntry;
            this.translator = translator;
      this.sql        = sql;
        }

    public boolean next() throws Exception
    {
      if (dataPreFetched == false)
        return rs.next();

      // data has been pre-fetched
      boolean rc = dataFetcher.next();

      currRowNdx = dataFetcher.getCurrentRowNdx();
      return rc;
    }


    public ResultSetMetaData getResultSetMetaData() throws Exception
    {
        return rs.getMetaData();
    }

    public ResultSet getResultSet() {
    	return rs;
    }

    public String getString(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return null;

        return o.toString();
      }

      return rs.getString(colNumber);
    }
    public String getString(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getString(colNumber);
      }

      return rs.getString(colName);
    }

    public InputStream getBinaryStream(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null || !(o instanceof InputStream))
          throw new Exception("getBinaryStream(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem");

        return (InputStream)o;
      }

      return rs.getBinaryStream(colNumber);
    }
    public InputStream getBinaryStream(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getBinaryStream(colNumber);
      }

      return rs.getBinaryStream(colName);
    }

    // EDD - AVM addition
    public Blob getBlob(int colNumber) throws Exception
    {
        if (dataPreFetched)
        {
            Object o = dataFetcher.getObject(currRowNdx, colNumber);
            if (o == null || !(o instanceof Blob))
                throw new Exception("getBlob(row=" + (currRowNdx + 1) + ", col=" + colNumber + ") exception - likely incompatible problem");
            
            return (Blob)o;
        }
        
        return rs.getBlob(colNumber);
    }
    
    public Blob getBlob(String colName) throws Exception
    {
        if (dataPreFetched)
        {
            int colNumber = dataFetcher.getColumnNumber(colName);
            return getBlob(colNumber);
        }
        
        return rs.getBlob(colName);
    }
    // END EDD
    
    public boolean getBoolean(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return false;
        if (!(o instanceof Boolean))
          throw new Exception("getBoolean(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem");

        return ((Boolean)o).booleanValue();
      }

      return rs.getBoolean(colNumber);
    }
    public boolean getBoolean(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getBoolean(colNumber);
      }

      return rs.getBoolean(colName);
    }

    // Catherine: AVM ----- begin -------
    public Clob getClob(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null || !(o instanceof Clob))
          throw new Exception("getClob(row=" + (currRowNdx + 1) + ", col=" + colNumber + ") exception - likely incompatible problem");
        
        return (Clob)o;
      }
      
      return rs.getClob(colNumber);
    }
    
    public Clob getClob(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getClob(colNumber);
      }
      
      return rs.getClob(colName);
    }
    // Catherine: AVM ----- end -------
    
    
    public double getDouble(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return 0.0;

        if (!(o instanceof Double))
        {
          // In certain cases the pre-fetched object may not be the correct type
          // such as:
          //
          //    . data is an integer type (object is Integer type)
          //    . column is something other than a simple field reference
          //
          // In the first case the programmer simply wishes a double representation of the
          // integer value and in essence this is fine.
          //
          // In the second case the meta data returned from the column expression may
          // simply be incorrect. For example the meta data for a column defined with
          // expression MIN(FLD) where FLD is defined as NUMBER(12, 3) has been seen to
          // indicate integer type (because the meta data incorrectly gives gives 0 for both
          // precision and scale!).
          //
          // In any event we try as a fallback to convert from the string representation obtained
          // when the data was pre-fetched. It has been observed that the string representation
          // can correctly represent floating point values (e.g. "8.45") even when the column meta data
          // incorrectly indicates integer type.
          //

          try
          {
            String s = dataFetcher.getString(currRowNdx, colNumber);
            if (s == null || (s != null && s.equals("")))
              return 0.0;
            return (new Double(s)).doubleValue();
          }
          catch (Exception e)
          {
            throw new Exception("getDouble(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem");
          }
        }

        return ((Double)o).doubleValue();
      }

      return rs.getDouble(colNumber);
    }
    public double getDouble(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getDouble(colNumber);
      }

      return rs.getDouble(colName);
    }

        /**
         * getter methods to return a Integer for a int column.
         */
        public Integer getInteger(int colNumber) throws Exception {

            Object ret;

            if (dataPreFetched) {
                ret = dataFetcher.getObject(currRowNdx, colNumber);
            } else {
                ret = rs.getObject(colNumber);
                if (ret != null) {
                    ret = new Integer(rs.getInt(colNumber));
                }
            }

            if (ret == null) {
                return null;
            }

            if (!(ret instanceof Integer))
                throw new Exception("getInt(row="+(currRowNdx+1)+", col="+
                                    colNumber+") exception - likely incompatible problem");

            return (Integer) ret;
        }
        public Integer getInteger(String colName) throws Exception {

            Object ret;

            if (dataPreFetched) {
                int colNumber = dataFetcher.getColumnNumber(colName);
                ret = getInteger(colNumber);
            } else {
                ret = rs.getObject(colName);
                if (ret != null) {
                    ret = new Integer(rs.getInt(colName));
                }
            }

            if (ret == null) {
                return null;
            }

            if (!(ret instanceof Integer))
                throw new Exception("getInt(row=" + (currRowNdx+1) + ", col=" +
                                    colName + ") exception - likely incompatible problem");

            return (Integer) ret;
        }

    public int getInt(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return 0;
        if (!(o instanceof Integer))
          throw new Exception("getInt(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem");

        return ((Integer)o).intValue();
      }

      return rs.getInt(colNumber);
    }
    public int getInt(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getInt(colNumber);
      }

      return rs.getInt(colName);
    }
    public long getLong(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return 0L;
        if (!(o instanceof Integer))
          throw new Exception("getLong(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem");

        return (long)((Integer)o).intValue();
      }

      return rs.getLong(colNumber);
    }
    public long getLong(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getLong(colNumber);
      }

      return rs.getLong(colName);
    }
    public short getShort(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return 0;
        if (!(o instanceof Integer))
          throw new Exception("getShort(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem");

        return (short)((Integer)o).intValue();
      }

      return rs.getShort(colNumber);
    }
    public short getShort(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getShort(colNumber);
      }

      return rs.getShort(colName);
    }
    public java.sql.Date getSQLDate(int colNumber) throws Exception
    {
      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return null;

        if (!(o instanceof java.sql.Date))
          throw new Exception("getSQLDate(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem");

        return (java.sql.Date)o;
      }

      return rs.getDate(colNumber);
    }
    public java.sql.Date getSQLDate(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getSQLDate(colNumber);
      }

      return rs.getDate(colName);
    }
    public java.util.Date getDate(int colNumber) throws Exception
    {
      java.sql.Timestamp ts = null;

      if (dataPreFetched)
      {
        Object o = dataFetcher.getObject(currRowNdx, colNumber);
        if (o == null)
          return null;

        //--> Bug !!
        //--> When the data get per-Fetched (i.e. getNumRows() method was called before) the Date Type Fetched
        //--> can be Date, Time or TimeStamp.  It was caused exception to throw if the DB type was not
        //--> TimeStamp.  It was working fine so far as JDBC for Java 1.1 (i.e. classes111.zip) alway convert to TimeStamp.
        //--> Problem happened when we upgraded our JDBC dirver to 1.2 and above (i.e. classes12.zip)
        //--> Solution is to force to TimeStamp type if DB tyoe is Date or Time.  Please refer to fetchData()
        //--> method for details.
        //--> Fixed by Billy 28Nov2002
        if (o instanceof java.sql.Timestamp)
          ts = (java.sql.Timestamp)o;
        else if(o instanceof java.sql.Date)
          // This will work but the Time will lost (i.e. SQL Date doesn't store time)
          ts = new java.sql.Timestamp(((java.sql.Date)o).getTime());
        else if(o instanceof java.sql.Time)
          ts = new java.sql.Timestamp(((java.sql.Time)o).getTime());
        else
          throw new Exception("getDate(row="+(currRowNdx+1)+", col="+colNumber+") exception - likely incompatible problem :: o is " + o.getClass().getName());
        //==============================================================================================
      }
      else
      {
        ts = rs.getTimestamp(colNumber);
      }

      if (ts == null)
        return null;

      return new java.util.Date(ts.getTime() + (ts.getNanos() / 1000000));
    }

    public java.util.Date getDate(String colName) throws Exception
    {
      if (dataPreFetched)
      {
        int colNumber = dataFetcher.getColumnNumber(colName);
        return getDate(colNumber);
      }

      java.sql.Timestamp ts = rs.getTimestamp(colName);

      if (ts == null)
        return null;

      return new java.util.Date(ts.getTime() + (ts.getNanos() / 1000000));
    }

    public int getNumRows() throws Exception
    {
            if (dataFetcher == null)
      {
                dataFetcher = new JdbcResultSetDataFetcher();
      }

      dataPreFetched = true;

      return dataFetcher.getNumRows(rs, allowNulls, needTypedData, translationArray);
    }


    public int getNumColumns() throws Exception
    {
            if (dataFetcher == null)
                dataFetcher = new JdbcResultSetDataFetcher();

      return dataFetcher.getNumCols(rs, allowNulls, needTypedData, translationArray);
    }

        public Vector getVectorData() throws Exception
        {
            if (dataFetcher == null)
            {
                dataFetcher = new JdbcResultSetDataFetcher();
                dataFetcher.fetchData(rs, allowNulls, needTypedData, translationArray);
            }

      dataPreFetched = true;

            return dataFetcher.getVectorData();
        }

        public String [][] getData() throws Exception
        {
            if (dataFetcher == null)
            {
                dataFetcher = new JdbcResultSetDataFetcher();
                dataFetcher.setTranslator(translator);

                dataFetcher.fetchData(rs, allowNulls, needTypedData, translationArray);
            }

      dataPreFetched = true;

            return dataFetcher.getData();
        }

        // version that allows translation array to be passed

        private int [] translationArray = null;

        public String [][] getData(int [] translationArray) throws Exception
        {
            this.translationArray = translationArray;

            String [][]r = getData();

            this.translationArray = null;

            return r;
        }

        // version that allows nulls in table (normally nulls convert to empty string)
        //
        // Note: Multiple calls to getData() are possible.  The value of <allowNulls>
        //       is significant on the first call only (inherited by subsequent calls!).
        //       Also!, if getDataStorage() is called before getData() then any
        //       subsequent call to getData([b]) will have <allowNulls> forced to 'false'.

        private boolean allowNulls = false;

        public String [][] getData(boolean allowNulls) throws Exception
        {
            this.allowNulls = allowNulls;

            String [][]r = getData();

            this.allowNulls = false;

            return r;
        }

        // version that allows nulls (as above) and also selection of types data retrieval (default is to
    // retrieve types data)

        private boolean needTypedData = true;

        public String [][] getData(boolean allowNulls, boolean getTypedData) throws Exception
        {
            this.allowNulls    = allowNulls;
      this.needTypedData = getTypedData;

            String [][]r = getData();

            this.allowNulls = false;
      this.needTypedData = true;

            return r;
        }

		public String getLongAsString(int column) throws Exception {
	    	
	    	if(rs == null)
	    		return null;
	    	
	    	String line = null;
	    	StringBuffer buffer = new StringBuffer();

	    	InputStream is = rs.getAsciiStream(column);
	    	if(is == null)
	    		return null;
	    	InputStreamReader isr = new InputStreamReader(is);
	    	logTrace("InputStreamReader.getEncoding() = "+isr.getEncoding());
	    	BufferedReader br = new BufferedReader(isr);

			while ((line = br.readLine()) != null) {
				buffer.append(line + "\n");
			}
			
			br.close();
			isr.close();
			is.close();
	    	
	    	return buffer.toString();
	    }
		
		public String getLongAsString(String colName) throws Exception {
			
			JdbcResultSetDataFetcher dataFetcher = null;
			
			if (!dataPreFetched) {
				dataFetcher = new JdbcResultSetDataFetcher();
				dataFetcher.retrieveMetaData(rs);
			}
				
			int colNumber = dataFetcher.getColumnNumber(colName);
	        return getLongAsString(colNumber);
	          
	    }

    }

    class JdbcResultSetDataFetcher
    {
        int numCols = -1;
        int numRows = -1;

    int nextRowNdx = 0;

    // meta data:
    //
    //    vColMetaData: one element per column returned in the result set
    //
    Vector vColMetaData = null;

    //  data from result set:
    //
    //    vFetchedStringData: one element per row fetched, each element String[]
    //    vFetchedTypedData:  one element per row fetched each element Object[] (element types
    //                        determined by data type as per meta data)

        Vector vFetchedStringData = null;
    Vector vFetchedTypedData  = null;

        private Object translator;

        public void setTranslator(Object translator)
        {
            this.translator = translator;
        }

    // used to emulate ResultSet.next() when application is operating on prefetched data
    public boolean next()
    {
      if (numRows == -1)
        return false;    // likely call in error since appears data not fetched!

      if (nextRowNdx < numRows)
      {
        nextRowNdx++;
        return true;
      }

      return false;
    }

    public int getCurrentRowNdx()
    {
      return nextRowNdx - 1;
    }

        // -- //

        private Ipipe pipe;

        public void setPipe(Ipipe pipe)
        {
            this.pipe = pipe;
        }

    public int getNumRows(ResultSet rs, boolean allowNulls, boolean needTypedData, int [] translationArray) throws Exception
    {
      // only way to determine number of rows is to fetch data from the result set
      if (numRows == -1)
        fetchData(rs, allowNulls, needTypedData, translationArray);

      return numRows;
    }


    public int getNumCols(ResultSet rs, boolean allowNulls, boolean needTypedData, int [] translationArray) throws Exception
    {
      retrieveMetaData(rs);
      return numCols;
    }


    public void retrieveMetaData(ResultSet rs) throws Exception
    {
      if (vColMetaData != null)
        return;

      ResultSetMetaData metaResult = rs.getMetaData();
      numCols                      = metaResult.getColumnCount();

      vColMetaData = new Vector();

      int colType = 0, colScale = 0, colPrecision = 0;

      for (int index = 1; index <= numCols; ++index)
      {
        ColumnMetaData colMD = new ColumnMetaData();
        vColMetaData.add(colMD);

        colMD.colIndex = index;
        colMD.colName = metaResult.getColumnName(index);

        colType       = metaResult.getColumnType(index);
        colPrecision  = metaResult.getPrecision(index);
        colScale      = metaResult.getScale(index);

        if (colType == Types.NUMERIC ||
            colType == Types.DECIMAL ||
            colType == Types.FLOAT ||
            colType == Types.REAL ||
            colType == Types.DOUBLE)
        {
           if (colScale == 0)
           {
              if (colPrecision > 4)
                 colType = Types.INTEGER;
              else
                 colType = Types.SMALLINT;
           }
           else
           {
              colType = Types.DOUBLE;
           }
        }

        colMD.colType       = colType;
        colMD.colPrecision  = colPrecision;
        colMD.colScale      = colScale;

        // colMD.bNull = metaAResult.isNullable(index);
      }
    }

  // Meta-data related inner classes
  public class ColumnMetaData
  {
    public int      colIndex;
    public String   colName;
    public int      colType;
    public int      colPrecision;
    public int      colScale;
    public short    colLength;
    public boolean  colNullable;
  }

    public int getColumnNumber(String colName) throws Exception
    {
      if (colName == null || colName.length() == 0)
        throw getInvalidColNameException(colName);

      Iterator iter = vColMetaData.iterator();

      while (iter.hasNext())
      {
        ColumnMetaData colMD = (ColumnMetaData)iter.next();

        if (colMD.colName.equals(colName))
          return colMD.colIndex;
      }

      throw getInvalidColNameException(colName);
    }

    private Exception getInvalidColNameException(String colName)
    {
      return new Exception("Invalid column name <" + colName + ">");
    }

        // fetchData
        //
        // Retrieve data from result set (into a vector). Return number of rows retrieved.

        // version that:
        //
        // . allows preservation of NULLs (normally converted to empty strings)
        //
        // . allows specification of a translation array (see dbTranslate in NdataStore)

        public int fetchData(ResultSet rs, boolean allowNulls, boolean needTypedData, int [] translationArray) throws Exception
        {
            vFetchedStringData = new Vector();
      vFetchedTypedData  = new Vector();

            int translationArrayLength = 0;

            if (translationArray != null && translator != null)
                translationArrayLength = translationArray.length;

            try
            {
        retrieveMetaData(rs);

                int rowNum = 0;
        int index  = 0;
                while(rs.next())
                {
          String[]  stringData = new String[numCols];
          Object[]  typedData = new Object[numCols];

                    rowNum++;
                    for (int i = 0; i < numCols; i++)
                    {
            index = i+1;

            // string representation of data ...
                        stringData[i] = rs.getString(index);

                        if (stringData[i] == null && allowNulls == false)
                            stringData[i] = "";

                        // call translator (if necessary)

                        ///// PREVIOUSLY NdataStore HELD TRANSLATION FUNCTIONS
                        /////
                        ///// THIS WOULD HAVE TO BE REPLACED BY A GENERIC TRANSLATOR INTERFACE
                        /////
                        /////
                        ///// if (i <= translationArrayLength )
                        /////    stringData[i] = ((NdataStore)translator).dbTranslation(stringData[i], translationArray[i-1]);
                        /////

            if (needTypedData == false)
              continue;

            // typed representation of data ...

            ColumnMetaData colMD = (ColumnMetaData)vColMetaData.elementAt(i);

            switch (colMD.colType)
            {
               case Types.BIT:
                  typedData[i] = (Object)(new Boolean(rs.getBoolean(index)));
                  break;
               case Types.TINYINT:
               case Types.SMALLINT:
                  //--> Bug this may cause Overflow in Oracle 9i
                  //--> By Billy 20Mar2003
                  //typedData[i] = (Object)(new Integer(rs.getShort(index)));
                  typedData[i] = (Object)(new Integer(rs.getInt(index)));
                  break;
               case Types.INTEGER:
                  typedData[i] = (Object)(new Integer(rs.getInt(index)));
                  break;
               case Types.NUMERIC:
                  if (colMD.colScale == 0)
                     typedData[i] = (Object)(new Integer(rs.getInt(index)));
                  else
                     typedData[i] = (Object)(new Double(rs.getDouble(index)));
                  break;
               case Types.FLOAT:
               case Types.REAL:
               case Types.DECIMAL:
               case Types.DOUBLE:
                  typedData[i] = (Object)(new Double(rs.getDouble(index)));
                  break;
               case Types.CHAR:
               case Types.VARCHAR:
               case Types.LONGVARCHAR:
                  typedData[i] = (Object)rs.getString(index);
                  break;
               case Types.BINARY:
               case Types.VARBINARY:
               case Types.LONGVARBINARY:
                  typedData[i] = (Object)rs.getBinaryStream(index);
                  break;

               //--> Bug !!
              //--> When the data get per-Fetched (i.e. getNumRows() method was called before) the Date Type Fetched
              //--> can be Date, Time or TimeStamp.  It was caused exception to throw if the DB type was not
              //--> TimeStamp.  It was working fine so far as JDBC for Java 1.1 (i.e. classes111.zip) alway convert to TimeStamp.
              //--> Problem happened when we upgraded our JDBC dirver to 1.2 and above (i.e. classes12.zip)
              //--> Solution is to force to TimeStamp type if DB tyoe is Date or Time.
              //--> Fixed by Billy 28Nov2002
               case Types.DATE:


               case Types.TIME:


               case Types.TIMESTAMP:
                  typedData[i] = (Object)rs.getTimestamp(index);
                  break;
              //========================================================================

               case Types.BLOB:
                  typedData[i] = (Object)rs.getBlob(index);
                  break;
               case Types.CLOB:
                  typedData[i] = (Object)rs.getClob(index);
                  break;
    //               case Types.BIGINT:
    //               case Types.DISTINCT:
               default:
               }
                    }

          // store typed data
          if (needTypedData == true)
            vFetchedTypedData.addElement(typedData);

          // store string data - but ...
                    //  if piping object provided - pipe the row and don't bother to store the
                    //  record retrieved.
                    if (pipe != null)
                    {
                        pipe.pipe(stringData);
                    }
                    else
                    {
                        // store data

                        vFetchedStringData.addElement(stringData);
                    }
                }
                numRows = rowNum;
            }
            catch (Exception err)
            {
                System.err.println(err);

                throw err;
            }

            return numRows;
        }

        // getVectorData:
        //
        // Returns data from result set in a vector. Rows of the result set correspond
        // to elements of vector. Each element is an array of strings.
        //
        // fetchData() must be called before executing this function.

        public Vector getVectorData()
        {
            return vFetchedStringData;
        }

        // getData:
        //
        // Returns data from result set in a 2-dimensional array of strings.
        //
        // fetchData() must be called before executing this function.

        public String[][] getData()
        {
            int i = 0;
            int k = 0;

            String[][] data = null;

            // check to see if 'master' table created (i.e. we return copies of the
            // master table allowing several calls to this method)
            if (vFetchedStringData != null)
            {
                data = new String[numRows][numCols];

                for (i=0; i < numRows; i++)
                {
                    String []rowData = (String[])vFetchedStringData.elementAt(i);

                    for (k=0; k < numCols; k++)
                        data[i][k] = rowData[k];
                }
            }

            return data;
        }

    public Object getObject(int rowNdx, int colNumber)
    {
      int colNdx = colNumber - 1;

      Object []row = (Object[])vFetchedTypedData.elementAt(rowNdx);

      return (Object)row[colNdx];
    }

    public String getString(int rowNdx, int colNumber)
    {
      int colNdx = colNumber - 1;

      String []row = (String[])vFetchedStringData.elementAt(rowNdx);

      return row[colNdx];
    }

    }

  // Method to get a PreparedStatement
  //   -- By Billy 26Feb2002
  public PreparedStatement getPreparedStatement(String sql)  throws Exception
  {
    logTrace("@getPreparedStatement() : the SQL = " + sql);
    return getCon().prepareStatement(sql);

  }
  // Method to execute a PreparedStatement
  //  -- caller have to setup the Statement and pass it back
  //  -- for performance test only currently
  //  -- By Billy 26Feb2002
  public int executePreparedStatement(PreparedStatement stat, String sql) throws Exception
    {
        ResultSet rs = null;

    // For Performance Testing only -- By BILLY 19April2002
    //long tmpTime = 0;
    //if(this.dbPerformanceCheck)
    //  tmpTime = new java.util.Date().getTime();
    //====================================================

        try
        {
            /* //ML FXP21161
            if (allowSimultaneousJdbcExecutions == true)
            {
            */
                rs = stat.executeQuery();
            /* //ML FXP21161
            }
            else
            {
                synchronized (jdbcExecutionToken)
                {
                    rs = stat.executeQuery();
                }
            }
            */
        }
        catch( Exception e )
        {
            try{
                logError(e);  
                logError("SQL = " + sql);
                logError("VPD = " + jCon.getActualVPDStateForDebug());
                logError(StringUtil.stack2string(e));
            }catch(Throwable ignore){}
      // on exception assume worst, eg.
      //
      //  . that a result set was opened (so we attempt to close it)
      //  . that we should discontinue usage of the current statement

      try { rs.close(); } catch (Exception ec1) { ; }

      try
      {
      //  currentStatmentTableEntry.setNotInUse();
      //  currentStatmentTableEntry.close();
          stat.close();
      }
      catch (Exception ec2) {;}

            logError("@executePreparedStatement: exception encountered : SQL = " + sql);
            logError(e);
            throw e;
        }

//logTrace("@executePreparedStatement ==> After execute !!!");
//    statementExecuteCount++;

    //currentStatmentTableEntry.resultSetOpened();
//logTrace("@executePreparedStatement ==> After resultSetOpened() !!!");
    // add entry to sqlResults table
    nextResultSetKey++;
//logTrace("@executePreparedStatement ==> Before sqlResults.put() !!!");
    sqlResults.put(new Integer(nextResultSetKey), new JdbcResultSetEntry(rs, null, null, sql));

    // Perform test only -- By BILLY 19April2002
    //if(this.dbPerformanceCheck)
    //{
    //  this.dbTime += new java.util.Date().getTime() - tmpTime;
    //  this.dbSqlCount ++;
    //}
    //========================================================

    // Debug by BILLY
    logTrace("@executePreparedStatement ==> SQL done !!!");

        return nextResultSetKey;
    }

  // Method to refresh the JDBC connection for Fault recovery -- By BILLY 05March2002
  public void refreshConnection()
  {
    try
    {
      // Connection trashed after cancel -- have to reset connection
      con = jCon.resetConnection();
      closeAllStatements();
      // reset tx flag -- All current transactions will be rollback by DataBase when disconnect
      txStarted = false;
      getStatement();
    }
    catch(Exception e)
    {
      logger.error("@JdbcExecutor.refreshConnection: Exception when Refreshing Connection : " + e.getMessage());
    }
  }

  /* #DG652 not used - delete after Jan/2009
   * 
  // Implement timeout for blocked SQL -- by BILLY 31Jan2002
  //  - I tried the Statement.setQueryTimeOut() but it's not working as expected
  //  - Currently I only used it @ Update Statements, as the readonly (Select) statement
  //    less likely to be locked and for performance concern as well
  public class StatementMonitor extends Thread
  {
    private JdbcExecutor theJExec = null;
    private Statement   statToMon;
    private Connection  conToMon;
    private boolean     keepRunning = true;
    private int         timeoutInSec = 0;
    private SysLogger   logger;

    public StatementMonitor(JdbcExecutor theJExec, int timeoutInSec)
    {
      super("StMonit");		//#DG652
      this.theJExec = theJExec;
      this.logger = theJExec.logger;
      this.statToMon = theJExec.stat;
      this.conToMon = theJExec.con;
      this.timeoutInSec = timeoutInSec;
      this.setDaemon(true);
      this.start();
    }

    public void run()
    {
//logger.debug("BILLY ===> @statementMonitor.run(): Timmer started with timeout = " + this.timeoutInSec);
      while(timeoutInSec > 0 && keepRunning == true)
      {
        try{
          // delay 1 sec
          Thread.sleep(1000);
          timeoutInSec = timeoutInSec - 1;
        }
        catch(Exception e)
        {
          // Stop timmer anyway
          keepRunning = false;
        }
      }

      // Check if timeout
      if(timeoutInSec <= 0 && keepRunning == true)
      {
        // Cancel current statement
        logger.warning("@statementMonitor: TIMEOUT !! : Statement being Cancelled !!!.");
        try{
        //logger.warning("@statementMonitor: Before Cancel()");
          //--> To sync the thread if timeout occured -- Otherwise may cause out of sync problem
          //--> test by Billy 26Feb2003
          synchronized(theJExec.timeoutSyncToken)
          {
            statToMon.cancel();
            // Connection trashed after cancel -- have to reset connection
            theJExec.con = theJExec.jCon.resetConnection();
            theJExec.closeAllStatements();
            // reset tx flag -- All current transactions will be rollback by DataBase when disconnect
            theJExec.txStarted = false;
            theJExec.getStatement();
          }
        }
        catch(Exception e)
        {
          logger.error("@statementMonitor.run(): Exception when cancelling timeout statement : " + e.getMessage());
        }
//logger.debug("BILLY ===> @statementMonitor.run(): TIMEOUT !! : After Cancel Statement. ");
      }
    }

    public void stopMonitor()
    {
      keepRunning = false;
    }
  }*/
}




