package com.basis100.jdbcservices.jdbcexecutor;

// SqlStatementBuilder
//
// The statement builder provides the facilities for creating a non-DBMS specific
// sql statement. This is represented as a vector that contains a combination of
// strings and instances of Non-DBMS specific statement support objects (see class Nnc
// for a specific example).

import java.util.*;

public class SqlStatementBuilder
{
	Vector v = new Vector();

	public void add(String s)
	{
		v.addElement(s);
	}
	// add an object that is a member of the non-DBMS support classes
	public void add(Nxx xx)
	{
		v.addElement(xx);
	}
	public Object getStatement()
	{
		return (Object)v;
	}
	// method to form the DBMS specific statement given the non-DBMS vector
	// and a expression generator - used by connection wrapper classes (i.e.
	// NConnection)

	static public String getStatementForDBMS(Object vSql, SqlExpressionGenerator nseg) throws Exception
	{
		Vector v = (Vector)vSql;
		int lim = v.size();

		StringBuffer sql = new StringBuffer();

		for (int i = 0; i < lim; ++i)
		{
			Object elem = v.elementAt(i);

			if (elem instanceof String)
				sql.append((String)elem);
			else if (elem instanceof Nnc)
				sql.append(nseg.getNumToString((Nnc)elem));
			else if (elem instanceof Nds)
				sql.append(nseg.getDateToString((Nds)elem));
			else if (elem instanceof Nsd)
				sql.append(nseg.getStringToDate((Nsd)elem));
			else if (elem instanceof Nfn)
				sql.append(nseg.getFieldName((Nfn)elem));
			else if (elem instanceof Nsysdate)
				sql.append(nseg.getSystemDate((Nsysdate)elem));
			else
				throw new NullPointerException();
		}

		return sql.toString();
	}
}
