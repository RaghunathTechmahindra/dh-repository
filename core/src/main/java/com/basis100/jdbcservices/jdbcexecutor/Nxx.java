package com.basis100.jdbcservices.jdbcexecutor;

// Nxx
//
// Base class for Non-DBMS specific statement support classes used by class
// NSqlStatementBuild.
//
// Programmer Note: Exists so that the support classes can be referred in polymorphic
//                  terms (i.e. see add(Nxx xx) in

public class Nxx
{
	int dummy;
}
