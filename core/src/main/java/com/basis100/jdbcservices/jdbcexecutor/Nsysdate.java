package com.basis100.jdbcservices.jdbcexecutor;

// Nsysdate:
//
// Non-DBMS specific statement support class used by class NSqlStatementBuild when building
// non-DBMS specific statements.
//
// This class represents a request for the current system date.
//
// Example:  let now = new Nsysdate() be an object in a non-DBMS specific
//           statement (see class NSqlStatementBuild for details)
//
//           let SQLAnywhere5x be an object of a class that implements NsqlExpressionGenerator
//           for producing SQL Anywhere 5x expressions (see interface NsqlExpressionGenerator
//           for details:
//
//           then, in the SQL Anywhere 5x specific statement now could be represented as:
//
//                        "sysdate"
//
//  When instantiated with a mask string this class represents a request for the current
//  system date in a particular format.
//
// Example: as above but let now new Nsysdate("DD MON YY HH24:MI:SS") ...
//
//          then, ...
//
//                        to_char(sysdate, "DD MON YY HH24:MI:SS");
//
// Notes:
//
// The <mask> parameter in constructor is the date format expected in Oracle 7x to_date()
// function.


public class Nsysdate extends Nxx
{
	String mask = null;

	public Nsysdate()
	{
		;
	}
	public Nsysdate(String mask)
	{
		this.mask = mask;
	}
	public String getMask()
	{
		return mask;
	}
}
