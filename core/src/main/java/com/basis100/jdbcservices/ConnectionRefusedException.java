package com.basis100.jdbcservices;

/**
* If failure happens during getting connection by exceeding the no of
* connections then instance of
* this exception class should be thrown
* @see JdbcConnectionPool
*/

public final class ConnectionRefusedException extends Exception
{
	public ConnectionRefusedException()
	{
		super();
	}
	public ConnectionRefusedException(String message)
	{
		super(message);
	}
}
