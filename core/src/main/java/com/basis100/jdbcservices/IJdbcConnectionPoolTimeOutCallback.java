package com.basis100.jdbcservices;

/**
  *  A JdbcConnectionPool object has the capability to reclaim connection that are not returned
  *  within the usage time limit. Additionally whena connection is obtained for the pool a
  *  time out call-back object may be specified. The call back object implement this interface.
  *
  **/


public interface IJdbcConnectionPoolTimeOutCallback
{
	public abstract void connectionTimeOut();
}
