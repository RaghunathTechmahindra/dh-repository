package com.basis100.jdbcservices;

public interface IJdbcConnectionPool
{
	/**
	* getConection returns the connection instance from the pool
	* if there's no available connection then check the no of connection
	* has been established. If the no of connection established is equal to
	* maximum connection should be opened they conection request will be refused
	*/
	public abstract JdbcConnection getConnection(String typeName)
			throws ConnectionRequestFailedException,
			ConnectionRefusedException;
	public abstract void releaseConnection(JdbcConnection jdbccon);
}
