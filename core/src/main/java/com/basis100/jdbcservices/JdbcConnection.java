package com.basis100.jdbcservices;

/*
 * 22/Apr/2008 ML FXP21161: deleting allowSimultaneousJdbcExecutions field, due to dead lockin
 * Feb 2, 2010, 4.4GR, making JdbcConnection configurable with Spring 
 */

// Class: JdbcConnection
//
// Server class used to manage a single connection. Instances are constructed with
// the appropriate driver and location (URL) information. A connection is not established
// upon object creation:
//
// Interface:
//
// Constructor:
//
// . JdbcConnection()
//
//   Default constructor. Setters that must be called prior to attempting a connection
//   (via getConnection()) are:
//
//   . void setDriverName(String driverName)
//   . void setDriverUrl(String driverUrl)
//
// Methods/Services
//
// . java.sql.Connection getConnection(loginId, password)
//
//   Returns a Jdbc connection object. If a connection is not currently ebstablished a new
//   connection is attempted, otherwise the current connection is returned. Throws an
//   exception if a connection attempt fails.
//
//   LoginId and password provided are available via getters:
//
//       String getLoginId();
//       String getPassword();
//
// . java.sql.Connection getConnection()
//
//   Version that will use previously used provided loginId and password if a connection
//   attempt is required. Throws an exception if a connection attempt fails.
//
// . JdbcExecutor getJdbcExecutor()
//
//   Return a JDBC executor for the connection (see JdbcExecutor.java). If an executor
//   has already been created it is returned, otherwise an executor is created and
//   returned.
//
// . void closeConnection()
//
//   Closes the connection established by getConnection() (if one). If an JDBC executor
//   has been allocated it is closed as well.
//
// . void setConnectionLost()
//
//   Called to inform that connection is no longer established (e.g client should call this
//   service to inform that the connection was [unexpectantly] lost).
//
// . booloean isConnected()
//
//   Return state of jdbc connection as follows:
//
//   false - no connection established (no connection attempted, connection closed via
//           closeConnection(), or have been informed that the connection was lost via
//           setConnectionLost())
//
//   true  - connection established
//


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import oracle.jdbc.OracleConnection;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.SqlExpressionGenerator;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.core.ExpressCoreContext;
import com.filogix.express.datasource.DataSourceConnectionFactory;
import com.filogix.express.datasource.ExpressVPDConnection;
import com.filogix.express.state.ExpressState;

/**
 * @since 3.3
 */
public class JdbcConnection
{
    // the Express state.
    private ExpressState _expressState;

    // JDBC Driver
    String driverName;

    // Driver Url
    String driverUrl;

    // JDBC Connection
    Connection con;

    // Logon On User Identification Information
    String loginId = null;
    String password = null;

    // connection established flag
    boolean connected = false;

    // jdbc executor related properties
    protected JdbcExecutor executor = null;

    //protected boolean  allowSimultaneousExecutions     = false; //ML FXP21161
    protected boolean  statementReuseAllowed           = true;
    protected boolean  multipleOpenResultSetsAllowed   = true;
    protected int      statementReuseMaximum           = 10;
    protected boolean  safeDataMode                    = false;
    protected String   jdbcExpGeneratorClass           = null;
    protected int      queryTimeout                    = 0;  // default = unlimited

    protected SysLogger logger = null;

    private String id = "jCon";
    
    //4.4GR FXP27535: Static variables needed to keep track of connections.
    private static final int MAX_ACTIVE = 9999;
	static final String jConId = "jConId";
	static int conCnt = 0;
	//End of FXP27535

    //4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- start

    private DataSourceConnectionFactory dataSourceConnectionFactory;
    
    public static JdbcConnection getInstance(ExpressState expressstate) {

        JdbcConnectionLocator locator = (JdbcConnectionLocator) 
            ExpressCoreContext.getService("jdbcConnectionLocator");

        JdbcConnection instance = locator.getJdbcConnection();
        instance.initialize(expressstate);

        return instance;
    }

    //Preventing from being called "new", Spring can call this method. 
    private JdbcConnection(){
    }

    private void initialize(ExpressState state) {
        _expressState = state;
        this.con = getDbConn();
    }
    //4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- end

    protected Connection getDbConn() {

        //4.4GR, Feb 2, 2010 -- start
        /*  
         * Deleting the following code but we can still use it,  
         * if you want to simply create ExpressConnection without JdbcConnection.
         * 
        DataSourceConnectionFactory factory =
            (DataSourceConnectionFactory) ExpressCoreContext.
            getService(ExpressCoreContext.
                DATASOURCE_CONNECTION_FACTORY_KEY);
        Connection connection =
            factory.getConnection((VPDStateDetectable) _expressState);
         */
        Connection connection =
            dataSourceConnectionFactory.getConnection(_expressState);
        //4.4GR, Feb 2, 2010 -- end
        
        //4.4GR FXP27535: Adding a serial number for each db connection.
        InvocationHandler invokh = Proxy.getInvocationHandler(connection);
      	if(invokh instanceof ExpressVPDConnection)	{
       		ExpressVPDConnection xc = (ExpressVPDConnection) invokh;
       		Connection dconn = xc.getInnermostDelegate();
       		if(dconn instanceof oracle.jdbc.OracleConnection) {
       			final OracleConnection ocon = ((oracle.jdbc.OracleConnection)dconn);
       			final Properties props = ocon.getProperties();
       			String conId = (String) props.getProperty(jConId);
       			if(conId == null)	{ //new connection. Add it to the properties table.
					conCnt = conCnt >= MAX_ACTIVE? 1: conCnt+1;
					conId = "C" + conCnt;
					props.put(jConId, conId);
       			}
       			setId(conId);
       			logger = SysLog.getSysLogger(getId());
       		}
      	}
        //End of FXP27535.

        // set the connected to true.
        this.connected = true;

        return connection;
    }

    public void closeConnection()
    {
        if (executor != null)
            closeExecutor();

        if (con == null)
            return;

        try
        {
            if (connected == true)
            {
                con.close();
            }
        }
        catch (Exception e) {;}

        connected = false;
        con       = null;
    }
    public void closeExecutor()
    {
        if (executor == null)
            return;

        executor.closeExecutor();
        executor = null;
    }

    public Connection getCon()
    {
        return con;
    }

    public Connection getConnection() throws Exception
    {
        if (connected)
            return con;

        closeExecutor(); // precaution

        con = null;

        this.loginId  = loginId;
        this.password = password;

        Properties props = new Properties();
        try {
            props.put("user",  loginId);
            props.put("password", password);

            //          driver = (Driver)Class.forName(driverName).newInstance();
            //          con    = driver.connect(driverUrl, props);

            con = getDbConn();

            con.setTransactionIsolation(con.TRANSACTION_READ_COMMITTED);
            connected = true;
        }
        catch (Exception e)
        {
            logConnectFail(e.toString(), e.getMessage());
            connected = false;

            throw e;
        }
        catch (Error x)
        {
            logConnectFail(x.toString(), x.getMessage());
            connected = false;

            throw x;
        }

        log(0, "JdbcConnection.getConnection() - Success : [driver=" + driverName + ", driverUrl=" +
            driverUrl +", loginId=" + loginId + "]");

        return con;
    }
    public Connection getConnection(String loginId, String password) throws Exception
    {
        setLoginId(loginId);
        setPassword(password);

        return getConnection();
    }

    public boolean    isJdbcExecutor()
    {
        return (executor != null);
    }

    /**
     *  Sets the logger to be the logger (hence same behavior) of the SessionResourceKit
     *  that calls this method.
     */
    public JdbcExecutor getJdbcExecutor(SessionResourceKit owner) throws Exception
    {
        JdbcExecutor ret = getJdbcExecutor();

        SysLogger logger = owner.getSysLogger();

        if (logger != null)
        {
            this.logger = logger;
            ret.setLogger(logger);
        }

        return ret;
    }

    public JdbcExecutor getJdbcExecutor() throws Exception
    {
        // check if already instantiated
        if (executor != null)
            return executor;

        // make sql expression generator
        boolean ok = true;

        SqlExpressionGenerator nseg = null;

        if (jdbcExpGeneratorClass != null)
        {
            String errorMsg     = "JdbcConnection: @getConnectionClass: error creating sql generator, class = " + jdbcExpGeneratorClass;
            String exceptionMsg = "JdbcConnection: @getConnectionClass: Exception = ";
            try
            {
                nseg = (SqlExpressionGenerator)(Class.forName(jdbcExpGeneratorClass).newInstance());
                ok = true;
            }

            catch (InstantiationException e) { ok = false; log(1, errorMsg); log(1, exceptionMsg + e + ", message = " + e.getMessage()); }
            catch (IllegalAccessException e) { ok = false; log(1, errorMsg); log(1, exceptionMsg + e + ", message = " + e.getMessage()); }
            catch (ClassNotFoundException e) { ok = false; log(1, errorMsg); log(1, exceptionMsg + e + ", message = " + e.getMessage()); }
            catch (Exception e)              { ok = false; log(1, errorMsg); log(1, exceptionMsg + e + ", message = " + e.getMessage()); }
        }

        if (!ok)
            return null;

        executor = new JdbcExecutor(this, nseg,
            /* allowSimultaneousExecutions, //ML FXP21161 */
            statementReuseAllowed,
            statementReuseMaximum,
            multipleOpenResultSetsAllowed,
            safeDataMode,
            loginId,
            queryTimeout);

        executor.setLogger(this.logger);

        executor.setId(getId());

        return executor;
    }
    public String getLoginId()
    {
        return loginId;
    }
    public String getPassword()
    {
        return password;
    }
    public boolean isConnected()
    {
        return connected;
    }
    public boolean isExecutor()
    {
        return (executor != null);
    }
    public String getExecutorId()
    {
        if (executor == null)
            return "No jExec";

        return executor.getId();
    }


    // Implementation:

    protected void logConnectFail(String error, String message)
    {
        log(0, "Connection failure (exception): + Driver = " + driverName + " Url = " + driverUrl);
        log(0, "     Error  = " + error);
        log(0, "    Message = " + message);
    }
    // Interface:

    public void setDriverName(String driverName)
    {
        this.driverName = driverName;
    }
    public void setDriverUrl(String driverUrl)
    {
        this.driverUrl = driverUrl;
    }
    public void setLoginId(String loginId)
    {
        this.loginId = loginId;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    /*
     * Id set by caller for identification purposes (e.g. for a connection allocated in a connection
     * pool the id could be the connection number).
     *
     **/

    public void setId(String id)
    {
        this.id = id;
    }
    public String getId()
    {
        return id;
    }

    /*
     * Logger passed by caller.
     *
     **/

    public void setSysLogger(SysLogger logger)
    {
        this.logger = logger;
    }

    /*
     * Log message: type: 0 informational, 1 error, 2 warning
     *
     */

    private void log(int type, String msg)
    {
        if (logger == null)
            logger = SysLog.getSysLogger(getId());

        if (type == 0)
            logger.trace(msg);
        else if(type == 1)
            logger.error(msg);
        else // Type = 2
            logger.warning(msg);
    }

    // New method for reconnect the trashed connection -- By BILLY 31Jan2002
    public Connection resetConnection() throws Exception
    {
        if (connected)
            try{
                // Fixed to rollback the Tx before Disconnect, otherwise, all changes will Automatically Commited (??) -- 30May2002
                con.rollback();
                con.close();
            }
        catch(Exception e)
        {
            // Will get a unhandled Exception (Cancelled by User) when statement timeout (Ignored)
            log(2, "JdbcConnection.resetConnection: Exception when closing the connection (ignored): " + e.getMessage());
        }

        // will attempt connection ...
        con = null;

        this.loginId  = loginId;
        this.password = password;

        Properties props = new Properties();
        try {
            props.put("user",  loginId);
            props.put("password", password);

            con = getDbConn();
            con.setTransactionIsolation(con.TRANSACTION_READ_COMMITTED);

            connected = true;
        }
        catch (Exception e)
        {
            logConnectFail("JdbcConnection.resetConnection:" + e.toString(), e.getMessage());
            connected = false;

            throw e;
        }
        catch (Error x)
        {
            logConnectFail(x.toString(), x.getMessage());
            connected = false;

            throw x;
        }

        log(2, "JdbcConnection.resetConnection() - Success : [driver=" + driverName + ", driverUrl=" +
            driverUrl +", loginId=" + loginId + "]");

        return con;
    }

    public String getActualVPDStateForDebug() {
        if (con == null)
            return "connection is null";
        if (isConnected() == false)
            return "not connected to DB";

        String result = "";
        Statement stat = null;
        ResultSet rs = null;
        try {
            String sql = "select sys_context('vpd_app_context', "
                + "'institution_profile_id_list') from dual";
            stat = con.createStatement();
            rs = stat.executeQuery(sql);
            if (rs.next()) {
                result = rs.getString(1);
            } else {
                result = "VPD is not set";
            }
        } catch (Throwable th) {
            result = "error occurs but this is just for debug, never mind. "
                + th.getMessage();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stat != null)
                    stat.close();
            } catch (Throwable ignore) {
            }
        }
        return result;
    }

    //4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- start
    public void setStatementReuseAllowed(boolean statementReuseAllowed) {
        this.statementReuseAllowed = statementReuseAllowed;
    }

    public void setMultipleOpenResultSetsAllowed(
            boolean multipleOpenResultSetsAllowed) {
        this.multipleOpenResultSetsAllowed = multipleOpenResultSetsAllowed;
    }

    public void setStatementReuseMaximum(int statementReuseMaximum) {
        this.statementReuseMaximum = statementReuseMaximum;
    }

    public void setSafeDataMode(boolean safeDataMode) {
        this.safeDataMode = safeDataMode;
    }

    public void setJdbcExpGeneratorClass(String jdbcExpGeneratorClass) {
        this.jdbcExpGeneratorClass = jdbcExpGeneratorClass;
    }

    public void setQueryTimeout(int queryTimeout) {
        this.queryTimeout = queryTimeout;
    }

    public void setDataSourceConnectionFactory(
            DataSourceConnectionFactory dataSourceConnectionFactory) {
        this.dataSourceConnectionFactory = dataSourceConnectionFactory;
    }
    //4.4GR, Feb 2, 2010, making JdbcConnection configurable with Spring -- start

}
