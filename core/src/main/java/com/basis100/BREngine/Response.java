/*
 * Response.java 
 * 
 * Created: 20-FEB-2006
 * Author:  Edward Steel
 */
package com.basis100.BREngine;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author ESteel
 */
public class Response extends DealEntity {
	
	protected int    responseId;
	
	protected Date   responseDate;
	protected Date   statusDate;
	protected String statusMessage;
	protected String ChannelTransactionKey;
	
	protected int    requestId;
	protected int    dealId;
	protected int    copyId;
	protected int    responseStatusId;

	/**
	 * Creates a new entity
	 * @param interpretor
	 * @throws SQLException
	 */
	public Response(BRInterpretor interpretor) throws SQLException {
		super(interpretor);
	}

	/**
	 * Clone this Response
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */
	public Response deepCopy() throws CloneNotSupportedException {
		return (Response) this.clone();		
	}
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
	protected String getPrimaryKeyName() {
		return "responseId";
	}
	
	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
	protected String getDefaultSQL() {
		return "SELECT * FROM RESPONSE";
	}
	
	/**
	 * Closes the DB connection.
	 * @throws SQLException If an error occurs.
	 */
	protected void close() throws SQLException {
		super.close();
	}
	
	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
	protected boolean next() throws SQLException { 
		boolean bStatus = super.next();
		
		if (bStatus) {
      responseId = resultSet.getInt("RESPONSEID");
      responseDate = resultSet.getDate("RESPONSEDATE");
      statusDate = resultSet.getDate("STATUSDATE");
      statusMessage = resultSet.getString("STATUSMESSAGE");
      ChannelTransactionKey = resultSet.getString("CHANNELTRANSACTIONKEY");
      requestId = resultSet.getInt("REQUESTID");
      dealId = resultSet.getInt("DEALID");
      copyId = resultSet.getInt("COPYID");
      responseStatusId = resultSet.getInt("RESPONSESTATUSID");
		}
		
		return bStatus;
	}

//----- START getters/ setters -------------------------------------------------	
	/**
	 * @return Returns the channelTransactionKey.
	 */
	public String getChannelTransactionKey() {
		return ChannelTransactionKey;
	}

	/**
	 * @return Returns the copyId.
	 */
	public int getCopyId() {
		return copyId;
	}

	/**
	 * @return Returns the dealId.
	 */
	public int getDealId() {
		return dealId;
	}

	/**
	 * @return Returns the requestId.
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @return Returns the responseDate.
	 */
	public Date getResponseDate() {
		return responseDate;
	}

	/**
	 * @return Returns the responseId.
	 */
	public int getResponseId() {
		return responseId;
	}

	/**
	 * @return Returns the responseStatusId.
	 */
	public int getResponseStatusId() {
		return responseStatusId;
	}

	/**
	 * @return Returns the statusDate.
	 */
	public Date getStatusDate() {
		return statusDate;
	}

	/**
	 * @return Returns the statusMessage.
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param channelTransactionKey The channelTransactionKey to set.
	 */
	public void setChannelTransactionKey(String channelTransactionKey) {
		ChannelTransactionKey = channelTransactionKey;
	}

	/**
	 * @param copyId The copyId to set.
	 */
	public void setCopyId(int copyId) {
		this.copyId = copyId;
	}

	/**
	 * @param dealId The dealId to set.
	 */
	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	/**
	 * @param requestId The requestId to set.
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * @param responseDate The responseDate to set.
	 */
	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	/**
	 * @param responseId The responseId to set.
	 */
	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}

	/**
	 * @param responseStatusId The responseStatusId to set.
	 */
	public void setResponseStatusId(int responseStatusId) {
		this.responseStatusId = responseStatusId;
	}

	/**
	 * @param statusDate The statusDate to set.
	 */
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	/**
	 * @param statusMessage The statusMessage to set.
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}		
  

  
//------ END getters/ setters --------------------------------------------------
}
