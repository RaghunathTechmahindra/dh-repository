// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Contact extends DealEntity {
	protected  	int 	contactId ;
	protected  	int 	copyId ;
	protected  	String 	contactFirstName ;
	protected  	String 	contactMiddleInitial ;
	protected  	String 	contactLastName ;
	protected  	String 	contactPhoneNumber ;
	protected  	String 	contactFaxNumber ;
	protected  	String 	contactEmailAddress ;
	protected  	int 	addrId ;
	protected  	int 	salutationId ;
	protected  	String 	contactJobTitle ;
	protected  	String 	contactPhoneNumberExtension ;

  public Address  addr;

   protected String getPrimaryKeyName() {
      return "contactId";
   }

   public Contact(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public Contact(BRInterpretor interpretor, String filter)
              throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }
   public Contact deepCopy() throws CloneNotSupportedException {
      return (Contact)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from Contact";
   }

   protected void close() throws SQLException {
      if (addr != null)
         addr.close();

      super.close();
   }
   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				contactId  = resultSet.getInt("CONTACTID");
				copyId  = resultSet.getInt("COPYID");
				contactFirstName  = getString(resultSet.getString("CONTACTFIRSTNAME"));
				if (contactFirstName != null )
					contactFirstName  =  contactFirstName.trim();
				contactMiddleInitial  = getString(resultSet.getString("CONTACTMIDDLEINITIAL"));
				if (contactMiddleInitial != null )
					contactMiddleInitial  =  contactMiddleInitial.trim();
				contactLastName  = getString(resultSet.getString("CONTACTLASTNAME"));
				if (contactLastName != null )
					contactLastName  =  contactLastName.trim();
				contactPhoneNumber  = getString(resultSet.getString("CONTACTPHONENUMBER"));
				if (contactPhoneNumber != null )
					contactPhoneNumber  =  contactPhoneNumber.trim();
				contactFaxNumber  = getString(resultSet.getString("CONTACTFAXNUMBER"));
				if (contactFaxNumber != null )
					contactFaxNumber  =  contactFaxNumber.trim();
				contactEmailAddress  = getString(resultSet.getString("CONTACTEMAILADDRESS"));
				if (contactEmailAddress != null )
					contactEmailAddress  =  contactEmailAddress.trim();
				addrId  = resultSet.getInt("ADDRID");
				salutationId  = resultSet.getInt("SALUTATIONID");
				contactJobTitle  = getString(resultSet.getString("CONTACTJOBTITLE"));
				if (contactJobTitle != null )
					contactJobTitle  =  contactJobTitle.trim();
				contactPhoneNumberExtension  = getString(resultSet.getString("CONTACTPHONENUMBEREXTENSION"));
				if (contactPhoneNumberExtension != null )
					contactPhoneNumberExtension  =  contactPhoneNumberExtension.trim();

        // fetch addr record
        if (addr != null)
           addr.close();
        addr = new Address(interpretor, addrId, copyId);
        addr.close();
        
/*==================================================
         contactId               = resultSet.getInt(1);
         contactFirstName        = getString(resultSet.getString(2);
         if (contactFirstName != null)
            contactFirstName = contactFirstName.trim();

         contactMiddleInitial        = getString(resultSet.getString(3);
         if (contactMiddleInitial != null)
            contactMiddleInitial = contactMiddleInitial.trim();

         contactLastName        = getString(resultSet.getString(4);
         if (contactLastName != null)
            contactLastName = contactLastName.trim();

         contactPhoneNumber        = getString(resultSet.getString(5);
         if (contactPhoneNumber != null)
            contactPhoneNumber = contactPhoneNumber.trim();

         contactFaxNumber        = getString(resultSet.getString(6);
         if (contactFaxNumber != null)
            contactFaxNumber = contactFaxNumber.trim();

         contactEmailAddress        = getString(resultSet.getString(7);
         if (contactEmailAddress != null)
            contactEmailAddress = contactEmailAddress.trim();

         addrId                  = resultSet.getInt(8);
         salutationId          = resultSet.getShort(9);
         jobTitle             = getString(resultSet.getString(10);
         if (jobTitle != null)
            jobTitle = jobTitle.trim();
==============================*/
         }

      return bStatus;
   }
}
