// Business Rule Expression Scanner, add array
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class IfNode implements Cloneable {
   private      BRInterpretor interpretor;
//   boolean      bLeftParenthesis, bRightParenthesis;
   Object       expressionNode = null;
   DoNode       thenBlock = null;      // statement object list
   DoNode       elseBlock = null;      // Statement object list

   protected String   errorMsg;

   public IfNode(BRInterpretor interpretor) {
      this.interpretor = interpretor;
      thenBlock = null;
      elseBlock = null;
   }

   protected IfNode deepCopy() throws ParseException {
      IfNode     ifNode;

      try {
         ifNode = (IfNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return ifNode;
   }

   public void display() {
   }

   protected Object evaluate() throws ParseException, SQLException {
      Class       classObj;
      Object      result;
      BRExpNode   expNode = new BRExpNode(interpretor);
      BRFunctNode functNode = new BRFunctNode(interpretor);
      BRVarNode   varNode   = new BRVarNode(interpretor);

      substituteUserVars();

      if (expNode == null) {
         errorMsg = "Invalid if expression: null";
         throw new ParseException(errorMsg, 0);
         }

      classObj = expressionNode.getClass();
      if (classObj.isInstance(expNode)) {
         result = ((BRExpNode)expressionNode).evaluate();
         }
      else if (classObj.isInstance(functNode)) {
         result = ((BRFunctNode)expressionNode).evaluate();
         }
      else if (classObj.isInstance(varNode)) {
         result = ((BRVarNode)expressionNode).evaluate();
         }
      else
         result = new Boolean(true);

      if (((Boolean)result).booleanValue()) {       // evaluate then block if true
         if (thenBlock == null) return null;
         return thenBlock.evaluate();
         }
      else {                          // evaluate else block if false
         if (elseBlock == null) return null;
         return elseBlock.evaluate();
         }
   }

   protected Object evaluate(Object valueObj) throws SQLException {

      return null;
   }

   protected boolean parseExpressionPhase2() throws ParseException {
      Class     classObj;
      BRExpNode     expNode   = new BRExpNode(interpretor);

      classObj = expressionNode.getClass();
      if (classObj.isInstance(expNode)) {
         if (!((BRExpNode)expressionNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + expressionNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }

      if (thenBlock != null) {
         classObj = thenBlock.getClass();
         if (!((DoNode)thenBlock).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + thenBlock + ">";
            throw new ParseException(errorMsg, 0);
            }
         }

      if (elseBlock != null) {
         classObj = elseBlock.getClass();
         if (!((DoNode)elseBlock).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + elseBlock + ">";
            throw new ParseException(errorMsg, 0);
            }
         }

      return true;
   }

   protected void substituteUserVars() throws ParseException {
      Class   classObj;
      String        strObj = "";
      BRExpNode     expNode = new BRExpNode(interpretor);
      BRFunctNode   functNode = new BRFunctNode(interpretor);

      classObj = expressionNode.getClass();
      if (classObj.isInstance(expNode)) {
         ((BRExpNode)expressionNode).substituteUserVars();
      }
      else if (classObj.isInstance(functNode)) {
         ((BRFunctNode)expressionNode).substituteUserVars();
      }
      else if (classObj.isInstance(strObj)) {
         strObj = (String)expressionNode;
         expressionNode = (String)interpretor.substituteUserVars(strObj,
                              BRConstants.STR_DELIMITER);
      }

      if (thenBlock != null) {
         thenBlock.substituteUserVars();
      }

      if (elseBlock != null) {
         elseBlock.substituteUserVars();
      }
   }
}
