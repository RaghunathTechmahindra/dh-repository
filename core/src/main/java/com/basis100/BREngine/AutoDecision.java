/*
 * Created on Nov 16, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.BREngine;

import java.sql.SQLException;


/**
 * @author MMorris
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class AutoDecision extends DealEntity {
	
	private int dealId;
	protected int status;
	protected int attemptNum;	

	//Static variables used throughout the class
	private static final String STATUS = "status";
	private static final String DEAL_ID = "DealId";
	private static final String ATTEMPT_NUM = "attemptNum";
	private static final String SELECT_CLAUSE = "Select * from AutoDecision ";

	/**
	 * AutoDecision Contructor
	 * 
	 * @param interpretor
	 *            BRInterpretor
	 * @param whereClause
	 *            String
	 * @throws SQLException
	 */
	public AutoDecision(BRInterpretor interpretor, String whereClause)
			throws SQLException {
		super(interpretor);
		filter = whereClause;	
		
		if (Open()) {
			while (next()) {
				;
			}
			close();
		}
	}

	

	public boolean next() throws SQLException {

		boolean bStatus = super.next();	

		if (bStatus) {
			this.dealId = resultSet.getInt(DEAL_ID);			
			this.status = resultSet.getInt(STATUS);
			this.attemptNum = resultSet.getInt(ATTEMPT_NUM);			
		}
		return bStatus;
	}

	public String getDefaultSQL() {

		return SELECT_CLAUSE;
	}

	protected AutoDecision deepCopy() throws CloneNotSupportedException {
		return (AutoDecision) this.clone();
	}

	public String toString() {

		return dealId + " / " + status + " / " + attemptNum;
	}

	
	/**
	 * @return Returns the attemptNum.
	 */
	public int getAttemptNum() {
		return attemptNum;
	}

	/**
	 * @return Returns the status.
	 */
	public int getStatus() {
		return status;
	}

	public int getDealId() {
		return dealId;
	}
	
	

}