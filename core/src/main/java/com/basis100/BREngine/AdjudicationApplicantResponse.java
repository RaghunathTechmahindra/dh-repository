/* NBC-Implementation-Team  */
package com.basis100.BREngine;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.SQLException;



/**
 * <p>
 * Title: AdjudicationApplicantResponse
 * </p>
 * 
 * <p>
 * Description: BREntity class for Adjudication Applicant Response
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC Implementation Team
 * @version 1
 * 
 */

public class AdjudicationApplicantResponse extends DealEntity {
	
	protected int responseId;	
	protected int applicantNumber;	
	protected int copyId;	
	protected int borrowerId;	
	protected String applicantCurrentInd;	
	protected double riskRating;	
	protected int internalScore;	
	protected int creditBureauScore;	
	protected String creditBureauReport;	
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
    protected String getPrimaryKeyName() {
      return "responseId";
    }

	/**
	 * Creates a new BREntity
	 * @param interpretor
	 * @param template
	 * @throws SQLException
	 */
   public AdjudicationApplicantResponse(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   /**
	 * @param interpretor
	 * @throws SQLException
	 */
   public AdjudicationApplicantResponse(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   /**
	 * @param BRInterpretor
	 * @param filter
	 * @throws SQLException
	 */
   public AdjudicationApplicantResponse(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

	/**
	 * Clone this AdjudicationApplicantResponse
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */ 
   protected AdjudicationApplicantResponse deepCopy() throws CloneNotSupportedException {
      return (AdjudicationApplicantResponse)clone();
   }

	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
   protected String getDefaultSQL() {
      return "select * from AdjudicationApplicantResponse";
   }

	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
    	        responseId  = resultSet.getInt("RESPONSEID");
    	        applicantNumber  = resultSet.getInt("APPLICANTNUMBER");
				copyId  = resultSet.getInt("COPYID");
    	        borrowerId  = resultSet.getInt("BORROWERID");
    	        applicantCurrentInd  = resultSet.getString("APPLICANTCURRENTIND");
				riskRating = resultSet.getDouble("RISKRATING");
				internalScore = resultSet.getInt("INTERNALSCORE");
				creditBureauScore = resultSet.getInt("CREDITBUREAUSCORE");
				creditBureauReport = resultSet.getString("CREDITBUREAUREPORT");

        }

      return bStatus;
   }
}
