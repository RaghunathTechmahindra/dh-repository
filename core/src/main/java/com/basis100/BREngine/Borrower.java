// dynamic bind instance variables by its name
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Borrower extends DealEntity
{
  protected int borrowerId;
  protected int copyId;
  protected int dealId;
  protected int borrowerNumber;
  protected String borrowerFirstName;
  protected String borrowerMiddleInitial;
  protected String borrowerLastName;
  protected String firstTimeBuyer;
  protected Date borrowerBirthDate;
  protected int nsfOccurenceTypeId;

  //--DJ_LDI_CR--start--//
  // Now it is an integer referencing to the BorrowerGender table.
  //protected  	String 	borrowerGender ;
  protected int borrowerGenderId;

  //--DJ_LDI_CR--end--//
  protected int salutationId;
  protected int maritalStatusId;
  protected String socialInsuranceNumber;
  protected String borrowerHomePhoneNumber;
  protected String borrowerWorkPhoneNumber;
  protected String borrowerFaxNumber;
  protected String borrowerEmailAddress;
  protected String clientReferenceNumber;
  protected int numberOfDependents;
  protected int numberOfTimesBankrupt;
  protected int raceId;
  protected int creditScore;
  protected double GDS;
  protected double TDS;
  protected int educationLevelId;
  protected double netWorth;
  protected String existingClient;
  protected String creditSummary;
  protected int languagePreferenceId;
  protected int borrowerGeneralStatusId;
  protected int borrowerTypeId;
  protected int citizenshipTypeId;
  protected String primaryBorrowerFlag;
  protected double totalAssetAmount;
  protected double totalLiabilityAmount;
  protected int paymentHistoryTypeId;
  protected double totalIncomeAmount;
  protected Date dateOfCreditBureauProfile;
  protected Date creditBureauOnFileDate;
  protected String bureauAttachment;
  protected String existingClientComments;
  protected int bankruptcyStatusId;
  protected int age;
  protected String staffOfLender;
  protected String staffType;
  protected double totalLiabilityPayments;
  protected double GDS3Year;
  protected double TDS3Year;
  protected String SINCheckDigit;
  protected int creditBureauNameId;
  protected String borrowerWorkPhoneExtension;

  // Additional field -- Billy 09Jan2002
  protected String creditBureauSummary;

  //--DJ_LI_CR--start//
  protected int insuranceProportionsId;
  protected double lifePercentCoverageReq;
  protected int disabilityStatusId;
  protected int lifeStatusId;
  protected int smokeStatusId;
  protected double disPercentCoverageReq;

  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  protected String guarantorOtherLoans;

  //--DJ_CR201.2--end//

  protected int nEmploymentHistory;
  protected EmploymentHistory[] employmentHistory;
  protected int nBorrowerAddress;
  protected BorrowerAddress[] borrowerAddress;
  protected int nIncome;
  protected Income[] income;
  protected int nLiability;
  protected Liability[] liability;
  protected int nAsset;
  protected Asset[] asset;
  protected int nCreditReference;
  protected CreditReference[] creditReference;

  //--DJ_LDI_CR--start--//
  protected int nLifeDisabilityPremiums;
  protected LifeDisabilityPremiums[] lifeDisabilityPremiums;

  //--DJ_LDI_CR--end--//

  
  //--NBC-Implementaion - Start******************//

  protected int solicitationId;
  protected String employeeNumber;
  protected int prefContactMethodId;
  protected BorrowerIdentification[] borrowerIdentification;
  protected int nBorrowerIdentification;
 
  
  //--NBC-Implementation - End******************//

  //FFATE
  protected int suffixId;
  protected String borrowerCellPhoneNumber;
  protected Date cbAuthorizationDate;
  protected String cbAuthorizationMethod;
  
  
  protected String getPrimaryKeyName()
  {
    return "borrowerId";
  }

  public Borrower(BRInterpretor interpretor, boolean bTemplate) throws SQLException
  {
    super(interpretor);

    nEmploymentHistory = 0;
    employmentHistory = new EmploymentHistory[0];

    nBorrowerAddress = 0;
    borrowerAddress = new BorrowerAddress[0];

    nIncome = 0;
    income = new Income[0];

    nLiability = 0;
    liability = new Liability[0];

    nAsset = 0;
    asset = new Asset[0];

    nCreditReference = 0;
    creditReference = new CreditReference[0];

    //--DJ_LDI_CR--start--//
    nLifeDisabilityPremiums = 0;
    lifeDisabilityPremiums = new LifeDisabilityPremiums[0];
    //--DJ_LDI_CR--end--//

  }

  public Borrower(BRInterpretor interpretor) throws SQLException
  {
    super(interpretor);
    filter = null;
    if(Open())
    {
      next();
    }
  }

  public Borrower(BRInterpretor interpretor, String filter) throws SQLException
  {
    super(interpretor);
    this.filter = filter;

    if(Open())
    {
      next();
    }
  }

  public Borrower deepCopy() throws CloneNotSupportedException
  {
    return(Borrower)this.clone();
  }

  private EmploymentHistory[] FetchEmploymentHistories() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    EmploymentHistory employmentHistory = null;
    EmploymentHistory[] employmentHistories = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId =" + copyId;
      employmentHistory = new EmploymentHistory(interpretor, whereClause);
      if(employmentHistory.borrowerId != borrowerId)
      {
        employmentHistory.close();
        employmentHistory = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchEmploymentHistories: " + e);
      //==========================================

      throw e;
    }

    if(employmentHistory != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(employmentHistory.deepCopy());

      while(employmentHistory.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(employmentHistory.deepCopy());
      }
      employmentHistory.close();
    }

    nEmploymentHistory = aStack.size();
    employmentHistories = new EmploymentHistory[nEmploymentHistory];

    for(i = 0; i < nEmploymentHistory; ++i)
    {
      employmentHistories[i] = (EmploymentHistory)aStack.get(i);
    }

    return employmentHistories;
  }

  private BorrowerAddress[] FetchBorrowerAddresses() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    BorrowerAddress borrowerAddress = null;
    BorrowerAddress[] borrowerAddresses = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId = " + copyId;
      borrowerAddress = new BorrowerAddress(interpretor, whereClause);
      if(borrowerAddress.borrowerId != borrowerId)
      {
        borrowerAddress.close();
        borrowerAddress = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchBorrowerAddresses: " + e);
      //==========================================

      throw e;
    }

    if(borrowerAddress != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(borrowerAddress.deepCopy());

      while(borrowerAddress.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(borrowerAddress.deepCopy());
      }
      borrowerAddress.close();
    }

    nBorrowerAddress = aStack.size();
    borrowerAddresses = new BorrowerAddress[nBorrowerAddress];

    for(i = 0; i < nBorrowerAddress; ++i)
    {
      borrowerAddresses[i] = (BorrowerAddress)aStack.get(i);
    }

    return borrowerAddresses;
  }

  protected String getDefaultSQL()
  {
    return "select * from Borrower";
  }

  public Object fetchField(String fieldName) throws SQLException, CloneNotSupportedException
  {

    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.debug("@Borrower.fetchField Instantiating object: " + fieldName);
    //==================================================

    if(fieldName.compareToIgnoreCase("BorrowerAddress") == 0)
    {
      borrowerAddress = FetchBorrowerAddresses();
      return(Object)borrowerAddress;
    }
    else if(fieldName.compareToIgnoreCase("EmploymentHistory") == 0)
    {
      employmentHistory = FetchEmploymentHistories();
      return(Object)employmentHistory;
    }
    else if(fieldName.compareToIgnoreCase("Income") == 0)
    {
      income = FetchIncomes();
      return(Object)income;
    }
    else if(fieldName.compareToIgnoreCase("Liability") == 0)
    {
      liability = FetchLiabilities();
      return(Object)liability;
    }
    else if(fieldName.compareToIgnoreCase("Asset") == 0)
    {
      asset = FetchAssets();
      return(Object)asset;
    }
    else if(fieldName.compareToIgnoreCase("CreditReference") == 0)
    {
      creditReference = FetchCreditReferences();
      return(Object)creditReference;
    }
    //--DJ_LDI_CR--start--//
    else if(fieldName.compareToIgnoreCase("LifeDisabilityPremiums") == 0)
    {
      lifeDisabilityPremiums = FetchLifeDisabilityPremiums();
      return(Object)lifeDisabilityPremiums;
    }
    //--NBC-Implementation Start--//
    
    else if(fieldName.compareToIgnoreCase("BorrowerIdentification") == 0)
    {
    	borrowerIdentification = FetchBorrowerIdentifications();
    	return(Object)borrowerIdentification;
    }
    //--NBC-Implementation End--//

    //--DJ_LDI_CR--end--//
    /*
           else if (fieldName.compareToIgnoreCase("nsfOccurenceType") == 0) {
       // fetch NsfOccurenceType
       nsfOccurenceType = new NSFOccurenceType(interpretor, nsfOccurenceTypeId);
       nsfOccurenceType.close();
       return (Object)nsfOccurenceType;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("Salutation") == 0) {
       // fetch Salutation
       salutation = new Salutation(interpretor, salutationId);
       salutation.close();
       return (Object)salutation;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("MaritalStatus") == 0) {
       // fetch MaritalStatus
       maritalStatus = new MaritalStatus(interpretor, maritalStatusId);
       maritalStatus.close();
       return (Object)maritalStatus;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("Race") == 0) {
       // fetch Race
       race = new Race(interpretor, raceId);
       race.close();
       return (Object)race;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("EducationLevel") == 0) {
       // fetch EducationLevel
       educationLevel = new EducationLevel(interpretor, educationLevelId);
       educationLevel.close();
       return (Object)educationLevel;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("LanguagePreference") == 0) {
       // fetch LanguagePreference
       languagePreference = new LanguagePreference(interpretor, languagePreferenceId);
       languagePreference.close();
       return (Object)languagePreference;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("BorrowerGeneralStatus") == 0) {
       // fetch BorrowerGeneralStatus
       borrowerGeneralStatus = new BorrowerGeneralStatus(interpretor, borrowerGeneralStatusId);
       borrowerGeneralStatus.close();
       return (Object)borrowerGeneralStatus;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("BorrowerType") == 0) {
       // fetch BorrowerType
       borrowerType = new BorrowerType(interpretor, borrowerTypeId);
       borrowerType.close();
       return (Object)borrowerType;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("CitizenshipType") == 0) {
       // fetch CitizenshipType
       citizenshipType = new CitizenshipType(interpretor, citizenshipTypeId);
       citizenshipType.close();
       return (Object)citizenshipType;
       }
     */
    /*
           else if (fieldName.compareToIgnoreCase("PaymentHistoryType") == 0) {
       // fetch PaymentHistoryType
       paymentHistoryType = new PaymentHistoryType(interpretor, paymentHistoryTypeId);
       paymentHistoryType.close();
       return (Object)paymentHistoryType;
       }
     */
    else
    {
      return null;
    }
  }

  private Income[] FetchIncomes() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    Income income = null;
    Income[] incomes = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId = " + copyId;
      income = new Income(interpretor, whereClause);
      if(income.borrowerId != borrowerId)
      {
        income.close();
        income = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchIncomes: " + e);
      //==========================================

      throw e;
    }

    if(income != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(income.deepCopy());

      while(income.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(income.deepCopy());
      }
      income.close();
    }

    nIncome = aStack.size();
    incomes = new Income[nIncome];

    for(i = 0; i < nIncome; ++i)
    {
      incomes[i] = (Income)aStack.get(i);
    }

    return incomes;
  }

  private Liability[] FetchLiabilities() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    Liability liability = null;
    Liability[] liabilities = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId = " + copyId;
      liability = new Liability(interpretor, whereClause);
      if(liability.borrowerId != borrowerId)
      {
        liability.close();
        liability = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchLiabilities: " + e);
      //==========================================

      throw e;
    }

    if(liability != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(liability.deepCopy());

      while(liability.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(liability.deepCopy());
      }
      liability.close();
    }

    nLiability = aStack.size();
    liabilities = new Liability[nLiability];

    for(i = 0; i < nLiability; ++i)
    {
      liabilities[i] = (Liability)aStack.get(i);
    }

    return liabilities;
  }

  protected void close() throws SQLException
  {
    int i;

    for(i = 0; i < nEmploymentHistory; ++i)
    {
      if(employmentHistory[i] != null)
      {
        employmentHistory[i].close();
      }
    }

    for(i = 0; i < nBorrowerAddress; ++i)
    {
      if(borrowerAddress[i] != null)
      {
        borrowerAddress[i].close();
      }
    }

    for(i = 0; i < nIncome; ++i)
    {
      if(income[i] != null)
      {
        income[i].close();
      }
    }

    for(i = 0; i < nLiability; ++i)
    {
      if(liability[i] != null)
      {
        liability[i].close();
      }
    }

    for(i = 0; i < nAsset; ++i)
    {
      if(asset[i] != null)
      {
        asset[i].close();
      }
    }

    for(i = 0; i < nCreditReference; ++i)
    {
      if(creditReference[i] != null)
      {
        creditReference[i].close();
      }
    }

    //--DJ_LDI_CR--start--//
    for(i = 0; i < nLifeDisabilityPremiums; ++i)
    {
      if(lifeDisabilityPremiums[i] != null)
      {
        lifeDisabilityPremiums[i].close();
      }
    }
    //--DJ_LDI_CR--end--//

    super.close();
  }

  private Asset[] FetchAssets() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    Asset asset = null;
    Asset[] assets = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId = " + copyId;
      asset = new Asset(interpretor, whereClause);
      if(asset.borrowerId != borrowerId)
      {
        asset.close();
        asset = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchAssets: " + e);
      //==========================================

      throw e;
    }

    if(asset != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(asset.deepCopy());

      while(asset.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(asset.deepCopy());
      }
      asset.close();
    }

    nAsset = aStack.size();
    assets = new Asset[nAsset];

    for(i = 0; i < nAsset; ++i)
    {
      assets[i] = (Asset)aStack.get(i);
    }

    return assets;
  }

  private CreditReference[] FetchCreditReferences() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    CreditReference creditReference = null;
    CreditReference[] creditReferences = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId =" + copyId;
      creditReference = new CreditReference(interpretor, whereClause);
      if(creditReference.borrowerId != borrowerId)
      {
        creditReference.close();
        creditReference = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchCreditReferences: " + e);
      //==========================================

      throw e;
    }

    if(creditReference != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(creditReference.deepCopy());

      while(creditReference.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(creditReference.deepCopy());
      }
      creditReference.close();
    }

    nCreditReference = aStack.size();
    creditReferences = new CreditReference[nCreditReference];

    for(i = 0; i < nCreditReference; ++i)
    {
      creditReferences[i] = (CreditReference)aStack.get(i);
    }

    return creditReferences;
  }

  //--DJ_LDI_CR--start--//
  private LifeDisabilityPremiums[] FetchLifeDisabilityPremiums() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    LifeDisabilityPremiums ldp = null;
    LifeDisabilityPremiums[] ldps = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId = " + copyId;
      ldp = new LifeDisabilityPremiums(interpretor, whereClause);
      if(ldp.borrowerId != borrowerId)
      {
        ldp.close();
        ldp = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchLifeDisabilityPremiums: " + e);
      //==========================================

      throw e;
    }

    if(ldp != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(ldp.deepCopy());

      while(ldp.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(ldp.deepCopy());
      }
      ldp.close();
    }

    nLifeDisabilityPremiums = aStack.size();
    ldps = new LifeDisabilityPremiums[nLifeDisabilityPremiums];

    for(i = 0; i < nLifeDisabilityPremiums; ++i)
    {
      ldps[i] = (LifeDisabilityPremiums)aStack.get(i);
    }

    return ldps;
  }

  //--DJ_LDI_CR--end--//

  /*NBC-Implementation -- Start
  * New Method to fetch BorrwerIdentifications
  */
  private BorrowerIdentification[] FetchBorrowerIdentifications() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    BorrowerIdentification borrowerIdentification = null;
    BorrowerIdentification[] borrowerIdentifications = null;
    Stack aStack = new Stack();

    try
    {
      //String   whereClause = "borrowerId=" + borrowerId;
      String whereClause = "borrowerId=" + borrowerId + " and copyId = " + copyId;
      borrowerIdentification = new BorrowerIdentification(interpretor, whereClause);
      if(borrowerIdentification.borrowerId != borrowerId)
      {
    	  borrowerIdentification.close();
    	  borrowerIdentification = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Borrower.FetchBorrowerIdentifications: " + e);
      //==========================================

      throw e;
    }

    if(borrowerIdentification != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(borrowerIdentification.deepCopy());

      while(borrowerIdentification.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(borrowerIdentification.deepCopy());
      }
      borrowerIdentification.close();
    }

    nBorrowerIdentification = aStack.size();
    borrowerIdentifications = new BorrowerIdentification[nBorrowerIdentification];

    for(i = 0; i < nBorrowerIdentification; ++i)
    {
    	borrowerIdentifications[i] = (BorrowerIdentification)aStack.get(i);
    }

    return borrowerIdentifications;
  }
  
  //--NBC Implementation -- END of Method FetchBorrowerIdentifications--//

  public boolean next() throws SQLException
  {
    boolean bStatus;

    bStatus = super.next();
    if(bStatus)
    {
      borrowerId = resultSet.getInt("BORROWERID");
      copyId = resultSet.getInt("COPYID");
      dealId = resultSet.getInt("DEALID");
      borrowerNumber = resultSet.getInt("BORROWERNUMBER");
      borrowerFirstName = getString(resultSet.getString("BORROWERFIRSTNAME"));
      if(borrowerFirstName != null)
      {
        borrowerFirstName = borrowerFirstName.trim();
      }
      borrowerMiddleInitial = getString(resultSet.getString("BORROWERMIDDLEINITIAL"));
      if(borrowerMiddleInitial != null)
      {
        borrowerMiddleInitial = borrowerMiddleInitial.trim();
      }
      borrowerLastName = getString(resultSet.getString("BORROWERLASTNAME"));
      if(borrowerLastName != null)
      {
        borrowerLastName = borrowerLastName.trim();
      }
      firstTimeBuyer = getString(resultSet.getString("FIRSTTIMEBUYER"));
      if(firstTimeBuyer != null)
      {
        firstTimeBuyer = firstTimeBuyer.trim();
      }
      borrowerBirthDate = asDate(resultSet.getTimestamp("BORROWERBIRTHDATE"));
      nsfOccurenceTypeId = resultSet.getInt("NSFOCCURENCETYPEID");

      //--DJ_LI_CR--start//
      // It is an integer referencing to the BorrowerGender table now.
      ////borrowerGender  = getString(resultSet.getString("BORROWERGENDER"));
      ////if (borrowerGender != null )
      ////	borrowerGender  =  borrowerGender.trim();
      borrowerGenderId = resultSet.getInt("BORROWERGENDERID");
      //--DJ_LDI_CR--end--//

      salutationId = resultSet.getInt("SALUTATIONID");
      maritalStatusId = resultSet.getInt("MARITALSTATUSID");
      socialInsuranceNumber = getString(resultSet.getString("SOCIALINSURANCENUMBER"));
      if(socialInsuranceNumber != null)
      {
        socialInsuranceNumber = socialInsuranceNumber.trim();
      }
      borrowerHomePhoneNumber = getString(resultSet.getString("BORROWERHOMEPHONENUMBER"));
      if(borrowerHomePhoneNumber != null)
      {
        borrowerHomePhoneNumber = borrowerHomePhoneNumber.trim();
      }
      borrowerWorkPhoneNumber = getString(resultSet.getString("BORROWERWORKPHONENUMBER"));
      if(borrowerWorkPhoneNumber != null)
      {
        borrowerWorkPhoneNumber = borrowerWorkPhoneNumber.trim();
      }
      borrowerFaxNumber = getString(resultSet.getString("BORROWERFAXNUMBER"));
      if(borrowerFaxNumber != null)
      {
        borrowerFaxNumber = borrowerFaxNumber.trim();
      }
      borrowerEmailAddress = getString(resultSet.getString("BORROWEREMAILADDRESS"));
      if(borrowerEmailAddress != null)
      {
        borrowerEmailAddress = borrowerEmailAddress.trim();
      }
      clientReferenceNumber = getString(resultSet.getString("CLIENTREFERENCENUMBER"));
      if(clientReferenceNumber != null)
      {
        clientReferenceNumber = clientReferenceNumber.trim();
      }
      numberOfDependents = resultSet.getInt("NUMBEROFDEPENDENTS");
      numberOfTimesBankrupt = resultSet.getInt("NUMBEROFTIMESBANKRUPT");
      raceId = resultSet.getInt("RACEID");
      creditScore = resultSet.getInt("CREDITSCORE");
      GDS = resultSet.getDouble("GDS");
      TDS = resultSet.getDouble("TDS");
      educationLevelId = resultSet.getInt("EDUCATIONLEVELID");
      netWorth = resultSet.getDouble("NETWORTH");
      existingClient = getString(resultSet.getString("EXISTINGCLIENT"));
      if(existingClient != null)
      {
        existingClient = existingClient.trim();
      }
      creditSummary = getString(resultSet.getString("CREDITSUMMARY"));
      if(creditSummary != null)
      {
        creditSummary = creditSummary.trim();
      }
      languagePreferenceId = resultSet.getInt("LANGUAGEPREFERENCEID");
      borrowerGeneralStatusId = resultSet.getInt("BORROWERGENERALSTATUSID");
      borrowerTypeId = resultSet.getInt("BORROWERTYPEID");
      citizenshipTypeId = resultSet.getInt("CITIZENSHIPTYPEID");
      primaryBorrowerFlag = getString(resultSet.getString("PRIMARYBORROWERFLAG"));
      if(primaryBorrowerFlag != null)
      {
        primaryBorrowerFlag = primaryBorrowerFlag.trim();
      }
      totalAssetAmount = resultSet.getDouble("TOTALASSETAMOUNT");
      totalLiabilityAmount = resultSet.getDouble("TOTALLIABILITYAMOUNT");
      paymentHistoryTypeId = resultSet.getInt("PAYMENTHISTORYTYPEID");
      totalIncomeAmount = resultSet.getDouble("TOTALINCOMEAMOUNT");
      dateOfCreditBureauProfile = asDate(resultSet.getTimestamp("DATEOFCREDITBUREAUPROFILE"));
      creditBureauOnFileDate = asDate(resultSet.getTimestamp("CREDITBUREAUONFILEDATE"));
      bureauAttachment = getString(resultSet.getString("BUREAUATTACHMENT"));
      if(bureauAttachment != null)
      {
        bureauAttachment = bureauAttachment.trim();
      }
      existingClientComments = getString(resultSet.getString("EXISTINGCLIENTCOMMENTS"));
      if(existingClientComments != null)
      {
        existingClientComments = existingClientComments.trim();
      }
      bankruptcyStatusId = resultSet.getInt("BANKRUPTCYSTATUSID");
      age = resultSet.getInt("AGE");
      staffOfLender = getString(resultSet.getString("STAFFOFLENDER"));
      if(staffOfLender != null)
      {
        staffOfLender = staffOfLender.trim();
      }
      staffType = getString(resultSet.getString("STAFFTYPE"));
      if(staffType != null)
      {
        staffType = staffType.trim();
      }
      totalLiabilityPayments = resultSet.getDouble("TOTALLIABILITYPAYMENTS");
      GDS3Year = resultSet.getDouble("GDS3YEAR");
      TDS3Year = resultSet.getDouble("TDS3YEAR");
      SINCheckDigit = getString(resultSet.getString("SINCHECKDIGIT"));
      if(SINCheckDigit != null)
      {
        SINCheckDigit = SINCheckDigit.trim();
      }
      creditBureauNameId = resultSet.getInt("CREDITBUREAUNAMEID");
      borrowerWorkPhoneExtension = resultSet.getString("BORROWERWORKPHONEEXTENSION");

      //--DJ_LI_CR--start//
      insuranceProportionsId = resultSet.getInt("INSURANCEPROPORTIONSID");
      lifePercentCoverageReq = resultSet.getDouble("LIFEPERCENTCOVERAGEREQ");
      disabilityStatusId = resultSet.getInt("DISABILITYSTATUSID");
      lifeStatusId = resultSet.getInt("LIFESTATUSID");
      smokeStatusId = resultSet.getInt("SMOKESTATUSID");
      disPercentCoverageReq = resultSet.getDouble("DISPERCENTCOVERAGEREQ");
      //--DJ_LDI_CR--end--//

      //--DJ_CR201.2--start//
      guarantorOtherLoans = resultSet.getString("GUARANTOROTHERLOANS");
      //--DJ_CR201.2--end//

      
      //-- NBC-Implementation - Start --//
          
      solicitationId = resultSet.getInt("SOLICITATIONID");
      employeeNumber = resultSet.getString("EMPLOYEENUMBER");
      prefContactMethodId = resultSet.getInt("PREFCONTACTMETHODID");
      
      //-- NBC-Implementation - End --//
      
      /***** FFATE start *****/
      suffixId = resultSet.getInt("SUFFIXID");
      borrowerCellPhoneNumber = resultSet.getString("BORROWERCELLPHONENUMBER");
      cbAuthorizationDate = resultSet.getDate("CBAUTHORIZATIONDATE");
      cbAuthorizationMethod = resultSet.getString("CBAUTHORIZATIONMETHOD");
      /***** FFATE end *****/
      
      // fetch all employmentHistory using borrowerId as key
      try
      {
        employmentHistory = FetchEmploymentHistories();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 1 : " + e);
        //==========================================
      }
      catch(SQLException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next: " + e);
        //==========================================

        throw e;
      }

      // fetch all borrowerAddress using borrowerId as key
      try
      {
        borrowerAddress = FetchBorrowerAddresses();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 2 : " + e);
        //==========================================
      }

      // fetch all income records using borrowerId as key
      try
      {
        income = FetchIncomes();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 3 : " + e);
        //==========================================
      }

      // fetch all liability records using borrowerId as key
      try
      {
        liability = FetchLiabilities();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 4 : " + e);
        //==========================================
      }

      // fetch all asset records using borrowerId as key
      try
      {
        asset = FetchAssets();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 5 : " + e);
        //==========================================
      }

      // fetch all creditReference records using borrowerId as key
      try
      {
        creditReference = FetchCreditReferences();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 6 : " + e);
        //==========================================
      }

      //--DJ_LDI_CR--start--//
      // fetch all creditReference records using borrowerId as key
      try
      {
        lifeDisabilityPremiums = FetchLifeDisabilityPremiums();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 7 : " + e);
        //==========================================

      }
      //--DJ_LDI_CR--end--//

      //NBC-Implementation --start//
      //fetch all borrowerIdentification using borrowerId as key
      try{
    	  borrowerIdentification = FetchBorrowerIdentifications();
      }catch(CloneNotSupportedException e)
      {
           BRUtil.error("Exception @Borrower.next CloneNotSupportedException intercepted 8: " + e);
          //==========================================
      }

      //-- NBC-Implementation - End --//
      

    }

    return bStatus;
  }

  public void update(String updClause) throws SQLException
  {
    String sqlStmt = "update Borrower set ";

    sqlStmt += updClause + " where dealId=" + dealId;
    sqlStmt += " and borrowerId=" + borrowerId;

    if(execStmt == null)
    {
      execStmt = interpretor.getConnection().createStatement();

      execStmt.execute(sqlStmt);
    }
  }
}
