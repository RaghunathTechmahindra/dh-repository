// dynamic bind instance variables by its name
package com.basis100.BREngine;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 */

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import com.basis100.deal.util.TypeConverter;

public class DocumentTracking extends DealEntity {

	protected  	int 	dealId ;
	protected  	int 	copyId ;
	protected  	int 	documentTrackingId ;
	protected  	int 	documentStatusId ;
	protected  	String 	documentLabel ;
	protected  	Date 	documentRequestDate ;
	protected  	int 	conditionId ;
	protected  	Date 	documentDueDate ;
	protected  	String 	documentText ;
	protected  	String 	documentResponsibilityId ;
	protected  	int 	conditionTypeId ;
	protected  	int 	conditionResponsibilityRoleId ;
	protected  	int 	documentResponsibilityRoleId ;
	protected  	int 	propertyId ;
	protected  	int 	applicationId ;
	protected  	Date 	dStatusChangeDate ;

  public String getPrimaryKeyName() {
      return "statusId";
   }
   public DocumentTracking(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }
   public DocumentTracking(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public DocumentTracking(BRInterpretor interpretor, int dealId, int aCopyId) throws SQLException {
      super(interpretor);
      filter = "dealId=" + dealId + " and copyId = " + aCopyId;

      if (Open())
         next();
   }

   public DocumentTracking(BRInterpretor interpretor, int dealId) throws SQLException {
      super(interpretor);
      filter = "dealId=" + dealId;

      if (Open())
         next();
   }

   public String getDefaultSQL() {
  	 
   	  //=================================================================
   	  // CLOB Performance Enhancement June 2010
			 	String theSQL = "select documenttrackingid," +
				"languagepreferenceid," +
				"documentlabel," +
				"documenttext," +
				"copyid," +
				"institutionprofileid," +
				"documenttextlite " +
				"from documenttrackingverbiage";	 
  	 	return theSQL;
  	 	//=================================================================
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();

      //#DG638 rewritten
      if (!bStatus) 
        return bStatus;
      
			dealId  = resultSet.getInt("dealId");
			copyId  = resultSet.getInt("copyId");
			documentTrackingId  = resultSet.getInt("documentTrackingId");
			documentStatusId  = resultSet.getInt("documentStatusId");
			documentLabel  = getString(resultSet.getString("documentLabel"));
			/*#DG638 if (documentLabel != null )
				documentLabel  =  documentLabel.trim();*/
			documentRequestDate  = asDate(resultSet.getTimestamp("documentRequestDate"));
			conditionId  = resultSet.getInt("conditionId");
			documentDueDate  = asDate(resultSet.getTimestamp("documentDueDate"));
			/*#DG638 documentText  = getString(resultSet.getString("documentText"));
				if (documentText != null )
				documentText  =  documentText.trim();*/
    	//=================================================================
    	// CLOB Performance Enhancement June 2010
			String tmpText = resultSet.getString("documenttextlite");
			if(null != tmpText && tmpText.length() > 0) {
				documentText = tmpText;
			} else {
				try {
					documentText  = TypeConverter.stringFromClob(resultSet.getClob("documentText"));		
				}
				catch (IOException e) {
					throw new SQLException(e.toString());
				}
			}
			//=================================================================
			
			documentText  = getString(documentText);
			documentResponsibilityId  = getString(resultSet.getString("documentResponsibilityId"));
			/*#DG638 if (documentResponsibilityId != null )
				documentResponsibilityId  =  documentResponsibilityId.trim();*/
			conditionTypeId  = resultSet.getInt("conditionTypeId");
			conditionResponsibilityRoleId  = resultSet.getInt("conditionResponsibilityRoleId");
			documentResponsibilityRoleId  = resultSet.getInt("documentResponsibilityRoleId");
			propertyId  = resultSet.getInt("propertyId");
			applicationId  = resultSet.getInt("applicationId");
			dStatusChangeDate  = asDate(resultSet.getTimestamp("dStatusChangeDate"));
         
      return bStatus;
   }
}
