// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Address extends DealEntity {
	protected  	int 	addrId ;
	protected  	int 	copyId ;
	protected  	int 	provinceId ;
	protected  	String 	addressLine1 ;
	protected  	String 	addressLine2 ;
	protected  	String 	city ;
	protected  	String 	postalFSA ;
	protected  	String 	postalLDU ;

	//--NBC-Implementation Start--/
	
	protected String streetNumber;
    protected String streetName;
    protected int streetTypeId;
    protected int streetDirectionId;
    protected String unitNumber;
	
	//--NBC-Implementation End --//

  public String getPrimaryKeyName() {
      return "addrId";
   }

   public Address(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public Address(BRInterpretor interpretor, int addrId, int pCopyId)
           throws SQLException {
      super(interpretor);
      filter = getPrimaryKeyName() + "=" + addrId + " and copyId = " + pCopyId;
      if(Open())
        next();
   }

   protected Address deepCopy() throws CloneNotSupportedException {
      return (Address)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from addr";
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {

				addrId  = resultSet.getInt("ADDRID");
				copyId  = resultSet.getInt("COPYID");
				provinceId  = resultSet.getInt("PROVINCEID");
				addressLine1  = getString(resultSet.getString("ADDRESSLINE1"));
				if (addressLine1 != null )
					addressLine1  =  addressLine1.trim();
				addressLine2  = getString(resultSet.getString("ADDRESSLINE2"));
				if (addressLine2 != null )
					addressLine2  =  addressLine2.trim();
				city  = getString(resultSet.getString("CITY"));
				if (city != null )
					city  =  city.trim();
				postalFSA  = getString(resultSet.getString("POSTALFSA"));
				if (postalFSA != null )
					postalFSA  =  postalFSA.trim();
				postalLDU  = getString(resultSet.getString("POSTALLDU"));
				if (postalLDU != null )
					postalLDU  =  postalLDU.trim();
				
				//--NBC-Implementation Start--//
				streetNumber = resultSet.getString("STREETNUMBER");
	            streetName   = resultSet.getString("STREETNAME");
	            streetTypeId = resultSet.getInt("STREETTYPEID");
	            streetDirectionId = resultSet.getInt("STREETDIRECTIONID");
	            unitNumber = resultSet.getString("unitNumber");

				//--NBC-Implementation End--//
         }

      return bStatus;
   }
}
