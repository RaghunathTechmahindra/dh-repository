// dynamic bind instance variables by its name
package com.basis100.BREngine;

import java.sql.*;

public class AppraisalOrder
  extends DealEntity
{
  protected int     appraisalOrderId;
  protected int     copyId;
  protected int     propertyId;
  protected String  useBorrowerInfo;
  protected String  propertyOwnerName;
  protected String  contactName;
  protected String  contactWorkPhoneNum;
  protected String  contactWorkPhoneNumExt;
  protected String  contactCellPhoneNum;
  protected String  contactHomePhoneNum;
  protected String  commentsForAppraiser;
  protected int     appraisalStatusId;

  protected String getPrimaryKeyName()
  {
    return "appraisalOrderId";
  }

  public AppraisalOrder(BRInterpretor interpretor, boolean template) throws SQLException
  {
    super(interpretor);
  }

  public AppraisalOrder(BRInterpretor intrepretor) throws SQLException
  {
    super(intrepretor);
    filter = null;

    if(Open())
    {
      next();
    }
  }

  public AppraisalOrder(BRInterpretor intrepretor, String filter) throws SQLException
  {
    super(intrepretor);
    this.filter = filter;

    if(Open())
    {
      next();
    }
  }

  protected String getDefaultSQL()
  {
    return "select * from AppraisalOrder";
  }

  public AppraisalOrder deepCopy() throws CloneNotSupportedException
  {
    return(AppraisalOrder)this.clone();
  }

  protected boolean next() throws SQLException
  {
    boolean bStatus;

    bStatus = super.next();
    if(bStatus)
    {
      appraisalOrderId = resultSet.getInt("APPRAISALORDERID");
      copyId = resultSet.getInt("COPYID");
      propertyId = resultSet.getInt("PROPERTYID");
      useBorrowerInfo = getString(resultSet.getString("USEBORROWERINFO"));
      propertyOwnerName = getString(resultSet.getString("PROPERTYOWNERNAME"));
      contactName = getString(resultSet.getString("CONTACTNAME"));
      contactWorkPhoneNum = getString(resultSet.getString("CONTACTWORKPHONENUM"));
      contactWorkPhoneNumExt = getString(resultSet.getString("CONTACTWORKPHONENUMEXT"));
      contactCellPhoneNum = getString(resultSet.getString("CONTACTCELLPHONENUM"));
      contactHomePhoneNum = getString(resultSet.getString("CONTACTHOMEPHONENUM"));
      commentsForAppraiser = getString(resultSet.getString("COMMENTSFORAPPRAISER"));
      appraisalStatusId = resultSet.getInt("APPRAISALSTATUSID");
    }

    return bStatus;
  }
}
