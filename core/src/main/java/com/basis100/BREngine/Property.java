// dynamic bind instance variables by its name
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Property extends DealEntity
{
  protected int dealId;
  protected int propertyId;
  protected int copyId;
  protected int streetTypeId;
  protected int propertyTypeId;
  protected int propertyUsageId;
  protected int dwellingStyleId;
  protected int occupancyTypeId;
  protected String propertyStreetNumber;
  protected String propertyStreetName;
  protected int heatTypeId;
  protected String propertyAddressLine2;
  protected String propertyCity;
  protected String propertyPostalFSA;
  protected String propertyPostalLDU;
  protected String legalLine1;
  protected String legalLine2;
  protected String legalLine3;
  protected int numberOfBedrooms;
  protected int yearBuilt;
  protected String insulatedWithUFFI;
  protected int provinceId;
  protected double monthlyCondoFees;
  protected double purchasePrice;
  protected int municipalityId;
  protected double landValue;
  protected String propertyValueSource;
  protected int appraisalSourceId;
  protected String equityAvailable;
  protected int newConstructionId;
  protected short zoning;
  protected double estimatedAppraisalValue;
  protected double actualAppraisalValue;
  protected double loanToValue;
  protected String unitNumber;
  protected int lotSize;
  protected String primaryPropertyFlag;
  protected Date appraisalDateAct;
  protected int lotSizeDepth;
  protected int lotSizeFrontage;
  protected int lotSizeUnitOfMeasureId;
  protected int numberOfUnits;
  protected int propertyLocationId;
  protected int propertyOccurenceNumber;
  protected String propertyReferenceNumber;
  protected int sewageTypeId;
  protected int streetDirectionId;
  protected int waterTypeId;
  protected int livingSpace;
  protected int livingSpaceUnitOfMeasureId;
  protected int structureAge;
  protected int dwellingTypeId;
  protected double lendingValue;
  protected double totalPropertyExpenses;
  protected double totalAnnualTaxAmount;

  //Additional fields for CMHC modification -- By BILLY 12Feb2002
  protected String MLSListingFlag;
  protected int garageSizeId;
  protected int garageTypeId;
  protected String builderName;

  //=============================================================

//  protected Province province;

  // retrieved using propertyId
  protected int nPropertyExpense;
  protected PropertyExpense[] propertyExpense;

  //--> Added AppraisalOrder -- By Billy 27Jan2004
  protected AppraisalOrder appraisalOrder;

 //NBC - Implementation - Start
  protected int requestappraisalId;
  //NBC -  Implementation - End

  //==============================================

  // Bruce Apr.19, 2006, START - MI Additions
  protected int       tenureTypeId;
  protected String    miEnergyEfficiency;
  protected String    onReserveTrustAgreementNumber;
  protected String    subdivisionDiscount;
  // END - MI Additions
  
  protected String getPrimaryKeyName()
  {
    return "propertyId";
  }

  public Property(BRInterpretor interpretor, boolean bTemplate) throws SQLException
  {
    super(interpretor);
    if(bTemplate)
    {
      // place holder incase need to propagate template creation
    }
  }

  public Property(BRInterpretor interpretor) throws SQLException
  {
    super(interpretor);
    filter = null;

    if(Open())
    {
      next();
    }
  }

  public Property(BRInterpretor interpretor, Object anObj) throws SQLException
  {
    super(interpretor);
    //filter = getPrimaryKeyName() + "=" + ((Deal)anObj).dealId;
    filter = getPrimaryKeyName() + "=" + ((Deal)anObj).dealId + " and copyId = " + ((Deal)anObj).copyId;

    if(Open())
    {
      next();
    }
  }

  public Property(BRInterpretor interpretor, String filter) throws SQLException
  {
    super(interpretor);
    this.filter = filter;

    if(Open())
    {
      next();
    }
  }

  public Property deepCopy() throws CloneNotSupportedException
  {
    return(Property)this.clone();
  }

  protected String getDefaultSQL()
  {
    return "select * from Property";
  }

  private PropertyExpense[] FetchPropertyExpenses() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    PropertyExpense propertyExpense = null;
    PropertyExpense[] propertyExpenses = null;
    Stack aStack = new Stack();

    try
    {
      String filter = "propertyId=" + propertyId + " and copyId = " + copyId;
      propertyExpense = new PropertyExpense(interpretor, filter);
      if(propertyExpense.propertyId != propertyId)
      {
        propertyExpense.close();
        propertyExpense = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Property.FetchPropertyExpenses : " + e);
      //==========================================
      throw e;
    }

    if(propertyExpense != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(propertyExpense.deepCopy());

      while(propertyExpense.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(propertyExpense.deepCopy());
      }
      propertyExpense.close();
    }

    nPropertyExpense = aStack.size();
    propertyExpenses = new PropertyExpense[nPropertyExpense];

    for(i = 0; i < nPropertyExpense; ++i)
    {
      propertyExpenses[i] = (PropertyExpense)aStack.get(i);
    }

    return propertyExpenses;
  }

  public boolean next() throws SQLException
  {
    boolean bStatus;

    bStatus = super.next();
    if(bStatus)
    {
      dealId = resultSet.getInt("DEALID");
      propertyId = resultSet.getInt("PROPERTYID");
      copyId = resultSet.getInt("COPYID");
      streetTypeId = resultSet.getInt("STREETTYPEID");
      propertyTypeId = resultSet.getInt("PROPERTYTYPEID");
      propertyUsageId = resultSet.getInt("PROPERTYUSAGEID");
      dwellingStyleId = resultSet.getInt("DWELLINGSTYLEID");
      occupancyTypeId = resultSet.getInt("OCCUPANCYTYPEID");
      propertyStreetNumber = getString(resultSet.getString("PROPERTYSTREETNUMBER"));
      if(propertyStreetNumber != null)
      {
        propertyStreetNumber = propertyStreetNumber.trim();
      }
      propertyStreetName = getString(resultSet.getString("PROPERTYSTREETNAME"));
      if(propertyStreetName != null)
      {
        propertyStreetName = propertyStreetName.trim();
      }
      heatTypeId = resultSet.getInt("HEATTYPEID");
      propertyAddressLine2 = getString(resultSet.getString("PROPERTYADDRESSLINE2"));
      if(propertyAddressLine2 != null)
      {
        propertyAddressLine2 = propertyAddressLine2.trim();
      }
      propertyCity = getString(resultSet.getString("PROPERTYCITY"));
      if(propertyCity != null)
      {
        propertyCity = propertyCity.trim();
      }
      propertyPostalFSA = getString(resultSet.getString("PROPERTYPOSTALFSA"));
      if(propertyPostalFSA != null)
      {
        propertyPostalFSA = propertyPostalFSA.trim();
      }
      propertyPostalLDU = getString(resultSet.getString("PROPERTYPOSTALLDU"));
      if(propertyPostalLDU != null)
      {
        propertyPostalLDU = propertyPostalLDU.trim();
      }
      legalLine1 = getString(resultSet.getString("LEGALLINE1"));
      if(legalLine1 != null)
      {
        legalLine1 = legalLine1.trim();
      }
      legalLine2 = getString(resultSet.getString("LEGALLINE2"));
      if(legalLine2 != null)
      {
        legalLine2 = legalLine2.trim();
      }
      legalLine3 = getString(resultSet.getString("LEGALLINE3"));
      if(legalLine3 != null)
      {
        legalLine3 = legalLine3.trim();
      }
      numberOfBedrooms = resultSet.getInt("NUMBEROFBEDROOMS");
      yearBuilt = resultSet.getInt("YEARBUILT");
      insulatedWithUFFI = getString(resultSet.getString("INSULATEDWITHUFFI"));
      if(insulatedWithUFFI != null)
      {
        insulatedWithUFFI = insulatedWithUFFI.trim();
      }
      provinceId = resultSet.getInt("PROVINCEID");
      monthlyCondoFees = resultSet.getDouble("MONTHLYCONDOFEES");
      purchasePrice = resultSet.getDouble("PURCHASEPRICE");
      municipalityId = resultSet.getInt("MUNICIPALITYID");
      landValue = resultSet.getDouble("LANDVALUE");
      propertyValueSource = getString(resultSet.getString("PROPERTYVALUESOURCE"));
      if(propertyValueSource != null)
      {
        propertyValueSource = propertyValueSource.trim();
      }
      appraisalSourceId = resultSet.getInt("APPRAISALSOURCEID");
      equityAvailable = getString(resultSet.getString("EQUITYAVAILABLE"));
      if(equityAvailable != null)
      {
        equityAvailable = equityAvailable.trim();
      }
      newConstructionId = resultSet.getInt("NEWCONSTRUCTIONID");
      zoning = resultSet.getShort("ZONING");
      estimatedAppraisalValue = resultSet.getDouble("ESTIMATEDAPPRAISALVALUE");
      actualAppraisalValue = resultSet.getDouble("ACTUALAPPRAISALVALUE");
      loanToValue = resultSet.getDouble("LOANTOVALUE");
      unitNumber = getString(resultSet.getString("UNITNUMBER"));
      if(unitNumber != null)
      {
        unitNumber = unitNumber.trim();
      }
      lotSize = resultSet.getInt("LOTSIZE");
      primaryPropertyFlag = getString(resultSet.getString("PRIMARYPROPERTYFLAG"));
      if(primaryPropertyFlag != null)
      {
        primaryPropertyFlag = primaryPropertyFlag.trim();
      }
      appraisalDateAct = asDate(resultSet.getTimestamp("APPRAISALDATEACT"));
      lotSizeDepth = resultSet.getInt("LOTSIZEDEPTH");
      lotSizeFrontage = resultSet.getInt("LOTSIZEFRONTAGE");
      lotSizeUnitOfMeasureId = resultSet.getInt("LOTSIZEUNITOFMEASUREID");
      numberOfUnits = resultSet.getInt("NUMBEROFUNITS");
      propertyLocationId = resultSet.getInt("PROPERTYLOCATIONID");
      propertyOccurenceNumber = resultSet.getInt("PROPERTYOCCURENCENUMBER");
      propertyReferenceNumber = getString(resultSet.getString("PROPERTYREFERENCENUMBER"));
      if(propertyReferenceNumber != null)
      {
        propertyReferenceNumber = propertyReferenceNumber.trim();
      }
      sewageTypeId = resultSet.getInt("SEWAGETYPEID");
      streetDirectionId = resultSet.getInt("STREETDIRECTIONID");
      waterTypeId = resultSet.getInt("WATERTYPEID");
      livingSpace = resultSet.getInt("LIVINGSPACE");
      livingSpaceUnitOfMeasureId = resultSet.getInt("LIVINGSPACEUNITOFMEASUREID");
      structureAge = resultSet.getInt("STRUCTUREAGE");
      dwellingTypeId = resultSet.getInt("DWELLINGTYPEID");
      lendingValue = resultSet.getDouble("LENDINGVALUE");
      totalPropertyExpenses = resultSet.getDouble("TOTALPROPERTYEXPENSES");
      totalAnnualTaxAmount = resultSet.getDouble("TOTALANNUALTAXAMOUNT");

      //Additional fields for CMHC modification -- By BILLY 12Feb2002
      MLSListingFlag = getString(resultSet.getString("MLSLISTINGFLAG"));
      garageSizeId = resultSet.getInt("GARAGESIZEID");
      garageTypeId = resultSet.getInt("GARAGETYPEID");
      builderName = getString(resultSet.getString("BUILDERNAME"));
      //=============================================================

      //Bruce, Apr.19, 2006, MI additions
      tenureTypeId = resultSet.getInt("TENURETYPEID");
      miEnergyEfficiency = getString(resultSet.getString("MIENERGYEFFICIENCY"));
      onReserveTrustAgreementNumber = resultSet.getString("ONRESERVETRUSTAGREEMENTNUMBER");
      subdivisionDiscount = getString(resultSet.getString("SUBDIVISIONDISCOUNT"));
      //end MI additions
      
      //NBC - Implementation --Start//
      requestappraisalId = resultSet.getInt("REQUESTAPPRAISALID");
      //NBC - Implementation --End//      
      //=============================================================

      // fetch propertyExpense
      try
      {
        propertyExpense = FetchPropertyExpenses();
      }
      catch(CloneNotSupportedException e)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.error("Exception @Property.next : " + e);
        //==========================================
      }

      //--> Added AppraisalOrder -- By Billy 27Jan2004
      // fetch AppraisalOrder record
      if(appraisalOrder != null)
      {
        appraisalOrder.close();
      }
      filter = "propertyId=" + propertyId + " and copyId = " + copyId;
      appraisalOrder = new AppraisalOrder(interpretor, filter);
      appraisalOrder.close();

    }

    return bStatus;
  }

  public void update(String updClause) throws SQLException
  {
    String sqlStmt = "update Property set ";

    sqlStmt += updClause + " where provinceId=" + provinceId;

    if(execStmt == null)
    {
      execStmt = interpretor.connObj.createStatement();

      execStmt.execute(sqlStmt);
    }
  }
}
