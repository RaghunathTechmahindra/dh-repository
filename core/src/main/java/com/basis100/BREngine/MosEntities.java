// Business Rule Expression Scanner, add array
package com.basis100.BREngine;

import java.lang.reflect.Field;
import java.util.HashMap;

public class MosEntities
{
  private static String[] classNames =
    {
    "com.basis100.BREngine.Deal",
    "com.basis100.BREngine.Liability",
    "com.basis100.BREngine.Asset",
    "com.basis100.BREngine.CreditReference",
    "com.basis100.BREngine.BorrowerAddress",
    "com.basis100.BREngine.EmploymentHistory",
    "com.basis100.BREngine.Contact",
    "com.basis100.BREngine.Address",
    //--DJ_LDI_CR--start--//
    "com.basis100.BREngine.LifeDisabilityPremiums",
    //--DJ_LDI_CR--end--//
    //--DJ_CR136--start--//
    "com.basis100.BREngine.InsureOnlyApplicant",
    "com.basis100.BREngine.LifeDisPremiumsIOnlyA",
    //--DJ_CR136--end--//
    //--AVM / Valuation--start--//
    "com.basis100.BREngine.Request",
    "com.basis100.BREngine.ServiceRequest",
    "com.basis100.BREngine.ServiceRequestContact"
    //--AVM / Valuation--end--//
  };
  protected static HashMap EntityDictionary = null;

  public static String mapEntityName(String token)
  {
    if(EntityDictionary == null)
    {
      EntityDictionary = new HashMap();
      Class oneClass;
      Field[] fields = null;
      try
      {
        for(int i = 0; i < classNames.length; i++)
        {
          oneClass = Class.forName(classNames[i]);
          fields = oneClass.getDeclaredFields();
          for(int j = 0; j < fields.length; j++)
          {
            EntityDictionary.put(new String(fields[j].getName().trim().toLowerCase()), new String(fields[j].getName().trim()));
          }
        }
      }
      catch(Exception exc)
      {
        //--> Debug enhancement : by Billy 23Aug2004
        BRUtil.error("Exception @MosEntities.mapEntityName : " + exc);
        //==========================================
      }
    }
    /*
          if (EntityDictionary == null) {
             EntityDictionary = new HashMap();
             EntityDictionary.put("deal", "deal");

             // deal attributes
             EntityDictionary.put("sourceofbusinessprofileid",
                                  "sourceOfBusinessProfileId");
             EntityDictionary.put("sourceofbusinessprofile",
                                  "SourceOfBusinessProfile");
             EntityDictionary.put("applicationid", "applicationId");
             EntityDictionary.put("applicationdate", "applicationdate");
             EntityDictionary.put("totalloanamount", "totalLoanAmount");
             EntityDictionary.put("amortizationterm", "amortizationTerm");
             EntityDictionary.put("estimatedclosingdate", "estimatedClosingDate");
             EntityDictionary.put("dealtypeid","dealTypeId");
             EntityDictionary.put("lenderprofileid", "lenderProfileId");
             EntityDictionary.put("lenderprofile", "lenderProfile");
             EntityDictionary.put("sourceapplicationid", "sourceApplicationId");
             EntityDictionary.put("ltv", "LTV");
             EntityDictionary.put("totalappraisedvalue", "totalAppraisedValue");
             EntityDictionary.put("totalpurchaseprice", "totalPurchasePrice");
             EntityDictionary.put("postedinterestrate", "postedInterestRate");
             EntityDictionary.put("underwriteruserid", "underwriterUserId");
             EntityDictionary.put("estimatedpaymentamount", "estimatedPaymentAmount");
             EntityDictionary.put("pandlpaymentamount", "PandlPaymentAmount");
             EntityDictionary.put("escrowpaymentamount", "escrowPaymentAmount");
             EntityDictionary.put("mipremiumamount", "MIPremiumAmount");
             EntityDictionary.put("netloanamount", "netLoanAmount");
             EntityDictionary.put("statusdate", "statusDate");
             EntityDictionary.put("discount", "discount");
             EntityDictionary.put("secondaryfinancing", "secondaryFinancing");
             EntityDictionary.put("originalmortgagenumber", "originalMortgageNumber");
             EntityDictionary.put("downpaymentsourceid", "downPaymentSourceId");
             EntityDictionary.put("downpaymentsource", "downpaymentsource");
             EntityDictionary.put("denialreasonid", "denialReasonId");
             EntityDictionary.put("renewaloptionsid", "renewalOptionsId");
             EntityDictionary.put("netinterestrate", "netInterestRate");
             EntityDictionary.put("ratedate", "rateDate");
             EntityDictionary.put("loanpurposeid", "loanPurposeId");
             EntityDictionary.put("buydownrate", "buydownRate");
             EntityDictionary.put("specialfeatureid", "specialFeatureId");
             EntityDictionary.put("interiminterestamount", "interiminterestamount");
             EntityDictionary.put("downpaymentamount", "downPaymentAmount");
             EntityDictionary.put("lineofbusinessid", "lineOfBusinessId");
             EntityDictionary.put("mortgageinsurerid", "mortgageInsurerId");
             EntityDictionary.put("mtgprodid", "mtgProdId");
             EntityDictionary.put("mtgprod", "mtgProd");
             EntityDictionary.put("mipolicynumber", "MIPolicyNumber");
             EntityDictionary.put("mipayor", "MIPayor");
             EntityDictionary.put("firstpaymentdate", "firstPaymentDate");
             EntityDictionary.put("netpaymentamount", "netPaymentAmount");
             EntityDictionary.put("marsnamesearch", "MARSNameSearch");
             EntityDictionary.put("marspropertysearch", "MARSPropertySearch");
             EntityDictionary.put("lienposition", "lienPosition");
             EntityDictionary.put("miloannumber", "MILoanNumber");
             EntityDictionary.put("commisionamount", "commisionAmount");
             EntityDictionary.put("fundeddate", "fundedDate");
             EntityDictionary.put("maturitydate", "maturityDate");
             EntityDictionary.put("comittmentissuedate", "ComittmentIssueDate");
             EntityDictionary.put("comittmentacceptdate", "ComittmentAcceptDate");
             EntityDictionary.put("marsmortgagenumber", "MARSMortgageNumber");
             EntityDictionary.put("combinedtotalliabilities", "combinedTotalLiabilities");
             EntityDictionary.put("bankabanumber", "bankABANumber");
             EntityDictionary.put("bankaccountnumber", "bankAccountNumber");
             EntityDictionary.put("bankname", "bankName");
             EntityDictionary.put("actualclosingdate", "actualClosingDate");
             EntityDictionary.put("combinedtotalassets", "combinedTotalAssets");
             EntityDictionary.put("combinedtotalincome", "combinedTotalIncome");
             EntityDictionary.put("combinedtds", "combinedTDS");
             EntityDictionary.put("combinedgds", "combinedGDS");
             EntityDictionary.put("renewaldate", "renewalDate");
             EntityDictionary.put("numberofborrowers", "numberOfBorrowers");
             EntityDictionary.put("numberofguarantors", "numberOfGuarantors");
             EntityDictionary.put("servicingmortgagenumber", "servicingMortgageNumber");
             EntityDictionary.put("dealid", "dealId");
             EntityDictionary.put("paymenttermid", "paymentTermId");
             EntityDictionary.put("interesttypeid", "interestTypeId");
             EntityDictionary.put("paymentfequencyid", "paymentFrequencyId");
             EntityDictionary.put("prepaymentoptionsid", "prepaymentOptionsId");
             EntityDictionary.put("rateformulaid", "rateFormulaId");
             EntityDictionary.put("statusid", "statusId");
             EntityDictionary.put("riskratingid", "riskRatingId");
             EntityDictionary.put("electronicsystemsource", "electronicSystemSource");
             EntityDictionary.put("commitmentexpirationdate", "commitmentExpirationDate");
             EntityDictionary.put("requestedsolicitorname", "requestedSolicitorName");
             EntityDictionary.put("requestedappraisorname", "requestedAppraisorName");
             EntityDictionary.put("interestcompoundid", "interestCompoundId");
             EntityDictionary.put("investorprofileid", "investorprofileid");
             EntityDictionary.put("dealcol1", "dealCol1");
             EntityDictionary.put("secondapproverid", "secondApproverId");
             EntityDictionary.put("jointapproverid", "jointApproverId");
             EntityDictionary.put("sourcefirmprofileid", "sourceFirmProfileId");
             EntityDictionary.put("nproperty", "nProperty");
             EntityDictionary.put("property", "property");
             EntityDictionary.put("nborrower", "nBorrower");
             EntityDictionary.put("borrower", "borrower");

             EntityDictionary.put("bridgereference", "bridgeReference");
             EntityDictionary.put("pricingprofileid", "pricingingProfileId");
             EntityDictionary.put("refinancenewmoney", "refinanceNewMoney");
             EntityDictionary.put("interiminterestadjustmentdate", "interimInterestAdjustmentDate");
             EntityDictionary.put("reficurrentbalance", "refiCurrentBalance");
             EntityDictionary.put("privilegepaymentid", "privilegePaymentId");
             EntityDictionary.put("mitypeid", "MITypeId");
             EntityDictionary.put("downpaymentsourceid", "downpaymentSourceId");
             // borrower attributes
             EntityDictionary.put("borrowernumber", "borrowerNumber");
             EntityDictionary.put("borrowerfirstname", "borrowerFirstName");
             EntityDictionary.put("borrowermiddleinitial", "borrowerMiddleInitial");
             EntityDictionary.put("borrowerlastname", "borrowerLastName");
             EntityDictionary.put("firsttimebuyer", "firstTimeBuyer");
             EntityDictionary.put("borrowerbirthdate", "borrowerBirthDate");
             EntityDictionary.put("nsfoccurencetypeid", "nsfOccurenceTypeId");
             EntityDictionary.put("nsfoccurencetype", "nsfOccurenceType");
             EntityDictionary.put("borrowergender", "borrowerGender");
             EntityDictionary.put("salutationId", "salutationId");
             EntityDictionary.put("salutation", "salutation");
             EntityDictionary.put("maritalstatusid", "maritalStatusId");
             EntityDictionary.put("maritalstatus", "maritalStatus");
             EntityDictionary.put("socialinsurancenumber", "socialInsuranceNumber");
             EntityDictionary.put("borrowerhomephonenumber", "borrowerHomePhoneNumber");
             EntityDictionary.put("borrowerworkphonennumber", "borrowerWorkPhoneNumber");
             EntityDictionary.put("borrowerfaxnumber", "borrowerFaxNumber");
             EntityDictionary.put("borroweremailaddress", "borrowerEmailAddress");
             EntityDictionary.put("clientreferencenumber", "clientReferenceNumber");
             EntityDictionary.put("numberofdependents", "numberOfDependents");
             EntityDictionary.put("numberoftimesbankrupt", "numberOfTimesBankrupt");
             EntityDictionary.put("raceid", "raceId");
             EntityDictionary.put("race", "race");
             EntityDictionary.put("creditscore", "creditScore");
             EntityDictionary.put("gds", "GDS");
             EntityDictionary.put("tds", "TDS");
             EntityDictionary.put("educationlevelid", "educationLevelId");
             EntityDictionary.put("educationlevel", "educationLevel");
             EntityDictionary.put("networth", "netWorth");
             EntityDictionary.put("existingclient", "existingClient");
             EntityDictionary.put("creditsummary", "creditSummary");
             EntityDictionary.put("languagepreferenceid", "languagePreferenceId");
             EntityDictionary.put("languagepreference", "languagePreference");
             EntityDictionary.put("employee", "employee");
             EntityDictionary.put("borrowertypeid", "borrowerTypeId");
             EntityDictionary.put("borrowertype", "borrowerType");
             EntityDictionary.put("citizenshiptypeid", "citizenshipTypeId");
             EntityDictionary.put("ctizenshiptype", "citizenshipType");
             EntityDictionary.put("primaryborrowerflag", "primaryBorrowerFlag");
             EntityDictionary.put("totalassetamount", "totalAssetAmount");
             EntityDictionary.put("totalliabilityamount", "totalLiabilityAmount");
             EntityDictionary.put("paymenthistorytypeid", "paymentHistoryTypeId");
             EntityDictionary.put("paymenthistorytype", "paymentHistoryType");
             EntityDictionary.put("totalincomeamount", "totalIncomeAmount");
             EntityDictionary.put("nemploymenthistory", "nEmploymentHistory");
             EntityDictionary.put("employmenthistory", "employmentHistory");
             EntityDictionary.put("nborroweraddress", "nBorrowerAddress");
             EntityDictionary.put("borroweraddress", "borrowerAddress");
             EntityDictionary.put("nincome", "nIncome");
             EntityDictionary.put("income", "income");
             EntityDictionary.put("nliability", "nLiability");
             EntityDictionary.put("liability", "liability");
             EntityDictionary.put("nasset", "nAsset");
             EntityDictionary.put("asset", "asset");
             EntityDictionary.put("ncreditreference", "nCreditReference");
             EntityDictionary.put("creditreference", "creditReference");

             EntityDictionary.put("dateofcreditbureauprofile", "dateOfCreditBureauProfile");
             EntityDictionary.put("creditburealonfiledate", "creditBurealOnFileDate");
             EntityDictionary.put("bureauattachment", "bureauAttachment");
             EntityDictionary.put("creditbureauname", "creditBureauName");
             EntityDictionary.put("existingclientcomments", "existingClientComments");
             EntityDictionary.put("bankruptcystatusid", "bankruptcyStatusId");

             // income attributes
             EntityDictionary.put("incomeperiodid", "incomePeriodId");
             EntityDictionary.put("incomeperiod", "incomePeriod");
             EntityDictionary.put("incometypeid", "incomeTypeId");
             EntityDictionary.put("incometype", "incomeType");
             EntityDictionary.put("incomeid", "incomeId");
             EntityDictionary.put("incomeamount", "incomeAmount");

             // property attributes
             EntityDictionary.put("propertyiId", "propertyId");
             EntityDictionary.put("streettypeid", "streetTypeId");
             EntityDictionary.put("steettype", "streetType");
             EntityDictionary.put("propertytypeid", "propertyTypeId");
             EntityDictionary.put("propertytype", "propertyType");
             EntityDictionary.put("propertyusageid", "propertyUsageId");
             EntityDictionary.put("propertyusage", "propertyUsage");
             EntityDictionary.put("dwellingstyleid", "dwellingStyleId");
             EntityDictionary.put("dwellingstyle", "dwellingStyle");
             EntityDictionary.put("occupancytypeid", "occupancyTypeId");
             EntityDictionary.put("occupancytype", "occupancyType");
             EntityDictionary.put("propertystreetnumber", "propertyStreetNumber");
             EntityDictionary.put("propertystreetname", "propertyStreetName");
             EntityDictionary.put("heattypeid", "heatTypeId");
             EntityDictionary.put("heattype", "heatType");
             EntityDictionary.put("propertyaddressline2", "propertyAddressLine2");
             EntityDictionary.put("propertycity", "propertyCity");
             EntityDictionary.put("propertypostalfsa", "propertyPostalFSA");
             EntityDictionary.put("propertypostalldu", "propertyPostalLDU");
             EntityDictionary.put("legalline1", "legalLine1");
             EntityDictionary.put("legalline2", "legalLine2");
             EntityDictionary.put("legalline3", "legalLine3");
             EntityDictionary.put("numberofbedrooms", "numberOfBedrooms");
             EntityDictionary.put("yearbuilt", "yearBuilt");
             EntityDictionary.put("squarefootage", "squareFootage");
             EntityDictionary.put("insulatedwithuffi", "insulatedWithUFFI");
             EntityDictionary.put("watersewage", "waterSewage");
             EntityDictionary.put("provinceid", "provinceId");
             EntityDictionary.put("province", "province");
             EntityDictionary.put("annualtaxcost", "annualTaxCost");
             EntityDictionary.put("annualheatingcost", "annualHeatingCost");
             EntityDictionary.put("monthlycondofees", "monthlyCondoFees");
             EntityDictionary.put("purchaseprice", "purchasePrice");
             EntityDictionary.put("municipalityid", "municipalityId");
             EntityDictionary.put("municipality", "municipality");
             EntityDictionary.put("landvalue", "landValue");
             EntityDictionary.put("propertyvaluesource", "propertyValueSource");
//         EntityDictionary.put("appraisalsource", "appraisalSource");
             EntityDictionary.put("equityavailable", "equityAvailable");
             EntityDictionary.put("appraisaldate", "appraisalDate");
             EntityDictionary.put("newconstruction", "newConstruction");
             EntityDictionary.put("zoning", "zoning");
             EntityDictionary.put("urbansuburban", "urbanSuburban");
             EntityDictionary.put("propertystreetdirection", "propertyStreetDirection");
             EntityDictionary.put("estimatedappraisalvalue", "estimatedAppraisalValue");
             EntityDictionary.put("actualappraisalvalue", "actualAppraisalValue");
             EntityDictionary.put("propertyoccurancenumber", "propertyOccuranceNumber");
             EntityDictionary.put("loantovalue", "loanToValue");
             EntityDictionary.put("unitnumber", "unitNumber");
             EntityDictionary.put("lotsize", "lotSize");
             EntityDictionary.put("bankruptcystatusid", "bankruptcyStatusId");
             EntityDictionary.put("primarypropertyflag", "primaryPropertyFlag");

             // liability attributes
             EntityDictionary.put("liabilityid", "liabilityId");
             EntityDictionary.put("liabilitytypeid", "liabilityTypeId");
             EntityDictionary.put("labilitytype", "liabilityType");
             EntityDictionary.put("liabilityamount", "liabilityAmount");
             EntityDictionary.put("liabilitymonthlypayment",
                                  "liabilityMonthlyPayment");

             // asset
             EntityDictionary.put("assetid", "assetId");
             EntityDictionary.put("asetdescription", "assetDescription");
             EntityDictionary.put("assetvalue", "assetValue");
             EntityDictionary.put("includeingds", "includeIngDS");
             EntityDictionary.put("includeintds;", "includeIntDS");

             // creditreference
             EntityDictionary.put("creditreferenceid", "creditReferenceId");
             EntityDictionary.put("institutionname", "institutionName");
             EntityDictionary.put("accountnumber", "accountNumber");
             EntityDictionary.put("currentbalance", "currentBalance");
             EntityDictionary.put("creditreftypeid", "creditRefTypeId");
             EntityDictionary.put("creditreftype", "creditRefType");

             // borroweraddress
             EntityDictionary.put("borroweraddressid", "borrowerAddressId");
             EntityDictionary.put("borroweraddresstype", "borrowerAddressType");
             EntityDictionary.put("addrid", "addrId");
             EntityDictionary.put("addr", "addr");
             EntityDictionary.put("monthsataddress", "monthsAtAddress");
             EntityDictionary.put("todate", "toDate");
             EntityDictionary.put("fromdate", "fromDate");

             // employmenthistory
             EntityDictionary.put("employmenthistoryid", "employmentHistoryId");
             EntityDictionary.put("occupationid", "occupationId");
             EntityDictionary.put("occupation", "occupation");
             EntityDictionary.put("industrysectorid", "industrySectorId");
             EntityDictionary.put("industrysector", "industrySector");
             EntityDictionary.put("employername", "employerName");
             EntityDictionary.put("monthofservice", "monthOfService");
             EntityDictionary.put("employmenthistorynumber", "employmentHistoryNumber");
             EntityDictionary.put("contactid", "contactId");
             EntityDictionary.put("contact", "contact");
             EntityDictionary.put("employmenthistorytypeid", "employmentHistoryTypeId");

             // contact
             EntityDictionary.put("contactfirstname", "contactFirstName");
             EntityDictionary.put("contactmiddleinitial", "contactMiddleInitial");
             EntityDictionary.put("contactlastname", "contactLastName");
             EntityDictionary.put("contactphonenumber", "contactPhoneNumber");
             EntityDictionary.put("contactfaxnumber", "contactFaxNumber");
             EntityDictionary.put("contactemailaddress", "contactEmailAddress");
             EntityDictionary.put("jobtitle", "jobTitle");

             // addres
             EntityDictionary.put("addressline1", "addressLine1");
             EntityDictionary.put("addressline2", "addressLine2");
             EntityDictionary.put("city", "city");
             EntityDictionary.put("postalfsa", "postalFSA");
             EntityDictionary.put("postalldu", "postalLDU");

             // dealType
             EntityDictionary.put("dtdescription", "DTDescription");
             EntityDictionary.put("itdescription", "IPDescription");
             EntityDictionary.put("itdescription", "ITDescription");
             EntityDictionary.put("otdescription", "OTDescription");

             EntityDictionary.put("htdescription", "HTDescription");

             EntityDictionary.put("provincename", "provinceName");
             EntityDictionary.put("provinceabbreviation", "provinceAbbreviation");
             EntityDictionary.put("rdescription", "Rdescription");
             EntityDictionary.put("sdescription", "SDescription");
             EntityDictionary.put("eldescription", "ELDescription");
             EntityDictionary.put("sdescription", "SDescription");
             EntityDictionary.put("statusdescription", "StatusDescription");
             EntityDictionary.put("bgsdescription", "BGSDescription");
             EntityDictionary.put("ctdescription", "CTDescription");
             EntityDictionary.put("sdescription", "SDescription");
             EntityDictionary.put("odescription", "ODescription");
             EntityDictionary.put("crtdescription", "CRTDescription");
             EntityDictionary.put("defaultLender", "defaultLender");
             EntityDictionary.put("lendername", "lenderName");
             EntityDictionary.put("profilestatusid", "profileStatusId");
             EntityDictionary.put("profilestatus", "profileStatus");
             EntityDictionary.put("lpshortname", "lpShortName");
             EntityDictionary.put("lpbusinessid", "lpBusinessId");

             // mtgProd
             EntityDictionary.put("mtgprodname", "mtgProdName");
             EntityDictionary.put("minimumamount", "minimumamount");
             EntityDictionary.put("maximumamount", "maximumAmount");
             EntityDictionary.put("minimumltv", "minimumLTV");
             EntityDictionary.put("maximumltv", "maximumLTV");
             EntityDictionary.put("pricingprofileid", "pricingProfileId");
             EntityDictionary.put("pricingprofile", "pricingProfile");
             EntityDictionary.put("commitmentterm", "commitmentTerm");
             EntityDictionary.put("mpshortname", "mpShortName");
             EntityDictionary.put("mpbusinessid", "mpBusinessId");

             EntityDictionary.put("dpsdescription", "DPSDescription");
             EntityDictionary.put("drdescription", "DRDescription");
             EntityDictionary.put("rodescription", "RODescription");
             EntityDictionary.put("lpdescription", "LPDescription");
             EntityDictionary.put("SFDescription", "SFDescription");
             EntityDictionary.put("lobdescription", "LOBDescription");
             EntityDictionary.put("ldescription", "LDescription");
             EntityDictionary.put("phdescription", "PHDescription");
             EntityDictionary.put("ptdescription", "PTDescription");
             EntityDictionary.put("itdescription", "ITDescription");
             EntityDictionary.put("pudescription", "PUDescription");
             EntityDictionary.put("dsdescription", "DSDescription");
             EntityDictionary.put("isdescription", "ISDescription");
             EntityDictionary.put("rfdescription", "RFDescription");
             EntityDictionary.put("rrdescription", "RRDescription");
             EntityDictionary.put("municipalitycode", "municipalityCode");
             EntityDictionary.put("lpdescription", "LPdescription");
             EntityDictionary.put("nsfdescription", "NSFDescription");
             EntityDictionary.put("mortgageinsurernameription",
                                  "mortgageInsurerNameription");
             EntityDictionary.put("podescription", "PODescription");
             EntityDictionary.put("btdescription", "BTDescription");
             }
     */
    Object varName = EntityDictionary.get(token.toLowerCase());
    if(varName != null)
    {
      return(String)varName;
    }
    return token;
  }
}
