package com.basis100.BREngine;

import java.sql.SQLException;
/**
 * <p>Title: ComponentOverdraft</p>
 * <p>Description: BR Entity Class map to ComponentOverdraft </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 10, 2008)
 */
public class ComponentOverdraft extends DealEntity {

    protected int         componentId;
    protected int         copyId;
    protected double      overdraftAmount;


    /**
     * <p>Constructor</p>
     * @param interpretor
     * @param bTemplate
     * @throws SQLException
     */ 
    public ComponentOverdraft(BRInterpretor interpretor, boolean bTemplate) 
        throws SQLException {
        super(interpretor);
        if(bTemplate) {
        }
    }

    /**
     * <p>Constructor</P>
     * @param interpretor
     * @throws SQLException
     */ 
    public ComponentOverdraft(BRInterpretor interpretor) throws SQLException {
        super(interpretor);
        filter = null;
    
        if(Open()) {
            next();
        }
    }

    /**
     * <p>Constructor</p>
     * @param interpretor
     * @param anObj
     * @throws SQLException
     */
    public ComponentOverdraft(BRInterpretor interpretor, Object anObj) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + ((Component)anObj).componentId + " and copyId = " + ((Component)anObj).copyId;
    
        if(Open()) {
            next();
        }
    }
    
    /**
     * <p>Constructor</p>
     * @param interpretor
     * @param componentId
     * @param copyId
     * @throws SQLException
     */
    public ComponentOverdraft(BRInterpretor interpretor, int componentId, int copyId) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + componentId + " and copyId = " + copyId;
    
        if(Open()) {
            next();
        }
    }

    /**
    * <p>Constractor<p\/p>
    * @param interpretor
    * @param filter
    * @throws SQLException
    */
    public ComponentOverdraft(BRInterpretor interpretor, String filter) 
        throws SQLException {
    
        super(interpretor);
        this.filter = filter;
    
        if(Open()) {
            next();
        }
    }
    
    /**
    * <p>getPrimaryKeyName</p>
    */
    protected String getPrimaryKeyName()  {
        return "componentid";
    }
    
    /**
    * <p>deepCopy</p>
    * @return ComponentOverdraft
    */
    public ComponentOverdraft deepCopy() throws CloneNotSupportedException {
        return(ComponentOverdraft)this.clone();
    }

    /**
    * <p>getDefaultSQL</p>
    * @return String
    */
    protected String getDefaultSQL() {
        return "select * from ComponentOverdraft";
    }
    
    /* <p>next</p>
    * <p>Description: retrieve data from DB and set entity properties
    * @return boolean:true if data is exist, false if not
    */ 
    public boolean next() throws SQLException {
        
        boolean bStatus = super.next();
        if (!bStatus) return bStatus;
    
        componentId = resultSet.getInt("COMPONENTID");
        copyId = resultSet.getInt("COPYID");
        overdraftAmount = resultSet.getInt("OVERDRAFTAMOUNT");
    
        return bStatus;
    }
    
    
}
