// Business Rule Expression Scanner, add array
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class ForNode implements Cloneable
{
  private BRInterpretor interpretor;
  BRAssignNode initExpObj = null;
  BRExpNode loopExpObj = null;
  BRAssignNode incrExpObj = null;
  int nStmts = 0; // statment count
  List stmts = null; // String or ForNode object

  protected String errorMsg;

  public ForNode(BRInterpretor interpretor)
  {
    this.interpretor = interpretor;
    stmts = new ArrayList();
  }

  public ForNode deepCopy() throws ParseException
  {
    ForNode forNode;

    try
    {
      forNode = (ForNode)this.clone();
    }
    catch(CloneNotSupportedException e)
    {
      errorMsg = "CloneNotSupportedException caught!";
      throw new ParseException(errorMsg, 0);
    }
    return forNode;
  }

  public void display()
  {
  }

  public Object evaluate() throws ParseException, SQLException
  {
    int i;
    Object result = null, stmtObj;
    Class classObj;
    BRExpNode expObj = new BRExpNode(interpretor);
    BRVarNode varObj = new BRVarNode(interpretor);
    BRFunctNode functObj = new BRFunctNode(interpretor);
    BRAssignNode assignObj = new BRAssignNode(interpretor);
    IfNode ifNode = new IfNode(interpretor);
    ReturnNode returnNode = new ReturnNode(interpretor);
    BreakNode breakNode = new BreakNode(interpretor);
    ForNode forNode = new ForNode(interpretor);

    substituteUserVars();

    // evaulate init expression
    initExpObj.evaluate();
    while(evaluateLoopExpression())
    {
      for(i = 0; i < nStmts; ++i)
      {
        stmtObj = stmts.get(i);
        classObj = stmtObj.getClass();

        if(classObj.isInstance(expObj))
        {
          result = ((BRExpNode)stmtObj).evaluate();
        }
        else if(classObj.isInstance(varObj))
        {
          result = ((BRVarNode)stmtObj).evaluate();
        }
        else if(classObj.isInstance(functObj))
        {
          result = ((BRFunctNode)stmtObj).evaluate();
        }
        else if(classObj.isInstance(assignObj))
        {
          result = ((BRAssignNode)stmtObj).evaluate();
        }
        else if(classObj.isInstance(ifNode))
        {
          result = ((IfNode)stmtObj).evaluate();
        }
        else if(classObj.isInstance(forNode))
        {
          result = ((ForNode)stmtObj).evaluate();
        }
        else if(classObj.isInstance(breakNode))
        {
          return null;
        }
        else if(classObj.isInstance(returnNode))
        {
          return((ReturnNode)stmtObj).evaluate();
        }
        else
        {
          errorMsg = "Invalid node object <" + stmtObj + " detected";

          //--> Debug enhancement : by Billy 23Aug2004
          BRUtil.wran("Problem @ForNode.evaluate : " + errorMsg);
          //==========================================

          throw new ParseException(errorMsg, 0);
        }

        // return if returned node is a Return object
        //--> Buf Fixed by Billy 17June2004
        if(result != null)
        {
          classObj = result.getClass();
          if(classObj.isInstance(returnNode))
          {
            return((ReturnNode)result).evaluate();
          }
        }
        //=================================
      }

      // now evaluate the loop incrementexpression
      BRUtil.debug("BILLY ===> @evaluate incrExpObj :: " + incrExpObj.toString());

      incrExpObj.evaluate();
    }

    return result;
  }

  public Object evaluate(Object valueObj) throws SQLException
  {

    return null;
  }

  private boolean evaluateLoopExpression() throws ParseException
  {
    Boolean boolObj = new Boolean(false);

    Object result = loopExpObj.evaluate();
    Class classObj = result.getClass();

    //--> Debug enhancement : by Billy 23Aug2004
    BRUtil.debug("BILLY ===> @evaluateLoopExpression :: loopExpObj :: " + loopExpObj.toString() + "  ::  Result = " +
      result);
    //==========================================

    if(classObj.isInstance(boolObj))
    {
      return((Boolean)result).booleanValue();
    }

    return false;
  }

  public boolean parseExpressionPhase2() throws ParseException
  {
    int i;
    Object stmtObj;
    Class classObj;
    BRExpNode expObj = new BRExpNode(interpretor);
    BRAssignNode assignNode = new BRAssignNode(interpretor);
    ReturnNode returnNode = new ReturnNode(interpretor);
    ForNode forNode = new ForNode(interpretor);
    //--> Debug by Billy 17June2004
    BRFunctNode functNode = new BRFunctNode(interpretor);
    IfNode ifNode = new IfNode(interpretor);
    //==============================

    initExpObj.parseExpressionPhase2();
    loopExpObj.parseExpressionPhase2();
    incrExpObj.parseExpressionPhase2();

    for(i = 0; i < nStmts; ++i)
    {
      stmtObj = stmts.get(i);

      classObj = stmtObj.getClass();
      if(classObj.isInstance(expObj))
      {
        ((BRExpNode)stmtObj).parseExpressionPhase2();
      }
      else if(classObj.isInstance(assignNode))
      {
        ((BRAssignNode)stmtObj).parseExpressionPhase2();
      }
      else if(classObj.isInstance(forNode))
      {
        ((ForNode)stmtObj).parseExpressionPhase2();
      }
      else if(classObj.isInstance(returnNode))
      {
        ((ReturnNode)stmtObj).parseExpressionPhase2();
      }
      //--> Debug by Billy 17June2004
      else if(classObj.isInstance(functNode))
      {
        ((BRFunctNode)stmtObj).parseExpressionPhase2();
      }
      else if(classObj.isInstance(ifNode))
      {
        ((IfNode)stmtObj).parseExpressionPhase2();
      }
      //==============================
    }

    return true;
  }

  protected void substituteUserVars() throws ParseException
  {
    Class classObj;
    Object stmtObj;
    String strObj = "";
    BRExpNode expNode = new BRExpNode(interpretor);
    BRFunctNode functNode = new BRFunctNode(interpretor);
    BRAssignNode assignNode = new BRAssignNode(interpretor);
    IfNode ifNode = new IfNode(interpretor);
    ReturnNode returnNode = new ReturnNode(interpretor);
    ForNode forNode = new ForNode(interpretor);

    for(int i = 0; i < nStmts; ++i)
    {
      stmtObj = stmts.get(i);
      classObj = stmtObj.getClass();

      if(classObj.isInstance(expNode))
      {
        ((BRExpNode)stmtObj).substituteUserVars();
      }
      else if(classObj.isInstance(functNode))
      {
        ((BRFunctNode)stmtObj).substituteUserVars();
      }
      else if(classObj.isInstance(assignNode))
      {
        ((BRAssignNode)stmtObj).substituteUserVars();
      }
      else if(classObj.isInstance(ifNode))
      {
        ((IfNode)stmtObj).substituteUserVars();
      }
      else if(classObj.isInstance(returnNode))
      {
        ((ReturnNode)stmtObj).substituteUserVars();
      }
      else if(classObj.isInstance(forNode))
      {
        ((ForNode)stmtObj).substituteUserVars();
      }
      else if(classObj.isInstance(strObj))
      {
        strObj = (String)stmtObj;
        stmtObj = (String)interpretor.substituteUserVars(strObj,
          BRConstants.NO_DELIMITER);
        stmts.set(i, stmtObj);
      }
    }
  }
}
