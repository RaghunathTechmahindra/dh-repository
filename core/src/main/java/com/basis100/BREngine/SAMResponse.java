/*
 * SAMResponse.java 
 * 
 * Created: 14-MAR-2006
 * Author:  Alfredo Servilla
 */
package com.basis100.BREngine;

import java.sql.SQLException;

public class SAMResponse extends DealEntity {
	
	protected int    	responseId;
	protected String    samConfirmationNumber;
	protected int    	samDecisionId;
	protected int    	amountApproved;

	/**
	 * Creates a new entity
	 * @param interpretor
	 * @throws SQLException
	 */
	public SAMResponse(BRInterpretor interpretor) throws SQLException {
		super(interpretor);
	}

	/**
	 * Clone this SAMResponse
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */
	public SAMResponse deepCopy() throws CloneNotSupportedException {
		return (SAMResponse) this.clone();		
	}
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
	protected String getPrimaryKeyName() {
		return "responseId";
	}
	
	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
	protected String getDefaultSQL() {
		return "SELECT * FROM SAMResponse";
	}
	
	/**
	 * Closes the DB connection.
	 * @throws SQLException If an error occurs.
	 */
	protected void close() throws SQLException {
		super.close();
	}
	
	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
	protected boolean next() throws SQLException { 
		boolean bStatus = super.next();
		if (bStatus) {
			responseId = resultSet.getInt("responseId");
			samConfirmationNumber = resultSet.getString("samConfirmationNumber");
			samDecisionId = resultSet.getInt("samDecisionId");
			amountApproved = resultSet.getInt("amountApproved");
		}
		return bStatus;
	}
}
