// dynamic bind instance variables by its name
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class SourceOfBusinessProfile
  extends DealEntity
{
  protected int sourceOfBusinessProfileId;
  protected int lengthOfService;
  protected int contactId;
  protected int profileStatusId;
  protected int tierLevel;
  protected double compensationFactor;
  protected String SOBPShortName;
  protected String SOBPBusinessId;
  protected int sourceOfBusinessCategoryId;
  protected String alternativeId;
  protected int partyProfileId;
  //Added new fields -- By BILLY 27Jan2004
  protected String notes;
  protected int SOBRegionId;
  protected int sourceFirmProfileId;
  protected int systemTypeId;
  protected String sourceOfBusinessCode;
  protected int sourceOfBusinessPriorityId;
  //===========================================
  
  protected String sobLicenseRegistrationNumber;

  public Contact contact;

  protected String getPrimaryKeyName()
  {
    return "sourceOfBusinessProfileId";
  }

  public SourceOfBusinessProfile(BRInterpretor interpretor, boolean template) throws SQLException
  {
    super(interpretor);
  }

  public SourceOfBusinessProfile(BRInterpretor interpretor) throws SQLException
  {
    super(interpretor);
    filter = null;

    if(Open())
    {
      next();
    }
  }

  public SourceOfBusinessProfile(BRInterpretor interpretor, int anId) throws SQLException
  {
    super(interpretor);
    filter = getPrimaryKeyName() + "=" + anId;

    if(Open())
    {
      next();
    }
  }

  protected String getDefaultSQL()
  {
    return "select * from SourceOfBusinessProfile";
  }

  protected void close() throws SQLException
  {
    if(contact != null)
    {
      contact.close();

    }
    super.close();
  }

  protected boolean next() throws SQLException
  {
    boolean bStatus;
    String filter;

    bStatus = super.next();
    if(bStatus)
    {
      sourceOfBusinessProfileId = resultSet.getInt("SOURCEOFBUSINESSPROFILEID");
      lengthOfService = resultSet.getInt("LENGTHOFSERVICE");
      contactId = resultSet.getInt("CONTACTID");
      profileStatusId = resultSet.getInt("PROFILESTATUSID");
      tierLevel = resultSet.getInt("TIERLEVEL");
      compensationFactor = resultSet.getDouble("COMPENSATIONFACTOR");
      SOBPShortName = getString(resultSet.getString("SOBPSHORTNAME"));
      if(SOBPShortName != null)
      {
        SOBPShortName = SOBPShortName.trim();
      }
      SOBPBusinessId = getString(resultSet.getString("SOBPBUSINESSID"));
      if(SOBPBusinessId != null)
      {
        SOBPBusinessId = SOBPBusinessId.trim();
      }
      sourceOfBusinessCategoryId = resultSet.getInt("SOURCEOFBUSINESSCATEGORYID");
      alternativeId = getString(resultSet.getString("ALTERNATIVEID"));
      if(alternativeId != null)
      {
        alternativeId = alternativeId.trim();
      }
      partyProfileId = resultSet.getInt("PARTYPROFILEID");

      //Added new fields -- By BILLY 27Jan2004
      notes = getString(resultSet.getString("NOTES"));
      SOBRegionId = resultSet.getInt("SOBREGIONID");
      sourceFirmProfileId = resultSet.getInt("SOURCEFIRMPROFILEID");
      systemTypeId = resultSet.getInt("SYSTEMTYPEID");
      sourceOfBusinessCode = getString(resultSet.getString("SOURCEOFBUSINESSCODE"));
      sourceOfBusinessPriorityId = resultSet.getInt("SOURCEOFBUSINESSPRIORITYID");
      sobLicenseRegistrationNumber = resultSet.getString("SOBLICENSEREGISTRATIONNUMBER");

      //========================================
      // fetch contact record
      if(contact != null)
      {
        contact.close();
        //=========================================================
        // Contact has a copy ID field but sourceofbusinessprofile
        // does not. So we look for contactId with copy Id 1.
        // ========================================================
      }
      filter = "contactId=" + contactId + " and copyId = 1";
      contact = new Contact(interpretor, filter);
      contact.close();

    }
    return bStatus;
  }
}
