// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class PricingProfile extends DealEntity {
	protected  	int 	pricingProfileId ;
	protected  	String 	indexName ;
	protected  	int 	profileStatusId ;
	protected  	String 	ppBusinessId ;
	protected  	int 	interestRateChangeFrequency ;
	protected  	int 	pricingRegistrationTerm ;
	protected  	String 	primeIndexIndicator ;
	protected  	int 	pricingStatusId ;
	protected  	Date 	rateDisabledDate ;
	protected  	String 	PPDescription ;
	protected  	String 	rateCode ;
	protected  	String 	rateCodeDescription ;

   public String getPrimaryKeyName() {
      return "pricingProfileId";
   }

   public PricingProfile(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public PricingProfile(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public PricingProfile(BRInterpretor interpretor, int anId) throws SQLException {
      super(interpretor);
      filter = getPrimaryKeyName() + "=" + anId;

      if (Open())
         next();
   }

   public String getDefaultSQL() {
      return "select * from PricingProfile";
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				pricingProfileId  = resultSet.getInt("PRICINGPROFILEID");
				indexName  = getString(resultSet.getString("INDEXNAME"));
				if (indexName != null )
					indexName  =  indexName.trim();
				profileStatusId  = resultSet.getInt("PROFILESTATUSID");
				ppBusinessId  = getString(resultSet.getString("PPBUSINESSID"));
				if (ppBusinessId != null )
					ppBusinessId  =  ppBusinessId.trim();
				interestRateChangeFrequency  = resultSet.getInt("INTERESTRATECHANGEFREQUENCY");
				pricingRegistrationTerm  = resultSet.getInt("PRICINGREGISTRATIONTERM");
				primeIndexIndicator  = getString(resultSet.getString("PRIMEINDEXINDICATOR"));
				if (primeIndexIndicator != null )
					primeIndexIndicator  =  primeIndexIndicator.trim();
				pricingStatusId  = resultSet.getInt("PRICINGSTATUSID");
				rateDisabledDate  = asDate(resultSet.getTimestamp("RATEDISABLEDDATE"));
				PPDescription  = getString(resultSet.getString("PPDESCRIPTION"));
				if (PPDescription != null )
					PPDescription  =  PPDescription.trim();
				rateCode  = getString(resultSet.getString("RATECODE"));
				if (rateCode != null )
					rateCode  =  rateCode.trim();
				rateCodeDescription  = getString(resultSet.getString("RATECODEDESCRIPTION"));
				if (rateCodeDescription != null )
					rateCodeDescription  =  rateCodeDescription.trim();
         }

      return bStatus;
   }
}
