// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class CreditReference extends DealEntity {
	protected  	int 	creditReferenceId ;
	protected  	String 	institutionName ;
	protected  	String 	accountNumber ;
	protected  	double 	currentBalance ;
	protected  	int 	creditRefTypeId ;
	protected  	int 	borrowerId ;
	protected  	int 	copyId ;
	protected  	String 	creditReferenceDescription ;
	protected  	int 	timeWithReference ;

  public String getPrimaryKeyName() {
      return "creditReferenceId";
   }
   public CreditReference(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public CreditReference(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

   public CreditReference deepCopy() throws CloneNotSupportedException {
      return (CreditReference)this.clone();
   }

   public String getDefaultSQL() {
      return "select * from CreditReference";
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				creditReferenceId  = resultSet.getInt("CREDITREFERENCEID");
				institutionName  = getString(resultSet.getString("INSTITUTIONNAME"));
				if (institutionName != null )
					institutionName  =  institutionName.trim();
				accountNumber  = getString(resultSet.getString("ACCOUNTNUMBER"));
				if (accountNumber != null )
					accountNumber  =  accountNumber.trim();
				currentBalance  = resultSet.getDouble("CURRENTBALANCE");
				creditRefTypeId  = resultSet.getInt("CREDITREFTYPEID");
				borrowerId  = resultSet.getInt("BORROWERID");
				copyId  = resultSet.getInt("COPYID");
				creditReferenceDescription  = getString(resultSet.getString("CREDITREFERENCEDESCRIPTION"));
				if (creditReferenceDescription != null )
					creditReferenceDescription  =  creditReferenceDescription.trim();
				timeWithReference  = resultSet.getInt("TIMEWITHREFERENCE");
        }

      return bStatus;
   }
}
