// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Asset extends DealEntity {
	protected  	int 	assetId ;
	protected  	int 	borrowerId ;
	protected  	String 	assetDescription ;
	protected  	double 	assetValue ;
	protected  	int 	assetTypeId ;
	protected  	String 	includeInNetWorth ;
	protected  	int 	percentInNetWorth ;
	protected  	int 	copyId ;

  public String getPrimaryKeyName() {
      return "assetId";
   }

   public Asset(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public Asset(BRInterpretor interpretor, String whereClause) throws SQLException {
      super(interpretor);
      this.filter = whereClause;

      if (Open())
         next();
   }

   public Asset deepCopy() throws CloneNotSupportedException {
      return (Asset)this.clone();
   }

   public String getDefaultSQL() {
      return "select * from Asset";
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				assetId  = resultSet.getInt("ASSETID");
				borrowerId  = resultSet.getInt("BORROWERID");
				assetDescription  = getString(resultSet.getString("ASSETDESCRIPTION"));
				if (assetDescription != null )
					assetDescription  =  assetDescription.trim();
				assetValue  = resultSet.getDouble("ASSETVALUE");
				assetTypeId  = resultSet.getInt("ASSETTYPEID");
				includeInNetWorth  = getString(resultSet.getString("INCLUDEINNETWORTH"));
				if (includeInNetWorth != null )
					includeInNetWorth  =  includeInNetWorth.trim();
				percentInNetWorth  = resultSet.getInt("PERCENTINNETWORTH");
				copyId  = resultSet.getInt("COPYID");
        }
      return bStatus;
   }
}
