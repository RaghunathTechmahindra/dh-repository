// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class EscrowPayment extends DealEntity {
        protected       int     escrowPaymentId ;
        protected       int     copyId;
        protected       short   escrowTypeId;
        protected       int     dealId;
        protected       String  escrowPaymentDescription;
        protected       double  escrowPaymentAmount;

   protected String getPrimaryKeyName() {
      return "dealId";
   }

   public EscrowPayment(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public EscrowPayment(BRInterpretor interpretor, String whereClause) throws SQLException {
      super(interpretor);
      filter = whereClause;

      if (Open())
         next();
   }


   protected EscrowPayment deepCopy() throws CloneNotSupportedException {
      return (EscrowPayment)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from EscrowPayment";
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
         escrowPaymentId  = resultSet.getInt("ESCROWPAYMENTID");
         copyId  = resultSet.getInt("COPYID");
         escrowTypeId  = resultSet.getShort("ESCROWTYPEID");
         dealId  = resultSet.getInt("DEALID");
         escrowPaymentDescription  = getString(resultSet.getString("ESCROWPAYMENTDESCRIPTION"));
         if (escrowPaymentDescription != null )
            escrowPaymentDescription  =  escrowPaymentDescription.trim();

         escrowPaymentAmount  = resultSet.getDouble("ESCROWPAYMENTAMOUNT");
        }

      return bStatus;
   }

}
