/*
 * ServiceRequestContact.java 
 * 
 * Created: 20-FEB-2006
 * Author:  Edward Steel
 */
package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * @author ESteel
 */
public class ServiceRequestContact extends DealEntity {

	protected int requestId;
	protected int serviceRequestContactId;
	
	protected String firstName;
	protected String lastName;
	protected String emailAddress;
	
	protected String workAreaCode;
	protected String workPhoneNumber;
	protected String workExtension;
	
	protected String cellAreaCode;
	protected String cellPhoneNumber;
	
	protected String homeAreaCode;
	protected String homePhoneNumber;
	
	protected String comments;
	
	protected int copyId;
	protected int borrowerId;
    
    protected String propertyOwnerName;
	
	
	/**
	 * Creates a new entity
	 * @param interpretor
	 * @throws SQLException
	 */
	public ServiceRequestContact(BRInterpretor interpretor) throws SQLException {
		super(interpretor);
	}

	/**
	 * Clone this ServiceRequestContact
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */
	public ServiceRequestContact deepCopy() throws CloneNotSupportedException {
		return (ServiceRequestContact) this.clone();		
	}
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
	protected String getPrimaryKeyName() {
		return "serviceRequestContactId";
	}
	
	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
	protected String getDefaultSQL() {
		return "SELECT * FROM SERVICEREQUESTCONTACT";
	}
	
	/**
	 * Closes the DB connection.
	 * @throws SQLException If an error occurs.
	 */
	protected void close() throws SQLException {
		super.close();
	}
	
	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
	protected boolean next() throws SQLException { 
		boolean bStatus = super.next();
		
		if (bStatus) {
			requestId = resultSet.getInt("REQUESTID");
			serviceRequestContactId = resultSet.getInt("SERVICEREQUESTCONTACTID");			
			firstName = resultSet.getString("NAMEFIRST");
			lastName = resultSet.getString("NAMELAST");
			emailAddress = resultSet.getString("EMAILADDRESS");			
			workAreaCode = resultSet.getString("WORKAREACODE");
			workPhoneNumber = resultSet.getString("WORKPHONENUMBER");
			workExtension = resultSet.getString("WORKEXTENSION");
			cellAreaCode = resultSet.getString("CELLAREACODE");
			cellPhoneNumber = resultSet.getString("CELLPHONENUMBER");			
			homeAreaCode = resultSet.getString("HOMEAREACODE");
			homePhoneNumber = resultSet.getString("HOMEPHONENUMBER");			
			comments = resultSet.getString("COMMENTS");			
			copyId = resultSet.getInt("COPYID");
			borrowerId = resultSet.getInt("BORROWERID");
            propertyOwnerName = resultSet.getString("PROPERTYOWNERNAME");
		}
		
		return bStatus;
	}		
  
//----- START getters/ setters -------------------------------------------------
  
	/**
	 * @return Returns the borrowerId.
	 */
	public int getBorrowerId() {
		return borrowerId;
	}
	
	/**
	 * @return Returns the cellAreaCode.
	 */
	public String getCellAreaCode() {
		return cellAreaCode;
	}
	
	/**
	 * @return Returns the cellPhoneNumber.
	 */
	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}
	
	/**
	 * @return Returns the comments.
	 */
	public String getComments() {
		return comments;
	}
	
	/**
	 * @return Returns the copyId.
	 */
	public int getCopyId() {
		return copyId;
	}
	
	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	
	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @return Returns the homeAreaCode.
	 */
	public String getHomeAreaCode() {
		return homeAreaCode;
	}
	
	/**
	 * @return Returns the homePhoneNumber.
	 */
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}
	
	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * @return Returns the requestId.
	 */
	public int getRequestId() {
		return requestId;
	}
	
	/**
	 * @return Returns the serviceRequestContactId.
	 */
	public int getServiceRequestContactId() {
		return serviceRequestContactId;
	}
	
	/**
	 * @return Returns the workAreaCode.
	 */
	public String getWorkAreaCode() {
		return workAreaCode;
	}
	
	/**
	 * @return Returns the workExtension.
	 */
	public String getWorkExtension() {
		return workExtension;
	}
	
	/**
	 * @return Returns the workPhoneNumber.
	 */
	public String getWorkPhoneNumber() {
		return workPhoneNumber;
	}
	
    /**
     * @return Return the propertyOwnerName.
     */
    public String getPropertyOwnerName() {
        return propertyOwnerName;
    }
    
    
	/**
	 * @param borrowerId The borrowerId to set.
	 */
	public void setBorrowerId(int borrowerId) {
		this.borrowerId = borrowerId;
	}
	
	/**
	 * @param cellAreaCode The cellAreaCode to set.
	 */
	public void setCellAreaCode(String cellAreaCode) {
		this.cellAreaCode = cellAreaCode;
	}
	
	/**
	 * @param cellPhoneNumber The cellPhoneNumber to set.
	 */
	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}
	
	/**
	 * @param comments The comments to set.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	/**
	 * @param copyId The copyId to set.
	 */
	public void setCopyId(int copyId) {
		this.copyId = copyId;
	}
	
	/**
	 * @param emailAddress The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	/**
	 * @param firstName The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * @param homeAreaCode The homeAreaCode to set.
	 */
	public void setHomeAreaCode(String homeAreaCode) {
		this.homeAreaCode = homeAreaCode;
	}
	
	/**
	 * @param homePhoneNumber The homePhoneNumber to set.
	 */
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}
	
	/**
	 * @param lastName The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * @param requestId The requestId to set.
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	
	/**
	 * @param serviceRequestContactId The serviceRequestContactId to set.
	 */
	public void setServiceRequestContactId(int serviceRequestContactId) {
		this.serviceRequestContactId = serviceRequestContactId;
	}
	
	/**
	 * @param workAreaCode The workAreaCode to set.
	 */
	public void setWorkAreaCode(String workAreaCode) {
		this.workAreaCode = workAreaCode;
	}
	
	/**
	 * @param workExtension The workExtension to set.
	 */
	public void setWorkExtension(String workExtension) {
		this.workExtension = workExtension;
	}
	
	/**
	 * @param workPhoneNumber The workPhoneNumber to set.
	 */
	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}
    
    /**
     * @param propertyOwnerName The propertyOwnerName to set.
     */
    public void setPropertyOwnerName(String propertyOwnerName) {
        this.propertyOwnerName = propertyOwnerName;
    }
//------ END getters/ setters --------------------------------------------------	
}
