// You need to import the java.sql package to use JDBC
package com.basis100.BREngine;

import java.text.ParseException;


public class RuleEntity implements Cloneable
{
   public String  ruleName       = null;
   public String  ruleDesc       = null;
   public short   workflowId     = -1;
   public short   taskId         = -1;
   public String  taskEvent      = null;
   public String  ruleExpression = null;
   public short   rulePrivilege  = -1;
   public short   ruleOrder      = 1;
   public short   ruleState      = 0;
   public short   opCount        = 0;

   public short[]   opType       = new short[8];
   public short[]   opCat        = new short[8];
   public String[]  opValue      = new String[8];

   public String  extClassName   = null;
   public short   extClassType   = 0;
   public String  extMethod      = null;
   public short   procType       = 0;
   public String  actionProc     = null;
   public int   institutionProfileId = -1;
   
   public RuleEntity() {
      for (int i=0; i<8; ++i) {
         opType[i]  = 0;
         opCat[i]   = 0;
         opValue[i] = null;
         }
   }

   public RuleEntity deepCopy() throws ParseException {
      RuleEntity     entityNode;
      String         errorMsg;

      try {
         entityNode = (RuleEntity)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return entityNode;
   }

}

