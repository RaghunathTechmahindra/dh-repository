// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class LiabilityType extends DealEntity {
	protected  	int 	liabilityTypeId ;
	protected  	String 	LDescription ;
	protected  	double 	GDSInclusion ;
	protected  	double 	TDSInclusion ;
  protected   String LiabilityPaymentQualifier;
  protected   String PropertyExpenseEstimate;

   protected String getPrimaryKeyName() {
      return "liabilityTypeId";
   }
   public LiabilityType(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }
   public LiabilityType(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public LiabilityType(BRInterpretor interpretor, int anId) throws SQLException {
      super(interpretor);
      filter = getPrimaryKeyName() + "=" + anId;

      if (Open())
         next();
   }


   public LiabilityType deepCopy() throws CloneNotSupportedException {
      return (LiabilityType)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from LiabilityType";
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
      	liabilityTypeId  = resultSet.getInt("LIABILITYTYPEID");
				LDescription  = getString(resultSet.getString("LDESCRIPTION"));
				if (LDescription != null )
					LDescription  =  LDescription.trim();
				GDSInclusion  = resultSet.getDouble("GDSINCLUSION");
				TDSInclusion  = resultSet.getDouble("TDSINCLUSION");
        LiabilityPaymentQualifier = getString(resultSet.getString("LIABILITYPAYMENTQUALIFIER"));
        PropertyExpenseEstimate = getString(resultSet.getString("PROPERTYEXPENSEESTIMATE"));
        }

      return bStatus;
   }
}
