// Business Rule Expression Scanner, add array
/*
   2002-06-12   yml   add setItem(arrayObj, index, value) to allow user modification of
                      array element
   2002-06-13   yml   add array() and array(size)
   2002-06-13   yml   add addItem(arrayObj, element) support
   2002-06-13   yml   add deleteItem(index)
   2002-06-14   yml   add insertItem(arrayObj, 1-based-index, itemObj)
   2002-07-04   yml   add isLeapYear(object) and getDaysInMonth(object)
 */
package com.basis100.BREngine;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.BreakIterator;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class BRInterpretor
{

  public static boolean bLoaded = false;

  private static final char GLOBAL_SYMBOL_PREFIX = '$';
  private static final char LOCAL_SYMBOL_PREFIX = '%';

  private Map GlobalSymbols = null;
  private Map LocalSymbols = null;

  public Connection connObj = null;

// yml: 2000-08-23
  public Hashtable metaData = new Hashtable();

  int errorCode;
  Stack operands;
  Stack operators;
  Stack functions;
  String expression, prevToken, errorMsg;
  int leftParenthesisCount;
  int functionCount;
  BRFunctNode functObj = new BRFunctNode(this);
  List tokens = new ArrayList();
  Deal dealObj;
  private static Map SqlTypes = null;
  private String owner = null, tableName = null, colName = null;

  public Connection getConnection() throws SQLException
  {

    if(!bLoaded)
    {
      initialize_driver();

    }
    if(connObj == null)
    {
      initialize_connection();

    }
    return connObj;
  }

  public void setConnection(Connection aConnObj)
  {
    connObj = aConnObj;
    bLoaded = true;
  }

  public void closeConnection()
  {
    if(connObj != null)
    {
      try
      {
        connObj.close();
      }
      catch(Exception e)
      {
        ;
      }
    }

    connObj = null;
  }

  protected boolean isUserDefinedVar(String varName)
  {
    if(GlobalSymbols.containsKey(varName))
    {
      return true;
    }

    return LocalSymbols.containsKey(varName);
  }

  protected Object getUserDefinedVariable(String varName) throws ParseException
  {
    Object varObj = getGlobalSymbol(varName);
    if(varObj == null)
    {
      varObj = getLocalSymbol(varName);
    }
    return varObj;
  }

  public Object getGlobalSymbol(String strName)
  {
    Object varObj = null;
    String varName = strName;
    Class classObj;
    VarNode usrVarNode = new VarNode(this);

    if(varName.charAt(0) != GLOBAL_SYMBOL_PREFIX)
    {
      varName = "" + GLOBAL_SYMBOL_PREFIX + strName;

    }

    //--> Debug: Catherine, 22-Nov-05
    BRUtil.debug("BRInterpreter@getGlobalSymbol(): searching for symbol name = " + varName);
    //--> Debug: end
    
    if(GlobalSymbols.containsKey(varName))
    {

      //--> Debug: Catherine, 22-Nov-05
      BRUtil.debug("BRInterpreter@getGlobalSymbol(): found symbol = " + varName);
      //--> Debug: end

      varObj = GlobalSymbols.get(varName);
      classObj = varObj.getClass();
      if(classObj.isInstance(usrVarNode))
      {
        try
        {
          varObj = ((VarNode)varObj).evaluate();
        }
        catch(ParseException ex)
        {
          varObj = null;
        }
      }
    }

    return varObj;
  }

  public Object getLocalSymbol(String strName) throws ParseException
  {
    Object varObj = null;
    String varName = strName;
    Class classObj;
    VarNode usrVarNode = new VarNode(this);

    if(varName.charAt(0) != LOCAL_SYMBOL_PREFIX)
    {
      varName = "" + LOCAL_SYMBOL_PREFIX + strName;

    }
    if(LocalSymbols.containsKey(varName))
    {
      varObj = LocalSymbols.get(varName);
      classObj = varObj.getClass();
      if(classObj.isInstance(usrVarNode))
      {
        varObj = ((VarNode)varObj).evaluate();
      }
    }

    return varObj;
  }

  protected Object setIterateInstanceVariable(String token, Object anObj)
  {
    VarNode varNode;

    if(LocalSymbols.containsKey(token))
    {
      varNode = (VarNode)LocalSymbols.get(token);
    }
    else
    {
      varNode = new VarNode(this, token);
      LocalSymbols.put(token, varNode);
    }

    // set the actual object value
    varNode.varObj = anObj;

    return varNode;
  }

  public void setGlobalSymbol(String pSymName, Object pSymValue)
  {
    String symName = pSymName;
    if(symName.charAt(0) != GLOBAL_SYMBOL_PREFIX)
    {
      symName = "" + GLOBAL_SYMBOL_PREFIX + pSymName;

    }
    GlobalSymbols.put(symName, new VarNode(this, symName, pSymValue));
    //--> Debug: Catherine, 22-Nov-05
    BRUtil.debug("BRInterpreter@setGlobalSymbol(): new global symbol name (object) = " + symName );
    //--> Debug: end
  }

  protected void setDealObj(Deal dealObj)
  {
    this.dealObj = dealObj;
  }

  // basically for (unlikely) disaster treatment only -- i.e. likely never called!!
  public void setGlobalSymbol(String pSymName, VarNode varObj)
  {
    String symName = pSymName;
    if(symName.charAt(0) != GLOBAL_SYMBOL_PREFIX)
    {
      symName = "" + GLOBAL_SYMBOL_PREFIX + pSymName;

    }
    GlobalSymbols.put(symName, varObj);
    //--> Debug: Catherine, 22-Nov-05
    BRUtil.debug("BRInterpreter@setGlobalSymbol(): new global symbol name (node) = " + symName );
    //--> Debug: end
  }

  public void setUserTypeId(int userTypeId)
  {
    setGlobalSymbol("UserTypeId", new Integer(userTypeId));
  }

  public void setRecordId(int RecordId)
  {
    setGlobalSymbol("RecordId", new Integer(RecordId));
  }
  
  public void setRequestId(int RequestId)
  {
    setGlobalSymbol("RequestId", new Integer(RequestId));
  }

  public void setInstitutionProfileId(int instPId)
  {
    setGlobalSymbol("InstitutionProfileId", new Integer(instPId));
  }

  public void initGlobalSymbols()
  {
    GlobalSymbols = new HashMap();
    setGlobalSymbol("UserTypeId", null);
    setGlobalSymbol("RecordId", null);
    setGlobalSymbol("RequestId", null);
    setGlobalSymbol("InstitutionProfileId", null);
  }

  public void initLocalSymbols()
  {
    LocalSymbols = new HashMap();
  }

  public BRInterpretor()
  {

    // DON'T CALL INITIALIZE METHODS HERE!!!!

    operands = new Stack();
    operators = new Stack();
    leftParenthesisCount = 0;
    functionCount = 0;
    functions = new Stack();

    initGlobalSymbols();
    initLocalSymbols();

    dealObj = null;
  }

  public BRInterpretor(Deal dealObj)
  {
    operands = new Stack();
    operators = new Stack();
    leftParenthesisCount = 0;
    functionCount = 0;
    functions = new Stack();

    if(LocalSymbols != null)
    {
      LocalSymbols = null;

    }
    LocalSymbols = new HashMap();
    GlobalSymbols = new HashMap();
    setGlobalSymbol("UserTypeId", null);
    setGlobalSymbol("RecordId", null);
    setGlobalSymbol("RequestId", null);

    this.dealObj = dealObj;
  }

// yml: 2000-08-22
// new code for fetch support
  protected Object[] extractFetchedColumns(int colCount, ResultSet rSet, Sql_Column[] columns) throws SQLException
  {
    int i, index;
    Object[] rowCols = new Object[colCount];

    for(i = 0; i < colCount; ++i)
    {
      index = i + 1;
      switch(columns[i].sqlda.colType)
      {
        case Types.BIT:
          rowCols[i] = (Object)(new Boolean(rSet.getBoolean(index)));
          break;
        case Types.TINYINT:
        case Types.SMALLINT:
          rowCols[i] = (Object)(new Integer(rSet.getShort(index)));
          break;
        case Types.INTEGER:
          rowCols[i] = (Object)(new Integer(rSet.getInt(index)));
          break;
        case Types.NUMERIC:
          if(columns[i].sqlda.colScale == 0)
          {
            rowCols[i] = (Object)(new Integer(rSet.getInt(index)));
          }
          else
          {
            rowCols[i] = (Object)(new Double(rSet.getDouble(index)));
          }
          break;
        case Types.FLOAT:
        case Types.REAL:
        case Types.DECIMAL:
        case Types.DOUBLE:
          rowCols[i] = (Object)(new Double(rSet.getDouble(index)));
          break;
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
          rowCols[i] = rSet.getString(index);
          break;
        case Types.BINARY:
        case Types.VARBINARY:
        case Types.LONGVARBINARY:
          rowCols[i] = (Object)rSet.getBinaryStream(index);
          break;
        case Types.DATE:
          rowCols[i] = (Object)rSet.getDate(index);
          break;
        case Types.TIME:
          rowCols[i] = (Object)rSet.getTime(index);
          break;
        case Types.TIMESTAMP:
          rowCols[i] = (Object)rSet.getTimestamp(index);
          break;
        case Types.BLOB:
          rowCols[i] = (Object)rSet.getBlob(index);
          break;
        case Types.CLOB:
          rowCols[i] = (Object)rSet.getClob(index);
          break;
//               case Types.BIGINT:
//               case Types.DISTINCT:
        default:
      }
    }
    return rowCols;
  }

// yml: 2000-08-22
  protected Object fetchDBField(String sqlStmt) throws ParseException
  {
    int rowCount = 0;
    int colType, colPrecision, colScale;
    Statement queryStmt = null;
    ResultSet rSet = null;
    Object field = null;

    BRUtil.debug("@fetchDBField SQL Stmt: " + sqlStmt);

    try
    {
      if(connObj == null)
      {
        initialize_connection();

        // Create a Statement for query
      }
      queryStmt = connObj.createStatement();
      rSet = queryStmt.executeQuery(sqlStmt);
      ResultSetMetaData metaResult = rSet.getMetaData();
      Sql_Column[] rsMetaData = new Sql_Column[1];

      rsMetaData[0] = new Sql_Column();
      rsMetaData[0].sqlda = new SQLDA();

      rsMetaData[0].colIndex = 1;
      rsMetaData[0].sqlda.colName = metaResult.getColumnName(1);

      colType = metaResult.getColumnType(1);
      colPrecision = metaResult.getPrecision(1);
      colScale = metaResult.getScale(1);

      if(colType == Types.NUMERIC)
      {
        if(colScale == 0)
        {
          if(colPrecision > 4)
          {
            colType = Types.INTEGER;
          }
          else
          {
            colType = Types.SMALLINT;
          }
        }
        else
        {
          colType = Types.DOUBLE;
        }
      }

      rsMetaData[0].sqlda.colType = metaResult.getColumnType(1);
      rsMetaData[0].sqlda.colPrecision = metaResult.getPrecision(1);
      rsMetaData[0].sqlda.colScale = metaResult.getScale(1);
//         rsMetaData[0].sqlda.bNull = metaAResult.isNullable(1);

      // fetch only maxCount of rows to advoid blow up
      Object[] row = null;
      if(rSet.next())
      {
        row = extractFetchedColumns(1, rSet, rsMetaData);
        if(row == null)
        {
          return null;
        }
        rowCount = 1;
      }

      if(rowCount > 0)
      {
        field = row[0];
        if(field == null)
        {
          return null;
        }
        metaData.put(field, rsMetaData);
      }
    }
    catch(SQLException ex)
    {
      String errMsg = "SQL: " + sqlStmt + " msg: " + ex.getMessage();
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @fetchDBField SQL: " + sqlStmt + "::" + ex);
      //==========================================
      throw new ParseException(errMsg.trim(), 0);
    }
    finally
    {
      try
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
      }
      catch(Exception ex)
      {}
    }

    return field;
  }

// yml: 2000-08-21
// support for rows fetch
  protected Object[] fetchDBColumn(String sqlStmt) throws ParseException
  {
    int rowCount = 0;
    int colType, colPrecision, colScale;
    Statement queryStmt = null;
    ResultSet rSet = null;
    Object[] column = null;
    int maxCount = BRConstants.MAX_ROWS_PER_FETCH;
    Object[] rows = new Object[maxCount];

    BRUtil.debug("@fetchDBColumn SQL Stmt: " + sqlStmt);

    try
    {
      if(connObj == null)
      {
        initialize_connection();

        // Create a Statement for query
      }
      queryStmt = connObj.createStatement();
      rSet = queryStmt.executeQuery(sqlStmt);
      ResultSetMetaData metaResult = rSet.getMetaData();
      Sql_Column[] rsMetaData = new Sql_Column[1];

      rsMetaData[0] = new Sql_Column();
      rsMetaData[0].sqlda = new SQLDA();
      rsMetaData[0].colIndex = 1;
      rsMetaData[0].sqlda.colName = metaResult.getColumnName(1);

      colType = metaResult.getColumnType(1);
      colPrecision = metaResult.getPrecision(1);
      colScale = metaResult.getScale(1);

      if(colType == Types.NUMERIC)
      {
        if(colScale == 0)
        {
          if(colPrecision > 4)
          {
            colType = Types.INTEGER;
          }
          else
          {
            colType = Types.SMALLINT;
          }
        }
        else
        {
          colType = Types.DOUBLE;
        }
      }
      rsMetaData[0].sqlda.colType = colType;
      rsMetaData[0].sqlda.colPrecision = colPrecision;
      rsMetaData[0].sqlda.colScale = colScale;
//       rsMetaData[0].sqlda.bNull = metaAResult.isNullable(1);

      // fetch one column only
      while(rowCount < maxCount && rSet.next())
      {
        rows[rowCount] = extractFetchedColumns(1, rSet, rsMetaData);
        ++rowCount;
      }

      if(rowCount == 0)
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
        return null;
      }

      column = new Object[rowCount];
      Object[] row = null;
      for(int i = 0; i < rowCount; ++i)
      {
        row = (Object[])rows[i];
        if(i == 0 && row[0] == null)
        {
          return null;
        }
        column[i] = row[0];
      }
      metaData.put(column, rsMetaData);
    }
    catch(SQLException ex)
    {
      String errMsg = "SQL: " + sqlStmt + " msg: " + ex.getMessage();
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @fetchDBColumn SQL: " + sqlStmt + "::" + ex);
      throw new ParseException(errMsg.trim(), 0);
    }
    finally
    {
      try
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
      }
      catch(Exception ex)
      {}
    }

    return column;
  }

  protected Object[] fetchDBRow(String sqlStmt) throws ParseException
  {
    int i, rowCount = 0, colCount = 0, index;
    int colType, colPrecision, colScale;
    Statement queryStmt = null;
    ResultSet rSet = null;
    Object[] row = null;

    BRUtil.debug("@fetchDBRow SQL Stmt: " + sqlStmt);

    try
    {
      if(connObj == null)
      {
        initialize_connection();

        // Create a Statement for query
      }
      queryStmt = connObj.createStatement();
      rSet = queryStmt.executeQuery(sqlStmt);
      ResultSetMetaData metaResult = rSet.getMetaData();
      colCount = metaResult.getColumnCount();
      Sql_Column[] rsMetaData = new Sql_Column[colCount];

      for(i = 0; i < colCount; ++i)
      {
        rsMetaData[i] = new Sql_Column();
        rsMetaData[i].sqlda = new SQLDA();

        index = i + 1;
        rsMetaData[i].colIndex = index;
        rsMetaData[i].sqlda.colName = metaResult.getColumnName(index);

        colType = metaResult.getColumnType(index);
        colPrecision = metaResult.getPrecision(index);
        colScale = metaResult.getScale(index);

        if(colType == Types.NUMERIC)
        {
          if(colScale == 0)
          {
            if(colPrecision > 4)
            {
              colType = Types.INTEGER;
            }
            else
            {
              colType = Types.SMALLINT;
            }
          }
          else
          {
            colType = Types.DOUBLE;
          }
        }

        rsMetaData[i].sqlda.colType = colType;
        rsMetaData[i].sqlda.colPrecision = colPrecision;
        rsMetaData[i].sqlda.colScale = colScale;
//          rsMetaData[i].sqlda..bNull = metaAResult.isNullable(index);
      }

      // fetch only maxCount of rows to advoid blow up
      if(rSet.next())
      {
        row = extractFetchedColumns(colCount, rSet, rsMetaData);
        rowCount = 1;
      }

      if(rowCount == 0)
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
        return null;
      }
      if(row[0] == null)
      {
        return null;
      }

      metaData.put(row, rsMetaData);
    }
    catch(SQLException ex)
    {
      String errMsg = "SQL: " + sqlStmt + " msg: " + ex.getMessage();
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @fetchDBRow SQL: " + sqlStmt + "::" + ex);
      throw new ParseException(errMsg.trim(), 0);
    }
    finally
    {
      try
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
      }
      catch(Exception ex)
      {}
    }

    return row;
  }

// yml: 2000-08-21
// support for rows fetch
  protected Object[] fetchDBRows(String sqlStmt) throws ParseException
  {
    int i, rowCount = 0, colCount = 0, index;
    int colType, colPrecision, colScale;
    int maxCount = BRConstants.MAX_ROWS_PER_FETCH;
    Statement queryStmt = null;
    ResultSet rSet = null;
    Object[] rows = new Object[maxCount];
    Object[] actuals = null;

    BRUtil.debug("@fetchDBRows SQL Stmt: " + sqlStmt);

    try
    {
      if(connObj == null)
      {
        initialize_connection();

        // Create a Statement for query
      }
      queryStmt = connObj.createStatement();
      rSet = queryStmt.executeQuery(sqlStmt);

      ResultSetMetaData metaResult = rSet.getMetaData();
      colCount = metaResult.getColumnCount();
      Sql_Column[] rsMetaData = new Sql_Column[colCount];

      for(i = 0; i < colCount; ++i)
      {
        rsMetaData[i] = new Sql_Column();
        rsMetaData[i].sqlda = new SQLDA();

        index = i + 1;
        rsMetaData[i].colIndex = index;
        rsMetaData[i].sqlda.colName = metaResult.getColumnName(index);

        colType = metaResult.getColumnType(index);
        colPrecision = metaResult.getPrecision(index);
        colScale = metaResult.getScale(index);

        if(colType == Types.NUMERIC)
        {
          if(colScale == 0)
          {
            if(colPrecision > 4)
            {
              colType = Types.INTEGER;
            }
            else
            {
              colType = Types.SMALLINT;
            }
          }
          else
          {
            colType = Types.DOUBLE;
          }
        }
        rsMetaData[i].sqlda.colType = metaResult.getColumnType(index);
        rsMetaData[i].sqlda.colPrecision = metaResult.getPrecision(index);
        rsMetaData[i].sqlda.colScale = metaResult.getScale(index);
//          metaData[i].sqlda.bNull = metaAResult.isNullable(index);
      }

      // fetch only maxCount of rows to advoid blow up
      while(rowCount < maxCount && rSet.next())
      {
        rows[rowCount] = extractFetchedColumns(colCount, rSet, rsMetaData);
        ++rowCount;
      }

      if(rowCount == 0)
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
        return null;
      }

      if(rows[0] == null)
      {
        return null;
      }

      // trim off excess rows
      actuals = new Object[rowCount];
      Object[] row = null;
      for(i = 0; i < rowCount; ++i)
      {
        row = (Object[])rows[i];
        if(i == 0 && row[0] == null)
        {
          return null;
        }
        actuals[i] = row;
      }

      metaData.put(actuals, rsMetaData);
    }
    catch(SQLException ex)
    {
      String errMsg = "SQL: " + sqlStmt + " msg: " + ex.getMessage();
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @fetchDBRows SQL: " + sqlStmt + "::" + ex);
      throw new ParseException(errMsg.trim(), 0);
    }
    finally
    {
      try
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
      }
      catch(Exception ex)
      {}
    }

    return actuals;
  }

  protected Sql_Column[] fetchDBColumns(String selectClause, String filter) throws ParseException
  {
    int i, index;
    int colCount;
    String sqlStmt = "";
    Statement queryStmt = null;
    ResultSet rSet = null;
    ResultSetMetaData metaResult;
    Sql_Column[] columns = null;

    try
    {
      if(!bLoaded)
      {
        initialize_driver();

      }
      if(connObj == null)
      {
        initialize_connection();

        // Create a Statement for query
      }
      queryStmt = connObj.createStatement();

      sqlStmt = selectClause + " where " + filter;
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.debug("@fetchDBColumns SQL Stmt: " + sqlStmt);
      //==================================================
      rSet = queryStmt.executeQuery(sqlStmt);

      metaResult = rSet.getMetaData();
      colCount = metaResult.getColumnCount();

      columns = new Sql_Column[colCount];

      for(i = 0; i < colCount; ++i)
      {
        columns[i] = new Sql_Column();
        columns[i].sqlda = new SQLDA();

        index = i + 1;
        columns[i].colIndex = index;
        columns[i].sqlda.colType = metaResult.getColumnType(index);
        columns[i].sqlda.colPrecision = metaResult.getPrecision(index);
        columns[i].sqlda.colScale = metaResult.getScale(index);
        columns[i].sqlda.colName = metaResult.getColumnName(index);

//          column.bNull;
      }

      if(rSet.next())
      {
        for(i = 0; i < colCount; ++i)
        {
          index = i + 1;
          switch(columns[i].sqlda.colType)
          {
            case Types.BIT:
              columns[i].colValue = (Object)(new Boolean(rSet.getBoolean(index)));
              break;
            case Types.TINYINT:
            case Types.SMALLINT:
              columns[i].colValue = (Object)(new Integer(rSet.getShort(index)));
              break;
            case Types.INTEGER:
              columns[i].colValue = (Object)(new Integer(rSet.getInt(index)));
              break;
            case Types.NUMERIC:
              if(columns[i].sqlda.colScale == 0)
              {
                columns[i].colValue = (Object)(new Integer(rSet.getInt(index)));
              }
              else
              {
                columns[i].colValue = (Object)(new Double(rSet.getDouble(index)));
              }
              break;
            case Types.FLOAT:
            case Types.REAL:
            case Types.DECIMAL:
            case Types.DOUBLE:
              columns[i].colValue = (Object)(new Double(rSet.getDouble(index)));
              break;
            case Types.CHAR:
            case Types.VARCHAR:
            case Types.LONGVARCHAR:
              columns[i].colValue = (Object)rSet.getString(index);
              break;
            case Types.BINARY:
            case Types.VARBINARY:
            case Types.LONGVARBINARY:
              columns[i].colValue = (Object)rSet.getBinaryStream(index);
              break;
            case Types.DATE:
              columns[i].colValue = (Object)rSet.getDate(index);
              break;
            case Types.TIME:
              columns[i].colValue = (Object)rSet.getTime(index);
              break;
            case Types.TIMESTAMP:
              columns[i].colValue = (Object)rSet.getTimestamp(index);
              break;
            case Types.BLOB:
              columns[i].colValue = (Object)rSet.getBlob(index);
              break;
            case Types.CLOB:
              columns[i].colValue = (Object)rSet.getClob(index);
              break;
//                case Types.BIGINT:
//                case Types.DISTINCT:
            default:
          }
        }
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
        return columns;
      }
    }
    catch(SQLException ex)
    {
      String errMsg = "SQL: " + sqlStmt + " msg: " + ex.getMessage();
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @fetchDBColumns SQL: " + sqlStmt + "::" + ex);
      throw new ParseException(errMsg.trim(), 0);
    }
    finally
    {
      try
      {
        if(rSet != null)
        {
          rSet.close();
        }
        if(queryStmt != null)
        {
          queryStmt.close();
        }
      }
      catch(Exception ex)
      {}
    }

    return null;
  }

  public void destroyGlobalSymbol(String strName)
  {
    String varName = strName;

    if(varName.charAt(0) != GLOBAL_SYMBOL_PREFIX)
    {
      varName = "" + GLOBAL_SYMBOL_PREFIX + strName;

    }
    if(GlobalSymbols.containsKey(varName))
    {
      GlobalSymbols.remove(varName);
    }
  }

  protected Object execSQL(String strStmt) throws ParseException
  {
    int nAffected = 0;
    Statement execStmt = null;

    //--> Debug enhancement : by Billy 20Aug2004
    BRUtil.debug("@execSQL SQL Stmt: " + strStmt);
    //==================================================

    if (strStmt == null) return nAffected;
    
    try
    {
      if(connObj == null)
      {
        initialize_connection();

      }
      execStmt = connObj.createStatement();
      nAffected = execStmt.executeUpdate(strStmt);
    }
    catch(SQLException ex)
    {
      String errMsg = "SQL: " + strStmt + " msg: " + ex.getMessage();
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @execSQL SQL: " + strStmt + "::" + ex);
      throw new ParseException(errMsg.trim(), 0);
    }
    finally
    {
      try
      {
        if(execStmt != null)
        {
          execStmt.close();
        }
        execStmt = null;
      }
      catch(Exception ex)
      {}
    }

    return(new Integer(nAffected));
  }

  protected Object evaluateExpression(Object rootNode) throws ParseException
  {
    Object result;
    Class classObj;
    BRExpNode expNode = new BRExpNode(this);
    BRFunctNode functNode = new BRFunctNode(this);
    BRVarNode cachedVarNode = new BRVarNode(this);
    BRAssignNode assignNode = new BRAssignNode(this);
    VarNode usrVarNode = new VarNode(this);
    IfNode ifNode = new IfNode(this);
    DoNode doNode = new DoNode(this);
    String strObj = "";

    try
    {
      classObj = rootNode.getClass();
      if(classObj.isInstance(expNode))
      {
        result = ((BRExpNode)rootNode).evaluate();
      }
      else if(classObj.isInstance(functNode))
      {
        result = ((BRFunctNode)rootNode).evaluate();
      }
      else if(classObj.isInstance(cachedVarNode))
      {
        result = ((BRVarNode)rootNode).evaluate();
      }
      else if(classObj.isInstance(usrVarNode))
      {
        result = ((VarNode)rootNode).evaluate();
      }
      else if(classObj.isInstance(assignNode))
      {
        result = ((BRAssignNode)rootNode).evaluate();
      }
      else if(classObj.isInstance(ifNode))
      {
        result = ((IfNode)rootNode).evaluate();
      }
      else if(classObj.isInstance(doNode))
      {
        result = ((DoNode)rootNode).evaluate();
      }
      else if(classObj.isInstance(strObj))
      {
        result = rootNode;
      }
      else
      {
        errorMsg = "Syntax error, expression: \"" + expression
          + "\" error: Invalid node object \"" + rootNode + "\" detected!";
        errorCode = -1;
        //--> Debug enhancement : by Billy 20Aug2004
        BRUtil.error("Exception @evaluateExpression :: msg = " + errorMsg);
        //==========================================
        throw new ParseException(errorMsg, 0);
      }
    }
    catch(NullPointerException e)
    {
      errorMsg = "Unknown Interpretor error, expression: \"" + expression + ", @Node: [" + rootNode
        + "\" error: NullPointerExceptioin detected during Phase 2 Parsing!";
      errorCode = -1;
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @evaluateExpression :: msg = " + errorMsg);
      BRUtil.error(BRUtil.stack2string(e));
      //==========================================

      throw new ParseException(errorMsg, 0);
    }
    catch(ParseException e)
    {
      throw e;
    }
    catch(Exception e)
    {
      errorMsg = "Unknown Interpretor error: " + e + "!";
      errorCode = -1;
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @evaluateExpression :: msg = " + errorMsg);
      BRUtil.error(BRUtil.stack2string(e));
      //==========================================

      throw new ParseException(errorMsg, 0);
    }
    return result;
  }

  protected boolean isSelectWildCard(BRFunctNode currentNode, String token)
  {
    //boolean bStatus = false;
    String functName = currentNode.functName;

    if(token.equalsIgnoreCase("*"))
    {
      if(functName.equalsIgnoreCase("fetchRows"))
      {
        //bStatus = true;
      }
      else if(functName.equalsIgnoreCase("fetchRow"))
      {
        //bStatus = true;
      }
      else if(functName.equalsIgnoreCase("fetchColumn"))
      {
        //bStatus = true;
      }
      else if(functName.equalsIgnoreCase("fetchField"))
      {
        //bStatus = true;
      }
    }

    return false;
  }

  protected boolean isStmtKeyword(String token)
  {
    if(token.length() <= 0)
    {
      return false;
    }
    if(token.equalsIgnoreCase("else"))
    {
      return true;
    }

    return false;
  }

  protected boolean isTerminator(String token)
  {
    if(token.length() <= 0)
    {
      return false;
    }

    switch(token.charAt(0))
    {
      case ';':
        return true;
      case ')':
        if(leftParenthesisCount > 0)
        {
          return false;
        }
        return true;
    }

    return false;
  }

  protected boolean isUnaryOperator(String token)
  {
    char curOpCode;

    curOpCode = token.charAt(0);

    switch(curOpCode)
    {
      case '+':
      case '-':
        break;
      default:
        //
    }
    return true;
  }

  protected boolean isUnsupportedOpertor(String token)
  {
    if(token.length() <= 0)
    {
      return false;
    }
    char aChar = token.charAt(0);

    switch(aChar)
    {
      case '[':
      case ']':
      case ',':
        return true;
    }
    return false;
  }

  protected boolean isAssignmentOperator(String token)
  {
    if(token.length() <= 0)
    {
      return false;
    }
    if(token.compareTo(":=") == 0)
    {
      return true;
    }
    return false;
  }

  protected boolean isLocalVariable(String token) throws ParseException
  {
    if(token.length() <= 0)
    {
      return false;
    }
    return token.charAt(0) == LOCAL_SYMBOL_PREFIX;
  }

  protected boolean isGlobalVariable(String token) throws ParseException
  {
    if(token.length() <= 0)
    {
      return false;
    }
    if(token.charAt(0) != GLOBAL_SYMBOL_PREFIX)
    {
      return false;
    }

    // if variable not in hask, define it.
    if(!GlobalSymbols.containsKey(token))
    {
      setGlobalSymbol(token, null);

    }
    return true;
  }

  private boolean isNegativeNumber(String token)
  {
    if(token.length() <= 0)
    {
      return false;
    }
    if(token.charAt(0) != '-')
    {
      return false;
    }

    if(tokens.size() < 2)
    {
      return false;
    }
    String nextToken = (String)tokens.get(1);
    char digit = nextToken.charAt(0);
    return(digit >= '0' && digit <= '9');
  }

  protected boolean isOperator(String token)
  {
    int length = token.length();

    if(length <= 0)
    {
      return false;
    }

    switch(token.charAt(0))
    {
      case '(': // right bracket
        if(length == 1)
        {
          return true;
        }
        return false;
      case '+': // plus operator
      case '-': // minus operator
      case '*': // muliply operator
      case '/': // divide operator
      case '>': // greater than bracket
      case '<': // less than operator
      case ')': // left parenthesis
      case '&': // boolean and opeator
      case '|': // boolean or opeator
      case '=': // equal operator
      case '!': // not or not equal operator
        return true;
      default:
        if(token.compareToIgnoreCase("AND") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("OR") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("XOR") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("EQ") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("NE") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("GT") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("GE") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("LT") == 0)
        {
          return true;
        }
        else if(token.compareToIgnoreCase("LE") == 0)
        {
          return true;
        }
    }

    return false;
  }

  protected boolean illegalOperatorSyntax(String token)
  {
    char curChar = token.charAt(0);

    // check local/global symbol
    if(curChar == GLOBAL_SYMBOL_PREFIX || curChar == LOCAL_SYMBOL_PREFIX)
    {
      return false;
    }

    int size;
    char prevChar;
    String prevToken;

    size = tokens.size();
    if(size-- > 0)
    {
      prevToken = (String)tokens.get(size);
      if(prevToken.length() > 0)
      {
        prevChar = prevToken.charAt(0);
      }
      else
      {
        prevChar = ' ';
      }
    }
    else
    {
      prevChar = ' ';

    }
    switch(curChar)
    {
      case '<':
        switch(prevChar)
        {
          case '<': // << is illegal
          case '>': // >< is illegal
          case '+': // +< is illegal
          case '-': // -< is illegal
          case '*': // *< is illegal
          case '/': // /< is illegal
          case '=': // =< is illegal
          case '!': // != is illegal
            return true;
        }
        break;
      case '>':
        switch(prevChar)
        {
          case '<': // <> is legal
            break;
          case '>': // >> is illegal
          case '+': // +> is illegal
          case '-': // -> is illegal
          case '*': // *> is illegal
          case '/': // /> is illegal
          case '=': // => is illegal
          case '!': // !> is illegal
            return true;
        }
        break;
      case '+':
        switch(prevChar)
        {
          case '+': // ++ is illegal
          case '-': // -+ is illegal
          case '*': // *+ is illegal
          case '/': // /+ is illegal
          case '>': // >+ is illegal
          case '<': // <+ is illegal
          case '=': // =+ is illegal
          case '!': // !+ is illegal
            return true;
        }
        break;
      case '-':
        switch(prevChar)
        {
          case '+': // +- is legal
          case '-': // -- is legal
          case '*': // *- is legal
          case '/': // /- is legal
          case '>': // >- is legal
          case '<': // <- is legal
          case '=': // =- is legal
            break;
          case '!': // !- is illegal
            return true;
        }
        break;
      case '*':
        switch(prevChar)
        {
          case '+': // +* is illegal
          case '-': // -* is illegal
          case '*': // ** is illegal
//               case '/':      // /* is illegal
          case '>': // >* is illegal
          case '<': // <* is illegal
          case '=': // =* is illegal
          case '!': // !* is illegal
            return true;
        }
        break;
      case '/':
        switch(prevChar)
        {
          case '+': // +/ is illegal
          case '-': // -/ is illegal
//               case '*':      // */ is illegal
          case '/': // // is illegal
          case '>': // >/ is illegal
          case '<': // </ is illegal
          case '=': // =/ is illegal
          case '!': // !/ is illegal
            return true;
        }
        break;
      case ':':
        break;
      case '=':
        switch(prevChar)
        {
          case '<': // <= is legal
          case '>': // >= is legal
          case '!': // != is legal
          case ':': // := is legal
            break;
          case '=': // == is illegal
          case '+': // += is illegal
          case '-': // -= is illegal
          case '*': // *= is illegal
          case '/': // /= is illegal
            return true;
        }
        break;
      case '!':
        switch(prevChar)
        {
          case '<': // <! is illegal
          case '>': // >! is illegal
          case '=': // =! is illegal
          case '+': // +! is illegal
          case '-': // -! is illegal
          case '*': // *! is illegal
          case '/': // /! is illegal
            return true;
        }
        break;
      case '$': // following illegal (if not prefix for global or local symbol
      case '%':
      case '@':
      case '#':
      case '^':
      case '&':
      case '|':
//         case '{':
//         case '}':
//         case ';':    // now supported as stmt terminator
      case '~':
      case '`':
      case '?':
        return true;
      default:
    }

    return false;
  }

  public static void initialize_driver() throws SQLException
  {
    String driver_class = "oracle.jdbc.driver.OracleDriver";
    String errorMsg;

    if(!bLoaded)
    {
      // Load the Oracle JDBC driver
      try
      {
        Class.forName(driver_class);
      }
      catch(ClassNotFoundException e)
      {
        errorMsg = "Unable to load database driver: " + driver_class
          + "!";
        throw new SQLException(errorMsg);
      }
//         DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
      bLoaded = true;
    }
  }

  public void initialize_connection() throws SQLException
  {
    if(connObj == null)
    {
      throw new SQLException("Unable to establish a database connection!");
    }
  }

  protected SQLDA getColumnType(String owner, String tableName, String colName) throws ParseException
  {
    SQLDA sqlDA = new SQLDA();
    String strTemp, errorMsg, sqlStmt, filter = "";

    try
    {
      if(owner != null && owner.length() > 0)
      {
        filter += "owner=\'" + owner.toUpperCase() + "\'";
      }
      if(tableName != null && tableName.length() > 0)
      {
        if(filter.length() > 0)
        {
          filter += " and ";
        }
        filter += "table_name=\'" + tableName.toUpperCase() + "\'";
      }
      if(colName != null && colName.length() > 0)
      {
        if(filter.length() > 0)
        {
          filter += " and ";
        }
        filter += "column_name=\'" + colName.toUpperCase() + "\'";
      }

      sqlStmt = "select data_type,data_length,nullable,data_precision,data_scale "
        + " from all_tab_columns ";
      if(filter.length() > 0)
      {
        sqlStmt += " where " + filter;
      }

      if(SqlTypes == null)
      {
        getSqlDataTypes();

      }
      Statement stmt = connObj.createStatement();
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.debug("@getColumnType SQL Stmt: " + sqlStmt);
      //==================================================
      ResultSet rs = stmt.executeQuery(sqlStmt);
      if(rs.next())
      {
        Object dataType = SqlTypes.get(rs.getString(1));

        sqlDA.colName = colName;
        sqlDA.colType = (short)(((Short)dataType).intValue());
        sqlDA.colLength = rs.getShort(2);
        strTemp = rs.getString(3);
        sqlDA.colNullable = strTemp.charAt(0) == 'Y' ? true : false;
        sqlDA.colPrecision = rs.getShort(4);
        sqlDA.colScale = rs.getShort(5);

        return sqlDA;
      }
      rs.close();
      stmt.close();
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 20Aug2004
      BRUtil.error("Exception @getColumnType ::" + e);
    }

    errorMsg = "Invalid column: \"";
    if(owner != null)
    {
      errorMsg += owner + ".";
    }
    if(tableName == null)
    {
      errorMsg += "null.";
    }
    else
    {
      errorMsg += tableName + ".";
    }
    if(colName == null)
    {
      errorMsg += "null";
    }
    else
    {
      errorMsg += colName;

    }
    errorMsg += "\" specified!";
    //--> Debug enhancement : by Billy 20Aug2004
    BRUtil.error("Exception @getColumnType msg : " + errorMsg);
    //==========================================
    throw new ParseException(errorMsg, 0);
  }

  protected void getSqlDataTypes() throws SQLException
  {
    short dataType;
    String typeName;

    if(SqlTypes != null)
    {
      return;
    }

    SqlTypes = new HashMap();

    // retrieve the meta data object
    DatabaseMetaData metaData = connObj.getMetaData();

    // retrieve columns info
    ResultSet rset = metaData.getTypeInfo();

    while(rset.next())
    {
      typeName = rset.getString("TYPE_NAME");
      dataType = rset.getShort("DATA_TYPE");
      SqlTypes.put(typeName, new Short(dataType));
    }
    rset.close();
  }

// yml: 2000-08-23
  public Sql_Column[] getMetaData(Object rsObj)
  {
    return(Sql_Column[])metaData.get(rsObj);
  }

  // ALERT ::: CAN BE MORE THAN ONE CONSTRAINT
  protected String getPrimaryKey(String owner, String tableName) throws ParseException
  {
    String errorMsg, sqlStmt, filter = "position=1 ";
    String columnName;

    try
    {
      if(owner != null && owner.length() > 0)
      {
        filter += "and owner=\'" + owner.toUpperCase() + "\'";
      }
      if(tableName != null && tableName.length() > 0)
      {
        filter += " and table_name=\'" + tableName.toUpperCase() + "\'";
      }

      sqlStmt = "select column_name from all_cons_columns ";
      if(filter.length() > 0)
      {
        sqlStmt += " where " + filter;
      }

      Statement stmt = connObj.createStatement();
      ResultSet rs = stmt.executeQuery(sqlStmt);
      if(rs.next())
      {
        columnName = rs.getString(1);
      }
      else
      {
        columnName = null;

      }
      rs.close();
      stmt.close();
      return columnName;
    }
    catch(SQLException e)
    {
    }

    errorMsg = "Invalid column: \"";
    if(owner != null)
    {
      errorMsg += owner + ".";
    }
    if(tableName == null)
    {
      errorMsg += "null";
    }
    else
    {
      errorMsg += tableName;

    }
    errorMsg += "\" specified!";
    //--> Debug enhancement : by Billy 20Aug2004
    BRUtil.error("Problem @getPrimaryKey : " + errorMsg);
    throw new ParseException(errorMsg, 0);
  }

  protected SQLDA[] getPrimaryKeys(String owner, String tableName) throws ParseException, SQLException
  {
    int i, count = 0;
    String sqlStmt;
    Statement qryStmt;
    ResultSet rSet;
    Stack sqldaStack = new Stack();
    SQLDA sqlda = null;
    SQLDA[] sqldas = null;

    if(!bLoaded)
    {
      initialize_driver();

    }
    if(connObj == null)
    {
      initialize_connection();

      // Create a Statement for query
    }
    qryStmt = connObj.createStatement();

    sqlStmt = "select column_name from all_cons_columns where ";

    if(owner != null)
    {
      sqlStmt += "owner=\'" + owner.toUpperCase() + "\' and ";
    }

    sqlStmt += "table_name=\'" + tableName.toUpperCase()
      + "\' and position is not null ";
    sqlStmt += "order by position";
    //--> Debug enhancement : by Billy 23Aug2004
    BRUtil.debug("@getPrimaryKeys SQL Stmt: " + sqlStmt);
    //==================================================

    rSet = qryStmt.executeQuery(sqlStmt);

    while(rSet.next())
    { // fetch all primary sub keys
      sqlda = new SQLDA();
      sqlda.colName = rSet.getString(1);
      sqldaStack.push(sqlda);
      ++count;
    }

    // free sql resources
    rSet.close();
    qryStmt.close();
    sqldas = new SQLDA[count];

    for(i = 0; i < count; ++i)
    {
      sqldas[i] = (SQLDA)sqldaStack.elementAt(i);
      sqlda = getColumnType(owner, tableName, sqldas[i].colName);
      sqldas[i].colType = sqlda.colType;
      sqldas[i].colLength = sqlda.colLength;
      sqldas[i].colPrecision = sqlda.colPrecision;
      sqldas[i].colScale = sqlda.colScale;
      sqldas[i].colNullable = sqlda.colNullable;
    }

    return sqldas;
  }

  protected boolean isBusineRuleStmt(String aToken)
  {
    char char1;
    String token;

    if(aToken.length() <= 0)
    {
      return false;
    }
    token = aToken.toUpperCase();
    char1 = token.charAt(0);

    switch(char1)
    {
      case 'I':
        if(token.compareTo("IF") == 0)
        {
          return true;
        }
        break;
      default:
    }

    return false;
  }

  protected boolean isDBColumn(String owner, String tableName, String colName)
  {
    boolean bStatus = false;
    String sqlStmt, filter = "";
    Statement stmt;
    ResultSet rs;

    if(owner != null && owner.length() > 0)
    {
      filter += "owner=\'" + owner.toUpperCase() + "\'";
    }
    if(tableName != null && tableName.length() > 0)
    {
      if(filter.length() > 0)
      {
        filter += " and ";
      }
      filter += "table_name=\'" + tableName.toUpperCase() + "\'";
    }
    if(colName != null && colName.length() > 0)
    {
      if(filter.length() > 0)
      {
        filter += " and ";
      }
      filter += "column_name=\'" + colName.toUpperCase() + "\'";
    }

    sqlStmt = "select count(column_name) from all_tab_columns";
    if(filter.length() > 0)
    {
      sqlStmt += " where " + filter;
    }

    try
    {
      if(!bLoaded)
      {
        initialize_driver();

      }
      if(connObj == null)
      {
        initialize_connection();

      }
      stmt = connObj.createStatement();
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.debug("@isDBColumn SQL Stmt: " + sqlStmt);
      //==================================================

      rs = stmt.executeQuery(sqlStmt);
      bStatus = rs.next();
      if(rs.getInt(1) <= 0)
      {
        bStatus = false;

      }
      rs.close();
      stmt.close();
    }
    catch(SQLException e)
    {
      bStatus = false;
    }

    return bStatus;
  }

  protected boolean isDBEntity(String token) throws ParseException
  {
    int count = 0, length;
    int start, end;
    String tempToken;
    if(token.length() <= 0)
    {
      return false;
    }
    if(BRUtil.isInteger(token))
    {
      return false;
    }
    if(BRUtil.isFloat(token))
    {
      return false;
    }

    start = 0;

    length = token.length();
    end = length;
    while(end > 0)
    {
      end = token.indexOf('.', start);
      if(start == 0 && end < 0)
      {
        return false;
      }
      if(end >= 0)
      {
        tempToken = token.substring(start, end);
        if(BRUtil.isInteger(tempToken))
        {
          return false;
        }
        switch(count)
        {
          case 0:
            owner = tempToken;
            break;
          case 1:
            tableName = tempToken;
            break;
          case 2:
            colName = tempToken;
            break;
          default: // too many fields, owner.table.column
            break;
        }
      }
      else
      {
        colName = token.substring(start, length);
        if(count == 1)
        {
          tableName = owner;
          owner = null;
        }

        if(!isDBTable(owner, tableName))
        {
          return false;
        }
        return isDBColumn(owner, tableName, colName);
      }

      ++count;
      start = end + 1;
    }

    return false;
  }

  protected boolean isDBTable(String owner, String tableName)
  {
    boolean bStatus;
    String sqlStmt, filter = "";

    try
    {
      if(owner != null && owner.length() > 0)
      {
        filter += "owner=\'" + owner.toUpperCase() + "\'";
      }
      if(tableName != null && tableName.length() > 0)
      {
        if(filter.length() > 0)
        {
          filter += " and ";
        }
        filter += "table_name=\'" + tableName.toUpperCase() + "\'";
      }

      if(!bLoaded)
      {
        initialize_driver();

      }
      if(connObj == null)
      {
        initialize_connection();

      }
      sqlStmt = "select count(table_name) from all_catalog ";
      if(filter.length() > 0)
      {
        sqlStmt += " where " + filter;
      }

      Statement stmt = connObj.createStatement();
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.debug("@isDBTable SQL Stmt: " + sqlStmt);
      //==================================================

      ResultSet rs = stmt.executeQuery(sqlStmt);
      bStatus = rs.next();
      rs.close();
      stmt.close();
    }
    catch(SQLException e)
    {
      bStatus = false;
    }
    return bStatus;
  }

// yml: 2000-08-22
// 2002-06-12: add setItem(arrayObj, 1-based index, value)
// 2002-06-13: add array([nSize]), addItem(arrayObj, element) and deleteItem(array, index);
// 2002-06-14: add insertItem(arrayObj, 1-based-index, itemObj)
// 2002-07-04: add isLeapYear(object) and getDaysInMonth(object)
// 26Oct2004: Cervus II : Added new method getWFTrigger(triggerKey) (AME spec. section 8.2.2)
// 26Oct2004: Cervus II : Added new method setWFTrigger(triggerKey, value) (AME spec. section 8.2.2)
// 26Oct2004: Cervus II : Added new method isWFTrigger(triggerKey) (AME spec. section 8.2.2)

  protected boolean isFunction(String token)
  {
    if(token.length() <= 0)
    {
      return false;
    }
    switch(token.charAt(0))
    {
      case 'e':
      case 'E':
        if(token.equalsIgnoreCase("exist"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("existInDeal"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("exp"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("execSQL"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 'i':
      case 'I':
        if(token.equalsIgnoreCase("isEmpty"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("isTrue"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("isValidSIN"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("indexOf"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("insertItem"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("isLeapYear"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("isWFTrigger"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("isNumeric"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("isDigit"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case '_':
        if(token.equalsIgnoreCase("_and"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("_or"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("_xor"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 'm':
      case 'M':
        if(token.equalsIgnoreCase("min"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("max"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 's':
      case 'S':
        if(token.equalsIgnoreCase("sum"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("sizeOf"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("sqrt"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("strlen"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("substr"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("sin"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("setItem"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("setWFTrigger"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 'c':
      case 'C':
        if(token.equalsIgnoreCase("count"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("ceil"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("cos"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 'f':
      case 'F':
        if(token.equalsIgnoreCase("find"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("fetchColumn"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("fetchRows"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("fetchRow"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("fetch"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("fetchField"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("floor"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 'g':
      case 'G':
        if(token.equalsIgnoreCase("getItem"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getMetaData"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getColumnCount"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getColumnName"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getColumnType"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getColumnPrecision"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getColumnScale"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getDayOfYear"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getDaysInMonth"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getDay"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getDayOfWeek"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getYear"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getMonth"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("getWFTrigger"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 'a':
      case 'A':
        if(token.equalsIgnoreCase("abs"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("asin"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("acos"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("atan"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("array"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("addItem"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 'd':
      case 'D':
        if(token.equalsIgnoreCase("deleteItem"))
        {
          return true;
        }
        //--> Added debug(msg) function to print debug messages in the log file
        //--> By Billy 31Aug2004
        else if(token.equalsIgnoreCase("debug"))
        {
          return true;
        }

        //=====================================================================
        return false;
      case 'r':
      case 'R':
        if(token.equalsIgnoreCase("random"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("round"))
        {
          return true;
        }
        else
        {
          return false;
        }
      case 't':
      case 'T':
        if(token.equalsIgnoreCase("toDate"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("tan"))
        {
          return true;
        }
        else if(token.equalsIgnoreCase("tosqlstr"))
        {
          return true;
        }
        return false;
      case 'n':
      case 'N':
        if(token.equalsIgnoreCase("now"))
        {
          return true;
        }
        return false;
      case 'l':
      case 'L':
        if(token.equalsIgnoreCase("log"))
        {
          return true;
        }
        return false;
      case 'p':
      case 'P':
        if(token.equalsIgnoreCase("pow"))
        {
          return true;
        }
        return false;
      default:
        return false;
    }
  }

 //26Oct2004: Cervus II : Added new method getWFTrigger(triggerKey) (AME spec. section 8.2.2)
 //26Oct2004: Cervus II : Added new method setWFTrigger(triggerKey, value) (AME spec. section 8.2.2)
 //26Oct2004: Cervus II : Added new method isWFTrigger(triggerKey) (AME spec. section 8.2.2)
  protected String castFunctionName(String token)
  {
    if(token.length() <= 0)
    {
      return token;
    }
    switch(token.charAt(0))
    {
      case 'e':
      case 'E':
        if(token.equalsIgnoreCase("exist"))
        {
          return "exist";
        }
        else if(token.equalsIgnoreCase("existInDeal"))
        {
          return "existInDeal";
        }
        else if(token.equalsIgnoreCase("exp"))
        {
          return "exp";
        }
        else if(token.equalsIgnoreCase("execSQL"))
        {
          return "execSQL";
        }
        break;
      case 'i':
      case 'I':
        if(token.equalsIgnoreCase("isEmpty"))
        {
          return "isEmpty";
        }
        else if(token.equalsIgnoreCase("isValidSIN"))
        {
          return "isValidSIN";
        }
        else if(token.equalsIgnoreCase("indexOf"))
        {
          return "indexOf";
        }
        else if(token.equalsIgnoreCase("isWFTrigger"))
        {
          return "isWFTrigger";
        }
        else if(token.equalsIgnoreCase("isNumeric"))
        {
          return "isNumeric";
        }
        else if(token.equalsIgnoreCase("isDigit"))
        {
          return "isDigit";
        }
        break;
      case '_':
        if(token.equalsIgnoreCase("_and"))
        {
          return "_and";
        }
        else if(token.equalsIgnoreCase("_or"))
        {
          return "_or";
        }
        else if(token.equalsIgnoreCase("_xor"))
        {
          return "_xor";
        }
        break;
      case 'm':
      case 'M':
        if(token.equalsIgnoreCase("min"))
        {
          return "min";
        }
        else if(token.equalsIgnoreCase("max"))
        {
          return "max";
        }
        break;
      case 's':
      case 'S':
        if(token.equalsIgnoreCase("sum"))
        {
          return "sum";
        }
        else if(token.equalsIgnoreCase("sizeOf"))
        {
          return "sizeOf";
        }
        else if(token.equalsIgnoreCase("sqrt"))
        {
          return "sqrt";
        }
        else if(token.equalsIgnoreCase("strlen"))
        {
          return "strlen";
        }
        else if(token.equalsIgnoreCase("substr"))
        {
          return "substr";
        }
        else if(token.equalsIgnoreCase("sin"))
        {
          return "sin";
        }
        else if(token.equalsIgnoreCase("setWFTrigger"))
        {
          return "setWFTrigger";
        }
        break;
      case 'c':
      case 'C':
        if(token.equalsIgnoreCase("count"))
        {
          return "count";
        }
        else if(token.equalsIgnoreCase("ceil"))
        {
          return "ceil";
        }
        else if(token.equalsIgnoreCase("cos"))
        {
          return "cos";
        }
        break;
      case 'f':
      case 'F':
        if(token.equalsIgnoreCase("find"))
        {
          return "find";
        }
        else if(token.equalsIgnoreCase("fetchColumn"))
        {
          return "fetchColumn";
        }
        else if(token.equalsIgnoreCase("fetchRows"))
        {
          return "fetchRows";
        }
        else if(token.equalsIgnoreCase("fetchRow"))
        {
          return "fetchRow";
        }
        else if(token.equalsIgnoreCase("fetch"))
        {
          return "fetch";
        }
        else if(token.equalsIgnoreCase("fetchField"))
        {
          return "fetchField";
        }
        else if(token.equalsIgnoreCase("floor"))
        {
          return "floor";
        }
        break;
      case 'g':
      case 'G':
        if(token.equalsIgnoreCase("getItem"))
        {
          return "getItem";
        }
        else if(token.equalsIgnoreCase("getMetaData"))
        {
          return "getMetaData";
        }
        else if(token.equalsIgnoreCase("getColumnCount"))
        {
          return "getColumnCount";
        }
        else if(token.equalsIgnoreCase("getColumnName"))
        {
          return "getColumnName";
        }
        else if(token.equalsIgnoreCase("getColumnType"))
        {
          return "getColumnType";
        }
        else if(token.equalsIgnoreCase("getColumnPrecision"))
        {
          return "getColumnPrecision";
        }
        else if(token.equalsIgnoreCase("getColumnScale"))
        {
          return "getColumnScale";
        }
        else if(token.equalsIgnoreCase("getDayOfYear"))
        {
          return "getDayOfYear";
        }
        else if(token.equalsIgnoreCase("getDay"))
        {
          return "getDay";
        }
        else if(token.equalsIgnoreCase("getDayOfWeek"))
        {
          return "getDayOfWeek";
        }
        else if(token.equalsIgnoreCase("getYear"))
        {
          return "getYear";
        }
        else if(token.equalsIgnoreCase("getMonth"))
        {
          return "getMonth";
        }
        else if(token.equalsIgnoreCase("getWFTrigger"))
        {
          return "getWFTrigger";
        }
        break;
      case 'a':
      case 'A':
        if(token.equalsIgnoreCase("abs"))
        {
          return "abs";
        }
        else if(token.equalsIgnoreCase("asin"))
        {
          return "asin";
        }
        else if(token.equalsIgnoreCase("acos"))
        {
          return "acos";
        }
        else if(token.equalsIgnoreCase("atan"))
        {
          return "atan";
        }
        break;
      case 'r':
      case 'R':
        if(token.equalsIgnoreCase("random"))
        {
          return "random";
        }
        else if(token.equalsIgnoreCase("round"))
        {
          return "round";
        }
        break;
      case 't':
      case 'T':
        if(token.equalsIgnoreCase("toDate"))
        {
          return "toDate";
        }
        else if(token.equalsIgnoreCase("tan"))
        {
          return "tanb";
        }
        else if(token.equalsIgnoreCase("tosqlstr"))
        {
          return "tosqlstr";
        }
        break;
      case 'n':
      case 'N':
        if(token.equalsIgnoreCase("now"))
        {
          return "now";
        }
        break;
      case 'l':
      case 'L':
        if(token.equalsIgnoreCase("log"))
        {
          return "log";
        }
        break;
      case 'p':
      case 'P':
        if(token.equalsIgnoreCase("pow"))
        {
          return "pow";
        }
        break;
      //--> Added debug(msg) function to print debug messages in the log file
      //--> By Billy 31Aug2004
      case 'D':
      case 'd':
        if(token.equalsIgnoreCase("deleteItem"))
        {
          return "deleteItem";
        }
        else if(token.equalsIgnoreCase("debug"))
        {
          return "debug";
        }

        break;
       //=====================================================================
    }
    return token;
  }

  protected List tokenize(String source) throws ParseException
  {
    List strTokens = new ArrayList();

    StringReader strRdr = new StringReader(source);
    StreamTokenizer tokenizer = new StreamTokenizer(strRdr);
    tokenizer.resetSyntax();
    tokenizer.wordChars('A', 'Z');
    tokenizer.wordChars('a', 'z');
    tokenizer.wordChars('0', '9');
    tokenizer.wordChars('$', '$');
    tokenizer.wordChars('%', '%');
    tokenizer.wordChars('_', '_');
    tokenizer.wordChars('.', '.');
    tokenizer.wordChars('\'', '\'');

    tokenizer.ordinaryChar('+');
    tokenizer.ordinaryChar('-');
    tokenizer.ordinaryChar('*');
    tokenizer.ordinaryChar('/');
    tokenizer.ordinaryChar('(');
    tokenizer.ordinaryChar(')');
    tokenizer.ordinaryChar('<');
    tokenizer.ordinaryChar('>');
    tokenizer.ordinaryChar('=');

    String token;
    int type;
    char achar;
    String prevToken = "";
    boolean bQuoted = false;

    try
    {
      while((type = tokenizer.nextToken()) != StreamTokenizer.TT_EOF)
      {
        if(type == StreamTokenizer.TT_WORD)
        {
          token = tokenizer.sval;
          if(bQuoted)
          {
            prevToken += token;
          }
          else
          {
            strTokens.add(token);
          }
        }
        else
        {
          achar = (char)tokenizer.ttype;
          token = "" + achar;
          if(achar == '\"')
          {
            if(bQuoted)
            {
              bQuoted = false;
              token = prevToken + "\"";
              strTokens.add(token);
            }
            else
            {
              bQuoted = true;
              prevToken = "\"";
            }
          }
          else
          {
            if(bQuoted)
            {
              prevToken += token;
            }
            if(achar != ' ')
            {
              if(illegalOperatorSyntax(token))
              {
                int size = strTokens.size();
                errorMsg = "Syntax error, invalid operators detected: "
                  + strTokens.get(--size) + token + "!";
                //--> Debug enhancement : by Billy 23Aug2004
                BRUtil.wran("Problem @tokenize : " + errorMsg);
                //==========================================
                throw new ParseException(errorMsg, 0);
              }
              strTokens.add(token);
            }
          }
        }
      }
    }
    catch(IOException ex)
    {
      BRUtil.error("Exceptrion @tokenize : Exception caught during tokenization: " + ex);
    }

    strRdr.close();
    return strTokens;
  }

  protected List scanExpression(String source) throws ParseException
  {
    int index = 0, length = 0, size, start, end;
    int cbPrevToken = 0;
    char opCode1, opCode2, prevOpCode;
    boolean bString = false;
    boolean bJoin = false;
    String token = null, prevToken = null, tmpToken;
    BreakIterator boundary = BreakIterator.getWordInstance();

    tokens.clear();

    operands = new Stack();
    operators = new Stack();

    expression = source;
    boundary.setText(source);

    opCode1 = ' ';
    start = boundary.first();
    end = boundary.next();

    while(end != BreakIterator.DONE)
    {
      if(token != null)
      {
        prevToken = token;
        cbPrevToken = token.length();
      }

      token = source.substring(start, end);
      token = token.trim();

      length = token.length();
      if(length > 0 && end > index)
      {
        // terminate scanning if invalid operator detected, e.g. >>
        if(illegalOperatorSyntax(token))
        {
          size = tokens.size();
          errorMsg = "Syntax error, invalid operators detected: "
            + tokens.get(--size) + token + "!";
          throw new ParseException(errorMsg, 0);
        }

        prevOpCode = opCode1;
        opCode1 = token.charAt(0);

        if(length > 1)
        {
          opCode2 = token.charAt(1);
        }
        else
        {
          opCode2 = ' ';

        }
        size = tokens.size();
        switch(opCode1)
        {
          case '\"': // string constant
            if(bString)
            {
              bString = false;
            }
            else
            {
              bString = true;
              index = source.indexOf('\"', start + 1);
              if(index == -1)
              {
                errorMsg = "Invalid syntax, string constant without closing string delimiter!";
                throw new ParseException(errorMsg, 0);
              }
              tokens.add(source.substring(start, index + 1));
            }
            break;
          case '(':
          case ')':
          case '[':
          case ']':
            tokens.add(token);
            break;
          case '=':
            switch(prevOpCode)
            {
              case '=': // ==
                tokens.set(size - 1, "==");
                break;
              case '>': // >=
                tokens.set(size - 1, ">=");
                break;
              case '<': // <=
                tokens.set(size - 1, "<=");
                break;
              case '!': // <=
                tokens.set(size - 1, "!=");
                break;
              case ':': // :=
                tokens.set(size - 1, ":=");
                break;
              default:
                tokens.add(token);
            }
            break;
          case '>':
            if(prevOpCode == '<')
            {
              tokens.set(size - 1, "!=");
            }
            else
            {
              tokens.add(token);
            }
            break;
          case '<':
          case '!':
            tokens.add(token);
            break;
          case '.':

            // do not join '].' nor '[.' as one token
//                   if (size > 0 && prevOpCode != ']' && prevOpCode != '[') {
            if(size > 0)
            {
              switch(prevOpCode)
              {
                case '[':
                case ']':
                  tokens.add(token);
                  break;
                case '.':
                  bJoin = false;
                  tokens.set(size - 1, prevToken + ".");
                  break;
                default: // joint tableName.columnName
                  if(token.length() == 1)
                  {
                    //--> Here is a bug found in the BR ::
                    //--> The root cause is in java 1.3.1 the BreakIterator interpret the '.'
                    //--> determinated string as separated token but as 1 token in 1.2.x.
                    //--> i.e. BR like "employmentHistory[$cbCurrentEmployer].contact.addr.addressLine1"
                    //--> in 1.2.x the last token will interpret as |contact.addr.addressLine1|
                    //--> in 1.3.1 becomes |contact|.|addr|.|addressLine1|
                    //
                    //--> The following is fixed by Billy 30July2002
                    //tokens.set (size - 1, prevToken + ".");
                    tmpToken = (String)tokens.get(size - 1);
                    tokens.set(size - 1, tmpToken + ".");
                    //==============================================================================
                    bJoin = true; // join tableName.columnName
                  }
                  else
                  {
                    tokens.add(token);
                  }
              }
            }
            else
            {
              tokens.add(token);
            }
            break;
          case '*': // check for comment
            if(prevOpCode == '/')
            {
              start = end;
              end = boundary.next();
              while(end != BreakIterator.DONE)
              {
                token = source.substring(start, end);
                if(token.charAt(0) == '*')
                {
                  start = end;
                  end = boundary.next();
                  if(end != BreakIterator.DONE)
                  {
                    token = source.substring(start, end);
                    if(token.charAt(0) == '/')
                    {
                      tokens.remove(tokens.size() - 1);
                      break;
                    }
                  }
                }
                if(end == BreakIterator.DONE)
                {
                  tokens.remove(tokens.size() - 1);
                }
                else
                {
                  start = end;
                  end = boundary.next();
                }
              }
              break;
            }
          default:

            // do nothing if it is a token within a string
            while((index = token.indexOf(',')) >= 0)
            {
              if(token.length() <= 1)
              {
                break;
              }
              else
              {
                tokens.add(token.substring(0, index));
                tokens.add(",");
                token = token.substring(index + 1);
              }
            }

            // check for _and, _or functions
            if(prevToken == null || cbPrevToken == 0)
            {
              tokens.add(token);
            }
            // support property[1].purchasePrice'
            // support tableName.columnName(1)
            else if(bJoin && cbPrevToken > 0)
            {
              tmpToken = (String)tokens.get(size - 1);

              tokens.set(size - 1, tmpToken + token);
              bJoin = false;
            }
            else
            {
              if(cbPrevToken > 0 && prevOpCode == '_')
              {
                if(token.compareToIgnoreCase("and") == 0)
                {
                  tokens.set(size - 1, "_and");
                }
                else if(token.compareToIgnoreCase("or") == 0)
                {
                  tokens.set(size - 1, "_or");
                }
                else if(token.compareToIgnoreCase("xor") == 0)
                {
                  tokens.set(size - 1, "_xor");
                }
                else
                {
                  tokens.add(token);
                }
              }
              else if(cbPrevToken > 0 && prevOpCode == GLOBAL_SYMBOL_PREFIX)
              {
                tokens.set(size - 1, "" + GLOBAL_SYMBOL_PREFIX + token);
              }
              else if(cbPrevToken > 0 && prevOpCode == LOCAL_SYMBOL_PREFIX)
              {
                tokens.set(size - 1, "" + LOCAL_SYMBOL_PREFIX + token);
              }

              else
              {
                tokens.add(token);
              }
            }
        }
      }

      start = end;
      end = boundary.next();
    }

    size = tokens.size();

    StringBuffer buf = new StringBuffer();

    for(int i = 0; i < size; ++i)
    {
      buf.append("[" + tokens.get(i) + "]");
    }
    BRUtil.debug("The Tokens : " + buf);

    return tokens;
  }

  protected void shiftReduce()
  {
    Object curObj;
    Class classObj;
    BRExpNode expObj = new BRExpNode(this);
    BRAssignNode assignObj = new BRAssignNode(this);
    BRFunctNode functObj = new BRFunctNode(this);
    Object curNode;

    while(operators.size() > 0)
    {
      curObj = operators.peek();
      classObj = curObj.getClass();

      if(classObj.isInstance(expObj) && ((BRExpNode)curObj).OpName.charAt(0) == '(')
      {
        return;
      }

      if(classObj.isInstance(expObj))
      {
        curNode = operators.pop();
        ((BRExpNode)curNode).setRightNode(operands.pop());
        ((BRExpNode)curNode).setLeftNode(operands.pop());
        operands.push(curNode);
      }
      else if(classObj.isInstance(assignObj))
      {
        curNode = operators.pop();
        ((BRAssignNode)curNode).setRightNode(operands.pop());
        ((BRAssignNode)curNode).setLeftNode(operands.pop());
        operands.push(curNode);
      }
      else if(classObj.isInstance(functObj))
      {
        break;
      }
    }

    return;
  }

  protected void shiftReduce(BRExpNode newNode)
  {
    Object prevObj;
    Class classObj;
    BRExpNode expObj = new BRExpNode(this);
    BRExpNode prevNode;
    BRAssignNode assignNode = new BRAssignNode(this);
    BRFunctNode functNode = new BRFunctNode(this);

    while(operators.size() > 0)
    {
      prevObj = operators.peek();
      classObj = prevObj.getClass();
      if(classObj.isInstance(expObj) && ((BRExpNode)prevObj).OpName.charAt(0) == '(')
      {
        return;
      }

      if(classObj.isInstance(expObj))
      {
        prevNode = (BRExpNode)prevObj;
        if(newNode.compareTo(prevNode) <= 0)
        { // same or lower precedence
          operators.pop();
          prevNode.setRightNode(operands.pop());
          prevNode.setLeftNode(operands.pop());
          operands.push(prevNode);
        }
        else
        {
          break;
        }
      }
      else if(classObj.isInstance(functNode))
      { // yml: 2002-04-29
        if(operands.size() > 0)
        {
          return;
        }
      }
      else if(classObj.isInstance(assignNode))
      {
        break;
      }
    }

    return;
  }

  protected void shiftReduce(String token, BRExpNode newNode)
  {
    char op1;
    BRExpNode curNode;

    op1 = token.charAt(0);

    if(op1 == ')')
    {
      curNode = (BRExpNode)operators.pop();
      while(curNode.OpCode != BRConstants.OP_LEFT_PAREN)
      {
        curNode.setRightNode(operands.pop());
        curNode.setLeftNode(operands.pop());
        operands.push(curNode);

        curNode = (BRExpNode)operators.pop();
      }

      if(curNode.OpCode == BRConstants.OP_LEFT_PAREN)
      {
        --leftParenthesisCount;
      }
    }
    else
    {
      shiftReduce(newNode);
    }
  }

  protected Object phantomClass(String className) throws ParseException
  {
    String token = className;

    try
    {
      token = token.toLowerCase();
      switch(token.charAt(0))
      {
        case 'a':
          if(token.equals("asset"))
          {
            return new Asset(this, false);
          }
          else if(token.equals("addr"))
          {
            return new Address(this, false);
          }
          break;
        case 'b':
          if(token.equals("borrower"))
          {
            return new Borrower(this, false);
          }
          else if(token.equals("borroweraddress"))
          {
            return new BorrowerAddress(this, false);
          }
          else if(token.equals("bridge"))
          {
            return new Bridge(this, false);
          }
          break;
        case 'c':
          if(token.equals("contact"))
          {
            return new Contact(this, false);
          }
          else if(token.equals("creditreference"))
          {
            return new CreditReference(this, false);
          }
          break;
        case 'd':
          if(token.equals("documenttracking"))
          {
            return new DocumentTracking(this, false);
          }
          break;
        case 'e':
          if(token.equals("employmenthistory"))
          {
            return new EmploymentHistory(this, false);
          }
          else if(token.equals("escrowpayment"))
          {
            return new EscrowPayment(this, false);
          }
          break;
        case 'i':
          if(token.equals("income"))
          {
            return new Income(this, false);
          }
          else if(token.equals("incometype"))
          {
            return new IncomeType(this, false);
          }
          //--DJ_CR136--23Nov2004--start--//
          // IMPORTANT!! Add this class to the BREngine collection.
          else if(token.equals("insureonlyapplicant"))
          {
            return new InsureOnlyApplicant(this, false);
          }
          //--DJ_CR136--23Nov2004--start--//
          break;
        case 'l':
          if(token.equals("liability"))
          {
            return new Liability(this, false);
          }
          else if(token.equals("lenderprofile"))
          {
            return new LenderProfile(this, false);
          }
          else if(token.equals("liablitytype"))
          {
            return new LiabilityType(this, false);
          }
          //--DJ_LDI_CR--start--//
          else if(token.equals("lifedisabilitypremiums"))
          {
            return new LifeDisabilityPremiums(this, false);
          }
          //--DJ_LDI_CR--end--//
          //--DJ_CR136--start--//
          else if(token.equals("lifedispremiumsionlya"))
          {
            return new LifeDisPremiumsIOnlyA(this, false);
          }
          //--DJ_CR136--end--//
          break;
        case 'p':
          if(token.equals("property"))
          {
            return new Property(this, false);
          }
          else if(token.equals("propertyexpense"))
          {
            return new PropertyExpense(this, false);
          }
          else if(token.equals("pricingprofile"))
          {
            return new PricingProfile(this, false);
          }
          else if(token.equals("province"))
          {
            return new Province(this, false);
          }
          break;
        case 's':
          if(token.equals("sourceofbusinessprofile"))
          {
            return new SourceOfBusinessProfile(this, false);
          }
          else if(token.equals("status"))
          {
            return new Status(this, false);
          }
          break;
        default:
          String msg = "Unknown entity: " + className;
          //--> Debug enhancement : by Billy 23Aug2004
          BRUtil.wran("Problem @phantomClass : " + msg);
          //==========================================

          throw new ParseException(msg, 0);
      }
    }
    catch(Exception ex)
    {
      String msg = "Unknown exception: " + ex.getMessage();
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.error("Problem @phantomClass : " + msg + " : " + ex);
      //==========================================
      throw new ParseException(msg, 0);
    }

    return null;
  }

  protected void removeOutParams()
  {
    for(int i = 0; i < BRConstants.MAX_OUTPUT_PARAMETERS; ++i)
    {
      GlobalSymbols.remove("$OutParam" + (i + 1));
    }
  }

  protected Object lookUpOutParam(int index)
  {
    return GlobalSymbols.get("$OutParam" + (index + 1));
  }

  protected void parseAssignment(String curToken) throws ParseException
  {
    BRAssignNode curNode = null;

    curNode = new BRAssignNode(this, dealObj, curToken);

    if(operands.size() > 0)
    {
      operators.push(curNode);
    }
    else
    {
      errorMsg = "Expression \"" + expression + "\" syntax error: "
        + "No left operands fournd for assignment operator \""
        + curToken + "\"!";
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.wran("Problem @parseAssignment : " + errorMsg);
      //==========================================
      throw new ParseException(errorMsg, 0);
    }
  }

  protected Object parseIfStmt() throws ParseException
  {
    int exprBracketState = BRConstants.NOT_EXPECTING_BRACKET;
    int blockBracketState = BRConstants.NOT_EXPECTING_BRACKET;
    int elseBlockState = BRConstants.NOT_EXPECTING_ELSE_BLOCK;
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    boolean bMore = true;
    String token;
    IfNode ifNode = new IfNode(this);

    // peek into the tokens stream to fetch the rest of If stmt tokens
    while(bMore && --nStack > 0)
    {
      if(tokens.isEmpty())
      {
        return ifNode; // return if no more tokens available
      }

      token = (String)(tokens.get(0));

      if(token.length() > 0)
      {
        switch(token.charAt(0))
        {
          case '(': // got left parenthesis, start parsing if condition expression
            if(exprBracketState == BRConstants.EXPECTING_RIGHT_BRACKET)
            {
              errorMsg = "Syntax error, invalid '(' token found after the if expression clause!";
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @parseIfStmt : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }
            tokens.remove(0);
            exprBracketState = BRConstants.EXPECTING_RIGHT_BRACKET;
            parseConditionExpression();
            break;
          case ')': // got right parenthesis, start parsing then block
            if(exprBracketState != BRConstants.EXPECTING_RIGHT_BRACKET)
            {
              errorMsg = "Syntax error, invalid ')' token found!";
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @parseIfStmt : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }

            tokens.remove(0);
            exprBracketState = BRConstants.NOT_EXPECTING_BRACKET;
            ifNode.expressionNode = (BRExpNode)operands.pop();

            break;
          case '{': // Left curly bracket found
            if(elseBlockState == BRConstants.PROCESSING_ELSE_BLOCK)
            {
              tokens.remove(0);
              ifNode.elseBlock = parseDoBlock(true);
              blockBracketState = BRConstants.EXPECTING_RIGHT_BRACKET;
            }
            else if(ifNode.thenBlock == null)
            {
              tokens.remove(0);
              ifNode.thenBlock = parseDoBlock(true);
              blockBracketState = BRConstants.EXPECTING_RIGHT_BRACKET;
            }
            else
            { // we are not exping else block nor then block, we got a problem
              errorMsg = "Unexpected right curly bracket found!";
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @parseIfStmt : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }

            break;
          case '}': // right curly bracket found
            if(blockBracketState != BRConstants.EXPECTING_RIGHT_BRACKET)
            {
              return ifNode; // not expecting }, therefore concludes if node parsing
            }

            tokens.remove(0);
            // concludes if node parsing if we got else block already
            if(ifNode.elseBlock != null)
            {
              return ifNode;
            }

            // this conclude then block parsing
            elseBlockState = BRConstants.EXPECTING_ELSE_BLOCK;
            break;
          default:
            if(token.equalsIgnoreCase("else"))
            {
              // are we expecting else block?
              if(elseBlockState == BRConstants.EXPECTING_ELSE_BLOCK)
              {
                // yes, we are expecting else block
                tokens.remove(0);
                elseBlockState = BRConstants.PROCESSING_ELSE_BLOCK;
              }
              else
              { // no we are not expecting else, exit if node parsing
                return ifNode;
              }
            }
            else
            { // may it be a single line stmt for then or else block?
              if(elseBlockState == BRConstants.PROCESSING_ELSE_BLOCK)
              {
                blockBracketState = BRConstants.NOT_EXPECTING_BRACKET;
                ifNode.elseBlock = parseDoBlock(false);

                // we are done, exit if node parsing
                return ifNode;
              }
              else if(ifNode.thenBlock == null)
              {
                ifNode.thenBlock = parseDoBlock(false);
                blockBracketState = BRConstants.NOT_EXPECTING_BRACKET;
                elseBlockState = BRConstants.EXPECTING_ELSE_BLOCK;
              }
              else
              {
                // no else block was found, exit if node parsing
                return ifNode;
              }
            }
        }
      }
    }

    return null; // catch all, should not even get here
  }

  protected void parseIndex(Object varNode) throws ParseException
  {
    int nPeriod = 0, index;
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    char aChar;
    String token, varName;
    boolean bArray = false;
    Class classObj = varNode.getClass();
    BRVarNode cachedVarNode = new BRVarNode(this);
    VarNode usrVarNode = new VarNode(this);
    BRFunctNode functNode = new BRFunctNode(this);

    if(classObj.isInstance(cachedVarNode))
    {
      varName = ((BRVarNode)varNode).entityName;
    }
    else if(classObj.isInstance(usrVarNode))
    {
      varName = ((VarNode)varNode).varName;
    }
    else
    {
      varName = "";

      // peek into the tokens stream to check if need to parse array index
    }
    while(!tokens.isEmpty() && --nStack > 0)
    {
      token = (String)tokens.get(0);
      aChar = token.charAt(0);
      switch(aChar)
      {
        case '[':
          tokens.remove(0);
          bArray = true;
          nPeriod = 0;
          break;
        case '.': // array subfield parsing
          tokens.remove(0);
          if(token.length() == 2 && token.charAt(1) == '.')
          {
            nPeriod += 2;
          }
          else
          {
            ++nPeriod;
          }
          if(bArray)
          {
            // it is a n array iterator
            if(classObj.isInstance(cachedVarNode))
            {
              ((BRVarNode)varNode).arrayIndex = BRConstants.ARRAY_ITERATOR;
            }
            else if(classObj.isInstance(usrVarNode))
            {
              ((VarNode)varNode).arrayIndex = BRConstants.ARRAY_ITERATOR;
            }
            else
            {
              String msg = "Invalid use of array iterator '..' for variable "
                + varName;
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @parseIndex : " + msg);
              //==========================================
              throw new ParseException(msg, 0);
            }
          }
          else
          {
            //bIsPartOf = true;
          }
          break;
        case ']':
          if(!bArray)
          {
            return;
          }

          tokens.remove(0);
          checkArrayIndex(nPeriod, varNode);
          bArray = false;
          nPeriod = 0;
          break;
        case ';':
        case ',':
        case ')':
          if(bArray || nPeriod > 0)
          {
            String msg = "Invalid array variable usage for '"
              + varName;
            //--> Debug enhancement : by Billy 23Aug2004
            BRUtil.wran("Problem @parseIndex : " + msg);
            //==========================================
            throw new ParseException(msg, 0);
          }
          return;
        default: // now parse the index

          // no need to parse array index if we had not encounter '[' yet.
          if(!bArray)
          {
            return;
          }

          if(aChar == '-')
          {
            String msg = "Array index cannot be negative!";
            //--> Debug enhancement : by Billy 23Aug2004
            BRUtil.wran("Problem @parseIndex : " + msg);
            //==========================================
            throw new ParseException(msg, 0);
          }
          else if(isOperator(token))
          {
            String msg = "Syntax error in array index specification for "
              + varName;
            //--> Debug enhancement : by Billy 23Aug2004
            BRUtil.wran("Problem @parseIndex : " + msg);
            //==========================================
            throw new ParseException(msg, 0);
          }
          else if(isLocalVariable(token))
          {
            if(!bArray) // expecting array index
            {
              errorMsg = "Invalid use of variable: " + token + " for variable "
                + varName;
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @parseIndex : " + errorMsg);
              //==========================================
              throw new ParseException(errorMsg, 0);
            }

            tokens.remove(0);
            usrVarNode = (VarNode)parseLocalVariable(token);
            if(classObj.isInstance(cachedVarNode))
            {
              ((BRVarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
              ((BRVarNode)varNode).indexObj = usrVarNode;
            }
            else if(classObj.isInstance(usrVarNode))
            {
              ((VarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
              ((VarNode)varNode).indexObj = usrVarNode;
            }
          }
          else if(isGlobalVariable(token))
          {
            if(!bArray) // expecting array index
            {
              errorMsg = "Invalid use of variable: " + token + " for variable "
                + varName;
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @parseIndex : " + errorMsg);
              //==========================================
              throw new ParseException(errorMsg, 0);
            }

            tokens.remove(0);
            usrVarNode = (VarNode)parseGlobalVariable(token);
            if(classObj.isInstance(cachedVarNode))
            {
              ((BRVarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
              ((BRVarNode)varNode).indexObj = usrVarNode;
            }
            else if(classObj.isInstance(usrVarNode))
            {
              ((VarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
              ((VarNode)varNode).indexObj = usrVarNode;
            }
          }
          else if(isFunction(token)) // check for function
          {
            tokens.remove(0);
            functNode = (BRFunctNode)parseFunction(token);
            if(classObj.isInstance(cachedVarNode))
            {
              ((BRVarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
              ((BRVarNode)varNode).indexObj = functNode;
            }
            else if(classObj.isInstance(usrVarNode))
            {
              ((VarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
              ((VarNode)varNode).indexObj = functNode;
            }
          }
          else // expecting integer
          {
            // no need to parse array index if we had not encounter [ yet.
            if(!bArray)
            {
              return;
            }

            if(BRUtil.isInteger(token))
            {
              tokens.remove(0);
              index = BRUtil.mapInt(token).intValue();
              if(classObj.isInstance(cachedVarNode))
              {
                ((BRVarNode)varNode).arrayIndex = index;
              }
              else if(classObj.isInstance(usrVarNode))
              {
                ((VarNode)varNode).arrayIndex = index;
              }
            }
            else
            {
              Object stmtObj = parseStatement(token);
              if(classObj.isInstance(cachedVarNode))
              {
                ((BRVarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
                ((BRVarNode)varNode).indexObj = stmtObj;
              }
              else if(classObj.isInstance(usrVarNode))
              {
                ((VarNode)varNode).arrayIndex = BRConstants.INDEX_VARIABLE;
                ((VarNode)varNode).indexObj = stmtObj;
              }
            }
          }

          if(!bArray)
          {
            return;
          }
      }
    }
  }

  protected Object parseLocalVariable(String token)
  {
    VarNode varNode;

    if(LocalSymbols.containsKey(token))
    {
      // exists - get current value
      varNode = (VarNode)LocalSymbols.get(token);
    }
    else
    {
      // first encountered - add dynamically
      varNode = new VarNode(this, token);
      LocalSymbols.put(token, varNode);
    }

    return varNode;
  }

  protected Object parseGlobalVariable(String token) throws ParseException
  {
    VarNode varNode;

    if(GlobalSymbols.containsKey(token))
    {
      varNode = (VarNode)GlobalSymbols.get(token);
      if(varNode == null)
      {
        setGlobalSymbol(token, varNode = new VarNode(this, token));
      }
    }
    else
    {
      // disaster handling - i.e. should never happen!!
      setGlobalSymbol(token, varNode = new VarNode(this, token));
    }

    parseIndex(varNode);

    return varNode;
  }

  protected void parseOperator(String curToken) throws ParseException
  {
    char opCode;
    Class classNode;
    Object prevNode;
    BRExpNode curNode = null, expNode = new BRExpNode(this);

    opCode = curToken.charAt(0);
    curNode = new BRExpNode(this, dealObj, curToken);

    if(operators.size() > 0)
    {
      if(opCode == ')')
      {
        if(leftParenthesisCount <= 0)
        {
          //--> Debug enhancement : by Billy 23Aug2004
          BRUtil.wran("Problem @parseOperator : " + "Missing left parenthesis!");
          //==========================================
          throw new ParseException("Missing left parenthesis!", 0);
        }

        shiftReduce(curToken, curNode);
//             operators.push(curNode);            // 1999-11-26
      }
      else
      {
        prevNode = operators.peek();
        classNode = prevNode.getClass();
        if(classNode.isInstance(expNode))
        {
          expNode = (BRExpNode)prevNode;
          if(curNode.compareTo(expNode) > 0)
          { // curNode has lowe precedence
            if(opCode == '(')
            {
              ++leftParenthesisCount;
            }
            if(curNode == null)
            {
              errorMsg = "BRInterpretor.parseOperator(operator), error: "
                + "Invalid operator \"" + curToken + "\"!";
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @parseOperator : " + errorMsg);
              //==========================================
              throw new ParseException(errorMsg, 0);
            }
            operators.push(curNode);
          }
          else
          { // curNode is high or equal to prevNode's precedence
            if(opCode != '(' && expNode.OpName.charAt(0) != '(')
            {
              shiftReduce(curToken, curNode);
            }
            if(opCode == '(')
            {
              ++leftParenthesisCount;
            }
            operators.push(curNode);
          }
        }
        else
        {
          operators.push(curNode);
        }
      }
    }
    else
    {
      if(opCode == '(')
      {
        ++leftParenthesisCount;
      }
      operators.push(curNode);
    }
  }

  protected void shiftReduceExpression1() throws ParseException
  {
    Object nodeObj, operandObj;
    Class classObj, operandClass;

    BRVarNode entityNode = new BRVarNode(this);
    BRFunctNode functNode = new BRFunctNode(this);
    BRExpNode expNode = new BRExpNode(this);
    BRDbFieldNode fieldNode = new BRDbFieldNode(this);
    BRAssignNode assignNode = new BRAssignNode(this);
    VarNode varNode = new VarNode(this);
    ForNode forNode = new ForNode(this);

    while(operators.size() > 0)
    {
      nodeObj = operators.peek();
      classObj = nodeObj.getClass();
      if(classObj.isInstance(forNode))
      {
        return;
      }

      // return if encountering a '('
      if(classObj.isInstance(expNode) && ((BRExpNode)nodeObj).OpName.charAt(0) == '(')
      {
        return;
      }

      operators.pop();

      if(classObj.isInstance(expNode))
      {
        expNode = (BRExpNode)nodeObj;
        if(!expNode.OpName.equals("("))
        {
          expNode.shiftReduceExpression1(operators, operands);
          operands.push(expNode);
        }
      }
      else if(classObj.isInstance(functNode))
      {
      }
      else if(classObj.isInstance(assignNode))
      {
        assignNode = (BRAssignNode)nodeObj;

        // assign right operand
        assignNode.setRightNode(operands.pop());

        // assign left operand
        operandObj = operands.pop();
        operandClass = operandObj.getClass();

        if(operandClass.isInstance(functNode))
        {
          fieldNode = new BRDbFieldNode(dealObj, (BRFunctNode)operandObj);
          assignNode.setLeftNode(fieldNode);
        }
        else if(operandClass.isInstance(entityNode))
        {
          assignNode.setLeftNode(operandObj);
        }
        else if(operandClass.isInstance(varNode))
        {
          assignNode.setLeftNode(operandObj);
        }
        else
        {
          errorMsg = "Unsuppored Left Operands of the := assignment operator!";
          //--> Debug enhancement : by Billy 23Aug2004
          BRUtil.wran("Problem @shiftReduceExpression1 : " + errorMsg);
          //==========================================
          throw new ParseException(errorMsg, 0);
        }

        operands.push(assignNode);
      }
      else if(classObj.isInstance(fieldNode))
      {
      }
    }
  }

  protected Object shiftReduceExpression2() throws ParseException
  {
    shiftReduceExpression1();
    return operands.pop();
  }

  protected int findEndOfUserVar(String source, int start)
  {
    char aChar;
    int length = source.length();
    for(int i = start + 1; i < length; ++i)
    {
      aChar = source.charAt(i);
      if(aChar >= 'a' && aChar <= 'z')
      {
      }
      else if(aChar >= 'A' && aChar <= 'Z')
      {
      }
      else if(aChar >= '0' && aChar <= '9')
      {
      }
      else if(aChar == '_')
      {
      }
      else
      {
        return i;
      }
    }
    return length;
  }

  protected void substituteUserVars(Object aNode) throws ParseException
  {
    Class classObj = aNode.getClass();
    BRExpNode expNode = new BRExpNode(this);
    BRFunctNode functNode = new BRFunctNode(this);
    BRAssignNode assignNode = new BRAssignNode(this);
    IfNode ifNode = new IfNode(this);
    DoNode doNode = new DoNode(this);

    if(classObj.isInstance(expNode))
    {
      ((BRExpNode)aNode).substituteUserVars();
    }
    else if(classObj.isInstance(functNode))
    {
      ((BRFunctNode)aNode).substituteUserVars();
    }
    else if(classObj.isInstance(assignNode))
    {
      ((BRAssignNode)aNode).substituteUserVars();
    }
    else if(classObj.isInstance(ifNode))
    {
      ((IfNode)aNode).substituteUserVars();
    }
    else if(classObj.isInstance(doNode))
    {
      ((DoNode)aNode).substituteUserVars();
    }
  }

  protected String substituteUserVars(String source, char delimiter) throws ParseException
  {
    int start, end, index;
    String newSource, strTemp, strDelimiter = "";
    boolean bDelimiter = false;
    Integer intObj = new Integer(0);
    Double dfObj = new Double(0.0);
    Date dateObj = new Date(2000, 01, 02);
    Time timeObj = new Time(13, 14, 15);
    Timestamp tsObj = new Timestamp(2000, 1, 2, 13, 14, 15, 0);

    if(source == null)
    {
      return source;
    }

    if(source.length() <= 0)
    {
      return source;
    }

    start = source.indexOf('$');
    end = start;
    if(start >= 0)
    {
      end = findEndOfUserVar(source, start);
    }
    else
    {
      start = source.indexOf('%');
      if(start >= 0)
      {
        end = findEndOfUserVar(source, start);
      }
    }

    if(start >= 0)
    {
      //int length = source.length();
      String varName = source.substring(start, end);
      Object varObj = getUserDefinedVariable(varName);
      if(varObj != null)
      {
        Class classObj = varObj.getClass();

        if(classObj.isInstance(dateObj))
        {
          varName = "to_date(\'" + varObj.toString() + "\',\'YYYY-MM-DD\')";
        }
        else if(classObj.isInstance(timeObj))
        {
          varName = "to_date(\'" + varObj.toString() + "\',\'YYYY-MM-DD\')";
        }
        else if(classObj.isInstance(tsObj))
        {
          strTemp = varObj.toString();
          index = strTemp.indexOf(".", 0);
          if(index > 0)
          {
            strTemp = strTemp.substring(0, index);

          }
          varName = "to_date(\'" + strTemp + "\',\'YYYY-MM-DD HH24:MI:SS\')";
        }
        else if(classObj.isInstance(intObj))
        {
          bDelimiter = false;
          varName = varObj.toString();
        }
        else if(classObj.isInstance(dfObj))
        {
          bDelimiter = false;
          varName = varObj.toString();
        }
        else
        {
          switch(delimiter)
          {
            case BRConstants.STR_DELIMITER:
              strDelimiter = "\"";
              varName = strDelimiter;
              bDelimiter = true;
              break;
            case BRConstants.SQL_STR_DELIMITER:
              strDelimiter = "\'";
              varName = strDelimiter;
              bDelimiter = true;
              break;
            default:
              varName = "";
          }

          varName += varObj.toString();

          if(bDelimiter)
          {
            varName += strDelimiter;
          }
        }

        newSource = source.substring(0, start) + varName + source.substring(end);
        return substituteUserVars(newSource, delimiter);
      }
    }

    return source;
  }

  protected BRAssignNode parseAssignmentStmt(BRFunctNode functNode, String curToken) throws ParseException
  {
    int nParmExpBracket = 0;
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    char opCode;
    String token;
    boolean bMore = true;
    Object nodeObj;
    BRAssignNode currentNode = new BRAssignNode(this);
    BRVarNode entityNode = null;

    operands.push(functNode.popParameter());
    operators.push(currentNode);

    while(bMore && --nStack > 0)
    {
      if(tokens.isEmpty())
      {
        //--> Debug enhancement : by Billy 23Aug2004
        BRUtil.wran("Problem @parseAssignmentStmt : " + "Incomplete assignment statement specified!");
        //==========================================
        throw new ParseException("Incomplete assignment statement specified!", 0);
      }

      token = (String)(tokens.get(0));
      opCode = token.charAt(0);
      switch(opCode)
      {
        case '(':
          ++nParmExpBracket;
          break;
        case ',':
          break;
        case ')':

          // check if any oustanding left parenthesis first
          if(nParmExpBracket > 0)
          {
            tokens.remove(0);
            --nParmExpBracket;
            if(nParmExpBracket == 0)
            {
              shiftReduce();

            }
          }
          else
          { // end of function encountered
            shiftReduce();
            currentNode = (BRAssignNode)(operands.pop());
            return currentNode;
          }
          break;
        default:
          if(isOperator(token))
          {
            parseOperator(token);
            tokens.remove(0);
          }
          else if(isFunction(token))
          {
            tokens.remove(0);
            nodeObj = parseFunction(token);
            operands.push(nodeObj);
          }
          else if(BRUtil.isDealEntity((Object)dealObj, token))
          {
            tokens.remove(0);
            entityNode = parseDealEntity(dealObj, token);
            operands.push(entityNode);
          }
//             else if (isDBEntity(token)) {
//                tokens.remove(0);
//                parseDBEntity(token);
//                }
          else
          {
            tokens.remove(0);
            operands.push(token);
          }
      }
    }

    return currentNode;
  }

  protected BRExpNode parseParameterExpression(BRFunctNode functNode, String curToken) throws ParseException
  {
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    int nParmExpBracket = 0;
    char opCode;
    boolean bMore = true;
    String token = null;
    Object nodeObj;
    BRExpNode currentNode = null;
    BRExpNode expNode = null;
    BRVarNode entityNode = null;

    opCode = curToken.charAt(0);
    switch(opCode)
    {
      case '(':
        ++nParmExpBracket;
        break;
      case ')':
        break;
      default:
        currentNode = new BRExpNode(this, dealObj, curToken);

        // push last parameter into operands stack
        operands.push(functNode.popParameter());
        operators.push(currentNode);

        // peek into the tokens stream to fetch the right operands
        // parameter expression can only be terminated by ',' or ')'
        while(bMore && --nStack > 0)
        {
          if(tokens.isEmpty())
          {
            String strErrMsg = "Operand is missing from the right of operator \""
              + curToken + "\"!";
            //--> Debug enhancement : by Billy 23Aug2004
            BRUtil.wran("Problem @parseParameterExpression : " + strErrMsg);
            //==========================================

            throw new ParseException(strErrMsg, 0);
          }

          token = (String)(tokens.get(0));
          opCode = token.charAt(0);
          switch(opCode)
          {
            case ',':

              // need to check if missing right operand
              // ????

              // end of parameter expression
              shiftReduce();
              currentNode = (BRExpNode)(operands.pop());
              return currentNode;
            case '(':
              ++nParmExpBracket;
              break;
            case ')':

              // check if any oustanding left parenthesis first
              if(nParmExpBracket > 0)
              {
                tokens.remove(0);
                --nParmExpBracket;
                if(nParmExpBracket == 0)
                {
                  shiftReduce();
                }
              }
              else
              { // end function bracket encountered
                shiftReduce();
                currentNode = (BRExpNode)(operands.pop());
                return currentNode;
              }
              break;
            default:
              if(isOperator(token))
              {
                parseOperator(token);
                tokens.remove(0);
              }
              else if(isFunction(token))
              {
                tokens.remove(0);
                nodeObj = parseFunction(token);
                operands.push(nodeObj);
              }
//                     else if (isDealEntity((Object)dealObj, token)) {
              else if(BRUtil.isDealEntity((Object)dealObj, token))
              {
                tokens.remove(0);
                entityNode = parseDealEntity(dealObj, token);
                operands.push(entityNode);
              }
//                   else if (isDBEntity(token)) {
//                      tokens.remove(0);
//                      parseDBEntity(token);
//                      }
              else
              {
                tokens.remove(0);
                operands.push(token);
              }
          } // switch (opCode)
        } // while (bMore)
    } // switch (opCode)

    return expNode;
  }

  protected Object parseReturnStmt() throws ParseException
  {
    ReturnNode returnNode = new ReturnNode(this);

    tokens.remove(0);
    returnNode.stmtObj = parseExpressionPhase1();

    return returnNode;
  }

  protected Object parseStatement(String token) throws ParseException
  {
    Object stmtObj = null;

    switch(token.charAt(0))
    {
      case 'i':
      case 'I':
        if(token.equalsIgnoreCase("if"))
        {
          // strip if statement (if stmt within a do block)
          tokens.remove(0);
          return parseIfStmt();
        }
        break;
      case 'e':
      case 'E':
        if(token.equalsIgnoreCase("else"))
        {
          String msg = "illegal else without matching if!";
          //--> Debug enhancement : by Billy 23Aug2004
          BRUtil.wran("Problem @parseStatement : " + msg);
          //==========================================
          throw new ParseException(msg, 0);
        }
        break;
      case 'r':
      case 'R':
        if(token.equalsIgnoreCase("return"))
        {
          return parseReturnStmt();
        }
        break;
      case 'f':
      case 'F':
        if(token.equalsIgnoreCase("for"))
        {
          return parseForStmt();
        }
        break;
    }

//      errorMsg = "Unsuppored keyword: " + token + " detected!";
//      throw new ParseException(errorMsg, 0);
    stmtObj = parseExpressionPhase1();
    return stmtObj;
  }

  protected BRFunctNode parseDBFetch() throws ParseException
  {
    int nLeftParenthesis = 0;
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    boolean bMore = true;
    char aChar;
    String token;
    Object nodeObj;
    BRFunctNode currentNode = new BRFunctNode(this);
    BRExpNode expNode = null;
    BRVarNode entityNode = null;

    // treat function as an operator
    currentNode = new BRFunctNode(this, dealObj, "fetch");

    // peek into the tokens stream to fetch the rest of function parameters
    while(bMore && --nStack > 0)
    {
      if(tokens.isEmpty())
      {
        return currentNode;
      }
      token = (String)(tokens.get(0));
      if(token.length() > 0)
      {
        aChar = token.charAt(0);
        switch(aChar)
        {
          case '(': // got left parenthesis, start extracting parameters
            if(currentNode.nParmCount < 0)
            {
              currentNode.nParmCount = 0;
            }
            else
            { // got '(' inside parameters
              ++nLeftParenthesis;
            }
            tokens.remove(0);
            break;
          case ')':
            tokens.remove(0);
            if(nLeftParenthesis > 0)
            { // got matching parenthesis
              --nLeftParenthesis;
            }
            else
            { // end of function parameter parsing
              return currentNode;
            }
            tokens.remove(0);
            break;
          case ',':
            tokens.remove(0);
            break;
          default:
            if(isOperator(token))
            {
              tokens.remove(0);

              expNode = parseParameterExpression(currentNode, token);
              currentNode.pushParameter(expNode);
            }
            else if(isFunction(token))
            {
              tokens.remove(0);
              nodeObj = parseFunction(token);
              currentNode.pushParameter(nodeObj);
            }
            else if(BRUtil.isDealEntity((Object)dealObj, token))
            {
              tokens.remove(0);
              entityNode = parseDealEntity(dealObj, token);
              currentNode.pushParameter(entityNode);
            }
            else
            {
              tokens.remove(0);
              operands.push(token);
              currentNode.pushParameter(operands.pop());
            }
        }
      }
      else
      {
        tokens.remove(0);
        operands.push(token);
        currentNode.pushParameter(operands.pop());
      }
    }

    return currentNode;
  }

  protected Object parseForStmt() throws ParseException
  {
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    short nLoopStage = 0;
    boolean bMore = true;
    char aChar;
    String token;
    Object stmtObj;
    ForNode forNode = new ForNode(this);

    tokens.remove(0);
    operators.push(forNode);

    // peek into the tokens stream to fetch the rest of If stmt tokens
    while(bMore && --nStack > 0)
    {
      // return if no more tokens available
      if(tokens.isEmpty())
      {
        operators.pop();
        return forNode;
      }

      token = (String)(tokens.get(0));

      if(token.length() > 0)
      {
        aChar = token.charAt(0);
        switch(aChar)
        {
          case '(':
            //bMatchParen = true;
            tokens.remove(0);
            break;
          case ')':
            //bMatchParen = false;
            tokens.remove(0);
            break;
          case '{':
            //bMatchBlock = true;
            tokens.remove(0);
            break;
          case '}':
            //bMatchBlock = false;
            tokens.remove(0);
            bMore = false;
            break;
          case ';':
            tokens.remove(0);
            break;
          default:


            stmtObj = parseStatement(token);

            switch(nLoopStage)
            {
              case 0:

                // Check if the statement is an Assignment Node
                if(stmtObj instanceof BRAssignNode)
                {
                  forNode.initExpObj = (BRAssignNode)stmtObj;
                  ++nLoopStage;
                }
                else
                {
                  errorMsg = "Init condition is not as Assignment Statement in FOR loop !";
                  BRUtil.wran("Problem @parseForStmt : " + errorMsg);
                  throw new ParseException(errorMsg, 0);
                }

                break;

              case 1:

                // Check if the statement is an Assignment Node
                if(stmtObj instanceof BRExpNode)
                {
                  forNode.loopExpObj = (BRExpNode)stmtObj;
                  ++nLoopStage;
                }
                else
                {
                  errorMsg = "Loop condition is not as Expression Statement in FOR loop !";
                  BRUtil.wran("Problem @parseForStmt : " + errorMsg);
                  throw new ParseException(errorMsg, 0);
                }

                break;

              case 2:

                // Check if the statement is an Assignment Node
                if(stmtObj instanceof BRAssignNode)
                {
                  forNode.incrExpObj = (BRAssignNode)stmtObj;
                  ++nLoopStage;
                }
                else
                {
                  errorMsg = "Increment condition is not as Assignment Statement in FOR loop !";
                  BRUtil.wran("Problem @parseForStmt : " + errorMsg);
                  throw new ParseException(errorMsg, 0);
                }

                break;
              default:
                forNode.stmts.add(stmtObj);
                ++forNode.nStmts;
            }
        }
      }
    }

    operators.pop();
    return forNode;
  }

  protected Object parseFunction(String curToken) throws ParseException
  {
    int length, nLeftParenthesis = 0;
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    boolean bMore = true, bParmOptional = false, bParmExpected = false,
      bCommaOptional = false;
    char firstChar;
    String token;
    Object nodeObj;
    BRExpNode expNode = null;
    BRFunctNode currentNode = null;
    BRVarNode entityNode = null;

    // treat function as an operator
    currentNode = new BRFunctNode(this, dealObj, curToken);
    operators.push(currentNode);

    // peek into the tokens stream to fetch the rest of function parameters
    while(bMore && --nStack > 0)
    {
      if(tokens.isEmpty())
      {
        return currentNode;
      }
      token = (String)(tokens.get(0));
      length = token.length();
      if(length > 0)
      {
        firstChar = token.charAt(0);
        if(firstChar == '(' && length > 1)
        {
          firstChar = ' ';

        }
        switch(firstChar)
        {
          case '(': // got left parenthesis, start extracting parameters
            if(currentNode.nParmCount < 0)
            {
              currentNode.nParmCount = 0;
              bParmOptional = true;
            }
            else
            { // got '(' inside parameters
              ++nLeftParenthesis;
            }
            tokens.remove(0);
            break;
          case ')':
            tokens.remove(0);
            if(nLeftParenthesis > 0)
            { // got matching parenthesis
              --nLeftParenthesis;
            }
            else
            { // end of function parameter parsing
              // check if function syntax error
              if(bParmExpected)
              { // parsed ',', parm expected. e.g.xxx(parm1, )
                errorMsg = "Invalid function parameter syntax for \"" +
                  currentNode.functName + "\", parameter expected after comma";
                //--> Debug enhancement : by Billy 24Aug2004
                BRUtil.wran("Problem @parseFunction : " + errorMsg);
                //==========================================
                throw new ParseException(errorMsg, 0);
              }

              if(currentNode.functName.equalsIgnoreCase("_xor") && currentNode.nParmCount != 2)
              {
                errorMsg = "Invalid number of parameters for _xor(x,y), 2 expected!";
                //--> Debug enhancement : by Billy 24Aug2004
                BRUtil.wran("Problem @parseFunction : " + errorMsg);
                //==========================================

                throw new ParseException(errorMsg, 0);
              }

              operators.pop();
              return currentNode;
            }
            tokens.remove(0);
            break;
          case ',':
            if(!bCommaOptional)
            { // comma not expected
              errorMsg = "Invalid function parameter syntax for \"" +
                currentNode.functName + "\", missing parameter before comma!";
              //--> Debug enhancement : by Billy 24Aug2004
              BRUtil.wran("Problem @parseFunction : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }

            tokens.remove(0);
            bParmExpected = true;
            bCommaOptional = false;
            break;
          default:
            if(bCommaOptional)
            {
              if(isOperator(token))
              {
                bCommaOptional = false;
                bParmExpected = true;
              }
              else
              {
                errorMsg = "Invalid function parameter syntax for \"" +
                  currentNode.functName + "\", comma or right bracket expected instead of \""
                  + token + "\"";
                //--> Debug enhancement : by Billy 24Aug2004
                BRUtil.wran("Problem @parseFunction : " + errorMsg);
                //==========================================

                throw new ParseException(errorMsg, 0);
              }
            }
            if(!(bParmExpected || bParmOptional))
            { // parm not expected
              errorMsg = "Invalid function parameter syntax for \"" +
                currentNode.functName + "\", parameter not expected "
                + " yet parameter \"" + token + "\" encoutnered";
              //--> Debug enhancement : by Billy 24Aug2004
              BRUtil.wran("Problem @parseFunction : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }
            if(isNegativeNumber(token))
            {
              tokens.remove(0);
              token = "-" + tokens.get(0);
              tokens.remove(0);
              operands.push(token);
              currentNode.pushParameter(operands.pop());
            }
            else if(isOperator(token))
            {
              if(isSelectWildCard(currentNode, token))
              {
                tokens.remove(0);
                currentNode.pushParameter(token);
              }
              else
              {
                tokens.remove(0);
                expNode = parseParameterExpression(currentNode, token);
                currentNode.pushParameter(expNode);
                bCommaOptional = true;
              }
            }
            else if(isFunction(token))
            {
              tokens.remove(0);
              nodeObj = parseFunction(token);
              currentNode.pushParameter(nodeObj);
              bCommaOptional = true;
            }
            else if(isAssignmentOperator(token))
            {
              tokens.remove(0);
              nodeObj = parseAssignmentStmt(currentNode, token);
              currentNode.pushParameter(nodeObj);
            }
            else if(isGlobalVariable(token))
            {
              tokens.remove(0);
              nodeObj = parseGlobalVariable(token);
              currentNode.pushParameter(nodeObj);
            }
            else if(isLocalVariable(token))
            {
              tokens.remove(0);
              nodeObj = parseLocalVariable(token);
              currentNode.pushParameter(nodeObj);
            }
            else if(BRUtil.isDealEntity((Object)dealObj, token))
            {
              tokens.remove(0);
              entityNode = parseDealEntity(dealObj, token);
              currentNode.pushParameter(entityNode);
            }
            else
            {
              tokens.remove(0);
              currentNode.pushParameter(token);
            }

            // parm parsed, expecting either comma or )
            bParmExpected = false;
            bParmOptional = false;
            bCommaOptional = true;
        } // switch
      } // if (length > 0)
      else
      {
        tokens.remove(0);
        currentNode.pushParameter(token);
      } // if (length > 0)
    }

    return currentNode;
  }

  private boolean checkArrayIndex(int nPeriod, Object varNode) throws ParseException
  {
    int arrayIndex;
    String varName;
    Class classObj = varNode.getClass();
    BRVarNode cachedVarNode = new BRVarNode(this);
    VarNode usrVarNode = new VarNode(this);

    if(classObj.isInstance(cachedVarNode))
    {
      arrayIndex = ((BRVarNode)varNode).arrayIndex;
      varName = ((BRVarNode)varNode).entityName;
    }
    else if(classObj.isInstance(usrVarNode))
    {
      arrayIndex = ((VarNode)varNode).arrayIndex;
      varName = ((VarNode)varNode).varName;
    }
    else
    {
      arrayIndex = -1;
      varName = "";
    }

    switch(nPeriod)
    {
      case 2:
        return true;
      case 0:
        if(arrayIndex >= 0)
        {
          return true;
        }
        else
        {
          errorMsg = "Array index must be a positive integer with 1 as the first element instead of: \""
            + varName;

          switch(arrayIndex)
          {
            case -1:
              errorMsg += "[]\"!";
              break;
            case 0:
              errorMsg += "[" + arrayIndex + "]\"!";
              break;
            default:
              errorMsg += "[" + arrayIndex + "]\"!";
          }

          //--> Debug enhancement : by Billy 24Aug2004
          BRUtil.wran("Problem @checkArrayIndex : " + errorMsg);
          //==========================================

          throw new ParseException(errorMsg, 0);
        }
      default:
        errorMsg = "Invalid array index operator '.' was used for variable '"
          + varName + "'";
        //--> Debug enhancement : by Billy 24Aug2004
        BRUtil.wran("Problem @checkArrayIndex : " + errorMsg);
        //==========================================

        throw new ParseException(errorMsg, 0);
    }
  }

  private void parseArrayIndex(Object anEntity, BRVarNode currentNode, String token) throws ParseException
  {
    char aChar = token.charAt(0);

    if(aChar == '-')
    {
      errorMsg = "Array index cannot be negative!";
      //--> Debug enhancement : by Billy 24Aug2004
      BRUtil.wran("Problem @parseArrayIndex : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }
    else if(isOperator(token))
    {
      return; // end of deal entity parsing
    }
    else if(isAssignmentOperator(token))
    { // handle :=
      return; // end of deal entity parsing
    }
    else if(isLocalVariable(token))
    {
      currentNode.arrayIndex = BRConstants.INDEX_VARIABLE;
      tokens.remove(0);
      currentNode.indexObj = parseLocalVariable(token);
    }
    else if(isGlobalVariable(token))
    {
      currentNode.arrayIndex = BRConstants.INDEX_VARIABLE;
      tokens.remove(0);
      currentNode.indexObj = parseGlobalVariable(token);
    }
    else
    {
      tokens.remove(0);
      currentNode.arrayIndex = Integer.parseInt(token);
    }
  }

  private boolean parseDealEntityNode(Object anEntity, BRVarNode currentNode) throws ParseException
  {
    int nPeriod = 0;
    boolean bArray = false;
    String token;
    Object entityObj;
    Object[] entityObjs;
    Class entityClass;
    Class[] parameterTypes;
    Method method;
    Object[] arguments;

    // peek into the tokens stream to fetch array
    while(!tokens.isEmpty())
    {
      token = MosEntities.mapEntityName((String)(tokens.get(0)));
      if(isOperator(token))
      {
        return true;
      }

      switch(token.charAt(0))
      {
        case '[': // start array index parsing
          tokens.remove(0);
          bArray = true;
          break;
        case ']': // end array index parsing
          tokens.remove(0);
          checkArrayIndex(nPeriod, currentNode);
          bArray = false;
          nPeriod = 0;
          break;
        case '.': // array subfield parsing
          tokens.remove(0);
          if(token.length() == 2 && token.charAt(1) == '.')
          {
            nPeriod += 2;
          }
          else
          {
            ++nPeriod;
          }
          if(bArray)
          {
            currentNode.arrayIndex = BRConstants.ARRAY_ITERATOR; // array iterator
          }
          else
          {
            //bIsPartOf = true;
          }
          break;
        case ';':
        case ',':
        case ')':
          return false;
        default:
          if(bArray)
          {
            parseArrayIndex(anEntity, currentNode, token);
          }
          else
          {
            tokens.remove(0);
            // get current entity class object
            Class classObj = anEntity.getClass();

            // get object as declared intity
            try
            {
              Field aField = classObj.getDeclaredField(currentNode.entityName);
              switch(currentNode.arrayIndex)
              {
                case BRConstants.SCALAR_VARIABLE: // noindex
                  entityObj = aField.get(anEntity);
                  break;
                case BRConstants.ARRAY_ITERATOR: // array iterator '..'
                  entityObjs = (Object[])aField.get(anEntity);

                  // instantiate object if null
                  if(entityObjs == null)
                  {
                    entityClass = anEntity.getClass();

                    parameterTypes = new Class[]
                      {
                      String.class};
                    method = entityClass.getMethod("fetchField", parameterTypes);
                    arguments = new Object[]
                      {
                      currentNode.entityName};
                    entityObjs = (Object[])method.invoke(anEntity, arguments);
                    entityObj = entityObjs[0];
                  }
                  else
                  {
                    if(Array.getLength(entityObjs) > 0)
                    {
                      entityObj = entityObjs[0];
                    }
                    else
                    {
                      entityObj = phantomClass(currentNode.entityName);
                      if(entityObj != null)
                      {
                        ((DealEntity)entityObj).close();
                      }
                    }
                  }
                  break;
                case BRConstants.INDEX_VARIABLE: // index variable
                  entityObjs = (Object[])aField.get(anEntity);
                  // instantiate object if null
                  if(entityObjs == null)
                  {
                    entityClass = anEntity.getClass();

                    parameterTypes = new Class[]
                      {
                      String.class};
                    method = entityClass.getMethod("fetchField", parameterTypes);
                    arguments = new Object[]
                      {
                      currentNode.entityName};
                    entityObjs = (Object[])method.invoke(anEntity, arguments);
                  }

                  // use the first item of the array since we don't know the value
                  // of the index yet
                  if(entityObjs != null && Array.getLength(entityObjs) > 0)
                  {
                    entityObj = entityObjs[0];
                  }
                  else
                  {
                    entityObj = phantomClass(currentNode.entityName);
                    if(entityObj != null)
                    {
                      ((DealEntity)entityObj).close();
                    }
                  }
                  break;
                default: // indexed array
                  entityObjs = (Object[])aField.get(anEntity);

                  // instantiate object if null
                  if(entityObjs == null)
                  {
                    entityClass = anEntity.getClass();

                    parameterTypes = new Class[]
                      {
                      String.class};
                    method = entityClass.getMethod("fetchField", parameterTypes);
                    arguments = new Object[]
                      {
                      currentNode.entityName};
                    entityObjs = (Object[])method.invoke(anEntity, arguments);
                  }

                  /* 2001-03-19
                                             int arrayIndex = currentNode.arrayIndex - 1;
                                             if (Array.getLength(entityObjs) <= arrayIndex) {
                                                errorMsg = "Entity array index \'" + currentNode.arrayIndex
                                                         + "\' out of bound!";
                                                throw new ParseException(errorMsg, 0);
                                                }
                                             else
                                                entityObj = entityObjs[arrayIndex];
                   */
                  entityObj = entityObjs[0];
              }

              currentNode.attribute = parseDealEntity(entityObj, token);
            }
            catch(Exception ex)
            {
              throw new ParseException(ex.getMessage(), 0);
            }
            //bIsPartOf = false;
          }
      } // switch
    } // while

    if(bArray)
    {
      // parsing error, array index syntax error
      errorMsg = "Missing array index closing operator ']'";
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.wran("Problem @parseDealEntityNode : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }

    return true;
  }

  protected BRVarNode parseDealEntity(Object anEntity, String token) throws ParseException
  {
    int index, length;
    String curToken, nextToken = null, tmpToken;

    index = token.indexOf('.');
    if(index >= 0)
    {
      length = token.length();
      curToken = token.substring(0, index);
      nextToken = token.substring(index + 1, length);
    }
    else
    {
      curToken = token;

    }
    curToken = MosEntities.mapEntityName(curToken);
    if(nextToken != null)
    {
      nextToken = MosEntities.mapEntityName(nextToken);

    }
    if(curToken.compareTo("deal") == 0)
    {
      tmpToken = nextToken;
      nextToken = null;
      index = tmpToken.indexOf('.');
      if(index >= 0)
      {
        length = tmpToken.length();
        curToken = tmpToken.substring(0, index);
        nextToken = tmpToken.substring(index + 1, length);
      }
      else
      {
        curToken = tmpToken;
      }
    }

    // initialize an entity node
    BRVarNode currentNode = new BRVarNode(this, anEntity, curToken);

    // leaf node
    if(nextToken == null)
    { // e.g. totalLoanAmount or borrower[1].totalIncomeAmount
      parseDealEntityNode(anEntity, currentNode);
    }
    else
    { // qualified attribute, e.g. property.purchasePrice
      Class classObj = anEntity.getClass();
      // get object as declared intity
      try
      {
        Field aField = classObj.getDeclaredField(currentNode.entityName);

        Class entityClass = aField.getType();
        if(entityClass.isArray())
        {
          Object[] entityObjs = (Object[])aField.get(anEntity);
          currentNode.attribute = new BRVarNode(this, entityObjs, nextToken);
          currentNode.arrayIndex = BRConstants.ARRAY_ITERATOR;
        }
        else
        {
          Object entityObj = aField.get(anEntity);
          if(entityObj == null)
          {
            entityObj = phantomClass(currentNode.entityName);
            if(entityObj == null)
            {
              //--> Debug enhancement : by Billy 23Aug2004
              errorMsg = "No such field: " + currentNode.entityName;
              BRUtil.wran("Problem @parseDealEntity : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }

            ((DealEntity)entityObj).close();
          }
          currentNode.attribute = parseDealEntity(entityObj, nextToken);
        }
      }
      catch(Exception e)
      {
        //--> Debug enhancement : by Billy 23Aug2004
        errorMsg = "No such field: " + currentNode.entityName;
        BRUtil.wran("Problem @parseDealEntity : " + errorMsg);
        //==========================================

        throw new ParseException(errorMsg, 0);
      }
    }

    return currentNode;
  }

  protected BRFunctNode parseDBEntity() throws ParseException
  {
    int i, size;
    BRFunctNode functNode;

    functNode = parseDBFetch();

    if(functNode != null)
    {
      size = functNode.parameters.size();
      functNode.parameters.add(null);
      functNode.parameters.add(null);
      functNode.parameters.add(null);

      --size;
      for(i = size; i >= 0; --i)
      {
        functNode.parameters.set(i + 3, functNode.parameters.get(i));
      }
      functNode.parameters.set(0, owner);
      functNode.parameters.set(1, tableName);
      functNode.parameters.set(2, colName);
      functNode.nParmCount += 3;
    }
    return functNode;
  }

  /*---------------------------------------------------------------------------
      input:   bMulti-line if set to true, multiple lines will be scanned until encountering
           a "}" character.  If set to false, one line will be parsed.
      ----------------------------------------------------------------------------*/
  protected DoNode parseDoBlock(boolean bMulti) throws ParseException
  {
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    String token;
    Object stmtObj;
    DoNode doNode = new DoNode(this);

    while(!tokens.isEmpty() && --nStack > 0)
    {
      // get next token
      token = (String)(tokens.get(0));
      if(token.equalsIgnoreCase("else"))
      {
        if(bMulti)
        {
          errorMsg = "Missing } before statement keyword \'else\'";
          BRUtil.wran("Problem @parseDoBlock : " + errorMsg);
          throw new ParseException(errorMsg, 0);
        }
        return doNode;
      }

      if(doNode.nStmts == 1 && !bMulti)
      {
        return doNode;
      }

      switch(token.charAt(0))
      {
        case '}':
          if(doNode.nStmts > 0)
          {
            return doNode;
          }
          return null;
        case ';':
          tokens.remove(0);
          break;
        default:
          stmtObj = parseStatement(token);
          doNode.stmts.add(stmtObj);
          ++doNode.nStmts;
      }
    }

    return doNode;
  }

  protected Object parseElseStmtX()
  {
    return null;
  }

  protected Object parseExpressionPhase1() throws ParseException
  {
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    String token = null;
    Object nodeObj, prevOperator;
    Class classObj;
    BRExpNode expNode = new BRExpNode(this);

    while(!tokens.isEmpty() && --nStack > 0)
    {
      // get current token
      token = (String)(tokens.get(0));
      prevToken = null;

      if(token.length() == 0)
      {
        tokens.remove(0);
        // unary operator cannot follow a constant, a variable or a function
        operands.push(token);
      }
      else if(token.charAt(0) == ')' && leftParenthesisCount == 0)
      {
        tokens.remove(0);
        shiftReduceExpression1();
        if(operators.size() > 0)
        {
          nodeObj = operators.peek();
          classObj = nodeObj.getClass();
          if(classObj.isInstance(expNode) && ((BRExpNode)nodeObj).OpName.charAt(0) == '(')
          {
            operators.pop();
          }
        }
// 2001-01-23: break stmt terminates nested (()) prematurely, don't know why it
// is needed here
//            break;
      }
      else if(token.charAt(0) == ')' && leftParenthesisCount > 0)
      {
        tokens.remove(0);
        shiftReduceExpression1();
        if(operators.size() > 0)
        {
          nodeObj = operators.peek();
          classObj = nodeObj.getClass();
          if(classObj.isInstance(expNode) && ((BRExpNode)nodeObj).OpName.charAt(0) == '(')
          {
            operators.pop();
          }
        }
        --leftParenthesisCount;
// 2001-01-23: break stmt terminates nested (()) prematurely, don't know why it
// is needed here
//            break;
      }
      else if(isTerminator(token))
      {
        tokens.remove(0);
        shiftReduceExpression1();
        break;
      }
      else if(isNegativeNumber(token))
      {
        tokens.remove(0);
        token = "-" + tokens.get(0);
        tokens.remove(0);
        operands.push(token);
      }
      else if(isOperator(token))
      { // check for operator
        tokens.remove(0);
        parseOperator(token);
      }
      else if(isStmtKeyword(token))
      {
        break;
      }
      else if(isAssignmentOperator(token))
      {
        tokens.remove(0);
        parseAssignment(token);
      }
      else if(isUnsupportedOpertor(token))
      {
        tokens.remove(0);
        errorMsg = "Syntax error, invalid operator: " + token;
        //--> Debug enhancement : by Billy 23Aug2004
        BRUtil.wran("Problem @parseExpressionPhase1 : " + errorMsg);
        //==========================================
        throw new ParseException(errorMsg, 0);
      }
      else if(isFunction(token))
      { // check for function
        tokens.remove(0);
        nodeObj = parseFunction(token);

        if(operators.size() > 0)
        {
          prevOperator = operators.peek();
          classObj = prevOperator.getClass();
          if(classObj.isInstance(expNode))
          {
            expNode = (BRExpNode)prevOperator;
            if(expNode.OpName.equals("!"))
            {
              expNode.setLeftNode(nodeObj);
              operators.pop();
              nodeObj = expNode;
            }
          }
        }

        operands.push(nodeObj);
      }
      else if(isLocalVariable(token))
      {
        tokens.remove(0);
        nodeObj = parseLocalVariable(token);
        operands.push(nodeObj);
      }
      else if(isGlobalVariable(token))
      {
        tokens.remove(0);
        nodeObj = parseGlobalVariable(token);
        operands.push(nodeObj);
      }
      else if(BRUtil.isDealEntity((Object)dealObj, token))
      { // check for deal object
        tokens.remove(0);
        nodeObj = parseDealEntity(dealObj, token);
        operands.push(nodeObj);
      }
      else if(isDBEntity(token))
      { // check for database object
        tokens.remove(0);
        nodeObj = parseDBEntity();
        operands.push(nodeObj);
      }
      else if(isBusineRuleStmt(token))
      {
        tokens.remove(0);
        nodeObj = parseStatement(token);
        operands.push(nodeObj);
      }
      else
      { // variable, constant
        tokens.remove(0);
        // unary operator cannot follow a constant, a variable or a function
        operands.push(token);
      }
    }

    if(leftParenthesisCount > 0)
    {
      //--> Debug enhancement : by Billy 23Aug2004
      errorMsg = "Missing " + leftParenthesisCount + " right parenthesis!";
      BRUtil.wran("Problem @parseExpressionPhase1 : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }

    return shiftReduceExpression2();
  }

  protected Object parseExpressionPhase2(Object rootNode) throws ParseException
  {
    Class classObj;
    BRExpNode expNode = new BRExpNode(this);
    BRFunctNode functNode = new BRFunctNode(this);
    BRAssignNode assignNode = new BRAssignNode(this);
    IfNode ifNode = new IfNode(this);
    DoNode doNode = new DoNode(this);
    BRVarNode varNode = new BRVarNode(this);

    // Mapp constants, deal attributes and database variables
    try
    {
      classObj = rootNode.getClass();
      if(classObj.isInstance(expNode))
      {
        ((BRExpNode)rootNode).parseExpressionPhase2();
      }
      else if(classObj.isInstance(functNode))
      {
        ((BRFunctNode)rootNode).parseExpressionPhase2();
      }
      else if(classObj.isInstance(assignNode))
      {
        ((BRAssignNode)rootNode).parseExpressionPhase2();
      }
      else if(classObj.isInstance(ifNode))
      {
        ((IfNode)rootNode).parseExpressionPhase2();
      }
      else if(classObj.isInstance(varNode))
      {
        ((BRVarNode)rootNode).parseExpressionPhase2();
      }
      else if(classObj.isInstance(doNode))
      {
        ((DoNode)rootNode).parseExpressionPhase2();
      }
      else
      {
        errorMsg = "Syntax error, expression: \"" + expression
          + "\" error: Invalid node object \"" + rootNode
          + "\" detected during Phase 2 Parsing!";
        errorCode = -1;
        //--> Debug enhancement : by Billy 23Aug2004
        BRUtil.wran("Problem @parseExpressionPhase2 : " + errorMsg);
        //==========================================

        throw new ParseException(errorMsg, 0);
      }
    }
    catch(ParseException e)
    {
      errorMsg = "Syntax error, expression: \"" + expression
        + "\" during phase 2 parsing, error: " + e.getMessage();
      errorCode = -1;
      throw new ParseException(errorMsg, 0);
    }
    catch(NullPointerException e)
    {
      errorMsg = "Unknown Interpretor error, expression: \"" + expression
        + "\" error: NullPointerExceptioin detected during phase 2 parsing!!!!";
      errorCode = -1;
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.wran("Problem @parseExpressionPhase2 : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }
    return null;
  }

  protected void parseConditionExpression() throws ParseException
  {
    int nStack = BRConstants.MAX_TOKEN_DEPTH;
    String token = null;
    Object nodeObj;

    while(!tokens.isEmpty() && --nStack > 0)
    {
      // get current token
      token = (String)(tokens.get(0));
      prevToken = null;

      if(leftParenthesisCount == 0 && token.charAt(0) == ')')
      {
        shiftReduceExpression1();
        return;
      }

      tokens.remove(0);

      if(isOperator(token))
      { // check for operator
        parseOperator(token);
      }
      else if(isAssignmentOperator(token))
      {
        parseAssignment(token);
      }
      else if(isUnsupportedOpertor(token))
      {
        errorMsg = "Syntax error, invalid operator: " + token;
        //--> Debug enhancement : by Billy 23Aug2004
        BRUtil.wran("Problem @parseConditionExpression : " + errorMsg);
        //==========================================

        throw new ParseException(errorMsg, 0);
      }
      else if(isFunction(token))
      { // check for function
        nodeObj = parseFunction(token);
        operands.push(nodeObj);
      }
      else if(BRUtil.isDealEntity((Object)dealObj, token))
      { // check for deal object
        nodeObj = parseDealEntity(dealObj, token);
        operands.push(nodeObj);
      }
      else if(isDBEntity(token))
      { // check for database object
        nodeObj = parseDBEntity();
        operands.push(nodeObj);
      }
      else if(isBusineRuleStmt(token))
      {
        nodeObj = parseStatement(token);
        operands.push(nodeObj);
      }
      else if(isLocalVariable(token))
      {
        nodeObj = parseLocalVariable(token);
        operands.push(nodeObj);
      }
      else if(isGlobalVariable(token))
      {
        nodeObj = parseGlobalVariable(token);
        operands.push(nodeObj);
      }
      else
      { // variable, constant
        // unary operator cannot follow a constant, a variable or a function
        operands.push(token);
      }
    }

    if(leftParenthesisCount > 0)
    {
      //--> Debug enhancement : by Billy 23Aug2004
      errorMsg = "Missing " + leftParenthesisCount + " right parenthesis!";
      BRUtil.wran("Problem @parseConditionExpression : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }

    shiftReduceExpression1();
  }
}
