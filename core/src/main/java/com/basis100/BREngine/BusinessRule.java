package com.basis100.BREngine;

import java.lang.Exception;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Vector;

import com.basis100.deal.util.StringUtil;
import com.basis100.jdbcservices.JdbcConnection;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.FilogixCache;
import com.filogix.util.Xc;

/**
 * 
 * @mosified Maida
 * @date July 30, 2009
 * changed based on CIBC Performance enhancement
 *
 */
public class BusinessRule
{
  private BRInterpretor interpretor = null;
  private boolean bTemplate = false;
  protected Statement queryStmt = null;
  protected Statement execStmt = null;
  protected ResultSet resultSet = null;
  public RuleEntity ruleEntity = null;
  public RuleResult ruleResult = null;

  protected int errorCode;
  protected String sqlState;
  protected String errorMsg;
  protected Deal dealObj;

  // For optimazation -- By Billy 03Oct2001
  static boolean cachMode = (PropertiesCache.getInstance().getProperty(-1,
			Xc.COM_BASIS100_STARTUP_ENABLE_BUSINESS_RULE_CACH, "N")).equals("Y"); // default = false
  //static private HashMap businessRuleCach = new HashMap();
  private FilogixCache businessRuleCach = FilogixCache.getInstance();

  // =======================================

  protected boolean cleanUpConnection = true;

  // rule entities prefetched when using external connection
  Vector ruleEntitiesPreFetched = null;
  int numPreFetched = 0;
  int currNdxPreFetched = 0;

  //****** WARNING **********
  // NOT USED
  // NOT TESTED FOR ML
  //*************************
  public BusinessRule() throws SQLException
  {

    interpretor = new BRInterpretor(dealObj);

    if(!BRInterpretor.bLoaded)
    {
      interpretor.initialize_driver();

    }
    interpretor.initialize_connection();

    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }
  }

  //****** WARNING **********
  // NOT USED
  // NOT TESTED FOR ML
  //*************************
  public BusinessRule(boolean bTemplate)
  {

    interpretor = new BRInterpretor(dealObj);

    /*
           BJH - Aug 11 - not needed right?

           if (!BRInterpretor.bLoaded)
      interpretor.initialize_driver();

           interpretor.initialize_connection();
     */

    this.bTemplate = true;
  }

  //****** WARNING **********
  // NOT USED
  // NOT TESTED FOR ML
  //*************************
  public BusinessRule(int dealId, boolean bTemplate) throws SQLException
  {
    interpretor = new BRInterpretor(dealObj);

    if(!BRInterpretor.bLoaded)
    {
      interpretor.initialize_driver();

    }
    interpretor.initialize_connection();

    if(dealId == -1)
    {
      dealObj = new Deal(interpretor, bTemplate);
    }
    else
    {
      dealObj = new Deal(interpretor, dealId);
    }
    this.bTemplate = true;
  }

  //****** WARNING **********
  // NOT USED
  // NOT TESTED FOR ML
  //*************************
  public BusinessRule(int workflowId, int taskId, String taskEvent,
    String ruleName, int instId) throws SQLException, ParseException
  {

    interpretor = new BRInterpretor(dealObj);

    if(!BRInterpretor.bLoaded)
    {
      interpretor.initialize_driver();

    }
    interpretor.initialize_connection();

    reQuery(workflowId, taskId, taskEvent, ruleName, instId);
  }

  //****** WARNING **********
  // NOT USED
  // NOT TESTED FOR ML
  //*************************
  public BusinessRule(int workflowId, int taskId, String taskEvent,
    String ruleName, String ruleExpression, int instId) throws SQLException, ParseException
  {

    interpretor = new BRInterpretor(dealObj);

    if(!BRInterpretor.bLoaded)
    {
      interpretor.initialize_driver();

    }
    interpretor.initialize_connection();

    reQuery(workflowId, taskId, taskEvent, ruleName, ruleExpression, instId);
  }

  //
  // Following accept an external connection
  //
  // before being called this, VPD must be set for this connection.
  public BusinessRule(SessionResourceKit srk, int workflowId, int taskId, String taskEvent,
	    String ruleName) throws SQLException, ParseException
  {

    interpretor = new BRInterpretor(dealObj);
    setConnection(srk.getJdbcExecutor().getCon());
    cleanUpConnection = false;

    reQuery(workflowId, taskId, taskEvent, ruleName, srk.getExpressState().getDealInstitutionId());
  }
  
// called from Ingestion
  public BusinessRule(SessionResourceKit srk, int workflowId, int taskId, String taskEvent,
    String ruleName, String ruleExpression) throws SQLException, ParseException
  {

    interpretor = new BRInterpretor(dealObj);
    setConnection(srk.getJdbcExecutor().getCon());
    cleanUpConnection = false;

    reQuery(workflowId, taskId, taskEvent, ruleName, ruleExpression, srk.getExpressState().getDealInstitutionId());
  }

  public BusinessRule(Connection connObj, boolean bTemplate)
  {

    interpretor = new BRInterpretor(dealObj);
    setConnection(connObj);
    cleanUpConnection = false;

    this.bTemplate = true;
  }

  public void close() throws SQLException
  {
    if(resultSet != null)
    {
      resultSet.close();
      resultSet = null;
    }
    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }

    if(cleanUpConnection == true)
    {
      interpretor.closeConnection();
    }
  }

  public void setDealId(int dealId) throws SQLException
  {
    dealObj = new Deal(interpretor, dealId);
    dealObj.close();

    interpretor.setGlobalSymbol("DealId", new Integer(dealId)); // NEW - BJH
    interpretor.setGlobalSymbol("CopyId", new Integer(1)); // NEW - BJH
    // NEW for ML
    interpretor.setGlobalSymbol("InstitutionProfileId", 
                                new Integer(dealObj.institutionProfileId)); 
  }

  public void setDealId(int dealId, int dCopyId) throws SQLException
  {
    dealObj = new Deal(interpretor, dealId, dCopyId);
    if(dealObj != null)
    {
      interpretor.setDealObj(dealObj);
    }
    dealObj.close();

    interpretor.setGlobalSymbol("DealId", new Integer(dealId)); // NEW - BJH
    interpretor.setGlobalSymbol("CopyId", new Integer(dCopyId)); // NEW - BJH
    // NEW for ML
    interpretor.setGlobalSymbol("InstitutionProfileId", 
                                new Integer(dealObj.institutionProfileId)); 
  }
  
  // #2242: Catherine, 23-Nov-05 --------- start ---------  
  public void setLenderName()
  {
    // find the lender profile
    LenderProfile lp;
    try {
      lp = new LenderProfile(interpretor);
      interpretor.setGlobalSymbol("LenderName", lp.lenderName);
      BRUtil.debug("BusinessRule@setLenderName(): global LenderName = " + lp.lenderName);

      lp.close();//LEN189642: Hiro, 5-Jun-07
      
    } catch (SQLException e) {
      BRUtil.debug("BusinessRule@setLenderName(): error!"  + StringUtil.stack2string(e));
    }
    
  }
  //#2242: Catherine, 23-Nov-05 --------- end --------- 
  
  // #4271: Midori, 16-Aug-06 --------- start ---------  
  public void setRequestId(int requestId)
  {
      interpretor.setGlobalSymbol("RequestId", new Integer(requestId));
  }
  // #4271: Midori, 16-Aug-06 --------- end ---------  

  // MCM Team 10 July 2008 --------- start ---------  
  public void setComponentId(int componentId)
  {
      interpretor.setGlobalSymbol("ComponentId", new Integer(componentId));
  }
  // MCM Team 10 July 2008 --------- end ---------  
  
  public void setGlobal(String varName, Object value)
  {
    interpretor.setGlobalSymbol(varName, value);
  }

  public Object getGlobal(String varName)
  {
    return interpretor.getGlobalSymbol(varName);
  }

  public void destroyGlobal(String varName)
  {
    interpretor.destroyGlobalSymbol(varName);
  }

  public void setConnection(Connection connObj)
  {
    if(interpretor != null)
    {
      interpretor.setConnection(connObj);
    }
  }

  public void setDeal(Deal dealObj)
  {
    this.dealObj = dealObj;
  }

// yml: 2000-08-23

  public int getRowCount(Object rows)
  {
    if((rows.getClass()).isArray())
    {
      return Array.getLength(rows);
    }

    return 0;
  }

  public Sql_Column[] getMetaData(Object rsObj)
  {
    return interpretor.getMetaData(rsObj);
  }

// yml: end

  public Deal getDeal()
  {
    return this.dealObj;
  }

  protected boolean isTypeBoolean(Object node)
  {
    Class classObj;
    BRExpNode expNode = new BRExpNode(interpretor);
    BRFunctNode functNode = new BRFunctNode(interpretor);

    // rule expression must return boolean value
    classObj = node.getClass();
    if(classObj.isInstance(expNode))
    {
      return((BRExpNode)node).isTypeBoolean();
    }
    if(classObj.isInstance(functNode))
    {
      return((BRFunctNode)node).isTypeBoolean();
    }

    return false;
  }

  private void preFetchRules() throws SQLException
  {
    if(resultSet == null)
    {
      return; // precaution
    }

    Vector v = new Vector();

    while(next())
    {
      v.add(ruleEntity);
      numPreFetched++;
    }

    ruleEntitiesPreFetched = v;

    ruleEntity = null;

    // close resources

    if(resultSet != null)
    {
      resultSet.close();
      resultSet = null;
    }
    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }
  }

  private void clearPreFetched()
  {
    ruleEntitiesPreFetched = null;
    numPreFetched = 0;
    currNdxPreFetched = 0;
  }

  public boolean next() throws SQLException
  {
    if(ruleEntitiesPreFetched != null)
    {
      if(currNdxPreFetched >= numPreFetched)
      {
        return false; // precaution ... will never happen
      }

      ruleEntity = (RuleEntity)ruleEntitiesPreFetched.elementAt(currNdxPreFetched);
      currNdxPreFetched++;

      return true;
    }

    int j;
    boolean bStatus = false;
    String strTemp;

    if(bTemplate)
    {
      return true;
    }

    bStatus = resultSet.next();
    if(bStatus == false)
    {
      return false;
    }

    ruleEntity = new RuleEntity();

    ruleEntity.ruleName = resultSet.getString(1).trim();
    ruleEntity.ruleDesc = resultSet.getString(2);
    if(ruleEntity.ruleDesc != null)
    {
      ruleEntity.ruleDesc = ruleEntity.ruleDesc.trim();

    }
    ruleEntity.workflowId = resultSet.getShort(3);
    ruleEntity.taskId = resultSet.getShort(4);
    ruleEntity.taskEvent = resultSet.getString(5).trim();
    ruleEntity.ruleExpression = resultSet.getString(6).trim();
    ruleEntity.rulePrivilege = resultSet.getShort(7);
    ruleEntity.ruleOrder = resultSet.getShort(8);
    ruleEntity.ruleState = resultSet.getShort(9);
    ruleEntity.opCount = (short)resultSet.getInt(10);

    j = 11;
    for(int i = 0; i < ruleEntity.opCount; ++i)
    {
      ruleEntity.opType[i] = resultSet.getShort(j);
      ruleEntity.opCat[i] = resultSet.getShort(j + 1);
      strTemp = resultSet.getString(j + 2);
      if(strTemp == null)
      {
        ruleEntity.opValue[i] = null;
      }
      else
      {
        ruleEntity.opValue[i] = strTemp.trim();
      }
      j += 3;
    }

    ruleEntity.extClassName = resultSet.getString(35);
    if(ruleEntity.extClassName != null)
    {
      ruleEntity.extClassName = ruleEntity.extClassName.trim();

    }
    ruleEntity.extClassType = resultSet.getShort(36);

    ruleEntity.extMethod = resultSet.getString(37);
    if(ruleEntity.extMethod != null)
    {
      ruleEntity.extMethod = ruleEntity.extMethod.trim();
    }
    ruleEntity.procType = resultSet.getShort(38);

    //CLOB Performance Enhancement June 2010
    //==================================================================
    int len = resultSet.getInt("actionproc_len");
    // Dynamically retrieve action based on the length 
    if(len <= 4000)
    {
    	// if Length less than 4000 chars then get it as VarChar(4000)
    	ruleEntity.actionProc = resultSet.getString("actionproc_in_varchar");
    }
    else
    {
    	// if Length MORE than 4000 chars then get it from CLOB as usual)
    	ruleEntity.actionProc = resultSet.getString("actionproc");
    }
    //==================================================================
    if(ruleEntity.actionProc != null)
    {
      ruleEntity.actionProc = ruleEntity.actionProc.trim();
    }

    BRUtil.debug("@next : ruleName: " + ruleEntity.ruleName + " ruleDesc: " + ruleEntity.ruleDesc);

    return true;
  }

  public int getErrorCode() throws SQLException
  {
    return errorCode;
  }

  public String getSQLState() throws SQLException
  {
    return sqlState;
  }

  public String getMessage() throws SQLException
  {
    return errorMsg;
  }

  public String getExpression()
  {
    return ruleEntity.ruleExpression;
  }

  public boolean insert() throws SQLException
  {
    return true;
  }
  
  // THIS METHOD SHOULD NOT BE USED AFTER ML
  // IT DOESN"T HAVE CAPABLE FOR ML
  public boolean insert(RuleEntity ruleEntity) throws SQLException
  {
    int i, rowCount;
    String sqlStmt, errorMsg;

    if(ruleEntity == null)
    {
      errorMsg = "BusinessRule.insert method parameter error: Invalid null "
        + "RuleEntity object specified!";
      throw new SQLException(errorMsg);
    }

    if(ruleEntity.ruleName.length() <= 0)
    {
      errorMsg = "BusinessRule.insert method parameter error: "
        + "RuleName cannot be null or empty!";
      throw new SQLException(errorMsg);
    }

    if(ruleEntity.taskEvent == null)
    {
      errorMsg = "BusinessRule.insert method parameter error: "
        + "taskEvent cannot be null or empty!";
      throw new SQLException(errorMsg);
    }

    if(ruleEntity.ruleExpression == null || ruleEntity.ruleExpression.length() <= 0)
    {
      errorMsg = "BusinessRule.insert method parameter error: "
        + "Rule expression cannot be null or empty!";
      throw new SQLException(errorMsg);
    }

    // construct the sql statement for the insert
    sqlStmt = "insert into BusinessRule values(" +
      "\'" + doubleQuoteQuote(ruleEntity.ruleName) + "\',";

    if(ruleEntity.ruleDesc == null || ruleEntity.ruleDesc.length() <= 0)
    {
      sqlStmt += "null,";
    }
    else
    {
      sqlStmt += "\'" + doubleQuoteQuote(ruleEntity.ruleDesc) + "\',";

    }
    sqlStmt += ruleEntity.workflowId + "," + ruleEntity.taskId + ","
      + "\'" + doubleQuoteQuote(ruleEntity.taskEvent) + "\',\'"
      + doubleQuoteQuote(ruleEntity.ruleExpression)
      + "\'," + ruleEntity.rulePrivilege + "," + ruleEntity.ruleOrder
      + "," + ruleEntity.ruleState + "," + ruleEntity.opCount;

    // setup output parameter from user
    for(i = 0; i < ruleEntity.opCount; ++i)
    {
      sqlStmt += "," + ruleEntity.opType[i] + "," + ruleEntity.opCat[i] + ",";
      if(ruleEntity.opType[i] == 0)
      {
        sqlStmt += "null";
      }
      else
      {
        sqlStmt += "\'" + doubleQuoteQuote(ruleEntity.opValue[i]) + "\'";
      }
    }

    // standard default for output parameters not used
    for(i = ruleEntity.opCount; i < 8; ++i)
    {
      sqlStmt += ",0,0,null";
    }

    if(ruleEntity.extClassName == null)
    {
      sqlStmt += ",null,";
    }
    else
    {
      sqlStmt += ",\'" + ruleEntity.extClassName + "\',";

    }
    sqlStmt += ruleEntity.extClassType + ",";

    if(ruleEntity.extMethod == null)
    {
      sqlStmt += "null,";
    }
    else
    {
      sqlStmt += "\'" + ruleEntity.extMethod + "\',";

    }
    sqlStmt += ruleEntity.procType;

    if(ruleEntity.actionProc == null)
    {
      sqlStmt += ",null)";
    }
    else
    {
      sqlStmt += ",\'" + doubleQuoteQuote(ruleEntity.actionProc) + "\')";

      // reallocate sql statement every time
    }
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }

    if(execStmt == null)
    {
      // instantiate an sql statement
      execStmt = interpretor.getConnection().createStatement();
    }

    //--> Debug enhancement : by Billy 20Aug2004
    BRUtil.debug("@BusinessRule.insert SQL: " + sqlStmt);
    //==================================================

    try
    {
      rowCount = execStmt.executeUpdate(sqlStmt);
      if(rowCount <= 0)
      {
        rowCount = -1;
        errorMsg = "Record not inserted: " + sqlStmt;
        errorCode = -1;
        sqlState = "";
      }
    }
    catch(SQLException e)
    {
      rowCount = -1;
      errorMsg = e.getMessage();
      errorCode = e.getErrorCode();
      sqlState = e.getSQLState();
      throw e;
    }

    if(rowCount < 0)
    {
      return false;
    }

    return true;
  }

  public boolean delete() throws SQLException
  {
    int rowCount;
    String sqlStmt, filter;

    if(ruleEntity == null)
    {
      throw new SQLException("Rule entity cannot be deleted without first performing a next()!");
    }

    if(ruleEntity.ruleName.length() <= 0)
    {
      throw new SQLException("RuleName cannot be null or empty!");
    }

    if(ruleEntity.taskEvent == null)
    {
      throw new SQLException("Task event cannot be null!");
    }

    if(ruleEntity.ruleExpression == null || ruleEntity.ruleExpression.length() <= 0)
    {
      throw new SQLException("Rule expression cannot be null or empty!");
    }

    // construct the sql statement for the delete
    filter = "ruleName=\'" + ruleEntity.ruleName + "\' and workFlowId=" +
      ruleEntity.workflowId + " and taskId=" +
      ruleEntity.taskId + " and taskEvent=\'" +
      ruleEntity.taskEvent + "\' and expression=\'" +
      doubleQuoteQuote(ruleEntity.ruleExpression) + "\'";

    sqlStmt = "delete from BusinessRule where " + filter;

    // reallocate sql statement every time
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }

    if(execStmt == null)
    {
      // instantiate an sql statement
      execStmt = interpretor.getConnection().createStatement();
    }

    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.debug("@BusinessRule.delete SQL: " + sqlStmt);
    //==================================================

    try
    {
      rowCount = execStmt.executeUpdate(sqlStmt);
      if(rowCount <= 0)
      {
        rowCount = -1;
        errorMsg = "Record not found with filter: where " + filter;
        errorCode = -1;
        sqlState = "";
      }
    }
    catch(SQLException e)
    {
      rowCount = -1;
      errorMsg = e.getMessage();
      errorCode = e.getErrorCode();
      sqlState = e.getSQLState();
    }

    if(rowCount < 0)
    {
      return false;
    }

    return true;
  }

  public boolean delete(RuleEntity ruleEntity) throws SQLException
  {
    int rowCount;
    String sqlStmt, filter;

    if(ruleEntity == null)
    {
      throw new SQLException("Invalid null RuleEntity object specified!");
    }

    if(ruleEntity.ruleName.length() <= 0)
    {
      throw new SQLException("RuleName cannot be null or empty!");
    }

    if(ruleEntity.taskEvent == null)
    {
      throw new SQLException("Task event cannot be null!");
    }

    if(ruleEntity.ruleExpression == null || ruleEntity.ruleExpression.length() <= 0)
    {
      throw new SQLException("Rule expression cannot be null or empty!");
    }

    // construct the sql statement for the delete
    filter = "ruleName=\'" + ruleEntity.ruleName + "\' and workFlowId=" +
      ruleEntity.workflowId + " and taskId=" +
      ruleEntity.taskId + " and taskEvent=\'" +
      ruleEntity.taskEvent + "\' and expression=\'" +
      doubleQuoteQuote(ruleEntity.ruleExpression) + "\'";

    sqlStmt = "delete from BusinessRule where " + filter;

    // reallocate sql statement every time
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }

    if(execStmt == null)
    {
      // instantiate an sql statement
      execStmt = interpretor.getConnection().createStatement();
    }

    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.debug("@BusinessRule.delete SQL: " + sqlStmt);
    //==================================================

    rowCount = execStmt.executeUpdate(sqlStmt);
    if(rowCount <= 0)
    {
      rowCount = -1;
      errorMsg = "Record not found with filter: where " + filter;
      errorCode = -1;
      sqlState = "";
    }
//         }
//      catch (SQLException e) {
//         rowCount = -1;
//         errorMsg = e.getMessage();
//         errorCode = e.getErrorCode();
//         sqlState = e.getSQLState();
//         }

    if(rowCount < 0)
    {
      return false;
    }

    return true;
  }

  public boolean exist(int workflowId, int taskId, String taskEvent,
    String ruleName, String ruleExpression) throws SQLException, ParseException
  {
    String sqlStmt, filter, errorMsg;

    if(ruleName.length() <= 0)
    {
      errorMsg = "Java method parameter error: ruleName cannot be null or empty!";
      throw new ParseException(errorMsg, 0);
    }

    if(taskEvent == null)
    {
      errorMsg = "Java method parameter error: taskEvent cannot be null!";
      throw new ParseException(errorMsg, 0);
    }

    if(ruleExpression == null || ruleExpression.length() <= 0)
    {
      errorMsg = "Rule Expression cannot be null or empty!";
      throw new ParseException(errorMsg, 0);
    }

    // construct the sql statement for the delete
    filter = "ruleName=\'" + ruleName + "\' and workFlowId="
      + workflowId + " and taskId=" + taskId + " and taskEvent=\'"
      + taskEvent + "\'";

    if(ruleExpression != null)
    {
      filter += " and expression=\'" + ruleExpression + "\'";

    }
    sqlStmt = "select count(ruleName) from BusinessRule";

    int rowCount = fetchRowCount(sqlStmt, filter);

    return rowCount > 0 ? true : false;
  }

  private String doubleQuoteQuote(String token)
  {
    int nStart = 0, nEnd, length;
    String newToken = "";

    length = token.length();
    while((nEnd = token.indexOf('\'', nStart)) >= 0)
    {
      newToken += token.substring(nStart, ++nEnd);
      newToken += '\'';
      nStart = nEnd;
    }
    if(nStart < length)
    {
      newToken += token.substring(nStart, length);
    }

    return newToken;
  }

  public RuleResult evaluate() throws ParseException
  {
    boolean bActionProc = false;
    Class classObj;
    Object rsObj, procResult;
    Integer intObj = new Integer(0);
    Boolean boolObj = new Boolean(true);

    ruleResult = new RuleResult(ruleEntity);
    interpretor.removeOutParams();
    rsObj = evaluateRuleExpression(ruleEntity.ruleExpression);
//      ruleResult.ruleMetaData = interpretor.getMetaData();
    ruleResult.result = rsObj;
    classObj = rsObj.getClass();
    if(classObj.isInstance(boolObj))
    {
      ruleResult.ruleStatus = ((Boolean)rsObj).booleanValue();
    }
    else if(classObj.isInstance(intObj))
    {
      ruleResult.ruleStatus = ((Integer)rsObj).intValue() > 0 ? true : false;
    }
    else
    {
      errorMsg = "Business rule expression must return a boolean value!";
      throw new ParseException(errorMsg, 0);
    }

    if(ruleResult.result == null)
    {
      return null;
    }

    // no need to evaluate furhter if the qualifying condition is false
    if(ruleResult.ruleStatus)
    {
      // evaluate actionProc depends on whetehr actionProc is rule extension or not
      switch(ruleEntity.procType)
      {
        case BRConstants.OP_RULE_EXTENSION:
          procResult = evaluateRuleActionProc();
          classObj = procResult.getClass();
          if(classObj.isInstance(boolObj))
          {
            bActionProc = ((Boolean)procResult).booleanValue();
          }
          else if(classObj.isInstance(intObj))
          {
            bActionProc = ((Integer)procResult).intValue() > 0 ? true : false;
          }
          else
          {
            errorMsg = "Business rule expression extension must return a boolean value!";
            throw new ParseException(errorMsg, 0);
          }
          if(bActionProc)
          {
            evaluateOutputParams();
          }
          else
          {
            ruleResult.ruleStatus = false;
            return ruleResult;
          }
          break;
        case BRConstants.OP_RULE_ACTION:
          evaluateOutputParams();
          evaluateRuleActionProc();
          overrideOutputParams();
          break;
        default:
          evaluateOutputParams();
          overrideOutputParams();
      }
    }
    return ruleResult;
  }

// ymlam: 2000-08-23
  private Object evaluateNode(Object rootNode) throws ParseException
  {
    Object result;
    Class classObj = rootNode.getClass();
    String strObj = "";
    Integer intObj = new Integer(0);
    Double doubleObj = new Double(0.0);
    Date dateObj = new Date(0);
    Time timeObj = new Time(0);
    Timestamp tsObj = new Timestamp(0);
    BRExpNode expNode = new BRExpNode(interpretor);
    BRFunctNode functNode = new BRFunctNode(interpretor);
    BRVarNode varNode = new BRVarNode(interpretor);
    BRAssignNode assignNode = new BRAssignNode(interpretor);

    if(classObj.isInstance(expNode))
    {
      result = ((BRExpNode)rootNode).evaluate();
    }
    else if(classObj.isInstance(functNode))
    {
      result = ((BRFunctNode)rootNode).evaluate();
    }
    else if(classObj.isInstance(varNode))
    {
      result = ((BRVarNode)rootNode).evaluate();
    }
    else if(classObj.isInstance(assignNode))
    {
      result = ((BRAssignNode)rootNode).evaluate();
    }
    else if(classObj.isInstance(strObj))
    {
      result = (String)rootNode;
    }
    else if(classObj.isInstance(intObj))
    {
      result = rootNode;
    }
    else if(classObj.isInstance(doubleObj))
    {
      result = rootNode;
    }
    else if(classObj.isInstance(dateObj))
    {
      result = rootNode;
    }
    else if(classObj.isInstance(timeObj))
    {
      result = rootNode;
    }
    else if(classObj.isInstance(tsObj))
    {
      result = rootNode;
    }
    else
    {
      result = rootNode;

    }
    return result;
  }

  private void overrideOutputParams() throws ParseException
  {
    Object rsObj;

    for(int i = 0; i < ruleEntity.opCount; ++i)
    {
      // extract each $OutParamx if available
      rsObj = interpretor.lookUpOutParam(i);
      if(rsObj != null)
      {
        rsObj = ((VarNode)rsObj).evaluate();
        if(rsObj != null)
        {
          ruleResult.opValue[i] = rsObj;
        }
      }
    }
  }

  private void evaluateOutputParams() throws ParseException
  {
    Object opValue;

    // evaluate all output parameters
    for(int i = 0; i < ruleEntity.opCount; ++i)
    {
      // evaluate each output parameters expression/constant provided
      // the actionProc had not over-rode the v
      opValue = evaluateRuleOutput(i);
      BRUtil.debug("result opValue [" + i + "] = " + opValue);
      ruleResult.opValue[i] = opValue;
    }
  }

  private Object evaluateRuleActionProc() throws ParseException
  {
    Object rootNode, result = null;

    if(ruleEntity.actionProc == null)
    {
      return new Boolean(true);
    }

    // scanning expression
    if(interpretor == null)
    {
      interpretor = new BRInterpretor(dealObj);
    }

    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.debug("@BusinessRule.evaluateRuleActionProc : " + ruleEntity.actionProc);
    //==================================================

    interpretor.scanExpression(ruleEntity.actionProc);

    // parse expression
    rootNode = interpretor.parseDoBlock(true);

    if(rootNode == null)
    {
      errorCode = -1;
      sqlState = "";
      errorMsg = "Syntax error, expression: \"" + ruleEntity.ruleExpression
        + "\" error: Invalid node object <" + rootNode
        + "> detected during Phase 1 Parsing!";

      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.wran("Problem @BusinessRule.evaluateRuleActionProc : " + errorMsg);
      //==================================================

      throw new ParseException(errorMsg, 0);
    }

    // map all viables into real value
    interpretor.parseExpressionPhase2(rootNode);
//      interpretor.substituteUserVars(rootNode);
    result = interpretor.evaluateExpression(rootNode);

    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.debug("@BusinessRule.evaluateRuleActionProc Result: " + result);
    //==================================================

    return result;
  }

  public Object evaluateRuleExpression(String ruleExpression) throws ParseException
  {
    Object rootNode;
    Object result;

    if(ruleExpression == null || ruleExpression.length() <= 0)
    {
      errorMsg = "Rule Expression cannot be null or empty!";
      throw new ParseException(errorMsg, 0);
    }

    // scanning expression
    if(interpretor == null)
    {
      interpretor = new BRInterpretor(dealObj);
    }

    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.debug("@BusinessRule.evaluateRuleExpression : RuleName = " + ruleEntity.ruleName + " : " + ruleExpression);
    //==================================================

    interpretor.scanExpression(ruleExpression);

    // parse expression
    rootNode = interpretor.parseExpressionPhase1();

    if(rootNode == null)
    {
      errorCode = -1;
      sqlState = "";
      errorMsg = "Syntax error, expression: \"" + ruleEntity.ruleExpression
        + "\" error: Invalid node object <" + rootNode
        + "> detected during Phase 1 Parsing!";

      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.wran("Problem @BusinessRule.evaluateRuleExpression : RuleName = " +
        ruleEntity.ruleName + " : " + errorMsg);
      //==================================================

      throw new ParseException(errorMsg, 0);
    }

    if(!isTypeBoolean(rootNode))
    {
      errorCode = -1;
      errorMsg = "Syntax error, expression: \"" + ruleEntity.ruleExpression
        + "\" does not return boolean value!";

      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.wran("Problem @BusinessRule.evaluateRuleExpression : RuleName = " +
        ruleEntity.ruleName + " : " + errorMsg);
      //==================================================

      throw new ParseException(errorMsg, 0);
    }

    // Mapp constants, deal attributes and database variables
    interpretor.parseExpressionPhase2(rootNode);

    // evaluate expression
    result = interpretor.evaluateExpression(rootNode);

    return result;
  }

// yml: 2000-08-23
  public Object evaluateRuleOutputExpression(int parmType, int parmCat,
    String parmExpression) throws ParseException
  {
    Object rootNode = null;
    Object result;

    if(parmExpression == null || parmExpression.length() <= 0)
    {
      errorMsg = "Syntax error: rule parameter expression cannot be null or empty!";
      throw new ParseException(errorMsg, 0);
    }

    // no need to parse parmValue if it is not an expression
    if(parmCat != BRConstants.OP_CAT_EXPRESSION)
    {
      rootNode = parmExpression;
    }
    else
    {
      // scanning expression
      if(interpretor == null)
      {
        interpretor = new BRInterpretor(dealObj); // should never happen!
      }
      interpretor.scanExpression(parmExpression);

      // parse expression
      rootNode = interpretor.parseExpressionPhase1();

      if(rootNode == null)
      {
        errorCode = -1;
        sqlState = "";
        errorMsg = "Syntax error, unable to compile output expression: \""
          + ruleEntity.ruleExpression + "\"!";
        throw new ParseException(errorMsg, 0);
      }
    }

    // Mapp constants, deal attributes and database variables
    if((rootNode.getClass()).isInstance(""))
    {
      // (must be) constant -> conversion from string representation if required
      rootNode = parseConstant(parmType, rootNode);
    }
    else
    {
      parseNodePhase2(rootNode);
    }

    // evaluate expression
    result = evaluateNode(rootNode);

    // check parmType versus result
    if(result != null)
    {
      validateDataType(parmType, result);

    }
    return result;
  }

  private Object parseConstant(int parmType, Object node) throws ParseException
  {
    String errorMsg = null;
    String strParmType = null;

    String str = "";

    if(!(node.getClass()).isInstance(str))
    {
      // not a string!
      errorMsg = "Inalid parameter (constant): <" + node + ">";
      errorCode = -1;
      sqlState = "";
      throw new ParseException(errorMsg, 0);
    }

    str = (String)node;

    try
    {
      switch(parmType)
      {
        case Types.INTEGER:
        case Types.BIGINT:
        case Types.NUMERIC:
        case Types.SMALLINT:
        case Types.TINYINT:
          strParmType = "SmallInt/Integer";
          node = BRUtil.mapInt(str);
          break;
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
          strParmType = "String";
          break;
        case Types.DECIMAL:
        case Types.DOUBLE:
        case Types.FLOAT:
        case Types.REAL:
          strParmType = "Double";
          node = BRUtil.mapDouble(str);
          break;
        case Types.BIT:
          strParmType = "Boolean";
          break;
        case Types.DATE:
          strParmType = "Date";
          node = Date.valueOf(str);
          break;
        case Types.TIME:
          strParmType = "Time";
          node = Time.valueOf(str);
          break;
        case Types.TIMESTAMP:
          strParmType = "Timestamp";
          node = Timestamp.valueOf(str);
          break;
        default:
          int i = 5;
          int j = 0;
          strParmType = "Unsupported type: " + parmType;
          i = i / j; // generate exception!
      }
    }
    catch(Exception e)
    {
      errorMsg = "Invalid output parameter (constant), type = <" + strParmType
        + ">, value = < " + str + ">";

      errorCode = -1;
      sqlState = "";
      throw new ParseException(errorMsg, 0);
    }

    return node;
  }

  private Object parseNodePhase1()
  {
    return null;
  }

  private void parseNodePhase2(Object rootNode) throws ParseException
  {
    BRExpNode expNode = new BRExpNode(interpretor);
    BRFunctNode functNode = new BRFunctNode(interpretor);
    BRVarNode varNode = new BRVarNode(interpretor);
    BRAssignNode assignNode = new BRAssignNode(interpretor);
    Class classObj = rootNode.getClass();

    if(classObj.isInstance(expNode)) // expression
    {
      ((BRExpNode)rootNode).parseExpressionPhase2();
    }
    else if(classObj.isInstance(functNode)) // function
    {
      ((BRFunctNode)rootNode).parseExpressionPhase2();
    }
    else if(classObj.isInstance(assignNode))
    {
      ((BRAssignNode)rootNode).parseExpressionPhase2();
    }
    else if(classObj.isInstance(varNode)) // variable
    {
      // do nothing
    }
  }

  private Object evaluateRuleOutput(int index) throws ParseException
  {
    String errorMsg;

    // index is zero based internally
    if(index < 0 || index > ruleEntity.opCount)
    {
      errorMsg = "Invalid output parameter index: <" + index + ">";
      errorCode = -1;
      sqlState = "";
      throw new ParseException(errorMsg, 0);
    }

// yml: 2000-08-23
//      return evaluateRuleOutputExpression(ruleEntity.opType[index],
//                    ruleEntity.opCat[index], ruleEntity.opValue[index]);
    return evaluateRuleOutputExpression(ruleEntity.opType[index],
      ruleEntity.opCat[index], ruleEntity.opValue[index]);
  }

  private int fetchRowCount(String selectClause, String filter) throws SQLException
  {
    int rowCount = 0;
    String sqlStmt;
    Statement queryStmt;
    ResultSet resultSet = null;

    // Create a Statement for query
    queryStmt = interpretor.getConnection().createStatement();

    sqlStmt = selectClause + " where " + filter;
    resultSet = queryStmt.executeQuery(sqlStmt);

    if(resultSet.next())
    {
      rowCount = resultSet.getInt("COUNT(RULENAME)");
    }

    resultSet.close();
    queryStmt.close();

    return rowCount;
  }

  public boolean reQuery(String selectStmt) throws SQLException
  {
    clearPreFetched();

    if(resultSet != null)
    {
      resultSet.close();
      resultSet = null;
    }

    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }

    queryStmt = interpretor.getConnection().createStatement();

    // BR table result Caching -- By Billy 03Oct2001
    // Check if we already stored the results
    boolean rc;
    if(isCachMod() == true)
    {
      ruleEntitiesPreFetched = (Vector)businessRuleCach.get(selectStmt, 
    		  BusinessRule.class.getName()+ "_" + dealObj.institutionProfileId);

      if(ruleEntitiesPreFetched == null)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.debug("@BusinessRule.reQuery SQL Stmt: " + selectStmt);
        //==================================================

        // not in cach yet -- then Cach it !!
        resultSet = queryStmt.executeQuery(selectStmt);

        if(resultSet != null)
        {
          preFetchRules();
          businessRuleCach.put(selectStmt, ruleEntitiesPreFetched, 
        		  BusinessRule.class.getName() + "_" + dealObj.institutionProfileId);
          rc = true;
        }
        else
        {
          rc = false;
        }
      }
      else
      {
        currNdxPreFetched = 0;
        numPreFetched = ruleEntitiesPreFetched.size();
        rc = true;
      }
    }
    else
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.debug("@BusinessRule.reQuery SQL Stmt: " + selectStmt);
      //==================================================

      resultSet = queryStmt.executeQuery(selectStmt);
      rc = resultSet == null ? false : true;
      // pre-fetch rule (entities) if external connection
      if(cleanUpConnection == false)
      {
        preFetchRules();
      }
    }

    // =========== End of Changes ================

    return rc;
  }

  public boolean reQuery(int workflowId, int taskId, String taskEvent,
    String ruleName, int instId) throws SQLException, ParseException
  {
    clearPreFetched();

    //String selectStmt = "select * from BusinessRule";
    
    ////CLOB Performance Enhancement June 2010
    String selectStmt = 
    "select rulename, ruledesc, workflowid, taskid, taskevent, expression, " +
    "ruleprivilege, ruleorder, rulestate, opcount, optype1, opcat1, opvalue1, optype2, opcat2, opvalue2, " +
    "optype3, opcat3, opvalue3, optype4, opcat4, opvalue4, optype5, opcat5, opvalue5, optype6, opcat6, " +
    "opvalue6, optype7, opcat7, opvalue7, optype8, opcat8, opvalue8, extclassname, extclasstype, " +
    "extmethod, proctype, " +
    "actionproc, " +
    "length(actionproc) actionproc_len, " +
    "CAST(SUBSTR(actionproc,1,4000) as VARCHAR(4000)) actionproc_in_varchar , " +
    "institutionprofileid " +
    " from BusinessRule";
    //==================================================

    
    String whereClause = "";

    if(resultSet != null)
    {
      resultSet.close();
      resultSet = null;
    }

    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }

    // parameter check
    /*
          if (ruleName == null || ruleName.length() <= 0) {
             errorMsg = "Rule Name cannot be null or empty string!";
             throw new ParseException(errorMsg, 0);
             }

          if (taskEvent == null || taskEvent.length() <= 0) {
             errorMsg = "TaskEvent cannot be null or empty string!";
             throw new ParseException(errorMsg, 0);
             }
     */
    // Create a Statement for query
    queryStmt = interpretor.getConnection().createStatement();
    /*
          selectStmt = "select * from BusinessRule where workflowId=" + workflowId;
          selectStmt += " and taskId=" + taskId;
          selectStmt += " and taskEvent=\'" + taskEvent + "\'";
          selectStmt += " and ruleName=\'" + ruleName + "\'";
     */

    if(workflowId != BRConstants.NULL_INTEGER)
    {
      whereClause += "workflowId = " + workflowId;
    }

    if(taskId != BRConstants.NULL_INTEGER)
    {
      if(whereClause.length() > 0)
      {
        whereClause += " and ";
      }
      whereClause += "taskId=" + taskId;
    }

    if(taskEvent != null)
    {
      if(whereClause.length() > 0)
      {
        whereClause += " and ";
      }
      whereClause += "taskEvent ";

      if(taskEvent.indexOf('%') >= 0)
      {
        whereClause += "like \'";
      }
      else
      {
        whereClause += "= \'";
      }
      whereClause += taskEvent + "\'";
    }

    if(ruleName != null)
    {
      if(whereClause.length() > 0)
      {
        whereClause += " and ";

      }
      whereClause += "ruleName ";
      if(ruleName.indexOf('%') >= 0)
      {
        whereClause += "like \'";
      }
      else
      {
        whereClause += "= \'";
      }
      whereClause += ruleName + "\'";
      
    }

    if(whereClause.length() > 0)
    {
      selectStmt += " where " + whereClause;

    }
    selectStmt += " order by ruleOrder";

    boolean rc;
    if(isCachMod() == true)
    {
      ruleEntitiesPreFetched = (Vector)businessRuleCach.get(selectStmt, 
    		  BusinessRule.class.getName() + "_" + instId);

      if(ruleEntitiesPreFetched == null)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.debug("@BusinessRule.reQuery SQL Stmt: " + selectStmt);
        //==================================================

        // not in cach yet -- then Cach it !!
        resultSet = queryStmt.executeQuery(selectStmt);

        if(resultSet != null)
        {
          preFetchRules();
          businessRuleCach.put(selectStmt, ruleEntitiesPreFetched,
        		  BusinessRule.class.getName() + "_" + instId);
          rc = true;
        }
        else
        {
          rc = false;
        }
      }
      else
      {
        currNdxPreFetched = 0;
        numPreFetched = ruleEntitiesPreFetched.size();
        rc = true;
      }
    }
    else
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.debug("@BusinessRule.reQuery SQL Stmt: " + selectStmt);
      //==================================================

      resultSet = queryStmt.executeQuery(selectStmt);
      rc = resultSet == null ? false : true;
      // pre-fetch rule (entities) if external connection
      if(cleanUpConnection == false)
      {
        preFetchRules();
      }
    }

    // =========== End of Changes ================

    return rc;
  }

  public boolean reQuery(int workflowId, int taskId, String taskEvent,
    String ruleName, String ruleExpression, int instId) throws SQLException, ParseException
  {
    clearPreFetched();
    
    //CLOB Performance Enhancement June 2010
    String selectStmt = 
    "select rulename, ruledesc, workflowid, taskid, taskevent, expression, " +
    "ruleprivilege, ruleorder, rulestate, opcount, optype1, opcat1, opvalue1, optype2, opcat2, opvalue2, " +
    "optype3, opcat3, opvalue3, optype4, opcat4, opvalue4, optype5, opcat5, opvalue5, optype6, opcat6, " +
    "opvalue6, optype7, opcat7, opvalue7, optype8, opcat8, opvalue8, extclassname, extclasstype, " +
    "extmethod, proctype, " +
    "actionproc, " +
    "length(actionproc) actionproc_len, " +
    "CAST(SUBSTR(actionproc,1,4000) as VARCHAR(4000)) actionproc_in_varchar , " +
    "institutionprofileid " +
    " from BusinessRule";
    //==================================================

    
    String whereClause = "";

    if(resultSet != null)
    {
      resultSet.close();
      resultSet = null;
    }

    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }

    // parameter check
    /*
          if (ruleName == null || ruleName.length() <= 0) {
             errorMsg = "Rule Name cannot be null or empty string!";
             throw new ParseException(errorMsg, 0);
             }

          if (taskEvent == null || taskEvent.length() <= 0) {
             errorMsg = "TaskEvent cannot be null or empty string!";
             throw new ParseException(errorMsg, 0);
             }

          if (ruleExpression == null || ruleExpression.length() <= 0) {
             errorMsg = "Rule Expression cannot be null or empty string!";
             throw new ParseException(errorMsg, 0);
             }
     */
    // Create a Statement for query
    queryStmt = interpretor.getConnection().createStatement();
    /*
          selectStmt = "select * from BusinessRule where workflowId=" + workflowId;
          selectStmt += " and taskId=" + taskId;
          selectStmt += " and taskEvent=\'" + taskEvent + "\'";
          selectStmt += " and ruleName=\'" + ruleName + "\'";
          selectStmt += " and expression=\'" + ruleExpression + "\'";
     */
    if(workflowId != BRConstants.NULL_INTEGER)
    {
      whereClause += "workflowId=" + workflowId;
    }

    if(taskId != BRConstants.NULL_INTEGER)
    {
      if(whereClause.length() > 0)
      {
        whereClause += " and ";
      }
      whereClause += "taskId=" + taskId;
    }

    if(taskEvent != null)
    {
      if(whereClause.length() > 0)
      {
        whereClause += " and ";
      }
      whereClause += "taskEvent ";

      if(taskEvent.indexOf('%') >= 0)
      {
        whereClause += "like \'";
      }
      else
      {
        whereClause += "= \'";
      }
      whereClause += taskEvent + "\'";
    }

    if(ruleName != null)
    {
      if(whereClause.length() > 0)
      {
        whereClause += " and ";

      }
      whereClause += " ruleName ";
      if(ruleName.indexOf('%') >= 0)
      {
        whereClause += "like \'";
      }
      else
      {
        whereClause += "= \'";
      }
      whereClause += ruleName + "\'";
    }

    if(ruleExpression != null)
    {
      if(whereClause.length() > 0)
      {
        whereClause += " and ";

      }
      whereClause += " expression ";
      if(ruleExpression.indexOf('%') >= 0)
      {
        whereClause += "like \'";
      }
      else
      {
        whereClause += "= \'";
      }
      whereClause += ruleExpression + "\'";
    }

    if(whereClause.length() > 0)
    {
      selectStmt += " where " + whereClause;

    }
    selectStmt += " order by ruleOrder";

    boolean rc;
    if(isCachMod() == true)
    {
      ruleEntitiesPreFetched = (Vector)businessRuleCach.get(selectStmt,
    		  BusinessRule.class.getName()+ "_" + dealObj.institutionProfileId);

      if(ruleEntitiesPreFetched == null)
      {
        //--> Debug enhancement : by Billy 26Aug2004
        BRUtil.debug("@BusinessRule.reQuery SQL Stmt: " + selectStmt);
        //==================================================

        // not in cach yet -- then Cach it !!
        resultSet = queryStmt.executeQuery(selectStmt);

        if(resultSet != null)
        {
          preFetchRules();
          businessRuleCach.put(selectStmt, ruleEntitiesPreFetched,
        		  BusinessRule.class.getName() + "_" + dealObj.institutionProfileId);
          rc = true;
        }
        else
        {
          rc = false;
        }
      }
      else
      {
        currNdxPreFetched = 0;
        numPreFetched = ruleEntitiesPreFetched.size();
        rc = true;
      }
    }
    else
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.debug("@BusinessRule.reQuery SQL Stmt: " + selectStmt);
      //==================================================

      resultSet = queryStmt.executeQuery(selectStmt);
      rc = resultSet == null ? false : true;
      // pre-fetch rule (entities) if external connection
      if(cleanUpConnection == false)
      {
        preFetchRules();
      }
    }

    // =========== End of Changes ================

    return rc;
  }

  public void setOpCount(int count)
  {
    if(ruleEntity != null)
    {
      ruleEntity.opCount = (short)count;
    }
  }

  public void setUserTypeId(int anInt)
  {
    interpretor.setGlobalSymbol("UserTypeId", new Integer(anInt));
  }

  public void setRecordId(int anInt)
  {
    interpretor.setGlobalSymbol("RecordId", new Integer(anInt));
  }

  public boolean update() throws SQLException
  {
    int i, k, rowCount;
    String sqlStmt, updClause, filter;

    if(ruleEntity == null)
    {
      throw new SQLException("Invalid null RuleEntity object specified!");
    }

    // construct the filter statement
    filter = "ruleName=\'" + ruleEntity.ruleName + "\' and workflowId="
      + ruleEntity.workflowId + " and taskId=" + ruleEntity.taskId
      + " and taskEvent=\'" + ruleEntity.taskEvent + "\' and expression=\'"
      + doubleQuoteQuote(ruleEntity.ruleExpression) + "\'";

    updClause = "ruleDesc=";
    if(ruleEntity.ruleDesc == null || ruleEntity.ruleDesc.length() <= 0)
    {
      updClause += "null,";
    }
    else
    {
      updClause += "\'" + ruleEntity.ruleDesc + "\',";

    }
    updClause += "ruleOrder=" + ruleEntity.ruleOrder + ",opCount=" +
      ruleEntity.opCount;

    for(i = 0; i < ruleEntity.opCount; ++i)
    {
      k = i + 1;
      updClause += ",opType" + k + "=" + ruleEntity.opType[i]
        + ",opCat" + k + "=" + ruleEntity.opCat[i]
        + ",opValue" + k;
      if(ruleEntity.opType[i] == 0)
      {
        updClause += "=null";
      }
      else
      {
        updClause += "=\'" + doubleQuoteQuote(ruleEntity.opValue[i]) + "\'";
      }
    }

    if(ruleEntity.extClassName == null)
    {
      updClause += ",extClassName=null,";
    }
    else
    {
      updClause += ",extClassName=\'" + ruleEntity.extClassName + "\',";

    }
    updClause += "extClassType=" + ruleEntity.extClassType;

    if(ruleEntity.extMethod == null)
    {
      updClause += ",extMethod=null";
    }
    else
    {
      updClause += ",extMethod=\'" + ruleEntity.extMethod + "\'";

    }
    if(ruleEntity.actionProc == null)
    {
      updClause += ",actionProc=null";
    }
    else
    {
      updClause += ",actionProc=\'" + doubleQuoteQuote(ruleEntity.actionProc)
        + "\'";

      // construct the sql statement for the insert
    }
    sqlStmt = "update BusinessRule set " + updClause + " where " + filter;

    // reallocate sql statement every time
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }

    if(execStmt == null)
    {
      execStmt = interpretor.getConnection().createStatement();
    }

    try
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.debug("@BusinessRule.update SQL Stmt: " + sqlStmt);
      //==================================================

      rowCount = execStmt.executeUpdate(sqlStmt);
      if(rowCount <= 0)
      {
        rowCount = -1;
        errorMsg = "Record not found with filter: where " + filter;
        errorCode = -1;
        sqlState = "";
      }
    }
    catch(SQLException e)
    {
      rowCount = -1;
      errorMsg = e.getMessage();
      errorCode = e.getErrorCode();
      sqlState = e.getSQLState();
      throw e;
    }

    if(rowCount < 0)
    {
      return false;
    }

    return true;
  }

  // you can only update the following fields:
  //   - ruleDesc, ruleOrder, opCount, opType1, opValue1 ... opType8, opValue8,
  //     extClassName, extClassType, extMethod
  // to update other field, you must use delete and reinsert
  public boolean update(RuleEntity ruleEntity) throws SQLException
  {
    int i, k, rowCount;
    String sqlStmt, updClause, filter, errorMsg;

    if(ruleEntity == null)
    {
      errorMsg = "BusinessRule.update(ruleEntity) parameter error: "
        + "Invalid null RuleEntity object specified!";
      throw new SQLException(errorMsg);
    }

    if(ruleEntity.ruleName.length() <= 0)
    {
      errorMsg = "BusinessRule.update(ruleEntity) parameter error: "
        + "RuleName cannot be null or empty!";
      throw new SQLException(errorMsg);
    }

    if(ruleEntity.taskEvent == null)
    {
      errorMsg = "BusinessRule.update(ruleEntity) parameter error: "
        + "Task event cannot be null or empty!";
      throw new SQLException(errorMsg);
    }

    if(ruleEntity.ruleExpression == null || ruleEntity.ruleExpression.length() <= 0)
    {
      errorMsg = "BusinessRule.update(ruleEntity) parameter error: "
        + "Rule expression cannot be null or empty!";
      throw new SQLException(errorMsg);
    }

    // construct the filter statement
    filter = "ruleName=\'" + ruleEntity.ruleName + "\' and workflowId="
      + ruleEntity.workflowId + " and taskId=" + ruleEntity.taskId
      + " and taskEvent=\'" + ruleEntity.taskEvent + "\' and expression=\'"
      + doubleQuoteQuote(ruleEntity.ruleExpression) + "\'";

    updClause = "";
    boolean bUpdate = false;

    if(ruleEntity.ruleDesc != null && ruleEntity.ruleDesc.length() > 0)
    {
      updClause += "ruleDesc=\'" + ruleEntity.ruleDesc + "\'";
      bUpdate = true;
    }

    if(bUpdate)
    {
      updClause += ",";
    }
    else
    {
      bUpdate = true;

    }
    updClause += "ruleOrder=" + ruleEntity.ruleOrder + ",opCount=" +
      ruleEntity.opCount;

    k = 0;
    for(i = 0; i < ruleEntity.opCount; ++i)
    {
      ++k;
      updClause += ",opType" + k + "=" + ruleEntity.opType[i]
        + ",opCat" + k + "=" + ruleEntity.opCat[i] + ",opValue"
        + k;
      if(ruleEntity.opType[i] == 0)
      {
        updClause += "=null";
      }
      else
      {
        updClause += "=\'" + doubleQuoteQuote(ruleEntity.opValue[i]) + "\'";
      }
    }

    if(ruleEntity.extClassName != null && ruleEntity.extClassName.length() > 0)
    {
      updClause += ",extClassName=\'" + ruleEntity.extClassName + "\'";

    }
    updClause += ",extClassType=" + ruleEntity.extClassType;

    if(ruleEntity.extMethod != null && ruleEntity.extMethod.length() > 0)
    {
      updClause += ",extMethod=\'" + ruleEntity.extMethod + "\'";

    }
    updClause += ",procType=" + ruleEntity.procType;

    if(ruleEntity.actionProc != null && ruleEntity.actionProc.length() > 0)
    {
      updClause += ",actionProc=\'" + doubleQuoteQuote(ruleEntity.actionProc) + "\'";

      // construct the sql statement for the insert
    }
    sqlStmt = "update BusinessRule set " + updClause + " where " + filter;

    // reallocate sql statement every time
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }

    if(execStmt == null)
    {
      execStmt = interpretor.getConnection().createStatement();
    }

    try
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.debug("@BusinessRule.update SQL Stmt: " + sqlStmt);
      //==================================================

      rowCount = execStmt.executeUpdate(sqlStmt);
      if(rowCount <= 0)
      {
        rowCount = -1;
        errorMsg = "Record not found with filter: where " + filter;
        errorCode = -1;
        sqlState = "";
      }
    }
    catch(SQLException e)
    {
      rowCount = -1;
      errorMsg = e.getMessage();
      errorCode = e.getErrorCode();
      sqlState = e.getSQLState();
      throw e;
    }

    if(rowCount < 0)
    {
      return false;
    }

    return true;
  }

  public boolean validate() throws ParseException
  {
    Object rootNode;
    Class classObj;
    String expression, msg;
    String strObj = "";

    // validate rule expression
    try
    {
      rootNode = validate(ruleEntity.ruleExpression);
    }
    catch(Exception ex)
    {
      msg = "Syntax error in rule expression: " + ruleEntity.ruleExpression;
      throw new ParseException(msg, 0);
    }

    if(rootNode == null)
    {
      msg = "Syntax error in rule expression: " + ruleEntity.ruleExpression;
      throw new ParseException(msg, 0);
    }

    classObj = rootNode.getClass();
    if(classObj.isInstance(strObj))
    {
      msg = "Syntax error in rule expression: " + ruleEntity.ruleExpression;
      throw new ParseException(msg, 0);
    }

    // validate all output output
    for(int i = 0; i < ruleEntity.opCount; ++i)
    {
      expression = ruleEntity.opValue[i];
      if(expression != null && expression.length() > 0
        && ruleEntity.opCat[i] == BRConstants.OP_CAT_EXPRESSION)
      {
        rootNode = validate(expression);
        if(rootNode == null)
        {
          msg = "Syntax error in output expression[" + (i + 1) +
            "]: " + expression;
          throw new ParseException(msg, 0);
        }
      }
    }

    // validate rule actionProc
    expression = ruleEntity.actionProc;
    if(expression != null && expression.length() > 0)
    {
      try
      {
        rootNode = validate(expression);
      }
      catch(Exception ex)
      {
        msg = "Syntax error in action procedure: " + expression;
        throw new ParseException(msg, 0);
      }
    }

    return true;
  }

  protected Object validate(String ruleExpression) throws ParseException
  {
    Object rootNode;

    if(ruleExpression == null || ruleExpression.length() <= 0)
    {
      errorMsg = "Rule Expression cannot be null or empty!";
      throw new ParseException(errorMsg, 0);
    }

    if(ruleExpression == null || ruleExpression.length() <= 0)
    {
      errorMsg = "Rule Expression cannot be null or empty!";
      throw new ParseException(errorMsg, 0);
    }

    // scanning expression
    if(interpretor == null)
    {
      interpretor = new BRInterpretor(dealObj);
    }
    interpretor.scanExpression(ruleExpression);

    // parse expression
    rootNode = interpretor.parseExpressionPhase1();

    if(rootNode == null)
    {
      errorMsg = "Syntax error, expression: \"" + ruleEntity.ruleExpression;
      throw new ParseException(errorMsg, 0);
    }

    return rootNode;
  }

  public boolean validateDataType(int parmType, Object resultObj) throws ParseException
  {
    Class classObj = resultObj.getClass();
    String errorMsg = "Invalid output parameter type: <" + classObj.getName() + ">, ";
    String strObj = "";
    Integer intObj = new Integer(0);
    Double doubleObj = new Double(0.0);
    Date dateObj = new Date(0);
    Time timeObj = new Time(0);
    Timestamp tsObj = new Timestamp(0);
    boolean bStatus = true;

    switch(parmType)
    {
      case Types.INTEGER:
      case Types.BIGINT:
      case Types.NUMERIC:
      case Types.SMALLINT:
      case Types.TINYINT:
        if(!classObj.isInstance(intObj))
        {
          throw new ParseException(errorMsg + "short/int expected", 0);
        }
        break;
      case Types.CHAR:
      case Types.VARCHAR:
      case Types.LONGVARCHAR:
        if(!classObj.isInstance(strObj))
        {
          throw new ParseException(errorMsg + "string expected", 0);
        }
        break;
      case Types.DECIMAL:
      case Types.DOUBLE:
      case Types.FLOAT:
      case Types.REAL:
        if(!classObj.isInstance(doubleObj))
        {
          throw new ParseException(errorMsg + "double expecdted", 0);
        }
        break;
      case Types.BIT:
        break;
      case Types.DATE:
        if(!classObj.isInstance(dateObj))
        {
          throw new ParseException(errorMsg + "date expecdted", 0);
        }
        break;
      case Types.TIME:
        if(!classObj.isInstance(timeObj))
        {
          throw new ParseException(errorMsg + "time expecdted", 0);
        }
        break;
      case Types.TIMESTAMP:
        if(!classObj.isInstance(tsObj))
        {
          throw new ParseException(errorMsg + "timestamp expecdted", 0);
        }
        break;
      case Types.JAVA_OBJECT:
        break;
      default:
        bStatus = false;
    }
    return bStatus;
  }

  // Methods to set / get Cach Mode Flag
  static public void setCachMode(boolean mode)
  {
    cachMode = mode;
  }

  static public boolean isCachMod()
  {
    return cachMode;
  }

}
