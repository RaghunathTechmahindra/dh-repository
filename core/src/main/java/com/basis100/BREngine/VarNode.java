/*
change history:
   date     author                 description
 2002-06-12   yml   add setItem(arrayObj, index, element) to allow user construction
                    of array
 2002-06-13   yml   add addItem(object) support
 2002-06-14   yml   deleteItem(arrayObj, 1-based-index) and
                    insertItem(arrayObj, 1-based-index, element) support
 2002-06-14: indexOf(itemObj)
*/
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.Date;

public class VarNode implements Cloneable {
   private BRInterpretor interpretor;
   protected int          arrayIndex;    // -1 if not an array, else zero based
                                        // -2 if dictionary
   protected Object       indexObj;
   protected String       varName;    // e.g. "totalPurchasePrice"
   protected Object       varObj;
   protected String       errorMsg;

   public VarNode(BRInterpretor interpretor) {
      this.interpretor = interpretor;
      arrayIndex = -1;                          // default no index
      this.varName = null;
      varObj = null;
   }

   public VarNode(BRInterpretor interpretor, String name) {
      arrayIndex = BRConstants.DEFERRED_ATTRIBUTE;           // index unknown
      this.varName = name;
      varObj = null;
      this.interpretor = interpretor;
   }

   public VarNode(BRInterpretor interpretor, String name, Object aValue) {
      arrayIndex = BRConstants.DEFERRED_ATTRIBUTE;         // index unknown
      this.varName = name;
      varObj = aValue;
      this.interpretor = interpretor;
   }

   protected VarNode deepCopy() throws ParseException {
      VarNode     varNode;

      try {
         varNode = (VarNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return varNode;
   }

   // method is here as place holder, no logic is needed as there should not
   // be any variable need to be mapped until run time.
   protected boolean parseExpressionPhase2() throws ParseException {
      boolean     bStatus = true;
      switch (arrayIndex)
      {
         case BRConstants.SCALAR_VARIABLE:
         case BRConstants.DEFERRED_ATTRIBUTE:
         case BRConstants.ARRAY_ITERATOR:
            break;
         case BRConstants.INDEX_VARIABLE:
            Class       classObj = indexObj.getClass();
            BRExpNode   expNode       = new BRExpNode(interpretor);
            BRVarNode   cachedVarNode = new BRVarNode(interpretor);
            VarNode     usrVarNode    = new VarNode(interpretor);

            if (classObj.isInstance(cachedVarNode))
               bStatus = ((BRVarNode)indexObj).parseExpressionPhase2();
            else if (classObj.isInstance(usrVarNode))
               bStatus = ((VarNode)indexObj).parseExpressionPhase2();
            else if (classObj.isInstance(expNode))
               bStatus = ((BRExpNode)indexObj).parseExpressionPhase2();
            break;
      }
      return bStatus;
   }

   protected int count() {
      Object result=null;

      try {
         result = evaluate();
      }
      catch (Exception ex) {}

      return (result != null) ? 1 : 0;
   }

   protected Object evaluate() throws ParseException {
      int         index;
      Object[]    arrayObj;
      Class       classObj;

      switch (arrayIndex)
      {
         case BRConstants.SCALAR_VARIABLE:
            return varObj;
         case BRConstants.DEFERRED_ATTRIBUTE:
            return varObj;
         case BRConstants.ARRAY_ITERATOR:
            return varObj;
         case BRConstants.INDEX_VARIABLE:
            classObj = indexObj.getClass();
            Integer     intObj  = new Integer(0);
            BRExpNode   expNode       = new BRExpNode(interpretor);
            BRVarNode   cachedVarNode = new BRVarNode(interpretor);
            VarNode     usrVarNode    = new VarNode(interpretor);

            if (classObj.isInstance(cachedVarNode))
               intObj = (Integer)((BRVarNode)indexObj).evaluate();
            else if (classObj.isInstance(usrVarNode))
               intObj = (Integer)((VarNode)indexObj).evaluate();
            else if (classObj.isInstance(expNode))
               intObj = (Integer)((BRExpNode)indexObj).evaluate();
            else
            {
               String msg = "Invalid array index object: " + classObj.getName();
               throw new ParseException(msg, 0);
            }
            index = intObj.intValue();
            arrayObj = (Object[])varObj;
            if (--index < 0 || index > Array.getLength(arrayObj)) {
               String msg = "Run-time error, array index out of bound!";
               throw new ParseException(msg, 0);
               }
            return arrayObj[index];
         default:
            index = arrayIndex;
            arrayObj = (Object[])varObj;
            if (--index < 0 || index > Array.getLength(arrayObj)) {
               String msg = "Run-time error, array index out of bound!";
               throw new ParseException(msg, 0);
               }
            return arrayObj[index];
      }
   }

   protected Object evaluate(Object value) throws ParseException {
      if ( value != null)
      {
         if (value.getClass().isArray())
            arrayIndex = BRConstants.DEFERRED_ATTRIBUTE;
         else
            arrayIndex = BRConstants.SCALAR_VARIABLE;
      }

      varObj = value;

      return varObj;
   }

   public boolean hasArrayIterator() {
      return (arrayIndex == BRConstants.ARRAY_ITERATOR) ? true : false;
   }

   protected boolean isEmpty(boolean bAny) throws ParseException {
      int       length, nSize;
      String    strObj = "";
      String    emInvArray1 = "Syntax error: a scalar variable: ";
      String    emInvArray2 = " is being used as an array!";
      Object[]    arrayObj;
      Object    rsObj;
      Class     rsClass;

      Object varObj = evaluate();
      if (varObj == null)
         return true;

      Class class1 = varObj.getClass();

      switch (arrayIndex)       {
         case BRConstants.DEFERRED_ATTRIBUTE:
         case BRConstants.ARRAY_ITERATOR:
            if (!class1.isArray()) {
               strObj = emInvArray1 + varName + emInvArray2;
               break;
            }

            arrayObj = (Object[])varObj;
            nSize = Array.getLength(arrayObj);

            // return true if every element is empty, otherwise return false
            for (int i=0; i<nSize; ++i) {
               varObj = arrayObj[i];
               if (varObj == null) {
                  if (bAny)
                     return true;
               }
               else {
                  class1 = varObj.getClass();
                  if (class1.isInstance(strObj)) {
                     length = ((String)varObj).length();
                     if (bAny) {          // any empty object is consider empty even for
                        if (length <= 0)  // the whole aray
                           return true;
                     }
                     else {             // no need to check further as long as one or
                        if (length > 0)  // more element is not empty
                           return false;
                     }
                  }
                  else {
                     return false;
                  }
               }
            }

            return true;
         case BRConstants.SCALAR_VARIABLE:
            rsObj = evaluate();
            if (rsObj == null)
               return true;
            rsClass = rsObj.getClass();
            if (rsClass.isInstance(strObj) && ((String)rsObj).length() == 0)
               return true;
            return false;
         case BRConstants.INDEX_VARIABLE:
            break;
         default:       // constant index
            if (varObj == null)
               return true;
            else if (class1.isInstance(strObj)) {
               return ((String)varObj).length() == 0;
            }

            strObj = "isEmpty() parameter type mismatch: " + class1.getName()
                   + ", expecting string or string array!";
      }

      throw new ParseException(strObj, 0);
   }

   protected boolean isTrue(boolean bAny) throws ParseException {
      return false;
   }

   protected boolean isTypeBoolean() {
      return true;
   }

   protected boolean isUserDefinedVariable() {
      return interpretor.isUserDefinedVar(varName);
   }

   // 2002-06-12: add setItem(index, value)
   protected Object setItem(int index, Object value) throws ParseException {
      Object var = evaluate();
      Class classObj = var.getClass();

      Object[] arrayObj = (Object[])varObj;
      arrayObj[index] = value;

      return new Boolean(true);
   }

   // 2002-06-13
   protected void addItem(Object element) throws ParseException {
      Class     classObj;
      Object [] arrayObj;
      String    strMsg;

      classObj = varObj.getClass();
      if (!classObj.isArray()) {
         strMsg = "Variable type mismatch, addItem function expect array variable as its first argument!";
         throw new ParseException(strMsg, 0);
      }

      arrayObj = (Object [])varObj;
      int nSize = arrayObj.length;
      Object [] newArray = new Object[nSize+1];
      for (int i=0; i<nSize; ++i)
         newArray[i] = arrayObj[i];

      newArray[nSize] = element;
      varObj = (Object)newArray;
   }

   // 2002-06-13
   // index is 0-based
   protected Object deleteItem(int index) throws ParseException {
      Class     classObj;
      Object [] arrayObj;
      String    strMsg;

      classObj = varObj.getClass();
      if (!classObj.isArray()) {
         strMsg = "Variable type mismatch, deleteItem function expect array variable as its first argument!";
         throw new ParseException(strMsg, 0);
      }

      arrayObj = (Object [])varObj;
      int nSize = arrayObj.length;
      int i;
      Object [] newArray = new Object[nSize-1];

      for (i=0; i<index; ++i)
         newArray[i] = arrayObj[i];
      --nSize;
      for (i=index; i<nSize; ++i)
         newArray[i] = arrayObj[i+1];

      varObj = (Object)newArray;

      return new Boolean(true);
   }

   // 2002-06-14: insertItem(0-based-index, itemObj)
   protected Object insertItem(int index, Object element) throws ParseException {
      Class     classObj;
      Object [] arrayObj;
      String    strMsg;

      classObj = varObj.getClass();
      if (!classObj.isArray()) {
         strMsg = "Variable type mismatch, deleteItem function expect array variable as its first argument!";
         throw new ParseException(strMsg, 0);
      }

      arrayObj = (Object [])varObj;
      int nSize = arrayObj.length;
      int i;
      Object [] newArray = new Object[nSize+1];

      for (i=0; i<index; ++i)
         newArray[i] = arrayObj[i];

      newArray[index] = element;

      ++nSize;
      for (i=index+1; i<nSize; ++i)
         newArray[i] = arrayObj[i-1];

      varObj = (Object)newArray;

      return new Boolean(true);
   }

   // 2002-06-14: indexOf(itemObj)
   protected int indexOf(Object element) throws ParseException {
      int       i;
      Class     classObj;
      Object [] arrayObj;
      Object    anObj;
      Integer   intObj = new Integer(0);
      Double    douObj = new Double(0.0);
      String    strObj = "";
      String    strMsg;

      classObj = varObj.getClass();
      if (!classObj.isArray()) {
         strMsg = "Variable type mismatch, indexOf function expect array variable as its first argument!";
         throw new ParseException(strMsg, 0);
      }

      arrayObj = (Object [])varObj;
      int nSize = arrayObj.length;
      classObj = element.getClass();

      for (i=0; i<nSize; ++i) {
         anObj = arrayObj[i];
         if (classObj.isInstance(intObj)) {
            if (anObj.equals(element))
               return i;
         }
         else if (classObj.isInstance(douObj)) {
            if (anObj.equals(element))
               return i;
         }
         else if (classObj.isInstance(strObj)) {
            if (anObj.equals(element))
               return i;
         }
      }

      return -1;
   }

   protected Object sum() throws ParseException {
      Object      resultObj;
      Integer     intObj = new Integer(0);
      Double      doubleObj = new Double(0.0);
      String      strObj = "";
      Object varObj = evaluate();
      Class classObj = varObj.getClass();
      String    emInvArray1 = "Syntax error: a scalar variable: ";
      String    emInvArray2 = " is being used as an array!";

      switch (arrayIndex) {
         case BRConstants.DEFERRED_ATTRIBUTE:
         case BRConstants.ARRAY_ITERATOR:
            if (!classObj.isArray()) {
               strObj = emInvArray1 + varName + emInvArray2;
               break;
            }

            Object[] arrayObj = (Object[])varObj;
            int nSize = Array.getLength(arrayObj);
            int intResult = 0;
            double doubleResult = 0.0;

            for (int i=0; i<nSize; ++i) {
               varObj = arrayObj[i];
               if (varObj != null) {
                  classObj = varObj.getClass();
                  if (classObj.isInstance(intObj)) {
                     intObj = (Integer)varObj;
                     intResult += intObj.intValue();
                  }
                  else if (classObj.isInstance(doubleObj)) {
                     doubleObj = (Double)varObj;
                     doubleResult += doubleObj.doubleValue();
                  }
                  else {
                     strObj = "sum() parameter type mismatch: " + classObj.getName()
                         + ", expecting integer or double variable array!";
                     break;
                  }
               }
            }

            if (classObj.isInstance(intObj))
               return new Integer(intResult);
            else
               return new Double(doubleResult);
         case BRConstants.SCALAR_VARIABLE:
            resultObj = evaluate();
            if (resultObj == null)
               return new Integer(0);

            classObj = resultObj.getClass();
            if (classObj.isInstance(intObj))
               return resultObj;
            else if (classObj.isInstance(doubleObj))
               return resultObj;

            strObj = "sum() parameter type mismatch: " + classObj.getName()
                   + ", expecting integer or double variable array!";

            break;
         case BRConstants.INDEX_VARIABLE:
            break;
         default:       // constant index
            if (varObj == null)
               return new Integer(0);

            if (classObj.isInstance(intObj))
               return intObj;
            else if (classObj.isInstance(doubleObj))
               return doubleObj;

            strObj = "sum() parameter type mismatch: " + classObj.getName()
                   + ", expecting integer or double variable array!";
      }

      throw new ParseException(strObj, 0);
   }

   public String toString()
   {
     return this.varName + "(" + this.varObj +  ")";
   }

}
