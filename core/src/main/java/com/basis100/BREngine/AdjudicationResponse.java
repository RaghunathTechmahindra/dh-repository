/*
 * AdjudicationResponse.java 
 * 
 * Created: 14-MAR-2006
 * Author:  Alfredo Servilla
 */
package com.basis100.BREngine;

import java.sql.SQLException;

public class AdjudicationResponse extends DealEntity {
	
	protected int    responseId;
	protected int    adjudicationStatusId;

	/**
	 * Creates a new entity
	 * @param interpretor
	 * @throws SQLException
	 */
	public AdjudicationResponse(BRInterpretor interpretor) throws SQLException {
		super(interpretor);
	}

	/**
	 * Clone this AdjudicationResponse
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */
	public AdjudicationResponse deepCopy() throws CloneNotSupportedException {
		return (AdjudicationResponse) this.clone();		
	}
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
	protected String getPrimaryKeyName() {
		return "responseId";
	}
	
	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
	protected String getDefaultSQL() {
		return "SELECT * FROM AdjudicationResponse";
	}
	
	/**
	 * Closes the DB connection.
	 * @throws SQLException If an error occurs.
	 */
	protected void close() throws SQLException {
		super.close();
	}
	
	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
	protected boolean next() throws SQLException { 
		boolean bStatus = super.next();
		if (bStatus) {
			responseId = resultSet.getInt("responseId");
			adjudicationStatusId = resultSet.getInt("adjudicationStatusId");
		}
		
		return bStatus;
	}
}
