/* NBC-Implementation-Team  */
package com.basis100.BREngine;
import java.sql.SQLException;



/**
 * <p>
 * Title: BncAdjudicationResponse
 * </p>
 * 
 * <p>
 * Description: BREntity class for BNC Adjudication Response
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC Implementation Team
 * @version 1
 * 
 */

public class BncAdjudicationResponse extends DealEntity {
	
	protected int responseId;
	protected int recommendedCodeId;
	protected double amountInsured;
	protected int primaryApplicantRiskId;
	protected int secondaryApplicantRiskId;
	protected int marketRiskId;
	protected int propertyRiskId;
	protected int neighbourHoodRiskId;
	protected int randomDigit;
	protected double globalNetWorth;
	protected int globalUsedCreditBureauNameId;
	protected String productCode;
	protected int submissionCount;
	protected String miDecision;
	

	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
    protected String getPrimaryKeyName() {
      return "responseId";
    }

	/**
	 * Creates a new BREntity
	 * @param interpretor
	 * @param template
	 * @throws SQLException
	 */
   public BncAdjudicationResponse(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   /**
	 * @param interpretor
	 * @throws SQLException
	 */
   public BncAdjudicationResponse(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   /**
	 * @param BRInterpretor
	 * @param filter
	 * @throws SQLException
	 */
   public BncAdjudicationResponse(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

	/**
	 * Clone this BncAdjudicationResponse
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */ 
   protected BncAdjudicationResponse deepCopy() throws CloneNotSupportedException {
      return (BncAdjudicationResponse)clone();
   }

	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
   protected String getDefaultSQL() {
      return "select * from BncAdjudicationResponse";
   }

	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
    	  
    	        responseId = resultSet.getInt("RESPONSEID");	
    		    recommendedCodeId = resultSet.getInt("RECOMMENDEDCODEID");
    		    amountInsured = resultSet.getDouble("AMOUNTINSURED");
    		    primaryApplicantRiskId = resultSet.getInt("PRIMARYAPPLICANTRISKID");
    		    secondaryApplicantRiskId = resultSet.getInt("SECONDARYAPPLICANTRISKID");
    		    marketRiskId = resultSet.getInt("MARKETRISKID");
    		    propertyRiskId = resultSet.getInt("PROPERTYRISKID");
    		    neighbourHoodRiskId = resultSet.getInt("NEIGHBOURHOODRISKID");
    		    randomDigit = resultSet.getInt("RANDOMDIGIT");
    		    globalNetWorth = resultSet.getDouble("GLOBALNETWORTH");
    		    globalUsedCreditBureauNameId = resultSet.getInt("GLOBALUSEDCREDITBUREAUNAMEID");
    		    productCode = resultSet.getString("PRODUCTCODE");
    		    submissionCount = resultSet.getInt("SUBMISSIONCOUNT");
    		    miDecision = resultSet.getString("MIDECISION");
	
 
      }

      return bStatus;
   }
}
