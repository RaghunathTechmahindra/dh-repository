// Business Rule Expression Scanner, add array
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class BRAssignNode implements Cloneable {
   private BRInterpretor interpretor;
   protected Object   LeftNode;
   protected Object   RightNode;
   protected int      OpCode;
   protected int      Precedence;
   protected int      leafCount;
   protected String   OpName;
   protected Deal     dealObj = null;
   protected String   errorMsg;

   public BRAssignNode(BRInterpretor interpretor) {
      this.interpretor = interpretor;
      LeftNode = null;
      RightNode = null;
      OpCode = '?';
      OpName = "?";
   }

   public BRAssignNode(BRInterpretor interpretor, Deal dealObj, String token) {
      this.interpretor = interpretor;
      LeftNode = null;
      RightNode = null;
      leafCount = 0;
      this.dealObj = dealObj;

      setOperator(token);
   }

   protected void setLeftNode(Object aNode) {
      LeftNode = aNode;
      ++leafCount;
   }

   protected void setRightNode(Object aNode) {
      RightNode = aNode;
      ++leafCount;
   }

   protected void setOperator(String OpName) {
      char     op1, op2;

      this.OpName = OpName;
      op1 = OpName.charAt(0);
      if (OpName.length() > 1)
         op2 = OpName.charAt(1);
      else
         op2 = ' ';

      leafCount = 2;
      OpCode = BRConstants.OP_ASSIGN;
      Precedence = 10;   // highest

      return;
   }

   protected int compareTo(BRExpNode prevNode) {
      return Precedence - prevNode.Precedence;
   }

   protected BRAssignNode deepCopy() throws ParseException {
      BRAssignNode     assignNode;

      try {
         assignNode = (BRAssignNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return assignNode;
   }

   protected boolean parseExpressionPhase2() throws ParseException {
      int         length;
      Class       classObj;
      String      token, errorMsg;
      BRExpNode   expNode = new BRExpNode(interpretor);
      BRFunctNode functNode = new BRFunctNode(interpretor);
      BRVarNode   varNode   = new BRVarNode(interpretor);
      BRDbFieldNode fieldNode = new BRDbFieldNode(interpretor);
      VarNode     tmpVarNode    = new VarNode(interpretor);

      classObj = LeftNode.getClass();
      if (classObj.isInstance(varNode)) {       // do nothing
         }
      else if (classObj.isInstance(fieldNode)) {
         if (!((BRDbFieldNode)LeftNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + LeftNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(tmpVarNode)) {   // do nothing
         }
      else {
         errorMsg = "Invalid Left Node: <" + LeftNode + ">, data object expected!";
         throw new ParseException(errorMsg, 0);
         }

      if (leafCount < 2) {
         errorMsg = "Syntax error: missing right operands on \":=\" operator!";
         throw new ParseException(errorMsg, 0);
         }

      classObj = RightNode.getClass();
      if (classObj.isInstance(expNode)) {
         if (!((BRExpNode)RightNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + RightNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(functNode)) {
         if (!((BRFunctNode)RightNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + RightNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(varNode)) {
         if (!((BRVarNode)RightNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + RightNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(tmpVarNode)) {     // do nothing
         if (!((VarNode)RightNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + RightNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else {
         token = (String)RightNode;
         try {
            if (BRUtil.isInteger(token)) {       // integer
               RightNode = BRUtil.mapInt(token);
               }
            else if (BRUtil.isFloat(token))      // double
               RightNode = BRUtil.mapDouble(token);
            else if (BRUtil.isNull(token))      // null
               RightNode = BRUtil.mapNull(token);
            else {                         // string
               // now strip the string delimeter
               if (token.charAt(0) == '\"') {
                  length = token.length();
                  token = token.substring(1, length-1);
                  }
               RightNode = token;
               }
            }
         catch (Exception e) {
            errorMsg = "Invalid token \"" + token + "\" found at the right of \""
                     + OpName + "\"";
            throw new ParseException(errorMsg, 0);
            }
         }

      return true;
   }

   protected Object evaluate() throws ParseException {
      Class       classObj;
      Object      result;
      Integer     intObj = new Integer(0);
      Double      floatObj = new Double(0.0);
      String      strObj = "";
      BRExpNode   expNode = new BRExpNode(interpretor);
      BRFunctNode functNode = new BRFunctNode(interpretor);
      BRVarNode   varNode   = new BRVarNode(interpretor);
      BRDbFieldNode fieldNode = new BRDbFieldNode(interpretor);
      VarNode     tmpVarNode  = new VarNode(interpretor);

      substituteUserVars();

      if (RightNode == null)
         result = null;
      else {
         classObj = RightNode.getClass();
         result = RightNode;
         if (classObj.isInstance(expNode)) {
        	 BRUtil.debug("evaluate@BRAssignNode - Right : BRExpNode");
            result = ((BRExpNode)RightNode).evaluate();
            }
         else if (classObj.isInstance(functNode)) {
        	 BRUtil.debug("evaluate@BRAssignNode - Right : BRFunctNode");
            result = ((BRFunctNode)RightNode).evaluate();
            }
         else if (classObj.isInstance(varNode)) {
        	 BRUtil.debug("evaluate@BRAssignNode - Right : BRVarNode");
            result = ((BRVarNode)RightNode).evaluate();
            }
         else if (classObj.isInstance(tmpVarNode)) {     // do nothing
        	 BRUtil.debug("evaluate@BRAssignNode - Right : VarNode");
            result = ((VarNode)RightNode).evaluate();
            }
         else if (classObj.isInstance(intObj)) {     // do nothing
        	 BRUtil.debug("evaluate@BRAssignNode - Right : Integer");
            }
         else if (classObj.isInstance(floatObj)) {   // do nothing
        	 BRUtil.debug("evaluate@BRAssignNode - Right : float");
            }
         else if (classObj.isInstance(strObj)) {     // do nothing
        	 BRUtil.debug("evaluate@BRAssignNode: String");
            result = interpretor.substituteUserVars((String)RightNode,
                        BRConstants.STR_DELIMITER);
            }
         else {
        	 BRUtil.debug("evaluate@BRAssignNode - Right: Other");
            errorMsg = "Syntax error, unsupported object specified as right operand(s)!";
            throw new ParseException(errorMsg, 0);
         }
      }

      classObj = LeftNode.getClass();
      if (classObj.isInstance(fieldNode)) {
     	 BRUtil.debug("evaluate@BRAssignNode - Left : BRDbFieldNode");
         ((BRDbFieldNode)LeftNode).evaluate(result);
         }
      else if (classObj.isInstance(varNode)) {
      	 BRUtil.debug("evaluate@BRAssignNode - Left : BRVarNode");
         ((BRVarNode)LeftNode).evaluate(result);
         }
      else if (classObj.isInstance(tmpVarNode)) {
      	 BRUtil.debug("evaluate@BRAssignNode - Left : VarNode");
         ((VarNode)LeftNode).evaluate(result);
         }
      else {
         errorMsg = "Syntax error, unsupported object specified as left operand(s)!";
         throw new ParseException(errorMsg, 0);
         }

      // return computed value
      return result;
   }

   protected void substituteUserVars() throws ParseException {
      String    strObj = "";
      Class     classObj;
      BRExpNode   expNode = new BRExpNode(interpretor);
      BRFunctNode   functNode = new BRFunctNode(interpretor);

      if (RightNode == null) return;

      classObj = RightNode.getClass();
      if (classObj.isInstance(expNode)) {
         ((BRExpNode)RightNode).substituteUserVars();
      }
      else if (classObj.isInstance(functNode)) {
         ((BRFunctNode)RightNode).substituteUserVars();
      }
      else if (classObj.isInstance(strObj)) {
         strObj = (String)RightNode;
         RightNode = (String)interpretor.substituteUserVars(strObj,
                              BRConstants.STR_DELIMITER);
      }
   }

   public String toString()
   {
     return this.LeftNode + " " + this.OpName +  " " + this.RightNode;
   }

}
