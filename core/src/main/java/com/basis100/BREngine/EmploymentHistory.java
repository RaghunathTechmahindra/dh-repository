// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class EmploymentHistory extends DealEntity {
	protected  	int 	employmentHistoryId ;
	protected  	int 	occupationId ;
	protected  	int 	industrySectorId ;
	protected  	String 	employerName ;
	protected  	int 	borrowerId ;
	protected  	int 	monthsOfService ;
	protected  	int 	employmentHistoryNumber ;
	protected  	int 	contactId ;
	protected  	int 	employmentHistoryTypeId ;
	protected  	int 	incomeId ;
	protected  	int 	employmentHistoryStatusId ;
	protected  	String 	jobTitle ;
	protected  	int 	copyId ;
	// SEAN Ticket #2157 Sept. 26, 2005
	protected   int     jobTitleId;
	// SEAN Ticket #2157 END

  protected Contact  contact;

  protected String getPrimaryKeyName() {
      return "employmentHistoryId";
   }

   public EmploymentHistory(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public EmploymentHistory(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public EmploymentHistory(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

   protected EmploymentHistory deepCopy() throws CloneNotSupportedException {
      return (EmploymentHistory)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from EmploymentHistory";
   }

   protected void close() throws SQLException {
      if (contact != null)
         contact.close();
         
      super.close();
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				employmentHistoryId  = resultSet.getInt("EMPLOYMENTHISTORYID");
				occupationId  = resultSet.getInt("OCCUPATIONID");
				industrySectorId  = resultSet.getInt("INDUSTRYSECTORID");
				employerName  = getString(resultSet.getString("EMPLOYERNAME"));
				if (employerName != null )
					employerName  =  employerName.trim();
				borrowerId  = resultSet.getInt("BORROWERID");
				monthsOfService  = resultSet.getInt("MONTHSOFSERVICE");
				employmentHistoryNumber  = resultSet.getInt("EMPLOYMENTHISTORYNUMBER");
				contactId  = resultSet.getInt("CONTACTID");
				employmentHistoryTypeId  = resultSet.getInt("EMPLOYMENTHISTORYTYPEID");
				incomeId  = resultSet.getInt("INCOMEID");
				employmentHistoryStatusId  = resultSet.getInt("EMPLOYMENTHISTORYSTATUSID");
				jobTitle  = getString(resultSet.getString("JOBTITLE"));
				if (jobTitle != null )
					jobTitle  =  jobTitle.trim();
				copyId  = resultSet.getInt("COPYID");
				// SEAN Ticket #2157 Sept. 26, 2005
				jobTitleId = resultSet.getInt("JOBTITLEID");
				// SEAN Ticket #2157 END

        // fetch contact record
        if (contact != null)
           contact.close();
        filter = "contactId=" + contactId + " and copyId = " + copyId;
        contact = new Contact(interpretor, filter);
        contact.close();
        }

      return bStatus;
   }
}
