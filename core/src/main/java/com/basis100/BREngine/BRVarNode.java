// Business Rule Expression Scanner, add array
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

//
// arrasyIndex:
//    -2    - array iterator, '..'
//    -1    - no index specified
//     x    - zero-based index
//
public class BRVarNode implements Cloneable
{
  private BRInterpretor interpretor;
  protected int arrayIndex; // -1 if not an array, else zero based
  protected Object indexObj;
  protected String entityName; // e.g. "totalPurchasePrice"
  protected BRVarNode attribute; // not null if not leaf node
  protected Object entityObj;
  protected String errorMsg;

  public BRVarNode(BRInterpretor interpretor)
  {
    this.interpretor = interpretor;
    arrayIndex = -1; // default no index
    this.entityName = null;
    attribute = (BRVarNode)null; // default no offspring
    entityObj = null;
    indexObj = null;
  }

  public BRVarNode(BRInterpretor interpretor, String entityName)
  {
    this.interpretor = interpretor;
    arrayIndex = -1; // default no index
    this.entityName = entityName;
    attribute = (BRVarNode)null; // default no offspring
    entityObj = null;
    indexObj = null;
  }

  public BRVarNode(BRInterpretor interpretor, Object anEntity, String entityName)
  {
    this.interpretor = interpretor;
    arrayIndex = -1; // default no index
    this.entityName = entityName;
    attribute = (BRVarNode)null; // default no offspring
    entityObj = anEntity;
    indexObj = null;
  }

  protected BRVarNode deepCopy() throws ParseException
  {
    BRVarNode varNode;

    try
    {
      varNode = (BRVarNode)this.clone();
    }
    catch(CloneNotSupportedException e)
    {
      errorMsg = "CloneNotSupportedException caught!";
      throw new ParseException(errorMsg, 0);
    }
    return varNode;
  }

  protected int count()
  {
    Object result = null;

    try
    {
      result = evaluate();
    }
    catch(Exception ex)
    {}

    return(result != null) ? 1 : 0;
  }

  // method is here as place holder, no logic is needed as there should not
  // be any variable need to be mapped until run time.
  protected boolean parseExpressionPhase2() throws ParseException
  {
    if(arrayIndex == BRConstants.INDEX_VARIABLE)
    {
      if(indexObj != null)
      {
        BRExpNode expObj = new BRExpNode(interpretor);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        VarNode usrVarObj = new VarNode(interpretor);
        Class classObj = indexObj.getClass();
        if(classObj.isInstance(expObj))
        {
          ((BRExpNode)indexObj).parseExpressionPhase2();
        }
        else if(classObj.isInstance(cachedVarObj))
        {
          ((BRVarNode)indexObj).parseExpressionPhase2();
        }
        else if(classObj.isInstance(usrVarObj))
        {
          ((VarNode)indexObj).parseExpressionPhase2();
        }
      }
      else
      {
        errorMsg = "Array index variable cannot be a null expression!";
        throw new ParseException(errorMsg, 0);
      }
    }
    return true;
  }

  protected BRVarNode[] expandsArrayIterator() throws ParseException
  {
    int i, j, size, jSize;
    Class classObj;
    Object[] entityObjs;
    Field fieldObj;
    BRVarNode varNode = null;
    BRVarNode[] varNodes = null;
    Stack aStack;

    switch(arrayIndex)
    {
      case BRConstants.ARRAY_ITERATOR:

        // get arrayobj using the entityName
        classObj = entityObj.getClass();
        try
        {
          fieldObj = classObj.getDeclaredField(entityName);
          entityObjs = (Object[])fieldObj.get(entityObj);
        }
        catch(NoSuchFieldException e)
        {
          errorMsg = "Invalid entity name: <" + entityName
            + "> detected by NoSuchFieldException trap!";
          throw new ParseException(errorMsg, 0);
        }
        catch(IllegalAccessException e)
        {
          errorMsg = "Invalid entity name: <" + entityName
            + "> detected by IllegalAccessException trap!";
          throw new ParseException(errorMsg, 0);
        }

        size = Array.getLength(entityObjs);
        aStack = new Stack();
        for(i = 0; i < size; ++i)
        {
          varNode = this.deepCopy();
          varNode.arrayIndex = i + 1;
          varNode.attribute.entityObj = entityObjs[i];
          varNodes = varNode.expandsArrayIterator();
          jSize = Array.getLength(varNodes);
          for(j = 0; j < jSize; ++j)
          {
            aStack.push(varNodes[j].deepCopy());
          }
        }

        size = aStack.size();
        varNodes = new BRVarNode[size];
        for(i = 0; i < size; ++i)
        {
          varNodes[i] = (BRVarNode)aStack.get(i);
        }
        break;
      case BRConstants.INDEX_VARIABLE:
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        Integer intObj = new Integer(0);

        Class indxClass = indexObj.getClass();
        int index = -1;
        if(indxClass.isInstance(expNode))
        {
        }
        else if(indxClass.isInstance(cachedVarNode))
        {
          intObj = (Integer)((BRVarNode)indexObj).evaluate();
          index = intObj.intValue();
        }
        else if(indxClass.isInstance(usrVarNode))
        {
          intObj = (Integer)((VarNode)indexObj).evaluate();
          index = intObj.intValue();
        }
        else
        {
          errorMsg = "Unknown object: " + indxClass.getName() +
            " used as array index";
          throw new ParseException(errorMsg, 0);
        }

        // get arrayobj using the entityName
        classObj = entityObj.getClass();
        try
        {
          fieldObj = classObj.getDeclaredField(entityName);
          entityObjs = (Object[])fieldObj.get(entityObj);
        }
        catch(NoSuchFieldException e)
        {
          errorMsg = "Invalid entity name: <" + entityName
            + "> detected by NoSuchFieldException trap!";
          throw new ParseException(errorMsg, 0);
        }
        catch(IllegalAccessException e)
        {
          errorMsg = "Invalid entity name: <" + entityName
            + "> detected by IllegalAccessException trap!";
          throw new ParseException(errorMsg, 0);
        }
        size = Array.getLength(entityObjs);
        if(index > size)
        {
          errorMsg = "Indexed variable out of bound!";
          throw new ParseException(errorMsg, 0);
        }
        varNode = this.deepCopy();
        varNode.arrayIndex = index;
        Object entityObj = entityObjs[--index];
        varNode.attribute.entityObj = entityObj;

        varNodes = varNode.expandsArrayIterator();
        break;
      default:
        if(attribute == null)
        {
          varNodes = new BRVarNode[1];
          varNodes[0] = this;
        }
        else
        {
          // otherwise, check if it has subnodes that has array iterator
          BRVarNode[] attributes = attribute.expandsArrayIterator();
          size = Array.getLength(attributes);
          varNodes = new BRVarNode[size];
          for(i = 0; i < size; ++i)
          {
            varNode = this.deepCopy();
            varNode.attribute = attributes[i];
            varNodes[i] = varNode;
          }
        }
    }

    return varNodes;
  }

  protected Object evaluate() throws ParseException
  {
    boolean bFound = false;
    int i, length, index;
    double result = 0;
    Short shortObj = new Short((short)0);
    Integer intObj = new Integer(0);
    Class classObj, valueClass;
    Object fieldObj;
    Object fieldObjs[];
    Field aField;
    String errorMsg, tableName, fieldName = "";

    if(entityObj == null)
    {
      errorMsg = "Entity object: " + entityName + " is null!";
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.wran("Problem @BRVarNode.evaluate : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }

    classObj = entityObj.getClass();
    try
    {
      // no need to navigate if this node is a leaf node
      if(attribute == null)
      {
        // get the proper field name
        Field[] fields = classObj.getDeclaredFields();
        int nFieldCount = Array.getLength(fields);
        for(i = 0; i < nFieldCount; ++i)
        {
          fieldName = fields[i].getName();
          if(fieldName.equalsIgnoreCase(entityName))
          {
            bFound = true;
            break;
          }
        }

        if(!bFound)
        {
          String msg = "Invalid field name: " + entityName + "!";
          //--> Debug enhancement : by Billy 23Aug2004
          BRUtil.wran("Problem @BRVarNode.evaluate : " + msg);
          //==========================================

          throw new ParseException(msg, 0);
        }
        aField = classObj.getDeclaredField(fieldName);
        fieldObj = aField.get(entityObj);
        if(fieldObj == null)
        {
          return null;
        }
        valueClass = fieldObj.getClass();
        if(valueClass.isInstance(shortObj))
        {
          return new Integer(((Short)fieldObj).intValue());
        }
        else if(valueClass.isInstance(intObj))
        {
          return(Integer)fieldObj;
        }
//            else if (valueClass.isInstance(doubleObj))
//               return (Double)fieldObj;
        else if(valueClass.isArray())
        {
          if(arrayIndex == BRConstants.SCALAR_VARIABLE)
          {
            return fieldObj;
          }

          fieldObjs = (Object[])fieldObj;
          length = Array.getLength(fieldObj);
          if(length == 0)
          {
            return null;
          }
          if(arrayIndex > length)
          {
            errorMsg = "Array index out of bound!";
            //--> Debug enhancement : by Billy 23Aug2004
            BRUtil.wran("Problem @BRVarNode.evaluate : " + errorMsg);
            //==========================================

            throw new ParseException(errorMsg, 0);
          }
          fieldObj = fieldObjs[arrayIndex - 1];
          return fieldObj;
        }
        else
        {
          return fieldObj;
        }
      }
      else
      {
        switch(arrayIndex)
        {
          case BRConstants.SCALAR_VARIABLE: // single variable
            aField = classObj.getDeclaredField(entityName);
            fieldObj = aField.get(entityObj);
            attribute.entityObj = fieldObj;
            return attribute.evaluate();
          case BRConstants.ARRAY_ITERATOR: // whole array
            aField = classObj.getDeclaredField(entityName);
            fieldObjs = (Object[])aField.get(entityObj);
            length = Array.getLength(fieldObjs);

            return new Double(result);
          case BRConstants.INDEX_VARIABLE:
            BRExpNode expNode = new BRExpNode(interpretor);
            BRVarNode cachedVarNode = new BRVarNode(interpretor);
            VarNode usrVarNode = new VarNode(interpretor);

            Class indxClass = indexObj.getClass();
            index = -1;
            if(indxClass.isInstance(expNode))
            {
            }
            else if(indxClass.isInstance(cachedVarNode))
            {
              intObj = (Integer)((BRVarNode)indexObj).evaluate();
              index = intObj.intValue();
            }
            else if(indxClass.isInstance(usrVarNode))
            {
              intObj = (Integer)((VarNode)indexObj).evaluate();
              index = intObj.intValue();
            }
            else
            {
              errorMsg = "Unknown object: " + indxClass.getName() +
                " used as array index";
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @BRVarNode.evaluate : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }
            aField = classObj.getDeclaredField(entityName);
            fieldObjs = (Object[])aField.get(entityObj);
            --index;
            if(index >= Array.getLength(fieldObjs))
            {
              return null;
            }

            attribute.entityObj = fieldObjs[index];
            return attribute.evaluate();
          default: // variable array
            aField = classObj.getDeclaredField(entityName);
            fieldObjs = (Object[])aField.get(entityObj);
            length = Array.getLength(fieldObjs);
            if(arrayIndex > length || arrayIndex < 1)
            {
              errorMsg = "Array index: " + arrayIndex + " out of bound!";
              //--> Debug enhancement : by Billy 23Aug2004
              BRUtil.wran("Problem @BRVarNode.evaluate : " + errorMsg);
              //==========================================

              throw new ParseException(errorMsg, 0);
            }

            attribute.entityObj = fieldObjs[arrayIndex - 1];

            return attribute.evaluate();
        }
      }
    }
    catch(NoSuchFieldException e)
    {
      tableName = classObj.getName();
      index = tableName.lastIndexOf('.');

      if(index >= 0)
      {
        tableName = tableName.substring(index + 1);

      }
      errorMsg = "Table \"" + tableName + "\" does not have field name: "
        + entityName + "!";

      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.wran("Problem @BRVarNode.evaluate : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }
    catch(IllegalAccessException e)
    {
      errorMsg = "IllegalAccessException caught!";
      //--> Debug enhancement : by Billy 23Aug2004
      BRUtil.wran("Problem @BRVarNode.evaluate : " + errorMsg);
      //==========================================

      throw new ParseException(errorMsg, 0);
    }
  }

  protected Object evaluate(Object valueObj) throws ParseException
  {
    String updClause;
    Class classObj;
//????????? must use generic method to send update to class instead of instantiating
//          each class

    try
    {
      Property propertyObj = new Property(interpretor);
      Borrower borrowerObj = new Borrower(interpretor);
      classObj = entityObj.getClass();
      if(attribute == null)
      {
        updClause = entityName + "=" + BRUtil.asSQLColumnValue(valueObj);
        if(classObj.isInstance(propertyObj))
        {
          ((Property)entityObj).update(updClause);
          return valueObj;
        }
        else if(classObj.isInstance(borrowerObj))
        {
          ((Borrower)entityObj).update(updClause);
          return valueObj;
        }
      }
    }
    catch(SQLException ex)
    {
      throw new ParseException(ex.getMessage(), 0);
    }

    return attribute.evaluate(valueObj);
  }

  protected String getColumnName()
  {
    String columnName;

    if(attribute == null)
    {
      columnName = entityName.toUpperCase();
      return columnName;
    }

    return attribute.getColumnName();
  }

  protected String getTableName()
  {
    String tableName;

    if(attribute == null)
    {
      return null;
    }

    tableName = attribute.getTableName();
    if(tableName == null)
    {
      tableName = entityName.toUpperCase();
    }

    return tableName;
  }

  protected boolean hasArrayIterator()
  {
    // immediate return if current node has array iterator
    if(arrayIndex == BRConstants.ARRAY_ITERATOR)
    {
      return true;
    }

    // return if current node is leaf node
    if(attribute == null)
    {
      return false;
    }

    // check if leaf node has array iterator
    return attribute.hasArrayIterator();
  }

  protected boolean isTrue(boolean bAny) throws ParseException
  {
    int nSize;
    Object fieldObj;
    String strObj = "";
    String emInvArgumentType = "Invalid argument datatype for isTrue() function, boolean expected!";
    String emInvIndexType = "invalid index variable datatype, integer expected!";
    String emInvArray1 = "Syntax error: a scalar variable: ";
    String emInvArray2 = " is being used as an array!";
    String emInvArraySize = "index out of bound!";
    Field aField;
    Boolean boolObj = new Boolean(true);
    Object[] fieldObjs;

    Class varClass = entityObj.getClass();

    try
    {
      aField = varClass.getDeclaredField(entityName);
      fieldObj = aField.get(entityObj);
    }
    catch(Exception ex)
    {
      strObj = "Invalid entity: " + entityName;
      throw new ParseException(strObj, 0);
    }

    switch(arrayIndex)
    {
      case BRConstants.ARRAY_ITERATOR:
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;
        nSize = Array.getLength(fieldObjs);

        // creative lying by returning false when we are unable to locate obj
        // by Bernard Hickey and Ravi P. witnessed by Steve Sschrump
        if(nSize == 0)
        {
          return(bAny) ? false : true;
        }

        Object anEntity;
        Class entityClass;
        for(int i = 0; i < nSize; ++i)
        {
          anEntity = fieldObjs[i];
          entityClass = anEntity.getClass();
          attribute.entityObj = anEntity;
          if(!attribute.isEmptyArray(bAny))
          {
            if(attribute.isTrue(bAny))
            {
              if(bAny)
              {
                return true;
              }
            }
            else
            {
              if(!bAny)
              {
                return false;
              }
            }
          }
        }

        return(bAny) ? false : true;
      case BRConstants.INDEX_VARIABLE:
        fieldObjs = (Object[])fieldObj;
        nSize = Array.getLength(fieldObjs);

        // dynamic lying based on agreement make by Bernard Hickey, Ravi Prashad,
        // witnessed by Steve Sschrump.
        // if (nSize == 0) return true;
        if(nSize == 0)
        {
          return(bAny) ? false : true;
        }

        Integer intObj = new Integer(0);
        Object varIndexObj = interpretor.evaluateExpression(indexObj);
        Class varIndexClass = indexObj.getClass();
        if(varIndexClass.isInstance(intObj))
        {
          arrayIndex = ((Integer)indexObj).intValue();
//               return isTrue(false);
          fieldObj = fieldObjs[arrayIndex - 1];
          attribute.entityObj = fieldObj;
          return attribute.isTrue(bAny);
        }

        strObj = emInvIndexType;
        break;
      case BRConstants.SCALAR_VARIABLE:
        if(fieldObj == null)
        {
          return true; // null is equivalence to false
        }

        Class fieldClass = fieldObj.getClass();
        if(fieldClass.isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        if(attribute != null)
        {
          attribute.entityObj = fieldObj;
          return attribute.isTrue(bAny);
        }

        if(fieldClass.isInstance(boolObj))
        {
          return((Boolean)boolObj).booleanValue();
        }

        strObj = emInvArgumentType;
        break;
      default: // 1-based index constant specified
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;

        if(arrayIndex > Array.getLength(fieldObjs))
        {
          strObj = emInvArraySize;
          break;
        }

        fieldObj = fieldObjs[arrayIndex - 1];
        fieldClass = fieldObj.getClass();
        attribute.entityObj = fieldObj;
        return attribute.isTrue(bAny);
    }

    throw new ParseException(strObj, 0);
  }

  protected boolean isTypeBoolean()
  {
    return true;
  }

  protected boolean isEmptyArray(boolean bAny) throws ParseException
  {
    boolean bEmpty = false;
    String strObj = "";
    Field aField;
    Object fieldObj;
    Object[] fieldObjs;
    BRVarNode attribObj;

    Class varClass = entityObj.getClass();

    try
    {
      aField = varClass.getDeclaredField(entityName);
      fieldObj = aField.get(entityObj);
    }
    catch(Exception ex)
    {
      strObj = "Invalid entity: " + entityName;
      throw new ParseException(strObj, 0);
    }

    // ????? why do we return if fieldObj is not an array?
    if(!fieldObj.getClass().isArray())
    {
      return false;
    }

    fieldObjs = (Object[])fieldObj;
    int nSize = Array.getLength(fieldObjs);
    for(int i = 0; i < nSize; ++i)
    {
      attribObj = (BRVarNode)fieldObjs[i];
      bEmpty = attribObj.isEmpty(bAny);
      if(attribObj.isEmpty(bAny))
      {
        if(bAny)
        {
          return true; // first empty with bAny set to true
        }
      }
      else
      {
        if(!bAny)
        {
          return false; // first non-empty when bAny is set to false
        }
      }
    }

    return(bAny) ? false : true;

//      return (Array.getLength(fieldObjs) == 0) ? true : false;
  }

  protected boolean isEmpty(boolean bAny) throws ParseException
  {
    int nSize;
    Object fieldObj;
    String strObj = "";
    String emInvArgumentType = "Invalid argument datatype for isEmpty() function, string expected!";
    String emInvIndexType = "invalid index variable datatype, integer expected!";
    String emInvArray1 = "Syntax error: a scalar variable: ";
    String emInvArray2 = " is being used as an array!";
    String emInvArraySize = "index out of bound!";
    Field aField;
    Object[] fieldObjs;

    Class varClass = entityObj.getClass();

    try
    {
      aField = varClass.getDeclaredField(entityName);
      fieldObj = aField.get(entityObj);
    }
    catch(Exception ex)
    {
      strObj = "Invalid entity: " + entityName;
      throw new ParseException(strObj, 0);
    }

    switch(arrayIndex)
    {
      case BRConstants.ARRAY_ITERATOR:
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;
        nSize = Array.getLength(fieldObjs);

        // dynamic lying based on agreement make by Bernard Hickey, Ravi Prashad,
        // witnessed by Steve Sschrump.
        // if (nSize == 0) return true;
        if(nSize == 0)
        {
          return(bAny) ? false : true;
        }

        Object anEntity;
        Class entityClass;
        for(int i = 0; i < nSize; ++i)
        {
          anEntity = fieldObjs[i];
          entityClass = anEntity.getClass();
          attribute.entityObj = anEntity;

          if(attribute.isEmpty(bAny))
          {
            if(bAny)
            {
              return true; // any, one empty found, return true
            }
          }
          else
          {
            if(!bAny)
            {
              return false; // not empty, any not specified, return false;
            }
          }
        }

        return(bAny) ? false : true;
      case BRConstants.INDEX_VARIABLE:
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;
        nSize = Array.getLength(fieldObjs);
        // dynamic lying based on agreement make by Bernard Hickey, Ravi Prashad,
        // witnessed by Steve Sschrump.
        // if (nSize == 0) return true;
        if(nSize == 0)
        {
          return(bAny) ? false : true;
        }

        Integer intObj = new Integer(0);
        Object varIndexObj = interpretor.evaluateExpression(indexObj);
        if(varIndexObj != null)
        {
          Class varIndexClass = varIndexObj.getClass();
          if(varIndexClass.isInstance(intObj))
          {
            arrayIndex = ((Integer)varIndexObj).intValue();
            if(--arrayIndex >= 0 && arrayIndex < nSize)
            {
              fieldObj = fieldObjs[arrayIndex];
              attribute.entityObj = fieldObj;
              return attribute.isEmpty(bAny);
            }
            strObj = emInvArraySize;
          }
          else
          {
            strObj = emInvIndexType;
          }
        }
        else
        {
          strObj = "null value index variable not allowed!";
        }

        break;
      case BRConstants.SCALAR_VARIABLE:
        if(fieldObj == null)
        {
          // dynamic lying based on agreement make by Bernard Hickey, Ravi Prashad,
          // witnessed by Steve Sschrump.
          // return true;
          return true; // null is equivalence to empty
        }

        Class fieldClass = fieldObj.getClass();
        if(fieldClass.isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        if(attribute != null)
        {
          attribute.entityObj = fieldObj;
          return attribute.isEmpty(bAny);
        }

        if(fieldClass.isInstance(strObj))
        {
          return(((String)fieldObj).length() == 0) ? true : false;
        }

        strObj = emInvArgumentType;
        break;
      default: // 1-based index constant specified
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;

        if(arrayIndex > Array.getLength(fieldObjs))
        {
          strObj = emInvArraySize;
          break;
        }

        fieldObj = fieldObjs[arrayIndex - 1];
        fieldClass = fieldObj.getClass();
        attribute.entityObj = fieldObj;
        return attribute.isEmpty(bAny);
    }

    throw new ParseException(strObj, 0);
  }

  protected Object sum() throws ParseException
  {
    int nSize, intResult = 0;
    double doubleResult = 0.0;
    Object fieldObj, resultObj;
    Integer intObj = new Integer(0);
    Double doubleObj = new Double(0.0);
    Class fieldClass, classObj = null;
    Class varClass = entityObj.getClass();
    Field aField;
    Object[] fieldObjs;
    String strObj = "";
    String emInvArray1 = "Syntax error: a scalar variable: ";
    String emInvArray2 = " is being used as an array!";
    String emInvArraySize = "index out of bound!";
    String emInvIndexType = "invalid index variable datatype, integer expected!";
    String emInvArgumentType = "Invalid argument datatype for isEmpty() function, string expected!";

    try
    {
      aField = varClass.getDeclaredField(entityName);
      fieldObj = aField.get(entityObj);
    }
    catch(Exception ex)
    {
      strObj = "Invalid entity: " + entityName;
      throw new ParseException(strObj, 0);
    }

    switch(arrayIndex)
    {
      case BRConstants.ARRAY_ITERATOR:
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;
        nSize = Array.getLength(fieldObjs);

        if(nSize == 0)
        {
          return new Integer(0);
        }

        Object anEntity;
        Class entityClass;
        for(int i = 0; i < nSize; ++i)
        {
          anEntity = fieldObjs[i];
          entityClass = anEntity.getClass();
          attribute.entityObj = anEntity;

          resultObj = attribute.sum();
          if(resultObj == null)
          {
            return null;
          }

          classObj = resultObj.getClass();
          if(classObj.isInstance(doubleObj))
          {
            doubleObj = (Double)resultObj;
            doubleResult += doubleObj.doubleValue();
          }
          else if(classObj.isInstance(intObj))
          {
            intObj = (Integer)resultObj;
            intResult += intObj.intValue();
          }
          else
          {
            //String msg = "Invalid datatype for sum(x) parameter!";
            throw new ParseException(errorMsg, 0);
          }
        }

        if(classObj.isInstance(intObj))
        {
          return new Integer(intResult);
        }
        else
        {
          return new Double(doubleResult);
        }
      case BRConstants.INDEX_VARIABLE:
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;
        nSize = Array.getLength(fieldObjs);
        intObj = new Integer(0);
        if(nSize == 0)
        {
          return intObj;
        }

        Object varIndexObj = interpretor.evaluateExpression(indexObj);
        if(varIndexObj != null)
        {
          Class varIndexClass = varIndexObj.getClass();
          if(varIndexClass.isInstance(intObj))
          {
            arrayIndex = ((Integer)varIndexObj).intValue();
            if(--arrayIndex >= 0 && arrayIndex < nSize)
            {
              fieldObj = fieldObjs[arrayIndex];
              attribute.entityObj = fieldObj;
              return attribute.sum();
            }
            strObj = emInvArraySize;
          }
          else
          {
            strObj = emInvIndexType;
          }
        }
        else
        {
          strObj = "null value index variable not allowed!";
        }
        break;
      case BRConstants.SCALAR_VARIABLE:
        if(fieldObj == null)
        {
          return new Integer(0); // null is equivalence to zero
        }

        fieldClass = fieldObj.getClass();
        if(fieldClass.isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        if(fieldClass.isInstance(intObj))
        {
          return fieldObj;
        }
        else if(fieldClass.isInstance(doubleObj))
        {
          return fieldObj;
        }

        strObj = emInvArgumentType;
        break;
      default: // 1-based index constant specified
        if(!fieldObj.getClass().isArray())
        {
          strObj = emInvArray1 + entityName + emInvArray2;
          break;
        }

        fieldObjs = (Object[])fieldObj;

        if(arrayIndex > Array.getLength(fieldObjs))
        {
          strObj = emInvArraySize;
          break;
        }

        fieldObj = fieldObjs[arrayIndex - 1];
        fieldClass = fieldObj.getClass();
        attribute.entityObj = fieldObj;
        return attribute.sum();
    }

    throw new ParseException(strObj, 0);
  }
}
