// Business Rule Expression Scanner, add array
package com.basis100.BREngine;

import java.lang.Exception;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Stack;

public class BRExpNode implements Cloneable {

   private BRInterpretor interpretor;

   protected Object   LeftNode;
   protected Object   RightNode;
   protected int      OpCode;
   protected int      Precedence;
   protected int      leafCount;
   protected String   OpName;
   protected Deal     dealObj = null;
   protected String   errorMsg;
   private static final double MSEC_IN_DAY = 86400000.00;

   public BRExpNode(BRInterpretor interpretor) {
      this.interpretor = interpretor;
      LeftNode = null;
      RightNode = null;
      OpCode = '?';
      OpName = "?";
   }

   public BRExpNode(BRInterpretor interpretor, Deal dealObj, String token) {
      this.interpretor = interpretor;
      LeftNode = null;
      RightNode = null;
      this.dealObj = dealObj;

      setOperator(token);
   }

   protected void setLeftNode(Object aNode) {
      LeftNode = aNode;
   }

   protected void setRightNode(Object aNode) {
      RightNode = aNode;
   }

   protected void setOperator(String OpName) {
      char     op1, op2;

      OpCode = '?';

      this.OpName = OpName;
      op1 = OpName.charAt(0);
      if (OpName.length() > 1)
         op2 = OpName.charAt(1);
      else
         op2 = ' ';

      leafCount = 2;
      switch (op1) {
         case '!':
            if (op2 == '=') {                // 2
               OpCode = BRConstants.OP_NE;
               Precedence = 2;
               }
            else {                           // 6
               OpCode = BRConstants.OP_NOT;
               Precedence = 6;
               leafCount = 1;
               }
            return;
         case '(':                           // 5
            OpCode = BRConstants.OP_LEFT_PAREN;
            Precedence = 5;
            leafCount = 0;
            return;
         case ')':                           // 5
            OpCode = BRConstants.OP_RIGHT_PAREN;
            Precedence = 5;
            return;

         case '*':                           // 4
            OpCode = BRConstants.OP_MULTIPLY;
            Precedence = 4;
            return;
         case '/':                           // 4
            OpCode = BRConstants.OP_DIVISION;
            Precedence = 4;
            return;
         case '+':                           // 3
            OpCode = BRConstants.OP_PLUS;
            Precedence = 3;
            return;
         case '-':                           // 3
            OpCode = BRConstants.OP_MINUS;
            Precedence = 3;
            return;
         case '=':                           // 2
            OpCode = BRConstants.OP_EQ;
            Precedence = 2;
            return;
         case '<':                           // 2
            switch (op2) {
               case '=':
                  OpCode = BRConstants.OP_LE;
                  break;
               case '>':
                  OpCode = BRConstants.OP_NE;
                  break;
               default:
                  OpCode = BRConstants.OP_LT;
                  break;
               }
            Precedence = 2;
            return;
         case '>':
            if (op2 == '=')
               OpCode = BRConstants.OP_GE;
            else
               OpCode = BRConstants.OP_GT;
            Precedence = 2;
            return;
         case '&':
            OpCode = BRConstants.OP_AND;     // 1
            Precedence = 1;
            return;
         case '|':
            OpCode = BRConstants.OP_OR;
            Precedence = 1;
            return;
         case 'G':
            switch (op2) {
               case 'T':
                  OpCode = BRConstants.OP_GT;
                  break;
               case 'E':
                  OpCode = BRConstants.OP_GE;
                  break;
               }
            Precedence = 2;
            return;
         case 'L':
            switch (op2) {
               case 'T':
                  OpCode = BRConstants.OP_LT;
                  break;
               case 'E':
                  OpCode = BRConstants.OP_LE;
                  break;
               }
            Precedence = 2;
            return;
         case ':':            // assignment operator
            OpCode = BRConstants.OP_ASSIGN;
            Precedence = 10;   // highest
            return;
         default:
            if (OpName.compareToIgnoreCase("AND") == 0) {
               OpCode = BRConstants.OP_AND;     // 1
               Precedence = 1;
               return;
               }
            else if (OpName.compareToIgnoreCase("OR") == 0) {
               OpCode = BRConstants.OP_OR;
               Precedence = 1;
               return;
               }
            else if (OpName.compareToIgnoreCase("XOR") == 0) {
               OpCode = BRConstants.OP_XOR;
               Precedence = 1;
               return;
               }
            else if (OpName.compareToIgnoreCase("EQ") == 0) {
               OpCode = BRConstants.OP_EQ;
               Precedence = 2;
               return;
               }
            else if (OpName.compareToIgnoreCase("NE") == 0) {
               OpCode = BRConstants.OP_NE;
               Precedence = 2;
               return;
               }
            else {
               OpCode = '?';
               this.OpName = "?";
               }
         }

      return;
   }

   protected boolean isTrue(boolean bAny) throws ParseException {
      Object  result;
      Class   classObj;
      Boolean boolObj = new Boolean(false);

      result = evaluate();
      classObj = result.getClass();

      if (classObj.isInstance(boolObj)) {
         return ((Boolean)result).booleanValue();
      }

      String msg = "Result does not evaluate to boolean datatype!";
         throw new ParseException(msg, 0);
   }

   protected boolean isTypeBoolean() {
      switch (OpCode) {
         case BRConstants.OP_EQ:
         case BRConstants.OP_GT:
         case BRConstants.OP_LT:
         case BRConstants.OP_GE:
         case BRConstants.OP_LE:
         case BRConstants.OP_NE:
         case BRConstants.OP_AND:
         case BRConstants.OP_OR:
         case BRConstants.OP_NOT:
            return true;
         }
      return false;
   }

   protected boolean isLeftParenthesis() {
      if (OpCode == BRConstants.OP_LEFT_PAREN) return true;

      return false;
   }

   protected int compareTo(BRExpNode prevNode) {
      if (prevNode.OpCode == BRConstants.OP_NOT)
         return 6;
// 2001-01-22
//      if (prevNode.OpCode == BRConstants.OP_LEFT_PAREN)
//         return 5;
      if (prevNode.OpCode == BRConstants.OP_RIGHT_PAREN)
         return 5;

      return Precedence - prevNode.Precedence;
   }

   protected int count() {
      Object result=null;

      try {
         result = evaluate();
      }
      catch (Exception ex) {}

      if (result == null)
         return 0;

      return (((Boolean)result).booleanValue()) ? 1 : 0;
   }

   public BRExpNode deepCopy() throws ParseException {
      BRExpNode     expNode;

      try {
         expNode = (BRExpNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return expNode;
   }

   private boolean matchResultSetAttribute(Object anObj, Sql_Column[] metaData)
          throws ParseException {
      int       i, nFields = Array.getLength(metaData);
      Object[]  rowObj = (Object[])anObj;
      Object    saveObj, result, boolObj=new Boolean(true);;
      Class     classObj;
      String    varName = "";

      Class varClass = LeftNode.getClass();
      if (varClass.isInstance(varName)) {
         for (i=0; i<nFields; ++i) {
            if (metaData[i].sqlda.colName.compareToIgnoreCase((String)LeftNode) == 0) {
               saveObj = LeftNode;
               LeftNode = rowObj[i];
               result = evaluate();
               LeftNode = saveObj;
               classObj = result.getClass();
               if (classObj.isInstance(boolObj)) {
                  return ((Boolean)result).booleanValue();
                  }
               }
            }
         }

      return false;
   }

   protected void rebindCopyId() {
      Class   classObj;
      BRVarNode   cachedVarNode = new BRVarNode(interpretor);
      BRExpNode   expNode = new BRExpNode(interpretor);

      classObj = LeftNode.getClass();
      if (classObj.isInstance(expNode)) {
         ((BRExpNode)LeftNode).rebindCopyId();
      }
      else if (classObj.isInstance(cachedVarNode)) {
         cachedVarNode = (BRVarNode)LeftNode;
         if (cachedVarNode.entityName.equalsIgnoreCase("copyId")) {
            LeftNode = cachedVarNode.entityName;
         }
      }

      classObj = RightNode.getClass();
      if (classObj.isInstance(expNode)) {
         ((BRExpNode)RightNode).rebindCopyId();
      }
      else if (classObj.isInstance(cachedVarNode)) {
         cachedVarNode = (BRVarNode)RightNode;
         if (cachedVarNode.entityName.equalsIgnoreCase("copyId")) {
            RightNode = cachedVarNode.entityName;
         }
      }
   }

   protected void refreshDeferredEntityObj(Object entityObj)
   {
      BRExpNode     expNode = new BRExpNode(interpretor);
      BRVarNode     cachedVarNode = new BRVarNode(interpretor);
      Class         classObj;

      classObj = LeftNode.getClass();
      if (classObj.isInstance(expNode)) {
         ((BRExpNode)LeftNode).refreshDeferredEntityObj(entityObj);
      }
      else if (classObj.isInstance(cachedVarNode)) {
         ((BRVarNode)LeftNode).entityObj = entityObj;
      }

      classObj = RightNode.getClass();
      if (classObj.isInstance(expNode)) {
         ((BRExpNode)RightNode).refreshDeferredEntityObj(entityObj);
      }
      else if (classObj.isInstance(cachedVarNode)) {
         ((BRVarNode)RightNode).entityObj = entityObj;
      }
   }

   protected void findMatchCachedVar(Object varObj) throws ParseException {
      int     i, length;
      Class   classObj;
      String  strObj = "";
      String  leftVarName="", rightVarName="";
      BRVarNode cachedVarNode = new BRVarNode(interpretor);
      BRVarNode   cachedVarObj;
      VarNode   usrVarNode = new VarNode(interpretor);
      BRFunctNode  functNode = new BRFunctNode(interpretor);
      BRExpNode     expNode = new BRExpNode(interpretor);

      classObj = varObj.getClass();
      if (!classObj.isArray())
         throw new ParseException("Find() argument 1 is not an array!", 0);

      Object[] arrayObj = (Object[])varObj;
      int nSize = Array.getLength(varObj);
      if (nSize <= 0) return;

      Object entityObj = arrayObj[0];
      classObj = entityObj.getClass();
      Field[] fields = classObj.getDeclaredFields();
      int nFieldCount = Array.getLength(fields);

      // check leftNode first
      classObj = LeftNode.getClass();
      if (classObj.isInstance(cachedVarNode)) {
         LeftNode = ((BRVarNode)LeftNode).evaluate();
         classObj = LeftNode.getClass();
      }
      else if (classObj.isInstance(usrVarNode)) {
         LeftNode = ((VarNode)LeftNode).evaluate();
         classObj = LeftNode.getClass();
      }
      else if (classObj.isInstance(functNode)) {
         LeftNode = ((BRFunctNode)LeftNode).evaluate();
         classObj = LeftNode.getClass();
      }
      else if (classObj.isInstance(expNode)) {
         ((BRExpNode)LeftNode).findMatchCachedVar(varObj);
      }

      if (classObj.isInstance(strObj)) {
         if (BRUtil.isInteger((String)LeftNode))        // integer
            LeftNode = BRUtil.mapInt((String)LeftNode);
         else if (BRUtil.isFloat((String)LeftNode))      // double
            LeftNode = BRUtil.mapDouble((String)LeftNode);
         else {  // a string name, is it attribute name of the varObj being searched?
            strObj = (String)LeftNode;
            // now strip the string delimeter
            if (strObj.charAt(0) == '\"') {
               length = strObj.length();
               strObj = strObj.substring(1, length-1);
               }

            for (i=0; i<nFieldCount; ++i) {
               leftVarName = fields[i].getName();
               if (leftVarName.equalsIgnoreCase(strObj)) { // field name match
                  // assume cached non dotted variable for now, e.g.
                  // find(borrower, expresson) rather than find(borrower[..].income, expr);
                  cachedVarObj = new BRVarNode(interpretor);
                  cachedVarObj.arrayIndex = BRConstants.DEFERRED_ATTRIBUTE;
                  cachedVarObj.entityName = leftVarName;
                  cachedVarObj.attribute = null;
                  cachedVarObj.entityObj = null;    // to be fill in later
                  LeftNode = (Object)cachedVarObj;
                  break;
               }
            }
         }
      }

      // check rightNode next
      classObj = RightNode.getClass();
      if (classObj.isInstance(cachedVarNode)) {
         RightNode = ((BRVarNode)RightNode).evaluate();
         classObj = RightNode.getClass();
      }
      else if (classObj.isInstance(usrVarNode)) {
         RightNode = ((VarNode)RightNode).evaluate();
         classObj = RightNode.getClass();
      }
      else if (classObj.isInstance(functNode)) {
         RightNode = ((BRFunctNode)RightNode).evaluate();
         classObj = RightNode.getClass();
      }
      else if (classObj.isInstance(expNode)) {
         ((BRExpNode)RightNode).findMatchCachedVar(varObj);
      }

      if (classObj.isInstance(strObj)) {
         if (BRUtil.isInteger((String)RightNode))        // integer
            RightNode = BRUtil.mapInt((String)RightNode);
         else if (BRUtil.isFloat((String)RightNode))      // double
            RightNode = BRUtil.mapDouble((String)RightNode);
         else {   // a string name, is it attribute name of the varObj being searched?
            strObj = (String)RightNode;
            // now strip the string delimeter
            if (strObj.charAt(0) == '\"') {
               length = strObj.length();
               strObj = strObj.substring(1, length-1);
               }

            for (i=0; i<nFieldCount; ++i) {
               rightVarName = fields[i].getName();
               if (rightVarName.equalsIgnoreCase(strObj)) {
                  // assume cached non dotted variable for now, e.g.
                  // find(borrower, expresson) rather than find(borrower[..].income, expr);
                  cachedVarObj = new BRVarNode(interpretor);
                  cachedVarObj.arrayIndex = BRConstants.DEFERRED_ATTRIBUTE;
                  cachedVarObj.entityName = rightVarName;
                  cachedVarObj.attribute = null;
                  cachedVarObj.entityObj = null;    // to be fill in later
                  RightNode = (Object)cachedVarObj;
                  break;
               }
            }
         }
      }
   }

   protected int findMatchCachedVar2(Object varObj) throws ParseException {
      int     i,leftIndex=-1, rightIndex=-1, length;
      Class   classObj;
      Boolean boolObj = new Boolean(true);
      String  strObj = "";
      String  aVarName="", leftVarName="", rightVarName="";
      BRVarNode cachedVarObj = new BRVarNode(interpretor);
      VarNode   usrVarObj = new VarNode(interpretor);
      BRFunctNode  functNode = new BRFunctNode(interpretor);
      Field     aField;

      classObj = varObj.getClass();
      if (!classObj.isArray())
         throw new ParseException("Find() argument 1 is not an array!", 0);

      Object[] arrayObj = (Object[])varObj;
      int nSize = Array.getLength(varObj);
      Object entityObj = arrayObj[0];
      classObj = entityObj.getClass();
      Field[] fields = classObj.getDeclaredFields();
      int nFieldCount = Array.getLength(fields);

      // check leftNode first
      classObj = LeftNode.getClass();
      if (classObj.isInstance(cachedVarObj))
      {
         LeftNode = ((BRVarNode)LeftNode).evaluate();
         classObj = LeftNode.getClass();
      }
      else if (classObj.isInstance(usrVarObj))
      {
         LeftNode = ((VarNode)LeftNode).evaluate();
         classObj = LeftNode.getClass();
      }
      else if (classObj.isInstance(functNode))
      {
         LeftNode = ((BRFunctNode)LeftNode).evaluate();
         classObj = LeftNode.getClass();
      }

      if (classObj.isInstance(strObj))
      {
         if (BRUtil.isInteger((String)LeftNode))        // integer
            LeftNode = BRUtil.mapInt((String)LeftNode);
         else if (BRUtil.isFloat((String)LeftNode))      // double
            LeftNode = BRUtil.mapDouble((String)LeftNode);
         else
         {
            strObj = (String)LeftNode;
            // now strip the string delimeter
            if (strObj.charAt(0) == '\"') {
               length = strObj.length();
               strObj = strObj.substring(1, length-1);
               }
            for (i=0; i<nFieldCount; ++i)
            {
               leftVarName = fields[i].getName();
               if (leftVarName.equalsIgnoreCase(strObj))
               {
                  leftIndex = i;
                  break;
               }
            }
         }
      }

      // check rightNode next
      classObj = RightNode.getClass();
      if (classObj.isInstance(cachedVarObj))
      {
         RightNode = ((BRVarNode)RightNode).evaluate();
         classObj = RightNode.getClass();
      }
      else if (classObj.isInstance(usrVarObj))
      {
         RightNode = ((VarNode)RightNode).evaluate();
         classObj = RightNode.getClass();
      }
      else if (classObj.isInstance(functNode))
      {
         RightNode = ((BRFunctNode)RightNode).evaluate();
         classObj = RightNode.getClass();
      }

      if (classObj.isInstance(strObj))
      {
         if (BRUtil.isInteger((String)RightNode))        // integer
            RightNode = BRUtil.mapInt((String)RightNode);
         else if (BRUtil.isFloat((String)RightNode))      // double
            RightNode = BRUtil.mapDouble((String)RightNode);
         else
         {
            strObj = (String)RightNode;
            // now strip the string delimeter
            if (strObj.charAt(0) == '\"') {
               length = strObj.length();
               strObj = strObj.substring(1, length-1);
               }
            for (i=0; i<nFieldCount; ++i)
            {
               rightVarName = fields[i].getName();
               if (rightVarName.equalsIgnoreCase(strObj))
               {
                  rightIndex = i;
                  break;
               }
            }
         }
      }

      // now find the match
      for (i=0; i<nSize; ++i)
      {
         entityObj = arrayObj[i];
         classObj = entityObj.getClass();
         try
         {
            if (leftIndex >= 0)
            {
               aVarName = leftVarName;
               aField = classObj.getDeclaredField(leftVarName);
               LeftNode = aField.get(entityObj);
            }
            if (rightIndex >= 0)
            {
               aVarName = rightVarName;
               aField = classObj.getDeclaredField(rightVarName);
               RightNode = aField.get(entityObj);
            }
         }
         catch (Exception ex)
         {
            String msg = "Unable to access variable: " + classObj.getName()
                       + "." + aVarName;
            throw new ParseException(msg, 0);
         }

         // now evaluate the match
         Object statusObj = evaluate();
         if (statusObj != null)
         {
            classObj = statusObj.getClass();
            if (classObj.isInstance(boolObj))    // ignore if result is not boolean
            {
               if (((Boolean)statusObj).booleanValue())
                  return i;
            }
         }
      }

      return -1;
   }

   protected int findMatchUserVar(Object varObj) throws ParseException {
      Class   classObj;
      int   i, nFieldCount, rowCount;
      Object[]    arrayObj;
      Object      rowObj;

      classObj = varObj.getClass();
      if (classObj.isArray()) {
         Sql_Column[] metaData = interpretor.getMetaData(varObj);
         nFieldCount = Array.getLength(metaData);
         arrayObj = (Object[])varObj;
         rowCount = Array.getLength(arrayObj);

         for (i=0; i<rowCount; ++i) {
            rowObj = arrayObj[i];
            if (matchResultSetAttribute(rowObj, metaData))
               return i;
            }
         }

      return -1;
   }

   protected boolean parseExpressionPhase2() throws ParseException {
      int         length;
      Class       classObj;
      String      token, errorMsg;
      BRExpNode   expNode = new BRExpNode(interpretor);
      BRFunctNode functNode = new BRFunctNode(interpretor);
      BRVarNode   varNode   = new BRVarNode(interpretor);
      VarNode     tmpVarNode  = new VarNode(interpretor);

      classObj = LeftNode.getClass();
      if (classObj.isInstance(expNode)) {


         if (!((BRExpNode)LeftNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + LeftNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(functNode)) {
         if (!((BRFunctNode)LeftNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + LeftNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(varNode)) {

         }
      else if (classObj.isInstance(tmpVarNode)) {   // do nothing
         }
      else {
         token = (String)LeftNode;
         try {
            if (BRUtil.isInteger(token)) {       // integer
               LeftNode = BRUtil.mapInt(token);
               }
            else if (BRUtil.isFloat(token)) {     // double
               LeftNode = BRUtil.mapDouble(token);
               }
            else if (BRUtil.isDealEntity(dealObj, token)) {
               LeftNode = token;
               }
//            else if (isDBAttribute(token)) {
//               LeftNode = token;
//               }
            else {                         // string
               // now strip the string delimeter
               if (token.charAt(0) == '\"') {
                  length = token.length();
                  token = token.substring(1, length-1);
                  }
               switch (OpCode) {
                  case BRConstants.OP_PLUS:
                     LeftNode = token;
                     break;
                  case BRConstants.OP_MINUS:
                  case BRConstants.OP_MULTIPLY:
                  case BRConstants.OP_DIVISION:
                     errorMsg = "Invalid token \"" + token
                              + "\" for operator \"" + OpName + "\"";

                     throw new ParseException(errorMsg, 0);
                  default:                 // assume to be string
                     LeftNode = token;
                  }
               }
            }
         catch (Exception e) {
            if (OpName == null) {
               errorMsg = "Invalid token \"" + token + "\" found at the left of \""
                     + RightNode + "\"";
               }
            else {
               errorMsg = "Invalid token \"" + token + "\" found at the left of \""
                     + OpName + "\"";
               }

            throw new ParseException(errorMsg, 0);
            }
         }

      if (leafCount < 2)
         return true;

      classObj = RightNode.getClass();
      if (classObj.isInstance(expNode)) {
         if (!((BRExpNode)RightNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + LeftNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(functNode)) {
         if (!((BRFunctNode)RightNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + LeftNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(varNode)) {
         if (!((BRVarNode)RightNode).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + LeftNode + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(tmpVarNode)) {     // do nothing
         }
      else {
         token = (String)RightNode;
         try {
            if (BRUtil.isInteger(token)) {       // integer
               RightNode = BRUtil.mapInt(token);
               }
            else if (BRUtil.isFloat(token))      // double
               RightNode = BRUtil.mapDouble(token);
            else if (BRUtil.isBoolean(token))
               RightNode = BRUtil.mapBoolean(token);
            else if (BRUtil.isNull(token))
               RightNode = BRUtil.mapNull(token);
            else {                          // string
               // now strip the string delimeter
               if (token.charAt(0) == '\"') {
                  length = token.length();
                  token = token.substring(1, length-1);
                  }
               RightNode = token;
               }
            }
         catch (Exception e) {
            errorMsg = "Invalid token \"" + token + "\" found at the right of \""
                     + OpName + "\"";
            throw new ParseException(errorMsg, 0);
            }
         }

      return true;
   }

   protected Object add(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj    = new Integer(0);
      Double    doubleObj = new Double(0.0);
      String    strObj    = "";
      Date      dateObj = new Date(0,0,0);
      Class     class1, class2;

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();
         return (Object)(new Integer(intValue1 + intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 + doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 + doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 + doubleValue2));
         }
      else if (class1.isInstance(strObj)) {
         String   strTemp;

         if (class2.isInstance(strObj))
            strTemp = (String)rightResult;
         else
            strTemp = rightResult.toString();

         return (Object)((String)leftResult + strTemp);
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(intObj)) {
         Date date1 = (Date)leftResult;

         long mSecond1 = date1.getTime();
         long mSecond2 = ((Integer)rightResult).intValue();
         mSecond2 *= MSEC_IN_DAY;
         Date date3 = new Date(mSecond1 + mSecond2);

         return (Object)date3;
         }
      else {
         }
      BRUtil.debug("BRExpNode@add: Left Class:" + class1);
      BRUtil.debug("BRExpNode@add: Right Class:" + class2);

      errorMsg = "Incompatible variable types between \"" + leftResult + "\" and \""
               + rightResult + "\"";
      throw new ParseException(errorMsg, 0);
   }

   protected void shiftReduceExpression1(Stack operators, Stack operands)
          throws ParseException {
      int   count;

      if (leafCount <= 0) return;

      count = operands.size();
      if (leafCount > count) {
         errorMsg = "Operators: \"" + OpName + "\" requires " + leafCount
                  + " operands, " + count + " found!";
         throw new ParseException(errorMsg, 0);
         }

      if (leafCount >= 1)
         setRightNode(operands.pop());

      // check if operators and operands mismatch
      if (leafCount == 1){
         if (LeftNode == null && RightNode != null) {
             LeftNode = RightNode;
             RightNode = null;
             }
         }
      else
         setLeftNode(operands.pop());
   }

   protected void substituteUserVars() throws ParseException {
      Class   classObj;
      String        strObj = "";
      BRExpNode     expNode = new BRExpNode(interpretor);
      BRFunctNode   functNode = new BRFunctNode(interpretor);

      if (LeftNode != null) {
         classObj = LeftNode.getClass();
         if (classObj.isInstance(expNode)) {
            ((BRExpNode)LeftNode).substituteUserVars();
         }
         else if (classObj.isInstance(functNode)) {
            ((BRFunctNode)LeftNode).substituteUserVars();
         }
         else if (classObj.isInstance(strObj)) {
            strObj = (String)LeftNode;
            LeftNode = (String)interpretor.substituteUserVars(strObj,
                              BRConstants.NO_DELIMITER);
         }
      }

      if (RightNode != null) {
         classObj = RightNode.getClass();
         if (classObj.isInstance(expNode)) {
            ((BRExpNode)RightNode).substituteUserVars();
         }
         else if (classObj.isInstance(functNode)) {
            ((BRFunctNode)RightNode).substituteUserVars();
         }
         else if (classObj.isInstance(strObj)) {
            strObj = (String)RightNode;
            RightNode = (String)interpretor.substituteUserVars(strObj,
                                        BRConstants.NO_DELIMITER);
         }
      }
   }

   protected Object substract(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      Date      dateObj = new Date(0,0,0);
      Timestamp tsObj = new Timestamp(0);
      Class     class1, class2;

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();
      BRUtil.debug("BRExpNode@substract: Left Class:" + class1);
      BRUtil.debug("BRExpNode@substract: Right Class:" + class2);

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Integer(intValue1 - intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 - doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 - doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 - doubleValue2));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(dateObj)) {
         Date date1 = (Date)leftResult;
         Date date2 = (Date)rightResult;
         double nDay = date1.getTime() - date2.getTime();
         nDay /= MSEC_IN_DAY;
         if (nDay > 0)
            nDay += 0.99999;
         else
            nDay -= 0.9999;
         return (Object)(new Integer((int)nDay));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(tsObj)) {
          Timestamp date1 = (Timestamp)leftResult;
          Timestamp date2 = (Timestamp)rightResult;
          double nDay = date1.getTime() - date2.getTime();
          nDay /= MSEC_IN_DAY;
          if (nDay > 0)
             nDay += 0.99999;
          else
             nDay -= 0.9999;
          return (Object)(new Integer((int)nDay));
          }
       else if (class1.isInstance(dateObj) && class2.isInstance(tsObj)) {
          Timestamp date1 = new Timestamp(((Date)leftResult).getTime());
          Timestamp date2 = (Timestamp)rightResult;
          double nDay = date1.getTime() - date2.getTime();
          nDay /= MSEC_IN_DAY;
          if (nDay > 0)
             nDay += 0.99999;
          else
             nDay -= 0.9999;
          return (Object)(new Integer((int)nDay));
          }
       else if (class1.isInstance(tsObj) && class2.isInstance(dateObj)) {
          Timestamp date1 = (Timestamp)leftResult;
          Timestamp date2 = new Timestamp(((Date)rightResult).getTime());
          double nDay = date1.getTime() - date2.getTime();
          nDay /= MSEC_IN_DAY;
          if (nDay > 0)
             nDay += 0.99999;
          else
             nDay -= 0.9999;
          return (Object)(new Integer((int)nDay));
          }
      else if (class1.isInstance(dateObj) && class2.isInstance(intObj)) {
         Date date1 = (Date)leftResult;

         long mSecond1 = date1.getTime();
         long mSecond2 = ((Integer)rightResult).intValue();
         mSecond2 *= MSEC_IN_DAY;
         Date date3 = new Date(mSecond1 - mSecond2);

         return (Object)date3;
         }
//      else if (class1.isInstance(intObj) && class2.isInstance(dateObj)) {
//          Date date2 = (Date)rightResult;
//
//          long mSecond1 = ((Integer)leftResult).intValue();
//          long mSecond2 = date2.getTime();
//          mSecond1 *= MSEC_IN_DAY;
//          Date date3 = new Date(mSecond1 - mSecond2);
//
//          return (Object)date3;
//          }
      else {
          BRUtil.debug("BRExpNode@substract: Left Class:" + class1);
          BRUtil.debug("BRExpNode@substract: Right Class:" + class2);
          String errorMsg = "Incompatible variable types between \""  + leftResult + "\" and \""
              + rightResult + "\"";
              throw new ParseException(errorMsg, 0);          

         }
   }

   protected Object multiply(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      Class     class1, class2;

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();
         return (Object)(new Integer(intValue1 * intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 * doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 * doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 * doubleValue2));
         }
      else {
          BRUtil.debug("BRExpNode@multiply: Left Class:" + class1);
          BRUtil.debug("BRExpNode@multiply: Right Class:" + class2);
    
          errorMsg = "Incompatible variable types between \"" + leftResult + "\" and \""
                   + rightResult + "\"";
          throw new ParseException(errorMsg, 0);
      }
   }

   protected Object divide(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      Class     class1, class2;

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Integer(intValue1 / intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 / doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 / doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();
         return (Object)(new Double(doubleValue1 / doubleValue2));
         }
      else {
          BRUtil.debug("BRExpNode@divide: Left Class:" + class1);
          BRUtil.debug("BRExpNode@divide: Right Class:" + class2);
          errorMsg = "Incompatible variable types between \"" + leftResult + "\" and \""
              + rightResult + "\"";
          throw new ParseException(errorMsg, 0);
         }

   }

   protected Object GT(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      String    strObj = new String("");
      Date      dateObj = new Date(0,0,0);
      Timestamp tsObj = new Timestamp(0);
      Class     class1, class2;

      if (leftResult == null && rightResult == null)
         return (Object)(new Boolean(false));
      else if (leftResult != null && rightResult == null)
         return (Object)(new Boolean(true));
      else if (leftResult == null && rightResult != null)
         return (Object)(new Boolean(false));

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Boolean(intValue1 > intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 > doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 > doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 > doubleValue2));
         }
      else if (class1.isInstance(strObj) && class2.isInstance(strObj)) {
         String str1 = (String)leftResult;
         String str2 = (String)rightResult;
         return (Object)(new Boolean(str1.compareTo(str2) > 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(dateObj)) {
         Date date1 = (Date)leftResult;
         Date date2 = (Date)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) > 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) > 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = new Timestamp(((Date)leftResult).getTime());
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(dateObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = new Timestamp(((Date)rightResult).getTime());
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else {
          BRUtil.debug("BRExpNode@GT: Left Class:" + class1);
          BRUtil.debug("BRExpNode@GT: Right Class:" + class2);
         errorMsg = "Unsupported variable type operation between \"" + leftResult + "\" and \""
               + rightResult + "\"";
         throw new ParseException(errorMsg, 0);
         }
   }

   protected boolean hasArrayIterator() {
      Class         classObj;
      BRVarNode     varNode   = new BRVarNode(interpretor);
      BRExpNode     expNode   = new BRExpNode(interpretor);

      classObj = LeftNode.getClass();
      if (classObj.isInstance(varNode)) {
         if (((BRVarNode)LeftNode).hasArrayIterator())
            return true;
         }
      else if (classObj.isInstance(expNode)) {
         if (((BRExpNode)LeftNode).hasArrayIterator())
            return true;
         }
      else {
         }

      if (leafCount > 1) {
         classObj = RightNode.getClass();
         if (classObj.isInstance(varNode)) {
            if (((BRVarNode)RightNode).hasArrayIterator())
               return true;
            }
         else if (classObj.isInstance(expNode)) {
            if (((BRExpNode)RightNode).hasArrayIterator())
               return true;
            }
         else {
            }
         }
      return false;
   }

   protected Object GE(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      String    strObj  = "";
      Date      dateObj = new Date(0,0,0);
      Timestamp tsObj = new Timestamp(0);
      Class     class1, class2;

      if (leftResult == null && rightResult == null)
         return (Object)(new Boolean(true));
      else if (leftResult != null && rightResult == null)
         return (Object)(new Boolean(true));
      else if (leftResult == null && rightResult != null)
         return (Object)(new Boolean(false));

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Boolean(intValue1 >= intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 >= doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 >= doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 >= doubleValue2));
         }
      else if (class1.isInstance(strObj) && class2.isInstance(strObj)) {
         String str1 = (String)leftResult;
         String str2 = (String)rightResult;
         return (Object)(new Boolean(str1.compareTo(str2) >= 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(dateObj)) {
         Date date1 = (Date)leftResult;
         Date date2 = (Date)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) >= 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) >= 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = new Timestamp(((Date)leftResult).getTime());
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(dateObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = new Timestamp(((Date)rightResult).getTime());
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else {
          BRUtil.debug("BRExpNode@GE: Left Class:" + class1);
          BRUtil.debug("BRExpNode@GE: Right Class:" + class2);
         errorMsg = "Unsupported variable type operation between \"" + leftResult + "\" and \""
               + rightResult + "\"";
         throw new ParseException(errorMsg, 0);
         }
   }

   protected Object EQ(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      String    strObj = new String();
      Date      dateObj = new Date(0,0,0);
      Timestamp tsObj = new Timestamp(0);
      Boolean   boolObj = new Boolean(false);
      Class     class1, class2;

      if (leftResult == null && rightResult == null)
         return (Object)(new Boolean(true));
      else if (leftResult != null && rightResult == null)
         return (Object)(new Boolean(false));
      else if (leftResult == null && rightResult != null)
         return (Object)(new Boolean(false));

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Boolean(intValue1 == intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 == doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 == doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 == doubleValue2));
         }
      else if (class1.isInstance(boolObj) && class2.isInstance(boolObj)) {
         boolean boolValue1 = ((Boolean)leftResult).booleanValue();
         boolean boolValue2 = ((Boolean)rightResult).booleanValue();

         return (Object)(new Boolean(boolValue1 == boolValue2));
         }
      else if (class1.isInstance(strObj) && class2.isInstance(strObj)) {
         String strValue1 = (String)leftResult;
         String strValue2 = (String)rightResult;
         boolean boolValue = (strValue1.compareTo(strValue2)== 0);
         return (Object)(new Boolean(boolValue));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(dateObj)) {
         Date date1 = (Date)leftResult;
         Date date2 = (Date)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = new Timestamp(((Date)leftResult).getTime());
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(dateObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = new Timestamp(((Date)rightResult).getTime());
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else {
          BRUtil.debug("BRExpNode@EQ: Left Class:" + class1);
          BRUtil.debug("BRExpNode@EQ: Right Class:" + class2);
         errorMsg = "Unsupported variable types operation between \"" + leftResult + "\" and \""
               + rightResult + "\"";
         throw new ParseException(errorMsg, 0);
         }
   }

   protected Object LE(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      String    strObj = "";
      Date      dateObj = new Date(0,0,0);
      Timestamp tsObj = new Timestamp(0);
      Class     class1, class2;

      if (leftResult == null && rightResult == null)
         return (Object)(new Boolean(true));
      else if (leftResult == null && rightResult != null)
         return (Object)(new Boolean(true));
      else if (leftResult != null && rightResult == null)
         return (Object)(new Boolean(false));

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Boolean(intValue1 <= intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 <= doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 <= doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 <= doubleValue2));
         }
      else if (class1.isInstance(strObj) && class2.isInstance(strObj)) {
         String str1 = (String)leftResult;
         String str2 = (String)rightResult;
         return (Object)(new Boolean(str1.compareTo(str2) <= 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(dateObj)) {
         Date date1 = (Date)leftResult;
         Date date2 = (Date)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) <= 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) <= 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = new Timestamp(((Date)leftResult).getTime());
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(dateObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = new Timestamp(((Date)rightResult).getTime());
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else {
          BRUtil.debug("BRExpNode@LE: Left Class:" + class1);
          BRUtil.debug("BRExpNode@LE: Right Class:" + class2);
         errorMsg = "Unsupported variable type operation between \"" + leftResult + "\" and \""
               + rightResult + "\"";
         throw new ParseException(errorMsg, 0);
         }
   }

   protected Object LT(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      String    strObj = "";
      Date      dateObj = new Date(0,0,0);
      Timestamp tsObj = new Timestamp(0);
      Class     class1, class2;

      if (leftResult == null && rightResult == null)
         return (Object)(new Boolean(false));
      else if (leftResult == null && rightResult != null)
         return (Object)(new Boolean(true));
      else if (leftResult != null && rightResult == null)
         return (Object)(new Boolean(false));

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Boolean(intValue1 < intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 < doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 < doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 < doubleValue2));
         }
      else if (class1.isInstance(strObj) && class2.isInstance(strObj)) {
         String str1 = (String)leftResult;
         String str2 = (String)rightResult;
         return (Object)(new Boolean(str1.compareTo(str2) < 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(dateObj)) {
         Date date1 = (Date)leftResult;
         Date date2 = (Date)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) < 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) < 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = new Timestamp(((Date)leftResult).getTime());
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(dateObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = new Timestamp(((Date)rightResult).getTime());
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else {
          BRUtil.debug("BRExpNode@LT: Left Class:" + class1);
          BRUtil.debug("BRExpNode@LT: Right Class:" + class2);
         errorMsg = "Unsupported variable type operation between \"" + leftResult + "\" and \""
               + rightResult + "\"";
         throw new ParseException(errorMsg, 0);
         }
   }

   protected Object NE(Object leftResult, Object rightResult) throws ParseException {
      int       intValue1, intValue2;
      double    doubleValue1, doubleValue2;
      Integer   intObj = new Integer(0);
      Double    doubleObj = new Double(0.0);
      Boolean   boolObj = new Boolean(false);
      String    strObj = "";
      Date      dateObj = new Date(0,0,0);
      Timestamp tsObj = new Timestamp(0);
      Class     class1, class2;

      if (leftResult == null && rightResult == null)
         return (Object)(new Boolean(false));
      else if (leftResult == null && rightResult != null)
         return (Object)(new Boolean(true));
      else if (leftResult != null && rightResult == null)
         return (Object)(new Boolean(true));

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
         intValue1 = ((Integer)leftResult).intValue();
         intValue2 = ((Integer)rightResult).intValue();

         return (Object)(new Boolean(intValue1 != intValue2));
         }
      else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Integer)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 != doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Integer)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 != doubleValue2));
         }
      else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
         doubleValue1 = ((Double)leftResult).doubleValue();
         doubleValue2 = ((Double)rightResult).doubleValue();

         return (Object)(new Boolean(doubleValue1 != doubleValue2));
         }
      else if (class1.isInstance(boolObj) && class2.isInstance(boolObj)) {
         boolean boolValue1 = ((Boolean)leftResult).booleanValue();
         boolean boolValue2 = ((Boolean)rightResult).booleanValue();

         return (Object)(new Boolean(boolValue1 != boolValue2));
         }
      else if (class1.isInstance(strObj) && class2.isInstance(strObj)) {
         String str1 = (String)leftResult;
         String str2 = (String)rightResult;
         return (Object)(new Boolean(str1.compareTo(str2) != 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(dateObj)) {
         Date date1 = (Date)leftResult;
         Date date2 = (Date)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) != 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) != 0));
         }
      else if (class1.isInstance(dateObj) && class2.isInstance(tsObj)) {
         Timestamp date1 = new Timestamp(((Date)leftResult).getTime());
         Timestamp date2 = (Timestamp)rightResult;
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else if (class1.isInstance(tsObj) && class2.isInstance(dateObj)) {
         Timestamp date1 = (Timestamp)leftResult;
         Timestamp date2 = new Timestamp(((Date)rightResult).getTime());
         return (Object)(new Boolean(date1.compareTo(date2) == 0));
         }
      else {
          BRUtil.debug("BRExpNode@NE: Left Class:" + class1);
          BRUtil.debug("BRExpNode@NE: Right Class:" + class2);
         errorMsg = "Unsupported variable type operation between \"" + leftResult + "\" and \""
               + rightResult + "\"";
         throw new ParseException(errorMsg, 0);
         }
   }

   protected Object OR(Object leftResult, Object rightResult) throws ParseException {
      Boolean boolObj = new Boolean(true);
      Class     class1, class2;

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(boolObj) && class2.isInstance(boolObj)) {
         boolean boolValue1 = ((Boolean)leftResult).booleanValue();
         boolean boolValue2 = ((Boolean)rightResult).booleanValue();

         return (Object)(new Boolean(boolValue1 || boolValue2));
         }

      BRUtil.debug("BRExpNode@OR: Left Class:" + class1);
      BRUtil.debug("BRExpNode@OR: Right Class:" + class2);
      errorMsg = "Incompatible variable types between \"" + leftResult + "\" and \""
               + rightResult + "\"";
      throw new ParseException(errorMsg, 0);
   }

   protected Object AND(Object leftResult, Object rightResult) throws ParseException {
      Boolean boolObj = new Boolean(true);
      Class     class1, class2;

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(boolObj) && class2.isInstance(boolObj)) {
         boolean boolValue1 = ((Boolean)leftResult).booleanValue();
         boolean boolValue2 = ((Boolean)rightResult).booleanValue();

         return (Object)(new Boolean(boolValue1 && boolValue2));
         }

      BRUtil.debug("BRExpNode@AND: Left Class:" + class1);
      BRUtil.debug("BRExpNode@AND: Right Class:" + class2);
      errorMsg = "Incompatible variable types between \"" + leftResult + "\" and \""
               + rightResult + "\"";
      throw new ParseException(errorMsg, 0);
   }

   protected Object not(Object leftResult) throws ParseException {
      Boolean boolObj = new Boolean(true);
      Class     class1;

      class1 = leftResult.getClass();

      if (class1.isInstance(boolObj)) {
         boolean boolValue1 = ((Boolean)leftResult).booleanValue();

         return (Object)(new Boolean(!boolValue1));
         }

      BRUtil.debug("BRExpNode@AND: Left Class:" + class1);
      errorMsg = "Invalid variable type \"" + leftResult + "\"";
      throw new ParseException(errorMsg, 0);
   }

   protected Object evaluate() throws ParseException {
      boolean     bStatus;
      Class       classObj;
      Object      leftResult=null, rightResult=null;
      Boolean     boolObj = new Boolean(false);
      BRExpNode   expNode = new BRExpNode(interpretor);
      BRFunctNode functNode = new BRFunctNode(interpretor);
      BRVarNode   varNode   = new BRVarNode(interpretor);
      VarNode     tmpVarNode = new VarNode(interpretor);

      substituteUserVars();

      classObj = LeftNode.getClass();
      leftResult = LeftNode;
      if (classObj.isInstance(expNode)) {
         leftResult = ((BRExpNode)LeftNode).evaluate();
         }
      else if (classObj.isInstance(functNode)) {
         try {
            leftResult = ((BRFunctNode)LeftNode).evaluate();
            }
         catch (Exception e) {
            errorMsg = "General exception caught during execution, msg: " +
                       e.getMessage() + "!";
            throw new ParseException (errorMsg, 0);
            }
         }
      else if (classObj.isInstance(varNode)) {
         leftResult = ((BRVarNode)LeftNode).evaluate();
         }
      else if (classObj.isInstance(tmpVarNode)) {
         leftResult = ((VarNode)LeftNode).evaluate();
         }

      if (leftResult != null) {
         classObj = leftResult.getClass();
         if (classObj.isInstance(boolObj)) {
            bStatus = ((Boolean)leftResult).booleanValue();
            if (!bStatus && OpCode == BRConstants.OP_AND)
               return leftResult;
            if (bStatus && OpCode == BRConstants.OP_OR)
               return leftResult;
         }
      }

      if (leafCount > 1) {
         if (RightNode == null)
            rightResult = null;
         else {
            classObj = RightNode.getClass();
            rightResult = RightNode;
            if (classObj.isInstance(expNode)) {
               rightResult = ((BRExpNode)RightNode).evaluate();
               }
            else if (classObj.isInstance(functNode)) {
               try {
                  rightResult = ((BRFunctNode)RightNode).evaluate();
                  }
               catch (Exception e) {
                  errorMsg = "General exception caught during execution, msg: " +
                             e.getMessage() + "!";
                  throw new ParseException (errorMsg, 0);
                  }
               }
            else if (classObj.isInstance(varNode)) {
               rightResult = ((BRVarNode)RightNode).evaluate();
               }
            else if (classObj.isInstance(tmpVarNode)) {
               rightResult = ((VarNode)RightNode).evaluate();
               }
            }
            //--> Added more protections :: By Billy 09Nov2004
            if(leftResult != null && leftResult instanceof String && rightResult == null)
              rightResult = "null";
            //================================================

         }


      // calculate value
      switch (OpCode) {
         case BRConstants.OP_PLUS:              // +
            return add(leftResult, rightResult);
         case BRConstants.OP_MINUS:             // -
            return substract(leftResult, rightResult);
         case BRConstants.OP_MULTIPLY:          // *
            return multiply(leftResult, rightResult);
         case BRConstants.OP_DIVISION:          // /
            return divide(leftResult, rightResult);
         case BRConstants.OP_GT:                // >
            return GT(leftResult, rightResult);
         case BRConstants.OP_GE:                // >=
            return GE(leftResult, rightResult);
         case BRConstants.OP_LT:                // <
            return LT(leftResult, rightResult);
         case BRConstants.OP_LE:                // <=
            return LE(leftResult, rightResult);
         case BRConstants.OP_EQ:                // ==
            return EQ(leftResult, rightResult);
         case BRConstants.OP_AND:               // &
            return AND(leftResult, rightResult);
         case BRConstants.OP_OR:                // |
            return OR(leftResult, rightResult);
         case BRConstants.OP_XOR:
            return XOR(leftResult, rightResult);
         case BRConstants.OP_NE:                // ~
            return NE(leftResult, rightResult);
         case BRConstants.OP_NOT:               // !
            return not(leftResult);
         default:
            return null;
         }
   }

   protected BRExpNode[] expandsArrayIterator() throws ParseException {
      int             i, size;
      boolean         bExpanded=false;
      Class           classObj;
      BRExpNode       expNode   = new BRExpNode(interpretor);
      BRExpNode[]     expNodes  = null;
      BRExpNode[]     tempExpNodes = null;
      BRVarNode       varNode   = new BRVarNode(interpretor);
      BRVarNode[]     varNodes  = null;

      // expands the left operand first
      classObj = LeftNode.getClass();
      if (classObj.isInstance(varNode)) {
         varNodes = ((BRVarNode)LeftNode).expandsArrayIterator();
         if (varNodes != null) {
            size = Array.getLength(varNodes);
            expNodes = new BRExpNode[size];

            for (i=0; i<size; ++i) {
               expNode = this.deepCopy();
               expNode.LeftNode = varNodes[i];
               expNodes[i] = expNode;
               }
            bExpanded = true;
            }
         }
      else if (classObj.isInstance(expNode)) {
         tempExpNodes = ((BRExpNode)LeftNode).expandsArrayIterator();
         if (tempExpNodes != null) {
            size = Array.getLength(tempExpNodes);
            expNodes = new BRExpNode[size];

            for (i=0; i<size; ++i) {
               expNode = this.deepCopy();
               expNode.LeftNode = tempExpNodes[i];
               expNodes[i] = expNode;
               }
            bExpanded = true;
            }
         }
      else {
         }

      // array iterator is not allowed on both sides of the expression operator
      if (leafCount == 1)
         return expNodes;

      // expands the right operand
      if (RightNode == null)
         return expNodes;

      classObj = RightNode.getClass();
      if (classObj.isInstance(varNode)) {
         varNodes = ((BRVarNode)RightNode).expandsArrayIterator();

         if (varNodes != null && bExpanded) {
            errorMsg = "Array iterators used in both sides of an operator is not allowed!";
            throw new ParseException(errorMsg, 0);
            }

         size = Array.getLength(varNodes);
         expNodes = new BRExpNode[size];

         for (i=0; i<size; ++i) {
            expNode = this.deepCopy();
            expNode.RightNode = varNodes[i];
            expNodes[i] = expNode;
            }
         return expNodes;
         }
      else if (classObj.isInstance(expNode)) {
         tempExpNodes = ((BRExpNode)RightNode).expandsArrayIterator();
         if (tempExpNodes != null) {
            size = Array.getLength(tempExpNodes);
            expNodes = new BRExpNode[size];

            for (i=0; i<size; ++i) {
               expNode = this.deepCopy();
               expNode.RightNode = tempExpNodes[i];
               expNodes[i] = expNode;
               }
            bExpanded = true;
            }
         }
      else {
         }

      return expNodes;
   }

   protected Object XOR(Object leftResult, Object rightResult) throws ParseException {
      Boolean boolObj = new Boolean(true);
      Class     class1, class2;

      class1 = leftResult.getClass();
      class2 = rightResult.getClass();

      if (class1.isInstance(boolObj) && class2.isInstance(boolObj)) {
         boolean boolValue1 = ((Boolean)leftResult).booleanValue();
         boolean boolValue2 = ((Boolean)rightResult).booleanValue();

         return (Object)(new Boolean(boolValue1 ^ boolValue2));
         }

      errorMsg = "Incompatible variable types between \"" + leftResult + "\" and \""
               + rightResult + "\"";
      throw new ParseException(errorMsg, 0);
   }

   public String toString()
   {
     return this.LeftNode + " " + this.OpName +  " " + this.RightNode;
   }
}
