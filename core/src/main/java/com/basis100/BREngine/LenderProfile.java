// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class LenderProfile extends DealEntity {
	protected  	int 	lenderProfileId ;
	protected  	String 	defaultLender ;
	protected  	String 	lenderName ;
	protected  	int 	contactId ;
	protected  	int 	profileStatusId ;
	protected  	String 	lpShortName ;
	protected  	String 	lpBusinessId ;
	protected  	String 	upFrontMIAllowed ;
	protected  	int 	defaultInvestorId ;
	protected  	String 	privateLabelIndicator ;
	protected  	String 	registrationName ;
	protected  	int 	interestCompoundingId ;
	//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//	
	protected	String  supportRateFloatDown;
	//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//

   public Contact contact;  

   protected String getPrimaryKeyName() {
      return "lenderProfileId";
   }
   public LenderProfile(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }
   public LenderProfile(BRInterpretor intrepretor) throws SQLException {
      super(intrepretor);
      filter = null;

      if (Open())
         next();
   }

   public LenderProfile(BRInterpretor intrepretor, int anId) throws SQLException {
      super(intrepretor);
      filter = getPrimaryKeyName() + "=" + anId;

      if (Open())
         next();
   }

   protected String getDefaultSQL() {
      return "select * from LenderProfile";
   }

   protected void close() throws SQLException {
      if (contact != null)
         contact.close();
         
      super.close();
   }

   /**
    * next
    * 
    * @param none <br>
    * @return boolean : true, if there is a row to retrieve <br>
    * @version 1.1 <br>
    * Date: 06/27/2006 <br>
    * Author: NBC/PP Implementation Team <br>
    * Change: <br>
    * 	Added call to set supportRateFloatDown attribute
    *
    */
   protected boolean next() throws SQLException {
      boolean   bStatus;
      String    filter;

      bStatus = super.next();
      if (bStatus) {
				lenderProfileId  = resultSet.getInt("LENDERPROFILEID");
				defaultLender  = getString(resultSet.getString("DEFAULTLENDER"));
				if (defaultLender != null )
					defaultLender  =  defaultLender.trim();
				lenderName  = getString(resultSet.getString("LENDERNAME"));
				if (lenderName != null )
					lenderName  =  lenderName.trim();
				contactId  = resultSet.getInt("CONTACTID");
				profileStatusId  = resultSet.getInt("PROFILESTATUSID");
				lpShortName  = getString(resultSet.getString("LPSHORTNAME"));
				if (lpShortName != null )
					lpShortName  =  lpShortName.trim();
				lpBusinessId  = getString(resultSet.getString("LPBUSINESSID"));
				if (lpBusinessId != null )
					lpBusinessId  =  lpBusinessId.trim();
				upFrontMIAllowed  = getString(resultSet.getString("UPFRONTMIALLOWED"));
				if (upFrontMIAllowed != null )
					upFrontMIAllowed  =  upFrontMIAllowed.trim();
				defaultInvestorId  = resultSet.getInt("DEFAULTINVESTORID");
				privateLabelIndicator  = getString(resultSet.getString("PRIVATELABELINDICATOR"));
				if (privateLabelIndicator != null )
					privateLabelIndicator  =  privateLabelIndicator.trim();
				registrationName  = getString(resultSet.getString("REGISTRATIONNAME"));
				if (registrationName != null )
					registrationName  =  registrationName.trim();
				interestCompoundingId  = resultSet.getInt("INTERESTCOMPOUNDINGID");

				//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//	
				supportRateFloatDown = resultSet.getString("SUPPORTRATEFLOATDOWN");
				//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//			
				
         // fetch contact record
         if (contact != null)
            contact.close();
         //=========================================================
         // Contact has a copy ID field but sourceofbusinessprofile
         // does not. So we look for contactId with copy Id 1.
         // ========================================================
         filter = "contactId=" + contactId + " and copyId = 1";
         contact = new Contact(interpretor, filter);
         contact.close();

         
/*==========================================
         lenderProfileId = resultSet.getShort(1);

         defaultLender = getString(resultSet.getString(2);
         if (defaultLender != null)
            defaultLender = defaultLender.trim();

         lenderName = resultSet.getString (3);
         if (lenderName != null)
            lenderName = lenderName.trim();

         contactId = resultSet.getInt(4);
         profileStatusId = resultSet.getShort(5);
         lpShortName = resultSet.getString (6);
         lpBusinessId    = resultSet.getString (7);

         // fetch profileStatus record
         if (profileStatus != null)
            profileStatus.close();
         profileStatus = new ProfileStatus(profileStatusId);
         profileStatus.close();
===================================*/
         }

      return bStatus;
   }
}

