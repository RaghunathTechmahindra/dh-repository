package com.basis100.BREngine;

import java.sql.Date;
import java.sql.SQLException;

/**
 * <p>Title: </p>
 * <p>Description: BR Entity Class map to ComponentLoan </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 8, 2008)
 */

public class ComponentLoan extends DealEntity {

    protected int componentId;
    protected int copyId;
    protected int paymentFrequencyId;
    protected int actualPaymentTerm;
    protected double discount;
    protected Date firstPaymentDate;
    protected int iadNumberOfDays;
    protected double interestAdjustmentAmount;
    protected Date interestAdjustmentDate;
    protected double loanAmount;
    protected Date maturityDate;
    protected double netInterestRate;
    protected double pAndIPaymentAmount;
    protected double perDiemInterestAmount;
    protected double premium;

/**
* <p>Constructor</p>
* @param interpretor
* @param bTemplate
* @throws SQLException
*/
    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param bTemplate boolean
     * @throws SQLException
     */
    public ComponentLoan(BRInterpretor interpretor, boolean bTemplate) 
        throws SQLException {
        super(interpretor);
        if(bTemplate) {
        }
    }

    /**
     * <p>Constructor</P>
     * @param interpretor BRInterpretor
     * @throws SQLException
     */    
    public ComponentLoan(BRInterpretor interpretor) throws SQLException {
        super(interpretor);
        filter = null;
    
        if(Open()) {
            next();
        }
    }

/**
* <p>Constructor</p>
* @param interpretor
* @param anObj
* @throws SQLException
*/
    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param anObj Object
     * @throws SQLException
     */
    public ComponentLoan(BRInterpretor interpretor, Object anObj) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + ((Component)anObj).componentId + " and copyId = " + ((Component)anObj).copyId;
    
        if(Open()) {
            next();
        }
    }

    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param componentId int
     * @param copyId int
     * @throws SQLException
     */
    public ComponentLoan(BRInterpretor interpretor, int componentId, int copyId) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + componentId + " and copyId = " + copyId;
    
        if(Open()) {
            next();
        }
    }

/**
* <p>Constractor<p\/p>
* @param interpretor
* @param filter
* @throws SQLException
*/
    /**
     * <p>Constractor</p>
     * @param interpretor BRInterpretor
     * @param filter String
     * @throws SQLException
     */
    public ComponentLoan(BRInterpretor interpretor, String filter) 
        throws SQLException {
    
        super(interpretor);
        this.filter = filter;
    
        if(Open()) {
            next();
        }
    }

/**
* <p>getPrimaryKeyName</p>
*/
    /**
     * <p>getPrimaryKeyName</p>
     * <p>Description:get first primary key name </p>
     * @return String
     */
    protected String getPrimaryKeyName()  {
        return "componentid";
    }

/**
* <p>deepCopy</p>
* @return ComponentLoan
*/
    /**
     * <p>deepCopy</p>
     * @return ComponentLoan
     */
    public ComponentLoan deepCopy() throws CloneNotSupportedException {
        return(ComponentLoan)this.clone();
    }
 
/**
* <p>getDefaultSQL</p>
* @return String
*/
    /**
     * <p>getDefaultSQL</p>
     * @return String
     */
    protected String getDefaultSQL() {
        return "select * from ComponentLoan";
    }
 
/**
* <p>next</p>
* <p>Description: retrieve data from DB and set entity properties
* @return boolean:true if data is exist, false if not
*/    
    /**
     * <p>next</p>
     * <p>Description: retrieve data from DB and set entity properties
     * @return boolean:true if data is exist, false if not
     */
    public boolean next() throws SQLException {
        
        boolean bStatus = super.next();
        if (!bStatus) return bStatus;
    
        componentId = resultSet.getInt("COMPONENTID");
        copyId = resultSet.getInt("COPYID");
        paymentFrequencyId = resultSet.getInt("PAYMENTFREQUENCYID");
        actualPaymentTerm = resultSet.getInt("ACTUALPAYMENTTERM");
        discount = resultSet.getDouble("DISCOUNT");
        firstPaymentDate = resultSet.getDate("FIRSTPAYMENTDATE");
        iadNumberOfDays = resultSet.getInt("IADNUMBEROFDAYS");
        interestAdjustmentAmount = resultSet.getDouble("INTERESTADJUSTMENTAMOUNT");
        interestAdjustmentDate = resultSet.getDate("INTERESTADJUSTMENTDATE");
        loanAmount = resultSet.getDouble("LOANAMOUNT");
        maturityDate = resultSet.getDate("MATURITYDATE");
        netInterestRate = resultSet.getDouble("NETINTERESTRATE");
        pAndIPaymentAmount = resultSet.getDouble("PANDIPAYMENTAMOUNT");
        perDiemInterestAmount = resultSet.getDouble("PERDIEMINTERESTAMOUNT");
        premium = resultSet.getDouble("PREMIUM");
        
        return bStatus;
    }

}
