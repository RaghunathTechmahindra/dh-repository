// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Bridge extends DealEntity {
        protected       int     bridgeId;
        protected       int     investorProfileId;
        protected       int     lenderProfileId;
        protected       short   bridgePurposeId;
        protected       short   interestTypeId;
        protected       short   paymentTermId;
        protected       int     pricingProfileId;
        protected       double  discount;
        protected       short   actualPaymentTerm;
        protected       double  postedInterestRate;
        protected       double  netInterestRate;
        protected       double  netLoanAmount;
        protected       double  postedRate;
        protected       Date    maturityDate;
        protected       Date    rateDate;
        protected       double  primium;
        protected       int     dealId;
        protected       int     copyId;


   protected String getPrimaryKeyName() {
      return "dealId";
   }

   public Bridge(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public Bridge(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public Bridge(BRInterpretor interpretor, String whereClause) throws SQLException {
      super(interpretor);
      filter = whereClause;

      if (Open())
         next();
   }


   protected Bridge deepCopy() throws CloneNotSupportedException {
      return (Bridge)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from Bridge";
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
         bridgeId  = resultSet.getInt("BRIDGEID");
         investorProfileId = resultSet.getInt("INVESTORPROFILEID");
         lenderProfileId = resultSet.getInt("LENDERPROFILEID");

         bridgePurposeId = resultSet.getShort("BRIDGEPURPOSEID");
         interestTypeId = resultSet.getShort("INTERESTTYPEID");
         paymentTermId = resultSet.getShort("PAYMENTTERMID");

         pricingProfileId = resultSet.getInt("PRICINGPROFILEID");

         discount = resultSet.getDouble("DISCOUNT");

         actualPaymentTerm = resultSet.getShort("ACTUALPAYMENTTERM");

         postedInterestRate = resultSet.getDouble("POSTEDINTERESTRATE");
         netInterestRate = resultSet.getDouble("NETINTERESTRATE");
         netLoanAmount = resultSet.getDouble("NETLOANAMOUNT");
         postedRate = resultSet.getDouble("POSTEDRATE");

         maturityDate = getDate(resultSet.getTimestamp("MATURITYDATE"));
         rateDate = getDate(resultSet.getTimestamp("RATEDATE"));

         primium = resultSet.getDouble("PREMIUM");

         dealId = resultSet.getInt("DEALID");
         copyId = resultSet.getInt("COPYID");
        }

      return bStatus;
   }

}
