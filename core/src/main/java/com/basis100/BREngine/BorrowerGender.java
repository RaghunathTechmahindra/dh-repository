/*
 * by Neil on Dec/01/2004
 */
package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * @see com.basis100.BREngine.DealEntity
 * @author Neil Kong on Dec/01/2004
 */
public class BorrowerGender extends DealEntity
{
    private int borrowerGenderId;
    private String borrowerGenderDescription;
    private static final String sql = "SELECT * FROM BORROWERGENDER ";
    private static final String primaryKeyName = "borrowerGenderId";

    /**
     * @param interpretor
     * @throws SQLException
     */
    public BorrowerGender(BRInterpretor interpretor) throws SQLException
    {
        this(interpretor, null);
    }

    /**
     * 
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public BorrowerGender(BRInterpretor interpretor, String filter)
            throws SQLException
    {
        super(interpretor);
        this.filter = filter;
        if (Open())
        {
            next();
        }
    }

    /**
     * 
     * @return
     * @throws CloneNotSupportedException
     */
    public BorrowerGender deepCopy() throws CloneNotSupportedException
    {
        try
        {
            return (BorrowerGender) this.clone();
        }
        catch (CloneNotSupportedException cnse)
        {
            String errMsg = "@BorrowerGender.deepCopy() : " + cnse.getMessage();
            throw new CloneNotSupportedException(errMsg);
        }
    }

    /**
     * @return
     */
    public String getDefaultSQL()
    {
        return sql;
    }

    /**
     * @return
     */
    public String getPrimaryKeyName()
    {
        return primaryKeyName;
    }

    /**
     * @return
     * @throws SQLException
     */
    public boolean next() throws SQLException
    {
        boolean bStatus = super.next();
        if (bStatus)
        {
            borrowerGenderDescription = resultSet
                    .getString("BORROWERGENDERDESC");
            borrowerGenderId = resultSet.getInt("BORROWERGENDERID");
        }
        return false;
    }

    /**
     * @return Returns the borrowerGenderDescription.
     */
    public String getBorrowerGenderDescription()
    {
        return borrowerGenderDescription;
    }

    /**
     * @return Returns the borrowerGenderId.
     */
    public int getBorrowerGenderId()
    {
        return borrowerGenderId;
    }
}