// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class BorrowerAddress extends DealEntity {
	protected  	int 	borrowerAddressId ;
	protected  	int 	borrowerAddressTypeId ;
	protected  	int 	borrowerId ;
	protected  	int 	monthsAtAddress ;
	protected  	int 	addrId ;
	protected  	int 	copyId ;
  //Additional Field for CMHC modification -- By BILLY 14Feb2002
  protected int      residentialStatusId;
  //============================================================

  public Address  addr;

   protected String getPrimaryKeyName() {
      return "borrowerAddressId";
   }

   public BorrowerAddress(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public BorrowerAddress(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public BorrowerAddress(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

   protected BorrowerAddress deepCopy() throws CloneNotSupportedException {
      return (BorrowerAddress)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from BorrowerAddress";
   }

   protected void close() throws SQLException {
      if (addr != null)
         addr.close();

      super.close();
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				borrowerAddressId  = resultSet.getInt("BORROWERADDRESSID");
				borrowerAddressTypeId  = resultSet.getInt("BORROWERADDRESSTYPEID");
				borrowerId  = resultSet.getInt("BORROWERID");
				monthsAtAddress  = resultSet.getInt("MONTHSATADDRESS");
				addrId  = resultSet.getInt("ADDRID");
				copyId  = resultSet.getInt("COPYID");
        //Additional Field for CMHC modification -- By BILLY 14Feb2002
        residentialStatusId = resultSet.getInt("residentialstatusid");
        //============================================================

        // fetch addr record
        if (addr != null)
           addr.close();

        addr = new Address(interpretor, addrId, copyId);
        addr.close();
        }

      return bStatus;
   }
}
