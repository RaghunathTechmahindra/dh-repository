package com.basis100.BREngine;

import java.sql.SQLException;
import java.util.Date;

import MosSystem.Mc;
import MosSystem.Sc;

/**
 * <p>Title: Component</p>
 * <p>Description: BusinessRule entity map to Component table </p>
 * 
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 8, 2008)
 */
public class Component extends DealEntity {

    protected int dealId;
    protected int copyId;
    protected int componentId;
    protected int componentTypeId;
    protected int mtgProdId;
    protected ComponentMortgage componentMortgage;
    protected ComponentLOC componentLOC;
    protected ComponentLoan componentLoan;
    protected ComponentCreditCard componentCreditCard;
    protected ComponentOverdraft componentOverdraft;
    protected String additionalInformation;
    protected Date firstPaymentDateLimit;
    protected double postedRate;
    protected int pricingRateInventoryId;
    protected int repaymentTypeId;
    protected Date teaserMaturityDate;
    protected double teaserNetInterestRate;
    protected double teaserPiAmount;
    protected double teaserRateInterestSaving;
    
    /**
     * <p>Component</p>
     * <p>Description: constractor</p>
     * @param interpretor
     * @param bTemplate
     * @throws SQLException
     */
    public Component(BRInterpretor interpretor, boolean bTemplate) throws SQLException {
        super(interpretor);
        if(bTemplate) {
        // place holder incase need to propagate template creation
        }
    }

    /**
     * <p>Component</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @throws SQLException
     */
    public Component(BRInterpretor interpretor) throws SQLException {
        super(interpretor);
        filter = null;

        if(Open()) {
            next();
        } 
    }

    /**
     * <p>Component</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @param componentId int
     * @param copyId int
     * @throws SQLException
     */
    public Component(BRInterpretor interpretor, int componentId, int copyId) throws SQLException {
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + componentId + " and copyId = " + copyId;

        if(Open()) {
          next();
        }
    }

    /**
     * <p>Component</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @param filter String
     * @throws SQLException
     */
    public Component(BRInterpretor interpretor, String filter) throws SQLException {
        super(interpretor);
        this.filter = filter;

        if(Open()) {
            next();
        }
    }
    
    /**
     * <p>deepCopy</p>
     * <p>Description:get clone</p>
     * @return Component
     * @throws CloneNotSupportedException
     */
    public Component deepCopy() throws CloneNotSupportedException {
        return(Component)this.clone();
    }
    
    /**
     * <p>getPrimaryKeyName</p>
     * <p>Description:get first primary key name </p>
     * @return String
     */
    protected String getPrimaryKeyName()  {
        return "componentid";
    }

    /**
     * <p>getDefaultSQL</p>
     * <p>Description:get base select sql </p>
     * @return String
     */
    protected String getDefaultSQL() {
        return "select * from Component";
    }
    
    /**
     * <p>fetchField</p>
     * <p>Description:get child object</p>
     * @param String
     * @return Object, one of ComponentMortgage, ComponentLOC, componentLoan, 
     *                 ComponentCreditCard or ComponentOverdraft 
     */
    public Object fetchField(String fieldName) 
        throws SQLException, CloneNotSupportedException {

        if (fieldName == null || fieldName.trim().length()==0) {
            return null;
        }
  
        String childName = fieldName.trim();
        BRUtil.debug("@Component.fetchField Instantiating object: " + childName);
        
        
        if("ComponentMortgage".equalsIgnoreCase(childName)) {
            return fetchComponentMortgage();
        } else if ("ComponentLOC".equalsIgnoreCase(childName)) {
            return fetchComponentLOC();
        } else if ("ComponentLoan".equalsIgnoreCase(childName)) {
            return fetchComponentLoan();
        } else if ("ComponentCreditCard".equalsIgnoreCase(childName)) {
            return fetchComponentCreditCard();
        } else if ("componentOverdraft".equalsIgnoreCase(childName)) {
            return fetchComponentOverdraft();
        }
        else return null;
    }

    /**
     * <p>fetchComponentMortgage</p>
     * <p>Description:get child ComponentMortgage if componentType is matched</p>
     * @param 
     * @return Object ComponentMortgage
     **/
    private Object fetchComponentMortgage() 
        throws SQLException, CloneNotSupportedException {
        
        if (componentTypeId != Sc.COMPONENT_TYPE_MORTGAGE) {
            return null;
        }
        
        ComponentMortgage mtg = null;
    
        try
        {
          String filter = "componentId=" + componentId + " and copyId = " + copyId;
          mtg = new ComponentMortgage(interpretor, filter);
          if(mtg.componentId != componentId) {
              mtg.close();
              mtg = null;
          }
        }
        catch(SQLException e)
        {
          BRUtil.error("Exception @Component.fetchComponentMortgage : " + e);
          throw e;
        }
        
        return mtg;
    }

    /**
     * <p>fetchComponentLOC</p>
     * <p>Description:get child ComponentLOC if componentType is matched</p>
     * @param 
     * @return Object ComponentLOC
     **/
    private Object fetchComponentLOC() 
        throws SQLException, CloneNotSupportedException {
    
        if (componentTypeId != Sc.COMPONENT_TYPE_LOC) {
            return null;
        }
        
        ComponentLOC loc = null;
    
        try
        {
          String filter = "componentId=" + componentId + " and copyId = " + copyId;
          loc = new ComponentLOC(interpretor, filter);
          if(loc.componentId != componentId) {
              loc.close();
              loc = null;
          }
        }
        catch(SQLException e)
        {
          BRUtil.error("Exception @Component.fetchComponentLOC : " + e);
          throw e;
        }
        
        return loc;
    }

    /**
     * <p>fetchComponentLoan</p>
     * <p>Description:get child ComponentLoan if componentType is matched</p>
     * @param 
     * @return Object ComponentLoan
     **/
    private Object fetchComponentLoan() 
        throws SQLException, CloneNotSupportedException {
        
        if (componentTypeId != Sc.COMPONENT_TYPE_LOAN) {
            return null;
        }
        ComponentLoan loan = null;
        
        try
        {
          String filter = "componentId=" + componentId + " and copyId = " + copyId;
          loan = new ComponentLoan(interpretor, filter);
          if(loan.componentId != componentId) {
              loan.close();
              loan = null;
          }
        }
        catch(SQLException e)
        {
          BRUtil.error("Exception @Component.fetchComponentLoan : " + e);
          throw e;
        }
        
        return loan;
    }

    /**
     * <p>fetchComponentCreditCard</p>
     * <p>Description:get child ComponentCreditCard if componentType is matched</p>
     * @param 
     * @return Object ComponentCreditCard
     **/
    private Object fetchComponentCreditCard() 
        throws SQLException, CloneNotSupportedException {
        
        if (componentTypeId != Sc.COMPONENT_TYPE_CREDITCARD) {
            return null;
        }
        
        ComponentCreditCard cc = null;
        try
        {
          String filter = "componentId=" + componentId + " and copyId = " + copyId;
          cc = new ComponentCreditCard(interpretor, filter);
          if(cc.componentId != componentId) {
              cc.close();
              cc = null;
          }
        }
        catch(SQLException e)
        {
          BRUtil.error("Exception @Component.fetchComponentCreditCard : " + e);
          throw e;
        }
        
        return cc;
    }

    /**
     * <p>fetchComponentOverdraft</p>
     * <p>Description:get child ComponentOverdraft if componentType is matched</p>
     * @param 
     * @return Object ComponentOverdraft
     **/
    private Object fetchComponentOverdraft() 
        throws SQLException, CloneNotSupportedException {
        
        if (componentTypeId != Sc.COMPONENT_TYPE_OVERDRAFT) {
            return null;
        }
        ComponentOverdraft od = null;
        try
        {
          String filter = "componentId=" + componentId + " and copyId = " + copyId;
          od = new ComponentOverdraft(interpretor, filter);
          if(od.componentId != componentId) {
              od.close();
              od = null;
          }
        }
        catch(SQLException e)
        {
          BRUtil.error("Exception @Component.fetchComponentOverdraft : " + e);
          throw e;
        }
        
        return od;
    }

    /**
     * <p>next</p>
     * <p>Description:get child ComponentOverdraft if componentType is matched</p>
     * @param 
     * @return Object ComponentOverdraft
     **/
    public boolean next() throws SQLException {
        boolean bStatus;

        bStatus = super.next();
        if (!bStatus) {
            return bStatus;
        }

        dealId = resultSet.getInt("DEALID");
        componentId = resultSet.getInt("COMPONENTID");
        copyId = resultSet.getInt("COPYID");
        componentTypeId = resultSet.getInt("COMPONENTTYPEID");
        mtgProdId = resultSet.getInt("MTGPRODID");
        additionalInformation = resultSet.getString("ADDITIONALINFORMATION");
        firstPaymentDateLimit = asDate(resultSet.getTimestamp("FIRSTPAYMENTDATELIMIT"));
        postedRate = resultSet.getDouble("POSTEDRATE");
        pricingRateInventoryId = resultSet.getInt("PRICINGRATEINVENTORYID");
        repaymentTypeId = resultSet.getInt("REPAYMENTTYPEID");
         teaserMaturityDate = asDate(resultSet.getTimestamp("TEASERMATURITYDATE"));
        teaserNetInterestRate = resultSet.getDouble("TEASERNETINTERESTRATE");
        teaserRateInterestSaving = resultSet.getDouble("TEASERRATEINTERESTSAVING");
        teaserPiAmount = resultSet.getDouble("TEASERPIAMOUNT");

        try {
            switch (componentTypeId) {
            case Mc.COMPONENT_TYPE_MORTGAGE: 
                componentMortgage = (ComponentMortgage)fetchComponentMortgage();
                break;
            case Mc.COMPONENT_TYPE_LOC: 
                componentLOC = (ComponentLOC)fetchComponentLOC();
                break;
            case Mc.COMPONENT_TYPE_CREDITCARD: 
                componentCreditCard = (ComponentCreditCard)fetchComponentCreditCard();
                break;
            case Mc.COMPONENT_TYPE_LOAN: 
                componentLoan = (ComponentLoan)fetchComponentLoan();
                break;
            case Mc.COMPONENT_TYPE_OVERDRAFT: 
                componentOverdraft = (ComponentOverdraft)(fetchComponentOverdraft());
                break;
            default: break;
            }
        } catch (CloneNotSupportedException cd) {
            BRUtil.debug("CloneNotSupportedException was thrown with componentId = " + componentId);
            BRUtil.debug(cd.getMessage());
        }
        return bStatus;
    }
}

