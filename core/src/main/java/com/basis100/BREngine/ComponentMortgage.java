package com.basis100.BREngine;

import java.sql.Date;
import java.sql.SQLException;

/**
 * <p>Title: ComponentMortgage</p>
 * <p>Description: BR Entity Class map to ComponentMortgage </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 8, 2008)
 */

public class ComponentMortgage extends DealEntity {

    protected int componentId;
    protected int copyId;
    protected String propertyTaxAllocateFlag;
    protected String existingAccountIndicator;
    protected String existingAccountNumber;
    protected int rateGaranteePeriod;
    protected String cashabckAmountOverride;
    protected Date firstpaymentDate;
    protected double cashbackAmount;
    protected double cashbackPercent;
    protected int paymentFrequencyId;
    protected int privilegePaymentId;
    protected int prePaymentOptionsId;
    protected int actualPaymentTerm;
    protected double additionalPrincipal;
    protected double advanceHold;
    protected int amortizationTerm;
    protected double balanceRemainingAtEndOfTerm;
    protected double buyDownRate;
    protected String commissionCode;
    protected double discount;
    protected int effectiveAmortizationMonths;
    protected Date firstPaymentDateMonthly;
    protected int iadNumberOfDays;
    protected double interestAdjustmentAmount;
    protected Date interestAdjustmentDate;
    protected Date maturityDate;
    protected String miAllocateFlag;
    protected double netInterestRate;
    protected double pAndIPaymentAmount;
    protected double pAndIPaymentAmountMonthly;
    protected double perDiemInterestAmount;
    protected double premium;
    protected double propertyTaxEscrowAmount;
    protected String rateLock;
    protected double totalMortgageAmount;
    protected double totalPaymentAmount;
    protected double mortgageAmount;

    /**
     * <p>ComponentMortgage</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @param bTemplate boolean
     * @throws SQLException
     */
    public ComponentMortgage(BRInterpretor interpretor, boolean bTemplate) 
        throws SQLException {
        super(interpretor);
        if(bTemplate) {
        // place holder incase need to propagate template creation
        }
    }

    /**
     * <p>ComponentMortgage</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @throws SQLException
     */
    public ComponentMortgage(BRInterpretor interpretor) throws SQLException {
        super(interpretor);
        filter = null;

        if(Open()) {
            next();
        }
    }

    /**
     * <p>ComponentMortgage</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @param anObj Object
     * @throws SQLException
     */
    public ComponentMortgage(BRInterpretor interpretor, Object anObj) 
        throws SQLException {
    
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + ((Component)anObj).componentId + " and copyId = " + ((Deal)anObj).copyId;
    
        if(Open()) {
            next();
        }
    }

    /**
     * <p>ComponentMortgage</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @param componentId int
     * @param copyId int
     * @throws SQLException
     */
    public ComponentMortgage(BRInterpretor interpretor, int componentId, int copyId) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + componentId + " and copyId = " + copyId;
    
        if(Open()) {
            next();
        }
    }

    /**
     * <p>ComponentMortgage</p>
     * <p>Description: constractor</p>
     * @param interpretor BRInterpretor
     * @param filter String
     * @throws SQLException
     */
    public ComponentMortgage(BRInterpretor interpretor, String filter) 
        throws SQLException {

        super(interpretor);
        this.filter = filter;

        if(Open()) {
            next();
        }
    }

    /**
     * <p>getPrimaryKeyName</p>
     * <p>Description:get first primary key name </p>
     * @return String
     */
    protected String getPrimaryKeyName()  {
        return "componentId";
    }

    /**
     * <p>deepCopy</p>
     * <p>Description:get clone</p>
     * @return ComponentMortgage
     * @throws CloneNotSupportedException
     */
    public ComponentMortgage deepCopy() throws CloneNotSupportedException {
      return(ComponentMortgage)this.clone();
    }

    /**
     * <p>getDefaultSQL</p>
     * <p>Description:get base select sql </p>
     * @return String
     */
    protected String getDefaultSQL() {
      return "select * from Componentmortgage";
    }
    
    /**
     * <p>next</p>
     * <p>Description:get child ComponentOverdraft if componentType is matched</p>
     * @param 
     * @return Object ComponentOverdraft
     **/
    public boolean next() throws SQLException
    {
        boolean bStatus = super.next();
        if (!bStatus) {
            return bStatus;
        }

        componentId = resultSet.getInt("COMPONENTID");
        copyId = resultSet.getInt("COPYID");
        propertyTaxAllocateFlag = resultSet.getString("PROPERTYTAXALLOCATEFLAG");
        existingAccountIndicator = resultSet.getString("EXISTINGACCOUNTINDICATOR");
        existingAccountNumber = resultSet.getString("EXISTINGACCOUNTNUMBER");
        rateGaranteePeriod = resultSet.getInt("RATEGUARANTEEPERIOD");
        cashabckAmountOverride = resultSet.getString("CASHBACKAMOUNTOVERRIDE");
        cashbackAmount = resultSet.getDouble("CASHBACKAMOUNT");
        cashbackPercent = resultSet.getDouble("CASHBACKPERCENT");
        firstpaymentDate = asDate(resultSet.getTimestamp("FIRSTPAYMENTDATE"));
        advanceHold = resultSet.getDouble("ADVANCEHOLD");
        amortizationTerm = resultSet.getInt("AMORTIZATIONTERM");
        balanceRemainingAtEndOfTerm = resultSet.getInt("BALANCEREMAININGATENDOFTERM");
        buyDownRate = resultSet.getDouble("BUYDOWNRATE");
        commissionCode = resultSet.getString("COMMISSIONCODE");
        discount = resultSet.getDouble("DISCOUNT");
        effectiveAmortizationMonths = resultSet.getInt("EFFECTIVEAMORTIZATIONMONTHS");
        privilegePaymentId = resultSet.getInt("PRIVILEGEPAYMENTID");
        prePaymentOptionsId = resultSet.getInt("PREPAYMENTOPTIONSID");
        actualPaymentTerm = resultSet.getInt("ACTUALPAYMENTTERM");
        additionalPrincipal = resultSet.getInt("ADDITIONALPRINCIPAL");
        firstPaymentDateMonthly = asDate(resultSet.getTimestamp("FIRSTPAYMENTDATEMONTHLY"));
        iadNumberOfDays = resultSet.getInt("IADNUMBEROFDAYS");
        interestAdjustmentAmount = resultSet.getInt("INTERESTADJUSTMENTAMOUNT");
        interestAdjustmentDate = asDate(resultSet.getTimestamp("INTERESTADJUSTMENTDATE"));
        maturityDate = resultSet.getDate("MATURITYDATE");
        miAllocateFlag = resultSet.getString("MIALLOCATEFLAG");
        netInterestRate = resultSet.getDouble("NETINTERESTRATE");
        pAndIPaymentAmount = resultSet.getDouble("PANDIPAYMENTAMOUNT");
        pAndIPaymentAmountMonthly = resultSet.getDouble("PANDIPAYMENTAMOUNTMONTHLY");
        perDiemInterestAmount = resultSet.getDouble("PERDIEMINTERESTAMOUNT");
        premium = resultSet.getDouble("PREMIUM");
        propertyTaxEscrowAmount = resultSet.getDouble("PROPERTYTAXESCROWAMOUNT");
        rateLock = resultSet.getString("RATELOCK");
        totalMortgageAmount = resultSet.getDouble("TOTALMORTGAGEAMOUNT");
        totalPaymentAmount = resultSet.getDouble("TOTALPAYMENTAMOUNT");
        mortgageAmount = resultSet.getDouble("MORTGAGEAMOUNT");

        return bStatus;
    }
}
