//
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class IterateNode implements Cloneable {
   private BRInterpretor interpretor = null;
   protected int         nParseFlag = 0;
   protected int         nElements;
   protected int         curIndex;
   protected String      entityName;
   protected String      instanceName;
   protected Object[]    arrayObj;
   protected Object      instObj;
   protected BRVarNode   attribute;     // not null if not leaf node
   protected Object      entityObj;
   DoNode   doBlock = null;      // block statements to iterate over

   protected String   errorMsg;

   public IterateNode(BRInterpretor interpretor) {
      this.interpretor = interpretor;
   }

   public IterateNode deepCopy() throws ParseException {
      IterateNode     iterateNode;

      try {
         iterateNode = (IterateNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return iterateNode;
   }

   public void display() {
   }

   public Object evaluate() throws ParseException, SQLException {
      int        i;
      Object     result = null;
      Class      classObj;
      ReturnNode  returnNode = new ReturnNode(interpretor);

      // nothing to do if doBlock is null
      if (doBlock == null) return new Boolean(false);

      // iterate through the array
      for (i=0; i<nElements; ++i) {
         // get the current instance
         interpretor.setIterateInstanceVariable(instanceName, arrayObj[i]);
         result = doBlock.evaluate();

         classObj = result.getClass();
         if (classObj.isInstance(returnNode)) {
            return ((ReturnNode)result).evaluate();
            }
         }

      return result;
   }

   public boolean parseExpressionPhase2() throws ParseException, NoSuchFieldException,
          IllegalAccessException {
      String    curToken;
      Class   classObj;
      Field         aField;

      // verify if the specified array name is valid entity
      curToken = MosEntities.mapEntityName(entityName);

      // get current entity class object
      classObj = entityObj.getClass();

      // verify if the specified array name is an attribute of the
      // entityObj
      try {
         aField = classObj.getDeclaredField(entityName);
         arrayObj = (Object [])aField.get(entityObj);
         }
      catch (Exception e) {
         errorMsg = "Invalid object: " + entityName + " specified for Iterate statement!";
         throw new ParseException(errorMsg, 0);
         }

        // save the number of elements within the array
        nElements = Array.getLength(arrayObj);

      if (doBlock != null) {
         classObj = doBlock.getClass();
         if (!((DoNode)doBlock).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + doBlock + ">";
            throw new ParseException(errorMsg, 0);
            }
         }

      return true;
   }
}
