// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

//--DJ_LDI_CR--//
public class LifeDisPremiumsIOnlyA extends DealEntity {

  protected  int      lifeDisPremiumsIOnlyAId;
  protected  int      LDInsuranceRateId;
  protected  int      insureOnlyApplicantId;
  protected  int      copyId;
  protected  int      disabilityStatusId;
  protected  int      lifeStatusId;
  protected  int      lenderProfileId;
  protected  int      LDInsuranceTypeId;

  public String getPrimaryKeyName() {
      return "lifeDisabilityPremiumsId";
   }

   public LifeDisPremiumsIOnlyA(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public LifeDisPremiumsIOnlyA(BRInterpretor interpretor, String whereClause) throws SQLException {
      super(interpretor);
      this.filter = whereClause;

      if (Open())
         next();
   }

   public LifeDisPremiumsIOnlyA deepCopy() throws CloneNotSupportedException {
      return (LifeDisPremiumsIOnlyA)this.clone();
   }

   public String getDefaultSQL() {
      return "select * from LifeDisPremiumsIOnlyA";
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				lifeDisPremiumsIOnlyAId  = resultSet.getInt("LIFEDISPREMIUMSIONLYAID");
				LDInsuranceRateId  = resultSet.getInt("LDINSURANCERATEID");
				insureOnlyApplicantId  = resultSet.getInt("INSUREONLYAPPLICANTID");
				copyId  = resultSet.getInt("COPYID");
				disabilityStatusId  = resultSet.getInt("disabilityStatusId");
				lifeStatusId  = resultSet.getInt("lifeStatusId");
        lenderProfileId  = resultSet.getInt("lenderProfileId");
				LDInsuranceTypeId  = resultSet.getInt("LDInsuranceTypeId");
        }

      return bStatus;
   }
}
