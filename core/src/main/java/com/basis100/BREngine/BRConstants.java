
// Business Rule Expression Scanner, add array
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;

public class BRConstants {

   public static int    MAX_COLUMNS_PER_ROW = 64;
   public static int    MAX_ROWS_PER_FETCH = 64;
   public static int    MOS_MAX_MSG_LEN = 102;         // max msg length
   public static int    MAX_OUTPUT_PARAMETERS = 8;

   public static final int    DEFERRED_ATTRIBUTE  = -3;
   public static final int    ARRAY_ITERATOR  = -2;
   public static final int    SCALAR_VARIABLE = -1;
   public static final int    INDEX_VARIABLE  = 0;
   public static final int    NULL_INTEGER  = -999;

   public static final int    NOT_EXPECTING_BRACKET = 0;
   public static final int    EXPECTING_RIGHT_BRACKET = 1;

   public static final int    NOT_EXPECTING_ELSE_BLOCK = 0;
   public static final int    EXPECTING_ELSE_BLOCK     = 1;
   public static final int    PROCESSING_ELSE_BLOCK    = 2;

   public static boolean bDebug         = false;

   public static final char NO_DELIMITER     = ' ';
   public static final char STR_DELIMITER     = '\"';
   public static final char SQL_STR_DELIMITER = '\'';

   public static final int OP_PLUS     = '+';
   public static final int OP_MINUS    = '-';
   public static final int OP_MULTIPLY = '*';
   public static final int OP_DIVISION   = '/';
   public static final int OP_GT   = '>';
   public static final int OP_GE   = '>' + 100;
   public static final int OP_LT   = '<';
   public static final int OP_LE   = '<' + 100;
   public static final int OP_NE   = '~';
   public static final int OP_EQ   = '=';
   public static final int OP_AND  = '&';
   public static final int OP_OR   = '|';
   public static final int OP_XOR  = '^';
   public static final int OP_NOT  = '!';
   public static final int OP_ASSIGN          = '=' + 100;
   public static final int OP_RIGHT_PAREN  = ')';
   public static final int OP_LEFT_PAREN  = '(';
   public static final int OP_CAT_EXPRESSION = 2;
   public static final int OP_CAT_CONSTANT = 1;
   public static final short OP_RULE_EXTENSION = 1;
   public static final short OP_RULE_ACTION = 2;

   public static final short MAX_TOKEN_DEPTH = 1000;
}
