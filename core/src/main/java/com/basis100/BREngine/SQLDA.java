// Business Rule Expression Scanner, add array
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;

// yml: 2000-08-23
public class SQLDA {
   public String   colName;
   public int      colType;
   public int      colPrecision;
   public int      colScale;
   public short    colLength;
   public boolean  colNullable;
}
