package com.basis100.BREngine;

import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * <p>Title: WorkflowTrigger</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright Filogix (c) 2002</p>
 *
 * <p>Company: Filogix</p>
 * @author Mike Morris
 * @version 1.0
 */
public class WorkflowTrigger
  extends DealEntity
{
  private int dealId;
  protected String value;
  protected String triggerKey;
  protected WFTriggerHelper helper;

  //Static variables used throughout the class
  private static final String WORKFLOW_TRIGGER_KEY = "WorkflowTriggerKey";
  private static final String DEAL_ID = "DealId";
  private static final String KEY_VALUE = "Value";
  private static final String SELECT_CLAUSE = "Select * from workflowTrigger ";

  public int getDealId()
  {
    return dealId;
  }

  public String getTriggerKey()
  {
    return triggerKey;
  }

  public String getValue()
  {

    return value;
  }

  /**
   * WorkflowTrigger Contructor
   *
   * @param interpretor BRInterpretor
   * @param whereClause String
   * @throws SQLException
   */
  public WorkflowTrigger(BRInterpretor interpretor, String whereClause) throws SQLException
  {
    super(interpretor);
    filter = whereClause;
    helper = new WFTriggerHelper();
   
    if(Open())
    {
      while(next())
      {
        ;
      }
      close();
    }
  }

  /**
   * WorkflowTrigger Constructor
   *
   * This constructor is really only meant to be used my the
   * help inner class because the parent class DealEntity does
   * not provide a zero args contructor.
   *
   * @param interpretor BRInterpretor
   * @throws SQLException
   */
  private WorkflowTrigger(BRInterpretor interpretor) throws SQLException
  {
    super(interpretor);
  }

  public boolean next() throws SQLException
  {

    boolean bStatus = super.next();
    WorkflowTrigger key = new WorkflowTrigger(interpretor);

    if(bStatus)
    {
      this.dealId = resultSet.getInt(DEAL_ID);
      key.dealId = resultSet.getInt(DEAL_ID);
      key.triggerKey = resultSet.getString(WORKFLOW_TRIGGER_KEY) != null ?
        resultSet.getString(WORKFLOW_TRIGGER_KEY).trim() : null;
      key.value = resultSet.getString(KEY_VALUE) != null ?
        resultSet.getString(KEY_VALUE).trim() : null;

      helper.addTrigger(key.getTriggerKey(), key);
    }

    return bStatus;
  }

  public String getDefaultSQL()
  {

    return SELECT_CLAUSE;
  }

  protected WorkflowTrigger deepCopy() throws CloneNotSupportedException
  {
    return(WorkflowTrigger)this.clone();
  }
   
 
  public boolean isWFTriggerSet()
  {

    return "Y".equalsIgnoreCase(value) ? true : false;
  }

  public WorkflowTrigger[] getTriggers()
  {
    return helper.getTriggers();
  }

  public String toString(){
      
      return dealId + " / " + triggerKey + " / " + value; 
  }
  
  //************************Start of WFtriggerHelper Inner class**********************
   /*
    * <p>Title: WFTriggerHelper</p>
    *
    * <p>Description: this inner class will contain a listing of all the
    *    WorkflowTrigger objects that are returned from the initial
    *    instantiation of the WorkflowTrigger(Interpretor, whereClause)
    *    constructor.  
    * </p>
    *
    * <p>Copyright: Copyright Filogix (c) 2002</p>
    *
    * <p>Company: Filogix </p>
    * @author Mike Morris
    * @version 1.0
    */
   public class WFTriggerHelper
   {

     private Hashtable wfTriggers;

     public WFTriggerHelper()
     {

       wfTriggers = new Hashtable();
     }

     public void addTrigger(String key, WorkflowTrigger aTrigger)
     {

       wfTriggers.put(key, aTrigger);
     }

     public WorkflowTrigger getTrigger(String key)
     {

       return(WorkflowTrigger)wfTriggers.get(key);
     }

     public WorkflowTrigger[] getTriggers()
     {

       int cnt = wfTriggers.size(), i = 0;
       WorkflowTrigger[] triggers = new WorkflowTrigger[cnt];
       Iterator iter = wfTriggers.values().iterator();

       while(iter.hasNext())
       {
         triggers[i] = (WorkflowTrigger)iter.next();
         i++;
       }
       return triggers;
     }
   }

   //************************End of WFTriggerHelper inner Class************************

}
