/*
 * ServiceRequest.java 
 * 
 * Created: 20-FEB-2006
 * Author:  Edward Steel
 */
package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * @author ESteel
 */
public class ServiceRequest extends DealEntity {

	protected int requestId;
	
	protected String specialInstructions;
	protected String serviceProviderRefNo;
	
	protected int propertyId;
	protected int copyId;
	
	/**
	 * Creates a new entity
	 * @param interpretor
	 * @throws SQLException
	 */
	public ServiceRequest(BRInterpretor interpretor) throws SQLException {
		super(interpretor);
	}

	/**
	 * Clone this ServiceRequest
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */
	public ServiceRequest deepCopy() throws CloneNotSupportedException {
		return (ServiceRequest) this.clone();		
	}
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
	protected String getPrimaryKeyName() {
		return "requestId";
	}
	
	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
	protected String getDefaultSQL() {
		return "SELECT * FROM SERVICEREQUEST";
	}
	
	/**
	 * Closes the DB connection.
	 * @throws SQLException If an error occurs.
	 */
	protected void close() throws SQLException {
		super.close();
	}
	
	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
	protected boolean next() throws SQLException { 
		boolean bStatus = super.next();
		
		if (bStatus) {
			requestId = resultSet.getInt("REQUESTID");			
			specialInstructions = resultSet.getString("SPECIALINSTRUCTIONS");
			serviceProviderRefNo = resultSet.getString("SERVICEPROVIDERREFNO");			
			propertyId = resultSet.getInt("PROPERTYID");
			copyId = resultSet.getInt("COPYID");
		}
		
		return bStatus;
	}

//----- START getters/ setters -------------------------------------------------
	/**
	 * @return Returns the copyId.
	 */
	public int getCopyId() {
		return copyId;
	}

	/**
	 * @return Returns the propertyId.
	 */
	public int getPropertyId() {
		return propertyId;
	}

	/**
	 * @return Returns the requestId.
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @return Returns the serviceProviderRefNo.
	 */
	public String getServiceProviderRefNo() {
		return serviceProviderRefNo;
	}

	/**
	 * @return Returns the specialInstructions.
	 */
	public String getSpecialInstructions() {
		return specialInstructions;
	}

	/**
	 * @param copyId The copyId to set.
	 */
	public void setCopyId(int copyId) {
		this.copyId = copyId;
	}

	/**
	 * @param propertyId The propertyId to set.
	 */
	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * @param requestId The requestId to set.
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * @param serviceProviderRefNo The serviceProviderRefNo to set.
	 */
	public void setServiceProviderRefNo(String serviceProviderRefNo) {
		this.serviceProviderRefNo = serviceProviderRefNo;
	}

	/**
	 * @param specialInstructions The specialInstructions to set.
	 */
	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}		
//------ END getters/ setters --------------------------------------------------
}
