// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

//--DJ_LDI_CR--//
public class LifeDisabilityPremiums extends DealEntity {

  protected  int      lifeDisabilityPremiumsId;
  protected  int      borrowerId;
  protected  int      copyId;
  protected  int      lenderProfileId;
  protected  int      LDInsuranceRateId;
  protected  int      LDInsuranceTypeId;
  protected  int      disabilityStatusId;
  protected  int      lifeStatusId;

  public String getPrimaryKeyName() {
      return "lifeDisabilityPremiumsId";
   }

   public LifeDisabilityPremiums(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public LifeDisabilityPremiums(BRInterpretor interpretor, String whereClause) throws SQLException {
      super(interpretor);
      this.filter = whereClause;

      if (Open())
         next();
   }

   public LifeDisabilityPremiums deepCopy() throws CloneNotSupportedException {
      return (LifeDisabilityPremiums)this.clone();
   }

   public String getDefaultSQL() {
      return "select * from LifeDisabilityPremiums";
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				lifeDisabilityPremiumsId  = resultSet.getInt("lifeDisabilityPremiumsId");
				LDInsuranceRateId  = resultSet.getInt("LDInsuranceRateId");
				borrowerId  = resultSet.getInt("BORROWERID");
				copyId  = resultSet.getInt("COPYID");
				disabilityStatusId  = resultSet.getInt("disabilityStatusId");
				lifeStatusId  = resultSet.getInt("lifeStatusId");
        lenderProfileId  = resultSet.getInt("lenderProfileId");
				LDInsuranceTypeId  = resultSet.getInt("LDInsuranceTypeId");
        }

      return bStatus;
   }
}
