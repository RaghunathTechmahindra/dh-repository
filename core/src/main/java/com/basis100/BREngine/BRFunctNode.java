// Business Rule Expression Scanner, add array
// change history
/*
 date     auther                 description
 2002-06-12   yml   add setItem(arrayObj, index, element) to allow user modification
 of array element
 2002-06-12   yml   add array(int index) to allow user construction of array object.
 2002-06-12   yml   add addItem(arrayObj, element) to allow yserconstruction of user array
 2002-06-12   yml   add deleteItem(arrayObjl, index) to allow user deletion of element

 2002-06-12   yml   enhance indexof() function to return the first array element
 that matches the specified value
 2002-06-13   yml   add Array([size]) support
 2002-06-13   yml   add addItem(arrayObj, value) support
 2002-06-14   yml   deleteItem(arrayObj, 1-based-index) and
 insertItem(arrayObj, 1-based-index, element) support
 2002-07-04   yml   isLeapYear(object) and getDaysInMonth(object)
 2002-07-05   yml   enrich isLeapYear(), getDaysInMonth(), getDay(), etc function

 08Sep2003   BL    Added tosqlstr(srcStr) function to check and return a Quote Safe String
 31Aug2004   BL    Added debug(msg) function to print debug messages in the log file

 26Oct2004: Cervus II : Added new method getWFTrigger(triggerKey) (AME spec. section 8.2.2)
 26Oct2004: Cervus II : Added new method setWFTrigger(triggerKey, value) (AME spec. section 8.2.2)
 26Oct2004: Cervus II : Added new method isWFTrigger(triggerKey) (AME spec. section 8.2.2)

 */
package com.basis100.BREngine;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.lang.Exception;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

public class BRFunctNode {

    private BRInterpretor interpretor;

    public int nParmCount;

    public List parameters;

    public String functName;

    protected Deal dealObj;

    private String errorMsg;

    public BRFunctNode(BRInterpretor interpretor) {
        this.interpretor = interpretor;
        parameters = new ArrayList();
        nParmCount = -1; // -1 before "(" was parsed
        //  0 after ( was parsed
        //  n number of parameters parsed
    }

    public BRFunctNode(BRInterpretor interpretor, Deal dealObj, String aName) {
        this.interpretor = interpretor;
        parameters = new ArrayList();
        nParmCount = -1; // -1 before "(" was parsed
        //  0 after ( was parsed
        functName = interpretor.castFunctionName(aName);
        this.dealObj = dealObj;
    }

    protected void pushParameter(Object parmObj) {
        parameters.add(parmObj);
        ++nParmCount;
    }

    protected Object popParameter() {
        int i = parameters.size();
        --nParmCount;
        return parameters.remove(--i);
    }

    /*
     * -------------------------------------------------------------------------------
     * Function: count(String expression); Input: expression, e.g.
     * borrower[..].employmentHistory[..].employmentId > 0 Output: Integer of
     * the count of occurence when the expression is true
     * ----------------------------------------------------------------------------------
     */
    private Object count() throws ParseException {
        int i, nSize, nCount = 0;
        Object parmObj, result;
        Object[] arrayObjs;
        Class classObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);

        parmObj = parameters.get(0);
        classObj = parmObj.getClass();

        if (classObj.isInstance(expNode)) {
            expNode = (BRExpNode) parmObj;

            // check if full array was specified
            if (expNode.hasArrayIterator()) {
                // expands the expNode
                BRExpNode[] expNodes = expNode.expandsArrayIterator();
                nSize = Array.getLength(expNodes);
                for (i = 0; i < nSize; ++i) {
                    expNode = expNodes[i];
                    nCount += expNode.count();
                }
            }
        } else if (classObj.isInstance(cachedVarObj)) {
            cachedVarObj = (BRVarNode) parmObj;
            if (cachedVarObj.hasArrayIterator()) {
                BRVarNode[] varObjs = cachedVarObj.expandsArrayIterator();
                nSize = Array.getLength(varObjs);
                for (i = 0; i < nSize; ++i) {
                    cachedVarObj = varObjs[i];
                    nCount += cachedVarObj.count();
                }
            }
        } else if (classObj.isInstance(usrVarNode)) {
            result = ((VarNode) parmObj).evaluate();
            if (result.getClass().isArray()) {
                arrayObjs = (Object[]) result;
                nCount += Array.getLength(arrayObjs);
            }
        } else if (classObj.isArray()) {
            nCount += Array.getLength(parmObj);
        }

        return new Integer(nCount);
    }

    private Object getDayOfYear() throws ParseException {
        int intValue = -1;
        Calendar calObj = evaluateDate();
        if (calObj != null) {
            intValue = calObj.get(Calendar.DAY_OF_YEAR);

        }
        return (Object) (new Integer(intValue));
    }

    // 2002-07-05: yml enrich dateObj parameter
    // dateObj argument can be specified as ISO string format,
    // Date/Time/Timestamp
    // object, local/global/cached variable or function that return an object in
    // ISO string or Date/Time/Timestamp object. If no argument was specified,
    // use current day.
    private Calendar evaluateDate() throws ParseException {
        int year, month, day;
        Object varObj;
        Class classObj;
        BRVarNode varNode = new BRVarNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        String strObj = "";

        if (nParmCount == 0) {
            return new GregorianCalendar();
        }

        varObj = parameters.get(0);
        if (varObj == null) return null;
        
        classObj = varObj.getClass();
        
        //added null check after evaluate for FXP21278
        //got NullPointerException when mostly first payment date is missing.
        if (classObj.isInstance(usrVarNode)) {
            varObj = ((VarNode) varObj).evaluate();
            if (varObj == null) return null;
            classObj = varObj.getClass();
        } else if (classObj.isInstance(varNode)) {
            varObj = ((BRVarNode) varObj).evaluate();
            if (varObj == null) return null;
            classObj = varObj.getClass();
        } else if (classObj.isInstance(expNode)) {
            varObj = ((BRExpNode) varObj).evaluate();
            if (varObj == null) return null;
            classObj = varObj.getClass();
        } else if (classObj.isInstance(functNode)) {
            varObj = ((BRFunctNode) varObj).evaluate();
            if (varObj == null) return null;
            classObj = varObj.getClass();
        }

        if (classObj.isInstance(strObj)) {
            String strDate = (String) varObj;
            int offset = strDate.indexOf('-');
            String strToken = strDate.substring(0, offset);
            year = (new Integer(strToken)).intValue();
            int start = offset + 1;
            offset = strDate.indexOf('-', start);
            strToken = strDate.substring(start, offset);
            month = (new Integer(strToken)).intValue();

            strToken = strDate.substring(offset + 1);
            day = (new Integer(strToken)).intValue();
            return new GregorianCalendar(year, month - 1, day);
        }

        if (varObj == null) {
            return null;
        }

        java.util.Date dateObj = (java.util.Date) varObj;
        Calendar calObj = Calendar.getInstance();
        calObj.setTime(dateObj);
        return calObj;
    }

    private Object evaluateVar(Object varObj) throws ParseException {
        Object parm1 = varObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);

        Class class1 = varObj.getClass();
        // evaluate expression or function node if present

        if (class1.isInstance(expNode)) {
            parm1 = ((BRExpNode) parm1).evaluate();
        } else if (class1.isInstance(functNode)) {
            parm1 = ((BRFunctNode) parm1).evaluate();
        } else if (class1.isInstance(cachedVarNode)) {
            parm1 = ((BRVarNode) parm1).evaluate();
        } else if (class1.isInstance(usrVarNode)) {
            parm1 = ((VarNode) parm1).evaluate();
        }
        // why did I convert int to double? function max(10, 20 + 5) failed
        //      else if (class1.isInstance(intObj)) {
        //         parm1 = new Double(((Integer)parm1).intValue());
        //         }

        return parm1;
    }

    private Object getYear() throws ParseException {
        int intValue = -1;
        Calendar calObj = evaluateDate();
        if (calObj != null) {
            intValue = calObj.get(Calendar.YEAR);

        }
        return (Object) (new Integer(intValue));
    }

    private Object getMonth() throws ParseException {
        int intValue = -1;
        Calendar calObj = evaluateDate();
        if (calObj != null) {
            intValue = calObj.get(Calendar.MONTH) + 1;

        }
        return (Object) (new Integer(intValue));
    }

    private Object getDay() throws ParseException {
        int intValue = -1;
        Calendar calObj = evaluateDate();
        if (calObj != null) {
            intValue = calObj.get(Calendar.DAY_OF_MONTH);

        }
        return (Object) (new Integer(intValue));
    }

    // 2002-07-04: getDaysInMonth( [dateObj] ), where dateObj can be a constant
    // specifing an ISO String date, an actual date object, a
    // local/global/cached variable,
    // an expression or a function evaluates to a string ISO date of date
    // object.
    // If no argument is provided, default to today
    private Object getDaysInMonth() throws ParseException {
        int year, month, day;
        Object varObj;
        Class classObj;
        BRVarNode varNode = new BRVarNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        java.util.Date dateObj;
        String strMsg = "";

        if (nParmCount > 1) {
            strMsg = "Too many agruments specified for isLeapYear, zero or one is required!";
            throw new ParseException(strMsg, 0);
        }

        if (nParmCount == 0) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date today = new java.util.Date();
            varObj = (Object) formatter.format(today);
            classObj = varObj.getClass();
        } else {
            varObj = parameters.get(0);
            classObj = varObj.getClass();
        }

        if (classObj.isInstance(usrVarNode)) {
            varObj = ((VarNode) varObj).evaluate();
            classObj = varObj.getClass();
        } else if (classObj.isInstance(varNode)) {
            varObj = ((BRVarNode) varObj).evaluate();
            classObj = varObj.getClass();
        } else if (classObj.isInstance(expNode)) {
            varObj = ((BRExpNode) varObj).evaluate();
            classObj = varObj.getClass();
        } else if (classObj.isInstance(functNode)) {
            varObj = ((BRFunctNode) varObj).evaluate();
            classObj = varObj.getClass();
        }

        if (varObj == null) {
            return null;
        } else if (classObj.isInstance(strMsg)) {
            String strDate = (String) varObj;
            int offset = strDate.indexOf('-');
            String strToken = strDate.substring(0, offset);
            year = (new Integer(strToken)).intValue();
            int start = offset + 1;
            offset = strDate.indexOf('-', start);
            strToken = strDate.substring(start, offset);
            month = (new Integer(strToken)).intValue();

            strToken = strDate.substring(offset + 1);
            day = (new Integer(strToken)).intValue();
        } else {
            dateObj = (java.util.Date) varObj;
            year = dateObj.getYear();
            month = dateObj.getMonth() + 1;
            day = dateObj.getDay();
        }

        GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
        int nDays = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return (Object) (new Integer(nDays));
    }

    // 2002-07-04: isLeapYear( [dateObj] ), where dateObj can be an integer
    // constant
    // specifying the year, a string constant specifying an ISO date string,
    // a local/global/cached variable, an expression or function that evaluates
    // to an integer, an ISO date string or an actual date object.
    private Object isLeapYear() throws ParseException {
        int year;
        Object varObj;
        Class classObj;
        Integer intObj = new Integer(0);
        Date dateObj = new Date(0);
        Time timeObj = new Time(0);
        Timestamp tsObj = new Timestamp(0);
        BRVarNode varNode = new BRVarNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        String strMsg = "";

        if (nParmCount > 1) {
            strMsg = "Too many argument specified for isLeapYear, zero or one is required!";
            throw new ParseException(strMsg, 0);
        }

        if (nParmCount == 0) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date today = new java.util.Date();
            varObj = (Object) formatter.format(today);
            classObj = varObj.getClass();
        } else {
            varObj = parameters.get(0);
            classObj = varObj.getClass();
        }

        if (classObj.isInstance(usrVarNode)) {
            varObj = ((VarNode) varObj).evaluate();
            classObj = varObj.getClass();
        } else if (classObj.isInstance(varNode)) {
            varObj = ((BRVarNode) varObj).evaluate();
            classObj = varObj.getClass();
        } else if (classObj.isInstance(expNode)) {
            varObj = ((BRExpNode) varObj).evaluate();
            classObj = varObj.getClass();
        } else if (classObj.isInstance(functNode)) {
            varObj = ((BRFunctNode) varObj).evaluate();
            classObj = varObj.getClass();
        }

        if (varObj == null) {
            return null;
        } else if (classObj.isInstance(strMsg)) {
            String strDate = (String) varObj;
            int offset = strDate.indexOf('-');
            String strToken = strDate.substring(0, offset);
            year = (new Integer(strToken)).intValue();
        } else if (classObj.isInstance(intObj)) {
            year = ((Integer) varObj).intValue();
        } else if (classObj.isInstance(tsObj)) {
            year = ((java.util.Date) varObj).getYear();
        } else if (classObj.isInstance(timeObj)) {
            year = ((java.util.Date) varObj).getYear();
        } else if (classObj.isInstance(dateObj)) {
            year = ((java.util.Date) varObj).getYear();
        } else {
            year = ((java.util.Date) varObj).getYear();
        }

        GregorianCalendar calendar = new GregorianCalendar();
        return (Object) (new Boolean(calendar.isLeapYear(year)));
    }

    private Object getDayOfWeek() throws ParseException {
        int intValue = -1;
        Calendar calObj = evaluateDate();
        if (calObj != null) {
            intValue = calObj.get(Calendar.DAY_OF_WEEK);

        }
        return (Object) (new Integer(intValue));
    }

    protected static boolean isInteger(String token) {
        int i, nStart = 0;
        int length = token.length();

        if (length == 0) {
            return false;
        }
        char aChar = token.charAt(0);
        if (aChar == '-' || aChar == '+') {
            nStart = 1;

        }
        for (i = nStart; i < length; ++i) {
            if (!Character.isDigit(token.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private Object isEmpty_Array(Object[] arrayObj) throws ParseException {
        int length;
        boolean bAny = false;
        Object varObj;
        Class class1;
        String strObj = "";

        if (nParmCount == 2) {
            varObj = parameters.get(1);
            if (varObj.getClass().isInstance(strObj)) {
                strObj = (String) varObj;
                bAny = strObj.equalsIgnoreCase("any");
            }
        }

        int nSize = Array.getLength(arrayObj);

        // return true if every element is empty, otherwise return false
        for (int i = 0; i < nSize; ++i) {
            varObj = arrayObj[i];
            class1 = varObj.getClass();
            if (class1.isInstance(strObj)) {
                length = ((String) varObj).length();
                if (bAny) {
                    if (length == 0) {
                        return new Boolean(true);
                    }
                } else {
                    if (length > 0) {
                        return new Boolean(false);
                    }
                }
            } else {
                strObj = "isEmpty() parameter type mismatch: "
                        + class1.getName()
                        + ", expecting string or string array!";
                throw new ParseException(strObj, 0);
            }
        }

        if (bAny) {
            return new Boolean(false);
        }

        return new Boolean(true);
    }

    private Object isEmpty() throws ParseException {
        boolean bEmpty = false, bAny = false;
        Object parmObj;
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        String strObj = "";

        if (nParmCount == 2) {
            parmObj = parameters.get(1);
            parmObj = interpretor.evaluateExpression(parmObj);
            if (parmObj.getClass().isInstance(strObj)) {
                bAny = ((String) parmObj).equalsIgnoreCase("any");
                if (!bAny) {
                    strObj = "Invalid agrument 2 for isEmpty(), \"ANY\" is expected!";
                    throw new ParseException(strObj, 0);
                }
            }
        }

        parmObj = parameters.get(0);
        Class classObj = parmObj.getClass();
        if (classObj.isInstance(cachedVarObj)) {
            bEmpty = ((BRVarNode) parmObj).isEmpty(bAny);
        } else if (classObj.isInstance(usrVarNode)) {
            bEmpty = ((VarNode) parmObj).isEmpty(bAny);
        } else if (classObj.isArray()) {
            return isEmpty_Array((Object[]) parmObj);
        } else if (classObj.isInstance(strObj)) {
            return new Boolean(((String) parmObj).trim().length() == 0 ? true
                    : false);
        }

        return new Boolean(bEmpty);
    }

    protected static boolean isFloat(String str) {
        int i, decimalCount = 0, nStart = 0;
        int length = str.length();
        char aChar, charArray[];

        charArray = str.toCharArray();
        if (charArray[0] == '-' || charArray[0] == '+') {
            nStart = 1;
        }
        for (i = nStart; i < length; ++i) {
            aChar = charArray[i];
            if (aChar == '.' && ++decimalCount > 1) {
                return false;
            }

            if (aChar != '.' && !Character.isDigit(aChar)) {
                return false;
            }
        }
        return decimalCount == 1 ? true : false;
    }

    private Object isTrue() throws ParseException {
        boolean bAny = false, bTrue = false;
        Object parmObj;
        String strObj = "";
        Boolean boolObj = new Boolean(true);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        BRExpNode[] expNodes;

        if (nParmCount == 2) {
            parmObj = parameters.get(1);
            parmObj = interpretor.evaluateExpression(parmObj);
            if (parmObj.getClass().isInstance(strObj)) {
                bAny = ((String) parmObj).equalsIgnoreCase("any");
                if (!bAny) {
                    strObj = "Invalid agrument 2 for isTrue(), \"ANY\" is expected!";
                    throw new ParseException(strObj, 0);
                }
            }
        }

        parmObj = parameters.get(0);
        Class classObj = parmObj.getClass();
        if (classObj.isInstance(expNode)) {
            expNode = (BRExpNode) parmObj;

            // check if full array was specified
            if (expNode.hasArrayIterator()) {
                // expands the expNode
                expNodes = expNode.expandsArrayIterator();
                return isTrue_Array((Object[]) expNodes);
            } else {
                bTrue = expNode.isTrue(bAny);
            }
        } else if (classObj.isInstance(cachedVarObj)) {
            bTrue = ((BRVarNode) parmObj).isTrue(bAny);
        } else if (classObj.isInstance(usrVarNode)) {
            bTrue = ((VarNode) parmObj).isTrue(bAny);
        } else if (classObj.isArray()) {
            return isTrue_Array((Object[]) parmObj);
        } else if (classObj.isInstance(boolObj)) {
            return (Boolean) parmObj;
        }

        return new Boolean(bTrue);
    }

    private Object isTrue_Array(Object[] arrayObj) throws ParseException {
        boolean bAny = false, bTrue;
        Boolean boolObj = new Boolean(true);
        Object varObj;
        Class class1;
        String strObj = "";
        BRExpNode expNode = new BRExpNode(interpretor);

        if (nParmCount == 2) {
            varObj = parameters.get(1);
            if (varObj.getClass().isInstance(strObj)) {
                strObj = (String) varObj;
                bAny = strObj.equalsIgnoreCase("any");
            }
        }

        int nSize = Array.getLength(arrayObj);

        // return true if every element is empty, otherwise return false
        for (int i = 0; i < nSize; ++i) {
            varObj = arrayObj[i];
            class1 = varObj.getClass();
            if (class1.isInstance(boolObj)) {
                bTrue = ((Boolean) varObj).booleanValue();
                if (bAny && bTrue) {
                    return new Boolean(true);
                }
                if (bAny) {
                    if (bTrue) {
                        return new Boolean(true);
                    }
                } else {
                    if (!bTrue) {
                        return new Boolean(false);
                    }
                }
            } else if (class1.isInstance(expNode)) {
                bTrue = ((BRExpNode) varObj).isTrue(bAny);
                if (bAny) {
                    if (bTrue) {
                        return new Boolean(true);
                    }
                } else {
                    if (!bTrue) {
                        return new Boolean(false);
                    }
                }
            } else {
                strObj = "isTrue() parameter type mismatch: "
                        + class1.getName()
                        + ", expecting boolean or boolean array!";
                throw new ParseException(strObj, 0);
            }
        }

        bTrue = (bAny) ? false : true;
        return new Boolean(bTrue);
    }

    protected boolean isTypeBoolean() {
        return true;
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
     * isValidSIN(String strSIN) return true if the specified SIN number is a
     * valid SIN. Valid SIN format include straight SIN number, with space or
     * "-" as separator. For examples: "222-323-222", "222 323 222" or
     * "222323222"
     *
     * A good sin number for testing: 222-323-222
     */
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    private Object isValidSIN() throws ParseException {
        Object parmObj;
        Class classObj;
        int index = 0;
        String strSin, strObj = "";
        char[] charArray;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);

        parmObj = parameters.get(0);
        classObj = parmObj.getClass();

        // evaluate expression or function node if present
        if (classObj.isInstance(expNode)) {
            parmObj = ((BRExpNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(functNode)) {
            parmObj = ((BRFunctNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(cachedVarNode)) {
            parmObj = ((BRVarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(usrVarNode)) {
            parmObj = ((VarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(strObj)) {
            classObj = parmObj.getClass();
        } else {
            errorMsg = "Invalid datatype specified for isValidSIN(), string expected!";
            throw new ParseException(errorMsg, 0);
        }

        if (parmObj == null) {
            return new Boolean(false);
        }

        classObj = parmObj.getClass();
        if (!classObj.isInstance(strObj)) {
            errorMsg = "Invalid datatype specified for isValidSIN(), string expected!";
            throw new ParseException(errorMsg, 0);
        }

        // strim leading and trailing blanks
        strSin = ((String) parmObj).trim();

        // strim '-' from string
        index = strSin.indexOf('-');
        while (index >= 0) {
            strSin = strSin.substring(0, index) + strSin.substring(index + 1);
            index = strSin.indexOf('-');
        }

        // strip all blanks
        index = strSin.indexOf(' ');
        while (index >= 0) {
            strSin = strSin.substring(0, index) + strSin.substring(index + 1);
            index = strSin.indexOf(' ');
        }

        // length must be 9 character long
        int length = strSin.length();
        if (length != 9) {
            return new Boolean(false);
        }

        // all nine characters must all be digits

        charArray = strSin.toCharArray();

        int b = 0;
        for (index = 0; index < 4; ++index) {
            b += (charArray[index * 2] - '0');
        }

        int c = 0;
        for (index = 0; index < 4; ++index) {
            c *= 10;
            c += charArray[index * 2 + 1] - '0';
        }
        int f = b + sumOfDigits(c * 2);

        int g = f - f / 10 * 10;

        int j = 10 - g;
        int k = j - j / 10 * 10;
        int l = charArray[8] - '0';

        return new Boolean(k == l ? true : false);
    }

//  #4827 
    private Object isDigit() throws ParseException
    {
        Object parmObj = null;
        Class classObj = null;
	    String strObj = "";
	    BRExpNode expNode = new BRExpNode(interpretor);
	    BRVarNode cachedVarNode = new BRVarNode(interpretor);
	    VarNode usrVarNode = new VarNode(interpretor);
	    BRFunctNode functNode = new BRFunctNode(interpretor);
	
	    parmObj = parameters.get(0);
	    classObj = parmObj.getClass();
	
	    // evaluate expression or function node if present
	    if (classObj.isInstance(expNode)) {
	        parmObj = ((BRExpNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(functNode)) {
	        parmObj = ((BRFunctNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(cachedVarNode)) {
	        parmObj = ((BRVarNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(usrVarNode)) {
	        parmObj = ((VarNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(strObj)) {
	        classObj = parmObj.getClass();
	    } else {
	        errorMsg = "Invalid datatype specified for isValidSIN(), string expected!";
	        throw new ParseException(errorMsg, 0);
	    }

    	if (parmObj == null) {
            return new Boolean(false);
        }

        classObj = parmObj.getClass();
        if (!classObj.isInstance(strObj)) {
            errorMsg = "Invalid datatype specified for isDigit(), string expected!";
            throw new ParseException(errorMsg, 0);
        }
        char char1 = ((String) parmObj).trim().charAt(0);
  	  return new Boolean(Character.isDigit(char1));
  	  
    }
    
    //#4827 
    private Object isNumeric() throws ParseException
    {
        Object parmObj = null;
        Class classObj = null;
	    String strObj = "";
	    BRExpNode expNode = new BRExpNode(interpretor);
	    BRVarNode cachedVarNode = new BRVarNode(interpretor);
	    VarNode usrVarNode = new VarNode(interpretor);
	    BRFunctNode functNode = new BRFunctNode(interpretor);
	
	    parmObj = parameters.get(0);
	    classObj = parmObj.getClass();
	
	    // evaluate expression or function node if present
	    if (classObj.isInstance(expNode)) {
	        parmObj = ((BRExpNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(functNode)) {
	        parmObj = ((BRFunctNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(cachedVarNode)) {
	        parmObj = ((BRVarNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(usrVarNode)) {
	        parmObj = ((VarNode) parmObj).evaluate();
	        classObj = parmObj.getClass();
	    } else if (classObj.isInstance(strObj)) {
	        classObj = parmObj.getClass();
	    } else {
	        errorMsg = "Invalid datatype specified for isValidSIN(), string expected!";
	        throw new ParseException(errorMsg, 0);
	    }

        if (parmObj == null) {
            return new Boolean(false);
        }

        classObj = parmObj.getClass();
        if (!classObj.isInstance(strObj)) {
            errorMsg = "Invalid datatype specified for isNumeric(), string expected!";
            throw new ParseException(errorMsg, 0);
        }

        String str = ((String) parmObj).trim();

        if (str == null) return Boolean.FALSE;
        if ("".equals(str)) return Boolean.FALSE;
  	  
    	for (int i=0; i<str.length(); i++)
    	{
  		  if (!Character.isDigit(str.charAt(i)))
  			  return Boolean.FALSE;
    	}
  	  return Boolean.TRUE;
    }
    
    private Object now() {
        Calendar calObj = Calendar.getInstance();
        long mSeconds = (calObj.getTime()).getTime();
        Date dateObj = new Date(mSeconds);
        return dateObj;
    }

    protected Object or() throws ParseException {
        int i, j, size;
        boolean bStatus = false;
        Boolean boolObj = new Boolean(true);
        Object parmObj;
        Class classObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRExpNode[] expNodes;
        String msg = "_or(x) parameter type mismatch, boolean value expected instead of: ";

        for (i = 0; i < nParmCount; ++i) {
            parmObj = parameters.get(i);
            classObj = parmObj.getClass();

            if (classObj.isInstance(expNode)
                    && ((BRExpNode) parmObj).hasArrayIterator()) {
                // expands the expNode
                expNode = (BRExpNode) parmObj;
                expNodes = expNode.expandsArrayIterator();
                size = Array.getLength(expNodes);
                bStatus = false;
                for (j = 0; j < size; ++j) {
                    parmObj = (Boolean) expNodes[j].evaluate();
                    bStatus = ((Boolean) parmObj).booleanValue();
                    if (bStatus) {
                        return new Boolean(true);
                    }
                }
            } else {
                parmObj = evaluateVar(parmObj);
                if (parmObj == null) {
                    throw new ParseException(msg + " null!", 0);
                }
                classObj = parmObj.getClass();
                if (classObj.isInstance(boolObj)) {
                    bStatus = ((Boolean) parmObj).booleanValue();
                }
            }

            if (bStatus) {
                return new Boolean(true);
            }
        }

        return new Boolean(false);
    }

    protected boolean parseDBPrimaryKeys() throws ParseException {
        String owner, tableName, colName, primaryKey, filter, parmStr = null;
        Object parmObj;
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);
        String strObj = "";
        Class classObj;
        SQLDA sqlDA;

        owner = (String) (parameters.get(0));
        tableName = (String) (parameters.get(1));
        primaryKey = interpretor.getPrimaryKey(owner, tableName);

        colName = (String) (parameters.get(2));
        sqlDA = interpretor.getColumnType(owner, tableName, colName);
        parmObj = parameters.get(3);
        classObj = parmObj.getClass();

        filter = primaryKey + "=";

        switch (sqlDA.colType) {
        case Types.TINYINT:
        case Types.INTEGER:
        case Types.SMALLINT:
            if (!classObj.isInstance(intObj)) {
                errorMsg = "Mismatch primary key datatype, short or int is expected!";
                throw new ParseException(errorMsg, 0);
            }
            parmStr = parmObj.toString();
            break;
        case Types.FLOAT:
        case Types.DOUBLE:
        case Types.REAL:
        case Types.DECIMAL:
            if (!classObj.isInstance(doubleObj)) {
                errorMsg = "Mismatch primary key datatype, double is expected!";
                throw new ParseException(errorMsg, 0);
            }
            parmStr = parmObj.toString();
            break;
        case Types.NUMERIC:
            if (sqlDA.colScale == 0) {
                if (!classObj.isInstance(intObj)) {
                    errorMsg = "Mismatch primary key datatype, short or int is expected!";
                    throw new ParseException(errorMsg, 0);
                }
            } else {
                if (!classObj.isInstance(doubleObj)) {
                    errorMsg = "Mismatch primary key datatype, double is expected!";
                    throw new ParseException(errorMsg, 0);
                }
            }
            parmStr = parmObj.toString();
            break;
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
            if (!classObj.isInstance(strObj)) {
                errorMsg = "Mismatch primary key datatype, string is expected!";
                throw new ParseException(errorMsg, 0);
            }
            parmStr = "\'" + (String) parmObj + "\'";
            break;
        case Types.TIME: // ??????????????
        case Types.TIMESTAMP:
        case Types.DATE:
            break;
        default:
        }
        filter += parmStr;
        parameters.set(3, filter);

        return true;
    }

    /*
     * yml: 2002-04-29 change to support execSQL(sqlStmt) for pasthrough
     * execution of user specified sql statement.
     */
    private void parseExecSQL() throws ParseException {
        if (nParmCount != 1) {
            errorMsg = "SQL statement missing!";
            throw new ParseException(errorMsg, 0);
        }
    }

    // yml: 2000-08-21
    // support fetch returning scalar value by returning first colomn of the
    // first
    // rolw of the result set only.
    private void parseFetch() throws ParseException {
        Object parmObj;
        Class classObj;
        BRVarNode cachedVarNode = new BRVarNode(interpretor);

        switch (nParmCount) {
        case 2: // fetch(selectClause, fromClause)
            break;
        case 3: // fetch(selectClause, fromClause, whereClass)
            break;
        case 4: // fetch(selectClause, fromClause, whereClass, orderClause)
            break;
        default: // unsupported arguments
            errorMsg = "Fetch function with " + nParmCount + " not supported!";
            throw new ParseException(errorMsg, 0);
        }
        for (int i = 0; i < nParmCount; ++i) {
            parmObj = parameters.get(i);
            classObj = parmObj.getClass();
            if (classObj.isInstance(cachedVarNode)) {
                parameters.set(i, ((BRVarNode) parmObj).entityName);
            }
        }
    }

    private void parseToDate() throws ParseException {

        if (nParmCount != 3) {
            errorMsg = "Fetch function with " + nParmCount + " not supported!";
            throw new ParseException(errorMsg, 0);
        }
    }

    // yml: 2000-08-22
    protected boolean parseExpressionPhase2() throws ParseException {
        int i, length;
        String token, errorMsg;
        Class classObj;
        Object parmNode;
        String tableName, primaryKey;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRVarNode varNode = new BRVarNode(interpretor);
        VarNode tmpVarNode = new VarNode(interpretor);
        BRAssignNode assignNode = new BRAssignNode(interpretor);

        for (i = 0; i < nParmCount; ++i) {
            parmNode = parameters.get(i);
            if (parmNode != null) {
                classObj = parmNode.getClass();

                if (classObj.isInstance(expNode)) {
                    if (!((BRExpNode) parmNode).parseExpressionPhase2()) {
                        errorMsg = "Unable to map node: <" + parmNode + ">";
                        throw new ParseException(errorMsg, 0);
                    }
                } else if (classObj.isInstance(functNode)) {
                    if (!((BRFunctNode) parmNode).parseExpressionPhase2()) {
                        errorMsg = "Unable to map node: <" + parmNode + ">";
                        throw new ParseException(errorMsg, 0);
                    }
                } else if (classObj.isInstance(varNode)) {
                } else if (classObj.isInstance(tmpVarNode)) {
                } else if (classObj.isInstance(assignNode)) {
                    ((BRAssignNode) parmNode).parseExpressionPhase2();
                } else {
                    token = (String) parmNode;
                    if (token.compareToIgnoreCase("null") == 0) {
                        parmNode = null;
                        parameters.set(i, parmNode);
                    } else {
                        try {
                            if (isInteger(token)) { // integer
                                parmNode = BRUtil.mapInt(token);
                            } else if (isFloat(token)) // double
                            {
                                parmNode = BRUtil.mapDouble(token);
                            } else { // string
                                // now strip the string delimeter
                                if (token.charAt(0) == '\"') {
                                    length = token.length();
                                    token = token.substring(1, length - 1);
                                }

                                parmNode = token;
                            }
                            parameters.set(i, parmNode);
                        } catch (Exception e) {
                            errorMsg = "Invalid token \"" + token
                                    + "\" found as argument of function \""
                                    + functName + "\"";
                            throw new ParseException(errorMsg, 0);
                        }
                    }
                }
            }
        }

        // verify filter datatypes if function is "exist"
        if (functName.compareToIgnoreCase("exist") == 0) {
            /*
             * owner = (String)(parameters.get(0)); tableName =
             * (String)(parameters.get(1)); primaryKey =
             * interpretor.getPrimaryKey(owner, tableName); if
             * (primaryKey.compareToIgnoreCase("dealid") == 0) { token =
             * parameters.get(2) + " and dealId=" + dealObj.dealId;
             * parameters.set(2, token); }
             */
            parseFetch();
        } else if (functName.compareToIgnoreCase("execSQL") == 0) {
            parseExecSQL();
        } else if (functName.compareToIgnoreCase("existInDeal") == 0) {
            tableName = (String) (parameters.get(0));
            primaryKey = interpretor.getPrimaryKey("", tableName);
            if (primaryKey.compareToIgnoreCase("dealid") == 0) {
                token = parameters.get(1) + " and dealId=" + dealObj.dealId;
                parameters.set(1, token);
            }
        } else if (functName.compareToIgnoreCase("fetchColumn") == 0) {
            parseFetch();
        } else if (functName.compareToIgnoreCase("fetchRows") == 0) {
            parseFetch();
        } else if (functName.compareToIgnoreCase("fetchRow") == 0) {
            parseFetch();
        } else if (functName.compareToIgnoreCase("fetchField") == 0) {
            parseFetch();
        } else if (functName.compareToIgnoreCase("fetch") == 0) {
            parseFetch();
        } else if (functName.equalsIgnoreCase("toDate")) {
            parseToDate();
        }

        return true;
    }

    private Object and() throws ParseException {
        int i, j, size;
        boolean bStatus = true;
        Boolean boolObj = new Boolean(true);
        Object parmObj;
        Class classObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRExpNode[] expNodes;

        String msg = "_and(x) parameter type mismatch, boolean value expected instead of: ";
        for (i = 0; i < nParmCount; ++i) {
            parmObj = parameters.get(i);
            classObj = parmObj.getClass();
            if (classObj.isInstance(expNode)
                    && ((BRExpNode) parmObj).hasArrayIterator()) {
                // expands the expNode
                expNode = (BRExpNode) parmObj;
                expNodes = expNode.expandsArrayIterator();
                size = Array.getLength(expNodes);
                bStatus = false;
                for (j = 0; j < size; ++j) {
                    parmObj = (Boolean) expNodes[j].evaluate();
                    bStatus = ((Boolean) parmObj).booleanValue();
                    if (!bStatus) {
                        return new Boolean(false);
                    }
                }
            } else {
                parmObj = evaluateVar(parmObj);
                if (parmObj == null) {
                    throw new ParseException(msg + " null!", 0);
                }
                classObj = parmObj.getClass();
                if (classObj.isInstance(boolObj)) {
                    bStatus = ((Boolean) parmObj).booleanValue();
                }
            }

            if (!bStatus) {
                return new Boolean(false);
            }
        }

        return new Boolean(true);
    }

    private Object min() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object parm1 = evaluateVar(parameters.get(0));
        Class class1 = parm1.getClass();
        Object parm2 = evaluateVar(parameters.get(1));
        Class class2 = parm2.getClass();

        // make sure mixed datatypes are converted to float
        if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
            parm2 = new Double(((Integer) parm2).intValue());
            class2 = parm2.getClass();
        } else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
            parm1 = new Double(((Integer) parm1).intValue());
            class1 = parm1.getClass();
        }

        if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
            int intValue1 = ((Integer) parm1).intValue();
            int intValue2 = ((Integer) parm2).intValue();
            return (new Integer(Math.min(intValue1, intValue2)));
        } else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
            double doubleValue1 = ((Double) parm1).doubleValue();
            double doubleValue2 = ((Double) parm2).doubleValue();
            return (new Double(Math.min(doubleValue1, doubleValue2)));
        } else {
            throw new ParseException("min(x,y) parameter type mismatch", 0);
        }
    }

    private Object max() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object parm1 = evaluateVar(parameters.get(0));
        Class class1 = parm1.getClass();
        Object parm2 = evaluateVar(parameters.get(1));
        Class class2 = parm2.getClass();

        if (class1.isInstance(doubleObj) && class2.isInstance(intObj)) {
            parm2 = new Double(((Integer) parm2).intValue());
            class2 = parm2.getClass();
        } else if (class1.isInstance(intObj) && class2.isInstance(doubleObj)) {
            parm1 = new Double(((Integer) parm1).intValue());
            class1 = parm1.getClass();
        }

        if (class1.isInstance(intObj) && class2.isInstance(intObj)) {
            int intValue1 = ((Integer) parm1).intValue();
            int intValue2 = ((Integer) parm2).intValue();

            return (new Integer(Math.max(intValue1, intValue2)));
        } else if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
            double doubleValue1 = ((Double) parm1).doubleValue();
            double doubleValue2 = ((Double) parm2).doubleValue();

            return (new Double(Math.max(doubleValue1, doubleValue2)));
        } else {
            throw new ParseException("max(x,y) parameter type mismatch", 0);
        }
    }

    // exist(owner, tableName, filter);
    // e.g. exist(null, documentTracking, "conditionId=2 and conditionStatus =
    // 2");
    private Object exist() throws ParseException {
        Object param;
        String sqlStmt = null;
        boolean bStatus;

        switch (nParmCount) {
        case 2:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            break;
        case 3:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);

            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;
            }
            break;
        case 4:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;

            }
            param = parameters.get(3);
            if (param != null) {
                sqlStmt += " order by " + param;
            }
            break;
        }

        Object[] row = interpretor.fetchDBRow(sqlStmt);
        if (row == null) {
            bStatus = false;
        } else {
            bStatus = row.getClass().isArray();

        }
        return new Boolean(bStatus);
    }

    // ymlam: 2000-08-22
    private Object fetch() throws ParseException {
        String sqlStmt = null;

        switch (nParmCount) {
        case 2:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            break;
        case 3:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1) + " where " + parameters.get(2);
            break;
        case 4:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1) + " where " + parameters.get(2)
                    + " order by " + parameters.get(3);
            break;
        }

        // now execute the stmt and return first 64 rows as array of rows
        return interpretor.fetchDBField(sqlStmt);

        //      Sql_Column[] columns = interpretor.fetchDBColumns(sqlStmt, filter);

        // return first column
        //      return columns[0].colValue;
    }

    // yml: 2000-08-23
    private Object fetchField() throws ParseException {
        Object param;
        String sqlStmt = null;

        switch (nParmCount) {
        case 2:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            break;
        case 3:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);

            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;
            }
            break;
        case 4:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;

            }
            param = parameters.get(3);
            if (param != null) {
                sqlStmt += " order by " + param;
            }
            break;
        }
        return interpretor.fetchDBField(sqlStmt);
    }

    // yml: 2000-08-22
    private Object[] fetchColumn() throws ParseException {
        Object param;
        String sqlStmt = null;

        switch (nParmCount) {
        case 2:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            break;
        case 3:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);

            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;
            }
            break;
        case 4:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;

            }
            param = parameters.get(3);
            if (param != null) {
                sqlStmt += " order by " + param;
            }
            break;
        }

        return interpretor.fetchDBColumn(sqlStmt);
    }

    private Object[] fetchRow() throws ParseException {
        Object param;
        String sqlStmt = null;

        switch (nParmCount) {
        case 2:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            break;
        case 3:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);

            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;
            }
            break;
        case 4:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;

            }
            param = parameters.get(3);
            if (param != null) {
                sqlStmt += " order by " + param;
            }
            break;
        }

        return interpretor.fetchDBRow(sqlStmt);
    }

    // yml: 2000-08-22
    private Object[] fetchRows() throws ParseException {
        Object param;
        String sqlStmt = null;

        switch (nParmCount) {
        case 2:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            break;
        case 3:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);

            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;
            }
            break;
        case 4:
            sqlStmt = "select " + parameters.get(0) + " from "
                    + parameters.get(1);
            param = parameters.get(2);
            if (param != null) {
                sqlStmt += " where " + param;

            }
            param = parameters.get(3);
            if (param != null) {
                sqlStmt += " order by " + param;
            }
            break;
        }
        return interpretor.fetchDBRows(sqlStmt);
    }

    private Object find() throws ParseException {
        int i, index = -1;
        boolean bUserVar = false, bCachedVar = false;
        Boolean boolObj = new Boolean(false);
        String filter = "";
        BRExpNode expObj = new BRExpNode(interpretor);
        BRFunctNode functObj = new BRFunctNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);

        Object filterObj = parameters.get(1); // filter
        Class classObj = filterObj.getClass();

        if (classObj.isInstance(filter)) {
            filter = interpretor.substituteUserVars((String) filterObj,
                    BRConstants.STR_DELIMITER);
        } else if (classObj.isInstance(expObj)) {
            filter = (String) ((BRExpNode) filterObj).evaluate();
        } else if (classObj.isInstance(functObj)) {
            filter = (String) ((BRFunctNode) filterObj).evaluate();
        } else if (classObj.isInstance(cachedVarNode)) {
            filter = (String) ((BRVarNode) filterObj).evaluate();
        } else if (classObj.isInstance(usrVarNode)) {
            filter = (String) ((VarNode) filterObj).evaluate();
        } else {
            filter = filterObj.toString();

        }
        Object varObj = parameters.get(0);
        classObj = varObj.getClass();
        if (classObj.isInstance(usrVarNode)) {
            bUserVar = ((VarNode) varObj).isUserDefinedVariable();
            varObj = ((VarNode) varObj).evaluate();
            classObj = varObj.getClass();
        } else if (classObj.isInstance(cachedVarNode)) {
            bCachedVar = true;
            varObj = ((BRVarNode) varObj).evaluate();
            classObj = varObj.getClass();
        }

        // now extract attribute name to bind with the array
        interpretor.scanExpression(filter);
        try {
            BRExpNode expNode = (BRExpNode) interpretor.parseExpressionPhase1();
            expNode.rebindCopyId();
            interpretor.parseExpressionPhase2(expNode);
            if (bUserVar) {
                index = expNode.findMatchUserVar(varObj);
            } else if (bCachedVar) {
                expNode.findMatchCachedVar(varObj);

                classObj = varObj.getClass();
                if (!classObj.isArray()) {
                    throw new ParseException(
                            "Find() argument 1 is not an array!", 0);
                }

                Object[] arrayObj = (Object[]) varObj;
                int nSize = Array.getLength(varObj);
                // now find the match
                for (i = 0; i < nSize; ++i) {
                    expNode.refreshDeferredEntityObj(arrayObj[i]);

                    // now evaluate the match
                    Object statusObj = expNode.evaluate();
                    if (statusObj != null) {
                        classObj = statusObj.getClass();
                        if (classObj.isInstance(boolObj)) { // ignore if result
                            // is not boolean
                            if (((Boolean) statusObj).booleanValue()) {
                                index = i;
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw new ParseException(ex.getMessage(), 0);
        }

        if (index > -1) {
            ++index;
        }
        return new Integer(index);
    }

    // existInDeal(tableName, filter);
    // e.g. exist(documentTracking, "conditionId=2 and conditionStatus = 2");
    // dealId = xxxx will be added as filter as well.
    private Object existInDeal() throws ParseException {
        String strObj = "";
        String sqlStmt, filter = null, errorMsg;
        Object parm1, parm2;
        Class class1 = null, class2;
        BRVarNode varNode = new BRVarNode(interpretor);
        Sql_Column[] columns;

        parm1 = parameters.get(0); // table name
        if (parm1 != null) {
            class1 = parm1.getClass();

        }
        parm2 = parameters.get(1); // filter
        class2 = parm2.getClass();

        // construct query statement
        sqlStmt = "select * from ";

        // specify table name
        if (class1.isInstance(varNode)) {
            sqlStmt += (String) (((BRVarNode) parm1).evaluate());
        } else if (class1.isInstance(strObj)) {
            sqlStmt += (String) parm1;
        } else {
            errorMsg = "Invalid table name specified: \"" + parm1
                    + "\" specified!";
            throw new ParseException(errorMsg, 0);
        }

        // check if filter specified
        if (class2 != null) {
            if (class2.isInstance(varNode)) {
                filter = (String) (((BRVarNode) parm2).evaluate());
            } else if (class2.isInstance(strObj)) {
                filter = (String) parm2;
            } else {
                errorMsg = "Invalid filter speecified: \"" + parm2
                        + "\" specified!";
                throw new ParseException(errorMsg, 0);
            }
        }

        // now fetch the column
        columns = interpretor.fetchDBColumns(sqlStmt, filter);
        return new Boolean(columns != null ? true : false);
    }

    // ymlam: 2000-08-23
    private Object sizeOf() throws ParseException {
        int nSize = 0;
        Class classObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRVarNode dbVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRAssignNode assignNode = new BRAssignNode(interpretor);
        Object argv = parameters.get(0);
        classObj = argv.getClass();

        // evaluate argv if it is either an expression or a funciton
        if (classObj.isInstance(expNode)) {
            expNode = (BRExpNode) argv;
            argv = expNode.evaluate();
            classObj = argv.getClass();
        } else if (classObj.isInstance(functNode)) {
            functNode = (BRFunctNode) argv;
            argv = functNode.evaluate();
            classObj = argv.getClass();
        } else if (classObj.isInstance(assignNode)) {
            argv = ((BRAssignNode) argv).evaluate();
            classObj = argv.getClass();
        } else if (classObj.isInstance(dbVarNode)) {
            argv = ((BRVarNode) argv).evaluate();
            classObj = argv.getClass();
        } else if (classObj.isInstance(usrVarNode)) {
            argv = ((VarNode) argv).evaluate();
            if (argv == null) {
                return new Integer(0);
            }
            classObj = argv.getClass();
        }

        // now proceed to determine the sizeof this result
        if (classObj.isArray()) {
            nSize = Array.getLength(argv);
        } else if (argv == null) {
            nSize = 0;
        } else {
            nSize = 1;

        }
        return new Integer(nSize);
    }

    private int sum_Integer(Object[] arrayObj) throws ParseException {
        int i, nCount;
        int value = 0;
        Object element;
        Class classObj;
        Integer intObj = new Integer(0);
        Double dfObj = new Double(0.0);
        Float fObj = new Float(0.0);

        nCount = arrayObj.length;
        for (i = 0; i < nCount; ++i) {
            element = arrayObj[i];
            classObj = element.getClass();

            if (classObj.isInstance(intObj)) {
                value += ((Integer) element).intValue();
            } else if (classObj.isInstance(dfObj)) {
                value += ((Double) element).intValue();
            } else if (classObj.isInstance(fObj)) {
                value += ((Float) element).intValue();
            } else {
                String strMsg = "Invalid datatype for sum(x) parameter!";
                throw new ParseException(strMsg, 0);
            }
        }

        return value;
    }

    private double sum_Double(Object[] arrayObj) throws ParseException {
        int i, nCount;
        double value = 0.0;
        Object element;
        Class classObj;
        Integer intObj = new Integer(0);
        Double dfObj = new Double(0.0);
        Float fObj = new Float(0.0);

        nCount = arrayObj.length;
        for (i = 0; i < nCount; ++i) {
            element = arrayObj[i];
            classObj = element.getClass();

            if (classObj.isInstance(intObj)) {
                value += ((Integer) element).doubleValue();
            } else if (classObj.isInstance(dfObj)) {
                value += ((Double) element).doubleValue();
            } else if (classObj.isInstance(fObj)) {
                value += ((Float) element).doubleValue();
            } else {
                String strMsg = "Invalid datatype for sum(x) parameter!";
                throw new ParseException(strMsg, 0);
            }
        }

        return value;
    }

    private Object sum_Array(Object[] arrayObj) throws ParseException {
        Object element;
        Class classObj;
        Object value = null;
        Integer intObj = new Integer(0);
        Double dfObj = new Double(0.0);
        Float fObj = new Float(0.0);

        element = arrayObj[0];
        classObj = element.getClass();

        if (classObj.isInstance(intObj)) {
            value = new Integer(sum_Integer(arrayObj));
        } else if (classObj.isInstance(dfObj)) {
            value = new Double(sum_Double(arrayObj));
        } else if (classObj.isInstance(fObj)) {
            value = new Double(sum_Double(arrayObj));
        } else {
            String strMsg = "Invalid datatype for sum(x) parameter!";
            throw new ParseException(strMsg, 0);
        }

        return value;
    }

    private Object sum() throws ParseException {
        Object parmObj, valObj = null;
        Class classObj;
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);

        parmObj = parameters.get(0);
        classObj = parmObj.getClass();

        if (classObj.isInstance(cachedVarObj)) {
            valObj = ((BRVarNode) parmObj).sum();
        } else if (classObj.isInstance(usrVarNode)) {
            valObj = ((VarNode) parmObj).sum();
        } else if (classObj.isArray()) {
            valObj = sum_Array((Object[]) parmObj);
        } else {
            errorMsg = "Invalid variable: \"" + parmObj + "\" specified!";
            throw new ParseException(errorMsg, 0);
        }

        return valObj;
    }

    private Object sum1() throws ParseException {
        int intResult = 0;
        double doubleResult = 0.0;
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);
        Object parm1, anEntity, value;
        Class classObj, entityClass;
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        Field aField = null;
        Object[] fieldObjs = null;

        parm1 = parameters.get(0);
        classObj = parm1.getClass();

        if (classObj.isInstance(cachedVarObj)) {
            cachedVarObj = (BRVarNode) parm1;
            classObj = cachedVarObj.entityObj.getClass();
            try {
                aField = classObj.getDeclaredField(cachedVarObj.entityName);
                fieldObjs = (Object[]) aField.get(cachedVarObj.entityObj);
            } catch (Exception ex) {
                String msg = "Invalid entity: " + cachedVarObj.entityName;
                throw new ParseException(msg, 0);
            }

            int nSize = Array.getLength(fieldObjs);
            for (int i = 0; i < nSize; ++i) {
                anEntity = fieldObjs[i];
                entityClass = anEntity.getClass();
                cachedVarObj.attribute.entityObj = anEntity;
                value = cachedVarObj.attribute.evaluate();
                if (value == null) {
                    return null;
                }
                classObj = value.getClass();
                if (classObj.isInstance(doubleObj)) {
                    doubleObj = (Double) value;
                    doubleResult += doubleObj.doubleValue();
                } else if (classObj.isInstance(intObj)) {
                    intObj = (Integer) value;
                    intResult += intObj.doubleValue();
                } else {
                    String msg = "Invalid datatype for sum(x) parameter!";
                    throw new ParseException(msg, 0);
                }
            }

            if (classObj.isInstance(intObj)) {
                return new Integer(intResult);
            } else {
                return new Double(doubleResult);
            }
        }

        return null;
    }

    private int sumOfDigits(int value) {
        String strTemp = "" + value;

        int size = strTemp.length();
        char[] charArray = strTemp.toCharArray();
        int sum = 0;
        for (int i = 0; i < size; ++i) {
            sum += (charArray[i] - '0');
        }
        return sum;
    }

    private Object abs() throws ParseException {
        Object varObj;
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);
        Class classObj;

        varObj = evaluateVar(parameters.get(0));
        classObj = varObj.getClass();

        if (classObj.isInstance(intObj)) {
            int intValue1 = ((Integer) varObj).intValue();

            return (new Integer(Math.abs(intValue1)));
        } else if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.abs(doubleValue)));
        } else {
            throw new ParseException("abs(x) parameter type mismatch", 0);
        }
    }

    private Object ceil() throws ParseException {
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(doubleObj)) {
            double doubleVar = ((Double) varObj).doubleValue();

            return (new Double(Math.ceil(doubleVar)));
        } else {
            throw new ParseException("ceil(x,y) parameter type mismatch", 0);
        }
    }

    private Object floor() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(intObj)) {
            varObj = new Double(((Integer) varObj).intValue());
            classObj = varObj.getClass();
        }

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.floor(doubleValue)));
        } else {
            throw new ParseException("floor(x,y) parameter type mismatch", 0);
        }
    }

    private Object getColumnCount() {
        VarNode usrVariable = new VarNode(interpretor);

        Object rsObj = parameters.get(0);
        Class class1 = rsObj.getClass();
        if (class1.isInstance(usrVariable)) {
            try {
                rsObj = ((VarNode) rsObj).evaluate();
                class1 = rsObj.getClass();
            } catch (ParseException ex) {
                return null;
            }
        }

        if (!class1.isArray()) {
            return null;
        }

        Sql_Column[] rsMetaData = interpretor.getMetaData(rsObj);
        return (Object) (new Integer(Array.getLength(rsMetaData)));
    }

    private SQLDA getColumnInfo() throws ParseException {
        Object intObj = new Integer(0);
        VarNode usrVariable = new VarNode(interpretor);

        Object rsObj = parameters.get(0);
        Class classObj = rsObj.getClass();
        if (classObj.isInstance(usrVariable)) {
            rsObj = ((VarNode) rsObj).evaluate();
            classObj = rsObj.getClass();
        }

        if (!classObj.isArray()) {
            return null;
        }

        // now handle the index
        Object indObj = parameters.get(1);
        classObj = indObj.getClass();
        if (classObj.isInstance(usrVariable)) {
            indObj = ((VarNode) indObj).evaluate();
            classObj = indObj.getClass();
        }
        if (!classObj.isInstance(intObj)) {
            return null;
        }

        int colIndex = ((Integer) indObj).intValue();

        Sql_Column[] rsMetaData = interpretor.getMetaData(rsObj);
        int nSize = Array.getLength(rsMetaData);

        if (colIndex < 1 || colIndex > nSize) {
            throw new ParseException(
                    "Invalid column index: array index out of bound", 0);
        }

        return rsMetaData[--colIndex].sqlda;
    }

    private Object getColumnName() throws ParseException {
        SQLDA sqlda = getColumnInfo();

        return sqlda.colName;
    }

    private Object getColumnPrecision() throws ParseException {
        SQLDA sqlda = getColumnInfo();

        return new Integer(sqlda.colPrecision);
    }

    private Object getColumnScale() throws ParseException {
        SQLDA sqlda = getColumnInfo();

        return new Integer(sqlda.colScale);
    }

    private Object getColumnType() throws ParseException {
        SQLDA sqlda = getColumnInfo();

        return new Integer(sqlda.colType);
    }

    private Object getItem() throws ParseException {
        int index;
        Object[] arrayObj;
        Object parm1 = parameters.get(0);
        Object parm2 = parameters.get(1);
        Integer intObj = new Integer(0);
        Class class1 = parm1.getClass();
        Class class2 = parm2.getClass();
        VarNode usrVariable = new VarNode(interpretor);
        String msg;

        if (class1.isInstance(usrVariable)) {
            parm1 = ((VarNode) parm1).evaluate();
            class1 = parm1.getClass();
        }

        // first parameter must be an array, otherwise always return itself
        if (class1.isArray()) {
            // make sure the second parameter is an integer
            if (!class2.isInstance(intObj)) {
                msg = "Invalid argument type for 2nd parameter, integer expected!";
                throw new ParseException(msg, 0);
            }

            index = ((Integer) parm2).intValue(); // 1-based index
            arrayObj = (Object[]) parm1;
            if (Array.getLength(arrayObj) < index) { // array index out of bound
                msg = "Array index out of bound!";
                throw new ParseException(msg, 0);
            }

            --index; // convert to 0-based index
            // extract the element
            Object element = arrayObj[index];
            class2 = element.getClass();

            if (class2.isInstance(usrVariable)) {
                return ((VarNode) element).evaluate();
            }
            return element;
        }

        return parm1;
    }

    // 2002-06-12: add setItem(arrayObj, 1-based index, value)
    private Object setItem() throws ParseException {
        int index, nSize;
        boolean bStatus = true;
        Integer intObj = new Integer(0);
        VarNode usrVariable = new VarNode(interpretor);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        String strMsg;

        // must have three arguments
        if ((nSize = parameters.size()) != 3) {
            strMsg = "Invalid argument count!";
            throw new ParseException(strMsg, 0);
        }

        Object parm1 = parameters.get(0);
        Object parm2 = parameters.get(1);
        Object parm3 = parameters.get(2);

        Class class1 = parm1.getClass();
        Class class2 = parm2.getClass();
        Class class3 = parm2.getClass();

        if (class1.isInstance(usrVariable)) {
            parm1 = ((VarNode) parm1).evaluate();
            class1 = parm1.getClass();
        }

        if (class2.isInstance(usrVariable)) {
            parm2 = ((VarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(cachedVarObj)) {
            parm2 = ((BRVarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(functNode)) {
            parm2 = ((BRFunctNode) parm2).evaluate();
            class2 = parm2.getClass();
        }

        if (class3.isInstance(usrVariable)) {
            parm3 = ((VarNode) parm3).evaluate();
            class3 = parm3.getClass();
        }

        // first parameter must be an array, otherwise always return itself
        if (!class1.isArray()) {
            return new Boolean(false);
        }

        // make sure the second parameter is an integer
        if (!class2.isInstance(intObj)) {
            strMsg = "Invalid argument type for parameter 2, integer expected!";
            throw new ParseException(strMsg, 0);
        }

        nSize = Array.getLength(parm1);
        index = ((Integer) parm2).intValue(); // 1-based index
        if (nSize < index) { // array index out of bound
            strMsg = "Array index out of bound!";
            throw new ParseException(strMsg, 0);
        }

        --index;

        parm1 = parameters.get(0);
        class1 = parm1.getClass();
        if (class1.isInstance(usrVariable)) {
            return ((VarNode) parm1).setItem(index, parm3);
        }

        return new Boolean(bStatus);
    }

    // 2002-06-14: deleteItem(arrayObj, 1-based-index)
    private Object deleteItem() throws ParseException {
        int nSize;
        Object parm1, parm2;
        Class class1, class2;
        Integer intObj = new Integer(0);
        VarNode usrVariable = new VarNode(interpretor);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        String strMsg;

        // must have three arguments
        if ((nSize = parameters.size()) != 2) {
            strMsg = "Invalid argument count, 2 is expected!";
            throw new ParseException(strMsg, 0);
        }

        parm1 = parameters.get(0);
        parm2 = parameters.get(1);

        class1 = parm1.getClass();
        class2 = parm2.getClass();

        if (class1.isInstance(usrVariable)) {
            parm1 = ((VarNode) parm1).evaluate();
            class1 = parm1.getClass();
        }

        if (class2.isInstance(usrVariable)) {
            parm2 = ((VarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(cachedVarObj)) {
            parm2 = ((BRVarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(functNode)) {
            parm2 = ((BRFunctNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(expNode)) {
            parm2 = ((BRExpNode) parm2).evaluate();
            class2 = parm2.getClass();
        }

        // first parameter must be an array, otherwise always return itself
        if (!class1.isArray()) {
            strMsg = "Invalid argument type for parameter 1, array object expected!";
            throw new ParseException(strMsg, 0);
        }

        // make sure the second parameter is an integer
        if (!class2.isInstance(intObj)) {
            strMsg = "Invalid argument type for parameter 2, integer expected!";
            throw new ParseException(strMsg, 0);
        }

        nSize = Array.getLength(parm1);
        int index = ((Integer) parm2).intValue(); // 1-based index
        if (nSize < index) { // array index out of bound
            strMsg = "Array index out of bound!";
            throw new ParseException(strMsg, 0);
        }

        --index;

        parm1 = parameters.get(0);
        class1 = parm1.getClass();
        if (class1.isInstance(usrVariable)) {
            return ((VarNode) parm1).deleteItem(index);
        }
        return new Boolean(true);
    }

    // 2002-06-14: insertItem(arrayObj, 1-based-index, itemObj)
    private Object insertItem() throws ParseException {
        int nSize;
        Object parm1, parm2, parm3;
        Class class1, class2, class3;
        Integer intObj = new Integer(0);
        VarNode usrVariable = new VarNode(interpretor);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        String strMsg;

        // must have three arguments
        if ((nSize = parameters.size()) != 3) {
            strMsg = "Invalid argument count, 3 is expected!";
            throw new ParseException(strMsg, 0);
        }

        parm1 = parameters.get(0);
        parm2 = parameters.get(1);
        parm3 = parameters.get(2);

        class1 = parm1.getClass();
        class2 = parm2.getClass();
        class3 = parm3.getClass();

        if (class1.isInstance(usrVariable)) {
            parm1 = ((VarNode) parm1).evaluate();
            class1 = parm1.getClass();
        }

        if (class2.isInstance(usrVariable)) {
            parm2 = ((VarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(cachedVarObj)) {
            parm2 = ((BRVarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(functNode)) {
            parm2 = ((BRFunctNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(expNode)) {
            parm2 = ((BRExpNode) parm2).evaluate();
            class2 = parm2.getClass();
        }

        if (class3.isInstance(usrVariable)) {
            parm3 = ((VarNode) parm3).evaluate();
            class3 = parm3.getClass();
        } else if (class3.isInstance(cachedVarObj)) {
            parm3 = ((BRVarNode) parm3).evaluate();
            class3 = parm3.getClass();
        } else if (class3.isInstance(functNode)) {
            parm3 = ((BRFunctNode) parm3).evaluate();
            class3 = parm3.getClass();
        } else if (class3.isInstance(expNode)) {
            parm3 = ((BRExpNode) parm3).evaluate();
            class3 = parm3.getClass();
        }

        // first parameter must be an array, otherwise always return itself
        if (!class1.isArray()) {
            strMsg = "Invalid argument type for parameter 1, array object expected!";
            throw new ParseException(strMsg, 0);
        }

        // make sure the second parameter is an integer
        if (!class2.isInstance(intObj)) {
            strMsg = "Invalid argument type for parameter 2, integer expected!";
            throw new ParseException(strMsg, 0);
        }

        nSize = Array.getLength(parm1);
        int index = ((Integer) parm2).intValue(); // 1-based index
        if (nSize < index) { // array index out of bound
            strMsg = "Array index out of bound!";
            throw new ParseException(strMsg, 0);
        }

        --index;

        parm1 = parameters.get(0);
        class1 = parm1.getClass();
        if (class1.isInstance(usrVariable)) {
            return ((VarNode) parm1).insertItem(index, parm3);
        }
        return new Boolean(true);
    }

    // 2002-06-12: add array(1-based array size)
    private Object array() throws ParseException {
        int nSize;
        Object[] row = new Object[0];
        Object parm1;
        Class class1;
        Integer intObj = new Integer(0);
        VarNode usrVariable = new VarNode(interpretor);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        String strMsg;

        if (nParmCount > 0) {
            parm1 = parameters.get(0);
            class1 = parm1.getClass();

            if (class1.isInstance(usrVariable)) {
                parm1 = ((VarNode) parm1).evaluate();
                class1 = parm1.getClass();
            } else if (class1.isInstance(cachedVarObj)) {
                parm1 = ((BRVarNode) parm1).evaluate();
                class1 = parm1.getClass();
            } else if (class1.isInstance(functNode)) {
                parm1 = ((BRFunctNode) parm1).evaluate();
                class1 = parm1.getClass();
            } else if (class1.isInstance(expNode)) {
                parm1 = ((BRExpNode) parm1).evaluate();
                class1 = parm1.getClass();
            }

            // size parameter must be an integer
            if (!class1.isInstance(intObj)) {
                strMsg = "Invalid argument type for parameter 1, integer expected!";
                throw new ParseException(strMsg, 0);
            }

            nSize = ((Integer) parm1).intValue();
            row = new Object[nSize];
        }

        return row;
    }

    // 2002-06-13: add addItem(arrayObj, value)
    private Object addItem() throws ParseException {
        Object parm1, parm2;
        Class class1, class2;
        VarNode usrVariable = new VarNode(interpretor);
        BRVarNode cachedVarObj = new BRVarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        Object[] arrayObj;
        String strMsg;

        if (nParmCount > 2) {
            strMsg = "Too many parameters specified, 2 is expected!";
            throw new ParseException(strMsg, 0);
        }
        if (nParmCount < 2) {
            strMsg = "Too few parameters specified, 2 is expected!";
            throw new ParseException(strMsg, 0);
        }

        parm1 = parameters.get(0);
        class1 = parm1.getClass();

        parm2 = parameters.get(1);
        class2 = parm2.getClass();

        if (class2.isInstance(usrVariable)) {
            parm2 = ((VarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(cachedVarObj)) {
            parm2 = ((BRVarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(functNode)) {
            parm2 = ((BRFunctNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(expNode)) {
            parm2 = ((BRExpNode) parm2).evaluate();
            class2 = parm2.getClass();
        }

        if (class1.isInstance(usrVariable)) {
            ((VarNode) parm1).addItem(parm2);
        } else if (class1.isArray()) {
            arrayObj = (Object[]) parm1;
            int nSize = arrayObj.length;
            Object[] newArray = new Object[nSize + 1];
            for (int i = 0; i < nSize; ++i) {
                newArray[i] = arrayObj[i];

            }
            newArray[nSize] = parm2;
            parameters.set(0, newArray);
        }

        return new Boolean(true);
    }

    private Object getMetaData() {
        VarNode usrVariable = new VarNode(interpretor);

        Object rsObj = parameters.get(0);
        Class class1 = rsObj.getClass();
        if (class1.isInstance(usrVariable)) {
            try {
                rsObj = ((VarNode) rsObj).evaluate();
                class1 = rsObj.getClass();
            } catch (ParseException ex) {
                return null;
            }
        }

        if (!class1.isArray()) {
            return null;
        }

        Sql_Column[] rsMetaData = interpretor.getMetaData(rsObj);
        int nSize = Array.getLength(rsMetaData);
        SQLDA[] metaData = new SQLDA[nSize];
        for (int i = 0; i < nSize; ++i) {
            metaData[i] = new SQLDA();
            metaData[i].colType = rsMetaData[i].sqlda.colType;
            metaData[i].colPrecision = rsMetaData[i].sqlda.colPrecision;
            metaData[i].colScale = rsMetaData[i].sqlda.colScale;
            metaData[i].colName = rsMetaData[i].sqlda.colName;
            //         metaData[i].bNull = rsMetaData[i].sqlda.bNull;
        }

        return (Object) metaData;
    }

    private Object log() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object parm1 = evaluateVar(parameters.get(0));
        Class class1 = parm1.getClass();

        if (class1.isInstance(intObj)) {
            parm1 = new Double(((Integer) parm1).intValue());
            class1 = parm1.getClass();
        }

        if (class1.isInstance(doubleObj)) {
            double doubleValue = ((Double) parm1).doubleValue();

            return (new Double(Math.log(doubleValue)));
        } else {
            throw new ParseException("log(x) parameter type mismatch", 0);
        }
    }

    private Object exp() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(intObj)) {
            varObj = new Double(((Integer) varObj).intValue());
            classObj = varObj.getClass();
        }

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.exp(doubleValue)));
        } else {
            throw new ParseException("exp(x) parameter type mismatch", 0);
        }
    }

    private Object execSQL() throws ParseException {
        String sqlStmt = "";

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        sqlStmt = (String) varObj;

        BRUtil.debug("@BRFunctNode.execSQL Stmt: " + sqlStmt);

        return interpretor.execSQL(sqlStmt);
    }

    private Object random() {
        return new Double(Math.random());
    }

    private Object round() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object parm1 = evaluateVar(parameters.get(0));
        Class class1 = parm1.getClass();

        if (class1.isInstance(intObj)) {
            parm1 = new Double(((Integer) parm1).intValue());
            class1 = parm1.getClass();
        }

        if (class1.isInstance(doubleObj)) {
            double doubleValue = ((Double) parm1).doubleValue();

            return (new Integer((int) Math.round(doubleValue)));
        } else {
            throw new ParseException("round(x) parameter type mismatch", 0);
        }
    }

    private Object sqrt() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object parm1 = evaluateVar(parameters.get(0));
        Class class1 = parm1.getClass();

        if (class1.isInstance(intObj)) {
            parm1 = new Double(((Integer) parm1).intValue());
            class1 = parm1.getClass();
        }

        if (class1.isInstance(doubleObj)) {
            double doubleValue = ((Double) parm1).doubleValue();

            return (new Double(Math.sqrt(doubleValue)));
        } else {
            throw new ParseException("sqrt(x) parameter type mismatch", 0);
        }
    }

    private Object pow() throws ParseException {
        Integer intObj = new Integer(0);
        Double doubleObj = new Double(0.0);

        Object parm1 = evaluateVar(parameters.get(0));
        Class class1 = parm1.getClass();
        Object parm2 = evaluateVar(parameters.get(1));
        Class class2 = parm2.getClass();

        if (class1.isInstance(intObj)) {
            parm1 = new Double(((Integer) parm1).intValue());
            class1 = parm1.getClass();
        }
        if (class2.isInstance(intObj)) {
            parm2 = new Double(((Integer) parm2).intValue());
            class2 = parm2.getClass();
        }

        if (class1.isInstance(doubleObj) && class2.isInstance(doubleObj)) {
            double doubleValue1 = ((Double) parm1).doubleValue();
            double doubleValue2 = ((Double) parm2).doubleValue();

            return (new Double(Math.pow(doubleValue1, doubleValue2)));
        } else {
            throw new ParseException("pow(x,y) parameter type mismatch", 0);
        }
    }

    private Object cos() throws ParseException {
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.cos(Math.toRadians(doubleValue))));
        } else {
            throw new ParseException("cos(x) parameter type mismatch", 0);
        }
    }

    private Object sin() throws ParseException {
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.sin(Math.toRadians(doubleValue))));
        } else {
            throw new ParseException("sin(x) parameter type mismatch", 0);
        }
    }

    private Object tan() throws ParseException {
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.tan(Math.toRadians(doubleValue))));
        } else {
            throw new ParseException("tan(x) parameter type mismatch", 0);
        }
    }

    private Object acos() throws ParseException {
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.toDegrees(Math.acos(doubleValue))));
        } else {
            throw new ParseException("acos(x) parameter type mismatch", 0);
        }
    }

    private Object asin() throws ParseException {
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.toDegrees(Math.asin(doubleValue))));
        } else {
            throw new ParseException("asin(x) parameter type mismatch", 0);
        }
    }

    private Object atan() throws ParseException {
        Double doubleObj = new Double(0.0);

        Object varObj = evaluateVar(parameters.get(0));
        Class classObj = varObj.getClass();

        if (classObj.isInstance(doubleObj)) {
            double doubleValue = ((Double) varObj).doubleValue();

            return (new Double(Math.toDegrees(Math.atan(doubleValue))));
        } else {
            throw new ParseException("atan(x) parameter type mismatch", 0);
        }
    }

    private Object strlen() throws ParseException {
        Object parmObj;
        String strObj = new String();
        Class classObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);

        parmObj = parameters.get(0);
        classObj = parmObj.getClass();

        // evaluate expression or function node if present
        if (classObj.isInstance(expNode)) {
            expNode = (BRExpNode) parmObj;
            parmObj = expNode.evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(functNode)) {
            functNode = (BRFunctNode) parmObj;
            parmObj = functNode.evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(cachedVarNode)) {
            parmObj = (Object) ((BRVarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(usrVarNode)) {
            parmObj = (Object) ((VarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        }

        if (classObj.isInstance(strObj)) {
            strObj = (String) parmObj;
            return (new Integer(strObj.length()));
        } else {
            throw new ParseException("strlen(srcStr) parameter type mismatch",
                    0);
        }
    }

    // indexOf(srcStr, subStr [, offset])
    // 2002-06-14: enhance indexof function to return index within the array
    // whose element
    // match the specified value, e.g. indexOf(arrayObj, value)
    private Object indexOf() throws ParseException {
        Object parm1, parm2, parm3 = null;
        Integer intObj = new Integer(0);
        String strObj = new String();
        String srcStr, subStr;
        Class class1, class2, class3 = null;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        VarNode varNode = new VarNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);

        parm1 = parameters.get(0);
        class1 = parm1.getClass();
        parm2 = parameters.get(1);
        class2 = parm2.getClass();

        if (nParmCount > 2) {
            parm3 = parameters.get(2);
            class3 = parm3.getClass();
        }

        // evaluate expression or function node if present
        if (class1.isInstance(expNode)) {
            expNode = (BRExpNode) parm1;
            parm1 = expNode.evaluate();
            class1 = parm1.getClass();
        } else if (class1.isInstance(functNode)) {
            functNode = (BRFunctNode) parm1;
            parm1 = functNode.evaluate();
            class1 = parm1.getClass();
        } else if (class1.isInstance(varNode)) {
            parm1 = ((VarNode) parm1).evaluate();
            class1 = parm1.getClass();
        }

        if (class2.isInstance(expNode)) {
            expNode = (BRExpNode) parm2;
            parm2 = expNode.evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(functNode)) {
            functNode = (BRFunctNode) parm2;
            parm2 = functNode.evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(varNode)) {
            parm2 = ((VarNode) parm2).evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(cachedVarNode)) {
            parm2 = ((BRVarNode) parm2).evaluate();
            class2 = parm2.getClass();
        }

        if (nParmCount > 2) {
            if (class3.isInstance(expNode)) {
                expNode = (BRExpNode) parm3;
                parm3 = expNode.evaluate();
                class3 = parm3.getClass();
            } else if (class3.isInstance(functNode)) {
                functNode = (BRFunctNode) parm3;
                parm3 = functNode.evaluate();
                class3 = parm3.getClass();
            } else if (class3.isInstance(varNode)) {
                parm3 = ((VarNode) parm3).evaluate();
                class3 = parm3.getClass();
            }
        }

        if (nParmCount == 2) {
            if (class1.isInstance(strObj) && class2.isInstance(strObj)) {
                srcStr = (String) parm1;
                subStr = (String) parm2;
                return (new Integer(srcStr.indexOf(subStr, 0)));
            } else if (class1.isArray()) {
                parm1 = parameters.get(0);
                class1 = parm1.getClass();
                if (!class1.isInstance(varNode)) {
                    throw new ParseException(
                            "indexOf(x,y) parameter 1 type mismatch, array expected!",
                            0);
                }
                int index = ((VarNode) parm1).indexOf(parm2);
                if (index != -1) {
                    ++index;
                }
                return new Integer(index);
            } else {
                throw new ParseException(
                        "indexOf(x, y) parameter type mismatch", 0);
            }
        } else {
            if (class1.isInstance(strObj) && class2.isInstance(strObj)
                    && class3.isInstance(intObj)) {
                srcStr = (String) parm1;
                subStr = (String) parm2;
                int start = ((Integer) parm3).intValue();
                return (new Integer(srcStr.indexOf(subStr, start)));
            } else {
                throw new ParseException(
                        "indexOf(srcStr, subStr) parameter type mismatch", 0);
            }
        }
    }

    protected char strDelimiter(String functName) {
        if (functName.equalsIgnoreCase("find")) {
            return BRConstants.STR_DELIMITER;
        }
        if (functName.equalsIgnoreCase("exist")) {
            return BRConstants.SQL_STR_DELIMITER;
        }

        return BRConstants.NO_DELIMITER;
    }

    protected void substituteUserVars() throws ParseException {
        int i;
        char delimiter;
        Class classObj;
        Object parmObj;
        String strObj = "";
        BRExpNode expNode = new BRExpNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);

        for (i = 0; i < nParmCount; ++i) {
            parmObj = parameters.get(i);
            classObj = parmObj.getClass();

            if (classObj.isInstance(expNode)) {
                ((BRExpNode) parmObj).substituteUserVars();
            } else if (classObj.isInstance(functNode)) {
                ((BRFunctNode) parmObj).substituteUserVars();
            } else if (classObj.isInstance(strObj)) {
                delimiter = strDelimiter(functName);
                strObj = (String) interpretor.substituteUserVars(
                        (String) parmObj, delimiter);
                parameters.set(i, strObj);
            }
        }
    }

    // substr(srcStr, [start,] length)
    // for example:
    //   -- start extracting the last length characters from the string
    //      substr(srcStr, length)
    //   -- start extracting from offset start for length characters
    //      substr(srcStr, start, length)
    private Object substr() throws ParseException {
        Object parm1, parm2, parm3 = null;
        Integer intObj = new Integer(0);
        String strObj = new String();
        String srcStr;
        Class class1, class2, class3 = null;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);

        parm1 = parameters.get(0);
        class1 = parm1.getClass();
        parm2 = parameters.get(1);
        class2 = parm2.getClass();

        if (nParmCount > 2) {
            parm3 = parameters.get(2);
            class3 = parm3.getClass();
        }

        // evaluate expression or function node if present
        if (class1.isInstance(expNode)) {
            expNode = (BRExpNode) parm1;
            parm1 = expNode.evaluate();
            class1 = parm1.getClass();
        } else if (class1.isInstance(functNode)) {
            functNode = (BRFunctNode) parm1;
            parm1 = functNode.evaluate();
            class1 = parm1.getClass();
        } else if (class1.isInstance(cachedVarNode)) {
            parm1 = (Object) ((BRVarNode) parm1).evaluate();
            class1 = parm1.getClass();
        } else if (class1.isInstance(usrVarNode)) {
            parm1 = (Object) ((VarNode) parm1).evaluate();
            class1 = parm1.getClass();
        }

        if (class2.isInstance(expNode)) {
            expNode = (BRExpNode) parm2;
            parm2 = expNode.evaluate();
            class2 = parm2.getClass();
        } else if (class2.isInstance(functNode)) {
            functNode = (BRFunctNode) parm2;
            parm2 = functNode.evaluate();
            class2 = parm2.getClass();
        }

        if (nParmCount > 2) {
            if (class3.isInstance(expNode)) {
                expNode = (BRExpNode) parm3;
                parm3 = expNode.evaluate();
                class3 = parm3.getClass();
            } else if (class3.isInstance(functNode)) {
                functNode = (BRFunctNode) parm3;
                parm3 = functNode.evaluate();
                class3 = parm3.getClass();
            }
        }

        if (nParmCount == 2) {
            if (class1.isInstance(strObj) && class2.isInstance(intObj)) {
                srcStr = (String) parm1;
                int length = ((Integer) parm2).intValue();
                return (new String(srcStr.substring(0, length)));
            } else {
                throw new ParseException(
                        "substr(srcStr, length) parameter type mismatch", 0);
            }
        } else {
            if (class1.isInstance(strObj) && class2.isInstance(intObj)
                    && class3.isInstance(intObj)) {
                srcStr = (String) parm1;
                int start = ((Integer) parm2).intValue();
                int length = ((Integer) parm3).intValue();
                return (new String(srcStr.substring(start, start + length)));
            } else {
                throw new ParseException(
                        "substr(srcStr, offset, length) parameter type mismatch",
                        0);
            }
        }
    }

    // yml: 2000-08-23
    // 2002-06-13: add Array() and Array(size) support
    // 2002-06-13: add addItem() support
    // 2002-07-04: add isLeapYear & getDaysInMonth() support
    protected Object evaluate() throws ParseException {

        substituteUserVars();

        switch (functName.charAt(0)) {
        case 'f':
            if (functName.equalsIgnoreCase("find")) {
                return find();
            } else if (functName.equalsIgnoreCase("fetchColumn")) {
                return fetchColumn();
            } else if (functName.equalsIgnoreCase("fetchRows")) {
                return fetchRows();
            } else if (functName.equalsIgnoreCase("fetchRow")) {
                return fetchRow();
            } else if (functName.equalsIgnoreCase("fetchField")) {
                return fetchField();
            } else if (functName.equalsIgnoreCase("fetch")) {
                return fetch();
            } else if (functName.equalsIgnoreCase("floor")) {
                return floor();
            }
            break;
        case 'g':
            if (functName.equalsIgnoreCase("getItem")) {
                return getItem();
            } else if (functName.equalsIgnoreCase("getMetaData")) {
                return getMetaData();
            } else if (functName.equalsIgnoreCase("getColumnCount")) {
                return getColumnCount();
            } else if (functName.equalsIgnoreCase("getColumnName")) {
                return getColumnName();
            } else if (functName.equalsIgnoreCase("getColumnType")) {
                return getColumnType();
            } else if (functName.equalsIgnoreCase("getColumnPrecision")) {
                return getColumnPrecision();
            } else if (functName.equalsIgnoreCase("getColumnScale")) {
                return getColumnScale();
            } else if (functName.equalsIgnoreCase("getDayOfYear")) {
                return getDayOfYear();
            } else if (functName.equalsIgnoreCase("getYear")) {
                return getYear();
            } else if (functName.equalsIgnoreCase("getMonth")) {
                return getMonth();
            } else if (functName.equalsIgnoreCase("getDay")) {
                return getDay();
            } else if (functName.equalsIgnoreCase("getDayOfWeek")) {
                return getDayOfWeek();
            } else if (functName.equalsIgnoreCase("getDaysInMonth")) {
                return getDaysInMonth();
            }else if (functName.equalsIgnoreCase("getWFTrigger")) {
                return getWFTrigger();
            }
            break;
        case 'd':
            if (functName.equalsIgnoreCase("deleteItem")) {
                return deleteItem();
            }
            //--> Added debug(msg) function to print debug messages in the log
            // file
            //--> By Billy 31Aug2004
            else if (functName.equalsIgnoreCase("debug")) {
                return printDebugMsg();
            }

            //=====================================================================
            break;
        case 'e':
            if (functName.equalsIgnoreCase("exist")) {
                return exist();
            } else if (functName.equalsIgnoreCase("existInDeal")) {
                return existInDeal();
            } else if (functName.equalsIgnoreCase("execSQL")) {
                return execSQL();
            } else if (functName.equalsIgnoreCase("exp")) {
                return exp();
            }
            break;
        case 'i':
            if (functName.equalsIgnoreCase("isEmpty")) {
                return isEmpty();
            } else if (functName.equalsIgnoreCase("isTrue")) {
                return isTrue();
            } else if (functName.equalsIgnoreCase("indexOf")) {
                return indexOf();
            } else if (functName.equalsIgnoreCase("isValidSIN")) {
                return isValidSIN();
            } else if (functName.equalsIgnoreCase("insertItem")) {
                return insertItem();
            } else if (functName.equalsIgnoreCase("isLeapYear")) {
                return isLeapYear();
            }else if (functName.equalsIgnoreCase("isWFTrigger")) {
                return isWFTrigger();
            }else if (functName.equalsIgnoreCase("isDigit")) {
                return isDigit();
            }else if (functName.equalsIgnoreCase("isNumeric")) {
                return isNumeric();
            }
            break;
        case '_':
            if (functName.equalsIgnoreCase("_and")) {
                return and();
            } else if (functName.equalsIgnoreCase("_or")) {
                return or();
            } else if (functName.equalsIgnoreCase("_xor")) {
                return xor();
            }
            break;
        case 'c':
            if (functName.equalsIgnoreCase("count")) {
                return count();
            } else if (functName.equalsIgnoreCase("ceil")) {
                return ceil();
            } else if (functName.equalsIgnoreCase("cos")) {
                return cos();
            }
            break;
        case 's':
            if (functName.equalsIgnoreCase("sum")) {
                return sum();
            } else if (functName.equalsIgnoreCase("sizeOf")) {
                return sizeOf();
            } else if (functName.equalsIgnoreCase("sqrt")) {
                return sqrt();
            } else if (functName.equalsIgnoreCase("sin")) {
                return sin();
            } else if (functName.equalsIgnoreCase("substr")) {
                return substr();
            } else if (functName.equalsIgnoreCase("strlen")) {
                return strlen();
            } else if (functName.equalsIgnoreCase("setItem")) {
                return setItem();
            }else if (functName.equalsIgnoreCase("setWFTrigger")) {
                return setWFTrigger();
            }
            break;
        case 'a':
            if (functName.equalsIgnoreCase("abs")) {
                return abs();
            } else if (functName.equalsIgnoreCase("acos")) {
                return acos();
            } else if (functName.equalsIgnoreCase("asin")) {
                return asin();
            } else if (functName.equalsIgnoreCase("atan")) {
                return atan();
            } else if (functName.equalsIgnoreCase("array")) {
                return array();
            } else if (functName.equalsIgnoreCase("addItem")) {
                return addItem();
            }
            break;
        case 'm':
            if (functName.equalsIgnoreCase("min")) {
                return min();
            } else if (functName.equalsIgnoreCase("max")) {
                return max();
            }
            break;
        case 't':
            if (functName.equalsIgnoreCase("toDate")) {
                return toDate();
            } else if (functName.equalsIgnoreCase("tan")) {
                return tan();
            } else if (functName.equalsIgnoreCase("tosqlstr")) {
                return tosqlstr();
            }
            break;
        case 'n':
            if (functName.equalsIgnoreCase("now")) {
                return now();
            }
            break;
        case 'p':
            if (functName.equalsIgnoreCase("pow")) {
                return pow();
            }
            break;
        case 'l':
            if (functName.equalsIgnoreCase("log")) {
                return log();
            }
            break;
        case 'r':
            if (functName.equalsIgnoreCase("round")) {
                return round();
            } else if (functName.equalsIgnoreCase("random")) {
                return random();
            }
            break;
        }

        errorMsg = "Invalid function <" + functName + "> specified";
        throw new ParseException(errorMsg, 0);
    }

    private Object xor() throws ParseException {
        int i, j, size;
        boolean bStatus[] = new boolean[2];
        Boolean boolObj;
        Object parmObj;
        Object parmObj1, parmObj2;
        Class classObj;
        BRVarNode varNode = new BRVarNode(interpretor);
        BRExpNode expNode = new BRExpNode(interpretor);
        BRExpNode[] expNodes;

        if (nParmCount != 2) {
            errorMsg = "Invalid parameter count, 2 is epected!";
            throw new ParseException(errorMsg, 0);
        }

        parmObj1 = parameters.get(0);
        parmObj2 = parameters.get(1);

        for (i = 0; i < 2; ++i) {
            parmObj = parameters.get(i);
            classObj = parmObj.getClass();

            // evaluare expression or array variable
            if (classObj.isInstance(expNode)) {
                expNode = (BRExpNode) parmObj;

                // check if full array was specified
                if (expNode.hasArrayIterator()) {
                    // expands the expNode
                    expNodes = expNode.expandsArrayIterator();
                    size = Array.getLength(expNodes);
                    for (j = 0; j < size; ++j) {
                        boolObj = (Boolean) expNodes[j].evaluate();
                        bStatus[i] = boolObj.booleanValue();
                    }
                } else {
                    boolObj = (Boolean) (expNode.evaluate());
                    bStatus[i] = boolObj.booleanValue();
                }
            } else if (classObj.isInstance(varNode)) {
                boolObj = (Boolean) ((BRVarNode) parmObj).evaluate();
                bStatus[i] = boolObj.booleanValue();
            } else {
                throw new ParseException("_xor(x, y) parameter type mismatch",
                        0);
            }
        }

        if (bStatus[0] == bStatus[1]) {
            return new Boolean(false);
        }
        return new Boolean(true);
    }

    private Object evaluateParm(Object parmObj) throws ParseException {
        String strObj = "";
        Integer intObj = new Integer(0);
        Object varObj;
        Class classObj;
        BRExpNode expObj = new BRExpNode(interpretor);
        BRFunctNode functObj = new BRFunctNode(interpretor);
        BRVarNode varNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);

        varObj = parmObj;
        classObj = varObj.getClass();

        if (classObj.isInstance(strObj) && BRUtil.isInteger((String) varObj)) {
            varObj = BRUtil.mapInt((String) varObj);
        } else if (classObj.isInstance(expObj)) {
            varObj = ((BRExpNode) varObj).evaluate();
        } else if (classObj.isInstance(functObj)) {
            varObj = ((BRFunctNode) varObj).evaluate();
        } else if (classObj.isInstance(varNode)) {
            varObj = ((BRVarNode) varObj).evaluate();
        } else if (classObj.isInstance(usrVarNode)) {
            varObj = ((VarNode) varObj).evaluate();
        } else if (classObj.isInstance(intObj)) { // do nothing
        } else {
            String msg = "Invalid toDate parameter: " + classObj.getName()
                    + " integer expected!";
            throw new ParseException(msg, 0);
        }

        return varObj;
    }

    private Object toDate() throws ParseException {
        int nYear, nMonth, nDay;
        Integer intObj;

        // evaluate the year parameter
        intObj = (Integer) evaluateParm(parameters.get(0));
        nYear = intObj.intValue();

        // evaluate the year parameter
        intObj = (Integer) evaluateParm(parameters.get(1));
        nMonth = intObj.intValue();
        --nMonth;

        // evaluate the year parameter
        intObj = (Integer) evaluateParm(parameters.get(2));
        nDay = intObj.intValue();

        Calendar calObj = Calendar.getInstance();
        calObj.clear();
        calObj.set(nYear, nMonth, nDay, 0, 0, 0);
        long mSeconds = (calObj.getTime()).getTime();
        Date dateObj = new Date(mSeconds);
        return dateObj;
    }

    //--> Function to make a String with single quotes safe for sql statements
    // by doubling up on
    //     the single quote.<br> i.e. e'xample becomes e''xample
    // tosqlstr(srcStr)
    // for example:
    //   -- if srcStr = "e'xample"; tosqlstr(srcStr) ==> "e''xample"
    // By Billy 08Sept2003
    private Object tosqlstr() throws ParseException {
        Object parmObj;
        String strObj = new String();
        Class classObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);

        parmObj = parameters.get(0);
        classObj = parmObj.getClass();

        // evaluate expression or function node if present
        if (classObj.isInstance(expNode)) {
            expNode = (BRExpNode) parmObj;
            parmObj = expNode.evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(functNode)) {
            functNode = (BRFunctNode) parmObj;
            parmObj = functNode.evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(cachedVarNode)) {
            parmObj = (Object) ((BRVarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(usrVarNode)) {
            parmObj = (Object) ((VarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        }

        if (classObj.isInstance(strObj)) {
            strObj = (String) parmObj;
            return (makeQuoteSafe(strObj));
        } else {
            throw new ParseException(
                    "tosqlstr(srcStr) parameter type mismatch", 0);
        }
    }

    //--> This method is copied from com.basis100.deal.util.StringUtil
    private String makeQuoteSafe(String in) {
        if (in == null || in.length() == 0) {
            return in;
        }
        char quote = '\'';
        StringBuffer outbuf = new StringBuffer();

        char c;
        for (int i = 0; i < in.length(); i++) {
            c = in.charAt(i);
            outbuf.append(c);
            if (c == quote) {
                outbuf.append(quote);
            }
        }
        return outbuf.toString();
    }

    //===========================================================================

    //--> Added debug(msg) function to print debug messages in the log file
    //--> By Billy 31Aug2004
    /**
     * Function to print the debug message to the log file
     *
     * @throws ParseException
     * @return Object
     */
    private Object printDebugMsg() throws ParseException {
        Object parmObj;
        String strObj = new String();
        Class classObj;
        BRExpNode expNode = new BRExpNode(interpretor);
        BRVarNode cachedVarNode = new BRVarNode(interpretor);
        VarNode usrVarNode = new VarNode(interpretor);
        BRFunctNode functNode = new BRFunctNode(interpretor);

        parmObj = parameters.get(0);
        classObj = parmObj.getClass();

        // evaluate expression or function node if present
        if (classObj.isInstance(expNode)) {
            expNode = (BRExpNode) parmObj;
            parmObj = expNode.evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(functNode)) {
            functNode = (BRFunctNode) parmObj;
            parmObj = functNode.evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(cachedVarNode)) {
            parmObj = (Object) ((BRVarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        } else if (classObj.isInstance(usrVarNode)) {
            parmObj = (Object) ((VarNode) parmObj).evaluate();
            classObj = parmObj.getClass();
        }
        //--> Added more protections :: By Billy 09Nov2004
        else if (classObj == null) {
             parmObj = "null";
             classObj = parmObj.getClass();
        }
        //================================================

        if (classObj.isInstance(strObj)) {
            strObj = (String) parmObj;
            BRUtil.info("BR Debug ==> " + strObj);
            return (strObj);
        } else {
            BRUtil
                    .wran("Problem @printDebugMsg : debug(msg) parameter type mismatch !!");
            throw new ParseException("debug(msg) parameter type mismatch", 0);
        }
    }

    /**
     * getWFTrigger
     *
     * --> Cervus II : Added new method getWFTrigger(triggerKey) (AME spec. section 8.2.2)
     * --> By Mike 26Oct2004
     *
     * @return obj java.lang.Object
     * @throws ParseException
     */
    private Object getWFTrigger() throws ParseException {
        Object param = parameters.get(0);
        String sqlStmt = null;
        String strObj = new String();


        //the parameters may contain complex types, they must be taken into consideration.
        param = evaluateVar(param);

        if (param.getClass().isInstance(strObj)) {

            sqlStmt = "select value from WorkflowTrigger where dealId = "
                    + dealObj.dealId + " AND workflowTriggerKey='" + ((String) param).trim() + "'";
        } else {

            BRUtil
                    .wran("Problem @getWFTrigger : getWFTrigger(key) parameter type mismatch !!");
            throw new ParseException("getWFTrigger(key) parameter type mismatch", 0);
        }
        Object obj = interpretor.fetchDBField(sqlStmt);        
        //--> Billy ==> Mike : should use fetchDBField instead as we expect only one result
        //--> 09Nov2004
        return obj;        
                 
        //==========================================================================
    }

    /**
     * isWFTrigger
     *
     * --> Cervus II : Added new method isWFTrigger(triggerKey) (AME spec. section 8.2.2)
     * --> By Mike 26Oct2004
     *
     * @return obj java.lang.Object
     * @throws ParseException
     */
    private Object isWFTrigger() throws ParseException {

        String keyValue = (String) getWFTrigger();
        //Object row = getWFTrigger();

        //--> Billy ==> Mike : The following should be adjusted as we changed to use fetchDBField above
        //--> 09Nov2004
        //Sql_Column[] col = interpretor.getMetaData(row);      
        //keyValue = (String) col[0].colValue;
        
        return (Object) new Boolean("Y".equalsIgnoreCase(keyValue) ? true
                : false);
    }

    /**
     * setWFTrigger
     *
     * --> Cervus II : Added new method setWFTrigger(triggerKey) (AME spec. section 8.2.2)
     * --> By Mike 26Oct2004
     *
     * @return Object
     * @throws ParseException
     * @throws SQLException
     */
    private Object setWFTrigger() throws ParseException {

        Object triggerKeyParam = parameters.get(0);
        Object valueKeyParam = parameters.get(1);
        String sqlStmt = null;
        String strObj = new String();

        triggerKeyParam = evaluateVar(triggerKeyParam);
        valueKeyParam = evaluateVar(valueKeyParam);

        if (triggerKeyParam.getClass().isInstance(strObj) && valueKeyParam.getClass().isInstance(strObj)) {
          //--> Billy ==> Mike : Need to check if record already exist, if not it should be an insert instead
          //--> 09Nov2004
          if(getWFTrigger() == null)
          {
              // change for ML
            sqlStmt = "insert into workflowTrigger (dealId, workflowtriggerkey, value, institutionProfileId) " +
                    "values (" + dealObj.dealId + ", '" + ((String)triggerKeyParam).trim() + "', '"
                    + ((String)valueKeyParam).trim() + "', " + dealObj.institutionProfileId + ")";
          }
          else
          {
            sqlStmt = "update workflowTrigger set value = '" + ((String)valueKeyParam).trim() + "' "
            		+ " where dealId = " + dealObj.dealId + " and workflowtriggerkey = '" + ((String)triggerKeyParam).trim() + "'";
          }
          //===============================================================================

        } else {

            BRUtil.wran("Problem @setWFTrigger : setWFTrigger(key, value) parameter type mismatch !!");
            throw new ParseException("setWFTrigger(key, value) parameter type mismatch", 0);
        }

              return interpretor.execSQL(sqlStmt);
    }



    //=========================================================

}
