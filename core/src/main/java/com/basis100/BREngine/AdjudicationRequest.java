/*
 * AdjudicationRequest.java 
 * 
 * Created: 14-MAR-2006
 * Author:  Alfredo Servilla
 */
package com.basis100.BREngine;

import java.sql.SQLException;

public class AdjudicationRequest extends DealEntity {
	
	protected int   requestId;
	protected int   scenarioNumber;
	protected int	noOfApplicants;

	/**
	 * Creates a new entity
	 * @param interpretor
	 * @throws SQLException
	 */
	public AdjudicationRequest(BRInterpretor interpretor) throws SQLException {
		super(interpretor);
	}

	/**
	 * Clone this AdjudicationRequest
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */
	public AdjudicationRequest deepCopy() throws CloneNotSupportedException {
		return (AdjudicationRequest) this.clone();		
	}
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
	protected String getPrimaryKeyName() {
		return "requestId";
	}
	
	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
	protected String getDefaultSQL() {
		return "SELECT * FROM AdjudicationRequest";
	}
	
	/**
	 * Closes the DB connection.
	 * @throws SQLException If an error occurs.
	 */
	protected void close() throws SQLException {
		super.close();
	}
	
	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
	protected boolean next() throws SQLException { 
		boolean bStatus = super.next();
		if (bStatus) {
			requestId = resultSet.getInt("requestId");
			scenarioNumber = resultSet.getInt("scenarioNumber");
			noOfApplicants = resultSet.getInt("noOfApplicants");
		}
		
		return bStatus;
	}
}
