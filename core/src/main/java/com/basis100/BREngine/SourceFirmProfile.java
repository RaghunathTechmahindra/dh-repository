/**
 * Title:        MOS Core Development
 * Description:  dynamic bind instance for SourceFirmProfile Entity
 * Copyright:    Copyright (c) 2002
 * Company:      Basis100
 * @author Billy Lam
 * @version
 */

package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class SourceFirmProfile
  extends DealEntity
{
  protected String sourceFirmName;
  protected int systemTypeId;
  protected int sourceFirmProfileId;
  protected int contactId;
  protected int profileStatusId;
  protected String sfShortName;
  protected String sfBusinessId;
  protected String alternativeId;
  protected int sfMOProfileId;
  //-- FXLink Phase II --//
  // New additional field -- By Billy 27Jan2004
  protected String sourceFirmCode;
  protected String sfLicenseRegistrationNumber;

  //============================================

  public Contact contact;

  protected String getPrimaryKeyName()
  {
    return "sourceFirmProfileId";
  }

  public SourceFirmProfile(BRInterpretor interpretor, boolean template) throws SQLException
  {
    super(interpretor);
  }

  public SourceFirmProfile(BRInterpretor interpretor) throws SQLException
  {
    super(interpretor);
    filter = null;

    if(Open())
    {
      next();
    }
  }

  public SourceFirmProfile(BRInterpretor interpretor, int anId) throws SQLException
  {
    super(interpretor);
    filter = getPrimaryKeyName() + "=" + anId;

    if(Open())
    {
      next();
    }
  }

  protected String getDefaultSQL()
  {
    return "select * from SourceFirmProfile";
  }

  protected void close() throws SQLException
  {
    if(contact != null)
    {
      contact.close();

    }
    super.close();
  }

  protected boolean next() throws SQLException
  {
    boolean bStatus;
    String filter;

    bStatus = super.next();
    if(bStatus)
    {

      sourceFirmName = getString(resultSet.getString("sourceFirmName"));
      systemTypeId = resultSet.getInt("systemTypeId");
      sourceFirmProfileId = resultSet.getInt("sourceFirmProfileId");
      contactId = resultSet.getInt("contactId");
      profileStatusId = resultSet.getInt("profileStatusId");
      sfShortName = getString(resultSet.getString("sfShortName"));
      sfBusinessId = getString(resultSet.getString("sfBusinessId"));
      alternativeId = getString(resultSet.getString("alternativeId"));
      sfMOProfileId = resultSet.getInt("sfMOProfileId");
      // New additional field -- By Billy 27Jan2004
      sourceFirmCode = getString(resultSet.getString("SOURCEFIRMCODE"));
      sfLicenseRegistrationNumber = resultSet.getString("SFLICENSEREGISTRATIONNUMBER");

      //===========================================

      // fetch contact record
      if(contact != null)
      {
        contact.close();
        //=========================================================
        // Contact has a copy ID field but sourceofbusinessprofile
        // does not. So we look for contactId with copy Id 1.
        // ========================================================
      }
      filter = "contactId=" + contactId + " and copyId = 1";
      contact = new Contact(interpretor, filter);
      contact.close();

    }
    return bStatus;
  }
}
