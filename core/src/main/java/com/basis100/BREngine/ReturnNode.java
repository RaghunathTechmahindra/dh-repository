// Business Rule Expression Scanner, add array
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class ReturnNode implements Cloneable
{
  private BRInterpretor interpretor;

  Object stmtObj = null;

  protected String errorMsg;

  public ReturnNode(BRInterpretor interpretor)
  {
    this.interpretor = interpretor;
    stmtObj = null;
  }

  public ReturnNode deepCopy() throws ParseException
  {
    ReturnNode returnNode;

    try
    {
      returnNode = (ReturnNode)this.clone();
    }
    catch(CloneNotSupportedException e)
    {
      errorMsg = "CloneNotSupportedException caught!";
      throw new ParseException(errorMsg, 0);
    }
    return returnNode;
  }

  public void display()
  {
  }

  public Object evaluate() throws ParseException, SQLException
  {
    Object result = null;
    Class classObj;
    Boolean boolObj = new Boolean(false);
    BRExpNode expObj = new BRExpNode(interpretor);
    BRVarNode varObj = new BRVarNode(interpretor);
    BRFunctNode functObj = new BRFunctNode(interpretor);
    BRAssignNode assignObj = new BRAssignNode(interpretor);
    IfNode ifNode = new IfNode(interpretor);

    classObj = stmtObj.getClass();

    //--> Debug enhancement : by Billy 24Aug2004
    if(classObj.isInstance(expObj))
    {
      result = ((BRExpNode)stmtObj).evaluate();
    }
    else if(classObj.isInstance(varObj))
    {
      result = ((BRVarNode)stmtObj).evaluate();
    }
    else if(classObj.isInstance(functObj))
    {
      result = ((BRFunctNode)stmtObj).evaluate();
    }
    else if(classObj.isInstance(assignObj))
    {
      result = ((BRAssignNode)stmtObj).evaluate();
    }
    else if(classObj.isInstance(ifNode))
    {
      result = ((IfNode)stmtObj).evaluate();
    }
    else if(classObj.isInstance(boolObj))
    {
      result = stmtObj;
    }
    else
    {
      errorMsg = "Invalid node object <" + stmtObj + " detected";
      //--> Debug enhancement : by Billy 24Aug2004
      BRUtil.wran("Problem @DoNode.evaluate : " + errorMsg);
      //==========================================
      throw new ParseException(errorMsg, 0);
    }

    BRUtil.debug("@ReturnNode.evaluate result : " + result);
    return result;
  }

  public boolean parseExpressionPhase2() throws ParseException
  {
    Class classObj;
    String strObj = "";
    BRExpNode expObj = new BRExpNode(interpretor);
    BRAssignNode assignNode = new BRAssignNode(interpretor);

    classObj = stmtObj.getClass();
    if(classObj.isInstance(expObj))
    {
      ((BRExpNode)stmtObj).parseExpressionPhase2();
    }
    else if(classObj.isInstance(assignNode))
    {
      ((BRAssignNode)stmtObj).parseExpressionPhase2();
    }
    else if(classObj.isInstance(strObj))
    {
      strObj = (String)stmtObj;
      if(BRUtil.isBoolean(strObj))
      {
        stmtObj = BRUtil.mapBoolean(strObj);
      }
    }

    return true;
  }

  protected void substituteUserVars() throws ParseException
  {
    Class classObj;
    String strObj = "";
    BRExpNode expNode = new BRExpNode(interpretor);
    BRFunctNode functNode = new BRFunctNode(interpretor);
    BRAssignNode assignNode = new BRAssignNode(interpretor);
    IfNode ifNode = new IfNode(interpretor);
    ReturnNode returnNode = new ReturnNode(interpretor);
    ForNode forNode = new ForNode(interpretor);

    if(stmtObj == null)
    {
      return;
    }

    classObj = stmtObj.getClass();
    if(classObj.isInstance(expNode))
    {
      ((BRExpNode)stmtObj).substituteUserVars();
    }
    else if(classObj.isInstance(functNode))
    {
      ((BRFunctNode)stmtObj).substituteUserVars();
    }
    else if(classObj.isInstance(assignNode))
    {
      ((BRAssignNode)stmtObj).substituteUserVars();
    }
    else if(classObj.isInstance(ifNode))
    {
      ((IfNode)stmtObj).substituteUserVars();
    }
    else if(classObj.isInstance(returnNode))
    {
      ((ReturnNode)stmtObj).substituteUserVars();
    }
    else if(classObj.isInstance(forNode))
    {
      ((ForNode)stmtObj).substituteUserVars();
    }
    else if(classObj.isInstance(strObj))
    {
      strObj = (String)stmtObj;
      stmtObj = (String)interpretor.substituteUserVars(strObj,
        BRConstants.NO_DELIMITER);
    }
  }
}
