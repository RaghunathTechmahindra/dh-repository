// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Liability extends DealEntity {
	protected  	int 	liabilityId ;
	protected  	int 	liabilityTypeId ;
	protected  	int 	borrowerId ;
	protected  	double 	liabilityAmount ;
	protected  	double 	liabilityMonthlyPayment ;
	protected  	String 	includeInGDS ;
	protected  	String 	includeInTDS ;
	protected  	String 	liabilityDescription ;
	protected  	int 	liabilityPayoffTypeId ;
	protected  	int 	percentInGDS ;
	protected  	int 	percentInTDS ;
	protected  	String 	liabilityPaymentQualifier ;
	protected  	int 	percentOutGDS ;
	protected  	int 	percentOutTDS ;
	protected  	int 	copyId ;

//   public LiabilityType liabilityType;
	
	//FFATE
	protected   double  creditLimit;
	protected   Date    maturityDate;
	protected   String  creditBureauRecordIndicator;
	
   protected String getPrimaryKeyName() {
      return "liabilityId";
   }

   public Liability(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public Liability(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public Liability(BRInterpretor interpretor, String whereClause) throws SQLException {
      super(interpretor);
      filter = whereClause;

      if (Open())
         next();
   }


   protected Liability deepCopy() throws CloneNotSupportedException {
      return (Liability)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from Liability";
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				liabilityId  = resultSet.getInt("LIABILITYID");
				liabilityTypeId  = resultSet.getInt("LIABILITYTYPEID");
				borrowerId  = resultSet.getInt("BORROWERID");
				liabilityAmount  = resultSet.getDouble("LIABILITYAMOUNT");
				liabilityMonthlyPayment  = resultSet.getDouble("LIABILITYMONTHLYPAYMENT");
				includeInGDS  = getString(resultSet.getString("INCLUDEINGDS"));
				if (includeInGDS != null )
					includeInGDS  =  includeInGDS.trim();
				includeInTDS  = getString(resultSet.getString("INCLUDEINTDS"));
				if (includeInTDS != null )
					includeInTDS  =  includeInTDS.trim();
				liabilityDescription  = getString(resultSet.getString("LIABILITYDESCRIPTION"));
				if (liabilityDescription != null )
					liabilityDescription  =  liabilityDescription.trim();
				liabilityPayoffTypeId  = resultSet.getInt("LIABILITYPAYOFFTYPEID");
				percentInGDS  = resultSet.getInt("PERCENTINGDS");
				percentInTDS  = resultSet.getInt("PERCENTINTDS");
				liabilityPaymentQualifier  = getString(resultSet.getString("LIABILITYPAYMENTQUALIFIER"));
				if (liabilityPaymentQualifier != null )
					liabilityPaymentQualifier  =  liabilityPaymentQualifier.trim();
				percentOutGDS  = resultSet.getInt("PERCENTOUTGDS");
				percentOutTDS  = resultSet.getInt("PERCENTOUTTDS");
				copyId  = resultSet.getInt("COPYID");
				
				//FFATE
				creditLimit = resultSet.getDouble("CREDITLIMIT");
				maturityDate = resultSet.getDate("MATURITYDATE");
				creditBureauRecordIndicator = resultSet.getString("CREDITBUREAURECORDINDICATOR");
				
/*
        if (liabilityType != null)
           liabilityType.close();
        liabilityType = new LiabilityType(interpretor, liabilityTypeId);
        liabilityType.close();
*/
/*================================================
         liabilityId = resultSet.getInt(1);
         liabilityTypeId = resultSet.getShort(2);
         borrowerId = resultSet.getInt(3);
         liabilityAmount = resultSet.getDouble(3);
         liabilityMonthlyPayment = resultSet.getDouble(4);
         includeIngDS = getString(resultSet.getString(5);
         includeIntDS = getString(resultSet.getString(6);
=============================*/
         }
      return bStatus;
   }

}
