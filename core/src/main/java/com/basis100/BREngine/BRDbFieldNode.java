// Business Rule Expression Scanner, add array
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class BRDbFieldNode implements Cloneable {
   private BRInterpretor interpretor;
   protected String    owner = null;
   protected String    table = null;
   protected String    column = null;
   protected String    filter = null;
   protected Stack     keyValues;
   protected Deal     dealObj = null;
   protected SQLDA[]  sqldas = null;

   protected String   errorMsg;

   public BRDbFieldNode(BRInterpretor interpretor) {
      this.interpretor = interpretor;
      this.dealObj = null;
   }

// support table fetch as function with key value passed in
//    public BRDbFieldNode(Deal dealObj, BRFunctNode functNode) {
   public BRDbFieldNode(Deal dealObj, BRFunctNode functNode) {
      int     i, nParmCount;

      this.dealObj = dealObj;
      owner = (String)functNode.parameters.get(0);
      table = (String)functNode.parameters.get(1);
      column = (String)functNode.parameters.get(2);

      keyValues = new Stack();
      nParmCount = functNode.nParmCount - 3;
      for (i=0; i<nParmCount; ++i) {
         keyValues.push(functNode.parameters.get(i+3));
         }
   }

   public BRDbFieldNode deepCopy() throws ParseException {
      BRDbFieldNode     fieldNode;

      try {
         fieldNode = (BRDbFieldNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return fieldNode;
   }

   public void display() {
   }

   public Object evaluate(Object valueObj) throws ParseException {
      Object      result = null;
      Statement   updStmt;
      String      sqlStmt;

      try {
         Connection  connObj = interpretor.getConnection();

         // Create a Statement for update
         updStmt = connObj.createStatement();

         sqlStmt = "update ";
         if (owner != null)
            sqlStmt += owner + ".";

         sqlStmt += table + " set " + column + "="
                 + BRUtil.asSQLColumnValue(valueObj) +  " where " + filter;

         updStmt.execute(sqlStmt);
         }
      catch (SQLException ex) {
         throw new ParseException(ex.getMessage(), 0);
         }

      return result;
   }

   private Object parseKeyValue(Object keyObj) throws ParseException {
      int     length;
      Class   classObj;
      String  token, strObj  = "";
      BRExpNode   expNode   = new BRExpNode(interpretor);
      BRFunctNode functNode =  new BRFunctNode(interpretor);
      BRVarNode   varNode   = new BRVarNode(interpretor);

      classObj = keyObj.getClass();
      if (classObj.isInstance(expNode)) {
         if (!((BRExpNode)keyObj).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + keyObj + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(functNode)) {
         if (!((BRFunctNode)keyObj).parseExpressionPhase2()) {
            errorMsg = "Unable to map node: <" + keyObj + ">";
            throw new ParseException(errorMsg, 0);
            }
         }
      else if (classObj.isInstance(varNode)) {

         }
      else if (classObj.isInstance(strObj)) {
         token = (String)keyObj;
         try {
            if (BRUtil.isInteger(token)) {       // integer
               return BRUtil.mapInt(token);
               }
            else if (BRUtil.isFloat(token)) {     // double
               return BRUtil.mapDouble(token);
               }
            else if (BRUtil.isDealEntity(dealObj, token)) {
               return token;
               }
            else {                         // assume to be string
               // now strip the string delimeter
               if (token.charAt(0) == '\"') {
                  length = token.length();
                  token = token.substring(1, length-1);
                  }
               return token;
               }
            }
         catch (Exception e) {
            errorMsg = "Invalid key value: \"" + token + "\" found!";
            throw new ParseException(errorMsg, 0);
            }
         }
      else {
         }

      return null;
   }

   public boolean parseExpressionPhase2() throws ParseException {
//   protected String    filter = null;

      int   i, count, length;
      Integer intObj  = new Integer(0);
      Double  doubleObj = new Double(0.0);
      String  token, strObj = "";
      Boolean boolObj = new Boolean(true);
      Date    dateObj = new Date(0);
      Time    timeObj = new Time(0);
      Timestamp timestampObj = new Timestamp(0);
      Object  keyObj;
      Class   classObj;
      BRExpNode   expNode = new BRExpNode(interpretor);
      BRFunctNode functNode =  new BRFunctNode(interpretor);
      BRVarNode   varNode   = new BRVarNode(interpretor);
      BRDbFieldNode fieldNode = new BRDbFieldNode(interpretor);

      // determine the primary keys of the table
      try {
         sqldas = interpretor.getPrimaryKeys(owner, table);
         }
      catch (SQLException e) {
         errorMsg = "Unable to retrieve primary key inforation for table: <";
         if (owner != null)
            errorMsg += owner + ".";

         errorMsg += table + ">, error: " + e.getMessage();

         throw new ParseException(errorMsg, 0);
         }

      count = keyValues.size();
      if (count != Array.getLength(sqldas)) {
         errorMsg = "Invalid number of primary subkey values specified!";

         throw new ParseException(errorMsg, 0);
         }

      filter = "";
      for (i=0; i<count; ++i) {
         keyObj = keyValues.elementAt(i);
         classObj = keyObj.getClass();

         if (classObj.isInstance(varNode)) {       // do nothing
            }
         else if (classObj.isInstance(fieldNode)) {
            if (!((BRDbFieldNode)keyObj).parseExpressionPhase2()) {
               errorMsg = "Unable to map node: <" + keyObj + ">";
               throw new ParseException(errorMsg, 0);
               }
            }
         else if (classObj.isInstance(expNode)) {
            if (!((BRExpNode)keyObj).parseExpressionPhase2()) {
               errorMsg = "Unable to map node: <" + keyObj + ">";
               throw new ParseException(errorMsg, 0);
               }
            }
         else if (classObj.isInstance(functNode)) {
            if (!((BRFunctNode)keyObj).parseExpressionPhase2()) {
               errorMsg = "Unable to map node: <" + keyObj + ">";
               throw new ParseException(errorMsg, 0);
               }
            }
         else if (classObj.isInstance(varNode)) {
            if (!((BRVarNode)keyObj).parseExpressionPhase2()) {
               errorMsg = "Unable to map node: <" + keyObj + ">";
               throw new ParseException(errorMsg, 0);
               }
            }
         else {
            token = (String)keyObj;
            try {
               if (BRUtil.isInteger(token)) {       // integer
                  keyObj = BRUtil.mapInt(token);
                  }
               else if (BRUtil.isFloat(token))      // double
                  keyObj = BRUtil.mapDouble(token);
               else {                         // string
                  // now strip the string delimeter
                  if (token.charAt(0) == '\"') {
                     length = token.length();
                     token = token.substring(1, length-1);
                     }
                  keyObj = token;
                  }
               }
            catch (Exception e) {
               errorMsg = "Invalid token \"" + token + "\" specified as key: \""
                        + keyObj + "\"";
               throw new ParseException(errorMsg, 0);
               }
            }

         keyValues.set(i, keyObj);
         classObj = keyObj.getClass();
                  
         if (i != 0) filter += " and ";

         filter += sqldas[i].colName + "=";

         // now verify the data types
         switch (sqldas[i].colType) {
            case Types.BIT:
               if (!classObj.isInstance(boolObj)) {
                  errorMsg = "Key value datatype mismatch, boolean expected!";
                  throw new ParseException(errorMsg, 0);
                  }
               filter += keyObj.toString();
               break;
            case Types.SMALLINT:
            case Types.INTEGER:
               if (!classObj.isInstance(intObj)) {
                  errorMsg = "Key value datatype mismatch, integer expected!";
                  throw new ParseException(errorMsg, 0);
                  }
               filter += keyObj.toString();
               break;
            case Types.NUMERIC:
               if (sqldas[i].colScale == 0) {
                  if (!classObj.isInstance(intObj)) {
                     errorMsg = "Key value datatype mismatch, integer expected!";
                     throw new ParseException(errorMsg, 0);
                     }
                  }
               else {
                  if (!classObj.isInstance(doubleObj)) {
                     errorMsg = "Key value datatype mismatch, double expected!";
                     throw new ParseException(errorMsg, 0);
                     }
                  }
                filter += keyObj.toString();
                break;
            case Types.FLOAT:
            case Types.REAL:
            case Types.DECIMAL:
            case Types.DOUBLE:
               if (!classObj.isInstance(doubleObj)) {
                  errorMsg = "Key value datatype mismatch, double expected!";
                  throw new ParseException(errorMsg, 0);
                  }
               filter += keyObj.toString();
               break;
            case Types.CHAR:
            case Types.VARCHAR:
            case Types.LONGVARCHAR:
               if (!classObj.isInstance(strObj)) {
                  errorMsg = "Key value datatype mismatch, string expected!";
                  throw new ParseException(errorMsg, 0);
                  }
               filter += "\'" + keyObj.toString() + "\'";
               break;
            case Types.DATE:
               if (!classObj.isInstance(dateObj)) {
                  errorMsg = "Key value datatype mismatch, ISO date string expected!";
                  throw new ParseException(errorMsg, 0);
                  }
               filter += "To_Date(\'" + keyObj + "\', \'YYYY-MM-DD\')";
               break;
            case Types.TIME:
               if (!classObj.isInstance(timeObj)) {
                  errorMsg = "Key value datatype mismatch, ISO time string expected!";
                  throw new ParseException(errorMsg, 0);
                  }

               break;
            case Types.TIMESTAMP:
               if (!classObj.isInstance(timestampObj)) {
                  errorMsg = "Key value datatype mismatch, ISO timestamp string expected!";
                  throw new ParseException(errorMsg, 0);
                  }
               break;
            }
         }

      return true;

/*
      int         length;
      char        aChar;
      Class       classObj;
      String      token, errorMsg;



*/
   }
}
