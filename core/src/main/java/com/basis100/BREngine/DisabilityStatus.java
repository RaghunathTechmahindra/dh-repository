/*
 * by Neil on Dec/01/2004
 */
package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * @see com.basis100.BREngine.DealEntity
 * @author Neil Kong on Dec/01/2004
 */
public class DisabilityStatus extends DealEntity
{
    private int disabilityStatusId;
    private String disabilityStatusDescription;
    private String disabilityStatusValue;
    private static final String sql = "SELECT * FROM DISABILITYSTATUS ";
    private static final String primaryKeyName = "disabilityStatusId";

    /**
     * @param interpretor
     * @throws SQLException
     */
    public DisabilityStatus(BRInterpretor interpretor) throws SQLException
    {
        this(interpretor, null);
    }

    /**
     * 
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public DisabilityStatus(BRInterpretor interpretor, String filter)
            throws SQLException
    {
        super(interpretor);
        this.filter = filter;
        if (Open())
        {
            next();
        }
    }

    /**
     * 
     * @return
     * @throws CloneNotSupportedException
     */
    public DisabilityStatus deepCopy() throws CloneNotSupportedException
    {
        try
        {
            return (DisabilityStatus) this.clone();
        }
        catch (CloneNotSupportedException cnse)
        {
            String errMsg = "@DisabilityStatus.deepCopy() : "
                    + cnse.getMessage();
            throw new CloneNotSupportedException(errMsg);
        }
    }

    /**
     * @return
     */
    public String getDefaultSQL()
    {
        return sql;
    }

    /**
     * @return
     */
    public String getPrimaryKeyName()
    {
        return primaryKeyName;
    }

    /**
     * @return
     * @throws SQLException
     */
    public boolean next() throws SQLException
    {
        boolean bStatus = super.next();
        if (bStatus)
        {
            disabilityStatusDescription = resultSet
                    .getString("DISABILITYSTATUSDESC");
            disabilityStatusId = resultSet.getInt("DISABILITYSTATUSID");
            disabilityStatusValue = resultSet
                    .getString("DISABILITYSTATUSVALUE");
        }
        return false;
    }

    /**
     * @return Returns the disabilityStatusDescription.
     */
    public String getDisabilityStatusDescription()
    {
        return disabilityStatusDescription;
    }

    /**
     * @return Returns the disabilityStatusId.
     */
    public int getDisabilityStatusId()
    {
        return disabilityStatusId;
    }

    /**
     * @return Returns the disabilityStatusValue.
     */
    public String getDisabilityStatusValue()
    {
        return disabilityStatusValue;
    }
}