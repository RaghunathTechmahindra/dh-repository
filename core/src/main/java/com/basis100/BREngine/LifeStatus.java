/*
 * by Neil on Dec/01/2004
 */
package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * @see com.basis100.BREngine.DealEntity
 * @author Neil Kong on Dec/01/2004
 */
public class LifeStatus extends DealEntity
{
    private int lifeStatusId;
    private String lifeStatusDescription;
    private String lifeStatusValue;
    private static final String sql = "SELECT * FROM LIFESTATUS ";
    private static final String primaryKeyName = "lifeStatusId";

    /**
     * @param interpretor
     * @throws SQLException
     */
    public LifeStatus(BRInterpretor interpretor) throws SQLException
    {
        this(interpretor, null);
    }

    /**
     * 
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public LifeStatus(BRInterpretor interpretor, String filter)
            throws SQLException
    {
        super(interpretor);
        this.filter = filter;
        if (Open())
        {
            next();
        }
    }

    /**
     * 
     * @return
     * @throws CloneNotSupportedException
     */
    public LifeStatus deepCopy() throws CloneNotSupportedException
    {
        try
        {
            return (LifeStatus) this.clone();
        }
        catch (CloneNotSupportedException cnse)
        {
            String errMsg = "@LifeStatus.deepCopy() : " + cnse.getMessage();
            throw new CloneNotSupportedException(errMsg);
        }
    }

    /**
     * @return
     */
    public String getDefaultSQL()
    {
        return sql;
    }

    /**
     * @return
     */
    public String getPrimaryKeyName()
    {
        return primaryKeyName;
    }

    /**
     * @return
     * @throws SQLException
     */
    public boolean next() throws SQLException
    {
        boolean bStatus = super.next();
        if (bStatus)
        {
            lifeStatusDescription = resultSet.getString("LIFESTATUSDESC");
            lifeStatusId = resultSet.getInt("LIFESTATUSID");
            lifeStatusValue = resultSet.getString("LIFESTATUSVALUE");
        }
        return false;
    }

    /**
     * @return Returns the lifeStatusDescription.
     */
    public String getLifeStatusDescription()
    {
        return lifeStatusDescription;
    }

    /**
     * @return Returns the lifeStatusId.
     */
    public int getLifeStatusId()
    {
        return lifeStatusId;
    }

    /**
     * @return Returns the lifeStatusValue.
     */
    public String getLifeStatusValue()
    {
        return lifeStatusValue;
    }
}