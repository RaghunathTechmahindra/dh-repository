/*
 * Request.java 
 * 
 * Created: 20-FEB-2006
 * Author:  Edward Steel
 */
package com.basis100.BREngine;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author ESteel
 */
public class Request extends DealEntity {
	
	protected int    requestId;
	
	protected Date   requestDate;
	protected String statusMessage;
	protected Date   statusDate;
	
	protected int    userProfileId;
	protected int    dealId;
	protected int    copyId;
	protected int    serviceProductId;
	protected int    channelId;
	protected int    payloadTypeId;
	protected int    serviceTransactionTypeId;
	protected int    requestStatusId;

	/**
	 * Creates a new entity
	 * @param interpretor
	 * @throws SQLException
	 */
	public Request(BRInterpretor interpretor) throws SQLException {
		super(interpretor);
	}

	/**
	 * Clone this Request
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */
	public Request deepCopy() throws CloneNotSupportedException {
		return (Request) this.clone();		
	}
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
	protected String getPrimaryKeyName() {
		return "";
	}
	
	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
	protected String getDefaultSQL() {
		return "SELECT * FROM REQUEST";
	}
	
	/**
	 * Closes the DB connection.
	 * @throws SQLException If an error occurs.
	 */
	protected void close() throws SQLException {
		super.close();
	}
	
	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
	protected boolean next() throws SQLException { 
		boolean bStatus = super.next();
		
		if (bStatus) {
			requestId = resultSet.getInt("REQUESTID");
      requestDate = resultSet.getDate("REQUESTDATE");
      statusMessage = resultSet.getString("STATUSMESSAGE");
      statusDate = resultSet.getDate("STATUSDATE");
			userProfileId = resultSet.getInt("USERPROFILEID");
			dealId = resultSet.getInt("DEALID");
			copyId = resultSet.getInt("COPYID");
			serviceProductId = resultSet.getInt("SERVICEPRODUCTID");
			channelId = resultSet.getInt("CHANNELID");
			payloadTypeId = resultSet.getInt("PAYLOADTYPEID");
			serviceTransactionTypeId = resultSet.getInt("SERVICETRANSACTIONTYPEID");
			requestStatusId = resultSet.getInt("REQUESTSTATUSID");
		}
		
		return bStatus;
	}

//----- START getters/ setters -------------------------------------------------
	/**
	 * @return Returns the channelId.
	 */
	public int getChannelId() {
		return channelId;
	}

	/**
	 * @return Returns the copyId.
	 */
	public int getCopyId() {
		return copyId;
	}

	/**
	 * @return Returns the dealId.
	 */
	public int getDealId() {
		return dealId;
	}

	/**
	 * @return Returns the payloadTypeId.
	 */
	public int getPayloadTypeId() {
		return payloadTypeId;
	}

	/**
	 * @return Returns the requestDate.
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * @return Returns the requestId.
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @return Returns the requestStatusId.
	 */
	public int getRequestStatusId() {
		return requestStatusId;
	}

	/**
	 * @return Returns the serviceProductId.
	 */
	public int getServiceProductId() {
		return serviceProductId;
	}

	/**
	 * @return Returns the statusDate.
	 */
	public Date getStatusDate() {
		return statusDate;
	}

	/**
	 * @return Returns the statusMessage.
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @return Returns the transactionTypeId.
	 */
	public int getServiceTransactionTypeId() {
		return serviceTransactionTypeId;
	}

	/**
	 * @return Returns the userProfileId.
	 */
	public int getUserProfileId() {
		return userProfileId;
	}

	/**
	 * @param channelId The channelId to set.
	 */
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	/**
	 * @param copyId The copyId to set.
	 */
	public void setCopyId(int copyId) {
		this.copyId = copyId;
	}

	/**
	 * @param dealId The dealId to set.
	 */
	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	/**
	 * @param payloadTypeId The payloadTypeId to set.
	 */
	public void setPayloadTypeId(int payloadTypeId) {
		this.payloadTypeId = payloadTypeId;
	}

	/**
	 * @param requestDate The requestDate to set.
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * @param requestId The requestId to set.
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * @param requestStatusId The requestStatusId to set.
	 */
	public void setRequestStatusId(int requestStatusId) {
		this.requestStatusId = requestStatusId;
	}

	/**
	 * @param serviceProductId The serviceProductId to set.
	 */
	public void setServiceProductId(int serviceProductId) {
		this.serviceProductId = serviceProductId;
	}

	/**
	 * @param statusDate The statusDate to set.
	 */
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	/**
	 * @param statusMessage The statusMessage to set.
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * @param transactionTypeId The transactionTypeId to set.
	 */
	public void setServiceTransactionTypeId(int transactionTypeId) {
		this.serviceTransactionTypeId = transactionTypeId;
	}

	/**
	 * @param userProfileId The userProfileId to set.
	 */
	public void setUserProfileId(int userProfileId) {
		this.userProfileId = userProfileId;
	}		
  

  
//------ END getters/ setters --------------------------------------------------
}
