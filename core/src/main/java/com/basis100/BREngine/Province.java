// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Province extends DealEntity {
	protected  	int 	provinceId ;
	protected  	String 	provinceName ;
	protected  	String 	provinceAbbreviation ;
	protected  	double 	provinceTaxRate ;

/*
   public short    provinceId;
   public String   provinceName;
   public String   provinceAbbreviation;
*/
   protected boolean bProvinceId = false;
   protected boolean bProvinceName = false;
   protected boolean bProvinceAbbreviation = false;

   public String getPrimaryKeyName() {
      return "provinceId";
   }

   public Province(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public Province(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public Province(BRInterpretor interpretor, int anId) throws SQLException {
      super(interpretor);
      filter = getPrimaryKeyName() + "=" + anId;

      if (Open())
         bEmpty = !next();
   }

   public Province deepCopy() throws CloneNotSupportedException {
      return (Province)this.clone();
   }

   public String getDefaultSQL() {
      return "select * from province";
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				provinceId  = resultSet.getInt("PROVINCEID");
				provinceName  = getString(resultSet.getString("PROVINCENAME"));
				if (provinceName != null )
					provinceName  =  provinceName.trim();
				provinceAbbreviation  = getString(resultSet.getString("PROVINCEABBREVIATION"));
				if (provinceAbbreviation != null )
					provinceAbbreviation  =  provinceAbbreviation.trim();
				provinceTaxRate  = resultSet.getDouble("PROVINCETAXRATE");
      
/*
         provinceId = resultSet.getShort("PROVINCEID");
         provinceName = resultSet.getString ("PROVINCENAME");
         if (provinceName != null)
            provinceName = provinceName.trim();

         provinceAbbreviation = resultSet.getString ("PROVINCEABBREVIATION");
         if (provinceAbbreviation != null)
            provinceAbbreviation = provinceAbbreviation.trim();
*/
         }

      return bStatus;
   }

   public void setProvinceId(short id) {
      provinceId = id;
      bProvinceId = true;
   }
   public void setProvinceName(String aName) {
      provinceName = aName;
      bProvinceName = true;
   }
   public void setProvinceAbbreviation(String aName) {
      provinceAbbreviation = aName;
      bProvinceAbbreviation = true;
   }

   // needed because Oracle does not support JDBC2.0
   public void update() throws SQLException {
      String    updStmt="";
      String    sqlStmt = "update Province set ";

      if (bEmpty) return;

      if (bProvinceName) {
         bProvinceName = false;
         updStmt = "provinceName=\'" + provinceName + "\'";
         }
      if (bProvinceAbbreviation) {
         if (updStmt.length() > 0)
            updStmt += ",";
         bProvinceAbbreviation = false;
         updStmt += "provinceAbbreviation=\'" + provinceAbbreviation + "\'";
         }

      if (updStmt.length() > 0) {
         sqlStmt  += updStmt + " where provinceId=" + provinceId;
         if (execStmt == null)
            execStmt = interpretor.connObj.createStatement ();

         execStmt.execute(sqlStmt);
         }
   }

   // needed because Oracle does not support JDBC2.0
   public void insert() throws SQLException {
      String    sqlStmt="insert into Province values(";

      if (!bProvinceId) return;

      bProvinceId = false;
      sqlStmt += provinceId + ",";

      if (bProvinceName)
         sqlStmt += "\'" + provinceName + "\',";
      else
         sqlStmt += "null,";
      bProvinceName = false;

      if (bProvinceAbbreviation)
         sqlStmt += "\'" + provinceAbbreviation + "\')";
      else
         sqlStmt += "null)";
      bProvinceAbbreviation = false;

      if (execStmt == null)
         execStmt = interpretor.connObj.createStatement ();

      execStmt.execute(sqlStmt);
   }

   // needed because Oracle does not support JDBC2.0
   public void delete() throws SQLException {
      String    sqlStmt = "delete from Province where provinceId=";

      if (bEmpty) return;

      sqlStmt  +=  provinceId;
      if (execStmt == null)
         execStmt = interpretor.connObj.createStatement ();

      execStmt.execute(sqlStmt);
   }
}
