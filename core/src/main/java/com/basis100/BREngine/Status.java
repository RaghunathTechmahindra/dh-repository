// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Status extends DealEntity {
        protected       short     statusId;
        protected       String    statusDescription;
        protected       short     statusCategoryId;

   protected String getPrimaryKeyName() {
      return "statusId";
   }

   public Status(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }
   
   public Status(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }
   public Status(BRInterpretor interpretor, int anId) throws SQLException {
      super(interpretor);
      filter = getPrimaryKeyName() + "=" + anId;

      if (Open())
         next();
   }

   public Status(BRInterpretor interpretor, String whereClause) throws SQLException {
      super(interpretor);
      filter = whereClause;

      if (Open())
         next();
   }

   protected Status deepCopy() throws CloneNotSupportedException {
      return (Status)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from Status";
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
         statusId  = resultSet.getShort("STATUSID");

         statusDescription = resultSet.getString("STATUSDESCRIPTION");
         if (statusDescription == null)
            statusDescription = "";
         else
            statusDescription = statusDescription.trim();

         statusCategoryId = resultSet.getShort("STATUSCATEGORYID");
        }

      return bStatus;
   }

}
