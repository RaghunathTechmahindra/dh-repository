// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class IncomeType extends DealEntity {
	protected  	int 	incomeTypeId ;
	protected  	String 	ITDescription ;
	protected  	String 	employmentRelated ;
	protected  	int 	GDSInclusion ;
	protected  	int 	TDSInclusion ;

  protected String getPrimaryKeyName() {
      return "incomeTypeId";
   }

   public IncomeType(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }
   
   public IncomeType(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public IncomeType(BRInterpretor interpretor, int anId) throws SQLException {
      super(interpretor);
      filter = getPrimaryKeyName() + "=" + anId;

      if (Open())
         next();
   }

   public IncomeType deepCopy() throws CloneNotSupportedException {
      return (IncomeType)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from IncomeType";
   }

   public boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				incomeTypeId  = resultSet.getInt("INCOMETYPEID");
				ITDescription  = getString(resultSet.getString("ITDESCRIPTION"));
				employmentRelated  = getString(resultSet.getString("EMPLOYMENTRELATED"));
				if (employmentRelated != null )
					employmentRelated  =  employmentRelated.trim();
				GDSInclusion  = resultSet.getInt("GDSINCLUSION");
				TDSInclusion  = resultSet.getInt("TDSINCLUSION");
        }

      return bStatus;
   }
}
