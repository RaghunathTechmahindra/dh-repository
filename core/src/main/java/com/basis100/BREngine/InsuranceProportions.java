/*
 * by Neil on Dec/01/2004
 */
package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * @see com.basis100.BREngine.DealEntity
 * @author Neil Kong on Dec/01/2004
 */
public class InsuranceProportions extends DealEntity
{
    private int insuranceProportionsId;
    private String insuranceProportionsDescription;
    private static final String sql = "SELECT * FROM INSURANCEPROPORTIONS ";
    private static final String primaryKeyName = "insuranceProportionsId";

    /**
     * @param interpretor
     * @throws SQLException
     */
    public InsuranceProportions(BRInterpretor interpretor) throws SQLException
    {
        this(interpretor, null);
    }

    /**
     * 
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public InsuranceProportions(BRInterpretor interpretor, String filter)
            throws SQLException
    {
        super(interpretor);
        this.filter = filter;
        if (Open())
        {
            next();
        }
    }

    /**
     * 
     * @return
     * @throws CloneNotSupportedException
     */
    public InsuranceProportions deepCopy() throws CloneNotSupportedException
    {
        try
        {
            return (InsuranceProportions) this.clone();
        }
        catch (CloneNotSupportedException cnse)
        {
            String errMsg = "@InsuranceProportions.deepCopy() : "
                    + cnse.getMessage();
            throw new CloneNotSupportedException(errMsg);
        }
    }

    /**
     * @return
     */
    public String getDefaultSQL()
    {
        return sql;
    }

    /**
     * @return
     */
    public String getPrimaryKeyName()
    {
        return primaryKeyName;
    }

    /**
     * @return
     * @throws SQLException
     */
    public boolean next() throws SQLException
    {
        boolean bStatus = super.next();
        if (bStatus)
        {
            insuranceProportionsDescription = resultSet.getString("IPDESC");
            insuranceProportionsId = resultSet.getInt("INSURANCEPROPORTIONSID");
        }
        return false;
    }

    /**
     * @return Returns the insuranceProportionsDescription.
     */
    public String getInsuranceProportionsDescription()
    {
        return insuranceProportionsDescription;
    }

    /**
     * @return Returns the insuranceProportionsId.
     */
    public int getInsuranceProportionsId()
    {
        return insuranceProportionsId;
    }
}