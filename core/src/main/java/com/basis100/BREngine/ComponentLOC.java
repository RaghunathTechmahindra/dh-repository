package com.basis100.BREngine;

import java.sql.Date;
import java.sql.SQLException;

/**
 * <p>Title: ComponentLOC</p>
 * <p>Description: BR Entity Class map to ComponentLOC </p>
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 8, 2008)
 */
public class ComponentLOC extends DealEntity {

    protected int componentId;
    protected int copyId;
    protected String propertyTaxAllocateFlag;
    protected String existingAccountIndicator;
    protected String existingAccountNumber;
    protected Date firstpaymentDate;
    protected double advanceHold;
    protected String commissionCode;
    protected double discount;
    protected Date interestAdjustmentDate;
    protected double locAmount;
    protected String miAllocateFlag;
    protected double netInterestRate;
    protected double pAndIPaymentAmount;
    protected double pAndIPaymentAmountMonthly;
    protected int paymentFrequencyId;
    protected double premium;
    protected int prepaymentOptionsId;
    protected int privilegePaymentId;
    protected double propertyTaxEscrowAmount;
    protected double totalLocAmount;
    protected double totalPaymentAmount;

    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param bTemplate boolean
     * @throws SQLException
     */
    public ComponentLOC(BRInterpretor interpretor, boolean bTemplate) 
        throws SQLException {
        super(interpretor);
        if(bTemplate) {
        }
    }

    /**
     * <p>Constructor</P>
     * @param interpretor BRInterpretor
     * @throws SQLException
     */    
    public ComponentLOC(BRInterpretor interpretor) throws SQLException {
        super(interpretor);
        filter = null;
    
        if(Open()) {
            next();
        }
    }
    
    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param anObj Object
     * @throws SQLException
     */
    public ComponentLOC(BRInterpretor interpretor, Object anObj) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + ((Component)anObj).componentId + " and copyId = " + ((Component)anObj).copyId;
    
        if(Open()) {
            next();
        }
    }

    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param componentId int
     * @param copyId int
     * @throws SQLException
     */
    public ComponentLOC(BRInterpretor interpretor, int componentId, int copyId) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + componentId + " and copyId = " + copyId;
    
        if(Open()) {
            next();
        }
    }
    
    /**
     * <p>Constractor</p>
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public ComponentLOC(BRInterpretor interpretor, String filter) 
        throws SQLException {
    
        super(interpretor);
        this.filter = filter;
    
        if(Open()) {
            next();
        }
    }

    /**
     * <p>getPrimaryKeyName</p>
     * <p>Description:get first primary key name </p>
     * @return String
     */
    protected String getPrimaryKeyName()  {
        return "componentid";
    }

    /**
     * <p>deepCopy</p>
     * @return ComponentLOC
     */
    public ComponentLOC deepCopy() throws CloneNotSupportedException {
      return(ComponentLOC)this.clone();
    }

    /**
     * <p>getDefaultSQL</p>
     * @return String
     */
    protected String getDefaultSQL() {
      return "select * from ComponentLOC";
    }

    /**
     * <p>next</p>
     * <p>Description: retrieve data from DB and set entity properties
     * @return boolean:true if data is exist, false if not
     */
    public boolean next() throws SQLException {
        
        boolean bStatus = super.next();
        if (!bStatus) return bStatus;

        componentId = resultSet.getInt("COMPONENTID");
        copyId = resultSet.getInt("COPYID");
        propertyTaxAllocateFlag = resultSet.getString("PROPERTYTAXALLOCATEFLAG");
        existingAccountIndicator = resultSet.getString("EXISTINGACCOUNTINDICATOR");
        existingAccountNumber = resultSet.getString("EXISTINGACCOUNTNUMBER");
        firstpaymentDate = resultSet.getDate("FIRSTPAYMENTDATE");
        advanceHold = resultSet.getDouble("ADVANCEHOLD");
        commissionCode = resultSet.getString("COMMISSIONCODE");
        discount = resultSet.getDouble("DISCOUNT");
        interestAdjustmentDate = resultSet.getDate("INTERESTADJUSTMENTDATE");
        locAmount = resultSet.getDouble("LOCAMOUNT");
        miAllocateFlag = resultSet.getString("MIALLOCATEFLAG");
        netInterestRate = resultSet.getDouble("NETINTERESTRATE");
        pAndIPaymentAmount = resultSet.getDouble("PANDIPAYMENTAMOUNT");
        pAndIPaymentAmountMonthly = resultSet.getDouble("PANDIPAYMENTAMOUNTMONTHLY");
        paymentFrequencyId = resultSet.getInt("COPYID");
        premium = resultSet.getDouble("PREMIUM");
        prepaymentOptionsId = resultSet.getInt("PREPAYMENTOPTIONSID");
        privilegePaymentId = resultSet.getInt("PRIVILEGEPAYMENTID");
        propertyTaxEscrowAmount = resultSet.getDouble("PROPERTYTAXESCROWAMOUNT");
        totalLocAmount = resultSet.getDouble("TOTALLOCAMOUNT");
        totalPaymentAmount = resultSet.getDouble("TOTALPAYMENTAMOUNT");

        return bStatus;
    }
}