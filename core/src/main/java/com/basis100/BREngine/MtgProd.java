// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class MtgProd extends DealEntity {
	protected  	int 	interestTypeId ;
	protected  	int 	mtgProdId ;
	protected  	String 	mtgProdName ;
	protected  	double 	minimumAmount ;
	protected  	double 	maximumAmount ;
	protected  	double 	minimumLTV ;
	protected  	double 	maximumLTV ;
	protected  	int 	paymentTermId ;
	protected  	int 	pricingProfileId ;
	protected  	int 	lineOfBusinessId ;
	protected  	String 	commitmentTerm ;
	protected  	String 	mpShortName ;
	protected  	String 	mpBusinessId ;
	protected  	int 	prepaymentOptionsId ;
	protected  	int 	privilegePaymentId ;
  //--Ticket#1736--18July2005--start--//
	//protected  	double 	teaserDiscount ;
	//protected  	int 	teaserTerm ;
  //--Ticket#1736--18July2005--end--//
	//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//	
	protected	int  rateGuaranteePeriod;
	protected	int  rateFloatDownTypeId;
	//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//

    //***** Change by NBC/PP Implementation Team - Version 1.0 - Start *****//  
	protected double maxCashBackAmount;
	protected double maxCashBackPercentage;
	protected int minimumAmortization;
	protected int maximumAmortization;
	protected int affiliationProgramId ;
	protected int sourceOfBusinessCategoryId;
    //***** Change by NBC/PP Implementation Team - Version 1.0 - End *****//  
    
    //Qualify Rate
    protected String qualifyingRateEligibleFlag;

  public PricingProfile pricingProfile;

   protected String getPrimaryKeyName() {
      return "mtgProdId";
   }

   public MtgProd(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public MtgProd(BRInterpretor interpretor, int anId) throws SQLException {
      super(interpretor);
      filter = getPrimaryKeyName() + "=" + anId;

      if (Open())
         next();
   }

   protected String getDefaultSQL() {
      return "select * from MtgProd";
   }

   /**
    * next
    * 
    * @param none <br>
    * @return boolean : true, if there is a row to retrieve <br>
    * @version 1.1 <br>
    * Date: 06/27/2006 <br>
    * Author: NBC/PP Implementation Team <br>
    * Change: <br>
    * 	Added call to set rateGuaranteePeriod and rateFloatDownTypeId attributes
    *
    */
   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				interestTypeId  = resultSet.getInt("INTERESTTYPEID");
				mtgProdId  = resultSet.getInt("MTGPRODID");
				mtgProdName  = getString(resultSet.getString("MTGPRODNAME"));
				if (mtgProdName != null )
					mtgProdName  =  mtgProdName.trim();
				minimumAmount  = resultSet.getDouble("MINIMUMAMOUNT");
				maximumAmount  = resultSet.getDouble("MAXIMUMAMOUNT");
				minimumLTV  = resultSet.getDouble("MINIMUMLTV");
				maximumLTV  = resultSet.getDouble("MAXIMUMLTV");
				paymentTermId  = resultSet.getInt("PAYMENTTERMID");
				pricingProfileId  = resultSet.getInt("PRICINGPROFILEID");
				lineOfBusinessId  = resultSet.getInt("LINEOFBUSINESSID");
				commitmentTerm  = getString(resultSet.getString("COMMITMENTTERM"));
				if (commitmentTerm != null )
					commitmentTerm  =  commitmentTerm.trim();
				mpShortName  = getString(resultSet.getString("MPSHORTNAME"));
				if (mpShortName != null )
					mpShortName  =  mpShortName.trim();
				mpBusinessId  = getString(resultSet.getString("MPBUSINESSID"));
				if (mpBusinessId != null )
					mpBusinessId  =  mpBusinessId.trim();
				prepaymentOptionsId  = resultSet.getInt("PREPAYMENTOPTIONSID");
				privilegePaymentId  = resultSet.getInt("PRIVILEGEPAYMENTID");
        //--Ticket#1736--18July2005--start--//
				//teaserDiscount  = resultSet.getDouble("TEASERDISCOUNT");
				//teaserTerm  = resultSet.getInt("TEASERTERM");
        //--Ticket#1736--18July2005--end--//

				//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//	
				rateGuaranteePeriod  = resultSet.getInt("RATEGUARANTEEPERIOD");
				rateFloatDownTypeId  = resultSet.getInt("RATEFLOATDOWNTYPEID");				
				//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
			
                //***** Change by NBC/PP Implementation Team - Version 1.0 - Start *****//  
                maxCashBackAmount = resultSet.getDouble("MAXCASHBACKAMOUNT");
                maxCashBackPercentage = resultSet.getDouble("MAXCASHBACKPERCENTAGE");
                minimumAmortization = resultSet.getInt("MINIMUMAMORTIZATION");
                maximumAmortization = resultSet.getInt("MAXIMUMAMORTIZATION");
                affiliationProgramId  = resultSet.getInt("AFFILIATIONPROGRAMID");  //remove this
                sourceOfBusinessCategoryId = resultSet.getInt("SOURCEOFBUSINESSCATEGORYID");
                //***** Change by NBC/PP Implementation Team - Version 1.0 - End *****//  
                
                //Qualify Rate
                qualifyingRateEligibleFlag = resultSet.getString("QUALIFYINGRATEELIGIBLEFLAG");
				
         // fetch PricingProfile record
         if (pricingProfile != null)
            pricingProfile.close();
         pricingProfile = new PricingProfile(interpretor, pricingProfileId);
         pricingProfile.close();

/*============================
         mtgProdId = resultSet.getShort(1);
         interestTypeId = resultSet.getShort(2);

         mtgProdName        = getString(resultSet.getString(3);
         if (mtgProdName != null)
            mtgProdName = mtgProdName.trim();

         minimumAmount = resultSet.getDouble(4);
         maximumAmount = resultSet.getDouble(5);
         minimumLTV = resultSet.getDouble(6);
         maximumLTV = resultSet.getDouble(7);
         paymentTermId = resultSet.getShort(8);
         pricingProfileId = resultSet.getShort(9);
         lineOfBusinessId  = resultSet.getShort(10);
         commitmentTerm  = getString(resultSet.getString(11);
         mpShortName  = getString(resultSet.getString(12);
         mpBusinessId  = getString(resultSet.getString(13);

         // fetch InterestType record
         if (interestType != null)
            interestType.close();
         interestType = new InterestType(interestTypeId);

         // fetch PaymentTerm record
         if (paymentTerm != null)
            paymentTerm.close();
         paymentTerm = new PaymentTerm(paymentTermId);

         // fetch LineOfBusiness record
         if (lineOfBusiness != null)
            lineOfBusiness.close();
         lineOfBusiness = new LineOfBusiness(lineOfBusinessId);
         lineOfBusiness.close();
===============================================*/
         }
      return bStatus;
   }
}
