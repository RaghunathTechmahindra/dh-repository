/*
 * by Neil at Nov/30/2004
 */
package com.basis100.BREngine;

import java.sql.SQLException;
import java.util.Date;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;

import com.basis100.log.SysLog;


/**
 * @see com.basis100.BREngine.DealEntity
 * @author Neil Kong at Nov/30/2004
 */
public class InsureOnlyApplicant extends DealEntity
{
    //--Vlad variables should be protected.
    protected int insureOnlyApplicantId;
    protected int copyId;
    protected int dealId;
    protected String insureOnlyApplicantFirstName;
    protected String insureOnlyApplicantLastName;
    protected Date insureOnlyApplicantBirthdate;
    protected String primaryInsureOnlyApplicantFlag = "N";
    protected int insureOnlyApplicantGenderId;
    protected int insuranceProportionsId;
    protected double lifePercentCoverageReq;
    protected int smokeStatusId;
    protected int disabilityStatusId;
    protected int lifeStatusId;
    protected double disPercentCoverageReq;
    //---------- foreign objects begins ----------//
    //--Vlad: no need to implement them
    /**
    protected InsuranceProportions insuranceProportions;
    protected BorrowerGender borrowerGender;
    protected DisabilityStatus disabilityStatus;
    protected LifeStatus lifeStatus;
    protected SmokeStatus smokeStatus;
    **/
    //---------- foreign objects ends ----------//
    protected String sqlWhereClause = null;

    protected int nLifeDisPremiumsIOnlyA;
    protected LifeDisPremiumsIOnlyA[] lifeDisPremiumsIOnlyA;

    /**
     *
     * @param interpretor
     * @throws SQLException
     */
    public InsureOnlyApplicant(BRInterpretor interpretor) throws SQLException
    {
        this(interpretor, null);
    }

    /**
     *
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public InsureOnlyApplicant(BRInterpretor interpretor, String filter)
            throws SQLException
    {
        super(interpretor);
        this.filter = filter;
        this.sqlWhereClause = "INSUREONLYAPPLICANTID = "
                + this.insuranceProportionsId + " AND COPYID = " + this.copyId;
        if (Open())
        {
            next();
        }
    }

    protected String getPrimaryKeyName()
    {
      return "insureOnlyApplicantId";
    }

    public InsureOnlyApplicant(BRInterpretor interpretor, boolean bTemplate) throws SQLException
    {
      super(interpretor);

      nLifeDisPremiumsIOnlyA = 0;
      lifeDisPremiumsIOnlyA = new LifeDisPremiumsIOnlyA[0];

    }

    /**
    public InsureOnlyApplicant(BRInterpretor interpretor) throws SQLException
    {
      super(interpretor);
      filter = null;
      if(Open())
      {
        next();
      }
    }

    public InsureOnlyApplicant(BRInterpretor interpretor, String filter) throws SQLException
    {
      super(interpretor);
      this.filter = filter;

      if(Open())
      {
        next();
      }
    }
    **/
    /**
     *
     * @return
     * @throws CloneNotSupportedException
     */
    public InsureOnlyApplicant deepCopy() throws CloneNotSupportedException
    {
        try
        {
            return (InsureOnlyApplicant) this.clone();
        }
        catch (CloneNotSupportedException cnse)
        {
            String errMsg = "@InsureOnlyApplicant.deepCopy() : "
                    + cnse.getMessage();
            throw new CloneNotSupportedException(errMsg);
        }
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    /**
    private InsuranceProportions fetchInsuranceProportions()
            throws SQLException
    {
        try
        {
            insuranceProportions = new InsuranceProportions(interpretor,
                    sqlWhereClause);
            // if not matches, destory the insuranceProportions.
            if (insuranceProportions.getInsuranceProportionsId() != this.insuranceProportionsId)
            {
                insuranceProportions.close();
                insuranceProportions = null;
            }
            return insuranceProportions;
        }
        catch (SQLException sqle)
        {
            String errMsg = "@InsureOnlyApplicant.fetchInsuranceProportions() : "
                    + sqle.getMessage();
            throw new SQLException(errMsg);
        }
    }
    **/

    /**
     *
     * @return
     * @throws SQLException
     */
    /**
    private DisabilityStatus fetchDisabilityStatus() throws SQLException
    {
        try
        {
            disabilityStatus = new DisabilityStatus(interpretor, sqlWhereClause);
            // if not matches, destory the insuranceProportions.
            if (disabilityStatus.getDisabilityStatusId() != this.disabilityStatusId)
            {
                disabilityStatus.close();
                disabilityStatus = null;
            }
            return disabilityStatus;
        }
        catch (SQLException sqle)
        {
            String errMsg = "@InsureOnlyApplicant.fetchDisabilityStatus() : "
                    + sqle.getMessage();
            throw new SQLException(errMsg);
        }
    }
    **/
    /**
     *
     * @return
     * @throws SQLException
     */
    /**
    private LifeStatus fetchLifeStatus() throws SQLException
    {
        try
        {
            lifeStatus = new LifeStatus(interpretor, sqlWhereClause);
            // if not matches, destory the insuranceProportions.
            if (lifeStatus.getLifeStatusId() != this.lifeStatusId)
            {
                lifeStatus.close();
                lifeStatus = null;
            }
            return lifeStatus;
        }
        catch (SQLException sqle)
        {
            String errMsg = "@InsureOnlyApplicant.fetchLifeStatus() : "
                    + sqle.getMessage();
            throw new SQLException(errMsg);
        }
    }
    **/

    /**
     *
     * @return
     * @throws SQLException
     */
    /**
    private SmokeStatus fetchSmokeStatus() throws SQLException
    {
        try
        {
            smokeStatus = new SmokeStatus(interpretor, sqlWhereClause);
            // if not matches, destory the insuranceProportions.
            if (smokeStatus.getSmokeStatusId() != this.smokeStatusId)
            {
                smokeStatus.close();
                smokeStatus = null;
            }
            return smokeStatus;
        }
        catch (SQLException sqle)
        {
            String errMsg = "@InsureOnlyApplicant.fetchSmokeStatus() : "
                    + sqle.getMessage();
            throw new SQLException(errMsg);
        }
    }
    **/
    /**
     * Vlad update
     * @param fieldName
     * @return
     * @throws SQLException,
     */

    public Object fetchField(String fieldName) throws SQLException, CloneNotSupportedException
    {
        BRUtil.debug("@InsureOnlyApplicant.fetchField() : "
                + "Instantiating object = " + fieldName);
        /**
        // fetch objects
        if (fieldName.equalsIgnoreCase("INSURANCEPROPORTIONS"))
        {
            insuranceProportions = fetchInsuranceProportions();
            return (Object) insuranceProportions;
        }
        else if (fieldName.equalsIgnoreCase("DISABILITYSTATUS"))
        {
            disabilityStatus = fetchDisabilityStatus();
            return (Object) disabilityStatus;
        }
        else if (fieldName.equalsIgnoreCase("LIFESTATUS"))
        {
            lifeStatus = fetchLifeStatus();
            return (Object) lifeStatus;
        }
        else if (fieldName.equalsIgnoreCase("SMOKESTATUS"))
        {
            smokeStatus = fetchSmokeStatus();
            return (Object) smokeStatus;
        }
        **/
       //--DJ_CR136--start--//
       if (fieldName.compareToIgnoreCase("LifeDisPremiumsIOnlyA") == 0) {
         lifeDisPremiumsIOnlyA = FetchLifeDisPremiumsIOnlyA();
         return (Object) lifeDisPremiumsIOnlyA;
       }
       else
       {
         return null;
       }
       //--DJ_CR136--end--//
    }

    private LifeDisPremiumsIOnlyA[] FetchLifeDisPremiumsIOnlyA() throws SQLException, CloneNotSupportedException
    {
      int i = 0;
      LifeDisPremiumsIOnlyA ldpioa = null;
      LifeDisPremiumsIOnlyA[] ldpioas = null;
      Stack aStack = new Stack();

      try
      {
        //String   whereClause = "borrowerId=" + borrowerId;
        String whereClause = "insureOnlyApplicantId=" + insureOnlyApplicantId + " and copyId = " + copyId;
        ldpioa = new LifeDisPremiumsIOnlyA(interpretor, whereClause);
        if(ldpioa.insureOnlyApplicantId != insureOnlyApplicantId)
        {
          ldpioa.close();
          ldpioa = null;
        }
      }
      catch(SQLException e)
      {
        BRUtil.error("Exception @InsureOnlyApplicant.FetchLifeDisabilityPremiums: " + e);
        throw e;
      }

      if(ldpioa != null)
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(ldpioa.deepCopy());

        while(ldpioa.next())
        {
          // make a deep copy of the object and then inserted into the list.
          aStack.push(ldpioa.deepCopy());
        }
        ldpioa.close();
      }

      nLifeDisPremiumsIOnlyA = aStack.size();
      ldpioas = new LifeDisPremiumsIOnlyA[nLifeDisPremiumsIOnlyA];

      for(i = 0; i < nLifeDisPremiumsIOnlyA; ++i)
      {
        ldpioas[i] = (LifeDisPremiumsIOnlyA)aStack.get(i);
      }

      return ldpioas;
    }

    /**
     * @return boolean
     * @exception SQLException
     */
    public boolean next() throws SQLException
    {
        boolean bStatus = super.next();
        try
        {
            if (bStatus)
            {
                //---------- set value to class variables begins ----------//
                insureOnlyApplicantId = resultSet
                        .getInt("INSUREONLYAPPLICANTID");
                copyId = resultSet.getInt("COPYID");
                dealId = resultSet.getInt("DEALID");
                insureOnlyApplicantFirstName = resultSet.getString(
                        "INSUREONLYAPPLICANTFIRSTNAME").trim();
                insureOnlyApplicantLastName = resultSet.getString(
                        "INSUREONLYAPPLICANTLASTNAME").trim();
                insureOnlyApplicantBirthdate = resultSet
                        .getDate("INSUREONLYAPPLICANTBIRTHDATE");
                primaryInsureOnlyApplicantFlag = resultSet
                        .getString("PRIMARYINSUREONLYAPPLICANTFLAG");
                insureOnlyApplicantGenderId = resultSet
                        .getInt("INSUREONLYAPPLICANTGENDERID");
                insuranceProportionsId = resultSet
                        .getInt("INSUREANCEPROPORTIONSID");
                lifePercentCoverageReq = resultSet
                        .getDouble("LIFEPERCENTCOVERAGEREQ");
                smokeStatusId = resultSet.getInt("SMOKESTATUSID");
                disabilityStatusId = resultSet.getInt("DISABILITYSTATUSID");
                lifeStatusId = resultSet.getInt("LIFESTATUSID");
                disPercentCoverageReq = resultSet
                        .getDouble("DISPERCENTCOVERAGEREQ");
                //---------- set value to class variables ends ----------//
                //---------- initialize foreign objects begins ----------//
                /**
                insuranceProportions = fetchInsuranceProportions();
                disabilityStatus = fetchDisabilityStatus();
                lifeStatus = fetchLifeStatus();
                smokeStatus = fetchSmokeStatus();
                **/
                //---------- initialize foreign objects ends ----------//
            }
        }
        catch (SQLException sqle)
        {
            String errMsg = "@InsureOnlyApplicant.next() : "
                    + sqle.getMessage();
            SysLog.error(errMsg);
            SysLog.error(sqle);
        }
        return bStatus;
    }

    /**
     *
     * @param sqlUpdateClause
     * @throws SQLException
     */
    public void update(String sqlUpdateClause) throws SQLException
    {
        String sqlStmt = "UPDATE INSUREONLYAPPLICANT SET " + sqlUpdateClause
                + " WHERE DEALID = " + dealId + " AND INSUREONLYAPPLICANTID = "
                + insureOnlyApplicantId;
        if (execStmt == null)
        {
            execStmt = interpretor.getConnection().createStatement();
            execStmt.execute(sqlStmt);
        }
    }
}
