// dynamic bind instance variables by its name
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class Income extends DealEntity {
	protected  	int 	incomePeriodId ;
	protected  	int 	incomeTypeId ;
	protected  	int 	incomeId ;
	protected  	double 	incomeAmount ;
	protected  	int 	borrowerId ;
	protected  	String 	incIncludeInGDS ;
	protected  	String 	incIncludeInTDS ;
	protected  	int 	incPercentInGDS ;
	protected  	int 	incPercentInTDS ;
	protected  	int 	incPercentOutGDS ;
	protected  	int 	incPercentOutTDS ;
	protected  	String 	incomeDescription ;
	protected  	int 	copyId ;
	protected  	double 	annualIncomeAmount ;
	protected  	double 	monthlyIncomeAmount ;

  protected String getPrimaryKeyName() {
      return "incomeId";
   }

   public Income(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   public Income(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   public Income(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open()) {
         next();
         }
   }

   protected Income deepCopy() throws CloneNotSupportedException {
      return (Income)this.clone();
   }

   protected String getDefaultSQL() {
      return "select * from Income";
   }

   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
				incomePeriodId  = resultSet.getInt("INCOMEPERIODID");
				incomeTypeId  = resultSet.getInt("INCOMETYPEID");
				incomeId  = resultSet.getInt("INCOMEID");
				incomeAmount  = resultSet.getDouble("INCOMEAMOUNT");
				borrowerId  = resultSet.getInt("BORROWERID");
				incIncludeInGDS  = getString(resultSet.getString("INCINCLUDEINGDS"));
				if (incIncludeInGDS != null )
					incIncludeInGDS  =  incIncludeInGDS.trim();
				incIncludeInTDS  = getString(resultSet.getString("INCINCLUDEINTDS"));
				if (incIncludeInTDS != null )
					incIncludeInTDS  =  incIncludeInTDS.trim();
				incPercentInGDS  = resultSet.getInt("INCPERCENTINGDS");
				incPercentInTDS  = resultSet.getInt("INCPERCENTINTDS");
				incPercentOutGDS  = resultSet.getInt("INCPERCENTOUTGDS");
				incPercentOutTDS  = resultSet.getInt("INCPERCENTOUTTDS");
				incomeDescription  = getString(resultSet.getString("INCOMEDESCRIPTION"));
				if (incomeDescription == null )
           incomeDescription = "";
        else
					incomeDescription  =  incomeDescription.trim();
				copyId  = resultSet.getInt("COPYID");
				annualIncomeAmount  = resultSet.getDouble("ANNUALINCOMEAMOUNT");
				monthlyIncomeAmount  = resultSet.getDouble("MONTHLYINCOMEAMOUNT");
        }

      return bStatus;
   }
}
