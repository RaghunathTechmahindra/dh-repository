// You need to import the java.sql package to use JDBC
package com.basis100.BREngine;
import java.sql.Date;
import java.sql.Types;
import java.text.ParseException;

public class RuleResult
{
   public boolean  ruleStatus = false;
   public Object    result  = null;
   public short     opCount = 0;
   public short[]   opType  = new short[8];
   public Object[]  opValue = new Object[8];

   public RuleResult(RuleEntity ruleEntity) {
      opCount = ruleEntity.opCount;
      opType = ruleEntity.opType;
   }

   // 0 based result index
   public boolean getResult() {
      return ((Boolean)result).booleanValue();
   }

   public boolean getBoolean(int index) {
      // ????????????? make sure to check later if user store a string
      return ((Boolean)opValue[index]).booleanValue();
   }
   public int getInt(int index) throws ParseException {
      int       i;

      if (index < 0 || index > opCount)
         throw new ParseException("Invalid column index!", 0);
      i = index - 1;

      switch (opType[i]) {
         case Types.SMALLINT:
            return ((Integer)(opValue[i])).intValue();
         case Types.INTEGER:
            return ((Integer)(opValue[i])).intValue();
         case Types.DOUBLE:
            return (int)((Double)(opValue[i])).doubleValue();
         default:
            throw new ParseException("Rule result data type mismatch!", 0);
         }
   }

   public short getShort(int index) throws ParseException {
      int       i;

      if (index < 0 || index > opCount)
         throw new ParseException("Invalid column index!", 0);
      i = index - 1;

      switch (opType[i]) {
         case Types.SMALLINT:
            return (short)(((Integer)(opValue[i])).intValue());
         case Types.INTEGER:
            return (short)(((Integer)(opValue[i])).intValue());
         case Types.DOUBLE:
            return (short)( ((Double)(opValue[i])).doubleValue() );
         default:
            throw new ParseException("Rule result data type mismatch!", 0);
         }
   }

   public String getString(int index) throws ParseException {
      int    i;

      if (index < 0 || index > opCount)
         throw new ParseException("Invalid column index!", 0);
      i = index - 1;

      switch (opType[i]) {
         case Types.CHAR:
         case Types.VARCHAR:
         case Types.LONGVARCHAR:
            return (String)opValue[i];
         case Types.DATE:
            return BRUtil.asISODate( (Date)opValue[i] );
         default:
            throw new ParseException("Rule result data type mismatch!", 0);
         }
   }
   public double getDouble(int index) throws ParseException {
      int    i;

      if (index < 0 || index > opCount)
         throw new ParseException("Invalid column index!", 0);
      i = index - 1;

      switch (opType[i]) {
         case Types.DOUBLE:
            return ((Double)opValue[i]).doubleValue();
         case Types.INTEGER:
         case Types.SMALLINT:
            return (new Double(((Integer)opValue[i]).intValue())).doubleValue();
         default:
            throw new ParseException("Rule result data type mismatch!", 0);
         }
   }
   public Date getDate(int index) throws ParseException {
      int    i;

      if (index < 0 || index > opCount)
         throw new ParseException("Invalid column index!", 0);
      i = index - 1;

      switch (opType[i]) {
//         case BRConstants.DATATYPE_STR:
//            return BRUtil.toDate((String)opValue[i]);
         case Types.DATE:
            return (Date)opValue[i];
         default:
            throw new ParseException("Rule result data type mismatch!", 0);
         }
   }

}

