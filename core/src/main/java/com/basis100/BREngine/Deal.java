// dynamic bind instance variables by its name
package com.basis100.BREngine;

/**
 * 08/Sep/2005 DVG #DG310 #2052  CV E2E Ingestion Failure
 *     deal.refExistingMtgNumber changed to string
 *
 * @version 1.1
 * Author: MCM Impl Team <br>
 * Change Log:  XS 2.39 <br>
 * 15-JULY-2008
 * added component related fields, FetchComponents method 
 */
import java.sql.Date;
import java.sql.SQLException;
import java.util.Stack;

import com.basis100.log.SysLog;

public class Deal extends DealEntity
{
  protected int dealId;
  protected int copyId;
  protected int sourceOfBusinessProfileId;
  protected String applicationId;
  protected Date applicationDate;
  protected double totalLoanAmount;
  protected int amortizationTerm;
  protected Date estimatedClosingDate;
  protected int dealTypeId;
  protected int lenderProfileId;
  protected String sourceApplicationId;
  protected double totalPurchasePrice;
  protected double postedInterestRate;
  protected int underwriterUserId;
  protected double estimatedPaymentAmount;
  protected double PandIPaymentAmount;
  protected double escrowPaymentAmount;
  protected double MIPremiumAmount;
  protected double netLoanAmount;
  protected Date statusDate;
  protected double discount;
  protected String secondaryFinancing;
  protected String originalMortgageNumber;
  protected int denialReasonId;
  protected int renewalOptionsId;
  protected double netInterestRate;
  protected Date rateDate;
  protected int dealPurposeId;
  protected double buydownRate;
  protected int specialFeatureId;
  protected double interimInterestAmount;
  protected double downPaymentAmount;
  protected int lineOfBusinessId;
  protected int mortgageInsurerId;
  protected int mtgProdId;
  protected String MIPolicyNumber;
  protected Date firstPaymentDate;
  protected double netPaymentAmount;
  protected String MARSNameSearch;
  protected String MARSPropertySearch;
  protected String MILoanNumber;
  protected double commisionAmount;
  protected Date maturityDate;
  protected Date commitmentIssueDate;
  protected Date commitmentAcceptDate;
  //#DG310 protected int refExistingMtgNumber;
  protected String refExistingMtgNumber;
  protected double combinedTotalLiabilities;
  protected String bankABANumber;
  protected String bankAccountNumber;
  protected String bankName;
  protected Date actualClosingDate;
  protected double combinedTotalAssets;
  protected double combinedTotalIncome;
  protected double combinedTDS;
  protected double combinedGDS;
  protected Date renewalDate;
  protected int numberOfBorrowers;
  protected int numberOfGuarantors;
  protected String servicingMortgageNumber;
  protected int paymentTermId;
  protected int interestTypeId;
  protected int paymentFrequencyId;
  protected int prepaymentOptionsId;
  protected int rateFormulaId;
  protected int statusId;
  protected int riskRatingId;
  protected String electronicSystemSource;
  protected Date commitmentExpirationDate;
  protected String requestedSolicitorName;
  protected String requestedAppraisorName;
  protected int interestCompoundId;
  protected int investorProfileId;
  protected int secondApproverId;
  protected int jointApproverId;
  protected int branchProfileId;
  protected int sourceFirmProfileId;
  protected int groupProfileId;
  protected int administratorId;
  protected int pricingProfileId;
  protected int refinanceNewMoney;
  protected Date interimInterestAdjustmentDate;
  protected int privilegePaymentId;
  protected double advanceHold;
  protected String clientNumber;
  protected double combinedLTV;
  protected double combinedTotalAnnualHeatingexp;
  protected double combinedTotalAnnualTaxExp;
  protected double combinedTotalCondoFeeExp;
  protected double combinedTotalPropertyExp;
  protected int commitmentPeriod;
  protected String commitmentProduced;
  protected int crossSellProfileId;
  protected Date datePreAppFirm;
  protected int fifthApproverId;
  protected int finalApproverId;
  protected int financeProgramId;
  protected int fourthApproverId;
  protected String investorApproved;
  protected String investorConfirmationNumber;
  protected double lienHolderSecondaryFinancing;
  protected int lienPositionId;
  protected int MIPayorId;
  protected int MITypeId;
  protected double newMoney;
  protected Date nextRateChangeDate;
  protected int numberOfProperties;
  protected double oldMoney;
  protected Date onHoldDate;
  protected String preApproval;
  protected double premium;
  protected String rateLock;
  protected Date rateLockExpirationDate;
  protected int rateLockPeriod;
  protected String referenceDealNumber;
  protected int referenceDealTypeId;
  protected double refiCurrentBal;
  protected double remainingBalanceSecondaryFin;
  protected Date returnDate;
  protected String secondaryFinancingLienHolder;
  protected String sourceRefAppNbr;
  protected String sourceSystemMailBoxNbr;
  protected int taxPayorId;
  protected int thirdApproverId;
  protected double totalActAppraisedValue;
  protected double totalEstAppraisedValue;
  protected double totalPropertyExpenses;
  protected double additionalPrincipal;
  protected double totalPaymentAmount;
  protected int actualPaymentTerm;
  protected double combinedGDSBorrower;
  protected double combinedTDSBorrower;
  protected double combinedGDS3Year;
  protected double combinedTDS3Year;
  protected double combinedTotalEquityAvailable;
  protected int effectiveAmortizationMonths;
  protected double postedRate;
  protected double blendedRate;
  protected String channelMedia;
  protected int closingTypeid;
  protected String MIUpFront;
  protected Date preApprovalConclusionDate;
  protected Date preApprovalOriginalOfferDate;
  protected String previouslyDeclined;
  protected double requiredDownPayment;
  protected int MIIndicatorId;
  protected int repaymentTypeId;
  protected int decisionModificationTypeId;
  protected String scenarioApproved;
  protected String scenarioRecommended;
  protected int scenarioNumber;
  protected String scenarioDescription;
  protected String MIComments;
  protected int MIStatusId;
  protected int systemTypeId;
  protected double totalFeeAmount;
  protected double maximumLoanAmount;
  protected String scenarioLocked;
  protected double totalLoanBridgeAmount;
  protected String solicitorSpecialInstructions;
  protected String mortgageInsuranceResponse;
  protected double combinedLendingValue;
  protected double existingLoanAmount;
  protected double GDSTDS3YearRate;
  protected double maximumPrincipalAmount;
  protected double maximumTDSExpensesAllowed;
  protected String MICommunicationFlag;
  protected String MIExistingPolicyNumber;
  protected double minimumIncomeRequired;
  protected double PAPurchasePrice;
  protected String instructionProduced;
  protected double teaserDiscount;
  protected Date teaserMaturityDate;
  protected double teaserNetInterestRate;
  protected double teaserPIAmount;
  protected int teaserTerm;
  protected String copyType;
  protected String openMIResponseFlag;
  protected int resolutionConditionId;
  protected String checkNotesFlag;

  // New fields added -- Billy 09April2002
  protected Date closingDatePlus90Days;
  protected double MIPremiumPST;
  protected double perdiemInterestAmount;
  protected int IADNumberOfDays;
  protected double PandIPaymentAmountMonthly;
  protected Date firstPaymentDateMonthly;
  protected int funderProfileId;
  protected String progressAdvance;
  protected double advanceToDateAmount;
  protected int advanceNumber;
  protected String MIRUIntervention;
  protected String preQualificationMICertNum;
  protected String refiPurpose;
  protected double refiOrigPurchasePrice;
  protected String refiBlendedAmortization;
  protected double refiOrigMtgAmount;
  protected String refiImprovementsDesc;
  protected double refiImprovementAmount;
  protected double nextAdvanceAmount;
  protected Date refiOrigPurchaseDate;
  protected String refiCurMortgageHolder;

  // ==========================================================
  // New field for SOB resubmission handling -- By Billy 12June2002
  protected String resubmissionFlag;

  //===============================================================
  // New fields to handle MIPolicyNum problem : the number lost if user cancel MI
  //  and re-send again later.  -- Modified by Billy 14July2003
  protected String MIPolicyNumCMHC;
  protected String MIPolicyNumGE;

  //==============================================================
  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  protected String mccMarketType;

  //==============================================================
  //==============================================================
  //--> Ticket#127 -- BMO Tracking broker Commissions
  //--> New field added to indicate if the Broker is paid or not
  //--> By Billy 24Nov2003
  protected String brokerCommPaid;

  //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
  //--> New Field for HoldReasonId
  //--> By Billy 06Jan2004
  protected int holdReasonId;

  //==============================================================
  //--DJ_LI_CR--start//
  protected double pmntPlusLifeDisability;
  protected double lifePremium;
  protected double disabilityPremium;
  protected double combinedPremium;
  protected double interestRateIncrement;
  protected double interestRateIncLifeDisability;
  protected double equivalentInterestRate;

  //--DJ_LI_CR--end//
  //--DJ_CR010--start//
  protected String commisionCode;

  //--DJ_CR010--end//
  //--DJ_CR203.1--start//
  protected String multiProject;
  protected String proprietairePlus;
  protected double proprietairePlusLOC;

  //--DJ_CR203.1--end//



  //--DJ_CR203.1--end//

  //======================================== ActiveX Ver 1.0.2 Start ========================================
  // -- DJ Ticket#499--start--//
  protected double addCreditCostInsIncurred;
  protected double totalCreditCost;
  protected double intWithoutInsCreditCost;
  // -- DJ Ticket#499--end--//
  //======================================== ActiveX Ver 1.0.2 end ========================================

	// SEAN DJ SPEC-Progress Advance Type July 20, 2005: Added one new field.
	protected int progressAdvanceTypeId;
	// SEAN DJ SPEC-PAT END

	// SEAN GECF Income Verification Document Type Drop Down: define the field.
	protected int incomeVerificationTypeId;
	// END GECF IV Document Type Drop Down.

	//NBC-Implimentation - Start
	
    protected double cashBackAmount;
    protected double cashBackPercent;
    protected String cashBackAmountOverride;

	//NBC-Implimentation - End

  public Status status;

  //=====================================================
  //=====================================================
  private boolean bTemplate = false;
  public SourceOfBusinessProfile sourceOfBusinessProfile;

  // Added SourceFirmProfile -- By BILLY 09April2002
  public SourceFirmProfile sourceFirmProfile;
  public LenderProfile lenderProfile;
  public int nProperty;
  public Property[] property;

  public int nBorrower;
  public Borrower[] borrower;

  public int nEscrowPayment;
  public EscrowPayment[] escrowPayment;

  //Added by Mike Morris 2004-08-19
  public int nWFTriggers;
  public WorkflowTrigger[] wfTrigger;

  public Bridge bridge;

  //MCM team changes STARTS
  public int nComponent;
  public Component[] component;
  //MCM team changes ENDS
  //=====================================================
  // END OF newly defined variables
  //=====================================================

  protected String fundingUploadDone; // Catherine, 10-Mar-05, XD to MCAP
  protected String closingUploadDone; // GE to FCT Link, Catherine, 1-Sep-05

  //Start - MI additions, Apr.19, 2006, Bruce
  protected int           productTypeId;
  protected String        progressAdvanceInspectionBy;
  protected String        selfDirectedRRSP;
  protected String        cmhcProductTrackerIdentifier;
  protected int           locRepaymentTypeId;
  protected String        requestStandardService;
  protected int           locAmortizationMonths;
  protected Date          locInterestOnlyMaturityDate;
  protected int           refiProductTypeId;
  protected double        pandIUsing3YearRate;
  //End - MI additions
  
	//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//	
	protected String floatDownTaskCreatedFlag;
	protected String  floatDownCompletedFlag;
	protected int rateGuaranteePeriod;
	//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
	//***** Change by NBC/PP Implementation Team - Version 1.4 - Start *****//
	protected double teaserRateInterestSaving;
	protected int affiliationProgramId;
	//***** Change by NBC/PP Implementation Team - Version 1.4 - End *****//
  
    // XueBin 20061018 add new fields for DJ Credit Scoring
    protected String outCreditScoreResponse; 
    protected String criticalChangeFlag;
	
  //  **** added for CR03 - 3.2 Gen - Start ****//
  protected String autoCalcCommitExpiryDate;
  //  **** added for CR03 - 3.2 Gen - End ****//

  protected int institutionProfileId;
  
  //4.4 Agent On Deal
  protected int sourceOfBusinessProfile2ndaryId;
  public SourceOfBusinessProfile sourceOfBusinessProfile2ndary;
  //4.4 Agent On Deal - end
  
  //Qualify Rate
  protected String qualifyingOverrideFlag;
  protected String qualifyingRateOverrideFlag;
  protected int overrideQualProd;
  
  protected Date financingWaiverDate;
  
  //Generic MI
  protected int mIDealRequestNumber;
  
  public String getPrimaryKeyName()
  {
    return "dealId";
  }

  public String getCopyIdName()
  {
    return "copyId";
  }

  public Deal(BRInterpretor interpretor) throws SQLException
  {
    super(interpretor);
    filter = null;
    if(Open())
    {
      next();
    }
  }

  public Deal(BRInterpretor interpretor, boolean bTemplate) throws SQLException
  {
    super(interpretor);
    if(bTemplate)
    {
      this.bTemplate = bTemplate;
      nProperty = 1;
      property = new Property[1];
      property[0] = new Property(interpretor, bTemplate);
      nBorrower = 1;
      borrower = new Borrower[1];
      borrower[0] = new Borrower(interpretor, bTemplate);
    }
  }

  public Deal(BRInterpretor interpretor, int anId) throws SQLException
  {
    super(interpretor);

    if(anId == -1)
    {
      this.bTemplate = bTemplate;
      nProperty = 1;
      property = new Property[1];
      property[0] = new Property(interpretor, bTemplate);
      nBorrower = 1;
      borrower = new Borrower[1];
      borrower[0] = new Borrower(interpretor, bTemplate);
    }
    else
    {
      filter = getPrimaryKeyName() + "=" + anId;

      if(Open())
      {
        next();
      }
    }
  }

  public Deal(BRInterpretor interpretor, int anId, int anCopyId) throws SQLException
  {
    super(interpretor);
    filter = getPrimaryKeyName() + "=" + anId + " and " + getCopyIdName() + " = " + anCopyId;
    if(Open())
    {
      next();
    }
  }

  public String getDefaultSQL()
  {
    return "select * from Deal";
  }

  public Object fetchField(String fieldName) throws SQLException, CloneNotSupportedException
  {

    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.debug("@Deal.fetchField Instantiating object: " + fieldName);
    //==================================================

    if(fieldName.compareToIgnoreCase("Property") == 0)
    {
      property = FetchProperties();
      return(Object)property;
    }
    else if(fieldName.compareToIgnoreCase("Borrower") == 0)
    {
      borrower = FetchBorrowers();
      return(Object)borrower;
    }
    else if(fieldName.compareToIgnoreCase("SourceOfBusinessProfile") == 0)
    {
      // sourceOfBusinessProfile
      sourceOfBusinessProfile = new SourceOfBusinessProfile(interpretor, sourceOfBusinessProfileId);
      sourceOfBusinessProfile.close();
      return(Object)sourceOfBusinessProfile;
      // MCM Impl Team changes Starts
    } else if ("Component".equalsIgnoreCase(fieldName)) {
        component = FetchComponents();
        return (Object)component;
      // MCM Impl Team changes Ends
    }
    /*
           else if (fieldName.compareToIgnoreCase("DealType") == 0) {
       // dealType
       dealType = new DealType(dealTypeId);
       dealType.close();
       return (Object)dealType;
       }
     */
    else if(fieldName.compareToIgnoreCase("LenderProfile") == 0)
    {
      // lenderProfile
      lenderProfile = new LenderProfile(interpretor, lenderProfileId);
      lenderProfile.close();
      return(Object)lenderProfile;
    }
    else if(fieldName.compareToIgnoreCase("WorkflowTrigger") == 0)
    {
      // Not sure what the field name is.
      wfTrigger = fetchWorkflowTriggers();
      return(Object)wfTrigger;
    }
    else
    {
      return null;
    }
  }

  private EscrowPayment[] FetchEscrowPayments() throws SQLException,
    CloneNotSupportedException
  {
    int i;
    EscrowPayment escrowPayment;
    EscrowPayment[] escrowPayments = null;
    Stack aStack = new Stack();

    String filter = "dealId=" + dealId + " and copyId = " + copyId;
    escrowPayment = new EscrowPayment(interpretor, filter);
    if(escrowPayment.dealId != dealId)
    {
      escrowPayment.close();
      escrowPayment = null;
    }

    if(escrowPayment != null)
    {
      aStack.push(escrowPayment.deepCopy());

      while(escrowPayment.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(escrowPayment.deepCopy());
      }
      escrowPayment.close();
    }

    nEscrowPayment = aStack.size();
    escrowPayments = new EscrowPayment[nEscrowPayment];

    for(i = 0; i < nEscrowPayment; ++i)
    {
      escrowPayments[i] = (EscrowPayment)aStack.get(i);
    }

    return escrowPayments;
  }

  private Property[] FetchProperties() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    Property property = null;
    Property[] properties = null;
    Stack aStack = new Stack();

    try
    {
      //String   filter = "dealId=" + dealId;
      String filter = "dealId=" + dealId + " and copyId = " + copyId;
      property = new Property(interpretor, filter);
      if(property.dealId != dealId)
      {
        property.close();
        property = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.wran("Exception @Deal.FetchProperties: " + e);
      //==========================================

      throw e;
    }

    if(property != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(property.deepCopy());

      while(property.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(property.deepCopy());
      }
      property.close();
    }

    nProperty = aStack.size();
    properties = new Property[nProperty];

    for(i = 0; i < nProperty; ++i)
    {
      properties[i] = (Property)aStack.get(i);
    }

    return properties;
  }

  protected void close() throws SQLException
  {
    int i;

    if(sourceOfBusinessProfile != null)
    {
      sourceOfBusinessProfile.close();

    }
    if(lenderProfile != null)
    {
      lenderProfile.close();

    }
    for(i = 0; i < nProperty; ++i)
    {
      if(property[i] != null)
      {
        property[i].close();
      }
    }

    for(i = 0; i < nBorrower; ++i)
    {
      if(borrower[i] != null)
      {
        borrower[i].close();
      }
    }

    for(i = 0; i < nEscrowPayment; ++i)
    {
      if(escrowPayment[i] != null)
      {
        escrowPayment[i].close();
      }
    }

    if(bridge != null)
    {
      bridge.close();

    }
    
    if(sourceOfBusinessProfile2ndary != null)
    {
      sourceOfBusinessProfile2ndary.close();

    }
    super.close();
  }

  private Borrower[] FetchBorrowers() throws SQLException, CloneNotSupportedException
  {
    int i = 0;
    Borrower borrower = null;
    Borrower[] borrowers = null;
    Stack aStack = new Stack();

    try
    {
      //String   filter = "dealId=" + dealId;
      String filter = "dealId=" + dealId + " and copyId = " + copyId;
      borrower = new Borrower(interpretor, filter);
      if(borrower.dealId != dealId)
      {
        borrower.close();
        borrower = null;
      }
    }
    catch(SQLException e)
    {
      //--> Debug enhancement : by Billy 26Aug2004
      BRUtil.error("Exception @Deal.FetchBorrowers: " + e);
      //==========================================

      throw e;
    }

    if(borrower != null)
    {
      // make a deep copy of the object and then inserted into the list.
      aStack.push(borrower.deepCopy());

      while(borrower.next())
      {
        // make a deep copy of the object and then inserted into the list.
        aStack.push(borrower.deepCopy());
      }
      borrower.close();
    }

    nBorrower = aStack.size();
    borrowers = new Borrower[nBorrower];

    for(i = 0; i < nBorrower; ++i)
    {
      borrowers[i] = (Borrower)aStack.get(i);
    }

    return borrowers;
  }

  private void fetchPickListObjects() throws SQLException
  {
    // sourceOfBusinessProfile
    if(sourceOfBusinessProfile != null)
    {
      sourceOfBusinessProfile.close();
    }
    sourceOfBusinessProfile = new SourceOfBusinessProfile(interpretor, sourceOfBusinessProfileId);
    sourceOfBusinessProfile.close();

    // Added SourceFirmProfile -- By BILLY 09April2002
    if(sourceFirmProfile != null)
    {
      sourceFirmProfile.close();
    }
    sourceFirmProfile = new SourceFirmProfile(interpretor, sourceFirmProfileId);
    sourceFirmProfile.close();

    // lenderProfile;
    if(lenderProfile != null)
    {
      lenderProfile.close();
    }
    lenderProfile = new LenderProfile(interpretor, lenderProfileId);
    lenderProfile.close();

    // status;
    if(status != null)
    {
      status.close();
    }
    status = new Status(interpretor, statusId);
    status.close();
    
    if(sourceOfBusinessProfile2ndary != null)
    {
    	sourceOfBusinessProfile2ndary.close();
    }
    sourceOfBusinessProfile2ndary = new SourceOfBusinessProfile(interpretor, sourceOfBusinessProfile2ndaryId);
    sourceOfBusinessProfile2ndary.close();
  }

  /**
   * fetchWorkflowTriggers:  this method is used to get the list of workflow
   * triggers associtiated with a deal.
   *
   * @throws SQLException
   * @throws CloneNotSupportedException
   * @return WorkflowTrigger[]
   *
   * Added by Mike Morris 2004-08-19
   */
  private WorkflowTrigger[] fetchWorkflowTriggers() throws SQLException,
  CloneNotSupportedException
{
  WorkflowTrigger wfTrigger;
  WorkflowTrigger[] wfTriggers = null;

  String filter = "dealId=" + dealId;

  try
  {

    wfTrigger = new WorkflowTrigger(interpretor, filter);
    if(wfTrigger.getDealId() != dealId)
    {
      wfTrigger.close();
      wfTrigger = null;
    }else {
      wfTriggers = wfTrigger.getTriggers();
    }

  }
  catch(SQLException e)
  {
    //--> Debug enhancement : by Billy 26Aug2004
    BRUtil.wran("Exception @Deal.FetchWorkflowTriggers: " + e);
    //==========================================

    throw e;
  }

  return wfTriggers;
}

  /**
   * <p>FetchComponents</p>
   * @return Component[]
   * @throws SQLException
   * @throws CloneNotSupportedException
   * 
   * @auther MCM Team
   * @version 1.0 (initial version for this method)
   */
    private Component[] FetchComponents() throws SQLException, CloneNotSupportedException {
        int i = 0;
        Component component = null;
        Component[] components = null;
        Stack aStack = new Stack();
    
        try {
            //String   filter = "dealId=" + dealId;
            String filter = "dealId=" + dealId + " and copyId = " + copyId;
            component = new Component(interpretor, filter);
            if(component.dealId != dealId) {
                component.close();
                component = null;
            }
        } catch(SQLException e) {
            BRUtil.error("Exception @Deal.FetchComponents: " + e);
            throw e;
        }
    
        if(component != null) {
            // make a deep copy of the object and then inserted into the list.
            aStack.push(component.deepCopy());
            
            while(component.next()){
                // make a deep copy of the object and then inserted into the list.
                aStack.push(component.deepCopy());
            }
            component.close();
        }
    
        nComponent = aStack.size();
        components = new Component[nComponent];
    
        for(i = 0; i < nComponent; ++i) {
            components[i] = (Component)aStack.get(i);
        }
    
        return components;
    }

  /**
   * next
   * 
   * @param none <br>
   * @return boolean: true, if next row is available to read <br>
   * @version 1.1 <br>
   * Date: 06/27/2006 <br>
   * Author: NBC/PP Implementation Team <br>
   * Change: <br>
   * 	Added call to set floatDownTaskCreatedFlag, floatDownCompletedFlag & rateGuaranteePeriod attributes
   * NBc-Implementation Team</br>
   * Change: </br>
   *   Added call to set cashBackAmount,cashBackPercent,cashBackAmountOverride attributes
   */
  public boolean next() throws SQLException
  {
    boolean bStatus;
    String filter;

    bStatus = super.next();
    if(bStatus)
    {
      dealId = resultSet.getInt("DEALID");
      copyId = resultSet.getInt("COPYID");
      sourceOfBusinessProfileId = resultSet.getInt("SOURCEOFBUSINESSPROFILEID");
      applicationId = getString(resultSet.getString("APPLICATIONID"));
      if(applicationId != null)
      {
        applicationId = applicationId.trim();
      }
      applicationDate = getDate(resultSet.getTimestamp("APPLICATIONDATE"));
      totalLoanAmount = resultSet.getDouble("TOTALLOANAMOUNT");
      amortizationTerm = resultSet.getInt("AMORTIZATIONTERM");
      estimatedClosingDate = getDate(resultSet.getTimestamp("ESTIMATEDCLOSINGDATE"));
      dealTypeId = resultSet.getInt("DEALTYPEID");
      lenderProfileId = resultSet.getInt("LENDERPROFILEID");
      sourceApplicationId = getString(resultSet.getString("SOURCEAPPLICATIONID"));
      if(sourceApplicationId != null)
      {
        sourceApplicationId = sourceApplicationId.trim();
      }
      totalPurchasePrice = resultSet.getDouble("TOTALPURCHASEPRICE");
      postedInterestRate = resultSet.getDouble("POSTEDINTERESTRATE");
      underwriterUserId = resultSet.getInt("UNDERWRITERUSERID");
      estimatedPaymentAmount = resultSet.getDouble("ESTIMATEDPAYMENTAMOUNT");
      PandIPaymentAmount = resultSet.getDouble("PANDIPAYMENTAMOUNT");
      escrowPaymentAmount = resultSet.getDouble("ESCROWPAYMENTAMOUNT");
      MIPremiumAmount = resultSet.getDouble("MIPREMIUMAMOUNT");
      netLoanAmount = resultSet.getDouble("NETLOANAMOUNT");
      statusDate = getDate(resultSet.getTimestamp("STATUSDATE"));
      discount = resultSet.getDouble("DISCOUNT");
      secondaryFinancing = getString(resultSet.getString("SECONDARYFINANCING"));
      if(secondaryFinancing != null)
      {
        secondaryFinancing = secondaryFinancing.trim();
      }
      originalMortgageNumber = getString(resultSet.getString("ORIGINALMORTGAGENUMBER"));
      if(originalMortgageNumber != null)
      {
        originalMortgageNumber = originalMortgageNumber.trim();
      }
      denialReasonId = resultSet.getInt("DENIALREASONID");
      renewalOptionsId = resultSet.getInt("RENEWALOPTIONSID");
      netInterestRate = resultSet.getDouble("NETINTERESTRATE");
      rateDate = getDate(resultSet.getTimestamp("RATEDATE"));
      dealPurposeId = resultSet.getInt("DEALPURPOSEID");
      buydownRate = resultSet.getDouble("BUYDOWNRATE");
      specialFeatureId = resultSet.getInt("SPECIALFEATUREID");
      interimInterestAmount = resultSet.getDouble("INTERIMINTERESTAMOUNT");
      downPaymentAmount = resultSet.getDouble("DOWNPAYMENTAMOUNT");
      lineOfBusinessId = resultSet.getInt("LINEOFBUSINESSID");
      mortgageInsurerId = resultSet.getInt("MORTGAGEINSURERID");
      mtgProdId = resultSet.getInt("MTGPRODID");
      MIPolicyNumber = getString(resultSet.getString("MIPOLICYNUMBER"));
      if(MIPolicyNumber != null)
      {
        MIPolicyNumber = MIPolicyNumber.trim();
      }
      firstPaymentDate = getDate(resultSet.getTimestamp("FIRSTPAYMENTDATE"));
      netPaymentAmount = resultSet.getDouble("NETPAYMENTAMOUNT");
      MARSNameSearch = getString(resultSet.getString("MARSNAMESEARCH"));
      if(MARSNameSearch != null)
      {
        MARSNameSearch = MARSNameSearch.trim();
      }
      MARSPropertySearch = getString(resultSet.getString("MARSPROPERTYSEARCH"));
      if(MARSPropertySearch != null)
      {
        MARSPropertySearch = MARSPropertySearch.trim();
      }
      MILoanNumber = getString(resultSet.getString("MILOANNUMBER"));
      if(MILoanNumber != null)
      {
        MILoanNumber = MILoanNumber.trim();
      }
      commisionAmount = resultSet.getDouble("COMMISIONAMOUNT");
      maturityDate = getDate(resultSet.getTimestamp("MATURITYDATE"));
      commitmentIssueDate = getDate(resultSet.getTimestamp("COMMITMENTISSUEDATE"));
      commitmentAcceptDate = getDate(resultSet.getTimestamp("COMMITMENTACCEPTDATE"));
      //#DG310 refExistingMtgNumber = resultSet.getInt("REFEXISTINGMTGNUMBER");
      refExistingMtgNumber = resultSet.getString("REFEXISTINGMTGNUMBER");
      combinedTotalLiabilities = resultSet.getDouble("COMBINEDTOTALLIABILITIES");
      bankABANumber = getString(resultSet.getString("BANKABANUMBER"));
      if(bankABANumber != null)
      {
        bankABANumber = bankABANumber.trim();
      }
      bankAccountNumber = getString(resultSet.getString("BANKACCOUNTNUMBER"));
      if(bankAccountNumber != null)
      {
        bankAccountNumber = bankAccountNumber.trim();
      }
      bankName = getString(resultSet.getString("BANKNAME"));
      if(bankName != null)
      {
        bankName = bankName.trim();
      }
      actualClosingDate = getDate(resultSet.getTimestamp("ACTUALCLOSINGDATE"));
      combinedTotalAssets = resultSet.getDouble("COMBINEDTOTALASSETS");
      combinedTotalIncome = resultSet.getDouble("COMBINEDTOTALINCOME");
      combinedTDS = resultSet.getDouble("COMBINEDTDS");
      combinedGDS = resultSet.getDouble("COMBINEDGDS");
      renewalDate = getDate(resultSet.getTimestamp("RENEWALDATE"));
      numberOfBorrowers = resultSet.getInt("NUMBEROFBORROWERS");
      numberOfGuarantors = resultSet.getInt("NUMBEROFGUARANTORS");
      servicingMortgageNumber = getString(resultSet.getString("SERVICINGMORTGAGENUMBER"));
      if(servicingMortgageNumber != null)
      {
        servicingMortgageNumber = servicingMortgageNumber.trim();
      }
      paymentTermId = resultSet.getInt("PAYMENTTERMID");
      interestTypeId = resultSet.getInt("INTERESTTYPEID");
      paymentFrequencyId = resultSet.getInt("PAYMENTFREQUENCYID");
      prepaymentOptionsId = resultSet.getInt("PREPAYMENTOPTIONSID");
      rateFormulaId = resultSet.getInt("RATEFORMULAID");
      statusId = resultSet.getInt("STATUSID");
      riskRatingId = resultSet.getInt("RISKRATINGID");
      electronicSystemSource = getString(resultSet.getString("ELECTRONICSYSTEMSOURCE"));
      if(electronicSystemSource != null)
      {
        electronicSystemSource = electronicSystemSource.trim();
      }
      commitmentExpirationDate = getDate(resultSet.getTimestamp("COMMITMENTEXPIRATIONDATE"));
      requestedSolicitorName = getString(resultSet.getString("REQUESTEDSOLICITORNAME"));
      if(requestedSolicitorName != null)
      {
        requestedSolicitorName = requestedSolicitorName.trim();
      }
      requestedAppraisorName = getString(resultSet.getString("REQUESTEDAPPRAISORNAME"));
      if(requestedAppraisorName != null)
      {
        requestedAppraisorName = requestedAppraisorName.trim();
      }
      interestCompoundId = resultSet.getInt("INTERESTCOMPOUNDID");
      investorProfileId = resultSet.getInt("INVESTORPROFILEID");
      secondApproverId = resultSet.getInt("SECONDAPPROVERID");
      jointApproverId = resultSet.getInt("JOINTAPPROVERID");
      branchProfileId = resultSet.getInt("BRANCHPROFILEID");
      sourceFirmProfileId = resultSet.getInt("SOURCEFIRMPROFILEID");
      groupProfileId = resultSet.getInt("GROUPPROFILEID");
      administratorId = resultSet.getInt("ADMINISTRATORID");
      pricingProfileId = resultSet.getInt("PRICINGPROFILEID");
      refinanceNewMoney = resultSet.getInt("REFINANCENEWMONEY");
      interimInterestAdjustmentDate = getDate(resultSet.getTimestamp("INTERIMINTERESTADJUSTMENTDATE"));
      privilegePaymentId = resultSet.getInt("PRIVILEGEPAYMENTID");
      advanceHold = resultSet.getDouble("ADVANCEHOLD");
      clientNumber = getString(resultSet.getString("CLIENTNUMBER"));
      if(clientNumber != null)
      {
        clientNumber = clientNumber.trim();
      }
      combinedLTV = resultSet.getDouble("COMBINEDLTV");
      combinedTotalAnnualHeatingexp = resultSet.getDouble("COMBINEDTOTALANNUALHEATINGEXP");
      combinedTotalAnnualTaxExp = resultSet.getDouble("COMBINEDTOTALANNUALTAXEXP");
      combinedTotalCondoFeeExp = resultSet.getDouble("COMBINEDTOTALCONDOFEEEXP");
      combinedTotalPropertyExp = resultSet.getDouble("COMBINEDTOTALPROPERTYEXP");
      commitmentPeriod = resultSet.getInt("COMMITMENTPERIOD");
      commitmentProduced = getString(resultSet.getString("COMMITMENTPRODUCED"));
      if(commitmentProduced != null)
      {
        commitmentProduced = commitmentProduced.trim();
      }
      crossSellProfileId = resultSet.getInt("CROSSSELLPROFILEID");
      datePreAppFirm = getDate(resultSet.getTimestamp("DATEPREAPPFIRM"));
      fifthApproverId = resultSet.getInt("FIFTHAPPROVERID");
      finalApproverId = resultSet.getInt("FINALAPPROVERID");
      financeProgramId = resultSet.getInt("FINANCEPROGRAMID");
      fourthApproverId = resultSet.getInt("FOURTHAPPROVERID");
      investorApproved = getString(resultSet.getString("INVESTORAPPROVED"));
      if(investorApproved != null)
      {
        investorApproved = investorApproved.trim();
      }
      investorConfirmationNumber = getString(resultSet.getString("INVESTORCONFIRMATIONNUMBER"));
      if(investorConfirmationNumber != null)
      {
        investorConfirmationNumber = investorConfirmationNumber.trim();
      }
      lienHolderSecondaryFinancing = resultSet.getDouble("LIENHOLDERSECONDARYFINANCING");
      lienPositionId = resultSet.getInt("LIENPOSITIONID");
      MIPayorId = resultSet.getInt("MIPAYORID");
      MITypeId = resultSet.getInt("MITYPEID");
      newMoney = resultSet.getDouble("NEWMONEY");
      nextRateChangeDate = getDate(resultSet.getTimestamp("NEXTRATECHANGEDATE"));
      numberOfProperties = resultSet.getInt("NUMBEROFPROPERTIES");
      oldMoney = resultSet.getDouble("OLDMONEY");
      onHoldDate = getDate(resultSet.getTimestamp("ONHOLDDATE"));
      preApproval = getString(resultSet.getString("PREAPPROVAL"));
      if(preApproval != null)
      {
        preApproval = preApproval.trim();
      }
      premium = resultSet.getDouble("PREMIUM");
      rateLock = getString(resultSet.getString("RATELOCK"));
      if(rateLock != null)
      {
        rateLock = rateLock.trim();
      }
      rateLockExpirationDate = getDate(resultSet.getTimestamp("RATELOCKEXPIRATIONDATE"));
      rateLockPeriod = resultSet.getInt("RATELOCKPERIOD");
      referenceDealNumber = getString(resultSet.getString("REFERENCEDEALNUMBER"));
      if(referenceDealNumber != null)
      {
        referenceDealNumber = referenceDealNumber.trim();
      }
      referenceDealTypeId = resultSet.getInt("REFERENCEDEALTYPEID");
      refiCurrentBal = resultSet.getDouble("REFICURRENTBAL");
      remainingBalanceSecondaryFin = resultSet.getDouble("REMAININGBALANCESECONDARYFIN");
      returnDate = getDate(resultSet.getTimestamp("RETURNDATE"));
      secondaryFinancingLienHolder = getString(resultSet.getString("SECONDARYFINANCINGLIENHOLDER"));
      if(secondaryFinancingLienHolder != null)
      {
        secondaryFinancingLienHolder = secondaryFinancingLienHolder.trim();
      }
      sourceRefAppNbr = getString(resultSet.getString("SOURCEREFAPPNBR"));
      if(sourceRefAppNbr != null)
      {
        sourceRefAppNbr = sourceRefAppNbr.trim();
      }
      sourceSystemMailBoxNbr = getString(resultSet.getString("SOURCESYSTEMMAILBOXNBR"));
      if(sourceSystemMailBoxNbr != null)
      {
        sourceSystemMailBoxNbr = sourceSystemMailBoxNbr.trim();
      }
      taxPayorId = resultSet.getInt("TAXPAYORID");
      thirdApproverId = resultSet.getInt("THIRDAPPROVERID");
      totalActAppraisedValue = resultSet.getDouble("TOTALACTAPPRAISEDVALUE");
      totalEstAppraisedValue = resultSet.getDouble("TOTALESTAPPRAISEDVALUE");
      totalPropertyExpenses = resultSet.getDouble("TOTALPROPERTYEXPENSES");
      additionalPrincipal = resultSet.getDouble("ADDITIONALPRINCIPAL");
      totalPaymentAmount = resultSet.getDouble("TOTALPAYMENTAMOUNT");
      actualPaymentTerm = resultSet.getInt("ACTUALPAYMENTTERM");
      combinedGDSBorrower = resultSet.getDouble("COMBINEDGDSBORROWER");
      combinedTDSBorrower = resultSet.getDouble("COMBINEDTDSBORROWER");
      combinedGDS3Year = resultSet.getDouble("COMBINEDGDS3YEAR");
      combinedTDS3Year = resultSet.getDouble("COMBINEDTDS3YEAR");
      combinedTotalEquityAvailable = resultSet.getDouble("COMBINEDTOTALEQUITYAVAILABLE");
      effectiveAmortizationMonths = resultSet.getInt("EFFECTIVEAMORTIZATIONMONTHS");
      postedRate = resultSet.getDouble("POSTEDRATE");
      blendedRate = resultSet.getDouble("BLENDEDRATE");
      channelMedia = getString(resultSet.getString("CHANNELMEDIA"));
      if(channelMedia != null)
      {
        channelMedia = channelMedia.trim();
      }
      closingTypeid = resultSet.getInt("CLOSINGTYPEID");
      MIUpFront = getString(resultSet.getString("MIUPFRONT"));
      if(MIUpFront != null)
      {
        MIUpFront = MIUpFront.trim();
      }
      preApprovalConclusionDate = getDate(resultSet.getTimestamp("PREAPPROVALCONCLUSIONDATE"));
      preApprovalOriginalOfferDate = getDate(resultSet.getTimestamp("PREAPPROVALORIGINALOFFERDATE"));
      previouslyDeclined = getString(resultSet.getString("PREVIOUSLYDECLINED"));
      if(previouslyDeclined != null)
      {
        previouslyDeclined = previouslyDeclined.trim();
      }
      requiredDownPayment = resultSet.getDouble("REQUIREDDOWNPAYMENT");
      MIIndicatorId = resultSet.getInt("MIINDICATORID");
      repaymentTypeId = resultSet.getInt("REPAYMENTTYPEID");
      decisionModificationTypeId = resultSet.getInt("DECISIONMODIFICATIONTYPEID");
      scenarioApproved = getString(resultSet.getString("SCENARIOAPPROVED"));
      if(scenarioApproved != null)
      {
        scenarioApproved = scenarioApproved.trim();
      }
      scenarioRecommended = getString(resultSet.getString("SCENARIORECOMMENDED"));
      if(scenarioRecommended != null)
      {
        scenarioRecommended = scenarioRecommended.trim();
      }
      scenarioNumber = resultSet.getInt("SCENARIONUMBER");
      scenarioDescription = getString(resultSet.getString("SCENARIODESCRIPTION"));
      if(scenarioDescription != null)
      {
        scenarioDescription = scenarioDescription.trim();
      }
      MIComments = getString(resultSet.getString("MICOMMENTS"));
      if(MIComments != null)
      {
        MIComments = MIComments.trim();
      }
      MIStatusId = resultSet.getInt("MISTATUSID");
      systemTypeId = resultSet.getInt("SYSTEMTYPEID");
      totalFeeAmount = resultSet.getDouble("TOTALFEEAMOUNT");
      maximumLoanAmount = resultSet.getDouble("MAXIMUMLOANAMOUNT");
      scenarioLocked = getString(resultSet.getString("SCENARIOLOCKED"));
      if(scenarioLocked != null)
      {
        scenarioLocked = scenarioLocked.trim();
      }
      totalLoanBridgeAmount = resultSet.getDouble("TOTALLOANBRIDGEAMOUNT");
      solicitorSpecialInstructions = getString(resultSet.getString("SOLICITORSPECIALINSTRUCTIONS"));
      if(solicitorSpecialInstructions != null)
      {
        solicitorSpecialInstructions = solicitorSpecialInstructions.trim();
      }
      mortgageInsuranceResponse = resultSet.getString("MORTGAGEINSURANCERESPONSE");
      if(mortgageInsuranceResponse == null)
      {
        mortgageInsuranceResponse = "";
      }
      else
      {
        mortgageInsuranceResponse = mortgageInsuranceResponse.trim();
      }
      combinedLendingValue = resultSet.getDouble("COMBINEDLENDINGVALUE");
      existingLoanAmount = resultSet.getDouble("EXISTINGLOANAMOUNT");
      GDSTDS3YearRate = resultSet.getDouble("GDSTDS3YEARRATE");
      maximumPrincipalAmount = resultSet.getDouble("MAXIMUMPRINCIPALAMOUNT");
      maximumTDSExpensesAllowed = resultSet.getDouble("MAXIMUMTDSEXPENSESALLOWED");
      MICommunicationFlag = getString(resultSet.getString("MICOMMUNICATIONFLAG"));
      if(MICommunicationFlag != null)
      {
        MICommunicationFlag = MICommunicationFlag.trim();
      }
      MIExistingPolicyNumber = getString(resultSet.getString("MIEXISTINGPOLICYNUMBER"));
      if(MIExistingPolicyNumber != null)
      {
        MIExistingPolicyNumber = MIExistingPolicyNumber.trim();
      }
      minimumIncomeRequired = resultSet.getDouble("MINIMUMINCOMEREQUIRED");
      PAPurchasePrice = resultSet.getDouble("PAPURCHASEPRICE");
      instructionProduced = getString(resultSet.getString("INSTRUCTIONPRODUCED"));
      if(instructionProduced != null)
      {
        instructionProduced = instructionProduced.trim();
      }
      teaserDiscount = resultSet.getDouble("TEASERDISCOUNT");
      teaserMaturityDate = getDate(resultSet.getTimestamp("TEASERMATURITYDATE"));
      teaserNetInterestRate = resultSet.getDouble("TEASERNETINTERESTRATE");
      teaserPIAmount = resultSet.getDouble("TEASERPIAMOUNT");
      teaserTerm = resultSet.getInt("TEASERTERM");
      if(copyType != null)
      {
        copyType = copyType.trim();
      }
      openMIResponseFlag = getString(resultSet.getString("OPENMIRESPONSEFLAG"));
      if(openMIResponseFlag != null)
      {
        openMIResponseFlag = openMIResponseFlag.trim();
      }
      resolutionConditionId = resultSet.getInt("RESOLUTIONCONDITIONID");
      checkNotesFlag = getString(resultSet.getString("CHECKNOTESFLAG"));

      // New fields added -- Billy 09April2002
      closingDatePlus90Days = getDate(resultSet.getTimestamp("closingDatePlus90Days"));
      MIPremiumPST = resultSet.getDouble("MIPremiumPST");
      perdiemInterestAmount = resultSet.getDouble("perdiemInterestAmount");
      IADNumberOfDays = resultSet.getInt("IADNumberOfDays");
      PandIPaymentAmountMonthly = resultSet.getDouble("PandIPaymentAmountMonthly");
      firstPaymentDateMonthly = getDate(resultSet.getTimestamp("firstPaymentDateMonthly"));
      funderProfileId = resultSet.getInt("funderProfileId");
      progressAdvance = getString(resultSet.getString("progressAdvance"));
      advanceToDateAmount = resultSet.getDouble("advanceToDateAmount");
      advanceNumber = resultSet.getInt("advanceNumber");
      MIRUIntervention = getString(resultSet.getString("MIRUIntervention"));
      preQualificationMICertNum = getString(resultSet.getString("preQualificationMICertNum"));
      refiPurpose = getString(resultSet.getString("refiPurpose"));
      refiOrigPurchasePrice = resultSet.getDouble("refiOrigPurchasePrice");
      refiBlendedAmortization = getString(resultSet.getString("refiBlendedAmortization"));
      refiOrigMtgAmount = resultSet.getDouble("refiOrigMtgAmount");
      refiImprovementsDesc = getString(resultSet.getString("refiImprovementsDesc"));
      refiImprovementAmount = resultSet.getDouble("refiImprovementAmount");
      nextAdvanceAmount = resultSet.getDouble("nextAdvanceAmount");
      refiOrigPurchaseDate = getDate(resultSet.getTimestamp("refiOrigPurchaseDate"));
      refiCurMortgageHolder = getString(resultSet.getString("refiCurMortgageHolder"));
      //========================================================================================
      // New field for SOB resubmission handling -- By Billy 12June2002
      resubmissionFlag = getString(resultSet.getString("resubmissionFlag"));
      //===============================================================
      //===============================================================
      // New fields to handle MIPolicyNum problem : the number lost if user cancel MI
      //  and re-send again later.  -- Modified by Billy 14July2003
      MIPolicyNumCMHC = getString(resultSet.getString("MIPolicyNumCMHC"));
      MIPolicyNumGE = getString(resultSet.getString("MIPolicyNumGE"));
      //=============================================================================
      //=============================================================================
      //-- FXLink Phase II --//
      //--> New Field for Market Type Indicator
      //--> By Billy 18Nov2003
      mccMarketType = getString(resultSet.getString("mccMarketType"));
      //=============================================================================
      //==============================================================
      //--> Ticket#127 -- BMO Tracking broker Commissions
      //--> New field added to indicate if the Broker is paid or not
      //--> By Billy 24Nov2003
      brokerCommPaid = getString(resultSet.getString("brokerCommPaid"));
      //==============================================================
      //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
      //--> New Field for HoldReasonId
      //--> By Billy 06Jan2004
      holdReasonId = resultSet.getInt("holdReasonId");
      //=============================================================================
      //--DJ_LI_CR--start//
      pmntPlusLifeDisability = resultSet.getDouble("PMNTPLUSLIFEDISABILITY");
      lifePremium = resultSet.getDouble("LIFEPREMIUM");
      disabilityPremium = resultSet.getDouble("DISABILITYPREMIUM");
      combinedPremium = resultSet.getDouble("COMBINEDPREMIUM");
      interestRateIncrement = resultSet.getDouble("INTERESTRATEINCREMENT");
      interestRateIncLifeDisability = resultSet.getDouble("INTERESTRATEINCLIFEDISABILITY");
      equivalentInterestRate = resultSet.getDouble("EQUIVALENTINTERESTRATE");
      //--DJ_LI_CR--end//
      //--DJ_CR010--start//
      commisionCode = resultSet.getString("COMMISIONCODE");
      //--DJ_CR010--end//
      //--DJ_CR203.1--start//
      multiProject = resultSet.getString("MULTIPROJECT");
      proprietairePlus = resultSet.getString("PROPRIETAIREPLUS");
      proprietairePlusLOC = resultSet.getDouble("PROPRIETAIREPLUSLOC");
      //--DJ_CR203.1--end//

      // -- DJ Ticket#499--start--//
      addCreditCostInsIncurred = resultSet.getDouble("ADDCREDITCOSTINSINCURRED");
      totalCreditCost = resultSet.getDouble("TOTALCREDITCOST");
      intWithoutInsCreditCost = resultSet.getDouble("INTWITHOUTINSCREDITCOST");
      // -- DJ Ticket#499--end--//
      // ----------- Catherine, 10-Mar-05, XD to MCAP
      fundingUploadDone = resultSet.getString("FUNDINGUPLOADDONE");
      // ----------- Catherine, 10-Mar-05, XD to MCAP end 
      
	  // SEAN GECF Income Verification Document Type Drop Down: get value.
	  incomeVerificationTypeId = resultSet.getInt("INCOMEVERIFICATIONTYPEID");
	  // END GECF IV Document Type Drop Down.

      closingUploadDone = resultSet.getString("CLOSINGUPLOADDONE");     // Catherine, 1-Sep-05, GE to FCT

	  // SEAN DJ SPEC-Progress Advance Type July 20, 2005: Loaded data from database.
	  progressAdvanceTypeId = resultSet.getInt("PROGRESSADVANCETYPEID");
	  // SEAN DJ SPEC-PAT END

	  //Bruce, Apr.19, 2006, MI additions
	  productTypeId = resultSet.getInt("PRODUCTTYPEID");
	  progressAdvanceInspectionBy = resultSet.getString("PROGRESSADVANCEINSPECTIONBY");
	  selfDirectedRRSP = resultSet.getString("SELFDIRECTEDRRSP");
	  cmhcProductTrackerIdentifier = resultSet.getString("CMHCPRODUCTTRACKERIDENTIFIER");
	  locRepaymentTypeId = resultSet.getInt("LOCREPAYMENTTYPEID");
	  requestStandardService = resultSet.getString("REQUESTSTANDARDSERVICE");
	  locAmortizationMonths = resultSet.getInt("LOCAMORTIZATIONMONTHS");
	  locInterestOnlyMaturityDate = getDate(resultSet.getTimestamp("LOCINTERESTONLYMATURITYDATE"));
	  refiProductTypeId = resultSet.getInt("REFIPRODUCTTYPEID");
	  pandIUsing3YearRate = resultSet.getDouble("PANDIUSING3YEARRATE");
	  //end MI additions

	  // XueBin 20061018 add for DJ Credit Scoring
	  outCreditScoreResponse = resultSet.getString("OUTCREDITSCORERESPONSE");
	  criticalChangeFlag = resultSet.getString("CRITICALCHANGEFLAG");
	  // XueBin 20061018 add for DJ Credit Scoring END
	  
      filter = "dealId=" + dealId + " and copyId = " + copyId;
      bridge = new Bridge(interpretor, filter);
      bridge.close();
      if(bridge.dealId != dealId)
      {
        bridge = null;

      }
      
      //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//	
      floatDownTaskCreatedFlag = resultSet.getString("FLOATDOWNTASKCREATEDFLAG");
      floatDownCompletedFlag = resultSet.getString("FLOATDOWNCOMPLETEDFLAG");
      rateGuaranteePeriod = resultSet.getInt("RATEGUARANTEEPERIOD");
      //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
      
      //NBC Implementation Team -  Start *****//
      cashBackAmount = resultSet.getDouble("CASHBACKAMOUNT");
      cashBackPercent = resultSet.getDouble("CASHBACKPERCENT");
      cashBackAmountOverride = resultSet.getString("CASHBACKAMOUNTOVERRIDE");
      //NBC Implementation Team -  End *****//
	
      //NBC Implementation Team - Version 1.3 - Start *****//
      teaserRateInterestSaving = resultSet.getDouble("TEASERRATEINTERESTSAVING");
      affiliationProgramId = resultSet.getInt("AFFILIATIONPROGRAMID");
      //NBC Implementation Team - Version 1.3 - End *****//      
      autoCalcCommitExpiryDate = resultSet.getString("AUTOCALCCOMMITEXPIRYDATE");
      institutionProfileId = resultSet.getInt("INSTITUTIONPROFILEID");
      //4.4 Submission Agent
      sourceOfBusinessProfile2ndaryId = resultSet.getInt("SOURCEOFBUSINESSPROFILE2NDARY");
      financingWaiverDate = resultSet.getDate("FINANCINGWAIVERDATE");
      
      //Qualify Rate
      qualifyingOverrideFlag = resultSet.getString("QUALIFYINGOVERRIDEFLAG");
      overrideQualProd = resultSet.getInt("OVERRIDEQUALPROD");
      qualifyingRateOverrideFlag = resultSet.getString("QUALIFYINGRATEOVERRIDEFLAG");
      //MI
      mIDealRequestNumber = resultSet.getInt("MIDEALREQUESTNUMBER");
      
      fetchPickListObjects();

      try
      {
        // fetch property object associated with this deal
        property = FetchProperties();

        // fetch all Borrowers associated with this deal
        borrower = FetchBorrowers();

        // fetch escrowpayment
        escrowPayment = FetchEscrowPayments();
      }
      catch(CloneNotSupportedException e)
      {
        SysLog.error("Fetch property or borrower did not work.");
      }
    }
    else
    {
      nBorrower = 0;
      borrower = new Borrower[0];
      nEscrowPayment = 0;
      escrowPayment = new EscrowPayment[0];
      nProperty = 0;
      property = new Property[0];

    }

    return bStatus;
  }

}
