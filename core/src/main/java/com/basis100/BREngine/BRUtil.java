// Business Rule Expression Scanner, add array
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

//--> Added Log4j for debug enhancement
//--> by Billy 19Aug2004
import org.apache.log4j.*;

//=======================================

public class BRUtil
{
  protected static String errorMsg;

  //--> Added Log4j for debug enhancement
  //--> by Billy 19Aug2004
  private static Logger logger = Logger.getLogger(BRUtil.class.getName());
  static{
    System.out.println("BusinessRule Logger starter !!");
    logger.info("BusinessRule Logger starter !!");
  }
  //=====================================

  public static boolean isDealEntity(Object dealEntity, String token) throws ParseException
  {
    int index = 0, length;
    boolean bStatus = false;
    String tmpToken, curToken, nextToken = null;
    Class classObj, entityClass;
    Object entityObj;
    Object[] entityObjs;
    Field aField;
    Object[] arguments = null;
    Class[] parameterTypes;
    Method method = null;

    if(token.length() <= 0)
    {
      return false;
    }
    // no need to check if dealObj had not been initialized
    if(dealEntity == null)
    {
      return false;
    }

    index = token.indexOf('.');
    if(index >= 0)
    {
      length = token.length();
      curToken = token.substring(0, index);
      nextToken = token.substring(index + 1, length);
    }
    else
    {
      curToken = token;

    }
    curToken = MosEntities.mapEntityName(curToken);
    if(nextToken != null)
    {
      nextToken = MosEntities.mapEntityName(nextToken);

    }
    if(curToken.compareTo("deal") == 0)
    {
      tmpToken = nextToken;
      nextToken = null;
      index = tmpToken.indexOf('.');
      if(index >= 0)
      {
        length = tmpToken.length();
        curToken = tmpToken.substring(0, index);
        nextToken = tmpToken.substring(index + 1, length);
      }
      else
      {
        curToken = tmpToken;
      }
    }

    classObj = dealEntity.getClass();

    try
    {
      aField = classObj.getDeclaredField(curToken);
      if(aField == null)
      {
        bStatus = false;
      }
      else
      {
        bStatus = true;
      }
      entityObj = aField.get(dealEntity);
      entityClass = aField.getDeclaringClass();
    }
    catch(NoSuchFieldException e)
    {
//         errorMsg = "BRUtil.isDealEntity(expression) unknown error: NoSuchFieldException caught!";
//         throw new ParseException(errorMsg, 0);
      return false;
    }
    catch(IllegalAccessException e)
    {
      return false;
    }

    if(nextToken != null)
    {
      bStatus = false;
      try
      {
        if(entityObj == null)
        {
          parameterTypes = new Class[]
            {
            String.class};
          method = entityClass.getMethod("fetchField", parameterTypes);
          arguments = new Object[]
            {
            curToken};
          entityObj = method.invoke(dealEntity, arguments);
        }
        else
        {
          entityClass = aField.getType();
          if(entityClass.isArray())
          {
            entityObjs = (Object[])aField.get(dealEntity);
            entityObj = entityObjs[0];
          }
          else
          {
          }
        }
        bStatus = isDealEntity(entityObj, nextToken);
      }
      catch(NoSuchMethodException e)
      {
//            errorMsg = "BRUtil.isDealEntity(expression) unknown error: NoSuchMethodException caught!";
//            throw new ParseException(errorMsg, 0);
      }
      catch(InvocationTargetException e)
      {
//            errorMsg = "BRUtil.isDealEntity(expression) unknown error: InvocationTargetException caught!";
//            throw new ParseException(errorMsg, 0);
      }
      catch(IllegalAccessException e)
      {
//            errorMsg = "BRUtil.isDealEntity(expression) unknown error: IllegalAccessException caught!";
//            throw new ParseException(errorMsg, 0);
      }
    }

    return bStatus;
  }

  public static boolean isFloat(String str) throws ParseException
  {
    int i, decimalCount = 0, start = 0;
    int length = str.length();
    int last = length - 1;
    boolean bShortForm = false;
    char aChar, charArray[];

    if(str.length() <= 0)
    {
      return false; // empty string
    }
    charArray = str.toCharArray();

    char signChar = charArray[0];
    if(signChar == '-' || signChar == '+')
    {
      start = 1;

    }
    switch(charArray[last])
    {
      case 'k':
      case 'K':
      case 'm':
      case 'M':
        bShortForm = true;
        break;
      default:
        bShortForm = false;
    }

    for(i = start; i < length; ++i)
    {
      aChar = charArray[i];
      if(aChar == '.')
      {
        ++decimalCount;
      }
      else
      {
        if(!Character.isDigit(aChar))
        {
          if(!(i == last && bShortForm))
          {
            return false;
          }
        }
      }
    }

    switch(decimalCount)
    {
      case 0:
        return false;
      case 1:
        return true;
      default:
        String errorMsg = "Too many decimal within the floating point constant!";
        throw new ParseException(errorMsg, 0);
    }
  }

  public static boolean isInteger(String token)
  {
    int i, start = 0;
    int length = token.length();
    int last = length - 1;
    boolean bShortForm = false;
    char charArray[];

    if(token.length() <= 0)
    {
      return false; // empty string
    }
    charArray = token.toCharArray();
    switch(charArray[last])
    {
      case 'k':
      case 'K':
      case 'm':
      case 'M':
        bShortForm = true;
        break;
      default:
        bShortForm = false;
    }

    char signChar = charArray[0];
    if(signChar == '-' || signChar == '+')
    {
      start = 1;

    }
    for(i = start; i < length; ++i)
    {
      if(!Character.isDigit(charArray[i]))
      {
        if(i == last && bShortForm)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
    }

    return true;
  }

  public static boolean isBoolean(String token)
  {
    if(token.length() <= 0)
    {
      return false; // empty string
    }
    if(token.trim().equalsIgnoreCase("true"))
    {
      return true;
    }
    if(token.trim().equalsIgnoreCase("false"))
    {
      return true;
    }

    return false;
  }

  public static boolean isDate(String token)
  {
    boolean bStatus;
    try
    {
      Date dateObj = Date.valueOf(token);
      bStatus = true;
    }
    catch(NumberFormatException e)
    {
      bStatus = false;
    }

    return bStatus;
  }

  public static boolean isNull(String token)
  {
    if(token.length() >= 0 && token.equalsIgnoreCase("null"))
    {
      return true;
    }
    return false;
  }

  public static boolean isTime(String token)
  {
    boolean bStatus;
    try
    {
      Time timeObj = Time.valueOf(token);
      bStatus = true;
    }
    catch(NumberFormatException e)
    {
      bStatus = false;
    }

    return bStatus;
  }

  public static boolean isTimestamp(String token)
  {
    boolean bStatus;
    try
    {
      Timestamp tsObj = Timestamp.valueOf(token);
      bStatus = true;
    }
    catch(NumberFormatException e)
    {
      bStatus = false;
    }

    return bStatus;
  }

  public static String asISODate(Date dateObj)
  {
    String token, strDate, strMonth, strDay;
    List tokens = new ArrayList();
    char month[];

    StringTokenizer tokenizer = new StringTokenizer(dateObj.toString());
    while(tokenizer.hasMoreTokens())
    {
      tokens.add(tokenizer.nextToken());
    }

    month = ((String)tokens.get(1)).toCharArray();
    switch(month[0])
    {
      case 'J':
        switch(month[1])
        {
          case 'a':
            strMonth = "-01-";
            break;
          case 'u':
            strMonth = "-06-";
            break;
          default:
            strMonth = "-07-";
        }

        break;
      case 'F':
        strMonth = "-02-";
        break;
      case 'M':
        if(month[2] == 'r')
        {
          strMonth = "-03-";
        }
        else
        {
          strMonth = "-05-";
        }
        break;
      case 'A':
        if(month[1] == 'p')
        {
          strMonth = "-04-";
        }
        else
        {
          strMonth = "-08-";
        }
        break;
      case 'S':
        strMonth = "-09-";
        break;
      case 'O':
        strMonth = "-10-";
        break;
      case 'N':
        strMonth = "-11-";
        break;
      case 'D':
        strMonth = "-12-";
        break;
      default:
        strMonth = "-xx-";
    }

    token = (String)tokens.get(2);
    if(token.length() > 1)
    {
      strDay = token;
    }
    else
    {
      strDay = "0" + token;

    }
    strDate = tokens.get(5) + strMonth + strDay;

    return strDate;
  }

  public static String asISOTime(Date dateObj)
  {
    List tokens = new ArrayList();

    StringTokenizer tokenizer = new StringTokenizer(dateObj.toString());
    while(tokenizer.hasMoreTokens())
    {
      tokens.add(tokenizer.nextToken());
    }
    return(String)tokens.get(3);
  }

  public static String asISODateTime(Date dateObj)
  {
    return asISODate(dateObj) + " " + asISOTime(dateObj);
  }

  public static String asSQLColumnValue(Object valueObj)
  {
    String strObj = "";
    Date dateObj = new Date(0);
    Time timeObj = new Time(0);
    Timestamp timestampObj = new Timestamp(0);
    Class classObj;

    classObj = valueObj.getClass();
    if(classObj.isInstance(strObj))
    {
      return "\'" + valueObj + "\'";
    }
    else if(classObj.isInstance(dateObj))
    {
      return "To_Date(\'" + asISODate(dateObj) + "\',\'YYYY-MM-DD\')";
    }
    else if(classObj.isInstance(timeObj))
    {
      return "To_Date(\'" + asISODate(dateObj) + "\',\'YYYY-MM-DD\')";
    }
    else if(classObj.isInstance(timestampObj))
    {
      return "To_Date(\'" + asISODate(dateObj) + "\',\'YYYY-MM-DD\')";
    }

    return valueObj.toString();
  }

  public static Boolean mapBoolean(String token) throws ParseException
  {
    if(token.equalsIgnoreCase("true"))
    {
      return new Boolean(true);
    }
    if(token.equalsIgnoreCase("false"))
    {
      return new Boolean(false);
    }

    String msg = token + " is not type boolean!";
    throw new ParseException(msg, 0);
  }

  public static Double mapDouble(String token)
  {
    int last;
    double fieldValue;
    String strTemp;

    last = token.length() - 1;
    strTemp = token.substring(0, last);

    fieldValue = (Double.valueOf(strTemp)).doubleValue();

    switch(token.charAt(last))
    {
      case 'k':
      case 'K':

        // support short form, e.g. 10K, 1.2K, 10M, 1.2M
        fieldValue *= 1000.00;
        return new Double(fieldValue);
      case 'm':
      case 'M':
        fieldValue *= 1000000.00;
        return new Double(fieldValue);
      default:
        return Double.valueOf(token);
    }
  }

  public static Integer mapInt(String token)
  {
    int last;
    String strTemp;

    last = token.length() - 1;
    strTemp = token.substring(0, last);
    switch(token.charAt(last))
    {
      case 'k':
      case 'K':

        // support short form, e.g. 10K, 1.2K, 10M, 1.2M
        strTemp += "000";
        return Integer.valueOf(strTemp);
      case 'm':
      case 'M':
        strTemp += "000000";
        return Integer.valueOf(strTemp);
      default:
        return Integer.valueOf(token);
    }
  }

  public static Boolean mapNull(String token) throws ParseException
  {
    if(token.equalsIgnoreCase("null"))
    {
      return null;
    }

    String msg = token + " is not type null!";
    throw new ParseException(msg, 0);
  }

  //--> Added Log4j for debug enhancement
  //--> by Billy 19Aug2004
  public static void debug(String msg)
  {
    logger.debug(msg);
  }

  public static void info(String msg)
  {
    logger.info(msg);
  }

  public static void wran(String msg)
  {
    logger.warn(msg);
  }

  public static void error(String msg)
  {
    logger.error(msg);
  }

  static public String stack2string(Exception e)
  {
    try
    {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      e.printStackTrace(pw);
      return "BR Stack Trace >> ---------  Begin ------------ \r\n" + sw.toString() + "-------  End ----------<<\r\n";
    }
    catch(Exception e2)
    {
      return "bad stack2string : Return getMessage() instead :: " + e.getMessage();
    }
  }

  //====================================

}
