package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * <p>Title: ComponentCreditCard</p>
 * <p>Description: BR Entity Class map to ComponentCreditCard </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 10, 2008)
 */

public class ComponentCreditCard extends DealEntity {

    protected int         componentId;
    protected int         copyId;
    protected double      creditCardAmount;

    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param bTemplate boolean
     * @throws SQLException
     */
    public ComponentCreditCard(BRInterpretor interpretor, boolean bTemplate) 
        throws SQLException {
        super(interpretor);
        if(bTemplate) {
        }
    }

    /**
     * <p>Constructor</P>
     * @param interpretor BRInterpretor
     * @throws SQLException
     */    
    public ComponentCreditCard(BRInterpretor interpretor) throws SQLException {
        super(interpretor);
        filter = null;
    
        if(Open()) {
            next();
        }
    }
    
    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param anObj Object
     * @throws SQLException
     */
    public ComponentCreditCard(BRInterpretor interpretor, Object anObj) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + ((Component)anObj).componentId + " and copyId = " + ((Component)anObj).copyId;
    
        if(Open()) {
            next();
        }
    }

    /**
     * <p>Constructor</p>
     * @param interpretor BRInterpretor
     * @param componentId int
     * @param copyId int
     * @throws SQLException
     */
    public ComponentCreditCard(BRInterpretor interpretor, int componentId, int copyId) 
        throws SQLException {
        
        super(interpretor);
        filter = getPrimaryKeyName() + "=" + componentId + " and copyId = " + copyId;
    
        if(Open()) {
            next();
        }
    }
    
    /**
     * <p>Constractor</p>
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public ComponentCreditCard(BRInterpretor interpretor, String filter) 
        throws SQLException {
    
        super(interpretor);
        this.filter = filter;
    
        if(Open()) {
            next();
        }
    }
    
    /**
     * <p>getPrimaryKeyName</p>
     * <p>Description:get first primary key name </p>
     * @return String
     */
    protected String getPrimaryKeyName()  {
        return "componentid";
    }
    
    /**
     * <p>deepCopy</p>
     * @return ComponentCreditCard
     */
    public ComponentCreditCard deepCopy() throws CloneNotSupportedException {
        return(ComponentCreditCard)this.clone();
    }
    
    /**
     * <p>getDefaultSQL</p>
     * @return String
     */
    protected String getDefaultSQL() {
        return "select * from ComponentCreditCard";
    }
    
    /**
     * <p>next</p>
     * <p>Description: retrieve data from DB and set entity properties
     * @return boolean:true if data is exist, false if not
     */
    public boolean next() throws SQLException {
        
        boolean bStatus = super.next();
        if (!bStatus) return bStatus;
    
        componentId = resultSet.getInt("COMPONENTID");
        copyId = resultSet.getInt("COPYID");
        creditCardAmount = resultSet.getInt("CREDITCARDAMOUNT");

        return bStatus;
    }

}
