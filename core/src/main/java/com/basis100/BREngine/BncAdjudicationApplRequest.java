/* NBC-Implementation-Team  */
package com.basis100.BREngine;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.SQLException;



/**
 * <p>
 * Title: BncAdjudicationApplRequest
 * </p>
 * 
 * <p>
 * Description: BREntity class for BNC Adjudication Application Request
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC Implementation Team
 * @version 1
 * 
 */

public class BncAdjudicationApplRequest extends DealEntity {
	
	protected int requestId;	
	protected int applicantNumber; 	
	protected int requestedCreditBureauNameId;	
	
	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
    protected String getPrimaryKeyName() {
      return "requestId";
    }

	/**
	 * Creates a new BREntity
	 * @param interpretor
	 * @param template
	 * @throws SQLException
	 */
   public BncAdjudicationApplRequest(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   /**
	 * @param interpretor
	 * @throws SQLException
	 */
   public BncAdjudicationApplRequest(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   /**
	 * @param BRInterpretor
	 * @param filter
	 * @throws SQLException
	 */
   public BncAdjudicationApplRequest(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

	/**
	 * Clone this BncAdjudicationApplRequest
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */ 
   protected BncAdjudicationApplRequest deepCopy() throws CloneNotSupportedException {
      return (BncAdjudicationApplRequest)clone();
   }

	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
   protected String getDefaultSQL() {
      return "select * from BncAdjudicationApplRequest";
   }

	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
    	  
    	        requestId = resultSet.getInt("REQUESTID");	
    		    applicantNumber = resultSet.getInt("APPLICANTNUMBER");
    		    requestedCreditBureauNameId = resultSet.getInt("REQUESTEDCREDITBUREAUNAMEID");	
 
      }

      return bStatus;
   }
}
