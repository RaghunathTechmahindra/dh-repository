// dynamic bind instance variables by its name
package com.basis100.BREngine;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

public class PropertyExpense
  extends DealEntity
{
  protected int propertyExpenseId;
  protected int propertyExpensePeriodId;
  protected int propertyExpenseTypeId;
  protected int propertyId;
  protected double propertyExpenseAmount;
  protected String propertyExpenseDescription;
  protected double monthlyExpenseAmount;
  protected String peIncludeIngDS;
  protected String peIncludeIntDS;
  protected short pePercentIngDS;
  protected short pePercentIntDS;
  protected short pePercentOutGDS;
  protected short pePercentOutTDS;
  protected int copyId;

  protected String getPrimaryKeyName()
  {
    return "propertyId";
  }

  public PropertyExpense(BRInterpretor interpretor, boolean template) throws SQLException
  {
    super(interpretor);
  }

  public PropertyExpense(BRInterpretor intrepretor) throws SQLException
  {
    super(intrepretor);
    filter = null;

    if(Open())
    {
      next();
    }
  }

  public PropertyExpense(BRInterpretor intrepretor, String filter) throws SQLException
  {
    super(intrepretor);
    this.filter = filter;

    if(Open())
    {
      next();
    }
  }

  protected String getDefaultSQL()
  {
    return "select * from PropertyExpense";
  }

  public PropertyExpense deepCopy() throws CloneNotSupportedException
  {
    return(PropertyExpense)this.clone();
  }

  protected boolean next() throws SQLException
  {
    boolean bStatus;

    bStatus = super.next();
    if(bStatus)
    {
      propertyExpenseId = resultSet.getInt("PROPERTYEXPENSEID");
      propertyExpensePeriodId = resultSet.getInt("PROPERTYEXPENSEPERIODID");
      propertyExpenseTypeId = resultSet.getInt("PROPERTYEXPENSETYPEID");
      propertyId = resultSet.getInt("PROPERTYID");
      propertyExpenseAmount = resultSet.getDouble("PROPERTYEXPENSEAMOUNT");
      propertyExpenseDescription = getString(resultSet.getString("PROPERTYEXPENSEDESCRIPTION"));
      if(propertyExpenseDescription != null)
      {
        propertyExpenseDescription = propertyExpenseDescription.trim();
      }
      monthlyExpenseAmount = resultSet.getDouble("MONTHLYEXPENSEAMOUNT");
      peIncludeIngDS = getString(resultSet.getString("PEINCLUDEINGDS"));
      if(peIncludeIngDS != null)
      {
        peIncludeIngDS = peIncludeIngDS.trim();

      }
      peIncludeIntDS = getString(resultSet.getString("PEINCLUDEINTDS"));
      if(peIncludeIntDS != null)
      {
        peIncludeIntDS = peIncludeIntDS.trim();

      }
      pePercentIngDS = resultSet.getShort("PEPERCENTINGDS");
      pePercentIntDS = resultSet.getShort("PEPERCENTINTDS");
      pePercentOutGDS = resultSet.getShort("PEPERCENTOUTGDS");
      pePercentOutTDS = resultSet.getShort("PEPERCENTOUTTDS");
      copyId = resultSet.getInt("COPYID");
    }

    return bStatus;
  }
}
