// dynamic bind instance variables by its name
package com.basis100.BREngine;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

public class DealEntity implements Cloneable
{

  protected BRInterpretor interpretor = null;
  protected Statement queryStmt = null;
  protected Statement execStmt = null;
  protected ResultSet resultSet = null;
  protected String filter = null;
  protected boolean bEmpty = true;

  private String selectStmtSaved;

  protected String getString(String s)
  {
    if(s == null)
    {
      return "";
    }

    return s.trim();

  }

  protected Date asDate(Timestamp ts)
  {
    if(ts == null)
    {
      return null;
    }

    return new Date(ts.getTime());
  }

  protected Date getDate(Date dte)
  {
    if(dte == null)
    {

//      return new Date(System.currentTimeMillis());
      return null;
    }

    return dte;
  }

  protected Date getDate(Timestamp ts)
  {
    if(ts == null)
    {

//      return new Date(System.currentTimeMillis());
      return null;
    }

    return new Date(ts.getTime());
  }

  protected Timestamp getTimestamp(Timestamp ts)
  {
    if(ts == null)
    {

//      return new Timestamp(System.currentTimeMillis());
      return null;
    }

    return ts;
  }

  protected String getPrimaryKeyName()
  {
    return "";
  }

  public DealEntity(BRInterpretor interpretor) throws SQLException
  {
    this.interpretor = interpretor;
    initialize();
  }

  private void initialize() throws SQLException
  {
  }

  public boolean Open() throws SQLException
  {
    String selectStmt;

    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }

    // Create a Statement for query
    queryStmt = interpretor.getConnection().createStatement();

    selectStmt = getDefaultSQL();
    if(filter != null)
    {
      selectStmt = selectStmt + " where " + filter;

    }

    BRUtil.debug("@DealEntity.Open SQL Stmt: " + selectStmt);

    resultSet = queryStmt.executeQuery(selectStmt);

    return true;
  }

  protected void close() throws SQLException
  {
    if(resultSet != null)
    {
      resultSet.close();
      resultSet = null;
    }
    if(queryStmt != null)
    {
      queryStmt.close();
      queryStmt = null;
    }
    if(execStmt != null)
    {
      execStmt.close();
      execStmt = null;
    }
  }

  protected boolean next() throws SQLException
  {
    return resultSet.next();
  }

  protected String getDefaultSQL()
  {
    return "";
  }
}
