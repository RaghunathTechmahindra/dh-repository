/* NBC-Implementation-Team  */
package com.basis100.BREngine;
import java.sql.SQLException;



/**
 * <p>
 * Title: BncAdjudicationApplResponse
 * </p>
 * 
 * <p>
 * Description: BREntity class for BNC Adjudication Application Request
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC Implementation Team
 * @version 1
 * 
 */

public class BncAdjudicationApplResponse extends DealEntity {
	
	protected int responseId;	
	protected int applicantNumber; 	
	protected int usedCreditBureauNameId;
	protected double netWorth;
	

	/**
	 * Gets the String representation of this entity's primary key's name.
	 * @return The name of the primary key.
	 */
    protected String getPrimaryKeyName() {
      return "responseId";
    }

	/**
	 * Creates a new BREntity
	 * @param interpretor
	 * @param template
	 * @throws SQLException
	 */
   public BncAdjudicationApplResponse(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   /**
	 * @param interpretor
	 * @throws SQLException
	 */
   public BncAdjudicationApplResponse(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   /**
	 * @param BRInterpretor
	 * @param filter
	 * @throws SQLException
	 */
   public BncAdjudicationApplResponse(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

	/**
	 * Clone this BncAdjudicationApplResponse
	 * @return A clone.
	 * @throws CloneNotSupportedException If the clone cannot be created.
	 */ 
   protected BncAdjudicationApplResponse deepCopy() throws CloneNotSupportedException {
      return (BncAdjudicationApplResponse)clone();
   }

	/**
	 * Gets the SQL needed to retrieve records from this entity's table.
	 * @return A SELECT SQL statement.
	 */
   protected String getDefaultSQL() {
      return "select * from BncAdjudicationApplResponse";
   }

	/**
	 * Retrieves entity information from the database.
	 * @return <code>true</code>, if there is more information, <code>false</code> otherwise.
	 * @throws SQLException If an error occurs. 
	 */
   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
    	  
    	        responseId = resultSet.getInt("RESPONSEID");	
    		    applicantNumber = resultSet.getInt("APPLICANTNUMBER");
    		    usedCreditBureauNameId = resultSet.getInt("USEDCREDITBUREAUNAMEDID");
    		    netWorth = resultSet.getInt("NETWORTH");
 
      }

      return bStatus;
   }
}
