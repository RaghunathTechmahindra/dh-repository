// Business Rule Expression Scanner, add array
/*
   2002-06-13   yml   add addItem(...) support

*/
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class DoNode implements Cloneable {
   private BRInterpretor interpretor;
   int      nStmts = 0; // statment count
   List     stmts = null;      // String or DoNode object

   protected String   errorMsg;

   public DoNode(BRInterpretor interpretor) {
      this.interpretor = interpretor;
      stmts = new ArrayList();
   }

   public DoNode deepCopy() throws ParseException {
      DoNode     doNode;

      try {
         doNode = (DoNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return doNode;
   }

   public void display() {
   }

   public Object evaluate() throws ParseException, SQLException {
      int     i;
      Object  result=null, stmtObj;
      Class   classObj;
      BRExpNode     expObj  = new BRExpNode(interpretor);
      BRVarNode     varObj  = new BRVarNode(interpretor);
      BRFunctNode   functObj= new BRFunctNode(interpretor);
      BRAssignNode  assignObj = new BRAssignNode(interpretor);
      IfNode        ifNode    = new IfNode(interpretor);
      ReturnNode    returnNode = new ReturnNode(interpretor);
      BreakNode     breakNode = new BreakNode(interpretor);
      ForNode       forNode     = new ForNode(interpretor);

      for (i=0; i<nStmts; ++i) {
         stmtObj = stmts.get(i);
         classObj = stmtObj.getClass();

         if (classObj.isInstance(expObj))
            result = ((BRExpNode)stmtObj).evaluate();
         else if (classObj.isInstance(varObj))
            result = ((BRVarNode)stmtObj).evaluate();
         else if (classObj.isInstance(functObj))
            result = ((BRFunctNode)stmtObj).evaluate();
         else if (classObj.isInstance(assignObj))
            result = ((BRAssignNode)stmtObj).evaluate();
         else if (classObj.isInstance(ifNode))
            result = ((IfNode)stmtObj).evaluate();
         else if (classObj.isInstance(forNode))
            result = ((ForNode)stmtObj).evaluate();
         else if (classObj.isInstance(breakNode))
            return null;
         else if (classObj.isInstance(returnNode))
            return ((ReturnNode)stmtObj).evaluate();
         else {
            errorMsg = "Invalid node object <" + stmtObj + " detected";
            //--> Debug enhancement : by Billy 24Aug2004
            BRUtil.wran("Problem @DoNode.evaluate : " + errorMsg);
            //==========================================

            throw new ParseException(errorMsg, 0);
            }

         // return if returned node is a Return object
         if (result != null) {
            classObj = result.getClass();
            if (classObj.isInstance(returnNode)) {
               return ((ReturnNode)result).evaluate();
               }
            }
         }

      return result;
   }
   public Object evaluate(Object valueObj) throws SQLException {

      return null;
   }

   // 2002-06-13: add addItem(...) support
   public boolean parseExpressionPhase2() throws ParseException {
      int     i;
      Object  stmtObj;
      Class   classObj;
      BRExpNode   expObj = new BRExpNode(interpretor);
      BRAssignNode  assignNode = new BRAssignNode(interpretor);
      ForNode       forNode     = new ForNode(interpretor);
      BRFunctNode   functNode = new BRFunctNode(interpretor);
      IfNode        ifNode      = new IfNode(interpretor);
      ReturnNode    returnNode  = new ReturnNode(interpretor);

      for (i=0; i<nStmts; ++i) {
         stmtObj = stmts.get(i);

         classObj = stmtObj.getClass();
         if (classObj.isInstance(expObj)) {
            ((BRExpNode)stmtObj).parseExpressionPhase2();
            }
         else if (classObj.isInstance(assignNode)) {
            ((BRAssignNode)stmtObj).parseExpressionPhase2();
            }
         else if (classObj.isInstance(functNode)) {
            ((BRFunctNode)stmtObj).parseExpressionPhase2();
            }
         else if (classObj.isInstance(ifNode)) {
            ((IfNode)stmtObj).parseExpressionPhase2();
            }
         else if (classObj.isInstance(forNode)) {
            ((ForNode)stmtObj).parseExpressionPhase2();
            }
         else if (classObj.isInstance(returnNode)) {
            ((ReturnNode)stmtObj).parseExpressionPhase2();
            }
         }

      return true;
   }

   protected void substituteUserVars() throws ParseException {
      Class     classObj;
      Object    stmtObj;
      String        strObj = "";
      BRExpNode     expNode  = new BRExpNode(interpretor);
      BRFunctNode   functNode= new BRFunctNode(interpretor);
      BRAssignNode  assignNode = new BRAssignNode(interpretor);
      IfNode        ifNode    = new IfNode(interpretor);
      ReturnNode    returnNode = new ReturnNode(interpretor);
      ForNode       forNode     = new ForNode(interpretor);

      for (int i=0; i<nStmts; ++i) {
         stmtObj = stmts.get(i);
         classObj = stmtObj.getClass();

         if (classObj.isInstance(expNode)) {
            ((BRExpNode)stmtObj).substituteUserVars();
         }
         else if (classObj.isInstance(functNode)) {
            ((BRFunctNode)stmtObj).substituteUserVars();
         }
         else if (classObj.isInstance(assignNode)) {
            ((BRAssignNode)stmtObj).substituteUserVars();
         }
         else if (classObj.isInstance(ifNode)) {
            ((IfNode)stmtObj).substituteUserVars();
         }
         else if (classObj.isInstance(returnNode)) {
            ((ReturnNode)stmtObj).substituteUserVars();
         }
         else if (classObj.isInstance(forNode)) {
            ((ForNode)stmtObj).substituteUserVars();
         }
         else if (classObj.isInstance(strObj)) {
            strObj = (String)stmtObj;
            stmtObj = (String)interpretor.substituteUserVars(strObj,
                        BRConstants.NO_DELIMITER);
            stmts.set(i, stmtObj);
         }
      }
   }
}
