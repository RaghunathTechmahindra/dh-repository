/*
 * by Neil on Dec/01/2004
 */
package com.basis100.BREngine;

import java.sql.SQLException;

/**
 * @see com.basis100.BREngine.DealEntity
 * @author Neil Kong on Dec/01/2004
 */
public class SmokeStatus extends DealEntity
{
    private int smokeStatusId;
    private String smoker;
    private static final String sql = "SELECT * FROM SMOKESTATUS ";
    private static final String primaryKeyName = "smokeStatusId";

    /**
     * @param interpretor
     * @throws SQLException
     */
    public SmokeStatus(BRInterpretor interpretor) throws SQLException
    {
        this(interpretor, null);
    }

    /**
     * 
     * @param interpretor
     * @param filter
     * @throws SQLException
     */
    public SmokeStatus(BRInterpretor interpretor, String filter)
            throws SQLException
    {
        super(interpretor);
        this.filter = filter;
        if (Open())
        {
            next();
        }
    }

    /**
     * 
     * @return
     * @throws CloneNotSupportedException
     */
    public SmokeStatus deepCopy() throws CloneNotSupportedException
    {
        try
        {
            return (SmokeStatus) this.clone();
        }
        catch (CloneNotSupportedException cnse)
        {
            String errMsg = "@SmokeStatus.deepCopy() : " + cnse.getMessage();
            throw new CloneNotSupportedException(errMsg);
        }
    }

    /**
     * @return
     */
    public String getDefaultSQL()
    {
        return sql;
    }

    /**
     * @return
     */
    public String getPrimaryKeyName()
    {
        return primaryKeyName;
    }

    /**
     * @return
     * @throws SQLException
     */
    public boolean next() throws SQLException
    {
        boolean bStatus = super.next();
        if (bStatus)
        {
            smoker = resultSet.getString("SMOKER");
            smokeStatusId = resultSet.getInt("SMOKESTATUSID");
        }
        return false;
    }

    /**
     * @return Returns the smoker.
     */
    public String getSmoker()
    {
        return smoker;
    }

    /**
     * @return Returns the smokeStatusId.
     */
    public int getSmokeStatusId()
    {
        return smokeStatusId;
    }
}