// Business Rule Expression Scanner, add array
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.util.List;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.*;
import java.lang.reflect.Array;
import java.sql.*;
import java.sql.Date;

public class BreakNode implements Cloneable {
   private BRInterpretor interpretor;
   protected String   errorMsg;

   public BreakNode(BRInterpretor interpretor) {
    this.interpretor = interpretor;
   }

   public BreakNode deepCopy() throws ParseException {
      BreakNode     breakNode;

      try {
         breakNode = (BreakNode)this.clone();
         }
      catch (CloneNotSupportedException e) {
         errorMsg = "CloneNotSupportedException caught!";
         throw new ParseException(errorMsg, 0);
         }
      return breakNode;
   }

   public void display() {
   }


   public boolean parseExpressionPhase2() {
      return true;
   }
}
