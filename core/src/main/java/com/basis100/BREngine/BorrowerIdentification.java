/* NBC-Implementation-Team  */
package com.basis100.BREngine;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.Exception;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;

import com.basis100.deal.pk.BorrowerIdentificationPK;

/**
 * <p>
 * Title: BorrowerIdentification
 * </p>
 * 
 * <p>
 * Description: BREntity class for Borrower Identification
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC Implementation Team
 * @version 1
 * 
 */

public class BorrowerIdentification extends DealEntity {
	protected int identificationId;
	protected String identificationNumber;
	protected int identificationTypeId;
	protected int borrowerId;
	protected int copyId;
	protected String identificationCountry;
	
	private BorrowerIdentificationPK pk;
 
	/**
	 * gets the primaryKey name associated with this entity
	 * 
	 * @return a BorrowerIdentificationPK
	 */

   protected String getPrimaryKeyName() {
      return "identificationId";
   }

   /**
	 * @param BRInterpretor
	 * @param template
	 * @throws SQLException
	 */
   public BorrowerIdentification(BRInterpretor interpretor, boolean template) throws SQLException {
      super(interpretor);
   }

   /**
	 * @param BRInterpretor
	 * @throws SQLException
	 */
   public BorrowerIdentification(BRInterpretor interpretor) throws SQLException {
      super(interpretor);
      filter = null;

      if (Open())
         next();
   }

   /**
	 * @param BRInterpretor
	 * @param filter
	 * @throws SQLException
	 */
   public BorrowerIdentification(BRInterpretor interpretor, String filter) throws SQLException {
      super(interpretor);
      this.filter = filter;

      if (Open())
         next();
   }

   /**
    * 
    * @return clone of the BorrowerIdentification Object
    * @throws CloneNotSupportedException
    */ 
   protected BorrowerIdentification deepCopy() throws CloneNotSupportedException {
      return (BorrowerIdentification)clone();
   }

   /**
    * @return default SQL string
    */
   protected String getDefaultSQL() {
      return "select * from BorrowerIdentification";
   }
  
   /**
    * @return boolean
    * Sets the attributes from the resultset
    */
   protected boolean next() throws SQLException {
      boolean   bStatus;

      bStatus = super.next();
      if (bStatus) {
    	        identificationId  = resultSet.getInt("IDENTIFICATIONID");
    	        identificationNumber  = resultSet.getString("IDENTIFICATIONNUMBER");
    	        identificationTypeId  = resultSet.getInt("IDENTIFICATIONTYPEID");
    	        borrowerId  = resultSet.getInt("BORROWERID");
				copyId  = resultSet.getInt("COPYID");
				identificationCountry = resultSet.getString("IDENTIFICATIONCOUNTRY");

        }

      return bStatus;
   }
}
