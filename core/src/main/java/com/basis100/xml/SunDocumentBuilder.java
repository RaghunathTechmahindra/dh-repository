package com.basis100.xml;

import com.sun.xml.parser.*;
import com.sun.xml.tree.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import java.io.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import javax.xml.parsers.*;


import java.util.*;

public class SunDocumentBuilder extends DocumentBuilder
{

  private Document dom;
  private boolean errorState = false;
  private String errorMessage;

  public static boolean debug = true;


 /**
  * gets the root of the DOM Tree - a instance of the
  * DOM Document interface.
  */
/*  public Document getDomTree(String file) throws FileParseException
  {
    FileInputStream input;

    try
    {
      input = new FileInputStream(file);
      return XmlDocument.createXmlDocument(input,false);
    }
    catch(SAXException sax)
    {
      String msg = "Error: Parsing Failed " + sax.getMessage();
      throw new FileParseException(msg + " " + file);
    }
    catch (FileNotFoundException notFound)
    {
      String msg = "Error: The target file was not found: ";
      throw new FileParseException(msg +  " " + file);
    }
    catch (IOException io)
    {
      String msg = "Error: IOException: " + io.getMessage();
      throw new FileParseException(msg +  " " + file);
    }


  }     */

  public Document parse(InputSource is)
  {

    try
    {
      return XmlDocument.createXmlDocument(is,false);
    }
    catch(SAXException sax)
    {
      String msg = "Error: Parsing Failed " + sax.getMessage();
      System.out.println(msg);
     // throw new FileParseException(msg + " " + file);
    }
    catch (FileNotFoundException notFound)
    {
      String msg = "Error: The target file was not found: ";
     // throw new FileParseException(msg +  " " + file);
    }
    catch (IOException io)
    {
      String msg = "Error: IOException: " + io.getMessage();
      //throw new FileParseException(msg +  " " + file);
    }
    return null;

  }

  public Document parse(File file)
  {
    FileInputStream input;

    try
    {
      input = new FileInputStream(file);
      return XmlDocument.createXmlDocument(input,false);
    }
    catch(SAXException sax)
    {
      String msg = "Error: Parsing Failed " + sax.getMessage();
     // throw new FileParseException(msg + " " + file);
    }
    catch (FileNotFoundException notFound)
    {
      String msg = "Error: The target file was not found: ";
     // throw new FileParseException(msg +  " " + file);
    }
    catch (IOException io)
    {
      String msg = "Error: IOException: " + io.getMessage();
     // throw new FileParseException(msg +  " " + file);
    }

    return null;
  }

  public void setErrorHandler(ErrorHandler eh)
  {

  }

  public void setEntityResolver(EntityResolver er)
  {


  }

  public boolean isValidating()
  {
    return true;

  }

  public boolean isNamespaceAware()
  {
    return true;//????
  }

  public Document newDocument()
  {
    return new XmlDocument();
  }



  public void debug(String msg)
  {
     if(debug)System.out.println(msg);
  }

  public DOMImplementation getDOMImplementation()
  {
    return dom.getImplementation();
  }
}



