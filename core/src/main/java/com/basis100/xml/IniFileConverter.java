package com.basis100.xml;

import java.util.*;
import java.io.*;
import org.w3c.dom.*;
import com.basis100.util.file.*;

/**
 *  Converts ini file to xml format <br> where section headers form children of
 *  the given root name and field - name=value pairs form children of the root.
 *  Section elements have the name "Section" and a name attribute.
 *  Fields have the name field with name and value attributes.
 *
 **/

public class IniFileConverter
{

  public IniFileConverter(){}

  public Document convert(File file, String rootName)throws Exception
  {

    Document doc = XmlToolKit.getInstance().newDocument();

    Element root = doc.createElement(rootName);
    doc.appendChild(root);

    IniDataView ini = new IniDataView();
    boolean loaded = ini.loadIniFile(file.getAbsolutePath());

    if(!loaded)
      throw new Exception("Couldn't Load *.ini file");

    List sections = new ArrayList(ini.getSectionNames());

    Iterator sit = sections.iterator();

    String name = null;
    Element sectElement = null;

    while(sit.hasNext())
    {
      name = (String)sit.next();
      sectElement = doc.createElement("Section");
      sectElement.setAttribute("name",name);
      root.appendChild(sectElement);

      SectionData data = ini.getSection(name);

      int len = data.size();
      String key = null;
      String value = null;

      for(int i = 0; i < len; i++)
      {
        key = data.getKeyAt(i);
        value = data.getValueAt(i);

        if(key == null || key.trim().length() == 0)
         key = " ";

        if(value == null || value.trim().length() == 0)
         value = " ";

        Element field = doc.createElement("Field");

        field.setAttribute("name",key);
        field.setAttribute("value", value);

        sectElement.appendChild(field);
      }

    }
    
    return doc;
  }


}