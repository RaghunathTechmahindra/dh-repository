package com.basis100.xml;

import java.util.*;
import java.io.*;

import org.w3c.dom.*;
import org.w3c.dom.traversal.*;

import com.basis100.resources.*;


/**
 *  Provides a convenience methods for handling Nodes within a org.w3c.dom Document.
 *
 */

public class DomUtil
{
  /**
   * <pre>
   * Gets the Elements of a DOM subtree starting a the given node and adds them to<br>
   * a new <code>List</code> in 'Document Order'. This refers to <code>Element</code><br>
   * object members of List in a Depth First preorder arrangement.
   *  </pre>
   *  @return a List of Elements
   *  @param the root node at which depth first preorder walk begins.
   **/
  public static List getElementList(Node node)
  {
    List ret = new ArrayList();

    if(node == null) return ret;

    TreeWalker tw = XmlToolKit.getInstance().getElementTreeWalker(node);
    Node current = null;

    while(true)
    {
      current = tw.nextNode();

      if(current == null)
       break;

      if(current.getNodeType() == Node.ELEMENT_NODE)
      {
         ret.add(current);
      }
    }

    return ret;
  }


 /**
 *  Gets all Element nodes with the distinguished name supplied.<br>
 *  if Borrower is a child of Deal and BorrowerAddress is the child of Borrower - <br>
 *  The search String may be formed as follows<br>
 *  Deal.Borrower.BorrowerAddress
 *
 *  i.e. if  'BorrowerAddress' and the list at 1 holds 'Addr' <br>
 *  then only the nodes representing Addresses that are children of BorrowerAddresses will <br>
 *  be returned. In other words Addr nodes that are children of contact will not be <br>
 *  returned. <br>
 *  @param d - the document to search for distiquished name meeting criteria
 *  @returns a list of Element objects
 */
  public static List getElementsByDistinguishedName(Document d, String element)
  {
    String[] delement = parseElementName(element);
    int len = delement.length;
    Element current = null;

    List roots = getNamedDescendants(d, delement[len]);
    ListIterator rit = roots.listIterator();

    while(rit.hasNext())
    {
      current = (Element)rit.next();

      if(!hasDistinguishedName(current, delement))
       rit.remove();
    }

    return roots;
  }

  /**
   * helper method for above<br>
   * @return true if the array matchs the distinguished name of the element<br>
   * with out regard for highest parent.
   */
  private static boolean hasDistinguishedName(Element el, String[] name)
  {
      int len = name.length;
      Node current = el;
      Node parent = null;
      String currentName = null;

      for(int i = len-2 ; i >= 0; i--)
      {
        parent = current.getParentNode();

        if(parent == null || parent.getNodeType() != Node.ELEMENT_NODE)
          return false;

        currentName = parent.getNodeName();

        if(currentName == null || !currentName.equals(name[i]))
          return false;

        current = parent;
      }

     return true;
  }

  /**
   * checks to see if the given period delimited distinquished name matches <br>the tree position
   * for the given element without regard for highest parent. This means that<br>
   * any hierarchical sequence in the dom tree that matches the given name <br>will return true.
   * @return true if the array matchs the distinguished name of the element<br>
   * without regard for highest parent.
   *
   */
  public static boolean hasDistinguishedName(Element el, String name)
  {
      String[] narr = parseElementName(name);
      return hasDistinguishedName(el,narr);
  }

  private static String[] parseElementName(String name)
  {
    StringTokenizer t = new StringTokenizer(name,".",false);

    String[] out = new String[t.countTokens()];
    int index = 0;

    while(t.hasMoreTokens())
    {
      out[index++] = t.nextToken();
    }

    return out;
  }

  
  /**
   *  Indicates whether the given node has a parent with the supplied name.
   *  @returns true if the parent is found else returns false.
   */
  public static boolean hasParentNamed(Node n, String name)
  {
    return n.getParentNode().getNodeName().equals(name);
  }

  public static boolean hasChildNamed(Node parent, String childName)
  {
    if(parent == null || !parent.hasChildNodes())
          return false;

    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();
    Node current = null;
    String currentName = null;

    for(int i = 0; i < len; i++ )
    {
      current = nl.item(i);

      if(current != null && current.getNodeType() == Node.ELEMENT_NODE)
      {
        currentName = current.getNodeName();

        if(currentName != null && currentName.equals(childName))
          return true;

      }
    }

    return false;
  }

  /**
   *  Indicates whether the given node has a child of type: <code>Node.TEXT_NODE<code>.<br>
   *  @returns true if the has Text Node child else returns false.
   */
  public static boolean hasTextNodeChild(Element parent)
  {
    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();
    Node child = null;

    for(int i = 0; i < len; i++ )
    {
      child = nl.item(i);

      if(child.getNodeType() == Node.TEXT_NODE)
       return true;
    }

    return false;
  }

   /**
   *  Indicates whether the given Node has the given text String filling a normalized Text node.<br>
   *  @param parent - the parent
   *  @param text - the String to search for
   *  @return true if found, otherwise false
   */
  public static boolean hasText(Element parent, String text)
  {
    return hasText(parent,text,false);
  }

   /**
   *  Indicates whether the given Node has the given text String filling a normalized Text node.<br>
   *  @param parent - the parent
   *  @param text - the String to search for
   *  @param ignoreCase - true if case is to be ignored
   *  @return true if found, otherwise false
   */
  public static boolean hasText(Element parent, String text, boolean ignoreCase)
  {
    if(parent == null || text == null) return false;

    String val = getElementText(parent);
    if(val == null) return false;

    if(ignoreCase)
    {
      val = val.toUpperCase();
      text = text.toUpperCase();
    }

    if(val.equals(text))
    {
        return true;
    }

    return false;
  }

  /**
   *  Indicates whether the given Node has the given text String within the text of
   *  a normalized Text node.<br>
   *  @param parent - the parent
   *  @param text - the String to search for
   *  @return true if found, otherwise false
   */
  public static boolean containsText(Element parent, String text, boolean ignoreCase)
  {
    if(parent == null || text == null) return false;
    
    String val = getElementText(parent);
    if(val == null) return false;

    if(ignoreCase)
    {
      val = val.toUpperCase();
      text = text.toUpperCase();
    }

    int len = text.length();
    int end = val.length() - len;

    if(end < 1 ) return false;

    for(int i = 0; i <= end; i++)
    {
      if(val != null && val.regionMatches(i,text,0,text.length()))
        return true;
    }

    return false;
  }


  public static Element findElementWithText(Document dom, String text)
  {
    return findElementWithText(dom,text,false);
  }


   /**
   *  Finds the Element with the exact given text contained in a child Text Node.<br>
   *  @param dom - the document to search
   *  @param text - the text to find
   *  @return  the Element with the given text
   */
  public static Element findElementWithText(Document dom, String text, boolean ignoreCase)
  {
    if(dom == null || text == null) return null;

    TreeWalker tw = XmlToolKit.getInstance().getElementTreeWalker(dom);
    Node current = null;

    while(true)
    {
      current = tw.nextNode();

      if(current == null)
       break;

      if(current.getNodeType() == Node.ELEMENT_NODE)
      {
        if(hasText((Element)current,text,ignoreCase))
         return (Element)current;

      }
    }

    return null;
  }

 /**
   *  Finds the Element containing the given Case sensitiveString contained in a child Text Node. <br>
   *  @param dom - the document to search
   *  @param text - the text to find
   *  @return  the Element with the given text
   */
  public static Element findElementContainingText(Document dom, String text)
  {
    return findElementContainingText(dom,text,false);
  }


   /**
   *  Finds the Element containing the given String contained in a child Text Node. <br>
   *  @param dom - the document to search
   *  @param text - the text to find
   *  @param ignoreCase - true if case is to be ignored
   *  @return  the Element with the given text
   */
  public static Element findElementContainingText(Document dom, String text, boolean ignoreCase)
  {
    if(dom == null || text == null) return null;

    TreeWalker tw = XmlToolKit.getInstance().getElementTreeWalker(dom);
    Node current = null;

    while(true)
    {
      current = tw.nextNode();

      if(current == null)
       break;

      if(current.getNodeType() == Node.ELEMENT_NODE)
      {
        if(containsText((Element)current,text,ignoreCase))
         return (Element)current;

      }
    }
    return null;
  }

  /**
   *  Indicates whether the given Node has a child Element that does not return true
   *  <br>for the statement node.getNodeType() == Node.ELEMENT_NODE
   *  @param parent - the root of the subtree to search
   *  @return true if found, otherwise false
   */
  public static boolean hasNonElementChild(Node parent)
  {
    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();
    Node child = null;

    for(int i = 0; i < len; i++ )
    {
      child = nl.item(i);

      if(child.getNodeType() != Node.ELEMENT_NODE)
       return true;
    }

    return false;
  }

 /**
  *  gets the first Element child of the given Node with the given name.
  *  @param parent - the parent node
  *  @param name  - the child node name
  *  @returns the named child node if found else returns null.
  */
  public static Node getFirstNamedChild(Node parent, String name)
  {
    if(parent == null || name == null || !parent.hasChildNodes()) return null;

    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();

    for(int i = 0; i < len; i++ )
    {
      String currentName = nl.item(i).getNodeName();

      if(currentName.equals(name))
      return nl.item(i);

    }
    return null;
  }

   /**
  *  gets the first Element child of the given Node with the given name.
  *  @param parent - the parent node
  *  @param name  - the child node name
  *  @returns the named child node if found else returns null.
  */
  public static Node getFirstNamedChildIgnoreCase(Node parent, String name)
  {
    if(parent == null || name == null || !parent.hasChildNodes()) return null;

    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();

    for(int i = 0; i < len; i++ )
    {
      String currentName = nl.item(i).getNodeName();

      if(currentName.equalsIgnoreCase(name))
      return nl.item(i);

    }
    return null;
  }


  /**
   *  Gets the first child of the given parent with the given name whose
   *  supplied attribute has the given value.  <br>
   *  Example: <br> would return the first element found with the name 'Tag' <br>
   *  with attribute - 'name' whose value was 'test'
   *
   *  @return the found node
   *  @param - parent the node at which the search begins
   *  @param - name the name of the Element containing the attribute
   *  @param - attr the name of the attribute
   *  @param - value the value of the attribute
   */
  public static Node getFirstChildWithAttrValue(Node parent, String name, String attr, String value)
  {
    if(!parent.hasChildNodes()) return null;

    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();

    //loop vars
    Node current        = null;
    String currentName  = null;
    String attval       = null;
    NamedNodeMap nnm    = null;
    Node item           = null;

    for(int i = 0; i < len; i++ )
    {

      current = nl.item(i);
      if(current == null) continue;

      currentName = current.getNodeName();
      if(currentName == null) continue;

      if(currentName.equals(name))
      {
          nnm = current.getAttributes();

          item = nnm.getNamedItem(attr);

          if(item != null)
          {
             if(item.getNodeType() == Node.ATTRIBUTE_NODE)
             {
               attval = ((Attr)item).getValue();

               if(attval.equals(value))
                return current;

             }
          }

      }

    }
    return null;
  }

  
  public static Node getFirstChildWithAttrValue(Node parent, String name, String attr, String value, String attr2, String value2)
  {
    if(!parent.hasChildNodes()) return null;

    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();

    //loop vars
    Node current        = null;
    String currentName  = null;
    String attval       = null;
    String attval2       = null;
    NamedNodeMap nnm    = null;
    Node item           = null;
    Node item2           = null;

    for(int i = 0; i < len; i++ )
    {

      current = nl.item(i);
      if(current == null) continue;

      currentName = current.getNodeName();
      if(currentName == null) continue;

      if(currentName.equals(name))
      {
          nnm = current.getAttributes();

          item = nnm.getNamedItem(attr);
          item2 = nnm.getNamedItem(attr2);

          if(item != null)
          {
             if(item.getNodeType() == Node.ATTRIBUTE_NODE)
             {
               attval = ((Attr)item).getValue();
               if (item2 != null){
                   attval2 = ((Attr)item2).getValue();
                   if(attval2.equals(value2) && attval.equals(value))
                       return current;
               }
               else
               {
                   if(attval.equals(value))
                       return current;
               }

             }
          }

      }

    }
    return null;
  }

   /**
   *  Gets the first child of the given parent with the given name with and Attribute with
   *  the given name without regard for the Attribute's value;
   *  @return the found node
   *  @param - parent the node at which the search begins
   *  @param - name the name of the Element containing the attribute
   *  @param - attr the name of the attribute
   */
  public static Node getFirstChildWithAttr(Node parent, String name, String attr)
  {
    if(!parent.hasChildNodes()) return null;

    NodeList nl = parent.getChildNodes();

    int len = nl.getLength();

    //loop vars
    Node current        = null;
    String currentName  = null;
    NamedNodeMap nnm    = null;
    Node item           = null;

    for(int i = 0; i < len; i++ )
    {

      current = nl.item(i);
      if(current == null) continue;

      currentName = current.getNodeName();
      if(currentName == null) continue;

      if(currentName.equals(name))
      {

          nnm = current.getAttributes();

          item = nnm.getNamedItem(attr);

          if(item != null)
          {
             if(item.getNodeType() == Node.ATTRIBUTE_NODE)
             {
                return current;
             }
          }

      }

    }
    return null;
  }


    /**
   *  @param parent - the parent node
   *  @param name  - the child node name
   *  @returns the String value contained by named child node if found else returns null.
   */
  public static String getChildValue(Node parent, String name)
  {
    try
    {
      Node child = getFirstNamedChild(parent,name);
      return child.getFirstChild().getNodeValue();
    }
    catch(NullPointerException npe)
    {
      return null;
    }
  }

     /**
   *  @param parent - the parent node
   *  @param name  - the child node name
   *  @returns the String value contained by named child (Ignoring case) node if found else returns null.
   */
  public static String getChildValueIgnoreCase(Node parent, String name)
  {
    try
    {
      Node child = getFirstNamedChildIgnoreCase(parent,name);
      return child.getFirstChild().getNodeValue();
    }
    catch(NullPointerException npe)
    {
      return null;
    }
  }

  /**
   *  Indicates whether the given Node has a descendant element with the given
   *  name.<br>
   *  @param parent - the root of the subtree to search
   *  @param name - the name of the search target element.
   *  @return true if found, otherwise false
   */
  public static boolean hasDescendant(Node parent, String name)
  {
    if(parent == null || name == null) return false;

    TreeWalker tw = XmlToolKit.getInstance().getElementTreeWalker(parent);
    Node current = null;
    String currentName = null;

    while(true)
    {
      current = tw.nextNode();

      if(current == null)
       break;

      if(current.getNodeType() == Node.ELEMENT_NODE)
      {
        currentName = current.getNodeName();

        if(currentName != null && currentName.equals(name))
         return true;
      }
    }
    return false;

  }



 /**
 *  gets a List of all descendant Elements of the given <code>Node</code> with the given name. <br>
 *  i.e. Seaches the entire subtree. <br>
 *  @param parent - the root of the subtree to search
 *  @param name - the name of the search target <code>Element</code>.
 *  @return a <code>List</code> of all elements found else return an empty List
 */
  public static List getNamedDescendants(Node parent, String name)
  {

    List ret = new ArrayList();

    if(parent == null || name == null) return ret;

    TreeWalker tw = XmlToolKit.getInstance().getElementTreeWalker(parent);
    Node current = null;
    String currentName = null;

    while(true)
    {
      current = tw.nextNode();

      if(current == null)
       break;

      if(current.getNodeType() == Node.ELEMENT_NODE)
      {
        currentName = current.getNodeName();

        if(currentName != null && currentName.equals(name))
         ret.add(current);
      }
    }

    return ret;
  }

  /**
 *  gets a List of all descendant Elements of the given <code>Node</code> with the given name. <br>
 *  i.e. Seaches the entire subtree. <br>
 *  @param parent - the root of the subtree to search
 *  @param name - the name of the search target <code>Element</code>.
 *  @return a <code>List</code> of all elements found else return an empty List
 */
  public static Element getFirstNamedDescendant(Node parent, String name)
  {

    if(parent == null || name == null) return null;

    TreeWalker tw = XmlToolKit.getInstance().getElementTreeWalker(parent);
    Node current = null;
    String currentName = null;

    while(true)
    {
      current = tw.nextNode();

      if(current == null)
       break;

      if(current.getNodeType() == Node.ELEMENT_NODE)
      {
        currentName = current.getNodeName();

        if(currentName != null && currentName.equals(name))
         return (Element)current;
      }
    }

    return null;
  }


  /**
 *  gets a List of all child Elements of the given <code>Node</code> with the given name. <br>
 *  i.e. Seaches only the children of the given <code>Node</code> <br>
 *  @param parent - the root of the subtree to search
 *  @param name - the name of the search target <code>Element</code>.
 *  @return a <code>List</code> of all elements found else return an empty List
 */
  public static List getNamedChildren(Node parent, String name)
  {

    List ch = new ArrayList();
    
    try
    {
      NodeList nl = parent.getChildNodes();

      for(int i = 0; i < nl.getLength(); i++)
      {
          if(nl.item(i).getNodeName().equals(name))
          {
            ch.add(nl.item(i));
          }
      }
    }
    catch(Exception e)
    {
      return ch;
    }

    return ch;
  }

  /**
 *  gets a List of all children given <code>Node</code> with the type Node.ELEMENT_NODE <br>
 *  i.e. Seaches only the children of the given <code>Node</code> <br>
 *  @param parent - the root of the subtree to search
 *  @return a <code>List</code> of all elements found else return an empty List
 */
  public static List getChildElements(Node parent)
  {

    List ch = new ArrayList();
    
    try
    {
      NodeList nl = parent.getChildNodes();

      for(int i = 0; i < nl.getLength(); i++)
      {
          if(nl.item(i).getNodeType() == Node.ELEMENT_NODE)
          {
            ch.add(nl.item(i));
          }
      }
    }
    catch(Exception e)
    {
      return ch;
    }

    return ch;
  }


  public static void moveChildren(Element from, Element to, Document owner) throws Exception
  {
     Node ch = from.getFirstChild();
     Node nh = null;

     if(ch == null) return;

     while(true)
     {
       nh = ch.getNextSibling();
       to.appendChild(ch);

       ch = nh;

       if(ch == null)
        break;
     }

  }

  /**
   *  Gets the text associated with this element node if any.<br>
   *  The method normalizes the subtree of the given element and returns the text
   *  of the first child (the text node).
   *  @returns text or null if none is found
   *  @param the Element container of the Text Nodes.
   */
  public static String getElementText(Element parNode)
  {

    if(parNode != null && parNode.getNodeType() == Node.ELEMENT_NODE)
    {
      parNode.normalize();

      Node text = parNode.getFirstChild();

      if(text != null)
        return text.getNodeValue();
    }

    return null;
  }

  //backward compatibility
  /**
   *  performs the same activities as getElementText. Included for compatibility with
   *  existing code.
   *  @deprecated
   */
  public static String getNodeText(Node parNode)
  {

    if(parNode != null && parNode.getNodeType() == Node.ELEMENT_NODE)
    {
      return getElementText((Element)parNode);
    }

    return null;
  }

  /**
   *  gets value of the named attribute child of the given Node owner.
   *
   *  @returns true if the parent is found else returns false.
   */
  public static String getAttributeValue(Node owner, String attName)
  {
    if(owner != null && owner.getNodeType() == Node.ELEMENT_NODE)
    {
      return ((Element)owner).getAttribute(attName);
    }

    return null;
  }

  /**
   *  Replaces the text in the text element child of this node with the given text.<br>
   *  if no child text element is found one is created with the supplied text.<br>
   *  @param elem - the parent element
   *  @param text - the text to set
   *  @return true if text is set else return false
   */
  public static boolean setElementText(Element elem, String text)
  {
    if(elem != null)
    {
      elem.normalize();

      if(text != null)
      {
        NodeList nl = elem.getChildNodes();

        for(int i = 0; i < nl.getLength(); i++)
        {
            if(nl.item(i).getNodeType() == Node.TEXT_NODE)
            {
               nl.item(i).setNodeValue(text);
               return true;
            }
        }

        appendString(elem,text);
        return true;
      }

    }

    return false;
  }

  /**
   *  Appends the text to the text contained in element child of this Element node <br>
   *  if no child text element is found one is created with the supplied text.<br>
   *  @param elem - the parent element
   *  @param text - the text to set
   */
  public static void appendNormalizedString(Element elem, String data)
  {
    Document doc = elem.getOwnerDocument();

    if(elem != null)
    {
      if(data != null)
      {
        Node nd = doc.createTextNode(data);
        elem.appendChild(nd);
        elem.normalize();
      }
    }
  }

  /**
   *  Appends the text to the text contained in element child of this Element node <br>
   *  if no child text element is found one is created with the supplied text.<br>
   *  @param elem - the parent element
   *  @param text - the text to set
   */
  public static void appendString(Element elem, String data)
  {
    Document doc = elem.getOwnerDocument();

    if(elem != null)
    {
      if(data != null)
      {
        Node nd = doc.createTextNode(data);
        elem.appendChild(nd);
      }
    }
  }

   /**
   *  Tests the given node to see if it a non-null or
   *  @param elem - the parent element
   *  @param text - the text to set
   */
  public static boolean isElement(Node node)
  {
    if(node != null && node.getNodeType() == node.ELEMENT_NODE )
      return true;

    return false;
  }

}
