package com.basis100.xml;


import org.apache.xerces.parsers.DOMParser;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.*;
import org.xml.sax.*;
import java.io.*;
import com.basis100.log.*;
import com.basis100.resources.*;

import javax.xml.parsers.*;


import java.util.*;

public class XercesDocumentBuilder extends DocumentBuilder
{

  private Document dom;
  private boolean errorState = false;
  private String errorMessage;

  public static boolean debug = true;

  public Document parse(InputSource is)throws SAXException, IOException
  {
    DOMParser parser = new DOMParser();

    try
    {
      parser.parse(is);
    }
    catch (SAXException se)
    {
      String msg = "Error: Parsing Failed " + se.getMessage();

    }
    catch (IOException io)
    {
      String msg = "Error: IOException: " + io.getMessage();
    }

    return parser.getDocument();
  }

  public Document parse(File file)throws SAXException, IOException
  {
    FileInputStream input;

    DOMParser parser = new DOMParser();

    if(!file.exists())
    {
      String msg = "Error: The target file was not found: ";
      throw new IOException(msg + " " + file.getName());
    }

    try
    {
      InputSource src = new InputSource(new FileReader(file));
      parser.parse(src);
    }
    catch (SAXException se)
    {
      //String msg = "Error: Parsing Failed " + se.getMessage();
      //throw new SAXException(msg + " " + file);
      //The whole try catch block is pointless and redundant, and it hides
      //the original 'cause' without any other warning. 
      //This kind of coding behavior can also be found everywhere in Express..
      throw se;
    }
    catch (IOException io)
    {
      //String msg = "Error: IOException: " + io.getMessage();
      //throw new IOException(msg + " " + file);
      throw io;
    }

    return parser.getDocument();

  }

  public void setErrorHandler(ErrorHandler eh)
  {

  }

  public void setEntityResolver(EntityResolver er)
  {


  }

  public boolean isValidating()
  {
    return true;
  }

  public boolean isNamespaceAware()
  {
    return true;//????
  }

  public Document newDocument()
  {
    return new DocumentImpl();
  }

  public void debug(String msg)
  {
     if(debug)System.out.println(msg);
  }

  public DOMImplementation getDOMImplementation()
  {
    return dom.getImplementation();
  }

}
