package com.basis100.xml;

//import com.basis100.deal.ingest.*;

/**
 *  Thrown in the event of an XML parsing error
 */

public class FileParseException extends Exception
{
  /**
  * Constructs a <code>FileParseException</code> with the specified message.
  * @param   msg  the related message.
  */
  public FileParseException(String msg)
  {
    super(msg);
  }
}



