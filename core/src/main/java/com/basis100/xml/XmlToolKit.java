package com.basis100.xml;
import org.w3c.dom.*;
import org.w3c.dom.traversal.*;
import org.xml.sax.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import java.io.*;
import javax.xml.parsers.*;

public class XmlToolKit extends DocumentBuilderFactory
{
  private static XmlToolKit instance;

  private String parser;
  private int api = -1;

  private static final int SUN = 1;
  private static final int APACHE = 2;
  // Catherine, 22 Feb 05, 
  // ----------------- define SAX2 parsers -----------------
  private static final int CRIMSONE = 3;
  private static final int ORACLE = 4;
  private static final int AELFRED_XML_READER = 5;  
  private static final int AELFRED_SAX_DRIVER = 6;  
  // ----------------- define SAX2 parsers end -----------------
  
  public XmlToolKit()
  {
    parser = PropertiesCache.getInstance().getInstanceProperty("com.basis100.system.xml.api");
//    parser = PropertiesCache.getInstance().getProperty("com.basis100.system.xml.api");

    if(parser == null) parser = "Xerces";

    if(parser.equalsIgnoreCase("JavaSoft")
        || parser.equalsIgnoreCase("Sun")
        || parser.equals("SunDocumentBuilder") || parser.equals("com.basis100.deal.xml.SunDocumentBuilder") )
    {
      api = SUN;
    }

    if(parser.equalsIgnoreCase("Xerces")
      || parser.equalsIgnoreCase("Apache")
      || parser.equals("XercesDocumentBuilder") || parser.equals("com.basis100.deal.xml.XercesDocumentBuilder"))
    {
      api = APACHE;
    }
  }

  public static synchronized XmlToolKit getInstance()
  {
    if(instance == null)
      instance = new XmlToolKit();

    return instance;
  }

  public void setAttribute(String str, Object o)
  {

  }

    /**
     * temp method for upgrading to jdk1.5
     */
    public boolean getFeature(String uri) {

        // return false for all features.
        return false;
    }
    public void setFeature(String uri, boolean b) {
		// empty for now.
    }

  public Object getAttribute(String str)
  {
    return new Object();
  }

  public DocumentBuilder newDocumentBuilder()
  {
    switch(api)
    {
       case SUN:
        return new SunDocumentBuilder();
       case APACHE:
        return new XercesDocumentBuilder();
    }

    return null;
  }

  public DocumentBuilder sunDocumentBuilder()
  {
     return new SunDocumentBuilder();
  }

  public DocumentBuilder xercesDocumentBuilder()
  {
     return new XercesDocumentBuilder();
  }

  public Parser newSAXParser()
  {
    switch(api)
    {
       case SUN:
        return new com.sun.xml.parser.Parser();
       case APACHE:
        return new org.apache.xerces.parsers.SAXParser();
    }

    return null;
  }

  // -------------------- Catherine Rutgaizer, 22 Feb 05 ---------------------
  public void setSAX2Driver()
  {
    switch (api)
    {
      case SUN:
      {
        System.setProperty("org.xml.sax.driver", "org.apache.xerces.parsers.SAXParser");
        break;
      }  
      case APACHE:
      {  
        System.setProperty("org.xml.sax.driver", "org.apache.xerces.parsers.SAXParser");
        break;
      }  
      case CRIMSONE:
      {
        System.setProperty("org.xml.sax.driver", "org.apache.crimson.parser.XMLReaderImpl");
        break;
      }
      case ORACLE:
      {
        System.setProperty("org.xml.sax.driver", "oracle.xml.parser.v2.SAXParser");
        break;
      }
      case AELFRED_XML_READER:  
      {
        System.setProperty("org.xml.sax.driver", "gnu.xml.aelfred2.XmlReader");
        break;
      }
      case AELFRED_SAX_DRIVER:  
      {
        System.setProperty("org.xml.sax.driver", "gnu.xml.aelfred2.SAXDriver");
        break;
      }
      default: 
      {
        System.setProperty("org.xml.sax.driver", "org.apache.xerces.parsers.SAXParser");
        break;
      }  
    }
  }
  // -------------------- Catherine Rutgaizer, 22 Feb 05 end ---------------------

  public Document newDocument()
  {
    switch(api)
    {
       case SUN:
        return (new SunDocumentBuilder()).newDocument();
       case APACHE:
        return (new XercesDocumentBuilder()).newDocument();
    }

    return null;
  }

  public XmlWriter getXmlWriter()
  {
     return new XmlWriter();
  }

  public TreeWalker getTreeWalker(Node start, NodeFilter filter)
  {
    return new org.apache.xerces.dom.TreeWalkerImpl(start,NodeFilter.SHOW_ALL,filter,false);
  }

  public TreeWalker getTreeWalker(Node start, NodeFilter filter, int whatToShow)
  {
    return new org.apache.xerces.dom.TreeWalkerImpl(start,whatToShow,filter,false);
  }

  public TreeWalker getTreeWalker(Node start, NodeFilter filter, int whatToShow, boolean expEnt)
  {
    return new org.apache.xerces.dom.TreeWalkerImpl(start,whatToShow,filter,expEnt);
  }

  public TreeWalker getTreeWalker(Node start)
  {
    NodeFilter nf = new DefaultFilter();
    return new org.apache.xerces.dom.TreeWalkerImpl(start,NodeFilter.SHOW_ALL,nf,false);
  }

  public TreeWalker getElementTreeWalker(Node start)
  {
    NodeFilter nf = new NonElementNodeFilter();
    return new org.apache.xerces.dom.TreeWalkerImpl(start,NodeFilter.SHOW_ELEMENT,nf,false);
  }

  public TreeWalker getFilteredTreeWalker(Node start, NodeFilter filter)
  {
    return new org.apache.xerces.dom.TreeWalkerImpl(start,NodeFilter.SHOW_ALL,filter,false);
  }

  class DefaultFilter implements NodeFilter
  {
     public short acceptNode(Node node)
     {
       return NodeFilter.FILTER_ACCEPT;
     }
  }

  class NonElementNodeFilter implements NodeFilter
  {
    public short acceptNode(Node n)
    {
      if(n.getNodeType() == Node.ELEMENT_NODE)
      {
        return NodeFilter.FILTER_ACCEPT;
      }

    return NodeFilter.FILTER_REJECT;
  }

}
}
