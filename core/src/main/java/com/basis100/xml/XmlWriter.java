package com.basis100.xml;
/**
 * 08/Apr/2005 DVG #DG178 #1187 Doc prep - in XML file & is converted to &amp; - which shows
 */
import java.io.*;
import org.w3c.dom.*;

public class XmlWriter
{
    private PrintWriter out;

    private String enc = "UTF-8";

    public static final int UNFORMATTED = 0;
    public static final int TAB_FORMATTED = 1;
    public static final int NORMALIZED_ONLY = 2;

    public static boolean printEmptyElements = false;

    private int formatLevel;

    private static final char CR = '\n';
    private static final char TAB = '\t';

    public XmlWriter() {}

    public String printString(Document doc)
    {
       StringWriter writer = new StringWriter();
       print(writer,doc);
       return writer.toString();
    }

    public String printString(Element elem)
    {
       StringWriter writer = new StringWriter();
       print(writer,elem);
       return writer.toString();
    }

    public String printFormattedString(Document doc)
    {
       StringWriter writer = new StringWriter();
       print(writer,doc,TAB_FORMATTED);
       return writer.toString();
    }

    public void print(Writer writer, Document doc)
    {
      out = new PrintWriter(writer);
      printNode(doc,0);
    }

    public void print(Writer writer, Element elem)
    {
      out = new PrintWriter(writer);
      printNode(elem,0);
    }

    public void print(Writer writer, Document doc, int format)
    {
      formatLevel = format;
      out = new PrintWriter(writer);
      printNode(doc,0);
    }

    public void print(OutputStream str, String encoding, Document doc, int format)
    throws UnsupportedEncodingException
    {
      formatLevel = format;
      OutputStreamWriter wr = new OutputStreamWriter(str,encoding);
      enc = encoding;
      out = new PrintWriter(wr);
      printNode(doc,0);
    }

    private void printNode(Node node, int tabs)
    {

        if(node == null)
          return;

        int type = node.getNodeType();


        switch ( type )
        {

          case Node.DOCUMENT_NODE:
          {

/*            String encode = "UTF-8";
            if(!enc.equalsIgnoreCase(encode))
            {
               enc = "UTF-16";
            }
*/

            if(formatLevel != NORMALIZED_ONLY)
            out.print("<?xml version=\"1.0\" encoding=\""+ enc + "\"?>");

            if(formatLevel == TAB_FORMATTED) out.print(CR);

            NodeList children = node.getChildNodes();

            int len = children.getLength();

            for ( int iChild = 0; iChild < len; iChild++ )
            {
              printNode(children.item(iChild),tabs);
            }

            out.flush();

            break;
          }


          case Node.ELEMENT_NODE:
          {
              String indent = "";

              if(formatLevel == TAB_FORMATTED)
              {
                out.print(CR);

                for(int i = 0; i < tabs; i++)
                {
                  indent += TAB;
                }

                out.print(indent);
              }

              out.print('<');

              out.print(node.getNodeName());

              NamedNodeMap map = node.getAttributes();

              int attlen = (map != null) ? map.getLength() : 0;

              for ( int i = 0; i < attlen; i++ )
              {
                  Attr attr = (Attr)map.item(i);
                  out.print(' ');
                  out.print(attr.getNodeName());
                  out.print("=\"");
                  out.print(putEntities(attr.getNodeValue()));
                  out.print('"');
              }

              NodeList children = node.getChildNodes();
              int len = -1;

              if ( children != null )
              {
                 len = children.getLength();

                 if ( len == 0 && printEmptyElements)
                 {
                   out.print("/>");
                   break;
                 }
                 else
                 {
                   out.print('>');
                 }
              }

              boolean els = hasElementNodes(children);

              if(els)tabs++;

              if (len != -1)
              {
                  Node current = null;

                  for ( int i = 0; i < len; i++ )
                  {
                     current = children.item(i);

                     if(formatLevel == NORMALIZED_ONLY)
                     {
                       printNode(current,tabs);
                     }
                     else if(formatLevel == TAB_FORMATTED || formatLevel == UNFORMATTED)
                     {
                       //don't print textnodes for parents with element children
                       if(!((current.getNodeType() == Node.TEXT_NODE) && els ))
                       {
                         printNode(current,tabs);
                       }
                     }

                  } //end for
              }

              if(len != 0 || !printEmptyElements)
              {
                if(els && formatLevel == TAB_FORMATTED)
                {
                  out.print(CR);
                  out.print(indent);
                }

                out.print("</");
                out.print(node.getNodeName());
                out.print('>');
              }

              break;
          }


          case Node.ENTITY_REFERENCE_NODE:
          {
              NodeList children = node.getChildNodes();

              if ( children != null )
              {
                int len = children.getLength();

                for ( int i = 0; i < len; i++ )
                {
                    printNode(children.item(i),tabs);
                }
              }
                break;
            }

          case Node.CDATA_SECTION_NODE:
          {
            out.print("<![CDATA[");
            out.print(node.getNodeValue());
            out.print("]]>");

            break;
          }

          case Node.TEXT_NODE:
          {
            out.print(putEntities(node.getNodeValue()));
            break;
          }

          case Node.PROCESSING_INSTRUCTION_NODE:
          {
            out.print("<?");

            out.print(node.getNodeName());

            String data = node.getNodeValue();

            if ( data != null && data.length() > 0 )
            {
              out.print(' ');
              out.print(data);
            }
            out.println("?>");
            break;
          }
        }

        out.flush();
    }


    private boolean hasElementNodes(NodeList children)
    {
      if ( children != null )
      {
         int len = children.getLength();

         for ( int i = 0; i < len; i++ )
         {
           if(children.item(i).getNodeType() == Node.ELEMENT_NODE)
            return true;
         }
      }

      return false;
    }

    /**
     *  if true element tags with null or empty content will print as <elementName/>
     *  if false then print as <elementName><elementName/>
     */

    public void setPrintEmptyElements(boolean tf)
    {
      this.printEmptyElements = tf;
    }

    /** #DG178
     * if it has already (apparently) been quoted just leave it
     */
    static final char [] lt = {'&','l','t',';'};
    static final char [] gt = {'&','g','t',';'};
    static final char [] amp = {'&','a','m','p',';'};
    static final char [] quot = {'&','q','u','o','t',';'};
    static final char [][] allSpecial = {lt, gt, amp, quot};
    static boolean isQuotedChar(char[] inp, int st) {
      // search
      for (int i = 0; i < allSpecial.length; i++) {
        char[] tok = allSpecial[i];
        for (int j = 1, p = st; p < inp.length; j++, p++) { // from 1 since the first is known: '&'
          if (j >= tok.length)
            return true;
          if (Character.toLowerCase(inp[p]) != tok[j])
            break;
        }
      }
      return false;
    }

    public static String putEntities(String text)
    {

        StringBuffer buf = new StringBuffer();

        if(text == null)
         return text;

        char[] in = text.toCharArray();

        int len = in.length;

        for ( int i = 0; i < len; i++ )
        {
          char ch = in[i];

          switch(ch)
          {
            case '<':
              buf.append(lt);
              break;

            case '>':
              buf.append(gt);
              break;

            case '"':
              buf.append(quot);
              break;

            case '&':
              if (!isQuotedChar(in, i+1)) { //#DG178 rewritten
                buf.append(amp);
                break;
              } // if not just add char below

            default:  buf.append(ch);
          }
      }

      return(buf.toString());

    }

    public String getEncoding() { return enc; }
    public void setEncoding(String str) { enc = str; }
}
