package config;

import java.io.*;
import java.util.*;
import javax.xml.bind.*;
import javax.xml.bind.util.*;

import config.bindings.*;

import com.basis100.resources.*;
import com.basis100.log.*;

/**
 * Title:       DocTrackingDetailsConfigManager
 * Description: Manage and provide helper method for Document Tracking Details Screen
 * Copyright:   Copyright (c) 2003
 * Company:     Basis100
 * @author      Vladimir Ivanov
 * @version     1.0 (Initial Version Created on April 22, 2003)
 */

public class DocTrackingDetailsConfigManager {

  public static final String CONFIG_FILE = "DocTrackingDetailsConfig.xml";
  public static final String CTX =  "config.bindings";
  public static final String DISABLED = "disabled";
  public static final String NOTDISPLAY = "notDisplay";
  public static final String HIDDENTAG = "<font color=\"#808080\" size=\"2\">--Hidden--</font>";

  //The Global instance of DocTrackingDetailsConfigManager - will be init only once when the first time use it
  private static DocTrackingDetailsConfig theDocConfig;
  private static boolean init = false;

  //Local members
  private int userTypeId = -1;
  private HashSet disableFieldSet = null;
  private HashSet hiddenFieldSet = null;

  //Local members
  public SysLogger logger;

  public static synchronized DocTrackingDetailsConfigManager getInstance(SysLogger logger, int userTypeId)
  {
    DocTrackingDetailsConfigManager theInstance = new DocTrackingDetailsConfigManager(logger);
    theInstance.setup(userTypeId);
    return theInstance;
  }

  private DocTrackingDetailsConfigManager(SysLogger logger)
  {
    //Set the logger
    if(logger != null)
    {
      this.logger = logger;
    }
    else
    {
      logger = new SysLogger("DTDCM");
    }

    //Check if already inited
    if(init == false)
    {
      try
      {
        this.logger.info("@DocTrackingDetailsConfigManager :: Start to cache the DocTrackingDetailsConfigManager Objects ...");
        JAXBContext ctx = JAXBContext.newInstance(CTX);
        logger.debug("@DocTrackingDetailsConfigManager :: Got configuration context for: " + CTX + " - " + ctx);
        Unmarshaller un = ctx.createUnmarshaller();

        if(un != null)
        {
            theDocConfig = (DocTrackingDetailsConfig)un.unmarshal(this.getClass().getClassLoader().getResourceAsStream(CONFIG_FILE));
        }
        else
        {
          //Fall back to set NO config needed
          theDocConfig = null;
        }
      }
      catch(Exception e)
      {
        logger.warning("Exception @DocTrackingDetailsConfigManager() when create first instance:: " + e);
        //--> Assume no configuration needed ==> display all field as normal
        theDocConfig = null;
      }
      init = true;
    }
  }


  /**
  //Method to populate the Disable and Hidden Field Maps
  public void setup(int userTypeId)
  {
    this.userTypeId = userTypeId;
    //Do nothing if no configuration
    if(theDocConfig == null) return;

    disableFieldSet = null;
    hiddenFieldSet = null;

    java.util.List theDisableFList = theDocConfig.getDisableFields();
    logger.debug("Total # of Disable Fields List : " + theDisableFList.size() +
      " Setup for UserTypeId : " + userTypeId);

    int x = 0;
    int toUserType = -1;
    int fromUserType = -1;
    for(Iterator i = theDisableFList.iterator(); i.hasNext(); )
    {
      x++;
      DocTrackingDetailsConfig.DisableFieldsType disableFields = (DocTrackingDetailsConfig.DisableFieldsType)i.next();
      logger.debug("DisableFields["+x+"]: Description = " + disableFields.getDescription());
      fromUserType = disableFields.getUserTypeIdFrom();
      toUserType = disableFields.getUserTypeIdTo();
      logger.debug("DisableFields["+x+"]: UserTypeIdFrom = " + fromUserType);
      logger.debug("DisableFields["+x+"]: UserTypeIdTo = " + toUserType);

      //Check if userType in the range
      if(toUserType < 0 || toUserType < fromUserType)
        toUserType = fromUserType;
      if(userTypeId >= fromUserType && userTypeId <= toUserType)
      {
        java.util.List theFields = disableFields.getField();
        logger.debug("DisableFields["+x+"]: # of fileds to disable = " + theFields.size());
        for(Iterator ii = theFields.iterator(); ii.hasNext(); )
        {
          DocTrackingDetailsConfig.DisableFieldsType.FieldType oneField =
              (DocTrackingDetailsConfig.DisableFieldsType.FieldType) ii.next();
          if(oneField.getType().equalsIgnoreCase(DISABLED))
          {
            //Set Disable Field
            logger.debug("Disable Field : Name = " + oneField.getName());
            if(disableFieldSet == null)
              disableFieldSet = new HashSet();
            disableFieldSet.add(oneField.getName());
          }
          else
          {
            //Set Hidden Field
            logger.debug("Hidden Field : Name = " + oneField.getName());
            if(hiddenFieldSet == null)
              hiddenFieldSet = new HashSet();
            hiddenFieldSet.add(oneField.getName());
          }
        }//for
        //Stop searching once one record found
        return;
      }//if
    }//for
  }//setup
  **/
////////////
  //Method to populate the Disable and Hidden Field Maps
  public void setup(int userTypeId)
  {
    this.userTypeId = userTypeId;
    //Do nothing if no configuration
    if(theDocConfig == null) return;

    disableFieldSet = null;
    hiddenFieldSet = null;
    java.util.List theDisableFList = theDocConfig.getDisableFields();
    logger.debug("Total # of Disable Fields List : " + theDisableFList.size() +
      " Setup for UserTypeId : " + userTypeId);

    int x = 0;
    int toUserType = -1;
    int fromUserType = -1;
    for(Iterator i = theDisableFList.iterator(); i.hasNext(); )
    {
      x++;
      DisableFieldsType disableFields = (DisableFieldsType)i.next();
      logger.debug("DisableFields["+x+"]: Description = " + disableFields.getDescription());
      fromUserType = disableFields.getUserTypeIdFrom();
      toUserType = disableFields.getUserTypeIdTo();
      logger.debug("DisableFields["+x+"]: UserTypeIdFrom = " + fromUserType);
      logger.debug("DisableFields["+x+"]: UserTypeIdTo = " + toUserType);

      //Check if userType in the range
      if(toUserType < 0 || toUserType < fromUserType)
        toUserType = fromUserType;
      if(userTypeId >= fromUserType && userTypeId <= toUserType)
      {
        java.util.List theFields = disableFields.getField();
        logger.debug("DisableFields["+x+"]: # of fileds to disable = " + theFields.size());
        for(Iterator ii = theFields.iterator(); ii.hasNext(); )
        {
          FieldType oneField =
              (FieldType) ii.next();
          if(oneField.getType().equalsIgnoreCase(DISABLED))
          {
            //Set Disable Field
            logger.debug("Disable Field : Name = " + oneField.getName());
            if(disableFieldSet == null)
              disableFieldSet = new HashSet();
            disableFieldSet.add(oneField.getName());
          }
          else
          {
            //Set Hidden Field
            logger.debug("Hidden Field : Name = " + oneField.getName());
            if(hiddenFieldSet == null)
              hiddenFieldSet = new HashSet();
            hiddenFieldSet.add(oneField.getName());
          }
        }//for
        //Stop searching once one record found
        return;
      }//if
    }//for
  }//setup

///////////

  public int getUserTypeId()
  {
    return this.userTypeId;
  }

  public boolean isFieldDisable(String fieldName)
  {
    if(disableFieldSet == null) return false;
    return disableFieldSet.contains(fieldName);
  }

  public boolean isFieldHidden(String fieldName)
  {
    if(hiddenFieldSet == null) return false;
    return hiddenFieldSet.contains(fieldName);
  }

   public boolean isFieldDisableOrHidden(String fieldName)
  {
    return(isFieldHidden(fieldName) || isFieldDisable(fieldName));
  }

  public Iterator getAllDisableFields()
  {
    if(disableFieldSet == null) return null;
    return disableFieldSet.iterator();
  }

  public Iterator getAllHiddenFields()
  {
    if(disableFieldSet == null) return null;
    return hiddenFieldSet.iterator();
  }

}