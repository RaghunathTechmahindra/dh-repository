package config;

import java.io.*;
import java.util.*;
import javax.xml.bind.*;
import javax.xml.bind.util.*;

import config.bindings.*;
import com.basis100.resources.*;
import com.basis100.log.*;

/**
 * Title:       UserAdminConfigManager
 * Description: Manage and provide helper method for User Admin Configuration Purposes
 * Copyright:   Copyright (c) 2003
 * Company:     Basis100
 * @author      Billy Lam
 * @version     1.0 (Initial Version Created on 27Feb2003)
 */

public class UserAdminConfigManager {

  public static final String CONFIG_FILE = "AdminScreenConfig.xml";
  public static final String CTX =  "config.bindings";

  //Definitions for Field disable/hidden
  public static final String DISABLED = "disabled";
  public static final String NOTDISPLAY = "notDisplay";
  public static final String HIDDENTAG = "<font color=\"#808080\" size=\"2\">--Hidden--</font>";

  //Definitions for Auditable field
  // Static Definitions
  // Event type definitions
  public final static int UA_EVENT_TYPE_UPDATE = 0;
  public final static int UA_EVENT_TYPE_CREATE = 1;
  public final static int UA_EVENT_TYPE_DELETE = 2;
  // Aduit Type definition
  public static final String ALL = "all";
  public static final String UPDATE = "update";
  public static final String CREATE = "create";
  public static final String DELETE = "delete";

  //The Global instance of UserAdminConfig - will be init only once when the first time use it
  private static UserAdminConfig theConfig;
  private static boolean init = false;

  //Local members
  private int userTypeId = -1;
  //Cache Maps for Field Disable/Hidden
  private HashSet disableFieldSet = null;
  private HashSet hiddenFieldSet = null;
  //Cache Maps for Field Disable/Hidden
  private static HashSet auditUpdateFieldSet = null;
  private static HashSet auditCreateFieldSet = null;
  private static HashSet auditDeleteFieldSet = null;  //Not supported yet !!

  //Local members
  public SysLogger logger;

  public static synchronized UserAdminConfigManager getInstance(SysLogger logger, int userTypeId)
  {
    UserAdminConfigManager theInstance = new UserAdminConfigManager(logger);
    theInstance.setup(userTypeId);
    return theInstance;
  }

  private UserAdminConfigManager(SysLogger logger)
  {
    //Set the logger
    if(logger != null)
    {
      this.logger = logger;
    }
    else
    {
      logger = new SysLogger("UACM");
    }

    //Check if already inited
    if(init == false)
    {
      try
      {
        this.logger.info("@UserAdminConfigManager :: Start to cache the UserAdminConfig Objects ...");
        JAXBContext ctx = JAXBContext.newInstance(CTX);
        logger.debug("@UserAdminConfigManager :: Got configuration context for: " + CTX + " - " + ctx);
        Unmarshaller un = ctx.createUnmarshaller();

        if(un != null)
        {
          //String pathOfConfig = Config.getProperty("SYS_PROPERTIES_LOCATION") + CONFIG_FILE;
          //logger.debug("@UserAdminConfigManager :: the Path of Config File = " + pathOfConfig);
          theConfig = (UserAdminConfig)un.unmarshal(this.getClass().getClassLoader().getResourceAsStream(CONFIG_FILE));
        }
        else
        {
          //Fall back to set NO config needed
          theConfig = null;
        }

        //Perform setup for Auditable fields
        if(theConfig != null)
        {
          java.util.List theAFields = theConfig.getAuditableFields().getField();

          logger.debug("@UserAdminConfigManager.init :: # of fileds to audit = " + theAFields.size());
          int z = 0;
          for(Iterator ii = theAFields.iterator(); ii.hasNext(); )
          {
            z++;
            AFieldType oneField = (AFieldType) ii.next();
            if(oneField.getType().equalsIgnoreCase(ALL))
            {
              logger.debug("Auditable Field (all) : Name = " + oneField.getName());
              //Added the field to all Auditable maps
              if(auditUpdateFieldSet == null) auditUpdateFieldSet = new HashSet();
              auditUpdateFieldSet.add(oneField.getName());
              if(auditCreateFieldSet == null) auditCreateFieldSet = new HashSet();
              auditCreateFieldSet.add(oneField.getName());
              if(auditDeleteFieldSet == null) auditDeleteFieldSet = new HashSet();
              auditDeleteFieldSet.add(oneField.getName());
            }
            else if(oneField.getType().equalsIgnoreCase(UPDATE))
            {
              logger.debug("Auditable Field (update) : Name = " + oneField.getName());
              //Added the field to the Update map
              if(auditUpdateFieldSet == null) auditUpdateFieldSet = new HashSet();
              auditUpdateFieldSet.add(oneField.getName());
            }
            else if(oneField.getType().equalsIgnoreCase(CREATE))
            {
              logger.debug("Auditable Field (create) : Name = " + oneField.getName());
              //Added the field to the create map
              if(auditCreateFieldSet == null) auditCreateFieldSet = new HashSet();
              auditCreateFieldSet.add(oneField.getName());
            }
            else if(oneField.getType().equalsIgnoreCase(DELETE))
            {
              logger.debug("Auditable Field (delete) : Name = " + oneField.getName());
              //Added the field to the delete maps
              if(auditDeleteFieldSet == null) auditDeleteFieldSet = new HashSet();
              auditDeleteFieldSet.add(oneField.getName());
            }
          }
        }
      }
      catch(Exception e)
      {
        logger.warning("Excepotion @UserAdminConfigManager() when create first instance:: " + e);
        //--> Assume no configuration needed ==> display all field as normal
        theConfig = null;
      }
      init = true;
    }
  }

  //Method to populate the Disable and Hidden Field Maps
  public void setup(int userTypeId)
  {
    this.userTypeId = userTypeId;
    //Do nothing if no configuration
    if(theConfig == null) return;

    disableFieldSet = null;
    hiddenFieldSet = null;
    java.util.List theDisableFList = theConfig.getDisableFields();
    logger.debug("Total # of Disable Fields List : " + theDisableFList.size() +
      " Setup for UserTypeId : " + userTypeId);

    int x = 0;
    int toUserType = -1;
    int fromUserType = -1;
    for(Iterator i = theDisableFList.iterator(); i.hasNext(); )
    {
      x++;
      DisableFieldsType disableFields = (DisableFieldsType)i.next();
      logger.debug("DisableFields["+x+"]: Description = " + disableFields.getDescription());
      fromUserType = disableFields.getUserTypeIdFrom();
      toUserType = disableFields.getUserTypeIdTo();
      logger.debug("DisableFields["+x+"]: UserTypeIdFrom = " + fromUserType);
      logger.debug("DisableFields["+x+"]: UserTypeIdTo = " + toUserType);

      //Check if userType in the range
      if(toUserType < 0 || toUserType < fromUserType)
        toUserType = fromUserType;
      if(userTypeId >= fromUserType && userTypeId <= toUserType)
      {
        java.util.List theFields = disableFields.getField();
        logger.debug("DisableFields["+x+"]: # of fileds to disable = " + theFields.size());
        for(Iterator ii = theFields.iterator(); ii.hasNext(); )
        {
          FieldType oneField =
              (FieldType) ii.next();
          if(oneField.getType().equalsIgnoreCase(DISABLED))
          {
            //Set Disable Field
            logger.debug("Disable Field : Name = " + oneField.getName());
            if(disableFieldSet == null)
              disableFieldSet = new HashSet();
            disableFieldSet.add(oneField.getName());
          }
          else
          {
            //Set Hidden Field
            logger.debug("Hidden Field : Name = " + oneField.getName());
            if(hiddenFieldSet == null)
              hiddenFieldSet = new HashSet();
            hiddenFieldSet.add(oneField.getName());
          }
        }//for
        //Stop searching once one record found
        return;
      }//if
    }//for
  }//setup

  public int getUserTypeId()
  {
    return this.userTypeId;
  }

  public boolean isFieldDisable(String fieldName)
  {
    if(disableFieldSet == null) return false;
    return disableFieldSet.contains(fieldName);
  }

  public boolean isFieldHidden(String fieldName)
  {
    if(hiddenFieldSet == null) return false;
    return hiddenFieldSet.contains(fieldName);
  }

   public boolean isFieldDisableOrHidden(String fieldName)
  {
    return(isFieldHidden(fieldName) || isFieldDisable(fieldName));
  }

  public Iterator getAllDisableFields()
  {
    if(disableFieldSet == null) return null;
    return disableFieldSet.iterator();
  }

  public Iterator getAllHiddenFields()
  {
    if(disableFieldSet == null) return null;
    return hiddenFieldSet.iterator();
  }

  //Methods for Feild Auditable
  public Iterator getAllUpdateAuditableFields()
  {
    if(auditUpdateFieldSet == null) return null;
    return auditUpdateFieldSet.iterator();
  }

  public Iterator getAllCreateAuditableFields()
  {
    if(auditCreateFieldSet == null) return null;
    return auditCreateFieldSet.iterator();
  }

  public Iterator getAllDeleteAuditableFields()
  {
    if(auditDeleteFieldSet == null) return null;
    return auditDeleteFieldSet.iterator();
  }

  public boolean isFieldUpdateAuditable(String fieldName)
  {
    if(auditUpdateFieldSet == null) return false;
    return auditUpdateFieldSet.contains(fieldName);
  }

  public boolean isFieldCreateAuditable(String fieldName)
  {
    if(auditCreateFieldSet == null) return false;
    return auditCreateFieldSet.contains(fieldName);
  }

  public boolean isFieldDeleteAuditable(String fieldName)
  {
    if(auditDeleteFieldSet == null) return false;
    return auditDeleteFieldSet.contains(fieldName);
  }

  public boolean isFieldAuditable(String fieldName, int type)
  {
    switch (type)
    {
      case UA_EVENT_TYPE_CREATE:
        return isFieldCreateAuditable(fieldName);
      case UA_EVENT_TYPE_DELETE:
        return isFieldDeleteAuditable(fieldName);
      default:
        return isFieldUpdateAuditable(fieldName);
    }
  }

}