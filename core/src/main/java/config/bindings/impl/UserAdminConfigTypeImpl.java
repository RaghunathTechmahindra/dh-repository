//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v1.0 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2004.02.11 at 10:37:37 EST 
//


package config.bindings.impl;

public class UserAdminConfigTypeImpl implements config.bindings.UserAdminConfigType, com.sun.xml.bind.unmarshaller.UnmarshallableObject, com.sun.xml.bind.serializer.XMLSerializable, com.sun.xml.bind.validator.ValidatableObject
{

    protected config.bindings.AuditableFieldsType _AuditableFields;
    protected com.sun.xml.bind.util.ListImpl _DisableFields = new com.sun.xml.bind.util.ListImpl(new java.util.ArrayList());
    private final static com.sun.msv.grammar.Grammar schemaFragment = com.sun.xml.bind.validator.SchemaDeserializer.deserialize("\u00ac\u00ed\u0000\u0005sr\u0000\u001fcom.sun.msv.grammar.SequenceExp\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0000xr\u0000\u001dcom.sun.msv.grammar.BinaryExp\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0002L\u0000\u0004exp1t\u0000 Lcom/sun/msv/grammar/Expression;L\u0000\u0004exp2q\u0000~\u0000\u0002xr\u0000\u001ecom.sun.msv.grammar.Expression\u00f8\u0018\u0082\u00e8N5~O\u0002\u0000\u0003I\u0000\u000ecachedHashCodeL\u0000\u0013epsilonReducibilityt\u0000\u0013Ljava/lang/Boolean;L\u0000\u000bexpandedExpq\u0000~\u0000\u0002xp\u0000w\u00e4\fppsr\u0000\u001dcom.sun.msv.grammar.ChoiceExp\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0000xq\u0000~\u0000\u0001\u0000;\u00f2\u0005ppsr\u0000 com.sun.msv.grammar.OneOrMoreExp\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0000xr\u0000\u001ccom.sun.msv.grammar.UnaryExp\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0001L\u0000\u0003expq\u0000~\u0000\u0002xq\u0000~\u0000\u0003\u0000;\u00f1\u00fasr\u0000\u0011java.lang.Boolean\u00cd r\u0080\u00d5\u009c\u00fa\u00ee\u0002\u0000\u0001Z\u0000\u0005valuexp\u0000psr\u0000\'com.sun.msv.grammar.trex.ElementPattern\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0001L\u0000\tnameClasst\u0000\u001fLcom/sun/msv/grammar/NameClass;xr\u0000\u001ecom.sun.msv.grammar.ElementExp\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0002Z\u0000\u001aignoreUndeclaredAttributesL\u0000\fcontentModelq\u0000~\u0000\u0002xq\u0000~\u0000\u0003\u0000;\u00f1\u00f7q\u0000~\u0000\fp\u0000sq\u0000~\u0000\r\u0000;\u00f1\u00ecpp\u0000sq\u0000~\u0000\u0006\u0000;\u00f1\u00e1ppsq\u0000~\u0000\b\u0000;\u00f1\u00d6q\u0000~\u0000\fpsr\u0000 com.sun.msv.grammar.AttributeExp\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0002L\u0000\u0003expq\u0000~\u0000\u0002L\u0000\tnameClassq\u0000~\u0000\u000exq\u0000~\u0000\u0003\u0000;\u00f1\u00d3q\u0000~\u0000\fpsr\u00002com.sun.msv.grammar.Expression$AnyStringExpression\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0000xq\u0000~\u0000\u0003\u0000\u0000\u0000\bsq\u0000~\u0000\u000b\u0001q\u0000~\u0000\u0017sr\u0000 com.sun.msv.grammar.AnyNameClass\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0000xr\u0000\u001dcom.sun.msv.grammar.NameClass\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0000xpsr\u00000com.sun.msv.grammar.Expression$EpsilonExpression\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0000xq\u0000~\u0000\u0003\u0000\u0000\u0000\tq\u0000~\u0000\u0018psr\u0000#com.sun.msv.grammar.SimpleNameClass\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0002L\u0000\tlocalNamet\u0000\u0012Ljava/lang/String;L\u0000\fnamespaceURIq\u0000~\u0000\u001fxq\u0000~\u0000\u001at\u0000!config.bindings.DisableFieldsTypet\u0000+http://java.sun.com/jaxb/xjc/dummy-elementssq\u0000~\u0000\u001et\u0000\rDisableFieldst\u0000\u0000q\u0000~\u0000\u001dsq\u0000~\u0000\u0006\u0000;\u00f2\u0002ppsq\u0000~\u0000\r\u0000;\u00f1\u00f7q\u0000~\u0000\fp\u0000sq\u0000~\u0000\r\u0000;\u00f1\u00ecpp\u0000sq\u0000~\u0000\u0006\u0000;\u00f1\u00e1ppsq\u0000~\u0000\b\u0000;\u00f1\u00d6q\u0000~\u0000\fpsq\u0000~\u0000\u0014\u0000;\u00f1\u00d3q\u0000~\u0000\fpq\u0000~\u0000\u0017q\u0000~\u0000\u001bq\u0000~\u0000\u001dsq\u0000~\u0000\u001et\u0000#config.bindings.AuditableFieldsTypeq\u0000~\u0000\"sq\u0000~\u0000\u001et\u0000\u000fAuditableFieldsq\u0000~\u0000%q\u0000~\u0000\u001dsr\u0000\"com.sun.msv.grammar.ExpressionPool\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0001\u0002\u0000\u0001L\u0000\bexpTablet\u0000/Lcom/sun/msv/grammar/ExpressionPool$ClosedHash;xpsr\u0000-com.sun.msv.grammar.ExpressionPool$ClosedHash\u00d7j\u00d0N\u00ef\u00e8\u00ed\u001c\u0002\u0000\u0004I\u0000\u0005countI\u0000\tthresholdL\u0000\u0006parentq\u0000~\u00001[\u0000\u0005tablet\u0000![Lcom/sun/msv/grammar/Expression;xp\u0000\u0000\u0000\b\u0000\u0000\u00009pur\u0000![Lcom.sun.msv.grammar.Expression;\u00d68D\u00c3]\u00ad\u00a7\n\u0002\u0000\u0000xp\u0000\u0000\u0000\u00bfppppppppppppppppppppppppppppppppppppppppppppppq\u0000~\u0000\u0013q\u0000~\u0000*pppppppppq\u0000~\u0000\u0012q\u0000~\u0000)pppppppppppppppppppppppq\u0000~\u0000\npppppppq\u0000~\u0000&ppq\u0000~\u0000\u0007ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppq\u0000~\u0000\u0005pp");

    private final static java.lang.Class PRIMARY_INTERFACE_CLASS() {
        return config.bindings.UserAdminConfigType.class;
    }

    public config.bindings.AuditableFieldsType getAuditableFields() {
        return _AuditableFields;
    }

    public void setAuditableFields(config.bindings.AuditableFieldsType value) {
        _AuditableFields = value;
    }

    public java.util.List getDisableFields() {
        return _DisableFields;
    }

    public com.sun.xml.bind.unmarshaller.ContentHandlerEx getUnmarshaller(com.sun.xml.bind.unmarshaller.UnmarshallingContext context) {
        return new config.bindings.impl.UserAdminConfigTypeImpl.Unmarshaller(context);
    }

    public java.lang.Class getPrimaryInterfaceClass() {
        return PRIMARY_INTERFACE_CLASS();
    }

    public void serializeElements(com.sun.xml.bind.serializer.XMLSerializer context)
        throws org.xml.sax.SAXException
    {
        int idx2 = 0;
        final int len2 = _DisableFields.size();
        while (idx2 != len2) {
            if (_DisableFields.get(idx2) instanceof javax.xml.bind.Element) {
                context.childAsElements(((com.sun.xml.bind.serializer.XMLSerializable) _DisableFields.get(idx2 ++)));
            } else {
                context.startElement("", "DisableFields");
                int idx_0 = idx2;
                context.childAsAttributes(((com.sun.xml.bind.serializer.XMLSerializable) _DisableFields.get(idx_0 ++)));
                context.endAttributes();
                context.childAsElements(((com.sun.xml.bind.serializer.XMLSerializable) _DisableFields.get(idx2 ++)));
                context.endElement();
            }
        }
        if (_AuditableFields!= null) {
            if (_AuditableFields instanceof javax.xml.bind.Element) {
                context.childAsElements(((com.sun.xml.bind.serializer.XMLSerializable) _AuditableFields));
            } else {
                context.startElement("", "AuditableFields");
                context.childAsAttributes(((com.sun.xml.bind.serializer.XMLSerializable) _AuditableFields));
                context.endAttributes();
                context.childAsElements(((com.sun.xml.bind.serializer.XMLSerializable) _AuditableFields));
                context.endElement();
            }
        }
    }

    public void serializeAttributes(com.sun.xml.bind.serializer.XMLSerializer context)
        throws org.xml.sax.SAXException
    {
        final int len2 = _DisableFields.size();
    }

    public void serializeAttributeBodies(com.sun.xml.bind.serializer.XMLSerializer context)
        throws org.xml.sax.SAXException
    {
        final int len2 = _DisableFields.size();
    }

    public java.lang.Class getPrimaryInterface() {
        return (config.bindings.UserAdminConfigType.class);
    }

    public com.sun.msv.verifier.DocumentDeclaration createRawValidator() {
        return new com.sun.msv.verifier.regexp.REDocumentDeclaration(schemaFragment);
    }

    public class Unmarshaller
        extends com.sun.xml.bind.unmarshaller.ContentHandlerEx
    {


        public Unmarshaller(com.sun.xml.bind.unmarshaller.UnmarshallingContext context) {
            super(context, "-----");
        }

        protected com.sun.xml.bind.unmarshaller.UnmarshallableObject owner() {
            return config.bindings.impl.UserAdminConfigTypeImpl.this;
        }

        public void enterElement(java.lang.String ___uri, java.lang.String ___local, org.xml.sax.Attributes __atts)
            throws com.sun.xml.bind.unmarshaller.UnreportedException
        {
            switch (state) {
                case  0 :
                    if (("" == ___uri)&&("DisableFields" == ___local)) {
                        context.pushAttributes(__atts);
                        goto3();
                        return ;
                    }
                    if (("" == ___uri)&&("AuditableFields" == ___local)) {
                        context.pushAttributes(__atts);
                        state = 1;
                        return ;
                    }
                    revertToParentFromEnterElement(___uri, ___local, __atts);
                    return ;
                case  1 :
                    if (("" == ___uri)&&("Field" == ___local)) {
                        _AuditableFields = ((config.bindings.impl.AuditableFieldsTypeImpl) spawnChildFromEnterElement((config.bindings.impl.AuditableFieldsTypeImpl.class), 2, ___uri, ___local, __atts));
                        return ;
                    }
                    break;
                case  3 :
                    if (("" == ___uri)&&("Field" == ___local)) {
                        _DisableFields.add(((config.bindings.impl.DisableFieldsTypeImpl) spawnChildFromEnterElement((config.bindings.impl.DisableFieldsTypeImpl.class), 4, ___uri, ___local, __atts)));
                        return ;
                    }
                    break;
            }
            super.enterElement(___uri, ___local, __atts);
        }

        public void leaveElement(java.lang.String ___uri, java.lang.String ___local)
            throws com.sun.xml.bind.unmarshaller.UnreportedException
        {
            switch (state) {
                case  0 :
                    revertToParentFromLeaveElement(___uri, ___local);
                    return ;
                case  4 :
                    if (("" == ___uri)&&("DisableFields" == ___local)) {
                        context.popAttributes();
                        state = 0;
                        return ;
                    }
                    break;
                case  2 :
                    if (("" == ___uri)&&("AuditableFields" == ___local)) {
                        context.popAttributes();
                        state = 0;
                        return ;
                    }
                    break;
                case  1 :
                    if (("" == ___uri)&&("AuditableFields" == ___local)) {
                        _AuditableFields = ((config.bindings.impl.AuditableFieldsTypeImpl) spawnChildFromLeaveElement((config.bindings.impl.AuditableFieldsTypeImpl.class), 2, ___uri, ___local));
                        return ;
                    }
                    break;
            }
            super.leaveElement(___uri, ___local);
        }

        public void enterAttribute(java.lang.String ___uri, java.lang.String ___local)
            throws com.sun.xml.bind.unmarshaller.UnreportedException
        {
            switch (state) {
                case  0 :
                    revertToParentFromEnterAttribute(___uri, ___local);
                    return ;
                case  3 :
                    if (("" == ___uri)&&("UserTypeId_from" == ___local)) {
                        _DisableFields.add(((config.bindings.impl.DisableFieldsTypeImpl) spawnChildFromEnterAttribute((config.bindings.impl.DisableFieldsTypeImpl.class), 4, ___uri, ___local)));
                        return ;
                    }
                    if (("" == ___uri)&&("Description" == ___local)) {
                        _DisableFields.add(((config.bindings.impl.DisableFieldsTypeImpl) spawnChildFromEnterAttribute((config.bindings.impl.DisableFieldsTypeImpl.class), 4, ___uri, ___local)));
                        return ;
                    }
                    if (("" == ___uri)&&("UserTypeId_to" == ___local)) {
                        _DisableFields.add(((config.bindings.impl.DisableFieldsTypeImpl) spawnChildFromEnterAttribute((config.bindings.impl.DisableFieldsTypeImpl.class), 4, ___uri, ___local)));
                        return ;
                    }
                    break;
            }
            super.enterAttribute(___uri, ___local);
        }

        public void leaveAttribute(java.lang.String ___uri, java.lang.String ___local)
            throws com.sun.xml.bind.unmarshaller.UnreportedException
        {
            switch (state) {
                case  0 :
                    revertToParentFromLeaveAttribute(___uri, ___local);
                    return ;
            }
            super.leaveAttribute(___uri, ___local);
        }

        public void text(java.lang.String value)
            throws com.sun.xml.bind.unmarshaller.UnreportedException
        {
            try {
                switch (state) {
                    case  0 :
                        revertToParentFromText(value);
                        return ;
                }
            } catch (java.lang.RuntimeException e) {
                handleUnexpectedTextException(value, e);
            }
        }

        public void leaveChild(int nextState)
            throws com.sun.xml.bind.unmarshaller.UnreportedException
        {
            switch (nextState) {
                case  4 :
                    state = 4;
                    return ;
                case  2 :
                    state = 2;
                    return ;
            }
            super.leaveChild(nextState);
        }

        private void goto3()
            throws com.sun.xml.bind.unmarshaller.UnreportedException
        {
            int idx;
            state = 3;
            idx = context.getAttribute("", "UserTypeId_to");
            if (idx >= 0) {
                context.consumeAttribute(idx);
                return ;
            }
            idx = context.getAttribute("", "UserTypeId_from");
            if (idx >= 0) {
                context.consumeAttribute(idx);
                return ;
            }
            idx = context.getAttribute("", "Description");
            if (idx >= 0) {
                context.consumeAttribute(idx);
                return ;
            }
        }

    }

}
