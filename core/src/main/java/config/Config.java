package config;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Config
{

    private static final Log _log = LogFactory.getLog(Config.class);

  //==========================================================================================
  //  SYS_PROPERTIES_LOCATION
  //
  //  Modifications:
  //    - Modified to support multiple clients running on the same machine by reading the
  //      System Property ==> "basisxpress.client.name"
  //==========================================================================================


  //  Location of configuration file for BaxisXpress Application Server (MOSCore)

  //  For UNIX Deployment typically --> "/mosapps/admin/"
  // Default
  public static String SYS_PROPERTIES_LOCATION =  "C:/Work/BasisXpressJ2EE/Development/admin/";
  // Development environment : "basisxpress.client.name"=DEV
  public static String SYS_PROPERTIES_LOCATION_DEV = "C:/Work/BasisXpressJ2EE/Development/admin/";
  // BridgeWater Production : "basisxpress.client.name"=BW
  public static String SYS_PROPERTIES_LOCATION_BW = "c:/work/moscore/admin_BW/";
  // Co-op Production : "basisxpress.client.name"=CO
  public static String SYS_PROPERTIES_LOCATION_CO = "c:/work/moscore/admin/";
  // TD Production : "basisxpress.client.name"=TD
  public static String SYS_PROPERTIES_LOCATION_TD = "/moscore/admin_TD/";
  // Xceed Production : "basisxpress.client.name"=XC
  public static String SYS_PROPERTIES_LOCATION_XC = "/moscore/admin_XC/";


  //  Location of configuration file for BaxisXpress Ingestion Server

  // default
  public static String SYS_PROPERTIES_LOCATION_INGESTION = "c:/Mos_Ingestion/admin/";
  // Development environment : "basisxpress.client.name"=DEV
  public static String SYS_PROPERTIES_LOCATION_INGESTION_DEV = "c:/Mos_Ingestion/admin/";
  // BridgeWater Production : "basisxpress.client.name"=BW
  public static String SYS_PROPERTIES_LOCATION_INGESTION_BW = "c:/Mos_Ingestion/admin_BW/";
  // Co-op Production : "basisxpress.client.name"=CO
  public static String SYS_PROPERTIES_LOCATION_INGESTION_CO = "c:/Mos_Ingestion/admin_CO/";
  // TD Production : "basisxpress.client.name"=TD
  public static String SYS_PROPERTIES_LOCATION_INGESTION_TD = "c:/Mos_Ingestion/admin_TD/";
  // Xceed Production : "basisxpress.client.name"=XC
  public static String SYS_PROPERTIES_LOCATION_INGESTION_XC = "c:/Mos_Ingestion/admin_XC/";


  //  Location of configuration file for BaxisXpress Docprep Server

  // default
  public static String SYS_PROPERTIES_LOCATION_DOCPREP = "c:/Mos_Docprep/admin/";
  // Development environment : "basisxpress.client.name"=DEV
  public static String SYS_PROPERTIES_LOCATION_DOCPREP_DEV = "c:/Mos_Docprep/admin/";
  // BridgeWater Production : "basisxpress.client.name"=BW
  public static String SYS_PROPERTIES_LOCATION_DOCPREP_BW = "c:/Mos_Docprep/admin_BW/";
  // Co-op Production : "basisxpress.client.name"=CO
  public static String SYS_PROPERTIES_LOCATION_DOCPREP_CO = "c:/Mos_Docprep/admin_CO/";
  // TD Production : "basisxpress.client.name"=TD
  public static String SYS_PROPERTIES_LOCATION_DOCPREP_TD = "c:/Mos_Docprep/admin_TD/";
  // Xceed Production : "basisxpress.client.name"=XC
  public static String SYS_PROPERTIES_LOCATION_DOCPREP_XC = "c:/Mos_Docprep/admin_XC/";

  //
  //  Location of configuration file for BaxisXpress ATNServlet Servlet
  //
  // default
  public static String SYS_PROPERTIES_LOCATION_ATNSERVLET = "c:/work/BasisXpressServletAdmin/";
  // Development environment : "basisxpress.client.name"=DEV
  public static String SYS_PROPERTIES_LOCATION_ATNSERVLET_DEV = "c:/work/BasisXpressServletAdmin/";
  // BridgeWater Production : "basisxpress.client.name"=BW
  public static String SYS_PROPERTIES_LOCATION_ATNSERVLET_BW = "c:/work/BasisXpressServletAdmin_BW/";
  // Co-op Production : "basisxpress.client.name"=CO
  public static String SYS_PROPERTIES_LOCATION_ATNSERVLET_CO = "c:/work/BasisXpressServletAdmin_CO/";
  // TD Production : "basisxpress.client.name"=TD
  public static String SYS_PROPERTIES_LOCATION_ATNSERVLET_TD = "c:/BasisXpressServletAdmin_TD/";
  // XCeed Production : "basisxpress.client.name"=XC
  public static String SYS_PROPERTIES_LOCATION_ATNSERVLET_XC = "c:/BasisXpressServletAdmin_XC/";

  //
  //  Location of configuration file for BasisXpress RMIRateServer rmiserver
  //
  // default
  public static String SYS_PROPERTIES_LOCATION_RMIRATESERVER = "c:/work/BasisXpressRMIServerAdmin/";
  // Development environment : "basisxpress.client.name"=DEV
  public static String SYS_PROPERTIES_LOCATION_RMIRATESERVER_DEV = "c:/work/BasisXpressRMIServerAdmin/";
  // BridgeWater Production : "basisxpress.client.name"=BW
  public static String SYS_PROPERTIES_LOCATION_RMIRATESERVER_BW = "c:/BasisXpressRMIServerAdmin_BW/";
  // Co-op Production : "basisxpress.client.name"=CO
  public static String SYS_PROPERTIES_LOCATION_RMIRATESERVER_CO = "c:/work/BasisXpressRMIServerAdmin_CO/";
  // TD Production : "basisxpress.client.name"=TD
  public static String SYS_PROPERTIES_LOCATION_RMIRATESERVER_TD = "c:/BasisXpressRMIServerAdmin_TD/";
  // Xceed Production : "basisxpress.client.name"=XC
  public static String SYS_PROPERTIES_LOCATION_RMIRATESERVER_XC = "c:/BasisXpressRMIServerAdmin_XC/";


  //
  // Method to get properties based on the System Property "basisxpress.client.name"
  //
  //  return -- "" if not matched
  //
  public static Config theThis = new Config();
  public static String getProperty(String name)
  {
    //--> Check if it was defined as a VM parameter, in order to prevent hardcoding
    //--> Modified by Billy 04Dec2002
    String vmParam = System.getProperty(name);
    if (vmParam == null) {
        // load the properties.
        vmParam = getMosProperties().getProperty(name, "");
    }

    if(vmParam != null && vmParam.trim().length()>0)
      return vmParam;
    //===========================================

    String clientName = System.getProperty("basisxpress.client.name", "");
    Field theField = null;

    try{
      if(clientName == null || clientName.trim().equals(""))
      {
        theField = theThis.getClass().getDeclaredField(name);
      }
      else
      {
        theField = theThis.getClass().getDeclaredField(name+"_"+clientName);
      }

      return (String)(theField.get(theThis));
    }
    catch (Exception e)
    {
      // Something wrong !! May be the required property not exist !!
      return("");
    }

  }

  public static String getClientName()
  {
    String clientName = System.getProperty("basisxpress.client.name", "");

    return clientName;
  }

    // system configuration file.
    private static final String MOS_CONFIGURATION = "mos.properties";
    private static Properties _mosProperties = new Properties();

    /**
     * load the properties.
     */
    private static void loadMosProperties() {

        try {
            ClassLoader classLoader = Config.class.getClassLoader();
            URL url = classLoader.getResource(MOS_CONFIGURATION);
            _log.debug("URL: " + url.toString());
            _mosProperties.load(url.openStream());
            _log.debug("MOS Properties size: " + _mosProperties.size());
            // repair the value of SYS_PROPERTIES_LOCATION.
            if (_mosProperties.getProperty("SYS_PROPERTIES_LOCATION").equals("")) {
                _mosProperties.setProperty("SYS_PROPERTIES_LOCATION",
                                           splitToPath(url));
                _log.info("set SYS_PROPERTIES_LOCATION to: " +
                           splitToPath(url));
            }
        } catch (Throwable t) {
            _log.error("Error: " + t.toString(), t);
        }
    }

    /**
     * split to path.
     */
    private static String splitToPath(URL url) {

        String urlString = url.getPath();
        // file url starts with file:/
        return urlString.substring(0,
                                   urlString.length() -
                                   MOS_CONFIGURATION.length());
    }
        

    /**
     * return the mosproperties.
     */
    protected static Properties getMosProperties() {

        if (_mosProperties.isEmpty()) {
            loadMosProperties();
        }

        return _mosProperties;
    }

    /**
     * get the property.
     */
    public static String getProperty(String key, String defaultValue) {

        // try the system property first.
        String value = System.getProperty(key);
        if (value == null) {
            // don't have this key in the system property.
            // try the mos properties.
            value = getMosProperties().getProperty(key, defaultValue);
        }

        return value;
    }

}

