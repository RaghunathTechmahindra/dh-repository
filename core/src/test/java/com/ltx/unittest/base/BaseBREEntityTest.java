package com.ltx.unittest.base;

import org.ddsteps.data.DataLoader;
import org.ddsteps.data.support.DataLoaderFactory;
import org.junit.Ignore;

import com.basis100.BREngine.BRInterpretor;
import com.ltx.unittest.util.EntityTestUtil;

// Referenced classes of package com.ltx.unittest.base:
//            BaseTest

@Ignore
public class BaseBREEntityTest extends BaseTest
{

    protected BRInterpretor interpretor;

    public BaseBREEntityTest()
    {
        interpretor = new BRInterpretor();
        try
        {
            interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public DataLoader createDataLoader()
    {
        return DataLoaderFactory.getCachingExcelDataLoader();
    }
}