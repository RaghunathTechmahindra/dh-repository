package com.ltx.unittest.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.basis100.resources.ResourceManager;

public class PropertyManager {

	private static boolean propertyInitiated = false;

	private static PropertyManager propertyManager = null;
	private String utfDealInstitutionId = null;
	private PropertyManager()
	{
		
	}
	
	public static PropertyManager instance()
	{
		{
			if (propertyManager == null)
			{
				synchronized (PropertyManager.class) {
					propertyManager = new PropertyManager();
					propertyManager.initiate();
				}
			}
			
		}
		return propertyManager;
	}
	
	public  void initiate() {
		if (!propertyInitiated) {
			try {

				ResourceManager.init();
				Properties properties = new Properties();
			    try {
			      	String institutionid = "";
			      	properties.load( Thread.currentThread().getContextClassLoader().getResourceAsStream("utf.properties"));
			        institutionid = properties.getProperty("utf.deal.istitution.id");
			        if(null==institutionid)
			        {
			          throw new Exception("The value of property utf.deal.institution.id is null");
			        }
			        Integer.parseInt(institutionid.trim());
			        setUtfDealInstitutionId(institutionid.trim());
			    } catch (IOException e) {
			      Exception xe =new Exception ("utf.properties not found in project classpath");
			      xe.setStackTrace(e.getStackTrace());
			      throw xe ;
			    }catch (Exception e){
			      throw new Exception("Unexpected Error while reading properties from utf.properties");
			    }

			} catch (Exception e1) {
				// TODO Auto-generated catch block
			  System.out.println("There was problem reading property for dealinstitution id");
			  System.out.println("The cause for the same is shon below. the default institution id assumed will be 0.\n make sure your database supports it");
			  e1.printStackTrace();
			  setUtfDealInstitutionId("0");
			}
			propertyInitiated = true;
		}
		
	}

	public String getUtfDealInstitutionId()
	{
	  return utfDealInstitutionId;
	}

	private void setUtfDealInstitutionId(String utfDealInstitutionId)
	{
	  this.utfDealInstitutionId = utfDealInstitutionId;
	}
	

}
