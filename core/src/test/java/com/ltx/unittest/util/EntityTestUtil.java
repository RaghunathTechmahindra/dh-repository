package com.ltx.unittest.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.DealEntity;
import com.basis100.resources.SessionResourceKit;

public class EntityTestUtil {

	private static  SessionResourceKit srk = new SessionResourceKit();
	private static  CalcMonitor calcMonitor;
	private static EntityTestUtil entityTestUtil;
	
	static
	{
			PropertyManager.instance().initiate();
			calcMonitor = CalcMonitor.getMonitor(srk);
	}
	
	private EntityTestUtil()
	{
		
	}
    public static SessionResourceKit getSessionResourceKit()
    {
        return srk;
    }
	public static EntityTestUtil getInstance()
	{
		if (entityTestUtil  == null)
		{
			entityTestUtil = new EntityTestUtil();
		}
		return entityTestUtil;
	}
    public static CalcMonitor getCalcMonitor()
    {
        if(calcMonitor == null)
        {
            calcMonitor = CalcMonitor.getMonitor(srk);
        }
        return calcMonitor;
    }

    public static void setCalcMonitor(CalcMonitor calcMonitor)
    {
        calcMonitor = calcMonitor;
    }
	
	public DealEntity loadEntity(String entityName)
	{
		Class clazz;
		try {
		clazz = Class.forName(entityName);
		Class[] args1 = new Class[2];
		args1[0] = srk.getClass();
		args1[1] = calcMonitor.getClass();
		Object[] args = null;
		Constructor con;
		try
		{
			con = clazz.getConstructor(args1);
			args = new Object[2];
			args[0] = srk;
			args[1] = calcMonitor;
		}
		catch (NoSuchMethodException nsme)
		{
			args1 = new Class[1];
			args1[0] = srk.getClass();
			con = clazz.getConstructor(args1);
			args = new Object[1];
			args[0] = srk;
		}
		Object obj = con.newInstance(args);
		return(DealEntity) obj;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/*public DealEntity loadEntity(String entityName, int id, int copyId)
	{
		Class clazz;
		try {
			clazz = Class.forName(entityName);
			Class[] args1 = new Class[4];
			args1[0] = srk.getClass();
			args1[1] = calcMonitor.getClass();
			args1[2] = int.class;
			args1[3] = int.class;
			Constructor con = clazz.getConstructor(args1);
			Object[] args = new Object[4];
			args[0] = srk;
			args[1] = calcMonitor;
			args[2] = new Integer(id);
			args[3] = new Integer(copyId);
			Object obj = con.newInstance(args);
			return (DealEntity) obj;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	}*/
}
