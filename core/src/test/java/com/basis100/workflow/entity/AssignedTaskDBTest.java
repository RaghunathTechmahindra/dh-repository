package com.basis100.workflow.entity;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.workflow.pk.AssignedTaskBeanPK;

/**
 * <p>
 * DealQueryTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class AssignedTaskDBTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private AssignedTask assignedTask;
	private SysLogger logger = new SysLogger("JAI");

	public AssignedTaskDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				AssignedTask.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.assignedTask = new AssignedTask(srk);
	}

	public void testFindByPrimaryKey() throws Exception {

		// get input data from xml repository
		ITable expectedAssignedTask = dataSetTest
				.getTable("testFindByPrimaryKey");
		int assignedTasksWorkQueueId = (Integer) DataType.INTEGER
				.typeCast(expectedAssignedTask.getValue(0,
						"ASSIGNEDTASKSWORKQUEUEID"));
		// feed the input data and run the program.
		assignedTask.findByPrimaryKey(new AssignedTaskBeanPK(
				assignedTasksWorkQueueId));
		// verify the running result.
		assertEquals(assignedTasksWorkQueueId,
				assignedTask.getAssignedTasksWorkQueueId());

	}

	public void testRemove() throws Exception {

		// get input data from xml repository
		ITable expectedAssignedTask = dataSetTest.getTable("testRemove");
		srk.beginTransaction();
		int assignedTasksWorkQueueId = (Integer) DataType.INTEGER
				.typeCast(expectedAssignedTask.getValue(0,
						"ASSIGNEDTASKSWORKQUEUEID"));
		// feed the input data and run the program.
		assignedTask.remove();
		// verify the running result.
		assertNotNull(assignedTasksWorkQueueId);
		srk.rollbackTransaction();

	}

	public void testTraceShow() throws Exception {

		// get input data from xml repository
		ITable expectedAssignedTask = dataSetTest.getTable("testTraceShow");
		int assignedTasksWorkQueueId = (Integer) DataType.INTEGER
				.typeCast(expectedAssignedTask.getValue(0,
						"ASSIGNEDTASKSWORKQUEUEID"));
		// feed the input data and run the program.
		assignedTask.traceShow(logger);
		// verify the running result.
		assertNotNull(assignedTask);

	}

	public void testEjbStore() throws Exception {

		// get input data from xml repository
		ITable expectedAssignedTask = dataSetTest.getTable("testEjbStore");
		srk.beginTransaction();
		int assignedTasksWorkQueueId = (Integer) DataType.INTEGER
				.typeCast(expectedAssignedTask.getValue(0,
						"ASSIGNEDTASKSWORKQUEUEID"));
		// feed the input data and run the program.
		assignedTask.ejbStore();
		// verify the running result.
		assertNotNull(assignedTasksWorkQueueId);
		srk.rollbackTransaction();

	}

	public void testSqlDateStr() throws Exception {

		// get input data from xml repository
		ITable expectedAssignedTask = dataSetTest.getTable("testSqlDateStr");
		Date dueTimestamp = (Date) DataType.DATE.typeCast(expectedAssignedTask
				.getValue(0, "DUETIMESTAMP"));
		// feed the input data and run the program.
		assignedTask.sqlDateStr(dueTimestamp);
		// verify the running result.
		// assertEquals(dueTimestamp, assignedTask.getDueTimestamp());
		assertNotNull(assignedTask);
	}

	public void testSetForRetry1() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testSetRetryExpired = dataSetTest
				.getTable("testSetRetryExpired1");
		assignedTask.setTaskStatusId(1);
		assignedTask.setForRetry(true);
		assertTrue("assignedTask", true);
	}

	public void testSetRetryExpired1() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testSetRetryExpired = dataSetTest
				.getTable("testSetRetryExpired1");
		assignedTask.setTaskStatusId(1);
		assignedTask.setRetryExpired();
		assertNotNull(assignedTask);
	}

	public void testResetOutstandingTasks1() throws Exception {
		ITable testResetOutstandingTasks = dataSetTest
				.getTable("testResetOutstandingTasks1");
		srk.beginTransaction();
		String userProfile = (String) testResetOutstandingTasks.getValue(0,
				"USERPROFILEID");
		int userProfileId = Integer.parseInt(userProfile);
		srk.getExpressState().setDealInstitutionId(1);
		int result = assignedTask.resetOutstandingTasks(userProfileId);
		srk.rollbackTransaction();
		assert assignedTask.getApplicationId() != null;
		assert assignedTask.getApplicationId().length() <= 0;
	}

}