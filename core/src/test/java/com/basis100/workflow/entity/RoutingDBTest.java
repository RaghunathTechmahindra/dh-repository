package com.basis100.workflow.entity;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.pk.RoutingPK;


public class RoutingDBTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private Routing routing;
	Vector vector=null;
	public RoutingDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Routing.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.routing = new Routing(srk);	
	}
	
public void testFindByPrimaryKey() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
	   String routingid=(String)testFindByPrimaryKey.getValue(0,"ROUTINGID");
	   int routingId=Integer.parseInt(routingid);	  
	   RoutingPK pk=new RoutingPK(routingId);
	   routing=routing.findByPrimaryKey(pk);
    	assertEquals(routingId, pk.getRoutingId());
	} 
	
	
	public void testFindByRouteType() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testFindByRouteType = dataSetTest.getTable("testFindByRouteType");
	   String routingType=(String)testFindByRouteType.getValue(0,"ROUTETYPEID");
	   int routingTypeId=Integer.parseInt(routingType);	  
	    vector=routing.findByRouteType(routingTypeId);
	    assertNotNull(vector);
	} 
	
	public void testFindByAll() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testFindByAll = dataSetTest.getTable("testFindByAll");
	   String routingType=(String)testFindByAll.getValue(0,"ROUTETYPEID");
	   int routingTypeId=Integer.parseInt(routingType);
	   String routingid=(String)testFindByAll.getValue(0,"ROUTINGID");
	   int routingId=Integer.parseInt(routingid);	    
	    vector=routing.findByAll();
	    assertNotNull(vector);
	} 
	
	public void testGetOrderedList() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testGetOrderedList = dataSetTest.getTable("testGetOrderedList");
	   String routingType=(String)testGetOrderedList.getValue(0,"ROUTETYPEID");
	   int routingTypeId=Integer.parseInt(routingType);
	   String routingid=(String)testGetOrderedList.getValue(0,"ROUTINGID");
	   int routingId=Integer.parseInt(routingid);	  	 
	    vector=routing.getOrderedList();	  	    
	    assertNotNull(vector);
	} 
	
	//Error in Main code of routing.create hence commenting this method
	//SQL Error: ORA-01756: quoted string not properly terminated
	 //01756. 00000 -  "quoted string not properly terminated"
	// Insert into ROUTING (routingId, routeTypeId, routeKey, matchTypeId, matchValue) Values (100, '15', '1', 15', 1)
	// insert query itself wrong
	/*public void testCreate() throws Exception{	
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int routingId = Integer.parseInt((String)testCreate.getValue(0,"routingId"));	
		int routeTypeId = Integer.parseInt((String)testCreate.getValue(0,"routeTypeId"));
		String routeKey = (String)testCreate.getValue(0,"routeKey");		
		int matchTypeId = Integer.parseInt((String)testCreate.getValue(0,"matchTypeId"));
		int matchValue = Integer.parseInt((String)testCreate.getValue(0,"matchValue"));			
		srk.getExpressState().setDealInstitutionId(0);		
		routing=routing.create(routingId,routeTypeId,routeKey,matchTypeId,matchValue);		
		assert routing.getRoutingId()!=0;
		srk.rollbackTransaction();
	}*/
	
	
}