package com.basis100.workflow.entity;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.pk.AssignedTaskBeanPK;


public class AssignedTaskTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;
	private AssignedTask assignedTask;
	Vector vector=null;
	public AssignedTaskTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(AssignedTask.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(AssignedTask.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.assignedTask = new AssignedTask(srk);	
	}
	
	public void testFindByPrimaryKey1() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey1");
	   String assignedTasksWorkQueue=(String)testFindByPrimaryKey.getValue(0,"ASSIGNEDTASKSWORKQUEUEID");
	   int assignedTasksWorkQueueId=Integer.parseInt(assignedTasksWorkQueue);	  
	   AssignedTaskBeanPK pk=new AssignedTaskBeanPK(assignedTasksWorkQueueId);
	   srk.getExpressState().setDealInstitutionId(0);
	   assignedTask=assignedTask.findByPrimaryKey(pk);
       assertEquals(assignedTasksWorkQueueId, pk.getAssignedTasksWorkQueueId());
}
	
		
	public void testResetOutstandingTasks1() throws Exception{
		ITable testResetOutstandingTasks = dataSetTest.getTable("testResetOutstandingTasks1");
		srk.beginTransaction();
	   String userProfile=(String)testResetOutstandingTasks.getValue(0,"USERPROFILEID");
	   int userProfileId=Integer.parseInt(userProfile);
	   srk.getExpressState().setDealInstitutionId(1);
	   int result=assignedTask.resetOutstandingTasks(userProfileId);
	   srk.rollbackTransaction();
	   assert assignedTask.getApplicationId()!=null;
	   assert assignedTask.getApplicationId().length()<=0;
	   
	 
}
	
	
	public void testSetRetryExpired1() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testSetRetryExpired = dataSetTest.getTable("testSetRetryExpired1");	   
	    assignedTask.setTaskStatusId(1);
	   assignedTask.setRetryExpired();
       assertNotNull(assignedTask);
}
	
	public void testSetForRetry1() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testSetRetryExpired = dataSetTest.getTable("testSetRetryExpired1");	   
	    assignedTask.setTaskStatusId(1);
	   assignedTask.setForRetry(true);
       assertTrue("assignedTask", true);
}
	
	
}