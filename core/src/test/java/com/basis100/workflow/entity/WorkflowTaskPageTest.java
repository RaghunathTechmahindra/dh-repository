/*
 * @(#)WorkflowTaskPageTest.java    Sep 7, 2007
 *
 * Copyrightę 2007. Filogix Limited Partnership. All rights reserved. 
 */

package com.basis100.workflow.entity;

import java.util.Vector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.WorkflowTaskPageBeanPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test class for WorkflowTaskPage
 * 
 * @version 1.0 Sep 7, 2007
 * @author hiro
 */
public class WorkflowTaskPageTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(WorkflowTaskPageTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * Test method for create
     */
    @Test
    public void testCreate() throws Exception {
        _log.info(BORDER_START, "testCreate");
        // FIXME : the usage of institutionId needs reconsideration
        _log.warn("the usage of institutionId needs reconsideration");
        _log.info(BORDER_END, "testCreate");
    }

    /**
     * test method for WorkflowTaskPage.findByPage
     */
    @Test
    public void testFindByPage() throws Exception {

        _log.info(BORDER_START, "testFindByPage");

        //prepare data
        int institutionProfileId = _dataRepository.getInt("WorkflowTaskPage",
                "INSTITUTIONPROFILEID", 0);
        int pageId = _dataRepository.getInt("WorkflowTaskPage", "PAGEID", 0);
        int workflowId = _dataRepository.getInt("WorkflowTaskPage", "WORKFLOWID", 0);
        
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testfindByPage
        WorkflowTaskPage entity = new WorkflowTaskPage(_srk);
        Vector vector = entity.findByPage(pageId, workflowId);        
        assertNotNull(vector);
        assertTrue(vector.isEmpty() == false);
        
        entity = (WorkflowTaskPage)vector.get(0);
        
        assertEquals(pageId, entity.getPageId());
        assertEquals(workflowId, entity.getWorkflowId());
        
        assertEquals(institutionProfileId, entity.getInstitutionProfileId());
        
        _log.info(BORDER_END, "testFindByPage");
    }
    


    /**
     * test method for WorkflowTaskPage.findByPrimaryKey
     */
    @Test
    public void testfindByPrimaryKey() throws Exception {

        _log.info(BORDER_START, "testfindByPrimaryKey");

        // prepare data
        int institutionProfileId = _dataRepository.getInt("WorkflowTaskPage",
                "INSTITUTIONPROFILEID", 0);
        int workflowTaskPageId = _dataRepository.getInt("WorkflowTaskPage",
                "workflowTaskPageId", 0);
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        // target method
        WorkflowTaskPage entity = new WorkflowTaskPage(_srk);
        entity = entity.findByPrimaryKey(new WorkflowTaskPageBeanPK(
                workflowTaskPageId));

        // test
        assertEquals(_dataRepository.getInt("WorkflowTaskPage", 
                "workflowTaskPageId", 0), entity.getWorkflowTaskPageId());
        assertEquals(_dataRepository.getInt("WorkflowTaskPage", 
                "PAGEID", 0), entity.getPageId());
        assertEquals(_dataRepository.getInt("WorkflowTaskPage", 
                "WORKFLOWID", 0), entity .getWorkflowId());
        assertEquals(_dataRepository.getInt("WorkflowTaskPage", 
                "TASKID", 0), entity.getTaskId());
        assertEquals(_dataRepository.getInt("WorkflowTaskPage", 
                "TASKSEQUENCE", 0), entity.getTaskSequence());
        assertEquals(institutionProfileId, 
                entity.getInstitutionProfileId());
        
        _log.info(BORDER_END, "testfindByPrimaryKey");
    }


    
    /**
     * test method for WorkflowTaskPage.findByTask
     */
    @Test
    public void testfindByTask() throws Exception {

        _log.info(BORDER_START, "testfindByTask");

        //prepare data
        int institutionProfileId = _dataRepository.getInt("WorkflowTaskPage",
                "INSTITUTIONPROFILEID", 0);
        int taskId = _dataRepository.getInt("WorkflowTaskPage", "TASKID", 0);
        int workflowId = _dataRepository.getInt("WorkflowTaskPage", "workflowId", 0);
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testfindByTask
        WorkflowTaskPage entity = new WorkflowTaskPage(_srk);
        Vector vector = entity.findByTask(taskId, workflowId);

        assertNotNull(vector);
        assertTrue(vector.isEmpty() == false);
        
        entity = (WorkflowTaskPage)vector.get(0);
        
        assertEquals(taskId, entity.getTaskId());
        assertEquals(workflowId, entity.getWorkflowId());
        assertEquals(institutionProfileId, 
                entity.getInstitutionProfileId());
        _log.info(BORDER_END, "testfindByTask");
    }

}
