/*
 * @(#)PageTest.java     Sep 7, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.workflow.entity;

import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.PageBeanPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test for Page
 *
 * @version   1.0  Sep 7, 2007
 * @author    hiro
 * @since     3.3
 */
public class PageTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(PageTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }
    
    /**
     * test method for Page.findByPrimaryKey
     */
    @Test
    public void testfindByPrimaryKey() throws Exception {

        _log.info(BORDER_START, "testfindByPrimaryKey");

        //prepare data
        int institutionProfileId = _dataRepository.getInt("Page",
                "INSTITUTIONPROFILEID", 0);
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testfindByPrimaryKey
        Page entity = new Page(_srk);
        entity = entity.findByPrimaryKey(
                new PageBeanPK(_dataRepository.getInt("Page", "pageId", 0)));
        
        assertEquals(_dataRepository.getInt("Page", "pageId", 0), 
                entity.getPageId());
        assertEquals(_dataRepository.getString("Page", "PAGENAME", 0), 
                entity.getPageName());
        assertEquals(_dataRepository.getString("Page", "PAGELABEL", 0), 
                entity.getPageLabel());
        assertEquals(_dataRepository.getBoolean("Page", "EDITABLE", 0), 
                entity.getEditable());
        assertEquals(_dataRepository.getBoolean("Page", "EDITVIATXCOPY", 0), 
                entity.getEditViaTxCopy());
        assertEquals(_dataRepository.getBoolean("Page", "DEALLOCKONEDIT", 0), 
                entity.getDealLockOnEdit());
        assertEquals(_dataRepository.getBoolean("Page", "ALLOWEDITWHENLOCKED", 0), 
                entity.getAllowEditWhenLocked());
        assertEquals(_dataRepository.getBoolean("Page", "DEALPAGE", 0), 
                entity.getDealPage());
        assertEquals(_dataRepository.getBoolean("Page", "INCLUDEINGOTO", 0), 
                entity.getIncludeInGoto());
        assertEquals(_dataRepository.getInt("Page", "INSTITUTIONPROFILEID", 0), 
                entity.getInstitutionProfileId());
        assertEquals(_dataRepository.getBoolean("Page", "PAGELOCKONEDIT", 0), 
                entity.getPageLockOnEdit());
        
        _log.info(BORDER_END, "testfindByPrimaryKey");
    }

    /**
     * test method for Page.findByAll
     */
    //Commented 
    @Test
    public void testfindByAll() throws Exception {

        _log.info(BORDER_START, "testfindByAll");

        //prepare data
        int institutionProfileId = _dataRepository.getInt("Page",
                "INSTITUTIONPROFILEID", 0);
        
        ResultSet rs = _srk.getConnection().createStatement().executeQuery(
                "select count(*) from Page where INSTITUTIONPROFILEID = " 
                + institutionProfileId);
        int recordCount = rs.next() ? rs.getInt(1) : 0;
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testfindByAll
        Page entity = new Page(_srk);
        _log.info("recordCount = " + recordCount);
        
        assert recordCount!=0;
        assertNotNull(entity);
        _log.info(BORDER_END, "testfindByAll");
    }
    
    /**
     * test method for Page.FindByName
     */
    @Test
    public void testFindByName() throws Exception {

        _log.info(BORDER_START, "testFindByName");

        //prepare data
        int institutionProfileId = _dataRepository.getInt(
                "Page", "INSTITUTIONPROFILEID", 0);
        String pageName = _dataRepository.getString("Page", "PAGENAME", 0);
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testFindByName
        Page entity = new Page(_srk);
        entity = entity.findByName(pageName);
        
        assertEquals(_dataRepository.getInt("Page", "pageId", 0), 
                entity.getPageId());
        assertEquals(_dataRepository.getString("Page", "PAGENAME", 0), 
                entity.getPageName());
        assertEquals(_dataRepository.getString("Page", "PAGELABEL", 0), 
                entity.getPageLabel());
        assertEquals(_dataRepository.getBoolean("Page", "EDITABLE", 0), 
                entity.getEditable());
        assertEquals(_dataRepository.getBoolean("Page", "EDITVIATXCOPY", 0), 
                entity.getEditViaTxCopy());
        assertEquals(_dataRepository.getBoolean("Page", "DEALLOCKONEDIT", 0), 
                entity.getDealLockOnEdit());
        assertEquals(_dataRepository.getBoolean("Page", "ALLOWEDITWHENLOCKED", 0), 
                entity.getAllowEditWhenLocked());
        assertEquals(_dataRepository.getBoolean("Page", "DEALPAGE", 0), 
                entity.getDealPage());
        assertEquals(_dataRepository.getBoolean("Page", "INCLUDEINGOTO", 0), 
                entity.getIncludeInGoto());
        assertEquals(_dataRepository.getInt("Page", "INSTITUTIONPROFILEID", 0), 
                entity.getInstitutionProfileId());
        assertEquals(_dataRepository.getBoolean("Page", "PAGELOCKONEDIT", 0), 
                entity.getPageLockOnEdit());
        

        _log.info(BORDER_END, "testFindByName");
    }
    
    
    /**
     * test method for Page.Create
     */
    @Test
    public void testCreate() throws Exception {

        _log.info(BORDER_START, "testCreate");

        //prepare sample data
        int institutionProfileId = _dataRepository.getInt("Page", "INSTITUTIONPROFILEID", 0);
        String pageLabel = _dataRepository.getString("Page", "PAGELABEL", 0);
        boolean editable = _dataRepository.getBoolean("Page", "EDITABLE", 0);
        boolean editViaTxCopy = _dataRepository.getBoolean("Page", "EDITVIATXCOPY", 0);
        boolean dealLockOnEdit = _dataRepository.getBoolean("Page", "DEALLOCKONEDIT", 0);
        boolean allowEditWhenLocked = _dataRepository.getBoolean("Page", "ALLOWEDITWHENLOCKED", 0);
        boolean dealPage = _dataRepository.getBoolean("Page", "DEALPAGE", 0);
        boolean includeInGoto = _dataRepository.getBoolean("Page", "includeInGoto", 0);
        boolean pageLockOnEdit = _dataRepository.getBoolean("Page", "PAGELOCKONEDIT", 0);
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);
        _srk.beginTransaction();
        
        // get max pageid to avoid data collision
        Statement st  = _srk.getConnection().createStatement();
        ResultSet rs = st.executeQuery("SELECT MAX(PAGEID) from PAGE");
        int maxPageId = (rs.next()) ? rs.getInt(1) : -100;
        st.close();
        
        //test target of testCreate
        Page entity = new Page(_srk);
        entity.create(maxPageId + 1, "test", pageLabel, editable, editViaTxCopy,
                dealLockOnEdit, allowEditWhenLocked, dealPage, includeInGoto,
                institutionProfileId,pageLockOnEdit);
        
        // find the data just made 
        Page verify = new Page(_srk);
        verify.findByPrimaryKey(new PageBeanPK(maxPageId + 1));
        
        // check all data
        assertEquals(maxPageId + 1 , verify.getPageId());
        assertEquals("test", verify.getPageName());
        assertEquals(pageLabel, verify.getPageLabel());
        assertEquals(editable, verify.getEditable());
        assertEquals(editViaTxCopy, verify.getEditViaTxCopy());
        assertEquals(dealLockOnEdit, verify.getDealLockOnEdit());
        assertEquals(allowEditWhenLocked, verify.getAllowEditWhenLocked());
        assertEquals(dealPage, verify.getDealPage());
        assertEquals(includeInGoto, verify.getIncludeInGoto());
        assertEquals(institutionProfileId, verify.getInstitutionProfileId());
        assertEquals(pageLockOnEdit, verify.getPageLockOnEdit());
        
        _srk.rollbackTransaction();
        
        _log.info(BORDER_END, "testCreate");
    }
    
    /**
     * test method for Page.EjbStore
     */
    @Test
    public void testEjbStore() throws Exception {

        _log.info(BORDER_START, "testEjbStore");

        //prepare sample data
        int institutionProfileId = _dataRepository.getInt("Page", "INSTITUTIONPROFILEID", 0);
        int pageId = _dataRepository.getInt("Page", "PAGEID", 0);
        boolean editable = _dataRepository.getBoolean("Page", "EDITABLE", 0);
        boolean editViaTxCopy = _dataRepository.getBoolean("Page", "EDITVIATXCOPY", 0);
        boolean dealLockOnEdit = _dataRepository.getBoolean("Page", "DEALLOCKONEDIT", 0);
        boolean allowEditWhenLocked = _dataRepository.getBoolean("Page", "ALLOWEDITWHENLOCKED", 0);
        boolean dealPage = _dataRepository.getBoolean("Page", "DEALPAGE", 0);
        boolean includeInGoto = _dataRepository.getBoolean("Page", "includeInGoto", 0);
        boolean pageLockOnEdit = _dataRepository.getBoolean("Page", "PAGELOCKONEDIT", 0);
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);
        _srk.beginTransaction();
        
        //test target of testEjbStore
        Page pg = new Page(_srk);
        pg = pg.findByPrimaryKey(new PageBeanPK(pageId));
        pg.setPageName("testPageName");
        pg.setPageLabel("testPageLabel");
        pg.setEditable(!editable);
        pg.setEditViaTxCopy(!editViaTxCopy);
        pg.setDealLockOnEdit(!dealLockOnEdit);
        pg.setAllowEditWhenLocked(!allowEditWhenLocked);
        pg.setDealPage(!dealPage);
        pg.setIncludeInGoto(!includeInGoto);
        pg.setInstitutionProfileId(-1000); // never change;
        pg.setPageLockOnEdit(!pageLockOnEdit);
        pg.ejbStore();
        
        // find the data just modified 
        Page verify = new Page(_srk);
        verify.findByPrimaryKey(new PageBeanPK(pageId));
        
        // check all data
        assertEquals(pageId , verify.getPageId());
        assertEquals("testPageName", verify.getPageName());
        assertEquals("testPageLabel", verify.getPageLabel());
        assertEquals(!editable, verify.getEditable());
        assertEquals(!editViaTxCopy, verify.getEditViaTxCopy());
        assertEquals(!dealLockOnEdit, verify.getDealLockOnEdit());
        assertEquals(!allowEditWhenLocked, verify.getAllowEditWhenLocked());
        assertEquals(!dealPage, verify.getDealPage());
        assertEquals(!includeInGoto, verify.getIncludeInGoto());
        assertEquals(institutionProfileId, verify.getInstitutionProfileId()); //no change
        assertEquals(!pageLockOnEdit, verify.getPageLockOnEdit());
        
        _srk.rollbackTransaction();
        _log.info(BORDER_END, "testEjbStore");
    }
    
}