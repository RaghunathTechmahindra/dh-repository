/*
 * @(#)WorkflowTaskTest.java    Sep 7, 2007
 *
 * Copyrightę 2007. Filogix Limited Partnership. All rights reserved. 
 */
package com.basis100.workflow.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.WorkflowTaskBeanPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test class for WorkflowTask
 *
 * @version   1.0 Sep 7, 2007
 * @author    hiro
 */
public class WorkflowTaskTest extends ExpressEntityTestCase implements
UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
    .getLogger(WorkflowTaskTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }
    
    @After
    public void tearDown() throws Exception {
        _srk.freeResources();
    }

    @Test
    public void testFindByPrimaryKey() throws Exception {
        _log.info(BORDER_START, "testFindByPrimaryKey");
        
        // prepare test data
        int workFlowId = _dataRepository.getInt("WORKFLOWTASK", "WORKFLOWID", 0);
        int taskId = _dataRepository.getInt("WORKFLOWTASK", "TASKID", 0);
        int institutionId = _dataRepository.getInt("WORKFLOWTASK", "INSTITUTIONPROFILEID", 0);
        
        // set institution id
        _srk.getExpressState().setDealInstitutionId(institutionId);
        
        
        WorkflowTask wft = new WorkflowTask(_srk);
        // target method
        wft = wft.findByPrimaryKey(new WorkflowTaskBeanPK(workFlowId, taskId));
        
        
        // test all fields in workflowTask
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","WORKFLOWTASKID", 0)           
                ,    wft.getWorkflowTaskId());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","WORKFLOWID", 0)               
                ,    wft.getWorkflowId());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","WORKFLOWSTAGE", 0)            
                ,    wft.getWorkflowStage());
        assertEquals(_dataRepository.getString("WORKFLOWTASK","TASKNAME", 0)                 
                ,    wft.getTaskName());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","TASKID", 0)                   
                ,    wft.getTaskId());
        assertEquals(_dataRepository.getString("WORKFLOWTASK","TASKLABEL", 0)                
                ,    wft.getTaskLabel());
        assertEquals(_dataRepository.getString("WORKFLOWTASK","BASETASK", 0)                 
                ,    wft.getBaseTask());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","BASEDONTASKID", 0)            
                ,    wft.getBasedOnTaskId());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","FOLLOWUPTASKID", 0)           
                ,    wft.getFollowUpTaskId());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","LINKTASKID", 0)               
                ,    wft.getLinkTaskId());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","DURATION", 0)                 
                ,    wft.getDuration());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","WARNINGTIME", 0)              
                ,    wft.getWarningTime());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","REISSUEALARMDURATION", 0)     
                ,    wft.getReissueAlarmDuration());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","PRIORITYID", 0)               
                ,    wft.getPriorityId());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","REMINDERTIME", 0)             
                ,    wft.getReminderTime());
        assertEquals(_dataRepository.getString("WORKFLOWTASK","CRITICALFLAG", 0)             
                ,    wft.getCriticalFlag());
        assertEquals(_dataRepository.getString("WORKFLOWTASK","SENTINELFLAG", 0)             
                ,    wft.getSentinelFlag());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","SENTINELON", 0)               
                ,    wft.getSentinelOn());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","VISIBLEFLAG", 0)              
                ,    wft.getVisibleFlag());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","SENTINELTO", 0)               
                ,    wft.getSentinelTo());
//        assertEquals(_dataRepository.getInt("WORKFLOWTASK","AUTOTASKRETRYTIME", 0)        
//                ,    wft.getAutoTaskRetryTime());
        assertEquals(_dataRepository.getInt("WORKFLOWTASK","INSTITUTIONPROFILEID", 0)     
                ,    wft.getInstitutionProfileId());
        assertEquals(_dataRepository.getString("WORKFLOWTASK","BYPASSWFREGRESSION", 0)     
                ,    wft.getBypassWFRegression());
        _log.info(BORDER_END, "testFindByPrimaryKey");
    }
    
    @Test
    /**
     * ejbStore method is never called in real situation.
     */
    public void testEjbStore() throws Exception {
        _log.info(BORDER_START, "testEjbStore");
        
        //prepare test data
        int workFlowId = _dataRepository.getInt("WORKFLOWTASK", "WORKFLOWID", 0);
        int taskId = _dataRepository.getInt("WORKFLOWTASK", "TASKID", 0);
        int institutionId = _dataRepository.getInt("WORKFLOWTASK", "INSTITUTIONPROFILEID", 0);
        
        int dulation =  _dataRepository.getInt("WORKFLOWTASK","DURATION", 0) ;
        
        // set institutionId 
        _srk.getExpressState().setDealInstitutionId(institutionId);
        
        // load a workflowtask recoed from db
        // this data must be the same as the data from _dataRepository
        WorkflowTask wft = new WorkflowTask(_srk);
        wft = wft.findByPrimaryKey(new WorkflowTaskBeanPK(workFlowId, taskId));
        
        // set different value. just for test
        wft.setDuration(dulation + 1);
        // test target: update data
        wft.ejbStore();

        // check if the data is updated
        WorkflowTask wft2 = new WorkflowTask(_srk);
        wft2 = wft2.findByPrimaryKey(new WorkflowTaskBeanPK(workFlowId, taskId));
        
        assertEquals(dulation + 1, wft2.getDuration());
        
        _log.info(BORDER_END, "testEjbStore");
    }
    
}
