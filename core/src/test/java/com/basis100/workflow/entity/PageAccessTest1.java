package com.basis100.workflow.entity;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.pk.PageAccessBeanPK;

public class PageAccessTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;
	private PageAccess pageAccess;

	public PageAccessTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PageAccess.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(PageAccess.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
		//return DatabaseOperation.UPDATE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.pageAccess = new PageAccess(srk);	
	}
	
	public void testFindByPrimaryKey() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
	   String userType=(String)testFindByPrimaryKey.getValue(0,"USERTYPEID");
	   int userTypeId=Integer.parseInt(userType);			
	   String userProfile=(String)testFindByPrimaryKey.getValue(0,"USERPROFILEID");
	   int userProfileId=Integer.parseInt(userProfile);		
	   PageAccessBeanPK pk=new PageAccessBeanPK(userTypeId,userProfileId);
    	pageAccess=pageAccess.findByPrimaryKey(pk);
    	assertEquals(userTypeId, pk.getUserProfileId());
}
	
	public void testApplyAsOverrides() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testApplyAsOverrides = dataSetTest.getTable("testApplyAsOverrides");
	    Hashtable<String, Vector<Integer>> accessList = new Hashtable<String, Vector<Integer>>(); 
	     PageAccess pageAccess=new PageAccess(srk);	     
    	 pageAccess.applyAsOverrides(pageAccess);
    	 assertNotNull(pageAccess);    	
}
	
	public void testGetAccessTable() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testGetAccessTable = dataSetTest.getTable("testGetAccessTable");
		 Hashtable<String, Integer> accessList = new Hashtable<String, Integer>();
		 PageAccess pageAccess=new PageAccess(srk);
	     accessList=pageAccess.getAccessTable();
	     assertNotNull(accessList);   	
}	
}