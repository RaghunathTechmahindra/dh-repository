/*
 * @(#)WorkflowTaskProcessAssocTest.java     Sep 7, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.workflow.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.WorkflowTaskProcessAssocPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test for WorkflowTaskProcessAssoc
 * 
 * @version 1.0 Sep 7, 2007
 * @author hiro
 */
public class WorkflowTaskProcessAssocTest extends ExpressEntityTestCase
        implements UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(WorkflowTaskProcessAssocTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test method for WorkflowTaskProcessAssoc.FindByPrimaryKey
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        _log.info(BORDER_START, "testFindByPrimaryKey");

        _srk.beginTransaction();
        Map<String, String> testData = createTestData();
        
        // prepare data
        int institutionProfileId = Integer.parseInt(testData
                .get("INSTITUTIONPROFILEID"));
        _log.info("#### instid = {}", institutionProfileId);
        int taskId = Integer.parseInt(testData.get("TASKID"));
        int orderNo = Integer.parseInt(testData.get("ORDERNO"));
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testFindByPrimaryKey
        WorkflowTaskProcessAssoc entity = new WorkflowTaskProcessAssoc(_srk);
        WorkflowTaskProcessAssocPK pk = new WorkflowTaskProcessAssocPK(taskId, orderNo);
        entity = entity.findByPrimaryKey(pk);
        
        //test
        assertEquals(testData.get("TASKID"), String.valueOf(entity.getTaskId()));
        assertEquals(testData.get("ORDERNO"), String.valueOf(entity.getOrderNo()));
        assertEquals(testData.get("PROCESSNAME"), String.valueOf(entity.getProcessName()));
        assertEquals(testData.get("INSTITUTIONPROFILEID"), String.valueOf(entity.getInstitutionProfileId()));
        

        _srk.cleanTransaction();
        _log.info(BORDER_END, "testFindByPrimaryKey");
    }
    
    /**
     * test method for WorkflowTaskProcessAssoc.FindByTaskId
     */
    @Test
    public void testFindByTaskId() throws Exception {

        _log.info(BORDER_START, "testFindByTaskId");

        _srk.beginTransaction();
        Map<String, String> testData = createTestData();
        
        // prepare data
        int institutionProfileId = Integer.parseInt(testData
                .get("INSTITUTIONPROFILEID"));
        _log.info("#### instid = {}", institutionProfileId);
        int taskId = Integer.parseInt(testData.get("TASKID"));
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testFindByTaskId
        WorkflowTaskProcessAssoc entity = new WorkflowTaskProcessAssoc(_srk);
        
        Collection<WorkflowTaskProcessAssoc> col = entity.findByTaskId(taskId);
        entity = (new ArrayList<WorkflowTaskProcessAssoc>(col)).get(0);
        
        //test
        assertEquals(testData.get("TASKID"), String.valueOf(entity.getTaskId()));
        assertEquals(testData.get("ORDERNO"), String.valueOf(entity.getOrderNo()));
        assertEquals(testData.get("PROCESSNAME"), String.valueOf(entity.getProcessName()));
        assertEquals(testData.get("INSTITUTIONPROFILEID"), String.valueOf(entity.getInstitutionProfileId()));
        

        _srk.cleanTransaction();
        _log.info(BORDER_END, "testFindByTaskId");
    }
    
    
    private Map<String, String> createTestData() throws Exception {

        _log.info("createTestData");
        // check if any data exists
        Connection con = _srk.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st
                .executeQuery("select count(*) from WORKFLOWTASKPROCESSASSOC");
        rs.next();
        if (rs.getInt(1) == 0) {
            _log.debug("no data exist in WORKFLOWTASKPROCESSASSOC");
            
            int anyTaskid = _dataRepository.getInt("TASK", "TASKID", 0);
            int anyInstitutionProfileId = _dataRepository.getInt("TASK",
                    "INSTITUTIONPROFILEID", 0);
            //test data create
            String sql = "INSERT INTO WORKFLOWTASKPROCESSASSOC "
                    + "( TASKID, ORDERNO, PROCESSNAME, INSTITUTIONPROFILEID )"
                    + " VALUES ( " + anyTaskid + ", 0, 'TEST-PROCESSNAME', "
                    + anyInstitutionProfileId + " )";
            st.execute(sql);
        }
        
        rs = st.executeQuery("select * from WORKFLOWTASKPROCESSASSOC");
        Map<String, String> map = new HashMap<String, String>();
        
        rs.next();
        map.put("TASKID", rs.getString("TASKID"));
        map.put("ORDERNO", rs.getString("ORDERNO"));
        map.put("PROCESSNAME", rs.getString("PROCESSNAME"));
        map.put("INSTITUTIONPROFILEID", rs.getString("INSTITUTIONPROFILEID"));
        
        _log.info("test data = " + map.toString());
        
        st.close();
        return map;
    }

}
