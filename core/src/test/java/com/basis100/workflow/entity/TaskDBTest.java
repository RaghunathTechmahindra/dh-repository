package com.basis100.workflow.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.workflow.pk.TaskBeanPK;

public class TaskDBTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private Task task;

	public TaskDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"TaskDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass()
				.getResource("TaskDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.task = new Task(srk);
	}

	public void testEjbStore() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException,
			JdbcTransactionException {
		ITable testEjbStore = dataSetTest.getTable("testEjbStore");
		srk.beginTransaction();
		String taskStore = (String) testEjbStore.getValue(0, "TASKID");
		int taskId = Integer.parseInt(taskStore);
		task.setTaskId(28);
		task.setTaskName("Request Mortgage Insurance");
		task.setTaskLabel("Request MI");
		task.setBaseTask("N");
		task.setBasedOnTaskId(-1);
		task.setFollowUpTaskId(-1);
		task.setLinkTaskId(-1);
		task.setPriorityId(91800);
		task.setDuration(0);
		task.setWarningTime(30600);
		task.setReissueAlarmDuration(3);
		task.setReminderTime(0);
		task.setCriticalFlag("N");
		task.setSentinelFlag("N");
		task.setSentinelOn(0);
		task.setVisibleFlag(1);
		task.setSentinelTo(0);
		task.setInstitutionId(2);
		// task.ejbStore();
		assert task.getBasedOnTaskId() != 0;
		assertNotNull(task);
		srk.rollbackTransaction();

	}

	public void testCreate() throws Exception {
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int taskId = Integer
				.parseInt((String) testCreate.getValue(0, "taskId"));
		String taskName = (String) testCreate.getValue(0, "taskName");
		String taskLabel = (String) testCreate.getValue(0, "taskLabel");
		String baseTask = (String) testCreate.getValue(0, "baseTask");
		int basedOnTaskId = Integer.parseInt((String) testCreate.getValue(0,
				"basedOnTaskId"));
		int followUpTaskId = Integer.parseInt((String) testCreate.getValue(0,
				"followUpTaskId"));
		int linkTaskId = Integer.parseInt((String) testCreate.getValue(0,
				"linkTaskId"));
		int priorityId = Integer.parseInt((String) testCreate.getValue(0,
				"priorityId"));
		long duration = Long.parseLong((String) testCreate.getValue(0,
				"duration"));
		long warningTime = Long.parseLong((String) testCreate.getValue(0,
				"warningTime"));
		long reissueAlarmDuration = Long.parseLong((String) testCreate
				.getValue(0, "reissueAlarmDuration"));
		long reminderTime = Long.parseLong((String) testCreate.getValue(0,
				"reminderTime"));
		String criticalFlag = (String) testCreate.getValue(0, "criticalFlag");
		String sentinelFlag = (String) testCreate.getValue(0, "sentinelFlag");
		int sentinelOn = Integer.parseInt((String) testCreate.getValue(0,
				"sentinelOn"));
		int visibleFlag = Integer.parseInt((String) testCreate.getValue(0,
				"visibleFlag"));
		int sentinelTo = Integer.parseInt((String) testCreate.getValue(0,
				"sentinelTo"));
		int institutionId = Integer.parseInt((String) testCreate.getValue(0,
				"institutionId"));
		task = task.create(taskId, taskName, taskLabel, baseTask,
				basedOnTaskId, followUpTaskId, linkTaskId, priorityId,
				duration, warningTime, reissueAlarmDuration, reminderTime,
				criticalFlag, sentinelFlag, sentinelOn, visibleFlag,
				sentinelTo, institutionId);
		assert task.getTaskId() != 0;
		srk.rollbackTransaction();
	}

	public void testFindByPrimaryKey() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKey");
		String task1 = (String) testFindByPrimaryKey.getValue(0, "taskId");
		int taskId = Integer.parseInt(task1);
		TaskBeanPK pk = new TaskBeanPK(taskId);
		task = task.findByPrimaryKey(pk);
		assertEquals(taskId, pk.getTaskId());
	}

	public void testFindByPrimaryKeyFailure() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKeyFailure");
		boolean status = false;
		try {
			String task1 = (String) testFindByPrimaryKey.getValue(0, "taskId");
			int taskId = Integer.parseInt(task1);
			TaskBeanPK pk = new TaskBeanPK(taskId);
			task = task.findByPrimaryKey(pk);
			status = true;
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);
	}
}
