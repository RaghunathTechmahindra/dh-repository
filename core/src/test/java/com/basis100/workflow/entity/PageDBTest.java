package com.basis100.workflow.entity;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.pk.PageBeanPK;

public class PageDBTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private Page page;
	Vector vector = null;

	public PageDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				Page.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(
				Page.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;

	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		page = new Page(srk);

	}

	public void testFindByPrimaryKey() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKey");
		String pagePrimary = (String) testFindByPrimaryKey
				.getValue(0, "PAGEID");
		int pageId = Integer.parseInt(pagePrimary);
		srk.getExpressState().setDealInstitutionId(2);
		PageBeanPK pk = new PageBeanPK(pageId);
		page = page.findByPrimaryKey(pk);
		assertEquals(pageId, pk.getPageId());
	}

	public void testFindByPrimaryKeyFailure() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKeyFailure");
		try {
			String pagePrimary = (String) testFindByPrimaryKey.getValue(0,
					"PAGEID");
			int pageId = Integer.parseInt(pagePrimary);
			srk.getExpressState().setDealInstitutionId(2);
			PageBeanPK pk = new PageBeanPK(pageId);
			page = page.findByPrimaryKey(pk);
			assertEquals(pageId, pk.getPageId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testFindByName() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByName = dataSetTest.getTable("testFindByName");
		String pageName = (String) testFindByName.getValue(0, "PAGENAME");
		page = page.findByName(pageName);
		assertEquals(pageName, page.getPageName());
	}

	public void testFindByNameFailure() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByName = dataSetTest.getTable("testFindByNameFailure");
		try {
			String pageName = (String) testFindByName.getValue(0, "PAGENAME");
			page = page.findByName(pageName);
			assertEquals(pageName, page.getPageName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testFindByAll() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByAll = dataSetTest.getTable("testFindByAll");
		String pageName = (String) testFindByAll.getValue(0, "PAGENAME");
		String pageAll = (String) testFindByAll.getValue(0, "PAGELABEL");
		String pagePrimary = (String) testFindByAll.getValue(0, "PAGEID");
		int pageId = Integer.parseInt(pagePrimary);
		String Institution = (String) testFindByAll.getValue(0,
				"INSTITUTIONPROFILEID");
		int InstitutionId = Integer.parseInt(Institution);
		vector = page.findByAll();
		assertNotNull(vector);
	}

	public void testCreate() throws Exception {
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int pageId = Integer
				.parseInt((String) testCreate.getValue(0, "pageId"));
		String pageName = (String) testCreate.getValue(0, "pageName");
		String pageLabel = (String) testCreate.getValue(0, "pageLabel");
		int institutionProfileId = Integer.parseInt((String) testCreate
				.getValue(0, "institutionProfileId"));
		page = page.create(pageId, pageName, pageLabel, true, true, false,
				false, false, false, institutionProfileId, false);
		assert page.getPageId() != 0;
		srk.rollbackTransaction();
	}
}
