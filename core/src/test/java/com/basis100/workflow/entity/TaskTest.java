package com.basis100.workflow.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.TaskBeanPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;


public class TaskTest extends ExpressEntityTestCase implements UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory.getLogger(TaskTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();

    }

    @After
    public void tearDown() throws Exception {
        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.workflow.entity.Task#create(int, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, long, long, long, long, java.lang.String, java.lang.String, int, int, int, int)}.
     */
    @Test
    public void testCreate() throws Exception {
        _log.info(BORDER_START, "testCreate");
        // FIXME : the usage of institutionId needs reconsideration
        _log.warn("the usage of institutionId needs reconsideration");
        _log.info(BORDER_END, "testCreate");
    }

   /**
     * Test method for
     * {@link com.basis100.workflow.entity.Task#findByPrimaryKey(com.basis100.workflow.pk.TaskBeanPK)}.
     * 
     * @throws Exception
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        _log.info(BORDER_START, "testFindByPrimaryKey");
        
        //set institutionid
        _srk.getExpressState().setDealInstitutionId(
                _dataRepository.getInt("TASK", "INSTITUTIONPROFILEID", 0));
        
        //test target : findByPrimaryKey
        Task task = new Task(_srk);
        task = task.findByPrimaryKey(
                new TaskBeanPK(_dataRepository.getInt("TASK", "TASKID", 0)));
        
        // test results of findByPrimaryKey
        assertEquals(_dataRepository.getInt("TASK", "TASKID", 0),
                task.getTaskId());
        assertEquals(_dataRepository.getString("TASK", "TASKNAME", 0), 
                task .getTaskName());
        assertEquals(_dataRepository.getString("TASK", "TASKLABEL", 0), 
                task .getTaskLabel());
        assertEquals(_dataRepository.getString("TASK", "BASETASK", 0), 
                task .getBaseTask());
        assertEquals(_dataRepository.getInt("TASK", "BASEDONTASKID", 0), 
                task .getBasedOnTaskId());
        assertEquals(_dataRepository.getInt("TASK", "FOLLOWUPTASKID", 0), 
                task .getFollowUpTaskId());
        assertEquals(_dataRepository.getInt("TASK", "LINKTASKID", 0), 
                task .getLinkTaskId());
        assertEquals(_dataRepository.getInt("TASK", "DURATION", 0), 
                task .getDuration());
        assertEquals(_dataRepository.getInt("TASK", "WARNINGTIME", 0), 
                task .getWarningTime());
        assertEquals(_dataRepository.getInt("TASK", "REISSUEALARMDURATION", 0), 
                task.getReissueAlarmDuration());
        assertEquals(_dataRepository.getInt("TASK", "PRIORITYID", 0), 
                task .getPriorityId());
        assertEquals(_dataRepository.getInt("TASK", "REMINDERTIME", 0), 
                task .getReminderTime());
        assertEquals(_dataRepository.getString("TASK", "CRITICALFLAG", 0), 
                task .getCriticalFlag());
        assertEquals(_dataRepository.getString("TASK", "SENTINELFLAG", 0), 
                task .getSentinelFlag());
        assertEquals(_dataRepository.getInt("TASK", "SENTINELON", 0), 
                task .getSentinelOn());
        assertEquals(_dataRepository.getInt("TASK", "VISIBLEFLAG", 0), 
                task .getVisibleFlag());
        assertEquals(_dataRepository.getInt("TASK", "SENTINELTO", 0), 
                task .getSentinelTo());

        assertEquals(_dataRepository.getInt("TASK", "INSTITUTIONPROFILEID", 0), 
                task.getInstitutionId());
        
        _log.info(BORDER_END, "testFindByPrimaryKey");
    }

}
