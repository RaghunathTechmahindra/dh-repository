package com.basis100.workflow.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.QuickLinks;
import com.basis100.deal.pk.QuickLinksBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class QuickLinksDBTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private QuickLinks quickLinks;
	Vector vector = null;

	public QuickLinksDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				QuickLinks.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.quickLinks = new QuickLinks(srk);
	}

	public void testUpdateQuickLinksInsert() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException,
			JdbcTransactionException {
		List<QuickLinks> quickLinksList = null;
		int oldBranchQuickLinks = 0;
		ITable testUpdateQuickLinks = dataSetTest
				.getTable("testUpdateQuickLinksInsert");
		try {
			srk.beginTransaction();
			int quicklinkId = Integer.parseInt((String) testUpdateQuickLinks
					.getValue(0, "QUICKLINKID"));
			String linkNameEnglish = (String) testUpdateQuickLinks.getValue(0,
					"LINKNAMEENGLISH");
			String linkNameFrench = (String) testUpdateQuickLinks.getValue(0,
					"LINKNAMEFRENCH");
			String uRLLink = (String) testUpdateQuickLinks.getValue(0,
					"URLLINK");
			int branchProfileId = Integer
					.parseInt((String) testUpdateQuickLinks.getValue(0,
							"BRANCHPROFILEID"));
			int institutionProfileId = Integer
					.parseInt((String) testUpdateQuickLinks.getValue(0,
							"INSTITUTIONPROFILEID"));
			int sortorder = Integer.parseInt((String) testUpdateQuickLinks
					.getValue(0, "SORTORDER"));
			String favicon = (String) testUpdateQuickLinks.getValue(0,
					"FAVICON");
			quickLinks.setQuickLinkId(quicklinkId);
			quickLinks.setLinkNameEnglish(linkNameEnglish);
			quickLinks.setLinkNameFrench(linkNameFrench);
			quickLinks.setUrlAddress(uRLLink);
			quickLinks.setBranchProfileId(branchProfileId);
			quickLinks.setInstitutionProfileId(institutionProfileId);
			quickLinks.setSortOrder(sortorder);
			quickLinks.setFavicon(favicon);
			quickLinksList = new ArrayList<QuickLinks>();
			quickLinksList.add(quickLinks);
			quickLinks.updateQuickLinks(quickLinksList, oldBranchQuickLinks);
			assertNotNull(quickLinks);
			srk.rollbackTransaction();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testUpdateQuickLinksUpdate() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException,
			JdbcTransactionException {
		List<QuickLinks> quickLinksList = null;
		int oldBranchQuickLinks = 0;
		ITable testUpdateQuickLinks = dataSetTest
				.getTable("testUpdateQuickLinksUpdate");
		try {
			srk.beginTransaction();
			int quicklinkId = Integer.parseInt((String) testUpdateQuickLinks
					.getValue(0, "QUICKLINKID"));
			String linkNameEnglish = (String) testUpdateQuickLinks.getValue(0,
					"LINKNAMEENGLISH");
			String linkNameFrench = (String) testUpdateQuickLinks.getValue(0,
					"LINKNAMEFRENCH");
			String uRLLink = (String) testUpdateQuickLinks.getValue(0,
					"URLLINK");
			int branchProfileId = Integer
					.parseInt((String) testUpdateQuickLinks.getValue(0,
							"BRANCHPROFILEID"));
			int institutionProfileId = Integer
					.parseInt((String) testUpdateQuickLinks.getValue(0,
							"INSTITUTIONPROFILEID"));
			int sortorder = Integer.parseInt((String) testUpdateQuickLinks
					.getValue(0, "SORTORDER"));
			String favicon = (String) testUpdateQuickLinks.getValue(0,
					"FAVICON");
			quickLinks.setQuickLinkId(quicklinkId);
			quickLinks.setLinkNameEnglish(linkNameEnglish);
			quickLinks.setLinkNameFrench(linkNameFrench);
			quickLinks.setUrlAddress(uRLLink);
			quickLinks.setBranchProfileId(branchProfileId);
			quickLinks.setInstitutionProfileId(institutionProfileId);
			quickLinks.setSortOrder(sortorder);
			quickLinks.setFavicon(favicon);
			quickLinksList = new ArrayList<QuickLinks>();
			quickLinksList.add(quickLinks);
			quickLinks.updateQuickLinks(quickLinksList, oldBranchQuickLinks);
			assertNotNull(quickLinks);
			srk.rollbackTransaction();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testUpdateQuickLinksDelete() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException,
			JdbcTransactionException {
		List<QuickLinks> quickLinksList = null;
		int oldBranchQuickLinks = 100;
		ITable testUpdateQuickLinks = dataSetTest
				.getTable("testUpdateQuickLinksDelete");
		try {
			srk.beginTransaction();
			int quicklinkId = Integer.parseInt((String) testUpdateQuickLinks
					.getValue(0, "QUICKLINKID"));
			String linkNameEnglish = (String) testUpdateQuickLinks.getValue(0,
					"LINKNAMEENGLISH");
			String linkNameFrench = (String) testUpdateQuickLinks.getValue(0,
					"LINKNAMEFRENCH");
			String uRLLink = (String) testUpdateQuickLinks.getValue(0,
					"URLLINK");
			int branchProfileId = Integer
					.parseInt((String) testUpdateQuickLinks.getValue(0,
							"BRANCHPROFILEID"));
			int institutionProfileId = Integer
					.parseInt((String) testUpdateQuickLinks.getValue(0,
							"INSTITUTIONPROFILEID"));
			int sortorder = Integer.parseInt((String) testUpdateQuickLinks
					.getValue(0, "SORTORDER"));
			String favicon = (String) testUpdateQuickLinks.getValue(0,
					"FAVICON");
			quickLinks.setQuickLinkId(quicklinkId);
			quickLinks.setLinkNameEnglish(linkNameEnglish);
			quickLinks.setLinkNameFrench(linkNameFrench);
			quickLinks.setUrlAddress(uRLLink);
			quickLinks.setBranchProfileId(branchProfileId);
			quickLinks.setInstitutionProfileId(institutionProfileId);
			quickLinks.setSortOrder(sortorder);
			quickLinks.setFavicon(favicon);
			quickLinksList = new ArrayList<QuickLinks>();
			quickLinksList.add(quickLinks);
			quickLinks.updateQuickLinks(quickLinksList, oldBranchQuickLinks);
			assertNotNull(quickLinks);
			srk.rollbackTransaction();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testGetNextQuickLinksId() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException,
			JdbcTransactionException {
		ITable testUpdateQuickLinks = dataSetTest
				.getTable("testGetNextQuickLinksId");
		srk.beginTransaction();
		quickLinks.getNextQuickLinksId();
		assertNotNull(quickLinks);
		srk.rollbackTransaction();
	}

	public void testFindByPrimaryKey() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException,
			JdbcTransactionException {
		ITable testUpdateQuickLinks = dataSetTest
				.getTable("testFindByPrimaryKey");
		srk.beginTransaction();
		int quicklinkId = Integer.parseInt((String) testUpdateQuickLinks
				.getValue(0, "QUICKLINKID"));
		QuickLinksBeanPK quickLinksBeanPK = new QuickLinksBeanPK(quicklinkId);
		quickLinks = quickLinks.findByPrimaryKey(quickLinksBeanPK);
		assertNotNull(quickLinks);
		assertEquals(quicklinkId, quickLinksBeanPK.getQuickLinkId());
		srk.rollbackTransaction();
	}

	public void testFindByPrimaryKeyFailure() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException,
			JdbcTransactionException {
		ITable testUpdateQuickLinks = dataSetTest
				.getTable("testFindByPrimaryKeyFailure");
		try {
			srk.beginTransaction();
			int quicklinkId = Integer.parseInt((String) testUpdateQuickLinks
					.getValue(0, "QUICKLINKID"));
			QuickLinksBeanPK quickLinksBeanPK = new QuickLinksBeanPK(
					quicklinkId);
			quickLinks = quickLinks.findByPrimaryKey(quickLinksBeanPK);
			assertNotNull(quickLinks);
			assertEquals(quicklinkId, quickLinksBeanPK.getQuickLinkId());
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}