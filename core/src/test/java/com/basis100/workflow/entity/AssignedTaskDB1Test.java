package com.basis100.workflow.entity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.pk.AssignedTaskBeanPK;

public class AssignedTaskDB1Test extends FXDBTestCase {

	private IDataSet dataSetTest;
	private AssignedTask assignedTask;
	Vector vector = null;
	private WorkflowTask wtTask = null;
	private SimpleDateFormat formate = null;

	public AssignedTaskDB1Test(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				AssignedTask.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(
				AssignedTask.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
		// return DatabaseOperation.UPDATE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.assignedTask = new AssignedTask(srk);
	}

	public void testFindByPrimaryKey1() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKey1");
		String assignedTasksWorkQueue = (String) testFindByPrimaryKey.getValue(
				0, "ASSIGNEDTASKSWORKQUEUEID");
		int assignedTasksWorkQueueId = Integer.parseInt(assignedTasksWorkQueue);
		AssignedTaskBeanPK pk = new AssignedTaskBeanPK(assignedTasksWorkQueueId);
		srk.getExpressState().setDealInstitutionId(0);
		assignedTask = assignedTask.findByPrimaryKey(pk);
		assertEquals(assignedTasksWorkQueueId, pk.getAssignedTasksWorkQueueId());
	}

	public void testResetOutstandingTasks1() throws Exception {
		ITable testResetOutstandingTasks = dataSetTest
				.getTable("testResetOutstandingTasks1");
		srk.beginTransaction();
		String userProfile = (String) testResetOutstandingTasks.getValue(0,
				"USERPROFILEID");
		int userProfileId = Integer.parseInt(userProfile);
		srk.getExpressState().setDealInstitutionId(1);
		int result = assignedTask.resetOutstandingTasks(userProfileId);
		srk.rollbackTransaction();
		assert assignedTask.getApplicationId() != null;
		assert assignedTask.getApplicationId().length() <= 0;

	}

	public void testSetRetryExpired1() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testSetRetryExpired = dataSetTest
				.getTable("testSetRetryExpired1");
		assignedTask.setTaskStatusId(1);
		assignedTask.setRetryExpired();
		assertNotNull(assignedTask);
	}

	public void testSetForRetry1() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testSetRetryExpired = dataSetTest
				.getTable("testSetRetryExpired1");
		assignedTask.setTaskStatusId(1);
		assignedTask.setForRetry(true);
		assertTrue("assignedTask", true);
	}

	public void testFindByDealbyStatus() throws Exception {
		Vector<AssignedTask> v = null;
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindByDealbyStatus");
		int dealId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "dealId"));
		int taskStatusId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "taskStatusId"));
		srk.getExpressState().setDealInstitutionId(1);
		v = assignedTask.findByDealbyStatus(dealId, taskStatusId);
		assert v.size() != 0;
	}

	public void testFindByDeal() throws Exception {
		Vector<AssignedTask> v = null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDeal");
		int dealId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "dealId"));
		srk.getExpressState().setDealInstitutionId(1);
		v = assignedTask.findByDeal(dealId);
		assert v.size() != 0;
	}

	public void testFindNextAutoTask() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindNextAutoTask");
		int userProfileId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "userProfileId"));
		int retryCounter = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "retryCounter"));
		srk.getExpressState().setDealInstitutionId(0);
		assignedTask = assignedTask.findNextAutoTask(userProfileId,
				retryCounter);
		assertEquals(userProfileId, assignedTask.getUserProfileId());
	}

	public void testFindNextAutoTaskFailure() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindNextAutoTaskFailure");
		int userProfileId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "userProfileId"));
		int retryCounter = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "retryCounter"));
		srk.getExpressState().setDealInstitutionId(1);
		assignedTask = assignedTask.findNextAutoTask(userProfileId,
				retryCounter);
		assertNull(assignedTask);
	}

	public void testFindByCreateCopy() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindByCreateCopy");
		int assignedTasksWorkQueueId = Integer
				.parseInt((String) findByDupeCheckCriteria.getValue(0,
						"assignedTasksWorkQueueId"));
		srk.getExpressState().setDealInstitutionId(1);
		AssignedTask assignedTask1 = new AssignedTask(srk);
		assignedTask1.setAssignedTasksWorkQueueId(assignedTasksWorkQueueId);
		assignedTask = assignedTask.findByCreateCopy(assignedTask1);
		assertNotNull(assignedTask);
	}

	public void testFindByCreateCopyFailure() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindByCreateCopyFailure");
		boolean status = false;
		try {
			int assignedTasksWorkQueueId = Integer
					.parseInt((String) findByDupeCheckCriteria.getValue(0,
							"assignedTasksWorkQueueId"));
			srk.getExpressState().setDealInstitutionId(1);
			AssignedTask assignedTask1 = new AssignedTask(srk);
			assignedTask1.setAssignedTasksWorkQueueId(assignedTasksWorkQueueId);
			assignedTask = assignedTask.findByCreateCopy(assignedTask1);
			status = true;
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);
	}

	public void testFindByPrimaryKey1Failure() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKey1Failure");
		boolean status = false;
		try {
			String assignedTasksWorkQueue = (String) testFindByPrimaryKey
					.getValue(0, "ASSIGNEDTASKSWORKQUEUEID");
			int assignedTasksWorkQueueId = Integer
					.parseInt(assignedTasksWorkQueue);
			AssignedTaskBeanPK pk = new AssignedTaskBeanPK(
					assignedTasksWorkQueueId);
			srk.getExpressState().setDealInstitutionId(0);
			assignedTask = assignedTask.findByPrimaryKey(pk);
			status = true;
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);
	}

	public void testHandleTimeEvent() throws Exception {
		boolean status = false;
		status = assignedTask.handleTimeEvent();
		assertEquals(true, status);
	}
}
