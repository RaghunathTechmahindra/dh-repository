/*
 * @(#)AssignedTaskTest.java     Sep 10, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.workflow.entity;

import java.sql.ResultSet;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.basis100.workflow.pk.WorkflowTaskBeanPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test for AssignedTask
 * 
 * @version 1.0 Sep 10, 2007
 * @author hiro
 * @since 3.3
 */
public class AssignedTaskTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(AssignedTaskTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }
    

    /**
     * test method for AssignedTask.Create
     */
    @Test
    public void testCreate() throws Exception {

        _log.info(BORDER_START, "testCreate");

        //prepare data
        int institutionProfileId = _dataRepository.getInt("deal",
                "INSTITUTIONPROFILEID", 0);

        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        _srk.beginTransaction();
        
        WorkflowTask wtTask = new WorkflowTask(_srk);
        int workflowId = _dataRepository.getInt("WorkflowTask",  "workflowId", 0);
        int taskId = _dataRepository.getInt("WorkflowTask",  "taskId", 0);
        _log.info("workflowId=" + workflowId);
        _log.info("taskId=" + taskId);
        wtTask = wtTask.findByPrimaryKey(new WorkflowTaskBeanPK(workflowId,taskId));
        
        int dealid = _dataRepository.getInt("deal",  "dealid", 0);
        int copyid = _dataRepository.getInt("deal",  "copyid", 0);
        int institutionIdId = _dataRepository.getInt("deal",  "institutionIdid", 0);
        _log.info("dealid=" + dealid);
        _log.info("copyid=" + copyid);
        Deal deal = new Deal(_srk,null,dealid,copyid);
        
        // test target method
        AssignedTask entity = new AssignedTask(_srk);
        entity.create(wtTask, institutionIdId, dealid, deal.getApplicationId(), deal.getUnderwriterUserId(),
                -1, 1, new Date(), deal.getEstimatedClosingDate(), -1);

        //#### AssignedTask.create method doesn't always return created brand-new record.
        //#### so load target record using ASSIGNEDTASKSWORKQUEUESEQ.currval 
        String sql = "select ASSIGNEDTASKSWORKQUEUESEQ.currval from dual";
        ResultSet rs = _srk.getConnection().createStatement().executeQuery(sql);
        int currvalAWQID = (rs.next()) ? rs.getInt(1) : -1;
        _log.info("ASSIGNEDTASKSWORKQUEUESEQ.currval = " + currvalAWQID);
        entity.findByPrimaryKey(new AssignedTaskBeanPK(currvalAWQID));
        
        // test
        assertEquals(institutionProfileId, entity.getInstitutionProfileId());
        assertEquals(currvalAWQID, entity.getAssignedTasksWorkQueueId());
        assertEquals(dealid, entity.getDealId());
        assertEquals(workflowId, entity.getWorkflowId());
        assertEquals(taskId, entity.getTaskId());
        
        _srk.rollbackTransaction();
        _log.info(BORDER_END, "testCreate");
    }

    /**
     * test method for AssignedTask.findByPrimaryKey
     */
    @Test
    public void testfindByPrimaryKey() throws Exception {

        _log.info(BORDER_START, "testfindByPrimaryKey");

        //prepare data
        int institutionProfileId = _dataRepository.getInt("ASSIGNEDTASKSWORKQUEUE",
                "INSTITUTIONPROFILEID", 0);
        int assignedTaskWorkQueueId = _dataRepository.getInt("ASSIGNEDTASKSWORKQUEUE",
                "ASSIGNEDTASKSWORKQUEUEID", 0);
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testfindByPrimaryKey
        AssignedTask entity = new AssignedTask(_srk);
        entity.findByPrimaryKey(new AssignedTaskBeanPK(assignedTaskWorkQueueId));
        
        assertEquals(institutionProfileId, entity.getInstitutionProfileId());
        assertEquals(assignedTaskWorkQueueId, entity.getAssignedTasksWorkQueueId());

        _log.info(BORDER_END, "testfindByPrimaryKey");
    }


}



