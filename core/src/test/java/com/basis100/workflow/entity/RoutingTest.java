/*
 * @(#)RoutingTest.java     Sep 11, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.workflow.entity;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * RoutingTest the unit test class for Routing class.
 *
 * @version   1.0 2005-5-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 *
 * @version   1.1 Sep 11, 2007
 * @author    hiro
 */
public class RoutingTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(RoutingTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test method for Routing.findByPrimaryKey
     */
    @Test
    public void testfindByPrimaryKey() throws Exception {

        _log.info(BORDER_START, "testfindByPrimaryKey");

        //prepare data
        int institutionProfileId = _dataRepository.getInt("ROUTING",
                "INSTITUTIONPROFILEID", 0);
        int routingId = _dataRepository.getInt("ROUTING", "ROUTINGID", 0);
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testfindByPrimaryKey
        Routing entity = new Routing(_srk);
        entity.findByPrimaryKey(routingId);
        
        //test
        assertEquals(routingId, entity.getRoutingId());
        assertEquals(_dataRepository.getInt("ROUTING", "ROUTETYPEID", 0), 
                entity.getRouteTypeId());
        assertEquals(_dataRepository.getString("ROUTING", "ROUTEKEY", 0), 
                entity.getRouteKey());
        assertEquals(_dataRepository.getInt("ROUTING", "MATCHTYPEID", 0), 
                entity.getMatchTypeId());
        assertEquals(_dataRepository.getInt("ROUTING", "MATCHVALUE", 0), 
                entity.getMatchValue());
        assertEquals(_dataRepository.getInt("ROUTING", "INSTITUTIONPROFILEID", 0), 
                entity.getInstitutionProfileId());
        
        _log.info(BORDER_END, "testfindByPrimaryKey");
    }
    

    /**
     * Testing get ordered list.
     */
    @Test
    public void testGetOrderedList() {
        Routing _routingDAO = new Routing(_srk);
        try {
            List orderedList = _routingDAO.getOrderedList();
            _log.info("The amount of routing records: " + orderedList.size());
            for (Iterator i = orderedList.iterator(); i.hasNext(); ) {
                _log.info("==================");
                Routing routing = (Routing) i.next();
                _log.info("          Route Type Id: " + routing.getRouteTypeId());
                _log.info("              Route Key: " + routing.getRouteKey());
                _log.info("          Match Type Id: " + routing.getMatchTypeId());
                _log.info("            Match Value: " + routing.getMatchValue());
                _log.info(" Institution Profile Id: " + routing.getInstitutionProfileId());
                
                
            }
        } catch (RemoteException re) {
            _log.error("Remote Exception: ", re);
        } catch (FinderException fe) {
            _log.error("Finder Exception: ", fe);
        }
    }
}
