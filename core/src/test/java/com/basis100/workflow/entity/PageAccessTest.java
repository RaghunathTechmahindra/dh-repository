/*
 * @(#)PageAccessTest.java     Sep 10, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.workflow.entity;

import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Sc;

import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.PageAccessBeanPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test for PageAccess
 *
 * @version   1.0  Sep 10, 2007
 * @author    hiro
 * @since     3.3
 */
public class PageAccessTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(PageAccessTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }
    
    
    /**
     * test method for PageAccess.Create
     */
       @Test(expected=CreateException.class)
   public void testCreate_fail() throws Exception {

        _log.info(BORDER_START, "testCreate");

        //prepare data
        //prepare data
        int institutionProfileId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "INSTITUTIONPROFILEID", 0);
        int userTypeId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "USERTYPEID", 0);
        int pageId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "PAGEID", 0);
        int accessTypeId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "ACCESSTYPEID", 0);
        
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        _srk.beginTransaction();

        PageAccess entity = new PageAccess(_srk);
        entity = entity.create(userTypeId, 0, institutionProfileId);

        _srk.rollbackTransaction();
        _log.info(BORDER_END, "testCreate");
    }
    
    /**
     * test method for PageAccess.Create
     */
    @Test
    public void testCreate_success() throws Exception {

        _log.info(BORDER_START, "testCreate");

        //prepare data
        int institutionProfileId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "INSTITUTIONPROFILEID", 0);
        int userTypeId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "USERTYPEID", 0);
        int pageId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "PAGEID", 0);
        int accessTypeId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "ACCESSTYPEID", 0);
        
        _srk.beginTransaction();
        
        
        String sql = "delete USERTYPEPAGEACCESS where USERTYPEID = " + userTypeId;
        Statement st = _srk.getConnection().createStatement();
        int cnt = st.executeUpdate(sql);
        
        PageAccess entity = new PageAccess(_srk);
        try{
            entity.findByPrimaryKey(new PageAccessBeanPK(userTypeId,0));
            fail("finder exception must be happend");
        }catch(FinderException e){
            assertTrue(true);
        }
        
        entity = entity.create(userTypeId, 0, institutionProfileId);
        entity.setAccessPermission(pageId,institutionProfileId, accessTypeId);
        entity.ejbStore();
        
        PageAccess entity2 = new PageAccess(_srk);
        entity2.findByPrimaryKey(new PageAccessBeanPK(userTypeId,0));
        
        int actual = entity2.getAccessPermission(pageId, institutionProfileId);
        assertEquals(accessTypeId, actual);
        
        _srk.rollbackTransaction();
        _log.info(BORDER_END, "testCreate");
    }

    /**
     * test method for PageAccess.Update
     */
    @Test
    public void testUpdate() throws Exception {

        _log.info(BORDER_START, "testUpdate");
         _srk.beginTransaction();
        //prepare data
        int institutionProfileId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "INSTITUTIONPROFILEID", 0);
        int userTypeId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "USERTYPEID", 0);
        int pageId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "PAGEID", 0);
        int accessTypeId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "ACCESSTYPEID", 0);

        //test target of testUpdate
        PageAccess entity = new PageAccess(_srk);
        entity = entity.findByPrimaryKey(new PageAccessBeanPK(userTypeId,0));
        
        int actual = entity.getAccessPermission(pageId, institutionProfileId);
        assertEquals(accessTypeId, actual);
        
        int testAccessTypeId = 0;
        if(accessTypeId == Sc.PAGE_ACCESS_EDIT){
            testAccessTypeId = Sc.PAGE_ACCESS_DISALLOWED;
        }else{
            testAccessTypeId = Sc.PAGE_ACCESS_EDIT;
        }
        
        entity.setAccessPermission(pageId, institutionProfileId, testAccessTypeId);
        entity.ejbStore();
        
        PageAccess entity2 = new PageAccess(_srk);
        entity2 = entity.findByPrimaryKey(new PageAccessBeanPK(userTypeId,0));
        actual = entity2.getAccessPermission(pageId, institutionProfileId);

        assertEquals(testAccessTypeId, actual);
        
        _log.info(BORDER_END, "testUpdate");
        _srk.rollbackTransaction();
    }
    
    /**
     * test method for PageAccess.Remove
     */
    @Test
    public void testRemove() throws Exception {
      _srk.beginTransaction();
        _log.info(BORDER_START, "testRemove");

        //prepare data
        int institutionProfileId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "INSTITUTIONPROFILEID", 0);
        int userTypeId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "USERTYPEID", 0);
        int pageId = _dataRepository.getInt(
                "USERTYPEPAGEACCESS", "PAGEID", 0);
        
        //test target of testRemove
        PageAccess entity = new PageAccess(_srk);
        entity = entity.findByPrimaryKey(new PageAccessBeanPK(userTypeId,0));
        entity.removeAccessPermission(pageId, institutionProfileId);
        entity.ejbStore();
        
        PageAccess entity2 = new PageAccess(_srk);
        entity2 = entity.findByPrimaryKey(new PageAccessBeanPK(userTypeId,0));
        int actual = entity2.getAccessPermission(pageId, institutionProfileId);
       
        assertEquals(0, actual);

        _log.info(BORDER_END, "testRemove");
        _srk.rollbackTransaction();
    }
    
    /**
     * test method for PageAccess.MakeKey
     */
    @Test
    public void testMakeKey() throws Exception {
        _log.info(BORDER_START, "testMakeKey");

        String actual = PageAccess.makeKey(12, 100);
        assertEquals("12#100", actual);
        
        _log.info(BORDER_END, "testMakeKey");
    }
    
    /**
     * test method for PageAccess.GetPageIdFromKey
     */
    @Test
    public void testGetPageIdFromKey() throws Exception {

        _log.info(BORDER_START, "testGetPageIdFromKey");

        int actual = PageAccess.getPageIdFromKey("12#315");
        assertEquals(12, actual);
        
        actual = PageAccess.getPageIdFromKey("hoge");
        assertEquals(0, actual);
        

        _log.info(BORDER_END, "testGetPageIdFromKey");
    }

    /**
     * test method for PageAccess.GetInstitutionIdFromKey
     */
    @Test
    public void testGetInstitutionIdFromKey() throws Exception {

        _log.info(BORDER_START, "testGetInstitutionIdFromKey");

        int actual = PageAccess.getInstitutionIdFromKey("12#315");
        assertEquals(315, actual);
        
        actual = PageAccess.getInstitutionIdFromKey("hoge");
        assertEquals(0, actual);
        

        _log.info(BORDER_END, "testGetInstitutionIdFromKey");
    }
    
    /**
     * test method for PageAccess.GetInstitutionIdFromKey
     */
    @Test
    public void testDecomposeKey() throws Exception {

        _log.info(BORDER_START, "testDecomposeKey");

        int[] actual = PageAccess.decomposeKey("12#315");
        assertEquals(12, actual[0]);
        assertEquals(315, actual[1]);
        
        _log.info(BORDER_END, "testDecomposeKey");
    }
    
    
    
    
}