package com.basis100.workflow.entity;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.pk.PageBeanPK;

public class PageTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private Page page;
	Vector vector = null;

	public PageTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				Page.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;

	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		page = new Page(srk);

	}

	public void testFindByPrimaryKey() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKey");
		String pagePrimary = (String) testFindByPrimaryKey
				.getValue(0, "PAGEID");
		int pageId = Integer.parseInt(pagePrimary);
		PageBeanPK pk = new PageBeanPK(pageId);
		page = page.findByPrimaryKey(pk);
		assertEquals(pageId, pk.getPageId());
	}

	public void testFindByName() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByName = dataSetTest.getTable("testFindByName");
		String pageName = (String) testFindByName.getValue(0, "PAGENAME");
		page = page.findByName(pageName);
		assertEquals(pageName, page.getPageName());
	}

	public void testFindByAll() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testFindByAll = dataSetTest.getTable("testFindByAll");
		String pageName = (String) testFindByAll.getValue(0, "PAGENAME");
		String pageAll = (String) testFindByAll.getValue(0, "PAGELABEL");
		String pagePrimary = (String) testFindByAll.getValue(0, "PAGEID");
		int pageId = Integer.parseInt(pagePrimary);
		String Institution = (String) testFindByAll.getValue(0,
				"INSTITUTIONPROFILEID");
		int InstitutionId = Integer.parseInt(Institution);
		vector = page.findByAll();
		assertNotNull(vector);
	}

}
