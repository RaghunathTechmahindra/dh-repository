package com.basis100.workflow.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class TaskTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private Task task;

	public TaskTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"TaskDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass()
				.getResource("TaskDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.task = new Task(srk);
	}

	public void testEjbStore() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException, JdbcTransactionException {
		ITable testEjbStore = dataSetTest.getTable("testEjbStore");
		srk.beginTransaction();
		String taskStore = (String) testEjbStore.getValue(0, "TASKID");
		int taskId = Integer.parseInt(taskStore);
		task.setTaskId(28);
		task.setTaskName("Request Mortgage Insurance");
		task.setTaskLabel("Request MI");
		task.setBaseTask("N");
		task.setBasedOnTaskId(-1);
		task.setFollowUpTaskId(-1);
		task.setLinkTaskId(-1);
		task.setPriorityId(91800);
		task.setDuration(0);
		task.setWarningTime(30600);
		task.setReissueAlarmDuration(3);
		task.setReminderTime(0);
		task.setCriticalFlag("N");
		task.setSentinelFlag("N");
		task.setSentinelOn(0);
		task.setVisibleFlag(1);
		task.setSentinelTo(0);
		task.setInstitutionId(2);
		//task.ejbStore();
		assert task.getBasedOnTaskId()!=0;
		assertNotNull(task);
		srk.rollbackTransaction();
		
	}

}
