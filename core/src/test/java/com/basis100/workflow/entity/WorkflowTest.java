/*
 * @(#)WorkflowTest.java    Sep 7, 2007
 *
 * Copyrightę 2007. Filogix Limited Partnership. All rights reserved. 
 */
package com.basis100.workflow.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.pk.WorkflowBeanPK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test class for Workflow
 *
 * @version   1.0 Sep 7, 2007
 * @author    hiro
 */
public class WorkflowTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(WorkflowTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    @Test
    public void testFindById() throws Exception {

        _log.info(BORDER_START, "testFindById");

        // prepare test target data
        int workFlowId = _dataRepository.getInt("WORKFLOW", "WORKFLOWID", 0);
        String wfName = _dataRepository.getString("WORKFLOW", "WFNAME", 0);
        int numberOfStages = _dataRepository.getInt("WORKFLOW",
                "NUMBEROFSTAGES", 0);
        int institutionProfileId = _dataRepository.getInt("WORKFLOW",
                "INSTITUTIONPROFILEID", 0);

        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        Workflow wf = new Workflow(_srk);
        // target method
        wf.findById(workFlowId);

        assertEquals(workFlowId, wf.getWorkflowId());
        assertEquals(wfName, wf.getWfName());
        assertEquals(numberOfStages, wf.getNumberOfStages());
        assertEquals(institutionProfileId, wf.getInstitutionProfileId());

        _log.info(BORDER_END, "testFindById");
    }

    /**
     * test method for WorkFlow.FindByPrimaryKey
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        _log.info(BORDER_START, "testFindByPrimaryKey");

        //prepare data
        int workFlowId = _dataRepository.getInt("WORKFLOW", "WORKFLOWID", 0);
        int numberOfStages = _dataRepository.getInt("WORKFLOW",
                "NUMBEROFSTAGES", 0);
        int institutionProfileId = _dataRepository.getInt("WORKFLOW",
                "INSTITUTIONPROFILEID", 0);
        String workFlowName = _dataRepository.getString("WORKFLOW", "WFNAME", 0);
        
        // apply institutionId
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        //test target of testFindByPrimaryKey
        Workflow wf = new Workflow(_srk);
        WorkflowBeanPK pk = new WorkflowBeanPK(workFlowName);
        wf = wf.findByPrimaryKey(pk);

        assertEquals(workFlowId, wf.getWorkflowId());
        assertEquals(workFlowName, wf.getWfName());
        assertEquals(numberOfStages, wf.getNumberOfStages());
        assertEquals(institutionProfileId, wf.getInstitutionProfileId());
        
        _log.info(BORDER_END, "testFindByPrimaryKey");
    }
    
    @Test
    public void testEjbStore() throws Exception {
        _log.info(BORDER_START, "testEjbStore");

        int workFlowId = _dataRepository.getInt("WORKFLOW", "WORKFLOWID", 0);
        int numberOfStages = _dataRepository.getInt("WORKFLOW",
                "NUMBEROFSTAGES", 0);
        int institutionProfileId = _dataRepository.getInt("WORKFLOW",
                "INSTITUTIONPROFILEID", 0);

        _srk.beginTransaction();
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        try {
            Workflow wf = new Workflow(_srk);
            wf = wf.findById(workFlowId);

            // will change field:numberOfStages.
            wf.addStage();

            // #target: ejbStore should store the changed numberOfStages into DB
            wf.ejbStore();

            // use another instance to check field:numberOfStages.
            Workflow wf2 = new Workflow(_srk);
            wf2 = wf2.findById(workFlowId);

            // data in DB must be increased.
            assertEquals(numberOfStages + 1, wf2.getNumberOfStages());

        } finally {
            _srk.cleanTransaction();
        }

        _log.info(BORDER_END, "testEjbStore");
    }

}
