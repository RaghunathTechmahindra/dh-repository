package com.basis100.workflow;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.BREngine.BusinessRule;
import com.basis100.deal.entity.Deal;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.entity.Workflow;
import com.basis100.workflow.entity.WorkflowTask;


/**
 * <p>DealQueryTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class WorkflowAssistBeanDBTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private WorkflowAssistBean workflowAssistBean;
	private AssignedTask assignedTask;
	private DealUserProfiles dealUserProfiles;
	private WFDealTaskHistory taskHistory;
	private WorkflowTask workflowTask;
	private Workflow workflow;
	private BusinessRule businessRule;
	Deal deal = new Deal();
	// session resource kit
    //private SessionResourceKit _srk;

	public WorkflowAssistBeanDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(WorkflowAssistBean.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(WorkflowAssistBean.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.workflowAssistBean = new WorkflowAssistBean();
	}
	
    public void testGenerateInitialTasks1() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testGenerateInitialTasks1");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "DEALID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		deal.setDealId(dealId);
		// feed the input data and run the program.
		 srk.getExpressState().setDealInstitutionId(instProfId);
		 boolean wfab = workflowAssistBean.generateInitialTasks(deal, srk);
        // verify the running result.
		 assertFalse(wfab);
        
    }
   
    public void testGenerateInitialTasks2() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testGenerateInitialTasks2");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "COPYID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		
		deal.setDealId(dealId);
		//deal.setCopyId(copyId);
		// feed the input data and run the program.
		 srk.getExpressState().setDealInstitutionId(instProfId);
		 boolean wfab =  workflowAssistBean.generateInitialTasks(dealId, copyId, srk);
        // verify the running result.
		 assertFalse(wfab);
        
    }
    
    public void testAssignAdminForNewUnderwriter() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testAssignAdminForNewUnderwriter");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "DEALID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		deal.setDealId(dealId);
		// feed the input data and run the program.
		 srk.getExpressState().setDealInstitutionId(instProfId);
		 boolean wfab = workflowAssistBean.generateInitialTasks(deal, srk);
        // verify the running result.
		 assertFalse(wfab);
        
    }
    
    public void testAssignFunderForNewUnderwriter() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testAssignFunderForNewUnderwriter");
		int dealId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "DEALID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		deal.setDealId(dealId);
		// feed the input data and run the program.
		srk.getExpressState().setDealInstitutionId(instProfId);
       // DocumentQueue entity = new DocumentQueue(srk, documentQueueId);
		deal.setDealId(dealId);
		// feed the input data and run the program.
		 srk.getExpressState().setDealInstitutionId(instProfId);
		 boolean wfab = workflowAssistBean.generateInitialTasks(deal, srk);
        // verify the running result.
		 assertFalse(wfab);
    }
    
   public void testGenerateTasks1() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testGenerateTasks1");
		int dealId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "DEALID"));
		int workflowId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "WORKFLOWID"));
		int stageNum = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "STAGE"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		//assignedTask.setDealId(dealId);
		AssignedTask linkableTask = null;
		
		taskHistory = new WFDealTaskHistory(srk,dealId);
		
		deal.setDealId(dealId);
		// feed the input data and run the program.
		 srk.getExpressState().setDealInstitutionId(instProfId);
		 boolean wfab = workflowAssistBean.generateInitialTasks(deal, srk);
        // verify the running result.
		 assertFalse(wfab);
        
    }
    
    public void testGenerateTasks2() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testGenerateTasks2");
		int workflowId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "WORKFLOWID"));
		int stageNum = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "STAGE"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		//deal.setDealId(dealId);
		// feed the input data and run the program.
		 srk.getExpressState().setDealInstitutionId(instProfId);
		 boolean wfab = workflowAssistBean.generateInitialTasks(deal, srk);
        // verify the running result.
		 assertFalse(wfab);
        
    }
    
    public void testGenerateRegressionTasks() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testGenerateRegressionTasks");
		int stageNum = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "STAGE"));
		int backToStageNum = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "NUMBEROFSTAGES"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		// feed the input data and run the program.
		srk.getExpressState().setDealInstitutionId(instProfId);
       // DocumentQueue entity = new DocumentQueue(srk, documentQueueId);
		workflowAssistBean.generateRegressionTasks(deal, workflow, stageNum, backToStageNum, dealUserProfiles, srk, true, taskHistory);
        // verify the running result.
		assertNotNull(workflowAssistBean);
		
    }
    
    public void testGenerateTask() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testGenerateTask");
		int dealId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "DEALID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		deal.setDealId(dealId);
		
		//workflowTask.setSessionResourceKit(srk);
		//businessRule.setDealId(dealId);
		// feed the input data and run the program.
		srk.getExpressState().setDealInstitutionId(instProfId);
		deal.setDealId(dealId);
		// feed the input data and run the program.
		 srk.getExpressState().setDealInstitutionId(instProfId);
		 boolean wfab = workflowAssistBean.generateInitialTasks(deal, srk);
        // verify the running result.
		 assertFalse(wfab);
    }
    
    public void testDealStatusUpdateRequest() throws Exception {
    	
    	// get input data from xml repository
    	ITable expectedWorkflowAssistBean = dataSetTest.getTable("testDealStatusUpdateRequest");
		int dealId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "DEALID"));
		int userId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "USERID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedWorkflowAssistBean.getValue(0, "INSTITUTIONPROFILEID"));
		
		// feed the input data and run the program.
		srk.getExpressState().setDealInstitutionId(instProfId);
		
		workflowAssistBean.dealStatusUpdateRequest(deal, userId);
        // verify the running result.
        assertNotNull(workflowAssistBean);
    }
    
}