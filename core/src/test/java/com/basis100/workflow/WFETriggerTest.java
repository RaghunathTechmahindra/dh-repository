/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 13-May-09
 *
 */
package com.basis100.workflow;

import java.sql.ResultSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;
import com.filogix.externallinks.framework.ServiceConst;

public class WFETriggerTest extends ExpressEntityTestCase 
    implements UnitTestLogging
{
    private static final int DATA_COUNT_DEAL = 50;
    private int testSeqForDeal = 0;
    private static final int DATA_COUNT_USER = 50;
    private int testSeqForUser = 0;
    int     dealId;
    int     copyId;
    int     institutionProfileId;
    int     systemTypeId;
    int     userId;
    Deal    deal;
    UserProfile up;


    // The logger
    private static Log _log = LogFactory.getLog(WFETrigger.class);
    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env, prepare the test cases...
     */
    @Before
    public void setUp() throws Exception{
        ResourceManager.init();
        _srk = new SessionResourceKit();
        _log.info("Testing WFETrigger ...");
        // get random seqence number
        
        dealId  = _dataRepository.getInt("DEAL", "DealId", testSeqForDeal);
        copyId  = _dataRepository.getInt("DEAL", "copyId", testSeqForDeal);
        institutionProfileId  =_dataRepository.getInt("DEAL", "InstitutionProfileId", testSeqForDeal);
        systemTypeId = _dataRepository.getInt("DEAL", "SystemTypeId", testSeqForDeal);
        int instituionProfileIdU = -1;
        while (instituionProfileIdU != institutionProfileId) {
            userId  = _dataRepository.getInt("UserProfile", "USERPROFILEID", testSeqForUser);
            instituionProfileIdU = _dataRepository.getInt("UserProfile", "institutionProfileId", testSeqForUser);
        }
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);
        deal = new Deal(_srk, null);
        deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
        up = new UserProfile(_srk);
        up = up.findByPrimaryKey(new UserProfileBeanPK(userId, institutionProfileId));

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _log.info("====================================================================");
    }
    
    @Test
    public void workflowTriggerForDSUTest() throws Exception {
        
    // insertData(dealId, institutionProfileId, systemTypeId, 1, 5, userId);
        
     // int qId = selectQueueId(dealId, institutionProfileId, systemTypeId);

        WFETrigger.workflowTrigger(2, _srk, userId, dealId,
                copyId, up.getUserTypeId(), null);
        int qId = selectQueueId(dealId, institutionProfileId, systemTypeId);
        
        ESBOutboundQueue entry = new ESBOutboundQueue(_srk);
        entry = entry.findByPrimaryKey(new ESBOutboundQueuePK(qId));
        assertEquals(qId, entry.getESBOutboundQueueId());
        
//        deleteData(qId);
    }

    private void insertData(int dealId, int institutionProfileId, int systemTypeId, 
            int dealEventStatusTypeId, int statusNameReplacementId, int userProfileId) 
        throws Exception {
    	 _srk.beginTransaction();
        String sql = "INSERT INTO ESBOUTBOUNDQUEUE values (" +
            "ESBOUTBOUNDQUEUESEQ.nextval, " + institutionProfileId + ", " 
            + ServiceConst.SERVICE_TYPE_DEALEVENT_STATUS_UPDATE + ", " 
            + dealId + ", " + systemTypeId + ", " + userProfileId + ", 1, sysdate, " 
            + " null, null, sysdate)";
        
        _dataRepository.executeSQL(sql);
        _srk.beginTransaction();
        
    }
    private int selectQueueId(int dealId, int instituionId, int systemTypeId) throws Exception{
        
        String sql ="SELECT ESBOUTBOUNDQUEUEId from ESBOUTBOUNDQUEUE where dealId= " + dealId
        + " AND INSTITUTIONPROFILEID = " + instituionId + " AND SYSTEMTYPEID = " + systemTypeId;
        
        ResultSet rset =  _dataRepository.executeSQL(sql);
        int qId = 0;
        while ( rset.next() )
        {
          qId = rset.getInt("ESBOUTBOUNDQUEUEId");
          break;
        }
        rset.close();
        
        return qId;
    }
    
    
    private void deleteData(int qId) 
        throws Exception {
        _srk.beginTransaction();
        String sql = "delete from ESBOUTBOUNDQUEUE  where ESBOUTBOUNDQUEUEid = " + qId;
         
        _dataRepository.executeSQL(sql);
        _srk.rollbackTransaction();
    }

}
