/*
 * @(#)SourceFirm2UserRoutingTest.java    2005-5-26
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.Iterator;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.workflow.DealUserProfiles;

import com.basis100.TestingHelper;

/**
 * SourceFirm2UserRoutingTest - 
 *
 * @version   1.0 2005-5-26
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class SourceFirm2UserRoutingTest extends UnderwriterRoutingTestBase {

    // The logger
    private static Log _log = LogFactory.getLog(SourceFirm2UserRoutingTest.class);

    // routing class.
    private SourceFirm2UserRouting _routing = new SourceFirm2UserRouting();

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(SourceFirm2UserRoutingTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new SourceFirm2UserRoutingTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public SourceFirm2UserRoutingTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public SourceFirm2UserRoutingTest(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {

        super.setUp();
        _log.info("Testing SourceFirm2UserRouting ...");
    }

    /**
     */
    public void testFindUnderwriters() {
        
        _log.debug("Testing [performFirmUserMapping] ...");

        for (Iterator i = _testingDeals.iterator(); i.hasNext(); ) {

            Deal aDeal = (Deal) i.next();
            _log.info("------ Perform Source Firm to User Routing for Deal: ");
            _log.info(TestingHelper.dealString(aDeal));
            DealUserProfiles dup = new DealUserProfiles(aDeal, _srk);
            List underwriterIds = _routing.findUnderwriters(aDeal, dup, _srk);
            int size = (underwriterIds == null) ? 0 : underwriterIds.size();
            _log.info("Found [" + size + "] available underwriters.");
            if (size > 0) {
                for (Iterator u = underwriterIds.iterator(); u.hasNext(); ) {
                    int id = ((Integer) u.next()).intValue();
                    _log.info("  Underwriter User ID: " + id);
                }
            }
        }
    }
}
