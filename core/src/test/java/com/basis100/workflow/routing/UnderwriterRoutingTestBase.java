/*
 * @(#)UnderwriterRoutingTestBase.java    2005-5-25
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow.routing;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.entity.Deal;
import com.basis100.workflow.WorkflowAssistBean;

import com.basis100.TestingHelper;
import com.basis100.TestingEnvironmentInitializer;

/**
 * UnderwriterRoutingTestBase is the base class for underwriter routing unit
 * test.  It will set up the testing environment and prepare the test cases.
 *
 * @version   1.0 2005-5-25
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class UnderwriterRoutingTestBase extends TestCase {

    // The logger
    private static Log _log = LogFactory.getLog(UnderwriterRoutingTestBase.class);

    protected SessionResourceKit _srk;
    // the workflow assist Bean.
    protected WorkflowAssistBean _assistBean;
    // the deals for Testing.
    protected List _testingDeals = new ArrayList();

    /**
     * Constructor function
     */
    public UnderwriterRoutingTestBase() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public UnderwriterRoutingTestBase(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {
        // initializing the test environment.
        TestingEnvironmentInitializer.initTestingEnv();
        // initializing the session resource kit.
        _srk = new SessionResourceKit("Underwriter Routing Test ...");
        // prepare the workflow assist bean.
        _assistBean = new WorkflowAssistBean();
        try {
            // we don't need CalcMonitor for testing.
            // prepare the deals for testing.
            _log.debug("Preparing the deals for testing ...");

            // for connectionpool = MOSXDQA  -- Xceed QA Database.
            //_testingDeals.add(new Deal(_srk, null, 82978, 70));
            //_testingDeals.add(new Deal(_srk, null, 81607, 1));
            //_testingDeals.add(new Deal(_srk, null, 80097, 7));
            //_testingDeals.add(new Deal(_srk, null, 80099, 15));
            //_testingDeals.add(new Deal(_srk, null, 80162, 1));

            // for connection = MOSLOCAL -- Local Database.
            //_testingDeals.add(new Deal(_srk, null, 212, 1));
            //_testingDeals.add(new Deal(_srk, null, 419, 1));
            //_testingDeals.add(new Deal(_srk, null, 167, 1));
            //// the following deals have rental as primary property.
            //_testingDeals.add(new Deal(_srk, null, 930, 1));
            //_testingDeals.add(new Deal(_srk, null, 931, 1));
            //// the following deals have SOB status id = 4.
            //_testingDeals.add(new Deal(_srk, null, 147, 1));

            // for connectionpool = MOSQADJ  -- DJ QA Database.
            //_testingDeals.add(new Deal(_srk, null, 1937, 1));
            //_testingDeals.add(new Deal(_srk, null, 1991, 1));
            //_testingDeals.add(new Deal(_srk, null, 1992, 1));
            //_testingDeals.add(new Deal(_srk, null, 1993, 1));
            //_testingDeals.add(new Deal(_srk, null, 1994, 1));
            //_testingDeals.add(new Deal(_srk, null, 1995, 1));
            //_testingDeals.add(new Deal(_srk, null, 1996, 1));
            //_testingDeals.add(new Deal(_srk, null, 1997, 1));

            // for BMO Q9
            _testingDeals.add(new Deal(_srk, null, 16609, 1));
            _testingDeals.add(new Deal(_srk, null, 31884, 1));
            _testingDeals.add(new Deal(_srk, null, 40102, 1));
        } catch (RemoteException re) {
            _log.error("Remote Exception: ", re);
        } catch (FinderException fe) {
            _log.error("Finder Exception: ", fe);
        }

        _log.info("==========================================================");
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
        _log.info("====================================================================");
    }
    
    public void testTBR(){
        System.out.println("test");
}

}
