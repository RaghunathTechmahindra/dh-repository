package com.basis100.workflow;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.UserProfile;
import com.basis100.entity.RemoteException;

public class UserProfileQueryTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private UserProfileQuery userProfileQuery;

	public UserProfileQueryTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				UserProfileQuery.class.getSimpleName() + "DataSetTest.xml"));
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.INSERT;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.userProfileQuery = new UserProfileQuery();
	}

	public void testGetUserProfile() throws Exception {
		ITable testGetUserProfile = null;
		testGetUserProfile = dataSetTest.getTable("testGetUserProfile");
		int rowCount = testGetUserProfile.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) testGetUserProfile.getValue(i, "userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) testGetUserProfile.getValue(i,
					"instituionId");
			int instituionid = Integer.parseInt(instituionId);
			String userTypeCheck = (String) testGetUserProfile.getValue(i,
					"userTypeCheck");
			int usertypecheck = Integer.parseInt(userTypeCheck);
			userProfile = userProfileQuery.getUserProfile(userid, instituionid,
					true, true, usertypecheck, srk);
			userProfileVector.add(userProfile);
			returnedUsers.add(userProfile.getUserProfileId());
			assert true == returnedUsers.containsAll(userProfileVector);
		}
	}

	public void testGetUserProfileFailure() throws Exception {
		ITable testGetUserProfileFailure = null;
		testGetUserProfileFailure = dataSetTest
				.getTable("testGetUserProfileFailure");
		int rowCount = testGetUserProfileFailure.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) testGetUserProfileFailure.getValue(i,
					"userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) testGetUserProfileFailure.getValue(
					i, "instituionId");
			int instituionid = Integer.parseInt(instituionId);
			String userTypeCheck = (String) testGetUserProfileFailure.getValue(
					i, "userTypeCheck");
			int usertypecheck = Integer.parseInt(userTypeCheck);
			userProfile = userProfileQuery.getUserProfile(userid, instituionid,
					true, true, usertypecheck, srk);
			userProfileVector.add(userProfile);
			returnedUsers.add(userProfile.getUserProfileId());
			assert false == returnedUsers.containsAll(userProfileVector);
		}
	}

	public void testGet2ndApproverUserProfile() throws Exception {
		ITable get2ndApproverUserProfile = null;
		get2ndApproverUserProfile = dataSetTest
				.getTable("testGet2ndApproverUserProfile");
		int rowCount = get2ndApproverUserProfile.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) get2ndApproverUserProfile.getValue(i,
					"userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) get2ndApproverUserProfile.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			userProfile = userProfileQuery.get2ndApproverUserProfile(userid,
					instituionid, true, true, srk);
			userProfileVector.add(userProfile);
			returnedUsers.add(userProfile.getUserProfileId());
			assert true == returnedUsers.containsAll(userProfileVector);
		}

	}

	public void testGet2ndApproverUserProfileFailure() throws Exception {
		ITable get2ndApproverUserProfileFailure = null;
		get2ndApproverUserProfileFailure = dataSetTest
				.getTable("testGet2ndApproverUserProfileFailure");
		int rowCount = get2ndApproverUserProfileFailure.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) get2ndApproverUserProfileFailure.getValue(
					i, "userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) get2ndApproverUserProfileFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			userProfile = userProfileQuery.get2ndApproverUserProfile(userid,
					instituionid, true, true, srk);
			userProfileVector.add(userProfile);
			returnedUsers.add(userProfile.getUserProfileId());
			assert false == returnedUsers.containsAll(userProfileVector);
		}

	}

	public void testGetLast2ndApproverUserProfile() throws Exception {

		ITable testGetLast2ndApproverUserProfile = null;
		testGetLast2ndApproverUserProfile = dataSetTest
				.getTable("testGetLast2ndApproverUserProfile");
		int rowCount = testGetLast2ndApproverUserProfile.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) testGetLast2ndApproverUserProfile
					.getValue(i, "userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) testGetLast2ndApproverUserProfile
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			userProfile = userProfileQuery.getLast2ndApproverUserProfile(
					userid, instituionid, srk);
			if (userProfile == null) {
				userProfile = new UserProfile(srk);
				userProfile.setUserProfileId(4762);
				returnedUsers.add(userProfile.getUserProfileId());
			} else {
				userProfileVector.add(userProfile);
				returnedUsers.add(userProfile.getUserProfileId());
			}
			assert true == returnedUsers.containsAll(userProfileVector);
		}
	}

	public void testGetLast2ndApproverUserProfileFailure() throws Exception {

		ITable testGetLast2ndApproverUserProfileFailure = null;
		testGetLast2ndApproverUserProfileFailure = dataSetTest
				.getTable("testGetLast2ndApproverUserProfileFailure");
		int rowCount = testGetLast2ndApproverUserProfileFailure.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) testGetLast2ndApproverUserProfileFailure
					.getValue(i, "userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) testGetLast2ndApproverUserProfileFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			userProfile = userProfileQuery.getLast2ndApproverUserProfile(
					userid, instituionid, srk);
			if (userProfile == null) {
				userProfile = new UserProfile(srk);
				userProfile.setUserProfileId(4762);
				returnedUsers.add(userProfile.getUserProfileId());
			} else {
				userProfileVector.add(userProfile);
				returnedUsers.add(userProfile.getUserProfileId());
			}
			assert false == returnedUsers.containsAll(userProfileVector);
		}
	}

	public void testGetJointApproverUserProfile() throws Exception {

		ITable testGetJointApproverUserProfile = null;
		testGetJointApproverUserProfile = dataSetTest
				.getTable("testGetJointApproverUserProfile");
		int rowCount = testGetJointApproverUserProfile.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) testGetJointApproverUserProfile.getValue(
					i, "userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) testGetJointApproverUserProfile
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			userProfile = userProfileQuery.getJointApproverUserProfile(userid,
					instituionid, true, true, srk);
			userProfileVector.add(userProfile);
			returnedUsers.add(userProfile.getUserProfileId());
			assert true == returnedUsers.containsAll(userProfileVector);
		}

	}

	public void testGetJointApproverUserProfileFailure() throws Exception {

		ITable testGetJointApproverUserProfileFailure = null;
		testGetJointApproverUserProfileFailure = dataSetTest
				.getTable("testGetJointApproverUserProfileFailure");
		int rowCount = testGetJointApproverUserProfileFailure.getRowCount();
		UserProfile userProfile = null;
		Vector userProfileVector = new Vector();
		Vector returnedUsers = new Vector();
		for (int i = 0; i < rowCount; i++) {
			String userId = (String) testGetJointApproverUserProfileFailure
					.getValue(i, "userId");
			int userid = Integer.parseInt(userId);
			String instituionId = (String) testGetJointApproverUserProfileFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			userProfile = userProfileQuery.getJointApproverUserProfile(userid,
					instituionid, true, true, srk);
			userProfileVector.add(userProfile);
			returnedUsers.add(userProfile.getUserProfileId());
			assert false == returnedUsers.containsAll(userProfileVector);
		}

	}
  
	public void testGetGroupUsersId() throws Exception {

		ITable testGetGroupUsersId = null;
		testGetGroupUsersId = dataSetTest.getTable("testGetGroupUsersId");
		int rowCount = testGetGroupUsersId.getRowCount();
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupid = (String) testGetGroupUsersId
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(groupid);
			String usertypecheck = (String) testGetGroupUsersId.getValue(i,
					"userType");
			int userType = Integer.parseInt(usertypecheck);
			String instituionId = (String) testGetGroupUsersId.getValue(i,
					"instituionId");
			int institutionId = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupUsersId(groupId, userType,
					true, srk, institutionId);
			assertNotNull(returned);
			assert returned.size()!=0;
			assertNotNull(userProfileQuery);
		}
	}

	public void testGetGroupUsersIdFailure() throws Exception {

		ITable testGetGroupUsersIdFailure = null;
		testGetGroupUsersIdFailure = dataSetTest
				.getTable("testGetGroupUsersIdFailure");
		int rowCount = testGetGroupUsersIdFailure.getRowCount();
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupid = (String) testGetGroupUsersIdFailure.getValue(i,
					"groupId");
			int groupId = Integer.parseInt(groupid);
			String usertypecheck = (String) testGetGroupUsersIdFailure
					.getValue(i, "userType");
			int userType = Integer.parseInt(usertypecheck);
			String instituionId = (String) testGetGroupUsersIdFailure.getValue(
					i, "instituionId");
			int institutionId = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupUsersId(groupId, userType,
					true, srk, institutionId);			
			assertNull(returned);
		}
	}
	
	public void testGetGroupUsers() throws Exception {

		ITable testGetGroupUsers = null;
		testGetGroupUsers = dataSetTest.getTable("testGetGroupUsers");
		int rowCount = testGetGroupUsers.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupUsers.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetGroupUsers
					.getValue(i, "userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetGroupUsers.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupUsers(groupid, usertype, true,
					srk, instituionid);
			assertNotNull(returned);
			assert returned.size()!=0;
			assertNotNull(userProfileQuery);
		}
	}

	public void testGetGroupUsersFailure() throws Exception {

		ITable testGetGroupUsersFailure = null;
		testGetGroupUsersFailure = dataSetTest
				.getTable("testGetGroupUsersFailure");
		int rowCount = testGetGroupUsersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupUsersFailure.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetGroupUsersFailure.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetGroupUsersFailure.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupUsers(groupid, usertype, true,
					srk, instituionid);
			assertNull(returned);
			assert returned.size()!=0;
			assertNotNull(userProfileQuery);
		}
	}
	
	
	public void testGetBranchUsersByGroup() throws Exception {

		ITable testGetBranchUsersByGroup = null;
		testGetBranchUsersByGroup = dataSetTest
				.getTable("testGetBranchUsersByGroup");
		int rowCount = testGetBranchUsersByGroup.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchUsersByGroup.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetBranchUsersByGroup.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetBranchUsersByGroup.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUsersByGroup(groupid,
					usertype, true, srk, instituionid);
			assertNotNull(returned);
		}

	}

	public void testGetBranchUsersByGroupFailure() throws Exception {

		ITable testGetBranchUsersByGroupFailure = null;
		testGetBranchUsersByGroupFailure = dataSetTest
				.getTable("testGetBranchUsersByGroupFailure");
		int rowCount = testGetBranchUsersByGroupFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchUsersByGroupFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetBranchUsersByGroupFailure
					.getValue(i, "userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetBranchUsersByGroupFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUsersByGroup(groupid,
					usertype, true, srk, instituionid);
			assertNull(returned);
		}
	}

	public void testGetBranchUsersByGroupId() throws Exception {

		ITable testGetBranchUsersByGroupId = null;
		testGetBranchUsersByGroupId = dataSetTest
				.getTable("testGetBranchUsersByGroupId");
		int rowCount = testGetBranchUsersByGroupId.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchUsersByGroupId.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetBranchUsersByGroupId.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetBranchUsersByGroupId
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUsersByGroupId(groupid,
					usertype, true, srk, instituionid);
			assertNotNull(returned);

		}

	}

	public void testGetBranchUsersByGroupIdFailure() throws Exception {

		ITable testGetBranchUsersByGroupIdFailure = null;
		testGetBranchUsersByGroupIdFailure = dataSetTest
				.getTable("testGetBranchUsersByGroupIdFailure");
		int rowCount = testGetBranchUsersByGroupIdFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchUsersByGroupIdFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetBranchUsersByGroupIdFailure
					.getValue(i, "userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetBranchUsersByGroupIdFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUsersByGroupId(groupid,
					usertype, true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetBranchUsers() throws Exception {

		ITable testGetBranchUsers = null;
		testGetBranchUsers = dataSetTest.getTable("testGetBranchUsers");
		int rowCount = testGetBranchUsers.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String branchId = (String) testGetBranchUsers.getValue(i,
					"branchId");
			int branchid = Integer.parseInt(branchId);
			String userType = (String) testGetBranchUsers.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetBranchUsers.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUsers(branchid, usertype,
					true, srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetBranchUsersFailure() throws Exception {

		ITable testGetBranchUsersFailure = null;
		testGetBranchUsersFailure = dataSetTest
				.getTable("testGetBranchUsersFailure");
		int rowCount = testGetBranchUsersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String branchId = (String) testGetBranchUsersFailure.getValue(i,
					"branchId");
			int branchid = Integer.parseInt(branchId);
			String userType = (String) testGetBranchUsersFailure.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetBranchUsersFailure.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUsers(branchid, usertype,
					true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetRegionUsersByGroup() throws Exception {

		ITable testGetRegionUsersByGroup = null;
		testGetRegionUsersByGroup = dataSetTest
				.getTable("testGetRegionUsersByGroup");
		int rowCount = testGetRegionUsersByGroup.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetRegionUsersByGroup.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetRegionUsersByGroup.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetRegionUsersByGroup.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionUsersByGroup(groupid,
					usertype, true, srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetRegionUsersByGroupFailure() throws Exception {

		ITable testGetRegionUsersByGroupFailure = null;
		testGetRegionUsersByGroupFailure = dataSetTest
				.getTable("testGetRegionUsersByGroupFailure");
		int rowCount = testGetRegionUsersByGroupFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetRegionUsersByGroupFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String userType = (String) testGetRegionUsersByGroupFailure
					.getValue(i, "userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetRegionUsersByGroupFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionUsersByGroup(groupid,
					usertype, true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetRegionUsers() throws Exception {

		ITable testGetRegionUsers = null;
		testGetRegionUsers = dataSetTest.getTable("testGetRegionUsers");
		int rowCount = testGetRegionUsers.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String regionId = (String) testGetRegionUsers.getValue(i,
					"regionId");
			int regionid = Integer.parseInt(regionId);
			String userType = (String) testGetRegionUsers.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetRegionUsers.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionUsers(regionid, usertype,
					true, srk, instituionid);
			assertNotNull(returned);
		}
	}

	public void testGetRegionUsersFailure() throws Exception {

		ITable testGetRegionUsersFailure = null;
		testGetRegionUsersFailure = dataSetTest
				.getTable("testGetRegionUsersFailure");
		int rowCount = testGetRegionUsersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String regionId = (String) testGetRegionUsersFailure.getValue(i,
					"regionId");
			int regionid = Integer.parseInt(regionId);
			String userType = (String) testGetRegionUsersFailure.getValue(i,
					"userType");
			int usertype = Integer.parseInt(userType);
			String instituionId = (String) testGetRegionUsersFailure.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionUsers(regionid, usertype,
					true, srk, instituionid);
			assertNull(returned);
		}
	}

	public void testGetAdministrators() throws Exception {

		ITable testGetAdministrators = null;
		testGetAdministrators = dataSetTest.getTable("testGetAdministrators");
		int rowCount = testGetAdministrators.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetAdministrators.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetAdministrators.getValue(i,
					"adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetAdministrators.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getAdministrators(groupid, admintype,
					true, srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetAdministratorsFailure() throws Exception {

		ITable testGetAdministratorsFailure = null;
		testGetAdministratorsFailure = dataSetTest
				.getTable("testGetAdministratorsFailure");
		int rowCount = testGetAdministratorsFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetAdministratorsFailure.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetAdministratorsFailure.getValue(
					i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetAdministratorsFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getAdministrators(groupid, admintype,
					true, srk, instituionid);
			assert returned.size()!=0;
			assertNotNull(userProfileQuery);

		}
	}

	public void testGetFunders() throws Exception {

		ITable testGetFunders = null;
		testGetFunders = dataSetTest.getTable("testGetFunders");
		int rowCount = testGetFunders.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetFunders.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String instituionId = (String) testGetFunders.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getFunders(groupid, true, srk,
					instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetFundersFailure() throws Exception {

		ITable testGetFundersFailure = null;
		testGetFundersFailure = dataSetTest.getTable("testGetFundersFailure");
		int rowCount = testGetFundersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetFundersFailure.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String instituionId = (String) testGetFundersFailure.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getFunders(groupid, true, srk,
					instituionid);
			assertNull(returned);

		}
	}

	public void testGetGroupAdministrators() throws Exception {

		ITable testGetGroupAdministrators = null;
		testGetGroupAdministrators = dataSetTest
				.getTable("testGetGroupAdministrators");
		int rowCount = testGetGroupAdministrators.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupAdministrators.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetGroupAdministrators.getValue(i,
					"adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetGroupAdministrators.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupAdministrators(groupid,
					admintype, true, srk, instituionid);
			assertNotNull(userProfileQuery);

		}
	}  
	public void testGetGroupAdministratorsFailure() throws Exception {

		ITable testGetGroupAdministratorsFailure = null;
		testGetGroupAdministratorsFailure = dataSetTest
				.getTable("testGetGroupAdministratorsFailure");
		int rowCount = testGetGroupAdministratorsFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupAdministratorsFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetGroupAdministratorsFailure
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetGroupAdministratorsFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupAdministrators(groupid,
					admintype, true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetGroupFunders() throws Exception {

		ITable testGetGroupFunders = null;
		testGetGroupFunders = dataSetTest.getTable("testGetGroupFunders");
		int rowCount = testGetGroupFunders.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupFunders
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String instituionId = (String) testGetGroupFunders.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupFunders(groupid, true, srk,
					instituionid);
			assertNotNull(returned);
			//assert returned.size() != 0;

		}
	}

	public void testGetGroupFundersFailure() throws Exception {

		ITable testGetGroupFundersFailure = null;
		testGetGroupFundersFailure = dataSetTest
				.getTable("testGetGroupFundersFailure");
		int rowCount = testGetGroupFundersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupFundersFailure.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String instituionId = (String) testGetGroupFundersFailure.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupFunders(groupid, true, srk,
					instituionid);
			assertNull(returned);

		}
	}

	public void testGetBranchAdministratorsByGroup() throws Exception {

		ITable testGetBranchAdministratorsByGroup = null;
		testGetBranchAdministratorsByGroup = dataSetTest
				.getTable("testGetBranchAdministratorsByGroup");
		int rowCount = testGetBranchAdministratorsByGroup.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchAdministratorsByGroup
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetBranchAdministratorsByGroup
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetBranchAdministratorsByGroup
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchAdministratorsByGroup(groupid,
					instituionid, admintype, true, srk);
			assertNotNull(returned);

		}
	}

	public void testGetBranchAdministratorsByGroupFailure() throws Exception {

		ITable testGetBranchAdministratorsByGroupFailure = null;
		testGetBranchAdministratorsByGroupFailure = dataSetTest
				.getTable("testGetBranchAdministratorsByGroupFailure");
		int rowCount = testGetBranchAdministratorsByGroupFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchAdministratorsByGroupFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetBranchAdministratorsByGroupFailure
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetBranchAdministratorsByGroupFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchAdministratorsByGroup(groupid,
					instituionid, admintype, true, srk);
			assertNull(returned);

		}
	}
	

	public void testGetBranchAdministrators() throws Exception {

		ITable testGetBranchAdministrators = null;
		testGetBranchAdministrators = dataSetTest
				.getTable("testGetBranchAdministrators");
		int rowCount = testGetBranchAdministrators.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String branchId = (String) testGetBranchAdministrators.getValue(i,
					"branchId");
			int branchid = Integer.parseInt(branchId);
			String adminType = (String) testGetBranchAdministrators.getValue(i,
					"adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetBranchAdministrators
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchAdministrators(branchid,
					admintype, true, srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetBranchAdministratorsFailure() throws Exception {

		ITable testGetBranchAdministratorsFailure = null;
		testGetBranchAdministratorsFailure = dataSetTest
				.getTable("testGetBranchAdministratorsFailure");
		int rowCount = testGetBranchAdministratorsFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		try{
		for (int i = 0; i < rowCount; i++) {
			String branchId = (String) testGetBranchAdministratorsFailure
					.getValue(i, "branchId");
			int branchid = Integer.parseInt(branchId);
			String adminType = (String) testGetBranchAdministratorsFailure
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetBranchAdministratorsFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchAdministrators(branchid,
					admintype, true, srk, instituionid);
			assert returned.size()!=0;
			//assertNull(returned);	
		}	
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void testGetBranchFundersByGroup() throws Exception {
		ITable testGetBranchFundersByGroup = null;
		testGetBranchFundersByGroup = dataSetTest
				.getTable("testGetBranchFundersByGroup");
		int rowCount = testGetBranchFundersByGroup.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchFundersByGroup.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String instituionId = (String) testGetBranchFundersByGroup
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchFundersByGroup(groupid, true,
					srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetBranchFundersByGroupFailure() throws Exception {

		ITable testGetBranchFundersByGroupFailure = null;
		testGetBranchFundersByGroupFailure = dataSetTest
				.getTable("testGetBranchFundersByGroupFailure");
		int rowCount = testGetBranchFundersByGroupFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetBranchFundersByGroupFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String instituionId = (String) testGetBranchFundersByGroupFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchFundersByGroup(groupid, true,
					srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetBranchFunders() throws Exception {

		ITable testGetBranchFunders = null;
		testGetBranchFunders = dataSetTest.getTable("testGetBranchFunders");
		int rowCount = testGetBranchFunders.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String branchId = (String) testGetBranchFunders.getValue(i,
					"branchId");
			int branchid = Integer.parseInt(branchId);
			String instituionId = (String) testGetBranchFunders.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchFunders(branchid, true, srk,
					instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetBranchFundersFailure() throws Exception {

		ITable testGetBranchFundersFailure = null;
		testGetBranchFundersFailure = dataSetTest
				.getTable("testGetBranchFundersFailure");
		int rowCount = testGetBranchFundersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String branchId = (String) testGetBranchFundersFailure.getValue(i,
					"branchId");
			int branchid = Integer.parseInt(branchId);
			String instituionId = (String) testGetBranchFundersFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchFunders(branchid, true, srk,
					instituionid);
			assertNull(returned);
		}
	}

	public void testGetRegionAdministratorsByGroup() throws Exception {

		ITable testGetRegionAdministratorsByGroup = null;
		testGetRegionAdministratorsByGroup = dataSetTest
				.getTable("testGetRegionAdministratorsByGroup");
		int rowCount = testGetRegionAdministratorsByGroup.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetRegionAdministratorsByGroup
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetRegionAdministratorsByGroup
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionAdministratorsByGroup
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionAdministratorsByGroup(groupid,
					admintype, true, srk, instituionid);
			assertNotNull(returned);

		}
	}
	
	public void testGetRegionAdministratorsByGroupFailure() throws Exception {

		ITable testGetRegionAdministratorsByGroupFailure = null;
		testGetRegionAdministratorsByGroupFailure = dataSetTest
				.getTable("testGetRegionAdministratorsByGroupFailure");
		int rowCount = testGetRegionAdministratorsByGroupFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetRegionAdministratorsByGroupFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetRegionAdministratorsByGroupFailure
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionAdministratorsByGroupFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionAdministratorsByGroup(groupid,
					admintype, true, srk, instituionid);
			assertNull(returned);
		}
	}
	
	public void testGetRegionAdministrators() throws Exception {
		ITable testGetRegionAdministrators = null;
		testGetRegionAdministrators = dataSetTest
				.getTable("testGetRegionAdministrators");
		int rowCount = testGetRegionAdministrators.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String regionId = (String) testGetRegionAdministrators.getValue(i,
					"regionId");
			int regionid = Integer.parseInt(regionId);
			String adminType = (String) testGetRegionAdministrators.getValue(i,
					"adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionAdministrators
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionAdministrators(regionid,
					admintype, true, srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetRegionAdministratorsFailure() throws Exception {
		ITable testGetRegionAdministratorsFailure = null;
		testGetRegionAdministratorsFailure = dataSetTest
				.getTable("testGetRegionAdministratorsFailure");
		int rowCount = testGetRegionAdministratorsFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String regionId = (String) testGetRegionAdministratorsFailure
					.getValue(i, "regionId");
			int regionid = Integer.parseInt(regionId);
			String adminType = (String) testGetRegionAdministratorsFailure
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionAdministratorsFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionAdministrators(regionid,
					admintype, true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetRegionFundersByGroup() throws Exception {

		ITable testGetRegionFundersByGroup = null;
		testGetRegionFundersByGroup = dataSetTest
				.getTable("testGetRegionFundersByGroup");
		int rowCount = testGetRegionFundersByGroup.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetRegionFundersByGroup.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetRegionFundersByGroup.getValue(i,
					"adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionFundersByGroup
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionFundersByGroup(groupid,
					admintype, true, srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetRegionFundersByGroupFailure() throws Exception {

		ITable testGetRegionFundersByGroupFailure = null;
		testGetRegionFundersByGroupFailure = dataSetTest
				.getTable("testGetRegionFundersByGroupFailure");
		int rowCount = testGetRegionFundersByGroupFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetRegionFundersByGroupFailure
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String adminType = (String) testGetRegionFundersByGroupFailure
					.getValue(i, "adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionFundersByGroupFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionFundersByGroup(groupid,
					admintype, true, srk, instituionid);
			assertNull(returned);
		}
	}

	public void testGetRegionFunders() throws Exception {

		ITable testGetRegionFunders = null;
		testGetRegionFunders = dataSetTest.getTable("testGetRegionFunders");
		int rowCount = testGetRegionFunders.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String regionId = (String) testGetRegionFunders.getValue(i,
					"regionId");
			int regionid = Integer.parseInt(regionId);
			String adminType = (String) testGetRegionFunders.getValue(i,
					"adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionFunders.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionFunders(regionid, admintype,
					true, srk, instituionid);
			assertNotNull(returned);
		}
	}

	public void testGetRegionFundersFailure() throws Exception {

		ITable testGetRegionFundersFailure = null;
		testGetRegionFundersFailure = dataSetTest
				.getTable("testGetRegionFundersFailure");
		int rowCount = testGetRegionFundersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String regionId = (String) testGetRegionFundersFailure.getValue(i,
					"regionId");
			int regionid = Integer.parseInt(regionId);
			String adminType = (String) testGetRegionFundersFailure.getValue(i,
					"adminType");
			int admintype = Integer.parseInt(adminType);
			String instituionId = (String) testGetRegionFundersFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getRegionFunders(regionid, admintype,
					true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetUnderwriters() throws Exception {

		ITable testGetUnderwriters = null;
		testGetUnderwriters = dataSetTest.getTable("testGetUnderwriters");
		int rowCount = testGetUnderwriters.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetUnderwriters
					.getValue(i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String underwriterType = (String) testGetUnderwriters.getValue(i,
					"underwriterType");
			int underwritertype = Integer.parseInt(underwriterType);
			String instituionId = (String) testGetUnderwriters.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getUnderwriters(groupid,
					underwritertype, true, srk, instituionid);
			assertNotNull(returned);

		}
	}

	public void testGetUnderwritersFailure() throws Exception {

		ITable testGetUnderwritersFailure = null;
		testGetUnderwritersFailure = dataSetTest
				.getTable("testGetUnderwritersFailure");
		int rowCount = testGetUnderwritersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetUnderwritersFailure.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String underwriterType = (String) testGetUnderwritersFailure
					.getValue(i, "underwriterType");
			int underwritertype = Integer.parseInt(underwriterType);
			String instituionId = (String) testGetUnderwritersFailure.getValue(
					i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getUnderwriters(groupid,
					underwritertype, true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetGroupUnderwriters() throws Exception {

		ITable testGetGroupUnderwriters = null;
		testGetGroupUnderwriters = dataSetTest
				.getTable("testGetGroupUnderwriters");
		int rowCount = testGetGroupUnderwriters.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupUnderwriters.getValue(i,
					"groupId");
			int groupid = Integer.parseInt(groupId);
			String underwriterType = (String) testGetGroupUnderwriters
					.getValue(i, "underwriterType");
			int underwritertype = Integer.parseInt(underwriterType);
			String instituionId = (String) testGetGroupUnderwriters.getValue(i,
					"institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupUnderwriters(groupid,
					underwritertype, true, srk, instituionid);
			assert returned.size() != 0;
		}
	}

	public void testGetGroupUnderwritersFailure() throws Exception {

		ITable testGetGroupUnderwritersFailure = null;
		testGetGroupUnderwritersFailure = dataSetTest
				.getTable("testGetGroupUnderwritersFailure");
		int rowCount = testGetGroupUnderwritersFailure.getRowCount();
		UserProfile userProfile = null;
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupId = (String) testGetGroupUnderwritersFailure.getValue(
					i, "groupId");
			int groupid = Integer.parseInt(groupId);
			String underwriterType = (String) testGetGroupUnderwritersFailure
					.getValue(i, "underwriterType");
			int underwritertype = Integer.parseInt(underwriterType);
			String instituionId = (String) testGetGroupUnderwritersFailure
					.getValue(i, "institutionId");
			int instituionid = Integer.parseInt(instituionId);
			returned = userProfileQuery.getGroupUnderwriters(groupid,
					underwritertype, true, srk, instituionid);
			assertNull(returned);

		}
	}

	public void testGetBranchUnderwritersByGroup() throws Exception {

		ITable testGetBranchUnderwritersByGroup = null;
		testGetBranchUnderwritersByGroup = dataSetTest
				.getTable("testGetBranchUnderwritersByGroup");
		int rowCount = testGetBranchUnderwritersByGroup.getRowCount();
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupid = (String) testGetBranchUnderwritersByGroup
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(groupid);
			String underwriterType = (String) testGetBranchUnderwritersByGroup
					.getValue(i, "underwriterType");
			int underWriterType = Integer.parseInt(underwriterType);
			String instituionId = (String) testGetBranchUnderwritersByGroup
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUnderwritersByGroup(groupId,
					underWriterType, true, srk, institutionId);
			assertNotNull(returned);

		}
	}

	public void testGetBranchUnderwritersByGroupFailure() throws Exception {

		ITable testGetBranchUnderwritersByGroupFailure = null;
		testGetBranchUnderwritersByGroupFailure = dataSetTest
				.getTable("testGetBranchUnderwritersByGroupFailure");
		int rowCount = testGetBranchUnderwritersByGroupFailure.getRowCount();
		Vector<UserProfile> returned = null;
		Vector returnedUsers = null;
		for (int i = 0; i < rowCount; i++) {
			String groupid = (String) testGetBranchUnderwritersByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(groupid);
			String underwriterType = (String) testGetBranchUnderwritersByGroupFailure
					.getValue(i, "underwriterType");
			int underWriterType = Integer.parseInt(underwriterType);
			String instituionId = (String) testGetBranchUnderwritersByGroupFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(instituionId);
			returned = userProfileQuery.getBranchUnderwritersByGroup(groupId,
					underWriterType, true, srk, institutionId);
			assertNull(returned);

		}
	}

	// -------------------------------------------------HARI
	// Files---------------------------------------------------------------------------------
	@Test
	public void testGetBranchUnderwriters() throws DataSetException {
		ITable testGetBranchUnderwriters = dataSetTest
				.getTable("testGetBranchUnderwriters");
		int rowCount = testGetBranchUnderwriters.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String branch = (String) testGetBranchUnderwriters.getValue(i,
					"branchId");
			int branchId = Integer.parseInt(branch);
			String underwriter = (String) testGetBranchUnderwriters.getValue(i,
					"underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetBranchUnderwriters.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getBranchUnderwriters(branchId, underwriterType, true,
							srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetBranchUnderwritersFailure() throws DataSetException {

		ITable testGetBranchUnderwritersFailure = dataSetTest
				.getTable("testGetBranchUnderwritersFailure");
		int rowCount = testGetBranchUnderwritersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String branch = (String) testGetBranchUnderwritersFailure.getValue(
					i, "branchId");
			int branchId = Integer.parseInt(branch);
			String underwriter = (String) testGetBranchUnderwritersFailure
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetBranchUnderwritersFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getBranchUnderwriters(branchId, underwriterType, true,
							srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetRegionUnderwritersByGroup() throws DataSetException {
		ITable testGetRegionUnderwritersByGroup = dataSetTest
				.getTable("testGetRegionUnderwritersByGroup");
		int rowCount = testGetRegionUnderwritersByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String group = (String) testGetRegionUnderwritersByGroup.getValue(
					i, "groupId");
			int groupId = Integer.parseInt(group);
			String underwriter = (String) testGetRegionUnderwritersByGroup
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetRegionUnderwritersByGroup
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getRegionUnderwritersByGroup(groupId, underwriterType,
							true, srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetRegionUnderwritersByGroupFailure()
			throws DataSetException {
		ITable testGetRegionUnderwritersByGroupFailure = dataSetTest
				.getTable("testGetRegionUnderwritersByGroupFailure");
		int rowCount = testGetRegionUnderwritersByGroupFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String group = (String) testGetRegionUnderwritersByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String underwriter = (String) testGetRegionUnderwritersByGroupFailure
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetRegionUnderwritersByGroupFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getRegionUnderwritersByGroup(groupId, underwriterType,
							true, srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetRegionUnderwriters() throws DataSetException {
		ITable testGetRegionUnderwriters = dataSetTest
				.getTable("testGetRegionUnderwriters");
		int rowCount = testGetRegionUnderwriters.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String region = (String) testGetRegionUnderwriters.getValue(i,
					"regionId");
			int regionId = Integer.parseInt(region);
			String underwriter = (String) testGetRegionUnderwriters.getValue(i,
					"underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetRegionUnderwriters.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getRegionUnderwriters(regionId, underwriterType, true,
							srk, institutionId);
			assertNotNull(returned);

		}
	}

	@Test
	public void testGetRegionUnderwritersFailure() throws DataSetException {
		ITable testGetRegionUnderwritersFailure = dataSetTest
				.getTable("testGetRegionUnderwritersFailure");
		int rowCount = testGetRegionUnderwritersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String region = (String) testGetRegionUnderwritersFailure.getValue(
					i, "regionId");
			int regionId = Integer.parseInt(region);
			String underwriter = (String) testGetRegionUnderwritersFailure
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetRegionUnderwritersFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getRegionUnderwriters(regionId, underwriterType, true,
							srk, institutionId);
			assertNull(returned);

		}
	}

	@Test
	public void testGetUsers() throws DataSetException {
		ITable testGetUsers = dataSetTest.getTable("testGetUsers");
		int rowCount = testGetUsers.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetUsers.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String user = (String) testGetUsers.getValue(i, "userType");
			int userType = Integer.parseInt(user);
			String institution = (String) testGetUsers.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getUsers(groupId,
					userType, true, srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetUsersFailure() throws DataSetException {
		ITable testGetUsersFailure = dataSetTest
				.getTable("testGetUsersFailure");
		int rowCount = testGetUsersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String group = (String) testGetUsersFailure.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String user = (String) testGetUsersFailure.getValue(i, "userType");
			int userType = Integer.parseInt(user);
			String institution = (String) testGetUsersFailure.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getUsers(groupId,
					userType, true, srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionAdministratorsByGroup()
			throws DataSetException {
		ITable testGetInstitutionAdministratorsByGroup = dataSetTest
				.getTable("testGetInstitutionAdministratorsByGroup");
		int rowCount = testGetInstitutionAdministratorsByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionAdministratorsByGroup
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetInstitutionAdministratorsByGroup
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionAdministratorsByGroup(groupId, adminType,
							true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionAdministratorsByGroupFailure()
			throws DataSetException {
		ITable testGetInstitutionAdministratorsByGroupFailure = dataSetTest
				.getTable("testGetInstitutionAdministratorsByGroupFailure");
		int rowCount = testGetInstitutionAdministratorsByGroupFailure
				.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionAdministratorsByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetInstitutionAdministratorsByGroupFailure
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionAdministratorsByGroup(groupId, adminType,
							true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionAdministrators() throws DataSetException {
		ITable testGetInstitutionAdministrators = dataSetTest
				.getTable("testGetInstitutionAdministrators");
		int rowCount = testGetInstitutionAdministrators.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String institution = (String) testGetInstitutionAdministrators
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			String admin = (String) testGetInstitutionAdministrators.getValue(
					i, "adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionAdministrators(institutionId, adminType,
							true, srk);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetInstitutionAdministratorsFailure()
			throws DataSetException {
		ITable testGetInstitutionAdministratorsFailure = dataSetTest
				.getTable("testGetInstitutionAdministratorsFailure");
		int rowCount = testGetInstitutionAdministratorsFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String institution = (String) testGetInstitutionAdministratorsFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			String admin = (String) testGetInstitutionAdministratorsFailure
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionAdministrators(institutionId, adminType,
							true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionFundersByGroup() throws DataSetException {
		ITable testGetInstitutionFundersByGroup = dataSetTest
				.getTable("testGetInstitutionFundersByGroup");
		int rowCount = testGetInstitutionFundersByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionFundersByGroup.getValue(
					i, "groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetInstitutionFundersByGroup.getValue(
					i, "adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionFundersByGroup(groupId, adminType, true, srk);
			assertNull(returned);

		}
	}

	@Test
	public void testGetInstitutionFundersByGroupFailure()
			throws DataSetException {
		ITable testGetInstitutionFundersByGroupFailure = dataSetTest
				.getTable("testGetInstitutionFundersByGroupFailure");
		int rowCount = testGetInstitutionFundersByGroupFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionFundersByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetInstitutionFundersByGroupFailure
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionFundersByGroup(groupId, adminType, true, srk);
			assertNull(returned);

		}
	}

	@Test
	public void testGetInstitutionFunders() throws DataSetException {
		ITable testGetInstitutionFunders = dataSetTest
				.getTable("testGetInstitutionFunders");
		int rowCount = testGetInstitutionFunders.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String institution = (String) testGetInstitutionFunders.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			String admin = (String) testGetInstitutionFunders.getValue(i,
					"adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionFunders(institutionId, adminType, true, srk);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetInstitutionFundersFailure() throws DataSetException {
		ITable testGetInstitutionFundersFailure = dataSetTest
				.getTable("testGetInstitutionFundersFailure");
		int rowCount = testGetInstitutionFundersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String institution = (String) testGetInstitutionFundersFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			String admin = (String) testGetInstitutionFundersFailure.getValue(
					i, "adminType");
			int adminType = Integer.parseInt(admin);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionFunders(institutionId, adminType, true, srk);
			assertNull(returned);

		}
	}

	@Test
	public void testGetInstitutionUnderwritersByGroup() throws DataSetException {
		ITable testGetInstitutionUnderwritersByGroup = dataSetTest
				.getTable("testGetInstitutionUnderwritersByGroup");
		int rowCount = testGetInstitutionUnderwritersByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionUnderwritersByGroup
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String underwriter = (String) testGetInstitutionUnderwritersByGroup
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUnderwritersByGroup(groupId,
							underwriterType, true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionUnderwritersByGroupFailure()
			throws DataSetException {
		ITable testGetInstitutionUnderwritersByGroupFailure = dataSetTest
				.getTable("testGetInstitutionUnderwritersByGroupFailure");
		int rowCount = testGetInstitutionUnderwritersByGroupFailure
				.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionUnderwritersByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String underwriter = (String) testGetInstitutionUnderwritersByGroupFailure
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUnderwritersByGroup(groupId,
							underwriterType, true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionUnderwriters() throws DataSetException {
		ITable testGetInstitutionUnderwriters = dataSetTest
				.getTable("testGetInstitutionUnderwriters");
		int rowCount = testGetInstitutionUnderwriters.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String institution = (String) testGetInstitutionUnderwriters
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			String underwriter = (String) testGetInstitutionUnderwriters
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUnderwriters(institutionId, underwriterType,
							true, srk);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetInstitutionUnderwritersFailure() throws DataSetException {
		ITable testGetInstitutionUnderwritersFailure = dataSetTest
				.getTable("testGetInstitutionUnderwritersFailure");
		int rowCount = testGetInstitutionUnderwritersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String institution = (String) testGetInstitutionUnderwritersFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			String underwriter = (String) testGetInstitutionUnderwritersFailure
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUnderwriters(institutionId, underwriterType,
							true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionUsersByGroup() throws DataSetException {
		ITable testGetInstitutionUsersByGroup = dataSetTest
				.getTable("testGetInstitutionUsersByGroup");
		int rowCount = testGetInstitutionUsersByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionUsersByGroup.getValue(i,
					"groupId");
			int groupId = Integer.parseInt(group);
			String user = (String) testGetInstitutionUsersByGroup.getValue(i,
					"userType");
			int userType = Integer.parseInt(user);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUsersByGroup(groupId, userType, true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionUsersByGroupFailure() throws DataSetException {
		ITable testGetInstitutionUsersByGroupFailure = dataSetTest
				.getTable("testGetInstitutionUsersByGroupFailure");
		int rowCount = testGetInstitutionUsersByGroupFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetInstitutionUsersByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String user = (String) testGetInstitutionUsersByGroupFailure
					.getValue(i, "userType");
			int userType = Integer.parseInt(user);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUsersByGroup(groupId, userType, true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInstitutionUsers() throws DataSetException {
		ITable testGetInstitutionUsers = dataSetTest
				.getTable("testGetInstitutionUsers");
		int rowCount = testGetInstitutionUsers.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String institution = (String) testGetInstitutionUsers.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			String user = (String) testGetInstitutionUsers.getValue(i,
					"userType");
			int userType = Integer.parseInt(user);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUsers(institutionId, userType, true, srk);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetInstitutionUsersFailure() throws DataSetException {
		ITable testGetInstitutionUsersFailure = dataSetTest
				.getTable("testGetInstitutionUsersFailure");
		int rowCount = testGetInstitutionUsersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String institution = (String) testGetInstitutionUsersFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			String user = (String) testGetInstitutionUsersFailure.getValue(i,
					"userType");
			int userType = Integer.parseInt(user);
			Vector<UserProfile> returned = userProfileQuery
					.getInstitutionUsers(institutionId, userType, true, srk);
			assertNull(returned);
		}
	}

	@Test
	public void testGetLenderAdministratorsByGroup() throws DataSetException {
		ITable testGetLenderAdministratorsByGroup = dataSetTest
				.getTable("testGetLenderAdministratorsByGroup");
		int rowCount = testGetLenderAdministratorsByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetLenderAdministratorsByGroup
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetLenderAdministratorsByGroup
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderAdministratorsByGroup
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderAdministratorsByGroup(groupId, adminType, true,
							srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderAdministratorsByGroupFailure()
			throws DataSetException {
		ITable testGetLenderAdministratorsByGroupFailure = dataSetTest
				.getTable("testGetLenderAdministratorsByGroupFailure");
		int rowCount = testGetLenderAdministratorsByGroupFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetLenderAdministratorsByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetLenderAdministratorsByGroupFailure
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderAdministratorsByGroupFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderAdministratorsByGroup(groupId, adminType, true,
							srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetLenderAdministrators() throws DataSetException {
		ITable testGetLenderAdministrators = dataSetTest
				.getTable("testGetLenderAdministrators");
		int rowCount = testGetLenderAdministrators.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String lender = (String) testGetLenderAdministrators.getValue(i,
					"lenderId");
			int lenderId = Integer.parseInt(lender);
			String admin = (String) testGetLenderAdministrators.getValue(i,
					"adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderAdministrators.getValue(
					i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderAdministrators(lenderId, adminType, true, srk,
							institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderAdministratorsFailure() throws DataSetException {
		ITable testGetLenderAdministratorsFailure = dataSetTest
				.getTable("testGetLenderAdministratorsFailure");
		int rowCount = testGetLenderAdministratorsFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String lender = (String) testGetLenderAdministratorsFailure
					.getValue(i, "lenderId");
			int lenderId = Integer.parseInt(lender);
			String admin = (String) testGetLenderAdministratorsFailure
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderAdministratorsFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderAdministrators(lenderId, adminType, true, srk,
							institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetLenderFundersByGroup() throws DataSetException {
		ITable testGetLenderFundersByGroup = dataSetTest
				.getTable("testGetLenderFundersByGroup");
		int rowCount = testGetLenderFundersByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String group = (String) testGetLenderFundersByGroup.getValue(i,
					"groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetLenderFundersByGroup.getValue(i,
					"adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderFundersByGroup.getValue(
					i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderFundersByGroup(groupId, adminType, true, srk,
							institutionId);
			assert returned.size()!=0;
			//assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderFundersByGroupFailure() throws DataSetException {
		ITable testGetLenderFundersByGroupFailure = dataSetTest
				.getTable("testGetLenderFundersByGroupFailure");
		int rowCount = testGetLenderFundersByGroupFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String group = (String) testGetLenderFundersByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String admin = (String) testGetLenderFundersByGroupFailure
					.getValue(i, "adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderFundersByGroupFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderFundersByGroup(groupId, adminType, true, srk,
							institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetLenderFunders() throws DataSetException {
		ITable testGetLenderFunders = dataSetTest
				.getTable("testGetLenderFunders");
		int rowCount = testGetLenderFunders.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String lender = (String) testGetLenderFunders.getValue(i,
					"lenderId");
			int lenderId = Integer.parseInt(lender);
			String admin = (String) testGetLenderFunders.getValue(i,
					"adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderFunders.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getLenderFunders(
					lenderId, adminType, true, srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderFundersFailure() throws DataSetException {
		ITable testGetLenderFundersFailure = dataSetTest
				.getTable("testGetLenderFundersFailure");
		int rowCount = testGetLenderFundersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String lender = (String) testGetLenderFundersFailure.getValue(i,
					"lenderId");
			int lenderId = Integer.parseInt(lender);
			String admin = (String) testGetLenderFundersFailure.getValue(i,
					"adminType");
			int adminType = Integer.parseInt(admin);
			String institution = (String) testGetLenderFundersFailure.getValue(
					i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getLenderFunders(
					lenderId, adminType, true, srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetLenderUnderwritersByGroup() throws DataSetException {
		ITable testGetLenderUnderwritersByGroup = dataSetTest
				.getTable("testGetLenderUnderwritersByGroup");
		int rowCount = testGetLenderUnderwritersByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String group = (String) testGetLenderUnderwritersByGroup.getValue(
					i, "groupId");
			int groupId = Integer.parseInt(group);
			String underwriter = (String) testGetLenderUnderwritersByGroup
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetLenderUnderwritersByGroup
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderUnderwritersByGroup(groupId, underwriterType,
							true, srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderUnderwritersByGroupFailure()
			throws DataSetException {
		ITable testGetLenderUnderwritersByGroupFailure = dataSetTest
				.getTable("testGetLenderUnderwritersByGroupFailure");
		int rowCount = testGetLenderUnderwritersByGroupFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String group = (String) testGetLenderUnderwritersByGroupFailure
					.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String underwriter = (String) testGetLenderUnderwritersByGroupFailure
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetLenderUnderwritersByGroupFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderUnderwritersByGroup(groupId, underwriterType,
							true, srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetInternalUserTypeId() throws DataSetException,
			RemoteException {
		ITable testGetInternalUserTypeId = dataSetTest
				.getTable("testGetInternalUserTypeId");
		int rowCount = testGetInternalUserTypeId.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String user = (String) testGetInternalUserTypeId.getValue(i,
					"userId");
			int userId = Integer.parseInt(user);
			String institution = (String) testGetInternalUserTypeId.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			int returned = userProfileQuery.getInternalUserTypeId(userId,
					institutionId, srk);
			assertNotNull(returned);

		}
	}

	public void testGetGroupsWhereManagerOrSupervisor() throws Exception {

		Hashtable result = userProfileQuery.getGroupsWhereManagerOrSupervisor(
				0, srk);
		assertNotNull(result);
	}

	public void testGetGroupsWhereManagerOrSupervisorFailure() throws Exception {

		Hashtable result = userProfileQuery.getGroupsWhereManagerOrSupervisor(
				110, srk);
		assertNull(result);
	}

	
	
	public void testGetUserProfileInternal() throws Exception {

		UserProfile userProfile = userProfileQuery.getUserProfileInternal(1640,
				0, new Vector(), 0, 0, srk);
		assertNotNull(userProfile);
	}

	public void testGetUserProfileInternalFailure() throws Exception {

		UserProfile userProfile = userProfileQuery.getUserProfileInternal(1640,
				3, new Vector(), 0, 0, srk);
		assertNull(userProfile);
	}

	@Test
	public void testGetInternalUserTypeIdFailure() throws DataSetException,
			RemoteException {
		ITable testGetInternalUserTypeIdFailure = dataSetTest
				.getTable("testGetInternalUserTypeIdFailure");
		int rowCount = testGetInternalUserTypeIdFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String user = (String) testGetInternalUserTypeIdFailure.getValue(i,
					"userId");
			int userId = Integer.parseInt(user);
			String institution = (String) testGetInternalUserTypeIdFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			int returned = userProfileQuery.getInternalUserTypeId(userId,
					institutionId, srk);
			assertNotSame(1, returned);
		}
	}

	@Test
	public void testGetLenderUnderwriters() throws DataSetException {
		ITable testGetLenderUnderwriters = dataSetTest
				.getTable("testGetLenderUnderwriters");
		int rowCount = testGetLenderUnderwriters.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String lender = (String) testGetLenderUnderwriters.getValue(i,
					"lenderId");
			int lenderId = Integer.parseInt(lender);
			String underwriter = (String) testGetLenderUnderwriters.getValue(i,
					"underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetLenderUnderwriters.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderUnderwriters(lenderId, underwriterType, true,
							srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderUnderwritersFailure() throws DataSetException {
		ITable testGetLenderUnderwritersFailure = dataSetTest
				.getTable("testGetLenderUnderwritersFailure");
		int rowCount = testGetLenderUnderwritersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {

			String lender = (String) testGetLenderUnderwritersFailure.getValue(
					i, "lenderId");
			int lenderId = Integer.parseInt(lender);
			String underwriter = (String) testGetLenderUnderwritersFailure
					.getValue(i, "underwriterType");
			int underwriterType = Integer.parseInt(underwriter);
			String institution = (String) testGetLenderUnderwritersFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderUnderwriters(lenderId, underwriterType, true,
							srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetLenderUsersByGroup() throws DataSetException {
		ITable testGetLenderUsersByGroup = dataSetTest
				.getTable("testGetLenderUsersByGroup");
		int rowCount = testGetLenderUsersByGroup.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetLenderUsersByGroup.getValue(i,
					"groupId");
			int groupId = Integer.parseInt(group);
			String user = (String) testGetLenderUsersByGroup.getValue(i,
					"userType");
			int userType = Integer.parseInt(user);
			String institution = (String) testGetLenderUsersByGroup.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderUsersByGroup(groupId, userType, true, srk,
							institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderUsersByGroupFailure() throws DataSetException {
		ITable testGetLenderUsersByGroupFailure = dataSetTest
				.getTable("testGetLenderUsersByGroupFailure");
		int rowCount = testGetLenderUsersByGroupFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetLenderUsersByGroupFailure.getValue(
					i, "groupId");
			int groupId = Integer.parseInt(group);
			String user = (String) testGetLenderUsersByGroupFailure.getValue(i,
					"userType");
			int userType = Integer.parseInt(user);
			String institution = (String) testGetLenderUsersByGroupFailure
					.getValue(i, "institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery
					.getLenderUsersByGroup(groupId, userType, true, srk,
							institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetLenderUsers() throws DataSetException {
		ITable testGetLenderUsers = dataSetTest.getTable("testGetLenderUsers");
		int rowCount = testGetLenderUsers.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String lender = (String) testGetLenderUsers.getValue(i, "lenderId");
			int lenderId = Integer.parseInt(lender);
			String user = (String) testGetLenderUsers.getValue(i, "userType");
			int userType = Integer.parseInt(user);
			String institution = (String) testGetLenderUsers.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getLenderUsers(
					lenderId, userType, true, srk, institutionId);
			assertNotNull(returned);
		}
	}

	@Test
	public void testGetLenderUsersFailure() throws DataSetException {
		ITable testGetLenderUsersFailure = dataSetTest
				.getTable("testGetLenderUsersFailure");
		int rowCount = testGetLenderUsersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String lender = (String) testGetLenderUsersFailure.getValue(i,
					"lenderId");
			int lenderId = Integer.parseInt(lender);
			String user = (String) testGetLenderUsersFailure.getValue(i,
					"userType");
			int userType = Integer.parseInt(user);
			String institution = (String) testGetLenderUsersFailure.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getLenderUsers(
					lenderId, userType, true, srk, institutionId);
			assertNull(returned);
		}
	}

	@Test
	public void testGetAnyUsers() throws DataSetException {
		ITable testGetAnyUsers = dataSetTest.getTable("testGetAnyUsers");
		int rowCount = testGetAnyUsers.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String user = (String) testGetAnyUsers.getValue(i, "userType");
			int userType = Integer.parseInt(user);
			Vector<UserProfile> returned = userProfileQuery.getAnyUsers(
					userType, true, srk);
			assertNotNull(returned);

		}
	}

	@Test
	public void testGetAnyUsersFailure() throws DataSetException {
		ITable testGetAnyUsersFailure = dataSetTest
				.getTable("testGetAnyUsersFailure");
		int rowCount = testGetAnyUsersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String user = (String) testGetAnyUsersFailure.getValue(i,
					"userType");
			int userType = Integer.parseInt(user);
			Vector<UserProfile> returned = userProfileQuery.getAnyUsers(
					userType, true, srk);
			assertNull(returned);

		}
	}

	public void testGetAutoUsers() throws DataSetException {
		ITable testGetAutoUsers = dataSetTest.getTable("testGetAutoUsers");
		int rowCount = testGetAutoUsers.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetAutoUsers.getValue(i, "groupId");
			int groupId = Integer.parseInt(group);
			String institution = (String) testGetAutoUsers.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getAutoUsers(
					groupId, true, srk, institutionId);
			assertNotNull(returned);
		}
	}

	public void testGetAutoUsersFailure() throws DataSetException {
		ITable testGetAutoUsersFailure = dataSetTest
				.getTable("testGetAutoUsersFailure");
		int rowCount = testGetAutoUsersFailure.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String group = (String) testGetAutoUsersFailure.getValue(i,
					"groupId");
			int groupId = Integer.parseInt(group);
			String institution = (String) testGetAutoUsersFailure.getValue(i,
					"institutionId");
			int institutionId = Integer.parseInt(institution);
			Vector<UserProfile> returned = userProfileQuery.getAutoUsers(
					groupId, true, srk, institutionId);
			assertNotSame(1, returned);
		}
	}

}
