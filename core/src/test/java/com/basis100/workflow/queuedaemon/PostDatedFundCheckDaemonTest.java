package com.basis100.workflow.queuedaemon;

import java.io.IOException;
import java.util.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.security.DLM;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

public class PostDatedFundCheckDaemonTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private PostDatedFundCheckDaemon postDatedFundCheckDaemon;
	Vector vector=null;
	public PostDatedFundCheckDaemonTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PostDatedFundCheckDaemon.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;
		//return DatabaseOperation.UPDATE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		postDatedFundCheckDaemon = PostDatedFundCheckDaemon.getInstance();
		postDatedFundCheckDaemon.run();
			
	}
	
	 public void testSendUpload() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testSendUpload = dataSetTest.getTable("testSendUpload");
	   String dealUpload=(String)testSendUpload.getValue(0,"DEALID");
	   int dealId=Integer.parseInt(dealUpload);	  
	   String copy=(String)testSendUpload.getValue(0,"COPYID");
	   int copyId=Integer.parseInt(copy);
	   String institution=(String)testSendUpload.getValue(0,"INSTITUTIONPROFILEID");
	   int institutionId=Integer.parseInt(institution);
		postDatedFundCheckDaemon.sendUpload();
        assertNotNull(postDatedFundCheckDaemon);
}

}
