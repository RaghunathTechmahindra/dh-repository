package com.basis100.workflow;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.workflow.entity.AssignedTask;


public class WFDealTaskHistoryTest extends  FXDBTestCase{
	
	private IDataSet dataSetTest;
	private WFDealTaskHistory wfDealTaskHistory=null;
	private WFDealTaskHistory wfDealTaskHistory1=null;

	public WFDealTaskHistoryTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("WFDealTaskHistoryDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("WFDealTaskHistoryDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		ITable testIsLockedByUser = dataSetTest.getTable("setUp");
		int dealId=Integer.parseInt(testIsLockedByUser.getValue(0, "dealId").toString());
		int dealId1=Integer.parseInt(testIsLockedByUser.getValue(0, "dealId1").toString());
		wfDealTaskHistory=new WFDealTaskHistory(srk,dealId);
		wfDealTaskHistory1=new WFDealTaskHistory(srk,dealId1);
	}

	
	public void testGetAssignedTaskUser() throws Exception {
		
		ITable testIsLockedByUser = dataSetTest.getTable("testGetAssignedTaskUser");
		int taskId=Integer.parseInt(testIsLockedByUser.getValue(0, "taskId").toString());
		int stageNum=Integer.parseInt(testIsLockedByUser.getValue(0, "stageNum").toString());
		int pass=Integer.parseInt(testIsLockedByUser.getValue(0, "pass").toString());
		int result=wfDealTaskHistory.getAssignedTaskUser(taskId,stageNum,pass,false);
		assertNotSame(0,result);
		
	}
      public void testGetAssignedTaskUserFailure() throws Exception {
    	ITable testIsLockedByUser = dataSetTest.getTable("testGetAssignedTaskUserFailure");
    	int taskId=Integer.parseInt(testIsLockedByUser.getValue(0, "taskId").toString());
		int stageNum=Integer.parseInt(testIsLockedByUser.getValue(0, "stageNum").toString());
		int pass=Integer.parseInt(testIsLockedByUser.getValue(0, "pass").toString());
		int result=wfDealTaskHistory1.getAssignedTaskUser(taskId,stageNum,pass,true);
		assertSame(0,result);
	}
      public void testGetAllOpenTasks() throws Exception {
    	 
    	  ITable testIsLockedByUser = dataSetTest.getTable("testGetAllOpenTasks");
      	  Vector resultVecor=wfDealTaskHistory.getAllOpenTasks();
          int resultSize=resultVecor.size();
          assertNotSame(0,resultSize);	
       }
      public void testGetAllOpenTasksFailure() throws Exception {
     	 
    	  ITable testIsLockedByUser = dataSetTest.getTable("testGetAllOpenTasksFailure");
      	  Vector resultVecor=wfDealTaskHistory1.getAllOpenTasks();
          int resultSize=resultVecor.size();
    	  assertSame(0,resultSize);	
       }
      public void testGetAllOpenTasksArray() throws Exception {
     	 
    	  ITable testIsLockedByUser = dataSetTest.getTable("testGetAllOpenTasksArray");
      	  AssignedTask result[]=wfDealTaskHistory.getAllOpenTasksArray();
          int resultSize=result.length;
          assertNotSame(0,resultSize);	
       }
      public void testGetAllOpenTasksArrayFailure() throws Exception {
     	 
    	  ITable testIsLockedByUser = dataSetTest.getTable("testGetAllOpenTasksArrayFailure");
      	  AssignedTask result[]=wfDealTaskHistory1.getAllOpenTasksArray();
          int resultSize=result.length;
          assertSame(0,resultSize);	

      }
    
      public void testGetOpenTasksForPage() throws Exception {

		ITable testIsLockedByUser = dataSetTest.getTable("testGetOpenTasksForPage");
		int pageId = Integer.parseInt(testIsLockedByUser.getValue(0, "pageId").toString());
		Vector resultVector=wfDealTaskHistory.getOpenTasksForPage(srk, pageId);
		int resultSize=resultVector.size();
		assertNotSame(0,resultSize);	
	}
      public void testGetOpenTasksForPageFailure() throws Exception {

  		ITable testIsLockedByUser = dataSetTest.getTable("testGetOpenTasksForPageFailure");
  		int pageId = Integer.parseInt(testIsLockedByUser.getValue(0, "pageId").toString());
  		Vector resultVector=wfDealTaskHistory.getOpenTasksForPage(srk, pageId);
  		int resultSize=resultVector.size();
  		assertSame(0,resultSize);	
  	}
      public void testGetWorkflowId() throws Exception{
    	  int result=wfDealTaskHistory.getWorkflowId();
    	  assertNotSame(-1,result);
      }
      public void testGetWorkflowIdFailure() throws Exception{
    	  int result=wfDealTaskHistory1.getWorkflowId();
    	  assertSame(-1,result);
     	  
      }
      public void testGetWorkflowTransitionStage() throws Exception{
    	  int result=wfDealTaskHistory.getWorkflowTransitionStage();
    	  assertNotSame(1,result);
      }
      public void testGetWorkflowTransitionStageFailure()throws Exception{
    	  int result=wfDealTaskHistory1.getWorkflowTransitionStage();
    	  assertSame(1,result);
     	  
      }
      
      public void testHaveOpenTasks()throws Exception{
    	  
    	  boolean result=wfDealTaskHistory.haveOpenTasks();
    	  assertSame(true,result);
      }
      public void testHaveOpenTasksFailure() throws Exception{
    	  boolean result=wfDealTaskHistory1.haveOpenTasks();
    	  assertSame(false,result);
     	  
      }
      public void testisTaskAlreadyAssigned()throws Exception{
    	  ITable testIsLockedByUser = dataSetTest.getTable("testisTaskAlreadyAssigned");
  		int taskId = Integer.parseInt(testIsLockedByUser.getValue(0, "taskId").toString());
  		int workflowStage = Integer.parseInt(testIsLockedByUser.getValue(0, "workflowStage").toString());
    	boolean result=wfDealTaskHistory.isTaskAlreadyAssigned(taskId, workflowStage);
    	  assertSame(true,result);
      } 
      public void testisTaskAlreadyAssignedFailure() throws Exception{
    	  ITable testIsLockedByUser = dataSetTest.getTable("testisTaskAlreadyAssignedFailure");
    	  int taskId = Integer.parseInt(testIsLockedByUser.getValue(0, "taskId").toString());
      	  int workflowStage = Integer.parseInt(testIsLockedByUser.getValue(0, "workflowStage").toString());
          boolean result=wfDealTaskHistory1.isTaskAlreadyAssigned(taskId, workflowStage);
    	  assertNotSame(true,result);
      } 
      public void testisTaskOpen() throws Exception{
    	  ITable testIsLockedByUser = dataSetTest.getTable("testisTaskOpen");
  		int taskId = Integer.parseInt(testIsLockedByUser.getValue(0, "taskId").toString());
  		int workflowStage = Integer.parseInt(testIsLockedByUser.getValue(0, "workflowStage").toString());
    	boolean result=wfDealTaskHistory.isTaskOpen(taskId, workflowStage);
    	  assertSame(true,result);
      } 
      public void testisTaskOpenFailure() throws Exception{
    	  ITable testIsLockedByUser = dataSetTest.getTable("testisTaskOpenFailure");
  		int taskId = Integer.parseInt(testIsLockedByUser.getValue(0, "taskId").toString());
  		int workflowStage = Integer.parseInt(testIsLockedByUser.getValue(0, "workflowStage").toString());
    	boolean result=wfDealTaskHistory1.isTaskOpen(taskId, workflowStage);
    	assertSame(false,result);
      } 
      
      public void testIsWorkflowTransition()
      {
    	  boolean result=wfDealTaskHistory.isWorkflowTransition();
    	  assertSame(false,result);
      }
      public void testIsWorkflowTransitionFailure()
      {
    	  boolean result=wfDealTaskHistory1.isWorkflowTransition();
    	  assertSame(true,result);
      }
 }
