/*
 * @(#)WorkflowAssistBeanTest.java    2005-5-2
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.workflow;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.entity.Deal;

import com.basis100.TestingHelper;
import com.basis100.TestingEnvironmentInitializer;
import com.basis100.workflow.routing.UnderwriterRoutingTestBase;

/**
 * WorkflowAssistBeanTest is the unit test class for class {@link
 * WorkflowAssistBean}.  We are going to load deal from database and use them to
 * exame the assign underwriter logic.  There is no transaction commited in this
 * class.  And we may change some attributes of the deal object but not the
 * entry in DB.
 *
 * @version   1.0 2005-5-2
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class WorkflowAssistBeanTest extends UnderwriterRoutingTestBase {

    // The logger
    private static Log _log = LogFactory.getLog(WorkflowAssistBeanTest.class);

    /**
     * Constructor function
     */
    public WorkflowAssistBeanTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public WorkflowAssistBeanTest(String name) {
        super(name);
    }

    /**
     * choose the methods you want to test.
     */
    public static Test suite() {

        //TestSuite suite = new TestSuite(WorkflowAssistBeanTest.class);
        TestSuite suite = new TestSuite();
        suite.addTest(new WorkflowAssistBeanTest("testAssignUnderwriter"));

        return suite;
    }

    /**
     * setting up the testing env, prepare the test cases...
     */
    protected void setUp() {

        super.setUp();
        _log.info("Testing WorkflowAssistBean ...");
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
        _log.info("====================================================================");
    }

    /**
     * Trying to simulate assign an underwriter.
     */
    public void testAssignUnderwriter() {

        _log.debug("Testing [assignUnderwriter] ...");

        for (Iterator i = _testingDeals.iterator(); i.hasNext(); ) {

            Deal aDeal = (Deal) i.next();
            _log.info("------ Trying to assign an underwriter for Deal: ");
            _log.info(TestingHelper.dealString(aDeal));

            // the workflow details info.
            WorkflowAssistBean.WorkflowDetails wfInfo =
                _assistBean.selectWorkflow(aDeal, _srk);
            _log.info("Workflow Info: " + workflowDetailsString(wfInfo));
            // Instantiate the DealUserProfiles object.
            aDeal.setUnderwriterUserId(0);  // set to zero for testing.
            aDeal.setAdministratorId(0); // set to zero for testing.
            DealUserProfiles dealUserProfiles =
                new DealUserProfiles(aDeal, _srk);
            // try to assign the underwriter.
            _assistBean.assignUnderwriter(aDeal, dealUserProfiles, wfInfo, _srk);
            _log.info("  Assign to underwriter: " +
                      dealUserProfiles.getUnderwriterProfile());
        }
    }

    /**
     * testing method select Workflow.
     */
    public void testSelectWorkflow() {

        _log.debug("Testing [selectWorkflow] ...");

        for (Iterator i = _testingDeals.iterator(); i.hasNext(); ) {

            Deal aDeal = (Deal) i.next();
            _log.info("------ Looking workflow details for deal: ");
            _log.info(TestingHelper.dealString(aDeal));

            WorkflowAssistBean.WorkflowDetails details =
                _assistBean.selectWorkflow(aDeal, _srk);
            _log.info("   Got Workflow Details: " +
                      workflowDetailsString(details));
        }
    }

    /**
     * return a String to represent the given WorkflowDetails.
     */
    public String workflowDetailsString(WorkflowAssistBean.WorkflowDetails
                                        details) {

        StringBuffer ret = new StringBuffer();
        ret.append("WorkflowDetails [");
        if (details != null) {
            ret.append("Workflow ID=").
                append(details.getWorkflowId()).
                append(" AllowSOBMapping=").
                append(details.getAllowSOBMapping()).
                append(" ApprovalAuthorityQualification=").
                append(details.getApprovalAuthorityQualification());
        } else {
            ret.append(details);
        }
        ret.append("]");

        return ret.toString();
    }
}
