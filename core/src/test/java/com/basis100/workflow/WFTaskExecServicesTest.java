package com.basis100.workflow;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.workflow.entity.AssignedTask;

public class WFTaskExecServicesTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	WFTaskExecServices wfTaskExecServices = null;
	WFTaskExecServices wfTaskExecServices1 = null;
	AssignedTask assignedTask = null;

	public WFTaskExecServicesTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				WFTaskExecServices.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		wfTaskExecServices1 = new WFTaskExecServices(srk, 0, 0, 0, 0);
		assignedTask = new AssignedTask(srk);
	}

	public void testCancelOpenClosingTasksfailure() throws Exception {
		srk.beginTransaction();
		ITable testCancelOpenClosingTasksfailure = dataSetTest
				.getTable("testCancelOpenClosingTasksfailure");
		String deal = (String) testCancelOpenClosingTasksfailure.getValue(0,
				"DEALID");
		int dealId = Integer.parseInt(deal);
		String copy = (String) testCancelOpenClosingTasksfailure.getValue(0,
				"COPYID");
		int copyId = Integer.parseInt(copy);
		wfTaskExecServices = new WFTaskExecServices(srk, 0, dealId, copyId, 0);
		boolean b = wfTaskExecServices.cancelOpenClosingTasks();
		assertTrue(b);
		srk.rollbackTransaction();
	}

	public void testCancelOpenClosingTasks() throws Exception {
		srk.beginTransaction();
		ITable testCancelOpenClosingTasks = dataSetTest
				.getTable("testCancelOpenClosingTasks");
		String deal = (String) testCancelOpenClosingTasks.getValue(0, "DEALID");
		int dealId = Integer.parseInt(deal);
		String copy = (String) testCancelOpenClosingTasks.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		wfTaskExecServices = new WFTaskExecServices(srk, 0, dealId, copyId, 0);
		boolean b = wfTaskExecServices.cancelOpenClosingTasks();
		assertTrue(b);
		srk.rollbackTransaction();

	}

	public void testRealtaskCompleted() throws Exception {
		ITable testIsLockedByUser = dataSetTest
				.getTable("testRealtaskCompleted");
		int userId = Integer.parseInt(testIsLockedByUser.getValue(0, "userId")
				.toString());
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int copyId = Integer.parseInt(testIsLockedByUser.getValue(0, "copyId")
				.toString());
		int userTypeId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userTypeId").toString());
		wfTaskExecServices = new WFTaskExecServices(srk, userId, dealId,
				copyId, userTypeId);
		boolean status = wfTaskExecServices.REALtaskCompleted();
		assertTrue(status);

	}

	public void testRealtaskCompletedFailure() throws Exception {
		ITable testIsLockedByUser = dataSetTest
				.getTable("testRealtaskCompletedFailure");
		try {
			int userId = Integer.parseInt(testIsLockedByUser.getValue(0,
					"userId").toString());
			int dealId = Integer.parseInt(testIsLockedByUser.getValue(0,
					"dealId").toString());
			int copyId = Integer.parseInt(testIsLockedByUser.getValue(0,
					"copyId").toString());
			int userTypeId = Integer.parseInt(testIsLockedByUser.getValue(0,
					"userTypeId").toString());
			wfTaskExecServices = new WFTaskExecServices(srk, userId, dealId,
					copyId, userTypeId);
			boolean status = wfTaskExecServices.REALtaskCompleted();
			assertFalse(status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testSetPageForAssignedTask() throws Exception {
		srk.beginTransaction();
		// getTaskId(), aTask.getWorkflowId()
		ITable testIsLockedByUser = dataSetTest
				.getTable("testSetPageForAssignedTask");
		int taskId = Integer.parseInt(testIsLockedByUser.getValue(0, "taskId")
				.toString());
		int workflowId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"workflowId").toString());
		// userProfileId, int userTypeId
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		int userTypeId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userTypeId").toString());
		assignedTask.setTaskId(taskId);
		assignedTask.setWorkflowId(workflowId);
		int result = wfTaskExecServices1.setPageForAssignedTask(assignedTask,
				userProfileId, userTypeId);
		assertNotSame(1, result);
		srk.rollbackTransaction();

	}

	public void testSetPageForAssignedTaskFailure() throws Exception {
		srk.beginTransaction();
		ITable testIsLockedByUser = dataSetTest
				.getTable("testSetPageForAssignedTaskFailure");
		int taskId = Integer.parseInt(testIsLockedByUser.getValue(0, "taskId")
				.toString());
		int workflowId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"workflowId").toString());
		// userProfileId, int userTypeId
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		int userTypeId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userTypeId").toString());
		assignedTask.setTaskId(taskId);
		assignedTask.setWorkflowId(workflowId);
		int result = wfTaskExecServices1.setPageForAssignedTask(assignedTask,
				0, 0);
		assertSame(0, result);
		srk.rollbackTransaction();
	}
}
