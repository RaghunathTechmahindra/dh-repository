package com.basis100.workflow.notification;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.pk.AssignedTaskBeanPK;

public class WorkflowNotifierTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private WorkflowNotifier workflowNotifier;
	Vector vector=null;
	public WorkflowNotifierTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(WorkflowNotifier.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
		//return DatabaseOperation.UPDATE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.workflowNotifier = new WorkflowNotifier(srk);	
	}
	
	public void testSend() throws Exception{
		ITable testSend = dataSetTest.getTable("testSend");
	   String id=(String)testSend.getValue(0,"DEALID");
	   int dealId=Integer.parseInt(id);	  
	  srk.getExpressState().setDealInstitutionId(1);
	   workflowNotifier.send(dealId,0,"hari");
       assertNotNull(workflowNotifier);
}
	public void testSend1() throws Exception{
		ITable testSend = dataSetTest.getTable("testSend1");
	   String id=(String)testSend.getValue(0,"DEALID");
	   int dealId=Integer.parseInt(id);	  
	  srk.getExpressState().setDealInstitutionId(1);
	   workflowNotifier.send(srk,dealId,0,"hari");
       assertNotNull(workflowNotifier);
}
}
