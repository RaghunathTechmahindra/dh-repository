package com.basis100.util.thread;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;

public class DayScheduleTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private DaySchedule daySchedule;

	public DayScheduleTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				DaySchedule.class.getSimpleName() + "DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testDaySchedule() throws Exception {
		int[] hourPoints = { 10, 23, 45, 56 };
		Boolean initialState = true;
		daySchedule = new DaySchedule(true, hourPoints);
		daySchedule.stateAt(new Date());
		assertTrue(initialState);
	}

	public void testGetFirstActiveTime() throws Exception {
		Date date = new Date();
		int[] hourPoints = { 10, 23, 45, 56 };
		daySchedule = new DaySchedule(true, hourPoints);
		date = daySchedule.getFirstActiveTime();
		assertNotNull(date);
	}

	public void testGetFollowingActiveTime() throws Exception {
		Date date = new Date();
		int[] hourPoints = { 10, 23, 45, 56 };
		daySchedule = new DaySchedule(true, hourPoints);
		date = daySchedule.getFollowingActiveTime(new Date());
		assertNotNull(date);
	}

	public void testGetNextStateTransition() throws Exception {
		Date date = new Date();
		int[] hourPoints = { 10, 23, 45, 56 };
		daySchedule = new DaySchedule(true, hourPoints);
		date = daySchedule.getNextStateTransition(new Date());
		assertNotNull(date);

	}

}
