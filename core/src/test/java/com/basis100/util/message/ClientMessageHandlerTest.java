/**
 * <p>@(#)ClientMessageHandlerTest.java Oct 15, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.util.message;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

public class ClientMessageHandlerTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(ClientMessageHandlerTest.class);

    private SessionResourceKit srk;
    public static final String ENTITY_NAME = "MESSAGE";
    private ClientMessage clientMessage;
    private int messageId;
    private int languagePreferenceId;
    private String messageText;
    private int institutionPRofileId;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception  {

        ResourceManager.init();
        
        int testSeqForGen = 0;
        //      get test data from repository
        srk = new SessionResourceKit("Testing");
        messageId = _dataRepository.getInt(ENTITY_NAME,  "MESSAGEID", 
                                           testSeqForGen);
        languagePreferenceId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "LANGUAGEPREFERENCEID", 
                                                  testSeqForGen);
        messageText = _dataRepository.getString(ENTITY_NAME,  "MESSAGETEXT", 
                                                testSeqForGen);
        institutionPRofileId = _dataRepository.getInt(ENTITY_NAME,  
                                                      "INSTITUTIONPROFILEID", 
                                                      testSeqForGen);

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        srk.freeResources();
    }

    /**
     * test load()
     */
    @Test
    public void findMessage() throws Exception{
        
        srk.getExpressState().setDealInstitutionId(institutionPRofileId);
        ClientMessage message = ClientMessageHandler.getInstance()
            .findMessage(srk, messageId, languagePreferenceId);
        assertEquals(message.getMessageText(), messageText);
    }
}
