/*
 * @(#)TestingEnvironmentInitializer.java    2005-3-16
 *
 */


package com.basis100;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import junit.framework.TestCase;

import config.Config;
import com.basis100.log.SysLog;
import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;

/**
 * TestingEnvironmentInitializer is not a unit test case, it is used to initialize the
 * system resources for the unit testing.
 *
 * @version   1.0 2005-3-16
 * @author    <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class TestingEnvironmentInitializer extends TestCase {

    // The logger
    private static Log _log = LogFactory.getLog(TestingEnvironmentInitializer.class);

    // the loading indicator.
    private static boolean isFirst = true;

    /**
     * initialize the testing environment.
     */
    public static void initTestingEnv() {

        if (isFirst) {
            // initializing the project enviornment.
            SysLog.init(Config.getProperty("SYS_PROPERTIES_LOCATION"));
            try {
                _log.debug("Initializing the property ...");
                PropertiesCache.getInstance().init();
                PropertiesCache.
                    addPropertiesFile(Config.getProperty("SYS_PROPERTIES_LOCATION")
                                      + "mossys.properties");
                _log.debug(Config.getProperty("SYS_PROPERTIES_LOCATION") + "mossys.properties");
                _log.debug("Initializing Resource Manager ...");
//                _log.debug("Pool Name: " + PropertiesCache.getInstance().
//                           getProperty("com.basis100.resource.connectionpoolname"));
                ResourceManager.init(Config.getProperty("SYS_PROPERTIES_LOCATION"));
//                ResourceManager.init(Config.getProperty("SYS_PROPERTIES_LOCATION"),
//                		PropertiesCache.getInstance().
//                		getProperty("com.basis100.resource.connectionpoolname"));
            } catch (Exception e) {
                _log.error("Initializing Error: ", e);
            }
        }
    }

    /**
     * init the testing env.
     */
    public void testInit() {

        TestingEnvironmentInitializer.initTestingEnv();
    }
}
