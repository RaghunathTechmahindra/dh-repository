package com.basis100.deal.calc;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.cache.SystemPropertyCache;
import com.filogix.express.cache.SystemPropertyCacheUtil;


public class DealCalcUtilTest extends Assert{

    private final static Logger logger = 
        LoggerFactory.getLogger(DealCalcUtilTest.class);

    @Test
    public void testCalcPaymentFrequency(){
        int paymentFreqId = 0; 
        boolean isDJDaysYear = true;

        double pmtFreq = Mc.PAY_FREQ_MONTHLY;

        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("MONTHLY: 12 = " + pmtFreq);
        assertEquals(pmtFreq, 12, 0);

        paymentFreqId = Mc.PAY_FREQ_SEMIMONTHLY;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("SEMIMONTHLY: 24 = " + pmtFreq);
        assertEquals(pmtFreq, 24, 0);

        paymentFreqId   = Mc.PAY_FREQ_WEEKLY;
        isDJDaysYear    = true;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("WEEKLY, 365.25d/7d = " + pmtFreq);
        assertEquals(pmtFreq, 365.25d/7d, 0);

        paymentFreqId   = Mc.PAY_FREQ_WEEKLY;
        isDJDaysYear    = false;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("WEEKLY, 365d/7d = " + pmtFreq);
        assertEquals(pmtFreq, 365d/7d, 0);

        paymentFreqId   = Mc.PAY_FREQ_BIWEEKLY;
        isDJDaysYear    = true;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("BIWEEKLY, 365.25d/14d = " + pmtFreq);
        assertEquals(pmtFreq, 365.25d/14d, 0);

        paymentFreqId   = Mc.PAY_FREQ_BIWEEKLY;
        isDJDaysYear    = false;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("BIWEEKLY, 365d/14d = " + pmtFreq);
        assertEquals(pmtFreq, 365d/14d, 0);


        paymentFreqId   = Mc.PAY_FREQ_ACCELERATED_WEEKLY;
        isDJDaysYear    = true;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("ACCELERATED_WEEKLY, 365.25d/7d = " + pmtFreq);
        assertEquals(pmtFreq, 365.25d/7d, 0);

        paymentFreqId   = Mc.PAY_FREQ_ACCELERATED_WEEKLY;
        isDJDaysYear    = false;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("ACCELERATED_WEEKLY, 365d/7d = " + pmtFreq);
        assertEquals(pmtFreq, 365d/7d, 0);

        paymentFreqId   = Mc.PAY_FREQ_ACCELERATED_BIWEEKLY;
        isDJDaysYear    = true;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("ACCELERATED_BIWEEKLY, 365.25d/14d = " + pmtFreq);
        assertEquals(pmtFreq, 365.25d/14d, 0);

        paymentFreqId   = Mc.PAY_FREQ_ACCELERATED_BIWEEKLY;
        isDJDaysYear    = false;
        pmtFreq = DealCalcUtil.calcPaymentFrequency(paymentFreqId, isDJDaysYear);
        logger.debug("ACCELERATED_BIWEEKLY, 365d/14d = " + pmtFreq);
        assertEquals(pmtFreq, 365d/14d, 0);


    }

	/**
     * 4.3GR
     */
    @Test
    public void testCalcPandIFully() throws Exception {

        //TODO: following fileds are depending on data in db
        int institutionProfileId = 0; 
        //this test expects institutionProfileid = 0 exists in db
        int interestCompoundingId = 0; 
        //this test expects the following data exists in DB  
        // interestCompoundingId:0 = INTERESTCOMPOUNDDESCRIPTION:2
        // interestCompoundingId:1 = INTERESTCOMPOUNDDESCRIPTION:12

        double totalLoanAmount = 200000;
        int paymentFrequencyId = Mc.PAY_FREQ_MONTHLY;
        double netInterestRate = 5; //5%
        int amortizationMonths = 300; //25 years
        int repaymentTypeId = 0;

        String rateCode = "00";

        PropertiesCache pc = PropertiesCache.getInstance();


        //RateCode: UNCNF
        double pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, "UNCNF");
        assertEquals(0, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //repayment type not in 2 or 3, compound frequency: Semi-annually: 2
        //Monthly
        paymentFrequencyId = Mc.PAY_FREQ_MONTHLY;
        interestCompoundingId = 0;

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding

        assertEquals(1163.21, pandiAmount, 0);

        //repayment type in 2 or 3, compound frequency: Semi-annually: 2
        //Monthly
        interestCompoundingId = 1;
        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(1169.18, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //repayment type not in 2 or 3, compound frequency: Semi-annually: 2
        //Semi Monthly
        paymentFrequencyId = Mc.PAY_FREQ_SEMIMONTHLY;
        interestCompoundingId = 0;
        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(581.01, pandiAmount, 0);

        //repayment type  in 2 or 3, compound frequency: Semi-annually: 2
        //Semi Monthly
        interestCompoundingId = 1;
        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(583.98, pandiAmount, 0);        

        //-------------------------------------------------------------------------------------------

        //compound frequency Semi-annually: 2
        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y
        paymentFrequencyId = Mc.PAY_FREQ_BIWEEKLY;
        interestCompoundingId = 0;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(535.36, pandiAmount, 0);

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(535.55, pandiAmount, 0);        

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(534.43, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(534.8, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "N");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(536.87, pandiAmount, 0);    

        //------------------------------------------------------------

        //compound frequency Monthly: 12

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y
        paymentFrequencyId = Mc.PAY_FREQ_BIWEEKLY;
        interestCompoundingId = 1;

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(538.1, pandiAmount, 0);

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(538.28, pandiAmount, 0);        

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(537.17, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(537.54, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //Bi weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "N");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(539.62, pandiAmount, 0);    

        //-------------------------------------------------------------------------------------------

        //compound frequency Semi-annually: 2
        //repayment type not in 2 or 3
        //Accelerated Bi weekly
        paymentFrequencyId = Mc.PAY_FREQ_ACCELERATED_BIWEEKLY;
        interestCompoundingId = 0;

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(581.6, pandiAmount, 0);

        //compound frequency Monthly: 12
        //repayment type not in 2 or 3
        //Accelerated Bi weekly
        paymentFrequencyId = Mc.PAY_FREQ_ACCELERATED_BIWEEKLY;
        interestCompoundingId = 1;

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(584.59, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //compound frequency Semi-annually: 2
        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y
        paymentFrequencyId = Mc.PAY_FREQ_WEEKLY;
        interestCompoundingId = 0;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(267.56, pandiAmount, 0);

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(267.65, pandiAmount, 0);        

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(267.09, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(267.27, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "N");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(268.43, pandiAmount, 0);    

        //------------------------------------------------------------

        //compound frequency Monthly: 12

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y
        paymentFrequencyId = Mc.PAY_FREQ_WEEKLY;
        interestCompoundingId = 1;

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(268.92, pandiAmount, 0);

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = Y
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "Y");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(269.01, pandiAmount, 0);        

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(268.46, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = Y
        //com.filogix.calc.DJNumberOfPayments = N
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "Y");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(268.64, pandiAmount, 0);    

        //repayment type not in 2 or 3
        //weekly
        //com.basis100.calc.useformulaforweeklybiweeklynonaccpay = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.useformulaforweeklybiweeklynonaccpay", "N");
        overWriteProperty(institutionProfileId, pc, "com.filogix.calc.DJNumberOfPayments", "N");
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(269.81, pandiAmount, 0);    

        //-------------------------------------------------------------------------------------------

        //compound frequency Semi-annually: 2
        //repayment type not in 2 or 3
        //Accelerated weekly
        paymentFrequencyId = Mc.PAY_FREQ_ACCELERATED_WEEKLY;
        interestCompoundingId = 0;

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(290.8, pandiAmount, 0);

        //compound frequency Monthly: 12
        //repayment type not in 2 or 3
        //Accelerated weekly
        paymentFrequencyId = Mc.PAY_FREQ_ACCELERATED_WEEKLY;
        interestCompoundingId = 1;

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(292.3, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------
        
        repaymentTypeId = 2;
        
        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //Monthly
        
        paymentFrequencyId = Mc.PAY_FREQ_MONTHLY;
        interestCompoundingId = 0;

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(824.78, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly: 12
        //Monthly
        interestCompoundingId = 1;
        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(833.33, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //semi Monthly
        paymentFrequencyId = Mc.PAY_FREQ_SEMIMONTHLY;
        interestCompoundingId = 0;

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(411.97, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly: 12
        //semi Monthly
        interestCompoundingId = 1;
        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(416.23, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //Bi weekly
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        paymentFrequencyId = Mc.PAY_FREQ_BIWEEKLY;
        interestCompoundingId = 0;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(378.94, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //Bi weekly
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(379.2, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly: 12
        //Bi weekly
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        paymentFrequencyId = Mc.PAY_FREQ_BIWEEKLY;
        interestCompoundingId = 1;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(382.87, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly 12
        //Bi weekly
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(383.13, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //ACCELERATED_BIWEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        paymentFrequencyId = Mc.PAY_FREQ_ACCELERATED_BIWEEKLY;
        interestCompoundingId = 0;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(378.94, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //ACCELERATED_BIWEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(379.2, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly: 12
        //ACCELERATED_BIWEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        paymentFrequencyId = Mc.PAY_FREQ_BIWEEKLY;
        interestCompoundingId = 1;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(382.87, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly 12
        //ACCELERATED_BIWEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(383.13, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        paymentFrequencyId = Mc.PAY_FREQ_WEEKLY;
        interestCompoundingId = 0;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(189.38, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(189.51, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly: 12
        //WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        interestCompoundingId = 1;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(191.34, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly 12
        //WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(191.47, pandiAmount, 0);

        //-------------------------------------------------------------------------------------------

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //ACCELERATED_WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        paymentFrequencyId = Mc.PAY_FREQ_ACCELERATED_WEEKLY;
        interestCompoundingId = 0;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(189.38, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency Semi-annually: 2
        //ACCELERATED_WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(189.51, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly: 12
        //ACCELERATED_WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = Y

        interestCompoundingId = 1;
        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "Y");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(191.34, pandiAmount, 0);

        //repayment type IN 2 or 3
        //compound frequency monthly 12
        //ACCELERATED_WEEKLY
        //com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear = N

        overWriteProperty(institutionProfileId, pc, "com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");

        pandiAmount = DealCalcUtil.calcPandIFully(institutionProfileId, totalLoanAmount, paymentFrequencyId, netInterestRate, 
            amortizationMonths, repaymentTypeId, interestCompoundingId, rateCode);
        pandiAmount = Math.round(pandiAmount * 100d) / 100d; //rounding
        assertEquals(191.47, pandiAmount, 0);
    }
    
    /*@Test
    public void testCalcPandIFully3Year() throws Exception {
        
        int mtgProdId = 0;//TODO: depending on data in Db, if it doesn't work, you have to change this value
        String DUMMY_RATECODE = "TEST1";
        int paymentFrequencyId = Mc.PAY_FREQ_MONTHLY;
        
        SessionResourceKit srk = new SessionResourceKit();
        try{
            srk.beginTransaction();
            PropertiesCache pc = PropertiesCache.getInstance();
            //this method won't test all logics in calcPandIFully method.
            int institutionProfId = 1;

            srk.getExpressState().setDealInstitutionId(institutionProfId);
            Deal deal = new Deal();
            deal.setSessionResourceKit(srk);

            deal.setInstitutionProfileId(institutionProfId);
            deal.setDiscount(1);
            deal.setTotalLoanAmount(200000);
            deal.setRepaymentTypeId(0);
            deal.setAmortizationTerm(360);

            //set same rate for deal and 3year's one
            overWriteProperty(institutionProfId, pc, "com.basis100.calc.gdstds3yratecode", DUMMY_RATECODE);
            
            deal.setMtgProdId(mtgProdId);
            MtgProd mtgProd = new MtgProd(srk, null, mtgProdId);
            mtgProd.setUnderwriteAsTypeId(Mc.PRODUCT_TYPE_MORTGAGE);
            mtgProd.setInterestCompoundingId(0); //2:semi annualy
            mtgProd.ejbStore();
            
            PricingProfile pprofile = mtgProd.getPricingProfile();
            
            pprofile.setRateCode(DUMMY_RATECODE);
            pprofile.ejbStore();
            
            PricingRateInventory pri = new PricingRateInventory(srk);
            pri = pri.findMostRecentByRateCode(DUMMY_RATECODE);
            
            pri.setInternalRatePercentage(5.6);
            pri.ejbStore();
            
            
            
            ////test start
            //system property:com.basis100.calc.forcepandifor3year = Y
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC"
            //institution.threeYearRateCompareDiscount = Y

            overWriteProperty(institutionProfId, pc, "com.basis100.calc.forcepandifor3year", "Y");
            deal.setProductTypeId(Mc.PRODUCT_TYPE_SECURED_LOC);
            InstitutionProfile inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(true);
            inst.ejbStore();
            CalcEntityCache entityCache = new CalcEntityCache(null);
            

            double actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1118.091591, actual, 0.00001);
            
            //system property:com.basis100.calc.forcepandifor3year = Y
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC"
            //institution.threeYearRateCompareDiscount = N
            //
            inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(false);
            inst.ejbStore();
            entityCache = new CalcEntityCache(null);
            

            actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1232.450122, actual, 0.00001);
            
            //system property:com.basis100.calc.forcepandifor3year = Y
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC" = FALSE
            //institution.threeYearRateCompareDiscount = Y

            deal.setProductTypeId(Mc.PRODUCT_TYPE_MORTGAGE);
            inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(true);
            inst.ejbStore();
            entityCache = new CalcEntityCache(null);
            

            actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1020.099016, actual, 0.00001);
            
            //system property:com.basis100.calc.forcepandifor3year = Y
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC" = FALSE
            //institution.threeYearRateCompareDiscount = N
            //
            inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(false);
            inst.ejbStore();
            entityCache = new CalcEntityCache(null);
            

            actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1140.069692, actual, 0.00001);
            
            //system property:com.basis100.calc.forcepandifor3year = N
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC"
            //institution.threeYearRateCompareDiscount = Y

            overWriteProperty(institutionProfId, pc, "com.basis100.calc.forcepandifor3year", "N");
            deal.setProductTypeId(Mc.PRODUCT_TYPE_SECURED_LOC);
            inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(true);
            inst.ejbStore();
            entityCache = new CalcEntityCache(null);
            

            actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1020.099016, actual, 0.00001);
            
            //system property:com.basis100.calc.forcepandifor3year = N
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC"
            //institution.threeYearRateCompareDiscount = N
            //
            inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(false);
            inst.ejbStore();
            entityCache = new CalcEntityCache(null);
            

            actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1140.069692, actual, 0.00001);
            
            //system property:com.basis100.calc.forcepandifor3year = N
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC" = FALSE
            //institution.threeYearRateCompareDiscount = Y

            deal.setProductTypeId(Mc.PRODUCT_TYPE_MORTGAGE);
            inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(true);
            inst.ejbStore();
            entityCache = new CalcEntityCache(null);
            

            actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1020.099016, actual, 0.00001);
            
            //system property:com.basis100.calc.forcepandifor3year = N
            //deal.productTypeid = "LOC" or deal.mtgProd.underwriteAS="LOC" = FALSE
            //institution.threeYearRateCompareDiscount = N
            //
            inst = new InstitutionProfile(srk, institutionProfId);
            inst.setThreeYearRateCompareDiscount(false);
            inst.ejbStore();
            entityCache = new CalcEntityCache(null);
            

            actual = DealCalcUtil.calcPandIFully3Year(deal, paymentFrequencyId, entityCache);
            assertEquals(1140.069692, actual, 0.00001);
            
         
//        }catch(Throwable t){
//            t.printStackTrace();
//            fail(t.getMessage());
        }finally{
            srk.cleanTransaction();
            srk.freeResources();
        }
    }*/


    static void overWriteProperty(int institutionId, PropertiesCache pc, String name, String valueToOverWrite) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        String currentValue = pc.getProperty(institutionId, name);
        
        if(StringUtils.equals(valueToOverWrite, currentValue) == false){
            
            Field f = PropertiesCache.class.getDeclaredField("sysProp");
            f.setAccessible(true);
            SystemPropertyCache sysProp = (SystemPropertyCache)f.get(pc);
            if(sysProp != null){
                Field f2 = SystemPropertyCache.class.getDeclaredField("cache");
                f2.setAccessible(true);
                Cache cache = (Cache)f2.get(sysProp);

                String cachekey = SystemPropertyCacheUtil.generateCacheKey(
                    institutionId, name);

                Element element = new Element(cachekey,valueToOverWrite);
                cache.put(element);

            }
        }
    }

}
