package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Income;


public class CombinedTotalIncomeAmountTest extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private CombinedTotalIncomeAmount combinedTotalIncomeAmount;

	public CombinedTotalIncomeAmountTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CombinedTotalIncomeAmount.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		combinedTotalIncomeAmount = new CombinedTotalIncomeAmount();

	}


	@Test
	public void testDoCalc() throws Exception  {
		
			boolean status = false;
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");	   	   
			String id=(String)testDoCalc.getValue(0,"DEALID");
			int dealId=Integer.parseInt(id);
			String copy=(String)testDoCalc.getValue(0,"COPYID");
			int copyId=Integer.parseInt(copy);
			setEntityCacheAndSession(combinedTotalIncomeAmount);
			CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
			Deal deal=new Deal(srk,calcMonitor,dealId, copyId);		
			combinedTotalIncomeAmount.doCalc(deal);
			status=true;
			assertTrue(status);
		}
	

	public void testgetTarget()throws Exception  {
			boolean status = false;
			ITable testgetTarget = dataSetTest.getTable("testgetTarget");	   	   
			String id=(String)testgetTarget.getValue(0,"INCOMEID");
			int incomeId=Integer.parseInt(id);
			String copy=(String)testgetTarget.getValue(0,"COPYID");
			int copyId=Integer.parseInt(copy);
			setEntityCacheAndSession(combinedTotalIncomeAmount);
			CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
			Income income=new Income(srk, calcMonitor, incomeId, copyId);
			combinedTotalIncomeAmount.getTarget(income, calcMonitor);	
			status=true;
			assertTrue(status);
	
	}


}
	

			 

