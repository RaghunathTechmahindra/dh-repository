package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;

public class TotalPaymentAmountComponentLOCTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private TotalPaymentAmountComponentLOC totalPaymentAmountComponentLOC;
	boolean status = false;

	public TotalPaymentAmountComponentLOCTest1(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TotalPaymentAmountComponentLOC.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.totalPaymentAmountComponentLOC = new TotalPaymentAmountComponentLOC();
	}

	/*@Test
	public void testDoCalc() throws Exception {
		setEntityCacheAndSession(totalPaymentAmountComponentLOC);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "COMPONENTLOCID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		ComponentLOC componentLOC = new ComponentLOC(srk, calcMonitor, dealId, copyId);
		totalPaymentAmountComponentLOC.doCalc(componentLOC);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		setEntityCacheAndSession(totalPaymentAmountComponentLOC);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		//ComponentLOC
		ITable testExtractComponentLOC = dataSetTest.getTable("testGetTargetComponentLOC");
		String id = (String) testExtractComponentLOC.getValue(0, "COMPONENTLOCID");
		int componentLOCId = Integer.parseInt(id);
		String copy = (String) testExtractComponentLOC.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		ComponentLOC componentLOC = new ComponentLOC(srk, calcMonitor, componentLOCId, copyId);
		totalPaymentAmountComponentLOC.getTarget(componentLOC,calcMonitor);
		//Deal
		ITable testExtractIncome = dataSetTest.getTable("testGetTargetDeal");
		String id1 = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id1);
		String copy1 = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId1 = Integer.parseInt(copy1);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId1);
		totalPaymentAmountComponentLOC.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}*/
	
	@Test
	public void testTBR(){
	System.out.println("test");
	 }

}
