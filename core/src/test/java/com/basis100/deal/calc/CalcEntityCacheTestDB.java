package com.basis100.deal.calc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.InsureOnlyApplicant;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class CalcEntityCacheTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private CalcEntityCache calcEntityCache;
	private Deal deal;
	private DealEntity entity;
	private CalcMonitor dcm;
	private Collection<?> coll = null;
	private IEntityBeanPK pk;
	private Component component;
	private PricingProfile pricingProfile;
	private DocumentTracking documentTracking;
	private EscrowPayment escrowPayment;
	private DealFee dealFee;
	private InsureOnlyApplicant insurerApp;
	private DownPaymentSource downPaymentSource;
	private Property property;

	public CalcEntityCacheTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				CalcEntityCache.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		dcm = CalcMonitor.getMonitor(srk);
		this.calcEntityCache = new CalcEntityCache(dcm);
	}

	public void testGetBorrowers() throws Exception {
		// get input data from xml repository
		coll = new ArrayList();
		ITable testGetBorrowers = dataSetTest.getTable("testGetBorrowers");
		int institutionId = Integer.parseInt((String) testGetBorrowers
				.getValue(0, "INSTITUTIONPROFILEID"));
		int dealId = Integer.parseInt((String) testGetBorrowers.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testGetBorrowers.getValue(0,
				"COPYID"));

		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;

		srk.getExpressState().setDealInstitutionId(institutionId);
		coll = calcEntityCache.getBorrowers(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(dealId, deal.getDealId());
		}
	}

	public void testGetProperties() throws Exception {
		// get input data from xml repository
		coll = new ArrayList();
		ITable testGetProperties = dataSetTest.getTable("testGetProperties");
		int institutionId = Integer.parseInt((String) testGetProperties
				.getValue(0, "INSTITUTIONPROFILEID"));
		int dealId = Integer.parseInt((String) testGetProperties.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testGetProperties.getValue(0,
				"COPYID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		srk.getExpressState().setDealInstitutionId(institutionId);
		coll = calcEntityCache.getProperties(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(dealId, deal.getDealId());
		}
	}

	public void testGetLiabilities() throws Exception {
		// get input data from xml repository
		coll = new ArrayList();
		ITable testGetLiabilities = dataSetTest.getTable("testGetLiabilities");
		int dealId = Integer.parseInt((String) testGetLiabilities.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testGetLiabilities.getValue(0,
				"COPYID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getLiabilities(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(dealId, deal.getDealId());
		}
	}

	public void testGetAssets() throws Exception {
		// get input data from xml repository
		coll = new ArrayList();
		ITable testGetAssets = dataSetTest.getTable("testGetAssets");
		int dealId = Integer.parseInt((String) testGetAssets.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testGetAssets.getValue(0,
				"COPYID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getAssets(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(dealId, deal.getDealId());
		}
	}

	public void testGetIncomes() throws Exception {
		// get input data from xml repository
		coll = new ArrayList();
		ITable testGetIncomes = dataSetTest.getTable("testGetIncomes");
		int dealId = Integer.parseInt((String) testGetIncomes.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testGetIncomes.getValue(0,
				"COPYID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getIncomes(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(dealId, deal.getDealId());
		}
	}

	public void testGetPropertyExpenses() throws Exception {
		// get input data from xml repository
		ITable testGetPropertyExpenses = dataSetTest
				.getTable("testGetPropertyExpenses");
		int dealId = Integer.parseInt((String) testGetPropertyExpenses
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testGetPropertyExpenses
				.getValue(0, "COPYID"));
		int PropertyId = Integer.parseInt((String) testGetPropertyExpenses
				.getValue(0, "PROPERTYID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		property = new Property(srk, dcm, PropertyId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getPropertyExpenses(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(PropertyId, property.getPropertyId());
		}
	}

	public void testGetDownPaymentSources() throws Exception {
		// get input data from xml repository
		ITable testGetDownPaymentSources = dataSetTest
				.getTable("testGetDownPaymentSources");
		int dealId = Integer.parseInt((String) testGetDownPaymentSources
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testGetDownPaymentSources
				.getValue(0, "COPYID"));
		int downPaymentSourceId = Integer
				.parseInt((String) testGetDownPaymentSources.getValue(0,
						"DOWNPAYMENTSOURCEID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		downPaymentSource = new DownPaymentSource(srk, dcm,
				downPaymentSourceId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getDownPaymentSources(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(downPaymentSourceId,
					downPaymentSource.getDownPaymentSourceId());
		}
	}

	public void testGetInsureOnlyApplicants() throws Exception {
		// get input data from xml repository
		ITable testGetInsureOnlyApplicants = dataSetTest
				.getTable("testGetInsureOnlyApplicants");
		int dealId = Integer.parseInt((String) testGetInsureOnlyApplicants
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testGetInsureOnlyApplicants
				.getValue(0, "COPYID"));
		int insureOnlyAppId = Integer
				.parseInt((String) testGetInsureOnlyApplicants.getValue(0,
						"INSUREONLYAPPLICANTID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		insurerApp = new InsureOnlyApplicant(srk, dcm, insureOnlyAppId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getInsureOnlyApplicants(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(insureOnlyAppId, insurerApp.getInsureOnlyApplicantId());
		}
	}

	public void testGetDealFees() throws Exception {
		// get input data from xml repository
		ITable testGetDealFees = dataSetTest.getTable("testGetDealFees");
		int dealId = Integer.parseInt((String) testGetDealFees.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testGetDealFees.getValue(0,
				"COPYID"));
		int dealFeeId = Integer.parseInt((String) testGetDealFees.getValue(0,
				"DEALFEEID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		dealFee = new DealFee(srk, dcm, dealFeeId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getDealFees(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(dealFeeId, dealFee.getDealFeeId());
		}
	}

	public void testGetEscrowPayments() throws Exception {
		// get input data from xml repository
		ITable testGetEscrowPayments = dataSetTest
				.getTable("testGetEscrowPayments");
		int dealId = Integer.parseInt((String) testGetEscrowPayments.getValue(
				0, "DEALID"));
		int copyId = Integer.parseInt((String) testGetEscrowPayments.getValue(
				0, "COPYID"));
		int escrowPaymentId = Integer.parseInt((String) testGetEscrowPayments
				.getValue(0, "ESCROWPAYMENTID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		escrowPayment = new EscrowPayment(srk, dcm, escrowPaymentId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getEscrowPayments(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(escrowPaymentId, escrowPayment.getEscrowPaymentId());
		}
	}

	public void testGetCommitmentDocumentTracking() throws Exception {
		// get input data from xml repository
		ITable commitmentDocumentTracking = dataSetTest
				.getTable("testGetCommitmentDocumentTracking");
		int dealId = Integer.parseInt((String) commitmentDocumentTracking
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) commitmentDocumentTracking
				.getValue(0, "COPYID"));
		int documentTrackingId = Integer
				.parseInt((String) commitmentDocumentTracking.getValue(0,
						"DOCUMENTTRACKINGID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		documentTracking = new DocumentTracking(srk, documentTrackingId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getCommitmentDocumentTracking(entity);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(documentTrackingId,
					documentTracking.getDocumentTrackingId());
		}
	}

	public void testGetPricingProfiles() throws Exception {
		// get input data from xml repository
		ITable testGetPricingProfiles = dataSetTest
				.getTable("testGetPricingProfiles");
		int pricingProfileId = Integer.parseInt((String) testGetPricingProfiles
				.getValue(0, "PRICINGPROFILEID"));
		String rateCode = (String) testGetPricingProfiles.getValue(0,
				"RATECODE");
		pricingProfile = new PricingProfile(srk, pricingProfileId);

		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getPricingProfiles(srk, rateCode);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(pricingProfileId, pricingProfile.getPricingProfileId());
		}
	}

	public void testGetComponents() throws Exception {
		// get input data from xml repository
		ITable testGetComponents = dataSetTest.getTable("testGetComponents");
		int dealId = Integer.parseInt((String) testGetComponents.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testGetComponents.getValue(0,
				"COPYID"));
		int componentId = Integer.parseInt((String) testGetComponents.getValue(
				0, "COMPONENTID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		component = new Component(srk, null, componentId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		coll = calcEntityCache.getComponents(entity, srk);
		if (coll != null) {
			assertTrue(coll.size() > 0);
			assertEquals(componentId, component.getComponentId());
		}
	}

	public void testFind() throws Exception {
		// get input data from xml repository
		ITable testFind = dataSetTest.getTable("testFind");
		int dealId = Integer.parseInt((String) testFind.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testFind.getValue(0, "COPYID"));

		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		pk = new DealPK(dealId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		entity = calcEntityCache.find(srk, pk);
		assertEquals(pk, entity.getPk());
	}

}