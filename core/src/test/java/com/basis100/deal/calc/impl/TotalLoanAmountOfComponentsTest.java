/*
 * @(#)TotalLoanAmountOfComponentsTest.java July 17, 2008 <story id : XS 11.16>
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: TotalLoanAmountOfComponentsTest
 * </p>
 * <p>
 * Description: Unit test class for total of loan amount for all eligible
 * components
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.16 Initial version
 */
public class TotalLoanAmountOfComponentsTest extends BaseCalcTest
{

    private Deal dealFromExcel = null;

    private Component componentFromExcelForLOC = null;

    private Component componentFromExcelForLoan = null;

    private Component componentFromExcelForMtg = null;

    private Component componentFromExcelForOverdraft = null;

    private Component componentFromExcelForCreditCard = null;

    private Component componentFromDBForLOC = null;

    private Component componentFromDBForLoan = null;

    private Component componentFromDBForMtg = null;

    private Component componentFromDBForOverdraft = null;

    private Component componentFromDBForCreditCard = null;

    private ComponentMortgage componentMortgageFromExcel = null;

    private ComponentSummary componentSummaryFromExcel = null;

    private ComponentLOC componentLOCFromExcel = null;

    private ComponentLoan componentLoanFromExcel = null;

    private ComponentCreditCard componentCreditCardFromExcel = null;

    private ComponentOverdraft componentOverdraftFromExcel = null;

    private ComponentSummary componentSummaryFromDB = null;

    private ComponentLOC componentLOCFromDB = null;

    private ComponentLoan componentLoanFromDB = null;

    private ComponentCreditCard componentCreditCardFromDB = null;

    private ComponentOverdraft componentOverdraftFromDB = null;

    private ComponentMortgage componentMortgageFromDB = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial version
     */
    public TotalLoanAmountOfComponentsTest()
    {
        super("com.basis100.deal.calc.impl.TotalLoanAmountOfComponents");

        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");

        componentFromExcelForLOC = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentFromExcelForLoan = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentFromExcelForMtg = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentFromExcelForOverdraft = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentFromExcelForCreditCard = (Component) loadTestDataObject("com.basis100.deal.entity.Component");

        componentMortgageFromExcel = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");
        componentOverdraftFromExcel = (ComponentOverdraft) loadTestDataObject("com.basis100.deal.entity.ComponentOverdraft");
        componentLoanFromExcel = (ComponentLoan) loadTestDataObject("com.basis100.deal.entity.ComponentLoan");
        componentLOCFromExcel = (ComponentLOC) loadTestDataObject("com.basis100.deal.entity.ComponentLOC");
        componentCreditCardFromExcel = (ComponentCreditCard) loadTestDataObject("com.basis100.deal.entity.ComponentCreditCard");
        componentSummaryFromExcel = (ComponentSummary) loadTestDataObject("com.basis100.deal.entity.ComponentSummary");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setMtgProdId(dealFromExcel.getMtgProdId());

        deal.ejbStore();

        setCopyId(deal.getCopyId());

        // Component Object for Loan
        componentFromDBForLoan = (Component) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.Component");
        componentFromDBForLoan.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcelForLoan.getComponentTypeId(),
                componentFromExcelForLoan.getMtgProdId());
        componentFromDBForLoan.setPricingRateInventoryId(componentFromExcelForLoan.getPricingRateInventoryId());
        componentFromDBForLoan.setRepaymentTypeId(componentFromExcelForLoan.getRepaymentTypeId());
        componentFromDBForLoan.ejbStore();

        // Create Loan Object
        componentLoanFromDB = (ComponentLoan) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentLoan");

        componentLoanFromDB.create(componentFromDBForLoan.getComponentId(),
                componentFromDBForLoan.getCopyId());
        // Set Loan amount
        componentLoanFromDB.setLoanAmount(componentLoanFromExcel
                .getLoanAmount());
        componentLoanFromDB.ejbStore();

        // Component Object for Mortgage
        componentFromDBForMtg = (Component) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.Component");
        componentFromDBForMtg.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcelForMtg.getComponentTypeId(),
                componentFromExcelForMtg.getMtgProdId());
        componentFromDBForMtg.setPricingRateInventoryId(componentFromExcelForMtg.getPricingRateInventoryId());
        componentFromDBForMtg.setRepaymentTypeId(componentFromExcelForMtg.getRepaymentTypeId());
        componentFromDBForMtg.ejbStore();

        // Create Mortgage Object
        componentMortgageFromDB = (ComponentMortgage) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentMortgage");
        componentMortgageFromDB.create(componentFromDBForMtg.getComponentId(),
                componentFromDBForMtg.getCopyId());
        // Set total mortgage amount
        componentMortgageFromDB
                .setTotalMortgageAmount(componentMortgageFromExcel
                        .getTotalMortgageAmount());
        componentMortgageFromDB.ejbStore();

        // Component Object for Overdraft
        componentFromDBForOverdraft = (Component) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.Component");
        componentFromDBForOverdraft.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcelForOverdraft.getComponentTypeId(),
                componentFromExcelForOverdraft.getMtgProdId());
        componentFromDBForOverdraft.setPricingRateInventoryId(componentFromExcelForOverdraft.getPricingRateInventoryId());
        componentFromDBForOverdraft.setRepaymentTypeId(componentFromExcelForOverdraft.getRepaymentTypeId());
        componentFromDBForOverdraft.ejbStore();

        // Create ComponentOverdraft Object
        componentOverdraftFromDB = (ComponentOverdraft) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentOverdraft");

        componentOverdraftFromDB.create(componentFromDBForOverdraft
                .getComponentId(), componentFromDBForOverdraft.getCopyId());
        // Set Component Overdraft Amount
        componentOverdraftFromDB.setOverDraftAmount(componentOverdraftFromExcel
                .getOverDraftAmount());
        componentOverdraftFromDB.ejbStore();

        // Component Object for CreditCard
        componentFromDBForCreditCard = (Component) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.Component");
        componentFromDBForCreditCard.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcelForCreditCard.getComponentTypeId(),
                componentFromExcelForCreditCard.getMtgProdId());
        
        componentFromDBForCreditCard.setPricingRateInventoryId(componentFromExcelForCreditCard.getPricingRateInventoryId());
        componentFromDBForCreditCard.setRepaymentTypeId(componentFromExcelForCreditCard.getRepaymentTypeId());
        componentFromDBForCreditCard.ejbStore();

        // Create ComponentCreditCard Object
        componentCreditCardFromDB = (ComponentCreditCard) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentCreditCard");

        componentCreditCardFromDB.create(componentFromDBForCreditCard
                .getComponentId(), componentFromDBForCreditCard.getCopyId());
        // Set Credit Card Amount
        componentCreditCardFromDB
                .setCreditCardAmount(componentCreditCardFromExcel
                        .getCreditCardAmount());
        componentCreditCardFromDB.ejbStore();

        // Component Object for LOC
        componentFromDBForLOC = (Component) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.Component");
        componentFromDBForLOC.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcelForLOC.getComponentTypeId(),
                componentFromExcelForLOC.getMtgProdId());
        
        componentFromDBForLOC.setPricingRateInventoryId(componentFromExcelForLOC.getPricingRateInventoryId());
        componentFromDBForLOC.setRepaymentTypeId(componentFromExcelForLOC.getRepaymentTypeId());
        componentFromDBForLOC.ejbStore();

        // Create LOC Object
        componentLOCFromDB = (ComponentLOC) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentLOC");

        componentLOCFromDB.create(componentFromDBForLOC.getComponentId(),
                componentFromDBForLOC.getCopyId());
        // Set LOC amount
        componentLOCFromDB.setLocAmount(componentLOCFromExcel.getLocAmount());
        componentLOCFromDB.ejbStore();

        // Create ComponentSummary Object
        componentSummaryFromDB = (ComponentSummary) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentSummary");

        componentSummaryFromDB.create(deal.getDealId(), deal.getCopyId());
        // Set Component Summary Amount
        componentSummaryFromDB.setTotalAmount(componentSummaryFromExcel
                .getTotalAmount());
        componentSummaryFromDB.ejbStore();

    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial version
     * @throws Exception
     */
    @Override
    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            setEntityCacheAndSession();
            getDealCalc().doCalc(componentSummaryFromDB);

            assertEquals(componentSummaryFromExcel.getTotalAmount(),
                    componentSummaryFromDB.getTotalAmount());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        finally
        {
            tearDownTestData();
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: This method removes test data from DB
     * </p>
     * @throws Exception
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial version
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }

            if (componentFromDBForLOC != null)
            {
                componentFromDBForLOC.ejbRemove(false);
            }
            if (componentFromDBForLoan != null)
            {
                componentFromDBForLoan.ejbRemove(false);
            }
            if (componentFromDBForMtg != null)
            {
                componentFromDBForMtg.ejbRemove(false);
            }
            if (componentFromDBForOverdraft != null)
            {
                componentFromDBForOverdraft.ejbRemove(false);
            }
            if (componentFromDBForCreditCard != null)
            {
                componentFromDBForCreditCard.ejbRemove(false);
            }

            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial version
     */
    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(TotalLoanAmountOfComponentsTest.class);
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getComponentMortgageFromExcel()
    {
        return componentMortgageFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param compMtgFromExcel -
     *            Component Mortgage data object
     */
    public void setComponentMortgageFromExcel(ComponentMortgage compMtgFromExcel)
    {
        this.componentMortgageFromExcel = compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentCreditCard test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentCreditCard - ComponentCreditCard Entity object
     */
    public ComponentCreditCard getComponentCreditCardFromExcel()
    {
        return componentCreditCardFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentCreditCard data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param componentCreditCardFromExcel -
     *            Component data object
     */
    public void setComponentCreditCardFromExcel(
            ComponentCreditCard componentCreditCardFromExcel)
    {
        this.componentCreditCardFromExcel = componentCreditCardFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLoan test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentLoan - ComponentLoan Entity object
     */
    public ComponentLoan getComponentLoanFromExcel()
    {
        return componentLoanFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLoan data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param componentLoanFromExcel -
     *            ComponentLoan data object
     */
    public void setComponentLoanFromExcel(ComponentLoan componentLoanFromExcel)
    {
        this.componentLoanFromExcel = componentLoanFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLOC test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentLOC - ComponentLOC Entity object
     */
    public ComponentLOC getComponentLOCFromExcel()
    {
        return componentLOCFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLOC data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param componentLOCFromExcel -
     *            ComponentLOC data object
     */
    public void setComponentLOCFromExcel(ComponentLOC componentLOCFromExcel)
    {
        this.componentLOCFromExcel = componentLOCFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentOverdraft test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentOverdraft - ComponentOverdraft Entity object
     */
    public ComponentOverdraft getComponentOverdraftFromExcel()
    {
        return componentOverdraftFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentOverdraft data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param ComponentOverdraft -
     *            ComponentOverdraft data object
     */
    public void setComponentOverdraftFromExcel(
            ComponentOverdraft componentOverdraftFromExcel)
    {
        this.componentOverdraftFromExcel = componentOverdraftFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentSummary test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentSummary - ComponentSummary Entity object
     */
    public ComponentSummary getComponentSummaryFromExcel()
    {
        return componentSummaryFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentSummary data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param componentSummaryFromExcel -
     *            ComponentSummary data object
     */
    public void setComponentSummaryFromExcel(
            ComponentSummary componentSummaryFromExcel)
    {
        this.componentSummaryFromExcel = componentSummaryFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity for CreditCard
     * component
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcelForCreditCard()
    {
        return componentFromExcelForCreditCard;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial Version
     * @param componentFromExcelForCreditCard -
     *            Component data object
     */
    public void setComponentFromExcelForCreditCard(
            Component componentFromExcelForCreditCard)
    {
        this.componentFromExcelForCreditCard = componentFromExcelForCreditCard;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity for Loan component
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcelForLoan()
    {
        return componentFromExcelForLoan;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial Version
     * @param componentFromExcelForLoan -
     *            Component data object
     */
    public void setComponentFromExcelForLoan(Component componentFromExcelForLoan)
    {
        this.componentFromExcelForLoan = componentFromExcelForLoan;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity for LOC component
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcelForLOC()
    {
        return componentFromExcelForLOC;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial Version
     * @param componentFromExcelForLOC -
     *            Component data object
     */
    public void setComponentFromExcelForLOC(Component componentFromExcelForLOC)
    {
        this.componentFromExcelForLOC = componentFromExcelForLOC;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity for Mortgage
     * component
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcelForMtg()
    {
        return componentFromExcelForMtg;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial Version
     * @param componentFromExcelForMtg -
     *            Component data object
     */
    public void setComponentFromExcelForMtg(Component componentFromExcelForMtg)
    {
        this.componentFromExcelForMtg = componentFromExcelForMtg;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity for Overdraft
     * component
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcelForOverdraft()
    {
        return componentFromExcelForOverdraft;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial Version
     * @param componentFromExcelForOverdraft -
     *            Component data object
     */
    public void setComponentFromExcelForOverdraft(
            Component componentFromExcelForOverdraft)
    {
        this.componentFromExcelForOverdraft = componentFromExcelForOverdraft;
    }

}
