package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;

public class InterestAdjustmentDateLoanComponentTest1 extends FXDBTestCase {


	private IDataSet dataSetTest;
	private InterestAdjustmentDateLoanComponent interestAdjustmentDateLoanComponent;

	public InterestAdjustmentDateLoanComponentTest1(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InterestAdjustmentDateLoanComponent.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interestAdjustmentDateLoanComponent = new InterestAdjustmentDateLoanComponent();

	}

	@Test
	public void testDoCalc()throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateLoanComponent);
		ComponentLoan componentLoan=new ComponentLoan(srk, componentId, copyId);
		interestAdjustmentDateLoanComponent.doCalc(componentLoan);
		status = true;
		assertTrue(status);
	}

	public void testgetTargetForComponentLoan()throws Exception {
		boolean status = false;
		ITable testgetTargetForComponentLoan = dataSetTest.getTable("testgetTargetForComponentLoan");
		String id = (String) testgetTargetForComponentLoan.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponentLoan.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateLoanComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLoan componentLoan=new ComponentLoan(srk, componentId, copyId);
		interestAdjustmentDateLoanComponent.getTarget(componentLoan,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForDeal()throws Exception {
		boolean status = false;
		ITable testgetTargetForDeal = dataSetTest.getTable("testgetTargetForDeal");
		String id = (String) testgetTargetForDeal.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTargetForDeal.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateLoanComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor, dealId, copyId);
		interestAdjustmentDateLoanComponent.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
