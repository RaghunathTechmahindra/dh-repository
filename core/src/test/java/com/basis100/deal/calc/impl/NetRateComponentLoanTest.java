/*
 * @(#)NetRateComponentLoanTest.java July 02, 2008 Copyright (C) 2008 Filogix
 * Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: NetRateComponentLoanTest
 * </p>
 * <p>
 * Description: Unit test class for Net Rate Loan Component calculation for the
 * Component Loan.
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.14 02-July-2008 Initial version
 */
public class NetRateComponentLoanTest extends BaseCalcTest
{

    private Component componentFromExcel = null;

    private ComponentLoan compLoanFromExcel = null;

    private Component componentDB = null;

    private ComponentLoan compLoanDB = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     */
    public NetRateComponentLoanTest()
    {
        super("com.basis100.deal.calc.impl.NetRateComponentLoan");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compLoanFromExcel = (ComponentLoan) loadTestDataObject("com.basis100.deal.entity.ComponentLoan");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();
        setCopyId(deal.getCopyId());
        // Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.setPostedRate(componentFromExcel.getPostedRate());
        componentDB.ejbStore();

        // Create Loan Object
        compLoanDB = (ComponentLoan) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLoan");

        compLoanDB.create(componentDB.getComponentId(),componentDB.getCopyId());
        compLoanDB.setPaymentFrequencyId(compLoanFromExcel.getPaymentFrequencyId());
        compLoanDB.setDiscount(compLoanFromExcel.getDiscount());
        compLoanDB.setPremium(compLoanFromExcel.getPremium());
        compLoanDB.ejbStore();
    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(compLoanDB);
            // Assert the data
            assertEquals(compLoanDB.getNetInterestRate(), compLoanFromExcel
                    .getNetInterestRate(), 0.00);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {
            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     */
    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(NetRateComponentLoanTest.class);
    }

    /**
     * <p>
     * Description: Returns the ComponentLoan test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 12-Jun-2008 Initial Version
     * @return ComponentLoan - ComponentLoan Entity object
     */
    public ComponentLoan getCompLoanFromExcel()
    {
        return compLoanFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLoan data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 12-Jun-2008 Initial Version
     * @param compLoanFromExcel -
     *            Component Mortgage data object
     */
    public void setCompLoanFromExcel(ComponentLoan compLoanFromExcel)
    {
        this.compLoanFromExcel = compLoanFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 12-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 12-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

}
