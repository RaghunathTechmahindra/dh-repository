package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;

public class MortgageTotalAmountTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MortgageTotalAmount mortgageTotalAmount;

	public MortgageTotalAmountTest1(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MortgageTotalAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.mortgageTotalAmount = new MortgageTotalAmount();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(mortgageTotalAmount);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "COMPONENTID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		ComponentMortgage componentMortgage = new ComponentMortgage(srk, calcMonitor, dealId, copyId);
		mortgageTotalAmount.doCalc(componentMortgage);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(mortgageTotalAmount);
		// Deal
		ITable testExtractIncome = dataSetTest.getTable("testGetTargetDeal");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		mortgageTotalAmount.getTarget(deal, calcMonitor);
		// ComponentMortgage
		ITable testExtractIncome1 = dataSetTest.getTable("testDoCalc");
		String id1 = (String) testExtractIncome1.getValue(0, "COMPONENTID");
		int dealId1 = Integer.parseInt(id1);
		String copy1 = (String) testExtractIncome1.getValue(0, "COPYID");
		int copyId1 = Integer.parseInt(copy1);
		ComponentMortgage componentMortgage = new ComponentMortgage(srk, calcMonitor, dealId1, copyId1);
		mortgageTotalAmount.getTarget(componentMortgage, calcMonitor);
		status = true;
		assertTrue(status);
	}
}
