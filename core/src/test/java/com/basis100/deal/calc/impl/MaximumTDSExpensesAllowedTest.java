package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Income;

public class MaximumTDSExpensesAllowedTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MaximumTDSExpensesAllowed maximumTDSExpensesAllowed;

	public MaximumTDSExpensesAllowedTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MaximumTDSExpensesAllowed.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.maximumTDSExpensesAllowed = new MaximumTDSExpensesAllowed();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(maximumTDSExpensesAllowed);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		maximumTDSExpensesAllowed.doCalc(deal);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(maximumTDSExpensesAllowed);
		// Income
		ITable testExtractIncome = dataSetTest.getTable("testGetTargetIncome");
		String id1 = (String) testExtractIncome.getValue(0, "INCOMEID");
		int incomeId = Integer.parseInt(id1);
		String copy1 = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId1 = Integer.parseInt(copy1);
		Income income = new Income(srk, calcMonitor, incomeId, copyId1);
		maximumTDSExpensesAllowed.getTarget(income, calcMonitor);
		// Deal
		
		ITable testExtractDeal = dataSetTest.getTable("testGetTargetDeal");
		String id = (String) testExtractDeal.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractDeal.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		maximumTDSExpensesAllowed.getTarget(deal, calcMonitor);
		status = true;
		assertTrue(status);
	}

}
