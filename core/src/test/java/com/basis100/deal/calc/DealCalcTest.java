package com.basis100.deal.calc;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.basis100.deal.docprep.extract.doctag.VIPText;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.resources.SessionResourceKit;

public class DealCalcTest {
	
	// the session kit.
    private SessionResourceKit _srk;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		_srk = new SessionResourceKit("DealCalcTest");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetMaximumCalculatedValue() throws Exception {
		MockCalc m = new MockCalc();
		assertEquals(999.99, m.getMaximumCalculatedValue(), 0.0001);
	}

	@Test
	public void testValidateDouble() throws ParamTypeException {
		MockCalc m = new MockCalc();
		
		assertEquals(998.99d, m.validate(998.99d), 0.0001);
		assertEquals(999.99d, m.validate(999.99d), 0.0001);
		assertEquals(999.99d, m.validate(1000.0d), 0.0001);
		assertEquals(-998.99d, m.validate(-998.99d), 0.0001);
		assertEquals(-999.99d, m.validate(-999.99d), 0.0001);
		assertEquals(-999.99d, m.validate(-1000d), 0.0001);
	}

	@Test
	public void testValidateLong() throws ParamTypeException {
		MockCalc m = new MockCalc();
		assertEquals(999L, m.validate(999L));
		assertEquals(999L, m.validate(1000L));
		assertEquals(999, m.validate(2147483648L));
		assertEquals(-999L, m.validate(-999L));
		assertEquals(-999L, m.validate(-1000L));
		assertEquals(-999, m.validate(-2147483649L));
	}

	@Test
	public void testValidateInt() throws ParamTypeException {
		MockCalc m = new MockCalc();
		assertEquals(999, m.validate(999));
		assertEquals(999, m.validate(1000));
		assertEquals(-999, m.validate(-999));
		assertEquals(-999, m.validate(-1000));
	}
	
	@Test
	public void testAddTargetParam() throws ParamTypeException 
	{
		MockCalc m = new MockCalc();
		m.targetParam = new ArrayList();
		
		m.addTargetParam(new CalcParam(ClassId.BORROWER, "GDS", 99.99));
		
		assert m.getTargetParams() != null;
		assert m.getTargetParams().size() >= 1;
	}
	
	@Test
	public void testAddInputParam() throws ParamTypeException 
	{
		MockCalc m = new MockCalc();
		
		m.addInputParam(new CalcParam(ClassId.DEAL, "lifepremium"));
		
		assert m.getInputParams() != null;
		assert m.getInputParams().size() >= 1;
	}
	
	@Test
	public void testReset() throws ParamTypeException 
	{
		MockCalc m = new MockCalc();
		
		m.reset();
		
		assert m.getInputs()!= null;
		assert m.getInputs().size() == 0;
		
		assert m.getTargets()!= null;
		assert m.getTargets().size() == 0;
	}
	
	@Test
	public void testHasInputClassId() throws ParamTypeException 
	{
		MockCalc m = new MockCalc();
		
		assert m.hasInputClassId(1);
		assert !m.hasInputClassId(2);
	}
	
	@Test
	public void testIsInputTo() throws ParamTypeException 
	{
		MockCalc m = new MockCalc();
		m.targetParam = new ArrayList();
		m.addTargetParam(new CalcParam(ClassId.DEAL, "repaymentTypeId", 999.99));
		
		assert m.isInputTo(new MockCalc());
		
		MockCalc m1 = new MockCalc();
		
		assert !m1.isInputTo(new MockCalc());
	}
	
	/*@Test
	public void testGetDoCalcErrorDetail() throws Exception 
	{
		MockCalc m = new MockCalc();
		
		Deal deal=new Deal(_srk, null,935047, 4);	    
	    DealEntity target=(DealEntity)deal;
		
		assert m.getDoCalcErrorDetail(target).contains("935047");
		
		MockCalc m1 = new MockCalc();
		
		VIPText vipText = new VIPText();
		String error = m1.getDoCalcErrorDetail(vipText);
		assert error.contains("Not DealEntity");
	}
	

	@Test
	public void testAttachInput() throws Exception 
	{
		MockCalc m = new MockCalc();
		
		Deal deal=new Deal(_srk, null,935047, 4);	    
	    DealEntity entity=(DealEntity)deal;
		
		m.attachInput(entity);
		assert m.getInputs() != null;
		assert m.getInputs().size() > 0;
		
		MockCalc m1 = new MockCalc();
		
		VIPText vipText = new VIPText();
		m1.attachInput(vipText);
		assert m1.getInputs() != null;
		assert m1.getInputs().size() == 0;
	}
	*/
	
}

class MockCalc extends DealCalc{

	public MockCalc() throws ParamTypeException{
	    
		targetParam = new ArrayList();
	    inputParam = new ArrayList();
	    inputClassTypes = new HashSet();
	    
	    addInputParam(new CalcParam(ClassId.DEAL, "repaymentTypeId"));
	    addTargetParam(new CalcParam(ClassId.BORROWER, "TDS", 999.99));

	}
	
	@Override
	public void doCalc(Object target) throws Exception {
	}

	@Override
	public void getTarget(Object input, CalcMonitor dcm)
			throws TargetNotFoundException {
	}
	
}