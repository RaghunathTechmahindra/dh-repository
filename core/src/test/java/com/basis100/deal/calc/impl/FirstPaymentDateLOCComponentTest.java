/*
 * @(#)FirstPaymentDateLOCComponentTest.java Jun 24, 2008 Copyright (C)
 * 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: FirstPaymentDateLOCComponentTest
 * </p>
 * <p>
 * Description: Unit test class for First Payment Date calculation for the
 * Component LOC.
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.10 25-Jun-2008 Initial version
 */
public class FirstPaymentDateLOCComponentTest extends BaseCalcTest
{

    MtgProd mtgProdDB = null;

    Component componentDB = null;

    ComponentLOC componentLOCDB = null;

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentLOC componentLOCFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial version
     */
    public FirstPaymentDateLOCComponentTest()
    {
        super("com.basis100.deal.calc.impl.FirstPaymentDateLOCComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentLOCFromExcel = (ComponentLOC) loadTestDataObject("com.basis100.deal.entity.ComponentLOC");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setEstimatedClosingDate(dealFromExcel.getEstimatedClosingDate());
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // Create Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel
                        .getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        // Create Component LOC Object
        componentLOCDB = (ComponentLOC) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentLOC");

        componentLOCDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        componentLOCDB.setPaymentFrequencyId(componentLOCFromExcel.getPaymentFrequencyId());
        componentLOCDB.setPrePaymentOptionsId(componentLOCFromExcel.getPrePaymentOptionsId());
        componentLOCDB.setPrivilegePaymentId(componentLOCFromExcel.getPrivilegePaymentId());

        componentLOCDB.setInterestAdjustmentDate(componentLOCFromExcel
                .getInterestAdjustmentDate());

        componentLOCDB.ejbStore();
    }

    /**
     * <p>
     * Description :This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(componentLOCDB);
            // Assert the Data
            assertEquals(componentLOCDB.getFirstPaymentDate(),
                    componentLOCFromExcel.getFirstPaymentDate());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            tearDownTestData();
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }

    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(FirstPaymentDateLOCComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLOC test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @return ComponentLOC - ComponentLOC Entity object
     */
    public ComponentLOC getComponentLOCFromExcel()
    {
        return componentLOCFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLOC data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.10 25-Jun-2008 Initial Version
     * @param componentLOCFromExcel -
     *            Component data object
     */
    public void setComponentLOCFromExcel(ComponentLOC componentLOCFromExcel)
    {
        this.componentLOCFromExcel = componentLOCFromExcel;
    }
}
