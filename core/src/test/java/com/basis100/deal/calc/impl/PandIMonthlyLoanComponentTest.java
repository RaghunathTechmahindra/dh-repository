package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: PandIMonthlyLoanComponentTest
 * </p>
 * <p>
 * Description: Unit test class for calculating Principal and Interest portion
 * of payment calculated using the monthly payment frequency values for Loan
 * component
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.13 03-Jul-2008 Initial version
 */
public class PandIMonthlyLoanComponentTest extends BaseCalcTest
{

    Component componentDB = null;

    Component componentDB1 = null;

    ComponentLoan compLoanDB = null;

    ComponentLoan compLoanDB1 = null;

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentLoan compLoanFromExcel = null;

    private Component componentFromExcel1 = null;

    private ComponentLoan compLoanFromExcel1 = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial version
     */
    public PandIMonthlyLoanComponentTest()
    {
        super("com.basis100.deal.calc.impl.PandIMonthlyLoanComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compLoanFromExcel = (ComponentLoan) loadTestDataObject("com.basis100.deal.entity.ComponentLoan");

        componentFromExcel1 = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compLoanFromExcel1 = (ComponentLoan) loadTestDataObject("com.basis100.deal.entity.ComponentLoan");

    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());

        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setTaxPayorId(dealFromExcel.getTaxPayorId());
        deal.ejbStore();

        setCopyId(deal.getCopyId());

        // CReate Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        // Create Component Loan Object
        compLoanDB = (ComponentLoan) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLoan");
        compLoanDB.create(componentDB.getComponentId(),componentDB.getCopyId());
        compLoanDB.setPaymentFrequencyId(compLoanFromExcel.getPaymentFrequencyId());
        compLoanDB.setLoanAmount(compLoanFromExcel.getLoanAmount());
        compLoanDB.setNetInterestRate(compLoanFromExcel.getNetInterestRate());
        compLoanDB.setActualPaymentTerm(compLoanFromExcel
                .getActualPaymentTerm());

        compLoanDB.ejbStore();

        // CReate Component Object with different repayment type
        componentDB1 = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB1.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel1.getComponentTypeId(), componentFromExcel1.getMtgProdId());
        componentDB1.setPricingRateInventoryId(componentFromExcel1.getPricingRateInventoryId());
        componentDB1.setRepaymentTypeId(componentFromExcel1.getRepaymentTypeId());
        componentDB1.ejbStore();
        // Create Component Loan Object using componentDB1 componentId and
        // CopyId
        compLoanDB1 = (ComponentLoan) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLoan");
        compLoanDB1.create(componentDB1.getComponentId(), componentDB1.getCopyId());
        compLoanDB1.setPaymentFrequencyId(compLoanFromExcel1.getPaymentFrequencyId());
        compLoanDB1.setLoanAmount(compLoanFromExcel1.getLoanAmount());
        compLoanDB1.setNetInterestRate(compLoanFromExcel1.getNetInterestRate());
        compLoanDB1.setActualPaymentTerm(compLoanFromExcel1
                .getActualPaymentTerm());

        compLoanDB1.ejbStore();

    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation for compLoanDB Object
            getDealCalc().doCalc(compLoanDB);
            // Trigger the Calculation for compLoanDB1 Object
            getDealCalc().doCalc(compLoanDB1);
            // Asserts the Data
            assertEquals("compLoanDB", compLoanDB
                    .getPAndIPaymentAmountMonthly(), compLoanFromExcel
                    .getPAndIPaymentAmountMonthly(), 0.0);

            assertEquals("compLoanDB1", compLoanDB1
                    .getPAndIPaymentAmountMonthly(), compLoanFromExcel1
                    .getPAndIPaymentAmountMonthly(), 0.0);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            if (componentDB1 != null)
            {
                componentDB1.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(PandIMonthlyLoanComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLoan test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @return ComponentLoan - ComponentLoan Entity object
     */
    public ComponentLoan getcompLoanFromExcel()
    {
        return compLoanFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLoan data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @param compLoanFromExcel -
     *            Component Loan data object
     */
    public void setcompLoanFromExcel(ComponentLoan compLoanFromExcel)
    {
        this.compLoanFromExcel = compLoanFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLoan test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @return ComponentLoan - ComponentLoan Entity object
     */
    public ComponentLoan getcompLoanFromExcel1()
    {
        return compLoanFromExcel1;
    }

    /**
     * <p>
     * Description: Sets the ComponentLoan data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @param compLoanFromExcel1 -
     *            Component Loan data object
     */
    public void setcompLoanFromExcel1(ComponentLoan compLoanFromExcel1)
    {
        this.compLoanFromExcel1 = compLoanFromExcel1;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel1()
    {
        return componentFromExcel1;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.13 03-Jul-2008 Initial Version
     * @param componentFromExcel1 -
     *            Component data object
     */
    public void setComponentFromExcel1(Component componentFromExcel1)
    {
        this.componentFromExcel1 = componentFromExcel1;
    }

}
