package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;

public class TeaserNetInterestRateComponentTest1 extends FXDBTestCase {


	private IDataSet dataSetTest;
	private TeaserNetInterestRateComponent teaserNetInterestRateComponent ;

	public TeaserNetInterestRateComponentTest1(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TeaserNetInterestRateComponent.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		teaserNetInterestRateComponent = new TeaserNetInterestRateComponent();

	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(teaserNetInterestRateComponent);
		Component component=new Component(srk, componentId, copyId);
		teaserNetInterestRateComponent.doCalc(component);
		status = true;
		assertTrue(status);
	}


	public void testgetTargetForComponent()throws Exception {
		boolean status = false;
		ITable testgetTargetForComponent = dataSetTest.getTable("testgetTargetForComponent");
		String id = (String) testgetTargetForComponent.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponent.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(teaserNetInterestRateComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Component component=new Component(srk, componentId, copyId);
		teaserNetInterestRateComponent.getTarget(component,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForcomponentMortgage()throws Exception {
		boolean status = false;
		ITable testgetTargetForcomponentMortgage = dataSetTest.getTable("testgetTargetForcomponentMortgage");
		String id = (String) testgetTargetForcomponentMortgage.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForcomponentMortgage.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(teaserNetInterestRateComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentMortgage componentMortgage=new ComponentMortgage(srk, componentId, copyId);
		teaserNetInterestRateComponent.getTarget(componentMortgage,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForComponentLoc()throws Exception {
		boolean status = false;
		ITable testgetTargetForComponentLoc = dataSetTest.getTable("testgetTargetForComponentLoc");
		String id = (String) testgetTargetForComponentLoc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponentLoc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(teaserNetInterestRateComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLOC componentLOC=new ComponentLOC(srk, componentId, copyId);
		teaserNetInterestRateComponent.getTarget(componentLOC,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForComponentLoan()throws Exception {
		boolean status = false;
		ITable testgetTargetForComponentLoan = dataSetTest.getTable("testgetTargetForComponentLoan");
		String id = (String) testgetTargetForComponentLoan.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponentLoan.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(teaserNetInterestRateComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLoan componentLoan=new ComponentLoan(srk, componentId, copyId);
		teaserNetInterestRateComponent.getTarget(componentLoan,calcMonitor);
		status = true;
		assertTrue(status);
	}


}
