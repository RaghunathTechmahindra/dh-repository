package com.basis100.deal.calc.impl;

import static org.junit.Assert.*;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.DealEntity;

public class TotalPropertyExpensesTest extends FXDBTestCase{
	private IDataSet dataSetTest;
	private TotalPropertyExpenses totalPropertyExpenses;
	boolean status = false;

	public TotalPropertyExpensesTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TotalPropertyExpenses.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.totalPropertyExpenses = new TotalPropertyExpenses();
	}

	@Test
	public void testDoCalc() throws Exception {
		setEntityCacheAndSession(totalPropertyExpenses);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "PROPERTYID");
		int propertyId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Property property = new Property(srk, calcMonitor, propertyId, copyId);
		totalPropertyExpenses.doCalc(property);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		setEntityCacheAndSession(totalPropertyExpenses);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testGetTarget");
		String id = (String) testExtractIncome.getValue(0, "PROPERTYEXPENSEID");
		int propertyExpenseId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		PropertyExpense propertyExpense = new PropertyExpense(srk, calcMonitor, propertyExpenseId, copyId);
		DealEntity entity = (DealEntity) propertyExpense;
		totalPropertyExpenses.getTarget(entity,calcMonitor);
		status = true;
		assertTrue(status);
	}

}
