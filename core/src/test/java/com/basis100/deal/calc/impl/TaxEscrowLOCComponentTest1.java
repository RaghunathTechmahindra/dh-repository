package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;

public class TaxEscrowLOCComponentTest1 extends FXDBTestCase {


	private IDataSet dataSetTest;
	private TaxEscrowLOCComponent taxEscrowLOCComponent ;

	public TaxEscrowLOCComponentTest1(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TaxEscrowLOCComponent.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		taxEscrowLOCComponent = new TaxEscrowLOCComponent();

	}

	/*@Test
	public void testDoCalc()throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowLOCComponent);
		ComponentLOC componentLOC=new ComponentLOC(srk, componentId, copyId);
		taxEscrowLOCComponent.doCalc(componentLOC);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForDeal()throws Exception  {
		boolean status = false;
		ITable testgetTargetForDeal = dataSetTest.getTable("testgetTargetForDeal");
		String id = (String) testgetTargetForDeal.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTargetForDeal.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowLOCComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk, calcMonitor,dealId, copyId);
		taxEscrowLOCComponent.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForComponentLoc()throws Exception  {
		boolean status = false;
		ITable testgetTargetForComponentLoc = dataSetTest.getTable("testgetTargetForComponentLoc");
		String id = (String) testgetTargetForComponentLoc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponentLoc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowLOCComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLOC componentLOC=new ComponentLOC(srk, componentId, copyId);
		taxEscrowLOCComponent.getTarget(componentLOC,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForProperty()throws Exception  {
		boolean status = false;
		ITable testgetTargetForProperty = dataSetTest.getTable("testgetTargetForProperty");
		String id = (String) testgetTargetForProperty.getValue(0, "PROPERTYID");
		int PropertyId = Integer.parseInt(id);
		String copy = (String) testgetTargetForProperty.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowLOCComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Property property=new Property(srk, calcMonitor, PropertyId, copyId);
		taxEscrowLOCComponent.getTarget(property,calcMonitor);
		status = true;
		assertTrue(status);
	}
	*/
	

	public void testTBR(){
	System.out.println("test");
	 }

}
