package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;

public class MaturityDateMortgageComponentTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MaturityDateMortgageComponent maturityDateMortgageComponent;

	public MaturityDateMortgageComponentTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MaturityDateMortgageComponent.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.maturityDateMortgageComponent = new MaturityDateMortgageComponent();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(maturityDateMortgageComponent);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		ComponentMortgage componentMortgage=new ComponentMortgage(srk,calcMonitor,componentId, copyId);
		maturityDateMortgageComponent.doCalc(componentMortgage);
		status = true;
		assertTrue(status);
	}
	
/*	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(maturityDateMortgageComponent);
          ITable testExtractDeal = dataSetTest.getTable("testGetTarget");                          
          String id=(String)testExtractDeal.getValue(0,"DEALID");
          int dealId=Integer.parseInt(id);
          String copy=(String)testExtractDeal.getValue(0,"COPYID");
          int copyId=Integer.parseInt(copy);
          Deal deal=new Deal(srk,calcMonitor,dealId, copyId);
          maturityDateMortgageComponent.getTarget(deal,calcMonitor);
          status = true;
          assertTrue(status);
    }*/


}
