package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.impl.NetRate;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class NumberOfBorrowersTestDB extends FXDBTestCase {


	private IDataSet dataSetTest;
	private NumberOfBorrowers numberOfBorrowersTest;

	public NumberOfBorrowersTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				NumberOfBorrowers.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.numberOfBorrowersTest = new NumberOfBorrowers();
	}

	@Test
	public void testDoCalc() throws Exception {
	    setEntityCacheAndSession(numberOfBorrowersTest);
		boolean status = false;
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		numberOfBorrowersTest.doCalc(deal);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		setEntityCacheAndSession(numberOfBorrowersTest);
		boolean status = false;
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		// Deal
		ITable testExtractIncome = dataSetTest.getTable("testGetTargetDeal");
		String id = (String) testExtractIncome.getValue(0, "BORROWERID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Borrower borrower = new Borrower(srk, calcMonitor, dealId, copyId);
		numberOfBorrowersTest.getTarget(borrower, calcMonitor);
		// Property
		

		status = true;
		assertTrue(status);
	}



}
