package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: PandIMonthlyMtgComponentTest
 * </p>
 * <p>
 * Description: Unit test class for calculating Principal and Interest portion
 * of payment calculated using the monthly payment frequency values for mortgage
 * component
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.1 19-Jun-2008 Initial version
 */
public class PandIMonthlyMtgComponentTest extends BaseCalcTest
{

    Component componentDB = null;

    Component componentDB1 = null;

    ComponentMortgage compMtgDB = null;

    ComponentMortgage compMtgDB1 = null;

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentMortgage compMtgFromExcel = null;

    private Component componentFromExcel1 = null;

    private ComponentMortgage compMtgFromExcel1 = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 20-Jun-2008 Initial version
     */
    public PandIMonthlyMtgComponentTest()
    {
        super("com.basis100.deal.calc.impl.PandIMonthlyMtgComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compMtgFromExcel = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");

        componentFromExcel1 = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compMtgFromExcel1 = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");

    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 20-Jun-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());

        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setTaxPayorId(dealFromExcel.getTaxPayorId());
        deal.ejbStore();

        setCopyId(deal.getCopyId());

        // CReate Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        // Create Component Mortgage Object
        compMtgDB = (ComponentMortgage) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentMortgage");
        compMtgDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        compMtgDB.setPaymentFrequencyId(compMtgFromExcel.getPaymentFrequencyId());
        compMtgDB.setPrePaymentOptionsId(compMtgFromExcel.getPrePaymentOptionsId());
        compMtgDB.setPrivilegePaymentId(compMtgFromExcel.getPrivilegePaymentId());
        compMtgDB.setCashBackAmountOverride(compMtgFromExcel.getCashBackAmountOverride());
        compMtgDB.setRateLock(compMtgFromExcel.getRateLock());
        compMtgDB.setTotalMortgageAmount(compMtgFromExcel.getTotalMortgageAmount());
        compMtgDB.setNetInterestRate(compMtgFromExcel.getNetInterestRate());
        compMtgDB.setAmortizationTerm(compMtgFromExcel.getAmortizationTerm());

        compMtgDB.ejbStore();

        // CReate Component Object with different repayment type
        componentDB1 = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB1.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel1.getComponentTypeId(), componentFromExcel1
                        .getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB1.ejbStore();
        // Create Component Mortgage Object using componentDB1 componentId and
        // CopyId
        compMtgDB1 = (ComponentMortgage) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentMortgage");
        compMtgDB1.create(componentDB1.getComponentId(), componentDB1.getCopyId());
        compMtgDB1.setPaymentFrequencyId(compMtgFromExcel1.getPaymentFrequencyId());
        compMtgDB1.setPrePaymentOptionsId(compMtgFromExcel1.getPrePaymentOptionsId());
        compMtgDB1.setPrivilegePaymentId(compMtgFromExcel1.getPrivilegePaymentId());
        compMtgDB1.setCashBackAmountOverride(compMtgFromExcel1.getCashBackAmountOverride());
        compMtgDB1.setRateLock(compMtgFromExcel1.getRateLock());
        compMtgDB1.setTotalMortgageAmount(compMtgFromExcel1.getTotalMortgageAmount());
        compMtgDB1.setNetInterestRate(compMtgFromExcel1.getNetInterestRate());
        compMtgDB1.setAmortizationTerm(compMtgFromExcel1.getAmortizationTerm());

        compMtgDB1.ejbStore();

    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 20-Jun-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation for compMtgDB Object            
            getDealCalc().doCalc(compMtgDB);
            //Trigger the Calculation for compMtgDB1 Object
            getDealCalc().doCalc(compMtgDB1);
            //Asserts the Data
            assertEquals("compMtgDB", compMtgDB.getPAndIPaymentAmountMonthly(),
                    compMtgFromExcel.getPAndIPaymentAmountMonthly(), 0.0);

            assertEquals("compMtgDB1", compMtgDB1
                    .getPAndIPaymentAmountMonthly(), compMtgFromExcel1
                    .getPAndIPaymentAmountMonthly(), 0.0);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 20-Jun-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            if (componentDB1 != null)
            {
                componentDB1.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 20-Jun-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(PandIMonthlyMtgComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getcompMtgFromExcel()
    {
        return compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @param compMtgFromExcel -
     *            Component Mortgage data object
     */
    public void setcompMtgFromExcel(ComponentMortgage compMtgFromExcel)
    {
        this.compMtgFromExcel = compMtgFromExcel;
    }
    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getCompMtgFromExcel1()
    {
        return compMtgFromExcel1;
    }
    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @param compMtgFromExcel1 -
     *            Component Mortgage data object
     */
    public void setCompMtgFromExcel1(ComponentMortgage compMtgFromExcel1)
    {
        this.compMtgFromExcel1 = compMtgFromExcel1;
    }
    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel1()
    {
        return componentFromExcel1;
    }
    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.1 12-Jun-2008 Initial Version
     * @param componentFromExcel1 -
     *            Component data object
     */
    public void setComponentFromExcel1(Component componentFromExcel1)
    {
        this.componentFromExcel1 = componentFromExcel1;
    }

}
