package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;

public class PandIMonthlyMtgComponentTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private PandIMonthlyMtgComponent pandIMonthlyMtgComponent;

	public PandIMonthlyMtgComponentTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PandIMonthlyMtgComponent.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.pandIMonthlyMtgComponent = new PandIMonthlyMtgComponent();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(pandIMonthlyMtgComponent);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		ComponentMortgage componentMortgage=new ComponentMortgage(srk,calcMonitor,componentId, copyId);
		pandIMonthlyMtgComponent.doCalc(componentMortgage);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(pandIMonthlyMtgComponent);
		//ComponentMortgage
		ITable testExtract = dataSetTest.getTable("testDoCalc");                          
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		ComponentMortgage componentMortgage=new ComponentMortgage(srk,calcMonitor,componentId, copyId);
		pandIMonthlyMtgComponent.getTarget(componentMortgage,calcMonitor);
		//ComponentMortgage
		ITable testExtractComponent = dataSetTest.getTable("testGetTargetComponent");                          
		String id1=(String)testExtractComponent.getValue(0,"COMPONENTID");
		int componentId1=Integer.parseInt(id1);
		String copy1=(String)testExtractComponent.getValue(0,"COPYID");
		int copyId1=Integer.parseInt(copy1);
		Component component=new Component(srk,calcMonitor,componentId1, copyId1);
		pandIMonthlyMtgComponent.getTarget(component,calcMonitor);
		status = true;
		assertTrue(status);
    }
}