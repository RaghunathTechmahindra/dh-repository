package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: TeaserPandIComponentTest
 * </p>
 * <p>
 * Description: Unit test class for calculating Teaser Principal and Interest
 * Payment for Components
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.8 10-July-2008 Initial version
 * @version 1.1 XS_11.8 24-Jul-2008 Refactored the Code
 */
public class TeaserPandIComponentTest extends BaseCalcTest
{
    // initialization variables for Data Base Objects
    Component componentDB = null;

    Component componentDBForLoc = null;

    Component componentDBForLoan = null;

    ComponentMortgage compMtgDB = null;

    ComponentLOC compLocDB = null;

    ComponentLoan compLoanDB = null;

    // initialization variables for Excel Data Objects

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private Component componentFromExcelForLoc = null;

    private Component componentFromExcelForLoan = null;

    private ComponentMortgage compMtgFromExcel = null;

    private ComponentLOC compLocFromExcel = null;

    private ComponentLoan compLoanFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     */
    public TeaserPandIComponentTest()
    {
        super("com.basis100.deal.calc.impl.TeaserPandIComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcelForLoc = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentFromExcelForLoan = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compMtgFromExcel = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");
        compLocFromExcel = (ComponentLOC) loadTestDataObject("com.basis100.deal.entity.ComponentLOC");
        compLoanFromExcel = (ComponentLoan) loadTestDataObject("com.basis100.deal.entity.ComponentLoan");

    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());

        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setEstimatedClosingDate(dealFromExcel.getEstimatedClosingDate());
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // CReate Component for Component Mortgage Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.setTeaserNetInterestRate(componentFromExcel
                .getTeaserNetInterestRate());
        componentDB.ejbStore();
        // Create Component Mortgage Object
        compMtgDB = (ComponentMortgage) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentMortgage");
        compMtgDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        compMtgDB.setPaymentFrequencyId(compMtgFromExcel
                .getPaymentFrequencyId());
        compMtgDB.setTotalMortgageAmount(compMtgFromExcel
                .getTotalMortgageAmount());
        compMtgDB.setAmortizationTerm(compMtgFromExcel.getAmortizationTerm());
        compMtgDB.ejbStore();

        // CReate Component for Component LOC Object
        componentDBForLoc = (Component) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.Component");
        componentDBForLoc.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcelForLoc.getComponentTypeId(),
                componentFromExcelForLoc.getMtgProdId());
        componentDBForLoc.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDBForLoc.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDBForLoc.setTeaserNetInterestRate(componentFromExcelForLoc
                .getTeaserNetInterestRate());
        componentDBForLoc.ejbStore();

        // Create Component LOC Object
        compLocDB = (ComponentLOC) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLOC");
        compLocDB.create(componentDBForLoc.getComponentId(), componentDBForLoc
                .getCopyId());
        compLocDB.setPaymentFrequencyId(compLocFromExcel
                .getPaymentFrequencyId());

        compLocDB.setTotalLocAmount(compLocFromExcel.getTotalLocAmount());
        compLocDB.ejbStore();

        // CReate Component for ComponentLOan Object
        componentDBForLoan = (Component) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.Component");
        componentDBForLoan.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcelForLoan.getComponentTypeId(),
                componentFromExcelForLoan.getMtgProdId());
        componentDBForLoan.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDBForLoan.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDBForLoan.setTeaserNetInterestRate(componentFromExcelForLoan
                .getTeaserNetInterestRate());
        componentDBForLoan.ejbStore();

        // Create Component Loan Object
        compLoanDB = (ComponentLoan) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLoan");
        compLoanDB.create(componentDBForLoan.getComponentId(),
                componentDBForLoan.getCopyId());
        compLoanDB.setPaymentFrequencyId(compLoanFromExcel
                .getPaymentFrequencyId());
        compLoanDB.setLoanAmount(compLoanFromExcel.getLoanAmount());
        compLoanDB.setActualPaymentTerm(compLoanFromExcel.getActualPaymentTerm());
        compLoanDB.ejbStore();

    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(componentDB);
            getDealCalc().doCalc(componentDBForLoc);
            getDealCalc().doCalc(componentDBForLoan);
            assertEquals("MtgComp", componentDB.getTeaserPiAmount(),
                    componentFromExcel.getTeaserPiAmount());
            assertEquals("LOCComp", componentDBForLoc.getTeaserPiAmount(),
                    componentFromExcelForLoc.getTeaserPiAmount());
            assertEquals("LoanComp", componentDBForLoan.getTeaserPiAmount(),
                    componentFromExcelForLoan.getTeaserPiAmount());
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(TeaserPandIComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getcompMtgFromExcel()
    {
        return compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @param compMtgFromExcel -
     *            Component Mortgage data object
     */
    public void setcompMtgFromExcel(ComponentMortgage compMtgFromExcel)
    {
        this.compMtgFromExcel = compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLoan test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @return ComponentLoan - Component Loan Entity object
     */
    public ComponentLoan getCompLoanFromExcel()
    {
        return compLoanFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLoan data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @param compLoanFromExcel -
     *            Component Loan data object
     */
    public void setCompLoanFromExcel(ComponentLoan compLoanFromExcel)
    {
        this.compLoanFromExcel = compLoanFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLOC test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @return ComponentLOC - Component LOC Entity object
     */
    public ComponentLOC getCompLocFromExcel()
    {
        return compLocFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLoan data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @param compLocFromExcel -
     *            Component LOC data object
     */
    public void setCompLocFromExcel(ComponentLOC compLocFromExcel)
    {
        this.compLocFromExcel = compLocFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcelForLoan()
    {
        return componentFromExcelForLoan;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @param componentFromExcelForLoan -
     *            Component data object
     */
    public void setComponentFromExcelForLoan(Component componentFromExcelForLoan)
    {
        this.componentFromExcelForLoan = componentFromExcelForLoan;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcelForLoc()
    {
        return componentFromExcelForLoc;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.8 10-July-2008 Initial version
     * @param componentFromExcelForLoc -
     *            Component data object
     */
    public void setComponentFromExcelForLoc(Component componentFromExcelForLoc)
    {
        this.componentFromExcelForLoc = componentFromExcelForLoc;
    }

}