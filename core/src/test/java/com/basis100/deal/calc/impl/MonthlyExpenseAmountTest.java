package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.PropertyExpense;

public class MonthlyExpenseAmountTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MonthlyExpenseAmount monthlyExpenseAmount;

	public MonthlyExpenseAmountTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MonthlyExpenseAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.monthlyExpenseAmount = new MonthlyExpenseAmount();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(monthlyExpenseAmount);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
        ITable testExtractPropertyExpense = dataSetTest.getTable("testDoCalc");                      
        String id2=(String)testExtractPropertyExpense.getValue(0,"PROPERTYEXPENSEID");
        int propertyExpenseId=Integer.parseInt(id2);
        String copy2=(String)testExtractPropertyExpense.getValue(0,"COPYID");
        int copyId2=Integer.parseInt(copy2);
        PropertyExpense propertyExpense=new PropertyExpense(srk,calcMonitor,propertyExpenseId, copyId2);
		monthlyExpenseAmount.doCalc(propertyExpense);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(monthlyExpenseAmount);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
        ITable testExtractPropertyExpense = dataSetTest.getTable("testDoCalc");                      
        String id2=(String)testExtractPropertyExpense.getValue(0,"PROPERTYEXPENSEID");
        int propertyExpenseId=Integer.parseInt(id2);
        String copy2=(String)testExtractPropertyExpense.getValue(0,"COPYID");
        int copyId2=Integer.parseInt(copy2);
        PropertyExpense propertyExpense=new PropertyExpense(srk,calcMonitor,propertyExpenseId, copyId2);
		monthlyExpenseAmount.getTarget(propertyExpense, calcMonitor);
		status = true;
		assertTrue(status);
	}

}
