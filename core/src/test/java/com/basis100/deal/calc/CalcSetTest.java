/*
 * @(#)CalcSetTest.java    Sep 6, 2007
 *
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.calc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.calc.CalcSet;

/**
 * @author JDai
 * 
 */
public class CalcSetTest {

    // The logger
    private static final Log _log = LogFactory.getLog(CalcSetTest.class);

    // The CalcSet
    private CalcSet loc;
    
    /**
     *  initialize 
     */
    @Before
    public void setUp() throws Exception {
        loc = new CalcSet();
    }

    /**
     *  teardown
     */
    @After
    public void tearDown() throws Exception {
    }

    /*
     * test the list set of calcset
     */
    @Test
    public void testCalcSet() throws Exception {
        loc.SaveToFile(System.out);
    }

}
