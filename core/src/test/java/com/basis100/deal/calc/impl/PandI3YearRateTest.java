/*
 * @(#)PandI3YearRateTest.java July 23, 2008 <story id : XS 11.16> Copyright (C)
 * 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import junit.framework.Test;

import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;

/**
 * <p>
 * Title: PandI3YearRateTest
 * </p>
 * <p>
 * Description: Unit test class for P & I Using 3 Year Rate
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.16 23-Jul-2008 Initial version
 */
public class PandI3YearRateTest extends BaseCalcTest
{

    private Deal dealFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 23-Jul-2008 Initial version
     */
    public PandI3YearRateTest()
    {
        super("com.basis100.deal.calc.impl.PandI3YearRate");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 23-Jul-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");

        deal.setMtgProdId(dealFromExcel.getMtgProdId());
        deal.setTotalLoanAmount(dealFromExcel.getTotalLoanAmount());
        deal.setAmortizationTerm(dealFromExcel.getAmortizationTerm());
        deal.setNetInterestRate(dealFromExcel.getNetInterestRate());
        deal.setRepaymentTypeId(dealFromExcel.getRepaymentTypeId());
        deal.setPricingProfileId(dealFromExcel.getPricingProfileId());
        deal.setProductTypeId(dealFromExcel.getProductTypeId());

        deal.ejbStore();

        setCopyId(deal.getCopyId());
    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 23-July-2008 Initial version
     * @throws Exception
     */
    @Override
    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            setEntityCacheAndSession();
            getDealCalc().doCalc(deal);

            assertEquals(DealCalcUtil.roundToNearestHundredth(dealFromExcel
                    .getPandIUsing3YearRate()), DealCalcUtil
                    .roundToNearestHundredth(deal.getPandIUsing3YearRate()),
                    0.0);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        finally
        {
            tearDownTestData();
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: This method removes test data from DB
     * </p>
     * @throws Exception
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 23-July-2008 Initial version
     */
    public void tearDownTestData() throws Exception
    {
        try
        {
            if (deal != null)
            {
                deal.dcm = null;
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 23-Jul-2008 Initial version
     */
    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory.createSuite(PandI3YearRateTest.class);
    }

    /**
     * <p>
     * Description: Returns the Deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 23-Jul-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 23-Jul-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

}
