package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.PropertyExpense;

public class CalculateMinimumIncomeRequiredTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private CalculateMinimumIncomeRequired calculateMinimumIncomeRequired;

	public CalculateMinimumIncomeRequiredTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CalculateMinimumIncomeRequired.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.calculateMinimumIncomeRequired = new CalculateMinimumIncomeRequired();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testGetTargetDeal");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(calculateMinimumIncomeRequired);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);
		calculateMinimumIncomeRequired.doCalc(deal);
		status = true;
		assertTrue(status);
	}
	
	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(calculateMinimumIncomeRequired);
		//Liability
		ITable testExtract = dataSetTest.getTable("testGetTargetLiability");			   	   
		String id=(String)testExtract.getValue(0,"LIABILITYID");
		int liablityId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Liability liability=new Liability(srk,calcMonitor,liablityId, copyId);
		calculateMinimumIncomeRequired.getTarget(liability,calcMonitor);		
		//PropertyExpense
		ITable testExtractPropertyExpense = dataSetTest.getTable("testGetTargetPropertyExpense");			   	   
		String id2=(String)testExtractPropertyExpense.getValue(0,"PROPERTYEXPENSEID");
		int propertyExpenseId=Integer.parseInt(id2);
		String copy2=(String)testExtractPropertyExpense.getValue(0,"COPYID");
		int copyId2=Integer.parseInt(copy2);
		PropertyExpense propertyExpense=new PropertyExpense(srk,calcMonitor,propertyExpenseId, copyId2);
		calculateMinimumIncomeRequired.getTarget(propertyExpense,calcMonitor);
		//Deal
		ITable testExtractDeal = dataSetTest.getTable("testGetTargetDeal");			   	   
		String id3=(String)testExtractDeal.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id3);
		String copy3=(String)testExtractDeal.getValue(0,"COPYID");
		int copyId3=Integer.parseInt(copy3);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId3);
		calculateMinimumIncomeRequired.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
