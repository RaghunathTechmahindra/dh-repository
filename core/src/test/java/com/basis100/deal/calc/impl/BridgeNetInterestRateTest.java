package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Bridge;
import com.basis100.deal.entity.Deal;

public class BridgeNetInterestRateTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private BridgeNetInterestRate bridgeNetInterestRate;

	public BridgeNetInterestRateTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BridgeNetInterestRate.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.bridgeNetInterestRate = new BridgeNetInterestRate();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"BRIDGEID");
		int bridgeId=Integer.parseInt(id);
		String deal=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(deal);
		setEntityCacheAndSession(bridgeNetInterestRate);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Bridge bridge=new Bridge(srk,calcMonitor);
		bridge.setBridgeId(bridgeId);
		bridge.setDealId(dealId);
		bridgeNetInterestRate.doCalc(bridge);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(bridgeNetInterestRate);
          ITable testExtract = dataSetTest.getTable("testGetTarget");                         
          String id=(String)testExtract.getValue(0,"DEALID");
          int dealId=Integer.parseInt(id);
          String copy=(String)testExtract.getValue(0,"COPYID");
          int copyId=Integer.parseInt(copy);
          Deal deal=new Deal(srk,calcMonitor,dealId, copyId);
          bridgeNetInterestRate.getTarget(deal,calcMonitor);          
          status = true;
          assertTrue(status);
    }
}
