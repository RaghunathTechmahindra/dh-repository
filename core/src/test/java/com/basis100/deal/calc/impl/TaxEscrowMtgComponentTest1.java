package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;

public class TaxEscrowMtgComponentTest1 extends FXDBTestCase {


	private IDataSet dataSetTest;
	private TaxEscrowMtgComponent taxEscrowMtgComponent ;

	public TaxEscrowMtgComponentTest1(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TaxEscrowMtgComponent.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		taxEscrowMtgComponent = new TaxEscrowMtgComponent();

	}

	@Test
	public void testDoCalc()throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowMtgComponent);
		ComponentMortgage componentMortgage=new ComponentMortgage(srk, componentId, copyId);
		taxEscrowMtgComponent.doCalc(componentMortgage);
		status = true;
		assertTrue(status);
	}


	/*public void testgetTargetForDeal()throws Exception {
		boolean status = false;
		ITable testgetTargetForDeal = dataSetTest.getTable("testgetTargetForDeal");
		String id = (String) testgetTargetForDeal.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTargetForDeal.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowMtgComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk, calcMonitor,dealId, copyId);
		taxEscrowMtgComponent.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}*/
	public void testgetTargetForcomponentMortgage()throws Exception {
		boolean status = false;
		ITable testgetTargetForcomponentMortgage = dataSetTest.getTable("testgetTargetForcomponentMortgage");
		String id = (String) testgetTargetForcomponentMortgage.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForcomponentMortgage.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowMtgComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentMortgage componentMortgage=new ComponentMortgage(srk, componentId, copyId);
		taxEscrowMtgComponent.getTarget(componentMortgage,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForProperty()throws Exception {
		boolean status = false;
		ITable testgetTargetForProperty = dataSetTest.getTable("testgetTargetForProperty");
		String id = (String) testgetTargetForProperty.getValue(0, "PROPERTYID");
		int PropertyId = Integer.parseInt(id);
		String copy = (String) testgetTargetForProperty.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(taxEscrowMtgComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Property property=new Property(srk, calcMonitor, PropertyId, copyId);
		taxEscrowMtgComponent.getTarget(property,calcMonitor);
		status = true;
		assertTrue(status);
	}

}
