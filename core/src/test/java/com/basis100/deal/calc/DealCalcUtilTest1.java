package com.basis100.deal.calc;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import MosSystem.Mc;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.IncomePK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.filogix.util.Xc;

public class DealCalcUtilTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	//	private DealCalcUtil dealCalcUtill;
	CalcEntityCache entityCache = new CalcEntityCache(null);

	public DealCalcUtilTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DealCalcUtil.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Test
	public void testAnnualize()  throws Exception {
		double result;
		ITable testAnnualize = dataSetTest.getTable("testAnnualize");	   	   
		String id=(String)testAnnualize.getValue(0,"INCOMEID");
		int incomeId=Integer.parseInt(id);
		String copy=(String)testAnnualize.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		CalcMonitor cal = CalcMonitor.getMonitor(srk); 
		Income entity = new Income(srk, cal);
		entity = entity.findByPrimaryKey(new IncomePK(incomeId, copyId));
		result= DealCalcUtil.annualize(entity.getIncomePeriodId(), "IncomePeriod");
		assertEquals(1.0, result);

	}
	public void testAnnualizePropertyExpensePeriod()  throws Exception {
		double result;
		ITable testAnnualize = dataSetTest.getTable("testAnnualize");	   	   
		String id=(String)testAnnualize.getValue(0,"INCOMEID");
		int incomeId=Integer.parseInt(id);
		String copy=(String)testAnnualize.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		CalcMonitor cal = CalcMonitor.getMonitor(srk); 
		Income entity = new Income(srk, cal);
		entity = entity.findByPrimaryKey(new IncomePK(incomeId, copyId));
		result= DealCalcUtil.annualize(entity.getIncomePeriodId(), "PropertyExpensePeriod");
		assertEquals(1.0, result);

	}

	public void testInterestFactor()throws Exception{
		double result;
		ITable testInterestFactor = dataSetTest.getTable("testInterestFactor");	   	   
		String id=(String)testInterestFactor.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testInterestFactor.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		double netInterestRate = deal.getNetInterestRate();
		int paymentFrequencyId = deal.getPaymentFrequencyId();
		String decimalYear = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
		double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
		int interestCompoundingId = -1;
		double paymentsPerYearForInterestFactor = DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId, mDaysInYear);
		String compound = PicklistData.getMatchingColumnValue(-1, "interestcompound",deal.getMtgProd().getInterestCompoundingId(),"interestcompounddescription");
		result =DealCalcUtil.interestFactor(netInterestRate, paymentsPerYearForInterestFactor, compound);
		assertEquals(0.0, Math.floor(result));

	}
	public void testSinCheckDigit()  throws Exception {
		int result;
		ITable testSinCheckDigit = dataSetTest.getTable("testSinCheckDigit");	   	   
		String id=(String)testSinCheckDigit.getValue(0,"BORROWERID");
		int borrowerId=Integer.parseInt(id);
		String copy=(String)testSinCheckDigit.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		CalcMonitor cal = CalcMonitor.getMonitor(srk); 
		Borrower borrower=new Borrower(srk,cal,borrowerId,copyId);
		String sin = borrower.getSocialInsuranceNumber();
		//String sin = "124898948949898498499849  3";
		result=DealCalcUtil.sinCheckDigit(sin);
		assertEquals(0, result);
	}
	public void testPCalcNumOfPaymentsPerYear() throws Exception {
		double result=0.0d;
		ITable testPCalcNumOfPaymentsPerYear = dataSetTest.getTable("testPCalcNumOfPaymentsPerYear");	   	   
		String id=(String)testPCalcNumOfPaymentsPerYear.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testPCalcNumOfPaymentsPerYear.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		int paymentFrequencyId = deal.getPaymentFrequencyId();
		String decimalYear = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
		double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
		result=DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId, mDaysInYear);
		assertEquals(12.0, Math.floor(result));
	}
	public void testPCalcNumOfPaymentsPerYearOne() throws Exception {
		double result=0.0d;
		ITable testPCalcNumOfPaymentsPerYear = dataSetTest.getTable("testPCalcNumOfPaymentsPerYear");	   	   
		String id=(String)testPCalcNumOfPaymentsPerYear.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testPCalcNumOfPaymentsPerYear.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		int paymentFrequencyId = deal.getPaymentFrequencyId();
		String decimalYear = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.calc.usepandipaymentsbasedondjdaysyear", "N");
		double mDaysInYear = decimalYear.equals("Y") ? 365.25 : 365;
		result=DealCalcUtil.pCalcNumOfPaymentsPerYear(paymentFrequencyId);
		assertEquals(12.0, result);
	}

	public void testExMIFees() throws Exception {
		double result=0.0d;
		ITable testExMIFees = dataSetTest.getTable("testExMIFees");	   	   
		String id=(String)testExMIFees.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExMIFees.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.exMIFees(deal, srk);
		assertEquals(0.0, result);
		
	}

	public void testExDefaultFeeAmount() throws Exception {
		double result=0.0d;
		ITable testExDefaultFeeAmount = dataSetTest.getTable("testExDefaultFeeAmount");	   	   
		String id=(String)testExDefaultFeeAmount.getValue(0,"FEETYPEID");
		int feeTypeId=Integer.parseInt(id);
		result=DealCalcUtil.exDefaultFeeAmount(feeTypeId, srk);
		assertEquals(25.0, result);

	}
	public void testExSelectedFeesTotalAmt() throws Exception {
		double result=0.0d;
		ITable testExSelectedFeesTotalAmt = dataSetTest.getTable("testExSelectedFeesTotalAmt");	   	   
		String id=(String)testExSelectedFeesTotalAmt.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExSelectedFeesTotalAmt.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		CalcMonitor cal = CalcMonitor.getMonitor(srk); 
		//fee.getFeeTypeId();
		//System.out.println("getFeeTypeId173===========>>>>>>>" +fee.getFeeTypeId());

	}
	public void testGetTotalAllFeeAmount() throws Exception {
		double result=0.0d;
		ITable testGetTotalAllFeeAmount = dataSetTest.getTable("testGetTotalAllFeeAmount");	   	   
		String id=(String)testGetTotalAllFeeAmount.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testGetTotalAllFeeAmount.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.getTotalAllFeeAmount(deal,srk);
		assertEquals(820.8, result);
	}

	public void testPandIExpenseTrue()throws Exception{
		double result=0.0d;
		ITable testPandIExpenseTrue = dataSetTest.getTable("testPandIExpenseTrue");	   	   
		String id=(String)testPandIExpenseTrue.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testPandIExpenseTrue.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.PandIExpense(srk, entityCache, deal, true);
		assertEquals(12353, Math.round(result));
	}
	public void testPandIExpenseFalse()throws Exception{
		double result=0.0d;
		ITable testPandIExpenseFalse = dataSetTest.getTable("testPandIExpenseFalse");	   	   
		String id=(String)testPandIExpenseFalse.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testPandIExpenseFalse.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.PandIExpense(srk, entityCache, deal, false);
		assertEquals(4638.96, result);
	}
	public void testCalcPaymentFrequency()throws Exception{
		double result=0.0d;
		ITable testCalcPaymentFrequency = dataSetTest.getTable("testCalcPaymentFrequency");	   	   
		String id=(String)testCalcPaymentFrequency.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalcPaymentFrequency.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		boolean dJDaysYearFlag = Boolean.getBoolean(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
				"com.basis100.calc.usePandIPaymentsBasedOnDjDaysYear"));
		result=DealCalcUtil.calcPaymentFrequency(deal.getPaymentFrequencyId(), dJDaysYearFlag);
		assertEquals(12.0, Math.floor(result));
	}
	public void testCalculatePropertyTaxEscrowForTD()throws Exception{
		double result=0.0d;
		ITable testCalculatePropertyTaxEscrowForTD = dataSetTest.getTable("testCalculatePropertyTaxEscrowForTD");	   	   
		String id=(String)testCalculatePropertyTaxEscrowForTD.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalculatePropertyTaxEscrowForTD.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		Property primaryProp = new Property(srk, null)
		.findByPrimaryProperty(deal.getDealId(), deal.getCopyId(),
				deal.getInstitutionProfileId());
		result=DealCalcUtil.calculatePropertyTaxEscrowForTD(deal.getInterimInterestAdjustmentDate(), primaryProp.getTotalAnnualTaxAmount());
		assertEquals(133.33,result);

	}
	public void testCalculatePropertyTaxEscrowForXceed()throws Exception{
		double result=0.0d;
		ITable testCalculatePropertyTaxEscrowForXceed = dataSetTest.getTable("testCalculatePropertyTaxEscrowForXceed");	   	   
		String id=(String)testCalculatePropertyTaxEscrowForXceed.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalculatePropertyTaxEscrowForXceed.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		Property primaryProp = new Property(srk, null)
		.findByPrimaryProperty(deal.getDealId(), deal.getCopyId(),
				deal.getInstitutionProfileId());
		result=DealCalcUtil.calculatePropertyTaxEscrowForXceed(deal.getFirstPaymentDate(),  primaryProp.getTotalAnnualTaxAmount(), primaryProp.getProvinceId(), deal.getPaymentFrequencyId());
		assertEquals(100.0,result);

	}
	public void testCalculatePropertyTaxEscrowForDesjardins()throws Exception{
		double result=0.0d;
		ITable testCalculatePropertyTaxEscrowForDesjardins = dataSetTest.getTable("testCalculatePropertyTaxEscrowForDesjardins");	   	   
		String id=(String)testCalculatePropertyTaxEscrowForDesjardins.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalculatePropertyTaxEscrowForDesjardins.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		Property primaryProp = new Property(srk, null)
		.findByPrimaryProperty(deal.getDealId(), deal.getCopyId(),
				deal.getInstitutionProfileId());
		result=DealCalcUtil.calculatePropertyTaxEscrowForDesjardins( deal.getPaymentFrequencyId(),primaryProp.getTotalAnnualTaxAmount());
		assertEquals(100.0,result);

	}
	public void testCalculatePerDiemInterestAmount()throws Exception{
		double result=0.0d;
		ITable testCalculatePerDiemInterestAmount = dataSetTest.getTable("testCalculatePerDiemInterestAmount");	   	   
		String id=(String)testCalculatePerDiemInterestAmount.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalculatePerDiemInterestAmount.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		LenderProfile lenderProfile = (LenderProfile) this.entityCache
				.find(srk, new LenderProfilePK(deal.getLenderProfileId()));
		String interestCompoundDesc = BXResources.getPickListDescription(
				lenderProfile.getInstitutionProfileId(),
				"INTERESTCOMPOUND", lenderProfile.getInterestCompoundId(),
				srk.getLanguageId());
		result=DealCalcUtil.calculatePerDiemInterestAmount(interestCompoundDesc, deal.getNetInterestRate(), deal.getTotalLoanAmount());
		assertEquals(18.0,Math.floor(result));
	}
	public void testcalculatePandI()throws Exception{
		double result=0.0d;
		String interestCompoundDesc;
		ITable testcalculatePandI = dataSetTest.getTable("testcalculatePandI");	   	   
		String id=(String)testcalculatePandI.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testcalculatePandI.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
				new MtgProdPK(deal.getMtgProdId()));
		interestCompoundDesc = PicklistData.getMatchingColumnValue(
				-1, "interestcompound", mtgProd.getInterestCompoundingId(),
				"interestcompounddescription");
		result=DealCalcUtil.calculatePandI(deal.getInstitutionProfileId(),deal.getActualPaymentTerm(), deal.getPaymentFrequencyId(),interestCompoundDesc,deal.getTotalLoanAmount(),deal.getNetInterestRate());
		assertEquals(1970.0,Math.floor(result));
	}
	public void testcalculatePandIOne()throws Exception{
		double result=0.0d;
		String interestCompoundDesc;
		ITable testcalculatePandIOne = dataSetTest.getTable("testcalculatePandIOne");	   	   
		String id=(String)testcalculatePandIOne.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testcalculatePandIOne.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
				new MtgProdPK(deal.getMtgProdId()));
		interestCompoundDesc = PicklistData.getMatchingColumnValue(
				-1, "interestcompound", mtgProd.getInterestCompoundingId(),
				"interestcompounddescription");
		double numOfPaymentsPerYear = DealCalcUtil.calculateNumOfPaymentsPerYear(deal.getInstitutionProfileId(),  deal.getPaymentFrequencyId());
		double interestFactor = DealCalcUtil.interestFactor(deal.getNetInterestRate(),numOfPaymentsPerYear, interestCompoundDesc);
		result=DealCalcUtil.calculatePandI(deal.getTotalLoanAmount(),interestFactor,numOfPaymentsPerYear);
		assertEquals(17328.0,Math.floor(result));
	}
	public void testCalculateNoOfPaymentsToAmortize()throws Exception{
		double result=0.0d;
		ITable testCalculateNoOfPaymentsToAmortize = dataSetTest.getTable("testCalculateNoOfPaymentsToAmortize");	   	   
		String id=(String)testCalculateNoOfPaymentsToAmortize.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalculateNoOfPaymentsToAmortize.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		double numOfPaymentsPerYear = DealCalcUtil.calculateNumOfPaymentsPerYear(deal.getInstitutionProfileId(),  deal.getPaymentFrequencyId());
		result=DealCalcUtil.calculateNoOfPaymentsToAmortize( deal.getPaymentFrequencyId(), deal.getActualPaymentTerm(), deal.getInstitutionProfileId(), numOfPaymentsPerYear);
		assertEquals(60.0,Math.floor(result));
	}

	public void testCalculateNumOfPaymentsPerYear()throws Exception{
		double result=0.0d;
		ITable testCalculateNumOfPaymentsPerYear = dataSetTest.getTable("testCalculateNumOfPaymentsPerYear");	   	   
		String id=(String)testCalculateNumOfPaymentsPerYear.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalculateNumOfPaymentsPerYear.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);

		result= DealCalcUtil.calculateNumOfPaymentsPerYear(deal.getInstitutionProfileId(),  deal.getPaymentFrequencyId());
		assertEquals(12.0,Math.floor(result));
	}
	public void testNumberOfPaymentsToAmortize()throws Exception{
		double result=0.0d;
		ITable testNumberOfPaymentsToAmortize = dataSetTest.getTable("estNumberOfPaymentsToAmortize");	   	   
		String id=(String)testNumberOfPaymentsToAmortize.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testNumberOfPaymentsToAmortize.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		double numOfPaymentsPerYear = DealCalcUtil.calculateNumOfPaymentsPerYear(deal.getInstitutionProfileId(),  deal.getPaymentFrequencyId());
		result=DealCalcUtil.numberOfPaymentsToAmortize(deal.getActualPaymentTerm(), numOfPaymentsPerYear);
		assertEquals(60,Math.round(result));
	}

	public void testGetDaysBetween()throws Exception{
		double result=0.0d;
		ITable testGetDaysBetween = dataSetTest.getTable("testGetDaysBetween");	   	   
		String id=(String)testGetDaysBetween.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testGetDaysBetween.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		Calendar estClosingDate = Calendar.getInstance();
		estClosingDate.setTime(deal.getEstimatedClosingDate());
		Calendar iADate = Calendar.getInstance();
		iADate.setTime(deal.getApplicationDate());
		result=DealCalcUtil.getDaysBetween(iADate, estClosingDate);
		assertEquals(19,Math.round(result));

	}
	public void testGetPeriodInDays()throws Exception{
		double result=0.0d;
		ITable testGetPeriodInDays = dataSetTest.getTable("testGetPeriodInDays");	   	   
		String id=(String)testGetPeriodInDays.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testGetPeriodInDays.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.getPeriodInDays(deal.getPaymentFrequencyId());
		assertEquals(30.0,result);
	}
	//regarding pro
	public void testCheckIfMIPremiumIsExtended()throws Exception{
		boolean result=false;
		ITable testCheckIfMIPremiumIsExtended = dataSetTest.getTable("testCheckIfMIPremiumIsExtended");	   	   
		String id=(String)testCheckIfMIPremiumIsExtended.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCheckIfMIPremiumIsExtended.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.checkIfMIPremiumIsExtended(deal);
		assertFalse(result);
	}
	public void testcalcPandIFully()throws Exception{
		double result=0.0d;
		String interestCompoundDesc;
		ITable testcalcPandIFully = dataSetTest.getTable("testcalcPandIFully");	   	   
		String id=(String)testcalcPandIFully.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testcalcPandIFully.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		MtgProd mtgProd = (MtgProd) this.entityCache.find(srk,
				new MtgProdPK(deal.getMtgProdId()));
		interestCompoundDesc = PicklistData.getMatchingColumnValue(
				-1, "interestcompound", mtgProd.getInterestCompoundingId(),
				"interestcompounddescription");
		MtgProd mtgProd1 = (MtgProd) entityCache.find(srk,
				new MtgProdPK(deal.getMtgProdId()));
		int interestCompoundingId = mtgProd1.getInterestCompoundingId();
		String rateCode = null;
		rateCode = mtgProd1.getPricingProfile().getRateCode();
		result=DealCalcUtil.calcPandIFully(deal.getInstitutionProfileId(), deal.getNetInterestRate(), deal.getPaymentFrequencyId(), deal.getNetInterestRate(), deal.getActualPaymentTerm(), deal.getRepaymentTypeId(), interestCompoundingId, rateCode);
		assertEquals(0.0,Math.floor(result));
	}
	public void testCalcPandIFully3Year()throws Exception{
		double result=0.0d;
		ITable testCalcPandIFully3Year = dataSetTest.getTable("testCalcPandIFully3Year");	   	   
		String id=(String)testCalcPandIFully3Year.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalcPandIFully3Year.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.calcPandIFully3Year(deal, entityCache);
		assertEquals(1029.0,Math.floor(result));
	}

	public void testCalcPandIFully3YearOne()throws Exception{
		double result=0.0d;
		ITable testCalcPandIFully3YearOne = dataSetTest.getTable("testCalcPandIFully3YearOne");	   	   
		String id=(String)testCalcPandIFully3YearOne.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testCalcPandIFully3YearOne.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		Deal deal=new Deal(srk,null,dealId, copyId);
		result=DealCalcUtil.calcPandIFully3Year(deal, deal.getPaymentFrequencyId(), entityCache);
		assertEquals(1029.0,Math.floor(result));
	}
	
}
