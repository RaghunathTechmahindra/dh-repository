/*
 * @(#)TaxEscrowLOCComponentTest.java June 20, 2008
 * <story id : XS 11.12>
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: TaxEscrowLOCComponentTest
 * </p>
 * <p>
 * Description: Unit test class for LOC Escrow payment amount calculation
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.12 20-Jun-2008 Initial version
 */
public class TaxEscrowLOCComponentTest extends BaseCalcTest
{

    Component componentDB = null;

    ComponentLOC compLOCDB = null;

    Property propertyDB = null;

    private Component componentFromExcel = null;

    private ComponentLOC compLOCFromExcel = null;

    private Property propertyFromExcel = null;

    private Deal dealFromExcel = null;

    private String primaryPropertyFlag = "";

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial version
     */
    public TaxEscrowLOCComponentTest()
    {
        super("com.basis100.deal.calc.impl.TaxEscrowLOCComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        propertyFromExcel = (Property) loadTestDataObject("com.basis100.deal.entity.Property");

        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compLOCFromExcel = (ComponentLOC) loadTestDataObject("com.basis100.deal.entity.ComponentLOC");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setTaxPayorId(dealFromExcel.getTaxPayorId());
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        dealPK = (DealPK) deal.getPk();
        // Create Property Object
        propertyDB = (Property) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Property");
        propertyDB.create(dealPK);
        propertyDB.setProvinceId(propertyFromExcel.getProvinceId());
        propertyDB.setTotalAnnualTaxAmount(propertyFromExcel
                .getTotalAnnualTaxAmount());
        propertyDB.setPrimaryPropertyFlag(primaryPropertyFlag);
        propertyDB.ejbStore();
        // Create Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        // Create LOC Object
        compLOCDB = (ComponentLOC) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLOC");

        compLOCDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        compLOCDB.setPaymentFrequencyId(compLOCFromExcel.getPaymentFrequencyId());
        compLOCDB.setPrePaymentOptionsId(compLOCFromExcel.getPrePaymentOptionsId());
        compLOCDB.setPrivilegePaymentId(compLOCFromExcel.getPrivilegePaymentId());
        compLOCDB.setPropertyTaxAllocateFlag(compLOCFromExcel.getPropertyTaxAllocateFlag());
        compLOCDB.setInterestAdjustmentDate(compLOCFromExcel.getInterestAdjustmentDate());
        compLOCDB.setFirstPaymentDate(compLOCFromExcel.getFirstPaymentDate());
        compLOCDB.ejbStore();
    }

    /**
     * <p>
     * Description : 1. Calls setUpTestData() to setup data in DB 2. Sets values
     * from Excel into DB 3. Call the CalcMonitor/Calculation Engine 4. Assert
     * for equality 5. Delete test data created in set-up section and reset's
     * the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(compLOCDB);
            // Assert the data
            assertEquals(compLOCDB.getPropertyTaxEscrowAmount(),
                    compLOCFromExcel.getPropertyTaxEscrowAmount(), 0.00);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {
            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(TaxEscrowLOCComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the ComponentLOC test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @return ComponentLOC - ComponentLOC Entity object
     */
    public ComponentLOC getCompLOCFromExcel()
    {
        return compLOCFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLOC data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @param compLOCFromExcel -
     *            ComponentLOC Entity object
     */
    public void setCompLOCFromExcel(ComponentLOC compLOCFromExcel)
    {
        this.compLOCFromExcel = compLOCFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Property test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @return Property - Property Entity object
     */
    public Property getPropertyFromExcel()
    {
        return propertyFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Property data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @param propertyFromExcel -
     *            property data object
     */
    public void setPropertyFromExcel(Property propertyFromExcel)
    {
        this.propertyFromExcel = propertyFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Primary property flag
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @return String - Primary property flag
     */
    public String getPrimaryPropertyFlag()
    {
        return primaryPropertyFlag;
    }

    /**
     * <p>
     * Description: Sets the Premiary property flag
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.12 20-Jun-2008 Initial Version
     * @param primaryPropertyFlag -
     *            String
     */
    public void setPrimaryPropertyFlag(String primaryPropertyFlag)
    {
        this.primaryPropertyFlag = primaryPropertyFlag;
    }

}
