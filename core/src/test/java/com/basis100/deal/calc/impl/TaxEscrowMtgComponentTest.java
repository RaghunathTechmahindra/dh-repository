package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: TaxEscrowMtgComponentTest
 * </p>
 * <p>
 * Description:Unit test class for the Escrow Payment Amount based on payment
 * frequency for the Mortgage component.
 * @author MCM Impl Team
 * @version 1.0 XS_11.4 11-Jun-2008 Initial version
 * @version 1.1 XS_11.4 24-Jun-2008 Review Comments Incorporated  
 */
public class TaxEscrowMtgComponentTest extends BaseCalcTest
{

    Component componentDB = null;

    ComponentMortgage compMtgDB = null;

    Property propertyDB = null;

    private Component componentFromExcel = null;

    private ComponentMortgage compMtgFromExcel = null;

    private Property propertyFromExcel = null;

    private Deal dealFromExcel = null;

    private String primaryPropertyFlag = "";

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial version
     */
    public TaxEscrowMtgComponentTest()
    {
        super("com.basis100.deal.calc.impl.TaxEscrowMtgComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        propertyFromExcel = (Property) loadTestDataObject("com.basis100.deal.entity.Property");

        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compMtgFromExcel = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setTaxPayorId(dealFromExcel.getTaxPayorId());
        deal.ejbStore();
        setCopyId(deal.getCopyId());
        // Create Property Object
        propertyDB = (Property) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Property");
        propertyDB.create(dealPK);
        propertyDB.setProvinceId(propertyFromExcel.getProvinceId());
        propertyDB.setTotalAnnualTaxAmount(propertyFromExcel
                .getTotalAnnualTaxAmount());
        propertyDB.setPrimaryPropertyFlag(primaryPropertyFlag);
        propertyDB.ejbStore();
        // Create Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel
                        .getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();
        // Create Mortgage Object
        compMtgDB = (ComponentMortgage) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentMortgage");
        compMtgDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        compMtgDB.setPaymentFrequencyId(compMtgFromExcel.getPaymentFrequencyId());
        compMtgDB.setPrePaymentOptionsId(compMtgFromExcel.getPrePaymentOptionsId());
        compMtgDB.setPrivilegePaymentId(compMtgFromExcel.getPrivilegePaymentId());
        compMtgDB.setCashBackAmountOverride(compMtgFromExcel.getCashBackAmountOverride());
        compMtgDB.setRateLock(compMtgFromExcel.getRateLock());
        
        compMtgDB.setPropertyTaxAllocateFlag(compMtgFromExcel
                .getPropertyTaxAllocateFlag());
        compMtgDB.setInterestAdjustmentDate(compMtgFromExcel
                .getInterestAdjustmentDate());
        compMtgDB.setFirstPaymentDate(compMtgFromExcel.getFirstPaymentDate());
        compMtgDB.setPaymentFrequencyId(compMtgFromExcel
                .getPaymentFrequencyId());
        compMtgDB.ejbStore();
    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(compMtgDB);
            // Assert the data
            assertEquals(compMtgDB.getPropertyTaxEscrowAmount(),
                    compMtgFromExcel.getPropertyTaxEscrowAmount(), 0.00);

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(TaxEscrowMtgComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getCompMtgFromExcel()
    {
        return compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @param compMtgFromExcel -
     *            Component Mortgage data object
     */
    public void setCompMtgFromExcel(ComponentMortgage compMtgFromExcel)
    {
        this.compMtgFromExcel = compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Property test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @return Property - Property Entity object
     */
    public Property getPropertyFromExcel()
    {
        return propertyFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Property data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @param propertyFromExcel -
     *            property data object
     */
    public void setPropertyFromExcel(Property propertyFromExcel)
    {
        this.propertyFromExcel = propertyFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Primary property flag
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @return primaryPropertyFlag - primary Property Flag
     */
    public String getPrimaryPropertyFlag()
    {
        return primaryPropertyFlag;
    }

    /**
     * <p>
     * Description: Sets the Primary property flag
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.4 16-Jun-2008 Initial Version
     * @param primaryPropertyFlag -
     *            primary property flag
     */
    public void setPrimaryPropertyFlag(String primaryPropertyFlag)
    {
        this.primaryPropertyFlag = primaryPropertyFlag;
    }

}
