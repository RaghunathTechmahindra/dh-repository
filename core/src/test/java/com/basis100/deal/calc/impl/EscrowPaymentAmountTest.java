package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Property;

public class EscrowPaymentAmountTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private EscrowPaymentAmount escrowPaymentAmount;

	public EscrowPaymentAmountTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(EscrowPaymentAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.escrowPaymentAmount = new EscrowPaymentAmount();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"ESCROWPAYMENTID");
		int escrowpaymentlId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(escrowPaymentAmount);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		EscrowPayment escrowPayment=new EscrowPayment(srk,calcMonitor,escrowpaymentlId, copyId);	
		escrowPaymentAmount.doCalc(escrowPayment);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(escrowPaymentAmount);
          ITable testExtract = dataSetTest.getTable("testGetTargetDeal");                         
          String id=(String)testExtract.getValue(0,"DEALID");
          int liablityId=Integer.parseInt(id);
          String copy=(String)testExtract.getValue(0,"COPYID");
          int copyId=Integer.parseInt(copy);
          Deal deal=new Deal(srk,calcMonitor,liablityId, copyId);
          escrowPaymentAmount.getTarget(deal,calcMonitor);
          //Property
          ITable testExtract1 = dataSetTest.getTable("testGetTargetProperty");                         
          String id1=(String)testExtract1.getValue(0,"PROPERTYID");
          int dealId=Integer.parseInt(id1);
          String copy1=(String)testExtract1.getValue(0,"COPYID");
          int copyId1=Integer.parseInt(copy1);
          Property property=new Property(srk,calcMonitor,dealId, copyId1);
          escrowPaymentAmount.getTarget(property,calcMonitor);
          //EscrowPayment
          ITable testExtract2 = dataSetTest.getTable("testDoCalc");                         
          String id2=(String)testExtract2.getValue(0,"ESCROWPAYMENTID");
          int escrowPaymentId=Integer.parseInt(id2);
          String copy2=(String)testExtract2.getValue(0,"COPYID");
          int copyId2=Integer.parseInt(copy2);
          EscrowPayment escrowPayment=new EscrowPayment(srk,calcMonitor,escrowPaymentId, copyId2);
          escrowPaymentAmount.getTarget(escrowPayment,calcMonitor);
          status = true;
          assertTrue(status);
    }
}
