package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: IADNumberOfDaysMtgComponentTest
 * </p>
 * <p>
 * Description: Unit test class for calculating the number of days between the
 * Closing Date and the Interest Adjustment Date (number of days in the interest
 * adjustment period) of a Mortgage Component
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.5 25-Jun-2008 Initial version
 */
public class IADNumberOfDaysMtgComponentTest extends BaseCalcTest
{

    Component componentDB = null;

    ComponentMortgage compMtgDB = null;

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentMortgage compMtgFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial version
     */
    public IADNumberOfDaysMtgComponentTest()
    {
        super("com.basis100.deal.calc.impl.IADNumberOfDaysMtgComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compMtgFromExcel = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");

    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());

        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setEstimatedClosingDate(dealFromExcel.getEstimatedClosingDate());
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // Create Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();
        // Create Component Mortgage Object
        compMtgDB = (ComponentMortgage) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentMortgage");
        compMtgDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        compMtgDB.setPaymentFrequencyId(compMtgFromExcel.getPaymentFrequencyId());
        compMtgDB.setPrePaymentOptionsId(compMtgFromExcel.getPrePaymentOptionsId());
        compMtgDB.setPrivilegePaymentId(compMtgFromExcel.getPrivilegePaymentId());
        compMtgDB.setCashBackAmountOverride(compMtgFromExcel.getCashBackAmountOverride());
        compMtgDB.setRateLock(compMtgFromExcel.getRateLock());
        compMtgDB.setInterestAdjustmentDate(compMtgFromExcel.getInterestAdjustmentDate());
        compMtgDB.ejbStore();

    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(compMtgDB);
            assertEquals("ComponentDB", compMtgDB.getIadNumberOfDays(),
                    compMtgFromExcel.getIadNumberOfDays());
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 11-Jun-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(IADNumberOfDaysMtgComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getcompMtgFromExcel()
    {
        return compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.5 25-Jun-2008 Initial Version
     * @param compMtgFromExcel -
     *            Component Mortgage data object
     */
    public void setcompMtgFromExcel(ComponentMortgage compMtgFromExcel)
    {
        this.compMtgFromExcel = compMtgFromExcel;
    }
}
