package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Borrower;

public class BorrowerTotalAssetTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private BorrowerTotalAsset borrowerTotalAsset;

	public BorrowerTotalAssetTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BorrowerTotalAsset.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.borrowerTotalAsset = new BorrowerTotalAsset();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"BORROWERID");
		int borrowerId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(borrowerTotalAsset);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Borrower borrower=new Borrower(srk,calcMonitor,borrowerId, copyId);
		borrowerTotalAsset.doCalc(borrower);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(borrowerTotalAsset);
          ITable testExtract = dataSetTest.getTable("testGetTarget");                         
          String id=(String)testExtract.getValue(0,"ASSETID");
          int assetId=Integer.parseInt(id);
          String copy=(String)testExtract.getValue(0,"COPYID");
          int copyId=Integer.parseInt(copy);
          Asset asset=new Asset(srk,calcMonitor,assetId, copyId);
          borrowerTotalAsset.getTarget(asset,calcMonitor);
          status = true;
          assertTrue(status);
    }

}
