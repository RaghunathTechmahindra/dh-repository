package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PropertyExpense;

public class CombinedTotalCondoFeeExpenseTest extends FXDBTestCase {


	private IDataSet dataSetTest;	
	private CombinedTotalCondoFeeExpense combinedTotalCondoFeeExpense;

	public CombinedTotalCondoFeeExpenseTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CombinedTotalCondoFeeExpense.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		combinedTotalCondoFeeExpense = new CombinedTotalCondoFeeExpense();

	}

	@Test
	public void testDoCalc() throws Exception {
		
			boolean status = false;
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");	   	   
			String id=(String)testDoCalc.getValue(0,"DEALID");
			int dealId=Integer.parseInt(id);
			String copy=(String)testDoCalc.getValue(0,"COPYID");
			int copyId=Integer.parseInt(copy);
			setEntityCacheAndSession(combinedTotalCondoFeeExpense);
			CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
			Deal deal=new Deal(srk,calcMonitor,dealId, copyId);				
			combinedTotalCondoFeeExpense.doCalc(deal);
			status=true;
			assertTrue(status);
		
	}
	public void testgetTarget() throws Exception {
		
			boolean status = false;
			ITable testgetTarget = dataSetTest.getTable("testgetTarget");	   	   
			String id=(String)testgetTarget.getValue(0,"PROPERTYEXPENSEID");
			int propertyexpenseId=Integer.parseInt(id);
			String copy=(String)testgetTarget.getValue(0,"COPYID");
			int copyId=Integer.parseInt(copy);
			setEntityCacheAndSession(combinedTotalCondoFeeExpense);
			CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
			PropertyExpense propertyExpense=new PropertyExpense(srk, calcMonitor, propertyexpenseId, copyId);		
			combinedTotalCondoFeeExpense.getTarget(propertyExpense, calcMonitor);
			status=true;
			assertTrue(status);
	}



}
	



