package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: InterestAdjustmentDateMCMTest
 * </p>
 * <p>
 * Description: Unit test class for InterestAdjustmentDate
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.17 04-Jul-2008 Initial version
 */
public class InterestAdjustmentDateMCMTest extends BaseCalcTest
{

    MtgProd mtgProdDB = null;

    private Deal dealFromExcel = null;

    private MtgProd mtgProdFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial version
     */
    public InterestAdjustmentDateMCMTest()
    {
        super("com.basis100.deal.calc.impl.InterestAdjustmentDate");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        mtgProdFromExcel = (MtgProd) loadTestDataObject("com.basis100.deal.entity.MtgProd");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setEstimatedClosingDate(dealFromExcel.getEstimatedClosingDate());
        deal.setPaymentFrequencyId(dealFromExcel.getPaymentFrequencyId());
        deal.setFirstPaymentDate(dealFromExcel.getFirstPaymentDate());
        deal.setMtgProdId(dealFromExcel.getMtgProdId());
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // Create MtgProd Object
        mtgProdDB = (MtgProd) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.MtgProd");

        
        MtgProdPK mtgProdPK = new MtgProdPK(deal.getMtgProdId());
        mtgProdDB.findByPrimaryKey(mtgProdPK);
        mtgProdDB.setComponentEligibleFlag(mtgProdFromExcel
                .getComponentEligibleFlag());
        mtgProdDB.setUnderwriteAsTypeId(mtgProdFromExcel
                .getUnderwriteAsTypeId());
        mtgProdDB.ejbStore();

    }

    /**
     * <p>
     * Description :This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*

        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(deal);
            // Assert the Data
            System.out.println("DB VALUE == "+deal.getInterimInterestAdjustmentDate());
            System.out.println("XL VALUE == "+dealFromExcel.getInterimInterestAdjustmentDate());
            String dateDB = formatDateJExcel(deal.getInterimInterestAdjustmentDate());
            String dateExcel = formatDateJExcel(dealFromExcel
                    .getInterimInterestAdjustmentDate());
            assertEquals(dateDB, dateExcel);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            tearDownTestData();
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }

    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(InterestAdjustmentDateMCMTest.class);
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public MtgProd getMtgProdFromExcel()
    {
        return mtgProdFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.17 04-Jul-2008 Initial Version
     * @param mtgProdFromExcel -
     *            Deal data object
     */
    public void setMtgProdFromExcel(MtgProd mtgProdFromExcel)
    {
        this.mtgProdFromExcel = mtgProdFromExcel;
    }

}
