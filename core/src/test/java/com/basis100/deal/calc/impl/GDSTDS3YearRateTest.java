/*
 * @(#)GDSTDS3YearRateTest.java    2007-10-10
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.calc.impl;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.Helper;
import com.basis100.deal.DealTestBase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.DealCalcException;
import com.basis100.deal.entity.Deal;

/**
 * <p>GDSTDS3YearRateTest</p>
 * Express Entity class unit test: GDSTDS3YearRate
 */
public class GDSTDS3YearRateTest extends DealTestBase {
    private final static Logger _log = LoggerFactory
    .getLogger(GDSTDS3YearRateTest.class);
    // the calculator.
    private CalcMonitor _calcMonitor;
    
    public GDSTDS3YearRateTest() {
    }
    
    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception{

        super.setUp();
       
        try {
            _calcMonitor = CalcMonitor.getMonitor(_srk);
        } catch (Exception e) {
            _log.error("New GDSTDS3YearRate error: ", e);
        }

        _log.info("Testing GDSTDS3YearRate ...");
    }

    /**
     * test the do calculation.
     */
    @Test
    public void testDoCalc() throws Exception {

        _log.debug("Tesing [doCalc]");
   
        try {
        for (Iterator i = _testingDeals.iterator(); i.hasNext(); ) {
            
            Deal aDeal = (Deal) i.next();
            _log.info("------ Calc GDSTDS3YearRate for Deal: ");
            _log.info(Helper.dealStr4CalcInGDSTDS3YearRate(aDeal));
            _log.info("------ here ------ ");
            aDeal.forceChange("Discount");
            _calcMonitor.inputEntity(aDeal);
            _calcMonitor.calc();
            _log.info("Calculation Result: --");
            _log.info(Helper.dealStr4CalcInGDSTDS3YearRate(aDeal));
        }
        } catch (DealCalcException dce) {
            _log.error("Deal calc exception: ", dce);
        }
    }
}
