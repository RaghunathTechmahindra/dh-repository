package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Income;

public class MonthlyIncomeAmountTest  extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MonthlyIncomeAmount monthlyIncomeAmount;

	public MonthlyIncomeAmountTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MonthlyIncomeAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.monthlyIncomeAmount = new MonthlyIncomeAmount();
	}

/*	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(monthlyIncomeAmount);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
        String id1=(String)testExtractIncome.getValue(0,"INCOMEID");
        int incomeId=Integer.parseInt(id1);
        String copy1=(String)testExtractIncome.getValue(0,"COPYID");
        int copyId1=Integer.parseInt(copy1);
        Income income=new Income(srk,calcMonitor,incomeId, copyId1);
		monthlyIncomeAmount.doCalc(income);
		status = true;
		assertTrue(status);
	}*/

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(monthlyIncomeAmount);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testGetTarget");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		monthlyIncomeAmount.getTarget(deal, calcMonitor);
		status = true;
		assertTrue(status);
	}

}