package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Income;

public class AnnualIncomeAmountTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private AnnualIncomeAmount annualIncomeAmount;
	private Income income;

	public AnnualIncomeAmountTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(AnnualIncomeAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.annualIncomeAmount = new AnnualIncomeAmount();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"INCOMEID");
		int incomeId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);				
		income=new Income(srk,null,incomeId, copyId);
		annualIncomeAmount.doCalc(income);
		status = true;
		assertTrue(status);
	}
	
	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testGetTarget");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
		setEntityCacheAndSession(annualIncomeAmount);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    CalcMonitor calc = null;
		annualIncomeAmount.getTarget(entity,calc);
		status = true;
		assertTrue(status);
	}

}
