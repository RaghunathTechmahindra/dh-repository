package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PropertyExpense;

public class CombinedTotalTaxExpenseTest extends FXDBTestCase{
	private IDataSet dataSetTest;
	private CombinedTotalTaxExpense combinedTotalTaxExpense;

	public CombinedTotalTaxExpenseTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CombinedTotalTaxExpense.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.combinedTotalTaxExpense = new CombinedTotalTaxExpense();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(combinedTotalTaxExpense);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);
		combinedTotalTaxExpense.doCalc(deal);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(combinedTotalTaxExpense);
          ITable testExtractPropertyExpense = dataSetTest.getTable("testGetTarget");                      
          String id2=(String)testExtractPropertyExpense.getValue(0,"PROPERTYEXPENSEID");
          int propertyExpenseId=Integer.parseInt(id2);
          String copy2=(String)testExtractPropertyExpense.getValue(0,"COPYID");
          int copyId2=Integer.parseInt(copy2);
          PropertyExpense propertyExpense=new PropertyExpense(srk,calcMonitor,propertyExpenseId, copyId2);
          combinedTotalTaxExpense.getTarget(propertyExpense,calcMonitor);          
          status = true;
          assertTrue(status);
    }

}
