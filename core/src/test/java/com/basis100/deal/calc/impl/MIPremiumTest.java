/*
 * @(#)MIPremiumTest.java    2007-10-10
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.calc.impl;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.DealCalcException;

import com.basis100.deal.DealTestBase;
import com.filogix.express.test.Helper;

/**
 * MIPremiumTest - 
 *
 * @version   1.0 2005-6-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class MIPremiumTest extends DealTestBase {

    private final static Logger _log = LoggerFactory
    .getLogger(MIPremiumTest.class);
    // the calculator.
    private CalcMonitor _calcMonitor;

    /**
     * Constructor function
     */
    public MIPremiumTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception{

        super.setUp();
       
        try {
            _calcMonitor = CalcMonitor.getMonitor(_srk);
        } catch (Exception e) {
            _log.error("New MIPremium error: ", e);
        }

        _log.info("Testing MIPremium ...");
    }

    /**
     * test the do calculation.
     */
    @Test
    public void testDoCalcImpl() throws Exception {

        _log.debug("Tesing [doCalc]");
     
        try {
        for (Iterator i = _testingDeals.iterator(); i.hasNext(); ) {
            
            Deal aDeal = (Deal) i.next();
            _log.info("------ Calc MIPremium for Deal: ");
            _log.info(Helper.dealStr4CalcInMIPremium(aDeal));
            _log.info("------ here ------ ");
            aDeal.forceChange("netLoanAmount");
            _calcMonitor.inputEntity(aDeal);
            _calcMonitor.calc();
            _log.info("Calculation Result: --");
            _log.info(Helper.dealStr4CalcInMIPremium(aDeal));
        }
        } catch (DealCalcException dce) {
            _log.error("Deal calc exception: ", dce);
        }
    }
}
