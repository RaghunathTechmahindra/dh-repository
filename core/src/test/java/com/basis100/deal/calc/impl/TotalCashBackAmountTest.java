/*
 * @(#)TotalCashBackAmountTest.java July 18, 2008 <story id : XS 11.16>
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: TotalCashBackAmountTest
 * </p>
 * <p>
 * Description: Unit test class for total amount of all eligible components
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.16 18-Jul-2008 Initial version
 */
public class TotalCashBackAmountTest extends BaseCalcTest
{

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentMortgage componentMortgageFromExcel = null;

    private ComponentSummary componentSummaryFromExcel = null;

    private ComponentSummary componentSummaryFromDB = null;

    private Component componentDB = null;

    private ComponentMortgage componentMortgageFromDB = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial version
     */
    public TotalCashBackAmountTest()
    {
        super("com.basis100.deal.calc.impl.TotalCashBackAmount");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentMortgageFromExcel = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");
        componentSummaryFromExcel = (ComponentSummary) loadTestDataObject("com.basis100.deal.entity.ComponentSummary");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setMtgProdId(dealFromExcel.getMtgProdId());
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel
                        .getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        // Create Mortgage Object
        componentMortgageFromDB = (ComponentMortgage) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentMortgage");
        componentMortgageFromDB.create(componentDB.getComponentId(),
                componentDB.getCopyId());
        componentMortgageFromDB.setCashBackAmount(componentMortgageFromExcel
                .getCashBackAmount());

        // Create ComponentSummary Object
        componentSummaryFromDB = (ComponentSummary) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentSummary");
        componentSummaryFromDB.create(deal.getDealId(), deal.getCopyId());
        // Set Component Summary Amount
        componentSummaryFromDB.setTotalAmount(componentSummaryFromExcel
                .getTotalAmount());

        componentSummaryFromDB.ejbStore();
    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial version
     * @throws Exception
     */
    @Override
    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            setEntityCacheAndSession();
            getDealCalc().doCalc(componentSummaryFromDB);
            assertEquals(componentSummaryFromExcel.getTotalCashbackAmount(),
                    componentSummaryFromDB.getTotalCashbackAmount());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        finally
        {
            tearDownTestData();
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: This method removes test data from DB
     * </p>
     * @throws Exception
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-July-2008 Initial version
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            if (componentMortgageFromDB != null)
            {
                componentMortgageFromDB.ejbRemove(false);
            }
            if (componentSummaryFromDB != null)
            {
                componentSummaryFromDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial version
     */
    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(TotalCashBackAmountTest.class);
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getComponentMortgageFromExcel()
    {
        return componentMortgageFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param compMtgFromExcel -
     *            Component Mortgage data object
     */
    public void setComponentMortgageFromExcel(ComponentMortgage compMtgFromExcel)
    {
        this.componentMortgageFromExcel = compMtgFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentSummary test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return ComponentSummary - ComponentSummary Entity object
     */
    public ComponentSummary getComponentSummaryFromExcel()
    {
        return componentSummaryFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentSummary data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentSummaryFromExcel(
            ComponentSummary componentSummaryFromExcel)
    {
        this.componentSummaryFromExcel = componentSummaryFromExcel;
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.16 18-Jul-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

}
