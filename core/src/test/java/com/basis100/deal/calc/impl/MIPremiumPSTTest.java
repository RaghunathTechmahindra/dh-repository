package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;

public class MIPremiumPSTTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private MIPremiumPST miPremiumPST;

	public MIPremiumPSTTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MIPremiumPST.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.miPremiumPST = new MIPremiumPST();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(miPremiumPST);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		miPremiumPST.doCalc(deal);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(miPremiumPST);
      	// Deal
  		ITable testExtractIncome = dataSetTest.getTable("testGetTargetDeal");
  		String id = (String) testExtractIncome.getValue(0, "DEALID");
  		int dealId = Integer.parseInt(id);
  		String copy = (String) testExtractIncome.getValue(0, "COPYID");
  		int copyId = Integer.parseInt(copy);
  		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
  		miPremiumPST.getTarget(deal, calcMonitor);
  		// Property
  		ITable testExtractPropery = dataSetTest.getTable("testGetTargetProperty");
  		String id1 = (String) testExtractPropery.getValue(0, "PROPERTYID");
  		int propertyId = Integer.parseInt(id1);
  		String copy1 = (String) testExtractPropery.getValue(0, "COPYID");
  		int copyId1 = Integer.parseInt(copy1);
  		Property property = new Property(srk, calcMonitor, propertyId, copyId1);
  		miPremiumPST.getTarget(property, calcMonitor);
          status = true;
          assertTrue(status);
    }

}
