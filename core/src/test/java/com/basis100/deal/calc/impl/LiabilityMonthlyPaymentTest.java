package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Liability;

public class LiabilityMonthlyPaymentTest extends FXDBTestCase {


	private IDataSet dataSetTest;
	private LiabilityMonthlyPayment liabilityMonthlyPayment ;

	public LiabilityMonthlyPaymentTest(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(LiabilityMonthlyPayment.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		liabilityMonthlyPayment = new LiabilityMonthlyPayment();

	}

	@Test
	public void testDoCalc()throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "LIABILITYID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(liabilityMonthlyPayment);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Liability liability=new Liability(srk, calcMonitor, dealId, copyId);
		liabilityMonthlyPayment.doCalc(liability);
		status = true;
		assertTrue(status);
	}
	public void testgetTarget()throws Exception {
		boolean status = false;
		ITable testgetTarget = dataSetTest.getTable("testgetTarget");
		String id = (String) testgetTarget.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTarget.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(liabilityMonthlyPayment);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor, dealId, copyId);
		liabilityMonthlyPayment.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}

}
