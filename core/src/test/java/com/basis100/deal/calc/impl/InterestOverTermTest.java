/*
 * @(#)InterestOverTermTest.java    2007-10-10
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.calc.impl;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.Helper;
import com.basis100.deal.DealTestBase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.DealCalcException;
import com.basis100.deal.entity.Deal;

/**
 * <p>InterestOverTermTest</p>
 * Express Entity class unit test: InterestOverTerm
 */
public class InterestOverTermTest extends DealTestBase {
    private final static Logger _log = LoggerFactory
    .getLogger(InterestOverTermTest.class);
    // the calculator.
    private CalcMonitor _calcMonitor;
    
    public InterestOverTermTest() {
    }
    
    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception{

        super.setUp();
       
        try {
            _calcMonitor = CalcMonitor.getMonitor(_srk);
        } catch (Exception e) {
            _log.error("New InterestOverTerm error: ", e);
        }

        _log.info("Testing InterestOverTerm ...");
    }

    /**
     * test the do calculation.
     */
    @Test
    public void testDoCalc() throws Exception {

        _log.debug("Tesing [doCalc]");
   
        try {
        for (Iterator i = _testingDeals.iterator(); i.hasNext(); ) {
            
            Deal aDeal = (Deal) i.next();
            _log.info("------ Calc InterestOverTerm for Deal: ");
            _log.info(Helper.dealStr4CalcInInterestOverTerm(aDeal));
            _log.info("------ here ------ ");
            aDeal.forceChange("ActualPaymentTerm");
            _calcMonitor.inputEntity(aDeal);
            _calcMonitor.calc();
            _log.info("Calculation Result: --");
            _log.info(Helper.dealStr4CalcInInterestOverTerm(aDeal));
        }
        } catch (DealCalcException dce) {
            _log.error("Deal calc exception: ", dce);
        }
    }
}
