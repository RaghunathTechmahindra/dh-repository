package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;

public class TotalPaymentAmountMtgComponentTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private TotalPaymentAmountMtgComponent totalPaymentAmountMtgComponent;
	boolean status = false;

	public TotalPaymentAmountMtgComponentTest1(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TotalPaymentAmountMtgComponent.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.totalPaymentAmountMtgComponent = new TotalPaymentAmountMtgComponent();
	}

	@Test
	public void testDoCalc() throws Exception {
		setEntityCacheAndSession(totalPaymentAmountMtgComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "COMPONENTID");
		int componentMortgageId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		ComponentMortgage componentMortgage = new ComponentMortgage(srk, calcMonitor, componentMortgageId, copyId);
		totalPaymentAmountMtgComponent.doCalc(componentMortgage);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		setEntityCacheAndSession(totalPaymentAmountMtgComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractComponentLOC = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractComponentLOC.getValue(0, "COMPONENTID");
		int componentMortgageId = Integer.parseInt(id);
		String copy = (String) testExtractComponentLOC.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		ComponentMortgage componentMortgage = new ComponentMortgage(srk, calcMonitor, componentMortgageId, copyId);
		totalPaymentAmountMtgComponent.getTarget(componentMortgage,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
