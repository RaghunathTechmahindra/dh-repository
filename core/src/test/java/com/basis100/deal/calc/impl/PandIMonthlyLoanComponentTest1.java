package com.basis100.deal.calc.impl;

import static org.junit.Assert.*;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.ComponentLoan;

public class PandIMonthlyLoanComponentTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;
	private PandIMonthlyLoanComponent pandIMonthlyLoanComponent;

	public PandIMonthlyLoanComponentTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PandIMonthlyLoanComponent.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.pandIMonthlyLoanComponent = new PandIMonthlyLoanComponent();
	}

	@Test
	public void testDoCalc() throws Exception {
		setEntityCacheAndSession(pandIMonthlyLoanComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);				
		ComponentLoan componentLoan=new ComponentLoan(srk,calcMonitor,componentId, copyId);
		pandIMonthlyLoanComponent.doCalc(componentLoan);
		status = true;
		assertTrue(status);
	}
	
	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
		setEntityCacheAndSession(pandIMonthlyLoanComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLoan componentLoan=new ComponentLoan(srk,calcMonitor,componentId, copyId);
		pandIMonthlyLoanComponent.getTarget(componentLoan,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
