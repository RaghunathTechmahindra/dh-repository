/*
 * @(#)CalcMonitorTest.java    2005-4-4
 *
 * Copyright
 */


package com.basis100.deal.calc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import junit.framework.TestCase;

import com.basis100.resources.SessionResourceKit;

// only for testing.
import com.basis100.TestingEnvironmentInitializer;

/**
 * CalcMonitorTest - unit test case for CalcMonitor
 *
 * @version   1.0 2005-4-4
 * @author    <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class CalcMonitorTest extends TestCase {

    // The logger
    private static Log _log = LogFactory.getLog(CalcMonitorTest.class);

    // the session kit.
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public CalcMonitorTest() {
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {

        TestingEnvironmentInitializer.initTestingEnv();
        _srk = new SessionResourceKit("CalcMonitorTest");
    }

    /**
     * clean the testin env.
     */
    protected void tearDown() {
    }

    /**
     * test the get monitor method.
     */
    public void testGetMonitor() {

        _log.debug("Trying to get monitor...");
        CalcMonitor monitor = CalcMonitor.getMonitor(_srk);
        
        assert monitor != null;
        _log.info("Get following CalcMonitor:");
        _log.info(monitor.toString());
    }

    /**
     * test the calc method.
     */
    public void testCalc() {

        try {
            _log.debug("Trying to calc ...");
            CalcMonitor.getMonitor(_srk).calc();
        } catch (DealCalcException de) {
            _log.error("Deal Calc Exception: ", de);
        }
    }
}
