package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;

public class FirstPaymentDateLimitComponentTest1 extends FXDBTestCase {


	private IDataSet dataSetTest;
	private  FirstPaymentDateLimitComponent  firstPaymentDateLimitComponent ;

	public FirstPaymentDateLimitComponentTest1(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource( FirstPaymentDateLimitComponent.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		firstPaymentDateLimitComponent = new  FirstPaymentDateLimitComponent();

	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(firstPaymentDateLimitComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Component component=new Component(srk, calcMonitor,componentId,copyId);
		firstPaymentDateLimitComponent.doCalc(component);
		status = true;
		assertTrue(status);
	}

	public void testgetTargetForDeal()throws Exception  {
		boolean status = false;
		ITable testgetTargetForDeal = dataSetTest.getTable("testgetTargetForDeal");
		String id = (String) testgetTargetForDeal.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTargetForDeal.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(firstPaymentDateLimitComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk, calcMonitor,dealId, copyId);
		firstPaymentDateLimitComponent.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForComponentLoc()throws Exception  {
		boolean status = false;
		ITable testgetTargetForComponentLoc = dataSetTest.getTable("testgetTargetForComponentLoc");
		String id = (String) testgetTargetForComponentLoc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponentLoc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(firstPaymentDateLimitComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLOC componentLOC=new ComponentLOC(srk, componentId, copyId);
		firstPaymentDateLimitComponent.getTarget(componentLOC,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForcomponentMortgage()throws Exception {
		boolean status = false;
		ITable testgetTargetForcomponentMortgage = dataSetTest.getTable("testgetTargetForcomponentMortgage");
		String id = (String) testgetTargetForcomponentMortgage.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForcomponentMortgage.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(firstPaymentDateLimitComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentMortgage componentMortgage=new ComponentMortgage(srk, componentId, copyId);
		firstPaymentDateLimitComponent.getTarget(componentMortgage,calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetforComponentLoan()throws Exception {
		boolean status = false;
		ITable testgetTargetforComponentLoan = dataSetTest.getTable("testgetTargetForComponentLoan");
		String id = (String) testgetTargetforComponentLoan.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetforComponentLoan.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(firstPaymentDateLimitComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLoan componentLoan=new ComponentLoan(srk, calcMonitor,componentId,copyId);
		firstPaymentDateLimitComponent.getTarget(componentLoan,calcMonitor);
		status = true;
		assertTrue(status);
	}

}
