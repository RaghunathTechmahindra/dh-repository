package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Income;

public class BorrowerTotalIncomeTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private BorrowerTotalIncome borrowerTotalIncome;

	public BorrowerTotalIncomeTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BorrowerTotalIncome.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.borrowerTotalIncome = new BorrowerTotalIncome();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"BORROWERID");
		int borrowerId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(borrowerTotalIncome);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Borrower borrower=new Borrower(srk,calcMonitor,borrowerId, copyId);
		borrowerTotalIncome.doCalc(borrower);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(borrowerTotalIncome);
          ITable testExtractIncome = dataSetTest.getTable("testGetTargetIncome");
          String id1=(String)testExtractIncome.getValue(0,"INCOMEID");
          int incomeId=Integer.parseInt(id1);
          String copy1=(String)testExtractIncome.getValue(0,"COPYID");
          int copyId1=Integer.parseInt(copy1);
          Income income=new Income(srk,calcMonitor,incomeId, copyId1);
          borrowerTotalIncome.getTarget(income,calcMonitor);
          status = true;
          assertTrue(status);
    }

}
