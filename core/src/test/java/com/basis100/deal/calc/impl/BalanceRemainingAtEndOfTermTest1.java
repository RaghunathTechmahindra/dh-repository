package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;

public class BalanceRemainingAtEndOfTermTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;
	private BalanceRemainingAtEndOfTerm balanceRemainingAtEndOfTerm;

	public BalanceRemainingAtEndOfTermTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BalanceRemainingAtEndOfTerm.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.balanceRemainingAtEndOfTerm = new BalanceRemainingAtEndOfTerm();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(balanceRemainingAtEndOfTerm);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);	
		balanceRemainingAtEndOfTerm.doCalc(deal);
		status = true;
		assertTrue(status);
	}
	
	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
		setEntityCacheAndSession(balanceRemainingAtEndOfTerm);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    CalcMonitor calc = null;
	    balanceRemainingAtEndOfTerm.getTarget(entity,calc);
		status = true;
		assertTrue(status);
	}

}
