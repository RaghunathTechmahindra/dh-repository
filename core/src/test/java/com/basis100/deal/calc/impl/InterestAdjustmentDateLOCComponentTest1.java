package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;

public class InterestAdjustmentDateLOCComponentTest1 extends FXDBTestCase {


	private IDataSet dataSetTest;
	private InterestAdjustmentDateLOCComponent interestAdjustmentDateLOCComponent;

	public InterestAdjustmentDateLOCComponentTest1(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InterestAdjustmentDateLOCComponent.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interestAdjustmentDateLOCComponent = new InterestAdjustmentDateLOCComponent();

	}

	@Test
	public void testDoCalc() throws Exception  {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateLOCComponent);
		ComponentLOC ComponentLOC=new ComponentLOC(srk, componentId, copyId);
		interestAdjustmentDateLOCComponent.doCalc(ComponentLOC);
		status = true;
		assertTrue(status);
	}

	public void testgetTargetForComponentLoc() throws Exception  {
		boolean status = false;
		ITable testgetTargetForComponentLoc = dataSetTest.getTable("testgetTargetForComponentLoc");
		String id = (String) testgetTargetForComponentLoc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponentLoc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateLOCComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLOC componentLoc=new ComponentLOC(srk, componentId, copyId);
		interestAdjustmentDateLOCComponent.getTarget(componentLoc,calcMonitor);
		status = true;
		assertTrue(status);
	}

	public void testgetTargetForDeal() throws Exception  {
		boolean status = false;
		ITable testgetTargetForDeal = dataSetTest.getTable("testgetTargetForDeal");
		String id = (String) testgetTargetForDeal.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTargetForDeal.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateLOCComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor, dealId, copyId);
		interestAdjustmentDateLOCComponent.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}

}
