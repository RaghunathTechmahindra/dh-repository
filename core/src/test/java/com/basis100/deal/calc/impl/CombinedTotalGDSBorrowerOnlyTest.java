package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.PropertyExpense;


public class CombinedTotalGDSBorrowerOnlyTest extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private CombinedTotalGDSBorrowerOnly combinedTotalGDSBorrowerOnly;


	public CombinedTotalGDSBorrowerOnlyTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CombinedTotalGDSBorrowerOnly.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		combinedTotalGDSBorrowerOnly = new CombinedTotalGDSBorrowerOnly();

	}


	@Test
	public void testDoCalc() throws Exception {

		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");	   	   
		String id=(String)testDoCalc.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testDoCalc.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(combinedTotalGDSBorrowerOnly);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);		
		combinedTotalGDSBorrowerOnly.doCalc(deal);
		status=true;
		assertTrue(status);

	}
	@Test
	public void testgetTargetforDeal()  throws Exception  {

		boolean status = false;
		ITable testgetTargetforDeal = dataSetTest.getTable("testgetTargetforDeal");	   	   
		String id=(String)testgetTargetforDeal.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testgetTargetforDeal.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(combinedTotalGDSBorrowerOnly);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);		
		combinedTotalGDSBorrowerOnly.getTarget(deal, calcMonitor);
		status=true;
		assertTrue(status);

	}

	/*@Test
	public void testgetTargetforIncome()  throws Exception {

		boolean status = false;
		ITable testgetTargetforIncome = dataSetTest.getTable("testgetTargetforIncome");	   	   
		String id=(String)testgetTargetforIncome.getValue(0,"INCOMEID");
		int incomeId=Integer.parseInt(id);
		String copy=(String)testgetTargetforIncome.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(combinedTotalGDSBorrowerOnly);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Income income=new Income(srk, calcMonitor, incomeId, copyId);
		combinedTotalGDSBorrowerOnly.getTarget(income, calcMonitor);	
		status=true;
		assertTrue(status);

	}
	@Test
	public void testgetTargetforLiability() throws Exception  {
		boolean status = false;
		ITable testgetTargetforLiability = dataSetTest.getTable("testgetTargetforLiability");	   	   
		String id=(String)testgetTargetforLiability.getValue(0,"LIABILITYID");
		int libilityId=Integer.parseInt(id);
		String copy=(String)testgetTargetforLiability.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(combinedTotalGDSBorrowerOnly);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Liability liability=new Liability(srk, calcMonitor, libilityId, copyId);
		combinedTotalGDSBorrowerOnly.getTarget(liability, calcMonitor);
		status=true;
		assertTrue(status);

	}
	@Test
	public void testgetTargetforPropertyExpense() throws Exception {

		boolean status = false;
		ITable testgetTargetforPropertyExpense = dataSetTest.getTable("testgetTargetforPropertyExpense");	   	   
		String id=(String)testgetTargetforPropertyExpense.getValue(0,"PROPERTYEXPENSEID");
		int propertyexpenseId=Integer.parseInt(id);
		String copy=(String)testgetTargetforPropertyExpense.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(combinedTotalGDSBorrowerOnly);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		PropertyExpense propertyExpense=new PropertyExpense(srk, calcMonitor, propertyexpenseId, copyId);
		combinedTotalGDSBorrowerOnly.getTarget(propertyExpense, calcMonitor);
		status=true;
		assertTrue(status);

	}*/

}




