package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;

public class MIPremiumTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MIPremium miPremium;

	public MIPremiumTest1(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MIPremium.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.miPremium = new MIPremium();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(miPremium);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		miPremium.doCalc(deal);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(miPremium);
		
		// Deal
		ITable testExtractIncome = dataSetTest.getTable("testGetTargetDeal");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		miPremium.getTarget(deal, calcMonitor);
		// Property
		
		
		ITable testExtractPropery = dataSetTest.getTable("testGetTargetProperty");
		String id1 = (String) testExtractPropery.getValue(0, "PROPERTYID");
		int propertyId = Integer.parseInt(id1);
		String copy1 = (String) testExtractPropery.getValue(0, "COPYID");
		int copyId1 = Integer.parseInt(copy1);
		Property property = new Property(srk, calcMonitor, propertyId, copyId1);
		miPremium.getTarget(property, calcMonitor);
		status = true;
		assertTrue(status);
	}

}
