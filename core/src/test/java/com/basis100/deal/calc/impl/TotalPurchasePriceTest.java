package com.basis100.deal.calc.impl;

import static org.junit.Assert.*;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;

public class TotalPurchasePriceTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private TotalPurchasePrice totalPurchasePrice;
	boolean status = false;

	public TotalPurchasePriceTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TotalPurchasePrice.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.totalPurchasePrice = new TotalPurchasePrice();
	}

	@Test
	public void testDoCalc() throws Exception {
		setEntityCacheAndSession(totalPurchasePrice);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		totalPurchasePrice.doCalc(deal);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		setEntityCacheAndSession(totalPurchasePrice);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testGetTarget");
		String id = (String) testExtractIncome.getValue(0, "PROPERTYID");
		int propertyId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Property property = new Property(srk, calcMonitor, propertyId, copyId);
		DealEntity entity = (DealEntity) property;
		totalPurchasePrice.getTarget(entity,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
