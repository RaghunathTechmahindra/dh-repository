package com.basis100.deal.calc.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PropertyExpense;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class CombinedHeatingExpenseTestDB extends FXDBTestCase {
	private IDataSet dataSetTest;
	private CombinedHeatingExpense combinedHeating;
	private PropertyExpense pe;
	private Deal deal;
	private CalcMonitor dcm;

	public CombinedHeatingExpenseTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				CombinedHeatingExpense.class.getSimpleName()
						+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcm = CalcMonitor.getMonitor(srk);
		this.combinedHeating = new CombinedHeatingExpense();
	}

	public void testGetTarget() throws Exception {
		ITable testGetTarget = dataSetTest.getTable("testGetTarget");
		int copyId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COPYID"));
		int propertyExpenseId = Integer.parseInt((String) testGetTarget
				.getValue(0, "PROPERTYEXPENSEID"));
		pe = new PropertyExpense(srk, dcm, propertyExpenseId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		setEntityCacheAndSession();
		getDealCalc().getTarget(pe, dcm);
		assert combinedHeating.getTargets() != null;
		assert combinedHeating.getTargets().size() >= 1;
	}

	public void testDoCalc() throws Exception {
		try {
			srk.beginTransaction();
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");
			int dealId = Integer.parseInt((String) testDoCalc.getValue(0,
					"DEAL"));
			int copyId = Integer.parseInt((String) testDoCalc.getValue(0,
					"COPYID"));
			deal = new Deal(srk, dcm, dealId, copyId);
			srk.getExpressState().setDealInstitutionId(1);
			setEntityCacheAndSession();
			// Trigger the Calculation
			getDealCalc().doCalc(deal);
			assert combinedHeating.getCalcNumber()!=null;
			assert combinedHeating.getCalcNumber().length() >=0;
			srk.rollbackTransaction();
		} catch (Exception ex) {
			throw ex;
		}

	}

	public void setEntityCacheAndSession() {
		Class classCalc = combinedHeating.getClass();
		Class superClassofCalc = classCalc.getSuperclass();
		Field ff[] = superClassofCalc.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase("entityCache")) {
				Class fieldClasstest = field.getType();
				System.out.println(fieldClasstest.getName());
				Constructor con[] = fieldClasstest.getDeclaredConstructors();
				for (Constructor conss : con) {
					conss.setAccessible(true);
					System.out.println(conss.getModifiers() + " "
							+ conss.getName());

					conss.setAccessible(true);
					CalcEntityCache cache;
					try {
						cache = (CalcEntityCache) conss
								.newInstance(EntityTestUtil.getCalcMonitor());
						field.set(combinedHeating, cache);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				break;
			}
		}

		combinedHeating.setResourceKit(EntityTestUtil.getSessionResourceKit());
	}

	public CombinedHeatingExpense getDealCalc() {
		return combinedHeating;
	}

}