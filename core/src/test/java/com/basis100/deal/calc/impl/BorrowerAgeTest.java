package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;

public class BorrowerAgeTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private BorrowerAge borrowerAge;

	public BorrowerAgeTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BorrowerAge.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.borrowerAge = new BorrowerAge();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"BORROWERID");
		int borrowerId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(borrowerAge);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Borrower borrower=new Borrower(srk,calcMonitor,borrowerId, copyId);
		borrowerAge.doCalc(borrower);
		status = true;
		assertTrue(status);
	}
	
	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testGetTarget");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(borrowerAge);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);
		borrowerAge.getTarget(deal,null);
		status = true;
		assertTrue(status);
	}
	
}
