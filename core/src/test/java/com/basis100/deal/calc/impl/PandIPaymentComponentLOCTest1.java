package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;

public class PandIPaymentComponentLOCTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private PandIPaymentComponentLOC pandIPaymentComponentLOC;

	public PandIPaymentComponentLOCTest1(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PandIPaymentComponentLOC.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.pandIPaymentComponentLOC = new PandIPaymentComponentLOC();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(pandIPaymentComponentLOC);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "COMPONENTID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		ComponentLOC componentLOC = new ComponentLOC(srk, calcMonitor, dealId, copyId);
		pandIPaymentComponentLOC.doCalc(componentLOC);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(pandIPaymentComponentLOC);
		//ComponentMortgage
		ITable testExtract = dataSetTest.getTable("testDoCalc");                          
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		ComponentLOC componentLOC=new ComponentLOC(srk,calcMonitor,componentId, copyId);
		pandIPaymentComponentLOC.getTarget(componentLOC,calcMonitor);
		//ComponentMortgage
		ITable testExtractComponent = dataSetTest.getTable("testGetTargetComponent");                          
		String id1=(String)testExtractComponent.getValue(0,"COMPONENTID");
		int componentId1=Integer.parseInt(id1);
		String copy1=(String)testExtractComponent.getValue(0,"COPYID");
		int copyId1=Integer.parseInt(copy1);
		Component component=new Component(srk,calcMonitor,componentId1, copyId1);
		pandIPaymentComponentLOC.getTarget(component,calcMonitor);
		status = true;
		assertTrue(status);
	}
}