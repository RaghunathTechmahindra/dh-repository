package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Property;

public class NumberOfPropertiesTestDB extends FXDBTestCase {



	private IDataSet dataSetTest;
	private NumberOfProperties numberOfPropertiesTest;

	public NumberOfPropertiesTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				NumberOfProperties.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.numberOfPropertiesTest = new NumberOfProperties();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(numberOfPropertiesTest);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		numberOfPropertiesTest.doCalc(deal);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(numberOfPropertiesTest);
		// Deal
		ITable testExtractIncome = dataSetTest.getTable("testGetTarget");
		String id = (String) testExtractIncome.getValue(0, "PROPERTYID");
		int propertyId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Property property = new Property(srk, calcMonitor, propertyId, copyId);
		numberOfPropertiesTest.getTarget(property, calcMonitor);
		// Property
		

		status = true;
		assertTrue(status);
	}




}
