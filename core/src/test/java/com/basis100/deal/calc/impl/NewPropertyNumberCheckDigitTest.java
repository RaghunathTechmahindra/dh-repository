package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;

public class NewPropertyNumberCheckDigitTest extends FXDBTestCase {



	private IDataSet dataSetTest;
	private NewPropertyNumberCheckDigit newPropertyNumberCheckDigitTest;

	public NewPropertyNumberCheckDigitTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				NewPropertyNumberCheckDigit.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.newPropertyNumberCheckDigitTest = new NewPropertyNumberCheckDigit();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		setEntityCacheAndSession(newPropertyNumberCheckDigitTest);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		newPropertyNumberCheckDigitTest.doCalc(deal);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		setEntityCacheAndSession(newPropertyNumberCheckDigitTest);
		// Deal
		ITable testExtractIncome = dataSetTest.getTable("testGetTargetDeal");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Deal deal = new Deal(srk, calcMonitor, dealId, copyId);
		newPropertyNumberCheckDigitTest.getTarget(deal, calcMonitor);
		// Property
		

		status = true;
		assertTrue(status);
	}





}
