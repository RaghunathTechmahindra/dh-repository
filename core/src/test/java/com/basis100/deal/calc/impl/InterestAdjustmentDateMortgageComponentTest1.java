package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;

public class InterestAdjustmentDateMortgageComponentTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private InterestAdjustmentDateMortgageComponent interestAdjustmentDateMortgageComponent;

	public InterestAdjustmentDateMortgageComponentTest1(String name)
			throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InterestAdjustmentDateMortgageComponent.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interestAdjustmentDateMortgageComponent = new InterestAdjustmentDateMortgageComponent();

	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateMortgageComponent);
		ComponentMortgage componentMortgage = new ComponentMortgage(srk, componentId,copyId);
		interestAdjustmentDateMortgageComponent.doCalc(componentMortgage);
		status = true;
		assertTrue(status);
	}

	public void testgetTargetForComponentMortgage() throws Exception {
		boolean status = false;
		ITable testgetTargetForComponentMortgage = dataSetTest.getTable("testgetTargetForComponentMortgage");
		String id = (String) testgetTargetForComponentMortgage.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testgetTargetForComponentMortgage.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateMortgageComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentMortgage componentMortgage = new ComponentMortgage(srk,
				componentId, copyId);
		interestAdjustmentDateMortgageComponent.getTarget(componentMortgage, calcMonitor);
		status = true;
		assertTrue(status);
	}
	public void testgetTargetForDeal() throws Exception {
		boolean status = false;
		ITable testgetTargetForDeal = dataSetTest.getTable("testgetTargetForDeal");
		String id = (String) testgetTargetForDeal.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTargetForDeal.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(interestAdjustmentDateMortgageComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor, dealId, copyId);
		interestAdjustmentDateMortgageComponent.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
