package com.basis100.deal.calc.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class CombinedLTVTestDB extends FXDBTestCase {
	private IDataSet dataSetTest;
	private CombinedLTV combinedLTV;
	private Deal deal;
	private CalcMonitor dcm;
	private Property property;

	public CombinedLTVTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				CombinedLTV.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcm = CalcMonitor.getMonitor(srk);
		this.combinedLTV = new CombinedLTV();
	}

	public void testGetTarget() throws Exception {
		ITable testGetTarget = dataSetTest.getTable("testGetTarget");
		int copyId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COPYID"));
		int dealId = Integer.parseInt((String) testGetTarget
				.getValue(0, "DEAL"));
		int propertyId = Integer.parseInt((String) testGetTarget.getValue(0,
				"PROPERTYID"));
		deal = new Deal(srk, dcm, dealId, copyId);
		property = new Property(srk, dcm, propertyId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		setEntityCacheAndSession();
		getDealCalc().getTarget(deal, dcm);
		getDealCalc().getTarget(property, dcm);
		assert combinedLTV.getTargets() != null;
		assert combinedLTV.getTargets().size() >= 1;
	}

	public void testDoCalc() throws Exception {
		try {
			srk.beginTransaction();
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");
			int dealId = Integer.parseInt((String) testDoCalc.getValue(0,
					"DEAL"));
			int copyId = Integer.parseInt((String) testDoCalc.getValue(0,
					"COPYID"));
			int propertyId = Integer.parseInt((String) testDoCalc.getValue(0,
					"PROPERTYID"));
			deal = new Deal(srk, dcm, dealId, copyId);
			property = new Property(srk, dcm, propertyId, copyId);
			srk.getExpressState().setDealInstitutionId(1);
			setEntityCacheAndSession();
			// Trigger the Calculation
			getDealCalc().doCalc(deal);
			getDealCalc().getTarget(property, dcm);
			assert combinedLTV.getCalcNumber()!=null;
			assert combinedLTV.getCalcNumber().length() >=0;
			srk.rollbackTransaction();
		} catch (Exception ex) {
			throw ex;
		}

	}

	public void setEntityCacheAndSession() {
		Class classCalc = combinedLTV.getClass();
		Class superClassofCalc = classCalc.getSuperclass();
		Field ff[] = superClassofCalc.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase("entityCache")) {
				Class fieldClasstest = field.getType();
				System.out.println(fieldClasstest.getName());
				Constructor con[] = fieldClasstest.getDeclaredConstructors();
				for (Constructor conss : con) {
					conss.setAccessible(true);
					System.out.println(conss.getModifiers() + " "
							+ conss.getName());

					conss.setAccessible(true);
					CalcEntityCache cache;
					try {
						cache = (CalcEntityCache) conss
								.newInstance(EntityTestUtil.getCalcMonitor());
						field.set(combinedLTV, cache);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				break;
			}
		}

		combinedLTV.setResourceKit(EntityTestUtil.getSessionResourceKit());
	}

	public CombinedLTV getDealCalc() {
		return combinedLTV;
	}

}