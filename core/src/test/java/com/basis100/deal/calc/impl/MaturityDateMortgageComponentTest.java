package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: MaturityDateMortgageComponentTest
 * </p>
 * <p>
 * Description: Unit test class for Maturity Date Mortgage Component calculation
 * for the Component Mortgage.
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.2 17-Jun-2008 Initial version
 */
public class MaturityDateMortgageComponentTest extends BaseCalcTest
{

    Component componentDB = null;

    ComponentMortgage componentMortgageDB = null;

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentMortgage componentMortgageFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 17-Jun-2008 Initial version
     */
    public MaturityDateMortgageComponentTest()
    {
        super("com.basis100.deal.calc.impl.MaturityDateMortgageComponent");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        componentMortgageFromExcel = (ComponentMortgage) loadTestDataObject("com.basis100.deal.entity.ComponentMortgage");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 17-Jun-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // Create Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel
                        .getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        // Create Component Mortgage Object
        componentMortgageDB = (ComponentMortgage) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentMortgage");

        componentMortgageDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        componentMortgageDB.setPaymentFrequencyId(componentMortgageFromExcel.getPaymentFrequencyId());
        componentMortgageDB.setPrePaymentOptionsId(componentMortgageFromExcel.getPrePaymentOptionsId());
        componentMortgageDB.setPrivilegePaymentId(componentMortgageFromExcel.getPrivilegePaymentId());
        componentMortgageDB.setCashBackAmountOverride(componentMortgageFromExcel.getCashBackAmountOverride());
        componentMortgageDB.setRateLock(componentMortgageFromExcel.getRateLock());
        componentMortgageDB.setInterestAdjustmentDate(componentMortgageFromExcel.getInterestAdjustmentDate());
        componentMortgageDB.setActualPaymentTerm(componentMortgageFromExcel.getActualPaymentTerm());
        componentMortgageDB.ejbStore();

    }

    /**
     * <p>
     * Description :This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 17-Jun-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*

        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(componentMortgageDB);
            // Assert the Data
            
            String dateDB = formatDateJExcel(componentMortgageDB.getMaturityDate());
            String dateExcel = formatDateJExcel(componentMortgageFromExcel.getMaturityDate());
            assertEquals(dateDB, dateExcel);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            tearDownTestData();
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 17-Jun-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }

    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 17-Jun-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(MaturityDateMortgageComponentTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 12-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 12-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 12-Jun-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 12-Jun-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentMortgage test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 12-Jun-2008 Initial Version
     * @return ComponentMortgage - ComponentMortgage Entity object
     */
    public ComponentMortgage getComponentMortgageFromExcel()
    {
        return componentMortgageFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentMortgage data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.2 12-Jun-2008 Initial Version
     * @param componentMortgageFromExcel -
     *            Component data object
     */
    public void setComponentMortgageFromExcel(
            ComponentMortgage componentMortgageFromExcel)
    {
        this.componentMortgageFromExcel = componentMortgageFromExcel;
    }
}
