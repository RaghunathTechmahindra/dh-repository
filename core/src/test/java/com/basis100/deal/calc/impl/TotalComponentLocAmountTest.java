package com.basis100.deal.calc.impl;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: TotalComponentLocAmountTest
 * </p>
 * <p>
 * Description: Unit test class for Total Component Loc Amount calculation for
 * the Component LOC.
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.11 11-Jun-2008 Initial version
 */
public class TotalComponentLocAmountTest extends BaseCalcTest
{

    Component componentDB = null;

    ComponentPK componentPK = null;

    ComponentLOC compLOCDB = null;

    ComponentLOCPK compLOCPK = null;

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentLOC compLOCFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 11-Jun-2008 Initial version
     */
    public TotalComponentLocAmountTest()
    {
        super("com.basis100.deal.calc.impl.TotalComponentLocAmount");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compLOCFromExcel = (ComponentLOC) loadTestDataObject("com.basis100.deal.entity.ComponentLOC");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 11-Jun-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setMIUpfront(dealFromExcel.getMIUpfront());
        deal.setMIPremiumAmount(dealFromExcel.getMIPremiumAmount());
        deal.forceChange("MIPremiumAmount");
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // CReate Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        componentPK = new ComponentPK(componentDB.getComponentId(), componentDB
                .getCopyId());
        // Create Component LOC Object
        compLOCDB = (ComponentLOC) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLOC");
        compLOCDB.create(componentDB.getComponentId(), componentDB.getCopyId());
        compLOCDB.setPaymentFrequencyId(compLOCFromExcel.getPaymentFrequencyId());
        compLOCDB.setPrePaymentOptionsId(compLOCFromExcel.getPrePaymentOptionsId());
        compLOCDB.setPrivilegePaymentId(compLOCFromExcel.getPrivilegePaymentId());
        compLOCDB.setLocAmount(compLOCFromExcel.getLocAmount());
        compLOCDB.setMiAllocateFlag(compLOCFromExcel.getMiAllocateFlag());
        compLOCDB.ejbStore();

        compLOCPK = new ComponentLOCPK(componentDB.getComponentId(),
                componentDB.getCopyId());
    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 11-Jun-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            setEntityCacheAndSession();
            getDealCalc().doCalc(compLOCDB);
            assertEquals(compLOCDB.getTotalLocAmount(), compLOCFromExcel
                    .getTotalLocAmount(), 0.0);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 11-Jun-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {

            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 11-Jun-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(TotalComponentLocAmountTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 12-Jun-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 12-Jun-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 12-Jun-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * 
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 12-Jun-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLOC test data entity
     * </p>
     * 
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 12-Jun-2008 Initial Version
     * @return ComponentLOC - ComponentLOC Entity object
     */
    public ComponentLOC getCompLOCFromExcel()
    {
        return compLOCFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLOC data entity
     * </p>
     * 
     * @author MCM Impl Team
     * @version 1.0 XS_11.11 12-Jun-2008 Initial Version
     * @param compLOCFromExcel -
     *            Component LOC data object
     */
    public void setCompLOCFromExcel(ComponentLOC compLOCFromExcel)
    {
        this.compLOCFromExcel = compLOCFromExcel;
    }

}
