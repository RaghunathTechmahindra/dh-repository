package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;

public class PerDiemInterestComponentLoanTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private PerDiemInterestComponentLoan perDiemInterestComponentLoan;

	public PerDiemInterestComponentLoanTest1(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PerDiemInterestComponentLoan.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.perDiemInterestComponentLoan = new PerDiemInterestComponentLoan();
	}

	@Test
	public void testDoCalc() throws Exception {
		setEntityCacheAndSession(perDiemInterestComponentLoan);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);				
		ComponentLoan componentLoan=new ComponentLoan(srk,calcMonitor,componentId, copyId);
		perDiemInterestComponentLoan.doCalc(componentLoan);
		status = true;
		assertTrue(status);
	}

	@Test
	public void testGetTarget() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"COMPONENTID");
		int componentId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
		setEntityCacheAndSession(perDiemInterestComponentLoan);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ComponentLoan componentLoan=new ComponentLoan(srk,calcMonitor,componentId, copyId);
		perDiemInterestComponentLoan.getTarget(componentLoan,calcMonitor);
		//Deal
		ITable testExtractDeal = dataSetTest.getTable("testGetTargetDeal");
		String id1 = (String) testExtractDeal.getValue(0, "DEALID");
		int dealId1 = Integer.parseInt(id1);
		String copy1 = (String) testExtractDeal.getValue(0, "COPYID");
		int copyId1 = Integer.parseInt(copy1);
		Deal deal = new Deal(srk, calcMonitor, dealId1, copyId1);
		perDiemInterestComponentLoan.getTarget(deal,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
