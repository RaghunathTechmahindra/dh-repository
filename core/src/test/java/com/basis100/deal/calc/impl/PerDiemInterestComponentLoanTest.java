/*
 * @(#)PerDiemInterestComponentLoanTest.java July 02, 2008 Copyright (C) 2008
 * Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.calc.impl;

import junit.framework.Test;
import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.ltx.unittest.base.BaseCalcTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * Title: PerDiemInterestComponentLoanTest
 * </p>
 * <p>
 * Description: Unit test class for calculating the amount of interest that is
 * to be charged for "each day" in the Interest Adjustment period on a Loan
 * component of a "component eligible" deal.
 * </p>
 * @author MCM Impl Team
 * @version 1.0 XS_11.14 02-July-2008 Initial version
 */
public class PerDiemInterestComponentLoanTest extends BaseCalcTest
{

    Component componentDB = null;

    ComponentLoan compLoanDB = null;

    private Deal dealFromExcel = null;

    private Component componentFromExcel = null;

    private ComponentLoan compLoanFromExcel = null;

    /**
     * <p>
     * Description: Default Constructor creates the entity objects
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     */
    public PerDiemInterestComponentLoanTest()
    {
        super("com.basis100.deal.calc.impl.PerDiemInterestComponentLoan");
        dealFromExcel = (Deal) loadTestDataObject("com.basis100.deal.entity.Deal");
        componentFromExcel = (Component) loadTestDataObject("com.basis100.deal.entity.Component");
        compLoanFromExcel = (ComponentLoan) loadTestDataObject("com.basis100.deal.entity.ComponentLoan");
    }

    /**
     * <p>
     * Description: This method will create test data in Data Base
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     * @throws Exception
     */
    public void setUpTestData() throws Exception
    {

        EntityTestUtil.getInstance().getSessionResourceKit().getExpressState()
                .setDealInstitutionId(dealFromExcel.getInstitutionProfileId());

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                dealFromExcel.getInstitutionProfileId());

        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setLenderProfileId(dealFromExcel.getLenderProfileId());
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        // CReate Component Object
        componentDB = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentDB.create(dealPK.getId(), dealPK.getCopyId(),
                componentFromExcel.getComponentTypeId(), componentFromExcel.getMtgProdId());
        componentDB.setPricingRateInventoryId(componentFromExcel.getPricingRateInventoryId());
        componentDB.setRepaymentTypeId(componentFromExcel.getRepaymentTypeId());
        componentDB.ejbStore();

        // Create Component Loan Object
        compLoanDB = (ComponentLoan) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLoan");
        compLoanDB.create(componentDB.getComponentId(),componentDB.getCopyId());
        compLoanDB.setPaymentFrequencyId(compLoanFromExcel.getPaymentFrequencyId());

        compLoanDB.setLoanAmount(compLoanFromExcel.getLoanAmount());
        compLoanDB.setNetInterestRate(compLoanFromExcel.getNetInterestRate());
        compLoanDB.ejbStore();
    }

    /**
     * <p>
     * Description : This method performs the below steps 1. Calls
     * setUpTestData() to setup data in DB 2. Sets values from Excel into DB 3.
     * Call the CalcMonitor/Calculation Engine 4. Assert for equality 5. Delete
     * test data created in set-up section and reset's the Entity Cache to null
     * </p> *
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     * @throws Exception
     */

    public void testDoCalc() throws Exception
    {/*
        try
        {
            setUpTestData();
            // Base class method set's the EntityCache and Session Resource Kit
            // to the DealCalc
            setEntityCacheAndSession();
            // Trigger the Calculation
            getDealCalc().doCalc(compLoanDB);
            assertEquals(compLoanDB.getPerDiemInterestAmount(),
                    compLoanFromExcel.getPerDiemInterestAmount(), 0.0);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            tearDownTestData();
            // Base class method clear's the EntityCache in the DealCalc
            clearEntityCache();
        }
    */}

    /**
     * <p>
     * Description: Method to remove test data from DB
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     * @throws Exception
     */
    public void tearDownTestData() throws Exception
    {
        try
        {
            if (deal != null)
            {
                deal.dcm = null;
            }
            if (componentDB != null)
            {
                componentDB.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * <p>
     * Description: This method invokes the createSuite() of DDStepsSuiteFactory
     * class.
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.14 02-July-2008 Initial version
     */

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(PerDiemInterestComponentLoanTest.class);
    }

    /**
     * <p>
     * Description: Returns the Component test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 02-July-2008 Initial Version
     * @return Component - Component Entity object
     */
    public Component getComponentFromExcel()
    {
        return componentFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Component data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 2-July-2008 Initial Version
     * @param componentFromExcel -
     *            Component data object
     */
    public void setComponentFromExcel(Component componentFromExcel)
    {
        this.componentFromExcel = componentFromExcel;
    }

    /**
     * <p>
     * Description: Returns the deal test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 02-July-2008 Initial Version
     * @return Deal - Deal Entity object
     */
    public Deal getDealFromExcel()
    {
        return dealFromExcel;
    }

    /**
     * <p>
     * Description: Sets the Deal data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 02-July-2008 Initial Version
     * @param dealFromExcel -
     *            Deal data object
     */
    public void setDealFromExcel(Deal dealFromExcel)
    {
        this.dealFromExcel = dealFromExcel;
    }

    /**
     * <p>
     * Description: Returns the ComponentLoan test data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 02-July-2008 Initial Version
     * @return ComponentLoan - ComponentLoan Entity object
     */
    public ComponentLoan getCompLoanFromExcel()
    {
        return compLoanFromExcel;
    }

    /**
     * <p>
     * Description: Sets the ComponentLoan data entity
     * </p>
     * @author MCM Impl Team
     * @version 1.0 XS_11.6 02-July-2008 Initial Version
     * @param compLoanFromExcel -
     *            Component Loan data object
     */
    public void setCompLoanFromExcel(ComponentLoan compLoanFromExcel)
    {
        this.compLoanFromExcel = compLoanFromExcel;
    }

}
