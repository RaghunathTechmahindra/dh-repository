package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.PropertyExpense;

public class BorrowerTDS3YearTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private BorrowerTDS3Year borrowerTDS3Year;

	public BorrowerTDS3YearTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BorrowerTDS3Year.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.borrowerTDS3Year = new BorrowerTDS3Year();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"BORROWERID");
		int borrowerId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(borrowerTDS3Year);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Borrower borrower=new Borrower(srk,calcMonitor,borrowerId, copyId);
		borrowerTDS3Year.doCalc(borrower);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(borrowerTDS3Year);
          //Liability
          ITable testExtract = dataSetTest.getTable("testGetTargetLiability");                         
          String id=(String)testExtract.getValue(0,"LIABILITYID");
          int liablityId=Integer.parseInt(id);
          String copy=(String)testExtract.getValue(0,"COPYID");
          int copyId=Integer.parseInt(copy);
          Liability liability=new Liability(srk,calcMonitor,liablityId, copyId);
          borrowerTDS3Year.getTarget(liability,calcMonitor);
          //Income
          ITable testExtractIncome = dataSetTest.getTable("testGetTargetIncome");
          String id1=(String)testExtractIncome.getValue(0,"INCOMEID");
          int incomeId=Integer.parseInt(id1);
          String copy1=(String)testExtractIncome.getValue(0,"COPYID");
          int copyId1=Integer.parseInt(copy1);
          Income income=new Income(srk,calcMonitor,incomeId, copyId1);
          borrowerTDS3Year.getTarget(income,calcMonitor);
          //PropertyExpense
          ITable testExtractPropertyExpense = dataSetTest.getTable("testGetTargetPropertyExpense");                      
          String id2=(String)testExtractPropertyExpense.getValue(0,"PROPERTYEXPENSEID");
          int propertyExpenseId=Integer.parseInt(id2);
          String copy2=(String)testExtractPropertyExpense.getValue(0,"COPYID");
          int copyId2=Integer.parseInt(copy2);
          PropertyExpense propertyExpense=new PropertyExpense(srk,calcMonitor,propertyExpenseId, copyId2);
          borrowerTDS3Year.getTarget(propertyExpense,calcMonitor);
          //Deal
          ITable testExtractDeal = dataSetTest.getTable("testGetTargetDeal");                          
          String id3=(String)testExtractDeal.getValue(0,"DEALID");
          int dealId=Integer.parseInt(id3);
          String copy3=(String)testExtractDeal.getValue(0,"COPYID");
          int copyId3=Integer.parseInt(copy3);
          Deal deal=new Deal(srk,calcMonitor,dealId, copyId3);
          borrowerTDS3Year.getTarget(deal,calcMonitor);
          status = true;
          assertTrue(status);
    }


}
