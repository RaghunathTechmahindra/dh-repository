/*
 * @(#) InterimInterestAmountTest 2007-10-10
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.calc.impl;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.DealCalcException;

import com.basis100.deal.DealTestBase;
import com.filogix.express.test.Helper;

/**
 * <p>InterimInterestAmountTest</p>
 * Express Entity class unit test: InterimInterestAmount
 */
public class InterimInterestAmountTest extends DealTestBase {

    private final static Logger _log = LoggerFactory
    .getLogger(InterimInterestAmountTest.class);
    // the calculator.
    private CalcMonitor _calcMonitor;
    
    public InterimInterestAmountTest() {
    }
    
    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception{

        super.setUp();
       
        try {
            _calcMonitor = CalcMonitor.getMonitor(_srk);
        } catch (Exception e) {
            _log.error("New InterimInterestAmount error: ", e);
        }

        _log.info("Testing InterimInterestAmount ...");
    }

    /**
     * test the do calculation.
     */
    @Test
    public void testDoCalc() throws Exception {

        _log.debug("Tesing [doCalc]");

        try {
        for (Iterator i = _testingDeals.iterator(); i.hasNext(); ) {
            
            Deal aDeal = (Deal) i.next();
            _log.info("------ Calc InterimInterestAmount for Deal: ");
            _log.info(Helper.dealStr4CalcInInterimInterestAmount(aDeal));
            _log.info("------ here ------ ");
            aDeal.forceChange("NetInterestRate");
            _calcMonitor.inputEntity(aDeal);
            _calcMonitor.calc();
            _log.info("Calculation Result: --");
            _log.info(Helper.dealStr4CalcInInterimInterestAmount(aDeal));
        }
        } catch (DealCalcException dce) {
            _log.error("Deal calc exception: ", dce);
        }
    }
}
