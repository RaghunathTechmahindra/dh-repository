package com.basis100.deal.calc.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class IADAmountTestDB extends FXDBTestCase {
	private IDataSet dataSetTest;
	private IADAmount amount = null;;
	private Deal deal;
	private CalcMonitor dcm;
	private ComponentMortgage cMortgage;

	public IADAmountTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				IADAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcm = CalcMonitor.getMonitor(srk);
		this.amount = new IADAmount();
	}

	public void testGetTarget() throws Exception {
		ITable testGetTarget = dataSetTest.getTable("testGetTarget");
		int dealId = Integer.parseInt((String) testGetTarget.getValue(0,
				"DEALID"));		
		int copyId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COPYID"));
		deal = new Deal(srk, dcm, dealId, copyId);		
		srk.getExpressState().setDealInstitutionId(1);
		setEntityCacheAndSession();
		getDealCalc().getTarget(deal, dcm);
		assert amount.getTargets() != null;
		assert amount.getTargets().size() >= 1;

	}	

	public void setEntityCacheAndSession() {
		Class classCalc = amount.getClass();
		Class superClassofCalc = classCalc.getSuperclass();
		Field ff[] = superClassofCalc.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase("entityCache")) {
				Class fieldClasstest = field.getType();
				System.out.println(fieldClasstest.getName());
				Constructor con[] = fieldClasstest.getDeclaredConstructors();
				for (Constructor conss : con) {
					conss.setAccessible(true);
					System.out.println(conss.getModifiers() + " "
							+ conss.getName());

					conss.setAccessible(true);
					CalcEntityCache cache;
					try {
						cache = (CalcEntityCache) conss
								.newInstance(EntityTestUtil.getCalcMonitor());
						field.set(amount, cache);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				break;
			}
		}

		amount.setResourceKit(EntityTestUtil.getSessionResourceKit());
	}

	public IADAmount getDealCalc() {
		return amount;
	}

}