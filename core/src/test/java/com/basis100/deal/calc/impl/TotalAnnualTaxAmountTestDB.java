package com.basis100.deal.calc.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class TotalAnnualTaxAmountTestDB extends FXDBTestCase {
	private IDataSet dataSetTest;
	private TotalAnnualTaxAmount totalAnnualTaxAmount = null;;
	private Property property;
	private CalcMonitor dcm;
	private Deal deal;
	private PropertyExpense propertyExpense;

	public TotalAnnualTaxAmountTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				TotalAnnualTaxAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcm = CalcMonitor.getMonitor(srk);
		this.totalAnnualTaxAmount = new TotalAnnualTaxAmount();
	}

	/*public void testGetTarget() throws Exception {
		ITable testGetTarget = dataSetTest.getTable("testGetTarget");

		int propertyExpenseId = Integer.parseInt((String) testGetTarget.getValue(0,
				"PROPERTYEXPENSEID"));
		int copyId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COPYID"));
		propertyExpense = new PropertyExpense(srk, dcm, propertyExpenseId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		setEntityCacheAndSession();
		getDealCalc().getTarget(propertyExpense, dcm);
		assert totalAnnualTaxAmount.getTargets() != null;
		assert totalAnnualTaxAmount.getTargets().size() >= 1;

	}

	public void testDoCalc() throws Exception {
		try {
			srk.beginTransaction();
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");
			int propertyId = Integer.parseInt((String) testDoCalc.getValue(0,
					"PROPERTYID"));
			int copyId = Integer.parseInt((String) testDoCalc.getValue(0,
					"COPYID"));
			property = new Property(srk, dcm, propertyId, copyId);
			srk.getExpressState().setDealInstitutionId(0);
			setEntityCacheAndSession();
			// Trigger the Calculation
			getDealCalc().doCalc(property);
			assert totalAnnualTaxAmount.getCalcNumber() != null;
			assert totalAnnualTaxAmount.getCalcNumber().length() >= 0;
			srk.rollbackTransaction();
		} catch (Exception ex) {
			throw ex;
		}

	}*/
	
	public void testTBR(){
	System.out.println("test");
	 }

	public void setEntityCacheAndSession() {
		Class classCalc = totalAnnualTaxAmount.getClass();
		Class superClassofCalc = classCalc.getSuperclass();
		Field ff[] = superClassofCalc.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase("entityCache")) {
				Class fieldClasstest = field.getType();
				System.out.println(fieldClasstest.getName());
				Constructor con[] = fieldClasstest.getDeclaredConstructors();
				for (Constructor conss : con) {
					conss.setAccessible(true);
					System.out.println(conss.getModifiers() + " "
							+ conss.getName());

					conss.setAccessible(true);
					CalcEntityCache cache;
					try {
						cache = (CalcEntityCache) conss
								.newInstance(EntityTestUtil.getCalcMonitor());
						field.set(totalAnnualTaxAmount, cache);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				break;
			}
		}

		totalAnnualTaxAmount.setResourceKit(EntityTestUtil.getSessionResourceKit());
	}

	public TotalAnnualTaxAmount getDealCalc() {
		return totalAnnualTaxAmount;
	}

}