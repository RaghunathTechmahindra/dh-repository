package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;

public class EquivalentInterestRateTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private EquivalentInterestRate equivalentInterestRate;

	public EquivalentInterestRateTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(EquivalentInterestRate.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.equivalentInterestRate = new EquivalentInterestRate();
	}

	@Test
	public void testDoCalc() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testDoCalc");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		setEntityCacheAndSession(equivalentInterestRate);
		CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
		Deal deal=new Deal(srk,calcMonitor,dealId, copyId);	
		equivalentInterestRate.doCalc(deal);
		status = true;
		assertTrue(status);
	}
	
	@Test
    public void testGetTarget() throws Exception {
          boolean status = false;
          CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
          setEntityCacheAndSession(equivalentInterestRate);
          ITable testExtract = dataSetTest.getTable("testDoCalc");                         
          String id=(String)testExtract.getValue(0,"DEALID");
          int liablityId=Integer.parseInt(id);
          String copy=(String)testExtract.getValue(0,"COPYID");
          int copyId=Integer.parseInt(copy);
          Deal deal=new Deal(srk,calcMonitor,liablityId, copyId);
          equivalentInterestRate.getTarget(deal,calcMonitor);
          status = true;
          assertTrue(status);
    }
}
