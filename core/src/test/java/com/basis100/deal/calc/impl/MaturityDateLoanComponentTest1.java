package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;

public class MaturityDateLoanComponentTest1 extends FXDBTestCase {


	private IDataSet dataSetTest;
	private MaturityDateLoanComponent maturityDateLoanComponent ;

	public MaturityDateLoanComponentTest1(String name) throws IOException,
	DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MaturityDateLoanComponent.class.getSimpleName()+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		maturityDateLoanComponent = new MaturityDateLoanComponent();

	}
	@Test
	public void testDoCalc()throws Exception {
		boolean status = false;
		ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		String id = (String) testDoCalc.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testDoCalc.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(maturityDateLoanComponent);
		ComponentLoan componentLoan=new ComponentLoan(srk, componentId, copyId);
		maturityDateLoanComponent.doCalc(componentLoan);
		status = true;
		assertTrue(status);
	}

	public void testgetTarget()throws Exception {
		boolean status = false;
		ITable testgetTarget = dataSetTest.getTable("testgetTarget");
		String id = (String) testgetTarget.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testgetTarget.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		setEntityCacheAndSession(maturityDateLoanComponent);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		Deal dealEntity=new Deal(srk, calcMonitor,dealId,copyId);
		maturityDateLoanComponent.getTarget(dealEntity,calcMonitor);
		status = true;
		assertTrue(status);
	}
}
