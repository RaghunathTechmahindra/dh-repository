package com.basis100.deal.calc.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentSummary;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class TotalCashBackAmountTestDB extends FXDBTestCase {
	private IDataSet dataSetTest;
	private TotalCashBackAmount totalCashBackAmount = null;;
	private ComponentSummary componentSummary;
	private CalcMonitor dcm;
	private Component component;

	public TotalCashBackAmountTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				TotalCashBackAmount.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcm = CalcMonitor.getMonitor(srk);
		this.totalCashBackAmount = new TotalCashBackAmount();
	}

	/*public void testGetTarget() throws Exception {
		ITable testGetTarget = dataSetTest.getTable("testGetTarget");

		int componentId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COMPONENTID"));
		int copyId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COPYID"));
		component = new Component(srk, dcm, componentId, copyId);
		srk.getExpressState().setDealInstitutionId(1);
		setEntityCacheAndSession();
		getDealCalc().getTarget(component, dcm);
		assert totalCashBackAmount.getTargets() != null;
		assert totalCashBackAmount.getTargets().size() >= 1;

	}

	public void testDoCalc() throws Exception {
		try {
			srk.beginTransaction();
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");
			int dealId = Integer.parseInt((String) testDoCalc.getValue(0,
					"DEALID"));
			int copyId = Integer.parseInt((String) testDoCalc.getValue(0,
					"COPYID"));
			componentSummary = new ComponentSummary(srk, dealId, copyId);
			srk.getExpressState().setDealInstitutionId(1);
			setEntityCacheAndSession();
			// Trigger the Calculation
			getDealCalc().doCalc(componentSummary);
			assert totalCashBackAmount.getCalcNumber() != null;
			assert totalCashBackAmount.getCalcNumber().length() >= 0;
			srk.rollbackTransaction();
		} catch (Exception ex) {
			throw ex;
		}

	}
*/
	public void setEntityCacheAndSession() {
		Class classCalc = totalCashBackAmount.getClass();
		Class superClassofCalc = classCalc.getSuperclass();
		Field ff[] = superClassofCalc.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase("entityCache")) {
				Class fieldClasstest = field.getType();
				System.out.println(fieldClasstest.getName());
				Constructor con[] = fieldClasstest.getDeclaredConstructors();
				for (Constructor conss : con) {
					conss.setAccessible(true);
					System.out.println(conss.getModifiers() + " "
							+ conss.getName());

					conss.setAccessible(true);
					CalcEntityCache cache;
					try {
						cache = (CalcEntityCache) conss
								.newInstance(EntityTestUtil.getCalcMonitor());
						field.set(totalCashBackAmount, cache);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				break;
			}
		}

		totalCashBackAmount.setResourceKit(EntityTestUtil
				.getSessionResourceKit());
	}

	public TotalCashBackAmount getDealCalc() {
		return totalCashBackAmount;
	}
	
	public void testTBR(){
        System.out.println("test");
}


}