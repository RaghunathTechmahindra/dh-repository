package com.basis100.deal.calc.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.ComponentMortgage;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class CashbackMtgComponentTestDB extends FXDBTestCase {
	private IDataSet dataSetTest;
	private CashbackMtgComponent cashBackComponent = null;
	private ComponentMortgage mortgage;
	private CalcMonitor dcm;

	public CashbackMtgComponentTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				CashbackMtgComponent.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcm = CalcMonitor.getMonitor(srk);
		this.cashBackComponent = new CashbackMtgComponent();
	}

	public void testGetTarget() throws Exception {
		ITable testGetTarget = dataSetTest.getTable("testGetTarget");
		int componentId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COMPONENTID"));
		int copyId = Integer.parseInt((String) testGetTarget.getValue(0,
				"COPYID"));

		mortgage = new ComponentMortgage(srk, dcm, componentId, copyId);

		srk.getExpressState().setDealInstitutionId(1);
		setEntityCacheAndSession();
		getDealCalc().getTarget(mortgage, dcm);
		assert cashBackComponent.getTargets() != null;
		assert cashBackComponent.getTargets().size() >= 1;

	}

	public void testDoCalc() throws Exception {
		try {
			srk.beginTransaction();
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");
			int componentId = Integer.parseInt((String) testDoCalc.getValue(0,
					"COMPONENTID"));
			int copyId = Integer.parseInt((String) testDoCalc.getValue(0,
					"COPYID"));
			mortgage = new ComponentMortgage(srk, dcm, componentId, copyId);
			srk.getExpressState().setDealInstitutionId(1);
			setEntityCacheAndSession();
			// Trigger the Calculation
			getDealCalc().doCalc(mortgage);
			assert cashBackComponent.getCalcNumber()!=null;
			assert cashBackComponent.getCalcNumber().length() >=0;
			srk.rollbackTransaction();
		} catch (Exception ex) {
			throw ex;
		}

	}

	public void setEntityCacheAndSession() {
		Class classCalc = cashBackComponent.getClass();
		Class superClassofCalc = classCalc.getSuperclass();
		Field ff[] = superClassofCalc.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase("entityCache")) {
				Class fieldClasstest = field.getType();
				System.out.println(fieldClasstest.getName());
				Constructor con[] = fieldClasstest.getDeclaredConstructors();
				for (Constructor conss : con) {
					conss.setAccessible(true);
					System.out.println(conss.getModifiers() + " "
							+ conss.getName());

					conss.setAccessible(true);
					CalcEntityCache cache;
					try {
						cache = (CalcEntityCache) conss
								.newInstance(EntityTestUtil.getCalcMonitor());
						field.set(cashBackComponent, cache);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				break;
			}
		}

		cashBackComponent
				.setResourceKit(EntityTestUtil.getSessionResourceKit());
	}

	public CashbackMtgComponent getDealCalc() {
		return cashBackComponent;
	}

}