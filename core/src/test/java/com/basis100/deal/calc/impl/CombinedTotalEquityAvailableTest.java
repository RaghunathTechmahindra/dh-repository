package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;

public class CombinedTotalEquityAvailableTest extends FXDBTestCase {


	private IDataSet dataSetTest;	
	private CombinedTotalEquityAvailable combinedTotalEquityAvailable;

	public CombinedTotalEquityAvailableTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource( CombinedTotalEquityAvailable.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		combinedTotalEquityAvailable = new CombinedTotalEquityAvailable();

	}

	@Test
	public void testDoCalc() throws Exception  {
			boolean status = false;
			ITable testDoCalc = dataSetTest.getTable("testDoCalc");	   	   
			String id=(String)testDoCalc.getValue(0,"DEALID");
			int dealId=Integer.parseInt(id);
			String copy=(String)testDoCalc.getValue(0,"COPYID");
			int copyId=Integer.parseInt(copy);
			setEntityCacheAndSession(combinedTotalEquityAvailable);
			CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
			Deal deal=new Deal(srk,calcMonitor,dealId, copyId);		
			combinedTotalEquityAvailable.doCalc(deal);
			status=true;
			assertTrue(status);
		
	}
	@Test	
	public void testGetTarget() throws Exception {
		
			boolean status = false;
			ITable testGetTarget = dataSetTest.getTable("testGetTarget");	   	   
			String id=(String)testGetTarget.getValue(0,"PROPERTYID");
			int propertyId=Integer.parseInt(id);
			String copy=(String)testGetTarget.getValue(0,"COPYID");
			int copyId=Integer.parseInt(copy);
			setEntityCacheAndSession(combinedTotalEquityAvailable);
			CalcMonitor calcMonitor=CalcMonitor.getMonitor(srk);
			Property property=new Property(srk, calcMonitor, propertyId, copyId);		
			combinedTotalEquityAvailable.getTarget(property, calcMonitor);
			status=true;
			assertTrue(status);
		
	}



}
	



