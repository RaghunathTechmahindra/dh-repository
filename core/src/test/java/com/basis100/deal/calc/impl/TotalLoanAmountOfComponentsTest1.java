package com.basis100.deal.calc.impl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;

public class TotalLoanAmountOfComponentsTest1 extends FXDBTestCase {

	private IDataSet dataSetTest;
	private TotalLoanAmountOfComponents totalLoanAmountOfComponents;
	boolean status = false;

	public TotalLoanAmountOfComponentsTest1(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TotalLoanAmountOfComponents.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.totalLoanAmountOfComponents = new TotalLoanAmountOfComponents();
	}

	@Test
	public void testDoCalc() throws Exception {
		setEntityCacheAndSession(totalLoanAmountOfComponents);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		ITable testExtractIncome = dataSetTest.getTable("testDoCalc");
		String id = (String) testExtractIncome.getValue(0, "DEALID");
		int dealId = Integer.parseInt(id);
		String copy = (String) testExtractIncome.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		ComponentSummary componentSummary = new ComponentSummary(srk, dealId, copyId);
		totalLoanAmountOfComponents.doCalc(componentSummary);
		status = true;
		assertTrue(status);
	}

	/*@Test
	public void testGetTarget() throws Exception {
		setEntityCacheAndSession(totalLoanAmountOfComponents);
		CalcMonitor calcMonitor = CalcMonitor.getMonitor(srk);
		//COMPONENT
		ITable testExtract = dataSetTest.getTable("testDoTargetComponent");
		String id = (String) testExtract.getValue(0, "COMPONENTID");
		int componentId = Integer.parseInt(id);
		String copy = (String) testExtract.getValue(0, "COPYID");
		int copyId = Integer.parseInt(copy);
		Component component = new Component(srk, calcMonitor, componentId, copyId);
		totalLoanAmountOfComponents.getTarget(component,calcMonitor);
		//COMPONENTMORTGAGE
		ITable testExtractComponentMortgage = dataSetTest.getTable("testDoTargetComponentMortgage");
		String id1 = (String) testExtractComponentMortgage.getValue(0, "COMPONENTID");
		int componentId1 = Integer.parseInt(id1);
		String copy1 = (String) testExtractComponentMortgage.getValue(0, "COPYID");
		int copyId1 = Integer.parseInt(copy1);
		ComponentMortgage componentMortgage = new ComponentMortgage(srk, calcMonitor, componentId1, copyId1);
		totalLoanAmountOfComponents.getTarget(componentMortgage,calcMonitor);
		
		//COMPONENTLOC
		ITable testExtractComponentLoc = dataSetTest.getTable("testDoTargetComponentLOC");
		String id2 = (String) testExtractComponentLoc.getValue(0, "COMPONENTLOCID");
		int ComponentLOCId = Integer.parseInt(id2);
		String copy2 = (String) testExtractComponentLoc.getValue(0, "COPYID");
		int copyId2 = Integer.parseInt(copy2);
		ComponentLOC componentLOC = new ComponentLOC(srk, calcMonitor, ComponentLOCId, copyId2);
		totalLoanAmountOfComponents.getTarget(componentLOC,calcMonitor);
		//COMPONENTLOAN
		ITable testExtractComponentLoan = dataSetTest.getTable("testDoTargetComponentLoan");
		String id3 = (String) testExtractComponentLoan.getValue(0, "COMPONENTLOANID");
		int componentLoanId = Integer.parseInt(id3);
		String copy3 = (String) testExtractComponentLoan.getValue(0, "COPYID");
		int copyId3 = Integer.parseInt(copy3);
		ComponentLoan componentLoan = new ComponentLoan(srk, calcMonitor, componentLoanId, copyId3);
		totalLoanAmountOfComponents.getTarget(componentLoan,calcMonitor);
		//COMPONENTOVERDRAFT
		ITable testExtractComponentOverdraft = dataSetTest.getTable("testDoTargetComponentOverdraft");
		String id4 = (String) testExtractComponentOverdraft.getValue(0, "COMPONENTOVERDRAFTID");
		int componentOverdraftId = Integer.parseInt(id4);
		String copy4 = (String) testExtractComponentOverdraft.getValue(0, "COPYID");
		int copyId4 = Integer.parseInt(copy4);
		ComponentOverdraft componentOverdraft = new ComponentOverdraft(srk, calcMonitor, componentOverdraftId, copyId4);
		totalLoanAmountOfComponents.getTarget(componentOverdraft,calcMonitor);
		//COMPONENTCREDITCARD
		ITable testExtractComponentCreditCard = dataSetTest.getTable("testDoTargetComponentCreditCard");
		String id5 = (String) testExtractComponentCreditCard.getValue(0, "COMPONENTCREDITCARDID");
		int componentCreditCardId = Integer.parseInt(id5);
		String copy5 = (String) testExtractComponentCreditCard.getValue(0, "COPYID");
		int copyId5 = Integer.parseInt(copy5);
		ComponentCreditCard componentCreditCard = new ComponentCreditCard(srk, calcMonitor, componentCreditCardId, copyId5);
		totalLoanAmountOfComponents.getTarget(componentCreditCard,calcMonitor);
		status = true;
		assertTrue(status);
	}*/
}
