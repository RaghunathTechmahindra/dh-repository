/*
 * @(#)ACMTest.java     Sep 17, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.security;

import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * unit test for ACM
 *
 * @version   1.0  Sep 17, 2007
 * @author    hiro
 * @since     3.3
 */
public class ACMTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _log = LoggerFactory
            .getLogger(ACMTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test method for ACM.PageAccess
     */
    @Test
    public void testPageAccess() throws Exception {

        _log.info(BORDER_START, "testPageAccess");
       
        //prepare data
        int userProfileId = _dataRepository.getInt("USERPROFILE", "USERPROFILEID", 0);
        int userInstitutionId =2;
        int userTypeid = _dataRepository.getInt("USERPROFILE", "USERTYPEID", 0);
        
        int paPageId = _dataRepository.getInt("USERTYPEPAGEACCESS", "PAGEID", 0);
       // _srk.getExpressState().setDealInstitutionId(userInstitutionId);
        // apply institutionId
        _srk.getExpressState().setUserIds(userProfileId, userInstitutionId);
        //test target of testPageAccess
        ACM acm = new ACM(_srk, userTypeid, userProfileId);

        //_srk.getExpressState().setDealInstitutionId(userInstitutionId);
        
        Statement st = _srk.getConnection().createStatement();
        String sql = "select * from USERTYPEPAGEACCESS where " +
        		"USERTYPEID = " + userTypeid + " and PAGEID = " + 
        		paPageId + " and INSTITUTIONPROFILEID = " + userInstitutionId;
        _log.info("sql = " + sql);
        ResultSet rs = st.executeQuery(sql);
       assertTrue(rs.next());       
        int accessType = rs.getInt("ACCESSTYPEID");
        //AccessResult result = acm.pageAccess(paPageId, 0, paInstiturionId);
        AccessResult result = acm.pageAccess(paPageId, 0, userInstitutionId);       
        assertEquals(accessType, result.getAccessTypeID());
        _log.info(BORDER_END, "testPageAccess");
    }
    
}