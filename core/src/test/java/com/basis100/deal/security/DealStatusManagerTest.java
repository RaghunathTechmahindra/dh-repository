package com.basis100.deal.security;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.resources.SessionResourceKit;

public class DealStatusManagerTest extends  FXDBTestCase   {

	private IDataSet dataSetTest;
	private DealStatusManager dealStatusManager;
	
	public DealStatusManagerTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DealStatusManagerDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("DealStatusManagerDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dealStatusManager=DealStatusManager.getInstance(srk);
	}
	public void testGetStatusDescription() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testGetStatusDescription");
		int statusId=Integer.parseInt(testIsLockedByUser.getValue(0, "statusId").toString());
	    String result=dealStatusManager.getStatusDescription(statusId, srk);
		assertEquals("Received",result);
	}
	
     public void testGetStatusDescriptionFailure() throws Exception {
    	ITable testIsLockedByUser = dataSetTest.getTable("testGetStatusDescriptionFailure");
		int statusId=Integer.parseInt(testIsLockedByUser.getValue(0, "statusId").toString());
		String result=dealStatusManager.getStatusDescription(statusId, srk);
		assertEquals("(50) Unknown Deal Status Code",result.trim());
	}
     
     public void testGetStatusCategory() throws Exception{
    	 ITable testIsLockedByUser = dataSetTest.getTable("testGetStatusCategory");
 		int statusId=Integer.parseInt(testIsLockedByUser.getValue(0, "statusId").toString());
 	    int result=dealStatusManager.getStatusCategory(statusId, srk);
 		assertEquals(1,result);
     }
     

     public void testGetStatusCategoryFailure() throws Exception{
    	 ITable testIsLockedByUser = dataSetTest.getTable("testGetStatusCategoryFailure");
 		int statusId=Integer.parseInt(testIsLockedByUser.getValue(0, "statusId").toString());
 		int result=dealStatusManager.getStatusCategory(statusId, srk);
 		assertEquals(0,result);
     }
    
     
}
